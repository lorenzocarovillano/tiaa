#!/bin/ksh

rm ./appo/*
dos2unix ./batch-jobs/*.xml

for pgm in ./batch-jobs/*.xml
do

  export PGMNEW=`echo $pgm | cut -d'/' -f3 `
cat $pgm | . ./naturalbatchlet.awk > ./appo/$PGMNEW

done

cd appo
unix2dos *
cd ..
