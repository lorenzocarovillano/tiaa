/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:11 PM
**        * FROM NATURAL PROGRAM : Ridp630
************************************************************
**        * FILE NAME            : Ridp630.java
**        * CLASS NAME           : Ridp630
**        * INSTANCE NAME        : Ridp630
************************************************************
************************************************************************
** PROGRAM     : RIDP630                                              **
** AUTHOR      : JOANNES C. AVE                                       **
** DATE        : 10/22/2015                                           **
** DESCRIPTION : READS BI QUERY CUSTOM PORTFOLIO RC/RCP EXTRACT AND   **
**               REFORMATS DATA AS FEED TO RIDP632                    **
**               - RDR2016A - PAPER                                   **
**               - RDR2016B - EMAIL                                   **
************************************************************************
** HISTORY:                                                           **
** 12/15/2015  : INITIAL IMPLEMENTATION                               **
** 07/15/2016  : CONSOLIDATE RECORDS UNDER RDR2016A REGARDLESS OF     **
**               DELIVERY PREFERENCE. REMOVE CALL TO CPM FOR EMAIL    **
**               PREFERENCE AS WELL AS CALL TO MDM TO GET NAME AND    **
**               ADDRESS AS CCP WILL OBTAIN THIS FROM MDM AS PART OF  **
**               DCS ELIMINATION                                      **
** 09/09/2017  : PIN EXPANSION                             PINEXP     **
** 05/03/2019  : RETIREPLUS WITH TSV MODIFICATION            CPTSV    **
**               ADDING #TSV-IND IN THE INPUT AND OUTPUT LAYOUT       **
**               ADDING AK, FL AND NY FOR STATE VARIATION             **
**               REMOVING COMMENTED LINE FOR CPM                      **
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridp630 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;
    private PdaPsta9017 pdaPsta9017;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Pin_A12;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Pin_N5;
    private DbsField pnd_Input_Record_Pnd_Pin_N7;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Pin_N12;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cref_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Date;
    private DbsField pnd_Input_Record_Pnd_Issue_State_Cde;
    private DbsField pnd_Input_Record_Pnd_Plan_Cde;
    private DbsField pnd_Input_Record_Pnd_Cover_Letter_Ind;
    private DbsField pnd_Input_Record_Pnd_Tsv_Ind;
    private DbsField pnd_Input_Record_Pnd_Institution_Name;
    private DbsField pnd_Prev_Inst_Name;
    private DbsField pnd_Prev_Pin_N12;

    private DbsGroup pnd_Prev_Pin_N12__R_Field_4;
    private DbsField pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5;
    private DbsField pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7;
    private DbsField pnd_Prev_Contract;
    private DbsField pnd_Prev_Cover;
    private DbsField pnd_Prev_Tsv_Ind;
    private DbsField pnd_K;
    private DbsField pnd_X;
    private DbsField pnd_Valid_Record;
    private DbsField pnd_Accepted;
    private DbsField pnd_Remarks;
    private DbsField pnd_Participant_Name;
    private DbsField pnd_Ctr_Valid_Contract_Read;
    private DbsField pnd_Ctr_Da_Contract_Not_In_Mdm;
    private DbsField pnd_Ctr_Da_Inactive_In_Mdm;
    private DbsField pnd_Ctr_Ph_Deceased;
    private DbsField pnd_Ctr_Ph_With_Mail_Suppression;
    private DbsField pnd_Ctr_Contract_Dispparoved_State;
    private DbsField pnd_Ctr_Contract_Accepted;
    private DbsField pnd_Ctr_Contract_Rejected;
    private DbsField pnd_Ctr_Contract_Read;
    private DbsField pnd_Ctr_Duplicate_Contract;
    private DbsField pnd_Ctr_Terminated_Contract;
    private DbsField pnd_Ctr_Contract_Inactive;
    private DbsField pnd_Ctr_Out_Of_State;
    private DbsField pnd_Ctr_Contract_Outof_Range;
    private DbsField pnd_Ctr_After_Cutoff;
    private DbsField pnd_Ctr_Plan_Not_For_Proc;
    private DbsField pnd_Ctr_Institution_Owned;
    private DbsField pnd_Ctr_Total_Rc_Rcp;
    private DbsField pnd_Break;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_Sum_Tiaa_Cref;

    private DbsGroup pnd_Ctrs;
    private DbsField pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Gn;
    private DbsField pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Il;
    private DbsField pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ak;
    private DbsField pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Fl;
    private DbsField pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ny;
    private DbsField pnd_Ndx;
    private DbsField pnd_Ctr_File1;
    private DbsField pnd_Ctr_File2;

    private DbsGroup pnd_Output;
    private DbsField pnd_Output_Pnd_Pin_A12;

    private DbsGroup pnd_Output__R_Field_5;
    private DbsField pnd_Output_Pnd_Pin_N7;
    private DbsField pnd_Output_Pnd_Pin_A5;
    private DbsField pnd_Output_Pnd_Full_Name;
    private DbsField pnd_Output_Pnd_Address_Line_Txt;
    private DbsField pnd_Output_Pnd_Postal_Data;
    private DbsField pnd_Output_Pnd_Address_Type_Cde;
    private DbsField pnd_Output_Pnd_Letter_Type;
    private DbsField pnd_Output_Pnd_Tsv_Ind;
    private DbsField pnd_Output_Pnd_Ctr_Rc_Rcp_Gn;
    private DbsField pnd_Output_Pnd_Ctr_Rc_Rcp_Il;
    private DbsField pnd_Output_Pnd_Ctr_Rc_Rcp_Ak;
    private DbsField pnd_Output_Pnd_Ctr_Rc_Rcp_Fl;
    private DbsField pnd_Output_Pnd_Ctr_Rc_Rcp_Ny;
    private DbsField pnd_Output_Pnd_Package_Code;
    private DbsField pnd_Output_Pnd_Tiaa_Contracts;
    private DbsField pnd_Output_Pnd_Cref_Contracts;

    private DbsGroup pnd_Output_Pnd_Endorse_Contracts_Gn;
    private DbsField pnd_Output_Pnd_Tiaa_Contracts_Gn;
    private DbsField pnd_Output_Pnd_Cref_Contracts_Gn;

    private DbsGroup pnd_Output_Pnd_Endorse_Contracts_Il;
    private DbsField pnd_Output_Pnd_Tiaa_Contracts_Il;
    private DbsField pnd_Output_Pnd_Cref_Contracts_Il;

    private DbsGroup pnd_Output_Pnd_Endorse_Contracts_Ak;
    private DbsField pnd_Output_Pnd_Tiaa_Contracts_Ak;
    private DbsField pnd_Output_Pnd_Cref_Contracts_Ak;

    private DbsGroup pnd_Output_Pnd_Endorse_Contracts_Fl;
    private DbsField pnd_Output_Pnd_Tiaa_Contracts_Fl;
    private DbsField pnd_Output_Pnd_Cref_Contracts_Fl;

    private DbsGroup pnd_Output_Pnd_Endorse_Contracts_Ny;
    private DbsField pnd_Output_Pnd_Tiaa_Contracts_Ny;
    private DbsField pnd_Output_Pnd_Cref_Contracts_Ny;
    private DbsField pnd_Output_Pnd_Institution_Name;
    private DbsField pnd_Output_Pnd_Email_Address;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);
        pdaPsta9017 = new PdaPsta9017(localVariables);

        // Local Variables
        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 170);

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Pin_A12 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N5 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N5", "#PIN-N5", FieldType.NUMERIC, 5);
        pnd_Input_Record_Pnd_Pin_N7 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N12 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N12", "#PIN-N12", FieldType.NUMERIC, 12);
        pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr", "#TIAA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cref_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cref_Cntrct_Nbr", "#CREF-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cntrct_Issue_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Date", "#CNTRCT-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Issue_State_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Issue_State_Cde", "#ISSUE-STATE-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Plan_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Plan_Cde", "#PLAN-CDE", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cover_Letter_Ind = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cover_Letter_Ind", "#COVER-LETTER-IND", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Tsv_Ind = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Tsv_Ind", "#TSV-IND", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Institution_Name = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Institution_Name", "#INSTITUTION-NAME", 
            FieldType.STRING, 120);
        pnd_Prev_Inst_Name = localVariables.newFieldInRecord("pnd_Prev_Inst_Name", "#PREV-INST-NAME", FieldType.STRING, 120);
        pnd_Prev_Pin_N12 = localVariables.newFieldInRecord("pnd_Prev_Pin_N12", "#PREV-PIN-N12", FieldType.NUMERIC, 12);

        pnd_Prev_Pin_N12__R_Field_4 = localVariables.newGroupInRecord("pnd_Prev_Pin_N12__R_Field_4", "REDEFINE", pnd_Prev_Pin_N12);
        pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5 = pnd_Prev_Pin_N12__R_Field_4.newFieldInGroup("pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5", "#PREV-PIN-N5", FieldType.NUMERIC, 
            5);
        pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7 = pnd_Prev_Pin_N12__R_Field_4.newFieldInGroup("pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7", "#PREV-PIN-N7", FieldType.NUMERIC, 
            7);
        pnd_Prev_Contract = localVariables.newFieldInRecord("pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Cover = localVariables.newFieldInRecord("pnd_Prev_Cover", "#PREV-COVER", FieldType.STRING, 1);
        pnd_Prev_Tsv_Ind = localVariables.newFieldInRecord("pnd_Prev_Tsv_Ind", "#PREV-TSV-IND", FieldType.STRING, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_Valid_Record = localVariables.newFieldInRecord("pnd_Valid_Record", "#VALID-RECORD", FieldType.BOOLEAN, 1);
        pnd_Accepted = localVariables.newFieldInRecord("pnd_Accepted", "#ACCEPTED", FieldType.BOOLEAN, 1);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 40);
        pnd_Participant_Name = localVariables.newFieldInRecord("pnd_Participant_Name", "#PARTICIPANT-NAME", FieldType.STRING, 35);
        pnd_Ctr_Valid_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Valid_Contract_Read", "#CTR-VALID-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Da_Contract_Not_In_Mdm = localVariables.newFieldInRecord("pnd_Ctr_Da_Contract_Not_In_Mdm", "#CTR-DA-CONTRACT-NOT-IN-MDM", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Da_Inactive_In_Mdm = localVariables.newFieldInRecord("pnd_Ctr_Da_Inactive_In_Mdm", "#CTR-DA-INACTIVE-IN-MDM", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_Deceased = localVariables.newFieldInRecord("pnd_Ctr_Ph_Deceased", "#CTR-PH-DECEASED", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_With_Mail_Suppression = localVariables.newFieldInRecord("pnd_Ctr_Ph_With_Mail_Suppression", "#CTR-PH-WITH-MAIL-SUPPRESSION", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Contract_Dispparoved_State = localVariables.newFieldInRecord("pnd_Ctr_Contract_Dispparoved_State", "#CTR-CONTRACT-DISPPAROVED-STATE", 
            FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Accepted = localVariables.newFieldInRecord("pnd_Ctr_Contract_Accepted", "#CTR-CONTRACT-ACCEPTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Rejected = localVariables.newFieldInRecord("pnd_Ctr_Contract_Rejected", "#CTR-CONTRACT-REJECTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Contract_Read", "#CTR-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Duplicate_Contract = localVariables.newFieldInRecord("pnd_Ctr_Duplicate_Contract", "#CTR-DUPLICATE-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Terminated_Contract = localVariables.newFieldInRecord("pnd_Ctr_Terminated_Contract", "#CTR-TERMINATED-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Inactive = localVariables.newFieldInRecord("pnd_Ctr_Contract_Inactive", "#CTR-CONTRACT-INACTIVE", FieldType.NUMERIC, 7);
        pnd_Ctr_Out_Of_State = localVariables.newFieldInRecord("pnd_Ctr_Out_Of_State", "#CTR-OUT-OF-STATE", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Outof_Range = localVariables.newFieldInRecord("pnd_Ctr_Contract_Outof_Range", "#CTR-CONTRACT-OUTOF-RANGE", FieldType.NUMERIC, 
            7);
        pnd_Ctr_After_Cutoff = localVariables.newFieldInRecord("pnd_Ctr_After_Cutoff", "#CTR-AFTER-CUTOFF", FieldType.NUMERIC, 7);
        pnd_Ctr_Plan_Not_For_Proc = localVariables.newFieldInRecord("pnd_Ctr_Plan_Not_For_Proc", "#CTR-PLAN-NOT-FOR-PROC", FieldType.NUMERIC, 7);
        pnd_Ctr_Institution_Owned = localVariables.newFieldInRecord("pnd_Ctr_Institution_Owned", "#CTR-INSTITUTION-OWNED", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Rc_Rcp = localVariables.newFieldInRecord("pnd_Ctr_Total_Rc_Rcp", "#CTR-TOTAL-RC-RCP", FieldType.NUMERIC, 7);
        pnd_Break = localVariables.newFieldInRecord("pnd_Break", "#BREAK", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_Sum_Tiaa_Cref = localVariables.newFieldInRecord("pnd_Sum_Tiaa_Cref", "#SUM-TIAA-CREF", FieldType.NUMERIC, 2);

        pnd_Ctrs = localVariables.newGroupInRecord("pnd_Ctrs", "#CTRS");
        pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Gn = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Gn", "#CTR-RC-RCP-GN", FieldType.NUMERIC, 2);
        pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Il = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Il", "#CTR-RC-RCP-IL", FieldType.NUMERIC, 2);
        pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ak = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ak", "#CTR-RC-RCP-AK", FieldType.NUMERIC, 2);
        pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Fl", "#CTR-RC-RCP-FL", FieldType.NUMERIC, 2);
        pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ny = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ny", "#CTR-RC-RCP-NY", FieldType.NUMERIC, 2);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 2);
        pnd_Ctr_File1 = localVariables.newFieldInRecord("pnd_Ctr_File1", "#CTR-FILE1", FieldType.NUMERIC, 5);
        pnd_Ctr_File2 = localVariables.newFieldInRecord("pnd_Ctr_File2", "#CTR-FILE2", FieldType.NUMERIC, 5);

        pnd_Output = localVariables.newGroupInRecord("pnd_Output", "#OUTPUT");
        pnd_Output_Pnd_Pin_A12 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);

        pnd_Output__R_Field_5 = pnd_Output.newGroupInGroup("pnd_Output__R_Field_5", "REDEFINE", pnd_Output_Pnd_Pin_A12);
        pnd_Output_Pnd_Pin_N7 = pnd_Output__R_Field_5.newFieldInGroup("pnd_Output_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);
        pnd_Output_Pnd_Pin_A5 = pnd_Output__R_Field_5.newFieldInGroup("pnd_Output_Pnd_Pin_A5", "#PIN-A5", FieldType.STRING, 5);
        pnd_Output_Pnd_Full_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Output_Pnd_Address_Line_Txt = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 35, 
            new DbsArrayController(1, 6));
        pnd_Output_Pnd_Postal_Data = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 35);
        pnd_Output_Pnd_Address_Type_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 1);
        pnd_Output_Pnd_Letter_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Tsv_Ind = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Tsv_Ind", "#TSV-IND", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Rc_Rcp_Gn = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Rc_Rcp_Gn", "#CTR-RC-RCP-GN", FieldType.NUMERIC, 2);
        pnd_Output_Pnd_Ctr_Rc_Rcp_Il = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Rc_Rcp_Il", "#CTR-RC-RCP-IL", FieldType.NUMERIC, 2);
        pnd_Output_Pnd_Ctr_Rc_Rcp_Ak = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Rc_Rcp_Ak", "#CTR-RC-RCP-AK", FieldType.NUMERIC, 2);
        pnd_Output_Pnd_Ctr_Rc_Rcp_Fl = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Rc_Rcp_Fl", "#CTR-RC-RCP-FL", FieldType.NUMERIC, 2);
        pnd_Output_Pnd_Ctr_Rc_Rcp_Ny = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Rc_Rcp_Ny", "#CTR-RC-RCP-NY", FieldType.NUMERIC, 2);
        pnd_Output_Pnd_Package_Code = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Output_Pnd_Tiaa_Contracts = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Tiaa_Contracts", "#TIAA-CONTRACTS", FieldType.STRING, 8, new DbsArrayController(1, 
            10));
        pnd_Output_Pnd_Cref_Contracts = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Cref_Contracts", "#CREF-CONTRACTS", FieldType.STRING, 8, new DbsArrayController(1, 
            10));

        pnd_Output_Pnd_Endorse_Contracts_Gn = pnd_Output.newGroupInGroup("pnd_Output_Pnd_Endorse_Contracts_Gn", "#ENDORSE-CONTRACTS-GN");
        pnd_Output_Pnd_Tiaa_Contracts_Gn = pnd_Output_Pnd_Endorse_Contracts_Gn.newFieldArrayInGroup("pnd_Output_Pnd_Tiaa_Contracts_Gn", "#TIAA-CONTRACTS-GN", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Output_Pnd_Cref_Contracts_Gn = pnd_Output_Pnd_Endorse_Contracts_Gn.newFieldArrayInGroup("pnd_Output_Pnd_Cref_Contracts_Gn", "#CREF-CONTRACTS-GN", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));

        pnd_Output_Pnd_Endorse_Contracts_Il = pnd_Output.newGroupInGroup("pnd_Output_Pnd_Endorse_Contracts_Il", "#ENDORSE-CONTRACTS-IL");
        pnd_Output_Pnd_Tiaa_Contracts_Il = pnd_Output_Pnd_Endorse_Contracts_Il.newFieldArrayInGroup("pnd_Output_Pnd_Tiaa_Contracts_Il", "#TIAA-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Output_Pnd_Cref_Contracts_Il = pnd_Output_Pnd_Endorse_Contracts_Il.newFieldArrayInGroup("pnd_Output_Pnd_Cref_Contracts_Il", "#CREF-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));

        pnd_Output_Pnd_Endorse_Contracts_Ak = pnd_Output.newGroupInGroup("pnd_Output_Pnd_Endorse_Contracts_Ak", "#ENDORSE-CONTRACTS-AK");
        pnd_Output_Pnd_Tiaa_Contracts_Ak = pnd_Output_Pnd_Endorse_Contracts_Ak.newFieldArrayInGroup("pnd_Output_Pnd_Tiaa_Contracts_Ak", "#TIAA-CONTRACTS-AK", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Output_Pnd_Cref_Contracts_Ak = pnd_Output_Pnd_Endorse_Contracts_Ak.newFieldArrayInGroup("pnd_Output_Pnd_Cref_Contracts_Ak", "#CREF-CONTRACTS-AK", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));

        pnd_Output_Pnd_Endorse_Contracts_Fl = pnd_Output.newGroupInGroup("pnd_Output_Pnd_Endorse_Contracts_Fl", "#ENDORSE-CONTRACTS-FL");
        pnd_Output_Pnd_Tiaa_Contracts_Fl = pnd_Output_Pnd_Endorse_Contracts_Fl.newFieldArrayInGroup("pnd_Output_Pnd_Tiaa_Contracts_Fl", "#TIAA-CONTRACTS-FL", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Output_Pnd_Cref_Contracts_Fl = pnd_Output_Pnd_Endorse_Contracts_Fl.newFieldArrayInGroup("pnd_Output_Pnd_Cref_Contracts_Fl", "#CREF-CONTRACTS-FL", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));

        pnd_Output_Pnd_Endorse_Contracts_Ny = pnd_Output.newGroupInGroup("pnd_Output_Pnd_Endorse_Contracts_Ny", "#ENDORSE-CONTRACTS-NY");
        pnd_Output_Pnd_Tiaa_Contracts_Ny = pnd_Output_Pnd_Endorse_Contracts_Ny.newFieldArrayInGroup("pnd_Output_Pnd_Tiaa_Contracts_Ny", "#TIAA-CONTRACTS-NY", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Output_Pnd_Cref_Contracts_Ny = pnd_Output_Pnd_Endorse_Contracts_Ny.newFieldArrayInGroup("pnd_Output_Pnd_Cref_Contracts_Ny", "#CREF-CONTRACTS-NY", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Output_Pnd_Institution_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 120);
        pnd_Output_Pnd_Email_Address = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Email_Address", "#EMAIL-ADDRESS", FieldType.STRING, 100);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Valid_Record.setInitialValue(false);
        pnd_Accepted.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridp630() throws Exception
    {
        super("Ridp630");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  EXCEPTION REPORT
        //*  STATISTICS REPORT                                                                                                                                            //Natural: FORMAT ( 1 ) LS = 120 PS = 60
        if (condition(getReports().getAstLinesLeft(1).less(5)))                                                                                                           //Natural: FORMAT ( 2 ) LS = 120 PS = 60;//Natural: FORMAT LS = 80 PS = 60;//Natural: NEWPAGE ( 1 ) IF LESS THAN 5 LINES
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        //*  OPEN MQ QUEUE                                       /* JCA20150324
        getReports().write(0, " FETCHING MDMP011");                                                                                                                       //Natural: WRITE ' FETCHING MDMP011'
        if (Global.isEscape()) return;
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 35T '2016 CUSTOM PORTFOLIO ENDORSEMENT' 84T 'DATE:' *DATU / 43T 'EXCEPTIONS LIST' 84T 'TIME:' *TIME ( EM = XXXXXXXX ) / 84T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) // 2T 'PIN       TIAA NBR   STATE   REMARKS'/ 2T '------------------------------------------------------------'/
        //*  WRITE (2) TITLE LEFT
        //*    'PROGRAM:' *PROGRAM
        //*     32T '2016 CUSTOM PORTFOLIO ENDORSEMENT'
        //*     84T 'DATE:' *DATU /
        //*     42T 'BAD ADDRESS REPORT'
        //*     84T 'TIME:' *TIME (EM=XXXXXXXX) /
        //*     84T 'PAGE:' 3X *PAGE-NUMBER (2) //
        //*  -----------------------------------------------------------------'/
        //*                                                 |RC/RCP  RC/RCP  |'/
        //*   PIN         NAME                              |GENERIC ILLINOIS|'/
        //*  -----------------------------------------------------------------' /
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #INPUT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  PINEXP
            pnd_Ctr_Contract_Read.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CTR-CONTRACT-READ
            //*                                                                                                                                                           //Natural: AT BREAK OF #INPUT-RECORD.#PIN-A12
            //*                                                                                                                                                           //Natural: AT END OF DATA
            if (condition(pnd_Break.getBoolean()))                                                                                                                        //Natural: IF #BREAK
            {
                pnd_Break.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #BREAK
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP
            pnd_Prev_Pin_N12.setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-N12 TO #PREV-PIN-N12
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Institution_Name);                                                                                           //Natural: MOVE #INPUT-RECORD.#INSTITUTION-NAME TO #PREV-INST-NAME
            pnd_Prev_Cover.setValue(pnd_Input_Record_Pnd_Cover_Letter_Ind);                                                                                               //Natural: MOVE #INPUT-RECORD.#COVER-LETTER-IND TO #PREV-COVER
            //*  CPTSV
            pnd_Prev_Tsv_Ind.setValue(pnd_Input_Record_Pnd_Tsv_Ind);                                                                                                      //Natural: MOVE #INPUT-RECORD.#TSV-IND TO #PREV-TSV-IND
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
        }                                                                                                                                                                 //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-STATISTICS-REPORT
        sub_Write_Statistics_Report();
        if (condition(Global.isEscape())) {return;}
        //*  CLOSE MQ QUEUE
        getReports().write(0, "FETCHING MDMP0012");                                                                                                                       //Natural: WRITE 'FETCHING MDMP0012'
        if (Global.isEscape()) return;
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* **********************************************************************
        //*                 S T A R T    O F    S U B R O U T I N E S
        //* **********************************************************************
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-RECORD
        //*  RC/RCP CONTRACTS
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RECORD-IN-MDM
        //*  --------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PIN-STATUS
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-IL-ARRAY
        //*  -------------------------------------------
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-AK-ARRAY
        //*  -------------------------------------------
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-FL-ARRAY
        //*  -------------------------------------------
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-NY-ARRAY
        //*  -------------------------------------------
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-GN-ARRAY
        //*  -------------------------------------------
        //*  ---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-ARRAY
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA-TO-FILE
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-STATISTICS-REPORT
        //* ******************************************
        //*    WRITE (1) // 'TOTAL INSTITUTION-OWNED CONTRACTS :'
        //*              #CTR-INSTITUTION-OWNED (EM=Z,ZZZ,ZZ9)
    }
    private void sub_Validate_Record() throws Exception                                                                                                                   //Natural: VALIDATE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        if (condition(pnd_Prev_Contract.equals(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr)))                                                                                    //Natural: IF #PREV-CONTRACT EQ #TIAA-CNTRCT-NBR
        {
            pnd_Ctr_Duplicate_Contract.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DUPLICATE-CONTRACT
            pnd_Remarks.setValue("DUPLICATE CONTRACT");                                                                                                                   //Natural: ASSIGN #REMARKS := 'DUPLICATE CONTRACT'
            getReports().write(1, pnd_Input_Record_Pnd_Pin_N12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-N12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prev_Contract.setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #PREV-CONTRACT
                                                                                                                                                                          //Natural: PERFORM CHECK-RECORD-IN-MDM
        sub_Check_Record_In_Mdm();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Valid_Record.getBoolean()))                                                                                                                     //Natural: IF #VALID-RECORD
        {
            //*  RC
            //*  RCP
            //*  RC
            //*  RC
            short decideConditionsMet674 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TIAA-CNTRCT-NBR EQ 'F0000000' THRU 'F0999999' OR #TIAA-CNTRCT-NBR EQ 'F5000000' THRU 'F5999999' OR #CREF-CNTRCT-NBR EQ 'H0000000' THRU 'H0999999' OR #CREF-CNTRCT-NBR EQ 'H5000000' THRU 'H5999999'
            if (condition(((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("F0000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("F0999999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("F5000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("F5999999"))) || 
                (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("H0000000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("H0999999"))) || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("H5000000") 
                && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("H5999999")))))
            {
                decideConditionsMet674++;
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                pnd_Ctr_Total_Rc_Rcp.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CTR-TOTAL-RC-RCP
                //*  CPTSV
                if (condition(pnd_Input_Record_Pnd_Cover_Letter_Ind.equals("1")))                                                                                         //Natural: IF #COVER-LETTER-IND = '1'
                {
                    if (condition(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.notEquals(" ") || pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                            //Natural: IF #TIAA-CNTRCT-NBR NE ' ' OR #CREF-CNTRCT-NBR NE ' '
                    {
                        //*  IL
                        if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("016")))                                                                                //Natural: IF #ISSUE-STATE-CDE EQ '016'
                        {
                            pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Il.nadd(1);                                                                                                           //Natural: ADD 1 TO #CTRS.#CTR-RC-RCP-IL
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-IL-ARRAY
                            sub_Add_Contract_To_Il_Array();
                            if (condition(Global.isEscape())) {return;}
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Gn.nadd(1);                                                                                                           //Natural: ADD 1 TO #CTRS.#CTR-RC-RCP-GN
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-GN-ARRAY
                            sub_Add_Contract_To_Gn_Array();
                            if (condition(Global.isEscape())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*   OTHER COVER-LETTER-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  AK
                    short decideConditionsMet694 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUES OF #ISSUE-STATE-CDE;//Natural: VALUE '002'
                    if (condition((pnd_Input_Record_Pnd_Issue_State_Cde.equals("002"))))
                    {
                        decideConditionsMet694++;
                        pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ak.nadd(1);                                                                                                               //Natural: ADD 1 TO #CTRS.#CTR-RC-RCP-AK
                        //*  FL
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-AK-ARRAY
                        sub_Add_Contract_To_Ak_Array();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: VALUE '011'
                    else if (condition((pnd_Input_Record_Pnd_Issue_State_Cde.equals("011"))))
                    {
                        decideConditionsMet694++;
                        pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Fl.nadd(1);                                                                                                               //Natural: ADD 1 TO #CTRS.#CTR-RC-RCP-FL
                        //*  NY
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-FL-ARRAY
                        sub_Add_Contract_To_Fl_Array();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: VALUE '035'
                    else if (condition((pnd_Input_Record_Pnd_Issue_State_Cde.equals("035"))))
                    {
                        decideConditionsMet694++;
                        pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ny.nadd(1);                                                                                                               //Natural: ADD 1 TO #CTRS.#CTR-RC-RCP-NY
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-NY-ARRAY
                        sub_Add_Contract_To_Ny_Array();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: NONE VALUES
                    else if (condition())
                    {
                        pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Gn.nadd(1);                                                                                                               //Natural: ADD 1 TO #CTRS.#CTR-RC-RCP-GN
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-GN-ARRAY
                        sub_Add_Contract_To_Gn_Array();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*  (2670) COVER-LETTER-IND = '1'     CPTSV <<<
                }                                                                                                                                                         //Natural: END-IF
                //*  062707 J.AVE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Ctr_Contract_Outof_Range.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CTR-CONTRACT-OUTOF-RANGE
                pnd_Remarks.setValue("CONTRACT OUT OF RANGE");                                                                                                            //Natural: ASSIGN #REMARKS := 'CONTRACT OUT OF RANGE'
                getReports().write(1, pnd_Input_Record_Pnd_Pin_N12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-N12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE-RECORD
    }
    private void sub_Check_Record_In_Mdm() throws Exception                                                                                                               //Natural: CHECK-RECORD-IN-MDM
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------
        //*  FOR PINEXP MODIFY MDM*210 TO MDM*211
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #MDMA211.#I-CONTRACT-NUMBER
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue("01");                                                                                                   //Natural: MOVE '01' TO #MDMA211.#I-PAYEE-CODE-A2
        //*    WRITE 'CALLING MDMN211A TO GET CONTRACT INFORMATION'
        //*  JCA20150324
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //*      WRITE '=' #MDMA211.#O-RETURN-CODE
        //*  JCA20150324
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA211.#O-RETURN-CODE NE '0000'
        {
            pnd_Remarks.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Text());                                                                                          //Natural: ASSIGN #REMARKS := #MDMA211.#O-RETURN-TEXT
            getReports().write(1, pnd_Input_Record_Pnd_Pin_N12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-N12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Contract_Not_In_Mdm.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-DA-CONTRACT-NOT-IN-MDM
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals(" ") || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("H")         //Natural: IF NOT ( #MDMA211.#O-CONTRACT-STATUS-CODE = ' ' OR = 'H' OR = 'Y' )
            || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("Y"))))
        {
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("CONTRACT INACTIVE IN COR - STATUS CODE:", pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code()));                    //Natural: COMPRESS 'CONTRACT INACTIVE IN COR - STATUS CODE:' #MDMA211.#O-CONTRACT-STATUS-CODE TO #REMARKS
            getReports().write(1, pnd_Input_Record_Pnd_Pin_N12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-N12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Inactive_In_Mdm.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DA-INACTIVE-IN-MDM
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PIN-STATUS
            sub_Determine_Pin_Status();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Accepted.getBoolean()))                                                                                                                     //Natural: IF #ACCEPTED
            {
                pnd_Valid_Record.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*    END-READ
        //*  CHECK-RECORD-IN-MDM
    }
    private void sub_Determine_Pin_Status() throws Exception                                                                                                              //Natural: DETERMINE-PIN-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------
        //*  MODIFY MDM*100 TO MDM*101                             /* PINEXP
        //*  JCA20150324    /* PINEXP >>>
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        //*    MOVE #INPUT-RECORD.#PIN TO #MDMA100.#I-PIN          /* JCA20150324
        if (condition(pnd_Input_Record_Pnd_Pin_A12.notEquals(" ")))                                                                                                       //Natural: IF #INPUT-RECORD.#PIN-A12 NE ' '
        {
            if (condition(pnd_Input_Record_Pnd_Pin_N5.equals(getZero())))                                                                                                 //Natural: IF #INPUT-RECORD.#PIN-N5 = 0
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N7);                                                                          //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #INPUT-RECORD.#PIN-N7
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                         //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #INPUT-RECORD.#PIN-N12
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP <<<
        }                                                                                                                                                                 //Natural: END-IF
        //*    WRITE 'CALLING MDMN101A TO GET ADDRESS/NAME'
        //*  JCA20150324
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  REJECT DECEASED PARTICIPANTS
        //*  JCA20150324
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero())))                                                                                //Natural: IF #MDMA101.#O-DATE-OF-DEATH GT 0
        {
            pnd_Ctr_Ph_Deceased.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CTR-PH-DECEASED
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("P/H DECEASED - DOD:", pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death()));                                               //Natural: COMPRESS 'P/H DECEASED - DOD:' #MDMA101.#O-DATE-OF-DEATH TO #REMARKS
            getReports().write(1, pnd_Input_Record_Pnd_Pin_N12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-N12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Accepted.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #ACCEPTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  REJECT PARTICIPANTS W/ STOP ALL MAILING SUPPRESSION CODE
            //*  JCA20150324
            DbsUtil.examine(new ExamineSource(pdaMdma101.getPnd_Mdma101_Pnd_O_Mail_Code().getValue("*")), new ExamineSearch("01"), new ExamineGivingPosition(pnd_I));     //Natural: EXAMINE #MDMA101.#O-MAIL-CODE ( * ) FOR '01' GIVING POSITION #I
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I GT 0
            {
                pnd_Ctr_Ph_With_Mail_Suppression.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTR-PH-WITH-MAIL-SUPPRESSION
                pnd_Remarks.setValue(DbsUtil.compress("P/H MAIL SUPPRESSED - MAIL CODE: 01"));                                                                            //Natural: COMPRESS 'P/H MAIL SUPPRESSED - MAIL CODE: 01' TO #REMARKS
                getReports().write(1, pnd_Input_Record_Pnd_Pin_N12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-N12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Accepted.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #ACCEPTED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Accepted.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #ACCEPTED
                //*          PERFORM GET-NAME-ADDRESS-DETAILS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE-PIN-STATUS
    }
    private void sub_Add_Contract_To_Il_Array() throws Exception                                                                                                          //Natural: ADD-CONTRACT-TO-IL-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_X.setValue(pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Il);                                                                                                                       //Natural: ASSIGN #X := #CTRS.#CTR-RC-RCP-IL
        pnd_Output_Pnd_Tiaa_Contracts_Il.getValue(pnd_X).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                  //Natural: MOVE #TIAA-CNTRCT-NBR TO #TIAA-CONTRACTS-IL ( #X )
        pnd_Output_Pnd_Cref_Contracts_Il.getValue(pnd_X).setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                  //Natural: MOVE #CREF-CNTRCT-NBR TO #CREF-CONTRACTS-IL ( #X )
        //*  ADD-CONTRACT-TO-IL- ARRAY
    }
    //*  CPTSV >>>
    private void sub_Add_Contract_To_Ak_Array() throws Exception                                                                                                          //Natural: ADD-CONTRACT-TO-AK-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_X.setValue(pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ak);                                                                                                                       //Natural: ASSIGN #X := #CTRS.#CTR-RC-RCP-AK
        pnd_Output_Pnd_Tiaa_Contracts_Ak.getValue(pnd_X).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                  //Natural: MOVE #TIAA-CNTRCT-NBR TO #TIAA-CONTRACTS-AK ( #X )
        pnd_Output_Pnd_Cref_Contracts_Ak.getValue(pnd_X).setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                  //Natural: MOVE #CREF-CNTRCT-NBR TO #CREF-CONTRACTS-AK ( #X )
        //*  ADD-CONTRACT-TO-AL-ARRAY
    }
    private void sub_Add_Contract_To_Fl_Array() throws Exception                                                                                                          //Natural: ADD-CONTRACT-TO-FL-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_X.setValue(pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Fl);                                                                                                                       //Natural: ASSIGN #X := #CTRS.#CTR-RC-RCP-FL
        pnd_Output_Pnd_Tiaa_Contracts_Fl.getValue(pnd_X).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                  //Natural: MOVE #TIAA-CNTRCT-NBR TO #TIAA-CONTRACTS-FL ( #X )
        pnd_Output_Pnd_Cref_Contracts_Fl.getValue(pnd_X).setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                  //Natural: MOVE #CREF-CNTRCT-NBR TO #CREF-CONTRACTS-FL ( #X )
        //*  ADD-CONTRACT-TO-FL-ARRAY
    }
    private void sub_Add_Contract_To_Ny_Array() throws Exception                                                                                                          //Natural: ADD-CONTRACT-TO-NY-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_X.setValue(pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ny);                                                                                                                       //Natural: ASSIGN #X := #CTRS.#CTR-RC-RCP-NY
        pnd_Output_Pnd_Tiaa_Contracts_Ny.getValue(pnd_X).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                  //Natural: MOVE #TIAA-CNTRCT-NBR TO #TIAA-CONTRACTS-NY ( #X )
        pnd_Output_Pnd_Cref_Contracts_Ny.getValue(pnd_X).setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                  //Natural: MOVE #CREF-CNTRCT-NBR TO #CREF-CONTRACTS-NY ( #X )
        //*  ADD-CONTRACT-TO-NY-ARRAY              /* CPTSV <<<
    }
    private void sub_Add_Contract_To_Gn_Array() throws Exception                                                                                                          //Natural: ADD-CONTRACT-TO-GN-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_X.setValue(pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Gn);                                                                                                                       //Natural: ASSIGN #X := #CTRS.#CTR-RC-RCP-GN
        pnd_Output_Pnd_Tiaa_Contracts_Gn.getValue(pnd_X).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                  //Natural: MOVE #TIAA-CNTRCT-NBR TO #TIAA-CONTRACTS-GN ( #X )
        pnd_Output_Pnd_Cref_Contracts_Gn.getValue(pnd_X).setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                  //Natural: MOVE #CREF-CNTRCT-NBR TO #CREF-CONTRACTS-GN ( #X )
        //*  ADD-CONTRACT-TO-GN-ARRAY
    }
    //*  062707 J.AVE
    private void sub_Add_Contract_To_Array() throws Exception                                                                                                             //Natural: ADD-CONTRACT-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------
        pnd_K.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #K
        if (condition(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.notEquals(" ")))                                                                                               //Natural: IF #TIAA-CNTRCT-NBR NE ' '
        {
            pnd_Output_Pnd_Tiaa_Contracts.getValue(pnd_K).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #TIAA-CONTRACTS ( #K )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                               //Natural: IF #CREF-CNTRCT-NBR NE ' '
        {
            pnd_Output_Pnd_Cref_Contracts.getValue(pnd_K).setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                 //Natural: MOVE #CREF-CNTRCT-NBR TO #CREF-CONTRACTS ( #K )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-CONTRACT-TO-ARRAY
    }
    private void sub_Write_Data_To_File() throws Exception                                                                                                                //Natural: WRITE-DATA-TO-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        pnd_Ctr_File1.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #CTR-FILE1
        pnd_Output_Pnd_Package_Code.setValue("RDR2016A");                                                                                                                 //Natural: MOVE 'RDR2016A' TO #PACKAGE-CODE
        getWorkFiles().write(2, false, pnd_Output);                                                                                                                       //Natural: WRITE WORK FILE 2 #OUTPUT
        //*  WRITE-DATA-TO-FILE
    }
    private void sub_Write_Statistics_Report() throws Exception                                                                                                           //Natural: WRITE-STATISTICS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ctr_Contract_Rejected.compute(new ComputeParameters(false, pnd_Ctr_Contract_Rejected), pnd_Ctr_Da_Contract_Not_In_Mdm.add(pnd_Ctr_Da_Inactive_In_Mdm).add(pnd_Ctr_Ph_Deceased).add(pnd_Ctr_Ph_With_Mail_Suppression).add(pnd_Ctr_Out_Of_State).add(pnd_Ctr_Contract_Outof_Range).add(pnd_Ctr_Plan_Not_For_Proc).add(pnd_Ctr_Institution_Owned).add(pnd_Ctr_Contract_Dispparoved_State).add(pnd_Ctr_Duplicate_Contract)); //Natural: ASSIGN #CTR-CONTRACT-REJECTED := #CTR-DA-CONTRACT-NOT-IN-MDM + #CTR-DA-INACTIVE-IN-MDM + #CTR-PH-DECEASED + #CTR-PH-WITH-MAIL-SUPPRESSION + #CTR-OUT-OF-STATE + #CTR-CONTRACT-OUTOF-RANGE + #CTR-PLAN-NOT-FOR-PROC + #CTR-INSTITUTION-OWNED + #CTR-CONTRACT-DISPPAROVED-STATE+ #CTR-DUPLICATE-CONTRACT
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ         :",pnd_Ctr_Contract_Read);                                                                                  //Natural: WRITE 'NUMBER OF RECORDS READ         :' #CTR-CONTRACT-READ
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS ACCEPTED     :",pnd_Ctr_Contract_Accepted);                                                                              //Natural: WRITE 'NUMBER OF RECORDS ACCEPTED     :' #CTR-CONTRACT-ACCEPTED
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS REJECTED     :",pnd_Ctr_Contract_Rejected);                                                                              //Natural: WRITE 'NUMBER OF RECORDS REJECTED     :' #CTR-CONTRACT-REJECTED
        if (Global.isEscape()) return;
        getReports().write(0, "BREAKDOWN OF RECORDS REJECTED  :");                                                                                                        //Natural: WRITE 'BREAKDOWN OF RECORDS REJECTED  :'
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT NOT IN MDM     :",pnd_Ctr_Da_Contract_Not_In_Mdm);                                                                              //Natural: WRITE ' -CONTRACT NOT IN MDM     :' #CTR-DA-CONTRACT-NOT-IN-MDM
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT INACTIVE IN MDM:",pnd_Ctr_Da_Inactive_In_Mdm);                                                                                  //Natural: WRITE ' -CONTRACT INACTIVE IN MDM:' #CTR-DA-INACTIVE-IN-MDM
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H DECEASED            :",pnd_Ctr_Ph_Deceased);                                                                                         //Natural: WRITE ' -P/H DECEASED            :' #CTR-PH-DECEASED
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H W/ MAIL SUPPRESSION :",pnd_Ctr_Ph_With_Mail_Suppression);                                                                            //Natural: WRITE ' -P/H W/ MAIL SUPPRESSION :' #CTR-PH-WITH-MAIL-SUPPRESSION
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT OUT OF RANGE   :",pnd_Ctr_Contract_Outof_Range);                                                                                //Natural: WRITE ' -CONTRACT OUT OF RANGE   :' #CTR-CONTRACT-OUTOF-RANGE
        if (Global.isEscape()) return;
        getReports().write(0, " -DUPLICATE CONTRACT      :",pnd_Ctr_Duplicate_Contract);                                                                                  //Natural: WRITE ' -DUPLICATE CONTRACT      :' #CTR-DUPLICATE-CONTRACT
        if (Global.isEscape()) return;
        //*    WRITE ' -ISSUED AFTER CUTOFF DATE:' #CTR-AFTER-CUTOFF
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY PRODUCT:");                                                                                                                       //Natural: WRITE 'TOTAL BY PRODUCT:'
        if (Global.isEscape()) return;
        getReports().write(0, " - RC/RCP : ",pnd_Ctr_Total_Rc_Rcp);                                                                                                       //Natural: WRITE ' - RC/RCP : ' #CTR-TOTAL-RC-RCP
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Ctr_File1);                                                                                                                         //Natural: WRITE '=' #CTR-FILE1
        if (Global.isEscape()) return;
        //*    WRITE '=' #CTR-FILE2
        //*  WRITE-STATISTICS-REPORT
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Input_Record_Pnd_Pin_A12IsBreak = pnd_Input_Record_Pnd_Pin_A12.isBreak(endOfData);
        if (condition(pnd_Input_Record_Pnd_Pin_A12IsBreak))
        {
            //*  CPTSV
            //*  CPTSV
            if (condition(pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Gn.greater(getZero()) || pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Il.greater(getZero()) || pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ak.greater(getZero()) //Natural: IF #CTRS.#CTR-RC-RCP-GN GT 0 OR #CTRS.#CTR-RC-RCP-IL GT 0 OR #CTRS.#CTR-RC-RCP-AK GT 0 OR #CTRS.#CTR-RC-RCP-FL GT 0 OR #CTRS.#CTR-RC-RCP-NY GT 0
                || pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Fl.greater(getZero()) || pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ny.greater(getZero())))
            {
                //*        MOVE #PREV-PIN               TO #OUTPUT.#PIN    /* PINEXP
                //*  PINEXP >>>
                if (condition(pnd_Prev_Pin_N12.greaterOrEqual(new DbsDecimal("100000000000"))))                                                                           //Natural: IF #PREV-PIN-N12 GE 100000000000
                {
                    pnd_Output_Pnd_Pin_A12.setValue(pnd_Prev_Pin_N12);                                                                                                    //Natural: MOVE #PREV-PIN-N12 TO #OUTPUT.#PIN-A12
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Pin_N7.setValue(pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7);                                                                                     //Natural: MOVE #PREV-PIN-N7 TO #OUTPUT.#PIN-N7
                    pnd_Output_Pnd_Pin_A5.setValue("     ");                                                                                                              //Natural: MOVE '     ' TO #OUTPUT.#PIN-A5
                    //*  PINEXP <<<
                }                                                                                                                                                         //Natural: END-IF
                pnd_Output_Pnd_Ctr_Rc_Rcp_Gn.setValue(pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Gn);                                                                                        //Natural: MOVE #CTRS.#CTR-RC-RCP-GN TO #OUTPUT.#CTR-RC-RCP-GN
                pnd_Output_Pnd_Ctr_Rc_Rcp_Il.setValue(pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Il);                                                                                        //Natural: MOVE #CTRS.#CTR-RC-RCP-IL TO #OUTPUT.#CTR-RC-RCP-IL
                //*  CPTSV
                pnd_Output_Pnd_Ctr_Rc_Rcp_Ak.setValue(pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ak);                                                                                        //Natural: MOVE #CTRS.#CTR-RC-RCP-AK TO #OUTPUT.#CTR-RC-RCP-AK
                //*  CPTSV
                pnd_Output_Pnd_Ctr_Rc_Rcp_Fl.setValue(pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Fl);                                                                                        //Natural: MOVE #CTRS.#CTR-RC-RCP-FL TO #OUTPUT.#CTR-RC-RCP-FL
                //*  CPTSV
                pnd_Output_Pnd_Ctr_Rc_Rcp_Ny.setValue(pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ny);                                                                                        //Natural: MOVE #CTRS.#CTR-RC-RCP-NY TO #OUTPUT.#CTR-RC-RCP-NY
                pnd_Output_Pnd_Letter_Type.setValue(pnd_Prev_Cover);                                                                                                      //Natural: MOVE #PREV-COVER TO #OUTPUT.#LETTER-TYPE
                //*  CPTSV
                pnd_Output_Pnd_Tsv_Ind.setValue(pnd_Prev_Tsv_Ind);                                                                                                        //Natural: MOVE #PREV-TSV-IND TO #OUTPUT.#TSV-IND
                pnd_Output_Pnd_Institution_Name.setValue(pnd_Prev_Inst_Name);                                                                                             //Natural: MOVE #PREV-INST-NAME TO #OUTPUT.#INSTITUTION-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-DATA-TO-FILE
                sub_Write_Data_To_File();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP
            if (condition(pnd_Input_Record_Pnd_Pin_N12.equals(new DbsDecimal("999999999999"))))                                                                           //Natural: IF #INPUT-RECORD.#PIN-N12 EQ 999999999999
            {
                Global.setEscape(true); Global.setEscapeCode(EscapeType.BottomImmediate);                                                                                 //Natural: ESCAPE BOTTOM IMMEDIATE
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  CPTSV
            //*  CPTSV
            pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Gn.reset();                                                                                                                           //Natural: RESET #CTRS.#CTR-RC-RCP-GN #CTRS.#CTR-RC-RCP-IL #CTRS.#CTR-RC-RCP-AK #CTRS.#CTR-RC-RCP-FL #CTRS.#CTR-RC-RCP-NY
            pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Il.reset();
            pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ak.reset();
            pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Fl.reset();
            pnd_Ctrs_Pnd_Ctr_Rc_Rcp_Ny.reset();
            pnd_Output.reset();                                                                                                                                           //Natural: RESET #OUTPUT #K #X #ENDORSE-CONTRACTS-GN #ENDORSE-CONTRACTS-GN
            pnd_K.reset();
            pnd_X.reset();
            pnd_Output_Pnd_Endorse_Contracts_Gn.reset();
            pnd_Output_Pnd_Endorse_Contracts_Gn.reset();
            pnd_Break.setValue(true);                                                                                                                                     //Natural: MOVE TRUE TO #BREAK
            //*  PINEXP
            pnd_Prev_Pin_N12.setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-N12 TO #PREV-PIN-N12
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Institution_Name);                                                                                           //Natural: MOVE #INPUT-RECORD.#INSTITUTION-NAME TO #PREV-INST-NAME
            pnd_Prev_Cover.setValue(pnd_Input_Record_Pnd_Cover_Letter_Ind);                                                                                               //Natural: MOVE #INPUT-RECORD.#COVER-LETTER-IND TO #PREV-COVER
            //*  CPTSV
            pnd_Prev_Tsv_Ind.setValue(pnd_Input_Record_Pnd_Tsv_Ind);                                                                                                      //Natural: MOVE #INPUT-RECORD.#TSV-IND TO #PREV-TSV-IND
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=120 PS=60");
        Global.format(2, "LS=120 PS=60");
        Global.format(0, "LS=80 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(35),"2016 CUSTOM PORTFOLIO ENDORSEMENT",new 
            TabSetting(84),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(43),"EXCEPTIONS LIST",new TabSetting(84),"TIME:",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,new TabSetting(84),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new TabSetting(2),"PIN       TIAA NBR   STATE   REMARKS",NEWLINE,new 
            TabSetting(2),"------------------------------------------------------------",NEWLINE);
    }
}
