/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:17:11 PM
**        * FROM NATURAL PROGRAM : Pstb5151
************************************************************
**        * FILE NAME            : Pstb5151.java
**        * CLASS NAME           : Pstb5151
**        * INSTANCE NAME        : Pstb5151
************************************************************
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: PURGE MAIL ITEMS
**SAG SYSTEM: PST
**SAG GDA: PSTG000
**SAG USER-DEFINED-LS(1): 080
**SAG USER-DEFINED-PS(1): 055
**SAG REPORT-HEADING(1): POST MAINTAIN OUTPUT QUEUE
**SAG PRINT-FILE(1): 1
**SAG REPORT-HEADING(2): PURGE MAIL ITEMS
**SAG PRINT-FILE(2): 1
**SAG HEADING-LINE: 12000000
**SAG DESCS(1): THIS PROGRAM PURGE MAIL ITEMS: FAILED ITEMS HAVE A
**SAG DESCS(2): SHORTER TIME BEFORE PURGE, SUCCESSFULL ITEMS HAVE A
**SAG DESCS(3): LONGER TIME. PROGRAM CAN BE RESTARTED AT ANY POINT (NO
**SAG DESCS(4): SPECIAL RESTART CODE REQUIRED SINCE RECORDS ARE PURGED)
**SAG PRIMARY-FILE: PST-MAIL-HEADER
**SAG PRIMARY-KEY: MAIL-ID-HDR-IND
**SAG DA-NAME: PSTL5152
************************************************************************
* PROGRAM  : PSTB5151
* SYSTEM   : PST
* TITLE    : PURGE MAIL ITEMS
* GENERATED: AUG 14,98 AT 04:56 PM
* FUNCTION : THIS PROGRAM PURGE MAIL ITEMS: FAILED ITEMS HAVE A
*            SHORTER TIME BEFORE PURGE, SUCCESSFULL ITEMS HAVE A
*            LONGER TIME. PROGRAM CAN BE RESTARTED AT ANY POINT (NO
*            SPECIAL RESTART CODE REQUIRED SINCE RECORDS ARE PURGED)
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
* CHANGED ON OCT 12,94 BY LOEDOLF FOR RELEASE ____
* CHANGED ON APR 28,95 BY ROBERTD FOR RELEASE V2
* CHANGED ON JUN 15,95 BY H LOEDOLFF FOR RLSE V2
*
* CHANGED ON MAR 25,97 BY E GRAF - BYPASS PURGE OF PTCISIA & PTCISMDO
*                                  SCAN FOR EG9703
* CHANGED ON MAY 22,97 BY E GRAF - TO FIX THE PURGE OF DATA FROM POST
*                                  FILES BASED ON POST-VRSN-CDE = '02'
*                                  INSTEAD OF THE PACKAGE CODE.
*                                  SCAN FOR EG9705
* CHANGED ON MARCH 25, BY E GRAF
*                  MODIFIED PROCESS TO ALLOW FOR OVERRIDE OF THE PURGE
*                  PARAMETERS BASED ON THE INDIVIDUAL PER PACKAGE PURGE
*                  PARAMETERS.
*
*
*
* CHANGES APPLIED TO THE PST-STNDRD-PCKGE (FILE #108)
*      DATA ITEMS: PURGE-DTLS(1:2) - NEW FIELDS.
*                  PURGE-STATUS-CDE(1) A8 - VALUE 'GOOD-GEN'
*                  PURGE-STATUS-CDE(2) A8 - VALUE 'BAD-GEN '
*                  PURGE-DAYS(1)  P3.0 - PACKAGE AGE LIMIT IN GOOD STAT
*                  PURGE-DAYS(2)  P3.0 - PACKAGE AGE LIMIT IN BAD  STAT
*                  KEY CHANGE IS IN CHECK-TO-PURGE.
*                  ALSO CHANGED TO CALLNAT NEW MODULE PSTN7220
*                  (WHICH WAS CLONED FROM OLD PSTN7205).
*                  PREVIOUSLY BYPASSED PACKAGES OF PTCISIA & PTCISMDO
*                  WILL BE BYPASSED EXTERNALLY VIA OPTION 3 (MP, DSYS
*                  PURGED PARAMETERS).
*
* LAURENC  01/19/01 ACCESS PST-SUPPORT-TBL INSTEAD OF CWF-SUPPORT-TBL BY
*                   CALLING PSTN9017 INSTEAD OF PSTN9007. ALSO, PSTA8510
*
* >
**SAG END-EXIT
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Pstb5151 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private LdaPstl5152 ldaPstl5152;
    private LdaPstl9902 ldaPstl9902;
    private PdaPsta9200 pdaPsta9200;
    private PdaPsta9017 pdaPsta9017;
    private PdaPsta9100 pdaPsta9100;
    private PdaPstpda_M pdaPstpda_M;
    private PdaPsta7205 pdaPsta7205;
    private PdaPsta7206 pdaPsta7206;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;
    private DbsField pnd_Error_Ind;

    private DataAccessProgramView vw_pst_Purge_V;
    private DbsField pst_Purge_V_Pst_Rqst_Id;

    private DataAccessProgramView vw_pst_Stndrd_Pckge;
    private DbsField pst_Stndrd_Pckge_Pckge_Cde;
    private DbsField pst_Stndrd_Pckge_Pckge_Vrsn_Cde;
    private DbsField pst_Stndrd_Pckge_Pckge_Short_Nme;

    private DbsGroup pst_Stndrd_Pckge_Purge_Dtls;
    private DbsField pst_Stndrd_Pckge_Purge_Status_Cde;
    private DbsField pst_Stndrd_Pckge_Purge_Days;
    private DbsField package_Key;

    private DbsGroup package_Key__R_Field_1;

    private DbsGroup package_Key_Package_Struct;
    private DbsField package_Key_Pckge_Cde;
    private DbsField package_Key_Pckge_Vrsn_Cde;
    private DbsField pnd_Hdr_Isn;
    private DbsField pnd_Purge_V1_Key;

    private DbsGroup pnd_Purge_V1_Key__R_Field_2;
    private DbsField pnd_Purge_V1_Key_Rqst_Id;
    private DbsField pnd_Purge_V1_Key_Rec_Type;
    private DbsField pnd_Count_Array_Search_Key;

    private DbsGroup pnd_Count_Array_Search_Key__R_Field_3;

    private DbsGroup pnd_Count_Array_Search_Key_Pnd_Count_Array_Search;
    private DbsField pnd_Count_Array_Search_Key_Pckge_Cde;
    private DbsField pnd_Count_Array_Search_Key_Pckge_Vrsn_Cde;
    private DbsField pnd_Count_Array_Max_Els;
    private DbsField pnd_Count_Index;
    private DbsField pnd_Count_Array_Size;

    private DbsGroup pnd_Count_Array;
    private DbsField pnd_Count_Array_Pnd_Pckge_Post_Vrsn_Cde;
    private DbsField pnd_Count_Array_Pnd_Pckge_Cde_Vrsn_Cde;
    private DbsField pnd_Count_Array_Pnd_Rqsts_Purged;
    private DbsField pnd_Count_Array_Pnd_Total_Recs_Purged;
    private DbsField pnd_Purge_Parms_Save;
    private DbsField pnd_Purge_Parms;

    private DbsGroup pnd_Purge_Parms__R_Field_4;
    private DbsField pnd_Purge_Parms_Pnd_Failed_Days;
    private DbsField pnd_Purge_Parms_Pnd_Good_Days;
    private DbsField pnd_Input;

    private DbsGroup check_To_Purge_Vars;
    private DbsField check_To_Purge_Vars_Pnd_Purge_Rqst;
    private DbsField check_To_Purge_Vars_Pnd_Days_Old;
    private DbsField check_To_Purge_Vars_Updte_Dte_Tme;
    private DbsField check_To_Purge_Vars_Pckge_Stts;
    private DbsField pnd_Et_Count;
    private DbsField pls_Trace;
    private int cmrollReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        ldaPstl5152 = new LdaPstl5152();
        registerRecord(ldaPstl5152);
        registerRecord(ldaPstl5152.getVw_mail_Header());
        registerRecord(ldaPstl5152.getVw_mail_Data());
        registerRecord(ldaPstl5152.getVw_mail_Item());
        registerRecord(ldaPstl5152.getVw_rqst_Header());
        ldaPstl9902 = new LdaPstl9902();
        registerRecord(ldaPstl9902);
        localVariables = new DbsRecord();
        pdaPsta9200 = new PdaPsta9200(localVariables);
        pdaPsta9017 = new PdaPsta9017(localVariables);
        pdaPsta9100 = new PdaPsta9100(localVariables);
        pdaPstpda_M = new PdaPstpda_M(localVariables);
        pdaPsta7205 = new PdaPsta7205(localVariables);
        pdaPsta7206 = new PdaPsta7206(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);

        // Local Variables
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 50);
        pnd_Error_Ind = localVariables.newFieldInRecord("pnd_Error_Ind", "#ERROR-IND", FieldType.STRING, 1);

        vw_pst_Purge_V = new DataAccessProgramView(new NameInfo("vw_pst_Purge_V", "PST-PURGE-V"), "PST_OUTGOING_MAIL_ITEM", "PST_REQUEST_DATA");
        pst_Purge_V_Pst_Rqst_Id = vw_pst_Purge_V.getRecord().newFieldInGroup("pst_Purge_V_Pst_Rqst_Id", "PST-RQST-ID", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "PST_RQST_ID");
        pst_Purge_V_Pst_Rqst_Id.setDdmHeader("EXTERNAL/REQUEST/ID");
        registerRecord(vw_pst_Purge_V);

        vw_pst_Stndrd_Pckge = new DataAccessProgramView(new NameInfo("vw_pst_Stndrd_Pckge", "PST-STNDRD-PCKGE"), "PST_STNDRD_PCKGE", "PST_STNDRD_PCKGE", 
            DdmPeriodicGroups.getInstance().getGroups("PST_STNDRD_PCKGE"));
        pst_Stndrd_Pckge_Pckge_Cde = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Cde", "PCKGE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "PCKGE_CDE");
        pst_Stndrd_Pckge_Pckge_Cde.setDdmHeader("PACKAGE/CODE");
        pst_Stndrd_Pckge_Pckge_Vrsn_Cde = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Vrsn_Cde", "PCKGE-VRSN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "PCKGE_VRSN_CDE");
        pst_Stndrd_Pckge_Pckge_Vrsn_Cde.setDdmHeader("PACKAGE/TYPE/CODE");
        pst_Stndrd_Pckge_Pckge_Short_Nme = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Short_Nme", "PCKGE-SHORT-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "PCKGE_SHORT_NME");
        pst_Stndrd_Pckge_Pckge_Short_Nme.setDdmHeader("PACKAGE/NAME");

        pst_Stndrd_Pckge_Purge_Dtls = vw_pst_Stndrd_Pckge.getRecord().newGroupArrayInGroup("pst_Stndrd_Pckge_Purge_Dtls", "PURGE-DTLS", new DbsArrayController(1, 
            2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PST_STNDRD_PCKGE_PURGE_DTLS");
        pst_Stndrd_Pckge_Purge_Dtls.setDdmHeader("PURGE/DETAILS");
        pst_Stndrd_Pckge_Purge_Status_Cde = pst_Stndrd_Pckge_Purge_Dtls.newFieldInGroup("pst_Stndrd_Pckge_Purge_Status_Cde", "PURGE-STATUS-CDE", FieldType.STRING, 
            8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PURGE_STATUS_CDE", "PST_STNDRD_PCKGE_PURGE_DTLS");
        pst_Stndrd_Pckge_Purge_Status_Cde.setDdmHeader("PURGE/STATUS/CODE");
        pst_Stndrd_Pckge_Purge_Days = pst_Stndrd_Pckge_Purge_Dtls.newFieldInGroup("pst_Stndrd_Pckge_Purge_Days", "PURGE-DAYS", FieldType.PACKED_DECIMAL, 
            3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PURGE_DAYS", "PST_STNDRD_PCKGE_PURGE_DTLS");
        pst_Stndrd_Pckge_Purge_Days.setDdmHeader("DAYS/ON/FILE");
        registerRecord(vw_pst_Stndrd_Pckge);

        package_Key = localVariables.newFieldInRecord("package_Key", "PACKAGE-KEY", FieldType.STRING, 10);

        package_Key__R_Field_1 = localVariables.newGroupInRecord("package_Key__R_Field_1", "REDEFINE", package_Key);

        package_Key_Package_Struct = package_Key__R_Field_1.newGroupInGroup("package_Key_Package_Struct", "PACKAGE-STRUCT");
        package_Key_Pckge_Cde = package_Key_Package_Struct.newFieldInGroup("package_Key_Pckge_Cde", "PCKGE-CDE", FieldType.STRING, 8);
        package_Key_Pckge_Vrsn_Cde = package_Key_Package_Struct.newFieldInGroup("package_Key_Pckge_Vrsn_Cde", "PCKGE-VRSN-CDE", FieldType.STRING, 2);
        pnd_Hdr_Isn = localVariables.newFieldInRecord("pnd_Hdr_Isn", "#HDR-ISN", FieldType.PACKED_DECIMAL, 11);
        pnd_Purge_V1_Key = localVariables.newFieldInRecord("pnd_Purge_V1_Key", "#PURGE-V1-KEY", FieldType.STRING, 19);

        pnd_Purge_V1_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Purge_V1_Key__R_Field_2", "REDEFINE", pnd_Purge_V1_Key);
        pnd_Purge_V1_Key_Rqst_Id = pnd_Purge_V1_Key__R_Field_2.newFieldInGroup("pnd_Purge_V1_Key_Rqst_Id", "RQST-ID", FieldType.STRING, 11);
        pnd_Purge_V1_Key_Rec_Type = pnd_Purge_V1_Key__R_Field_2.newFieldInGroup("pnd_Purge_V1_Key_Rec_Type", "REC-TYPE", FieldType.STRING, 8);
        pnd_Count_Array_Search_Key = localVariables.newFieldInRecord("pnd_Count_Array_Search_Key", "#COUNT-ARRAY-SEARCH-KEY", FieldType.STRING, 10);

        pnd_Count_Array_Search_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Count_Array_Search_Key__R_Field_3", "REDEFINE", pnd_Count_Array_Search_Key);

        pnd_Count_Array_Search_Key_Pnd_Count_Array_Search = pnd_Count_Array_Search_Key__R_Field_3.newGroupInGroup("pnd_Count_Array_Search_Key_Pnd_Count_Array_Search", 
            "#COUNT-ARRAY-SEARCH");
        pnd_Count_Array_Search_Key_Pckge_Cde = pnd_Count_Array_Search_Key_Pnd_Count_Array_Search.newFieldInGroup("pnd_Count_Array_Search_Key_Pckge_Cde", 
            "PCKGE-CDE", FieldType.STRING, 8);
        pnd_Count_Array_Search_Key_Pckge_Vrsn_Cde = pnd_Count_Array_Search_Key_Pnd_Count_Array_Search.newFieldInGroup("pnd_Count_Array_Search_Key_Pckge_Vrsn_Cde", 
            "PCKGE-VRSN-CDE", FieldType.STRING, 2);
        pnd_Count_Array_Max_Els = localVariables.newFieldInRecord("pnd_Count_Array_Max_Els", "#COUNT-ARRAY-MAX-ELS", FieldType.PACKED_DECIMAL, 7);
        pnd_Count_Index = localVariables.newFieldInRecord("pnd_Count_Index", "#COUNT-INDEX", FieldType.PACKED_DECIMAL, 7);
        pnd_Count_Array_Size = localVariables.newFieldInRecord("pnd_Count_Array_Size", "#COUNT-ARRAY-SIZE", FieldType.PACKED_DECIMAL, 7);

        pnd_Count_Array = localVariables.newGroupArrayInRecord("pnd_Count_Array", "#COUNT-ARRAY", new DbsArrayController(1, 1000));
        pnd_Count_Array_Pnd_Pckge_Post_Vrsn_Cde = pnd_Count_Array.newFieldInGroup("pnd_Count_Array_Pnd_Pckge_Post_Vrsn_Cde", "#PCKGE-POST-VRSN-CDE", FieldType.STRING, 
            2);
        pnd_Count_Array_Pnd_Pckge_Cde_Vrsn_Cde = pnd_Count_Array.newFieldInGroup("pnd_Count_Array_Pnd_Pckge_Cde_Vrsn_Cde", "#PCKGE-CDE-VRSN-CDE", FieldType.STRING, 
            10);
        pnd_Count_Array_Pnd_Rqsts_Purged = pnd_Count_Array.newFieldInGroup("pnd_Count_Array_Pnd_Rqsts_Purged", "#RQSTS-PURGED", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Count_Array_Pnd_Total_Recs_Purged = pnd_Count_Array.newFieldInGroup("pnd_Count_Array_Pnd_Total_Recs_Purged", "#TOTAL-RECS-PURGED", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Purge_Parms_Save = localVariables.newFieldInRecord("pnd_Purge_Parms_Save", "#PURGE-PARMS-SAVE", FieldType.STRING, 6);
        pnd_Purge_Parms = localVariables.newFieldInRecord("pnd_Purge_Parms", "#PURGE-PARMS", FieldType.STRING, 6);

        pnd_Purge_Parms__R_Field_4 = localVariables.newGroupInRecord("pnd_Purge_Parms__R_Field_4", "REDEFINE", pnd_Purge_Parms);
        pnd_Purge_Parms_Pnd_Failed_Days = pnd_Purge_Parms__R_Field_4.newFieldInGroup("pnd_Purge_Parms_Pnd_Failed_Days", "#FAILED-DAYS", FieldType.NUMERIC, 
            3);
        pnd_Purge_Parms_Pnd_Good_Days = pnd_Purge_Parms__R_Field_4.newFieldInGroup("pnd_Purge_Parms_Pnd_Good_Days", "#GOOD-DAYS", FieldType.NUMERIC, 3);
        pnd_Input = localVariables.newFieldInRecord("pnd_Input", "#INPUT", FieldType.STRING, 80);

        check_To_Purge_Vars = localVariables.newGroupInRecord("check_To_Purge_Vars", "CHECK-TO-PURGE-VARS");
        check_To_Purge_Vars_Pnd_Purge_Rqst = check_To_Purge_Vars.newFieldInGroup("check_To_Purge_Vars_Pnd_Purge_Rqst", "#PURGE-RQST", FieldType.BOOLEAN, 
            1);
        check_To_Purge_Vars_Pnd_Days_Old = check_To_Purge_Vars.newFieldInGroup("check_To_Purge_Vars_Pnd_Days_Old", "#DAYS-OLD", FieldType.PACKED_DECIMAL, 
            7);
        check_To_Purge_Vars_Updte_Dte_Tme = check_To_Purge_Vars.newFieldInGroup("check_To_Purge_Vars_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME);
        check_To_Purge_Vars_Pckge_Stts = check_To_Purge_Vars.newFieldInGroup("check_To_Purge_Vars_Pckge_Stts", "PCKGE-STTS", FieldType.STRING, 2);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.PACKED_DECIMAL, 5);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_pst_Purge_V.reset();
        vw_pst_Stndrd_Pckge.reset();

        ldaCdbatxa.initializeValues();
        ldaPstl5152.initializeValues();
        ldaPstl9902.initializeValues();

        localVariables.reset();
        pnd_Header1_1.setInitialValue("            POST Maintain Output Queue");
        pnd_Header1_2.setInitialValue("                 Purge Mail Items");
        pnd_Error_Ind.setInitialValue("E");
        pnd_Count_Array_Size.setInitialValue(1000);
        pnd_Purge_Parms_Save.setInitialValue(" ");
        pls_Trace.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Pstb5151() throws Exception
    {
        super("Pstb5151");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Pstb5151|Main");
        OnErrorManager.pushEvent("PSTB5151", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //*  DEFINE PRINTERS AND FORMATS
                getReports().definePrinter(2, "NOT DEFINED");                                                                                                             //Natural: DEFINE PRINTER ( 1 )
                //*                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 80 PS = 55 ZP = ON IS = OFF ES = OFF SG = OFF
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN #PROGRAM = *PROGRAM
                //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
                pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                              //Natural: ASSIGN #CUR-LANG = *LANGUAGE
                pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                       //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
                //* *SAG DEFINE EXIT START-OF-PROGRAM
                if (condition(Global.getSTACK().getDatacount().greater(getZero()), INPUT_1))                                                                              //Natural: FORMAT LS = 132;//Natural: IF *DATA > 0
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input);                                                                                        //Natural: INPUT #INPUT
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOOKUP-PURGE-CRITERIA
                sub_Lookup_Purge_Criteria();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Purge_Parms_Save.setValue(pnd_Purge_Parms);                                                                                                           //Natural: ASSIGN #PURGE-PARMS-SAVE := #PURGE-PARMS
                //* *SAG END-EXIT
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH' THEN
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA = 'INFP9000'
                //* ***********************
                //*   MAIN PROGRAM LOGIC  *
                //* ***********************
                //* *SAG DEFINE EXIT BEFORE-READ
                //* *SAG END-EXIT
                //*  PRIMARY FILE
                ldaPstl5152.getVw_mail_Item().startDatabaseRead                                                                                                           //Natural: READ MAIL-ITEM BY RQST-HDR-IND
                (
                "READ_PRIME",
                new Oc[] { new Oc("RQST_HDR_IND", "ASC") }
                );
                READ_PRIME:
                while (condition(ldaPstl5152.getVw_mail_Item().readNextRow("READ_PRIME")))
                {
                    //* *SAG DEFINE EXIT AFTER-PRIME-READ
                    //*  IF MAIL-ITEM.PCKGE-CDE =  'PTCISMDO'    /*EG9905
                    //*                  OR     =  'PTCISIA'     /*DELETE PARM IS EXTERNAL
                    //*     ESCAPE TOP
                    //*  END-IF
                    //*  BOTH POST V1 & V2 HAVE COMMON FIELDS ON MAIL-ITEM AND REQUEST-HEADER.
                    //*  SO READING BY MAIL-ITEM WILL RETRIEVE ALL MAIL-ITEMS AND
                    //*  REQUEST-HEADERS.  WE THEN DETERMINE THE VERSION BY LOOKING AT THE
                    //*  PCKGE-CDE.  THIS IS CRUDE, BUT SHOULD BE AN INTERIM STEP UNTIL WE CAN
                    //*  CONVERT V1 TO V2.
                    package_Key_Package_Struct.setValuesByName(ldaPstl5152.getVw_mail_Item());                                                                            //Natural: MOVE BY NAME MAIL-ITEM TO PACKAGE-STRUCT
                    vw_pst_Stndrd_Pckge.startDatabaseFind                                                                                                                 //Natural: FIND PST-STNDRD-PCKGE WITH PCKGE-CDE-VRSN-CDE = PACKAGE-KEY
                    (
                    "GET_PCKGE_PARMS",
                    new Wc[] { new Wc("PCKGE_CDE_VRSN_CDE", "=", package_Key, WcType.WITH) }
                    );
                    GET_PCKGE_PARMS:
                    while (condition(vw_pst_Stndrd_Pckge.readNextRow("GET_PCKGE_PARMS", true)))
                    {
                        vw_pst_Stndrd_Pckge.setIfNotFoundControlFlag(false);
                        //* RESTORE PURGE PARMS
                        if (condition(vw_pst_Stndrd_Pckge.getAstCOUNTER().equals(0)))                                                                                     //Natural: IF NO RECORDS FOUND
                        {
                            pnd_Purge_Parms.setValue(pnd_Purge_Parms_Save);                                                                                               //Natural: ASSIGN #PURGE-PARMS := #PURGE-PARMS-SAVE
                            if (true) break GET_PCKGE_PARMS;                                                                                                              //Natural: ESCAPE BOTTOM ( GET-PCKGE-PARMS. )
                        }                                                                                                                                                 //Natural: END-NOREC
                        pnd_Purge_Parms_Save.setValue(pnd_Purge_Parms);                                                                                                   //Natural: ASSIGN #PURGE-PARMS-SAVE := #PURGE-PARMS
                        if (condition(pst_Stndrd_Pckge_Purge_Days.getValue(1).greater(getZero())))                                                                        //Natural: IF PST-STNDRD-PCKGE.PURGE-DAYS ( 1 ) GT 0
                        {
                            pnd_Purge_Parms_Pnd_Good_Days.setValue(pst_Stndrd_Pckge_Purge_Days.getValue(1));                                                              //Natural: ASSIGN #GOOD-DAYS := PST-STNDRD-PCKGE.PURGE-DAYS ( 1 )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pst_Stndrd_Pckge_Purge_Days.getValue(2).greater(getZero())))                                                                        //Natural: IF PST-STNDRD-PCKGE.PURGE-DAYS ( 2 ) GT 0
                        {
                            pnd_Purge_Parms_Pnd_Failed_Days.setValue(pst_Stndrd_Pckge_Purge_Days.getValue(2));                                                            //Natural: ASSIGN #FAILED-DAYS := PST-STNDRD-PCKGE.PURGE-DAYS ( 2 )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    check_To_Purge_Vars.setValuesByName(ldaPstl5152.getVw_mail_Item());                                                                                   //Natural: MOVE BY NAME MAIL-ITEM TO CHECK-TO-PURGE-VARS
                    //*  WILL SET/RESET PURGE INDICATOR
                                                                                                                                                                          //Natural: PERFORM CHECK-TO-PURGE
                    sub_Check_To_Purge();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  ACCEPT IF #PURGE-RQST
                    if (condition(pls_Trace.getBoolean() && check_To_Purge_Vars_Pnd_Purge_Rqst.equals(false)))                                                            //Natural: IF +TRACE AND #PURGE-RQST = FALSE
                    {
                        getReports().write(0, "NOT PURGE !!!!! - PACKAGE: ",ldaPstl5152.getMail_Item_Pckge_Cde()," WITH STAT: ",check_To_Purge_Vars_Pckge_Stts,           //Natural: WRITE ( 0 ) 'NOT PURGE !!!!! - PACKAGE: ' MAIL-ITEM.PCKGE-CDE ' WITH STAT: ' CHECK-TO-PURGE-VARS.PCKGE-STTS ' AGE - ' #DAYS-OLD ' MAIL-ITEM: ' MAIL-ITEM.PST-RQST-ID
                            " AGE - ",check_To_Purge_Vars_Pnd_Days_Old," MAIL-ITEM: ",ldaPstl5152.getMail_Item_Pst_Rqst_Id());
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaPstl5152.getVw_mail_Item().reset();                                                                                                            //Natural: RESET MAIL-ITEM
                        //* RESTORE PURGE PARMS
                        check_To_Purge_Vars.reset();                                                                                                                      //Natural: RESET CHECK-TO-PURGE-VARS
                        pnd_Purge_Parms.setValue(pnd_Purge_Parms_Save);                                                                                                   //Natural: ASSIGN #PURGE-PARMS := #PURGE-PARMS-SAVE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pls_Trace.getBoolean()))                                                                                                                //Natural: IF +TRACE
                    {
                        getReports().print(0, "MAIL-ITEM:",ldaPstl5152.getMail_Item_Pst_Rqst_Id(),"Pckge:",ldaPstl5152.getMail_Item_Pckge_Cde(),new TabSetting(40),"Stat:",ldaPstl5152.getMail_Item_Pckge_Stts(),"Age:",check_To_Purge_Vars_Pnd_Days_Old,  //Natural: PRINT ( 0 ) 'MAIL-ITEM:' MAIL-ITEM.PST-RQST-ID 'Pckge:' MAIL-ITEM.PCKGE-CDE 40T 'Stat:' MAIL-ITEM.PCKGE-STTS 'Age:' #DAYS-OLD ( NL = 3 ) 'will be purged.' / 'PURGE PARMS USED: ' #PURGE-PARMS
                            new NumericLength (3),"will be purged.",NEWLINE,"PURGE PARMS USED: ",pnd_Purge_Parms);
                    }                                                                                                                                                     //Natural: END-IF
                    //*  TO KEEP COUNT OF RECS DELETED PER PCKGE
                                                                                                                                                                          //Natural: PERFORM FIND-COUNT-ELEMENT
                    sub_Find_Count_Element();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  EG9705 - START
                    if (condition(check_To_Purge_Vars_Pnd_Purge_Rqst.equals(false)))                                                                                      //Natural: IF #PURGE-RQST = FALSE
                    {
                        ldaPstl5152.getVw_mail_Item().reset();                                                                                                            //Natural: RESET MAIL-ITEM
                        //* RESTORE PURGE PARMS
                        check_To_Purge_Vars.reset();                                                                                                                      //Natural: RESET CHECK-TO-PURGE-VARS
                        pnd_Purge_Parms.setValue(pnd_Purge_Parms_Save);                                                                                                   //Natural: ASSIGN #PURGE-PARMS := #PURGE-PARMS-SAVE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Count_Index.equals(getZero())))                                                                                                     //Natural: IF #COUNT-INDEX = 0
                    {
                        ldaPstl5152.getVw_mail_Item().reset();                                                                                                            //Natural: RESET MAIL-ITEM
                        //* RESTORE PURGE PARMS
                        check_To_Purge_Vars.reset();                                                                                                                      //Natural: RESET CHECK-TO-PURGE-VARS
                        pnd_Purge_Parms.setValue(pnd_Purge_Parms_Save);                                                                                                   //Natural: ASSIGN #PURGE-PARMS := #PURGE-PARMS-SAVE
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IF MAIL-ITEM.PCKGE-CDE = 'PTCNFRM' OR = 'PFCORM' /* ONLY PCKGES FOR V2
                    if (condition(pnd_Count_Array_Pnd_Pckge_Post_Vrsn_Cde.getValue(pnd_Count_Index).equals("02")))                                                        //Natural: IF #COUNT-ARRAY.#PCKGE-POST-VRSN-CDE ( #COUNT-INDEX ) = '02'
                    {
                        //*  EG9705 -   END
                                                                                                                                                                          //Natural: PERFORM PURGE-POSTV2-RECORDS
                        sub_Purge_Postv2_Records();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM PURGE-POSTV1-RECORDS
                        sub_Purge_Postv1_Records();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Hdr_Isn.setValue(ldaPstl5152.getVw_mail_Item().getAstISN("READ_PRIME"));                                                                          //Natural: ASSIGN #HDR-ISN := *ISN ( READ-PRIME. )
                    //*  MAIL-ITEM PURGED AFTER ALL DATA IS PURGED
                    //* RESTORE PURGE PARMS
                                                                                                                                                                          //Natural: PERFORM PURGE-HEADER
                    sub_Purge_Header();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Purge_Parms.setValue(pnd_Purge_Parms_Save);                                                                                                       //Natural: ASSIGN #PURGE-PARMS := #PURGE-PARMS-SAVE
                    //* *SAG END-EXIT
                    //*  PRIMARY FILE.
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //* *SAG DEFINE EXIT END-OF-PROGRAM
                //*  FINAL ET
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB
                sub_End_Of_Job();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* *SAG END-EXIT
                //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-TO-PURGE
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PURGE-POSTV2-RECORDS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PURGE-POSTV1-RECORDS
                //* ***********************************************************************
                //*  READ BY RECORD TYPE FOR THE MAIL-ITEM FROM READ-PRIME LOOP, STARTING
                //*  FROM REC TYPE 'B'. REC TYPE 'A' IS THE HDR AND GETS DELETED FROM READ-
                //*  PRIME LOOP. THIS PURGE IS SPECIFIC FOR APPLICATIONS USING THE
                //*  NSTA4899 AND NSTA4898 FILES.
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PURGE-HEADER
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-PURGE-CRITERIA
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-TRANSACTION
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-COUNT-ELEMENT
                //*  EG9705 - START
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-POST-VERSION-CODE
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-JOB
                //* *SAG END-EXIT
                //* *SAG DEFINE EXIT ON-ERROR
                //* ***********************************************************************
                //* ***********************************************************************                                                                               //Natural: ON ERROR
                //* *SAG END-EXIT
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Check_To_Purge() throws Exception                                                                                                                    //Natural: CHECK-TO-PURGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        check_To_Purge_Vars_Pnd_Purge_Rqst.reset();                                                                                                                       //Natural: RESET #PURGE-RQST
        //*  CHCK AGNST
        //*  UPDTE DTE
        pdaPsta9100.getPsta9100_Date_A().setValueEdited(check_To_Purge_Vars_Updte_Dte_Tme,new ReportEditMask("MMDDYYYY"));                                                //Natural: MOVE EDITED CHECK-TO-PURGE-VARS.UPDTE-DTE-TME ( EM = MMDDYYYY ) TO PSTA9100.DATE-A
        pdaPsta9100.getPsta9100_Date_D().setValueEdited(new ReportEditMask("MMDDYYYY"),pdaPsta9100.getPsta9100_Date_A());                                                 //Natural: MOVE EDITED PSTA9100.DATE-A TO PSTA9100.DATE-D ( EM = MMDDYYYY )
        check_To_Purge_Vars_Pnd_Days_Old.compute(new ComputeParameters(false, check_To_Purge_Vars_Pnd_Days_Old), Global.getDATX().subtract(pdaPsta9100.getPsta9100_Date_D())); //Natural: ASSIGN #DAYS-OLD := *DATX - PSTA9100.DATE-D
        //*  GOOD STATUS
        if (condition(check_To_Purge_Vars_Pckge_Stts.greaterOrEqual("E")))                                                                                                //Natural: IF CHECK-TO-PURGE-VARS.PCKGE-STTS GE 'E'
        {
            if (condition(check_To_Purge_Vars_Pnd_Days_Old.greater(pnd_Purge_Parms_Pnd_Good_Days)))                                                                       //Natural: IF #DAYS-OLD > #GOOD-DAYS
            {
                check_To_Purge_Vars_Pnd_Purge_Rqst.setValue(true);                                                                                                        //Natural: ASSIGN #PURGE-RQST := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  ALL OTHER STATUSES ARE CONSIDERED BAD/INCOMPLETE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(check_To_Purge_Vars_Pnd_Days_Old.greater(pnd_Purge_Parms_Pnd_Failed_Days)))                                                                     //Natural: IF #DAYS-OLD > #FAILED-DAYS
            {
                check_To_Purge_Vars_Pnd_Purge_Rqst.setValue(true);                                                                                                        //Natural: ASSIGN #PURGE-RQST := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Purge_Postv2_Records() throws Exception                                                                                                              //Natural: PURGE-POSTV2-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  READ DOWN THE POST STRUCTURE DELETING ALL RECORDS THAT WE FIND.
        ldaPstl5152.getVw_mail_Header().startDatabaseRead                                                                                                                 //Natural: READ MAIL-HEADER BY OTGOING-MAIL-ITEM-ID = MAIL-ITEM.PST-RQST-ID
        (
        "RMH",
        new Wc[] { new Wc("OTGOING_MAIL_ITEM_ID", ">=", ldaPstl5152.getMail_Item_Pst_Rqst_Id(), WcType.BY) },
        new Oc[] { new Oc("OTGOING_MAIL_ITEM_ID", "ASC") }
        );
        RMH:
        while (condition(ldaPstl5152.getVw_mail_Header().readNextRow("RMH")))
        {
            if (condition(ldaPstl5152.getMail_Header_Otgoing_Mail_Item_Id().notEquals(ldaPstl5152.getMail_Item_Pst_Rqst_Id())))                                           //Natural: IF MAIL-HEADER.OTGOING-MAIL-ITEM-ID NE MAIL-ITEM.PST-RQST-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, "Will now look for mail data (PHV recs) for:",ldaPstl5152.getMail_Header_Otgoing_Mail_Item_Id());                                   //Natural: WRITE ( 0 ) 'Will now look for mail data (PHV recs) for:' MAIL-HEADER.OTGOING-MAIL-ITEM-ID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RMH"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RMH"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            ldaPstl5152.getVw_mail_Data().startDatabaseRead                                                                                                               //Natural: READ MAIL-DATA BY MAIL-ID-DATA-TYP = MAIL-HEADER.MAIL-ID
            (
            "RMD",
            new Wc[] { new Wc("MAIL_ID_DATA_TYP", ">=", ldaPstl5152.getMail_Header_Mail_Id(), WcType.BY) },
            new Oc[] { new Oc("MAIL_ID_DATA_TYP", "ASC") }
            );
            RMD:
            while (condition(ldaPstl5152.getVw_mail_Data().readNextRow("RMD")))
            {
                if (condition(ldaPstl5152.getMail_Data_Mail_Id().notEquals(ldaPstl5152.getMail_Header_Mail_Id())))                                                        //Natural: IF MAIL-DATA.MAIL-ID NE MAIL-HEADER.MAIL-ID
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pls_Trace.getBoolean()))                                                                                                                    //Natural: IF +TRACE
                {
                    getReports().write(0, "    Delete MAIL-DATA:",ldaPstl5152.getMail_Data_Mail_Id());                                                                    //Natural: WRITE ( 0 ) '    Delete MAIL-DATA:' MAIL-DATA.MAIL-ID
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RMD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RMD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  NO GET RQRD: WILL DO FINAL ET AFTER HDR DELETED
                ldaPstl5152.getVw_mail_Data().deleteDBRow("RMD");                                                                                                         //Natural: DELETE ( RMD. )
                pnd_Count_Array_Pnd_Total_Recs_Purged.getValue(pnd_Count_Index).nadd(1);                                                                                  //Natural: ADD 1 TO #TOTAL-RECS-PURGED ( #COUNT-INDEX )
                                                                                                                                                                          //Natural: PERFORM END-TRANSACTION
                sub_End_Transaction();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RMD"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RMD"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RMH"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RMH"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, "    Delete MAIL-HEADER:",ldaPstl5152.getMail_Item_Pst_Rqst_Id());                                                                  //Natural: WRITE ( 0 ) '    Delete MAIL-HEADER:' MAIL-ITEM.PST-RQST-ID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RMH"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RMH"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            ldaPstl5152.getVw_mail_Header().deleteDBRow("RMH");                                                                                                           //Natural: DELETE ( RMH. )
            pnd_Count_Array_Pnd_Total_Recs_Purged.getValue(pnd_Count_Index).nadd(1);                                                                                      //Natural: ADD 1 TO #TOTAL-RECS-PURGED ( #COUNT-INDEX )
            //*  LAST ET FOR THIS MAIL-ITEM DONE AFTER
                                                                                                                                                                          //Natural: PERFORM END-TRANSACTION
            sub_End_Transaction();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RMH"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RMH"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            //*  HDR IS DELETED IN READ-PRIME.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Purge_Postv1_Records() throws Exception                                                                                                              //Natural: PURGE-POSTV1-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Purge_V1_Key_Rqst_Id.setValue(ldaPstl5152.getMail_Item_Pst_Rqst_Id());                                                                                        //Natural: ASSIGN #PURGE-V1-KEY.RQST-ID := MAIL-ITEM.PST-RQST-ID
        pnd_Purge_V1_Key_Rec_Type.setValue("B");                                                                                                                          //Natural: ASSIGN #PURGE-V1-KEY.REC-TYPE := 'B'
        ldaPstl5152.getVw_rqst_Header().startDatabaseRead                                                                                                                 //Natural: READ RQST-HEADER BY RQST-RECRD-TYP STARTING FROM #PURGE-V1-KEY
        (
        "R_P_V1",
        new Wc[] { new Wc("RQST_RECRD_TYP", ">=", pnd_Purge_V1_Key, WcType.BY) },
        new Oc[] { new Oc("RQST_RECRD_TYP", "ASC") }
        );
        R_P_V1:
        while (condition(ldaPstl5152.getVw_rqst_Header().readNextRow("R_P_V1")))
        {
            if (condition(ldaPstl5152.getRqst_Header_Pst_Rqst_Id().notEquals(pnd_Purge_V1_Key_Rqst_Id)))                                                                  //Natural: IF RQST-HEADER.PST-RQST-ID NE #PURGE-V1-KEY.RQST-ID
            {
                if (true) break R_P_V1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R-P-V1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, "Delete V1",ldaPstl5152.getRqst_Header_Pst_Rqst_Id(),"; record type",ldaPstl5152.getRqst_Header_Pst_Record_Typ());                  //Natural: WRITE ( 0 ) 'Delete V1' RQST-HEADER.PST-RQST-ID '; record type' RQST-HEADER.PST-RECORD-TYP
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R_P_V1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R_P_V1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  NO GET NECESSARY; ALL RQST RECS ARE SEQUENTIAL
            ldaPstl5152.getVw_rqst_Header().deleteDBRow("R_P_V1");                                                                                                        //Natural: DELETE ( R-P-V1. )
            pnd_Count_Array_Pnd_Total_Recs_Purged.getValue(pnd_Count_Index).nadd(1);                                                                                      //Natural: ADD 1 TO #TOTAL-RECS-PURGED ( #COUNT-INDEX )
            //*  FINAL ET IS DONE AFTER HDR (REC TYPE 'A')
                                                                                                                                                                          //Natural: PERFORM END-TRANSACTION
            sub_End_Transaction();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R_P_V1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R_P_V1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            //*  IS DELETED, RIGHT AFTER THIS IN READ-PRIME.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Purge_Header() throws Exception                                                                                                                      //Natural: PURGE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        G_PURGE_HDR:                                                                                                                                                      //Natural: GET PST-PURGE-V #HDR-ISN
        vw_pst_Purge_V.readByID(pnd_Hdr_Isn.getLong(), "G_PURGE_HDR");
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Delete OUT-MAIL hdr (PST-record):",ldaPstl5152.getMail_Item_Pst_Rqst_Id());                                                            //Natural: WRITE ( 0 ) 'Delete OUT-MAIL hdr (PST-record):' MAIL-ITEM.PST-RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        vw_pst_Purge_V.deleteDBRow("G_PURGE_HDR");                                                                                                                        //Natural: DELETE ( G-PURGE-HDR. )
        //*  BECAUSE NXT RQST TO BE DELETED MAY BE ANY NMBR OF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*                 /* RECORDS DOWN THE LINE AND HOLD Q CAN FILL UP.
        pnd_Count_Array_Pnd_Rqsts_Purged.getValue(pnd_Count_Index).nadd(1);                                                                                               //Natural: ADD 1 TO #RQSTS-PURGED ( #COUNT-INDEX )
        pnd_Count_Array_Pnd_Total_Recs_Purged.getValue(pnd_Count_Index).nadd(1);                                                                                          //Natural: ADD 1 TO #TOTAL-RECS-PURGED ( #COUNT-INDEX )
    }
    private void sub_Lookup_Purge_Criteria() throws Exception                                                                                                             //Natural: LOOKUP-PURGE-CRITERIA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Input.equals(" ")))                                                                                                                             //Natural: IF #INPUT = ' '
        {
            pdaPsta9017.getPsta9017_Tbl_Table_Nme().setValue("PST-PURGE-PARMS");                                                                                          //Natural: ASSIGN PSTA9017.TBL-TABLE-NME := 'PST-PURGE-PARMS'
            pdaPsta9017.getPsta9017_Tbl_Scrty_Level_Ind().setValue("A");                                                                                                  //Natural: ASSIGN PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
            pdaPsta9017.getPsta9017_Tbl_Key_Field().setValue("KEEP-DAYS");                                                                                                //Natural: ASSIGN PSTA9017.TBL-KEY-FIELD := 'KEEP-DAYS'
            DbsUtil.callnat(Pstn9017.class , getCurrentProcessState(), pdaPsta9017.getPsta9017());                                                                        //Natural: CALLNAT 'PSTN9017' PSTA9017
            if (condition(Global.isEscape())) return;
            if (condition(pdaPsta9017.getPsta9017_Valid_Entry().getBoolean()))                                                                                            //Natural: IF PSTA9017.VALID-ENTRY
            {
                pnd_Purge_Parms.setValue(pdaPsta9017.getPsta9017_Tbl_Data_Field());                                                                                       //Natural: ASSIGN #PURGE-PARMS := PSTA9017.TBL-DATA-FIELD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,"Purge parameters not found on PST-SUPPORT-TBL",NEWLINE,"(Table name:",pdaPsta9017.getPsta9017_Tbl_Table_Nme(), //Natural: WRITE ( 1 ) 'Purge parameters not found on PST-SUPPORT-TBL' / '(Table name:' TBL-TABLE-NME ', key KEEP-DAYS)' ////
                    ", key KEEP-DAYS)",NEWLINE,NEWLINE,NEWLINE,NEWLINE);
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB
                sub_End_Of_Job();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                DbsUtil.terminate(4);  if (true) return;                                                                                                                  //Natural: TERMINATE 4
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Purge_Parms.setValue(pnd_Input);                                                                                                                          //Natural: ASSIGN #PURGE-PARMS := #INPUT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (DbsUtil.maskMatches(pnd_Purge_Parms,"NNNNNN"))))                                                                                                 //Natural: IF #PURGE-PARMS NE MASK ( NNNNNN )
        {
            if (condition(pnd_Input.equals(" ")))                                                                                                                         //Natural: IF #INPUT = ' '
            {
                getReports().write(1, ReportOption.NOTITLE,"Purge parameters found on:",pdaPsta9017.getPsta9017_Tbl_Table_Nme(),NEWLINE,"key KEEP-DAYS, are not numeric.", //Natural: WRITE ( 1 ) 'Purge parameters found on:' TBL-TABLE-NME / 'key KEEP-DAYS, are not numeric.' ////
                    NEWLINE,NEWLINE,NEWLINE,NEWLINE);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,"Purge parameters input via INPUT are not numeric",NEWLINE,NEWLINE,NEWLINE,NEWLINE);                           //Natural: WRITE ( 1 ) 'Purge parameters input via INPUT are not numeric' ////
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB
            sub_End_Of_Job();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,"Parameters used for this run - FAILED:",pnd_Purge_Parms_Pnd_Failed_Days,"days; GOOD:",pnd_Purge_Parms_Pnd_Good_Days, //Natural: WRITE ( 1 ) 'Parameters used for this run - FAILED:' #FAILED-DAYS 'days; GOOD:' #GOOD-DAYS 'days' ////
                "days",NEWLINE,NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_End_Of_Job() throws Exception                                                                                                                        //Natural: END-OF-JOB
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #COUNT-INDEX = 1 TO #COUNT-ARRAY-MAX-ELS
        for (pnd_Count_Index.setValue(1); condition(pnd_Count_Index.lessOrEqual(pnd_Count_Array_Max_Els)); pnd_Count_Index.nadd(1))
        {
            getReports().display(1, "Package  Vers/  Code   Code",                                                                                                        //Natural: DISPLAY ( 1 ) 'Package  Vers/  Code   Code' #PCKGE-CDE-VRSN-CDE ( #COUNT-INDEX ) 'Requests/Purged' #RQSTS-PURGED ( #COUNT-INDEX ) 'Records/Purged' #TOTAL-RECS-PURGED ( #COUNT-INDEX )
            		pnd_Count_Array_Pnd_Pckge_Cde_Vrsn_Cde.getValue(pnd_Count_Index),"Requests/Purged",
            		pnd_Count_Array_Pnd_Rqsts_Purged.getValue(pnd_Count_Index),"Records/Purged",
            		pnd_Count_Array_Pnd_Total_Recs_Purged.getValue(pnd_Count_Index));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"        *** END OF REPORT ***");                                                      //Natural: WRITE ( 1 ) //// '        *** END OF REPORT ***'
        if (Global.isEscape()) return;
    }
    private void sub_End_Transaction() throws Exception                                                                                                                   //Natural: END-TRANSACTION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Et_Count.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #ET-COUNT
        if (condition(pnd_Et_Count.greater(50)))                                                                                                                          //Natural: IF #ET-COUNT > 50
        {
            pnd_Et_Count.reset();                                                                                                                                         //Natural: RESET #ET-COUNT
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Find_Count_Element() throws Exception                                                                                                                //Natural: FIND-COUNT-ELEMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  DETERMINE PACKAGE-CDE/VRSN-CDE AND FIND ELEMENT IN COUNT ARRAY
        if (condition(ldaPstl5152.getMail_Item_Pckge_Cde().equals(pnd_Count_Array_Search_Key_Pckge_Cde) && ldaPstl5152.getMail_Item_Pckge_Vrsn_Cde().equals(pnd_Count_Array_Search_Key_Pckge_Vrsn_Cde))) //Natural: IF MAIL-ITEM.PCKGE-CDE = #COUNT-ARRAY-SEARCH-KEY.PCKGE-CDE AND MAIL-ITEM.PCKGE-VRSN-CDE = #COUNT-ARRAY-SEARCH-KEY.PCKGE-VRSN-CDE
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Count_Array_Search_Key_Pnd_Count_Array_Search.setValuesByName(ldaPstl5152.getVw_mail_Item());                                                                 //Natural: MOVE BY NAME MAIL-ITEM TO #COUNT-ARRAY-SEARCH
        if (condition(pnd_Count_Array_Max_Els.greater(getZero())))                                                                                                        //Natural: IF #COUNT-ARRAY-MAX-ELS > 0
        {
            DbsUtil.examine(new ExamineSource(pnd_Count_Array_Pnd_Pckge_Cde_Vrsn_Cde.getValue(1,":",pnd_Count_Array_Max_Els),true), new ExamineSearch(pnd_Count_Array_Search_Key,  //Natural: EXAMINE FULL #COUNT-ARRAY.#PCKGE-CDE-VRSN-CDE ( 1:#COUNT-ARRAY-MAX-ELS ) FULL #COUNT-ARRAY-SEARCH-KEY GIVING INDEX IN #COUNT-INDEX
                true), new ExamineGivingIndex(pnd_Count_Index));
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, "Looking for:",pnd_Count_Array_Search_Key,"got index:",pnd_Count_Index);                                                            //Natural: WRITE ( 0 ) 'Looking for:' #COUNT-ARRAY-SEARCH-KEY 'got index:' #COUNT-INDEX
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Count_Index.reset();                                                                                                                                      //Natural: RESET #COUNT-INDEX
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Count_Index.equals(getZero())))                                                                                                                 //Natural: IF #COUNT-INDEX = 0
        {
            //* EG9705
                                                                                                                                                                          //Natural: PERFORM GET-POST-VERSION-CODE
            sub_Get_Post_Version_Code();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //* EG9705
            if (condition(check_To_Purge_Vars_Pnd_Purge_Rqst.equals(false)))                                                                                              //Natural: IF #PURGE-RQST = FALSE
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Array_Max_Els.nadd(1);                                                                                                                              //Natural: ADD 1 TO #COUNT-ARRAY-MAX-ELS
            if (condition(pnd_Count_Array_Max_Els.greater(1000)))                                                                                                         //Natural: IF #COUNT-ARRAY-MAX-ELS > 1000
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Max Count Array Size exceeded");                                                                      //Natural: ASSIGN MSG-INFO-SUB.##MSG := 'Max Count Array Size exceeded'
                                                                                                                                                                          //Natural: PERFORM TERMINATE-JOB
                sub_Terminate_Job();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Index.setValue(pnd_Count_Array_Max_Els);                                                                                                            //Natural: ASSIGN #COUNT-INDEX := #COUNT-ARRAY-MAX-ELS
            pnd_Count_Array_Pnd_Pckge_Cde_Vrsn_Cde.getValue(pnd_Count_Index).setValue(pnd_Count_Array_Search_Key);                                                        //Natural: ASSIGN #COUNT-ARRAY.#PCKGE-CDE-VRSN-CDE ( #COUNT-INDEX ) := #COUNT-ARRAY-SEARCH-KEY
            pnd_Count_Array_Pnd_Pckge_Post_Vrsn_Cde.getValue(pnd_Count_Index).setValue(pdaPsta7205.getPsta7205_Pckge_Post_Vrsn_Cde());                                    //Natural: ASSIGN #COUNT-ARRAY.#PCKGE-POST-VRSN-CDE ( #COUNT-INDEX ) := PSTA7205.PCKGE-POST-VRSN-CDE
            //*  EG9705 -   END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Post_Version_Code() throws Exception                                                                                                             //Natural: GET-POST-VERSION-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*     READS  PST-STNDRD-PCKGE TO RETRIEVE POST VERSION STATUS CODE
        //*     THE POST VERSION CODE WILL BE USED TO DECIDE HOW PURGE IS DONE.
        pdaPsta7205.getPsta7205().reset();                                                                                                                                //Natural: RESET PSTA7205
        pdaPsta7205.getPsta7205_Pckge_Cde().setValue(ldaPstl5152.getMail_Item_Pckge_Cde());                                                                               //Natural: ASSIGN PSTA7205.PCKGE-CDE := MAIL-ITEM.PCKGE-CDE
        pdaPsta7205.getPsta7205_Pckge_Vrsn_Cde().setValue(ldaPstl5152.getMail_Item_Pckge_Vrsn_Cde());                                                                     //Natural: ASSIGN PSTA7205.PCKGE-VRSN-CDE := MAIL-ITEM.PCKGE-VRSN-CDE
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("GET");                                                                                                               //Natural: ASSIGN CDAOBJ.#FUNCTION := 'GET'
        DbsUtil.callnat(Pstn7220.class , getCurrentProcessState(), pdaPsta7205.getPsta7205(), pdaPsta7205.getPsta7205_Id(), pdaPsta7206.getPsta7206(),                    //Natural: CALLNAT 'PSTN7220' PSTA7205 PSTA7205-ID PSTA7206 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaPstpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals(pnd_Error_Ind)))                                                                           //Natural: IF MSG-INFO-SUB.##RETURN-CODE = #ERROR-IND
        {
            check_To_Purge_Vars_Pnd_Purge_Rqst.setValue(false);                                                                                                           //Natural: ASSIGN #PURGE-RQST := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //* GET-POST-VERSION-CODE
    }
    private void sub_Terminate_Job() throws Exception                                                                                                                     //Natural: TERMINATE-JOB
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*     TERMINATE JOB DUE TO AN ERROR.
        //*     NOTE:  THERE IS NO RETURN FROM THIS SUBROUTINE.
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        //* EXPAND ##ERROR-MSG TO MAKE IT READABLE
        DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaPstpda_M.getMsg_Info_Sub());                                                                        //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        getReports().write(0, "Job terminated due to following error(s):",NEWLINE,NEWLINE,pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),NEWLINE,NEWLINE,"Check list of errors above."); //Natural: WRITE ( 0 ) 'Job terminated due to following error(s):' // MSG-INFO-SUB.##MSG // 'Check list of errors above.'
        if (Global.isEscape()) return;
        ldaPstl5152.getPnd_Error_Nr().reset();                                                                                                                            //Natural: RESET #ERROR-NR #ERROR-LINE #ERROR-LEVEL
        ldaPstl5152.getPnd_Error_Line().reset();
        ldaPstl5152.getPnd_Error_Level().reset();
        //*  WILL TERMINATE WITH COND CODE 4
        Global.getSTACK().pushData(StackOption.TOP, ldaPstl5152.getPnd_Error_Nr(), ldaPstl5152.getPnd_Error_Line(), ldaPstl5152.getPnd_Error_Status(),                    //Natural: FETCH 'INFP9000' #ERROR-NR #ERROR-LINE #ERROR-STATUS #ERROR-PROGRAM #ERROR-LEVEL #ERROR-APPL
            ldaPstl5152.getPnd_Error_Program(), ldaPstl5152.getPnd_Error_Level(), ldaPstl5152.getPnd_Error_Appl());
        Global.setFetchProgram(DbsUtil.getBlType("INFP9000"));
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(15),pnd_Header1_1,new TabSetting(71),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 15T #HEADER1-1 71T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 15T #HEADER1-2 71T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(15),pnd_Header1_2,new TabSetting(71),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    //* *SAG DEFINE EXIT REPORT1-AT-TOP-OF-PAGE
                    //* *SAG END-EXIT
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //* ***********************************************************************
        //*    START OF COPY CODE PSTC9902   : RETRY LOGIC
        //*  - FIRST CHECK FOR SUBSEQUENT RETRY - SAME LINE NUMBER.
        //*  - IF SUBSEQUENT RETRY, CHECK TOTAL ELAPSED TIME
        //*  - IF NEW RETRY, SET DEFAULT VALUES FOR WAIT PARAMETERS IF NONE SPECI-
        //*    FIED, START CLOCK.
        //*  - CALL CMROLL TO ACTIVATE WAITING PERIOD
        //*  - RETRY
        //*  ----------------------------------------------------------------------
        //*         PROGRAM USING THIS COPYCODE MUST USE LDA PSTL9902
        //*                                          AND PDA PSTA9200 AS AN LDA.
        //*         #WAIT-TIME & #TOTAL-WAITING-TIME MAY BE SPECIFIED BY THE
        //*         PROGRAM USING THIS COPY CODE, JUST BEFORE THE INCLUDE STATEMENT
        //*         THEY ARE BOTH IN T FORMAT. TO INITIALIZE A T FIELD, USE
        //*         MOVE T'00:mm:ss' TO #WAIT-TIME
        //*         WHERE SS = SECONDS  (MAX OF 99 SECONDS)
        //*         (THE LIMITATION IS BECAUSE CMROLL ACCEPT UP TO 99 SECONDS ONLY)
        //*  ----------------------------------------------------------------------
        //* ***********************************************************************
        if (condition(Global.getERROR_NR().equals(3145)))                                                                                                                 //Natural: IF *ERROR-NR = 3145
        {
            ldaPstl9902.getPstl9902_Pnd_Start_Time_This_Error().setValue(Global.getTIMX());                                                                               //Natural: ASSIGN PSTL9902.#START-TIME-THIS-ERROR := *TIMX
            if (condition(ldaPstl9902.getPstl9902_Pnd_Prev_Error_Line().equals(Global.getERROR_LINE())))                                                                  //Natural: IF PSTL9902.#PREV-ERROR-LINE = *ERROR-LINE
            {
                ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time()),                  //Natural: ASSIGN PSTL9902.#TOTAL-ELAPSED-TIME := *TIMX - PSTL9902.#TIME-STARTED
                    Global.getTIMX().subtract(ldaPstl9902.getPstl9902_Pnd_Time_Started()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl9902.getPstl9902_Pnd_Time_Started().setValue(Global.getTIMX());                                                                                    //Natural: ASSIGN PSTL9902.#TIME-STARTED := *TIMX
                ldaPstl9902.getPstl9902_Pnd_Prev_Error_Line().setValue(Global.getERROR_LINE());                                                                           //Natural: ASSIGN PSTL9902.#PREV-ERROR-LINE := *ERROR-LINE
                short decideConditionsMet670 = 0;                                                                                                                         //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN PSTL9902.#WAIT-TIME LE 0
                if (condition(ldaPstl9902.getPstl9902_Pnd_Wait_Time().lessOrEqual(getZero())))
                {
                    decideConditionsMet670++;
                    if (condition(DbsUtil.maskMatches(Global.getINIT_USER(),"'RPC'.....") || DbsUtil.maskMatches(Global.getINIT_USER(),"'SNY'.....")))                    //Natural: IF *INIT-USER = MASK ( 'RPC'..... ) OR = MASK ( 'SNY'..... )
                    {
                        ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(20);                                                                                             //Natural: MOVE T'00:00:02' TO PSTL9902.#WAIT-TIME
                        getReports().write(0, "Setting WAIT TIME to",ldaPstl9902.getPstl9902_Pnd_Wait_Time());                                                            //Natural: WRITE 'Setting WAIT TIME to' PSTL9902.#WAIT-TIME
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                //Natural: IF *DEVICE = 'BATCH'
                        {
                            ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(150);                                                                                        //Natural: MOVE T'00:00:15' TO PSTL9902.#WAIT-TIME
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaPstl9902.getPstl9902_Pnd_Wait_Time().setValue(40);                                                                                         //Natural: MOVE T'00:00:04' TO PSTL9902.#WAIT-TIME
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN PSTL9902.#TOTAL-WAITING-TIME LE 0
                if (condition(ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().lessOrEqual(getZero())))
                {
                    decideConditionsMet670++;
                    if (condition(DbsUtil.maskMatches(Global.getINIT_USER(),"'RPC'.....") || DbsUtil.maskMatches(Global.getINIT_USER(),"'SNY'.....")))                    //Natural: IF *INIT-USER = MASK ( 'RPC'..... ) OR = MASK ( 'SNY'..... )
                    {
                        ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(100);                                                                                   //Natural: MOVE T'00:00:10' TO PSTL9902.#TOTAL-WAITING-TIME
                        getReports().write(0, "Setting TOTAL WAIT TIME to",ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time());                                             //Natural: WRITE 'Setting TOTAL WAIT TIME to' PSTL9902.#TOTAL-WAITING-TIME
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                //Natural: IF *DEVICE = 'BATCH'
                        {
                            ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(18000);                                                                             //Natural: MOVE T'00:30:00' TO PSTL9902.#TOTAL-WAITING-TIME
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time().setValue(900);                                                                               //Natural: MOVE T'00:01:30' TO PSTL9902.#TOTAL-WAITING-TIME
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet670 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaPstl9902.getPstl9902_Pnd_Total_Elapsed_Time().greater(ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time())))                                    //Natural: IF PSTL9902.#TOTAL-ELAPSED-TIME GT PSTL9902.#TOTAL-WAITING-TIME
            {
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    getReports().write(0, "Requested record in program",Global.getPROGRAM(),"line",Global.getERROR_LINE(),"was still on hold after",NEWLINE,ldaPstl9902.getPstl9902_Pnd_Total_Waiting_Time(),  //Natural: WRITE 'Requested record in program' *PROGRAM 'line' *ERROR-LINE 'was still on hold after' / PSTL9902.#TOTAL-WAITING-TIME ( EM = HH:II:SS )
                        new ReportEditMask ("HH:II:SS"));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaPsta9200.getPsta9200_Message_Text().setValue(DbsUtil.compress("Requested record in program", Global.getPROGRAM(), "line", Global.getERROR_LINE(),  //Natural: COMPRESS 'Requested record in program' *PROGRAM 'line' *ERROR-LINE 'exceeded hold time limit' INTO PSTA9200.MESSAGE-TEXT
                        "exceeded hold time limit"));
                    pdaPsta9200.getPsta9200_Enter_Allowed().reset();                                                                                                      //Natural: RESET PSTA9200.ENTER-ALLOWED PSTA9200.CONFIRM-ALLOWED
                    pdaPsta9200.getPsta9200_Confirm_Allowed().reset();
                    pdaPsta9200.getPsta9200_Exit_Allowed().setValue(true);                                                                                                //Natural: ASSIGN PSTA9200.EXIT-ALLOWED := TRUE
                    DbsUtil.callnat(Pstn9200.class , getCurrentProcessState(), pdaPsta9200.getPsta9200());                                                                //Natural: CALLNAT 'PSTN9200' PSTA9200
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl9902.getPstl9902_Pnd_Time_Elapsed().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Time_Elapsed()), Global.getTIMX().subtract(ldaPstl9902.getPstl9902_Pnd_Start_Time_This_Error())); //Natural: ASSIGN PSTL9902.#TIME-ELAPSED := *TIMX - PSTL9902.#START-TIME-THIS-ERROR
                if (condition(ldaPstl9902.getPstl9902_Pnd_Time_Elapsed().less(ldaPstl9902.getPstl9902_Pnd_Wait_Time())))                                                  //Natural: IF PSTL9902.#TIME-ELAPSED LT PSTL9902.#WAIT-TIME
                {
                    ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left().compute(new ComputeParameters(false, ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left()), ldaPstl9902.getPstl9902_Pnd_Wait_Time().subtract(ldaPstl9902.getPstl9902_Pnd_Time_Elapsed())); //Natural: ASSIGN PSTL9902.#WAIT-TIME-LEFT := PSTL9902.#WAIT-TIME - PSTL9902.#TIME-ELAPSED
                    ldaPstl9902.getPstl9902_Pnd_Seconds_Left_A().setValueEdited(ldaPstl9902.getPstl9902_Pnd_Wait_Time_Left(),new ReportEditMask("SS"));                   //Natural: MOVE EDITED PSTL9902.#WAIT-TIME-LEFT ( EM = SS ) TO PSTL9902.#SECONDS-LEFT-A
                    ldaPstl9902.getPstl9902_Pnd_Wait_Seconds_Left().setValue(ldaPstl9902.getPstl9902_Pnd_Seconds_Left_N());                                               //Natural: ASSIGN PSTL9902.#WAIT-SECONDS-LEFT := PSTL9902.#SECONDS-LEFT-N
                    cmrollReturnCode = DbsUtil.callExternalProgram("CMROLL",ldaPstl9902.getPstl9902_Pnd_Wait_Seconds_Left(),ldaPstl9902.getPstl9902_Pnd_Cics_Rqst());     //Natural: CALL 'CMROLL' PSTL9902.#WAIT-SECONDS-LEFT PSTL9902.#CICS-RQST
                }                                                                                                                                                         //Natural: END-IF
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*     END OF COPYCODE PSTC9902
        //* ***********************************************************************
        ldaPstl5152.getPnd_Error_Nr().setValue(Global.getERROR_NR());                                                                                                     //Natural: ASSIGN #ERROR-NR := *ERROR-NR
        ldaPstl5152.getPnd_Error_Line().setValue(Global.getERROR_LINE());                                                                                                 //Natural: ASSIGN #ERROR-LINE := *ERROR-LINE
        ldaPstl5152.getPnd_Error_Level().setValue(Global.getLEVEL());                                                                                                     //Natural: ASSIGN #ERROR-LEVEL := *LEVEL
        //*  WILL TERMINATE WITH COND CODE 4
        Global.getSTACK().pushData(StackOption.TOP, ldaPstl5152.getPnd_Error_Nr(), ldaPstl5152.getPnd_Error_Line(), ldaPstl5152.getPnd_Error_Status(),                    //Natural: FETCH 'INFP9000' #ERROR-NR #ERROR-LINE #ERROR-STATUS #ERROR-PROGRAM #ERROR-LEVEL #ERROR-APPL
            ldaPstl5152.getPnd_Error_Program(), ldaPstl5152.getPnd_Error_Level(), ldaPstl5152.getPnd_Error_Appl());
        Global.setFetchProgram(DbsUtil.getBlType("INFP9000"));
        if (condition(Global.isEscape())) return;
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=80 PS=55 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(0, "LS=132");

        getReports().setDisplayColumns(1, "Package  Vers/  Code   Code",
        		pnd_Count_Array_Pnd_Pckge_Cde_Vrsn_Cde,"Requests/Purged",
        		pnd_Count_Array_Pnd_Rqsts_Purged,"Records/Purged",
        		pnd_Count_Array_Pnd_Total_Recs_Purged);
    }
}
