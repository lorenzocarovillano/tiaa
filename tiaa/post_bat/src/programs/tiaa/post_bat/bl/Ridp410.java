/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:21:55 PM
**        * FROM NATURAL PROGRAM : Ridp410
************************************************************
**        * FILE NAME            : Ridp410.java
**        * CLASS NAME           : Ridp410
**        * INSTANCE NAME        : Ridp410
************************************************************
************************************************************************
** PROGRAM     : RIDP410                                              **
** AUTHOR      : JOANNES C. AVE                                       **
** DATE        : 10/11/2006                                           **
** DESCRIPTION : READS DA REPORTING TIAA ACCESS EXTRACT AND REFORMATS **
**               DATA AS FEED TO RIDP412                              **
**               PACKAGE CODES: RDR2009D,RDR2009E,RDR2009F,RDR2009G,  **
**                              RDR2009H,RDR2009I,RDR2009J,RDR2009K,  **
**                              RDR2009L,RDR2009M,RDR2009N,RDR2009O,  **
**                              RDR2009P,RDR2009Q                     **
************************************************************************
** HISTORY:                                                           **
** 10/11/2006  : INITIAL IMPLEMENTATION                               **
** 05/29/2007  : ADD INPUT FIELD #COVER-LETTER-IND AND PASS IT TO     **
**               OUTPUT FIELD #LETTER-TYPE TO DETERMINE THE COVER     **
**               LETTER TAG IN RIDN414.                               **
** 06/01/2007  : ADD #CONTRACTS ARRAY IN THE OUTPUT FILE LAYOUT       **
**               ADD SUBROUTINE ADD-CONTRACT-TO-ARRAY                 **
** 09/14/2009  : CONVERT TO ECS                                       **
** 04/13/2011  : ADD E-DELIVERY SELECTION AND PACKAGE RDR2009Q        **
**             : SET ENDORSEMENT TO 1 PER LOB                         **
** 2016/06/30  : ELIMINATE RDR2009E TO Q AND LUMP EVERYTHING TO       **
**             : RDR2009D AS PART OF THE DCS ELIMINATION PROJECT      **
** 2016/07/19  : REPLACE CALL TO COR/NAD WITH MDMN MODULES AND REMARK **
**               CALL TO CPM                                          **
** 2017/09/09  : PIN EXPANSION - RESTOW FOR RIDA410       /* PINEXP   **
**               MDM CALL CHANGE                          /* PINEXP   **
**               COMMENTED CPM RELATED CODE               /* PINEXP   **
** 2017/10     : PROD FIX TO INCLUDE AR/OR GSRA, WI GRA,  /* LS1      **
**                                   NH FOR ALL                       **
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridp410 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;
    private PdaRida410 pdaRida410;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Pin_A12;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Pin_N5;
    private DbsField pnd_Input_Record_Pnd_Pin_N7;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Pin_N12;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Date;
    private DbsField pnd_Input_Record_Pnd_Issue_State_Cde;
    private DbsField pnd_Input_Record_Pnd_Plan_Cde;
    private DbsField pnd_Input_Record_Pnd_Cover_Letter_Ind;
    private DbsField pnd_Input_Record_Pnd_Level;
    private DbsField pnd_Input_Record_Pnd_State_Inst;
    private DbsField pnd_Input_Record_Pnd_Inst_Name;
    private DbsField pnd_Prev_Inst_Name;
    private DbsField pnd_Prev_Pin_N12;

    private DbsGroup pnd_Prev_Pin_N12__R_Field_4;
    private DbsField pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5;
    private DbsField pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7;
    private DbsField pnd_Prev_Contract;
    private DbsField pnd_Prev_Cover;
    private DbsField pnd_Prev_Level;
    private DbsField pnd_Nbr_Pages;
    private DbsField pnd_J;
    private DbsField pnd_Issue_State;
    private DbsField pnd_Valid_Record;
    private DbsField pnd_Accepted;
    private DbsField pnd_Excluded;
    private DbsField pnd_With_State_Exclusion;
    private DbsField pnd_Remarks;
    private DbsField pnd_Lob;
    private DbsField pnd_Cutoff;
    private DbsField pnd_Wmmm;
    private DbsField pnd_Participant_Name;
    private DbsField pnd_Ctr_Access_Contract_Read;
    private DbsField pnd_Ctr_Valid_Access_Contract_Read;
    private DbsField pnd_Ctr_Da_Contract_Not_In_Cor;
    private DbsField pnd_Ctr_Da_Inactive_In_Cor;
    private DbsField pnd_Ctr_Ph_Deceased;
    private DbsField pnd_Ctr_Ph_With_Mail_Suppression;
    private DbsField pnd_Ctr_Contract_Accepted;
    private DbsField pnd_Ctr_Contract_Rejected;
    private DbsField pnd_Ctr_Contract_Read;
    private DbsField pnd_Ctr_Duplicate_Contract;
    private DbsField pnd_Ctr_Terminated_Contract;
    private DbsField pnd_Ctr_Contract_Inactive;
    private DbsField pnd_Ctr_Contract_Excluded;
    private DbsField pnd_Ctr_Out_Of_State;
    private DbsField pnd_Ctr_Contract_Outof_Range;
    private DbsField pnd_Ctr_Plan_Not_For_Proc;
    private DbsField pnd_Ctr_State_Inst_Not_For_Proc;
    private DbsField pnd_Ctr_Institution_Owned;
    private DbsField pnd_Ctr_Total_Contracts;
    private DbsField pnd_Ctr_Total_Ra_St;
    private DbsField pnd_Ctr_Total_Ra_Rp;
    private DbsField pnd_Ctr_Total_Ra_Sv;
    private DbsField pnd_Ctr_Total_Sr_St;
    private DbsField pnd_Ctr_Total_Sr_Rp;
    private DbsField pnd_Ctr_Total_Sr_Sv;
    private DbsField pnd_Ctr_Total_Gr_St;
    private DbsField pnd_Ctr_Total_Gr_Sv;
    private DbsField pnd_Ctr_Total_Gs_St;
    private DbsField pnd_Ctr_Total_Gs_Sv;
    private DbsField pnd_Ctr_Exclude_Ga;
    private DbsField pnd_Ctr_Exclude_Ok;
    private DbsField pnd_Ctr_Exclude_Ms;
    private DbsField pnd_State_Unknown;

    private DbsGroup pnd_State_Unknown__R_Field_5;
    private DbsField pnd_State_Unknown_Pnd_State_Unknown_A;
    private DbsField pnd_State_Unknown_Pnd_State_Unknown_B;
    private DbsField pnd_Break;
    private DbsField pnd_Ndx;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_K;

    private DbsGroup pnd_Arrays;
    private DbsField pnd_Arrays_Pnd_Combo_Prod;
    private DbsField pnd_Arrays_Pnd_Combo_Ctr;
    private DbsField pnd_Combo;
    private DbsField pnd_Type_Ra;
    private DbsField pnd_Type_Sra;
    private DbsField pnd_Type_Gra;
    private DbsField pnd_Type_Gsra;

    private DbsGroup pnd_State_Array;
    private DbsField pnd_State_Array_Pnd_State_Cde_N;
    private DbsField pnd_State_Array_Pnd_State_Cde_A;
    private DbsField pnd_State_Array_Pnd_State_Ra;
    private DbsField pnd_State_Array_Pnd_State_Sra;
    private DbsField pnd_State_Array_Pnd_State_Gra;
    private DbsField pnd_State_Array_Pnd_State_Gsra;
    private DbsField pnd_Ctr_Ra_Sv_Nj;
    private DbsField pnd_Ctr_Ra_Sv_Ct;
    private DbsField pnd_Ctr_Ra_Sv_Fl;
    private DbsField pnd_Ctr_Ra_Sv_Mo;
    private DbsField pnd_Ctr_Ra_Sv_Mt;
    private DbsField pnd_Ctr_Ra_Sv_Ne;
    private DbsField pnd_Ctr_Ra_Sv_Nh;
    private DbsField pnd_Ctr_Ra_Sv_Ok;
    private DbsField pnd_Ctr_Ra_Sv_Or;
    private DbsField pnd_Ctr_Ra_Sv_Pa;
    private DbsField pnd_Ctr_Ra_Sv_Pr;
    private DbsField pnd_Ctr_Ra_Sv_Sc;
    private DbsField pnd_Ctr_Ra_Sv_Tx;
    private DbsField pnd_Ctr_Ra_Sv_Ut;
    private DbsField pnd_Ctr_Ra_Sv_Va;
    private DbsField pnd_Ctr_Ra_Sv_Wi;
    private DbsField pnd_Ctr_Sra_Sv_Nj;
    private DbsField pnd_Ctr_Sra_Sv_Ct;
    private DbsField pnd_Ctr_Sra_Sv_Fl;
    private DbsField pnd_Ctr_Sra_Sv_Mo;
    private DbsField pnd_Ctr_Sra_Sv_Mt;
    private DbsField pnd_Ctr_Sra_Sv_Ne;
    private DbsField pnd_Ctr_Sra_Sv_Nh;
    private DbsField pnd_Ctr_Sra_Sv_Ok;
    private DbsField pnd_Ctr_Sra_Sv_Or;
    private DbsField pnd_Ctr_Sra_Sv_Pa;
    private DbsField pnd_Ctr_Sra_Sv_Sc;
    private DbsField pnd_Ctr_Sra_Sv_Tx;
    private DbsField pnd_Ctr_Sra_Sv_Va;
    private DbsField pnd_Ctr_Sra_Sv_Wi;
    private DbsField pnd_Ctr_Sra_Sv_Vt;
    private DbsField pnd_Ctr_Sra_Sv_Ut;
    private DbsField pnd_Ctr_Gra_Sv_Nj;
    private DbsField pnd_Ctr_Gra_Sv_Njabp;
    private DbsField pnd_Ctr_Gra_Sv_Ct;
    private DbsField pnd_Ctr_Gra_Sv_Fl;
    private DbsField pnd_Ctr_Gra_Sv_Mo;
    private DbsField pnd_Ctr_Gra_Sv_Mt;
    private DbsField pnd_Ctr_Gra_Sv_Ne;
    private DbsField pnd_Ctr_Gra_Sv_Nh;
    private DbsField pnd_Ctr_Gra_Sv_Ok;
    private DbsField pnd_Ctr_Gra_Sv_Or;
    private DbsField pnd_Ctr_Gra_Sv_Pa;
    private DbsField pnd_Ctr_Gra_Sv_Pr;
    private DbsField pnd_Ctr_Gra_Sv_Sc;
    private DbsField pnd_Ctr_Gra_Sv_Tx;
    private DbsField pnd_Ctr_Gra_Sv_Va;
    private DbsField pnd_Ctr_Gra_Sv_Wi;
    private DbsField pnd_Ctr_Gra_Sv_Nc;
    private DbsField pnd_Ctr_Gsra_Sv_Nj;
    private DbsField pnd_Ctr_Gsra_Sv_Suny;
    private DbsField pnd_Ctr_Gsra_Sv_Ct;
    private DbsField pnd_Ctr_Gsra_Sv_Fl;
    private DbsField pnd_Ctr_Gsra_Sv_Mo;
    private DbsField pnd_Ctr_Gsra_Sv_Mt;
    private DbsField pnd_Ctr_Gsra_Sv_Ne;
    private DbsField pnd_Ctr_Gsra_Sv_Nh;
    private DbsField pnd_Ctr_Gsra_Sv_Ok;
    private DbsField pnd_Ctr_Gsra_Sv_Or;
    private DbsField pnd_Ctr_Gsra_Sv_Pa;
    private DbsField pnd_Ctr_Gsra_Sv_Sc;
    private DbsField pnd_Ctr_Gsra_Sv_Tx;
    private DbsField pnd_Ctr_Gsra_Sv_Va;
    private DbsField pnd_Ctr_Gsra_Sv_Wi;
    private DbsField pnd_Ctr_Gsra_Sv_Mi;
    private DbsField pnd_Ctr_Gsra_Sv_Ar;
    private DbsField pnd_Ctr_Gsra_Sv_Ky;
    private DbsField pnd_Ctr_Gsra_Sv_Tn;
    private DbsField pnd_Ctr_Gsra_Sv_Vt;
    private DbsField pnd_Ctr_Gsra_Sv_Hi;
    private DbsField pnd_Residence_State_Cde;
    private DbsField pnd_Pst_Tbl_Data_Field1;

    private DbsGroup pnd_Pst_Tbl_Data_Field1__R_Field_6;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_State;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Filler;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);
        pdaRida410 = new PdaRida410(localVariables);

        // Local Variables
        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 175);

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Pin_A12 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N5 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N5", "#PIN-N5", FieldType.NUMERIC, 5);
        pnd_Input_Record_Pnd_Pin_N7 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N12 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N12", "#PIN-N12", FieldType.NUMERIC, 12);
        pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr", "#TIAA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cntrct_Issue_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Date", "#CNTRCT-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Issue_State_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Issue_State_Cde", "#ISSUE-STATE-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Plan_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Plan_Cde", "#PLAN-CDE", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cover_Letter_Ind = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cover_Letter_Ind", "#COVER-LETTER-IND", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Level = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Level", "#LEVEL", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_State_Inst = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_State_Inst", "#STATE-INST", FieldType.STRING, 
            5);
        pnd_Input_Record_Pnd_Inst_Name = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Inst_Name", "#INST-NAME", FieldType.STRING, 
            120);
        pnd_Prev_Inst_Name = localVariables.newFieldInRecord("pnd_Prev_Inst_Name", "#PREV-INST-NAME", FieldType.STRING, 120);
        pnd_Prev_Pin_N12 = localVariables.newFieldInRecord("pnd_Prev_Pin_N12", "#PREV-PIN-N12", FieldType.NUMERIC, 12);

        pnd_Prev_Pin_N12__R_Field_4 = localVariables.newGroupInRecord("pnd_Prev_Pin_N12__R_Field_4", "REDEFINE", pnd_Prev_Pin_N12);
        pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5 = pnd_Prev_Pin_N12__R_Field_4.newFieldInGroup("pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5", "#PREV-PIN-N5", FieldType.NUMERIC, 
            5);
        pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7 = pnd_Prev_Pin_N12__R_Field_4.newFieldInGroup("pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7", "#PREV-PIN-N7", FieldType.NUMERIC, 
            7);
        pnd_Prev_Contract = localVariables.newFieldInRecord("pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Cover = localVariables.newFieldInRecord("pnd_Prev_Cover", "#PREV-COVER", FieldType.STRING, 1);
        pnd_Prev_Level = localVariables.newFieldInRecord("pnd_Prev_Level", "#PREV-LEVEL", FieldType.STRING, 1);
        pnd_Nbr_Pages = localVariables.newFieldInRecord("pnd_Nbr_Pages", "#NBR-PAGES", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_Issue_State = localVariables.newFieldInRecord("pnd_Issue_State", "#ISSUE-STATE", FieldType.STRING, 2);
        pnd_Valid_Record = localVariables.newFieldInRecord("pnd_Valid_Record", "#VALID-RECORD", FieldType.BOOLEAN, 1);
        pnd_Accepted = localVariables.newFieldInRecord("pnd_Accepted", "#ACCEPTED", FieldType.BOOLEAN, 1);
        pnd_Excluded = localVariables.newFieldInRecord("pnd_Excluded", "#EXCLUDED", FieldType.BOOLEAN, 1);
        pnd_With_State_Exclusion = localVariables.newFieldInRecord("pnd_With_State_Exclusion", "#WITH-STATE-EXCLUSION", FieldType.BOOLEAN, 1);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 40);
        pnd_Lob = localVariables.newFieldInRecord("pnd_Lob", "#LOB", FieldType.STRING, 4);
        pnd_Cutoff = localVariables.newFieldInRecord("pnd_Cutoff", "#CUTOFF", FieldType.STRING, 6);
        pnd_Wmmm = localVariables.newFieldInRecord("pnd_Wmmm", "#WMMM", FieldType.STRING, 2);
        pnd_Participant_Name = localVariables.newFieldInRecord("pnd_Participant_Name", "#PARTICIPANT-NAME", FieldType.STRING, 35);
        pnd_Ctr_Access_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Access_Contract_Read", "#CTR-ACCESS-CONTRACT-READ", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Valid_Access_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Valid_Access_Contract_Read", "#CTR-VALID-ACCESS-CONTRACT-READ", 
            FieldType.NUMERIC, 7);
        pnd_Ctr_Da_Contract_Not_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Contract_Not_In_Cor", "#CTR-DA-CONTRACT-NOT-IN-COR", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Da_Inactive_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Inactive_In_Cor", "#CTR-DA-INACTIVE-IN-COR", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_Deceased = localVariables.newFieldInRecord("pnd_Ctr_Ph_Deceased", "#CTR-PH-DECEASED", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_With_Mail_Suppression = localVariables.newFieldInRecord("pnd_Ctr_Ph_With_Mail_Suppression", "#CTR-PH-WITH-MAIL-SUPPRESSION", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Contract_Accepted = localVariables.newFieldInRecord("pnd_Ctr_Contract_Accepted", "#CTR-CONTRACT-ACCEPTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Rejected = localVariables.newFieldInRecord("pnd_Ctr_Contract_Rejected", "#CTR-CONTRACT-REJECTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Contract_Read", "#CTR-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Duplicate_Contract = localVariables.newFieldInRecord("pnd_Ctr_Duplicate_Contract", "#CTR-DUPLICATE-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Terminated_Contract = localVariables.newFieldInRecord("pnd_Ctr_Terminated_Contract", "#CTR-TERMINATED-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Inactive = localVariables.newFieldInRecord("pnd_Ctr_Contract_Inactive", "#CTR-CONTRACT-INACTIVE", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Excluded = localVariables.newFieldInRecord("pnd_Ctr_Contract_Excluded", "#CTR-CONTRACT-EXCLUDED", FieldType.NUMERIC, 7);
        pnd_Ctr_Out_Of_State = localVariables.newFieldInRecord("pnd_Ctr_Out_Of_State", "#CTR-OUT-OF-STATE", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Outof_Range = localVariables.newFieldInRecord("pnd_Ctr_Contract_Outof_Range", "#CTR-CONTRACT-OUTOF-RANGE", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Plan_Not_For_Proc = localVariables.newFieldInRecord("pnd_Ctr_Plan_Not_For_Proc", "#CTR-PLAN-NOT-FOR-PROC", FieldType.NUMERIC, 7);
        pnd_Ctr_State_Inst_Not_For_Proc = localVariables.newFieldInRecord("pnd_Ctr_State_Inst_Not_For_Proc", "#CTR-STATE-INST-NOT-FOR-PROC", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Institution_Owned = localVariables.newFieldInRecord("pnd_Ctr_Institution_Owned", "#CTR-INSTITUTION-OWNED", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Contracts = localVariables.newFieldInRecord("pnd_Ctr_Total_Contracts", "#CTR-TOTAL-CONTRACTS", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_St = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_St", "#CTR-TOTAL-RA-ST", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_Rp = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Rp", "#CTR-TOTAL-RA-RP", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_Sv = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Sv", "#CTR-TOTAL-RA-SV", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Sr_St = localVariables.newFieldInRecord("pnd_Ctr_Total_Sr_St", "#CTR-TOTAL-SR-ST", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Sr_Rp = localVariables.newFieldInRecord("pnd_Ctr_Total_Sr_Rp", "#CTR-TOTAL-SR-RP", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Sr_Sv = localVariables.newFieldInRecord("pnd_Ctr_Total_Sr_Sv", "#CTR-TOTAL-SR-SV", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gr_St = localVariables.newFieldInRecord("pnd_Ctr_Total_Gr_St", "#CTR-TOTAL-GR-ST", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gr_Sv = localVariables.newFieldInRecord("pnd_Ctr_Total_Gr_Sv", "#CTR-TOTAL-GR-SV", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gs_St = localVariables.newFieldInRecord("pnd_Ctr_Total_Gs_St", "#CTR-TOTAL-GS-ST", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gs_Sv = localVariables.newFieldInRecord("pnd_Ctr_Total_Gs_Sv", "#CTR-TOTAL-GS-SV", FieldType.NUMERIC, 7);
        pnd_Ctr_Exclude_Ga = localVariables.newFieldInRecord("pnd_Ctr_Exclude_Ga", "#CTR-EXCLUDE-GA", FieldType.NUMERIC, 7);
        pnd_Ctr_Exclude_Ok = localVariables.newFieldInRecord("pnd_Ctr_Exclude_Ok", "#CTR-EXCLUDE-OK", FieldType.NUMERIC, 7);
        pnd_Ctr_Exclude_Ms = localVariables.newFieldInRecord("pnd_Ctr_Exclude_Ms", "#CTR-EXCLUDE-MS", FieldType.NUMERIC, 7);
        pnd_State_Unknown = localVariables.newFieldInRecord("pnd_State_Unknown", "#STATE-UNKNOWN", FieldType.NUMERIC, 4);

        pnd_State_Unknown__R_Field_5 = localVariables.newGroupInRecord("pnd_State_Unknown__R_Field_5", "REDEFINE", pnd_State_Unknown);
        pnd_State_Unknown_Pnd_State_Unknown_A = pnd_State_Unknown__R_Field_5.newFieldInGroup("pnd_State_Unknown_Pnd_State_Unknown_A", "#STATE-UNKNOWN-A", 
            FieldType.STRING, 1);
        pnd_State_Unknown_Pnd_State_Unknown_B = pnd_State_Unknown__R_Field_5.newFieldInGroup("pnd_State_Unknown_Pnd_State_Unknown_B", "#STATE-UNKNOWN-B", 
            FieldType.STRING, 3);
        pnd_Break = localVariables.newFieldInRecord("pnd_Break", "#BREAK", FieldType.BOOLEAN, 1);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);

        pnd_Arrays = localVariables.newGroupArrayInRecord("pnd_Arrays", "#ARRAYS", new DbsArrayController(1, 300));
        pnd_Arrays_Pnd_Combo_Prod = pnd_Arrays.newFieldInGroup("pnd_Arrays_Pnd_Combo_Prod", "#COMBO-PROD", FieldType.STRING, 12);
        pnd_Arrays_Pnd_Combo_Ctr = pnd_Arrays.newFieldInGroup("pnd_Arrays_Pnd_Combo_Ctr", "#COMBO-CTR", FieldType.NUMERIC, 7);
        pnd_Combo = localVariables.newFieldInRecord("pnd_Combo", "#COMBO", FieldType.STRING, 12);
        pnd_Type_Ra = localVariables.newFieldInRecord("pnd_Type_Ra", "#TYPE-RA", FieldType.STRING, 2);
        pnd_Type_Sra = localVariables.newFieldInRecord("pnd_Type_Sra", "#TYPE-SRA", FieldType.STRING, 2);
        pnd_Type_Gra = localVariables.newFieldInRecord("pnd_Type_Gra", "#TYPE-GRA", FieldType.STRING, 2);
        pnd_Type_Gsra = localVariables.newFieldInRecord("pnd_Type_Gsra", "#TYPE-GSRA", FieldType.STRING, 2);

        pnd_State_Array = localVariables.newGroupArrayInRecord("pnd_State_Array", "#STATE-ARRAY", new DbsArrayController(1, 52));
        pnd_State_Array_Pnd_State_Cde_N = pnd_State_Array.newFieldInGroup("pnd_State_Array_Pnd_State_Cde_N", "#STATE-CDE-N", FieldType.STRING, 3);
        pnd_State_Array_Pnd_State_Cde_A = pnd_State_Array.newFieldInGroup("pnd_State_Array_Pnd_State_Cde_A", "#STATE-CDE-A", FieldType.STRING, 2);
        pnd_State_Array_Pnd_State_Ra = pnd_State_Array.newFieldInGroup("pnd_State_Array_Pnd_State_Ra", "#STATE-RA", FieldType.STRING, 2);
        pnd_State_Array_Pnd_State_Sra = pnd_State_Array.newFieldInGroup("pnd_State_Array_Pnd_State_Sra", "#STATE-SRA", FieldType.STRING, 2);
        pnd_State_Array_Pnd_State_Gra = pnd_State_Array.newFieldInGroup("pnd_State_Array_Pnd_State_Gra", "#STATE-GRA", FieldType.STRING, 2);
        pnd_State_Array_Pnd_State_Gsra = pnd_State_Array.newFieldInGroup("pnd_State_Array_Pnd_State_Gsra", "#STATE-GSRA", FieldType.STRING, 2);
        pnd_Ctr_Ra_Sv_Nj = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Nj", "#CTR-RA-SV-NJ", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Ct = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Ct", "#CTR-RA-SV-CT", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Fl = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Fl", "#CTR-RA-SV-FL", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Mo = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Mo", "#CTR-RA-SV-MO", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Mt = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Mt", "#CTR-RA-SV-MT", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Ne = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Ne", "#CTR-RA-SV-NE", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Nh = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Nh", "#CTR-RA-SV-NH", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Ok = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Ok", "#CTR-RA-SV-OK", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Or = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Or", "#CTR-RA-SV-OR", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Pa = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Pa", "#CTR-RA-SV-PA", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Pr = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Pr", "#CTR-RA-SV-PR", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Sc = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Sc", "#CTR-RA-SV-SC", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Tx = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Tx", "#CTR-RA-SV-TX", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Ut = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Ut", "#CTR-RA-SV-UT", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Va = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Va", "#CTR-RA-SV-VA", FieldType.NUMERIC, 5);
        pnd_Ctr_Ra_Sv_Wi = localVariables.newFieldInRecord("pnd_Ctr_Ra_Sv_Wi", "#CTR-RA-SV-WI", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Nj = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Nj", "#CTR-SRA-SV-NJ", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Ct = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Ct", "#CTR-SRA-SV-CT", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Fl = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Fl", "#CTR-SRA-SV-FL", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Mo = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Mo", "#CTR-SRA-SV-MO", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Mt = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Mt", "#CTR-SRA-SV-MT", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Ne = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Ne", "#CTR-SRA-SV-NE", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Nh = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Nh", "#CTR-SRA-SV-NH", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Ok = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Ok", "#CTR-SRA-SV-OK", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Or = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Or", "#CTR-SRA-SV-OR", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Pa = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Pa", "#CTR-SRA-SV-PA", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Sc = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Sc", "#CTR-SRA-SV-SC", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Tx = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Tx", "#CTR-SRA-SV-TX", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Va = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Va", "#CTR-SRA-SV-VA", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Wi = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Wi", "#CTR-SRA-SV-WI", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Vt = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Vt", "#CTR-SRA-SV-VT", FieldType.NUMERIC, 5);
        pnd_Ctr_Sra_Sv_Ut = localVariables.newFieldInRecord("pnd_Ctr_Sra_Sv_Ut", "#CTR-SRA-SV-UT", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Nj = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Nj", "#CTR-GRA-SV-NJ", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Njabp = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Njabp", "#CTR-GRA-SV-NJABP", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Ct = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Ct", "#CTR-GRA-SV-CT", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Fl = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Fl", "#CTR-GRA-SV-FL", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Mo = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Mo", "#CTR-GRA-SV-MO", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Mt = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Mt", "#CTR-GRA-SV-MT", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Ne = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Ne", "#CTR-GRA-SV-NE", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Nh = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Nh", "#CTR-GRA-SV-NH", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Ok = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Ok", "#CTR-GRA-SV-OK", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Or = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Or", "#CTR-GRA-SV-OR", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Pa = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Pa", "#CTR-GRA-SV-PA", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Pr = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Pr", "#CTR-GRA-SV-PR", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Sc = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Sc", "#CTR-GRA-SV-SC", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Tx = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Tx", "#CTR-GRA-SV-TX", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Va = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Va", "#CTR-GRA-SV-VA", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Wi = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Wi", "#CTR-GRA-SV-WI", FieldType.NUMERIC, 5);
        pnd_Ctr_Gra_Sv_Nc = localVariables.newFieldInRecord("pnd_Ctr_Gra_Sv_Nc", "#CTR-GRA-SV-NC", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Nj = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Nj", "#CTR-GSRA-SV-NJ", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Suny = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Suny", "#CTR-GSRA-SV-SUNY", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Ct = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Ct", "#CTR-GSRA-SV-CT", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Fl = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Fl", "#CTR-GSRA-SV-FL", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Mo = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Mo", "#CTR-GSRA-SV-MO", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Mt = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Mt", "#CTR-GSRA-SV-MT", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Ne = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Ne", "#CTR-GSRA-SV-NE", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Nh = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Nh", "#CTR-GSRA-SV-NH", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Ok = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Ok", "#CTR-GSRA-SV-OK", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Or = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Or", "#CTR-GSRA-SV-OR", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Pa = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Pa", "#CTR-GSRA-SV-PA", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Sc = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Sc", "#CTR-GSRA-SV-SC", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Tx = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Tx", "#CTR-GSRA-SV-TX", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Va = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Va", "#CTR-GSRA-SV-VA", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Wi = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Wi", "#CTR-GSRA-SV-WI", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Mi = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Mi", "#CTR-GSRA-SV-MI", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Ar = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Ar", "#CTR-GSRA-SV-AR", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Ky = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Ky", "#CTR-GSRA-SV-KY", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Tn = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Tn", "#CTR-GSRA-SV-TN", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Vt = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Vt", "#CTR-GSRA-SV-VT", FieldType.NUMERIC, 5);
        pnd_Ctr_Gsra_Sv_Hi = localVariables.newFieldInRecord("pnd_Ctr_Gsra_Sv_Hi", "#CTR-GSRA-SV-HI", FieldType.NUMERIC, 5);
        pnd_Residence_State_Cde = localVariables.newFieldInRecord("pnd_Residence_State_Cde", "#RESIDENCE-STATE-CDE", FieldType.STRING, 3);
        pnd_Pst_Tbl_Data_Field1 = localVariables.newFieldInRecord("pnd_Pst_Tbl_Data_Field1", "#PST-TBL-DATA-FIELD1", FieldType.STRING, 253);

        pnd_Pst_Tbl_Data_Field1__R_Field_6 = localVariables.newGroupInRecord("pnd_Pst_Tbl_Data_Field1__R_Field_6", "REDEFINE", pnd_Pst_Tbl_Data_Field1);
        pnd_Pst_Tbl_Data_Field1_Pnd_State = pnd_Pst_Tbl_Data_Field1__R_Field_6.newFieldArrayInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_State", "#STATE", FieldType.STRING, 
            3, new DbsArrayController(1, 3));
        pnd_Pst_Tbl_Data_Field1_Pnd_Filler = pnd_Pst_Tbl_Data_Field1__R_Field_6.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Filler", "#FILLER", FieldType.STRING, 
            244);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Valid_Record.setInitialValue(false);
        pnd_Accepted.setInitialValue(false);
        pnd_Excluded.setInitialValue(false);
        pnd_With_State_Exclusion.setInitialValue(false);
        pnd_K.setInitialValue(0);
        pnd_State_Array_Pnd_State_Cde_N.getValue(1).setInitialValue("001");
        pnd_State_Array_Pnd_State_Cde_N.getValue(2).setInitialValue("002");
        pnd_State_Array_Pnd_State_Cde_N.getValue(3).setInitialValue("003");
        pnd_State_Array_Pnd_State_Cde_N.getValue(4).setInitialValue("004");
        pnd_State_Array_Pnd_State_Cde_N.getValue(5).setInitialValue("005");
        pnd_State_Array_Pnd_State_Cde_N.getValue(6).setInitialValue("007");
        pnd_State_Array_Pnd_State_Cde_N.getValue(7).setInitialValue("008");
        pnd_State_Array_Pnd_State_Cde_N.getValue(8).setInitialValue("009");
        pnd_State_Array_Pnd_State_Cde_N.getValue(9).setInitialValue("010");
        pnd_State_Array_Pnd_State_Cde_N.getValue(10).setInitialValue("011");
        pnd_State_Array_Pnd_State_Cde_N.getValue(11).setInitialValue("012");
        pnd_State_Array_Pnd_State_Cde_N.getValue(12).setInitialValue("014");
        pnd_State_Array_Pnd_State_Cde_N.getValue(13).setInitialValue("015");
        pnd_State_Array_Pnd_State_Cde_N.getValue(14).setInitialValue("016");
        pnd_State_Array_Pnd_State_Cde_N.getValue(15).setInitialValue("017");
        pnd_State_Array_Pnd_State_Cde_N.getValue(16).setInitialValue("018");
        pnd_State_Array_Pnd_State_Cde_N.getValue(17).setInitialValue("019");
        pnd_State_Array_Pnd_State_Cde_N.getValue(18).setInitialValue("020");
        pnd_State_Array_Pnd_State_Cde_N.getValue(19).setInitialValue("021");
        pnd_State_Array_Pnd_State_Cde_N.getValue(20).setInitialValue("022");
        pnd_State_Array_Pnd_State_Cde_N.getValue(21).setInitialValue("023");
        pnd_State_Array_Pnd_State_Cde_N.getValue(22).setInitialValue("024");
        pnd_State_Array_Pnd_State_Cde_N.getValue(23).setInitialValue("025");
        pnd_State_Array_Pnd_State_Cde_N.getValue(24).setInitialValue("026");
        pnd_State_Array_Pnd_State_Cde_N.getValue(25).setInitialValue("027");
        pnd_State_Array_Pnd_State_Cde_N.getValue(26).setInitialValue("028");
        pnd_State_Array_Pnd_State_Cde_N.getValue(27).setInitialValue("029");
        pnd_State_Array_Pnd_State_Cde_N.getValue(28).setInitialValue("030");
        pnd_State_Array_Pnd_State_Cde_N.getValue(29).setInitialValue("031");
        pnd_State_Array_Pnd_State_Cde_N.getValue(30).setInitialValue("032");
        pnd_State_Array_Pnd_State_Cde_N.getValue(31).setInitialValue("033");
        pnd_State_Array_Pnd_State_Cde_N.getValue(32).setInitialValue("034");
        pnd_State_Array_Pnd_State_Cde_N.getValue(33).setInitialValue("035");
        pnd_State_Array_Pnd_State_Cde_N.getValue(34).setInitialValue("036");
        pnd_State_Array_Pnd_State_Cde_N.getValue(35).setInitialValue("037");
        pnd_State_Array_Pnd_State_Cde_N.getValue(36).setInitialValue("038");
        pnd_State_Array_Pnd_State_Cde_N.getValue(37).setInitialValue("039");
        pnd_State_Array_Pnd_State_Cde_N.getValue(38).setInitialValue("040");
        pnd_State_Array_Pnd_State_Cde_N.getValue(39).setInitialValue("041");
        pnd_State_Array_Pnd_State_Cde_N.getValue(40).setInitialValue("043");
        pnd_State_Array_Pnd_State_Cde_N.getValue(41).setInitialValue("045");
        pnd_State_Array_Pnd_State_Cde_N.getValue(42).setInitialValue("046");
        pnd_State_Array_Pnd_State_Cde_N.getValue(43).setInitialValue("047");
        pnd_State_Array_Pnd_State_Cde_N.getValue(44).setInitialValue("048");
        pnd_State_Array_Pnd_State_Cde_N.getValue(45).setInitialValue("049");
        pnd_State_Array_Pnd_State_Cde_N.getValue(46).setInitialValue("050");
        pnd_State_Array_Pnd_State_Cde_N.getValue(47).setInitialValue("051");
        pnd_State_Array_Pnd_State_Cde_N.getValue(48).setInitialValue("054");
        pnd_State_Array_Pnd_State_Cde_N.getValue(49).setInitialValue("055");
        pnd_State_Array_Pnd_State_Cde_N.getValue(50).setInitialValue("056");
        pnd_State_Array_Pnd_State_Cde_N.getValue(51).setInitialValue("057");
        pnd_State_Array_Pnd_State_Cde_N.getValue(52).setInitialValue("042");
        pnd_State_Array_Pnd_State_Cde_A.getValue(1).setInitialValue("AL");
        pnd_State_Array_Pnd_State_Cde_A.getValue(2).setInitialValue("AK");
        pnd_State_Array_Pnd_State_Cde_A.getValue(3).setInitialValue("AZ");
        pnd_State_Array_Pnd_State_Cde_A.getValue(4).setInitialValue("AR");
        pnd_State_Array_Pnd_State_Cde_A.getValue(5).setInitialValue("CA");
        pnd_State_Array_Pnd_State_Cde_A.getValue(6).setInitialValue("CO");
        pnd_State_Array_Pnd_State_Cde_A.getValue(7).setInitialValue("CT");
        pnd_State_Array_Pnd_State_Cde_A.getValue(8).setInitialValue("DE");
        pnd_State_Array_Pnd_State_Cde_A.getValue(9).setInitialValue("DC");
        pnd_State_Array_Pnd_State_Cde_A.getValue(10).setInitialValue("FL");
        pnd_State_Array_Pnd_State_Cde_A.getValue(11).setInitialValue("GA");
        pnd_State_Array_Pnd_State_Cde_A.getValue(12).setInitialValue("HI");
        pnd_State_Array_Pnd_State_Cde_A.getValue(13).setInitialValue("ID");
        pnd_State_Array_Pnd_State_Cde_A.getValue(14).setInitialValue("IL");
        pnd_State_Array_Pnd_State_Cde_A.getValue(15).setInitialValue("IN");
        pnd_State_Array_Pnd_State_Cde_A.getValue(16).setInitialValue("IA");
        pnd_State_Array_Pnd_State_Cde_A.getValue(17).setInitialValue("KS");
        pnd_State_Array_Pnd_State_Cde_A.getValue(18).setInitialValue("KY");
        pnd_State_Array_Pnd_State_Cde_A.getValue(19).setInitialValue("LA");
        pnd_State_Array_Pnd_State_Cde_A.getValue(20).setInitialValue("ME");
        pnd_State_Array_Pnd_State_Cde_A.getValue(21).setInitialValue("MD");
        pnd_State_Array_Pnd_State_Cde_A.getValue(22).setInitialValue("MA");
        pnd_State_Array_Pnd_State_Cde_A.getValue(23).setInitialValue("MI");
        pnd_State_Array_Pnd_State_Cde_A.getValue(24).setInitialValue("MN");
        pnd_State_Array_Pnd_State_Cde_A.getValue(25).setInitialValue("MS");
        pnd_State_Array_Pnd_State_Cde_A.getValue(26).setInitialValue("MO");
        pnd_State_Array_Pnd_State_Cde_A.getValue(27).setInitialValue("MT");
        pnd_State_Array_Pnd_State_Cde_A.getValue(28).setInitialValue("NE");
        pnd_State_Array_Pnd_State_Cde_A.getValue(29).setInitialValue("NV");
        pnd_State_Array_Pnd_State_Cde_A.getValue(30).setInitialValue("NH");
        pnd_State_Array_Pnd_State_Cde_A.getValue(31).setInitialValue("NJ");
        pnd_State_Array_Pnd_State_Cde_A.getValue(32).setInitialValue("NM");
        pnd_State_Array_Pnd_State_Cde_A.getValue(33).setInitialValue("NY");
        pnd_State_Array_Pnd_State_Cde_A.getValue(34).setInitialValue("NC");
        pnd_State_Array_Pnd_State_Cde_A.getValue(35).setInitialValue("ND");
        pnd_State_Array_Pnd_State_Cde_A.getValue(36).setInitialValue("OH");
        pnd_State_Array_Pnd_State_Cde_A.getValue(37).setInitialValue("OK");
        pnd_State_Array_Pnd_State_Cde_A.getValue(38).setInitialValue("OR");
        pnd_State_Array_Pnd_State_Cde_A.getValue(39).setInitialValue("PA");
        pnd_State_Array_Pnd_State_Cde_A.getValue(40).setInitialValue("RI");
        pnd_State_Array_Pnd_State_Cde_A.getValue(41).setInitialValue("SC");
        pnd_State_Array_Pnd_State_Cde_A.getValue(42).setInitialValue("SD");
        pnd_State_Array_Pnd_State_Cde_A.getValue(43).setInitialValue("TN");
        pnd_State_Array_Pnd_State_Cde_A.getValue(44).setInitialValue("TX");
        pnd_State_Array_Pnd_State_Cde_A.getValue(45).setInitialValue("UT");
        pnd_State_Array_Pnd_State_Cde_A.getValue(46).setInitialValue("VT");
        pnd_State_Array_Pnd_State_Cde_A.getValue(47).setInitialValue("VA");
        pnd_State_Array_Pnd_State_Cde_A.getValue(48).setInitialValue("WA");
        pnd_State_Array_Pnd_State_Cde_A.getValue(49).setInitialValue("WV");
        pnd_State_Array_Pnd_State_Cde_A.getValue(50).setInitialValue("WI");
        pnd_State_Array_Pnd_State_Cde_A.getValue(51).setInitialValue("WY");
        pnd_State_Array_Pnd_State_Cde_A.getValue(52).setInitialValue("PR");
        pnd_State_Array_Pnd_State_Ra.getValue(1).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(2).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(3).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(4).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(5).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(6).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(7).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(8).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(9).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(10).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(11).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Ra.getValue(12).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Ra.getValue(13).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Ra.getValue(14).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(15).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(16).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(17).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(18).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(19).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Ra.getValue(20).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(21).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(22).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Ra.getValue(23).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Ra.getValue(24).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(25).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(26).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(27).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(28).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(29).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(30).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(31).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(32).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(33).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(34).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Ra.getValue(35).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(36).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(37).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(38).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(39).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(40).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(41).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(42).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(43).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(44).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(45).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(46).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(47).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(48).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Ra.getValue(49).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Ra.getValue(50).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Ra.getValue(51).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Ra.getValue(52).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(1).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(2).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(3).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(4).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(5).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(6).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(7).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(8).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(9).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(10).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(11).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Sra.getValue(12).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Sra.getValue(13).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Sra.getValue(14).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(15).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(16).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(17).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(18).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(19).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Sra.getValue(20).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(21).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(22).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Sra.getValue(23).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Sra.getValue(24).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(25).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(26).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(27).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(28).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(29).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(30).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(31).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(32).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(33).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(34).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Sra.getValue(35).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(36).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(37).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(38).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(39).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(40).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(41).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(42).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(43).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(44).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(45).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(46).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(47).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(48).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Sra.getValue(49).setInitialValue("RP");
        pnd_State_Array_Pnd_State_Sra.getValue(50).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Sra.getValue(51).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Sra.getValue(52).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(1).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(2).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(3).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(4).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(5).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(6).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(7).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(8).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(9).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(10).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(11).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(12).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(13).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(14).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(15).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(16).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(17).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(18).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(19).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(20).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(21).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(22).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(23).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(24).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(25).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(26).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(27).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(28).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(29).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(30).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(31).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(32).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(33).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(34).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(35).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(36).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(37).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(38).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(39).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(40).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(41).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(42).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(43).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(44).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(45).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(46).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(47).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(48).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(49).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(50).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gra.getValue(51).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gra.getValue(52).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(1).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(2).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(3).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(4).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(5).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(6).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(7).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(8).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(9).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(10).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(11).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(12).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(13).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(14).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(15).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(16).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(17).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(18).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(19).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(20).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(21).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(22).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(23).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(24).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(25).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(26).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(27).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(28).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(29).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(30).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(31).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(32).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(33).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(34).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(35).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(36).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(37).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(38).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(39).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(40).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(41).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(42).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(43).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(44).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(45).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(46).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(47).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(48).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(49).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(50).setInitialValue("SV");
        pnd_State_Array_Pnd_State_Gsra.getValue(51).setInitialValue("ST");
        pnd_State_Array_Pnd_State_Gsra.getValue(52).setInitialValue("ST");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridp410() throws Exception
    {
        super("Ridp410");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  STATISTICS REPORT
        //*  EXCLUSION REPORT                                                                                                                                             //Natural: FORMAT ( 1 ) LS = 120 PS = 60
        if (condition(getReports().getAstLinesLeft(1).less(5)))                                                                                                           //Natural: FORMAT ( 2 ) LS = 120 PS = 60;//Natural: FORMAT LS = 80 PS = 60;//Natural: NEWPAGE ( 1 ) IF LESS THAN 5 LINES
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        if (condition(getReports().getAstLinesLeft(2).less(5)))                                                                                                           //Natural: NEWPAGE ( 2 ) IF LESS THAN 5 LINES
        {
            getReports().newPage(2);
            if (condition(Global.isEscape())){return;}
        }
        //*  OPEN MQ QUEUE
        //*  WRITE ' FETCHING MDMP011'                         /* JCA20160719
        //*  JCA20160719
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*  PERFORM GET-STATES-FOR-EXCLUSION
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 37T '2006 TIAA ACCESS RIDER MAILING' 84T 'DATE:' *DATU / 43T 'EXCEPTIONS LIST' 84T 'TIME:' *TIME ( EM = XXXXXXXX ) / 84T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) // 2T 'PIN          TIAA NBR   STATE   REMARKS'/ 2T '------------------------------------------------------------'/
        //*  LS1
        //*  LS1
        //*  LS1
        //*  LS1
        //*  LS1
        //*  LS1
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT 'PROGRAM:' *PROGRAM 34T '2006 TIAA ACCESS RIDER MAILING' 84T 'DATE:' *DATU / 37T 'MA/OR EXCLUDED CONTRACTS' 84T 'TIME:' *TIME ( EM = XXXXXXXX ) / 84T 'PAGE:' 3X *PAGE-NUMBER ( 2 ) // 2T 'PIN         ' 16T 'TIAA #  ' 26T 'ISSUE-STATE' / 2T '------------' 16T '--------' 26T '-----------' //
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #INPUT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Ctr_Access_Contract_Read.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-ACCESS-CONTRACT-READ
            //*                                                                                                                                                           //Natural: AT BREAK OF #INPUT-RECORD.#PIN-A12
            //*                                                                                                                                                           //Natural: AT END OF DATA
            if (condition(pnd_Break.getBoolean()))                                                                                                                        //Natural: IF #BREAK
            {
                pnd_Break.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #BREAK
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Prev_Pin_N12.setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-N12 TO #PREV-PIN-N12
            pnd_Prev_Cover.setValue(pnd_Input_Record_Pnd_Cover_Letter_Ind);                                                                                               //Natural: MOVE #INPUT-RECORD.#COVER-LETTER-IND TO #PREV-COVER
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Inst_Name);                                                                                                  //Natural: MOVE #INPUT-RECORD.#INST-NAME TO #PREV-INST-NAME
            pnd_Prev_Level.setValue(pnd_Input_Record_Pnd_Level);                                                                                                          //Natural: MOVE #INPUT-RECORD.#LEVEL TO #PREV-LEVEL
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
        }                                                                                                                                                                 //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-STATISTICS-REPORT
        sub_Write_Statistics_Report();
        if (condition(Global.isEscape())) {return;}
        //*  CLOSE MQ QUEUE
        //*  JCA20160719
        getReports().write(0, "FETCHING MDMP0012");                                                                                                                       //Natural: WRITE 'FETCHING MDMP0012'
        if (Global.isEscape()) return;
        //*  JCA20160719
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* **********************************************************************
        //*                 S T A R T    O F    S U B R O U T I N E S
        //* **********************************************************************
        //*  ------------------------------------------
        //*  DEFINE SUBROUTINE GET-STATES-FOR-EXCLUSION
        //*  ------------------------------------------
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := '2006ACCESS'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD1 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*    IF #STATE(1) NE '   '
        //*      MOVE TRUE TO #WITH-STATE-EXCLUSION
        //*    END-IF
        //*  END-SUBROUTINE /* GET-STATES-FOR-EXCLUSION
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-RECORD
        //*  RA CONTRACTS
        //*  GRA CONTRACTS
        //*  SRA CONTRACTS
        //*  GSRA CONTRACTS
        //*  ---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COVER-LETTER-TYPE
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RESIDENCE-STATE-CODE
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RECORD-IN-COR
        //*  --------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PIN-STATUS
        //*  -----------------------------------
        //*  DEFINE SUBROUTINE CHECK-CPM-PROFILE
        //*  -----------------------------------
        //*    IF +TRACE
        //*      WRITE *PROGRAM 'CALLING CPMN550...
        //*    END-IF
        //*    #COMM-CPMN550.#COMM-CUSTOMER-PIN := #PREV-PIN
        //*    #COMM-CPMN550.#COMM-CUSTOMER-TYP := 'PH'
        //*    #COMM-CPMN550.#COMM-CUSTOMER-FK  := 'PPPPPPPPPPPPPPP'
        //*    #COMM-CPMN550.#COMM-DOC-ID       := 'D19'
        //*    CALLNAT 'CPMN550' #COMM-CPMN550
        //*    IF #COMM-CPMN550.#COMM-CUSTOMER-EMAIL NE ' '
        //*      #EML := TRUE
        //*      #OUTPUT.#EMAIL-ADDRESS  := #COMM-CPMN550.#COMM-CUSTOMER-EMAIL
        //*    ELSE
        //*      #EML := FALSE
        //*      RESET #OUTPUT.#EMAIL-ADDRESS
        //*    END-IF
        //*  END-SUBROUTINE /* CHECK-CPM-PROFILE
        //*  ---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-ARRAY
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA-TO-FILE
        //*  --------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-NUMBER-PAGES
        //*  --------------------------------------
        //*  -------------------------------------
        //*  DEFINE SUBROUTINE WRITE-DATA-TO-ARRAY
        //*  -------------------------------------
        //*    #CTR-TOTAL-CONTRACTS := #CTR-RA-ST + #CTR-RA-RP + #CTR-RA-SV +
        //*                            #CTR-SR-ST + #CTR-SR-RP + #CTR-SR-SV +
        //*                            #CTR-GR-ST + #CTR-GR-SV +
        //*                            #CTR-GS-ST + #CTR-GS-SV
        //*    WRITE '=' #CTR-TOTAL-CONTRACTS
        //*    IF #CTR-TOTAL-CONTRACTS GT 0
        //*      RESET #COMBO
        //*      COMPRESS #CTR-RA-ST #CTR-RA-RP #CTR-SR-ST #CTR-SR-RP
        //*               #CTR-GR-ST #CTR-GS-ST #CTR-RA-SV #CTR-SR-SV
        //*               #CTR-GR-SV #CTR-GS-SV
        //*          INTO #COMBO LEAVING NO SPACE
        //*      EXAMINE #COMBO-PROD(*) FOR #COMBO GIVING INDEX #I
        //*      IF #I EQ 0
        //*        ADD 1 TO #A
        //*        MOVE #A TO #I
        //*        MOVE #COMBO TO #COMBO-PROD(#I)
        //*        MOVE #PREV-PIN TO #COMBO-PIN(#I)
        //*      END-IF
        //*      ADD 1 TO #COMBO-CTR(#I)
        //*    END-IF
        //*  END-SUBROUTINE /* WRITE-DATA-TO-ARRAY
        //*  -----------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-STATISTICS-REPORT
        //*                              #CTR-OUT-OF-STATE              +
        //*                              #CTR-STATE-INST-NOT-FOR-PROC   +
        //*                              #CTR-INSTITUTION-OWNED         +
    }
    private void sub_Validate_Record() throws Exception                                                                                                                   //Natural: VALIDATE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        //*  DETERMINE RESIDENCE STATE
        if (condition(pnd_With_State_Exclusion.getBoolean()))                                                                                                             //Natural: IF #WITH-STATE-EXCLUSION
        {
                                                                                                                                                                          //Natural: PERFORM GET-RESIDENCE-STATE-CODE
            sub_Get_Residence_State_Code();
            if (condition(Global.isEscape())) {return;}
            //*  GEORGIA
            short decideConditionsMet1091 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #RESIDENCE-STATE-CDE;//Natural: VALUE '012'
            if (condition((pnd_Residence_State_Cde.equals("012"))))
            {
                decideConditionsMet1091++;
                pnd_Ctr_Exclude_Ga.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CTR-EXCLUDE-GA
                getWorkFiles().write(15, false, pnd_Input_Record);                                                                                                        //Natural: WRITE WORK FILE 15 #INPUT-RECORD
                //*  OKLAHOMA
                pnd_Excluded.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #EXCLUDED
            }                                                                                                                                                             //Natural: VALUE '039'
            else if (condition((pnd_Residence_State_Cde.equals("039"))))
            {
                decideConditionsMet1091++;
                pnd_Ctr_Exclude_Ok.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CTR-EXCLUDE-OK
                getWorkFiles().write(16, false, pnd_Input_Record);                                                                                                        //Natural: WRITE WORK FILE 16 #INPUT-RECORD
                //*  MISSISSIPPI
                pnd_Excluded.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #EXCLUDED
            }                                                                                                                                                             //Natural: VALUE '027'
            else if (condition((pnd_Residence_State_Cde.equals("027"))))
            {
                decideConditionsMet1091++;
                pnd_Ctr_Exclude_Ms.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CTR-EXCLUDE-MS
                getWorkFiles().write(17, false, pnd_Input_Record);                                                                                                        //Natural: WRITE WORK FILE 17 #INPUT-RECORD
                pnd_Excluded.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #EXCLUDED
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Excluded.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #EXCLUDED
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_Excluded.getBoolean()))                                                                                                                     //Natural: IF #EXCLUDED
            {
                pnd_Remarks.setValue("EXCLUDED RESIDENCE STATE");                                                                                                         //Natural: ASSIGN #REMARKS := 'EXCLUDED RESIDENCE STATE'
                getReports().write(1, pnd_Input_Record_Pnd_Pin_N12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-N12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Prev_Contract.equals(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr)))                                                                                    //Natural: IF #PREV-CONTRACT EQ #TIAA-CNTRCT-NBR
        {
            pnd_Ctr_Duplicate_Contract.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DUPLICATE-CONTRACT
            pnd_Remarks.setValue("DUPLICATE CONTRACT");                                                                                                                   //Natural: ASSIGN #REMARKS := 'DUPLICATE CONTRACT'
            getReports().write(1, pnd_Input_Record_Pnd_Pin_N12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-N12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prev_Contract.setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #PREV-CONTRACT
                                                                                                                                                                          //Natural: PERFORM CHECK-RECORD-IN-COR
        sub_Check_Record_In_Cor();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Valid_Record.getBoolean()))                                                                                                                     //Natural: IF #VALID-RECORD
        {
            DbsUtil.examine(new ExamineSource(pnd_State_Array_Pnd_State_Cde_N.getValue("*")), new ExamineSearch(pnd_Input_Record_Pnd_Issue_State_Cde),                    //Natural: EXAMINE #STATE-CDE-N ( * ) FOR #ISSUE-STATE-CDE GIVING INDEX #I
                new ExamineGivingIndex(pnd_I));
            if (condition(pnd_I.equals(getZero())))                                                                                                                       //Natural: IF #I EQ 0
            {
                                                                                                                                                                          //Natural: PERFORM GET-RESIDENCE-STATE-CODE
                sub_Get_Residence_State_Code();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.examine(new ExamineSource(pnd_State_Array_Pnd_State_Cde_N.getValue("*")), new ExamineSearch(pnd_Input_Record_Pnd_Issue_State_Cde),                //Natural: EXAMINE #STATE-CDE-N ( * ) FOR #ISSUE-STATE-CDE GIVING INDEX #I
                    new ExamineGivingIndex(pnd_I));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I GT 0
            {
                pnd_Type_Ra.setValue(pnd_State_Array_Pnd_State_Ra.getValue(pnd_I));                                                                                       //Natural: ASSIGN #TYPE-RA := #STATE-RA ( #I )
                pnd_Type_Sra.setValue(pnd_State_Array_Pnd_State_Sra.getValue(pnd_I));                                                                                     //Natural: ASSIGN #TYPE-SRA := #STATE-SRA ( #I )
                pnd_Type_Gra.setValue(pnd_State_Array_Pnd_State_Gra.getValue(pnd_I));                                                                                     //Natural: ASSIGN #TYPE-GRA := #STATE-GRA ( #I )
                pnd_Type_Gsra.setValue(pnd_State_Array_Pnd_State_Gsra.getValue(pnd_I));                                                                                   //Natural: ASSIGN #TYPE-GSRA := #STATE-GSRA ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Type_Ra.setValue("ST");                                                                                                                               //Natural: ASSIGN #TYPE-RA := 'ST'
                pnd_Type_Sra.setValue("ST");                                                                                                                              //Natural: ASSIGN #TYPE-SRA := 'ST'
                pnd_Type_Gra.setValue("ST");                                                                                                                              //Natural: ASSIGN #TYPE-GRA := 'ST'
                pnd_Type_Gsra.setValue("ST");                                                                                                                             //Natural: ASSIGN #TYPE-GSRA := 'ST'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida410.getPnd_Miscellaneous_Pnd_Suny().getBoolean() && pnd_Input_Record_Pnd_Issue_State_Cde.equals("035")))                                 //Natural: IF #SUNY AND #ISSUE-STATE-CDE EQ '035'
            {
                pnd_Type_Gsra.setValue("SV");                                                                                                                             //Natural: ASSIGN #TYPE-GSRA := 'SV'
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1149 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TIAA-CNTRCT-NBR EQ 'A0000010' THRU 'A9899999' OR #TIAA-CNTRCT-NBR EQ 'B0000010' THRU 'B9999999' OR #TIAA-CNTRCT-NBR EQ 'C0100000' THRU 'C7999999' OR #TIAA-CNTRCT-NBR EQ 'C9000000' THRU 'C9999999' OR #TIAA-CNTRCT-NBR EQ 'D0100000' THRU 'D9999999' OR #TIAA-CNTRCT-NBR EQ 'E0100000' THRU 'E9999999'
            if (condition(((((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("A0000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("A9899999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("B0000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("B9999999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C0100000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C7999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C9000000") 
                && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C9999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("D0100000") && 
                pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("D9999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("E0100000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("E9999999")))))
            {
                decideConditionsMet1149++;
                short decideConditionsMet1152 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #TYPE-RA;//Natural: VALUE 'ST'
                if (condition((pnd_Type_Ra.equals("ST"))))
                {
                    decideConditionsMet1152++;
                    pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_St().setValue(1);                                                                                                   //Natural: MOVE 1 TO #CTRS.#CTR-RA-ST
                    pnd_Ctr_Total_Ra_St.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-RA-ST
                }                                                                                                                                                         //Natural: VALUE 'RP'
                else if (condition((pnd_Type_Ra.equals("RP"))))
                {
                    decideConditionsMet1152++;
                    pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Rp().setValue(1);                                                                                                   //Natural: MOVE 1 TO #CTRS.#CTR-RA-RP
                    pnd_Ctr_Total_Ra_Rp.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-RA-RP
                }                                                                                                                                                         //Natural: VALUE 'SV'
                else if (condition((pnd_Type_Ra.equals("SV"))))
                {
                    decideConditionsMet1152++;
                    short decideConditionsMet1160 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ISSUE-STATE-CDE EQ '033'
                    if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("033")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Nj().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-NJ
                        pnd_Ctr_Ra_Sv_Nj.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-NJ
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '041'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("041")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Pa().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-PA
                        pnd_Ctr_Ra_Sv_Pa.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-PA
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '008'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("008")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ct().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-CT
                        pnd_Ctr_Ra_Sv_Ct.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-CT
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '048'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("048")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Tx().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-TX
                        pnd_Ctr_Ra_Sv_Tx.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-TX
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '051'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("051")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Va().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-VA
                        pnd_Ctr_Ra_Sv_Va.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-VA
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '028'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("028")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Mo().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-MO
                        pnd_Ctr_Ra_Sv_Mo.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-MO
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '011'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("011")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Fl().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-FL
                        pnd_Ctr_Ra_Sv_Fl.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-FL
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '039'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("039")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ok().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-OK
                        pnd_Ctr_Ra_Sv_Ok.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-OK
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '040'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("040")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Or().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-OR
                        pnd_Ctr_Ra_Sv_Or.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-OR
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '056'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("056")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Wi().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-WI
                        pnd_Ctr_Ra_Sv_Wi.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-WI
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '045'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("045")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Sc().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-SC
                        pnd_Ctr_Ra_Sv_Sc.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-SC
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '030'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("030")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ne().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-NE
                        pnd_Ctr_Ra_Sv_Ne.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-NE
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '029'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("029")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Mt().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-MT
                        pnd_Ctr_Ra_Sv_Mt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-MT
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '049'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("049")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ut().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-UT
                        //*  LS1
                        pnd_Ctr_Ra_Sv_Ut.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-UT
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '032'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("032")))
                    {
                        decideConditionsMet1160++;
                        //*  LS1
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Nh().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-NH
                        //*  LS1
                        pnd_Ctr_Ra_Sv_Nh.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-NH
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '042'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("042")))
                    {
                        decideConditionsMet1160++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Pr().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-PR
                        pnd_Ctr_Ra_Sv_Pr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-RA-SV-PR
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                        //*                  MOVE ADD 1 TO #CTR-RA-SV
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Ctr_Total_Ra_Sv.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-RA-SV
                    pdaRida410.getPnd_Miscellaneous_Pnd_State_Ra_Sv().getValue(pnd_I).setValue(1);                                                                        //Natural: MOVE 1 TO #STATE-RA-SV ( #I )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                //*  060107 J.AVE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #TIAA-CNTRCT-NBR EQ '20000010' THRU '20099999' OR #TIAA-CNTRCT-NBR EQ '20250000' THRU '28999999' OR #TIAA-CNTRCT-NBR EQ '30000100' THRU '39999999'
            else if (condition((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("20099999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20250000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("28999999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("30000100") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("39999999")))))
            {
                decideConditionsMet1149++;
                short decideConditionsMet1227 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #TYPE-GRA;//Natural: VALUE 'ST'
                if (condition((pnd_Type_Gra.equals("ST"))))
                {
                    decideConditionsMet1227++;
                    pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_St().setValue(1);                                                                                                   //Natural: MOVE 1 TO #CTRS.#CTR-GR-ST
                    pnd_Ctr_Total_Gr_St.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-GR-ST
                }                                                                                                                                                         //Natural: VALUE 'SV'
                else if (condition((pnd_Type_Gra.equals("SV"))))
                {
                    decideConditionsMet1227++;
                    short decideConditionsMet1232 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ISSUE-STATE-CDE EQ '033' AND #STATE-INST NE 'NJABP'
                    if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("033") && pnd_Input_Record_Pnd_State_Inst.notEquals("NJABP")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nj().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-NJ
                        pnd_Ctr_Gra_Sv_Nj.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-NJ
                        pdaRida410.getPnd_Miscellaneous_Pnd_Njabp().setValue(true);                                                                                       //Natural: MOVE TRUE TO #NJABP
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '033' AND #STATE-INST EQ 'NJABP'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("033") && pnd_Input_Record_Pnd_State_Inst.equals("NJABP")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Njabp().setValue(1);                                                                                            //Natural: MOVE 1 TO #CTRS.#CTR-GR-NJABP
                        pnd_Ctr_Gra_Sv_Njabp.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CTR-GRA-SV-NJABP
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '041'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("041")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Pa().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-PA
                        pnd_Ctr_Gra_Sv_Pa.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-PA
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '008'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("008")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ct().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-CT
                        pnd_Ctr_Gra_Sv_Ct.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-CT
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '040'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("040")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Or().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-OR
                        pnd_Ctr_Gra_Sv_Or.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-OR
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '051'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("051")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Va().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-VA
                        pnd_Ctr_Gra_Sv_Va.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-VA
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '011'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("011")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Fl().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-FL
                        pnd_Ctr_Gra_Sv_Fl.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-FL
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '045'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("045")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Sc().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-SC
                        pnd_Ctr_Gra_Sv_Sc.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-SC
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '028'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("028")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Mo().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-MO
                        pnd_Ctr_Gra_Sv_Mo.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-MO
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '048'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("048")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Tx().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-TX
                        pnd_Ctr_Gra_Sv_Tx.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-TX
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '036'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("036")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nc().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-NC
                        pnd_Ctr_Gra_Sv_Nc.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-NC
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '039'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("039")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ok().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-OK
                        pnd_Ctr_Gra_Sv_Ok.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-OK
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '030'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("030")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ne().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-NE
                        pnd_Ctr_Gra_Sv_Ne.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-NE
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '029'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("029")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Mt().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-MT
                        //*  LS1
                        pnd_Ctr_Gra_Sv_Mt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-MT
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '032'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("032")))
                    {
                        decideConditionsMet1232++;
                        //*  LS1
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nh().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-NH
                        //*  LS1
                        pnd_Ctr_Gra_Sv_Nh.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-NH
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '042'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("042")))
                    {
                        decideConditionsMet1232++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Pr().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-PR
                        //*  LS1
                        pnd_Ctr_Gra_Sv_Pr.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-PR
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '056'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("056")))
                    {
                        decideConditionsMet1232++;
                        //*  LS1
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Wi().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GR-WI
                        //*  LS1
                        pnd_Ctr_Gra_Sv_Wi.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-GRA-SV-WI
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                        //*                  MOVE 1 TO #CTR-GR-SV
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Ctr_Total_Gr_Sv.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-GR-SV
                    pdaRida410.getPnd_Miscellaneous_Pnd_State_Gra_Sv().getValue(pnd_I).setValue(1);                                                                       //Natural: MOVE 1 TO #STATE-GRA-SV ( #I )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                //*  060107 J.AVE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #TIAA-CNTRCT-NBR EQ 'K0000010' THRU 'K4999999' OR #TIAA-CNTRCT-NBR EQ 'K8000000' THRU 'K8099999' OR #TIAA-CNTRCT-NBR EQ 'L9000000' THRU 'L9899999'
            else if (condition((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K0000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K4999999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K8000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K8099999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("L9000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("L9899999")))))
            {
                decideConditionsMet1149++;
                short decideConditionsMet1305 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #TYPE-SRA;//Natural: VALUE 'ST'
                if (condition((pnd_Type_Sra.equals("ST"))))
                {
                    decideConditionsMet1305++;
                    pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_St().setValue(1);                                                                                                   //Natural: MOVE 1 TO #CTRS.#CTR-SR-ST
                    pnd_Ctr_Total_Sr_St.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-SR-ST
                }                                                                                                                                                         //Natural: VALUE 'RP'
                else if (condition((pnd_Type_Sra.equals("RP"))))
                {
                    decideConditionsMet1305++;
                    pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Rp().setValue(1);                                                                                                   //Natural: MOVE 1 TO #CTRS.#CTR-SR-RP
                    pnd_Ctr_Total_Sr_Rp.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-SR-RP
                }                                                                                                                                                         //Natural: VALUE 'SV'
                else if (condition((pnd_Type_Sra.equals("SV"))))
                {
                    decideConditionsMet1305++;
                    short decideConditionsMet1313 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ISSUE-STATE-CDE EQ '033'
                    if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("033")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Nj().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-NJ
                        pnd_Ctr_Sra_Sv_Nj.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-NJ
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '041'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("041")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Pa().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-PA
                        pnd_Ctr_Sra_Sv_Pa.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-PA
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '008'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("008")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ct().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-CT
                        pnd_Ctr_Sra_Sv_Ct.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-CT
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '048'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("048")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Tx().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-TX
                        pnd_Ctr_Sra_Sv_Tx.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-TX
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '051'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("051")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Va().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-VA
                        pnd_Ctr_Sra_Sv_Va.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-VA
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '028'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("028")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Mo().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-MO
                        pnd_Ctr_Sra_Sv_Mo.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-MO
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '040'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("040")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Or().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-OR
                        pnd_Ctr_Sra_Sv_Or.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-OR
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '011'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("011")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Fl().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-FL
                        pnd_Ctr_Sra_Sv_Fl.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-FL
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '056'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("056")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Wi().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-WI
                        pnd_Ctr_Sra_Sv_Wi.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-WI
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '050'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("050")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Vt().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-VT
                        pnd_Ctr_Sra_Sv_Vt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-VT
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '045'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("045")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Sc().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-SC
                        pnd_Ctr_Sra_Sv_Sc.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-SC
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '049'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("049")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ut().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-UT
                        pnd_Ctr_Sra_Sv_Ut.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-UT
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '039'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("039")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ok().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-OK
                        pnd_Ctr_Sra_Sv_Ok.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-OK
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '030'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("030")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ne().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-NE
                        pnd_Ctr_Sra_Sv_Ne.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-NE
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '029'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("029")))
                    {
                        decideConditionsMet1313++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Mt().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-MT
                        //*  LS1
                        pnd_Ctr_Sra_Sv_Mt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-MT
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '032'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("032")))
                    {
                        decideConditionsMet1313++;
                        //*  LS1
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Nh().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-SR-NH
                        //*  LS1
                        pnd_Ctr_Sra_Sv_Nh.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-SRA-SV-NH
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                        //*                  MOVE 1 TO #CTR-SR-SV
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Ctr_Total_Sr_Sv.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-SR-SV
                    pdaRida410.getPnd_Miscellaneous_Pnd_State_Sra_Sv().getValue(pnd_I).setValue(1);                                                                       //Natural: MOVE 1 TO #STATE-SRA-SV ( #I )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                //*  060107 J.AVE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #TIAA-CNTRCT-NBR EQ 'K5000000' THRU 'K7999999' OR #TIAA-CNTRCT-NBR EQ 'L0000100' THRU 'L8499999'
            else if (condition(((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K5000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K7999999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("L0000100") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("L8499999")))))
            {
                decideConditionsMet1149++;
                if (condition(pnd_Input_Record_Pnd_State_Inst.equals("SUNY") && pnd_Input_Record_Pnd_Issue_State_Cde.equals("035")))                                      //Natural: IF #STATE-INST EQ 'SUNY' AND #ISSUE-STATE-CDE EQ '035'
                {
                    pnd_Type_Gsra.setValue("SV");                                                                                                                         //Natural: ASSIGN #TYPE-GSRA := 'SV'
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet1383 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #TYPE-GSRA;//Natural: VALUE 'ST'
                if (condition((pnd_Type_Gsra.equals("ST"))))
                {
                    decideConditionsMet1383++;
                    pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_St().setValue(1);                                                                                                   //Natural: MOVE 1 TO #CTRS.#CTR-GS-ST
                    pnd_Ctr_Total_Gs_St.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-GS-ST
                }                                                                                                                                                         //Natural: VALUE 'SV'
                else if (condition((pnd_Type_Gsra.equals("SV"))))
                {
                    decideConditionsMet1383++;
                    short decideConditionsMet1388 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ISSUE-STATE-CDE EQ '033'
                    if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("033")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Nj().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-NJ
                        pnd_Ctr_Gsra_Sv_Nj.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-NJ
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '041'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("041")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Pa().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-PA
                        pnd_Ctr_Gsra_Sv_Pa.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-PA
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '008'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("008")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ct().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-CT
                        pnd_Ctr_Gsra_Sv_Ct.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-CT
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '048'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("048")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Tx().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-TX
                        pnd_Ctr_Gsra_Sv_Tx.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-TX
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '051'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("051")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Va().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-VA
                        pnd_Ctr_Gsra_Sv_Va.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-VA
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '025'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("025")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mi().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-MI
                        pnd_Ctr_Gsra_Sv_Mi.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-MI
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '028'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("028")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mo().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-MO
                        pnd_Ctr_Gsra_Sv_Mo.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-MO
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '056'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("056")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Wi().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-WI
                        pnd_Ctr_Gsra_Sv_Wi.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-WI
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '011'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("011")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Fl().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-FL
                        pnd_Ctr_Gsra_Sv_Fl.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-FL
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '050'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("050")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Vt().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-VT
                        pnd_Ctr_Gsra_Sv_Vt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-VT
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '020'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("020")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ky().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-KY
                        pnd_Ctr_Gsra_Sv_Ky.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-KY
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '045'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("045")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Sc().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-SC
                        pnd_Ctr_Gsra_Sv_Sc.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-SC
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '047'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("047")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Tn().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-TN
                        pnd_Ctr_Gsra_Sv_Tn.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-TN
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '039'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("039")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ok().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-OK
                        pnd_Ctr_Gsra_Sv_Ok.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-OK
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '030'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("030")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ne().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-NE
                        pnd_Ctr_Gsra_Sv_Ne.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-NE
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '029'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("029")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mt().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-MT
                        pnd_Ctr_Gsra_Sv_Mt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-MT
                    }                                                                                                                                                     //Natural: WHEN #STATE-INST EQ 'SUNY' AND #ISSUE-STATE-CDE EQ '035'
                    else if (condition(pnd_Input_Record_Pnd_State_Inst.equals("SUNY") && pnd_Input_Record_Pnd_Issue_State_Cde.equals("035")))
                    {
                        decideConditionsMet1388++;
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Suny().setValue(1);                                                                                             //Natural: MOVE 1 TO #CTRS.#CTR-GS-SUNY
                        //*  LS1
                        pnd_Ctr_Gsra_Sv_Suny.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CTR-GSRA-SV-SUNY
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '004'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("004")))
                    {
                        decideConditionsMet1388++;
                        //*  LS1
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ar().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-AR
                        //*  LS1
                        //*  LS1
                        pnd_Ctr_Gsra_Sv_Ar.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-AR
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '014'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("014")))
                    {
                        decideConditionsMet1388++;
                        //*  LS1
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Hi().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-HI
                        //*  LS1
                        //*  LS1
                        pnd_Ctr_Gsra_Sv_Hi.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-HI
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '032'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("032")))
                    {
                        decideConditionsMet1388++;
                        //*  LS1
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Nh().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-NH
                        //*  LS1
                        //*  LS1
                        pnd_Ctr_Gsra_Sv_Nh.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-NH
                    }                                                                                                                                                     //Natural: WHEN #ISSUE-STATE-CDE EQ '040'
                    else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("040")))
                    {
                        decideConditionsMet1388++;
                        //*  LS1
                        pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Or().setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GS-OR
                        //*  LS1
                        pnd_Ctr_Gsra_Sv_Or.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-GSRA-SV-OR
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                        //*                  MOVE 1 TO #CTR-GS-SV
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Ctr_Total_Gs_Sv.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-GS-SV
                    pdaRida410.getPnd_Miscellaneous_Pnd_State_Gsra_Sv().getValue(pnd_I).setValue(1);                                                                      //Natural: MOVE 1 TO #STATE-GSRA-SV ( #I )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                //*  060107 J.AVE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Ctr_Contract_Outof_Range.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CTR-CONTRACT-OUTOF-RANGE
                //*          #REMARKS := 'CONTRACT OUT OF RANGE'
                //*          WRITE #INPUT-RECORD.#PIN 3X #TIAA-CNTRCT-NBR
                //*                         #ISSUE-STATE-CDE    3X #REMARKS
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE-RECORD
    }
    private void sub_Get_Cover_Letter_Type() throws Exception                                                                                                             //Natural: GET-COVER-LETTER-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------
        short decideConditionsMet1492 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #COVER-LETTER-IND;//Natural: VALUE '1','3','5','7','9'
        if (condition((pnd_Input_Record_Pnd_Cover_Letter_Ind.equals("1") || pnd_Input_Record_Pnd_Cover_Letter_Ind.equals("3") || pnd_Input_Record_Pnd_Cover_Letter_Ind.equals("5") 
            || pnd_Input_Record_Pnd_Cover_Letter_Ind.equals("7") || pnd_Input_Record_Pnd_Cover_Letter_Ind.equals("9"))))
        {
            decideConditionsMet1492++;
            pnd_Wmmm.setValue("MM");                                                                                                                                      //Natural: ASSIGN #WMMM := 'MM'
        }                                                                                                                                                                 //Natural: VALUE '2','4','6'
        else if (condition((pnd_Input_Record_Pnd_Cover_Letter_Ind.equals("2") || pnd_Input_Record_Pnd_Cover_Letter_Ind.equals("4") || pnd_Input_Record_Pnd_Cover_Letter_Ind.equals("6"))))
        {
            decideConditionsMet1492++;
            pnd_Wmmm.setValue("WM");                                                                                                                                      //Natural: ASSIGN #WMMM := 'WM'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  GET-COVER-LETTER-TYPE
    }
    private void sub_Get_Residence_State_Code() throws Exception                                                                                                          //Natural: GET-RESIDENCE-STATE-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------
        if (condition(DbsUtil.maskMatches(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code(),"99")))                                                          //Natural: IF #O-BASE-ADDRESS-GEOGRAPHIC-CODE EQ MASK ( 99 )
        {
            pnd_State_Unknown.compute(new ComputeParameters(false, pnd_State_Unknown), DbsField.add(1000,pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Geographic_Code().val())); //Natural: ASSIGN #STATE-UNKNOWN := 1000 + VAL ( #O-BASE-ADDRESS-GEOGRAPHIC-CODE )
            pnd_Residence_State_Cde.setValue(pnd_State_Unknown_Pnd_State_Unknown_B);                                                                                      //Natural: ASSIGN #RESIDENCE-STATE-CDE := #STATE-UNKNOWN-B
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Residence_State_Cde.setValue("000");                                                                                                                      //Natural: ASSIGN #RESIDENCE-STATE-CDE := '000'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Input_Record_Pnd_Issue_State_Cde.setValue(pnd_Residence_State_Cde);                                                                                           //Natural: ASSIGN #ISSUE-STATE-CDE := #RESIDENCE-STATE-CDE
        //*  GET-RESIDENCE-STATE-CODE
    }
    private void sub_Check_Record_In_Cor() throws Exception                                                                                                               //Natural: CHECK-RECORD-IN-COR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------
        //*   MODIFY MDM CALL FOR PINEXP MDMN211A               /* PINEXP >>>
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #MDMA211.#I-CONTRACT-NUMBER
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue("01");                                                                                                   //Natural: MOVE '01' TO #MDMA211.#I-PAYEE-CODE-A2
        getReports().write(0, "CALLING MDMN211A TO GET CONTRACT INFORMATION");                                                                                            //Natural: WRITE 'CALLING MDMN211A TO GET CONTRACT INFORMATION'
        if (Global.isEscape()) return;
        //*  JCA20150324
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        getReports().write(0, "=",pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code());                                                                                         //Natural: WRITE '=' #MDMA211.#O-RETURN-CODE
        if (Global.isEscape()) return;
        //*  JCA20150324
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA211.#O-RETURN-CODE NE '0000'
        {
            pnd_Remarks.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Text());                                                                                          //Natural: ASSIGN #REMARKS := #MDMA211.#O-RETURN-TEXT
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Contract_Not_In_Cor.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-DA-CONTRACT-NOT-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals(" ") || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("H")         //Natural: IF NOT ( #MDMA211.#O-CONTRACT-STATUS-CODE = ' ' OR = 'H' OR = 'Y' )
            || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("Y"))))
        {
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("CONTRACT INACTIVE IN COR - STATUS CODE:", pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code()));                    //Natural: COMPRESS 'CONTRACT INACTIVE IN COR - STATUS CODE:' #MDMA211.#O-CONTRACT-STATUS-CODE TO #REMARKS
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Inactive_In_Cor.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DA-INACTIVE-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PIN-STATUS
            sub_Determine_Pin_Status();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Accepted.getBoolean()))                                                                                                                     //Natural: IF #ACCEPTED
            {
                pnd_Valid_Record.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-RECORD-IN-COR
    }
    private void sub_Determine_Pin_Status() throws Exception                                                                                                              //Natural: DETERMINE-PIN-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------
        //*    MODIFY MDM*100 TO MDM*101                           /* PINEXP
        //*  JCA20160719
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        //*    MOVE #INPUT-RECORD.#PIN TO #MDMA101.#I-PIN          /* JCA20160719
        //*  PINEXP >>>
        if (condition(pnd_Input_Record_Pnd_Pin_A12.notEquals(" ")))                                                                                                       //Natural: IF #INPUT-RECORD.#PIN-A12 NE ' '
        {
            if (condition(pnd_Input_Record_Pnd_Pin_N5.equals(getZero())))                                                                                                 //Natural: IF #INPUT-RECORD.#PIN-N5 = 0
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N7);                                                                          //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #INPUT-RECORD.#PIN-N7
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                         //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #INPUT-RECORD.#PIN-N12
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP <<<
        }                                                                                                                                                                 //Natural: END-IF
        //*    WRITE 'CALLING MDMN100A TO GET ADDRESS/NAME'
        //*  JCA20160719
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  REJECT DECEASED PARTICIPANTS
        //*  JCA20160719
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero())))                                                                                //Natural: IF #MDMA101.#O-DATE-OF-DEATH GT 0
        {
            pnd_Ctr_Ph_Deceased.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CTR-PH-DECEASED
            //*  JCA20160719
            pnd_Remarks.setValue(DbsUtil.compress("P/H DECEASED - DOD:", pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death()));                                               //Natural: COMPRESS 'P/H DECEASED - DOD:' #MDMA101.#O-DATE-OF-DEATH TO #REMARKS
            getReports().write(1, pnd_Input_Record_Pnd_Pin_N12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-N12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Accepted.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #ACCEPTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  REJECT PARTICIPANTS W/ STOP ALL MAILING SUPPRESSION CODE
            //*  JCA20160719
            DbsUtil.examine(new ExamineSource(pdaMdma101.getPnd_Mdma101_Pnd_O_Mail_Code().getValue("*")), new ExamineSearch("01"), new ExamineGivingPosition(pnd_I));     //Natural: EXAMINE #MDMA101.#O-MAIL-CODE ( * ) FOR '01' GIVING POSITION #I
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I GT 0
            {
                pnd_Ctr_Ph_With_Mail_Suppression.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTR-PH-WITH-MAIL-SUPPRESSION
                pnd_Remarks.setValue(DbsUtil.compress("P/H MAIL SUPPRESSED - MAIL CODE: 01"));                                                                            //Natural: COMPRESS 'P/H MAIL SUPPRESSED - MAIL CODE: 01' TO #REMARKS
                getReports().write(1, pnd_Input_Record_Pnd_Pin_N12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-N12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Accepted.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #ACCEPTED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Accepted.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #ACCEPTED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE-PIN-STATUS
    }
    //*  060107 J.AVE
    private void sub_Add_Contract_To_Array() throws Exception                                                                                                             //Natural: ADD-CONTRACT-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------
        pnd_K.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #K
        pdaRida410.getPnd_Output_Pnd_Contracts().getValue(pnd_K).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                          //Natural: MOVE #TIAA-CNTRCT-NBR TO #CONTRACTS ( #K )
        //*  ADD-CONTRACT-TO-ARRAY
    }
    private void sub_Write_Data_To_File() throws Exception                                                                                                                //Natural: WRITE-DATA-TO-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        //* *  IF #EML                               /* CPM ELIMINATION >>>
        //* *    MOVE 'RDR2009Q' TO #PACKAGE-CODE
        //* *    ADD 1 TO #CTR-FILE14
        //* *    WRITE WORK FILE 18 #OUTPUT
        //* *  ELSE                                  /* CPM ELIMINATION <<<
        //*      PERFORM COMPUTE-NUMBER-PAGES
        //*      DECIDE FOR FIRST CONDITION
        //*        WHEN #NBR-PAGES GE  8 AND #NBR-PAGES LE 14
        pdaRida410.getPnd_Output_Pnd_Package_Code().setValue("RDR2009D");                                                                                                 //Natural: MOVE 'RDR2009D' TO #PACKAGE-CODE
        pdaRida410.getPnd_Miscellaneous_Pnd_Ctr_File1().nadd(1);                                                                                                          //Natural: ADD 1 TO #CTR-FILE1
        getWorkFiles().write(2, false, pdaRida410.getPnd_Output());                                                                                                       //Natural: WRITE WORK FILE 2 #OUTPUT
        //*        WHEN #NBR-PAGES GE 15 AND #NBR-PAGES LE 20
        //*          MOVE 'RDR2009E' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE2
        //*          WRITE WORK FILE 3 #OUTPUT
        //*        WHEN #NBR-PAGES GE 21 AND #NBR-PAGES LE 26
        //*          MOVE 'RDR2009F' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE3
        //*          WRITE WORK FILE 4 #OUTPUT
        //*        WHEN #NBR-PAGES GE 27 AND #NBR-PAGES LE 32
        //*          MOVE 'RDR2009G' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE4
        //*          WRITE WORK FILE 5 #OUTPUT
        //*        WHEN #NBR-PAGES GE 33 AND #NBR-PAGES LE 39
        //*          MOVE 'RDR2009H' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE5
        //*          WRITE WORK FILE 6 #OUTPUT
        //*        WHEN #NBR-PAGES GE 40 AND #NBR-PAGES LE 45
        //*          MOVE 'RDR2009I' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE6
        //*          WRITE WORK FILE 7 #OUTPUT
        //*        WHEN #NBR-PAGES GE 46 AND #NBR-PAGES LE 51
        //*          MOVE 'RDR2009J' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE7
        //*          WRITE WORK FILE 8 #OUTPUT
        //*        WHEN #NBR-PAGES GE 52 AND #NBR-PAGES LE 57
        //*          MOVE 'RDR2009K' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE8
        //*          WRITE WORK FILE 9 #OUTPUT
        //*        WHEN #NBR-PAGES GE 58 AND #NBR-PAGES LE 64
        //*          MOVE 'RDR2009L' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE9
        //*          WRITE WORK FILE 10 #OUTPUT
        //*        WHEN #NBR-PAGES GE 65 AND #NBR-PAGES LE 70
        //*          MOVE 'RDR2009M' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE10
        //*          WRITE WORK FILE 11 #OUTPUT
        //*        WHEN #NBR-PAGES GE 71 AND #NBR-PAGES LE 76
        //*          MOVE 'RDR2009N' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE11
        //*          WRITE WORK FILE 12 #OUTPUT
        //*        WHEN #NBR-PAGES GE 77 AND #NBR-PAGES LE 82
        //*          MOVE 'RDR2009O' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE12
        //*          WRITE WORK FILE 13 #OUTPUT
        //*        WHEN NONE
        //*          MOVE 'RDR2009P' TO #PACKAGE-CODE
        //*          ADD 1 TO #CTR-FILE13
        //*          WRITE WORK FILE 14 #OUTPUT
        //*      END-DECIDE
        //* *    END-IF                           /* CPM ELIMINATION
        //*  WRITE-DATA-TO-FILE
    }
    private void sub_Compute_Number_Pages() throws Exception                                                                                                              //Natural: COMPUTE-NUMBER-PAGES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Nbr_Pages.compute(new ComputeParameters(false, pnd_Nbr_Pages), pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_St().multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Rp()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_St()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Rp()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_St()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_St()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Nj()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Nj()).multiply(12).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nj()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Nj()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Pa()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Pa()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Pa()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Pa()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ct()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ct()).multiply(12).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ct()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ct()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Tx()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Tx()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Tx()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Tx()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Mo()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Va()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Va()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Va()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Va()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mi()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Mo()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Mo()).multiply(12).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mo()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Fl()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Fl()).multiply(12).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Fl()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Fl()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ok()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ok()).multiply(12).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ok()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ok()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Or()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Or()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Or()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Or()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Sc()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Sc()).multiply(12).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Sc()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Sc()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Wi()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Wi()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Wi()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Wi()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ne()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ne()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ne()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ne()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Mt()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Mt()).multiply(12).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Mt()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mt()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ar()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Hi()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ky()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nc()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Tn()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ut()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ut()).multiply(12).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Vt()).multiply(12).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Vt()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Pr()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Pr()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Njabp()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Nh()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Nh()).multiply(13).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nh()).multiply(14).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Nh()).multiply(15).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Suny()).multiply(15).add(2)); //Natural: ASSIGN #NBR-PAGES := #CTRS.#CTR-RA-ST*13 + #CTRS.#CTR-RA-RP*13 + #CTRS.#CTR-SR-ST*13 + #CTRS.#CTR-SR-RP*13 + #CTRS.#CTR-GR-ST*14 + #CTRS.#CTR-GS-ST*15 + #CTRS.#CTR-RA-NJ*13 + #CTRS.#CTR-SR-NJ*12 + #CTRS.#CTR-GR-NJ*14 + #CTRS.#CTR-GS-NJ*15 + #CTRS.#CTR-RA-PA*13 + #CTRS.#CTR-SR-PA*13 + #CTRS.#CTR-GR-PA*14 + #CTRS.#CTR-GS-PA*15 + #CTRS.#CTR-RA-CT*13 + #CTRS.#CTR-SR-CT*12 + #CTRS.#CTR-GR-CT*14 + #CTRS.#CTR-GS-CT*15 + #CTRS.#CTR-RA-TX*13 + #CTRS.#CTR-SR-TX*13 + #CTRS.#CTR-GS-TX*15 + #CTRS.#CTR-GR-TX*14 + #CTRS.#CTR-GR-MO*14 + #CTRS.#CTR-RA-VA*13 + #CTRS.#CTR-SR-VA*13 + #CTRS.#CTR-GR-VA*14 + #CTRS.#CTR-GS-VA*15 + #CTRS.#CTR-GS-MI*15 + #CTRS.#CTR-RA-MO*13 + #CTRS.#CTR-SR-MO*12 + #CTRS.#CTR-GS-MO*15 + #CTRS.#CTR-RA-FL*13 + #CTRS.#CTR-SR-FL*12 + #CTRS.#CTR-GR-FL*14 + #CTRS.#CTR-GS-FL*15 + #CTRS.#CTR-RA-OK*13 + #CTRS.#CTR-SR-OK*12 + #CTRS.#CTR-GR-OK*14 + #CTRS.#CTR-GS-OK*15 + #CTRS.#CTR-RA-OR*13 + #CTRS.#CTR-SR-OR*13 + #CTRS.#CTR-GR-OR*14 + #CTRS.#CTR-GS-OR*15 + #CTRS.#CTR-RA-SC*13 + #CTRS.#CTR-SR-SC*12 + #CTRS.#CTR-GR-SC*14 + #CTRS.#CTR-GS-SC*15 + #CTRS.#CTR-RA-WI*13 + #CTRS.#CTR-SR-WI*13 + #CTRS.#CTR-GR-WI*14 + #CTRS.#CTR-GS-WI*15 + #CTRS.#CTR-RA-NE*13 + #CTRS.#CTR-SR-NE*13 + #CTRS.#CTR-GR-NE*14 + #CTRS.#CTR-GS-NE*15 + #CTRS.#CTR-RA-MT*13 + #CTRS.#CTR-SR-MT*12 + #CTRS.#CTR-GR-MT*14 + #CTRS.#CTR-GS-MT*15 + #CTRS.#CTR-GS-AR*15 + #CTRS.#CTR-GS-HI*15 + #CTRS.#CTR-GS-KY*15 + #CTRS.#CTR-GR-NC*14 + #CTRS.#CTR-GS-TN*15 + #CTRS.#CTR-RA-UT*13 + #CTRS.#CTR-SR-UT*12 + #CTRS.#CTR-SR-VT*12 + #CTRS.#CTR-GS-VT*15 + #CTRS.#CTR-RA-PR*13 + #CTRS.#CTR-GR-PR*13 + #CTRS.#CTR-GR-NJABP*14 + #CTRS.#CTR-RA-NH*13 + #CTRS.#CTR-SR-NH*13 + #CTRS.#CTR-GR-NH*14 + #CTRS.#CTR-GS-NH*15 + #CTRS.#CTR-GS-SUNY*15 + 2
        //*  COMPUTE-NUMBER-PAGES
    }
    private void sub_Write_Statistics_Report() throws Exception                                                                                                           //Natural: WRITE-STATISTICS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------------------
        getReports().write(1, NEWLINE,NEWLINE,"TOTAL INSTITUTION-OWNED CONTRACTS :",pnd_Ctr_Institution_Owned, new ReportEditMask ("Z,ZZZ,ZZ9"));                         //Natural: WRITE ( 1 ) // 'TOTAL INSTITUTION-OWNED CONTRACTS :' #CTR-INSTITUTION-OWNED ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        pnd_Ctr_Contract_Rejected.compute(new ComputeParameters(false, pnd_Ctr_Contract_Rejected), pnd_Ctr_Da_Contract_Not_In_Cor.add(pnd_Ctr_Da_Inactive_In_Cor).add(pnd_Ctr_Ph_Deceased).add(pnd_Ctr_Ph_With_Mail_Suppression).add(pnd_Ctr_Contract_Outof_Range).add(pnd_Ctr_Duplicate_Contract).add(pnd_Ctr_Contract_Excluded)); //Natural: ASSIGN #CTR-CONTRACT-REJECTED := #CTR-DA-CONTRACT-NOT-IN-COR + #CTR-DA-INACTIVE-IN-COR + #CTR-PH-DECEASED + #CTR-PH-WITH-MAIL-SUPPRESSION + #CTR-CONTRACT-OUTOF-RANGE + #CTR-DUPLICATE-CONTRACT + #CTR-CONTRACT-EXCLUDED
        getReports().write(0, "INSTITUTION NAME:",pnd_Input_Record_Pnd_Inst_Name, new AlphanumericLength (60));                                                           //Natural: WRITE 'INSTITUTION NAME:' #INST-NAME ( AL = 60 )
        if (Global.isEscape()) return;
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ         :",pnd_Ctr_Access_Contract_Read);                                                                           //Natural: WRITE 'NUMBER OF RECORDS READ         :' #CTR-ACCESS-CONTRACT-READ
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS ACCEPTED     :",pnd_Ctr_Contract_Accepted);                                                                              //Natural: WRITE 'NUMBER OF RECORDS ACCEPTED     :' #CTR-CONTRACT-ACCEPTED
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS REJECTED     :",pnd_Ctr_Contract_Rejected);                                                                              //Natural: WRITE 'NUMBER OF RECORDS REJECTED     :' #CTR-CONTRACT-REJECTED
        if (Global.isEscape()) return;
        getReports().write(0, "BREAKDOWN OF RECORDS REJECTED  :");                                                                                                        //Natural: WRITE 'BREAKDOWN OF RECORDS REJECTED  :'
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT NOT IN COR     :",pnd_Ctr_Da_Contract_Not_In_Cor);                                                                              //Natural: WRITE ' -CONTRACT NOT IN COR     :' #CTR-DA-CONTRACT-NOT-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT INACTIVE IN COR:",pnd_Ctr_Da_Inactive_In_Cor);                                                                                  //Natural: WRITE ' -CONTRACT INACTIVE IN COR:' #CTR-DA-INACTIVE-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H DECEASED            :",pnd_Ctr_Ph_Deceased);                                                                                         //Natural: WRITE ' -P/H DECEASED            :' #CTR-PH-DECEASED
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H W/ MAIL SUPPRESSION :",pnd_Ctr_Ph_With_Mail_Suppression);                                                                            //Natural: WRITE ' -P/H W/ MAIL SUPPRESSION :' #CTR-PH-WITH-MAIL-SUPPRESSION
        if (Global.isEscape()) return;
        //*    WRITE ' -ISSUE STATE OUTSIDE CA  :' #CTR-OUT-OF-STATE
        getReports().write(0, " -CONTRACT OUT OF RANGE   :",pnd_Ctr_Contract_Outof_Range);                                                                                //Natural: WRITE ' -CONTRACT OUT OF RANGE   :' #CTR-CONTRACT-OUTOF-RANGE
        if (Global.isEscape()) return;
        //*    WRITE ' -STATE-INST NOT FOR PROC :' #CTR-STATE-INST-NOT-FOR-PROC
        //*    WRITE ' -INSTITUTION OWNED       :' #CTR-INSTITUTION-OWNED
        getReports().write(0, " -DUPLICATE CONTRACT      :",pnd_Ctr_Duplicate_Contract);                                                                                  //Natural: WRITE ' -DUPLICATE CONTRACT      :' #CTR-DUPLICATE-CONTRACT
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT EXCLUDED       :",pnd_Ctr_Contract_Excluded);                                                                                   //Natural: WRITE ' -CONTRACT EXCLUDED       :' #CTR-CONTRACT-EXCLUDED
        if (Global.isEscape()) return;
        getReports().write(0, " -EXCLUDED GA RESIDENT    :",pnd_Ctr_Exclude_Ga);                                                                                          //Natural: WRITE ' -EXCLUDED GA RESIDENT    :' #CTR-EXCLUDE-GA
        if (Global.isEscape()) return;
        getReports().write(0, " -EXCLUDED OK RESIDENT    :",pnd_Ctr_Exclude_Ok);                                                                                          //Natural: WRITE ' -EXCLUDED OK RESIDENT    :' #CTR-EXCLUDE-OK
        if (Global.isEscape()) return;
        getReports().write(0, " -EXCLUDED MS RESIDENT    :",pnd_Ctr_Exclude_Ms);                                                                                          //Natural: WRITE ' -EXCLUDED MS RESIDENT    :' #CTR-EXCLUDE-MS
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY PRODUCT:");                                                                                                                       //Natural: WRITE 'TOTAL BY PRODUCT:'
        if (Global.isEscape()) return;
        getReports().write(0, " - RA STANDARD           :",pnd_Ctr_Total_Ra_St);                                                                                          //Natural: WRITE ' - RA STANDARD           :' #CTR-TOTAL-RA-ST
        if (Global.isEscape()) return;
        getReports().write(0, " - RA RETURN OF PREMIUM  :",pnd_Ctr_Total_Ra_Rp);                                                                                          //Natural: WRITE ' - RA RETURN OF PREMIUM  :' #CTR-TOTAL-RA-RP
        if (Global.isEscape()) return;
        getReports().write(0, " - RA STATE VARIATION    :",pnd_Ctr_Total_Ra_Sv);                                                                                          //Natural: WRITE ' - RA STATE VARIATION    :' #CTR-TOTAL-RA-SV
        if (Global.isEscape()) return;
        getReports().write(0, " - SRA STANDARD          :",pnd_Ctr_Total_Sr_St);                                                                                          //Natural: WRITE ' - SRA STANDARD          :' #CTR-TOTAL-SR-ST
        if (Global.isEscape()) return;
        getReports().write(0, " - SRA RETURN OF PREMIUM :",pnd_Ctr_Total_Sr_Rp);                                                                                          //Natural: WRITE ' - SRA RETURN OF PREMIUM :' #CTR-TOTAL-SR-RP
        if (Global.isEscape()) return;
        getReports().write(0, " - SRA STATE VARIATION   :",pnd_Ctr_Total_Sr_Sv);                                                                                          //Natural: WRITE ' - SRA STATE VARIATION   :' #CTR-TOTAL-SR-SV
        if (Global.isEscape()) return;
        getReports().write(0, " - GRA STANDARD          :",pnd_Ctr_Total_Gr_St);                                                                                          //Natural: WRITE ' - GRA STANDARD          :' #CTR-TOTAL-GR-ST
        if (Global.isEscape()) return;
        getReports().write(0, " - GRA STATE VARIATION   :",pnd_Ctr_Total_Gr_Sv);                                                                                          //Natural: WRITE ' - GRA STATE VARIATION   :' #CTR-TOTAL-GR-SV
        if (Global.isEscape()) return;
        getReports().write(0, " - GSRA STANDARD         :",pnd_Ctr_Total_Gs_St);                                                                                          //Natural: WRITE ' - GSRA STANDARD         :' #CTR-TOTAL-GS-ST
        if (Global.isEscape()) return;
        getReports().write(0, " - GSRA STATE VARIATION  :",pnd_Ctr_Total_Gs_Sv);                                                                                          //Natural: WRITE ' - GSRA STATE VARIATION  :' #CTR-TOTAL-GS-SV
        if (Global.isEscape()) return;
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY FILE:");                                                                                                                          //Natural: WRITE 'TOTAL BY FILE:'
        if (Global.isEscape()) return;
        getReports().write(0, "FILE  1: ",pdaRida410.getPnd_Miscellaneous_Pnd_Ctr_File1());                                                                               //Natural: WRITE 'FILE  1: ' #CTR-FILE1
        if (Global.isEscape()) return;
        //*    WRITE 'FILE  2: ' #CTR-FILE2
        //*    WRITE 'FILE  3: ' #CTR-FILE3
        //*    WRITE 'FILE  4: ' #CTR-FILE4
        //*    WRITE 'FILE  5: ' #CTR-FILE5
        //*    WRITE 'FILE  6: ' #CTR-FILE6
        //*    WRITE 'FILE  7: ' #CTR-FILE7
        //*    WRITE 'FILE  8: ' #CTR-FILE8
        //*    WRITE 'FILE  9: ' #CTR-FILE9
        //*    WRITE 'FILE 10: ' #CTR-FILE10
        //*    WRITE 'FILE 11: ' #CTR-FILE11
        //*    WRITE 'FILE 12: ' #CTR-FILE12
        //*    WRITE 'FILE 13: ' #CTR-FILE13
        //*    WRITE 'FILE 14: ' #CTR-FILE14  20T '(E-MAIL)'
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        getReports().write(0, "STATE VARIATION COUNT DETAILS:");                                                                                                          //Natural: WRITE 'STATE VARIATION COUNT DETAILS:'
        if (Global.isEscape()) return;
        getReports().write(0, "STATE                  RA        SRA       GRA       GSRA");                                                                               //Natural: WRITE 'STATE                  RA        SRA       GRA       GSRA'
        if (Global.isEscape()) return;
        getReports().write(0, "---------------------------------------------------------");                                                                               //Natural: WRITE '---------------------------------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "ARKANSAS       ",new TabSetting(50),pnd_Ctr_Gsra_Sv_Ar);                                                                                   //Natural: WRITE 'ARKANSAS       ' 50T #CTR-GSRA-SV-AR
        if (Global.isEscape()) return;
        getReports().write(0, "CONNECTICUT    ",new TabSetting(20),pnd_Ctr_Ra_Sv_Ct,new TabSetting(30),pnd_Ctr_Sra_Sv_Ct,new TabSetting(40),pnd_Ctr_Gra_Sv_Ct,new         //Natural: WRITE 'CONNECTICUT    ' 20T #CTR-RA-SV-CT 30T #CTR-SRA-SV-CT 40T #CTR-GRA-SV-CT 50T #CTR-GSRA-SV-CT
            TabSetting(50),pnd_Ctr_Gsra_Sv_Ct);
        if (Global.isEscape()) return;
        getReports().write(0, "FLORIDA        ",new TabSetting(20),pnd_Ctr_Ra_Sv_Fl,new TabSetting(30),pnd_Ctr_Sra_Sv_Fl,new TabSetting(40),pnd_Ctr_Gra_Sv_Fl,new         //Natural: WRITE 'FLORIDA        ' 20T #CTR-RA-SV-FL 30T #CTR-SRA-SV-FL 40T #CTR-GRA-SV-FL 50T #CTR-GSRA-SV-FL
            TabSetting(50),pnd_Ctr_Gsra_Sv_Fl);
        if (Global.isEscape()) return;
        getReports().write(0, "HAWAII         ",new TabSetting(50),pnd_Ctr_Gsra_Sv_Hi);                                                                                   //Natural: WRITE 'HAWAII         ' 50T #CTR-GSRA-SV-HI
        if (Global.isEscape()) return;
        getReports().write(0, "KENTUCKY       ",new TabSetting(50),pnd_Ctr_Gsra_Sv_Ky);                                                                                   //Natural: WRITE 'KENTUCKY       ' 50T #CTR-GSRA-SV-KY
        if (Global.isEscape()) return;
        getReports().write(0, "MICHIGAN       ",new TabSetting(50),pnd_Ctr_Gsra_Sv_Mi);                                                                                   //Natural: WRITE 'MICHIGAN       ' 50T #CTR-GSRA-SV-MI
        if (Global.isEscape()) return;
        getReports().write(0, "MISSOURI       ",new TabSetting(20),pnd_Ctr_Ra_Sv_Mo,new TabSetting(30),pnd_Ctr_Sra_Sv_Mo,new TabSetting(40),pnd_Ctr_Gra_Sv_Mo,new         //Natural: WRITE 'MISSOURI       ' 20T #CTR-RA-SV-MO 30T #CTR-SRA-SV-MO 40T #CTR-GRA-SV-MO 50T #CTR-GSRA-SV-MO
            TabSetting(50),pnd_Ctr_Gsra_Sv_Mo);
        if (Global.isEscape()) return;
        getReports().write(0, "MONTANA        ",new TabSetting(20),pnd_Ctr_Ra_Sv_Mt,new TabSetting(30),pnd_Ctr_Sra_Sv_Mt,new TabSetting(40),pnd_Ctr_Gra_Sv_Mt,new         //Natural: WRITE 'MONTANA        ' 20T #CTR-RA-SV-MT 30T #CTR-SRA-SV-MT 40T #CTR-GRA-SV-MT 50T #CTR-GSRA-SV-MT
            TabSetting(50),pnd_Ctr_Gsra_Sv_Mt);
        if (Global.isEscape()) return;
        getReports().write(0, "NEBRASKA       ",new TabSetting(20),pnd_Ctr_Ra_Sv_Ne,new TabSetting(30),pnd_Ctr_Sra_Sv_Ne,new TabSetting(40),pnd_Ctr_Gra_Sv_Ne,new         //Natural: WRITE 'NEBRASKA       ' 20T #CTR-RA-SV-NE 30T #CTR-SRA-SV-NE 40T #CTR-GRA-SV-NE 50T #CTR-GSRA-SV-NE
            TabSetting(50),pnd_Ctr_Gsra_Sv_Ne);
        if (Global.isEscape()) return;
        getReports().write(0, "NEW JERSEY     ",new TabSetting(20),pnd_Ctr_Ra_Sv_Nj,new TabSetting(30),pnd_Ctr_Sra_Sv_Nj,new TabSetting(40),pnd_Ctr_Gra_Sv_Nj,new         //Natural: WRITE 'NEW JERSEY     ' 20T #CTR-RA-SV-NJ 30T #CTR-SRA-SV-NJ 40T #CTR-GRA-SV-NJ 50T #CTR-GSRA-SV-NJ
            TabSetting(50),pnd_Ctr_Gsra_Sv_Nj);
        if (Global.isEscape()) return;
        getReports().write(0, "NEW HAMPSHIRE  ",new TabSetting(20),pnd_Ctr_Ra_Sv_Nh,new TabSetting(30),pnd_Ctr_Sra_Sv_Nh,new TabSetting(40),pnd_Ctr_Gra_Sv_Nh,new         //Natural: WRITE 'NEW HAMPSHIRE  ' 20T #CTR-RA-SV-NH 30T #CTR-SRA-SV-NH 40T #CTR-GRA-SV-NH 50T #CTR-GSRA-SV-NH
            TabSetting(50),pnd_Ctr_Gsra_Sv_Nh);
        if (Global.isEscape()) return;
        getReports().write(0, "NORTH CAROLINA ",new TabSetting(40),pnd_Ctr_Gra_Sv_Nc);                                                                                    //Natural: WRITE 'NORTH CAROLINA ' 40T #CTR-GRA-SV-NC
        if (Global.isEscape()) return;
        getReports().write(0, "OKLAHOMA       ",new TabSetting(20),pnd_Ctr_Ra_Sv_Ok,new TabSetting(30),pnd_Ctr_Sra_Sv_Ok,new TabSetting(40),pnd_Ctr_Gra_Sv_Ok,new         //Natural: WRITE 'OKLAHOMA       ' 20T #CTR-RA-SV-OK 30T #CTR-SRA-SV-OK 40T #CTR-GRA-SV-OK 50T #CTR-GSRA-SV-OK
            TabSetting(50),pnd_Ctr_Gsra_Sv_Ok);
        if (Global.isEscape()) return;
        getReports().write(0, "OREGON         ",new TabSetting(20),pnd_Ctr_Ra_Sv_Or,new TabSetting(30),pnd_Ctr_Sra_Sv_Or,new TabSetting(40),pnd_Ctr_Gra_Sv_Or,new         //Natural: WRITE 'OREGON         ' 20T #CTR-RA-SV-OR 30T #CTR-SRA-SV-OR 40T #CTR-GRA-SV-OR 50T #CTR-GSRA-SV-OR
            TabSetting(50),pnd_Ctr_Gsra_Sv_Or);
        if (Global.isEscape()) return;
        getReports().write(0, "PENNSYLVANIA   ",new TabSetting(20),pnd_Ctr_Ra_Sv_Pa,new TabSetting(30),pnd_Ctr_Sra_Sv_Pa,new TabSetting(40),pnd_Ctr_Gra_Sv_Pa,new         //Natural: WRITE 'PENNSYLVANIA   ' 20T #CTR-RA-SV-PA 30T #CTR-SRA-SV-PA 40T #CTR-GRA-SV-PA 50T #CTR-GSRA-SV-PA
            TabSetting(50),pnd_Ctr_Gsra_Sv_Pa);
        if (Global.isEscape()) return;
        getReports().write(0, "PUERTO RICO    ",new TabSetting(20),pnd_Ctr_Ra_Sv_Pr,new TabSetting(40),pnd_Ctr_Gra_Sv_Pr);                                                //Natural: WRITE 'PUERTO RICO    ' 20T #CTR-RA-SV-PR 40T #CTR-GRA-SV-PR
        if (Global.isEscape()) return;
        getReports().write(0, "SOUTH CAROLINA ",new TabSetting(20),pnd_Ctr_Ra_Sv_Sc,new TabSetting(30),pnd_Ctr_Sra_Sv_Sc,new TabSetting(40),pnd_Ctr_Gra_Sv_Sc,new         //Natural: WRITE 'SOUTH CAROLINA ' 20T #CTR-RA-SV-SC 30T #CTR-SRA-SV-SC 40T #CTR-GRA-SV-SC 50T #CTR-GSRA-SV-SC
            TabSetting(50),pnd_Ctr_Gsra_Sv_Sc);
        if (Global.isEscape()) return;
        getReports().write(0, "TENNESSEE      ",new TabSetting(50),pnd_Ctr_Gsra_Sv_Tn);                                                                                   //Natural: WRITE 'TENNESSEE      ' 50T #CTR-GSRA-SV-TN
        if (Global.isEscape()) return;
        getReports().write(0, "TEXAS          ",new TabSetting(20),pnd_Ctr_Ra_Sv_Tx,new TabSetting(30),pnd_Ctr_Sra_Sv_Tx,new TabSetting(40),pnd_Ctr_Gra_Sv_Tx,new         //Natural: WRITE 'TEXAS          ' 20T #CTR-RA-SV-TX 30T #CTR-SRA-SV-TX 40T #CTR-GRA-SV-TX 50T #CTR-GSRA-SV-TX
            TabSetting(50),pnd_Ctr_Gsra_Sv_Tx);
        if (Global.isEscape()) return;
        getReports().write(0, "UTAH           ",new TabSetting(20),pnd_Ctr_Ra_Sv_Ut,new TabSetting(30),pnd_Ctr_Sra_Sv_Ut);                                                //Natural: WRITE 'UTAH           ' 20T #CTR-RA-SV-UT 30T #CTR-SRA-SV-UT
        if (Global.isEscape()) return;
        getReports().write(0, "VERMONT        ",new TabSetting(30),pnd_Ctr_Sra_Sv_Vt,new TabSetting(50),pnd_Ctr_Gsra_Sv_Vt);                                              //Natural: WRITE 'VERMONT        ' 30T #CTR-SRA-SV-VT 50T #CTR-GSRA-SV-VT
        if (Global.isEscape()) return;
        getReports().write(0, "VIRGINIA       ",new TabSetting(20),pnd_Ctr_Ra_Sv_Va,new TabSetting(30),pnd_Ctr_Sra_Sv_Va,new TabSetting(40),pnd_Ctr_Gra_Sv_Va,new         //Natural: WRITE 'VIRGINIA       ' 20T #CTR-RA-SV-VA 30T #CTR-SRA-SV-VA 40T #CTR-GRA-SV-VA 50T #CTR-GSRA-SV-VA
            TabSetting(50),pnd_Ctr_Gsra_Sv_Va);
        if (Global.isEscape()) return;
        getReports().write(0, "WISCONSIN      ",new TabSetting(20),pnd_Ctr_Ra_Sv_Wi,new TabSetting(30),pnd_Ctr_Sra_Sv_Wi,new TabSetting(40),pnd_Ctr_Gra_Sv_Wi,new         //Natural: WRITE 'WISCONSIN      ' 20T #CTR-RA-SV-WI 30T #CTR-SRA-SV-WI 40T #CTR-GRA-SV-WI 50T #CTR-GSRA-SV-WI
            TabSetting(50),pnd_Ctr_Gsra_Sv_Wi);
        if (Global.isEscape()) return;
        getReports().write(0, "NEW YORK (SUNY)",new TabSetting(50),pnd_Ctr_Gsra_Sv_Suny);                                                                                 //Natural: WRITE 'NEW YORK (SUNY)' 50T #CTR-GSRA-SV-SUNY
        if (Global.isEscape()) return;
        getReports().write(0, "NJABP          ",new TabSetting(40),pnd_Ctr_Gra_Sv_Njabp);                                                                                 //Natural: WRITE 'NJABP          ' 40T #CTR-GRA-SV-NJABP
        if (Global.isEscape()) return;
        //*  WRITE-STATISTICS-REPORT
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Input_Record_Pnd_Pin_A12IsBreak = pnd_Input_Record_Pnd_Pin_A12.isBreak(endOfData);
        if (condition(pnd_Input_Record_Pnd_Pin_A12IsBreak))
        {
            pnd_Ctr_Total_Contracts.compute(new ComputeParameters(false, pnd_Ctr_Total_Contracts), pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_St().add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Rp()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Sv()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_St()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Rp()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Sv()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_St()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Sv()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_St()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Sv()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Nj()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Nj()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nj()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Nj()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Pa()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Pa()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Pa()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Pa()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ct()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ct()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ct()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ct()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Tx()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Tx()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Tx()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Tx()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Mo()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Va()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Va()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Va()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Va()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mi()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Mo()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Mo()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mo()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Fl()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Fl()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Fl()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Fl()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ok()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ok()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ok()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ok()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Or()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Or()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Or()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Or()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Sc()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Sc()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Sc()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Sc()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Wi()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Wi()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Wi()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Wi()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Mt()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Mt()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Mt()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mt()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ne()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ne()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ne()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ne()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ar()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Hi()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ky()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nc()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Tn()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ut()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ut()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Vt()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Vt()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Pr()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Pr()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Njabp()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Nh()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Nh()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nh()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Nh()).add(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Suny())); //Natural: ASSIGN #CTR-TOTAL-CONTRACTS := #CTRS.#CTR-RA-ST + #CTRS.#CTR-RA-RP + #CTRS.#CTR-RA-SV + #CTRS.#CTR-SR-ST + #CTRS.#CTR-SR-RP + #CTRS.#CTR-SR-SV + #CTRS.#CTR-GR-ST + #CTRS.#CTR-GR-SV + #CTRS.#CTR-GS-ST + #CTRS.#CTR-GS-SV + #CTRS.#CTR-RA-NJ + #CTRS.#CTR-SR-NJ + #CTRS.#CTR-GR-NJ + #CTRS.#CTR-GS-NJ + #CTRS.#CTR-RA-PA + #CTRS.#CTR-SR-PA + #CTRS.#CTR-GR-PA + #CTRS.#CTR-GS-PA + #CTRS.#CTR-RA-CT + #CTRS.#CTR-SR-CT + #CTRS.#CTR-GR-CT + #CTRS.#CTR-GS-CT + #CTRS.#CTR-RA-TX + #CTRS.#CTR-SR-TX + #CTRS.#CTR-GS-TX + #CTRS.#CTR-GR-TX + #CTRS.#CTR-GR-MO + #CTRS.#CTR-RA-VA + #CTRS.#CTR-SR-VA + #CTRS.#CTR-GR-VA + #CTRS.#CTR-GS-VA + #CTRS.#CTR-GS-MI + #CTRS.#CTR-RA-MO + #CTRS.#CTR-SR-MO + #CTRS.#CTR-GS-MO + #CTRS.#CTR-RA-FL + #CTRS.#CTR-SR-FL + #CTRS.#CTR-GR-FL + #CTRS.#CTR-GS-FL + #CTRS.#CTR-RA-OK + #CTRS.#CTR-SR-OK + #CTRS.#CTR-GR-OK + #CTRS.#CTR-GS-OK + #CTRS.#CTR-RA-OR + #CTRS.#CTR-SR-OR + #CTRS.#CTR-GR-OR + #CTRS.#CTR-GS-OR + #CTRS.#CTR-RA-SC + #CTRS.#CTR-SR-SC + #CTRS.#CTR-GR-SC + #CTRS.#CTR-GS-SC + #CTRS.#CTR-RA-WI + #CTRS.#CTR-SR-WI + #CTRS.#CTR-GR-WI + #CTRS.#CTR-GS-WI + #CTRS.#CTR-RA-MT + #CTRS.#CTR-SR-MT + #CTRS.#CTR-GR-MT + #CTRS.#CTR-GS-MT + #CTRS.#CTR-RA-NE + #CTRS.#CTR-SR-NE + #CTRS.#CTR-GR-NE + #CTRS.#CTR-GS-NE + #CTRS.#CTR-GS-AR + #CTRS.#CTR-GS-HI + #CTRS.#CTR-GS-KY + #CTRS.#CTR-GR-NC + #CTRS.#CTR-GS-TN + #CTRS.#CTR-RA-UT + #CTRS.#CTR-SR-UT + #CTRS.#CTR-SR-VT + #CTRS.#CTR-GS-VT + #CTRS.#CTR-RA-PR + #CTRS.#CTR-GR-PR + #CTRS.#CTR-GR-NJABP + #CTRS.#CTR-RA-NH + #CTRS.#CTR-SR-NH + #CTRS.#CTR-GR-NH + #CTRS.#CTR-GS-NH + #CTRS.#CTR-GS-SUNY
            if (condition(pnd_Ctr_Total_Contracts.greater(getZero())))                                                                                                    //Natural: IF #CTR-TOTAL-CONTRACTS GT 0
            {
                //*      PERFORM CHECK-CPM-PROFILE
                //*      MOVE #PREV-PIN                   TO #OUTPUT.#PIN  /* PINEXP  >>>
                if (condition(pnd_Prev_Pin_N12.greaterOrEqual(new DbsDecimal("100000000000"))))                                                                           //Natural: IF #PREV-PIN-N12 GE 100000000000
                {
                    pdaRida410.getPnd_Output_Pnd_Pin_A12().setValue(pnd_Prev_Pin_N12);                                                                                    //Natural: MOVE #PREV-PIN-N12 TO #OUTPUT.#PIN-A12
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaRida410.getPnd_Output_Pnd_Pin_N7().setValue(pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7);                                                                     //Natural: MOVE #PREV-PIN-N7 TO #OUTPUT.#PIN-N7
                    pdaRida410.getPnd_Output_Pnd_Pin_A5().setValue("     ");                                                                                              //Natural: MOVE '     ' TO #OUTPUT.#PIN-A5
                    //*  PINEXP <<<
                }                                                                                                                                                         //Natural: END-IF
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_St().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_St());                                                                //Natural: MOVE #CTRS.#CTR-RA-ST TO #OUTPUT.#CTR-RA-ST
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Rp().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Rp());                                                                //Natural: MOVE #CTRS.#CTR-RA-RP TO #OUTPUT.#CTR-RA-RP
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Sv().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Sv());                                                                //Natural: MOVE #CTRS.#CTR-RA-SV TO #OUTPUT.#CTR-RA-SV
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_St().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_St());                                                                //Natural: MOVE #CTRS.#CTR-SR-ST TO #OUTPUT.#CTR-SR-ST
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Rp().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Rp());                                                                //Natural: MOVE #CTRS.#CTR-SR-RP TO #OUTPUT.#CTR-SR-RP
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Sv().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Sv());                                                                //Natural: MOVE #CTRS.#CTR-SR-SV TO #OUTPUT.#CTR-SR-SV
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_St().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_St());                                                                //Natural: MOVE #CTRS.#CTR-GR-ST TO #OUTPUT.#CTR-GR-ST
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Sv().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Sv());                                                                //Natural: MOVE #CTRS.#CTR-GR-SV TO #OUTPUT.#CTR-GR-SV
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_St().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_St());                                                                //Natural: MOVE #CTRS.#CTR-GS-ST TO #OUTPUT.#CTR-GS-ST
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Sv().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Sv());                                                                //Natural: MOVE #CTRS.#CTR-GS-SV TO #OUTPUT.#CTR-GS-SV
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Nj().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Nj());                                                                //Natural: MOVE #CTRS.#CTR-RA-NJ TO #OUTPUT.#CTR-RA-NJ
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Nj().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Nj());                                                                //Natural: MOVE #CTRS.#CTR-SR-NJ TO #OUTPUT.#CTR-SR-NJ
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Nj().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nj());                                                                //Natural: MOVE #CTRS.#CTR-GR-NJ TO #OUTPUT.#CTR-GR-NJ
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Nj().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Nj());                                                                //Natural: MOVE #CTRS.#CTR-GS-NJ TO #OUTPUT.#CTR-GS-NJ
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Pa().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Pa());                                                                //Natural: MOVE #CTRS.#CTR-RA-PA TO #OUTPUT.#CTR-RA-PA
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Pa().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Pa());                                                                //Natural: MOVE #CTRS.#CTR-SR-PA TO #OUTPUT.#CTR-SR-PA
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Pa().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Pa());                                                                //Natural: MOVE #CTRS.#CTR-GR-PA TO #OUTPUT.#CTR-GR-PA
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Pa().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Pa());                                                                //Natural: MOVE #CTRS.#CTR-GS-PA TO #OUTPUT.#CTR-GS-PA
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Ct().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ct());                                                                //Natural: MOVE #CTRS.#CTR-RA-CT TO #OUTPUT.#CTR-RA-CT
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Ct().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ct());                                                                //Natural: MOVE #CTRS.#CTR-SR-CT TO #OUTPUT.#CTR-SR-CT
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Ct().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ct());                                                                //Natural: MOVE #CTRS.#CTR-GR-CT TO #OUTPUT.#CTR-GR-CT
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Ct().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ct());                                                                //Natural: MOVE #CTRS.#CTR-GS-CT TO #OUTPUT.#CTR-GS-CT
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Tx().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Tx());                                                                //Natural: MOVE #CTRS.#CTR-RA-TX TO #OUTPUT.#CTR-RA-TX
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Tx().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Tx());                                                                //Natural: MOVE #CTRS.#CTR-SR-TX TO #OUTPUT.#CTR-SR-TX
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Tx().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Tx());                                                                //Natural: MOVE #CTRS.#CTR-GR-TX TO #OUTPUT.#CTR-GR-TX
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Tx().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Tx());                                                                //Natural: MOVE #CTRS.#CTR-GS-TX TO #OUTPUT.#CTR-GS-TX
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Va().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Va());                                                                //Natural: MOVE #CTRS.#CTR-RA-VA TO #OUTPUT.#CTR-RA-VA
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Va().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Va());                                                                //Natural: MOVE #CTRS.#CTR-SR-VA TO #OUTPUT.#CTR-SR-VA
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Va().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Va());                                                                //Natural: MOVE #CTRS.#CTR-GR-VA TO #OUTPUT.#CTR-GR-VA
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Va().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Va());                                                                //Natural: MOVE #CTRS.#CTR-GS-VA TO #OUTPUT.#CTR-GS-VA
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Mi().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mi());                                                                //Natural: MOVE #CTRS.#CTR-GS-MI TO #OUTPUT.#CTR-GS-MI
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Mo().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Mo());                                                                //Natural: MOVE #CTRS.#CTR-RA-MO TO #OUTPUT.#CTR-RA-MO
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Mo().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Mo());                                                                //Natural: MOVE #CTRS.#CTR-SR-MO TO #OUTPUT.#CTR-SR-MO
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Mo().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Mo());                                                                //Natural: MOVE #CTRS.#CTR-GR-MO TO #OUTPUT.#CTR-GR-MO
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Mo().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mo());                                                                //Natural: MOVE #CTRS.#CTR-GS-MO TO #OUTPUT.#CTR-GS-MO
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Fl().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Fl());                                                                //Natural: MOVE #CTRS.#CTR-RA-FL TO #OUTPUT.#CTR-RA-FL
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Fl().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Fl());                                                                //Natural: MOVE #CTRS.#CTR-SR-FL TO #OUTPUT.#CTR-SR-FL
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Fl().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Fl());                                                                //Natural: MOVE #CTRS.#CTR-GR-FL TO #OUTPUT.#CTR-GR-FL
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Fl().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Fl());                                                                //Natural: MOVE #CTRS.#CTR-GS-FL TO #OUTPUT.#CTR-GS-FL
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Ok().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ok());                                                                //Natural: MOVE #CTRS.#CTR-RA-OK TO #OUTPUT.#CTR-RA-OK
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Ok().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ok());                                                                //Natural: MOVE #CTRS.#CTR-SR-OK TO #OUTPUT.#CTR-SR-OK
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Ok().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ok());                                                                //Natural: MOVE #CTRS.#CTR-GR-OK TO #OUTPUT.#CTR-GR-OK
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Ok().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ok());                                                                //Natural: MOVE #CTRS.#CTR-GS-OK TO #OUTPUT.#CTR-GS-OK
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Or().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Or());                                                                //Natural: MOVE #CTRS.#CTR-RA-OR TO #OUTPUT.#CTR-RA-OR
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Or().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Or());                                                                //Natural: MOVE #CTRS.#CTR-SR-OR TO #OUTPUT.#CTR-SR-OR
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Or().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Or());                                                                //Natural: MOVE #CTRS.#CTR-GR-OR TO #OUTPUT.#CTR-GR-OR
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Or().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Or());                                                                //Natural: MOVE #CTRS.#CTR-GS-OR TO #OUTPUT.#CTR-GS-OR
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Wi().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Wi());                                                                //Natural: MOVE #CTRS.#CTR-RA-WI TO #OUTPUT.#CTR-RA-WI
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Wi().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Wi());                                                                //Natural: MOVE #CTRS.#CTR-SR-WI TO #OUTPUT.#CTR-SR-WI
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Wi().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Wi());                                                                //Natural: MOVE #CTRS.#CTR-GR-WI TO #OUTPUT.#CTR-GR-WI
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Wi().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Wi());                                                                //Natural: MOVE #CTRS.#CTR-GS-WI TO #OUTPUT.#CTR-GS-WI
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Sc().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Sc());                                                                //Natural: MOVE #CTRS.#CTR-RA-SC TO #OUTPUT.#CTR-RA-SC
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Sc().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Sc());                                                                //Natural: MOVE #CTRS.#CTR-SR-SC TO #OUTPUT.#CTR-SR-SC
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Sc().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Sc());                                                                //Natural: MOVE #CTRS.#CTR-GR-SC TO #OUTPUT.#CTR-GR-SC
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Sc().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Sc());                                                                //Natural: MOVE #CTRS.#CTR-GS-SC TO #OUTPUT.#CTR-GS-SC
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Ne().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ne());                                                                //Natural: MOVE #CTRS.#CTR-RA-NE TO #OUTPUT.#CTR-RA-NE
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Ne().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ne());                                                                //Natural: MOVE #CTRS.#CTR-SR-NE TO #OUTPUT.#CTR-SR-NE
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Ne().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Ne());                                                                //Natural: MOVE #CTRS.#CTR-GR-NE TO #OUTPUT.#CTR-GR-NE
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Ne().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ne());                                                                //Natural: MOVE #CTRS.#CTR-GS-NE TO #OUTPUT.#CTR-GS-NE
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Mt().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Mt());                                                                //Natural: MOVE #CTRS.#CTR-RA-MT TO #OUTPUT.#CTR-RA-MT
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Mt().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Mt());                                                                //Natural: MOVE #CTRS.#CTR-SR-MT TO #OUTPUT.#CTR-SR-MT
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Mt().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Mt());                                                                //Natural: MOVE #CTRS.#CTR-GR-MT TO #OUTPUT.#CTR-GR-MT
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Mt().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Mt());                                                                //Natural: MOVE #CTRS.#CTR-GS-MT TO #OUTPUT.#CTR-GS-MT
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Ar().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ar());                                                                //Natural: MOVE #CTRS.#CTR-GS-AR TO #OUTPUT.#CTR-GS-AR
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Hi().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Hi());                                                                //Natural: MOVE #CTRS.#CTR-GS-HI TO #OUTPUT.#CTR-GS-HI
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Ky().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Ky());                                                                //Natural: MOVE #CTRS.#CTR-GS-KY TO #OUTPUT.#CTR-GS-KY
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Nc().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nc());                                                                //Natural: MOVE #CTRS.#CTR-GR-NC TO #OUTPUT.#CTR-GR-NC
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Tn().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Tn());                                                                //Natural: MOVE #CTRS.#CTR-GS-TN TO #OUTPUT.#CTR-GS-TN
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Ut().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Ut());                                                                //Natural: MOVE #CTRS.#CTR-RA-UT TO #OUTPUT.#CTR-RA-UT
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Ut().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Ut());                                                                //Natural: MOVE #CTRS.#CTR-SR-UT TO #OUTPUT.#CTR-SR-UT
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Vt().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Vt());                                                                //Natural: MOVE #CTRS.#CTR-SR-VT TO #OUTPUT.#CTR-SR-VT
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Vt().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Vt());                                                                //Natural: MOVE #CTRS.#CTR-GS-VT TO #OUTPUT.#CTR-GS-VT
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Pr().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Pr());                                                                //Natural: MOVE #CTRS.#CTR-RA-PR TO #OUTPUT.#CTR-RA-PR
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Pr().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Pr());                                                                //Natural: MOVE #CTRS.#CTR-GR-PR TO #OUTPUT.#CTR-GR-PR
                pdaRida410.getPnd_Output_Pnd_Ctr_Ra_Nh().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Ra_Nh());                                                                //Natural: MOVE #CTRS.#CTR-RA-NH TO #OUTPUT.#CTR-RA-NH
                pdaRida410.getPnd_Output_Pnd_Ctr_Sr_Nh().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Sr_Nh());                                                                //Natural: MOVE #CTRS.#CTR-SR-NH TO #OUTPUT.#CTR-SR-NH
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Nh().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Nh());                                                                //Natural: MOVE #CTRS.#CTR-GR-NH TO #OUTPUT.#CTR-GR-NH
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Nh().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Nh());                                                                //Natural: MOVE #CTRS.#CTR-GS-NH TO #OUTPUT.#CTR-GS-NH
                pdaRida410.getPnd_Output_Pnd_Ctr_Gr_Njabp().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gr_Njabp());                                                          //Natural: MOVE #CTRS.#CTR-GR-NJABP TO #OUTPUT.#CTR-GR-NJABP
                pdaRida410.getPnd_Output_Pnd_Ctr_Gs_Suny().setValue(pdaRida410.getPnd_Ctrs_Pnd_Ctr_Gs_Suny());                                                            //Natural: MOVE #CTRS.#CTR-GS-SUNY TO #OUTPUT.#CTR-GS-SUNY
                pdaRida410.getPnd_Output_Pnd_Letter_Type().setValue(pnd_Prev_Cover);                                                                                      //Natural: MOVE #PREV-COVER TO #OUTPUT.#LETTER-TYPE
                pdaRida410.getPnd_Output_Pnd_Level().setValue(pnd_Prev_Level);                                                                                            //Natural: MOVE #PREV-LEVEL TO #OUTPUT.#LEVEL
                pdaRida410.getPnd_Output_Pnd_Institution_Name().setValue(pnd_Prev_Inst_Name);                                                                             //Natural: MOVE #PREV-INST-NAME TO #OUTPUT.#INSTITUTION-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-DATA-TO-FILE
                sub_Write_Data_To_File();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pdaRida410.getPnd_Ctrs().reset();                                                                                                                             //Natural: RESET #CTRS #CTR-TOTAL-CONTRACTS #NJABP #SUNY
            pnd_Ctr_Total_Contracts.reset();
            pdaRida410.getPnd_Miscellaneous_Pnd_Njabp().reset();
            pdaRida410.getPnd_Miscellaneous_Pnd_Suny().reset();
            pdaRida410.getPnd_Miscellaneous_Pnd_State_Ra_Sv().getValue("*").reset();                                                                                      //Natural: RESET #STATE-RA-SV ( * ) #STATE-SRA-SV ( * ) #STATE-GRA-SV ( * ) #STATE-GSRA-SV ( * )
            pdaRida410.getPnd_Miscellaneous_Pnd_State_Sra_Sv().getValue("*").reset();
            pdaRida410.getPnd_Miscellaneous_Pnd_State_Gra_Sv().getValue("*").reset();
            pdaRida410.getPnd_Miscellaneous_Pnd_State_Gsra_Sv().getValue("*").reset();
            pdaRida410.getPnd_Output().resetInitial();                                                                                                                    //Natural: RESET INITIAL #OUTPUT #K
            pnd_K.resetInitial();
            pnd_Break.setValue(true);                                                                                                                                     //Natural: MOVE TRUE TO #BREAK
            pnd_Prev_Pin_N12.setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-N12 TO #PREV-PIN-N12
            pnd_Prev_Cover.setValue(pnd_Input_Record_Pnd_Cover_Letter_Ind);                                                                                               //Natural: MOVE #INPUT-RECORD.#COVER-LETTER-IND TO #PREV-COVER
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Inst_Name);                                                                                                  //Natural: MOVE #INPUT-RECORD.#INST-NAME TO #PREV-INST-NAME
            pnd_Prev_Level.setValue(pnd_Input_Record_Pnd_Level);                                                                                                          //Natural: MOVE #INPUT-RECORD.#LEVEL TO #PREV-LEVEL
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=120 PS=60");
        Global.format(2, "LS=120 PS=60");
        Global.format(0, "LS=80 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(37),"2006 TIAA ACCESS RIDER MAILING",new 
            TabSetting(84),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(43),"EXCEPTIONS LIST",new TabSetting(84),"TIME:",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,new TabSetting(84),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new TabSetting(2),"PIN          TIAA NBR   STATE   REMARKS",NEWLINE,new 
            TabSetting(2),"------------------------------------------------------------",NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(34),"2006 TIAA ACCESS RIDER MAILING",new 
            TabSetting(84),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(37),"MA/OR EXCLUDED CONTRACTS",new TabSetting(84),"TIME:",Global.getTIME(), new 
            ReportEditMask ("XXXXXXXX"),NEWLINE,new TabSetting(84),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(2),NEWLINE,NEWLINE,new TabSetting(2),"PIN         ",new 
            TabSetting(16),"TIAA #  ",new TabSetting(26),"ISSUE-STATE",NEWLINE,new TabSetting(2),"------------",new TabSetting(16),"--------",new TabSetting(26),
            "-----------",NEWLINE,NEWLINE);
    }
}
