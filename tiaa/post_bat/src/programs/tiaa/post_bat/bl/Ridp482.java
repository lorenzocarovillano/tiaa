/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:02 PM
**        * FROM NATURAL PROGRAM : Ridp482
************************************************************
**        * FILE NAME            : Ridp482.java
**        * CLASS NAME           : Ridp482
**        * INSTANCE NAME        : Ridp482
************************************************************
************************************************************************
** PROGRAM     : RIDP482                                              **
** AUTHOR      : JOANNES C. AVE                                       **
** DATE        : 04/07/2008                                           **
** DESCRIPTION : READS THE RIDP480 OUTPUT FILE, GETS NAME AND         **
**               ADDRESS DETAILS BY CALLING THE SUBPROGRAM PSTN3029,  **
**               ASSIGN A RID NUMBER BY CALLING THE SUBPROGRAM        **
**               PSTN9650, WRITE COMPUSET STATEMENTS TO GENERATE THE  **
**               ADDRESS PAGE AND COVER LETTER, AND PRINT REPORTS     **
************************************************************************
** HISTORY:                                                           **
** 04/07/2008  : INITIAL IMPLEMENTATION                               **
** 03/24/2015  : COR/NAAD SUNSET - REMOVE ALL REFERENCES TO COR/NAAD  **
**               AND CALL THE MDMN MODULES INSTEAD FOR PARTICIPANT    **
**               AND CONTRACT INFORMATION                             **
** 07/15/2016  : REMOVE CALL TO MDM FOR NAME AND ADDRESS. CCP WILL    **
**               OBTAIN THIS FROM MDM AS PART OF DCS ELIMINAION       **
**             : REPLACE POST CALL TO ASSIGN RID#. REPLACE IT WITH    **
**               JUST A COUNTER                                       **
** 05/29/2017  : PIN EXPANSION                             PINEXP     **
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridp482 extends BLNatBase
{
    // Data Areas
    private LdaRidl482 ldaRidl482;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ph_Data;
    private DbsField pnd_Ph_Data_Pnd_Pin_Number;
    private DbsField pnd_Ph_Data_Pnd_Letter_Type;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gra_Tiaa_A;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gra_Cref_A;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gsra_Tiaa_A;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gsra_Cref_A;
    private DbsField pnd_Ph_Data_Pnd_Package_Code;
    private DbsField pnd_Ph_Data_Pnd_Tiaa_Contracts;

    private DbsGroup pnd_Ph_Data__R_Field_1;
    private DbsField pnd_Ph_Data_Pnd_Contract1;
    private DbsField pnd_Ph_Data_Pnd_Cref_Contracts;
    private DbsField pnd_Ph_Data_Pnd_Institution_Name;
    private DbsField pnd_Current_Date;

    private DbsGroup pnd_Current_Date__R_Field_2;
    private DbsField pnd_Current_Date_Pnd_Current_Year;
    private DbsField pnd_Current_Date_Pnd_Current_Month;
    private DbsField pnd_Current_Date_Pnd_Current_Day;
    private DbsField pnd_Rdr_Date;

    private DbsGroup pnd_Rdr_Date__R_Field_3;
    private DbsField pnd_Rdr_Date_Pnd_Rdr_Month;
    private DbsField pnd_Rdr_Date_Pnd_Rdr_Year;

    private DbsGroup counters;
    private DbsField counters_Pnd_Cnt_Record_Read;
    private DbsField counters_Pnd_Cnt_Record_Stored;
    private DbsField counters_Pnd_Cnt_Record_Addr_B;
    private DbsField counters_Pnd_Cnt_Record_Addr_C;
    private DbsField counters_Pnd_Cnt_Record_Addr_F;
    private DbsField counters_Pnd_Cnt_Record_Addr_U;
    private DbsField counters_Pnd_Cnt_Ph_Deceased;
    private DbsField pnd_Address_Blank;
    private DbsField pnd_Blank;
    private DbsField pnd_Batch_Id_Found;
    private DbsField pnd_First_Pst_Nbr_Allocated;
    private DbsField pnd_Error_Ind;

    private DbsGroup control_Variables;
    private DbsField control_Variables_Run_Type;
    private DbsField control_Variables_Batch_Number;
    private DbsField control_Variables_Current_Pst_Rqst_Id;

    private DbsGroup error_Handler_Fields;
    private DbsField error_Handler_Fields_Pnd_Error_Nr;
    private DbsField error_Handler_Fields_Pnd_Error_Line;
    private DbsField error_Handler_Fields_Pnd_Error_Status;
    private DbsField error_Handler_Fields_Pnd_Error_Program;
    private DbsField error_Handler_Fields_Pnd_Error_Level;
    private DbsField error_Handler_Fields_Pnd_Error_Appl;
    private DbsField pnd_Msg_Parts;

    private DbsGroup pnd_Package_Variables;
    private DbsField pnd_Package_Variables_Pnd_Package_Cdes;
    private DbsField pnd_Package_Variables_Pnd_Cnt_Record_By_Package;
    private DbsField pnd_Package_Variables_Pnd_First_Pst;
    private DbsField pnd_Package_Variables_Pnd_Last_Pst;
    private DbsField pnd_Prev_Package_Code;
    private DbsField pnd_Prev_Pst;
    private DbsField pnd_K;
    private DbsField pnd_First_Record;
    private DbsField pnd_First_Bad_Record;
    private DbsField pnd_Cnt_Et;

    private DbsGroup pnd_Package_Table;
    private DbsField pnd_Package_Table_Pnd_Package_Code;
    private DbsField pnd_Package_Table_Pnd_Bin_Content;
    private DbsField pnd_Package_Table_Pnd_File_Desc;
    private DbsField pnd_Ndx;
    private DbsField pnd_Deceased;
    private DbsField pnd_Rem;
    private DbsField pnd_Bad_Address;
    private DbsField pnd_Rid_Ctr;
    private DbsField pnd_Rid_Nbr;

    private DbsGroup pnd_Rid_Nbr__R_Field_4;
    private DbsField pnd_Rid_Nbr_Pnd_Rid_Nbr_Prefix;
    private DbsField pnd_Rid_Nbr_Pnd_Rid_Nbr_A;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaRidl482 = new LdaRidl482();
        registerRecord(ldaRidl482);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ph_Data = localVariables.newGroupInRecord("pnd_Ph_Data", "#PH-DATA");
        pnd_Ph_Data_Pnd_Pin_Number = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);
        pnd_Ph_Data_Pnd_Letter_Type = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Ph_Data_Pnd_Ctr_Gra_Tiaa_A = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gra_Tiaa_A", "#CTR-GRA-TIAA-A", FieldType.STRING, 1);
        pnd_Ph_Data_Pnd_Ctr_Gra_Cref_A = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gra_Cref_A", "#CTR-GRA-CREF-A", FieldType.STRING, 1);
        pnd_Ph_Data_Pnd_Ctr_Gsra_Tiaa_A = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gsra_Tiaa_A", "#CTR-GSRA-TIAA-A", FieldType.STRING, 1);
        pnd_Ph_Data_Pnd_Ctr_Gsra_Cref_A = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gsra_Cref_A", "#CTR-GSRA-CREF-A", FieldType.STRING, 1);
        pnd_Ph_Data_Pnd_Package_Code = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Ph_Data_Pnd_Tiaa_Contracts = pnd_Ph_Data.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Tiaa_Contracts", "#TIAA-CONTRACTS", FieldType.STRING, 8, new 
            DbsArrayController(1, 20));

        pnd_Ph_Data__R_Field_1 = pnd_Ph_Data.newGroupInGroup("pnd_Ph_Data__R_Field_1", "REDEFINE", pnd_Ph_Data_Pnd_Tiaa_Contracts);
        pnd_Ph_Data_Pnd_Contract1 = pnd_Ph_Data__R_Field_1.newFieldInGroup("pnd_Ph_Data_Pnd_Contract1", "#CONTRACT1", FieldType.STRING, 8);
        pnd_Ph_Data_Pnd_Cref_Contracts = pnd_Ph_Data.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Cref_Contracts", "#CREF-CONTRACTS", FieldType.STRING, 8, new 
            DbsArrayController(1, 20));
        pnd_Ph_Data_Pnd_Institution_Name = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 120);
        pnd_Current_Date = localVariables.newFieldInRecord("pnd_Current_Date", "#CURRENT-DATE", FieldType.NUMERIC, 8);

        pnd_Current_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Current_Date__R_Field_2", "REDEFINE", pnd_Current_Date);
        pnd_Current_Date_Pnd_Current_Year = pnd_Current_Date__R_Field_2.newFieldInGroup("pnd_Current_Date_Pnd_Current_Year", "#CURRENT-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Current_Date_Pnd_Current_Month = pnd_Current_Date__R_Field_2.newFieldInGroup("pnd_Current_Date_Pnd_Current_Month", "#CURRENT-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Current_Date_Pnd_Current_Day = pnd_Current_Date__R_Field_2.newFieldInGroup("pnd_Current_Date_Pnd_Current_Day", "#CURRENT-DAY", FieldType.NUMERIC, 
            2);
        pnd_Rdr_Date = localVariables.newFieldInRecord("pnd_Rdr_Date", "#RDR-DATE", FieldType.NUMERIC, 6);

        pnd_Rdr_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Rdr_Date__R_Field_3", "REDEFINE", pnd_Rdr_Date);
        pnd_Rdr_Date_Pnd_Rdr_Month = pnd_Rdr_Date__R_Field_3.newFieldInGroup("pnd_Rdr_Date_Pnd_Rdr_Month", "#RDR-MONTH", FieldType.NUMERIC, 2);
        pnd_Rdr_Date_Pnd_Rdr_Year = pnd_Rdr_Date__R_Field_3.newFieldInGroup("pnd_Rdr_Date_Pnd_Rdr_Year", "#RDR-YEAR", FieldType.NUMERIC, 4);

        counters = localVariables.newGroupInRecord("counters", "COUNTERS");
        counters_Pnd_Cnt_Record_Read = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Read", "#CNT-RECORD-READ", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Stored = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Stored", "#CNT-RECORD-STORED", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Addr_B = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Addr_B", "#CNT-RECORD-ADDR-B", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Addr_C = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Addr_C", "#CNT-RECORD-ADDR-C", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Addr_F = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Addr_F", "#CNT-RECORD-ADDR-F", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Addr_U = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Addr_U", "#CNT-RECORD-ADDR-U", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Ph_Deceased = counters.newFieldInGroup("counters_Pnd_Cnt_Ph_Deceased", "#CNT-PH-DECEASED", FieldType.NUMERIC, 7);
        pnd_Address_Blank = localVariables.newFieldInRecord("pnd_Address_Blank", "#ADDRESS-BLANK", FieldType.BOOLEAN, 1);
        pnd_Blank = localVariables.newFieldInRecord("pnd_Blank", "#BLANK", FieldType.STRING, 1);
        pnd_Batch_Id_Found = localVariables.newFieldInRecord("pnd_Batch_Id_Found", "#BATCH-ID-FOUND", FieldType.BOOLEAN, 1);
        pnd_First_Pst_Nbr_Allocated = localVariables.newFieldInRecord("pnd_First_Pst_Nbr_Allocated", "#FIRST-PST-NBR-ALLOCATED", FieldType.BOOLEAN, 1);
        pnd_Error_Ind = localVariables.newFieldInRecord("pnd_Error_Ind", "#ERROR-IND", FieldType.STRING, 1);

        control_Variables = localVariables.newGroupInRecord("control_Variables", "CONTROL-VARIABLES");
        control_Variables_Run_Type = control_Variables.newFieldInGroup("control_Variables_Run_Type", "RUN-TYPE", FieldType.PACKED_DECIMAL, 1);
        control_Variables_Batch_Number = control_Variables.newFieldInGroup("control_Variables_Batch_Number", "BATCH-NUMBER", FieldType.STRING, 11);
        control_Variables_Current_Pst_Rqst_Id = control_Variables.newFieldInGroup("control_Variables_Current_Pst_Rqst_Id", "CURRENT-PST-RQST-ID", FieldType.STRING, 
            11);

        error_Handler_Fields = localVariables.newGroupInRecord("error_Handler_Fields", "ERROR-HANDLER-FIELDS");
        error_Handler_Fields_Pnd_Error_Nr = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Nr", "#ERROR-NR", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Line = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Line", "#ERROR-LINE", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Status = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Status", "#ERROR-STATUS", FieldType.STRING, 
            1);
        error_Handler_Fields_Pnd_Error_Program = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Program", "#ERROR-PROGRAM", FieldType.STRING, 
            8);
        error_Handler_Fields_Pnd_Error_Level = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Level", "#ERROR-LEVEL", FieldType.NUMERIC, 
            2);
        error_Handler_Fields_Pnd_Error_Appl = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Appl", "#ERROR-APPL", FieldType.STRING, 
            8);
        pnd_Msg_Parts = localVariables.newFieldArrayInRecord("pnd_Msg_Parts", "#MSG-PARTS", FieldType.STRING, 80, new DbsArrayController(1, 10));

        pnd_Package_Variables = localVariables.newGroupInRecord("pnd_Package_Variables", "#PACKAGE-VARIABLES");
        pnd_Package_Variables_Pnd_Package_Cdes = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_Package_Cdes", "#PACKAGE-CDES", FieldType.STRING, 
            8);
        pnd_Package_Variables_Pnd_Cnt_Record_By_Package = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_Cnt_Record_By_Package", "#CNT-RECORD-BY-PACKAGE", 
            FieldType.NUMERIC, 6);
        pnd_Package_Variables_Pnd_First_Pst = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_First_Pst", "#FIRST-PST", FieldType.STRING, 
            11);
        pnd_Package_Variables_Pnd_Last_Pst = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_Last_Pst", "#LAST-PST", FieldType.STRING, 
            11);
        pnd_Prev_Package_Code = localVariables.newFieldInRecord("pnd_Prev_Package_Code", "#PREV-PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Prev_Pst = localVariables.newFieldInRecord("pnd_Prev_Pst", "#PREV-PST", FieldType.STRING, 11);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_First_Record = localVariables.newFieldInRecord("pnd_First_Record", "#FIRST-RECORD", FieldType.BOOLEAN, 1);
        pnd_First_Bad_Record = localVariables.newFieldInRecord("pnd_First_Bad_Record", "#FIRST-BAD-RECORD", FieldType.BOOLEAN, 1);
        pnd_Cnt_Et = localVariables.newFieldInRecord("pnd_Cnt_Et", "#CNT-ET", FieldType.NUMERIC, 3);

        pnd_Package_Table = localVariables.newGroupArrayInRecord("pnd_Package_Table", "#PACKAGE-TABLE", new DbsArrayController(1, 2));
        pnd_Package_Table_Pnd_Package_Code = pnd_Package_Table.newFieldInGroup("pnd_Package_Table_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 
            8);
        pnd_Package_Table_Pnd_Bin_Content = pnd_Package_Table.newFieldInGroup("pnd_Package_Table_Pnd_Bin_Content", "#BIN-CONTENT", FieldType.STRING, 67);
        pnd_Package_Table_Pnd_File_Desc = pnd_Package_Table.newFieldInGroup("pnd_Package_Table_Pnd_File_Desc", "#FILE-DESC", FieldType.STRING, 60);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 2);
        pnd_Deceased = localVariables.newFieldInRecord("pnd_Deceased", "#DECEASED", FieldType.BOOLEAN, 1);
        pnd_Rem = localVariables.newFieldInRecord("pnd_Rem", "#REM", FieldType.STRING, 2);
        pnd_Bad_Address = localVariables.newFieldInRecord("pnd_Bad_Address", "#BAD-ADDRESS", FieldType.BOOLEAN, 1);
        pnd_Rid_Ctr = localVariables.newFieldInRecord("pnd_Rid_Ctr", "#RID-CTR", FieldType.NUMERIC, 8);
        pnd_Rid_Nbr = localVariables.newFieldInRecord("pnd_Rid_Nbr", "#RID-NBR", FieldType.NUMERIC, 9);

        pnd_Rid_Nbr__R_Field_4 = localVariables.newGroupInRecord("pnd_Rid_Nbr__R_Field_4", "REDEFINE", pnd_Rid_Nbr);
        pnd_Rid_Nbr_Pnd_Rid_Nbr_Prefix = pnd_Rid_Nbr__R_Field_4.newFieldInGroup("pnd_Rid_Nbr_Pnd_Rid_Nbr_Prefix", "#RID-NBR-PREFIX", FieldType.STRING, 
            1);
        pnd_Rid_Nbr_Pnd_Rid_Nbr_A = pnd_Rid_Nbr__R_Field_4.newFieldInGroup("pnd_Rid_Nbr_Pnd_Rid_Nbr_A", "#RID-NBR-A", FieldType.STRING, 8);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaRidl482.initializeValues();

        localVariables.reset();
        pnd_Blank.setInitialValue(" ");
        pnd_Error_Ind.setInitialValue("E");
        error_Handler_Fields_Pnd_Error_Status.setInitialValue("O");
        error_Handler_Fields_Pnd_Error_Program.setInitialValue("PROGRAM");
        error_Handler_Fields_Pnd_Error_Appl.setInitialValue("RIDER");
        pnd_K.setInitialValue(1);
        pnd_First_Record.setInitialValue(true);
        pnd_First_Bad_Record.setInitialValue(true);
        pnd_Cnt_Et.setInitialValue(0);
        pnd_Package_Table_Pnd_Package_Code.getValue(1).setInitialValue("RDR2008U");
        pnd_Package_Table_Pnd_Package_Code.getValue(2).setInitialValue("RDR2008V");
        pnd_Package_Table_Pnd_Bin_Content.getValue(1).setInitialValue("INSTREAM TRIFOLDED");
        pnd_Package_Table_Pnd_Bin_Content.getValue(2).setInitialValue("INSTREAM FLATS");
        pnd_Package_Table_Pnd_File_Desc.getValue(1).setInitialValue("1-2 GRA CONTRACTS");
        pnd_Package_Table_Pnd_File_Desc.getValue(2).setInitialValue("3 OR MORE GRA CONTRACTS");
        pnd_Bad_Address.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridp482() throws Exception
    {
        super("Ridp482");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDP482", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60;//Natural: FORMAT LS = 132 PS = 60
        //*  ----------------------------------------------------------------------
        //*                      E R R O R    H A N D L I N G
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  GET RIDER DATE
        pnd_Current_Date.setValue(Global.getDATN());                                                                                                                      //Natural: ASSIGN #CURRENT-DATE = *DATN
        pnd_Rdr_Date_Pnd_Rdr_Year.setValue(pnd_Current_Date_Pnd_Current_Year);                                                                                            //Natural: ASSIGN #RDR-YEAR = #CURRENT-YEAR
        pnd_Rdr_Date_Pnd_Rdr_Month.setValue(pnd_Current_Date_Pnd_Current_Month);                                                                                          //Natural: ASSIGN #RDR-MONTH = #CURRENT-MONTH
        //*  FETCH RETURN 'MDMP0011' /* OPEN MQ  QUEUE           /* JCA20160715
        //*  WRITE REPORT HEADINGS
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 39T '2008 FORCED CASHOUT RIDER MAILING POST LOAD PROCESSING STATS' 118T 'DATE:' *DATU / 118T 'TIME:' *TIME ( EM = XXXXXXXX ) / 118T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT 'PROGRAM:' *PROGRAM 20T '2008 FORCED CASHOUT RIDER MAILING POST LOAD PROCESSING STATS' 84T 'DATE:' *DATU / 42T 'BAD ADDRESS REPORT' 84T 'TIME:' *TIME ( EM = XXXXXXXX ) / 84T 'PAGE:' 3X *PAGE-NUMBER ( 2 ) // '----------------------------------------------------------------------------------------------------------------------|' / '                                             GRA  GRA  GSR  GSR     ' '  PIN    NAME                                TIA  CRF  TIA  CRF           INSTITUTION NAME        ' '----------------------------------------------------------------------------------------------------------------------|' /
        //*  ----------------------------------------------------------------------
        //*                           M A I N    L O O P
        //*  ----------------------------------------------------------------------
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #PH-DATA
        while (condition(getWorkFiles().read(1, pnd_Ph_Data)))
        {
            counters_Pnd_Cnt_Record_Read.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CNT-RECORD-READ
            ldaRidl482.getPnd_Post_Data().reset();                                                                                                                        //Natural: RESET #POST-DATA
            ldaRidl482.getPnd_Post_Data().setValuesByName(pnd_Ph_Data);                                                                                                   //Natural: MOVE BY NAME #PH-DATA TO #POST-DATA
            ldaRidl482.getPnd_Post_Data_Pnd_Rider_Date().setValue(pnd_Rdr_Date);                                                                                          //Natural: ASSIGN #POST-DATA.#RIDER-DATE = #RDR-DATE
            //*    PERFORM GET-NAME-ADDRESS-DETAILS             /* JCA20160715
            //*    IF #BAD-ADDRESS                              /* JCA20160715
            //*      PERFORM WRITE-BAD-ADDRESS-REPORT
            //*    ELSE                                         /* JCA20160715
            //*      PERFORM STORE-COUNTS-BY-BIN                /* JCA20160715
                                                                                                                                                                          //Natural: PERFORM GET-PST-NUMBER
            sub_Get_Pst_Number();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* **   PERFORM DISPLAY-POST-DATA
            if (condition(pnd_First_Record.getBoolean()))                                                                                                                 //Natural: IF #FIRST-RECORD
            {
                pnd_Prev_Package_Code.setValue(ldaRidl482.getPnd_Post_Data_Pnd_Package_Code());                                                                           //Natural: ASSIGN #PREV-PACKAGE-CODE := #POST-DATA.#PACKAGE-CODE
                pnd_First_Record.setValue(false);                                                                                                                         //Natural: ASSIGN #FIRST-RECORD := FALSE
                pnd_Package_Variables_Pnd_First_Pst.setValue(ldaRidl482.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                              //Natural: ASSIGN #FIRST-PST := #POST-DATA.#PST-RQST-ID
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prev_Package_Code.equals(ldaRidl482.getPnd_Post_Data_Pnd_Package_Code())))                                                                  //Natural: IF #PREV-PACKAGE-CODE EQ #POST-DATA.#PACKAGE-CODE
            {
                pnd_Prev_Pst.setValue(ldaRidl482.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                                                     //Natural: ASSIGN #PREV-PST := #POST-DATA.#PST-RQST-ID
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Package_Variables_Pnd_Last_Pst.setValue(pnd_Prev_Pst);                                                                                                //Natural: ASSIGN #LAST-PST := #PREV-PST
                pnd_Package_Variables_Pnd_First_Pst.setValue(ldaRidl482.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                              //Natural: ASSIGN #FIRST-PST := #POST-DATA.#PST-RQST-ID
                pnd_Prev_Package_Code.setValue(ldaRidl482.getPnd_Post_Data_Pnd_Package_Code());                                                                           //Natural: ASSIGN #PREV-PACKAGE-CODE := #POST-DATA.#PACKAGE-CODE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM STORE-POST-DATA
            sub_Store_Post_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  END TRANSACTION
        pnd_Package_Variables_Pnd_Last_Pst.setValue(pnd_Prev_Pst);                                                                                                        //Natural: ASSIGN #LAST-PST := #PREV-PST
                                                                                                                                                                          //Natural: PERFORM PRINT-PROCESSING-REPORT
        sub_Print_Processing_Report();
        if (condition(Global.isEscape())) {return;}
        //*  FETCH RETURN 'MDMP0012'  /* CLOSE MQ QUEUE     /* JCA20160715
        //*  ----------------------------------------------------------------------
        //*                   E N D    O F    P R O C E S S I N G
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*                 S T A R T    O F   S U B R O U T I N E S
        //*  ----------------------------------------------------------------------
        //*  ------------------------------------------
        //*  DEFINE SUBROUTINE GET-NAME-ADDRESS-DETAILS
        //*  ------------------------------------------
        //*  CALL PSTN3029 AND USE RETURNED ADDRESS - NOT USED
        //*    RESET INITIAL #BAD-ADDRESS #MDMA100
        //*    PSTA3029.PIN-NBR := #POST-DATA.#PIN-NUMBER
        //*    CALLNAT 'PSTN3029' PSTA3029
        //*      PSTA3029-MAIL-ADDRSS
        //*      PSTA3029-PRMNNT-ADDRSS
        //*      DIALOG-INFO-SUB
        //*      MSG-INFO-SUB
        //*      PASS-SUB
        //*    IF NOT (PSTA3029-MAIL-ADDRSS.ADDRSS-LNE(*) NE ' ')
        //*      MOVE #POST-DATA.#PIN-NUMBER TO PSTA3049.PH-UNQUE-ID-NMBR
        //*      MOVE #CONTRACT1 TO PSTA3049.CNTRCT-NMBR
        //*      CALLNAT 'PSTN3049' PSTA3049      /* GET NEXT GOOD ADDRESS
        //*      IF PSTA3049.ADDRSS-LNE(*) <> ' '
        //*       #POST-DATA.#ADDRESS-LINE-TXT(*) := PSTA3049.ADDRSS-LNE(*)
        //*       #POST-DATA.#POSTAL-DATA := PSTA3049.ADDRSS-POSTAL-DATA
        //*       #POST-DATA.#ADDRESS-TYPE-CDE := PSTA3049.ADDRSS-TYPE-CDE
        //*       MOVE FALSE TO #BAD-ADDRESS
        //*      ELSE
        //*    ELSE
        //*  #POST-DATA.#ADDRESS-LINE-TXT(*) := PSTA3029-MAIL-ADDRSS.ADDRSS-LNE(*)
        //*  #POST-DATA.#POSTAL-DATA := PSTA3029-MAIL-ADDRSS.ADDRSS-POSTAL-DATA
        //*   #POST-DATA.#ADDRESS-TYPE-CDE := PSTA3029-MAIL-ADDRSS.ADDRSS-TYPE-CDE
        //*      MOVE FALSE TO #BAD-ADDRESS
        //*   END-IF
        //*  CALL MDMN100A AND USE RETURNED ADDRESS
        //*    RESET INITIAL #BAD-ADDRESS #MDMA100
        //*    #MDMA100.#I-PIN  := #POST-DATA.#PIN-NUMBER
        //*    CALLNAT 'MDMN100A' #MDMA100
        //*    IF #MDMA100.#O-BASE-ADDRESS-LINE-1 EQ ' '
        //*      MOVE TRUE TO #BAD-ADDRESS
        //*    ELSE
        //*      #POST-DATA.#ADDRESS-LINE-TXT(1):= #MDMA100.#O-BASE-ADDRESS-LINE-1
        //*      #POST-DATA.#ADDRESS-LINE-TXT(2):= #MDMA100.#O-BASE-ADDRESS-LINE-2
        //*      #POST-DATA.#ADDRESS-LINE-TXT(3):= #MDMA100.#O-BASE-ADDRESS-LINE-3
        //*      #POST-DATA.#ADDRESS-LINE-TXT(4):= #MDMA100.#O-BASE-ADDRESS-LINE-4
        //*      #POST-DATA.#POSTAL-DATA := #MDMA100.#O-BASE-ADDRESS-POSTAL-DATA
        //*      #POST-DATA.#ADDRESS-TYPE-CDE :=#MDMA100.#O-BASE-ADDRESS-TYPE-CODE
        //*      MOVE FALSE TO #BAD-ADDRESS
        //*    END-IF
        //*  IF NOT (#POST-DATA.#ADDRESS-LINE-TXT(*) NE #BLANK)
        //*    #ADDRESS-TYPE-CDE := 'B'      /* CAUSE ITEM TO BE DIVERTED
        //*  END-IF
        //*  DECIDE ON FIRST VALUE OF #ADDRESS-TYPE-CDE
        //*    VALUE 'B'
        //*      ADD 1 TO #CNT-RECORD-ADDR-B
        //*      MOVE TRUE TO #BAD-ADDRESS
        //*    VALUE 'C'
        //*      ADD 1 TO #CNT-RECORD-ADDR-C
        //*    VALUE 'F'
        //*      ADD 1 TO #CNT-RECORD-ADDR-F
        //*    VALUE 'U'
        //*      ADD 1 TO #CNT-RECORD-ADDR-U
        //*    NONE
        //*      IGNORE
        //*  END-DECIDE
        //*  GET NAME OF PARTICIPANT
        //*    #POST-DATA.#FULL-NAME := PSTA3029.FULL-NAME
        //*    IF #POST-DATA.#FULL-NAME = #BLANK
        //* **   WRITE / 'PIN NUMBER ' #POST-DATA.#PIN-NUMBER
        //* **           'HAS NO FULLNAME IN NAME & ADDRESS FILE'
        //*      #COR-KEY.PIN := #POST-DATA.#PIN-NUMBER
        //*      #COR-KEY.REC := 01
        //*      FIND (1) COR-PH-V WITH COR-SUPER-PIN-RCDTYPE = #COR-KEY
        //*        COMPRESS COR-PH-V.PH-FIRST-NME COR-PH-V.PH-LAST-NME
        //*          INTO #POST-DATA.#FULL-NAME
        //*      END-FIND
        //*    END-IF
        //*    COMPRESS #MDMA100.#O-PREFERRED-FIRST-NAME " "
        //*             #MDMA100.#O-PREFERRED-MIDDLE-NAME " "
        //*             #MDMA100.#O-PREFERRED-LAST-NAME " "
        //*             #MDMA100.#O-PREFERRED-SUFFIX TO
        //*             #POST-DATA.#FULL-NAME
        //*  ASSIGN RIDER DATE TO POST DATA
        //*  ASSIGN #POST-DATA.#RIDER-DATE = #RDR-DATE
        //*  END-SUBROUTINE     /* GET-NAME-ADDRESS-DETAILS
        //*  --------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PST-NUMBER
        //*  --------------------------------
        //*   GET NEXT MUNBER FOR PST-OUTGOING-MAIL-ITEM. ON THE FIRST CALL,
        //*   ENSURE THAT THE NUMBER ALLOCATED IS THE HIGHEST ON THE FILE.
        //*  MOVE 'RID' TO CURRENT-PST-RQST-ID
        //*  REPEAT
        //*    CALLNAT 'PSTN9650' CURRENT-PST-RQST-ID MSG-INFO-SUB
        //*    IF MSG-INFO-SUB.##RETURN-CODE = #ERROR-IND
        //*      PERFORM TERMINATE-JOB
        //*    END-IF
        //*    #BATCH-ID-FOUND := FALSE
        //*    IF NOT #FIRST-PST-NBR-ALLOCATED
        //*      HISTOGRAM (1) HIST2-MAIL-ITEM FOR RQST-HDR-IND STARTING FROM
        //*          CURRENT-PST-RQST-ID
        //*        IF HIST2-MAIL-ITEM.#PST-RQST-ID >= CURRENT-PST-RQST-ID
        //*          #BATCH-ID-FOUND := TRUE
        //*        END-IF
        //*      END-HISTOGRAM
        //* **   WRITE *PROGRAM 'PST-RQST-ID:' CURRENT-PST-RQST-ID 'is'
        //* **      #BATCH-ID-FOUND (EM='   '/'NOT') 'highest on file. FOUND'
        //* **      HIST2-MAIL-ITEM.#PST-RQST-ID
        //*    END-IF
        //*  WHILE #BATCH-ID-FOUND
        //*  END-REPEAT
        //*  #FIRST-PST-NBR-ALLOCATED := TRUE
        //*  #POST-DATA.#PST-RQST-ID  := CURRENT-PST-RQST-ID
        //* **  IF +TRACE
        //* **   WRITE (0) *PROGRAM 'MAIL ITEM nbr' CURRENT-PST-RQST-ID 'allocated'
        //* **  END-IF
        //*  IF #CNT-ET = 100
        //*    END TRANSACTION
        //*    RESET #CNT-ET
        //*  ELSE
        //*    ADD 1 TO #CNT-ET
        //*  END-IF
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-POST-DATA
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-PROCESSING-REPORT
        //*  -----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-POST-DATA
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BAD-ADDRESS-REPORT
        //*  ----------------------------------------------------------------------
        //*                    E N D    O F    S U B R O U T I N E S
        //*  ----------------------------------------------------------------------
    }
    private void sub_Get_Pst_Number() throws Exception                                                                                                                    //Natural: GET-PST-NUMBER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rid_Ctr.nadd(1);                                                                                                                                              //Natural: ASSIGN #RID-CTR := #RID-CTR + 1
        pnd_Rid_Nbr.compute(new ComputeParameters(false, pnd_Rid_Nbr), DbsField.add(100000000,pnd_Rid_Ctr));                                                              //Natural: ASSIGN #RID-NBR := 100000000 + #RID-CTR
        ldaRidl482.getPnd_Post_Data_Pnd_Pst_Rqst_Id().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "RID", pnd_Rid_Nbr_Pnd_Rid_Nbr_A));                        //Natural: COMPRESS 'RID' #RID-NBR-A TO #POST-DATA.#PST-RQST-ID LEAVING NO
        //*  GET-PST-NUMBER
    }
    private void sub_Store_Post_Data() throws Exception                                                                                                                   //Natural: STORE-POST-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        getWorkFiles().write(2, false, ldaRidl482.getPnd_Post_Data());                                                                                                    //Natural: WRITE WORK FILE 2 #POST-DATA
        ldaRidl482.getPnd_Post_Data().reset();                                                                                                                            //Natural: RESET #POST-DATA
        counters_Pnd_Cnt_Record_Stored.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CNT-RECORD-STORED
        //*  STORE-POST-DATA
    }
    private void sub_Print_Processing_Report() throws Exception                                                                                                           //Natural: PRINT-PROCESSING-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------
        getReports().write(1, NEWLINE,"NUMBER OF RECORDS READ                                :",counters_Pnd_Cnt_Record_Read, new ReportEditMask ("ZZZ,ZZ9"));            //Natural: WRITE ( 1 ) / 'NUMBER OF RECORDS READ                                :' #CNT-RECORD-READ ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"NUMBER OF RECORDS WRITTEN FOR ECS GENERATION          :",counters_Pnd_Cnt_Record_Stored, new ReportEditMask ("ZZZ,ZZ9"));          //Natural: WRITE ( 1 ) / 'NUMBER OF RECORDS WRITTEN FOR ECS GENERATION          :' #CNT-RECORD-STORED ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, "BREAKDOWN OF RECORDS WRITTEN");                                                                                                            //Natural: WRITE ( 1 ) 'BREAKDOWN OF RECORDS WRITTEN'
        if (Global.isEscape()) return;
        getReports().write(1, "   - DOMESTIC ADDRESS (U)                             :",counters_Pnd_Cnt_Record_Addr_U, new ReportEditMask ("ZZZ,ZZ9"));                  //Natural: WRITE ( 1 ) '   - DOMESTIC ADDRESS (U)                             :' #CNT-RECORD-ADDR-U ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, "   - FOREIGN  ADDRESS (F)                             :",counters_Pnd_Cnt_Record_Addr_F, new ReportEditMask ("ZZZ,ZZ9"));                  //Natural: WRITE ( 1 ) '   - FOREIGN  ADDRESS (F)                             :' #CNT-RECORD-ADDR-F ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, "   - CANADIAN ADDRESS (C)                             :",counters_Pnd_Cnt_Record_Addr_C, new ReportEditMask ("ZZZ,ZZ9"));                  //Natural: WRITE ( 1 ) '   - CANADIAN ADDRESS (C)                             :' #CNT-RECORD-ADDR-C ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, "EXCLUDED RECORDS:");                                                                                                                       //Natural: WRITE ( 1 ) 'EXCLUDED RECORDS:'
        if (Global.isEscape()) return;
        getReports().write(1, "   - BAD      ADDRESS (B)                             :",counters_Pnd_Cnt_Record_Addr_B, new ReportEditMask ("ZZZ,ZZ9"));                  //Natural: WRITE ( 1 ) '   - BAD      ADDRESS (B)                             :' #CNT-RECORD-ADDR-B ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE);                                                                                                                           //Natural: WRITE ( 1 ) //
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(pnd_Package_Table_Pnd_Package_Code.getValue("*")), new ExamineSearch(pnd_Ph_Data_Pnd_Package_Code), new ExamineGivingIndex(pnd_K)); //Natural: EXAMINE #PACKAGE-TABLE.#PACKAGE-CODE ( * ) FOR #PH-DATA.#PACKAGE-CODE GIVING INDEX #K
        //*  WRITE '=' #K '=' #PH-DATA.#PACKAGE-CODE
        getReports().write(1, NEWLINE,"PACKAGE CODE     : ",pnd_Ph_Data_Pnd_Package_Code," - ",pnd_Package_Table_Pnd_File_Desc.getValue(pnd_K));                          //Natural: WRITE ( 1 ) / 'PACKAGE CODE     : ' #PH-DATA.#PACKAGE-CODE ' - ' #FILE-DESC ( #K )
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"FIRST PST NUMBER : ",pnd_Package_Variables_Pnd_First_Pst);                                                                         //Natural: WRITE ( 1 ) / 'FIRST PST NUMBER : ' #FIRST-PST
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"LAST  PST NUMBER : ",pnd_Package_Variables_Pnd_Last_Pst);                                                                          //Natural: WRITE ( 1 ) / 'LAST  PST NUMBER : ' #LAST-PST
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE," ",NEWLINE,NEWLINE);                                                                                                               //Natural: WRITE / ' ' //
        if (Global.isEscape()) return;
        getReports().write(1, "FINISHING        : ",pnd_Package_Table_Pnd_Bin_Content.getValue(pnd_K),NEWLINE);                                                           //Natural: WRITE ( 1 ) 'FINISHING        : ' #BIN-CONTENT ( #K ) /
        if (Global.isEscape()) return;
        //*  PRINT-PROCESSING-REPORT
    }
    private void sub_Display_Post_Data() throws Exception                                                                                                                 //Natural: DISPLAY-POST-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------------
        getReports().write(0, NEWLINE,"PST : ",ldaRidl482.getPnd_Post_Data_Pnd_Pst_Rqst_Id(),"PIN : ",ldaRidl482.getPnd_Post_Data_Pnd_Pin_Number());                      //Natural: WRITE ( 0 ) / 'PST : ' #POST-DATA.#PST-RQST-ID 'PIN : ' #POST-DATA.#PIN-NUMBER
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl482.getPnd_Post_Data_Pnd_Full_Name());                                                                                   //Natural: WRITE ( 0 ) / '=' #POST-DATA.#FULL-NAME
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl482.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(1));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 1 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl482.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(2));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 2 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl482.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(3));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 3 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl482.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(4));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 4 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl482.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(5));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 5 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl482.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(6));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 6 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl482.getPnd_Post_Data_Pnd_Postal_Data());                                                                                 //Natural: WRITE ( 0 ) / '=' #POST-DATA.#POSTAL-DATA
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl482.getPnd_Post_Data_Pnd_Address_Type_Cde());                                                                            //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-TYPE-CDE
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl482.getPnd_Post_Data_Pnd_Test_Code(),"PCKG-CDE : ",ldaRidl482.getPnd_Post_Data_Pnd_Package_Code());                      //Natural: WRITE ( 0 ) / '=' #POST-DATA.#TEST-CODE 'PCKG-CDE : ' #POST-DATA.#PACKAGE-CODE
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE," ");                                                                                                                               //Natural: WRITE ( 0 ) / ' '
        if (Global.isEscape()) return;
        //*  DISPLAY-POST-DATA
    }
    private void sub_Write_Bad_Address_Report() throws Exception                                                                                                          //Natural: WRITE-BAD-ADDRESS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------
        if (condition(pnd_First_Bad_Record.getBoolean()))                                                                                                                 //Natural: IF #FIRST-BAD-RECORD
        {
            //*      WRITE (2) 'INSTITUTION : ' #PH-DATA.#INSTITUTION-NAME /
            pnd_First_Bad_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #FIRST-BAD-RECORD
        }                                                                                                                                                                 //Natural: END-IF
        //*  PINEXP >>>
        //*  PINEXP <<<
        getReports().write(2, pnd_Ph_Data_Pnd_Pin_Number,new TabSetting(15),ldaRidl482.getPnd_Post_Data_Pnd_Full_Name(), new AlphanumericLength (35),new                  //Natural: WRITE ( 2 ) #PH-DATA.#PIN-NUMBER 15T #POST-DATA.#FULL-NAME ( AL = 35 ) 51T #PH-DATA.#CTR-GRA-TIAA-A 55T #PH-DATA.#CTR-GRA-CREF-A 59T #PH-DATA.#CTR-GSRA-CREF-A 63T #PH-DATA.#CTR-GSRA-CREF-A 68T #PH-DATA.#INSTITUTION-NAME ( AL = 60 )
            TabSetting(51),pnd_Ph_Data_Pnd_Ctr_Gra_Tiaa_A,new TabSetting(55),pnd_Ph_Data_Pnd_Ctr_Gra_Cref_A,new TabSetting(59),pnd_Ph_Data_Pnd_Ctr_Gsra_Cref_A,new 
            TabSetting(63),pnd_Ph_Data_Pnd_Ctr_Gsra_Cref_A,new TabSetting(68),pnd_Ph_Data_Pnd_Institution_Name, new AlphanumericLength (60));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");
        Global.format(0, "LS=132 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(39),"2008 FORCED CASHOUT RIDER MAILING POST LOAD PROCESSING STATS",new 
            TabSetting(118),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(118),"TIME:",Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,new TabSetting(118),"PAGE:",new 
            ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(20),"2008 FORCED CASHOUT RIDER MAILING POST LOAD PROCESSING STATS",new 
            TabSetting(84),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(42),"BAD ADDRESS REPORT",new TabSetting(84),"TIME:",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,new TabSetting(84),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(2),NEWLINE,NEWLINE,"----------------------------------------------------------------------------------------------------------------------|",
            NEWLINE,"                                             GRA  GRA  GSR  GSR     ","  PIN    NAME                                TIA  CRF  TIA  CRF           INSTITUTION NAME        ",
            "----------------------------------------------------------------------------------------------------------------------|",NEWLINE);
    }
}
