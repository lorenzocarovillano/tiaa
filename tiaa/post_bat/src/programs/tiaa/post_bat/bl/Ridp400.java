/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:21:51 PM
**        * FROM NATURAL PROGRAM : Ridp400
************************************************************
**        * FILE NAME            : Ridp400.java
**        * CLASS NAME           : Ridp400
**        * INSTANCE NAME        : Ridp400
************************************************************
************************************************************************
** PROGRAM     : RIDP400                                              **
** AUTHOR      : JOANNES C. AVE                                       **
** DATE        : 08/14/2006                                           **
** DESCRIPTION : READS DA REPORTING ROTH 403(B) EXTRACT AND REFORMATS **
**               DATA AS FEED TO RIDP401                              **
**               PACKAGE CODES: RDR2006T,RDR2006U,RDR2006V,RDR2006W   **
**                              RDR2006X,RDR2006Y                     **
************************************************************************
** HISTORY:                                                           **
** 08/14/2006  : INITIAL IMPLEMENTATION                               **
** 06/09/2007  : ADD INPUT FIELD #COVER-LETTER-IND AND PASS IT TO     **
**               OUTPUT FIELD #LETTER-TYPE TO DETERMINE THE COVER     **
**               LETTER TAG IN RIDN404.                               **
**             : ADD #CONTRACTS ARRAY IN THE OUTPUT FILE LAYOUT       **
**               ADD SUBROUTINE ADD-CONTRACT-TO-ARRAY                 **
** 02/25/2008  : ADD CREF CONTRACT ALTERNATING WITH TIAA CONTRACT     **
**               SEPARATED WITH A SLASH '/'                           **
** 02/01/2009  : REDUCE PACKAGE TYPES TO:                             **
**               RDR2006T - TOTAL CONTRACTS LE 2                      **
**               RDR2006U - TOTAL CONTRACTS GT 2                      **
** 08/31/2011  : EXCLUDE THE RC/RCP CONTRACTS.                        **
**               ADD FLORIDA SPECIFIC CREF ENDORSEMENTS               **
**               SEND 1 ENDORSEMENT PER LINE OF BUSINESS              **
** 01/17/2012  : ADD THE FOLLOWING STATE SPECIFIC ENDORSEMENTS:       **
**                  TIAA 457(B) FOR AR/HI AND NH                      **
**                  CREF 457(B) FOR IL                                **
** 03/24/2015  : COR/NAAD SUNSET - REMOVE ALL REFERENCES TO COR/NAAD  **
**               AND CALL THE MDMN MODULES INSTEAD FOR PARTICIPANT    **
**               AND CONTRACT INFORMATION                             **
** 07/15/2016  : LUMP PACKAGE CODE TO JUST RDR2006T AS PART OF DCS    **
**               ELIMINATION PROJECT.                                 **
** 05/22/2017  : PIN EXPANSION                                PINEXP  **
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridp400 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;
    private PdaPsta9017 pdaPsta9017;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Pin_A12;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Pin_N7;
    private DbsField pnd_Input_Record_Pnd_Pin_A5;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Pin_N12;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cref_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Date;
    private DbsField pnd_Input_Record_Pnd_Issue_State_Cde;
    private DbsField pnd_Input_Record_Pnd_Plan_Cde;
    private DbsField pnd_Input_Record_Pnd_Cover_Letter_Ind;
    private DbsField pnd_Input_Record_Pnd_Inst_Name;
    private DbsField pnd_Prev_Inst_Name;
    private DbsField pnd_Prev_Plan_Cde;
    private DbsField pnd_Prev_Pin_A12;
    private DbsField pnd_Prev_Contract;
    private DbsField pnd_Prev_Cover;
    private DbsField pnd_X;
    private DbsField pnd_K;
    private DbsField pnd_Valid_Record;
    private DbsField pnd_Accepted;
    private DbsField pnd_Remarks;
    private DbsField pnd_Lob;
    private DbsField pnd_Cutoff;
    private DbsField pnd_Participant_Name;
    private DbsField pnd_Ctr_Roth_Contract_Read;
    private DbsField pnd_Ctr_Valid_Roth_Contract_Read;
    private DbsField pnd_Ctr_Da_Contract_Not_In_Cor;
    private DbsField pnd_Ctr_Da_Inactive_In_Cor;
    private DbsField pnd_Ctr_Ph_Deceased;
    private DbsField pnd_Ctr_Ph_With_Mail_Suppression;
    private DbsField pnd_Ctr_Contract_Accepted;
    private DbsField pnd_Ctr_Contract_Rejected;
    private DbsField pnd_Ctr_Contract_Read;
    private DbsField pnd_Ctr_Duplicate_Contract;
    private DbsField pnd_Ctr_Terminated_Contract;
    private DbsField pnd_Ctr_Contract_Inactive;
    private DbsField pnd_Ctr_Out_Of_State;
    private DbsField pnd_Ctr_Contract_Outof_Range;
    private DbsField pnd_Ctr_Plan_Not_For_Proc;
    private DbsField pnd_Ctr_Total_Contracts;
    private DbsField pnd_Ctr_Total_Ra_Sra_Gn;
    private DbsField pnd_Ctr_Total_Gra_Gsra_Rs_Gn;
    private DbsField pnd_Ctr_Total_Ra_Sra_Il;
    private DbsField pnd_Ctr_Total_Gra_Gsra_Rs_Il;
    private DbsField pnd_Ctr_Total_Ra_Sra_Cref_Fl;
    private DbsField pnd_Ctr_Total_Gra_Gsra_Rs_Cref_Fl;
    private DbsField pnd_Ctr_Total_Gsra_457b;
    private DbsField pnd_Ctr_Total_Gsra_457b_Cref;
    private DbsField pnd_Ctr_Total_Gsra_457b_Nl;
    private DbsField pnd_Ctr_Total_Gsra_457b_Nh;
    private DbsField pnd_Ctr_Total_Gsra_457b_Cref_Il;
    private DbsField pnd_Break;
    private DbsField pnd_I;
    private DbsField pnd_A;

    private DbsGroup pnd_Arrays;
    private DbsField pnd_Arrays_Pnd_Combo_Prod;
    private DbsField pnd_Arrays_Pnd_Combo_Ctr;
    private DbsField pnd_Combo;

    private DbsGroup pnd_Ctrs;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Sra_Gn;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Gn;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Sra_Il;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Il;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Sra_Cref_Fl;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gsra_457b;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nl;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nh;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref_Il;
    private DbsField pnd_Ctr_File1;
    private DbsField pnd_Ctr_File2;

    private DbsGroup pnd_Output;
    private DbsField pnd_Output_Pnd_Pin_A12;
    private DbsField pnd_Output_Pnd_Letter_Type;
    private DbsField pnd_Output_Pnd_Institution_Name;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Sra_Gn;
    private DbsField pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Gn;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Sra_Il;
    private DbsField pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Il;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Sra_Cref_Fl;
    private DbsField pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl;
    private DbsField pnd_Output_Pnd_Ctr_Gsra_457b;
    private DbsField pnd_Output_Pnd_Ctr_Gsra_457b_Cref;
    private DbsField pnd_Output_Pnd_Ctr_Gsra_457b_Nl;
    private DbsField pnd_Output_Pnd_Ctr_Gsra_457b_Nh;
    private DbsField pnd_Output_Pnd_Ctr_Gsra_457b_Cref_Il;
    private DbsField pnd_Output_Pnd_Package_Code;
    private DbsField pnd_Output_Pnd_Contracts;
    private DbsField pnd_Pst_Tbl_Data_Field2;

    private DbsGroup pnd_Pst_Tbl_Data_Field2__R_Field_4;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Inst_Pln;
    private DbsField pnd_Ndx;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);
        pdaPsta9017 = new PdaPsta9017(localVariables);

        // Local Variables
        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 115);

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Pin_A12 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N7 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);
        pnd_Input_Record_Pnd_Pin_A5 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A5", "#PIN-A5", FieldType.STRING, 5);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N12 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N12", "#PIN-N12", FieldType.NUMERIC, 12);
        pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr", "#TIAA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cref_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cref_Cntrct_Nbr", "#CREF-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cntrct_Issue_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Date", "#CNTRCT-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Issue_State_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Issue_State_Cde", "#ISSUE-STATE-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Plan_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Plan_Cde", "#PLAN-CDE", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cover_Letter_Ind = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cover_Letter_Ind", "#COVER-LETTER-IND", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Inst_Name = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Inst_Name", "#INST-NAME", FieldType.STRING, 
            60);
        pnd_Prev_Inst_Name = localVariables.newFieldInRecord("pnd_Prev_Inst_Name", "#PREV-INST-NAME", FieldType.STRING, 60);
        pnd_Prev_Plan_Cde = localVariables.newFieldInRecord("pnd_Prev_Plan_Cde", "#PREV-PLAN-CDE", FieldType.STRING, 6);
        pnd_Prev_Pin_A12 = localVariables.newFieldInRecord("pnd_Prev_Pin_A12", "#PREV-PIN-A12", FieldType.STRING, 12);
        pnd_Prev_Contract = localVariables.newFieldInRecord("pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Cover = localVariables.newFieldInRecord("pnd_Prev_Cover", "#PREV-COVER", FieldType.STRING, 1);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_Valid_Record = localVariables.newFieldInRecord("pnd_Valid_Record", "#VALID-RECORD", FieldType.BOOLEAN, 1);
        pnd_Accepted = localVariables.newFieldInRecord("pnd_Accepted", "#ACCEPTED", FieldType.BOOLEAN, 1);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 40);
        pnd_Lob = localVariables.newFieldInRecord("pnd_Lob", "#LOB", FieldType.STRING, 4);
        pnd_Cutoff = localVariables.newFieldInRecord("pnd_Cutoff", "#CUTOFF", FieldType.STRING, 6);
        pnd_Participant_Name = localVariables.newFieldInRecord("pnd_Participant_Name", "#PARTICIPANT-NAME", FieldType.STRING, 35);
        pnd_Ctr_Roth_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Roth_Contract_Read", "#CTR-ROTH-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Valid_Roth_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Valid_Roth_Contract_Read", "#CTR-VALID-ROTH-CONTRACT-READ", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Da_Contract_Not_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Contract_Not_In_Cor", "#CTR-DA-CONTRACT-NOT-IN-COR", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Da_Inactive_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Inactive_In_Cor", "#CTR-DA-INACTIVE-IN-COR", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_Deceased = localVariables.newFieldInRecord("pnd_Ctr_Ph_Deceased", "#CTR-PH-DECEASED", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_With_Mail_Suppression = localVariables.newFieldInRecord("pnd_Ctr_Ph_With_Mail_Suppression", "#CTR-PH-WITH-MAIL-SUPPRESSION", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Contract_Accepted = localVariables.newFieldInRecord("pnd_Ctr_Contract_Accepted", "#CTR-CONTRACT-ACCEPTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Rejected = localVariables.newFieldInRecord("pnd_Ctr_Contract_Rejected", "#CTR-CONTRACT-REJECTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Contract_Read", "#CTR-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Duplicate_Contract = localVariables.newFieldInRecord("pnd_Ctr_Duplicate_Contract", "#CTR-DUPLICATE-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Terminated_Contract = localVariables.newFieldInRecord("pnd_Ctr_Terminated_Contract", "#CTR-TERMINATED-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Inactive = localVariables.newFieldInRecord("pnd_Ctr_Contract_Inactive", "#CTR-CONTRACT-INACTIVE", FieldType.NUMERIC, 7);
        pnd_Ctr_Out_Of_State = localVariables.newFieldInRecord("pnd_Ctr_Out_Of_State", "#CTR-OUT-OF-STATE", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Outof_Range = localVariables.newFieldInRecord("pnd_Ctr_Contract_Outof_Range", "#CTR-CONTRACT-OUTOF-RANGE", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Plan_Not_For_Proc = localVariables.newFieldInRecord("pnd_Ctr_Plan_Not_For_Proc", "#CTR-PLAN-NOT-FOR-PROC", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Contracts = localVariables.newFieldInRecord("pnd_Ctr_Total_Contracts", "#CTR-TOTAL-CONTRACTS", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_Sra_Gn = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Sra_Gn", "#CTR-TOTAL-RA-SRA-GN", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gra_Gsra_Rs_Gn = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra_Gsra_Rs_Gn", "#CTR-TOTAL-GRA-GSRA-RS-GN", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Total_Ra_Sra_Il = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Sra_Il", "#CTR-TOTAL-RA-SRA-IL", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gra_Gsra_Rs_Il = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra_Gsra_Rs_Il", "#CTR-TOTAL-GRA-GSRA-RS-IL", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Total_Ra_Sra_Cref_Fl = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Sra_Cref_Fl", "#CTR-TOTAL-RA-SRA-CREF-FL", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Total_Gra_Gsra_Rs_Cref_Fl = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra_Gsra_Rs_Cref_Fl", "#CTR-TOTAL-GRA-GSRA-RS-CREF-FL", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Total_Gsra_457b = localVariables.newFieldInRecord("pnd_Ctr_Total_Gsra_457b", "#CTR-TOTAL-GSRA-457B", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gsra_457b_Cref = localVariables.newFieldInRecord("pnd_Ctr_Total_Gsra_457b_Cref", "#CTR-TOTAL-GSRA-457B-CREF", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Total_Gsra_457b_Nl = localVariables.newFieldInRecord("pnd_Ctr_Total_Gsra_457b_Nl", "#CTR-TOTAL-GSRA-457B-NL", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gsra_457b_Nh = localVariables.newFieldInRecord("pnd_Ctr_Total_Gsra_457b_Nh", "#CTR-TOTAL-GSRA-457B-NH", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gsra_457b_Cref_Il = localVariables.newFieldInRecord("pnd_Ctr_Total_Gsra_457b_Cref_Il", "#CTR-TOTAL-GSRA-457B-CREF-IL", FieldType.NUMERIC, 
            7);
        pnd_Break = localVariables.newFieldInRecord("pnd_Break", "#BREAK", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);

        pnd_Arrays = localVariables.newGroupArrayInRecord("pnd_Arrays", "#ARRAYS", new DbsArrayController(1, 240));
        pnd_Arrays_Pnd_Combo_Prod = pnd_Arrays.newFieldInGroup("pnd_Arrays_Pnd_Combo_Prod", "#COMBO-PROD", FieldType.STRING, 12);
        pnd_Arrays_Pnd_Combo_Ctr = pnd_Arrays.newFieldInGroup("pnd_Arrays_Pnd_Combo_Ctr", "#COMBO-CTR", FieldType.NUMERIC, 7);
        pnd_Combo = localVariables.newFieldInRecord("pnd_Combo", "#COMBO", FieldType.STRING, 12);

        pnd_Ctrs = localVariables.newGroupInRecord("pnd_Ctrs", "#CTRS");
        pnd_Ctrs_Pnd_Ctr_Ra_Sra_Gn = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Sra_Gn", "#CTR-RA-SRA-GN", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Gn = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Gn", "#CTR-GRA-GSRA-RS-GN", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Sra_Il = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Sra_Il", "#CTR-RA-SRA-IL", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Il = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Il", "#CTR-GRA-GSRA-RS-IL", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Sra_Cref_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Sra_Cref_Fl", "#CTR-RA-SRA-CREF-FL", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl", "#CTR-GRA-GSRA-RS-CREF-FL", FieldType.NUMERIC, 
            1);
        pnd_Ctrs_Pnd_Ctr_Gsra_457b = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gsra_457b", "#CTR-GSRA-457B", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref", "#CTR-GSRA-457B-CREF", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nl", "#CTR-GSRA-457B-NL", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nh = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nh", "#CTR-GSRA-457B-NH", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref_Il = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref_Il", "#CTR-GSRA-457B-CREF-IL", FieldType.NUMERIC, 
            1);
        pnd_Ctr_File1 = localVariables.newFieldInRecord("pnd_Ctr_File1", "#CTR-FILE1", FieldType.NUMERIC, 7);
        pnd_Ctr_File2 = localVariables.newFieldInRecord("pnd_Ctr_File2", "#CTR-FILE2", FieldType.NUMERIC, 7);

        pnd_Output = localVariables.newGroupInRecord("pnd_Output", "#OUTPUT");
        pnd_Output_Pnd_Pin_A12 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);
        pnd_Output_Pnd_Letter_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Institution_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 60);
        pnd_Output_Pnd_Ctr_Ra_Sra_Gn = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Sra_Gn", "#CTR-RA-SRA-GN", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Gn = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Gn", "#CTR-GRA-GSRA-RS-GN", FieldType.NUMERIC, 
            1);
        pnd_Output_Pnd_Ctr_Ra_Sra_Il = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Sra_Il", "#CTR-RA-SRA-IL", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Il = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Il", "#CTR-GRA-GSRA-RS-IL", FieldType.NUMERIC, 
            1);
        pnd_Output_Pnd_Ctr_Ra_Sra_Cref_Fl = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Sra_Cref_Fl", "#CTR-RA-SRA-CREF-FL", FieldType.NUMERIC, 
            1);
        pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl", "#CTR-GRA-GSRA-RS-CREF-FL", FieldType.NUMERIC, 
            1);
        pnd_Output_Pnd_Ctr_Gsra_457b = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gsra_457b", "#CTR-GSRA-457B", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gsra_457b_Cref = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gsra_457b_Cref", "#CTR-GSRA-457B-CREF", FieldType.NUMERIC, 
            1);
        pnd_Output_Pnd_Ctr_Gsra_457b_Nl = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gsra_457b_Nl", "#CTR-GSRA-457B-NL", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gsra_457b_Nh = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gsra_457b_Nh", "#CTR-GSRA-457B-NH", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gsra_457b_Cref_Il = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gsra_457b_Cref_Il", "#CTR-GSRA-457B-CREF-IL", FieldType.NUMERIC, 
            1);
        pnd_Output_Pnd_Package_Code = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Output_Pnd_Contracts = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Contracts", "#CONTRACTS", FieldType.STRING, 17, new DbsArrayController(1, 
            20));
        pnd_Pst_Tbl_Data_Field2 = localVariables.newFieldInRecord("pnd_Pst_Tbl_Data_Field2", "#PST-TBL-DATA-FIELD2", FieldType.STRING, 253);

        pnd_Pst_Tbl_Data_Field2__R_Field_4 = localVariables.newGroupInRecord("pnd_Pst_Tbl_Data_Field2__R_Field_4", "REDEFINE", pnd_Pst_Tbl_Data_Field2);
        pnd_Pst_Tbl_Data_Field2_Pnd_Inst_Pln = pnd_Pst_Tbl_Data_Field2__R_Field_4.newFieldArrayInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Inst_Pln", "#INST-PLN", 
            FieldType.STRING, 6, new DbsArrayController(1, 42));
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Valid_Record.setInitialValue(false);
        pnd_Accepted.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridp400() throws Exception
    {
        super("Ridp400");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  STATISTICS REPORT
        if (condition(getReports().getAstLinesLeft(1).less(5)))                                                                                                           //Natural: FORMAT ( 1 ) LS = 120 PS = 60;//Natural: FORMAT LS = 80 PS = 60;//Natural: NEWPAGE ( 1 ) IF LESS THAN 5 LINES
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        //*  PERFORM GET-PLANS-WITH-457B
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 43T 'ROTH RIDER MAILING' 84T 'DATE:' *DATU / 43T 'EXCEPTIONS LIST' 84T 'TIME:' *TIME ( EM = XXXXXXXX ) / 84T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) // 2T 'PIN            TIAA NBR   STATE   REMARKS'/ 2T '------------------------------------------------------------'/
        //*  OPEN MQ QUEUE                                       /* JCA20150324
        getReports().write(0, " FETCHING MDMP011");                                                                                                                       //Natural: WRITE ' FETCHING MDMP011'
        if (Global.isEscape()) return;
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM GET-PLANS-WITH-457B
        sub_Get_Plans_With_457b();
        if (condition(Global.isEscape())) {return;}
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #INPUT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  PINEXP
            pnd_Ctr_Roth_Contract_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-ROTH-CONTRACT-READ
            //*                                                                                                                                                           //Natural: AT BREAK OF #INPUT-RECORD.#PIN-A12
            //*                                                                                                                                                           //Natural: AT END OF DATA
            if (condition(pnd_Break.getBoolean()))                                                                                                                        //Natural: IF #BREAK
            {
                pnd_Break.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #BREAK
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP
            pnd_Prev_Pin_A12.setValue(pnd_Input_Record_Pnd_Pin_A12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-A12 TO #PREV-PIN-A12
            pnd_Prev_Cover.setValue(pnd_Input_Record_Pnd_Cover_Letter_Ind);                                                                                               //Natural: MOVE #INPUT-RECORD.#COVER-LETTER-IND TO #PREV-COVER
            pnd_Prev_Plan_Cde.setValue(pnd_Input_Record_Pnd_Plan_Cde);                                                                                                    //Natural: MOVE #INPUT-RECORD.#PLAN-CDE TO #PREV-PLAN-CDE
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Inst_Name);                                                                                                  //Natural: MOVE #INPUT-RECORD.#INST-NAME TO #PREV-INST-NAME
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
        }                                                                                                                                                                 //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-STATISTICS-REPORT
        sub_Write_Statistics_Report();
        if (condition(Global.isEscape())) {return;}
        //*  CLOSE MQ QUEUE
        getReports().write(0, "FETCHING MDMP0012");                                                                                                                       //Natural: WRITE 'FETCHING MDMP0012'
        if (Global.isEscape()) return;
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* **********************************************************************
        //*                 S T A R T    O F    S U B R O U T I N E S
        //* **********************************************************************
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PLANS-WITH-457B
        //*  -------------------------------------
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-RECORD
        //*  RA/SRA CONTRACTS
        //*  GRA/RS CONTRACTS
        //*  GSRA CONTRACTS
        //*  ---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-ARRAY
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RECORD-IN-COR
        //*  --------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PIN-STATUS
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA-TO-FILE
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA-TO-ARRAY
        //* **************************************
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-STATISTICS-REPORT
        //* ******************************************
    }
    private void sub_Get_Plans_With_457b() throws Exception                                                                                                               //Natural: GET-PLANS-WITH-457B
    {
        if (BLNatReinput.isReinput()) return;

        pdaPsta9017.getPsta9017_Tbl_Table_Nme().setValue("RIDER-MISCELLANEOUS");                                                                                          //Natural: ASSIGN PSTA9017.TBL-TABLE-NME := 'RIDER-MISCELLANEOUS'
        pdaPsta9017.getPsta9017_Tbl_Key_Field().setValue("PLANS-WITH-457B");                                                                                              //Natural: ASSIGN PSTA9017.TBL-KEY-FIELD := 'PLANS-WITH-457B'
        pdaPsta9017.getPsta9017_Tbl_Scrty_Level_Ind().setValue("A");                                                                                                      //Natural: ASSIGN PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        DbsUtil.callnat(Pstn9017.class , getCurrentProcessState(), pdaPsta9017.getPsta9017());                                                                            //Natural: CALLNAT 'PSTN9017' PSTA9017
        if (condition(Global.isEscape())) return;
        if (condition(pdaPsta9017.getPsta9017_Valid_Entry().getBoolean()))                                                                                                //Natural: IF PSTA9017.VALID-ENTRY
        {
            pnd_Pst_Tbl_Data_Field2.setValue(pdaPsta9017.getPsta9017_Tbl_Data_Field());                                                                                   //Natural: ASSIGN #PST-TBL-DATA-FIELD2 := PSTA9017.TBL-DATA-FIELD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "TABLE ENTRY NOT FOUND 1");                                                                                                             //Natural: WRITE 'TABLE ENTRY NOT FOUND 1'
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-PLANS-WITH-457B
    }
    private void sub_Validate_Record() throws Exception                                                                                                                   //Natural: VALIDATE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        if (condition(pnd_Prev_Contract.equals(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr)))                                                                                    //Natural: IF #PREV-CONTRACT EQ #TIAA-CNTRCT-NBR
        {
            pnd_Ctr_Duplicate_Contract.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DUPLICATE-CONTRACT
            pnd_Remarks.setValue("DUPLICATE CONTRACT");                                                                                                                   //Natural: ASSIGN #REMARKS := 'DUPLICATE CONTRACT'
            //*  PINEXP
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prev_Contract.setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #PREV-CONTRACT
                                                                                                                                                                          //Natural: PERFORM CHECK-RECORD-IN-COR
        sub_Check_Record_In_Cor();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Valid_Record.getBoolean()))                                                                                                                     //Natural: IF #VALID-RECORD
        {
            short decideConditionsMet636 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TIAA-CNTRCT-NBR EQ 'A0000010' THRU 'A9899999' OR #TIAA-CNTRCT-NBR EQ 'B0000010' THRU 'B9999999' OR #TIAA-CNTRCT-NBR EQ 'C0100000' THRU 'C7999999' OR #TIAA-CNTRCT-NBR EQ 'C9000000' THRU 'C9999999' OR #TIAA-CNTRCT-NBR EQ 'D0100000' THRU 'D9999999' OR #TIAA-CNTRCT-NBR EQ 'E0100000' THRU 'E9999999' OR #TIAA-CNTRCT-NBR EQ 'K0000010' THRU 'K4999999' OR #TIAA-CNTRCT-NBR EQ 'K8000000' THRU 'K8099999' OR #TIAA-CNTRCT-NBR EQ 'L9000000' THRU 'L9899999'
            if (condition((((((((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("A0000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("A9899999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("B0000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("B9999999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C0100000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C7999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C9000000") 
                && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C9999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("D0100000") && 
                pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("D9999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("E0100000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("E9999999"))) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K0000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K4999999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K8000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K8099999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("L9000000") 
                && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("L9899999")))))
            {
                decideConditionsMet636++;
                if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("016")))                                                                                        //Natural: IF #ISSUE-STATE-CDE EQ '016'
                {
                    pnd_Ctrs_Pnd_Ctr_Ra_Sra_Il.setValue(1);                                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-RA-SRA-IL
                    pnd_Ctr_Total_Ra_Sra_Il.nadd(1);                                                                                                                      //Natural: ADD 1 TO #CTR-TOTAL-RA-SRA-IL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("011") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                             //Natural: IF #ISSUE-STATE-CDE EQ '011' AND #CREF-CNTRCT-NBR NE ' '
                    {
                        pnd_Ctrs_Pnd_Ctr_Ra_Sra_Cref_Fl.setValue(1);                                                                                                      //Natural: MOVE 1 TO #CTRS.#CTR-RA-SRA-CREF-FL
                        pnd_Ctr_Total_Ra_Sra_Cref_Fl.nadd(1);                                                                                                             //Natural: ADD 1 TO #CTR-TOTAL-RA-SRA-CREF-FL
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ctrs_Pnd_Ctr_Ra_Sra_Gn.setValue(1);                                                                                                           //Natural: MOVE 1 TO #CTRS.#CTR-RA-SRA-GN
                        pnd_Ctr_Total_Ra_Sra_Gn.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTR-TOTAL-RA-SRA-GN
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                //*  061107 J.AVE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #TIAA-CNTRCT-NBR EQ '20000010' THRU '20099999' OR #TIAA-CNTRCT-NBR EQ '20250000' THRU '28999999' OR #TIAA-CNTRCT-NBR EQ '30000100' THRU '39999999' OR #TIAA-CNTRCT-NBR EQ 'F3000000' THRU 'F3999999' OR #TIAA-CNTRCT-NBR EQ 'F4000000' THRU 'F4999999' OR #TIAA-CNTRCT-NBR EQ 'F7500000' THRU 'F7999999'
            else if (condition(((((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("20099999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20250000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("28999999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("30000100") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("39999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("F3000000") 
                && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("F3999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("F4000000") && 
                pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("F4999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("F7500000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("F7999999")))))
            {
                decideConditionsMet636++;
                if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("016")))                                                                                        //Natural: IF #ISSUE-STATE-CDE EQ '016'
                {
                    pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Il.setValue(1);                                                                                                          //Natural: MOVE 1 TO #CTRS.#CTR-GRA-GSRA-RS-IL
                    pnd_Ctr_Total_Gra_Gsra_Rs_Il.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTR-TOTAL-GRA-GSRA-RS-IL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("011") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                             //Natural: IF #ISSUE-STATE-CDE EQ '011' AND #CREF-CNTRCT-NBR NE ' '
                    {
                        pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl.setValue(1);                                                                                                 //Natural: MOVE 1 TO #CTRS.#CTR-GRA-GSRA-RS-CREF-FL
                        pnd_Ctr_Total_Gra_Gsra_Rs_Cref_Fl.nadd(1);                                                                                                        //Natural: ADD 1 TO #CTR-TOTAL-GRA-GSRA-RS-CREF-FL
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Gn.setValue(1);                                                                                                      //Natural: MOVE 1 TO #CTRS.#CTR-GRA-GSRA-RS-GN
                        pnd_Ctr_Total_Gra_Gsra_Rs_Gn.nadd(1);                                                                                                             //Natural: ADD 1 TO #CTR-TOTAL-GRA-GSRA-RS-GN
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                //*  060117 J.AVE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #TIAA-CNTRCT-NBR EQ 'K5000000' THRU 'K7999999' OR #TIAA-CNTRCT-NBR EQ 'L0000100' THRU 'L8499999'
            else if (condition(((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K5000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K7999999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("L0000100") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("L8499999")))))
            {
                decideConditionsMet636++;
                DbsUtil.examine(new ExamineSource(pnd_Pst_Tbl_Data_Field2_Pnd_Inst_Pln.getValue("*")), new ExamineSearch(pnd_Input_Record_Pnd_Plan_Cde),                  //Natural: EXAMINE #INST-PLN ( * ) FOR #PLAN-CDE GIVING INDEX #NDX
                    new ExamineGivingIndex(pnd_Ndx));
                if (condition(pnd_Ndx.equals(getZero())))                                                                                                                 //Natural: IF #NDX EQ 0
                {
                    if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("016")))                                                                                    //Natural: IF #ISSUE-STATE-CDE EQ '016'
                    {
                        pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Il.setValue(1);                                                                                                      //Natural: MOVE 1 TO #CTRS.#CTR-GRA-GSRA-RS-IL
                        pnd_Ctr_Total_Gra_Gsra_Rs_Il.nadd(1);                                                                                                             //Natural: ADD 1 TO #CTR-TOTAL-GRA-GSRA-RS-IL
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("011") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                         //Natural: IF #ISSUE-STATE-CDE EQ '011' AND #CREF-CNTRCT-NBR NE ' '
                        {
                            pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl.setValue(1);                                                                                             //Natural: MOVE 1 TO #CTRS.#CTR-GRA-GSRA-RS-CREF-FL
                            pnd_Ctr_Total_Gra_Gsra_Rs_Cref_Fl.nadd(1);                                                                                                    //Natural: ADD 1 TO #CTR-TOTAL-GRA-GSRA-RS-CREF-FL
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Gn.setValue(1);                                                                                                  //Natural: MOVE 1 TO #CTRS.#CTR-GRA-GSRA-RS-GN
                            pnd_Ctr_Total_Gra_Gsra_Rs_Gn.nadd(1);                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-GRA-GSRA-RS-GN
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("016")))                                                                                    //Natural: IF #ISSUE-STATE-CDE EQ '016'
                    {
                        pnd_Ctrs_Pnd_Ctr_Gsra_457b.setValue(1);                                                                                                           //Natural: MOVE 1 TO #CTRS.#CTR-GSRA-457B
                        pnd_Ctr_Total_Gsra_457b.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTR-TOTAL-GSRA-457B
                        if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                               //Natural: IF #CREF-CNTRCT-NBR NE ' '
                        {
                            pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref_Il.setValue(1);                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GSRA-457B-CREF-IL
                            pnd_Ctr_Total_Gsra_457b_Cref_Il.nadd(1);                                                                                                      //Natural: ADD 1 TO #CTR-TOTAL-GSRA-457B-CREF-IL
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("004") || pnd_Input_Record_Pnd_Issue_State_Cde.equals("014")))                          //Natural: IF #ISSUE-STATE-CDE EQ '004' OR EQ '014'
                        {
                            pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nl.setValue(1);                                                                                                    //Natural: MOVE 1 TO #CTRS.#CTR-GSRA-457B-NL
                            pnd_Ctr_Total_Gsra_457b_Nl.nadd(1);                                                                                                           //Natural: ADD 1 TO #CTR-TOTAL-GSRA-457B-NL
                            if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                           //Natural: IF #CREF-CNTRCT-NBR NE ' '
                            {
                                pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref.setValue(1);                                                                                              //Natural: MOVE 1 TO #CTRS.#CTR-GSRA-457B-CREF
                                pnd_Ctr_Total_Gsra_457b_Cref.nadd(1);                                                                                                     //Natural: ADD 1 TO #CTR-TOTAL-GSRA-457B-CREF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("034")))                                                                            //Natural: IF #ISSUE-STATE-CDE EQ '034'
                            {
                                pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nh.setValue(1);                                                                                                //Natural: MOVE 1 TO #CTRS.#CTR-GSRA-457B-NH
                                pnd_Ctr_Total_Gsra_457b_Nh.nadd(1);                                                                                                       //Natural: ADD 1 TO #CTR-TOTAL-GSRA-457B-NH
                                if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                       //Natural: IF #CREF-CNTRCT-NBR NE ' '
                                {
                                    pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref.setValue(1);                                                                                          //Natural: MOVE 1 TO #CTRS.#CTR-GSRA-457B-CREF
                                    pnd_Ctr_Total_Gsra_457b_Cref.nadd(1);                                                                                                 //Natural: ADD 1 TO #CTR-TOTAL-GSRA-457B-CREF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ctrs_Pnd_Ctr_Gsra_457b.setValue(1);                                                                                                   //Natural: MOVE 1 TO #CTRS.#CTR-GSRA-457B
                                pnd_Ctr_Total_Gsra_457b.nadd(1);                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-GSRA-457B
                                if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                       //Natural: IF #CREF-CNTRCT-NBR NE ' '
                                {
                                    pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref.setValue(1);                                                                                          //Natural: MOVE 1 TO #CTRS.#CTR-GSRA-457B-CREF
                                    pnd_Ctr_Total_Gsra_457b_Cref.nadd(1);                                                                                                 //Natural: ADD 1 TO #CTR-TOTAL-GSRA-457B-CREF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                //*  060117 J.AVE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Ctr_Contract_Outof_Range.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CTR-CONTRACT-OUTOF-RANGE
                pnd_Remarks.setValue("CONTRACT OUT OF RANGE");                                                                                                            //Natural: ASSIGN #REMARKS := 'CONTRACT OUT OF RANGE'
                //*  PINEXP
                //*  PINEXP
                getReports().write(1, new ColumnSpacing(1),pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new                     //Natural: WRITE ( 1 ) 1X #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE-RECORD
    }
    //*  061107 J.AVE
    private void sub_Add_Contract_To_Array() throws Exception                                                                                                             //Natural: ADD-CONTRACT-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------
        pnd_K.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #K
        if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                               //Natural: IF #CREF-CNTRCT-NBR NE ' '
        {
            pnd_Output_Pnd_Contracts.getValue(pnd_K).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr, "/",                  //Natural: COMPRESS #TIAA-CNTRCT-NBR '/' #CREF-CNTRCT-NBR TO #CONTRACTS ( #K ) LEAVING NO SPACE
                pnd_Input_Record_Pnd_Cref_Cntrct_Nbr));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_Pnd_Contracts.getValue(pnd_K).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                      //Natural: MOVE #TIAA-CNTRCT-NBR TO #CONTRACTS ( #K )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-CONTRACT-TO-ARRAY
    }
    private void sub_Check_Record_In_Cor() throws Exception                                                                                                               //Natural: CHECK-RECORD-IN-COR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #MDMA211.#I-CONTRACT-NUMBER
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue("01");                                                                                                   //Natural: MOVE '01' TO #MDMA211.#I-PAYEE-CODE-A2
        //*    WRITE 'CALLING MDMN211A TO GET CONTRACT INFORMATION'
        //*  JCA20150324
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //*      WRITE '=' #MDMA210.#O-RETURN-CODE
        //*  JCA20150324
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA211.#O-RETURN-CODE NE '0000'
        {
            pnd_Remarks.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Text());                                                                                          //Natural: ASSIGN #REMARKS := #MDMA211.#O-RETURN-TEXT
            //*  PINEXP
            //*  PINEXP
            getReports().write(1, new ColumnSpacing(1),pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) 1X #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Contract_Not_In_Cor.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-DA-CONTRACT-NOT-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals(" ") || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("H")         //Natural: IF NOT ( #MDMA211.#O-CONTRACT-STATUS-CODE = ' ' OR = 'H' OR = 'Y' )
            || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("Y"))))
        {
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("CONTRACT INACTIVE IN COR - STATUS CODE:", pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code()));                    //Natural: COMPRESS 'CONTRACT INACTIVE IN COR - STATUS CODE:' #MDMA211.#O-CONTRACT-STATUS-CODE TO #REMARKS
            //*  PINEXP
            //*  PINEXP
            getReports().write(1, new ColumnSpacing(1),pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) 1X #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Inactive_In_Cor.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DA-INACTIVE-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PIN-STATUS
            sub_Determine_Pin_Status();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Accepted.getBoolean()))                                                                                                                     //Natural: IF #ACCEPTED
            {
                pnd_Valid_Record.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-RECORD-IN-COR
    }
    private void sub_Determine_Pin_Status() throws Exception                                                                                                              //Natural: DETERMINE-PIN-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------
        //*  PINEXP >>>   /* JCA20150324
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        //*    MOVE #INPUT-RECORD.#PIN TO #MDMA101.#I-PIN          /* JCA20150324
        if (condition(pnd_Input_Record_Pnd_Pin_A12.notEquals(" ")))                                                                                                       //Natural: IF #INPUT-RECORD.#PIN-A12 NE ' '
        {
            if (condition(pnd_Input_Record_Pnd_Pin_A12.getSubstring(8,1).equals(" ")))                                                                                    //Natural: IF SUBSTRING ( #INPUT-RECORD.#PIN-A12,8,1 ) = ' '
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N7);                                                                          //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #PIN-N7
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                         //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #PIN-N12
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP <<<
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "CALLING MDMN101A TO GET ADDRESS/NAME");                                                                                                    //Natural: WRITE 'CALLING MDMN101A TO GET ADDRESS/NAME'
        if (Global.isEscape()) return;
        //*  PINEXP       /* JCA20150324
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  REJECT DECEASED PARTICIPANTS
        //*  PINEXP     /* JCA20150324
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero())))                                                                                //Natural: IF #MDMA101.#O-DATE-OF-DEATH GT 0
        {
            pnd_Ctr_Ph_Deceased.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CTR-PH-DECEASED
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("P/H DECEASED - DOD:", pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death()));                                               //Natural: COMPRESS 'P/H DECEASED - DOD:' #MDMA101.#O-DATE-OF-DEATH TO #REMARKS
            //*  PINEXP
            getReports().write(1, new ColumnSpacing(1),pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) 1X #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Accepted.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #ACCEPTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  REJECT PARTICIPANTS W/ STOP ALL MAILING SUPPRESSION CODE
            //*  PINEXP
            //*  JCA20150324
            DbsUtil.examine(new ExamineSource(pdaMdma101.getPnd_Mdma101_Pnd_O_Mail_Code().getValue("*")), new ExamineSearch("01"), new ExamineGivingPosition(pnd_I));     //Natural: EXAMINE #MDMA101.#O-MAIL-CODE ( * ) FOR '01' GIVING POSITION #I
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I GT 0
            {
                pnd_Ctr_Ph_With_Mail_Suppression.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTR-PH-WITH-MAIL-SUPPRESSION
                pnd_Remarks.setValue(DbsUtil.compress("P/H MAIL SUPPRESSED - MAIL CODE: 01"));                                                                            //Natural: COMPRESS 'P/H MAIL SUPPRESSED - MAIL CODE: 01' TO #REMARKS
                //* PINEXP
                getReports().write(1, new ColumnSpacing(1),pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new                     //Natural: WRITE ( 1 ) 1X #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Accepted.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #ACCEPTED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Accepted.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #ACCEPTED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE-PIN-STATUS
    }
    private void sub_Write_Data_To_File() throws Exception                                                                                                                //Natural: WRITE-DATA-TO-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        //*    IF #CTR-TOTAL-CONTRACTS LE 2                      */ JCA20160715
        pnd_Output_Pnd_Package_Code.setValue("RDR2006T");                                                                                                                 //Natural: MOVE 'RDR2006T' TO #PACKAGE-CODE
        pnd_Ctr_File1.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #CTR-FILE1
        getWorkFiles().write(2, false, pnd_Output);                                                                                                                       //Natural: WRITE WORK FILE 2 #OUTPUT
        //*    ELSE                                              */ JCA20160715
        //*      MOVE 'RDR2006U' TO #PACKAGE-CODE                */ JCA20160715
        //*      ADD 1 TO #CTR-FILE2                             */ JCA20160715
        //*      WRITE WORK FILE 3 #OUTPUT                       */ JCA20160715
        //*    END-IF                                            */ JCA20160715
        //*  WRITE-DATA-TO-FILE
    }
    private void sub_Write_Data_To_Array() throws Exception                                                                                                               //Natural: WRITE-DATA-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ctr_Total_Contracts.compute(new ComputeParameters(false, pnd_Ctr_Total_Contracts), pnd_Ctrs_Pnd_Ctr_Ra_Sra_Gn.add(pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Gn).add(pnd_Ctrs_Pnd_Ctr_Ra_Sra_Il).add(pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Il).add(pnd_Ctrs_Pnd_Ctr_Ra_Sra_Cref_Fl).add(pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl)); //Natural: ASSIGN #CTR-TOTAL-CONTRACTS := #CTRS.#CTR-RA-SRA-GN + #CTRS.#CTR-GRA-GSRA-RS-GN + #CTRS.#CTR-RA-SRA-IL + #CTRS.#CTR-GRA-GSRA-RS-IL + #CTRS.#CTR-RA-SRA-CREF-FL + #CTRS.#CTR-GRA-GSRA-RS-CREF-FL
        //*  WRITE-DATA-TO-ARRAY
    }
    private void sub_Write_Statistics_Report() throws Exception                                                                                                           //Natural: WRITE-STATISTICS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ctr_Duplicate_Contract.nsubtract(1);                                                                                                                          //Natural: ASSIGN #CTR-DUPLICATE-CONTRACT := #CTR-DUPLICATE-CONTRACT - 1
        pnd_Ctr_Contract_Rejected.compute(new ComputeParameters(false, pnd_Ctr_Contract_Rejected), pnd_Ctr_Da_Contract_Not_In_Cor.add(pnd_Ctr_Da_Inactive_In_Cor).add(pnd_Ctr_Ph_Deceased).add(pnd_Ctr_Ph_With_Mail_Suppression).add(pnd_Ctr_Out_Of_State).add(pnd_Ctr_Contract_Outof_Range).add(pnd_Ctr_Plan_Not_For_Proc).add(pnd_Ctr_Duplicate_Contract)); //Natural: ASSIGN #CTR-CONTRACT-REJECTED := #CTR-DA-CONTRACT-NOT-IN-COR + #CTR-DA-INACTIVE-IN-COR + #CTR-PH-DECEASED + #CTR-PH-WITH-MAIL-SUPPRESSION + #CTR-OUT-OF-STATE + #CTR-CONTRACT-OUTOF-RANGE + #CTR-PLAN-NOT-FOR-PROC + #CTR-DUPLICATE-CONTRACT
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ         :",pnd_Ctr_Roth_Contract_Read);                                                                             //Natural: WRITE 'NUMBER OF RECORDS READ         :' #CTR-ROTH-CONTRACT-READ
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS ACCEPTED     :",pnd_Ctr_Contract_Accepted);                                                                              //Natural: WRITE 'NUMBER OF RECORDS ACCEPTED     :' #CTR-CONTRACT-ACCEPTED
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS REJECTED     :",pnd_Ctr_Contract_Rejected);                                                                              //Natural: WRITE 'NUMBER OF RECORDS REJECTED     :' #CTR-CONTRACT-REJECTED
        if (Global.isEscape()) return;
        getReports().write(0, "BREAKDOWN OF RECORDS REJECTED  :");                                                                                                        //Natural: WRITE 'BREAKDOWN OF RECORDS REJECTED  :'
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT NOT IN COR     :",pnd_Ctr_Da_Contract_Not_In_Cor);                                                                              //Natural: WRITE ' -CONTRACT NOT IN COR     :' #CTR-DA-CONTRACT-NOT-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT INACTIVE IN COR:",pnd_Ctr_Da_Inactive_In_Cor);                                                                                  //Natural: WRITE ' -CONTRACT INACTIVE IN COR:' #CTR-DA-INACTIVE-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H DECEASED            :",pnd_Ctr_Ph_Deceased);                                                                                         //Natural: WRITE ' -P/H DECEASED            :' #CTR-PH-DECEASED
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H W/ MAIL SUPPRESSION :",pnd_Ctr_Ph_With_Mail_Suppression);                                                                            //Natural: WRITE ' -P/H W/ MAIL SUPPRESSION :' #CTR-PH-WITH-MAIL-SUPPRESSION
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT OUT OF RANGE   :",pnd_Ctr_Contract_Outof_Range);                                                                                //Natural: WRITE ' -CONTRACT OUT OF RANGE   :' #CTR-CONTRACT-OUTOF-RANGE
        if (Global.isEscape()) return;
        getReports().write(0, " -PLAN NOT FOR PROCESSING :",pnd_Ctr_Plan_Not_For_Proc);                                                                                   //Natural: WRITE ' -PLAN NOT FOR PROCESSING :' #CTR-PLAN-NOT-FOR-PROC
        if (Global.isEscape()) return;
        getReports().write(0, " -DUPLICATE CONTRACT      :",pnd_Ctr_Duplicate_Contract);                                                                                  //Natural: WRITE ' -DUPLICATE CONTRACT      :' #CTR-DUPLICATE-CONTRACT
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY PRODUCT:");                                                                                                                       //Natural: WRITE 'TOTAL BY PRODUCT:'
        if (Global.isEscape()) return;
        getReports().write(0, " - RA/SRA      - GN :",pnd_Ctr_Total_Ra_Sra_Gn);                                                                                           //Natural: WRITE ' - RA/SRA      - GN :' #CTR-TOTAL-RA-SRA-GN
        if (Global.isEscape()) return;
        getReports().write(0, " - GRA/GSRA/RS - GN :",pnd_Ctr_Total_Gra_Gsra_Rs_Gn);                                                                                      //Natural: WRITE ' - GRA/GSRA/RS - GN :' #CTR-TOTAL-GRA-GSRA-RS-GN
        if (Global.isEscape()) return;
        getReports().write(0, " - RA/SRA      - IL :",pnd_Ctr_Total_Ra_Sra_Il);                                                                                           //Natural: WRITE ' - RA/SRA      - IL :' #CTR-TOTAL-RA-SRA-IL
        if (Global.isEscape()) return;
        getReports().write(0, " - GRA/GSRA/RS - IL :",pnd_Ctr_Total_Gra_Gsra_Rs_Il);                                                                                      //Natural: WRITE ' - GRA/GSRA/RS - IL :' #CTR-TOTAL-GRA-GSRA-RS-IL
        if (Global.isEscape()) return;
        getReports().write(0, " - RA/SRA      - FL :",pnd_Ctr_Total_Ra_Sra_Cref_Fl);                                                                                      //Natural: WRITE ' - RA/SRA      - FL :' #CTR-TOTAL-RA-SRA-CREF-FL
        if (Global.isEscape()) return;
        getReports().write(0, " - GRA/GSRA/RS - FL :",pnd_Ctr_Total_Gra_Gsra_Rs_Cref_Fl);                                                                                 //Natural: WRITE ' - GRA/GSRA/RS - FL :' #CTR-TOTAL-GRA-GSRA-RS-CREF-FL
        if (Global.isEscape()) return;
        getReports().write(0, " - GSRA-457B        :",pnd_Ctr_Total_Gsra_457b);                                                                                           //Natural: WRITE ' - GSRA-457B        :' #CTR-TOTAL-GSRA-457B
        if (Global.isEscape()) return;
        getReports().write(0, " - GSRA-457B CREF   :",pnd_Ctr_Total_Gsra_457b_Cref);                                                                                      //Natural: WRITE ' - GSRA-457B CREF   :' #CTR-TOTAL-GSRA-457B-CREF
        if (Global.isEscape()) return;
        getReports().write(0, " - GSRA-457B -AR/FL :",pnd_Ctr_Total_Gsra_457b_Nl);                                                                                        //Natural: WRITE ' - GSRA-457B -AR/FL :' #CTR-TOTAL-GSRA-457B-NL
        if (Global.isEscape()) return;
        getReports().write(0, " - GSRA-457B   - NH :",pnd_Ctr_Total_Gsra_457b_Nh);                                                                                        //Natural: WRITE ' - GSRA-457B   - NH :' #CTR-TOTAL-GSRA-457B-NH
        if (Global.isEscape()) return;
        getReports().write(0, " - GSRA-457B CREFIL :",pnd_Ctr_Total_Gsra_457b_Cref_Il);                                                                                   //Natural: WRITE ' - GSRA-457B CREFIL :' #CTR-TOTAL-GSRA-457B-CREF-IL
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY FILE:");                                                                                                                          //Natural: WRITE 'TOTAL BY FILE:'
        if (Global.isEscape()) return;
        getReports().write(0, " - FILE 1 : ",pnd_Ctr_File1,new ColumnSpacing(5),"(tri-folds)");                                                                           //Natural: WRITE ' - FILE 1 : ' #CTR-FILE1 5X '(tri-folds)'
        if (Global.isEscape()) return;
        getReports().write(0, " - FILE 2 : ",pnd_Ctr_File2,new ColumnSpacing(5),"(flats)");                                                                               //Natural: WRITE ' - FILE 2 : ' #CTR-FILE2 5X '(flats)'
        if (Global.isEscape()) return;
        //*  WRITE-STATISTICS-REPORT
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Input_Record_Pnd_Pin_A12IsBreak = pnd_Input_Record_Pnd_Pin_A12.isBreak(endOfData);
        if (condition(pnd_Input_Record_Pnd_Pin_A12IsBreak))
        {
            pnd_Ctr_Total_Contracts.compute(new ComputeParameters(false, pnd_Ctr_Total_Contracts), pnd_Ctrs_Pnd_Ctr_Ra_Sra_Gn.add(pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Gn).add(pnd_Ctrs_Pnd_Ctr_Ra_Sra_Il).add(pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Il).add(pnd_Ctrs_Pnd_Ctr_Ra_Sra_Cref_Fl).add(pnd_Ctrs_Pnd_Ctr_Gsra_457b).add(pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl).add(pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref).add(pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nl).add(pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nh).add(pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref_Il)); //Natural: ASSIGN #CTR-TOTAL-CONTRACTS := #CTRS.#CTR-RA-SRA-GN + #CTRS.#CTR-GRA-GSRA-RS-GN + #CTRS.#CTR-RA-SRA-IL + #CTRS.#CTR-GRA-GSRA-RS-IL + #CTRS.#CTR-RA-SRA-CREF-FL + #CTRS.#CTR-GSRA-457B + #CTRS.#CTR-GRA-GSRA-RS-CREF-FL + #CTRS.#CTR-GSRA-457B-CREF + #CTRS.#CTR-GSRA-457B-NL + #CTRS.#CTR-GSRA-457B-NH + #CTRS.#CTR-GSRA-457B-CREF-IL
            if (condition(pnd_Ctr_Total_Contracts.greater(getZero())))                                                                                                    //Natural: IF #CTR-TOTAL-CONTRACTS GT 0
            {
                //*  PINEXP
                pnd_Output_Pnd_Pin_A12.setValue(pnd_Prev_Pin_A12);                                                                                                        //Natural: MOVE #PREV-PIN-A12 TO #OUTPUT.#PIN-A12
                pnd_Output_Pnd_Ctr_Ra_Sra_Gn.setValue(pnd_Ctrs_Pnd_Ctr_Ra_Sra_Gn);                                                                                        //Natural: MOVE #CTRS.#CTR-RA-SRA-GN TO #OUTPUT.#CTR-RA-SRA-GN
                pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Gn.setValue(pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Gn);                                                                              //Natural: MOVE #CTRS.#CTR-GRA-GSRA-RS-GN TO #OUTPUT.#CTR-GRA-GSRA-RS-GN
                pnd_Output_Pnd_Ctr_Ra_Sra_Il.setValue(pnd_Ctrs_Pnd_Ctr_Ra_Sra_Il);                                                                                        //Natural: MOVE #CTRS.#CTR-RA-SRA-IL TO #OUTPUT.#CTR-RA-SRA-IL
                pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Il.setValue(pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Il);                                                                              //Natural: MOVE #CTRS.#CTR-GRA-GSRA-RS-IL TO #OUTPUT.#CTR-GRA-GSRA-RS-IL
                pnd_Output_Pnd_Ctr_Ra_Sra_Cref_Fl.setValue(pnd_Ctrs_Pnd_Ctr_Ra_Sra_Cref_Fl);                                                                              //Natural: MOVE #CTRS.#CTR-RA-SRA-CREF-FL TO #OUTPUT.#CTR-RA-SRA-CREF-FL
                pnd_Output_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl.setValue(pnd_Ctrs_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl);                                                                    //Natural: MOVE #CTRS.#CTR-GRA-GSRA-RS-CREF-FL TO #OUTPUT.#CTR-GRA-GSRA-RS-CREF-FL
                pnd_Output_Pnd_Ctr_Gsra_457b.setValue(pnd_Ctrs_Pnd_Ctr_Gsra_457b);                                                                                        //Natural: MOVE #CTRS.#CTR-GSRA-457B TO #OUTPUT.#CTR-GSRA-457B
                pnd_Output_Pnd_Ctr_Gsra_457b_Cref.setValue(pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref);                                                                              //Natural: MOVE #CTRS.#CTR-GSRA-457B-CREF TO #OUTPUT.#CTR-GSRA-457B-CREF
                pnd_Output_Pnd_Ctr_Gsra_457b_Nl.setValue(pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nl);                                                                                  //Natural: MOVE #CTRS.#CTR-GSRA-457B-NL TO #OUTPUT.#CTR-GSRA-457B-NL
                pnd_Output_Pnd_Ctr_Gsra_457b_Nh.setValue(pnd_Ctrs_Pnd_Ctr_Gsra_457b_Nh);                                                                                  //Natural: MOVE #CTRS.#CTR-GSRA-457B-NH TO #OUTPUT.#CTR-GSRA-457B-NH
                pnd_Output_Pnd_Ctr_Gsra_457b_Cref_Il.setValue(pnd_Ctrs_Pnd_Ctr_Gsra_457b_Cref_Il);                                                                        //Natural: MOVE #CTRS.#CTR-GSRA-457B-CREF-IL TO #OUTPUT.#CTR-GSRA-457B-CREF-IL
                pnd_Output_Pnd_Letter_Type.setValue(pnd_Prev_Cover);                                                                                                      //Natural: MOVE #PREV-COVER TO #OUTPUT.#LETTER-TYPE
                pnd_Output_Pnd_Institution_Name.setValue(pnd_Prev_Inst_Name);                                                                                             //Natural: MOVE #PREV-INST-NAME TO #OUTPUT.#INSTITUTION-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-DATA-TO-FILE
                sub_Write_Data_To_File();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ctrs.reset();                                                                                                                                             //Natural: RESET #CTRS #CTR-TOTAL-CONTRACTS
            pnd_Ctr_Total_Contracts.reset();
            pnd_Output.reset();                                                                                                                                           //Natural: RESET #OUTPUT #K
            pnd_K.reset();
            pnd_Break.setValue(true);                                                                                                                                     //Natural: MOVE TRUE TO #BREAK
            //*  PINEXP
            pnd_Prev_Pin_A12.setValue(pnd_Input_Record_Pnd_Pin_A12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-A12 TO #PREV-PIN-A12
            pnd_Prev_Cover.setValue(pnd_Input_Record_Pnd_Cover_Letter_Ind);                                                                                               //Natural: MOVE #INPUT-RECORD.#COVER-LETTER-IND TO #PREV-COVER
            pnd_Prev_Plan_Cde.setValue(pnd_Input_Record_Pnd_Plan_Cde);                                                                                                    //Natural: MOVE #INPUT-RECORD.#PLAN-CDE TO #PREV-PLAN-CDE
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Inst_Name);                                                                                                  //Natural: MOVE #INPUT-RECORD.#INST-NAME TO #PREV-INST-NAME
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=120 PS=60");
        Global.format(0, "LS=80 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(43),"ROTH RIDER MAILING",new 
            TabSetting(84),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(43),"EXCEPTIONS LIST",new TabSetting(84),"TIME:",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,new TabSetting(84),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new TabSetting(2),"PIN            TIAA NBR   STATE   REMARKS",NEWLINE,new 
            TabSetting(2),"------------------------------------------------------------",NEWLINE);
    }
}
