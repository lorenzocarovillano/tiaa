/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:21:40 PM
**        * FROM NATURAL PROGRAM : Ridb414
************************************************************
**        * FILE NAME            : Ridb414.java
**        * CLASS NAME           : Ridb414
**        * INSTANCE NAME        : Ridb414
************************************************************
************************************************************************
** PROGRAM  : RIDB414                                                 **
** SYSTEM   : RIDER                                                   **
** TITLE    : COMPUSET DATA GENERATION MODULE                         **
** PURPOSE  : THIS MODULE CREATES A SEQUENTIAL FILE CONTAINING        **
**          : COMPUSET DATA FOR MAP PROCESSING OF THE 2006 TIAA ACCESS**
**          : ENDORSEMENT MAILINGS. IT ALSO GENERATES STATISTICS      **
**          : REPORTS.                                                **
**                                                                    **
** HISTORY                                                            **
**                                                                    **
**   WHO         WHEN                    WHY                          **
** --------    -------  --------------------------------------------- **
** J.AVE       10/2006  INITIAL IMPLEMENTATION                        **
** J.AVE       06/2007  RE-STOW DUE TO ADDL FIELD #CONTRACTS IN PDA   **
**                      RIDA414                                       **
** J.AVE       09/2009  RE-STOW DUE TO ADDL CTR FIELDS FOR PR & NV    **
** J.AVE       04/2011  RE-STOW DUE TO ADDL E-MAIL-ADDRESS FIELD      **
** L.SHU       05/2017  PIN EXPANSION PROJECT              PINEXP     **
**                      TO SPECIFY OCC FOR #FIRST-PST (1) AND         **
**                      #LAST-PST(1) - FIX NATURAL ERROR NAT0281      **
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridb414 extends BLNatBase
{
    // Data Areas
    private LdaPstl9902 ldaPstl9902;
    private PdaPsta9200 pdaPsta9200;
    private LdaRidl412 ldaRidl412;
    private LdaRidl4040 ldaRidl4040;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_File_No_Ctr;

    private DbsGroup constants;
    private DbsField constants_Pnd_Blank;
    private DbsField constants_Pnd_Error_Ind;

    private DbsGroup counters;
    private DbsField counters_Pnd_Cnt_Record_Read;
    private DbsField counters_Pnd_Cnt_Record_Write_Max;
    private DbsField counters_Pnd_Cnt_Print;
    private DbsField counters_Pnd_Cnt_Divert;
    private DbsField counters_Pnd_Cnt_Total;
    private DbsField counters_Pnd_L;
    private DbsField counters_Pnd_I;
    private DbsField counters_Pnd_J;

    private DbsGroup counters__R_Field_1;
    private DbsField counters_Pnd_J1;
    private DbsField counters_Pnd_M;
    private DbsField counters_Pnd_N;
    private DbsField counters_Pnd_Work_Idx;
    private DbsField counters_Pnd_Wk8_Count;
    private DbsField pnd_Mail_Item_Ctr;
    private DbsField pnd_Already_Done;
    private DbsField pnd_First_Call;
    private DbsField pnd_Pst_Tbl_Data_Field2;

    private DbsGroup pnd_Pst_Tbl_Data_Field2__R_Field_2;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4;
    private DbsField pls_Current_Mail_Id;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaPstl9902 = new LdaPstl9902();
        registerRecord(ldaPstl9902);
        localVariables = new DbsRecord();
        pdaPsta9200 = new PdaPsta9200(localVariables);
        ldaRidl412 = new LdaRidl412();
        registerRecord(ldaRidl412);
        ldaRidl4040 = new LdaRidl4040();
        registerRecord(ldaRidl4040);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);

        // Local Variables
        pnd_File_No_Ctr = localVariables.newFieldInRecord("pnd_File_No_Ctr", "#FILE-NO-CTR", FieldType.PACKED_DECIMAL, 7);

        constants = localVariables.newGroupInRecord("constants", "CONSTANTS");
        constants_Pnd_Blank = constants.newFieldInGroup("constants_Pnd_Blank", "#BLANK", FieldType.STRING, 1);
        constants_Pnd_Error_Ind = constants.newFieldInGroup("constants_Pnd_Error_Ind", "#ERROR-IND", FieldType.STRING, 1);

        counters = localVariables.newGroupInRecord("counters", "COUNTERS");
        counters_Pnd_Cnt_Record_Read = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Read", "#CNT-RECORD-READ", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Write_Max = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Write_Max", "#CNT-RECORD-WRITE-MAX", FieldType.PACKED_DECIMAL, 
            7);
        counters_Pnd_Cnt_Print = counters.newFieldInGroup("counters_Pnd_Cnt_Print", "#CNT-PRINT", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Divert = counters.newFieldInGroup("counters_Pnd_Cnt_Divert", "#CNT-DIVERT", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Total = counters.newFieldInGroup("counters_Pnd_Cnt_Total", "#CNT-TOTAL", FieldType.NUMERIC, 7);
        counters_Pnd_L = counters.newFieldInGroup("counters_Pnd_L", "#L", FieldType.NUMERIC, 2);
        counters_Pnd_I = counters.newFieldInGroup("counters_Pnd_I", "#I", FieldType.NUMERIC, 2);
        counters_Pnd_J = counters.newFieldInGroup("counters_Pnd_J", "#J", FieldType.NUMERIC, 2);

        counters__R_Field_1 = counters.newGroupInGroup("counters__R_Field_1", "REDEFINE", counters_Pnd_J);
        counters_Pnd_J1 = counters__R_Field_1.newFieldInGroup("counters_Pnd_J1", "#J1", FieldType.STRING, 2);
        counters_Pnd_M = counters.newFieldInGroup("counters_Pnd_M", "#M", FieldType.PACKED_DECIMAL, 3);
        counters_Pnd_N = counters.newFieldInGroup("counters_Pnd_N", "#N", FieldType.PACKED_DECIMAL, 3);
        counters_Pnd_Work_Idx = counters.newFieldInGroup("counters_Pnd_Work_Idx", "#WORK-IDX", FieldType.INTEGER, 2);
        counters_Pnd_Wk8_Count = counters.newFieldInGroup("counters_Pnd_Wk8_Count", "#WK8-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Mail_Item_Ctr = localVariables.newFieldInRecord("pnd_Mail_Item_Ctr", "#MAIL-ITEM-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Already_Done = localVariables.newFieldInRecord("pnd_Already_Done", "#ALREADY-DONE", FieldType.BOOLEAN, 1);
        pnd_First_Call = localVariables.newFieldInRecord("pnd_First_Call", "#FIRST-CALL", FieldType.BOOLEAN, 1);
        pnd_Pst_Tbl_Data_Field2 = localVariables.newFieldInRecord("pnd_Pst_Tbl_Data_Field2", "#PST-TBL-DATA-FIELD2", FieldType.STRING, 253);

        pnd_Pst_Tbl_Data_Field2__R_Field_2 = localVariables.newGroupInRecord("pnd_Pst_Tbl_Data_Field2__R_Field_2", "REDEFINE", pnd_Pst_Tbl_Data_Field2);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory = pnd_Pst_Tbl_Data_Field2__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory", "#MM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1 = pnd_Pst_Tbl_Data_Field2__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1", "#MM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2 = pnd_Pst_Tbl_Data_Field2__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2", "#MM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3 = pnd_Pst_Tbl_Data_Field2__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3", "#MM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4 = pnd_Pst_Tbl_Data_Field2__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4", "#MM-TITLE4", 
            FieldType.STRING, 50);
        pls_Current_Mail_Id = WsIndependent.getInstance().newFieldInRecord("pls_Current_Mail_Id", "+CURRENT-MAIL-ID", FieldType.STRING, 11);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaPstl9902.initializeValues();
        ldaRidl412.initializeValues();
        ldaRidl4040.initializeValues();

        localVariables.reset();
        pnd_File_No_Ctr.setInitialValue(1);
        constants_Pnd_Blank.setInitialValue(" ");
        constants_Pnd_Error_Ind.setInitialValue("E");
        pnd_Already_Done.setInitialValue(false);
        pls_Trace.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridb414() throws Exception
    {
        super("Ridb414");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDB414", onError);
        setupReports();
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
        //*  ----------------------------------------------------------------------                                                                                       //Natural: ON ERROR
        pnd_File_No_Ctr.setValue(1);                                                                                                                                      //Natural: ASSIGN #FILE-NO-CTR := 1
        //*  CALLNAT 'PSTN9670' PSTA9670          /* DETERMINE ENVIRONMENT
                                                                                                                                                                          //Natural: PERFORM GET-SIGNATORY
        sub_Get_Signatory();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-INPUT-PARMS
        sub_Get_Input_Parms();
        if (condition(Global.isEscape())) {return;}
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 21T '2006 TIAA ACCESS ENDORSEMENT MAILING' 65T 'DATE:' *DATU / 30T 'BATCH CONTROL TOTALS' 65T 'TIME:' *TIME ( EM = XXXXXXXX ) / 65T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT 'PROGRAM:' *PROGRAM 21T '2006 TIAA ACCESS ENDORSEMENT MAILING' 65T 'DATE:' *DATU / 30T 'BATCH CONTROL DETAILS' 65T 'TIME:' *TIME ( EM = XXXXXXXX ) / 65T 'PAGE:' 3X *PAGE-NUMBER ( 2 ) // 27T 'PACKAGE CODE :  ' INPUT-PARMS.#PCKGE-CDE / / 10T '                                                        ADDR' / 10T ' MAIL ITEM              FULL NAME                 PIN   TYPE' / 10T '----------- ----------------------------------- ------- ----'
        //*          'CARTRIDGE NO :  ' #FILE-NO-CTR
        pnd_First_Call.setValue(true);                                                                                                                                    //Natural: ASSIGN #FIRST-CALL := TRUE
        //*  ----------------------------------------------------------------------
        //*                  S T A R T    O F    M A I N    L O O P
        //*  ----------------------------------------------------------------------
        pnd_Mail_Item_Ctr.reset();                                                                                                                                        //Natural: RESET #MAIL-ITEM-CTR
        //*  RIDL412
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #POST-DATA
        while (condition(getWorkFiles().read(1, ldaRidl412.getPnd_Post_Data())))
        {
            //*  FIRST AND LAST PST PER FILE.
            //*  PINEXP
            if (condition(pnd_First_Call.getBoolean()))                                                                                                                   //Natural: IF #FIRST-CALL
            {
                //*    #FIRST-PST := #POST-DATA.#PST-RQST-ID
                ldaRidl4040.getPnd_First_Pst().getValue(1).setValue(ldaRidl412.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                       //Natural: ASSIGN #FIRST-PST ( 1 ) := #POST-DATA.#PST-RQST-ID
                pnd_First_Call.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-CALL := FALSE
                //*  PINEXP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    #LAST-PST  := #POST-DATA.#PST-RQST-ID
                ldaRidl4040.getPnd_Last_Pst().getValue(1).setValue(ldaRidl412.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                        //Natural: ASSIGN #LAST-PST ( 1 ) := #POST-DATA.#PST-RQST-ID
            }                                                                                                                                                             //Natural: END-IF
            counters_Pnd_Cnt_Record_Read.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CNT-RECORD-READ
            //*  IF +TRACE
            //*    WRITE (0) '-' (131) /
            //*      *PROGRAM 'READ ITEM RQST:' #POST-DATA.#PST-RQST-ID
            //*  END-IF
            //*  NOW GET THE STANDARD PACKAGE
            //*  IF PSTA7205.PCKGE-CDE NE #POST-DATA.#PACKAGE-CODE
            //*    PSTA7205.PCKGE-CDE        := #POST-DATA.#PACKAGE-CODE
            //*    CDAOBJ.#FUNCTION          := 'GET'
            //*    IF PSTA9670.ENVIRONMENT = 'PROD'
            //*      PSTA7205.PCKGE-VRSN-CDE := 'P'
            //*    ELSE
            //*      PSTA7205.PCKGE-VRSN-CDE := 'D'
            //*    END-IF
            //*    CALLNAT 'PSTN7205'   /* GET PST-STNDRD-PCKGE     GETS THE GEN MODULE
            //*      PSTA7205
            //*      PSTA7205-ID
            //*      PSTA7206
            //*      CDAOBJ
            //*      DIALOG-INFO-SUB
            //*      MSG-INFO-SUB
            //*      PASS-SUB
            //*    IF MSG-INFO-SUB.##RETURN-CODE = #ERROR-IND
            //*      PERFORM TERMINATE-JOB
            //*    END-IF
            //*  END-IF
            //*  MAKE CALL TO GENERATION MODULE TO GENERATE DATA
                                                                                                                                                                          //Natural: PERFORM CALL-GEN-MODULE
            sub_Call_Gen_Module();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  PINEXP
            getReports().write(2, new TabSetting(10),ldaRidl412.getPnd_Post_Data_Pnd_Pst_Rqst_Id(),new TabSetting(22),ldaRidl412.getPnd_Post_Data_Pnd_Full_Name(),        //Natural: WRITE ( 2 ) 10T #POST-DATA.#PST-RQST-ID 22T #POST-DATA.#FULL-NAME ( AL = 33 ) 57T #POST-DATA.#PIN-NUMBER 73T #POST-DATA.#ADDRESS-TYPE-CDE
                new AlphanumericLength (33),new TabSetting(57),ldaRidl412.getPnd_Post_Data_Pnd_Pin_Number(),new TabSetting(73),ldaRidl412.getPnd_Post_Data_Pnd_Address_Type_Cde());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Mail_Item_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #MAIL-ITEM-CTR
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        counters_Pnd_Cnt_Total.compute(new ComputeParameters(false, counters_Pnd_Cnt_Total), counters_Pnd_Cnt_Total.add(counters_Pnd_Cnt_Print).add(counters_Pnd_Cnt_Divert)); //Natural: ADD #CNT-PRINT #CNT-DIVERT TO #CNT-TOTAL
        if (condition(counters_Pnd_Cnt_Total.notEquals(getZero())))                                                                                                       //Natural: IF #CNT-TOTAL NE 0
        {
            getReports().write(1, "RECORDS READ        : ",counters_Pnd_Cnt_Record_Read, new ReportEditMask ("Z,ZZZ,ZZ9"));                                               //Natural: WRITE ( 1 ) 'RECORDS READ        : ' #CNT-RECORD-READ ( EM = Z,ZZZ,ZZ9 )
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE," ");                                                                                                                           //Natural: WRITE ( 1 ) / ' '
            if (Global.isEscape()) return;
            //*  NAT0281
            getReports().write(1, NEWLINE,"PACKAGE CODE    : ",ldaRidl4040.getInput_Parms_Pnd_Pckge_Cde(),new TabSetting(45),"FIRST MAIL ITEM : ",ldaRidl4040.getPnd_First_Pst().getValue(1)); //Natural: WRITE ( 1 ) / 'PACKAGE CODE    : ' #PCKGE-CDE 45T 'FIRST MAIL ITEM : ' #FIRST-PST ( 1 )
            if (Global.isEscape()) return;
            //*  NAT0281
            getReports().write(1, NEWLINE,"PACKAGE         :  2006 TIAA ACCESS MAILING",new TabSetting(45),"LAST MAIL ITEM  : ",ldaRidl4040.getPnd_Last_Pst().getValue(1)); //Natural: WRITE ( 1 ) / 'PACKAGE         :  2006 TIAA ACCESS MAILING' 45T 'LAST MAIL ITEM  : ' #LAST-PST ( 1 )
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE," ");                                                                                                                           //Natural: WRITE ( 1 ) / ' '
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE," ");                                                                                                                           //Natural: WRITE ( 1 ) / ' '
            if (Global.isEscape()) return;
            getReports().write(1, new TabSetting(18),"SENT FOR PRINTING          : ",counters_Pnd_Cnt_Print, new ReportEditMask ("ZZZ,ZZ9"));                             //Natural: WRITE ( 1 ) 18T 'SENT FOR PRINTING          : ' #CNT-PRINT ( EM = ZZZ,ZZ9 )
            if (Global.isEscape()) return;
            getReports().write(1, new TabSetting(18),"SENT,DIVERT,FOREIGN ADDRESS: ",counters_Pnd_Cnt_Divert, new ReportEditMask ("ZZZ,ZZ9"));                            //Natural: WRITE ( 1 ) 18T 'SENT,DIVERT,FOREIGN ADDRESS: ' #CNT-DIVERT ( EM = ZZZ,ZZ9 )
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE," ");                                                                                                                           //Natural: WRITE ( 1 ) / ' '
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE," ");                                                                                                                           //Natural: WRITE ( 1 ) / ' '
            if (Global.isEscape()) return;
            getReports().write(1, new TabSetting(18),"TOTAL NUMBER OF MAIL ITEMS : ",counters_Pnd_Cnt_Total, new ReportEditMask ("ZZZ,ZZ9"));                             //Natural: WRITE ( 1 ) 18T 'TOTAL NUMBER OF MAIL ITEMS : ' #CNT-TOTAL ( EM = ZZZ,ZZ9 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-FOOTER
        sub_Write_Footer();
        if (condition(Global.isEscape())) {return;}
        //*  ----------------------------------------------------------------------
        //*                    E N D    O F    P R O C E S S I N G
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*                 S T A R T    O F    S U B R O U T I N E S
        //*  ----------------------------------------------------------------------
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SIGNATORY
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-INPUT-PARMS
        //*  ---------------------------------
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-GEN-MODULE
        //*  ------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADING
        //*  ------------------------------
        //*  -----------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FOOTER
        //*  -----------------------------
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-JOB
        //*  ----------------------------------------------------------------------
        //*                   E N D    O F    S U B R O U T I N E S
        //*  ----------------------------------------------------------------------
    }
    private void sub_Get_Signatory() throws Exception                                                                                                                     //Natural: GET-SIGNATORY
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 3 #PST-TBL-DATA-FIELD2
        while (condition(getWorkFiles().read(3, pnd_Pst_Tbl_Data_Field2)))
        {
            getReports().write(0, "READING SIGNATORY WORK FILE");                                                                                                         //Natural: WRITE 'READING SIGNATORY WORK FILE'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory);                                                                                          //Natural: WRITE '=' #MM-SIGNATORY
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1);                                                                                             //Natural: WRITE '=' #MM-TITLE1
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2);                                                                                             //Natural: WRITE '=' #MM-TITLE2
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3);                                                                                             //Natural: WRITE '=' #MM-TITLE3
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4);                                                                                             //Natural: WRITE '=' #MM-TITLE4
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*  GET-SIGNATORY
    }
    //*  FROM  RIDL4040
    private void sub_Get_Input_Parms() throws Exception                                                                                                                   //Natural: GET-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("RIDB414|sub_Get_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, ldaRidl4040.getInput_Parms_Pnd_Pckge_Cde());                                                           //Natural: INPUT INPUT-PARMS.#PCKGE-CDE
                DbsUtil.invokeInput(setInputStatus(INPUT_2), this, ldaRidl4040.getInput_Parms_Pnd_Start_Rqst_Id());                                                       //Natural: INPUT INPUT-PARMS.#START-RQST-ID
                DbsUtil.invokeInput(setInputStatus(INPUT_3), this, ldaRidl4040.getInput_Parms_Pnd_End_Rqst_Id());                                                         //Natural: INPUT INPUT-PARMS.#END-RQST-ID
                DbsUtil.invokeInput(setInputStatus(INPUT_4), this, ldaRidl4040.getInput_Parms_Pnd_Trace());                                                               //Natural: INPUT INPUT-PARMS.#TRACE
                //*  WRITE OUT PARAMTETERS
                getReports().write(0, "Input Parameters for",Global.getPROGRAM(),NEWLINE,NEWLINE,"    Package Code: ",ldaRidl4040.getInput_Parms_Pnd_Pckge_Cde(),NEWLINE,"Start Request ID: ",ldaRidl4040.getInput_Parms_Pnd_Start_Rqst_Id(),NEWLINE,"  End Request ID: ",ldaRidl4040.getInput_Parms_Pnd_End_Rqst_Id(),NEWLINE,NEWLINE,"  VOLSER: __________________________",NEWLINE,"   Trace Program: ",ldaRidl4040.getInput_Parms_Pnd_Trace(),  //Natural: WRITE ( 0 ) 'Input Parameters for' *PROGRAM // '    Package Code: ' INPUT-PARMS.#PCKGE-CDE / 'Start Request ID: ' INPUT-PARMS.#START-RQST-ID / '  End Request ID: ' INPUT-PARMS.#END-RQST-ID // '  VOLSER: __________________________' / '   Trace Program: ' INPUT-PARMS.#TRACE ( EM = NO/YES ) /
                    new ReportEditMask ("NO/YES"),NEWLINE);
                if (Global.isEscape()) return;
                if (condition(ldaRidl4040.getInput_Parms_Pnd_Pckge_Cde().equals(" ") && ldaRidl4040.getInput_Parms_Pnd_Start_Rqst_Id().equals(" ")))                      //Natural: IF INPUT-PARMS.#PCKGE-CDE = ' ' AND INPUT-PARMS.#START-RQST-ID = ' '
                {
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("PACKAGE CODE OR START/END REQUEST ID MUST BE SUPPLIED");                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG := 'PACKAGE CODE OR START/END REQUEST ID MUST BE SUPPLIED'
                }                                                                                                                                                         //Natural: END-IF
                pls_Trace.setValue(ldaRidl4040.getInput_Parms_Pnd_Trace().getBoolean());                                                                                  //Natural: ASSIGN +TRACE := INPUT-PARMS.#TRACE
                //*  GET-INPUT-PARMS
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Call_Gen_Module() throws Exception                                                                                                                   //Natural: CALL-GEN-MODULE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        //*  THIS MODULE INVOKES THE DATA GENERATION MODULE.
        //*  PACKAGE INFO PSTA7205 COMES FROM POST 'Maintain Packages' FUNCTION.
        //*  PSTA9531.GEN-PROGRAM        := PSTA7205.PCKGE-CSF-DTA-GNRTN-MDLE
        //*  PSTA9531.PCKGE-CDE          := PSTA7205.PCKGE-CDE
        //*  PSTA9531.PCKGE-VRSN-CDE     := PSTA7205.PCKGE-VRSN-CDE
        //*  IF +TRACE
        //*    WRITE '='  PSTA9531.GEN-PROGRAM
        //*          '='  PSTA9531.PCKGE-CDE
        //*          '='  PSTA9531.PCKGE-VRSN-CDE
        //*  END-IF
        //*  CALLNAT PSTA9531.GEN-PROGRAM
        //*    PSTA9531
        //*    #POST-DATA
        //*    PSTA9670
        //*    MSG-INFO-SUB
        DbsUtil.callnat(Ridn414.class , getCurrentProcessState(), ldaRidl412.getPnd_Post_Data(), pdaCwfpda_M.getMsg_Info_Sub(), pnd_Pst_Tbl_Data_Field2);                 //Natural: CALLNAT 'RIDN414' #POST-DATA MSG-INFO-SUB #PST-TBL-DATA-FIELD2
        if (condition(Global.isEscape())) return;
        //* **
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals(constants_Pnd_Error_Ind)))                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = #ERROR-IND
        {
                                                                                                                                                                          //Natural: PERFORM TERMINATE-JOB
            sub_Terminate_Job();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*    IF #POST-DATA.#ADDRESS-TYPE-CDE = 'U'
        counters_Pnd_Cnt_Print.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #CNT-PRINT
        //*    ELSE
        //*      ADD 1 TO #CNT-DIVERT
        //*    END-IF
    }
    private void sub_Write_Heading() throws Exception                                                                                                                     //Natural: WRITE-HEADING
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------
        getReports().write(2, "PROGRAM:",Global.getPROGRAM(),new TabSetting(23),"2006 TIAA ACCESS ENDOSERMENTS MAILING",new TabSetting(65),"DATE:",Global.getDATU(),NEWLINE,new  //Natural: WRITE ( 2 ) 'PROGRAM:' *PROGRAM 23T '2006 TIAA ACCESS ENDOSERMENTS MAILING' 65T 'DATE:' *DATU / 33T 'BATCH DETAILS' 65T 'TIME:' *TIME ( EM = XXXXXXXX ) / 65T 'PAGE:' 3X *PAGE-NUMBER ( 2 ) /
            TabSetting(33),"BATCH DETAILS",new TabSetting(65),"TIME:",Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,new TabSetting(65),"PAGE:",new 
            ColumnSpacing(3),getReports().getPageNumberDbs(2),NEWLINE);
        if (Global.isEscape()) return;
        //*  ------------------------------
        //*  WRITE-HEADING
    }
    private void sub_Write_Footer() throws Exception                                                                                                                      //Natural: WRITE-FOOTER
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------
        getReports().write(2, NEWLINE," ",NEWLINE);                                                                                                                       //Natural: WRITE ( 2 ) / ' ' /
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(10),"Total Number of Mail Items : ",pnd_Mail_Item_Ctr, new ReportEditMask ("ZZZ,ZZ9"));                                      //Natural: WRITE ( 2 ) 10T 'Total Number of Mail Items : ' #MAIL-ITEM-CTR ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* **                                           #CNT-TOTAL(#I)(EM=ZZZ,ZZ9)
        //*  -----------------------------
        //*  WRITE-FOOTER
    }
    private void sub_Terminate_Job() throws Exception                                                                                                                     //Natural: TERMINATE-JOB
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("RIDB414|sub_Terminate_Job");
        while(true)
        {
            try
            {
                //*  -------------------------------
                //*  TERMINATE JOB DUE TO AN ERROR.
                //*  NOTE:  THERE IS NO RETURN FROM THIS SUBROUTINE
                //*  EXPAND ##MSG TO MAKE IT READABLE.
                DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
                if (condition(Global.isEscape())) return;
                DbsUtil.invokeInput(setInputStatus(INPUT_5), this, new FieldAttributes ("AD=O"),"-",new RepeatItem(131),NEWLINE,NEWLINE,"Job terminated due to the following error(s):", //Natural: INPUT ( AD = O ) '-' ( 131 ) // 'Job terminated due to the following error(s):' / MSG-INFO-SUB.##MSG // 'Check list of errors above.' / 'No output has been produced.' /
                    NEWLINE,pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),NEWLINE,NEWLINE,"Check list of errors above.",NEWLINE,"No output has been produced.",
                    NEWLINE);
                ldaRidl4040.getError_Handler_Fields_Pnd_Error_Nr().reset();                                                                                               //Natural: RESET #ERROR-NR #ERROR-LINE #ERROR-LEVEL
                ldaRidl4040.getError_Handler_Fields_Pnd_Error_Line().reset();
                ldaRidl4040.getError_Handler_Fields_Pnd_Error_Level().reset();
                //*  WILL TERMINATE WITH COND CODE 4
                Global.getSTACK().pushData(StackOption.TOP, ldaRidl4040.getError_Handler_Fields_Pnd_Error_Nr(), ldaRidl4040.getError_Handler_Fields_Pnd_Error_Line(),     //Natural: FETCH 'INFP9000' #ERROR-NR #ERROR-LINE #ERROR-STATUS #ERROR-PROGRAM #ERROR-LEVEL #ERROR-APPL
                    ldaRidl4040.getError_Handler_Fields_Pnd_Error_Status(), ldaRidl4040.getError_Handler_Fields_Pnd_Error_Program(), ldaRidl4040.getError_Handler_Fields_Pnd_Error_Level(), 
                    ldaRidl4040.getError_Handler_Fields_Pnd_Error_Appl());
                Global.setFetchProgram(DbsUtil.getBlType("INFP9000"));
                if (condition(Global.isEscape())) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(21),"2006 TIAA ACCESS ENDORSEMENT MAILING",new 
            TabSetting(65),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(30),"BATCH CONTROL TOTALS",new TabSetting(65),"TIME:",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,new TabSetting(65),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(21),"2006 TIAA ACCESS ENDORSEMENT MAILING",new 
            TabSetting(65),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(30),"BATCH CONTROL DETAILS",new TabSetting(65),"TIME:",Global.getTIME(), new 
            ReportEditMask ("XXXXXXXX"),NEWLINE,new TabSetting(65),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(2),NEWLINE,NEWLINE,new TabSetting(27),"PACKAGE CODE :  ",ldaRidl4040.getInput_Parms_Pnd_Pckge_Cde(),NEWLINE,NEWLINE,new 
            TabSetting(10),"                                                        ADDR",NEWLINE,new TabSetting(10)," MAIL ITEM              FULL NAME                 PIN   TYPE",NEWLINE,new 
            TabSetting(10),"----------- ----------------------------------- ------- ----");
    }
}
