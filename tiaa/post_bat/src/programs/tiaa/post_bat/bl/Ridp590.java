/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:08 PM
**        * FROM NATURAL PROGRAM : Ridp590
************************************************************
**        * FILE NAME            : Ridp590.java
**        * CLASS NAME           : Ridp590
**        * INSTANCE NAME        : Ridp590
************************************************************
************************************************************************
** PROGRAM     : RIDP590                                              **
** AUTHOR      : JOANNES C. AVE                                       **
** DATE        : 01/20/2012                                           **
** DESCRIPTION : READS BUSINESS USER PROVIDED EXTRACT AND REFORMATS   **
**               DATA AS FEED TO RIDP592                              **
**               PACKAGE CODES: RDR2012A                              **
************************************************************************
** HISTORY:                                                           **
** 04/20/2012  : INITIAL IMPLEMENTATION                               **
** 03/24/2015  : COR/NAAD SUNSET - REMOVE ALL REFERENCES TO COR/NAAD  **
**               AND CALL THE MDMN MODULES INSTEAD FOR PARTICIPANT    **
**               AND CONTRACT INFORMATION                             **
** 05/31/2017  : PIN EXPANSION                              PINEXP    **
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridp590 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;
    private PdaPsta9017 pdaPsta9017;
    private PdaPsta9670 pdaPsta9670;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Pin_A12;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Pin_N7;
    private DbsField pnd_Input_Record_Pnd_Pin_A5;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Pin_N12;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Level;
    private DbsField pnd_Input_Record_Pnd_Inst_Name;
    private DbsField pnd_Prev_Inst_Name;
    private DbsField pnd_Prev_Pin_A12;
    private DbsField pnd_Prev_Contract;
    private DbsField pnd_Prev_Level;
    private DbsField pnd_X;
    private DbsField pnd_K;
    private DbsField pnd_Valid_Record;
    private DbsField pnd_Accepted;
    private DbsField pnd_Remarks;
    private DbsField pnd_Lob;
    private DbsField pnd_Cutoff;
    private DbsField pnd_Participant_Name;
    private DbsField pnd_Ctr_Mdo_Contract_Read;
    private DbsField pnd_Ctr_Valid_Mdo_Contract_Read;
    private DbsField pnd_Ctr_Da_Contract_Not_In_Cor;
    private DbsField pnd_Ctr_Da_Inactive_In_Cor;
    private DbsField pnd_Ctr_Ph_Deceased;
    private DbsField pnd_Ctr_Ph_With_Mail_Suppression;
    private DbsField pnd_Ctr_Contract_Accepted;
    private DbsField pnd_Ctr_Contract_Rejected;
    private DbsField pnd_Ctr_Contract_Read;
    private DbsField pnd_Ctr_Duplicate_Contract;
    private DbsField pnd_Ctr_Terminated_Contract;
    private DbsField pnd_Ctr_Contract_Inactive;
    private DbsField pnd_Ctr_Out_Of_State;
    private DbsField pnd_Ctr_Contract_Outof_Range;
    private DbsField pnd_Ctr_Plan_Not_For_Proc;
    private DbsField pnd_Ctr_Institution_Owned;
    private DbsField pnd_Ctr_Total_Contracts;
    private DbsField pnd_Ctr_Total_Ra_Gra_Mdo;
    private DbsField pnd_Ctr_Total_Ira_Sra_Gsra_Mdo;
    private DbsField pnd_Break;
    private DbsField pnd_I;
    private DbsField pnd_A;

    private DbsGroup pnd_Arrays;
    private DbsField pnd_Arrays_Pnd_Combo_Prod;
    private DbsField pnd_Arrays_Pnd_Combo_Ctr;
    private DbsField pnd_Combo;

    private DbsGroup pnd_Ctrs;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Gra_Mdo;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ira_Sra_Gsra_Mdo;
    private DbsField pnd_Ctr_File1;

    private DbsGroup pnd_Output;
    private DbsField pnd_Output_Pnd_Pin_A12;
    private DbsField pnd_Output_Pnd_Level;
    private DbsField pnd_Output_Pnd_Institution_Name;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Gra_Mdo;
    private DbsField pnd_Output_Pnd_Ctr_Ira_Sra_Gsra_Mdo;
    private DbsField pnd_Output_Pnd_Package_Code;
    private DbsField pnd_Output_Pnd_Contracts;
    private DbsField pnd_Ndx;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);
        pdaPsta9017 = new PdaPsta9017(localVariables);
        pdaPsta9670 = new PdaPsta9670(localVariables);

        // Local Variables
        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 115);

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Pin_A12 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N7 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);
        pnd_Input_Record_Pnd_Pin_A5 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A5", "#PIN-A5", FieldType.STRING, 5);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N12 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N12", "#PIN-N12", FieldType.NUMERIC, 12);
        pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr", "#TIAA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Level = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Level", "#LEVEL", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Inst_Name = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Inst_Name", "#INST-NAME", FieldType.STRING, 
            60);
        pnd_Prev_Inst_Name = localVariables.newFieldInRecord("pnd_Prev_Inst_Name", "#PREV-INST-NAME", FieldType.STRING, 60);
        pnd_Prev_Pin_A12 = localVariables.newFieldInRecord("pnd_Prev_Pin_A12", "#PREV-PIN-A12", FieldType.STRING, 12);
        pnd_Prev_Contract = localVariables.newFieldInRecord("pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Level = localVariables.newFieldInRecord("pnd_Prev_Level", "#PREV-LEVEL", FieldType.STRING, 1);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_Valid_Record = localVariables.newFieldInRecord("pnd_Valid_Record", "#VALID-RECORD", FieldType.BOOLEAN, 1);
        pnd_Accepted = localVariables.newFieldInRecord("pnd_Accepted", "#ACCEPTED", FieldType.BOOLEAN, 1);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 40);
        pnd_Lob = localVariables.newFieldInRecord("pnd_Lob", "#LOB", FieldType.STRING, 4);
        pnd_Cutoff = localVariables.newFieldInRecord("pnd_Cutoff", "#CUTOFF", FieldType.STRING, 6);
        pnd_Participant_Name = localVariables.newFieldInRecord("pnd_Participant_Name", "#PARTICIPANT-NAME", FieldType.STRING, 35);
        pnd_Ctr_Mdo_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Mdo_Contract_Read", "#CTR-MDO-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Valid_Mdo_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Valid_Mdo_Contract_Read", "#CTR-VALID-MDO-CONTRACT-READ", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Da_Contract_Not_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Contract_Not_In_Cor", "#CTR-DA-CONTRACT-NOT-IN-COR", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Da_Inactive_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Inactive_In_Cor", "#CTR-DA-INACTIVE-IN-COR", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_Deceased = localVariables.newFieldInRecord("pnd_Ctr_Ph_Deceased", "#CTR-PH-DECEASED", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_With_Mail_Suppression = localVariables.newFieldInRecord("pnd_Ctr_Ph_With_Mail_Suppression", "#CTR-PH-WITH-MAIL-SUPPRESSION", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Contract_Accepted = localVariables.newFieldInRecord("pnd_Ctr_Contract_Accepted", "#CTR-CONTRACT-ACCEPTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Rejected = localVariables.newFieldInRecord("pnd_Ctr_Contract_Rejected", "#CTR-CONTRACT-REJECTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Contract_Read", "#CTR-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Duplicate_Contract = localVariables.newFieldInRecord("pnd_Ctr_Duplicate_Contract", "#CTR-DUPLICATE-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Terminated_Contract = localVariables.newFieldInRecord("pnd_Ctr_Terminated_Contract", "#CTR-TERMINATED-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Inactive = localVariables.newFieldInRecord("pnd_Ctr_Contract_Inactive", "#CTR-CONTRACT-INACTIVE", FieldType.NUMERIC, 7);
        pnd_Ctr_Out_Of_State = localVariables.newFieldInRecord("pnd_Ctr_Out_Of_State", "#CTR-OUT-OF-STATE", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Outof_Range = localVariables.newFieldInRecord("pnd_Ctr_Contract_Outof_Range", "#CTR-CONTRACT-OUTOF-RANGE", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Plan_Not_For_Proc = localVariables.newFieldInRecord("pnd_Ctr_Plan_Not_For_Proc", "#CTR-PLAN-NOT-FOR-PROC", FieldType.NUMERIC, 7);
        pnd_Ctr_Institution_Owned = localVariables.newFieldInRecord("pnd_Ctr_Institution_Owned", "#CTR-INSTITUTION-OWNED", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Contracts = localVariables.newFieldInRecord("pnd_Ctr_Total_Contracts", "#CTR-TOTAL-CONTRACTS", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_Gra_Mdo = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Gra_Mdo", "#CTR-TOTAL-RA-GRA-MDO", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ira_Sra_Gsra_Mdo = localVariables.newFieldInRecord("pnd_Ctr_Total_Ira_Sra_Gsra_Mdo", "#CTR-TOTAL-IRA-SRA-GSRA-MDO", FieldType.NUMERIC, 
            7);
        pnd_Break = localVariables.newFieldInRecord("pnd_Break", "#BREAK", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);

        pnd_Arrays = localVariables.newGroupArrayInRecord("pnd_Arrays", "#ARRAYS", new DbsArrayController(1, 240));
        pnd_Arrays_Pnd_Combo_Prod = pnd_Arrays.newFieldInGroup("pnd_Arrays_Pnd_Combo_Prod", "#COMBO-PROD", FieldType.STRING, 12);
        pnd_Arrays_Pnd_Combo_Ctr = pnd_Arrays.newFieldInGroup("pnd_Arrays_Pnd_Combo_Ctr", "#COMBO-CTR", FieldType.NUMERIC, 7);
        pnd_Combo = localVariables.newFieldInRecord("pnd_Combo", "#COMBO", FieldType.STRING, 12);

        pnd_Ctrs = localVariables.newGroupInRecord("pnd_Ctrs", "#CTRS");
        pnd_Ctrs_Pnd_Ctr_Ra_Gra_Mdo = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Gra_Mdo", "#CTR-RA-GRA-MDO", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ira_Sra_Gsra_Mdo = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ira_Sra_Gsra_Mdo", "#CTR-IRA-SRA-GSRA-MDO", FieldType.NUMERIC, 
            1);
        pnd_Ctr_File1 = localVariables.newFieldInRecord("pnd_Ctr_File1", "#CTR-FILE1", FieldType.NUMERIC, 7);

        pnd_Output = localVariables.newGroupInRecord("pnd_Output", "#OUTPUT");
        pnd_Output_Pnd_Pin_A12 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);
        pnd_Output_Pnd_Level = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Level", "#LEVEL", FieldType.STRING, 1);
        pnd_Output_Pnd_Institution_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 60);
        pnd_Output_Pnd_Ctr_Ra_Gra_Mdo = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Gra_Mdo", "#CTR-RA-GRA-MDO", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ira_Sra_Gsra_Mdo = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ira_Sra_Gsra_Mdo", "#CTR-IRA-SRA-GSRA-MDO", FieldType.NUMERIC, 
            1);
        pnd_Output_Pnd_Package_Code = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Output_Pnd_Contracts = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Contracts", "#CONTRACTS", FieldType.STRING, 8, new DbsArrayController(1, 
            20));
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Valid_Record.setInitialValue(false);
        pnd_Accepted.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridp590() throws Exception
    {
        super("Ridp590");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  STATISTICS REPORT
        if (condition(getReports().getAstLinesLeft(1).less(5)))                                                                                                           //Natural: FORMAT ( 1 ) LS = 120 PS = 60;//Natural: FORMAT LS = 80 PS = 60;//Natural: NEWPAGE ( 1 ) IF LESS THAN 5 LINES
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        //*  OPEN MQ QUEUE                                       /* JCA20150324
        getReports().write(0, " FETCHING MDMP011");                                                                                                                       //Natural: WRITE ' FETCHING MDMP011'
        if (Global.isEscape()) return;
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #INPUT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  PINEXP
            pnd_Ctr_Mdo_Contract_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CTR-MDO-CONTRACT-READ
            //*                                                                                                                                                           //Natural: AT BREAK OF #INPUT-RECORD.#PIN-A12
            //*                                                                                                                                                           //Natural: AT END OF DATA
            if (condition(pnd_Break.getBoolean()))                                                                                                                        //Natural: IF #BREAK
            {
                pnd_Break.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #BREAK
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP
            pnd_Prev_Pin_A12.setValue(pnd_Input_Record_Pnd_Pin_A12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-A12 TO #PREV-PIN-A12
            pnd_Prev_Level.setValue(pnd_Input_Record_Pnd_Level);                                                                                                          //Natural: MOVE #INPUT-RECORD.#LEVEL TO #PREV-LEVEL
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Inst_Name);                                                                                                  //Natural: MOVE #INPUT-RECORD.#INST-NAME TO #PREV-INST-NAME
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
        }                                                                                                                                                                 //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-STATISTICS-REPORT
        sub_Write_Statistics_Report();
        if (condition(Global.isEscape())) {return;}
        //*  CLOSE MQ QUEUE
        getReports().write(0, "FETCHING MDMP0012");                                                                                                                       //Natural: WRITE 'FETCHING MDMP0012'
        if (Global.isEscape()) return;
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* **********************************************************************
        //*                 S T A R T    O F    S U B R O U T I N E S
        //* **********************************************************************
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-RECORD
        //*  RA/GRA MDO CONTRACTS
        //*  IRA/SRA/GSRA MDO CONTRACTS
        //*  ---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-ARRAY
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RECORD-IN-COR
        //*  --------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PIN-STATUS
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA-TO-FILE
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA-TO-ARRAY
        //* **************************************
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-STATISTICS-REPORT
    }
    private void sub_Validate_Record() throws Exception                                                                                                                   //Natural: VALIDATE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        if (condition(pnd_Prev_Contract.equals(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr)))                                                                                    //Natural: IF #PREV-CONTRACT EQ #TIAA-CNTRCT-NBR
        {
            pnd_Ctr_Duplicate_Contract.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DUPLICATE-CONTRACT
            pnd_Remarks.setValue("DUPLICATE CONTRACT");                                                                                                                   //Natural: ASSIGN #REMARKS := 'DUPLICATE CONTRACT'
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(3),pnd_Remarks);               //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 3X #REMARKS
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prev_Contract.setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #PREV-CONTRACT
                                                                                                                                                                          //Natural: PERFORM CHECK-RECORD-IN-COR
        sub_Check_Record_In_Cor();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Valid_Record.getBoolean()))                                                                                                                     //Natural: IF #VALID-RECORD
        {
            //*  RAMDO
            //*  RASIP
            //*  GRAMDO
            //*  GRASIP
            short decideConditionsMet554 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TIAA-CNTRCT-NBR EQ 'C8000000' THRU 'C8499999' OR #TIAA-CNTRCT-NBR EQ 'C8500000' THRU 'C8739999' OR #TIAA-CNTRCT-NBR EQ 'C8740000' THRU 'C8939999' OR #TIAA-CNTRCT-NBR EQ 'C8940000' THRU 'C8999999'
            if (condition(((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C8000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C8499999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C8500000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C8739999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C8740000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C8939999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C8940000") 
                && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C8999999")))))
            {
                decideConditionsMet554++;
                pnd_Ctrs_Pnd_Ctr_Ra_Gra_Mdo.setValue(1);                                                                                                                  //Natural: MOVE 1 TO #CTRS.#CTR-RA-GRA-MDO
                pnd_Ctr_Total_Ra_Gra_Mdo.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-RA-GRA-MDO
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                //*  GSRMDO
                //*  GSRSIP
                //*  IRAMDO
                //*  IRASIP
                //*  IRASIP
                //*  SRAMDO
                //*  SRASIP
                //*  SRASIP
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #TIAA-CNTRCT-NBR EQ 'K8860000' THRU 'K8959999' OR #TIAA-CNTRCT-NBR EQ 'K8960000' THRU 'K8999999' OR #TIAA-CNTRCT-NBR EQ 'K8300000' THRU 'K8459999' OR #TIAA-CNTRCT-NBR EQ 'K9950000' THRU 'K9999999' OR #TIAA-CNTRCT-NBR EQ 'K8460000' THRU 'K8499999' OR #TIAA-CNTRCT-NBR EQ 'K8500000' THRU 'K8799999' OR #TIAA-CNTRCT-NBR EQ 'L9900000' THRU 'L9999999' OR #TIAA-CNTRCT-NBR EQ 'K8800000' THRU 'K8859999'
            else if (condition(((((((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K8860000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K8959999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K8960000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K8999999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K8300000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K8459999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K9950000") 
                && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K9999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K8460000") && 
                pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K8499999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K8500000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K8799999"))) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("L9900000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("L9999999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K8800000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K8859999")))))
            {
                decideConditionsMet554++;
                pnd_Ctrs_Pnd_Ctr_Ira_Sra_Gsra_Mdo.setValue(1);                                                                                                            //Natural: MOVE 1 TO #CTRS.#CTR-IRA-SRA-GSRA-MDO
                pnd_Ctr_Total_Ira_Sra_Gsra_Mdo.nadd(1);                                                                                                                   //Natural: ADD 1 TO #CTR-TOTAL-IRA-SRA-GSRA-MDO
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Ctr_Contract_Outof_Range.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CTR-CONTRACT-OUTOF-RANGE
                pnd_Remarks.setValue("CONTRACT OUT OF RANGE");                                                                                                            //Natural: ASSIGN #REMARKS := 'CONTRACT OUT OF RANGE'
                getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(3),pnd_Remarks);           //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 3X #REMARKS
                if (Global.isEscape()) return;
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE-RECORD
    }
    private void sub_Add_Contract_To_Array() throws Exception                                                                                                             //Natural: ADD-CONTRACT-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------
        pnd_K.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #K
        pnd_Output_Pnd_Contracts.getValue(pnd_K).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                          //Natural: MOVE #TIAA-CNTRCT-NBR TO #CONTRACTS ( #K )
        //*  ADD-CONTRACT-TO-ARRAY
    }
    private void sub_Check_Record_In_Cor() throws Exception                                                                                                               //Natural: CHECK-RECORD-IN-COR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------
        //*  MODIFY #MDM*210 TO MDM*211 IN THIS SUBROUTINE           /* PINEXP
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #MDMA211.#I-CONTRACT-NUMBER
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue("01");                                                                                                   //Natural: MOVE '01' TO #MDMA211.#I-PAYEE-CODE-A2
        //*    WRITE 'CALLING MDMN211A TO GET CONTRACT INFORMATION'
        //*  JCA20150324
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //*    WRITE '=' #MDMA211.#O-RETURN-CODE
        //*  JCA20150324
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA211.#O-RETURN-CODE NE '0000'
        {
            pnd_Remarks.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Text());                                                                                          //Natural: ASSIGN #REMARKS := #MDMA211.#O-RETURN-TEXT
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(3),pnd_Remarks);               //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 3X #REMARKS
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Contract_Not_In_Cor.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-DA-CONTRACT-NOT-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals(" ") || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("H")         //Natural: IF NOT ( #MDMA211.#O-CONTRACT-STATUS-CODE = ' ' OR = 'H' OR = 'Y' )
            || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("Y"))))
        {
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("CONTRACT INACTIVE IN COR - STATUS CODE:", pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code()));                    //Natural: COMPRESS 'CONTRACT INACTIVE IN COR - STATUS CODE:' #MDMA211.#O-CONTRACT-STATUS-CODE TO #REMARKS
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(3),pnd_Remarks);               //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 3X #REMARKS
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Inactive_In_Cor.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DA-INACTIVE-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PIN-STATUS
            sub_Determine_Pin_Status();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Accepted.getBoolean()))                                                                                                                     //Natural: IF #ACCEPTED
            {
                pnd_Valid_Record.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-RECORD-IN-COR
    }
    private void sub_Determine_Pin_Status() throws Exception                                                                                                              //Natural: DETERMINE-PIN-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------
        //*  MODIFY MDM*100 TO MDM*101 IN THIS SUBROUTINE FOR PINEXP
        //*  JCA20150324
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        //*    MOVE #INPUT-RECORD.#PIN TO #MDMA101.#I-PIN          /* JCA20150324
        //*    WRITE 'CALLING MDMN101A TO GET ADDRESS/NAME'
        //*  PINEXP >>>
        if (condition(pnd_Input_Record_Pnd_Pin_A12.notEquals(" ")))                                                                                                       //Natural: IF #INPUT-RECORD.#PIN-A12 NE ' '
        {
            if (condition(pnd_Input_Record_Pnd_Pin_A12.getSubstring(8,1).equals(" ")))                                                                                    //Natural: IF SUBSTRING ( #INPUT-RECORD.#PIN-A12,8,1 ) = ' '
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N7);                                                                          //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #PIN-N7
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                         //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #PIN-N12
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP <<<
        }                                                                                                                                                                 //Natural: END-IF
        //*  JCA20150324
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  JCA20150324
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero())))                                                                                //Natural: IF #MDMA101.#O-DATE-OF-DEATH GT 0
        {
            pnd_Ctr_Ph_Deceased.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CTR-PH-DECEASED
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("P/H DECEASED - DOD:", pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death()));                                               //Natural: COMPRESS 'P/H DECEASED - DOD:' #MDMA101.#O-DATE-OF-DEATH TO #REMARKS
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(3),pnd_Remarks);               //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 3X #REMARKS
            if (Global.isEscape()) return;
            pnd_Accepted.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #ACCEPTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  REJECT PARTICIPANTS W/ STOP ALL MAILING SUPPRESSION CODE
            //*  JCA20150324
            DbsUtil.examine(new ExamineSource(pdaMdma101.getPnd_Mdma101_Pnd_O_Mail_Code().getValue("*")), new ExamineSearch("01"), new ExamineGivingPosition(pnd_I));     //Natural: EXAMINE #MDMA101.#O-MAIL-CODE ( * ) FOR '01' GIVING POSITION #I
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I GT 0
            {
                pnd_Ctr_Ph_With_Mail_Suppression.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTR-PH-WITH-MAIL-SUPPRESSION
                pnd_Remarks.setValue(DbsUtil.compress("P/H MAIL SUPPRESSED - MAIL CODE: 01"));                                                                            //Natural: COMPRESS 'P/H MAIL SUPPRESSED - MAIL CODE: 01' TO #REMARKS
                getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(3),pnd_Remarks);           //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 3X #REMARKS
                if (Global.isEscape()) return;
                pnd_Accepted.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #ACCEPTED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Accepted.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #ACCEPTED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE-PIN-STATUS
    }
    private void sub_Write_Data_To_File() throws Exception                                                                                                                //Natural: WRITE-DATA-TO-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        pnd_Output_Pnd_Package_Code.setValue("RDR2012A");                                                                                                                 //Natural: MOVE 'RDR2012A' TO #PACKAGE-CODE
        pnd_Ctr_File1.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #CTR-FILE1
        getWorkFiles().write(2, false, pnd_Output);                                                                                                                       //Natural: WRITE WORK FILE 2 #OUTPUT
        //*  WRITE-DATA-TO-FILE
    }
    private void sub_Write_Data_To_Array() throws Exception                                                                                                               //Natural: WRITE-DATA-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ctr_Total_Contracts.compute(new ComputeParameters(false, pnd_Ctr_Total_Contracts), pnd_Ctrs_Pnd_Ctr_Ra_Gra_Mdo.add(pnd_Ctrs_Pnd_Ctr_Ira_Sra_Gsra_Mdo));       //Natural: ASSIGN #CTR-TOTAL-CONTRACTS := #CTRS.#CTR-RA-GRA-MDO + #CTRS.#CTR-IRA-SRA-GSRA-MDO
        //*  WRITE-DATA-TO-ARRAY
    }
    private void sub_Write_Statistics_Report() throws Exception                                                                                                           //Natural: WRITE-STATISTICS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(1, NEWLINE,NEWLINE,"TOTAL INSTITUTION-OWNED CONTRACTS :",pnd_Ctr_Institution_Owned, new ReportEditMask ("Z,ZZZ,ZZ9"));                         //Natural: WRITE ( 1 ) // 'TOTAL INSTITUTION-OWNED CONTRACTS :' #CTR-INSTITUTION-OWNED ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        pnd_Ctr_Duplicate_Contract.nsubtract(1);                                                                                                                          //Natural: ASSIGN #CTR-DUPLICATE-CONTRACT := #CTR-DUPLICATE-CONTRACT - 1
        pnd_Ctr_Contract_Rejected.compute(new ComputeParameters(false, pnd_Ctr_Contract_Rejected), pnd_Ctr_Da_Contract_Not_In_Cor.add(pnd_Ctr_Da_Inactive_In_Cor).add(pnd_Ctr_Ph_Deceased).add(pnd_Ctr_Ph_With_Mail_Suppression).add(pnd_Ctr_Out_Of_State).add(pnd_Ctr_Contract_Outof_Range).add(pnd_Ctr_Plan_Not_For_Proc).add(pnd_Ctr_Institution_Owned).add(pnd_Ctr_Duplicate_Contract)); //Natural: ASSIGN #CTR-CONTRACT-REJECTED := #CTR-DA-CONTRACT-NOT-IN-COR + #CTR-DA-INACTIVE-IN-COR + #CTR-PH-DECEASED + #CTR-PH-WITH-MAIL-SUPPRESSION + #CTR-OUT-OF-STATE + #CTR-CONTRACT-OUTOF-RANGE + #CTR-PLAN-NOT-FOR-PROC + #CTR-INSTITUTION-OWNED + #CTR-DUPLICATE-CONTRACT
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ         :",pnd_Ctr_Mdo_Contract_Read);                                                                              //Natural: WRITE 'NUMBER OF RECORDS READ         :' #CTR-MDO-CONTRACT-READ
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS ACCEPTED     :",pnd_Ctr_Contract_Accepted);                                                                              //Natural: WRITE 'NUMBER OF RECORDS ACCEPTED     :' #CTR-CONTRACT-ACCEPTED
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS REJECTED     :",pnd_Ctr_Contract_Rejected);                                                                              //Natural: WRITE 'NUMBER OF RECORDS REJECTED     :' #CTR-CONTRACT-REJECTED
        if (Global.isEscape()) return;
        getReports().write(0, "BREAKDOWN OF RECORDS REJECTED  :");                                                                                                        //Natural: WRITE 'BREAKDOWN OF RECORDS REJECTED  :'
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT NOT IN COR     :",pnd_Ctr_Da_Contract_Not_In_Cor);                                                                              //Natural: WRITE ' -CONTRACT NOT IN COR     :' #CTR-DA-CONTRACT-NOT-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT INACTIVE IN COR:",pnd_Ctr_Da_Inactive_In_Cor);                                                                                  //Natural: WRITE ' -CONTRACT INACTIVE IN COR:' #CTR-DA-INACTIVE-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H DECEASED            :",pnd_Ctr_Ph_Deceased);                                                                                         //Natural: WRITE ' -P/H DECEASED            :' #CTR-PH-DECEASED
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H W/ MAIL SUPPRESSION :",pnd_Ctr_Ph_With_Mail_Suppression);                                                                            //Natural: WRITE ' -P/H W/ MAIL SUPPRESSION :' #CTR-PH-WITH-MAIL-SUPPRESSION
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT OUT OF RANGE   :",pnd_Ctr_Contract_Outof_Range);                                                                                //Natural: WRITE ' -CONTRACT OUT OF RANGE   :' #CTR-CONTRACT-OUTOF-RANGE
        if (Global.isEscape()) return;
        getReports().write(0, " -PLAN NOT FOR PROCESSING :",pnd_Ctr_Plan_Not_For_Proc);                                                                                   //Natural: WRITE ' -PLAN NOT FOR PROCESSING :' #CTR-PLAN-NOT-FOR-PROC
        if (Global.isEscape()) return;
        getReports().write(0, " -INSTITUTION OWNED       :",pnd_Ctr_Institution_Owned);                                                                                   //Natural: WRITE ' -INSTITUTION OWNED       :' #CTR-INSTITUTION-OWNED
        if (Global.isEscape()) return;
        getReports().write(0, " -DUPLICATE CONTRACT      :",pnd_Ctr_Duplicate_Contract);                                                                                  //Natural: WRITE ' -DUPLICATE CONTRACT      :' #CTR-DUPLICATE-CONTRACT
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY PRODUCT:");                                                                                                                       //Natural: WRITE 'TOTAL BY PRODUCT:'
        if (Global.isEscape()) return;
        getReports().write(0, " - RA/GRA MDO       :",pnd_Ctr_Total_Ra_Gra_Mdo);                                                                                          //Natural: WRITE ' - RA/GRA MDO       :' #CTR-TOTAL-RA-GRA-MDO
        if (Global.isEscape()) return;
        getReports().write(0, " - IRA/SRA/GSRA MDO :",pnd_Ctr_Total_Ira_Sra_Gsra_Mdo);                                                                                    //Natural: WRITE ' - IRA/SRA/GSRA MDO :' #CTR-TOTAL-IRA-SRA-GSRA-MDO
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY FILE:");                                                                                                                          //Natural: WRITE 'TOTAL BY FILE:'
        if (Global.isEscape()) return;
        getReports().write(0, " - FILE 1 : ",pnd_Ctr_File1,new ColumnSpacing(5),"(tri-folds)");                                                                           //Natural: WRITE ' - FILE 1 : ' #CTR-FILE1 5X '(tri-folds)'
        if (Global.isEscape()) return;
        //*  WRITE-STATISTICS-REPORT
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Input_Record_Pnd_Pin_A12IsBreak = pnd_Input_Record_Pnd_Pin_A12.isBreak(endOfData);
        if (condition(pnd_Input_Record_Pnd_Pin_A12IsBreak))
        {
            pnd_Ctr_Total_Contracts.compute(new ComputeParameters(false, pnd_Ctr_Total_Contracts), pnd_Ctrs_Pnd_Ctr_Ra_Gra_Mdo.add(pnd_Ctrs_Pnd_Ctr_Ira_Sra_Gsra_Mdo));   //Natural: ASSIGN #CTR-TOTAL-CONTRACTS := #CTRS.#CTR-RA-GRA-MDO + #CTRS.#CTR-IRA-SRA-GSRA-MDO
            if (condition(pnd_Ctr_Total_Contracts.greater(getZero())))                                                                                                    //Natural: IF #CTR-TOTAL-CONTRACTS GT 0
            {
                //*  PINEXP
                pnd_Output_Pnd_Pin_A12.setValue(pnd_Prev_Pin_A12);                                                                                                        //Natural: MOVE #PREV-PIN-A12 TO #OUTPUT.#PIN-A12
                pnd_Output_Pnd_Ctr_Ra_Gra_Mdo.setValue(pnd_Ctrs_Pnd_Ctr_Ra_Gra_Mdo);                                                                                      //Natural: MOVE #CTRS.#CTR-RA-GRA-MDO TO #OUTPUT.#CTR-RA-GRA-MDO
                pnd_Output_Pnd_Ctr_Ira_Sra_Gsra_Mdo.setValue(pnd_Ctrs_Pnd_Ctr_Ira_Sra_Gsra_Mdo);                                                                          //Natural: MOVE #CTRS.#CTR-IRA-SRA-GSRA-MDO TO #OUTPUT.#CTR-IRA-SRA-GSRA-MDO
                pnd_Output_Pnd_Level.setValue(pnd_Prev_Level);                                                                                                            //Natural: MOVE #PREV-LEVEL TO #OUTPUT.#LEVEL
                pnd_Output_Pnd_Institution_Name.setValue(pnd_Prev_Inst_Name);                                                                                             //Natural: MOVE #PREV-INST-NAME TO #OUTPUT.#INSTITUTION-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-DATA-TO-FILE
                sub_Write_Data_To_File();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ctrs.reset();                                                                                                                                             //Natural: RESET #CTRS #CTR-TOTAL-CONTRACTS
            pnd_Ctr_Total_Contracts.reset();
            pnd_Output.reset();                                                                                                                                           //Natural: RESET #OUTPUT #K
            pnd_K.reset();
            pnd_Break.setValue(true);                                                                                                                                     //Natural: MOVE TRUE TO #BREAK
            //*  PINEXP
            pnd_Prev_Pin_A12.setValue(pnd_Input_Record_Pnd_Pin_A12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-A12 TO #PREV-PIN-A12
            pnd_Prev_Level.setValue(pnd_Input_Record_Pnd_Level);                                                                                                          //Natural: MOVE #INPUT-RECORD.#LEVEL TO #PREV-LEVEL
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Inst_Name);                                                                                                  //Natural: MOVE #INPUT-RECORD.#INST-NAME TO #PREV-INST-NAME
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=120 PS=60");
        Global.format(0, "LS=80 PS=60");
    }
}
