/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:15 PM
**        * FROM NATURAL PROGRAM : Ridp640
************************************************************
**        * FILE NAME            : Ridp640.java
**        * CLASS NAME           : Ridp640
**        * INSTANCE NAME        : Ridp640
************************************************************
************************************************************************
** PROGRAM     : RIDP640                                              **
** AUTHOR      : JOANNES C. AVE                                       **
** DATE        : 05/06/2015                                           **
** DESCRIPTION : READS BI QUERY CREF REDESIGN SHARE CLASS EXTRACT AMD **
**               REFORMATS DATA AS FEED TO RIDP642                    **
**               - RDR2016C - PAPER                                   **
**               - RDR2016D - EMAIL                                   **
************************************************************************
** HISTORY:                                                           **
** 12/15/2015  : INITIAL IMPLEMENTATION                               **
** 05/22/2017  : PIN EXPANSION                                PINEXP  **
**               COMMENTED OUT CPMA550 - OBSOLETE                     **
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridp640 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;
    private PdaPsta9017 pdaPsta9017;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Pin_A12;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Pin_N5;
    private DbsField pnd_Input_Record_Pnd_Pin_N7;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Pin_N12;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cref_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Date;
    private DbsField pnd_Input_Record_Pnd_Issue_State_Cde;
    private DbsField pnd_Input_Record_Pnd_Plan_Cde;
    private DbsField pnd_Input_Record_Pnd_Cover_Letter_Ind;
    private DbsField pnd_Input_Record_Pnd_Institution_Name;
    private DbsField pnd_Input_Record_Pnd_Effective_Date;
    private DbsField pnd_Input_Record_Pnd_Plan_Name_A;
    private DbsField pnd_Input_Record_Pnd_Share_Class_A;
    private DbsField pnd_Prev_Inst_Name;
    private DbsField pnd_Prev_Pin_N12;

    private DbsGroup pnd_Prev_Pin_N12__R_Field_4;
    private DbsField pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5;
    private DbsField pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7;
    private DbsField pnd_Prev_Contract;
    private DbsField pnd_Prev_Effective_Date;
    private DbsField pnd_Prev_Cover;
    private DbsField pnd_K;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Valid_Record;
    private DbsField pnd_Accepted;
    private DbsField pnd_Remarks;
    private DbsField pnd_Lob;
    private DbsField pnd_Participant_Name;
    private DbsField pnd_Ctr_Valid_Contract_Read;
    private DbsField pnd_Ctr_Da_Contract_Not_In_Mdm;
    private DbsField pnd_Ctr_Da_Inactive_In_Mdm;
    private DbsField pnd_Ctr_Ph_Deceased;
    private DbsField pnd_Ctr_Ph_With_Mail_Suppression;
    private DbsField pnd_Ctr_Contract_Dispparoved_State;
    private DbsField pnd_Ctr_Contract_Accepted;
    private DbsField pnd_Ctr_Contract_Rejected;
    private DbsField pnd_Ctr_Contract_Read;
    private DbsField pnd_Ctr_Duplicate_Contract;
    private DbsField pnd_Ctr_Terminated_Contract;
    private DbsField pnd_Ctr_Contract_Inactive;
    private DbsField pnd_Ctr_Out_Of_State;
    private DbsField pnd_Ctr_Contract_Outof_Range;
    private DbsField pnd_Ctr_After_Cutoff;
    private DbsField pnd_Ctr_Plan_Not_For_Proc;
    private DbsField pnd_Ctr_Institution_Owned;
    private DbsField pnd_Ctr_Total_Gra_Gsra_Rc_Rcp;
    private DbsField pnd_Ctr_Total_Ra_Sra_Mdo;
    private DbsField pnd_Ctr_Total_Cref_Group_Gn;
    private DbsField pnd_Ctr_Total_Cref_Group_Fl;
    private DbsField pnd_Ctr_Total_Cref_Group_Il;
    private DbsField pnd_Ctr_Total_Cref_Indvl_Gn;
    private DbsField pnd_Ctr_Total_Cref_Indvl_Fl;
    private DbsField pnd_Ctr_Total_Cref_Indvl_Il;
    private DbsField pnd_Break;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_Eml;
    private DbsField pnd_Bad_Address;
    private DbsField pnd_Sum_Tiaa_Cref;
    private DbsField pnd_Contract_Type_A;
    private DbsField pnd_Contract_State_A;
    private DbsField pnd_Ctr_Cref_Contract_A;
    private DbsField pnd_Ndx;
    private DbsField pnd_Ctr_File1;
    private DbsField pnd_Ctr_File2;

    private DbsGroup pnd_Ctrs;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Group_Gn;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Group_Fl;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Group_Il;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Gn;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Fl;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Il;

    private DbsGroup pnd_Output;
    private DbsField pnd_Output_Pnd_Pin_A12;

    private DbsGroup pnd_Output__R_Field_5;
    private DbsField pnd_Output_Pnd_Pin_N7;
    private DbsField pnd_Output_Pnd_Pin_A5;
    private DbsField pnd_Output_Pnd_Full_Name;
    private DbsField pnd_Output_Pnd_Address_Line_Txt;
    private DbsField pnd_Output_Pnd_Postal_Data;
    private DbsField pnd_Output_Pnd_Address_Type_Cde;
    private DbsField pnd_Output_Pnd_Letter_Type;
    private DbsField pnd_Output_Pnd_Ctr_Cref_Contract;
    private DbsField pnd_Output_Pnd_Package_Code;

    private DbsGroup pnd_Output_Pnd_Contract_Details;
    private DbsField pnd_Output_Pnd_Cref_Contracts;
    private DbsField pnd_Output_Pnd_Contract_Type;
    private DbsField pnd_Output_Pnd_Contract_State;
    private DbsField pnd_Output_Pnd_Multi_Plan_Ctr;
    private DbsField pnd_Output_Pnd_Plan_Name;
    private DbsField pnd_Output_Pnd_Share_Class;
    private DbsField pnd_Output_Pnd_Institution_Name;
    private DbsField pnd_Output_Pnd_Effective_Date;
    private DbsField pnd_Output_Pnd_Email_Address;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);
        pdaPsta9017 = new PdaPsta9017(localVariables);

        // Local Variables
        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 305);

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Pin_A12 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N5 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N5", "#PIN-N5", FieldType.NUMERIC, 5);
        pnd_Input_Record_Pnd_Pin_N7 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N12 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N12", "#PIN-N12", FieldType.NUMERIC, 12);
        pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr", "#TIAA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cref_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cref_Cntrct_Nbr", "#CREF-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cntrct_Issue_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Date", "#CNTRCT-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Issue_State_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Issue_State_Cde", "#ISSUE-STATE-CDE", 
            FieldType.STRING, 2);
        pnd_Input_Record_Pnd_Plan_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Plan_Cde", "#PLAN-CDE", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cover_Letter_Ind = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cover_Letter_Ind", "#COVER-LETTER-IND", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Institution_Name = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Institution_Name", "#INSTITUTION-NAME", 
            FieldType.STRING, 120);
        pnd_Input_Record_Pnd_Effective_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.STRING, 
            8);
        pnd_Input_Record_Pnd_Plan_Name_A = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Plan_Name_A", "#PLAN-NAME-A", FieldType.STRING, 
            80);
        pnd_Input_Record_Pnd_Share_Class_A = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Share_Class_A", "#SHARE-CLASS-A", FieldType.STRING, 
            2);
        pnd_Prev_Inst_Name = localVariables.newFieldInRecord("pnd_Prev_Inst_Name", "#PREV-INST-NAME", FieldType.STRING, 120);
        pnd_Prev_Pin_N12 = localVariables.newFieldInRecord("pnd_Prev_Pin_N12", "#PREV-PIN-N12", FieldType.NUMERIC, 12);

        pnd_Prev_Pin_N12__R_Field_4 = localVariables.newGroupInRecord("pnd_Prev_Pin_N12__R_Field_4", "REDEFINE", pnd_Prev_Pin_N12);
        pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5 = pnd_Prev_Pin_N12__R_Field_4.newFieldInGroup("pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5", "#PREV-PIN-N5", FieldType.NUMERIC, 
            5);
        pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7 = pnd_Prev_Pin_N12__R_Field_4.newFieldInGroup("pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7", "#PREV-PIN-N7", FieldType.NUMERIC, 
            7);
        pnd_Prev_Contract = localVariables.newFieldInRecord("pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Effective_Date = localVariables.newFieldInRecord("pnd_Prev_Effective_Date", "#PREV-EFFECTIVE-DATE", FieldType.STRING, 10);
        pnd_Prev_Cover = localVariables.newFieldInRecord("pnd_Prev_Cover", "#PREV-COVER", FieldType.STRING, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 2);
        pnd_Valid_Record = localVariables.newFieldInRecord("pnd_Valid_Record", "#VALID-RECORD", FieldType.BOOLEAN, 1);
        pnd_Accepted = localVariables.newFieldInRecord("pnd_Accepted", "#ACCEPTED", FieldType.BOOLEAN, 1);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 40);
        pnd_Lob = localVariables.newFieldInRecord("pnd_Lob", "#LOB", FieldType.STRING, 4);
        pnd_Participant_Name = localVariables.newFieldInRecord("pnd_Participant_Name", "#PARTICIPANT-NAME", FieldType.STRING, 35);
        pnd_Ctr_Valid_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Valid_Contract_Read", "#CTR-VALID-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Da_Contract_Not_In_Mdm = localVariables.newFieldInRecord("pnd_Ctr_Da_Contract_Not_In_Mdm", "#CTR-DA-CONTRACT-NOT-IN-MDM", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Da_Inactive_In_Mdm = localVariables.newFieldInRecord("pnd_Ctr_Da_Inactive_In_Mdm", "#CTR-DA-INACTIVE-IN-MDM", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_Deceased = localVariables.newFieldInRecord("pnd_Ctr_Ph_Deceased", "#CTR-PH-DECEASED", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_With_Mail_Suppression = localVariables.newFieldInRecord("pnd_Ctr_Ph_With_Mail_Suppression", "#CTR-PH-WITH-MAIL-SUPPRESSION", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Contract_Dispparoved_State = localVariables.newFieldInRecord("pnd_Ctr_Contract_Dispparoved_State", "#CTR-CONTRACT-DISPPAROVED-STATE", 
            FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Accepted = localVariables.newFieldInRecord("pnd_Ctr_Contract_Accepted", "#CTR-CONTRACT-ACCEPTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Rejected = localVariables.newFieldInRecord("pnd_Ctr_Contract_Rejected", "#CTR-CONTRACT-REJECTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Contract_Read", "#CTR-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Duplicate_Contract = localVariables.newFieldInRecord("pnd_Ctr_Duplicate_Contract", "#CTR-DUPLICATE-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Terminated_Contract = localVariables.newFieldInRecord("pnd_Ctr_Terminated_Contract", "#CTR-TERMINATED-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Inactive = localVariables.newFieldInRecord("pnd_Ctr_Contract_Inactive", "#CTR-CONTRACT-INACTIVE", FieldType.NUMERIC, 7);
        pnd_Ctr_Out_Of_State = localVariables.newFieldInRecord("pnd_Ctr_Out_Of_State", "#CTR-OUT-OF-STATE", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Outof_Range = localVariables.newFieldInRecord("pnd_Ctr_Contract_Outof_Range", "#CTR-CONTRACT-OUTOF-RANGE", FieldType.NUMERIC, 
            7);
        pnd_Ctr_After_Cutoff = localVariables.newFieldInRecord("pnd_Ctr_After_Cutoff", "#CTR-AFTER-CUTOFF", FieldType.NUMERIC, 7);
        pnd_Ctr_Plan_Not_For_Proc = localVariables.newFieldInRecord("pnd_Ctr_Plan_Not_For_Proc", "#CTR-PLAN-NOT-FOR-PROC", FieldType.NUMERIC, 7);
        pnd_Ctr_Institution_Owned = localVariables.newFieldInRecord("pnd_Ctr_Institution_Owned", "#CTR-INSTITUTION-OWNED", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gra_Gsra_Rc_Rcp = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra_Gsra_Rc_Rcp", "#CTR-TOTAL-GRA-GSRA-RC-RCP", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Total_Ra_Sra_Mdo = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Sra_Mdo", "#CTR-TOTAL-RA-SRA-MDO", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Cref_Group_Gn = localVariables.newFieldInRecord("pnd_Ctr_Total_Cref_Group_Gn", "#CTR-TOTAL-CREF-GROUP-GN", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Cref_Group_Fl = localVariables.newFieldInRecord("pnd_Ctr_Total_Cref_Group_Fl", "#CTR-TOTAL-CREF-GROUP-FL", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Cref_Group_Il = localVariables.newFieldInRecord("pnd_Ctr_Total_Cref_Group_Il", "#CTR-TOTAL-CREF-GROUP-IL", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Cref_Indvl_Gn = localVariables.newFieldInRecord("pnd_Ctr_Total_Cref_Indvl_Gn", "#CTR-TOTAL-CREF-INDVL-GN", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Cref_Indvl_Fl = localVariables.newFieldInRecord("pnd_Ctr_Total_Cref_Indvl_Fl", "#CTR-TOTAL-CREF-INDVL-FL", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Cref_Indvl_Il = localVariables.newFieldInRecord("pnd_Ctr_Total_Cref_Indvl_Il", "#CTR-TOTAL-CREF-INDVL-IL", FieldType.NUMERIC, 7);
        pnd_Break = localVariables.newFieldInRecord("pnd_Break", "#BREAK", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_Eml = localVariables.newFieldInRecord("pnd_Eml", "#EML", FieldType.BOOLEAN, 1);
        pnd_Bad_Address = localVariables.newFieldInRecord("pnd_Bad_Address", "#BAD-ADDRESS", FieldType.BOOLEAN, 1);
        pnd_Sum_Tiaa_Cref = localVariables.newFieldInRecord("pnd_Sum_Tiaa_Cref", "#SUM-TIAA-CREF", FieldType.NUMERIC, 2);
        pnd_Contract_Type_A = localVariables.newFieldInRecord("pnd_Contract_Type_A", "#CONTRACT-TYPE-A", FieldType.STRING, 2);
        pnd_Contract_State_A = localVariables.newFieldInRecord("pnd_Contract_State_A", "#CONTRACT-STATE-A", FieldType.STRING, 2);
        pnd_Ctr_Cref_Contract_A = localVariables.newFieldInRecord("pnd_Ctr_Cref_Contract_A", "#CTR-CREF-CONTRACT-A", FieldType.NUMERIC, 2);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 2);
        pnd_Ctr_File1 = localVariables.newFieldInRecord("pnd_Ctr_File1", "#CTR-FILE1", FieldType.NUMERIC, 5);
        pnd_Ctr_File2 = localVariables.newFieldInRecord("pnd_Ctr_File2", "#CTR-FILE2", FieldType.NUMERIC, 5);

        pnd_Ctrs = localVariables.newGroupInRecord("pnd_Ctrs", "#CTRS");
        pnd_Ctrs_Pnd_Ctr_Cref_Group_Gn = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Group_Gn", "#CTR-CREF-GROUP-GN", FieldType.NUMERIC, 2);
        pnd_Ctrs_Pnd_Ctr_Cref_Group_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Group_Fl", "#CTR-CREF-GROUP-FL", FieldType.NUMERIC, 2);
        pnd_Ctrs_Pnd_Ctr_Cref_Group_Il = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Group_Il", "#CTR-CREF-GROUP-IL", FieldType.NUMERIC, 2);
        pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Gn = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Gn", "#CTR-CREF-INDVL-GN", FieldType.NUMERIC, 2);
        pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Fl", "#CTR-CREF-INDVL-FL", FieldType.NUMERIC, 2);
        pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Il = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Il", "#CTR-CREF-INDVL-IL", FieldType.NUMERIC, 2);

        pnd_Output = localVariables.newGroupInRecord("pnd_Output", "#OUTPUT");
        pnd_Output_Pnd_Pin_A12 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);

        pnd_Output__R_Field_5 = pnd_Output.newGroupInGroup("pnd_Output__R_Field_5", "REDEFINE", pnd_Output_Pnd_Pin_A12);
        pnd_Output_Pnd_Pin_N7 = pnd_Output__R_Field_5.newFieldInGroup("pnd_Output_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);
        pnd_Output_Pnd_Pin_A5 = pnd_Output__R_Field_5.newFieldInGroup("pnd_Output_Pnd_Pin_A5", "#PIN-A5", FieldType.STRING, 5);
        pnd_Output_Pnd_Full_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Output_Pnd_Address_Line_Txt = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 35, 
            new DbsArrayController(1, 6));
        pnd_Output_Pnd_Postal_Data = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 32);
        pnd_Output_Pnd_Address_Type_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 1);
        pnd_Output_Pnd_Letter_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Cref_Contract = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cref_Contract", "#CTR-CREF-CONTRACT", FieldType.NUMERIC, 2);
        pnd_Output_Pnd_Package_Code = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);

        pnd_Output_Pnd_Contract_Details = pnd_Output.newGroupArrayInGroup("pnd_Output_Pnd_Contract_Details", "#CONTRACT-DETAILS", new DbsArrayController(1, 
            10));
        pnd_Output_Pnd_Cref_Contracts = pnd_Output_Pnd_Contract_Details.newFieldInGroup("pnd_Output_Pnd_Cref_Contracts", "#CREF-CONTRACTS", FieldType.STRING, 
            8);
        pnd_Output_Pnd_Contract_Type = pnd_Output_Pnd_Contract_Details.newFieldInGroup("pnd_Output_Pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 
            2);
        pnd_Output_Pnd_Contract_State = pnd_Output_Pnd_Contract_Details.newFieldInGroup("pnd_Output_Pnd_Contract_State", "#CONTRACT-STATE", FieldType.STRING, 
            2);
        pnd_Output_Pnd_Multi_Plan_Ctr = pnd_Output_Pnd_Contract_Details.newFieldInGroup("pnd_Output_Pnd_Multi_Plan_Ctr", "#MULTI-PLAN-CTR", FieldType.NUMERIC, 
            2);
        pnd_Output_Pnd_Plan_Name = pnd_Output_Pnd_Contract_Details.newFieldArrayInGroup("pnd_Output_Pnd_Plan_Name", "#PLAN-NAME", FieldType.STRING, 80, 
            new DbsArrayController(1, 10));
        pnd_Output_Pnd_Share_Class = pnd_Output_Pnd_Contract_Details.newFieldArrayInGroup("pnd_Output_Pnd_Share_Class", "#SHARE-CLASS", FieldType.STRING, 
            2, new DbsArrayController(1, 10));
        pnd_Output_Pnd_Institution_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 120);
        pnd_Output_Pnd_Effective_Date = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.STRING, 8);
        pnd_Output_Pnd_Email_Address = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Email_Address", "#EMAIL-ADDRESS", FieldType.STRING, 100);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Valid_Record.setInitialValue(false);
        pnd_Accepted.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridp640() throws Exception
    {
        super("Ridp640");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  EXCEPTION REPORT
        //*  STATISTICS REPORT                                                                                                                                            //Natural: FORMAT ( 1 ) LS = 120 PS = 60
        if (condition(getReports().getAstLinesLeft(1).less(5)))                                                                                                           //Natural: FORMAT ( 2 ) LS = 120 PS = 60;//Natural: FORMAT LS = 80 PS = 60;//Natural: NEWPAGE ( 1 ) IF LESS THAN 5 LINES
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        //*  OPEN MQ QUEUE                                       /* JCA20150324
        getReports().write(0, " FETCHING MDMP011");                                                                                                                       //Natural: WRITE ' FETCHING MDMP011'
        if (Global.isEscape()) return;
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 30T '2016 CREF REDESIGN SHARE CLASS ENDORSEMENT' 84T 'DATE:' *DATU / 43T 'EXCEPTIONS LIST' 84T 'TIME:' *TIME ( EM = XXXXXXXX ) / 84T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) // 2T 'PIN           TIAA NBR   STATE   REMARKS'/ 2T '------------------------------------------------------------'/
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT 'PROGRAM:' *PROGRAM 27T '2016 CREF REDESIGN SHARE CLASS ENDORSEMENT' 84T 'DATE:' *DATU / 42T 'BAD ADDRESS REPORT' 84T 'TIME:' *TIME ( EM = XXXXXXXX ) / 84T 'PAGE:' 3X *PAGE-NUMBER ( 2 ) // '--------------------------------------------------------------------'/ '                                           |---GROUP--  ---INDVL-- |'/ '  PIN          NAME                         |GN  FL  IN  GN  FL  IL |'/ '--------------------------------------------------------------------'/
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #INPUT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  PINEXP
            pnd_Ctr_Contract_Read.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CTR-CONTRACT-READ
            //*                                                                                                                                                           //Natural: AT BREAK OF #INPUT-RECORD.#PIN-A12
            //*                                                                                                                                                           //Natural: AT END OF DATA
            if (condition(pnd_Break.getBoolean()))                                                                                                                        //Natural: IF #BREAK
            {
                pnd_Break.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #BREAK
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP
            pnd_Prev_Pin_N12.setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-N12 TO #PREV-PIN-N12
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Institution_Name);                                                                                           //Natural: MOVE #INPUT-RECORD.#INSTITUTION-NAME TO #PREV-INST-NAME
            pnd_Prev_Cover.setValue(pnd_Input_Record_Pnd_Cover_Letter_Ind);                                                                                               //Natural: MOVE #INPUT-RECORD.#COVER-LETTER-IND TO #PREV-COVER
            pnd_Prev_Effective_Date.setValue(pnd_Input_Record_Pnd_Effective_Date);                                                                                        //Natural: MOVE #INPUT-RECORD.#EFFECTIVE-DATE TO #PREV-EFFECTIVE-DATE
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
        }                                                                                                                                                                 //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  WRITE LAST RECORD AFTER 999999999999
        if (condition(pnd_Ctr_Cref_Contract_A.greater(getZero())))                                                                                                        //Natural: IF #CTR-CREF-CONTRACT-A GT 0
        {
            //*    PERFORM CHECK-CPM-PROFILE
            //*    IF #EML OR NOT(#BAD-ADDRESS)
            //*      MOVE #PREV-PIN-A12         TO #OUTPUT.#PIN-A12
            //*  PINEXP >>>
            if (condition(pnd_Prev_Pin_N12.greaterOrEqual(new DbsDecimal("100000000000"))))                                                                               //Natural: IF #PREV-PIN-N12 GE 100000000000
            {
                pnd_Output_Pnd_Pin_A12.setValue(pnd_Prev_Pin_N12);                                                                                                        //Natural: MOVE #PREV-PIN-N12 TO #OUTPUT.#PIN-A12
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Output_Pnd_Pin_N7.setValue(pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7);                                                                                         //Natural: MOVE #PREV-PIN-N7 TO #OUTPUT.#PIN-N7
                pnd_Output_Pnd_Pin_A5.setValue("     ");                                                                                                                  //Natural: MOVE '     ' TO #OUTPUT.#PIN-A5
                //*  PINEXP <<<
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_Pnd_Ctr_Cref_Contract.setValue(pnd_Ctr_Cref_Contract_A);                                                                                           //Natural: MOVE #CTR-CREF-CONTRACT-A TO #OUTPUT.#CTR-CREF-CONTRACT
            pnd_Output_Pnd_Letter_Type.setValue(pnd_Prev_Cover);                                                                                                          //Natural: MOVE #PREV-COVER TO #OUTPUT.#LETTER-TYPE
            pnd_Output_Pnd_Institution_Name.setValue(pnd_Prev_Inst_Name);                                                                                                 //Natural: MOVE #PREV-INST-NAME TO #OUTPUT.#INSTITUTION-NAME
            pnd_Output_Pnd_Effective_Date.setValue(pnd_Prev_Effective_Date);                                                                                              //Natural: MOVE #PREV-EFFECTIVE-DATE TO #OUTPUT.#EFFECTIVE-DATE
                                                                                                                                                                          //Natural: PERFORM WRITE-DATA-TO-FILE
            sub_Write_Data_To_File();
            if (condition(Global.isEscape())) {return;}
            //*    ELSE
            //*      PERFORM WRITE-BAD-ADDRESS-REPORT
            //*    END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-STATISTICS-REPORT
        sub_Write_Statistics_Report();
        if (condition(Global.isEscape())) {return;}
        //*  CLOSE MQ QUEUE
        //*  WRITE 'FETCHING MDMP0012'
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* **********************************************************************
        //*                 S T A R T    O F    S U B R O U T I N E S
        //* **********************************************************************
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-RECORD
        //*  GRA / GSRA / RC / RCP CONTRACTS
        //*  RA / SRA / MDO CONTRACTS
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RECORD-IN-MDM
        //*  --------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PIN-STATUS
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME-ADDRESS-DETAILS
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BAD-ADDRESS-REPORT
        //*  ---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-ARRAY
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-PLAN-DETAILS
        //*  -----------------------------------
        //*  DEFINE SUBROUTINE CHECK-CPM-PROFILE
        //*  -----------------------------------
        //*    IF +TRACE
        //*      WRITE *PROGRAM 'CALLING CPMN550...
        //*    END-IF
        //*    #COMM-CPMN550.#COMM-CUSTOMER-PIN := #PREV-PIN
        //*    #COMM-CPMN550.#COMM-CUSTOMER-TYP := 'PH'
        //*    #COMM-CPMN550.#COMM-CUSTOMER-FK  := 'PPPPPPPPPPPPPPP'
        //*    #COMM-CPMN550.#COMM-DOC-ID       := 'D19'
        //*    CALLNAT 'CPMN550' #COMM-CPMN550
        //*    IF #COMM-CPMN550.#COMM-CUSTOMER-EMAIL NE ' '
        //*      #EML := TRUE
        //*      #OUTPUT.#EMAIL-ADDRESS  := #COMM-CPMN550.#COMM-CUSTOMER-EMAIL
        //*    ELSE
        //*      #EML := FALSE
        //*    END-IF
        //*  END-SUBROUTINE /* CHECK-CPM-PROFILE
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA-TO-FILE
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-STATISTICS-REPORT
        //* ******************************************
        //*                              #CTR-PH-DECEASED               +
    }
    private void sub_Validate_Record() throws Exception                                                                                                                   //Natural: VALIDATE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        if (condition(pnd_Prev_Contract.equals(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr)))                                                                                    //Natural: IF #PREV-CONTRACT EQ #CREF-CNTRCT-NBR
        {
            if (condition(pnd_Valid_Record.getBoolean()))                                                                                                                 //Natural: IF #VALID-RECORD
            {
                                                                                                                                                                          //Natural: PERFORM ADD-PLAN-DETAILS
                sub_Add_Plan_Details();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prev_Contract.setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                                                 //Natural: MOVE #CREF-CNTRCT-NBR TO #PREV-CONTRACT
                                                                                                                                                                          //Natural: PERFORM CHECK-RECORD-IN-MDM
        sub_Check_Record_In_Mdm();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Valid_Record.getBoolean()))                                                                                                                     //Natural: IF #VALID-RECORD
        {
            //*  GRA
            //*  GRA
            //*  GRA
            //*  GRAII
            //*  GSRA
            //*  GSRA
            //*  RC
            //*  RCP
            short decideConditionsMet674 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CREF-CNTRCT-NBR EQ '40000100' THRU '49499999' OR #CREF-CNTRCT-NBR EQ '10000010' THRU '10099999' OR #CREF-CNTRCT-NBR EQ '10250000' THRU '18999999' OR #CREF-CNTRCT-NBR EQ '10100000' THRU '10249999' OR #CREF-CNTRCT-NBR EQ 'M0000100' THRU 'M8499999' OR #CREF-CNTRCT-NBR EQ 'J5000000' THRU 'J7999999' OR #CREF-CNTRCT-NBR EQ 'H0000000' THRU 'H0999999' OR #CREF-CNTRCT-NBR EQ 'H5000000' THRU 'H5999999'
            if (condition(((((((((pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("40000100") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("49499999")) 
                || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("10000010") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("10099999"))) || 
                (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("10250000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("18999999"))) || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("10100000") 
                && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("10249999"))) || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("M0000100") && 
                pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("M8499999"))) || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("J5000000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("J7999999"))) 
                || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("H0000000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("H0999999"))) || 
                (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("H5000000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("H5999999")))))
            {
                decideConditionsMet674++;
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                pnd_Ctr_Total_Gra_Gsra_Rc_Rcp.nadd(1);                                                                                                                    //Natural: ADD 1 TO #CTR-TOTAL-GRA-GSRA-RC-RCP
                pnd_Ctr_Cref_Contract_A.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-CREF-CONTRACT-A
                pnd_Contract_Type_A.setValue("GR");                                                                                                                       //Natural: MOVE 'GR' TO #CONTRACT-TYPE-A
                //*  FL
                short decideConditionsMet683 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #ISSUE-STATE-CDE;//Natural: VALUE 'FL'
                if (condition((pnd_Input_Record_Pnd_Issue_State_Cde.equals("FL"))))
                {
                    decideConditionsMet683++;
                    pnd_Ctr_Total_Cref_Group_Fl.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTR-TOTAL-CREF-GROUP-FL
                    pnd_Ctrs_Pnd_Ctr_Cref_Group_Fl.nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR-CREF-GROUP-FL
                    //*  IL
                    pnd_Contract_State_A.setValue("FL");                                                                                                                  //Natural: MOVE 'FL' TO #CONTRACT-STATE-A
                }                                                                                                                                                         //Natural: VALUE 'IL'
                else if (condition((pnd_Input_Record_Pnd_Issue_State_Cde.equals("IL"))))
                {
                    decideConditionsMet683++;
                    pnd_Ctr_Total_Cref_Group_Il.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTR-TOTAL-CREF-GROUP-IL
                    pnd_Ctrs_Pnd_Ctr_Cref_Group_Il.nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR-CREF-GROUP-IL
                    //*  GN
                    pnd_Contract_State_A.setValue("IL");                                                                                                                  //Natural: MOVE 'IL' TO #CONTRACT-STATE-A
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Ctr_Total_Cref_Group_Gn.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTR-TOTAL-CREF-GROUP-GN
                    pnd_Ctrs_Pnd_Ctr_Cref_Group_Gn.nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR-CREF-GROUP-GN
                    pnd_Contract_State_A.setValue("GN");                                                                                                                  //Natural: MOVE 'GN' TO #CONTRACT-STATE-A
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  RA
                //*  RA
                //*  RA
                //*  RA
                //*  RA
                //*  RA
                //*  RA PRE
                //*  SRA
                //*  SRA
                //*  RAMDO
                //*  SRAMDO
                //*  GSRMDO
                //*  GRAMDO
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CREF-CNTRCT-NBR EQ 'V0100000' THRU 'V9599999' OR #CREF-CNTRCT-NBR EQ 'U9000000' THRU 'U9999999' OR #CREF-CNTRCT-NBR EQ 'P0316250' THRU 'P8999999' OR #CREF-CNTRCT-NBR EQ 'Q0000010' THRU 'Q7999999' OR #CREF-CNTRCT-NBR EQ 'Q8015000' THRU 'Q9999989' OR #CREF-CNTRCT-NBR EQ 'U0100000' THRU 'U7999999' OR #CREF-CNTRCT-NBR EQ 'P0000010' THRU 'P0316249' OR #CREF-CNTRCT-NBR EQ 'M9000000' THRU 'M9899999' OR #CREF-CNTRCT-NBR EQ 'J0000010' THRU 'J4999999' OR #CREF-CNTRCT-NBR EQ 'U8000000' THRU 'U8499999' OR #CREF-CNTRCT-NBR EQ 'J8500000' THRU 'J8799999' OR #CREF-CNTRCT-NBR EQ 'J8860000' THRU 'J8959999' OR #CREF-CNTRCT-NBR EQ 'U8740000' THRU 'U8939999'
            else if (condition((((((((((((((pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("V0100000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("V9599999")) 
                || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("U9000000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("U9999999"))) || 
                (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("P0316250") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("P8999999"))) || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("Q0000010") 
                && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("Q7999999"))) || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("Q8015000") && 
                pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("Q9999989"))) || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("U0100000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("U7999999"))) 
                || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("P0000010") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("P0316249"))) || 
                (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("M9000000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("M9899999"))) || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("J0000010") 
                && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("J4999999"))) || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("U8000000") && 
                pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("U8499999"))) || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("J8500000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("J8799999"))) 
                || (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("J8860000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("J8959999"))) || 
                (pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.greaterOrEqual("U8740000") && pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.lessOrEqual("U8939999")))))
            {
                decideConditionsMet674++;
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                pnd_Ctr_Total_Ra_Sra_Mdo.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-RA-SRA-MDO
                pnd_Ctr_Cref_Contract_A.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-CREF-CONTRACT-A
                pnd_Contract_Type_A.setValue("IN");                                                                                                                       //Natural: MOVE 'IN' TO #CONTRACT-TYPE-A
                //*  FL
                short decideConditionsMet723 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #ISSUE-STATE-CDE;//Natural: VALUE 'FL'
                if (condition((pnd_Input_Record_Pnd_Issue_State_Cde.equals("FL"))))
                {
                    decideConditionsMet723++;
                    pnd_Ctr_Total_Cref_Indvl_Fl.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTR-TOTAL-CREF-INDVL-FL
                    pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Fl.nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR-CREF-INDVL-FL
                    //*  IL
                    pnd_Contract_State_A.setValue("FL");                                                                                                                  //Natural: MOVE 'FL' TO #CONTRACT-STATE-A
                }                                                                                                                                                         //Natural: VALUE 'IL'
                else if (condition((pnd_Input_Record_Pnd_Issue_State_Cde.equals("IL"))))
                {
                    decideConditionsMet723++;
                    pnd_Ctr_Total_Cref_Indvl_Il.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTR-TOTAL-CREF-INDVL-IL
                    pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Il.nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR-CREF-INDVL-IL
                    //*  GN
                    pnd_Contract_State_A.setValue("IL");                                                                                                                  //Natural: MOVE 'IL' TO #CONTRACT-STATE-A
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Ctr_Total_Cref_Indvl_Gn.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTR-TOTAL-CREF-INDVL-GN
                    pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Gn.nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR-CREF-INDVL-GN
                    pnd_Contract_State_A.setValue("GN");                                                                                                                  //Natural: MOVE 'GN' TO #CONTRACT-STATE-A
                }                                                                                                                                                         //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Ctr_Contract_Outof_Range.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CTR-CONTRACT-OUTOF-RANGE
                pnd_Remarks.setValue("CONTRACT OUT OF RANGE");                                                                                                            //Natural: ASSIGN #REMARKS := 'CONTRACT OUT OF RANGE'
                getReports().write(1, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Cref_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-A12 3X #CREF-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  UNCHECK LATER
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE-RECORD
    }
    private void sub_Check_Record_In_Mdm() throws Exception                                                                                                               //Natural: CHECK-RECORD-IN-MDM
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------
        //*  REPLACE ALL MDM*210 TO MDM*211 FOR PINEXP
        //*  PINEXP MODIFY FOR MDM >>>
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                 //Natural: MOVE #CREF-CNTRCT-NBR TO #MDMA211.#I-CONTRACT-NUMBER
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue("01");                                                                                                   //Natural: MOVE '01' TO #MDMA211.#I-PAYEE-CODE-A2
        //*    WRITE 'CALLING MDMN211A TO GET CONTRACT INFORMATION'
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //*      WRITE '=' #MDMA211.#O-RETURN-CODE
        //*  PINEXP <<<
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA211.#O-RETURN-CODE NE '0000'
        {
            pnd_Remarks.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Text());                                                                                          //Natural: ASSIGN #REMARKS := #MDMA211.#O-RETURN-TEXT
            getReports().write(1, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Cref_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-A12 3X #CREF-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Contract_Not_In_Mdm.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-DA-CONTRACT-NOT-IN-MDM
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*                          /* PINEXP MODIFY MDMA210 TO MDMA211
        if (condition(! (pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals(" ") || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("H")         //Natural: IF NOT ( #MDMA211.#O-CONTRACT-STATUS-CODE = ' ' OR = 'H' OR = 'Y' )
            || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("Y"))))
        {
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("CONTRACT INACTIVE IN MDM-STATUS CODE:", pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code()));                      //Natural: COMPRESS 'CONTRACT INACTIVE IN MDM-STATUS CODE:' #MDMA211.#O-CONTRACT-STATUS-CODE TO #REMARKS
            getReports().write(1, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Cref_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-A12 3X #CREF-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Inactive_In_Mdm.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DA-INACTIVE-IN-MDM
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PIN-STATUS
            sub_Determine_Pin_Status();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Accepted.getBoolean()))                                                                                                                     //Natural: IF #ACCEPTED
            {
                pnd_Valid_Record.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-RECORD-IN-MDM
    }
    private void sub_Determine_Pin_Status() throws Exception                                                                                                              //Natural: DETERMINE-PIN-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------
        //*  PINEXP >>>      /* JCA20150324
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        //*    MOVE #INPUT-RECORD.#PIN TO #MDMA101.#I-PIN          /* JCA20150324
        //*    WRITE 'CALLING MDMN101A TO GET ADDRESS/NAME'
        //*  PINEXP >>>
        if (condition(pnd_Input_Record_Pnd_Pin_A12.notEquals(" ")))                                                                                                       //Natural: IF #INPUT-RECORD.#PIN-A12 NE ' '
        {
            if (condition(pnd_Input_Record_Pnd_Pin_N5.equals(getZero())))                                                                                                 //Natural: IF #INPUT-RECORD.#PIN-N5 = 0
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N7);                                                                          //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #INPUT-RECORD.#PIN-N7
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                         //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #INPUT-RECORD.#PIN-N12
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP <<<
        }                                                                                                                                                                 //Natural: END-IF
        //*  PINEXP    /* JCA20150324
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  REJECT DECEASED PARTICIPANTS
        //*  JCA20150324
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero())))                                                                                //Natural: IF #MDMA101.#O-DATE-OF-DEATH GT 0
        {
            pnd_Ctr_Ph_Deceased.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CTR-PH-DECEASED
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("P/H DECEASED - DOD:", pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death()));                                               //Natural: COMPRESS 'P/H DECEASED - DOD:' #MDMA101.#O-DATE-OF-DEATH TO #REMARKS
            getReports().write(1, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Cref_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-A12 3X #CREF-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Accepted.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #ACCEPTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  REJECT PARTICIPANTS W/ STOP ALL MAILING SUPPRESSION CODE
            //*  PINEXP <<<
            //*  JCA20150324
            DbsUtil.examine(new ExamineSource(pdaMdma101.getPnd_Mdma101_Pnd_O_Mail_Code().getValue("*")), new ExamineSearch("01"), new ExamineGivingPosition(pnd_I));     //Natural: EXAMINE #MDMA101.#O-MAIL-CODE ( * ) FOR '01' GIVING POSITION #I
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I GT 0
            {
                pnd_Ctr_Ph_With_Mail_Suppression.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTR-PH-WITH-MAIL-SUPPRESSION
                pnd_Remarks.setValue(DbsUtil.compress("P/H MAIL SUPPRESSED - MAIL CODE: 01"));                                                                            //Natural: COMPRESS 'P/H MAIL SUPPRESSED - MAIL CODE: 01' TO #REMARKS
                getReports().write(1, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Cref_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-A12 3X #CREF-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Accepted.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #ACCEPTED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Accepted.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #ACCEPTED
                //*          PERFORM GET-NAME-ADDRESS-DETAILS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE-PIN-STATUS
    }
    private void sub_Get_Name_Address_Details() throws Exception                                                                                                          //Natural: GET-NAME-ADDRESS-DETAILS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------
        //*  MODIFY FOR PINEXP FOR #MDMA100 TO #MDMA101 IN THIS SUBROUTINE
        pnd_Bad_Address.resetInitial();                                                                                                                                   //Natural: RESET INITIAL #BAD-ADDRESS
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Status_Code().equals("B")))                                                                            //Natural: IF #MDMA101.#O-BASE-ADDRESS-STATUS-CODE EQ 'B'
        {
            pnd_Bad_Address.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #BAD-ADDRESS
            pnd_Output_Pnd_Address_Type_Cde.setValue("B");                                                                                                                //Natural: ASSIGN #OUTPUT.#ADDRESS-TYPE-CDE := 'B'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_Pnd_Address_Line_Txt.getValue(1).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                                  //Natural: ASSIGN #OUTPUT.#ADDRESS-LINE-TXT ( 1 ) := #MDMA101.#O-BASE-ADDRESS-LINE-1
            pnd_Output_Pnd_Address_Line_Txt.getValue(2).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2());                                                  //Natural: ASSIGN #OUTPUT.#ADDRESS-LINE-TXT ( 2 ) := #MDMA101.#O-BASE-ADDRESS-LINE-2
            pnd_Output_Pnd_Address_Line_Txt.getValue(3).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3());                                                  //Natural: ASSIGN #OUTPUT.#ADDRESS-LINE-TXT ( 3 ) := #MDMA101.#O-BASE-ADDRESS-LINE-3
            pnd_Output_Pnd_Address_Line_Txt.getValue(4).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_4());                                                  //Natural: ASSIGN #OUTPUT.#ADDRESS-LINE-TXT ( 4 ) := #MDMA101.#O-BASE-ADDRESS-LINE-4
            pnd_Output_Pnd_Postal_Data.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Postal_Data());                                                              //Natural: ASSIGN #OUTPUT.#POSTAL-DATA := #MDMA101.#O-BASE-ADDRESS-POSTAL-DATA
            pnd_Output_Pnd_Address_Type_Cde.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Type_Code());                                                           //Natural: ASSIGN #OUTPUT.#ADDRESS-TYPE-CDE := #MDMA101.#O-BASE-ADDRESS-TYPE-CODE
            pnd_Bad_Address.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #BAD-ADDRESS
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Output_Pnd_Address_Type_Cde.equals("B")))                                                                                                       //Natural: IF #ADDRESS-TYPE-CDE EQ 'B'
        {
            pnd_Bad_Address.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #BAD-ADDRESS
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Output_Pnd_Full_Name.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_Preferred_First_Name(), " ", pdaMdma101.getPnd_Mdma101_Pnd_O_Preferred_Middle_Name(),  //Natural: COMPRESS #MDMA101.#O-PREFERRED-FIRST-NAME " " #MDMA101.#O-PREFERRED-MIDDLE-NAME " " #MDMA101.#O-PREFERRED-LAST-NAME " " #MDMA101.#O-PREFERRED-SUFFIX TO #OUTPUT.#FULL-NAME
            " ", pdaMdma101.getPnd_Mdma101_Pnd_O_Preferred_Last_Name(), " ", pdaMdma101.getPnd_Mdma101_Pnd_O_Preferred_Suffix()));
        //*  GET-NAME-ADDRESS-DETAILS
    }
    private void sub_Write_Bad_Address_Report() throws Exception                                                                                                          //Natural: WRITE-BAD-ADDRESS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------
        getReports().write(2, pnd_Prev_Pin_N12,new TabSetting(15),pnd_Output_Pnd_Full_Name,new TabSetting(51),pnd_Ctrs_Pnd_Ctr_Cref_Group_Gn,new TabSetting(55),pnd_Ctrs_Pnd_Ctr_Cref_Group_Fl,new  //Natural: WRITE ( 2 ) #PREV-PIN-N12 15T #OUTPUT.#FULL-NAME 51T #CTRS.#CTR-CREF-GROUP-GN 55T #CTRS.#CTR-CREF-GROUP-FL 59T #CTRS.#CTR-CREF-GROUP-IL 63T #CTRS.#CTR-CREF-INDVL-GN 67T #CTRS.#CTR-CREF-INDVL-FL 71T #CTRS.#CTR-CREF-INDVL-IL
            TabSetting(59),pnd_Ctrs_Pnd_Ctr_Cref_Group_Il,new TabSetting(63),pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Gn,new TabSetting(67),pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Fl,new 
            TabSetting(71),pnd_Ctrs_Pnd_Ctr_Cref_Indvl_Il);
        if (Global.isEscape()) return;
        //*  WRITE-BAD-ADDRESS-REPORT
    }
    private void sub_Add_Contract_To_Array() throws Exception                                                                                                             //Natural: ADD-CONTRACT-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------
        pnd_X.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #X
        pnd_Y.reset();                                                                                                                                                    //Natural: RESET #Y
        pnd_Output_Pnd_Cref_Contracts.getValue(pnd_X).setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                     //Natural: MOVE #CREF-CNTRCT-NBR TO #CREF-CONTRACTS ( #X )
        pnd_Output_Pnd_Contract_Type.getValue(pnd_X).setValue(pnd_Contract_Type_A);                                                                                       //Natural: MOVE #CONTRACT-TYPE-A TO #CONTRACT-TYPE ( #X )
        pnd_Output_Pnd_Contract_State.getValue(pnd_X).setValue(pnd_Contract_State_A);                                                                                     //Natural: MOVE #CONTRACT-STATE-A TO #CONTRACT-STATE ( #X )
                                                                                                                                                                          //Natural: PERFORM ADD-PLAN-DETAILS
        sub_Add_Plan_Details();
        if (condition(Global.isEscape())) {return;}
        //*  ADD-CONTRACT-TO-ARRAY
    }
    private void sub_Add_Plan_Details() throws Exception                                                                                                                  //Natural: ADD-PLAN-DETAILS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        pnd_Y.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #Y
        getReports().write(0, "=",pnd_Input_Record_Pnd_Pin_A12);                                                                                                          //Natural: WRITE '=' #INPUT-RECORD.#PIN-A12
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                                                  //Natural: WRITE '=' #CREF-CNTRCT-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_X,"=",pnd_Y);                                                                                                                       //Natural: WRITE '=' #X '=' #Y
        if (Global.isEscape()) return;
        pnd_Output_Pnd_Multi_Plan_Ctr.getValue(pnd_X).nadd(1);                                                                                                            //Natural: ADD 1 TO #MULTI-PLAN-CTR ( #X )
        pnd_Output_Pnd_Plan_Name.getValue(pnd_X,pnd_Y).setValue(pnd_Input_Record_Pnd_Plan_Name_A);                                                                        //Natural: MOVE #PLAN-NAME-A TO #PLAN-NAME ( #X,#Y )
        pnd_Output_Pnd_Share_Class.getValue(pnd_X,pnd_Y).setValue(pnd_Input_Record_Pnd_Share_Class_A);                                                                    //Natural: MOVE #SHARE-CLASS-A TO #SHARE-CLASS ( #X,#Y )
        //*  ADD-PLAN-DETAILS
    }
    private void sub_Write_Data_To_File() throws Exception                                                                                                                //Natural: WRITE-DATA-TO-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        //*    DECIDE FOR FIRST CONDITION
        //*      WHEN NOT(#EML) AND NOT(#BAD-ADDRESS)
        pnd_Ctr_File1.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #CTR-FILE1
        pnd_Output_Pnd_Package_Code.setValue("RDR2016C");                                                                                                                 //Natural: MOVE 'RDR2016C' TO #PACKAGE-CODE
        getWorkFiles().write(2, false, pnd_Output);                                                                                                                       //Natural: WRITE WORK FILE 2 #OUTPUT
        //*      WHEN #EML
        //*        ADD 1 TO #CTR-FILE2
        //*        MOVE 'RDR2016D' TO #PACKAGE-CODE
        //*        WRITE WORK FILE 3 #OUTPUT
        //*      WHEN NONE
        //*        IGNORE
        //*    END-DECIDE
        //*  WRITE-DATA-TO-FILE
    }
    private void sub_Write_Statistics_Report() throws Exception                                                                                                           //Natural: WRITE-STATISTICS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ctr_Contract_Rejected.compute(new ComputeParameters(false, pnd_Ctr_Contract_Rejected), pnd_Ctr_Da_Contract_Not_In_Mdm.add(pnd_Ctr_Da_Inactive_In_Mdm).add(pnd_Ctr_Ph_With_Mail_Suppression).add(pnd_Ctr_Out_Of_State).add(pnd_Ctr_Contract_Outof_Range).add(pnd_Ctr_Plan_Not_For_Proc).add(pnd_Ctr_Institution_Owned).add(pnd_Ctr_Contract_Dispparoved_State).add(pnd_Ctr_Duplicate_Contract)); //Natural: ASSIGN #CTR-CONTRACT-REJECTED := #CTR-DA-CONTRACT-NOT-IN-MDM + #CTR-DA-INACTIVE-IN-MDM + #CTR-PH-WITH-MAIL-SUPPRESSION + #CTR-OUT-OF-STATE + #CTR-CONTRACT-OUTOF-RANGE + #CTR-PLAN-NOT-FOR-PROC + #CTR-INSTITUTION-OWNED + #CTR-CONTRACT-DISPPAROVED-STATE+ #CTR-DUPLICATE-CONTRACT
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ         :",pnd_Ctr_Contract_Read);                                                                                  //Natural: WRITE 'NUMBER OF RECORDS READ         :' #CTR-CONTRACT-READ
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS ACCEPTED     :",pnd_Ctr_Contract_Accepted);                                                                              //Natural: WRITE 'NUMBER OF RECORDS ACCEPTED     :' #CTR-CONTRACT-ACCEPTED
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS REJECTED     :",pnd_Ctr_Contract_Rejected);                                                                              //Natural: WRITE 'NUMBER OF RECORDS REJECTED     :' #CTR-CONTRACT-REJECTED
        if (Global.isEscape()) return;
        getReports().write(0, "BREAKDOWN OF RECORDS REJECTED  :");                                                                                                        //Natural: WRITE 'BREAKDOWN OF RECORDS REJECTED  :'
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT NOT IN MDM     :",pnd_Ctr_Da_Contract_Not_In_Mdm);                                                                              //Natural: WRITE ' -CONTRACT NOT IN MDM     :' #CTR-DA-CONTRACT-NOT-IN-MDM
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT INACTIVE IN MDM:",pnd_Ctr_Da_Inactive_In_Mdm);                                                                                  //Natural: WRITE ' -CONTRACT INACTIVE IN MDM:' #CTR-DA-INACTIVE-IN-MDM
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H DECEASED            :",pnd_Ctr_Ph_Deceased);                                                                                         //Natural: WRITE ' -P/H DECEASED            :' #CTR-PH-DECEASED
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H W/ MAIL SUPPRESSION :",pnd_Ctr_Ph_With_Mail_Suppression);                                                                            //Natural: WRITE ' -P/H W/ MAIL SUPPRESSION :' #CTR-PH-WITH-MAIL-SUPPRESSION
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT OUT OF RANGE   :",pnd_Ctr_Contract_Outof_Range);                                                                                //Natural: WRITE ' -CONTRACT OUT OF RANGE   :' #CTR-CONTRACT-OUTOF-RANGE
        if (Global.isEscape()) return;
        getReports().write(0, " -DISAPPROVED STATE       :",pnd_Ctr_Contract_Dispparoved_State);                                                                          //Natural: WRITE ' -DISAPPROVED STATE       :' #CTR-CONTRACT-DISPPAROVED-STATE
        if (Global.isEscape()) return;
        getReports().write(0, " -DUPLICATE CONTRACT      :",pnd_Ctr_Duplicate_Contract);                                                                                  //Natural: WRITE ' -DUPLICATE CONTRACT      :' #CTR-DUPLICATE-CONTRACT
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY PRODUCT GROUP");                                                                                                                  //Natural: WRITE 'TOTAL BY PRODUCT GROUP'
        if (Global.isEscape()) return;
        getReports().write(0, " - GRA/GSRA/RC/RCP :",pnd_Ctr_Total_Gra_Gsra_Rc_Rcp);                                                                                      //Natural: WRITE ' - GRA/GSRA/RC/RCP :' #CTR-TOTAL-GRA-GSRA-RC-RCP
        if (Global.isEscape()) return;
        getReports().write(0, " - RA/SRA/MDO      :",pnd_Ctr_Total_Ra_Sra_Mdo);                                                                                           //Natural: WRITE ' - RA/SRA/MDO      :' #CTR-TOTAL-RA-SRA-MDO
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Ctr_File1);                                                                                                                         //Natural: WRITE '=' #CTR-FILE1
        if (Global.isEscape()) return;
        //*    WRITE '=' #CTR-FILE2
        //*  WRITE-STATISTICS-REPORT
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Input_Record_Pnd_Pin_A12IsBreak = pnd_Input_Record_Pnd_Pin_A12.isBreak(endOfData);
        if (condition(pnd_Input_Record_Pnd_Pin_A12IsBreak))
        {
            //*  PINEXP
            if (condition(pnd_Input_Record_Pnd_Pin_N12.equals(new DbsDecimal("999999999999"))))                                                                           //Natural: IF #INPUT-RECORD.#PIN-N12 EQ 999999999999
            {
                Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                          //Natural: ESCAPE BOTTOM
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ctr_Cref_Contract_A.greater(getZero())))                                                                                                    //Natural: IF #CTR-CREF-CONTRACT-A GT 0
            {
                //*      PERFORM CHECK-CPM-PROFILE
                //*      IF #EML OR NOT(#BAD-ADDRESS)
                //*        MOVE #PREV-PIN-A12         TO #OUTPUT.#PIN-A12    /* PINEXP
                //*  PINEXP >>>
                if (condition(pnd_Prev_Pin_N12.greaterOrEqual(new DbsDecimal("100000000000"))))                                                                           //Natural: IF #PREV-PIN-N12 GE 100000000000
                {
                    pnd_Output_Pnd_Pin_A12.setValue(pnd_Prev_Pin_N12);                                                                                                    //Natural: MOVE #PREV-PIN-N12 TO #OUTPUT.#PIN-A12
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Pnd_Pin_N7.setValue(pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7);                                                                                     //Natural: MOVE #PREV-PIN-N7 TO #OUTPUT.#PIN-N7
                    pnd_Output_Pnd_Pin_A5.setValue("     ");                                                                                                              //Natural: MOVE '     ' TO #OUTPUT.#PIN-A5
                    //*  PINEXP <<<
                }                                                                                                                                                         //Natural: END-IF
                pnd_Output_Pnd_Ctr_Cref_Contract.setValue(pnd_Ctr_Cref_Contract_A);                                                                                       //Natural: MOVE #CTR-CREF-CONTRACT-A TO #OUTPUT.#CTR-CREF-CONTRACT
                pnd_Output_Pnd_Letter_Type.setValue(pnd_Prev_Cover);                                                                                                      //Natural: MOVE #PREV-COVER TO #OUTPUT.#LETTER-TYPE
                pnd_Output_Pnd_Institution_Name.setValue(pnd_Prev_Inst_Name);                                                                                             //Natural: MOVE #PREV-INST-NAME TO #OUTPUT.#INSTITUTION-NAME
                pnd_Output_Pnd_Effective_Date.setValue(pnd_Prev_Effective_Date);                                                                                          //Natural: MOVE #PREV-EFFECTIVE-DATE TO #OUTPUT.#EFFECTIVE-DATE
                                                                                                                                                                          //Natural: PERFORM WRITE-DATA-TO-FILE
                sub_Write_Data_To_File();
                if (condition(Global.isEscape())) {return;}
                //*      ELSE
                //*        PERFORM WRITE-BAD-ADDRESS-REPORT
                //*      END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output.reset();                                                                                                                                           //Natural: RESET #OUTPUT #K #X #Y #CTR-CREF-CONTRACT-A #CTRS
            pnd_K.reset();
            pnd_X.reset();
            pnd_Y.reset();
            pnd_Ctr_Cref_Contract_A.reset();
            pnd_Ctrs.reset();
            pnd_Output_Pnd_Contract_Details.getValue("*").reset();                                                                                                        //Natural: RESET #CONTRACT-DETAILS ( * ) #MULTI-PLAN-CTR ( * )
            pnd_Output_Pnd_Multi_Plan_Ctr.getValue("*").reset();
            pnd_Break.setValue(true);                                                                                                                                     //Natural: MOVE TRUE TO #BREAK
            //*  PINEXP
            pnd_Prev_Pin_N12.setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-N12 TO #PREV-PIN-N12
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Institution_Name);                                                                                           //Natural: MOVE #INPUT-RECORD.#INSTITUTION-NAME TO #PREV-INST-NAME
            pnd_Prev_Cover.setValue(pnd_Input_Record_Pnd_Cover_Letter_Ind);                                                                                               //Natural: MOVE #INPUT-RECORD.#COVER-LETTER-IND TO #PREV-COVER
            pnd_Prev_Effective_Date.setValue(pnd_Input_Record_Pnd_Effective_Date);                                                                                        //Natural: MOVE #INPUT-RECORD.#EFFECTIVE-DATE TO #PREV-EFFECTIVE-DATE
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=120 PS=60");
        Global.format(2, "LS=120 PS=60");
        Global.format(0, "LS=80 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(30),"2016 CREF REDESIGN SHARE CLASS ENDORSEMENT",new 
            TabSetting(84),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(43),"EXCEPTIONS LIST",new TabSetting(84),"TIME:",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,new TabSetting(84),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new TabSetting(2),"PIN           TIAA NBR   STATE   REMARKS",NEWLINE,new 
            TabSetting(2),"------------------------------------------------------------",NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(27),"2016 CREF REDESIGN SHARE CLASS ENDORSEMENT",new 
            TabSetting(84),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(42),"BAD ADDRESS REPORT",new TabSetting(84),"TIME:",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,new TabSetting(84),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(2),NEWLINE,NEWLINE,"--------------------------------------------------------------------",
            NEWLINE,"                                           |---GROUP--  ---INDVL-- |",NEWLINE,"  PIN          NAME                         |GN  FL  IN  GN  FL  IL |",
            NEWLINE,"--------------------------------------------------------------------",NEWLINE);
    }
}
