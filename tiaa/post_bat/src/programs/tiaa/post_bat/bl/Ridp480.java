/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:01 PM
**        * FROM NATURAL PROGRAM : Ridp480
************************************************************
**        * FILE NAME            : Ridp480.java
**        * CLASS NAME           : Ridp480
**        * INSTANCE NAME        : Ridp480
************************************************************
************************************************************************
** PROGRAM     : RIDP480                                              **
** AUTHOR      : JOANNES C. AVE                                       **
** DATE        : 04/07/2008                                           **
** DESCRIPTION : READS DA REPORTING FORCED CASHOUT EXTRACT AND        **
**               REFORMATS DATA AS FEED TO RIDP482                    **
************************************************************************
** HISTORY:                                                           **
** 04/07/2008  : INITIAL IMPLEMENTATION                               **
** 06/14/2010  : ADD GSRA TIAA/CREF ENDORSEMENTS                      **
** 03/24/2015  : COR/NAAD SUNSET - REMOVE ALL REFERENCES TO COR/NAAD  **
**               AND CALL THE MDMN MODULES INSTEAD FOR PARTICIPANT    **
**               AND CONTRACT INFORMATION                             **
** 05/26/2017  : PIN EXPANSION PROJECT                    PINEXP      **
**               MODIFY MDM MODULE TO CALL FOR NEW MDM PIN MODULE     **
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridp480 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;
    private PdaPsta9017 pdaPsta9017;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Pin_A12;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Pin_N7;
    private DbsField pnd_Input_Record_Pnd_Pin_A5;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Pin_N12;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cref_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Date;
    private DbsField pnd_Input_Record_Pnd_Issue_State_Cde;
    private DbsField pnd_Input_Record_Pnd_Plan_Cde;
    private DbsField pnd_Input_Record_Pnd_Cover_Letter_Ind;
    private DbsField pnd_Input_Record_Pnd_Institution_Name;
    private DbsField pnd_Prev_Inst_Name;
    private DbsField pnd_Prev_Pin_A12;
    private DbsField pnd_Prev_Contract;
    private DbsField pnd_Prev_Cover;
    private DbsField pnd_K;
    private DbsField pnd_X;
    private DbsField pnd_Valid_Record;
    private DbsField pnd_Accepted;
    private DbsField pnd_Remarks;
    private DbsField pnd_Lob;
    private DbsField pnd_Participant_Name;
    private DbsField pnd_Ctr_Valid_Contract_Read;
    private DbsField pnd_Ctr_Da_Contract_Not_In_Cor;
    private DbsField pnd_Ctr_Da_Inactive_In_Cor;
    private DbsField pnd_Ctr_Ph_Deceased;
    private DbsField pnd_Ctr_Ph_With_Mail_Suppression;
    private DbsField pnd_Ctr_Contract_Accepted;
    private DbsField pnd_Ctr_Contract_Rejected;
    private DbsField pnd_Ctr_Contract_Read;
    private DbsField pnd_Ctr_Duplicate_Contract;
    private DbsField pnd_Ctr_Terminated_Contract;
    private DbsField pnd_Ctr_Contract_Inactive;
    private DbsField pnd_Ctr_Out_Of_State;
    private DbsField pnd_Ctr_Contract_Outof_Range;
    private DbsField pnd_Ctr_After_Cutoff;
    private DbsField pnd_Ctr_Plan_Not_For_Proc;
    private DbsField pnd_Ctr_Institution_Owned;
    private DbsField pnd_Ctr_Total_Gra;
    private DbsField pnd_Ctr_Total_Gsra;
    private DbsField pnd_Break;
    private DbsField pnd_I;
    private DbsField pnd_A;

    private DbsGroup pnd_Arrays;
    private DbsField pnd_Arrays_Pnd_Combo_Prod;
    private DbsField pnd_Arrays_Pnd_Combo_Ctr;
    private DbsField pnd_Combo;
    private DbsField pnd_Sum_Tiaa_Cref;

    private DbsGroup pnd_Ctrs;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gra_Tiaa;

    private DbsGroup pnd_Ctrs__R_Field_4;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gra_Tiaa_A;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gra_Cref;

    private DbsGroup pnd_Ctrs__R_Field_5;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gra_Cref_A;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gsra_Tiaa;

    private DbsGroup pnd_Ctrs__R_Field_6;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gsra_Tiaa_A;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gsra_Cref;

    private DbsGroup pnd_Ctrs__R_Field_7;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gsra_Cref_A;
    private DbsField pnd_Pst_Tbl_Data_Field1;

    private DbsGroup pnd_Pst_Tbl_Data_Field1__R_Field_8;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Cutoff;

    private DbsGroup pnd_Output;
    private DbsField pnd_Output_Pnd_Pin_A12;
    private DbsField pnd_Output_Pnd_Letter_Type;
    private DbsField pnd_Output_Pnd_Ctr_Gra_Tiaa;
    private DbsField pnd_Output_Pnd_Ctr_Gra_Cref;
    private DbsField pnd_Output_Pnd_Ctr_Gsra_Tiaa;
    private DbsField pnd_Output_Pnd_Ctr_Gsra_Cref;
    private DbsField pnd_Output_Pnd_Package_Code;
    private DbsField pnd_Output_Pnd_Tiaa_Contracts;
    private DbsField pnd_Output_Pnd_Cref_Contracts;
    private DbsField pnd_Output_Pnd_Institution_Name;
    private DbsField pnd_Ndx;
    private DbsField pnd_Inst_Pln;
    private DbsField pnd_Ctr_File1;
    private DbsField pnd_Ctr_File1a;
    private DbsField pnd_Ctr_File1b;
    private DbsField pnd_Ctr_File2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);
        pdaPsta9017 = new PdaPsta9017(localVariables);

        // Local Variables
        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 175);

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Pin_A12 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N7 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);
        pnd_Input_Record_Pnd_Pin_A5 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A5", "#PIN-A5", FieldType.STRING, 5);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N12 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N12", "#PIN-N12", FieldType.NUMERIC, 12);
        pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr", "#TIAA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cref_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cref_Cntrct_Nbr", "#CREF-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cntrct_Issue_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Date", "#CNTRCT-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Issue_State_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Issue_State_Cde", "#ISSUE-STATE-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Plan_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Plan_Cde", "#PLAN-CDE", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cover_Letter_Ind = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cover_Letter_Ind", "#COVER-LETTER-IND", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Institution_Name = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Institution_Name", "#INSTITUTION-NAME", 
            FieldType.STRING, 120);
        pnd_Prev_Inst_Name = localVariables.newFieldInRecord("pnd_Prev_Inst_Name", "#PREV-INST-NAME", FieldType.STRING, 120);
        pnd_Prev_Pin_A12 = localVariables.newFieldInRecord("pnd_Prev_Pin_A12", "#PREV-PIN-A12", FieldType.STRING, 12);
        pnd_Prev_Contract = localVariables.newFieldInRecord("pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Cover = localVariables.newFieldInRecord("pnd_Prev_Cover", "#PREV-COVER", FieldType.STRING, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_Valid_Record = localVariables.newFieldInRecord("pnd_Valid_Record", "#VALID-RECORD", FieldType.BOOLEAN, 1);
        pnd_Accepted = localVariables.newFieldInRecord("pnd_Accepted", "#ACCEPTED", FieldType.BOOLEAN, 1);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 40);
        pnd_Lob = localVariables.newFieldInRecord("pnd_Lob", "#LOB", FieldType.STRING, 4);
        pnd_Participant_Name = localVariables.newFieldInRecord("pnd_Participant_Name", "#PARTICIPANT-NAME", FieldType.STRING, 35);
        pnd_Ctr_Valid_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Valid_Contract_Read", "#CTR-VALID-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Da_Contract_Not_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Contract_Not_In_Cor", "#CTR-DA-CONTRACT-NOT-IN-COR", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Da_Inactive_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Inactive_In_Cor", "#CTR-DA-INACTIVE-IN-COR", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_Deceased = localVariables.newFieldInRecord("pnd_Ctr_Ph_Deceased", "#CTR-PH-DECEASED", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_With_Mail_Suppression = localVariables.newFieldInRecord("pnd_Ctr_Ph_With_Mail_Suppression", "#CTR-PH-WITH-MAIL-SUPPRESSION", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Contract_Accepted = localVariables.newFieldInRecord("pnd_Ctr_Contract_Accepted", "#CTR-CONTRACT-ACCEPTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Rejected = localVariables.newFieldInRecord("pnd_Ctr_Contract_Rejected", "#CTR-CONTRACT-REJECTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Contract_Read", "#CTR-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Duplicate_Contract = localVariables.newFieldInRecord("pnd_Ctr_Duplicate_Contract", "#CTR-DUPLICATE-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Terminated_Contract = localVariables.newFieldInRecord("pnd_Ctr_Terminated_Contract", "#CTR-TERMINATED-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Inactive = localVariables.newFieldInRecord("pnd_Ctr_Contract_Inactive", "#CTR-CONTRACT-INACTIVE", FieldType.NUMERIC, 7);
        pnd_Ctr_Out_Of_State = localVariables.newFieldInRecord("pnd_Ctr_Out_Of_State", "#CTR-OUT-OF-STATE", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Outof_Range = localVariables.newFieldInRecord("pnd_Ctr_Contract_Outof_Range", "#CTR-CONTRACT-OUTOF-RANGE", FieldType.NUMERIC, 
            7);
        pnd_Ctr_After_Cutoff = localVariables.newFieldInRecord("pnd_Ctr_After_Cutoff", "#CTR-AFTER-CUTOFF", FieldType.NUMERIC, 7);
        pnd_Ctr_Plan_Not_For_Proc = localVariables.newFieldInRecord("pnd_Ctr_Plan_Not_For_Proc", "#CTR-PLAN-NOT-FOR-PROC", FieldType.NUMERIC, 7);
        pnd_Ctr_Institution_Owned = localVariables.newFieldInRecord("pnd_Ctr_Institution_Owned", "#CTR-INSTITUTION-OWNED", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gra = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra", "#CTR-TOTAL-GRA", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gsra = localVariables.newFieldInRecord("pnd_Ctr_Total_Gsra", "#CTR-TOTAL-GSRA", FieldType.NUMERIC, 7);
        pnd_Break = localVariables.newFieldInRecord("pnd_Break", "#BREAK", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);

        pnd_Arrays = localVariables.newGroupArrayInRecord("pnd_Arrays", "#ARRAYS", new DbsArrayController(1, 240));
        pnd_Arrays_Pnd_Combo_Prod = pnd_Arrays.newFieldInGroup("pnd_Arrays_Pnd_Combo_Prod", "#COMBO-PROD", FieldType.STRING, 2);
        pnd_Arrays_Pnd_Combo_Ctr = pnd_Arrays.newFieldInGroup("pnd_Arrays_Pnd_Combo_Ctr", "#COMBO-CTR", FieldType.NUMERIC, 7);
        pnd_Combo = localVariables.newFieldInRecord("pnd_Combo", "#COMBO", FieldType.STRING, 4);
        pnd_Sum_Tiaa_Cref = localVariables.newFieldInRecord("pnd_Sum_Tiaa_Cref", "#SUM-TIAA-CREF", FieldType.NUMERIC, 2);

        pnd_Ctrs = localVariables.newGroupInRecord("pnd_Ctrs", "#CTRS");
        pnd_Ctrs_Pnd_Ctr_Gra_Tiaa = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gra_Tiaa", "#CTR-GRA-TIAA", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_4 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_4", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Gra_Tiaa);
        pnd_Ctrs_Pnd_Ctr_Gra_Tiaa_A = pnd_Ctrs__R_Field_4.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gra_Tiaa_A", "#CTR-GRA-TIAA-A", FieldType.STRING, 1);
        pnd_Ctrs_Pnd_Ctr_Gra_Cref = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gra_Cref", "#CTR-GRA-CREF", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_5 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_5", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Gra_Cref);
        pnd_Ctrs_Pnd_Ctr_Gra_Cref_A = pnd_Ctrs__R_Field_5.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gra_Cref_A", "#CTR-GRA-CREF-A", FieldType.STRING, 1);
        pnd_Ctrs_Pnd_Ctr_Gsra_Tiaa = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gsra_Tiaa", "#CTR-GSRA-TIAA", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_6 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_6", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Gsra_Tiaa);
        pnd_Ctrs_Pnd_Ctr_Gsra_Tiaa_A = pnd_Ctrs__R_Field_6.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gsra_Tiaa_A", "#CTR-GSRA-TIAA-A", FieldType.STRING, 1);
        pnd_Ctrs_Pnd_Ctr_Gsra_Cref = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gsra_Cref", "#CTR-GSRA-CREF", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_7 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_7", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Gsra_Cref);
        pnd_Ctrs_Pnd_Ctr_Gsra_Cref_A = pnd_Ctrs__R_Field_7.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gsra_Cref_A", "#CTR-GSRA-CREF-A", FieldType.STRING, 1);
        pnd_Pst_Tbl_Data_Field1 = localVariables.newFieldInRecord("pnd_Pst_Tbl_Data_Field1", "#PST-TBL-DATA-FIELD1", FieldType.STRING, 253);

        pnd_Pst_Tbl_Data_Field1__R_Field_8 = localVariables.newGroupInRecord("pnd_Pst_Tbl_Data_Field1__R_Field_8", "REDEFINE", pnd_Pst_Tbl_Data_Field1);
        pnd_Pst_Tbl_Data_Field1_Pnd_Cutoff = pnd_Pst_Tbl_Data_Field1__R_Field_8.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Cutoff", "#CUTOFF", FieldType.NUMERIC, 
            8);

        pnd_Output = localVariables.newGroupInRecord("pnd_Output", "#OUTPUT");
        pnd_Output_Pnd_Pin_A12 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);
        pnd_Output_Pnd_Letter_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Gra_Tiaa = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gra_Tiaa", "#CTR-GRA-TIAA", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Gra_Cref = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gra_Cref", "#CTR-GRA-CREF", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Gsra_Tiaa = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gsra_Tiaa", "#CTR-GSRA-TIAA", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Gsra_Cref = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gsra_Cref", "#CTR-GSRA-CREF", FieldType.STRING, 1);
        pnd_Output_Pnd_Package_Code = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Output_Pnd_Tiaa_Contracts = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Tiaa_Contracts", "#TIAA-CONTRACTS", FieldType.STRING, 8, new DbsArrayController(1, 
            20));
        pnd_Output_Pnd_Cref_Contracts = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Cref_Contracts", "#CREF-CONTRACTS", FieldType.STRING, 8, new DbsArrayController(1, 
            20));
        pnd_Output_Pnd_Institution_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 120);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 2);
        pnd_Inst_Pln = localVariables.newFieldArrayInRecord("pnd_Inst_Pln", "#INST-PLN", FieldType.STRING, 6, new DbsArrayController(1, 84));
        pnd_Ctr_File1 = localVariables.newFieldInRecord("pnd_Ctr_File1", "#CTR-FILE1", FieldType.NUMERIC, 5);
        pnd_Ctr_File1a = localVariables.newFieldInRecord("pnd_Ctr_File1a", "#CTR-FILE1A", FieldType.NUMERIC, 5);
        pnd_Ctr_File1b = localVariables.newFieldInRecord("pnd_Ctr_File1b", "#CTR-FILE1B", FieldType.NUMERIC, 5);
        pnd_Ctr_File2 = localVariables.newFieldInRecord("pnd_Ctr_File2", "#CTR-FILE2", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Valid_Record.setInitialValue(false);
        pnd_Accepted.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridp480() throws Exception
    {
        super("Ridp480");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  STATISTICS REPORT
        if (condition(getReports().getAstLinesLeft(1).less(5)))                                                                                                           //Natural: FORMAT ( 1 ) LS = 120 PS = 60;//Natural: FORMAT LS = 80 PS = 60;//Natural: NEWPAGE ( 1 ) IF LESS THAN 5 LINES
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        //*  OPEN MQ QUEUE                                       /* JCA20150324
        //* * WRITE ' FETCHING MDMP011'
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*  PERFORM GET-CUTOFF-DATE                             /* JCA20160715
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 35T '2008 FORCED CASH RIDER MAILING' 84T 'DATE:' *DATU / 33T 'LIST OF INSTITUTION-OWNED CONTRACTS' 84T 'TIME:' *TIME ( EM = XXXXXXXX ) / 84T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) // 2T 'PIN    ' 11T 'PARTICIPANT NAME   ' 48T 'TIAA #  ' 58T 'LOB  ' 65T 'CUTOFF' 73T 'PPG/PLAN'/ 2T '-------' 11T '-----------------------------------' 48T '--------' 58T '-----' 65T '------' 73T '--------'//
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #INPUT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  PINEXP
            pnd_Ctr_Contract_Read.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CTR-CONTRACT-READ
            //*                                                                                                                                                           //Natural: AT BREAK OF #INPUT-RECORD.#PIN-A12
            //*                                                                                                                                                           //Natural: AT END OF DATA
            if (condition(pnd_Break.getBoolean()))                                                                                                                        //Natural: IF #BREAK
            {
                pnd_Break.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #BREAK
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP
            pnd_Prev_Pin_A12.setValue(pnd_Input_Record_Pnd_Pin_A12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-A12 TO #PREV-PIN-A12
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Institution_Name);                                                                                           //Natural: MOVE #INPUT-RECORD.#INSTITUTION-NAME TO #PREV-INST-NAME
            pnd_Prev_Cover.setValue(pnd_Input_Record_Pnd_Cover_Letter_Ind);                                                                                               //Natural: MOVE #INPUT-RECORD.#COVER-LETTER-IND TO #PREV-COVER
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
        }                                                                                                                                                                 //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-STATISTICS-REPORT
        sub_Write_Statistics_Report();
        if (condition(Global.isEscape())) {return;}
        //*  CLOSE MQ QUEUE
        //* * WRITE 'FETCHING MDMP0012'
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* **********************************************************************
        //*                 S T A R T    O F    S U B R O U T I N E S
        //* **********************************************************************
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CUTOFF-DATE
        //*  ---------------------------------
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-RECORD
        //*  GRA CONTRACTS
        //*  GSRA CONTRACTS
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RECORD-IN-COR
        //*  --------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PIN-STATUS
        //*  ---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-ARRAY
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA-TO-FILE
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-STATISTICS-REPORT
    }
    private void sub_Get_Cutoff_Date() throws Exception                                                                                                                   //Natural: GET-CUTOFF-DATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaPsta9017.getPsta9017_Tbl_Table_Nme().setValue("RIDER-MISCELLANEOUS");                                                                                          //Natural: ASSIGN PSTA9017.TBL-TABLE-NME := 'RIDER-MISCELLANEOUS'
        pdaPsta9017.getPsta9017_Tbl_Key_Field().setValue("2008CASH");                                                                                                     //Natural: ASSIGN PSTA9017.TBL-KEY-FIELD := '2008CASH'
        pdaPsta9017.getPsta9017_Tbl_Scrty_Level_Ind().setValue("A");                                                                                                      //Natural: ASSIGN PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        DbsUtil.callnat(Pstn9017.class , getCurrentProcessState(), pdaPsta9017.getPsta9017());                                                                            //Natural: CALLNAT 'PSTN9017' PSTA9017
        if (condition(Global.isEscape())) return;
        if (condition(pdaPsta9017.getPsta9017_Valid_Entry().getBoolean()))                                                                                                //Natural: IF PSTA9017.VALID-ENTRY
        {
            pnd_Pst_Tbl_Data_Field1.setValue(pdaPsta9017.getPsta9017_Tbl_Data_Field());                                                                                   //Natural: ASSIGN #PST-TBL-DATA-FIELD1 := PSTA9017.TBL-DATA-FIELD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "TABLE ENTRY NOT FOUND");                                                                                                               //Natural: WRITE 'TABLE ENTRY NOT FOUND'
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-CUTOFF-DATE
    }
    private void sub_Validate_Record() throws Exception                                                                                                                   //Natural: VALIDATE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        //*    IF #CNTRCT-ISSUE-DATE GT #CUTOFF
        //*      ADD 1 TO #CTR-AFTER-CUTOFF
        //*      ESCAPE ROUTINE
        //*    END-IF
        if (condition(pnd_Prev_Contract.equals(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr)))                                                                                    //Natural: IF #PREV-CONTRACT EQ #TIAA-CNTRCT-NBR
        {
            pnd_Ctr_Duplicate_Contract.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DUPLICATE-CONTRACT
            pnd_Remarks.setValue("DUPLICATE CONTRACT");                                                                                                                   //Natural: ASSIGN #REMARKS := 'DUPLICATE CONTRACT'
            //*  PINEXP
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prev_Contract.setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #PREV-CONTRACT
                                                                                                                                                                          //Natural: PERFORM CHECK-RECORD-IN-COR
        sub_Check_Record_In_Cor();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Valid_Record.getBoolean()))                                                                                                                     //Natural: IF #VALID-RECORD
        {
            //*    MOVE TRUE TO #VALID-RECORD
            short decideConditionsMet619 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TIAA-CNTRCT-NBR EQ '20000010' THRU '20099999' OR #TIAA-CNTRCT-NBR EQ '20250000' THRU '28999999' OR #TIAA-CNTRCT-NBR EQ '30000100' THRU '39999999'
            if (condition((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("20099999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20250000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("28999999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("30000100") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("39999999")))))
            {
                decideConditionsMet619++;
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                pnd_Ctr_Total_Gra.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CTR-TOTAL-GRA
                if (condition(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.notEquals(" ")))                                                                                       //Natural: IF #TIAA-CNTRCT-NBR NE ' '
                {
                    pnd_Ctrs_Pnd_Ctr_Gra_Tiaa.setValue(1);                                                                                                                //Natural: MOVE 1 TO #CTRS.#CTR-GRA-TIAA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                       //Natural: IF #CREF-CNTRCT-NBR NE ' '
                {
                    pnd_Ctrs_Pnd_Ctr_Gra_Cref.setValue(1);                                                                                                                //Natural: MOVE 1 TO #CTRS.#CTR-GRA-CREF
                }                                                                                                                                                         //Natural: END-IF
                //*  062707 J.AVE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #TIAA-CNTRCT-NBR EQ 'K5000000' THRU 'K7999999' OR #TIAA-CNTRCT-NBR EQ 'L0000100' THRU 'L8499999'
            else if (condition(((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K5000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K7999999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("L0000100") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("L8499999")))))
            {
                decideConditionsMet619++;
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                pnd_Ctr_Total_Gsra.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CTR-TOTAL-GSRA
                if (condition(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.notEquals(" ")))                                                                                       //Natural: IF #TIAA-CNTRCT-NBR NE ' '
                {
                    pnd_Ctrs_Pnd_Ctr_Gsra_Tiaa.setValue(1);                                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GSRA-TIAA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                       //Natural: IF #CREF-CNTRCT-NBR NE ' '
                {
                    pnd_Ctrs_Pnd_Ctr_Gsra_Cref.setValue(1);                                                                                                               //Natural: MOVE 1 TO #CTRS.#CTR-GSRA-CREF
                }                                                                                                                                                         //Natural: END-IF
                //*  062707 J.AVE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Ctr_Contract_Outof_Range.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CTR-CONTRACT-OUTOF-RANGE
                pnd_Remarks.setValue("CONTRACT OUT OF RANGE");                                                                                                            //Natural: ASSIGN #REMARKS := 'CONTRACT OUT OF RANGE'
                //*  PINEXP
                getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE-RECORD
    }
    private void sub_Check_Record_In_Cor() throws Exception                                                                                                               //Natural: CHECK-RECORD-IN-COR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------
        //*  MDM*210 CHANGE TO MDM*211 FOR PIN EXPANSION            /* PINEXP
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #MDMA211.#I-CONTRACT-NUMBER
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue("01");                                                                                                   //Natural: MOVE '01' TO #MDMA211.#I-PAYEE-CODE-A2
        //*    READ (1) COR BY COR-SUPER-CNTRCT-PIN-PAYEE STARTING FROM
        //*      #TIAA-CNTRCT-NBR
        getReports().write(0, "CALLING MDMN211A TO GET CONTRACT INFORMATION");                                                                                            //Natural: WRITE 'CALLING MDMN211A TO GET CONTRACT INFORMATION'
        if (Global.isEscape()) return;
        //*  JCA20150324
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //*      IF COR.CNTRCT-NBR GT #TIAA-CNTRCT-NBR
        getReports().write(0, "=",pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code());                                                                                         //Natural: WRITE '=' #MDMA211.#O-RETURN-CODE
        if (Global.isEscape()) return;
        //*  JCA20150324
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA211.#O-RETURN-CODE NE '0000'
        {
            pnd_Remarks.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Text());                                                                                          //Natural: ASSIGN #REMARKS := #MDMA211.#O-RETURN-TEXT
            //*  PINEXP
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Contract_Not_In_Cor.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-DA-CONTRACT-NOT-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*      IF NOT(CNTRCT-STATUS-CODE = ' ' OR = 'H' OR = 'Y')  /* JCA20150324
        if (condition(! (pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals(" ") || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("H")         //Natural: IF NOT ( #MDMA211.#O-CONTRACT-STATUS-CODE = ' ' OR = 'H' OR = 'Y' )
            || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("Y"))))
        {
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("CONTRACT INACTIVE IN COR - STATUS CODE:", pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code()));                    //Natural: COMPRESS 'CONTRACT INACTIVE IN COR - STATUS CODE:' #MDMA211.#O-CONTRACT-STATUS-CODE TO #REMARKS
            //*                 CNTRCT-STATUS-CDE TO #REMARKS
            //*  PINEXP
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Inactive_In_Cor.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DA-INACTIVE-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*        MOVE PH-UNIQUE-ID-NBR  TO #PH-UNIQUE-ID-NBR
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PIN-STATUS
            sub_Determine_Pin_Status();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Accepted.getBoolean()))                                                                                                                     //Natural: IF #ACCEPTED
            {
                pnd_Valid_Record.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*    END-READ
        //*  CHECK-RECORD-IN-COR
    }
    private void sub_Determine_Pin_Status() throws Exception                                                                                                              //Natural: DETERMINE-PIN-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------
        //*  MODIFY MDM*100 TO MDM*101                             /* PINEXP
        //*    #PH-RCD-TYPE-CDE := '01'                            /* JCA20150324
        //*    READ (1) COR BY COR-SUPER-PIN-RCDTYPE STARTING FROM /* JCA20150324
        //*                   #COR-SUPER-PIN-RCDTYPE               /* JCA20150324
        //*  JCA20150324
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        //*    MOVE #INPUT-RECORD.#PIN TO #MDMA101.#I-PIN /* PINEXP/* JCA20150324
        getReports().write(0, "CALLING MDMN101A TO GET ADDRESS/NAME");                                                                                                    //Natural: WRITE 'CALLING MDMN101A TO GET ADDRESS/NAME'
        if (Global.isEscape()) return;
        //*  PINEXP >>>
        if (condition(pnd_Input_Record_Pnd_Pin_A12.notEquals(" ")))                                                                                                       //Natural: IF #INPUT-RECORD.#PIN-A12 NE ' '
        {
            if (condition(pnd_Input_Record_Pnd_Pin_A12.getSubstring(8,1).equals(" ")))                                                                                    //Natural: IF SUBSTRING ( #INPUT-RECORD.#PIN-A12,8,1 ) = ' '
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N7);                                                                          //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #PIN-N7
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                         //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #PIN-N12
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP <<<
        }                                                                                                                                                                 //Natural: END-IF
        //*  JCA20150324
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  REJECT DECEASED PARTICIPANTS
        //*      IF PH-DOD-DTE GT 0                                /* JCA20150324
        //*  JCA20150324
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero())))                                                                                //Natural: IF #MDMA101.#O-DATE-OF-DEATH GT 0
        {
            pnd_Ctr_Ph_Deceased.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CTR-PH-DECEASED
            //*        COMPRESS 'P/H DECEASED - DOD:' PH-DOD-DTE TO #REMARKS
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("P/H DECEASED - DOD:", pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death()));                                               //Natural: COMPRESS 'P/H DECEASED - DOD:' #MDMA101.#O-DATE-OF-DEATH TO #REMARKS
            //*  PINEXP
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Accepted.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #ACCEPTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  REJECT PARTICIPANTS W/ STOP ALL MAILING SUPPRESSION CODE
            //*        EXAMINE PH-MAIL-CDE(*) FOR '01' GIVING POSITION #I
            //*  JCA20150324
            DbsUtil.examine(new ExamineSource(pdaMdma101.getPnd_Mdma101_Pnd_O_Mail_Code().getValue("*")), new ExamineSearch("01"), new ExamineGivingPosition(pnd_I));     //Natural: EXAMINE #MDMA101.#O-MAIL-CODE ( * ) FOR '01' GIVING POSITION #I
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I GT 0
            {
                pnd_Ctr_Ph_With_Mail_Suppression.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTR-PH-WITH-MAIL-SUPPRESSION
                pnd_Remarks.setValue(DbsUtil.compress("P/H MAIL SUPPRESSED - MAIL CODE: 01"));                                                                            //Natural: COMPRESS 'P/H MAIL SUPPRESSED - MAIL CODE: 01' TO #REMARKS
                //*  PINEXP
                getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Accepted.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #ACCEPTED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Accepted.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #ACCEPTED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*    END-READ                                           /* JCA20150324
        //*  DETERMINE-PIN-STATUS
    }
    //*  062707 J.AVE
    private void sub_Add_Contract_To_Array() throws Exception                                                                                                             //Natural: ADD-CONTRACT-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------
        pnd_K.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #K
        if (condition(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.notEquals(" ")))                                                                                               //Natural: IF #TIAA-CNTRCT-NBR NE ' '
        {
            pnd_Output_Pnd_Tiaa_Contracts.getValue(pnd_K).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #TIAA-CONTRACTS ( #K )
            //*      ADD 1 TO #CTRS.#CTR-GRA-TIAA
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                               //Natural: IF #CREF-CNTRCT-NBR NE ' '
        {
            pnd_Output_Pnd_Cref_Contracts.getValue(pnd_K).setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                 //Natural: MOVE #CREF-CNTRCT-NBR TO #CREF-CONTRACTS ( #K )
            //*      ADD 1 TO #CTRS.#CTR-GRA-CREF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-CONTRACT-TO-ARRAY
    }
    private void sub_Write_Data_To_File() throws Exception                                                                                                                //Natural: WRITE-DATA-TO-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        //*    #SUM-TIAA-CREF := #CTRS.#CTR-GRA-TIAA  + #CTRS.#CTR-GRA-CREF  +
        //*                      #CTRS.#CTR-GSRA-TIAA + #CTRS.#CTR-GSRA-CREF
        //*    DECIDE FOR FIRST CONDITION
        //*      WHEN #SUM-TIAA-CREF LE 4
        pnd_Output_Pnd_Package_Code.setValue("RDR2008U");                                                                                                                 //Natural: MOVE 'RDR2008U' TO #PACKAGE-CODE
        pnd_Ctr_File1.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #CTR-FILE1
        getWorkFiles().write(2, false, pnd_Output);                                                                                                                       //Natural: WRITE WORK FILE 2 #OUTPUT
        //*      WHEN NONE
        //*        MOVE 'RDR2008V' TO #PACKAGE-CODE
        //*        WRITE WORK FILE 3 #OUTPUT
        //*        ADD 1 TO #CTR-FILE2
        //*    END-DECIDE
        //*  WRITE-DATA-TO-FILE
    }
    private void sub_Write_Statistics_Report() throws Exception                                                                                                           //Natural: WRITE-STATISTICS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(1, NEWLINE,NEWLINE,"TOTAL INSTITUTION-OWNED CONTRACTS :",pnd_Ctr_Institution_Owned, new ReportEditMask ("Z,ZZZ,ZZ9"));                         //Natural: WRITE ( 1 ) // 'TOTAL INSTITUTION-OWNED CONTRACTS :' #CTR-INSTITUTION-OWNED ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        pnd_Ctr_Contract_Rejected.compute(new ComputeParameters(false, pnd_Ctr_Contract_Rejected), pnd_Ctr_Da_Contract_Not_In_Cor.add(pnd_Ctr_Da_Inactive_In_Cor).add(pnd_Ctr_Ph_Deceased).add(pnd_Ctr_Ph_With_Mail_Suppression).add(pnd_Ctr_Out_Of_State).add(pnd_Ctr_Contract_Outof_Range).add(pnd_Ctr_Plan_Not_For_Proc).add(pnd_Ctr_Institution_Owned).add(pnd_Ctr_After_Cutoff).add(pnd_Ctr_Duplicate_Contract)); //Natural: ASSIGN #CTR-CONTRACT-REJECTED := #CTR-DA-CONTRACT-NOT-IN-COR + #CTR-DA-INACTIVE-IN-COR + #CTR-PH-DECEASED + #CTR-PH-WITH-MAIL-SUPPRESSION + #CTR-OUT-OF-STATE + #CTR-CONTRACT-OUTOF-RANGE + #CTR-PLAN-NOT-FOR-PROC + #CTR-INSTITUTION-OWNED + #CTR-AFTER-CUTOFF + #CTR-DUPLICATE-CONTRACT
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ         :",pnd_Ctr_Contract_Read);                                                                                  //Natural: WRITE 'NUMBER OF RECORDS READ         :' #CTR-CONTRACT-READ
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS ACCEPTED     :",pnd_Ctr_Contract_Accepted);                                                                              //Natural: WRITE 'NUMBER OF RECORDS ACCEPTED     :' #CTR-CONTRACT-ACCEPTED
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS REJECTED     :",pnd_Ctr_Contract_Rejected);                                                                              //Natural: WRITE 'NUMBER OF RECORDS REJECTED     :' #CTR-CONTRACT-REJECTED
        if (Global.isEscape()) return;
        getReports().write(0, "BREAKDOWN OF RECORDS REJECTED  :");                                                                                                        //Natural: WRITE 'BREAKDOWN OF RECORDS REJECTED  :'
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT NOT IN COR     :",pnd_Ctr_Da_Contract_Not_In_Cor);                                                                              //Natural: WRITE ' -CONTRACT NOT IN COR     :' #CTR-DA-CONTRACT-NOT-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT INACTIVE IN COR:",pnd_Ctr_Da_Inactive_In_Cor);                                                                                  //Natural: WRITE ' -CONTRACT INACTIVE IN COR:' #CTR-DA-INACTIVE-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H DECEASED            :",pnd_Ctr_Ph_Deceased);                                                                                         //Natural: WRITE ' -P/H DECEASED            :' #CTR-PH-DECEASED
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H W/ MAIL SUPPRESSION :",pnd_Ctr_Ph_With_Mail_Suppression);                                                                            //Natural: WRITE ' -P/H W/ MAIL SUPPRESSION :' #CTR-PH-WITH-MAIL-SUPPRESSION
        if (Global.isEscape()) return;
        //*    WRITE ' -ISSUE STATE OUTSIDE CA  :' #CTR-OUT-OF-STATE
        getReports().write(0, " -CONTRACT OUT OF RANGE   :",pnd_Ctr_Contract_Outof_Range);                                                                                //Natural: WRITE ' -CONTRACT OUT OF RANGE   :' #CTR-CONTRACT-OUTOF-RANGE
        if (Global.isEscape()) return;
        getReports().write(0, " -PLAN NOT FOR PROCESSING :",pnd_Ctr_Plan_Not_For_Proc);                                                                                   //Natural: WRITE ' -PLAN NOT FOR PROCESSING :' #CTR-PLAN-NOT-FOR-PROC
        if (Global.isEscape()) return;
        getReports().write(0, " -INSTITUTION OWNED       :",pnd_Ctr_Institution_Owned);                                                                                   //Natural: WRITE ' -INSTITUTION OWNED       :' #CTR-INSTITUTION-OWNED
        if (Global.isEscape()) return;
        getReports().write(0, " -DUPLICATE CONTRACT      :",pnd_Ctr_Duplicate_Contract);                                                                                  //Natural: WRITE ' -DUPLICATE CONTRACT      :' #CTR-DUPLICATE-CONTRACT
        if (Global.isEscape()) return;
        getReports().write(0, " -ISSUED AFTER CUTOFF DATE:",pnd_Ctr_After_Cutoff);                                                                                        //Natural: WRITE ' -ISSUED AFTER CUTOFF DATE:' #CTR-AFTER-CUTOFF
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY PRODUCT:");                                                                                                                       //Natural: WRITE 'TOTAL BY PRODUCT:'
        if (Global.isEscape()) return;
        getReports().write(0, " - GRA  :",pnd_Ctr_Total_Gra);                                                                                                             //Natural: WRITE ' - GRA  :' #CTR-TOTAL-GRA
        if (Global.isEscape()) return;
        getReports().write(0, " - GSRA :",pnd_Ctr_Total_Gsra);                                                                                                            //Natural: WRITE ' - GSRA :' #CTR-TOTAL-GSRA
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Ctr_File1);                                                                                                                         //Natural: WRITE '=' #CTR-FILE1
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Ctr_File2);                                                                                                                         //Natural: WRITE '=' #CTR-FILE2
        if (Global.isEscape()) return;
        //*  WRITE-STATISTICS-REPORT
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Input_Record_Pnd_Pin_A12IsBreak = pnd_Input_Record_Pnd_Pin_A12.isBreak(endOfData);
        if (condition(pnd_Input_Record_Pnd_Pin_A12IsBreak))
        {
            //*  PINEXP
            if (condition(pnd_Input_Record_Pnd_Pin_A12.equals("999999999999")))                                                                                           //Natural: IF #INPUT-RECORD.#PIN-A12 EQ '999999999999'
            {
                Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                          //Natural: ESCAPE BOTTOM
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ctrs_Pnd_Ctr_Gra_Tiaa.greater(getZero()) || pnd_Ctrs_Pnd_Ctr_Gra_Cref.greater(getZero()) || pnd_Ctrs_Pnd_Ctr_Gsra_Tiaa.greater(getZero())   //Natural: IF #CTRS.#CTR-GRA-TIAA GT 0 OR #CTRS.#CTR-GRA-CREF GT 0 OR #CTRS.#CTR-GSRA-TIAA GT 0 OR #CTRS.#CTR-GSRA-CREF GT 0
                || pnd_Ctrs_Pnd_Ctr_Gsra_Cref.greater(getZero())))
            {
                //*  PINEXP
                pnd_Output_Pnd_Pin_A12.setValue(pnd_Prev_Pin_A12);                                                                                                        //Natural: MOVE #PREV-PIN-A12 TO #OUTPUT.#PIN-A12
                pnd_Output_Pnd_Ctr_Gra_Tiaa.setValue(pnd_Ctrs_Pnd_Ctr_Gra_Tiaa_A);                                                                                        //Natural: MOVE #CTRS.#CTR-GRA-TIAA-A TO #OUTPUT.#CTR-GRA-TIAA
                pnd_Output_Pnd_Ctr_Gra_Cref.setValue(pnd_Ctrs_Pnd_Ctr_Gra_Cref_A);                                                                                        //Natural: MOVE #CTRS.#CTR-GRA-CREF-A TO #OUTPUT.#CTR-GRA-CREF
                pnd_Output_Pnd_Ctr_Gsra_Tiaa.setValue(pnd_Ctrs_Pnd_Ctr_Gsra_Tiaa_A);                                                                                      //Natural: MOVE #CTRS.#CTR-GSRA-TIAA-A TO #OUTPUT.#CTR-GSRA-TIAA
                pnd_Output_Pnd_Ctr_Gsra_Cref.setValue(pnd_Ctrs_Pnd_Ctr_Gsra_Cref_A);                                                                                      //Natural: MOVE #CTRS.#CTR-GSRA-CREF-A TO #OUTPUT.#CTR-GSRA-CREF
                pnd_Output_Pnd_Letter_Type.setValue(pnd_Prev_Cover);                                                                                                      //Natural: MOVE #PREV-COVER TO #OUTPUT.#LETTER-TYPE
                pnd_Output_Pnd_Institution_Name.setValue(pnd_Prev_Inst_Name);                                                                                             //Natural: MOVE #PREV-INST-NAME TO #OUTPUT.#INSTITUTION-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-DATA-TO-FILE
                sub_Write_Data_To_File();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ctrs.reset();                                                                                                                                             //Natural: RESET #CTRS
            pnd_Output.reset();                                                                                                                                           //Natural: RESET #OUTPUT #K
            pnd_K.reset();
            pnd_Break.setValue(true);                                                                                                                                     //Natural: MOVE TRUE TO #BREAK
            //*  PINEXP
            pnd_Prev_Pin_A12.setValue(pnd_Input_Record_Pnd_Pin_A12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-A12 TO #PREV-PIN-A12
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Institution_Name);                                                                                           //Natural: MOVE #INPUT-RECORD.#INSTITUTION-NAME TO #PREV-INST-NAME
            pnd_Prev_Cover.setValue(pnd_Input_Record_Pnd_Cover_Letter_Ind);                                                                                               //Natural: MOVE #INPUT-RECORD.#COVER-LETTER-IND TO #PREV-COVER
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=120 PS=60");
        Global.format(0, "LS=80 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(35),"2008 FORCED CASH RIDER MAILING",new 
            TabSetting(84),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(33),"LIST OF INSTITUTION-OWNED CONTRACTS",new TabSetting(84),"TIME:",Global.getTIME(), 
            new ReportEditMask ("XXXXXXXX"),NEWLINE,new TabSetting(84),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new 
            TabSetting(2),"PIN    ",new TabSetting(11),"PARTICIPANT NAME   ",new TabSetting(48),"TIAA #  ",new TabSetting(58),"LOB  ",new TabSetting(65),"CUTOFF",new 
            TabSetting(73),"PPG/PLAN",NEWLINE,new TabSetting(2),"-------",new TabSetting(11),"-----------------------------------",new TabSetting(48),"--------",new 
            TabSetting(58),"-----",new TabSetting(65),"------",new TabSetting(73),"--------",NEWLINE,NEWLINE);
    }
}
