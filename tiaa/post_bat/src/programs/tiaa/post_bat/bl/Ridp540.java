/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:04 PM
**        * FROM NATURAL PROGRAM : Ridp540
************************************************************
**        * FILE NAME            : Ridp540.java
**        * CLASS NAME           : Ridp540
**        * INSTANCE NAME        : Ridp540
************************************************************
************************************************************************
** PROGRAM     : RIDP540                                              **
** AUTHOR      : JOANNES C. AVE                                       **
** DATE        : 04/20/2010                                           **
** DESCRIPTION : READS DA REPORTING TIAA STABLE VALUE EXTRACT AND     **
**               REFORMATS DATA AS FEED TO RIDP542                    **
**               PACKAGE CODES: RDR2010C                              **
************************************************************************
** HISTORY:                                                           **
** 04/20/2010  : INITIAL CODING                                       **
** 01/03/2011  : ADD LA,MN,MT,NE,VA CERTIFICATES                      **
**               LOB COMBINATIONS TABLE                               **
** ------------------------------------------------------------------ **
**               RA     GRA     RC                                    **
**                              A                                     **
**               B                                                    **
**                      C                                             **
**               D              D                                     **
**                      E       E                                     **
**               F      F                                             **
**               G      G       G                                     **
** ------------------------------------------------------------------ **
** 03/09/2011  : ADD LETTER TYPE FIELD IN THE INPUT FILE LAYOUT       **
**             : 1 - GENERIC COVER LETTERS                            **
**             : 2 - BOSTON MEDICAL CENTER COVER LETTER               **
** 06/01/2011  : ADD THE FOLLOWING:                                   **
**             : FL CREF ENDORSEMENTS FOR RA AND GRA/RC               **
**             : NH TIAA ENDORSEMENTS FOR RA AND GRA/RC               **
**             : NH AND FL CERTIFICATES                               **
** 03/23/2015  : COR/NAAD SUNSET - REMOVE ALL REFERENCES TO COR/NAAD  **
**               AND CALL THE MDMN MODULES INSTEAD FOR PARTICIPANT    **
**               AND CONTRACT INFORMATION                             **
** 02/17/2017  : UPDATE NH CERTIFICATE TO GENERIC            (LS1)    **
**               A NEW VERSION OF 2.0 TSV CERTIFICATE SHOULD BE SENT  **
**               FOR RC CONTRACTS .                                   **
**               RA/GRA CONTINUE TO RECEIVE VERSION 1.0               **
** 05/30/2017  : PIN EXPANSION                               (PINEXP) **
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridp540 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;
    private PdaPsta9017 pdaPsta9017;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Pin_A12;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Pin_N7;
    private DbsField pnd_Input_Record_Pnd_Pin_A5;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Pin_N12;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cref_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Date;
    private DbsField pnd_Input_Record_Pnd_Issue_State_Cde;
    private DbsField pnd_Input_Record_Pnd_Plan_Cde;
    private DbsField pnd_Input_Record_Pnd_Inst_State_Cde;
    private DbsField pnd_Input_Record_Pnd_Inst_Sv_Lob;
    private DbsField pnd_Input_Record_Pnd_Letter_Type;
    private DbsField pnd_Input_Record_Pnd_Inst_Name;
    private DbsField pnd_Prev_Inst_Name;
    private DbsField pnd_Prev_Inst_Sv_Lob;
    private DbsField pnd_Prev_Plan_Cde;
    private DbsField pnd_Prev_Pin_A12;
    private DbsField pnd_Prev_Contract;
    private DbsField pnd_Prev_Cover;
    private DbsField pnd_Prev_Letter_Type;
    private DbsField pnd_X;
    private DbsField pnd_K;
    private DbsField pnd_Valid_Record;
    private DbsField pnd_Accepted;
    private DbsField pnd_Remarks;
    private DbsField pnd_Lob;
    private DbsField pnd_Cutoff;
    private DbsField pnd_Participant_Name;
    private DbsField pnd_Ctr_Svc_Contract_Read;
    private DbsField pnd_Ctr_Valid_Srv_Contract_Read;
    private DbsField pnd_Ctr_Da_Contract_Not_In_Cor;
    private DbsField pnd_Ctr_Da_Inactive_In_Cor;
    private DbsField pnd_Ctr_Ph_Deceased;
    private DbsField pnd_Ctr_Ph_With_Mail_Suppression;
    private DbsField pnd_Ctr_Contract_Accepted;
    private DbsField pnd_Ctr_Contract_Rejected;
    private DbsField pnd_Ctr_Contract_Read;
    private DbsField pnd_Ctr_Duplicate_Contract;
    private DbsField pnd_Ctr_Terminated_Contract;
    private DbsField pnd_Ctr_Contract_Inactive;
    private DbsField pnd_Ctr_Out_Of_State;
    private DbsField pnd_Ctr_Contract_Outof_Range;
    private DbsField pnd_Ctr_Institution_Owned;
    private DbsField pnd_Ctr_Total_Contracts;
    private DbsField pnd_Ctr_Total_Tiaa_Ra;
    private DbsField pnd_Ctr_Total_Tiaa_Gra_Rc;
    private DbsField pnd_Ctr_Total_Cref;
    private DbsField pnd_Break;
    private DbsField pnd_I;
    private DbsField pnd_A;

    private DbsGroup pnd_Arrays;
    private DbsField pnd_Arrays_Pnd_Combo_Prod;
    private DbsField pnd_Arrays_Pnd_Combo_Ctr;
    private DbsField pnd_Combo;

    private DbsGroup pnd_Ctrs;
    private DbsField pnd_Ctrs_Pnd_Ctr_Tiaa_Ra;

    private DbsGroup pnd_Ctrs__R_Field_4;
    private DbsField pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_A;
    private DbsField pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc;

    private DbsGroup pnd_Ctrs__R_Field_5;
    private DbsField pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_A;
    private DbsField pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_Nh;

    private DbsGroup pnd_Ctrs__R_Field_6;
    private DbsField pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_Nh_A;
    private DbsField pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_Nh;

    private DbsGroup pnd_Ctrs__R_Field_7;
    private DbsField pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref;

    private DbsGroup pnd_Ctrs__R_Field_8;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_A;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Il;

    private DbsGroup pnd_Ctrs__R_Field_9;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Il_A;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Ra_Fl;

    private DbsGroup pnd_Ctrs__R_Field_10;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Ra_Fl_A;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Gra_Rc_Fl;

    private DbsGroup pnd_Ctrs__R_Field_11;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cref_Gra_Rc_Fl_A;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Ca;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Ok;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Gn;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Nc;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_La;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Mn;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Mt;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Ne;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Va;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Fl;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Nh;
    private DbsField pnd_Ctrs_Pnd_Ctr_Oregon;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Or;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Nm;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Ny;
    private DbsField pnd_Ctrs_Pnd_Ctr_Cert_Pr;
    private DbsField pnd_Ctr_File1;

    private DbsGroup pnd_Output;
    private DbsField pnd_Output_Pnd_Pin_A12;
    private DbsField pnd_Output_Pnd_Letter_Type;
    private DbsField pnd_Output_Pnd_Institution_Name;
    private DbsField pnd_Output_Pnd_Ctr_Tiaa_Ra_A;
    private DbsField pnd_Output_Pnd_Ctr_Tiaa_Gra_Rc_A;
    private DbsField pnd_Output_Pnd_Ctr_Tiaa_Ra_Nh_A;
    private DbsField pnd_Output_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A;
    private DbsField pnd_Output_Pnd_Ctr_Cref_A;
    private DbsField pnd_Output_Pnd_Ctr_Cref_Il_A;
    private DbsField pnd_Output_Pnd_Ctr_Cref_Ra_Fl_A;
    private DbsField pnd_Output_Pnd_Ctr_Cref_Gra_Rc_Fl_A;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Gn;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Ca;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Ok;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Nc;
    private DbsField pnd_Output_Pnd_Ctr_Cert_La;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Mn;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Mt;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Ne;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Va;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Fl;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Nh;
    private DbsField pnd_Output_Pnd_Ctr_Oregon;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Or;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Nm;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Ny;
    private DbsField pnd_Output_Pnd_Ctr_Cert_Pr;
    private DbsField pnd_Output_Pnd_Inst_Sv_Lob;
    private DbsField pnd_Output_Pnd_Package_Code;
    private DbsField pnd_Output_Pnd_Contracts;
    private DbsField pnd_Ndx;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);
        pdaPsta9017 = new PdaPsta9017(localVariables);

        // Local Variables
        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 175);

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Pin_A12 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N7 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);
        pnd_Input_Record_Pnd_Pin_A5 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A5", "#PIN-A5", FieldType.STRING, 5);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N12 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N12", "#PIN-N12", FieldType.NUMERIC, 12);
        pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr", "#TIAA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cref_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cref_Cntrct_Nbr", "#CREF-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cntrct_Issue_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Date", "#CNTRCT-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Issue_State_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Issue_State_Cde", "#ISSUE-STATE-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Record_Pnd_Plan_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Plan_Cde", "#PLAN-CDE", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Inst_State_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Inst_State_Cde", "#INST-STATE-CDE", FieldType.STRING, 
            2);
        pnd_Input_Record_Pnd_Inst_Sv_Lob = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Inst_Sv_Lob", "#INST-SV-LOB", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Letter_Type = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Inst_Name = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Inst_Name", "#INST-NAME", FieldType.STRING, 
            120);
        pnd_Prev_Inst_Name = localVariables.newFieldInRecord("pnd_Prev_Inst_Name", "#PREV-INST-NAME", FieldType.STRING, 120);
        pnd_Prev_Inst_Sv_Lob = localVariables.newFieldInRecord("pnd_Prev_Inst_Sv_Lob", "#PREV-INST-SV-LOB", FieldType.STRING, 1);
        pnd_Prev_Plan_Cde = localVariables.newFieldInRecord("pnd_Prev_Plan_Cde", "#PREV-PLAN-CDE", FieldType.STRING, 6);
        pnd_Prev_Pin_A12 = localVariables.newFieldInRecord("pnd_Prev_Pin_A12", "#PREV-PIN-A12", FieldType.STRING, 12);
        pnd_Prev_Contract = localVariables.newFieldInRecord("pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Cover = localVariables.newFieldInRecord("pnd_Prev_Cover", "#PREV-COVER", FieldType.STRING, 1);
        pnd_Prev_Letter_Type = localVariables.newFieldInRecord("pnd_Prev_Letter_Type", "#PREV-LETTER-TYPE", FieldType.STRING, 1);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_Valid_Record = localVariables.newFieldInRecord("pnd_Valid_Record", "#VALID-RECORD", FieldType.BOOLEAN, 1);
        pnd_Accepted = localVariables.newFieldInRecord("pnd_Accepted", "#ACCEPTED", FieldType.BOOLEAN, 1);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 40);
        pnd_Lob = localVariables.newFieldInRecord("pnd_Lob", "#LOB", FieldType.STRING, 4);
        pnd_Cutoff = localVariables.newFieldInRecord("pnd_Cutoff", "#CUTOFF", FieldType.STRING, 6);
        pnd_Participant_Name = localVariables.newFieldInRecord("pnd_Participant_Name", "#PARTICIPANT-NAME", FieldType.STRING, 35);
        pnd_Ctr_Svc_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Svc_Contract_Read", "#CTR-SVC-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Valid_Srv_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Valid_Srv_Contract_Read", "#CTR-VALID-SRV-CONTRACT-READ", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Da_Contract_Not_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Contract_Not_In_Cor", "#CTR-DA-CONTRACT-NOT-IN-COR", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Da_Inactive_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Inactive_In_Cor", "#CTR-DA-INACTIVE-IN-COR", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_Deceased = localVariables.newFieldInRecord("pnd_Ctr_Ph_Deceased", "#CTR-PH-DECEASED", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_With_Mail_Suppression = localVariables.newFieldInRecord("pnd_Ctr_Ph_With_Mail_Suppression", "#CTR-PH-WITH-MAIL-SUPPRESSION", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Contract_Accepted = localVariables.newFieldInRecord("pnd_Ctr_Contract_Accepted", "#CTR-CONTRACT-ACCEPTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Rejected = localVariables.newFieldInRecord("pnd_Ctr_Contract_Rejected", "#CTR-CONTRACT-REJECTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Contract_Read", "#CTR-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Duplicate_Contract = localVariables.newFieldInRecord("pnd_Ctr_Duplicate_Contract", "#CTR-DUPLICATE-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Terminated_Contract = localVariables.newFieldInRecord("pnd_Ctr_Terminated_Contract", "#CTR-TERMINATED-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Inactive = localVariables.newFieldInRecord("pnd_Ctr_Contract_Inactive", "#CTR-CONTRACT-INACTIVE", FieldType.NUMERIC, 7);
        pnd_Ctr_Out_Of_State = localVariables.newFieldInRecord("pnd_Ctr_Out_Of_State", "#CTR-OUT-OF-STATE", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Outof_Range = localVariables.newFieldInRecord("pnd_Ctr_Contract_Outof_Range", "#CTR-CONTRACT-OUTOF-RANGE", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Institution_Owned = localVariables.newFieldInRecord("pnd_Ctr_Institution_Owned", "#CTR-INSTITUTION-OWNED", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Contracts = localVariables.newFieldInRecord("pnd_Ctr_Total_Contracts", "#CTR-TOTAL-CONTRACTS", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Tiaa_Ra = localVariables.newFieldInRecord("pnd_Ctr_Total_Tiaa_Ra", "#CTR-TOTAL-TIAA-RA", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Tiaa_Gra_Rc = localVariables.newFieldInRecord("pnd_Ctr_Total_Tiaa_Gra_Rc", "#CTR-TOTAL-TIAA-GRA-RC", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Cref = localVariables.newFieldInRecord("pnd_Ctr_Total_Cref", "#CTR-TOTAL-CREF", FieldType.NUMERIC, 7);
        pnd_Break = localVariables.newFieldInRecord("pnd_Break", "#BREAK", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);

        pnd_Arrays = localVariables.newGroupArrayInRecord("pnd_Arrays", "#ARRAYS", new DbsArrayController(1, 240));
        pnd_Arrays_Pnd_Combo_Prod = pnd_Arrays.newFieldInGroup("pnd_Arrays_Pnd_Combo_Prod", "#COMBO-PROD", FieldType.STRING, 12);
        pnd_Arrays_Pnd_Combo_Ctr = pnd_Arrays.newFieldInGroup("pnd_Arrays_Pnd_Combo_Ctr", "#COMBO-CTR", FieldType.NUMERIC, 7);
        pnd_Combo = localVariables.newFieldInRecord("pnd_Combo", "#COMBO", FieldType.STRING, 12);

        pnd_Ctrs = localVariables.newGroupInRecord("pnd_Ctrs", "#CTRS");
        pnd_Ctrs_Pnd_Ctr_Tiaa_Ra = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Tiaa_Ra", "#CTR-TIAA-RA", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_4 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_4", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Tiaa_Ra);
        pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_A = pnd_Ctrs__R_Field_4.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_A", "#CTR-TIAA-RA-A", FieldType.STRING, 1);
        pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc", "#CTR-TIAA-GRA-RC", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_5 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_5", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc);
        pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_A = pnd_Ctrs__R_Field_5.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_A", "#CTR-TIAA-GRA-RC-A", FieldType.STRING, 
            1);
        pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_Nh = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_Nh", "#CTR-TIAA-RA-NH", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_6 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_6", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_Nh);
        pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_Nh_A = pnd_Ctrs__R_Field_6.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_Nh_A", "#CTR-TIAA-RA-NH-A", FieldType.STRING, 1);
        pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_Nh = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_Nh", "#CTR-TIAA-GRA-RC-NH", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_7 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_7", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_Nh);
        pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A = pnd_Ctrs__R_Field_7.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A", "#CTR-TIAA-GRA-RC-NH-A", FieldType.STRING, 
            1);
        pnd_Ctrs_Pnd_Ctr_Cref = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref", "#CTR-CREF", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_8 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_8", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Cref);
        pnd_Ctrs_Pnd_Ctr_Cref_A = pnd_Ctrs__R_Field_8.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_A", "#CTR-CREF-A", FieldType.STRING, 1);
        pnd_Ctrs_Pnd_Ctr_Cref_Il = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Il", "#CTR-CREF-IL", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_9 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_9", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Cref_Il);
        pnd_Ctrs_Pnd_Ctr_Cref_Il_A = pnd_Ctrs__R_Field_9.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Il_A", "#CTR-CREF-IL-A", FieldType.STRING, 1);
        pnd_Ctrs_Pnd_Ctr_Cref_Ra_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Ra_Fl", "#CTR-CREF-RA-FL", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_10 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_10", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Cref_Ra_Fl);
        pnd_Ctrs_Pnd_Ctr_Cref_Ra_Fl_A = pnd_Ctrs__R_Field_10.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Ra_Fl_A", "#CTR-CREF-RA-FL-A", FieldType.STRING, 1);
        pnd_Ctrs_Pnd_Ctr_Cref_Gra_Rc_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Gra_Rc_Fl", "#CTR-CREF-GRA-RC-FL", FieldType.NUMERIC, 1);

        pnd_Ctrs__R_Field_11 = pnd_Ctrs.newGroupInGroup("pnd_Ctrs__R_Field_11", "REDEFINE", pnd_Ctrs_Pnd_Ctr_Cref_Gra_Rc_Fl);
        pnd_Ctrs_Pnd_Ctr_Cref_Gra_Rc_Fl_A = pnd_Ctrs__R_Field_11.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cref_Gra_Rc_Fl_A", "#CTR-CREF-GRA-RC-FL-A", FieldType.STRING, 
            1);
        pnd_Ctrs_Pnd_Ctr_Cert_Ca = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Ca", "#CTR-CERT-CA", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Ok = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Ok", "#CTR-CERT-OK", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Gn = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Gn", "#CTR-CERT-GN", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Nc = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Nc", "#CTR-CERT-NC", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_La = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_La", "#CTR-CERT-LA", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Mn = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Mn", "#CTR-CERT-MN", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Mt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Mt", "#CTR-CERT-MT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Ne = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Ne", "#CTR-CERT-NE", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Va = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Va", "#CTR-CERT-VA", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Fl", "#CTR-CERT-FL", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Nh = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Nh", "#CTR-CERT-NH", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Oregon = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Oregon", "#CTR-OREGON", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Or = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Or", "#CTR-CERT-OR", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Nm = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Nm", "#CTR-CERT-NM", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Ny = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Ny", "#CTR-CERT-NY", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Cert_Pr = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Cert_Pr", "#CTR-CERT-PR", FieldType.NUMERIC, 1);
        pnd_Ctr_File1 = localVariables.newFieldInRecord("pnd_Ctr_File1", "#CTR-FILE1", FieldType.NUMERIC, 7);

        pnd_Output = localVariables.newGroupInRecord("pnd_Output", "#OUTPUT");
        pnd_Output_Pnd_Pin_A12 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);
        pnd_Output_Pnd_Letter_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Institution_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 120);
        pnd_Output_Pnd_Ctr_Tiaa_Ra_A = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Tiaa_Ra_A", "#CTR-TIAA-RA-A", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Tiaa_Gra_Rc_A = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Tiaa_Gra_Rc_A", "#CTR-TIAA-GRA-RC-A", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Tiaa_Ra_Nh_A = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Tiaa_Ra_Nh_A", "#CTR-TIAA-RA-NH-A", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A", "#CTR-TIAA-GRA-RC-NH-A", FieldType.STRING, 
            1);
        pnd_Output_Pnd_Ctr_Cref_A = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cref_A", "#CTR-CREF-A", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Cref_Il_A = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cref_Il_A", "#CTR-CREF-IL-A", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Cref_Ra_Fl_A = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cref_Ra_Fl_A", "#CTR-CREF-RA-FL-A", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Cref_Gra_Rc_Fl_A = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cref_Gra_Rc_Fl_A", "#CTR-CREF-GRA-RC-FL-A", FieldType.STRING, 
            1);
        pnd_Output_Pnd_Ctr_Cert_Gn = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Gn", "#CTR-CERT-GN", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Ca = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Ca", "#CTR-CERT-CA", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Ok = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Ok", "#CTR-CERT-OK", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Nc = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Nc", "#CTR-CERT-NC", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_La = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_La", "#CTR-CERT-LA", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Mn = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Mn", "#CTR-CERT-MN", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Mt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Mt", "#CTR-CERT-MT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Ne = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Ne", "#CTR-CERT-NE", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Va = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Va", "#CTR-CERT-VA", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Fl = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Fl", "#CTR-CERT-FL", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Nh = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Nh", "#CTR-CERT-NH", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Oregon = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Oregon", "#CTR-OREGON", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Or = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Or", "#CTR-CERT-OR", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Nm = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Nm", "#CTR-CERT-NM", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Ny = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Ny", "#CTR-CERT-NY", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Cert_Pr = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Cert_Pr", "#CTR-CERT-PR", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Inst_Sv_Lob = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Inst_Sv_Lob", "#INST-SV-LOB", FieldType.STRING, 1);
        pnd_Output_Pnd_Package_Code = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Output_Pnd_Contracts = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Contracts", "#CONTRACTS", FieldType.STRING, 17, new DbsArrayController(1, 
            20));
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Valid_Record.setInitialValue(false);
        pnd_Accepted.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridp540() throws Exception
    {
        super("Ridp540");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  STATISTICS REPORT
        if (condition(getReports().getAstLinesLeft(1).less(5)))                                                                                                           //Natural: FORMAT ( 1 ) LS = 120 PS = 60;//Natural: FORMAT LS = 80 PS = 60;//Natural: NEWPAGE ( 1 ) IF LESS THAN 5 LINES
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        //*  OPEN MQ QUEUE                                        /JCA20150324
        getReports().write(0, " FETCHING MDMP011");                                                                                                                       //Natural: WRITE ' FETCHING MDMP011'
        if (Global.isEscape()) return;
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        //*  PINEXP >>>
        //*  PINEXP <<<
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 34T 'TIAA Stable Value RIDER MAILING' 84T 'DATE:' *DATU / 33T 'LIST OF INSTITUTION-OWNED CONTRACTS' 84T 'TIME:' *TIME ( EM = XXXXXXXX ) / 84T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) // 2T 'PIN         ' 16T 'PARTICIPANT NAME   ' 53T 'TIAA #  ' 63T 'LOB  ' 70T 'CUTOFF' 78T 'PPG/PLAN'/ 2T '------------' 16T '-----------------------------------' 53T '--------' 63T '-----' 70T '------' 78T '--------'//
        //*      2T 'INSTITUTION : ' #INST-NAME //
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #INPUT-RECORD
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  PINEXP
            //*  LS1
            //*  LS1
            //*  LS1
            pnd_Ctr_Svc_Contract_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CTR-SVC-CONTRACT-READ
            //*                                                                                                                                                           //Natural: AT BREAK OF #INPUT-RECORD.#PIN-A12
            //*                                                                                                                                                           //Natural: AT END OF DATA
            if (condition(pnd_Break.getBoolean()))                                                                                                                        //Natural: IF #BREAK
            {
                pnd_Break.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #BREAK
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP
            pnd_Prev_Pin_A12.setValue(pnd_Input_Record_Pnd_Pin_A12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-A12 TO #PREV-PIN-A12
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Inst_Name);                                                                                                  //Natural: MOVE #INPUT-RECORD.#INST-NAME TO #PREV-INST-NAME
            pnd_Prev_Inst_Sv_Lob.setValue(pnd_Input_Record_Pnd_Inst_Sv_Lob);                                                                                              //Natural: MOVE #INPUT-RECORD.#INST-SV-LOB TO #PREV-INST-SV-LOB
            //*  LS1
            pnd_Prev_Letter_Type.setValue(pnd_Input_Record_Pnd_Letter_Type);                                                                                              //Natural: MOVE #INPUT-RECORD.#LETTER-TYPE TO #PREV-LETTER-TYPE
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
        }                                                                                                                                                                 //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-STATISTICS-REPORT
        sub_Write_Statistics_Report();
        if (condition(Global.isEscape())) {return;}
        //*  CLOSE MQ QUEUE
        getReports().write(0, "FETCHING MDMP0012");                                                                                                                       //Natural: WRITE 'FETCHING MDMP0012'
        if (Global.isEscape()) return;
        //*  JCA20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* **********************************************************************
        //*                 S T A R T    O F    S U B R O U T I N E S
        //* **********************************************************************
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-RECORD
        //*  RA TIAA CONTRACTS
        //*  GRA CONTRACTS
        //*  RC CONTRACTS
        //*  ---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-CONTRACT-TO-ARRAY
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RECORD-IN-COR
        //*  --------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PIN-STATUS
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA-TO-FILE
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-STATISTICS-REPORT
    }
    private void sub_Validate_Record() throws Exception                                                                                                                   //Natural: VALIDATE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        if (condition(pnd_Prev_Contract.equals(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr)))                                                                                    //Natural: IF #PREV-CONTRACT EQ #TIAA-CNTRCT-NBR
        {
            pnd_Ctr_Duplicate_Contract.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DUPLICATE-CONTRACT
            pnd_Remarks.setValue("DUPLICATE CONTRACT");                                                                                                                   //Natural: ASSIGN #REMARKS := 'DUPLICATE CONTRACT'
            //*  PINEXP
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prev_Contract.setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #PREV-CONTRACT
                                                                                                                                                                          //Natural: PERFORM CHECK-RECORD-IN-COR
        sub_Check_Record_In_Cor();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Valid_Record.getBoolean()))                                                                                                                     //Natural: IF #VALID-RECORD
        {
            //*  RA
            //*  RA
            //*  RA
            //*  RA
            //*  RA
            //*  RA
            //*  RA PRE
            short decideConditionsMet686 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( #TIAA-CNTRCT-NBR EQ 'D0100000' THRU 'D9599999' OR #TIAA-CNTRCT-NBR EQ 'C9000000' THRU 'C9999999' OR #TIAA-CNTRCT-NBR EQ 'A0316250' THRU 'A8999999' OR #TIAA-CNTRCT-NBR EQ 'B0000010' THRU 'B7999999' OR #TIAA-CNTRCT-NBR EQ 'B8015000' THRU 'B9999989' OR #TIAA-CNTRCT-NBR EQ 'C0100000' THRU 'C7999999' OR #TIAA-CNTRCT-NBR EQ 'A0000010' THRU 'A0316249' ) AND ( #PREV-INST-SV-LOB = 'B' OR = 'D' OR = 'F' OR = 'G' )
            if (condition(((((((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("D0100000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("D9599999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C9000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C9999999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("A0316250") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("A8999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("B0000010") 
                && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("B7999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("B8015000") && 
                pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("B9999989"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C0100000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C7999999"))) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("A0000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("A0316249"))) && 
                (((pnd_Prev_Inst_Sv_Lob.equals("B") || pnd_Prev_Inst_Sv_Lob.equals("D")) || pnd_Prev_Inst_Sv_Lob.equals("F")) || pnd_Prev_Inst_Sv_Lob.equals("G")))))
            {
                decideConditionsMet686++;
                if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("040")))                                                                                        //Natural: IF #ISSUE-STATE-CDE EQ '040'
                {
                    pnd_Ctrs_Pnd_Ctr_Oregon.setValue(1);                                                                                                                  //Natural: MOVE 1 TO #CTRS.#CTR-OREGON
                }                                                                                                                                                         //Natural: END-IF
                //*  060112 J.AVE
                if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("032")))                                                                                        //Natural: IF #ISSUE-STATE-CDE EQ '032'
                {
                    pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_Nh.setValue(1);                                                                                                              //Natural: MOVE 1 TO #CTR-TIAA-RA-NH
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ctrs_Pnd_Ctr_Tiaa_Ra.setValue(1);                                                                                                                 //Natural: MOVE 1 TO #CTR-TIAA-RA
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr_Total_Tiaa_Ra.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CTR-TOTAL-TIAA-RA
                if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                       //Natural: IF #CREF-CNTRCT-NBR NE ' '
                {
                    if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("016")))                                                                                    //Natural: IF #ISSUE-STATE-CDE EQ '016'
                    {
                        pnd_Ctrs_Pnd_Ctr_Cref_Il.setValue(1);                                                                                                             //Natural: MOVE 1 TO #CTR-CREF-IL
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  060112 J.AVE
                        if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("011")))                                                                                //Natural: IF #ISSUE-STATE-CDE EQ '011'
                        {
                            pnd_Ctrs_Pnd_Ctr_Cref_Ra_Fl.setValue(1);                                                                                                      //Natural: MOVE 1 TO #CTR-CREF-RA-FL
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ctrs_Pnd_Ctr_Cref.setValue(1);                                                                                                            //Natural: MOVE 1 TO #CTR-CREF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ctr_Total_Cref.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-TOTAL-CREF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                //*  061107 J.AVE
                //*  GRA
                //*  GRA
                //*  GRA
                //*  GRAII
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN ( #TIAA-CNTRCT-NBR EQ '30000100' THRU '39499999' OR #TIAA-CNTRCT-NBR EQ '20000010' THRU '20099999' OR #TIAA-CNTRCT-NBR EQ '20250000' THRU '28999999' OR #TIAA-CNTRCT-NBR EQ '20100000' THRU '20249999' ) AND ( #PREV-INST-SV-LOB = 'C' OR = 'E' OR = 'F' OR = 'G' )
            else if (condition((((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("30000100") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("39499999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("20099999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20250000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("28999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20100000") 
                && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("20249999"))) && (((pnd_Prev_Inst_Sv_Lob.equals("C") || pnd_Prev_Inst_Sv_Lob.equals("E")) 
                || pnd_Prev_Inst_Sv_Lob.equals("F")) || pnd_Prev_Inst_Sv_Lob.equals("G")))))
            {
                decideConditionsMet686++;
                if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("040")))                                                                                        //Natural: IF #ISSUE-STATE-CDE EQ '040'
                {
                    pnd_Ctrs_Pnd_Ctr_Oregon.setValue(1);                                                                                                                  //Natural: MOVE 1 TO #CTRS.#CTR-OREGON
                }                                                                                                                                                         //Natural: END-IF
                //*  060112 J.AVE
                if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("032")))                                                                                        //Natural: IF #ISSUE-STATE-CDE EQ '032'
                {
                    pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_Nh.setValue(1);                                                                                                          //Natural: MOVE 1 TO #CTR-TIAA-GRA-RC-NH
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc.setValue(1);                                                                                                             //Natural: MOVE 1 TO #CTR-TIAA-GRA-RC
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr_Total_Tiaa_Gra_Rc.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-TOTAL-TIAA-GRA-RC
                if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                       //Natural: IF #CREF-CNTRCT-NBR NE ' '
                {
                    if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("016")))                                                                                    //Natural: IF #ISSUE-STATE-CDE EQ '016'
                    {
                        pnd_Ctrs_Pnd_Ctr_Cref_Il.setValue(1);                                                                                                             //Natural: MOVE 1 TO #CTR-CREF-IL
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  060112 J.AVE
                        if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("011")))                                                                                //Natural: IF #ISSUE-STATE-CDE EQ '011'
                        {
                            pnd_Ctrs_Pnd_Ctr_Cref_Gra_Rc_Fl.setValue(1);                                                                                                  //Natural: MOVE 1 TO #CTR-CREF-GRA-RC-FL
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ctrs_Pnd_Ctr_Cref.setValue(1);                                                                                                            //Natural: MOVE 1 TO #CTR-CREF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ctr_Total_Cref.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-TOTAL-CREF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                //*  060117 J.AVE
                //*  RC
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #TIAA-CNTRCT-NBR EQ 'F0000000' THRU 'F0999999' AND ( #PREV-INST-SV-LOB = 'A' OR = 'D' OR = 'E' OR = 'G' )
            else if (condition(((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("F0000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("F0999999")) 
                && (((pnd_Prev_Inst_Sv_Lob.equals("A") || pnd_Prev_Inst_Sv_Lob.equals("D")) || pnd_Prev_Inst_Sv_Lob.equals("E")) || pnd_Prev_Inst_Sv_Lob.equals("G")))))
            {
                decideConditionsMet686++;
                if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("040")))                                                                                        //Natural: IF #ISSUE-STATE-CDE EQ '040'
                {
                    pnd_Ctrs_Pnd_Ctr_Oregon.setValue(1);                                                                                                                  //Natural: MOVE 1 TO #CTRS.#CTR-OREGON
                }                                                                                                                                                         //Natural: END-IF
                //*  060112 J.AVE
                if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("032")))                                                                                        //Natural: IF #ISSUE-STATE-CDE EQ '032'
                {
                    pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_Nh.setValue(1);                                                                                                          //Natural: MOVE 1 TO #CTR-TIAA-GRA-RC-NH
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc.setValue(1);                                                                                                             //Natural: MOVE 1 TO #CTR-TIAA-GRA-RC
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr_Total_Tiaa_Gra_Rc.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-TOTAL-TIAA-GRA-RC
                if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                       //Natural: IF #CREF-CNTRCT-NBR NE ' '
                {
                    if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("016")))                                                                                    //Natural: IF #ISSUE-STATE-CDE EQ '016'
                    {
                        pnd_Ctrs_Pnd_Ctr_Cref_Il.setValue(1);                                                                                                             //Natural: MOVE 1 TO #CTR-CREF-IL
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  060112 J.AVE
                        if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("011")))                                                                                //Natural: IF #ISSUE-STATE-CDE EQ '011'
                        {
                            pnd_Ctrs_Pnd_Ctr_Cref_Gra_Rc_Fl.setValue(1);                                                                                                  //Natural: MOVE 1 TO #CTR-CREF-GRA-RC-FL
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ctrs_Pnd_Ctr_Cref.setValue(1);                                                                                                            //Natural: MOVE 1 TO #CTR-CREF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ctr_Total_Cref.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-TOTAL-CREF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctr_Contract_Accepted.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-ACCEPTED
                //*  060117 J.AVE
                                                                                                                                                                          //Natural: PERFORM ADD-CONTRACT-TO-ARRAY
                sub_Add_Contract_To_Array();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Ctr_Contract_Outof_Range.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CTR-CONTRACT-OUTOF-RANGE
                pnd_Remarks.setValue("CONTRACT OUT OF RANGE");                                                                                                            //Natural: ASSIGN #REMARKS := 'CONTRACT OUT OF RANGE'
                //*  PINEXP
                getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  DETERMINE CERTIFICATE
            short decideConditionsMet794 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #INST-STATE-CDE;//Natural: VALUE 'CA'
            if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("CA"))))
            {
                decideConditionsMet794++;
                pnd_Ctrs_Pnd_Ctr_Cert_Ca.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-CA
            }                                                                                                                                                             //Natural: VALUE 'OK'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("OK"))))
            {
                decideConditionsMet794++;
                pnd_Ctrs_Pnd_Ctr_Cert_Ok.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-OK
            }                                                                                                                                                             //Natural: VALUE 'NC'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("NC"))))
            {
                decideConditionsMet794++;
                pnd_Ctrs_Pnd_Ctr_Cert_Nc.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-NC
            }                                                                                                                                                             //Natural: VALUE 'LA'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("LA"))))
            {
                decideConditionsMet794++;
                pnd_Ctrs_Pnd_Ctr_Cert_La.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-LA
            }                                                                                                                                                             //Natural: VALUE 'MN'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("MN"))))
            {
                decideConditionsMet794++;
                pnd_Ctrs_Pnd_Ctr_Cert_Mn.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-MN
            }                                                                                                                                                             //Natural: VALUE 'MT'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("MT"))))
            {
                decideConditionsMet794++;
                pnd_Ctrs_Pnd_Ctr_Cert_Mt.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-MT
            }                                                                                                                                                             //Natural: VALUE 'NE'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("NE"))))
            {
                decideConditionsMet794++;
                pnd_Ctrs_Pnd_Ctr_Cert_Ne.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-NE
            }                                                                                                                                                             //Natural: VALUE 'VA'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("VA"))))
            {
                decideConditionsMet794++;
                //*  060112 J.AVE
                pnd_Ctrs_Pnd_Ctr_Cert_Va.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-VA
            }                                                                                                                                                             //Natural: VALUE 'FL'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("FL"))))
            {
                decideConditionsMet794++;
                //*  060112 J.AVE
                pnd_Ctrs_Pnd_Ctr_Cert_Fl.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-FL
            }                                                                                                                                                             //Natural: VALUE 'NH'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("NH"))))
            {
                decideConditionsMet794++;
                //*  LS1
                pnd_Ctrs_Pnd_Ctr_Cert_Nh.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-NH
            }                                                                                                                                                             //Natural: VALUE 'NM'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("NM"))))
            {
                decideConditionsMet794++;
                //*  LS1
                //*  LS1
                pnd_Ctrs_Pnd_Ctr_Cert_Nm.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-NM
            }                                                                                                                                                             //Natural: VALUE 'NY'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("NY"))))
            {
                decideConditionsMet794++;
                //*  LS1
                //*  LS1
                pnd_Ctrs_Pnd_Ctr_Cert_Ny.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-NY
            }                                                                                                                                                             //Natural: VALUE 'PR'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("PR"))))
            {
                decideConditionsMet794++;
                //*  LS1
                //*  LS1A
                pnd_Ctrs_Pnd_Ctr_Cert_Pr.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-PR
            }                                                                                                                                                             //Natural: VALUE 'OR'
            else if (condition((pnd_Input_Record_Pnd_Inst_State_Cde.equals("OR"))))
            {
                decideConditionsMet794++;
                //*  LS1A
                pnd_Ctrs_Pnd_Ctr_Cert_Or.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-OR
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ctrs_Pnd_Ctr_Cert_Gn.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #CTRS.#CTR-CERT-GN
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE-RECORD
    }
    //*  061107 J.AVE
    private void sub_Add_Contract_To_Array() throws Exception                                                                                                             //Natural: ADD-CONTRACT-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------
        pnd_K.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #K
        if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                               //Natural: IF #CREF-CNTRCT-NBR NE ' '
        {
            pnd_Output_Pnd_Contracts.getValue(pnd_K).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr, "/",                  //Natural: COMPRESS #TIAA-CNTRCT-NBR '/' #CREF-CNTRCT-NBR TO #CONTRACTS ( #K ) LEAVING NO SPACE
                pnd_Input_Record_Pnd_Cref_Cntrct_Nbr));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_Pnd_Contracts.getValue(pnd_K).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                      //Natural: MOVE #TIAA-CNTRCT-NBR TO #CONTRACTS ( #K )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-CONTRACT-TO-ARRAY
    }
    private void sub_Check_Record_In_Cor() throws Exception                                                                                                               //Natural: CHECK-RECORD-IN-COR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------
        //*  MODIFY MDM*210 TO MDM*211 IN THIS SUBROUTINE
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #MDMA211.#I-CONTRACT-NUMBER
        //*  JCA20150324
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue("01");                                                                                                   //Natural: MOVE '01' TO #MDMA211.#I-PAYEE-CODE-A2
        //*  JCA20150324
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //*  JCA20150324
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA211.#O-RETURN-CODE NE '0000'
        {
            pnd_Remarks.setValue("CONTRACT NOT IN COR");                                                                                                                  //Natural: ASSIGN #REMARKS := 'CONTRACT NOT IN COR'
            //*  PINEXP
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Contract_Not_In_Cor.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-DA-CONTRACT-NOT-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals(" ") || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("H")         //Natural: IF NOT ( #MDMA211.#O-CONTRACT-STATUS-CODE = ' ' OR = 'H' OR = 'Y' )
            || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("Y"))))
        {
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("CONTRACT INACTIVE IN COR - STATUS CODE:", pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code()));                    //Natural: COMPRESS 'CONTRACT INACTIVE IN COR - STATUS CODE:' #MDMA211.#O-CONTRACT-STATUS-CODE TO #REMARKS
            //*  PINEXP
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Inactive_In_Cor.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DA-INACTIVE-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PIN-STATUS
            sub_Determine_Pin_Status();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Accepted.getBoolean()))                                                                                                                     //Natural: IF #ACCEPTED
            {
                pnd_Valid_Record.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-RECORD-IN-COR
    }
    private void sub_Determine_Pin_Status() throws Exception                                                                                                              //Natural: DETERMINE-PIN-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------
        //*  MODIFY MDM*100 TO MDM*101 IN THIS SUBROUTINE         /* PINEXP
        //*  JCA20150324
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        //*    MOVE #INPUT-RECORD.#PIN TO #MDMA101.#I-PIN /* PINEXP /* JCA20150324
        //*    WRITE 'CALLING MDMN101A TO GET ADDRESS/NAME'
        //*  PINEXP >>>
        if (condition(pnd_Input_Record_Pnd_Pin_A12.notEquals(" ")))                                                                                                       //Natural: IF #INPUT-RECORD.#PIN-A12 NE ' '
        {
            if (condition(pnd_Input_Record_Pnd_Pin_A12.getSubstring(8,1).equals(" ")))                                                                                    //Natural: IF SUBSTRING ( #INPUT-RECORD.#PIN-A12,8,1 ) = ' '
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N7);                                                                          //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #PIN-N7
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                         //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #PIN-N12
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP <<<
        }                                                                                                                                                                 //Natural: END-IF
        //*  JCA20150324
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  REJECT DECEASED PARTICIPANTS
        //*  JCA20150324
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero())))                                                                                //Natural: IF #MDMA101.#O-DATE-OF-DEATH GT 0
        {
            pnd_Ctr_Ph_Deceased.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CTR-PH-DECEASED
            //*  JCA20150324
            pnd_Remarks.setValue(DbsUtil.compress("P/H DECEASED - DOD:", pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death()));                                               //Natural: COMPRESS 'P/H DECEASED - DOD:' #MDMA101.#O-DATE-OF-DEATH TO #REMARKS
            getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Accepted.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #ACCEPTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  REJECT PARTICIPANTS W/ STOP ALL MAILING SUPPRESSION CODE
            //*  JA20150323
            DbsUtil.examine(new ExamineSource(pdaMdma101.getPnd_Mdma101_Pnd_O_Mail_Code().getValue("*")), new ExamineSearch("01"), new ExamineGivingPosition(pnd_I));     //Natural: EXAMINE #MDMA101.#O-MAIL-CODE ( * ) FOR '01' GIVING POSITION #I
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I GT 0
            {
                pnd_Ctr_Ph_With_Mail_Suppression.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTR-PH-WITH-MAIL-SUPPRESSION
                pnd_Remarks.setValue(DbsUtil.compress("P/H MAIL SUPPRESSED - MAIL CODE: 01"));                                                                            //Natural: COMPRESS 'P/H MAIL SUPPRESSED - MAIL CODE: 01' TO #REMARKS
                getReports().write(0, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Accepted.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #ACCEPTED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Accepted.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #ACCEPTED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE-PIN-STATUS
    }
    private void sub_Write_Data_To_File() throws Exception                                                                                                                //Natural: WRITE-DATA-TO-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        pnd_Output_Pnd_Package_Code.setValue("RDR2010C");                                                                                                                 //Natural: MOVE 'RDR2010C' TO #PACKAGE-CODE
        pnd_Ctr_File1.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #CTR-FILE1
        getWorkFiles().write(2, false, pnd_Output);                                                                                                                       //Natural: WRITE WORK FILE 2 #OUTPUT
        pnd_Output.reset();                                                                                                                                               //Natural: RESET #OUTPUT
        //*  WRITE-DATA-TO-FILE
    }
    private void sub_Write_Statistics_Report() throws Exception                                                                                                           //Natural: WRITE-STATISTICS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        getReports().write(1, NEWLINE,NEWLINE,"TOTAL INSTITUTION-OWNED CONTRACTS :",pnd_Ctr_Institution_Owned, new ReportEditMask ("Z,ZZZ,ZZ9"));                         //Natural: WRITE ( 1 ) // 'TOTAL INSTITUTION-OWNED CONTRACTS :' #CTR-INSTITUTION-OWNED ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        pnd_Ctr_Duplicate_Contract.nsubtract(1);                                                                                                                          //Natural: ASSIGN #CTR-DUPLICATE-CONTRACT := #CTR-DUPLICATE-CONTRACT - 1
        pnd_Ctr_Contract_Rejected.compute(new ComputeParameters(false, pnd_Ctr_Contract_Rejected), pnd_Ctr_Da_Contract_Not_In_Cor.add(pnd_Ctr_Da_Inactive_In_Cor).add(pnd_Ctr_Ph_Deceased).add(pnd_Ctr_Ph_With_Mail_Suppression).add(pnd_Ctr_Out_Of_State).add(pnd_Ctr_Contract_Outof_Range).add(pnd_Ctr_Institution_Owned).add(pnd_Ctr_Duplicate_Contract)); //Natural: ASSIGN #CTR-CONTRACT-REJECTED := #CTR-DA-CONTRACT-NOT-IN-COR + #CTR-DA-INACTIVE-IN-COR + #CTR-PH-DECEASED + #CTR-PH-WITH-MAIL-SUPPRESSION + #CTR-OUT-OF-STATE + #CTR-CONTRACT-OUTOF-RANGE + #CTR-INSTITUTION-OWNED + #CTR-DUPLICATE-CONTRACT
        //*    WRITE 'INSTITUTION NAME:' #INST-NAME
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ         :",pnd_Ctr_Svc_Contract_Read);                                                                              //Natural: WRITE 'NUMBER OF RECORDS READ         :' #CTR-SVC-CONTRACT-READ
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS ACCEPTED     :",pnd_Ctr_Contract_Accepted);                                                                              //Natural: WRITE 'NUMBER OF RECORDS ACCEPTED     :' #CTR-CONTRACT-ACCEPTED
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS REJECTED     :",pnd_Ctr_Contract_Rejected);                                                                              //Natural: WRITE 'NUMBER OF RECORDS REJECTED     :' #CTR-CONTRACT-REJECTED
        if (Global.isEscape()) return;
        getReports().write(0, "BREAKDOWN OF RECORDS REJECTED  :");                                                                                                        //Natural: WRITE 'BREAKDOWN OF RECORDS REJECTED  :'
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT NOT IN COR     :",pnd_Ctr_Da_Contract_Not_In_Cor);                                                                              //Natural: WRITE ' -CONTRACT NOT IN COR     :' #CTR-DA-CONTRACT-NOT-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT INACTIVE IN COR:",pnd_Ctr_Da_Inactive_In_Cor);                                                                                  //Natural: WRITE ' -CONTRACT INACTIVE IN COR:' #CTR-DA-INACTIVE-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H DECEASED            :",pnd_Ctr_Ph_Deceased);                                                                                         //Natural: WRITE ' -P/H DECEASED            :' #CTR-PH-DECEASED
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H W/ MAIL SUPPRESSION :",pnd_Ctr_Ph_With_Mail_Suppression);                                                                            //Natural: WRITE ' -P/H W/ MAIL SUPPRESSION :' #CTR-PH-WITH-MAIL-SUPPRESSION
        if (Global.isEscape()) return;
        //*    WRITE ' -ISSUE STATE OUTSIDE CA  :' #CTR-OUT-OF-STATE
        getReports().write(0, " -CONTRACT OUT OF RANGE   :",pnd_Ctr_Contract_Outof_Range);                                                                                //Natural: WRITE ' -CONTRACT OUT OF RANGE   :' #CTR-CONTRACT-OUTOF-RANGE
        if (Global.isEscape()) return;
        getReports().write(0, " -INSTITUTION OWNED       :",pnd_Ctr_Institution_Owned);                                                                                   //Natural: WRITE ' -INSTITUTION OWNED       :' #CTR-INSTITUTION-OWNED
        if (Global.isEscape()) return;
        getReports().write(0, " -DUPLICATE CONTRACT      :",pnd_Ctr_Duplicate_Contract);                                                                                  //Natural: WRITE ' -DUPLICATE CONTRACT      :' #CTR-DUPLICATE-CONTRACT
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY PRODUCT:");                                                                                                                       //Natural: WRITE 'TOTAL BY PRODUCT:'
        if (Global.isEscape()) return;
        getReports().write(0, " - TIAA RA     - GN :",pnd_Ctr_Total_Tiaa_Ra);                                                                                             //Natural: WRITE ' - TIAA RA     - GN :' #CTR-TOTAL-TIAA-RA
        if (Global.isEscape()) return;
        getReports().write(0, " - TIAA GRA/RC - GN :",pnd_Ctr_Total_Tiaa_Gra_Rc);                                                                                         //Natural: WRITE ' - TIAA GRA/RC - GN :' #CTR-TOTAL-TIAA-GRA-RC
        if (Global.isEscape()) return;
        getReports().write(0, " - CREF        - GN :",pnd_Ctr_Total_Cref);                                                                                                //Natural: WRITE ' - CREF        - GN :' #CTR-TOTAL-CREF
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY FILE:");                                                                                                                          //Natural: WRITE 'TOTAL BY FILE:'
        if (Global.isEscape()) return;
        getReports().write(0, " - FILE 1 : ",pnd_Ctr_File1,new ColumnSpacing(5),"(flats)");                                                                               //Natural: WRITE ' - FILE 1 : ' #CTR-FILE1 5X '(flats)'
        if (Global.isEscape()) return;
        //*  WRITE-STATISTICS-REPORT
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Input_Record_Pnd_Pin_A12IsBreak = pnd_Input_Record_Pnd_Pin_A12.isBreak(endOfData);
        if (condition(pnd_Input_Record_Pnd_Pin_A12IsBreak))
        {
            pnd_Ctr_Total_Contracts.compute(new ComputeParameters(false, pnd_Ctr_Total_Contracts), pnd_Ctrs_Pnd_Ctr_Cert_Gn.add(pnd_Ctrs_Pnd_Ctr_Cert_Ca).add(pnd_Ctrs_Pnd_Ctr_Cert_Ok).add(pnd_Ctrs_Pnd_Ctr_Cert_Nc).add(pnd_Ctrs_Pnd_Ctr_Cert_La).add(pnd_Ctrs_Pnd_Ctr_Cert_Mn).add(pnd_Ctrs_Pnd_Ctr_Cert_Mt).add(pnd_Ctrs_Pnd_Ctr_Cert_Ne).add(pnd_Ctrs_Pnd_Ctr_Cert_Va).add(pnd_Ctrs_Pnd_Ctr_Cert_Fl).add(pnd_Ctrs_Pnd_Ctr_Cert_Nh).add(pnd_Ctrs_Pnd_Ctr_Cert_Or).add(pnd_Ctrs_Pnd_Ctr_Cert_Nm).add(pnd_Ctrs_Pnd_Ctr_Cert_Ny).add(pnd_Ctrs_Pnd_Ctr_Cert_Pr)); //Natural: ASSIGN #CTR-TOTAL-CONTRACTS := #CTRS.#CTR-CERT-GN + #CTRS.#CTR-CERT-CA + #CTRS.#CTR-CERT-OK + #CTRS.#CTR-CERT-NC + #CTRS.#CTR-CERT-LA + #CTRS.#CTR-CERT-MN + #CTRS.#CTR-CERT-MT + #CTRS.#CTR-CERT-NE + #CTRS.#CTR-CERT-VA + #CTRS.#CTR-CERT-FL + #CTRS.#CTR-CERT-NH + #CTRS.#CTR-CERT-OR + #CTRS.#CTR-CERT-NM + #CTRS.#CTR-CERT-NY + #CTRS.#CTR-CERT-PR
            if (condition(pnd_Ctr_Total_Contracts.greater(getZero())))                                                                                                    //Natural: IF #CTR-TOTAL-CONTRACTS GT 0
            {
                if (condition(pnd_Ctrs_Pnd_Ctr_Oregon.greater(getZero())))                                                                                                //Natural: IF #CTRS.#CTR-OREGON > 0
                {
                    pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_A.reset();                                                                                                                   //Natural: RESET #CTRS.#CTR-TIAA-RA-A #CTRS.#CTR-TIAA-GRA-RC-A #CTRS.#CTR-TIAA-RA-NH-A #CTRS.#CTR-TIAA-GRA-RC-NH-A #CTRS.#CTR-CREF-A #CTRS.#CTR-CREF-IL-A #CTRS.#CTR-CREF-RA-FL-A #CTRS.#CTR-CREF-GRA-RC-FL-A #OUTPUT.#CONTRACTS ( * )
                    pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_A.reset();
                    pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_Nh_A.reset();
                    pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A.reset();
                    pnd_Ctrs_Pnd_Ctr_Cref_A.reset();
                    pnd_Ctrs_Pnd_Ctr_Cref_Il_A.reset();
                    pnd_Ctrs_Pnd_Ctr_Cref_Ra_Fl_A.reset();
                    pnd_Ctrs_Pnd_Ctr_Cref_Gra_Rc_Fl_A.reset();
                    pnd_Output_Pnd_Contracts.getValue("*").reset();
                }                                                                                                                                                         //Natural: END-IF
                //*  PINEXP
                pnd_Output_Pnd_Pin_A12.setValue(pnd_Prev_Pin_A12);                                                                                                        //Natural: MOVE #PREV-PIN-A12 TO #OUTPUT.#PIN-A12
                pnd_Output_Pnd_Ctr_Tiaa_Ra_A.setValue(pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_A);                                                                                        //Natural: MOVE #CTRS.#CTR-TIAA-RA-A TO #OUTPUT.#CTR-TIAA-RA-A
                pnd_Output_Pnd_Ctr_Tiaa_Gra_Rc_A.setValue(pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_A);                                                                                //Natural: MOVE #CTRS.#CTR-TIAA-GRA-RC-A TO #OUTPUT.#CTR-TIAA-GRA-RC-A
                pnd_Output_Pnd_Ctr_Tiaa_Ra_Nh_A.setValue(pnd_Ctrs_Pnd_Ctr_Tiaa_Ra_Nh_A);                                                                                  //Natural: MOVE #CTRS.#CTR-TIAA-RA-NH-A TO #OUTPUT.#CTR-TIAA-RA-NH-A
                pnd_Output_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A.setValue(pnd_Ctrs_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A);                                                                          //Natural: MOVE #CTRS.#CTR-TIAA-GRA-RC-NH-A TO #OUTPUT.#CTR-TIAA-GRA-RC-NH-A
                pnd_Output_Pnd_Ctr_Cref_A.setValue(pnd_Ctrs_Pnd_Ctr_Cref_A);                                                                                              //Natural: MOVE #CTRS.#CTR-CREF-A TO #OUTPUT.#CTR-CREF-A
                pnd_Output_Pnd_Ctr_Cref_Il_A.setValue(pnd_Ctrs_Pnd_Ctr_Cref_Il_A);                                                                                        //Natural: MOVE #CTRS.#CTR-CREF-IL-A TO #OUTPUT.#CTR-CREF-IL-A
                pnd_Output_Pnd_Ctr_Cref_Ra_Fl_A.setValue(pnd_Ctrs_Pnd_Ctr_Cref_Ra_Fl_A);                                                                                  //Natural: MOVE #CTRS.#CTR-CREF-RA-FL-A TO #OUTPUT.#CTR-CREF-RA-FL-A
                pnd_Output_Pnd_Ctr_Cref_Gra_Rc_Fl_A.setValue(pnd_Ctrs_Pnd_Ctr_Cref_Gra_Rc_Fl_A);                                                                          //Natural: MOVE #CTRS.#CTR-CREF-GRA-RC-FL-A TO #OUTPUT.#CTR-CREF-GRA-RC-FL-A
                pnd_Output_Pnd_Ctr_Cert_Gn.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Gn);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-GN TO #OUTPUT.#CTR-CERT-GN
                pnd_Output_Pnd_Ctr_Cert_Ca.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Ca);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-CA TO #OUTPUT.#CTR-CERT-CA
                pnd_Output_Pnd_Ctr_Cert_Ok.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Ok);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-OK TO #OUTPUT.#CTR-CERT-OK
                pnd_Output_Pnd_Ctr_Cert_Nc.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Nc);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-NC TO #OUTPUT.#CTR-CERT-NC
                pnd_Output_Pnd_Ctr_Cert_La.setValue(pnd_Ctrs_Pnd_Ctr_Cert_La);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-LA TO #OUTPUT.#CTR-CERT-LA
                pnd_Output_Pnd_Ctr_Cert_Mn.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Mn);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-MN TO #OUTPUT.#CTR-CERT-MN
                pnd_Output_Pnd_Ctr_Cert_Mt.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Mt);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-MT TO #OUTPUT.#CTR-CERT-MT
                pnd_Output_Pnd_Ctr_Cert_Ne.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Ne);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-NE TO #OUTPUT.#CTR-CERT-NE
                pnd_Output_Pnd_Ctr_Cert_Va.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Va);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-VA TO #OUTPUT.#CTR-CERT-VA
                pnd_Output_Pnd_Ctr_Cert_Fl.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Fl);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-FL TO #OUTPUT.#CTR-CERT-FL
                pnd_Output_Pnd_Ctr_Cert_Nh.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Nh);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-NH TO #OUTPUT.#CTR-CERT-NH
                pnd_Output_Pnd_Ctr_Oregon.setValue(pnd_Ctrs_Pnd_Ctr_Oregon);                                                                                              //Natural: MOVE #CTRS.#CTR-OREGON TO #OUTPUT.#CTR-OREGON
                //*  LS1
                pnd_Output_Pnd_Ctr_Cert_Or.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Or);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-OR TO #OUTPUT.#CTR-CERT-OR
                //*  LS1
                pnd_Output_Pnd_Ctr_Cert_Nm.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Nm);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-NM TO #OUTPUT.#CTR-CERT-NM
                //*  LS1
                pnd_Output_Pnd_Ctr_Cert_Ny.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Ny);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-NY TO #OUTPUT.#CTR-CERT-NY
                //*  LS1
                pnd_Output_Pnd_Ctr_Cert_Pr.setValue(pnd_Ctrs_Pnd_Ctr_Cert_Pr);                                                                                            //Natural: MOVE #CTRS.#CTR-CERT-PR TO #OUTPUT.#CTR-CERT-PR
                pnd_Output_Pnd_Inst_Sv_Lob.setValue(pnd_Prev_Inst_Sv_Lob);                                                                                                //Natural: MOVE #PREV-INST-SV-LOB TO #OUTPUT.#INST-SV-LOB
                pnd_Output_Pnd_Institution_Name.setValue(pnd_Prev_Inst_Name);                                                                                             //Natural: MOVE #PREV-INST-NAME TO #OUTPUT.#INSTITUTION-NAME
                //*      MOVE #INPUT-RECORD.#LETTER-TYPE  TO #OUTPUT.#LETTER-TYPE  /* LS1
                //*  LS1
                pnd_Output_Pnd_Letter_Type.setValue(pnd_Prev_Letter_Type);                                                                                                //Natural: MOVE #PREV-LETTER-TYPE TO #OUTPUT.#LETTER-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-DATA-TO-FILE
                sub_Write_Data_To_File();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ctrs.reset();                                                                                                                                             //Natural: RESET #CTRS #CTR-TOTAL-CONTRACTS
            pnd_Ctr_Total_Contracts.reset();
            pnd_Output.reset();                                                                                                                                           //Natural: RESET #OUTPUT #K
            pnd_K.reset();
            pnd_Break.setValue(true);                                                                                                                                     //Natural: MOVE TRUE TO #BREAK
            //*  PINEXP
            pnd_Prev_Pin_A12.setValue(pnd_Input_Record_Pnd_Pin_A12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-A12 TO #PREV-PIN-A12
            pnd_Prev_Inst_Name.setValue(pnd_Input_Record_Pnd_Inst_Name);                                                                                                  //Natural: MOVE #INPUT-RECORD.#INST-NAME TO #PREV-INST-NAME
            pnd_Prev_Inst_Sv_Lob.setValue(pnd_Input_Record_Pnd_Inst_Sv_Lob);                                                                                              //Natural: MOVE #INPUT-RECORD.#INST-SV-LOB TO #PREV-INST-SV-LOB
            //*  LS1
            pnd_Prev_Letter_Type.setValue(pnd_Input_Record_Pnd_Letter_Type);                                                                                              //Natural: MOVE #INPUT-RECORD.#LETTER-TYPE TO #PREV-LETTER-TYPE
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=120 PS=60");
        Global.format(0, "LS=80 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(34),"TIAA Stable Value RIDER MAILING",new 
            TabSetting(84),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(33),"LIST OF INSTITUTION-OWNED CONTRACTS",new TabSetting(84),"TIME:",Global.getTIME(), 
            new ReportEditMask ("XXXXXXXX"),NEWLINE,new TabSetting(84),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new 
            TabSetting(2),"PIN         ",new TabSetting(16),"PARTICIPANT NAME   ",new TabSetting(53),"TIAA #  ",new TabSetting(63),"LOB  ",new TabSetting(70),"CUTOFF",new 
            TabSetting(78),"PPG/PLAN",NEWLINE,new TabSetting(2),"------------",new TabSetting(16),"-----------------------------------",new TabSetting(53),"--------",new 
            TabSetting(63),"-----",new TabSetting(70),"------",new TabSetting(78),"--------",NEWLINE,NEWLINE);
    }
}
