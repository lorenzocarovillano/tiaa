/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:16:52 PM
**        * FROM NATURAL PROGRAM : Pccb2520
************************************************************
**        * FILE NAME            : Pccb2520.java
**        * CLASS NAME           : Pccb2520
**        * INSTANCE NAME        : Pccb2520
************************************************************
************************************************************************
* PROGRAM  : PCCB2520
* SYSTEM   : POST FOR TOPS
* TITLE    : PRE-PROCESSOR FOR NET CHANGE DATA FILES
* FUNCTION : THIS MODULE WILL BUILD MI RECORDS FOR EACH REQUEST, CONVERT
*          : VARIABLE RECORDS TO FIXED LENGTH, WRITE A RECORD TO FILE
*          : FOR NDM COMMANDS BUILDER AND CUSTOMIZED THE DATADICT FILE
*          : TO THE APPLICATION DATA BEING PROCESSED.
*
*
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
* J. CRUZ  AUG 2014 INITIAL IMPLEMENTATION
* 06/02/2017   B.NEWSOM    PIN EXPANSION PROJECT -  PINEXP
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Pccb2520 extends BLNatBase
{
    // Data Areas
    private LdaPstl300x ldaPstl300x;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_600;

    private DbsGroup pnd_Input_600__R_Field_1;
    private DbsField pnd_Input_600_Pnd_In_Record_Type;
    private DbsField pnd_Input_600_Pnd_In_Batch;

    private DbsGroup pnd_Input_600__R_Field_2;
    private DbsField pnd_Input_600_Pnd_In600_A283;
    private DbsField pnd_Output_800;

    private DbsGroup pnd_Output_800__R_Field_3;
    private DbsField pnd_Output_800_Mi_Record_Id;
    private DbsField pnd_Output_800_Mi_Pin_Number;
    private DbsField pnd_Output_800_Mi_Ph_Postnet_Barcode;
    private DbsField pnd_Output_800_Mi_Ph_Full_Name;
    private DbsField pnd_Output_800_Mi_Ph_Address_Line;

    private DbsGroup pnd_Output_800__R_Field_4;
    private DbsField pnd_Output_800_Mi_Ph_Address_Line1;
    private DbsField pnd_Output_800_Mi_Ph_Address_Line2;
    private DbsField pnd_Output_800_Mi_Ph_Address_Line3;
    private DbsField pnd_Output_800_Mi_Ph_Address_Line4;
    private DbsField pnd_Output_800_Mi_Ph_Address_Line5;
    private DbsField pnd_Output_800_Mi_Ph_Address_Line6;
    private DbsField pnd_Output_800_Mi_Work_Prcss_Id;
    private DbsField pnd_Output_800_Mi_Image_Id;
    private DbsField pnd_Output_800_Mi_Rqst_Id;
    private DbsField pnd_Output_800_Mi_Environment;
    private DbsField pnd_Output_800_Mi_Mach_Func_2;
    private DbsField pnd_Output_800_Mi_Mach_Func_3;
    private DbsField pnd_Output_800_Mi_Mach_Func_4;
    private DbsField pnd_Output_800_Mi_Mach_Func_5;
    private DbsField pnd_Output_800_Mi_Package_Id;
    private DbsField pnd_Output_800_Mi_Document_Count;
    private DbsField pnd_Output_800_Mi_Document_Bad_Ind;
    private DbsField pnd_Output_800_Mi_Finish_Profile;
    private DbsField pnd_Output_800_Mi_Postnet_Zip_Full;
    private DbsField pnd_Output_800_Mi_Package_Delivery_Type;
    private DbsField pnd_Output_800_Mi_Image_Source_Id;
    private DbsField pnd_Output_800_Mi_Addrss_Typ_Cde;
    private DbsField pnd_Output_800_Mi_Approved_Ind;
    private DbsField pnd_Output_800_Mi_Alt_Dlvry_Addr;
    private DbsField pnd_Input_300;

    private DbsGroup pnd_Input_300__R_Field_5;
    private DbsField pnd_Input_300_Pnd_In_Package_Id;
    private DbsField pnd_App_Id;
    private DbsField pnd_Datadict_Id;
    private DbsField pnd_Profile_Num;
    private DbsField pnd_Record_Cnt;
    private DbsField pnd_Mi_Cnt;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaPstl300x = new LdaPstl300x();
        registerRecord(ldaPstl300x);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Input_600 = localVariables.newFieldInRecord("pnd_Input_600", "#INPUT-600", FieldType.STRING, 600);

        pnd_Input_600__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_600__R_Field_1", "REDEFINE", pnd_Input_600);
        pnd_Input_600_Pnd_In_Record_Type = pnd_Input_600__R_Field_1.newFieldInGroup("pnd_Input_600_Pnd_In_Record_Type", "#IN-RECORD-TYPE", FieldType.STRING, 
            2);
        pnd_Input_600_Pnd_In_Batch = pnd_Input_600__R_Field_1.newFieldInGroup("pnd_Input_600_Pnd_In_Batch", "#IN-BATCH", FieldType.STRING, 8);

        pnd_Input_600__R_Field_2 = localVariables.newGroupInRecord("pnd_Input_600__R_Field_2", "REDEFINE", pnd_Input_600);
        pnd_Input_600_Pnd_In600_A283 = pnd_Input_600__R_Field_2.newFieldInGroup("pnd_Input_600_Pnd_In600_A283", "#IN600-A283", FieldType.STRING, 283);
        pnd_Output_800 = localVariables.newFieldInRecord("pnd_Output_800", "#OUTPUT-800", FieldType.STRING, 800);

        pnd_Output_800__R_Field_3 = localVariables.newGroupInRecord("pnd_Output_800__R_Field_3", "REDEFINE", pnd_Output_800);
        pnd_Output_800_Mi_Record_Id = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Record_Id", "MI-RECORD-ID", FieldType.STRING, 2);
        pnd_Output_800_Mi_Pin_Number = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Pin_Number", "MI-PIN-NUMBER", FieldType.STRING, 12);
        pnd_Output_800_Mi_Ph_Postnet_Barcode = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Ph_Postnet_Barcode", "MI-PH-POSTNET-BARCODE", 
            FieldType.STRING, 12);
        pnd_Output_800_Mi_Ph_Full_Name = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Ph_Full_Name", "MI-PH-FULL-NAME", FieldType.STRING, 
            35);
        pnd_Output_800_Mi_Ph_Address_Line = pnd_Output_800__R_Field_3.newFieldArrayInGroup("pnd_Output_800_Mi_Ph_Address_Line", "MI-PH-ADDRESS-LINE", 
            FieldType.STRING, 35, new DbsArrayController(1, 6));

        pnd_Output_800__R_Field_4 = pnd_Output_800__R_Field_3.newGroupInGroup("pnd_Output_800__R_Field_4", "REDEFINE", pnd_Output_800_Mi_Ph_Address_Line);
        pnd_Output_800_Mi_Ph_Address_Line1 = pnd_Output_800__R_Field_4.newFieldInGroup("pnd_Output_800_Mi_Ph_Address_Line1", "MI-PH-ADDRESS-LINE1", FieldType.STRING, 
            35);
        pnd_Output_800_Mi_Ph_Address_Line2 = pnd_Output_800__R_Field_4.newFieldInGroup("pnd_Output_800_Mi_Ph_Address_Line2", "MI-PH-ADDRESS-LINE2", FieldType.STRING, 
            35);
        pnd_Output_800_Mi_Ph_Address_Line3 = pnd_Output_800__R_Field_4.newFieldInGroup("pnd_Output_800_Mi_Ph_Address_Line3", "MI-PH-ADDRESS-LINE3", FieldType.STRING, 
            35);
        pnd_Output_800_Mi_Ph_Address_Line4 = pnd_Output_800__R_Field_4.newFieldInGroup("pnd_Output_800_Mi_Ph_Address_Line4", "MI-PH-ADDRESS-LINE4", FieldType.STRING, 
            35);
        pnd_Output_800_Mi_Ph_Address_Line5 = pnd_Output_800__R_Field_4.newFieldInGroup("pnd_Output_800_Mi_Ph_Address_Line5", "MI-PH-ADDRESS-LINE5", FieldType.STRING, 
            35);
        pnd_Output_800_Mi_Ph_Address_Line6 = pnd_Output_800__R_Field_4.newFieldInGroup("pnd_Output_800_Mi_Ph_Address_Line6", "MI-PH-ADDRESS-LINE6", FieldType.STRING, 
            35);
        pnd_Output_800_Mi_Work_Prcss_Id = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Work_Prcss_Id", "MI-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        pnd_Output_800_Mi_Image_Id = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Image_Id", "MI-IMAGE-ID", FieldType.STRING, 11);
        pnd_Output_800_Mi_Rqst_Id = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Rqst_Id", "MI-RQST-ID", FieldType.STRING, 11);
        pnd_Output_800_Mi_Environment = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Environment", "MI-ENVIRONMENT", FieldType.STRING, 
            4);
        pnd_Output_800_Mi_Mach_Func_2 = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Mach_Func_2", "MI-MACH-FUNC-2", FieldType.STRING, 
            1);
        pnd_Output_800_Mi_Mach_Func_3 = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Mach_Func_3", "MI-MACH-FUNC-3", FieldType.STRING, 
            1);
        pnd_Output_800_Mi_Mach_Func_4 = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Mach_Func_4", "MI-MACH-FUNC-4", FieldType.STRING, 
            1);
        pnd_Output_800_Mi_Mach_Func_5 = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Mach_Func_5", "MI-MACH-FUNC-5", FieldType.STRING, 
            1);
        pnd_Output_800_Mi_Package_Id = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Package_Id", "MI-PACKAGE-ID", FieldType.STRING, 12);
        pnd_Output_800_Mi_Document_Count = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Document_Count", "MI-DOCUMENT-COUNT", FieldType.NUMERIC, 
            3);
        pnd_Output_800_Mi_Document_Bad_Ind = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Document_Bad_Ind", "MI-DOCUMENT-BAD-IND", FieldType.STRING, 
            1);
        pnd_Output_800_Mi_Finish_Profile = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Finish_Profile", "MI-FINISH-PROFILE", FieldType.STRING, 
            2);
        pnd_Output_800_Mi_Postnet_Zip_Full = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Postnet_Zip_Full", "MI-POSTNET-ZIP-FULL", FieldType.STRING, 
            14);
        pnd_Output_800_Mi_Package_Delivery_Type = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Package_Delivery_Type", "MI-PACKAGE-DELIVERY-TYPE", 
            FieldType.STRING, 3);
        pnd_Output_800_Mi_Image_Source_Id = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Image_Source_Id", "MI-IMAGE-SOURCE-ID", FieldType.STRING, 
            6);
        pnd_Output_800_Mi_Addrss_Typ_Cde = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Addrss_Typ_Cde", "MI-ADDRSS-TYP-CDE", FieldType.STRING, 
            1);
        pnd_Output_800_Mi_Approved_Ind = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Approved_Ind", "MI-APPROVED-IND", FieldType.STRING, 
            1);
        pnd_Output_800_Mi_Alt_Dlvry_Addr = pnd_Output_800__R_Field_3.newFieldInGroup("pnd_Output_800_Mi_Alt_Dlvry_Addr", "MI-ALT-DLVRY-ADDR", FieldType.STRING, 
            100);
        pnd_Input_300 = localVariables.newFieldInRecord("pnd_Input_300", "#INPUT-300", FieldType.STRING, 300);

        pnd_Input_300__R_Field_5 = localVariables.newGroupInRecord("pnd_Input_300__R_Field_5", "REDEFINE", pnd_Input_300);
        pnd_Input_300_Pnd_In_Package_Id = pnd_Input_300__R_Field_5.newFieldInGroup("pnd_Input_300_Pnd_In_Package_Id", "#IN-PACKAGE-ID", FieldType.STRING, 
            8);
        pnd_App_Id = localVariables.newFieldInRecord("pnd_App_Id", "#APP-ID", FieldType.STRING, 8);
        pnd_Datadict_Id = localVariables.newFieldInRecord("pnd_Datadict_Id", "#DATADICT-ID", FieldType.STRING, 8);
        pnd_Profile_Num = localVariables.newFieldInRecord("pnd_Profile_Num", "#PROFILE-NUM", FieldType.STRING, 2);
        pnd_Record_Cnt = localVariables.newFieldInRecord("pnd_Record_Cnt", "#RECORD-CNT", FieldType.NUMERIC, 10);
        pnd_Mi_Cnt = localVariables.newFieldInRecord("pnd_Mi_Cnt", "#MI-CNT", FieldType.PACKED_DECIMAL, 7);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaPstl300x.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Pccb2520() throws Exception
    {
        super("Pccb2520");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Pccb2520|Main");
        setupReports();
        while(true)
        {
            try
            {
                //* ***********************************************************************
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 133
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_App_Id);                                                                                           //Natural: INPUT #APP-ID
                DbsUtil.invokeInput(setInputStatus(INPUT_2), this, pnd_Datadict_Id);                                                                                      //Natural: INPUT #DATADICT-ID
                DbsUtil.invokeInput(setInputStatus(INPUT_3), this, pnd_Profile_Num);                                                                                      //Natural: INPUT #PROFILE-NUM
                READWORK01:                                                                                                                                               //Natural: READ WORK 01 #INPUT-600
                while (condition(getWorkFiles().read(1, pnd_Input_600)))
                {
                    pnd_Record_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #RECORD-CNT
                    if (condition(pnd_Input_600_Pnd_In_Record_Type.equals("01")))                                                                                         //Natural: IF #IN-RECORD-TYPE = '01'
                    {
                        pnd_Input_600_Pnd_In_Batch.setValue("BTCHXXXX");                                                                                                  //Natural: ASSIGN #IN-BATCH := 'BTCHXXXX'
                        getReports().write(0, "*** CHANGING RECORD 01 BATCH TO:",pnd_Input_600_Pnd_In_Batch,"***");                                                       //Natural: WRITE '*** CHANGING RECORD 01 BATCH TO:' #IN-BATCH '***'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Input_600_Pnd_In_Record_Type.equals("02")))                                                                                         //Natural: IF #IN-RECORD-TYPE = '02'
                    {
                        pnd_Input_600_Pnd_In_Record_Type.setValue("MI");                                                                                                  //Natural: ASSIGN #IN-RECORD-TYPE := 'MI'
                        pnd_Output_800.setValue(pnd_Input_600_Pnd_In600_A283);                                                                                            //Natural: ASSIGN #OUTPUT-800 := #IN600-A283
                        if (condition(pnd_App_Id.equals("PNTLABLF") || pnd_App_Id.equals("PNTFLATF") || pnd_App_Id.equals("PNTTRIF")))                                    //Natural: IF #APP-ID = 'PNTLABLF' OR = 'PNTFLATF' OR = 'PNTTRIF'
                        {
                            pnd_Output_800_Mi_Addrss_Typ_Cde.setValue("F");                                                                                               //Natural: ASSIGN MI-ADDRSS-TYP-CDE := 'F'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Output_800_Mi_Addrss_Typ_Cde.setValue("U");                                                                                               //Natural: ASSIGN MI-ADDRSS-TYP-CDE := 'U'
                        }                                                                                                                                                 //Natural: END-IF
                        getWorkFiles().write(2, false, pnd_Output_800);                                                                                                   //Natural: WRITE WORK FILE 02 #OUTPUT-800
                        pnd_Mi_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #MI-CNT
                        pnd_Input_600_Pnd_In_Record_Type.setValue("02");                                                                                                  //Natural: ASSIGN #IN-RECORD-TYPE := '02'
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Output_800.setValue(pnd_Input_600);                                                                                                               //Natural: ASSIGN #OUTPUT-800 := #INPUT-600
                    getWorkFiles().write(2, false, pnd_Output_800);                                                                                                       //Natural: WRITE WORK FILE 02 #OUTPUT-800
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                //* *
                READWORK02:                                                                                                                                               //Natural: READ WORK 03 #INPUT-300
                while (condition(getWorkFiles().read(3, pnd_Input_300)))
                {
                    if (condition(pnd_Input_300_Pnd_In_Package_Id.notEquals(pnd_Datadict_Id)))                                                                            //Natural: IF #IN-PACKAGE-ID NE #DATADICT-ID
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Input_300_Pnd_In_Package_Id.setValue(pnd_App_Id);                                                                                                 //Natural: ASSIGN #IN-PACKAGE-ID := #APP-ID
                    getWorkFiles().write(4, false, pnd_Input_300);                                                                                                        //Natural: WRITE WORK FILE 04 #INPUT-300
                }                                                                                                                                                         //Natural: END-WORK
                READWORK02_Exit:
                if (Global.isEscape()) return;
                //* *
                if (condition(pnd_Mi_Cnt.greater(getZero())))                                                                                                             //Natural: IF #MI-CNT GT 0
                {
                    getWorkFiles().write(5, false, pnd_Profile_Num, pnd_App_Id, pnd_Record_Cnt);                                                                          //Natural: WRITE WORK FILE 05 #PROFILE-NUM #APP-ID #RECORD-CNT
                }                                                                                                                                                         //Natural: END-IF
                //* *
                getReports().write(0, NEWLINE,NEWLINE,pnd_Record_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),"RECORDS IN INPUT DATA FILE",NEWLINE,NEWLINE,pnd_Mi_Cnt,           //Natural: WRITE // #RECORD-CNT ( EM = Z,ZZZ,ZZ9 ) 'RECORDS IN INPUT DATA FILE' // #MI-CNT ( EM = Z,ZZZ,ZZ9 ) '"MI" RECORDS ADDED TO FILE'
                    new ReportEditMask ("Z,ZZZ,ZZ9"),"'MI' RECORDS ADDED TO FILE");
                if (Global.isEscape()) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=133");
    }
}
