/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:13 PM
**        * FROM NATURAL PROGRAM : Ridp632
************************************************************
**        * FILE NAME            : Ridp632.java
**        * CLASS NAME           : Ridp632
**        * INSTANCE NAME        : Ridp632
************************************************************
************************************************************************
** PROGRAM     : RIDP612                                              **
** AUTHOR      : JOANNES C. AVE                                       **
** DESCRIPTION : READS THE RIDP610 OUTPUT FILE AND ASSIGN RID#        **
************************************************************************
** HISTORY:                                                           **
** 11/04/2015  : INITIAL CODING                                       **
** 09/09/2017  : MODIFY FOR PIN EXPANSION                  PINEXP     **
** 05/03/2019  : RETIREPLUS WITH TSV MODIFICATION          CPTSV      **
**               ADDING AK, FL AND NY FOR STATE VARIATION             **
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridp632 extends BLNatBase
{
    // Data Areas
    private LdaRidl632 ldaRidl632;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ph_Data;
    private DbsField pnd_Ph_Data_Pnd_Pin_Number;
    private DbsField pnd_Ph_Data_Pnd_Full_Name;
    private DbsField pnd_Ph_Data_Pnd_Address_Line_Txt;
    private DbsField pnd_Ph_Data_Pnd_Postal_Data;
    private DbsField pnd_Ph_Data_Pnd_Address_Type_Cde;
    private DbsField pnd_Ph_Data_Pnd_Letter_Type;
    private DbsField pnd_Ph_Data_Pnd_Tsv_Ind;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Gn;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Il;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Ak;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Fl;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Ny;
    private DbsField pnd_Ph_Data_Pnd_Package_Code;
    private DbsField pnd_Ph_Data_Pnd_Tiaa_Contracts;
    private DbsField pnd_Ph_Data_Pnd_Cref_Contracts;

    private DbsGroup pnd_Ph_Data_Endorse_Contracts_Gn;
    private DbsField pnd_Ph_Data_Pnd_Tiaa_Contracts_Gn;
    private DbsField pnd_Ph_Data_Pnd_Cref_Contracts_Gn;

    private DbsGroup pnd_Ph_Data_Endorse_Contracts_Il;
    private DbsField pnd_Ph_Data_Pnd_Tiaa_Contracts_Il;
    private DbsField pnd_Ph_Data_Pnd_Cref_Contracts_Il;

    private DbsGroup pnd_Ph_Data_Pnd_Endorse_Contracts_Ak;
    private DbsField pnd_Ph_Data_Pnd_Tiaa_Contracts_Ak;
    private DbsField pnd_Ph_Data_Pnd_Cref_Contracts_Ak;

    private DbsGroup pnd_Ph_Data_Pnd_Endorse_Contracts_Fl;
    private DbsField pnd_Ph_Data_Pnd_Tiaa_Contracts_Fl;
    private DbsField pnd_Ph_Data_Pnd_Cref_Contracts_Fl;

    private DbsGroup pnd_Ph_Data_Pnd_Endorse_Contracts_Ny;
    private DbsField pnd_Ph_Data_Pnd_Tiaa_Contracts_Ny;
    private DbsField pnd_Ph_Data_Pnd_Cref_Contracts_Ny;
    private DbsField pnd_Ph_Data_Pnd_Institution_Name;
    private DbsField pnd_Ph_Data_Pnd_Email_Address;
    private DbsField pnd_Current_Date;

    private DbsGroup pnd_Current_Date__R_Field_1;
    private DbsField pnd_Current_Date_Pnd_Current_Year;
    private DbsField pnd_Current_Date_Pnd_Current_Month;
    private DbsField pnd_Current_Date_Pnd_Current_Day;
    private DbsField pnd_Rdr_Date;

    private DbsGroup pnd_Rdr_Date__R_Field_2;
    private DbsField pnd_Rdr_Date_Pnd_Rdr_Month;
    private DbsField pnd_Rdr_Date_Pnd_Rdr_Year;

    private DbsGroup counters;
    private DbsField counters_Pnd_Cnt_Record_Read;
    private DbsField counters_Pnd_Cnt_Record_Stored;
    private DbsField counters_Pnd_Cnt_Ph_Deceased;
    private DbsField pnd_Address_Blank;
    private DbsField pnd_Cor_Key;

    private DbsGroup pnd_Cor_Key__R_Field_3;
    private DbsField pnd_Cor_Key_Pin;
    private DbsField pnd_Cor_Key_Rec;
    private DbsField pnd_Blank;
    private DbsField pnd_Batch_Id_Found;
    private DbsField pnd_First_Pst_Nbr_Allocated;
    private DbsField pnd_Error_Ind;

    private DbsGroup control_Variables;
    private DbsField control_Variables_Run_Type;
    private DbsField control_Variables_Batch_Number;
    private DbsField control_Variables_Current_Pst_Rqst_Id;

    private DbsGroup error_Handler_Fields;
    private DbsField error_Handler_Fields_Pnd_Error_Nr;
    private DbsField error_Handler_Fields_Pnd_Error_Line;
    private DbsField error_Handler_Fields_Pnd_Error_Status;
    private DbsField error_Handler_Fields_Pnd_Error_Program;
    private DbsField error_Handler_Fields_Pnd_Error_Level;
    private DbsField error_Handler_Fields_Pnd_Error_Appl;
    private DbsField pnd_Msg_Parts;

    private DbsGroup pnd_Package_Variables;
    private DbsField pnd_Package_Variables_Pnd_Package_Cdes;
    private DbsField pnd_Package_Variables_Pnd_Cnt_Record_By_Package;
    private DbsField pnd_Package_Variables_Pnd_First_Pst;
    private DbsField pnd_Package_Variables_Pnd_Last_Pst;
    private DbsField pnd_Prev_Package_Code;
    private DbsField pnd_Prev_Pst;
    private DbsField pnd_K;
    private DbsField pnd_First_Record;
    private DbsField pnd_First_Bad_Record;
    private DbsField pnd_Cnt_Et;

    private DbsGroup pnd_Package_Table;
    private DbsField pnd_Package_Table_Pnd_Package_Code;
    private DbsField pnd_Package_Table_Pnd_File_Desc;
    private DbsField pnd_Ndx;
    private DbsField pnd_Rid_Ctr;
    private DbsField pnd_Rid_Nbr;

    private DbsGroup pnd_Rid_Nbr__R_Field_4;
    private DbsField pnd_Rid_Nbr_Pnd_Rid_Nbr_Prefix;
    private DbsField pnd_Rid_Nbr_Pnd_Rid_Nbr_A;
    private DbsField pnd_Deceased;
    private DbsField pnd_Rem;
    private DbsField pnd_Bad_Address;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaRidl632 = new LdaRidl632();
        registerRecord(ldaRidl632);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ph_Data = localVariables.newGroupInRecord("pnd_Ph_Data", "#PH-DATA");
        pnd_Ph_Data_Pnd_Pin_Number = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);
        pnd_Ph_Data_Pnd_Full_Name = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Ph_Data_Pnd_Address_Line_Txt = pnd_Ph_Data.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 6));
        pnd_Ph_Data_Pnd_Postal_Data = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 35);
        pnd_Ph_Data_Pnd_Address_Type_Cde = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 1);
        pnd_Ph_Data_Pnd_Letter_Type = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Ph_Data_Pnd_Tsv_Ind = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Tsv_Ind", "#TSV-IND", FieldType.STRING, 1);
        pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Gn = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Gn", "#CTR-RC-RCP-GN", FieldType.NUMERIC, 2);
        pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Il = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Il", "#CTR-RC-RCP-IL", FieldType.NUMERIC, 2);
        pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Ak = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Ak", "#CTR-RC-RCP-AK", FieldType.NUMERIC, 2);
        pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Fl = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Fl", "#CTR-RC-RCP-FL", FieldType.NUMERIC, 2);
        pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Ny = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Rc_Rcp_Ny", "#CTR-RC-RCP-NY", FieldType.NUMERIC, 2);
        pnd_Ph_Data_Pnd_Package_Code = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Ph_Data_Pnd_Tiaa_Contracts = pnd_Ph_Data.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Tiaa_Contracts", "#TIAA-CONTRACTS", FieldType.STRING, 8, new 
            DbsArrayController(1, 10));
        pnd_Ph_Data_Pnd_Cref_Contracts = pnd_Ph_Data.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Cref_Contracts", "#CREF-CONTRACTS", FieldType.STRING, 8, new 
            DbsArrayController(1, 10));

        pnd_Ph_Data_Endorse_Contracts_Gn = pnd_Ph_Data.newGroupInGroup("pnd_Ph_Data_Endorse_Contracts_Gn", "ENDORSE-CONTRACTS-GN");
        pnd_Ph_Data_Pnd_Tiaa_Contracts_Gn = pnd_Ph_Data_Endorse_Contracts_Gn.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Tiaa_Contracts_Gn", "#TIAA-CONTRACTS-GN", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ph_Data_Pnd_Cref_Contracts_Gn = pnd_Ph_Data_Endorse_Contracts_Gn.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Cref_Contracts_Gn", "#CREF-CONTRACTS-GN", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));

        pnd_Ph_Data_Endorse_Contracts_Il = pnd_Ph_Data.newGroupInGroup("pnd_Ph_Data_Endorse_Contracts_Il", "ENDORSE-CONTRACTS-IL");
        pnd_Ph_Data_Pnd_Tiaa_Contracts_Il = pnd_Ph_Data_Endorse_Contracts_Il.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Tiaa_Contracts_Il", "#TIAA-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ph_Data_Pnd_Cref_Contracts_Il = pnd_Ph_Data_Endorse_Contracts_Il.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Cref_Contracts_Il", "#CREF-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));

        pnd_Ph_Data_Pnd_Endorse_Contracts_Ak = pnd_Ph_Data.newGroupInGroup("pnd_Ph_Data_Pnd_Endorse_Contracts_Ak", "#ENDORSE-CONTRACTS-AK");
        pnd_Ph_Data_Pnd_Tiaa_Contracts_Ak = pnd_Ph_Data_Pnd_Endorse_Contracts_Ak.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Tiaa_Contracts_Ak", "#TIAA-CONTRACTS-AK", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ph_Data_Pnd_Cref_Contracts_Ak = pnd_Ph_Data_Pnd_Endorse_Contracts_Ak.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Cref_Contracts_Ak", "#CREF-CONTRACTS-AK", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));

        pnd_Ph_Data_Pnd_Endorse_Contracts_Fl = pnd_Ph_Data.newGroupInGroup("pnd_Ph_Data_Pnd_Endorse_Contracts_Fl", "#ENDORSE-CONTRACTS-FL");
        pnd_Ph_Data_Pnd_Tiaa_Contracts_Fl = pnd_Ph_Data_Pnd_Endorse_Contracts_Fl.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Tiaa_Contracts_Fl", "#TIAA-CONTRACTS-FL", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ph_Data_Pnd_Cref_Contracts_Fl = pnd_Ph_Data_Pnd_Endorse_Contracts_Fl.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Cref_Contracts_Fl", "#CREF-CONTRACTS-FL", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));

        pnd_Ph_Data_Pnd_Endorse_Contracts_Ny = pnd_Ph_Data.newGroupInGroup("pnd_Ph_Data_Pnd_Endorse_Contracts_Ny", "#ENDORSE-CONTRACTS-NY");
        pnd_Ph_Data_Pnd_Tiaa_Contracts_Ny = pnd_Ph_Data_Pnd_Endorse_Contracts_Ny.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Tiaa_Contracts_Ny", "#TIAA-CONTRACTS-NY", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ph_Data_Pnd_Cref_Contracts_Ny = pnd_Ph_Data_Pnd_Endorse_Contracts_Ny.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Cref_Contracts_Ny", "#CREF-CONTRACTS-NY", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ph_Data_Pnd_Institution_Name = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 120);
        pnd_Ph_Data_Pnd_Email_Address = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Email_Address", "#EMAIL-ADDRESS", FieldType.STRING, 100);
        pnd_Current_Date = localVariables.newFieldInRecord("pnd_Current_Date", "#CURRENT-DATE", FieldType.NUMERIC, 8);

        pnd_Current_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Current_Date__R_Field_1", "REDEFINE", pnd_Current_Date);
        pnd_Current_Date_Pnd_Current_Year = pnd_Current_Date__R_Field_1.newFieldInGroup("pnd_Current_Date_Pnd_Current_Year", "#CURRENT-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Current_Date_Pnd_Current_Month = pnd_Current_Date__R_Field_1.newFieldInGroup("pnd_Current_Date_Pnd_Current_Month", "#CURRENT-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Current_Date_Pnd_Current_Day = pnd_Current_Date__R_Field_1.newFieldInGroup("pnd_Current_Date_Pnd_Current_Day", "#CURRENT-DAY", FieldType.NUMERIC, 
            2);
        pnd_Rdr_Date = localVariables.newFieldInRecord("pnd_Rdr_Date", "#RDR-DATE", FieldType.NUMERIC, 6);

        pnd_Rdr_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Rdr_Date__R_Field_2", "REDEFINE", pnd_Rdr_Date);
        pnd_Rdr_Date_Pnd_Rdr_Month = pnd_Rdr_Date__R_Field_2.newFieldInGroup("pnd_Rdr_Date_Pnd_Rdr_Month", "#RDR-MONTH", FieldType.NUMERIC, 2);
        pnd_Rdr_Date_Pnd_Rdr_Year = pnd_Rdr_Date__R_Field_2.newFieldInGroup("pnd_Rdr_Date_Pnd_Rdr_Year", "#RDR-YEAR", FieldType.NUMERIC, 4);

        counters = localVariables.newGroupInRecord("counters", "COUNTERS");
        counters_Pnd_Cnt_Record_Read = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Read", "#CNT-RECORD-READ", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Stored = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Stored", "#CNT-RECORD-STORED", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Ph_Deceased = counters.newFieldInGroup("counters_Pnd_Cnt_Ph_Deceased", "#CNT-PH-DECEASED", FieldType.NUMERIC, 7);
        pnd_Address_Blank = localVariables.newFieldInRecord("pnd_Address_Blank", "#ADDRESS-BLANK", FieldType.BOOLEAN, 1);
        pnd_Cor_Key = localVariables.newFieldInRecord("pnd_Cor_Key", "#COR-KEY", FieldType.BINARY, 9);

        pnd_Cor_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Cor_Key__R_Field_3", "REDEFINE", pnd_Cor_Key);
        pnd_Cor_Key_Pin = pnd_Cor_Key__R_Field_3.newFieldInGroup("pnd_Cor_Key_Pin", "PIN", FieldType.NUMERIC, 7);
        pnd_Cor_Key_Rec = pnd_Cor_Key__R_Field_3.newFieldInGroup("pnd_Cor_Key_Rec", "REC", FieldType.NUMERIC, 2);
        pnd_Blank = localVariables.newFieldInRecord("pnd_Blank", "#BLANK", FieldType.STRING, 1);
        pnd_Batch_Id_Found = localVariables.newFieldInRecord("pnd_Batch_Id_Found", "#BATCH-ID-FOUND", FieldType.BOOLEAN, 1);
        pnd_First_Pst_Nbr_Allocated = localVariables.newFieldInRecord("pnd_First_Pst_Nbr_Allocated", "#FIRST-PST-NBR-ALLOCATED", FieldType.BOOLEAN, 1);
        pnd_Error_Ind = localVariables.newFieldInRecord("pnd_Error_Ind", "#ERROR-IND", FieldType.STRING, 1);

        control_Variables = localVariables.newGroupInRecord("control_Variables", "CONTROL-VARIABLES");
        control_Variables_Run_Type = control_Variables.newFieldInGroup("control_Variables_Run_Type", "RUN-TYPE", FieldType.PACKED_DECIMAL, 1);
        control_Variables_Batch_Number = control_Variables.newFieldInGroup("control_Variables_Batch_Number", "BATCH-NUMBER", FieldType.STRING, 11);
        control_Variables_Current_Pst_Rqst_Id = control_Variables.newFieldInGroup("control_Variables_Current_Pst_Rqst_Id", "CURRENT-PST-RQST-ID", FieldType.STRING, 
            11);

        error_Handler_Fields = localVariables.newGroupInRecord("error_Handler_Fields", "ERROR-HANDLER-FIELDS");
        error_Handler_Fields_Pnd_Error_Nr = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Nr", "#ERROR-NR", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Line = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Line", "#ERROR-LINE", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Status = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Status", "#ERROR-STATUS", FieldType.STRING, 
            1);
        error_Handler_Fields_Pnd_Error_Program = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Program", "#ERROR-PROGRAM", FieldType.STRING, 
            8);
        error_Handler_Fields_Pnd_Error_Level = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Level", "#ERROR-LEVEL", FieldType.NUMERIC, 
            2);
        error_Handler_Fields_Pnd_Error_Appl = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Appl", "#ERROR-APPL", FieldType.STRING, 
            8);
        pnd_Msg_Parts = localVariables.newFieldArrayInRecord("pnd_Msg_Parts", "#MSG-PARTS", FieldType.STRING, 80, new DbsArrayController(1, 10));

        pnd_Package_Variables = localVariables.newGroupInRecord("pnd_Package_Variables", "#PACKAGE-VARIABLES");
        pnd_Package_Variables_Pnd_Package_Cdes = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_Package_Cdes", "#PACKAGE-CDES", FieldType.STRING, 
            8);
        pnd_Package_Variables_Pnd_Cnt_Record_By_Package = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_Cnt_Record_By_Package", "#CNT-RECORD-BY-PACKAGE", 
            FieldType.NUMERIC, 6);
        pnd_Package_Variables_Pnd_First_Pst = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_First_Pst", "#FIRST-PST", FieldType.STRING, 
            11);
        pnd_Package_Variables_Pnd_Last_Pst = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_Last_Pst", "#LAST-PST", FieldType.STRING, 
            11);
        pnd_Prev_Package_Code = localVariables.newFieldInRecord("pnd_Prev_Package_Code", "#PREV-PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Prev_Pst = localVariables.newFieldInRecord("pnd_Prev_Pst", "#PREV-PST", FieldType.STRING, 11);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_First_Record = localVariables.newFieldInRecord("pnd_First_Record", "#FIRST-RECORD", FieldType.BOOLEAN, 1);
        pnd_First_Bad_Record = localVariables.newFieldInRecord("pnd_First_Bad_Record", "#FIRST-BAD-RECORD", FieldType.BOOLEAN, 1);
        pnd_Cnt_Et = localVariables.newFieldInRecord("pnd_Cnt_Et", "#CNT-ET", FieldType.NUMERIC, 3);

        pnd_Package_Table = localVariables.newGroupArrayInRecord("pnd_Package_Table", "#PACKAGE-TABLE", new DbsArrayController(1, 2));
        pnd_Package_Table_Pnd_Package_Code = pnd_Package_Table.newFieldInGroup("pnd_Package_Table_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 
            8);
        pnd_Package_Table_Pnd_File_Desc = pnd_Package_Table.newFieldInGroup("pnd_Package_Table_Pnd_File_Desc", "#FILE-DESC", FieldType.STRING, 60);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 2);
        pnd_Rid_Ctr = localVariables.newFieldInRecord("pnd_Rid_Ctr", "#RID-CTR", FieldType.NUMERIC, 8);
        pnd_Rid_Nbr = localVariables.newFieldInRecord("pnd_Rid_Nbr", "#RID-NBR", FieldType.NUMERIC, 9);

        pnd_Rid_Nbr__R_Field_4 = localVariables.newGroupInRecord("pnd_Rid_Nbr__R_Field_4", "REDEFINE", pnd_Rid_Nbr);
        pnd_Rid_Nbr_Pnd_Rid_Nbr_Prefix = pnd_Rid_Nbr__R_Field_4.newFieldInGroup("pnd_Rid_Nbr_Pnd_Rid_Nbr_Prefix", "#RID-NBR-PREFIX", FieldType.STRING, 
            1);
        pnd_Rid_Nbr_Pnd_Rid_Nbr_A = pnd_Rid_Nbr__R_Field_4.newFieldInGroup("pnd_Rid_Nbr_Pnd_Rid_Nbr_A", "#RID-NBR-A", FieldType.STRING, 8);
        pnd_Deceased = localVariables.newFieldInRecord("pnd_Deceased", "#DECEASED", FieldType.BOOLEAN, 1);
        pnd_Rem = localVariables.newFieldInRecord("pnd_Rem", "#REM", FieldType.STRING, 2);
        pnd_Bad_Address = localVariables.newFieldInRecord("pnd_Bad_Address", "#BAD-ADDRESS", FieldType.BOOLEAN, 1);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaRidl632.initializeValues();

        localVariables.reset();
        pnd_Blank.setInitialValue(" ");
        pnd_Error_Ind.setInitialValue("E");
        error_Handler_Fields_Pnd_Error_Status.setInitialValue("O");
        error_Handler_Fields_Pnd_Error_Program.setInitialValue("PROGRAM");
        error_Handler_Fields_Pnd_Error_Appl.setInitialValue("RIDER");
        pnd_K.setInitialValue(1);
        pnd_First_Record.setInitialValue(true);
        pnd_First_Bad_Record.setInitialValue(true);
        pnd_Cnt_Et.setInitialValue(0);
        pnd_Package_Table_Pnd_Package_Code.getValue(1).setInitialValue("RDR2016A");
        pnd_Package_Table_Pnd_Package_Code.getValue(2).setInitialValue("RDR2016B");
        pnd_Package_Table_Pnd_File_Desc.getValue(1).setInitialValue("PAPER");
        pnd_Package_Table_Pnd_File_Desc.getValue(2).setInitialValue("EMAIL");
        pnd_Bad_Address.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridp632() throws Exception
    {
        super("Ridp632");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDP632", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT LS = 132 PS = 60
        //*  ----------------------------------------------------------------------
        //*                      E R R O R    H A N D L I N G
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  GET RIDER DATE
        pnd_Current_Date.setValue(Global.getDATN());                                                                                                                      //Natural: ASSIGN #CURRENT-DATE = *DATN
        pnd_Rdr_Date_Pnd_Rdr_Year.setValue(pnd_Current_Date_Pnd_Current_Year);                                                                                            //Natural: ASSIGN #RDR-YEAR = #CURRENT-YEAR
        pnd_Rdr_Date_Pnd_Rdr_Month.setValue(pnd_Current_Date_Pnd_Current_Month);                                                                                          //Natural: ASSIGN #RDR-MONTH = #CURRENT-MONTH
        //*  WRITE REPORT HEADINGS
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 44T '2015 CUSTOM PORTFOLIO RIDER PROCESSING STATS' 118T 'DATE:' *DATU / 118T 'TIME:' *TIME ( EM = XXXXXXXX ) / 118T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) /
        //*  ----------------------------------------------------------------------
        //*                           M A I N    L O O P
        //*  ----------------------------------------------------------------------
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #PH-DATA
        while (condition(getWorkFiles().read(1, pnd_Ph_Data)))
        {
            counters_Pnd_Cnt_Record_Read.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CNT-RECORD-READ
            ldaRidl632.getPnd_Post_Data().reset();                                                                                                                        //Natural: RESET #POST-DATA
            ldaRidl632.getPnd_Post_Data().setValuesByName(pnd_Ph_Data);                                                                                                   //Natural: MOVE BY NAME #PH-DATA TO #POST-DATA
                                                                                                                                                                          //Natural: PERFORM GET-PST-NUMBER
            sub_Get_Pst_Number();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* **   PERFORM DISPLAY-POST-DATA
            if (condition(pnd_First_Record.getBoolean()))                                                                                                                 //Natural: IF #FIRST-RECORD
            {
                pnd_Prev_Package_Code.setValue(ldaRidl632.getPnd_Post_Data_Pnd_Package_Code());                                                                           //Natural: ASSIGN #PREV-PACKAGE-CODE := #POST-DATA.#PACKAGE-CODE
                pnd_First_Record.setValue(false);                                                                                                                         //Natural: ASSIGN #FIRST-RECORD := FALSE
                pnd_Package_Variables_Pnd_First_Pst.setValue(ldaRidl632.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                              //Natural: ASSIGN #FIRST-PST := #POST-DATA.#PST-RQST-ID
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Prev_Package_Code.equals(ldaRidl632.getPnd_Post_Data_Pnd_Package_Code())))                                                                  //Natural: IF #PREV-PACKAGE-CODE EQ #POST-DATA.#PACKAGE-CODE
            {
                pnd_Prev_Pst.setValue(ldaRidl632.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                                                     //Natural: ASSIGN #PREV-PST := #POST-DATA.#PST-RQST-ID
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Package_Variables_Pnd_Last_Pst.setValue(pnd_Prev_Pst);                                                                                                //Natural: ASSIGN #LAST-PST := #PREV-PST
                pnd_Package_Variables_Pnd_First_Pst.setValue(ldaRidl632.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                              //Natural: ASSIGN #FIRST-PST := #POST-DATA.#PST-RQST-ID
                pnd_Prev_Package_Code.setValue(ldaRidl632.getPnd_Post_Data_Pnd_Package_Code());                                                                           //Natural: ASSIGN #PREV-PACKAGE-CODE := #POST-DATA.#PACKAGE-CODE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM STORE-POST-DATA
            sub_Store_Post_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*    END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  END TRANSACTION
        pnd_Package_Variables_Pnd_Last_Pst.setValue(pnd_Prev_Pst);                                                                                                        //Natural: ASSIGN #LAST-PST := #PREV-PST
                                                                                                                                                                          //Natural: PERFORM PRINT-PROCESSING-REPORT
        sub_Print_Processing_Report();
        if (condition(Global.isEscape())) {return;}
        //*  ----------------------------------------------------------------------
        //*                   E N D    O F    P R O C E S S I N G
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*                 S T A R T    O F   S U B R O U T I N E S
        //*  ----------------------------------------------------------------------
        //*  --------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PST-NUMBER
        //*  --------------------------------
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-POST-DATA
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-PROCESSING-REPORT
        //*  -----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-POST-DATA
        //*    WRITE (0) / '=' #POST-DATA.#FULL-NAME
        //*    WRITE (0) / '=' #POST-DATA.#ADDRESS-LINE-TXT(1)
        //*    WRITE (0) / '=' #POST-DATA.#ADDRESS-LINE-TXT(2)
        //*    WRITE (0) / '=' #POST-DATA.#ADDRESS-LINE-TXT(3)
        //*    WRITE (0) / '=' #POST-DATA.#ADDRESS-LINE-TXT(4)
        //*    WRITE (0) / '=' #POST-DATA.#ADDRESS-LINE-TXT(5)
        //*    WRITE (0) / '=' #POST-DATA.#ADDRESS-LINE-TXT(6)
        //*    WRITE (0) / '=' #POST-DATA.#POSTAL-DATA
        //*    WRITE (0) / '=' #POST-DATA.#ADDRESS-TYPE-CDE
        //*    WRITE (0) / '=' #POST-DATA.#TEST-CODE
        //*  ----------------------------------------------------------------------
        //*                    E N D    O F    S U B R O U T I N E S
        //*  ----------------------------------------------------------------------
    }
    private void sub_Get_Pst_Number() throws Exception                                                                                                                    //Natural: GET-PST-NUMBER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rid_Ctr.nadd(1);                                                                                                                                              //Natural: ASSIGN #RID-CTR := #RID-CTR + 1
        pnd_Rid_Nbr.compute(new ComputeParameters(false, pnd_Rid_Nbr), DbsField.add(100000000,pnd_Rid_Ctr));                                                              //Natural: ASSIGN #RID-NBR := 100000000 + #RID-CTR
        ldaRidl632.getPnd_Post_Data_Pnd_Pst_Rqst_Id().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "RID", pnd_Rid_Nbr_Pnd_Rid_Nbr_A));                        //Natural: COMPRESS 'RID' #RID-NBR-A TO #POST-DATA.#PST-RQST-ID LEAVING NO
        //*  GET-PST-NUMBER
    }
    private void sub_Store_Post_Data() throws Exception                                                                                                                   //Natural: STORE-POST-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        getWorkFiles().write(2, false, ldaRidl632.getPnd_Post_Data());                                                                                                    //Natural: WRITE WORK FILE 2 #POST-DATA
        ldaRidl632.getPnd_Post_Data().reset();                                                                                                                            //Natural: RESET #POST-DATA
        counters_Pnd_Cnt_Record_Stored.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CNT-RECORD-STORED
        //*  STORE-POST-DATA
    }
    private void sub_Print_Processing_Report() throws Exception                                                                                                           //Natural: PRINT-PROCESSING-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------
        getReports().write(1, NEWLINE,"NUMBER OF RECORDS READ                       :",counters_Pnd_Cnt_Record_Read, new ReportEditMask ("ZZZ,ZZ9"));                     //Natural: WRITE ( 1 ) / 'NUMBER OF RECORDS READ                       :' #CNT-RECORD-READ ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"NUMBER OF RECORDS WRITTEN FOR DCS GENERATION :",counters_Pnd_Cnt_Record_Stored, new ReportEditMask ("ZZZ,ZZ9"));                   //Natural: WRITE ( 1 ) / 'NUMBER OF RECORDS WRITTEN FOR DCS GENERATION :' #CNT-RECORD-STORED ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE);                                                                                                                           //Natural: WRITE ( 1 ) //
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(pnd_Package_Table_Pnd_Package_Code.getValue("*")), new ExamineSearch(pnd_Ph_Data_Pnd_Package_Code), new ExamineGivingIndex(pnd_K)); //Natural: EXAMINE #PACKAGE-TABLE.#PACKAGE-CODE ( * ) FOR #PH-DATA.#PACKAGE-CODE GIVING INDEX #K
        //*  WRITE '=' #K '=' #PH-DATA.#PACKAGE-CODE
        getReports().write(1, NEWLINE,"PACKAGE CODE     : ",pnd_Ph_Data_Pnd_Package_Code," - ",pnd_Package_Table_Pnd_File_Desc.getValue(pnd_K));                          //Natural: WRITE ( 1 ) / 'PACKAGE CODE     : ' #PH-DATA.#PACKAGE-CODE ' - ' #FILE-DESC ( #K )
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"FIRST PST NUMBER : ",pnd_Package_Variables_Pnd_First_Pst);                                                                         //Natural: WRITE ( 1 ) / 'FIRST PST NUMBER : ' #FIRST-PST
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"LAST  PST NUMBER : ",pnd_Package_Variables_Pnd_Last_Pst);                                                                          //Natural: WRITE ( 1 ) / 'LAST  PST NUMBER : ' #LAST-PST
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE," ",NEWLINE,NEWLINE);                                                                                                               //Natural: WRITE / ' ' //
        if (Global.isEscape()) return;
        //*  WRITE (1)   'FINISHING        : ' #BIN-CONTENT(#K) /
        //*  PRINT-PROCESSING-REPORT
    }
    private void sub_Display_Post_Data() throws Exception                                                                                                                 //Natural: DISPLAY-POST-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------------
        getReports().write(0, NEWLINE,"PST : ",ldaRidl632.getPnd_Post_Data_Pnd_Pst_Rqst_Id(),"PIN : ",ldaRidl632.getPnd_Post_Data_Pnd_Pin_Number(),"PCKG-CDE : ",         //Natural: WRITE ( 0 ) / 'PST : ' #POST-DATA.#PST-RQST-ID 'PIN : ' #POST-DATA.#PIN-NUMBER 'PCKG-CDE : ' #POST-DATA.#PACKAGE-CODE
            ldaRidl632.getPnd_Post_Data_Pnd_Package_Code());
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE," ");                                                                                                                               //Natural: WRITE ( 0 ) / ' '
        if (Global.isEscape()) return;
        //*  DISPLAY-POST-DATA
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
        Global.format(0, "LS=132 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(44),"2015 CUSTOM PORTFOLIO RIDER PROCESSING STATS",new 
            TabSetting(118),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(118),"TIME:",Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,new TabSetting(118),"PAGE:",new 
            ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE);
    }
}
