/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:18 PM
**        * FROM NATURAL PROGRAM : Ridp770
************************************************************
**        * FILE NAME            : Ridp770.java
**        * CLASS NAME           : Ridp770
**        * INSTANCE NAME        : Ridp770
************************************************************
************************************************************************
** PROGRAM     : RIDP770                                              **
** DATE        : 03/24/2020                                           **
** DESCRIPTION : READS INPUT RPL LOAN EXTRACT AND REFORMAT DATA AS    **
**               FEED TO RIDP772                                      **
**               PACKAGE CODES: RDR2013A                              **
************************************************************************
** HISTORY:                                                           **
** 02/24/2020  : INITIAL IMPLEMENTATION                               **
**                                                                    **
*   WHO      WHEN              WHY
* -------- ---------- --------------------------------------------------
* NEWSOM   03/24/2020 NEW
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridp770 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaMdma101 pdaMdma101;
    private PdaMdma211 pdaMdma211;
    private PdaPsta9017 pdaPsta9017;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Pin_A12;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Pin_N5;
    private DbsField pnd_Input_Record_Pnd_Pin_N7;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Pin_N12;
    private DbsField pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cref_Cntrct_Nbr;
    private DbsField pnd_Input_Record_Pnd_Cntrct_Issue_Date;
    private DbsField pnd_Input_Record_Pnd_Issue_State_Cde;
    private DbsField pnd_Input_Record_Pnd_Plan_Cde;
    private DbsField pnd_Input_Record_Pnd_Cover_Letter_Ind;
    private DbsField pnd_Input_Record_Pnd_Plan_Name;
    private DbsField pnd_Prev_Plan_Cde;
    private DbsField pnd_Prev_Pin_N12;

    private DbsGroup pnd_Prev_Pin_N12__R_Field_4;
    private DbsField pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5;
    private DbsField pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7;
    private DbsField pnd_Prev_Contract;
    private DbsField pnd_Prev_Use_Tiaa_Contract;
    private DbsField pnd_Prev_Use_Cref_Contract;
    private DbsField pnd_Prev_Cover;
    private DbsField pnd_Ra;
    private DbsField pnd_Gra;
    private DbsField pnd_Sra;
    private DbsField pnd_Rail;
    private DbsField pnd_Grail;
    private DbsField pnd_Srail;
    private DbsField pnd_Valid_Record;
    private DbsField pnd_Accepted;
    private DbsField pnd_Remarks;
    private DbsField pnd_Participant_Name;
    private DbsField pnd_Ctr_Rpl_Contract_Read;
    private DbsField pnd_Ctr_Da_Contract_Not_In_Cor;
    private DbsField pnd_Ctr_Da_Inactive_In_Cor;
    private DbsField pnd_Ctr_Ph_Deceased;
    private DbsField pnd_Ctr_Ph_With_Mail_Suppression;
    private DbsField pnd_Ctr_Ra_Reach_Max_Contracts;
    private DbsField pnd_Ctr_Gra_Reach_Max_Contracts;
    private DbsField pnd_Ctr_Sra_Reach_Max_Contracts;
    private DbsField pnd_Ctr_Ra_Contract_Accepted;
    private DbsField pnd_Ctr_Gra_Contract_Accepted;
    private DbsField pnd_Ctr_Sra_Contract_Accepted;
    private DbsField pnd_Ctr_Contract_Rejected;
    private DbsField pnd_Ctr_Contract_Read;
    private DbsField pnd_Ctr_Contract_Read_Eof;
    private DbsField pnd_Ctr_Duplicate_Contract;
    private DbsField pnd_Ctr_Terminated_Contract;
    private DbsField pnd_Ctr_Contract_Inactive;
    private DbsField pnd_Ctr_Out_Of_State;
    private DbsField pnd_Ctr_Contract_Outof_Range;
    private DbsField pnd_Ctr_Total_Contracts;
    private DbsField pnd_Ctr_Total_Ra_All_Other;
    private DbsField pnd_Ctr_Total_Ra_Fl;
    private DbsField pnd_Ctr_Total_Ra_Tx;
    private DbsField pnd_Ctr_Total_Ra_Pr;
    private DbsField pnd_Ctr_Total_Ra_Il;
    private DbsField pnd_Ctr_Total_Ra_Ar;
    private DbsField pnd_Ctr_Total_Ra_Il1;
    private DbsField pnd_Ctr_Total_Ra_Pa;
    private DbsField pnd_Ctr_Total_Gra_All_Other;
    private DbsField pnd_Ctr_Total_Gra_Fl;
    private DbsField pnd_Ctr_Total_Gra_Md;
    private DbsField pnd_Ctr_Total_Gra_Tx;
    private DbsField pnd_Ctr_Total_Gra_Pr;
    private DbsField pnd_Ctr_Total_Gra_Il;
    private DbsField pnd_Ctr_Total_Gra_Il1;
    private DbsField pnd_Ctr_Total_Sra_All_Other;
    private DbsField pnd_Ctr_Total_Sra_Fl;
    private DbsField pnd_Ctr_Total_Sra_Tx;
    private DbsField pnd_Ctr_Total_Sra_Il;
    private DbsField pnd_Ctr_Total_Sra_Il1;
    private DbsField pnd_Ctr_Total_Sra_Pa;
    private DbsField pnd_Break;
    private DbsField pnd_I;

    private DbsGroup pnd_Ctrs;
    private DbsField pnd_Ctrs_Pnd_Contract_Total;
    private DbsField pnd_Ctrs_Pnd_T_Ra_Rpl_E1_All_Other;
    private DbsField pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Fl;
    private DbsField pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Tx;
    private DbsField pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pr;
    private DbsField pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Il;
    private DbsField pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pa;
    private DbsField pnd_Ctrs_Pnd_C_Ra_Rpl_E1_All_Other;
    private DbsField pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Ar;
    private DbsField pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Fl;
    private DbsField pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Pr;
    private DbsField pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Il1;
    private DbsField pnd_Ctrs_Pnd_T_Gra_Rpl_E1_All_Other;
    private DbsField pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Fl;
    private DbsField pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Md;
    private DbsField pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Tx;
    private DbsField pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Pr;
    private DbsField pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Il;
    private DbsField pnd_Ctrs_Pnd_C_Gra_Rpl_E1_All_Other;
    private DbsField pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Fl;
    private DbsField pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Tx;
    private DbsField pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Pr;
    private DbsField pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Il;
    private DbsField pnd_Ctrs_Pnd_T_Sra_Rpl_E1_All_Other;
    private DbsField pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Fl;
    private DbsField pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Tx;
    private DbsField pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Il;
    private DbsField pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Pa;
    private DbsField pnd_Ctrs_Pnd_C_Sra_Rpl_E1_All_Other;
    private DbsField pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Fl;
    private DbsField pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Il1;
    private DbsField pnd_Ctr_File1;
    private DbsField pnd_Ctr_File2;

    private DbsGroup pnd_Output;
    private DbsField pnd_Output_Pnd_Pin_Number;

    private DbsGroup pnd_Output__R_Field_5;
    private DbsField pnd_Output_Pnd_Pin_N7;
    private DbsField pnd_Output_Pnd_Pin_A5;
    private DbsField pnd_Output_Pnd_Tiaa_Cntrct_Number;
    private DbsField pnd_Output_Pnd_Cref_Cntrct_Number;
    private DbsField pnd_Output_Pnd_Full_Name;
    private DbsField pnd_Output_Pnd_Last_Name;
    private DbsField pnd_Output_Pnd_Address_Line_Txt;
    private DbsField pnd_Output_Pnd_Postal_Data;
    private DbsField pnd_Output_Pnd_Address_Type_Cde;
    private DbsField pnd_Output_Pnd_Package_Code;
    private DbsField pnd_Output_Pnd_Letter_Type;

    private DbsGroup pnd_Output_Pnd_Retirement_Annuity;
    private DbsField pnd_Output_Pnd_T_Ra_Rpl_E1_All_Other;
    private DbsField pnd_Output_Pnd_T_Ra_Rpl_E1_Fl;
    private DbsField pnd_Output_Pnd_T_Ra_Rpl_E1_Tx;
    private DbsField pnd_Output_Pnd_T_Ra_Rpl_E1_Pr;
    private DbsField pnd_Output_Pnd_T_Ra_Rpl_E1_Il;
    private DbsField pnd_Output_Pnd_T_Ra_Rpl_E1_Pa;
    private DbsField pnd_Output_Pnd_C_Ra_Rpl_E1_All_Other;
    private DbsField pnd_Output_Pnd_C_Ra_Rpl_E1_Ar;
    private DbsField pnd_Output_Pnd_C_Ra_Rpl_E1_Fl;
    private DbsField pnd_Output_Pnd_C_Ra_Rpl_E1_Pr;
    private DbsField pnd_Output_Pnd_C_Ra_Rpl_E1_Il1;

    private DbsGroup pnd_Output_Pnd_Group_Retirement_Annuity;
    private DbsField pnd_Output_Pnd_T_Gra_Rpl_E1_All_Other;
    private DbsField pnd_Output_Pnd_T_Gra_Rpl_E1_Fl;
    private DbsField pnd_Output_Pnd_T_Gra_Rpl_E1_Md;
    private DbsField pnd_Output_Pnd_T_Gra_Rpl_E1_Tx;
    private DbsField pnd_Output_Pnd_T_Gra_Rpl_E1_Pr;
    private DbsField pnd_Output_Pnd_T_Gra_Rpl_E1_Il;
    private DbsField pnd_Output_Pnd_C_Gra_Rpl_E1_All_Other;
    private DbsField pnd_Output_Pnd_C_Gra_Rpl_E1_Fl;
    private DbsField pnd_Output_Pnd_C_Gra_Rpl_E1_Tx;
    private DbsField pnd_Output_Pnd_C_Gra_Rpl_E1_Pr;
    private DbsField pnd_Output_Pnd_C_Gra_Rpl_E1_Il;

    private DbsGroup pnd_Output_Pnd_Supplemental_Retirement_Annuity;
    private DbsField pnd_Output_Pnd_T_Sra_Rpl_E1_All_Other;
    private DbsField pnd_Output_Pnd_T_Sra_Rpl_E1_Fl;
    private DbsField pnd_Output_Pnd_T_Sra_Rpl_E1_Tx;
    private DbsField pnd_Output_Pnd_T_Sra_Rpl_E1_Il;
    private DbsField pnd_Output_Pnd_T_Sra_Rpl_E1_Pa;
    private DbsField pnd_Output_Pnd_C_Sra_Rpl_E1_All_Other;
    private DbsField pnd_Output_Pnd_C_Sra_Rpl_E1_Fl;
    private DbsField pnd_Output_Pnd_C_Sra_Rpl_E1_Il1;
    private DbsField pnd_Output_Pnd_Rider_Date;

    private DbsGroup pnd_Output__R_Field_6;
    private DbsField pnd_Output_Pnd_Rider_Month;
    private DbsField pnd_Output_Pnd_Rider_Year;

    private DbsGroup pnd_Output__R_Field_7;
    private DbsField pnd_Output_Pnd_Rider_Year_A;
    private DbsField pnd_Output_Pnd_Ra_Contracts;
    private DbsField pnd_Output_Pnd_Gra_Contracts;
    private DbsField pnd_Output_Pnd_Sra_Contracts;
    private DbsField pnd_Output_Pnd_Ra_Tiaa_Contracts_Il;
    private DbsField pnd_Output_Pnd_Ra_Cref_Contracts_Il1;
    private DbsField pnd_Output_Pnd_Gra_Tiaa_Contracts_Il;
    private DbsField pnd_Output_Pnd_Gra_Cref_Contracts_Il;
    private DbsField pnd_Output_Pnd_Sra_Tiaa_Contracts_Il;
    private DbsField pnd_Output_Pnd_Sra_Cref_Contracts_Il1;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma211 = new PdaMdma211(localVariables);
        pdaPsta9017 = new PdaPsta9017(localVariables);

        // Local Variables
        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 115);

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Pin_A12 = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);

        pnd_Input_Record__R_Field_2 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N5 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N5", "#PIN-N5", FieldType.NUMERIC, 5);
        pnd_Input_Record_Pnd_Pin_N7 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record__R_Field_1.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Pin_A12);
        pnd_Input_Record_Pnd_Pin_N12 = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Pin_N12", "#PIN-N12", FieldType.NUMERIC, 12);
        pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr", "#TIAA-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cref_Cntrct_Nbr = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cref_Cntrct_Nbr", "#CREF-CNTRCT-NBR", 
            FieldType.STRING, 8);
        pnd_Input_Record_Pnd_Cntrct_Issue_Date = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cntrct_Issue_Date", "#CNTRCT-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Input_Record_Pnd_Issue_State_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Issue_State_Cde", "#ISSUE-STATE-CDE", 
            FieldType.STRING, 2);
        pnd_Input_Record_Pnd_Plan_Cde = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Plan_Cde", "#PLAN-CDE", FieldType.STRING, 6);
        pnd_Input_Record_Pnd_Cover_Letter_Ind = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Cover_Letter_Ind", "#COVER-LETTER-IND", 
            FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Plan_Name = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Plan_Name", "#PLAN-NAME", FieldType.STRING, 
            60);
        pnd_Prev_Plan_Cde = localVariables.newFieldInRecord("pnd_Prev_Plan_Cde", "#PREV-PLAN-CDE", FieldType.STRING, 6);
        pnd_Prev_Pin_N12 = localVariables.newFieldInRecord("pnd_Prev_Pin_N12", "#PREV-PIN-N12", FieldType.NUMERIC, 12);

        pnd_Prev_Pin_N12__R_Field_4 = localVariables.newGroupInRecord("pnd_Prev_Pin_N12__R_Field_4", "REDEFINE", pnd_Prev_Pin_N12);
        pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5 = pnd_Prev_Pin_N12__R_Field_4.newFieldInGroup("pnd_Prev_Pin_N12_Pnd_Prev_Pin_N5", "#PREV-PIN-N5", FieldType.NUMERIC, 
            5);
        pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7 = pnd_Prev_Pin_N12__R_Field_4.newFieldInGroup("pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7", "#PREV-PIN-N7", FieldType.NUMERIC, 
            7);
        pnd_Prev_Contract = localVariables.newFieldInRecord("pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Use_Tiaa_Contract = localVariables.newFieldInRecord("pnd_Prev_Use_Tiaa_Contract", "#PREV-USE-TIAA-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Use_Cref_Contract = localVariables.newFieldInRecord("pnd_Prev_Use_Cref_Contract", "#PREV-USE-CREF-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Cover = localVariables.newFieldInRecord("pnd_Prev_Cover", "#PREV-COVER", FieldType.STRING, 1);
        pnd_Ra = localVariables.newFieldInRecord("pnd_Ra", "#RA", FieldType.NUMERIC, 3);
        pnd_Gra = localVariables.newFieldInRecord("pnd_Gra", "#GRA", FieldType.NUMERIC, 3);
        pnd_Sra = localVariables.newFieldInRecord("pnd_Sra", "#SRA", FieldType.NUMERIC, 3);
        pnd_Rail = localVariables.newFieldInRecord("pnd_Rail", "#RAIL", FieldType.NUMERIC, 3);
        pnd_Grail = localVariables.newFieldInRecord("pnd_Grail", "#GRAIL", FieldType.NUMERIC, 3);
        pnd_Srail = localVariables.newFieldInRecord("pnd_Srail", "#SRAIL", FieldType.NUMERIC, 3);
        pnd_Valid_Record = localVariables.newFieldInRecord("pnd_Valid_Record", "#VALID-RECORD", FieldType.BOOLEAN, 1);
        pnd_Accepted = localVariables.newFieldInRecord("pnd_Accepted", "#ACCEPTED", FieldType.BOOLEAN, 1);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 40);
        pnd_Participant_Name = localVariables.newFieldInRecord("pnd_Participant_Name", "#PARTICIPANT-NAME", FieldType.STRING, 35);
        pnd_Ctr_Rpl_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Rpl_Contract_Read", "#CTR-RPL-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Da_Contract_Not_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Contract_Not_In_Cor", "#CTR-DA-CONTRACT-NOT-IN-COR", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Da_Inactive_In_Cor = localVariables.newFieldInRecord("pnd_Ctr_Da_Inactive_In_Cor", "#CTR-DA-INACTIVE-IN-COR", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_Deceased = localVariables.newFieldInRecord("pnd_Ctr_Ph_Deceased", "#CTR-PH-DECEASED", FieldType.NUMERIC, 7);
        pnd_Ctr_Ph_With_Mail_Suppression = localVariables.newFieldInRecord("pnd_Ctr_Ph_With_Mail_Suppression", "#CTR-PH-WITH-MAIL-SUPPRESSION", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Ra_Reach_Max_Contracts = localVariables.newFieldInRecord("pnd_Ctr_Ra_Reach_Max_Contracts", "#CTR-RA-REACH-MAX-CONTRACTS", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Gra_Reach_Max_Contracts = localVariables.newFieldInRecord("pnd_Ctr_Gra_Reach_Max_Contracts", "#CTR-GRA-REACH-MAX-CONTRACTS", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Sra_Reach_Max_Contracts = localVariables.newFieldInRecord("pnd_Ctr_Sra_Reach_Max_Contracts", "#CTR-SRA-REACH-MAX-CONTRACTS", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Ra_Contract_Accepted = localVariables.newFieldInRecord("pnd_Ctr_Ra_Contract_Accepted", "#CTR-RA-CONTRACT-ACCEPTED", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Gra_Contract_Accepted = localVariables.newFieldInRecord("pnd_Ctr_Gra_Contract_Accepted", "#CTR-GRA-CONTRACT-ACCEPTED", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Sra_Contract_Accepted = localVariables.newFieldInRecord("pnd_Ctr_Sra_Contract_Accepted", "#CTR-SRA-CONTRACT-ACCEPTED", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Contract_Rejected = localVariables.newFieldInRecord("pnd_Ctr_Contract_Rejected", "#CTR-CONTRACT-REJECTED", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Read = localVariables.newFieldInRecord("pnd_Ctr_Contract_Read", "#CTR-CONTRACT-READ", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Read_Eof = localVariables.newFieldInRecord("pnd_Ctr_Contract_Read_Eof", "#CTR-CONTRACT-READ-EOF", FieldType.NUMERIC, 7);
        pnd_Ctr_Duplicate_Contract = localVariables.newFieldInRecord("pnd_Ctr_Duplicate_Contract", "#CTR-DUPLICATE-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Terminated_Contract = localVariables.newFieldInRecord("pnd_Ctr_Terminated_Contract", "#CTR-TERMINATED-CONTRACT", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Inactive = localVariables.newFieldInRecord("pnd_Ctr_Contract_Inactive", "#CTR-CONTRACT-INACTIVE", FieldType.NUMERIC, 7);
        pnd_Ctr_Out_Of_State = localVariables.newFieldInRecord("pnd_Ctr_Out_Of_State", "#CTR-OUT-OF-STATE", FieldType.NUMERIC, 7);
        pnd_Ctr_Contract_Outof_Range = localVariables.newFieldInRecord("pnd_Ctr_Contract_Outof_Range", "#CTR-CONTRACT-OUTOF-RANGE", FieldType.NUMERIC, 
            7);
        pnd_Ctr_Total_Contracts = localVariables.newFieldInRecord("pnd_Ctr_Total_Contracts", "#CTR-TOTAL-CONTRACTS", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_All_Other = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_All_Other", "#CTR-TOTAL-RA-ALL-OTHER", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_Fl = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Fl", "#CTR-TOTAL-RA-FL", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_Tx = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Tx", "#CTR-TOTAL-RA-TX", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_Pr = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Pr", "#CTR-TOTAL-RA-PR", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_Il = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Il", "#CTR-TOTAL-RA-IL", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_Ar = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Ar", "#CTR-TOTAL-RA-AR", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_Il1 = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Il1", "#CTR-TOTAL-RA-IL1", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Ra_Pa = localVariables.newFieldInRecord("pnd_Ctr_Total_Ra_Pa", "#CTR-TOTAL-RA-PA", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gra_All_Other = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra_All_Other", "#CTR-TOTAL-GRA-ALL-OTHER", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gra_Fl = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra_Fl", "#CTR-TOTAL-GRA-FL", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gra_Md = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra_Md", "#CTR-TOTAL-GRA-MD", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gra_Tx = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra_Tx", "#CTR-TOTAL-GRA-TX", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gra_Pr = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra_Pr", "#CTR-TOTAL-GRA-PR", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gra_Il = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra_Il", "#CTR-TOTAL-GRA-IL", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Gra_Il1 = localVariables.newFieldInRecord("pnd_Ctr_Total_Gra_Il1", "#CTR-TOTAL-GRA-IL1", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Sra_All_Other = localVariables.newFieldInRecord("pnd_Ctr_Total_Sra_All_Other", "#CTR-TOTAL-SRA-ALL-OTHER", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Sra_Fl = localVariables.newFieldInRecord("pnd_Ctr_Total_Sra_Fl", "#CTR-TOTAL-SRA-FL", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Sra_Tx = localVariables.newFieldInRecord("pnd_Ctr_Total_Sra_Tx", "#CTR-TOTAL-SRA-TX", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Sra_Il = localVariables.newFieldInRecord("pnd_Ctr_Total_Sra_Il", "#CTR-TOTAL-SRA-IL", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Sra_Il1 = localVariables.newFieldInRecord("pnd_Ctr_Total_Sra_Il1", "#CTR-TOTAL-SRA-IL1", FieldType.NUMERIC, 7);
        pnd_Ctr_Total_Sra_Pa = localVariables.newFieldInRecord("pnd_Ctr_Total_Sra_Pa", "#CTR-TOTAL-SRA-PA", FieldType.NUMERIC, 7);
        pnd_Break = localVariables.newFieldInRecord("pnd_Break", "#BREAK", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);

        pnd_Ctrs = localVariables.newGroupInRecord("pnd_Ctrs", "#CTRS");
        pnd_Ctrs_Pnd_Contract_Total = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Contract_Total", "#CONTRACT-TOTAL", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Ra_Rpl_E1_All_Other = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Ra_Rpl_E1_All_Other", "#T-RA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 
            8);
        pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Fl", "#T-RA-RPL-E1-FL", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Tx = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Tx", "#T-RA-RPL-E1-TX", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pr = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pr", "#T-RA-RPL-E1-PR", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Il = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Il", "#T-RA-RPL-E1-IL", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pa = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pa", "#T-RA-RPL-E1-PA", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_C_Ra_Rpl_E1_All_Other = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Ra_Rpl_E1_All_Other", "#C-RA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 
            8);
        pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Ar = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Ar", "#C-RA-RPL-E1-AR", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Fl", "#C-RA-RPL-E1-FL", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Pr = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Pr", "#C-RA-RPL-E1-PR", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Il1 = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Il1", "#C-RA-RPL-E1-IL1", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Gra_Rpl_E1_All_Other = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Gra_Rpl_E1_All_Other", "#T-GRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 
            8);
        pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Fl", "#T-GRA-RPL-E1-FL", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Md = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Md", "#T-GRA-RPL-E1-MD", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Tx = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Tx", "#T-GRA-RPL-E1-TX", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Pr = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Pr", "#T-GRA-RPL-E1-PR", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Il = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Il", "#T-GRA-RPL-E1-IL", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_C_Gra_Rpl_E1_All_Other = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Gra_Rpl_E1_All_Other", "#C-GRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 
            8);
        pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Fl", "#C-GRA-RPL-E1-FL", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Tx = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Tx", "#C-GRA-RPL-E1-TX", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Pr = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Pr", "#C-GRA-RPL-E1-PR", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Il = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Il", "#C-GRA-RPL-E1-IL", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Sra_Rpl_E1_All_Other = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Sra_Rpl_E1_All_Other", "#T-SRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 
            8);
        pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Fl", "#T-SRA-RPL-E1-FL", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Tx = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Tx", "#T-SRA-RPL-E1-TX", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Il = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Il", "#T-SRA-RPL-E1-IL", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Pa = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Pa", "#T-SRA-RPL-E1-PA", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_C_Sra_Rpl_E1_All_Other = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Sra_Rpl_E1_All_Other", "#C-SRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 
            8);
        pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Fl", "#C-SRA-RPL-E1-FL", FieldType.NUMERIC, 8);
        pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Il1 = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Il1", "#C-SRA-RPL-E1-IL1", FieldType.NUMERIC, 8);
        pnd_Ctr_File1 = localVariables.newFieldInRecord("pnd_Ctr_File1", "#CTR-FILE1", FieldType.NUMERIC, 7);
        pnd_Ctr_File2 = localVariables.newFieldInRecord("pnd_Ctr_File2", "#CTR-FILE2", FieldType.NUMERIC, 7);

        pnd_Output = localVariables.newGroupInRecord("pnd_Output", "#OUTPUT");
        pnd_Output_Pnd_Pin_Number = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);

        pnd_Output__R_Field_5 = pnd_Output.newGroupInGroup("pnd_Output__R_Field_5", "REDEFINE", pnd_Output_Pnd_Pin_Number);
        pnd_Output_Pnd_Pin_N7 = pnd_Output__R_Field_5.newFieldInGroup("pnd_Output_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);
        pnd_Output_Pnd_Pin_A5 = pnd_Output__R_Field_5.newFieldInGroup("pnd_Output_Pnd_Pin_A5", "#PIN-A5", FieldType.STRING, 5);
        pnd_Output_Pnd_Tiaa_Cntrct_Number = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Tiaa_Cntrct_Number", "#TIAA-CNTRCT-NUMBER", FieldType.STRING, 8);
        pnd_Output_Pnd_Cref_Cntrct_Number = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Cref_Cntrct_Number", "#CREF-CNTRCT-NUMBER", FieldType.STRING, 8);
        pnd_Output_Pnd_Full_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Output_Pnd_Last_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 35);
        pnd_Output_Pnd_Address_Line_Txt = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 35, 
            new DbsArrayController(1, 6));
        pnd_Output_Pnd_Postal_Data = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 32);
        pnd_Output_Pnd_Address_Type_Cde = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 1);
        pnd_Output_Pnd_Package_Code = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Output_Pnd_Letter_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);

        pnd_Output_Pnd_Retirement_Annuity = pnd_Output.newGroupInGroup("pnd_Output_Pnd_Retirement_Annuity", "#RETIREMENT-ANNUITY");
        pnd_Output_Pnd_T_Ra_Rpl_E1_All_Other = pnd_Output_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Ra_Rpl_E1_All_Other", "#T-RA-RPL-E1-ALL-OTHER", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_T_Ra_Rpl_E1_Fl = pnd_Output_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Ra_Rpl_E1_Fl", "#T-RA-RPL-E1-FL", FieldType.NUMERIC, 
            8);
        pnd_Output_Pnd_T_Ra_Rpl_E1_Tx = pnd_Output_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Ra_Rpl_E1_Tx", "#T-RA-RPL-E1-TX", FieldType.NUMERIC, 
            8);
        pnd_Output_Pnd_T_Ra_Rpl_E1_Pr = pnd_Output_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Ra_Rpl_E1_Pr", "#T-RA-RPL-E1-PR", FieldType.NUMERIC, 
            8);
        pnd_Output_Pnd_T_Ra_Rpl_E1_Il = pnd_Output_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Ra_Rpl_E1_Il", "#T-RA-RPL-E1-IL", FieldType.NUMERIC, 
            8);
        pnd_Output_Pnd_T_Ra_Rpl_E1_Pa = pnd_Output_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Ra_Rpl_E1_Pa", "#T-RA-RPL-E1-PA", FieldType.NUMERIC, 
            8);
        pnd_Output_Pnd_C_Ra_Rpl_E1_All_Other = pnd_Output_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Ra_Rpl_E1_All_Other", "#C-RA-RPL-E1-ALL-OTHER", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_C_Ra_Rpl_E1_Ar = pnd_Output_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Ra_Rpl_E1_Ar", "#C-RA-RPL-E1-AR", FieldType.NUMERIC, 
            8);
        pnd_Output_Pnd_C_Ra_Rpl_E1_Fl = pnd_Output_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Ra_Rpl_E1_Fl", "#C-RA-RPL-E1-FL", FieldType.NUMERIC, 
            8);
        pnd_Output_Pnd_C_Ra_Rpl_E1_Pr = pnd_Output_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Ra_Rpl_E1_Pr", "#C-RA-RPL-E1-PR", FieldType.NUMERIC, 
            8);
        pnd_Output_Pnd_C_Ra_Rpl_E1_Il1 = pnd_Output_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Ra_Rpl_E1_Il1", "#C-RA-RPL-E1-IL1", FieldType.NUMERIC, 
            8);

        pnd_Output_Pnd_Group_Retirement_Annuity = pnd_Output.newGroupInGroup("pnd_Output_Pnd_Group_Retirement_Annuity", "#GROUP-RETIREMENT-ANNUITY");
        pnd_Output_Pnd_T_Gra_Rpl_E1_All_Other = pnd_Output_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Gra_Rpl_E1_All_Other", "#T-GRA-RPL-E1-ALL-OTHER", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_T_Gra_Rpl_E1_Fl = pnd_Output_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Gra_Rpl_E1_Fl", "#T-GRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_T_Gra_Rpl_E1_Md = pnd_Output_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Gra_Rpl_E1_Md", "#T-GRA-RPL-E1-MD", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_T_Gra_Rpl_E1_Tx = pnd_Output_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Gra_Rpl_E1_Tx", "#T-GRA-RPL-E1-TX", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_T_Gra_Rpl_E1_Pr = pnd_Output_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Gra_Rpl_E1_Pr", "#T-GRA-RPL-E1-PR", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_T_Gra_Rpl_E1_Il = pnd_Output_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Gra_Rpl_E1_Il", "#T-GRA-RPL-E1-IL", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_C_Gra_Rpl_E1_All_Other = pnd_Output_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Gra_Rpl_E1_All_Other", "#C-GRA-RPL-E1-ALL-OTHER", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_C_Gra_Rpl_E1_Fl = pnd_Output_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Gra_Rpl_E1_Fl", "#C-GRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_C_Gra_Rpl_E1_Tx = pnd_Output_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Gra_Rpl_E1_Tx", "#C-GRA-RPL-E1-TX", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_C_Gra_Rpl_E1_Pr = pnd_Output_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Gra_Rpl_E1_Pr", "#C-GRA-RPL-E1-PR", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_C_Gra_Rpl_E1_Il = pnd_Output_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Gra_Rpl_E1_Il", "#C-GRA-RPL-E1-IL", 
            FieldType.NUMERIC, 8);

        pnd_Output_Pnd_Supplemental_Retirement_Annuity = pnd_Output.newGroupInGroup("pnd_Output_Pnd_Supplemental_Retirement_Annuity", "#SUPPLEMENTAL-RETIREMENT-ANNUITY");
        pnd_Output_Pnd_T_Sra_Rpl_E1_All_Other = pnd_Output_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Sra_Rpl_E1_All_Other", 
            "#T-SRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 8);
        pnd_Output_Pnd_T_Sra_Rpl_E1_Fl = pnd_Output_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Sra_Rpl_E1_Fl", "#T-SRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_T_Sra_Rpl_E1_Tx = pnd_Output_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Sra_Rpl_E1_Tx", "#T-SRA-RPL-E1-TX", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_T_Sra_Rpl_E1_Il = pnd_Output_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Sra_Rpl_E1_Il", "#T-SRA-RPL-E1-IL", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_T_Sra_Rpl_E1_Pa = pnd_Output_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_T_Sra_Rpl_E1_Pa", "#T-SRA-RPL-E1-PA", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_C_Sra_Rpl_E1_All_Other = pnd_Output_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Sra_Rpl_E1_All_Other", 
            "#C-SRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 8);
        pnd_Output_Pnd_C_Sra_Rpl_E1_Fl = pnd_Output_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Sra_Rpl_E1_Fl", "#C-SRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_C_Sra_Rpl_E1_Il1 = pnd_Output_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Output_Pnd_C_Sra_Rpl_E1_Il1", "#C-SRA-RPL-E1-IL1", 
            FieldType.NUMERIC, 8);
        pnd_Output_Pnd_Rider_Date = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Rider_Date", "#RIDER-DATE", FieldType.NUMERIC, 6);

        pnd_Output__R_Field_6 = pnd_Output.newGroupInGroup("pnd_Output__R_Field_6", "REDEFINE", pnd_Output_Pnd_Rider_Date);
        pnd_Output_Pnd_Rider_Month = pnd_Output__R_Field_6.newFieldInGroup("pnd_Output_Pnd_Rider_Month", "#RIDER-MONTH", FieldType.NUMERIC, 2);
        pnd_Output_Pnd_Rider_Year = pnd_Output__R_Field_6.newFieldInGroup("pnd_Output_Pnd_Rider_Year", "#RIDER-YEAR", FieldType.NUMERIC, 4);

        pnd_Output__R_Field_7 = pnd_Output__R_Field_6.newGroupInGroup("pnd_Output__R_Field_7", "REDEFINE", pnd_Output_Pnd_Rider_Year);
        pnd_Output_Pnd_Rider_Year_A = pnd_Output__R_Field_7.newFieldInGroup("pnd_Output_Pnd_Rider_Year_A", "#RIDER-YEAR-A", FieldType.STRING, 4);
        pnd_Output_Pnd_Ra_Contracts = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Ra_Contracts", "#RA-CONTRACTS", FieldType.STRING, 17, new DbsArrayController(1, 
            50));
        pnd_Output_Pnd_Gra_Contracts = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Gra_Contracts", "#GRA-CONTRACTS", FieldType.STRING, 17, new DbsArrayController(1, 
            50));
        pnd_Output_Pnd_Sra_Contracts = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Sra_Contracts", "#SRA-CONTRACTS", FieldType.STRING, 17, new DbsArrayController(1, 
            50));
        pnd_Output_Pnd_Ra_Tiaa_Contracts_Il = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Ra_Tiaa_Contracts_Il", "#RA-TIAA-CONTRACTS-IL", FieldType.STRING, 
            8, new DbsArrayController(1, 50));
        pnd_Output_Pnd_Ra_Cref_Contracts_Il1 = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Ra_Cref_Contracts_Il1", "#RA-CREF-CONTRACTS-IL1", FieldType.STRING, 
            8, new DbsArrayController(1, 50));
        pnd_Output_Pnd_Gra_Tiaa_Contracts_Il = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Gra_Tiaa_Contracts_Il", "#GRA-TIAA-CONTRACTS-IL", FieldType.STRING, 
            8, new DbsArrayController(1, 50));
        pnd_Output_Pnd_Gra_Cref_Contracts_Il = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Gra_Cref_Contracts_Il", "#GRA-CREF-CONTRACTS-IL", FieldType.STRING, 
            8, new DbsArrayController(1, 50));
        pnd_Output_Pnd_Sra_Tiaa_Contracts_Il = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Sra_Tiaa_Contracts_Il", "#SRA-TIAA-CONTRACTS-IL", FieldType.STRING, 
            8, new DbsArrayController(1, 50));
        pnd_Output_Pnd_Sra_Cref_Contracts_Il1 = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Sra_Cref_Contracts_Il1", "#SRA-CREF-CONTRACTS-IL1", FieldType.STRING, 
            8, new DbsArrayController(1, 50));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Valid_Record.setInitialValue(false);
        pnd_Accepted.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridp770() throws Exception
    {
        super("Ridp770");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  STATISTICS REPORT
        if (condition(getReports().getAstLinesLeft(1).less(5)))                                                                                                           //Natural: FORMAT ( 1 ) LS = 120 PS = 60;//Natural: FORMAT LS = 80 PS = 60;//Natural: NEWPAGE ( 1 ) IF LESS THAN 5 LINES
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 43T 'RPL LOAN   MAILING' 84T 'DATE:' *DATU / 43T 'EXCEPTIONS LIST' 84T 'TIME:' *TIME ( EM = XXXXXXXX ) / 84T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) // 2T 'PIN            TIAA NBR   STATE   REMARKS'/ 2T '------------------------------------------------------------'/
        //*  OPEN MQ QUEUE                                       /* JCA20150324
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-RECORD
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            pnd_Ctr_Rpl_Contract_Read.nadd(1);                                                                                                                            //Natural: ADD 1 TO #CTR-RPL-CONTRACT-READ
            if (condition(pnd_Prev_Pin_N12.equals(getZero())))                                                                                                            //Natural: IF #PREV-PIN-N12 = 0
            {
                pnd_Prev_Pin_N12.setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                                                  //Natural: ASSIGN #PREV-PIN-N12 := #INPUT-RECORD.#PIN-N12
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_Pin_N12.notEquals(pnd_Prev_Pin_N12) || pnd_Ctrs_Pnd_Contract_Total.equals(50)))                                            //Natural: IF #INPUT-RECORD.#PIN-N12 NE #PREV-PIN-N12 OR #CONTRACT-TOTAL = 50
            {
                pnd_Ctr_Total_Contracts.compute(new ComputeParameters(false, pnd_Ctr_Total_Contracts), pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Fl.add(pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Tx).add(pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pr).add(pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Il).add(pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pa).add(pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Ar).add(pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Fl).add(pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Pr).add(pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Il1).add(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Fl).add(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Md).add(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Tx).add(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Pr).add(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Il).add(pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Fl).add(pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Tx).add(pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Pr).add(pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Il).add(pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Fl).add(pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Tx).add(pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Il).add(pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Pa).add(pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Fl).add(pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Il1).add(pnd_Ctrs_Pnd_T_Ra_Rpl_E1_All_Other).add(pnd_Ctrs_Pnd_C_Ra_Rpl_E1_All_Other).add(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_All_Other).add(pnd_Ctrs_Pnd_C_Gra_Rpl_E1_All_Other).add(pnd_Ctrs_Pnd_T_Sra_Rpl_E1_All_Other).add(pnd_Ctrs_Pnd_C_Sra_Rpl_E1_All_Other)); //Natural: ASSIGN #CTR-TOTAL-CONTRACTS := #CTRS.#T-RA-RPL-E1-FL + #CTRS.#T-RA-RPL-E1-TX + #CTRS.#T-RA-RPL-E1-PR + #CTRS.#T-RA-RPL-E1-IL + #CTRS.#T-RA-RPL-E1-PA + #CTRS.#C-RA-RPL-E1-AR + #CTRS.#C-RA-RPL-E1-FL + #CTRS.#C-RA-RPL-E1-PR + #CTRS.#C-RA-RPL-E1-IL1 + #CTRS.#T-GRA-RPL-E1-FL + #CTRS.#T-GRA-RPL-E1-MD + #CTRS.#T-GRA-RPL-E1-TX + #CTRS.#T-GRA-RPL-E1-PR + #CTRS.#T-GRA-RPL-E1-IL + #CTRS.#C-GRA-RPL-E1-FL + #CTRS.#C-GRA-RPL-E1-TX + #CTRS.#C-GRA-RPL-E1-PR + #CTRS.#C-GRA-RPL-E1-IL + #CTRS.#T-SRA-RPL-E1-FL + #CTRS.#T-SRA-RPL-E1-TX + #CTRS.#T-SRA-RPL-E1-IL + #CTRS.#T-SRA-RPL-E1-PA + #CTRS.#C-SRA-RPL-E1-FL + #CTRS.#C-SRA-RPL-E1-IL1 + #CTRS.#T-RA-RPL-E1-ALL-OTHER + #CTRS.#C-RA-RPL-E1-ALL-OTHER + #CTRS.#T-GRA-RPL-E1-ALL-OTHER + #CTRS.#C-GRA-RPL-E1-ALL-OTHER + #CTRS.#T-SRA-RPL-E1-ALL-OTHER + #CTRS.#C-SRA-RPL-E1-ALL-OTHER
                if (condition(pnd_Ctr_Total_Contracts.greater(getZero())))                                                                                                //Natural: IF #CTR-TOTAL-CONTRACTS GT 0
                {
                    if (condition(pnd_Prev_Pin_N12.greaterOrEqual(new DbsDecimal("100000000000"))))                                                                       //Natural: IF #PREV-PIN-N12 GE 100000000000
                    {
                        pnd_Output_Pnd_Pin_Number.setValue(pnd_Prev_Pin_N12);                                                                                             //Natural: MOVE #PREV-PIN-N12 TO #OUTPUT.#PIN-NUMBER
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Output_Pnd_Pin_N7.setValue(pnd_Prev_Pin_N12_Pnd_Prev_Pin_N7);                                                                                 //Natural: MOVE #PREV-PIN-N7 TO #OUTPUT.#PIN-N7
                        pnd_Output_Pnd_Pin_A5.setValue("     ");                                                                                                          //Natural: MOVE '     ' TO #OUTPUT.#PIN-A5
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Output_Pnd_T_Ra_Rpl_E1_All_Other.setValue(pnd_Ctrs_Pnd_T_Ra_Rpl_E1_All_Other);                                                                    //Natural: MOVE #CTRS.#T-RA-RPL-E1-ALL-OTHER TO #OUTPUT.#T-RA-RPL-E1-ALL-OTHER
                    pnd_Output_Pnd_T_Ra_Rpl_E1_Fl.setValue(pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Fl);                                                                                  //Natural: MOVE #CTRS.#T-RA-RPL-E1-FL TO #OUTPUT.#T-RA-RPL-E1-FL
                    pnd_Output_Pnd_T_Ra_Rpl_E1_Tx.setValue(pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Tx);                                                                                  //Natural: MOVE #CTRS.#T-RA-RPL-E1-TX TO #OUTPUT.#T-RA-RPL-E1-TX
                    pnd_Output_Pnd_T_Ra_Rpl_E1_Pr.setValue(pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pr);                                                                                  //Natural: MOVE #CTRS.#T-RA-RPL-E1-PR TO #OUTPUT.#T-RA-RPL-E1-PR
                    pnd_Output_Pnd_T_Ra_Rpl_E1_Il.setValue(pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Il);                                                                                  //Natural: MOVE #CTRS.#T-RA-RPL-E1-IL TO #OUTPUT.#T-RA-RPL-E1-IL
                    pnd_Output_Pnd_T_Ra_Rpl_E1_Pa.setValue(pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pa);                                                                                  //Natural: MOVE #CTRS.#T-RA-RPL-E1-PA TO #OUTPUT.#T-RA-RPL-E1-PA
                    pnd_Output_Pnd_C_Ra_Rpl_E1_All_Other.setValue(pnd_Ctrs_Pnd_C_Ra_Rpl_E1_All_Other);                                                                    //Natural: MOVE #CTRS.#C-RA-RPL-E1-ALL-OTHER TO #OUTPUT.#C-RA-RPL-E1-ALL-OTHER
                    pnd_Output_Pnd_C_Ra_Rpl_E1_Ar.setValue(pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Ar);                                                                                  //Natural: MOVE #CTRS.#C-RA-RPL-E1-AR TO #OUTPUT.#C-RA-RPL-E1-AR
                    pnd_Output_Pnd_C_Ra_Rpl_E1_Fl.setValue(pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Fl);                                                                                  //Natural: MOVE #CTRS.#C-RA-RPL-E1-FL TO #OUTPUT.#C-RA-RPL-E1-FL
                    pnd_Output_Pnd_C_Ra_Rpl_E1_Pr.setValue(pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Pr);                                                                                  //Natural: MOVE #CTRS.#C-RA-RPL-E1-PR TO #OUTPUT.#C-RA-RPL-E1-PR
                    pnd_Output_Pnd_C_Ra_Rpl_E1_Il1.setValue(pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Il1);                                                                                //Natural: MOVE #CTRS.#C-RA-RPL-E1-IL1 TO #OUTPUT.#C-RA-RPL-E1-IL1
                    pnd_Output_Pnd_T_Gra_Rpl_E1_All_Other.setValue(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_All_Other);                                                                  //Natural: MOVE #CTRS.#T-GRA-RPL-E1-ALL-OTHER TO #OUTPUT.#T-GRA-RPL-E1-ALL-OTHER
                    pnd_Output_Pnd_T_Gra_Rpl_E1_Fl.setValue(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Fl);                                                                                //Natural: MOVE #CTRS.#T-GRA-RPL-E1-FL TO #OUTPUT.#T-GRA-RPL-E1-FL
                    pnd_Output_Pnd_T_Gra_Rpl_E1_Md.setValue(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Md);                                                                                //Natural: MOVE #CTRS.#T-GRA-RPL-E1-MD TO #OUTPUT.#T-GRA-RPL-E1-MD
                    pnd_Output_Pnd_T_Gra_Rpl_E1_Tx.setValue(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Tx);                                                                                //Natural: MOVE #CTRS.#T-GRA-RPL-E1-TX TO #OUTPUT.#T-GRA-RPL-E1-TX
                    pnd_Output_Pnd_T_Gra_Rpl_E1_Pr.setValue(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Pr);                                                                                //Natural: MOVE #CTRS.#T-GRA-RPL-E1-PR TO #OUTPUT.#T-GRA-RPL-E1-PR
                    pnd_Output_Pnd_T_Gra_Rpl_E1_Il.setValue(pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Il);                                                                                //Natural: MOVE #CTRS.#T-GRA-RPL-E1-IL TO #OUTPUT.#T-GRA-RPL-E1-IL
                    pnd_Output_Pnd_C_Gra_Rpl_E1_All_Other.setValue(pnd_Ctrs_Pnd_C_Gra_Rpl_E1_All_Other);                                                                  //Natural: MOVE #CTRS.#C-GRA-RPL-E1-ALL-OTHER TO #OUTPUT.#C-GRA-RPL-E1-ALL-OTHER
                    pnd_Output_Pnd_C_Gra_Rpl_E1_Fl.setValue(pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Fl);                                                                                //Natural: MOVE #CTRS.#C-GRA-RPL-E1-FL TO #OUTPUT.#C-GRA-RPL-E1-FL
                    pnd_Output_Pnd_C_Gra_Rpl_E1_Tx.setValue(pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Tx);                                                                                //Natural: MOVE #CTRS.#C-GRA-RPL-E1-TX TO #OUTPUT.#C-GRA-RPL-E1-TX
                    pnd_Output_Pnd_C_Gra_Rpl_E1_Pr.setValue(pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Pr);                                                                                //Natural: MOVE #CTRS.#C-GRA-RPL-E1-PR TO #OUTPUT.#C-GRA-RPL-E1-PR
                    pnd_Output_Pnd_C_Gra_Rpl_E1_Il.setValue(pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Il);                                                                                //Natural: MOVE #CTRS.#C-GRA-RPL-E1-IL TO #OUTPUT.#C-GRA-RPL-E1-IL
                    pnd_Output_Pnd_T_Sra_Rpl_E1_All_Other.setValue(pnd_Ctrs_Pnd_T_Sra_Rpl_E1_All_Other);                                                                  //Natural: MOVE #CTRS.#T-SRA-RPL-E1-ALL-OTHER TO #OUTPUT.#T-SRA-RPL-E1-ALL-OTHER
                    pnd_Output_Pnd_T_Sra_Rpl_E1_Fl.setValue(pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Fl);                                                                                //Natural: MOVE #CTRS.#T-SRA-RPL-E1-FL TO #OUTPUT.#T-SRA-RPL-E1-FL
                    pnd_Output_Pnd_T_Sra_Rpl_E1_Tx.setValue(pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Tx);                                                                                //Natural: MOVE #CTRS.#T-SRA-RPL-E1-TX TO #OUTPUT.#T-SRA-RPL-E1-TX
                    pnd_Output_Pnd_T_Sra_Rpl_E1_Il.setValue(pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Il);                                                                                //Natural: MOVE #CTRS.#T-SRA-RPL-E1-IL TO #OUTPUT.#T-SRA-RPL-E1-IL
                    pnd_Output_Pnd_T_Sra_Rpl_E1_Pa.setValue(pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Pa);                                                                                //Natural: MOVE #CTRS.#T-SRA-RPL-E1-PA TO #OUTPUT.#T-SRA-RPL-E1-PA
                    pnd_Output_Pnd_C_Sra_Rpl_E1_All_Other.setValue(pnd_Ctrs_Pnd_C_Sra_Rpl_E1_All_Other);                                                                  //Natural: MOVE #CTRS.#C-SRA-RPL-E1-ALL-OTHER TO #OUTPUT.#C-SRA-RPL-E1-ALL-OTHER
                    pnd_Output_Pnd_C_Sra_Rpl_E1_Fl.setValue(pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Fl);                                                                                //Natural: MOVE #CTRS.#C-SRA-RPL-E1-FL TO #OUTPUT.#C-SRA-RPL-E1-FL
                    pnd_Output_Pnd_C_Sra_Rpl_E1_Il1.setValue(pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Il1);                                                                              //Natural: MOVE #CTRS.#C-SRA-RPL-E1-IL1 TO #OUTPUT.#C-SRA-RPL-E1-IL1
                    pnd_Output_Pnd_Letter_Type.setValue(pnd_Prev_Cover);                                                                                                  //Natural: MOVE #PREV-COVER TO #OUTPUT.#LETTER-TYPE
                    if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().equals("0000")))                                                                          //Natural: IF #MDMA211.#O-RETURN-CODE EQ '0000'
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-DATA-TO-FILE
                        sub_Write_Data_To_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ctrs.reset();                                                                                                                                         //Natural: RESET #CTRS #CTR-TOTAL-CONTRACTS #PREV-USE-TIAA-CONTRACT #OUTPUT #RA #GRA #SRA #PREV-USE-CREF-CONTRACT #CONTRACT-TOTAL #RAIL #GRAIL #SRAIL
                pnd_Ctr_Total_Contracts.reset();
                pnd_Prev_Use_Tiaa_Contract.reset();
                pnd_Output.reset();
                pnd_Ra.reset();
                pnd_Gra.reset();
                pnd_Sra.reset();
                pnd_Prev_Use_Cref_Contract.reset();
                pnd_Ctrs_Pnd_Contract_Total.reset();
                pnd_Rail.reset();
                pnd_Grail.reset();
                pnd_Srail.reset();
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT END OF DATA
            if (condition(pnd_Input_Record_Pnd_Pin_N12.equals(new DbsDecimal("999999999999"))))                                                                           //Natural: IF #INPUT-RECORD.#PIN-N12 EQ 999999999999
            {
                pnd_Ctr_Contract_Read_Eof.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-CONTRACT-READ-EOF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  PINEXP
            pnd_Prev_Pin_N12.setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                                                      //Natural: MOVE #INPUT-RECORD.#PIN-N12 TO #PREV-PIN-N12
            pnd_Prev_Cover.setValue(pnd_Input_Record_Pnd_Cover_Letter_Ind);                                                                                               //Natural: MOVE #INPUT-RECORD.#COVER-LETTER-IND TO #PREV-COVER
                                                                                                                                                                          //Natural: PERFORM VALIDATE-RECORD
            sub_Validate_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
        }                                                                                                                                                                 //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-STATISTICS-REPORT
        sub_Write_Statistics_Report();
        if (condition(Global.isEscape())) {return;}
        //*  CLOSE MQ QUEUE
        //*  JCA 20150324
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //* **********************************************************************
        //*                 S T A R T    O F    S U B R O U T I N E S
        //* **********************************************************************
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-RECORD
        //*  RA CONTRACTS
        //*  GRA CONTRACTS
        //*  SRA CONTRACTS
        //*  ---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-RA-CONTRACT-TO-ARRAY
        //*  ---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-GRA-CONTRACT-TO-ARRAY
        //*  ---------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-SRA-CONTRACT-TO-ARRAY
        //*  -------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RECORD-IN-COR
        //*  --------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PIN-STATUS
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATA-TO-FILE
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-STATISTICS-REPORT
        //* ******************************************
    }
    private void sub_Validate_Record() throws Exception                                                                                                                   //Natural: VALIDATE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        if (condition(pnd_Prev_Contract.equals(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr)))                                                                                    //Natural: IF #PREV-CONTRACT EQ #TIAA-CNTRCT-NBR
        {
            pnd_Ctr_Duplicate_Contract.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DUPLICATE-CONTRACT
            pnd_Remarks.setValue("DUPLICATE CONTRACT");                                                                                                                   //Natural: ASSIGN #REMARKS := 'DUPLICATE CONTRACT'
            getReports().write(1, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prev_Contract.setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #PREV-CONTRACT
                                                                                                                                                                          //Natural: PERFORM CHECK-RECORD-IN-COR
        sub_Check_Record_In_Cor();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Valid_Record.getBoolean()))                                                                                                                     //Natural: IF #VALID-RECORD
        {
            pnd_Prev_Use_Tiaa_Contract.setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                                    //Natural: ASSIGN #PREV-USE-TIAA-CONTRACT := #TIAA-CNTRCT-NBR
            pnd_Prev_Use_Cref_Contract.setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                                    //Natural: ASSIGN #PREV-USE-CREF-CONTRACT := #CREF-CNTRCT-NBR
            short decideConditionsMet672 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TIAA-CNTRCT-NBR EQ 'D0100000' THRU 'D9599999' OR #TIAA-CNTRCT-NBR EQ 'C0900000' THRU 'C9999999' OR #TIAA-CNTRCT-NBR EQ 'A0316250' THRU 'A8999999' OR #TIAA-CNTRCT-NBR EQ 'B0000010' THRU 'B7999999' OR #TIAA-CNTRCT-NBR EQ 'B8015000' THRU 'B9999999' OR #TIAA-CNTRCT-NBR EQ 'C0100000' THRU 'C7999999' OR #TIAA-CNTRCT-NBR EQ 'A0000010' THRU 'A0316249'
            if (condition((((((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("D0100000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("D9599999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C0900000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C9999999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("A0316250") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("A8999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("B0000010") 
                && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("B7999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("B8015000") && 
                pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("B9999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("C0100000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("C7999999"))) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("A0000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("A0316249")))))
            {
                decideConditionsMet672++;
                short decideConditionsMet675 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ISSUE-STATE-CDE EQ 'FL'
                if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("FL")))
                {
                    decideConditionsMet675++;
                    pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Fl.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTRS.#T-RA-RPL-E1-FL
                    pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Fl.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTRS.#C-RA-RPL-E1-FL
                    pnd_Ctr_Total_Ra_Fl.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-RA-FL
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'TX'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("TX")))
                {
                    decideConditionsMet675++;
                    pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Tx.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTRS.#T-RA-RPL-E1-TX
                    pnd_Ctrs_Pnd_C_Ra_Rpl_E1_All_Other.nadd(1);                                                                                                           //Natural: ADD 1 TO #CTRS.#C-RA-RPL-E1-ALL-OTHER
                    pnd_Ctr_Total_Ra_Tx.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-RA-TX
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'PR'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("PR")))
                {
                    decideConditionsMet675++;
                    pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTRS.#T-RA-RPL-E1-PR
                    pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Pr.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTRS.#C-RA-RPL-E1-PR
                    pnd_Ctr_Total_Ra_Pr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-RA-PR
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'IL'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("IL")))
                {
                    decideConditionsMet675++;
                    pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Il.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTRS.#T-RA-RPL-E1-IL
                    pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Il1.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#C-RA-RPL-E1-IL1
                    pnd_Ctr_Total_Ra_Il.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-RA-IL
                    pnd_Ctr_Total_Ra_Il1.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-RA-IL1
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'PA'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("PA")))
                {
                    decideConditionsMet675++;
                    pnd_Ctrs_Pnd_T_Ra_Rpl_E1_Pa.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTRS.#T-RA-RPL-E1-PA
                    pnd_Ctrs_Pnd_C_Ra_Rpl_E1_All_Other.nadd(1);                                                                                                           //Natural: ADD 1 TO #CTRS.#C-RA-RPL-E1-ALL-OTHER
                    pnd_Ctr_Total_Ra_Pa.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-RA-PA
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'AR'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("AR")))
                {
                    decideConditionsMet675++;
                    pnd_Ctrs_Pnd_C_Ra_Rpl_E1_Ar.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTRS.#C-RA-RPL-E1-AR
                    pnd_Ctrs_Pnd_T_Ra_Rpl_E1_All_Other.nadd(1);                                                                                                           //Natural: ADD 1 TO #CTRS.#T-RA-RPL-E1-ALL-OTHER
                    pnd_Ctr_Total_Ra_Ar.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CTR-TOTAL-RA-AR
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Ctrs_Pnd_T_Ra_Rpl_E1_All_Other.nadd(1);                                                                                                           //Natural: ADD 1 TO #CTRS.#T-RA-RPL-E1-ALL-OTHER
                    pnd_Ctrs_Pnd_C_Ra_Rpl_E1_All_Other.nadd(1);                                                                                                           //Natural: ADD 1 TO #CTRS.#C-RA-RPL-E1-ALL-OTHER
                    pnd_Ctr_Total_Ra_All_Other.nadd(1);                                                                                                                   //Natural: ADD 1 TO #CTR-TOTAL-RA-ALL-OTHER
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(pnd_Ra.less(50)))                                                                                                                           //Natural: IF #RA < 50
                {
                                                                                                                                                                          //Natural: PERFORM ADD-RA-CONTRACT-TO-ARRAY
                    sub_Add_Ra_Contract_To_Array();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Ctr_Ra_Contract_Accepted.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTR-RA-CONTRACT-ACCEPTED
                    pnd_Ctrs_Pnd_Contract_Total.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CONTRACT-TOTAL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ctr_Ra_Reach_Max_Contracts.nadd(1);                                                                                                               //Natural: ADD 1 TO #CTR-RA-REACH-MAX-CONTRACTS
                    pnd_Remarks.setValue(" *** MAX 50 RA CONTRACTS / PIN - SKIP ***");                                                                                    //Natural: ASSIGN #REMARKS := ' *** MAX 50 RA CONTRACTS / PIN - SKIP ***'
                    getReports().write(1, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                        ColumnSpacing(3),pnd_Remarks);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #TIAA-CNTRCT-NBR EQ '30000100' THRU '39499999' OR #TIAA-CNTRCT-NBR EQ '20000010' THRU '20099999' OR #TIAA-CNTRCT-NBR EQ '20250000' THRU '28999999' OR #TIAA-CNTRCT-NBR EQ '20100000' THRU '20249999'
            else if (condition(((((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("30000100") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("39499999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("20099999"))) || 
                (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20250000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("28999999"))) || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("20100000") 
                && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("20249999")))))
            {
                decideConditionsMet672++;
                short decideConditionsMet720 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ISSUE-STATE-CDE EQ 'FL'
                if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("FL")))
                {
                    decideConditionsMet720++;
                    pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Fl.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#T-GRA-RPL-E1-FL
                    pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Fl.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#C-GRA-RPL-E1-FL
                    pnd_Ctr_Total_Gra_Fl.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-GRA-FL
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'MD'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("MD")))
                {
                    decideConditionsMet720++;
                    pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Md.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#T-GRA-RPL-E1-MD
                    pnd_Ctrs_Pnd_C_Gra_Rpl_E1_All_Other.nadd(1);                                                                                                          //Natural: ADD 1 TO #CTRS.#C-GRA-RPL-E1-ALL-OTHER
                    pnd_Ctr_Total_Gra_Md.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-GRA-MD
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'TX'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("TX")))
                {
                    decideConditionsMet720++;
                    pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Tx.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#T-GRA-RPL-E1-TX
                    pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Tx.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#C-GRA-RPL-E1-TX
                    pnd_Ctr_Total_Gra_Tx.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-GRA-TX
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'PR'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("PR")))
                {
                    decideConditionsMet720++;
                    pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Pr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#T-GRA-RPL-E1-PR
                    pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Pr.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#C-GRA-RPL-E1-PR
                    pnd_Ctr_Total_Gra_Pr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-GRA-PR
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'IL'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("IL")))
                {
                    decideConditionsMet720++;
                    pnd_Ctrs_Pnd_T_Gra_Rpl_E1_Il.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#T-GRA-RPL-E1-IL
                    pnd_Ctrs_Pnd_C_Gra_Rpl_E1_Il.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#C-GRA-RPL-E1-IL
                    pnd_Ctr_Total_Gra_Il.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-GRA-IL
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Ctrs_Pnd_T_Gra_Rpl_E1_All_Other.nadd(1);                                                                                                          //Natural: ADD 1 TO #CTRS.#T-GRA-RPL-E1-ALL-OTHER
                    pnd_Ctrs_Pnd_C_Gra_Rpl_E1_All_Other.nadd(1);                                                                                                          //Natural: ADD 1 TO #CTRS.#C-GRA-RPL-E1-ALL-OTHER
                    pnd_Ctr_Total_Gra_All_Other.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTR-TOTAL-GRA-ALL-OTHER
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(pnd_Gra.less(50)))                                                                                                                          //Natural: IF #GRA < 50
                {
                                                                                                                                                                          //Natural: PERFORM ADD-GRA-CONTRACT-TO-ARRAY
                    sub_Add_Gra_Contract_To_Array();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Ctr_Gra_Contract_Accepted.nadd(1);                                                                                                                //Natural: ADD 1 TO #CTR-GRA-CONTRACT-ACCEPTED
                    pnd_Ctrs_Pnd_Contract_Total.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CONTRACT-TOTAL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ctr_Gra_Reach_Max_Contracts.nadd(1);                                                                                                              //Natural: ADD 1 TO #CTR-GRA-REACH-MAX-CONTRACTS
                    pnd_Remarks.setValue(" *** MAX 50 GRA CONTRACTS / PIN - SKIP ***");                                                                                   //Natural: ASSIGN #REMARKS := ' *** MAX 50 GRA CONTRACTS / PIN - SKIP ***'
                    getReports().write(1, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                        ColumnSpacing(3),pnd_Remarks);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #TIAA-CNTRCT-NBR EQ 'L9000000' THRU 'L9899999' OR #TIAA-CNTRCT-NBR EQ 'K0000010' THRU 'K4999999'
            else if (condition(((pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("L9000000") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("L9899999")) 
                || (pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.greaterOrEqual("K0000010") && pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr.lessOrEqual("K4999999")))))
            {
                decideConditionsMet672++;
                short decideConditionsMet760 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ISSUE-STATE-CDE EQ 'FL'
                if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("FL")))
                {
                    decideConditionsMet760++;
                    pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Fl.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#T-SRA-RPL-E1-FL
                    pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Fl.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#C-SRA-RPL-E1-FL
                    pnd_Ctr_Total_Sra_Fl.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-SRA-FL
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'TX'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("TX")))
                {
                    decideConditionsMet760++;
                    pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Tx.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#T-SRA-RPL-E1-TX
                    pnd_Ctrs_Pnd_C_Sra_Rpl_E1_All_Other.nadd(1);                                                                                                          //Natural: ADD 1 TO #CTRS.#C-SRA-RPL-E1-ALL-OTHER
                    pnd_Ctr_Total_Sra_Tx.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-SRA-TX
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'IL'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("IL")))
                {
                    decideConditionsMet760++;
                    pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Il.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#T-SRA-RPL-E1-IL
                    pnd_Ctrs_Pnd_C_Sra_Rpl_E1_Il1.nadd(1);                                                                                                                //Natural: ADD 1 TO #CTRS.#C-SRA-RPL-E1-IL1
                    pnd_Ctr_Total_Sra_Il.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-SRA-IL
                    pnd_Ctr_Total_Sra_Il1.nadd(1);                                                                                                                        //Natural: ADD 1 TO #CTR-TOTAL-SRA-IL1
                }                                                                                                                                                         //Natural: WHEN #ISSUE-STATE-CDE EQ 'PA'
                else if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("PA")))
                {
                    decideConditionsMet760++;
                    pnd_Ctrs_Pnd_T_Sra_Rpl_E1_Pa.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTRS.#T-SRA-RPL-E1-PA
                    pnd_Ctrs_Pnd_C_Sra_Rpl_E1_All_Other.nadd(1);                                                                                                          //Natural: ADD 1 TO #CTRS.#C-SRA-RPL-E1-ALL-OTHER
                    pnd_Ctr_Total_Sra_Pa.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CTR-TOTAL-SRA-PA
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Ctrs_Pnd_T_Sra_Rpl_E1_All_Other.nadd(1);                                                                                                          //Natural: ADD 1 TO #CTRS.#T-SRA-RPL-E1-ALL-OTHER
                    pnd_Ctrs_Pnd_C_Sra_Rpl_E1_All_Other.nadd(1);                                                                                                          //Natural: ADD 1 TO #CTRS.#C-SRA-RPL-E1-ALL-OTHER
                    pnd_Ctr_Total_Sra_All_Other.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CTR-TOTAL-SRA-ALL-OTHER
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(pnd_Sra.less(50)))                                                                                                                          //Natural: IF #SRA < 50
                {
                                                                                                                                                                          //Natural: PERFORM ADD-SRA-CONTRACT-TO-ARRAY
                    sub_Add_Sra_Contract_To_Array();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Ctr_Sra_Contract_Accepted.nadd(1);                                                                                                                //Natural: ADD 1 TO #CTR-SRA-CONTRACT-ACCEPTED
                    pnd_Ctrs_Pnd_Contract_Total.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CONTRACT-TOTAL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ctr_Sra_Reach_Max_Contracts.nadd(1);                                                                                                              //Natural: ADD 1 TO #CTR-SRA-REACH-MAX-CONTRACTS
                    pnd_Remarks.setValue(" *** MAX 50 SRA CONTRACTS / PIN - SKIP ***");                                                                                   //Natural: ASSIGN #REMARKS := ' *** MAX 50 SRA CONTRACTS / PIN - SKIP ***'
                    getReports().write(1, pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                        ColumnSpacing(3),pnd_Remarks);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Ctr_Contract_Outof_Range.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CTR-CONTRACT-OUTOF-RANGE
                pnd_Remarks.setValue("CONTRACT OUT OF RANGE");                                                                                                            //Natural: ASSIGN #REMARKS := 'CONTRACT OUT OF RANGE'
                getReports().write(1, new ColumnSpacing(1),pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new                     //Natural: WRITE ( 1 ) 1X #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Prev_Use_Tiaa_Contract.greater(" ")))                                                                                                       //Natural: IF #PREV-USE-TIAA-CONTRACT > ' '
            {
                pnd_Output_Pnd_Tiaa_Cntrct_Number.setValue(pnd_Prev_Use_Tiaa_Contract);                                                                                   //Natural: ASSIGN #TIAA-CNTRCT-NUMBER := #PREV-USE-TIAA-CONTRACT
                pnd_Output_Pnd_Cref_Cntrct_Number.setValue(pnd_Prev_Use_Cref_Contract);                                                                                   //Natural: ASSIGN #CREF-CNTRCT-NUMBER := #PREV-USE-CREF-CONTRACT
                pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().setValue("0000");                                                                                           //Natural: ASSIGN #MDMA211.#O-RETURN-CODE := '0000'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE-RECORD
    }
    private void sub_Add_Ra_Contract_To_Array() throws Exception                                                                                                          //Natural: ADD-RA-CONTRACT-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------
        pnd_Ra.nadd(1);                                                                                                                                                   //Natural: ADD 1 TO #RA
        if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                               //Natural: IF #CREF-CNTRCT-NBR NE ' '
        {
            pnd_Output_Pnd_Ra_Contracts.getValue(pnd_Ra).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,                   //Natural: COMPRESS #TIAA-CNTRCT-NBR '/' #CREF-CNTRCT-NBR TO #RA-CONTRACTS ( #RA ) LEAVING NO SPACE
                "/", pnd_Input_Record_Pnd_Cref_Cntrct_Nbr));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_Pnd_Ra_Contracts.getValue(pnd_Ra).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                  //Natural: MOVE #TIAA-CNTRCT-NBR TO #RA-CONTRACTS ( #RA )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("IL")))                                                                                                 //Natural: IF #ISSUE-STATE-CDE EQ 'IL'
        {
            pnd_Rail.nadd(1);                                                                                                                                             //Natural: ASSIGN #RAIL := #RAIL + 1
            pnd_Output_Pnd_Ra_Tiaa_Contracts_Il.getValue(pnd_Rail).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                        //Natural: MOVE #TIAA-CNTRCT-NBR TO #RA-TIAA-CONTRACTS-IL ( #RAIL )
            pnd_Output_Pnd_Ra_Cref_Contracts_Il1.getValue(pnd_Rail).setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                       //Natural: MOVE #CREF-CNTRCT-NBR TO #RA-CREF-CONTRACTS-IL1 ( #RAIL )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-RA-CONTRACT-TO-ARRAY
    }
    private void sub_Add_Gra_Contract_To_Array() throws Exception                                                                                                         //Natural: ADD-GRA-CONTRACT-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------
        pnd_Gra.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #GRA
        if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                               //Natural: IF #CREF-CNTRCT-NBR NE ' '
        {
            pnd_Output_Pnd_Gra_Contracts.getValue(pnd_Gra).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,                 //Natural: COMPRESS #TIAA-CNTRCT-NBR '/' #CREF-CNTRCT-NBR TO #GRA-CONTRACTS ( #GRA ) LEAVING NO SPACE
                "/", pnd_Input_Record_Pnd_Cref_Cntrct_Nbr));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_Pnd_Gra_Contracts.getValue(pnd_Gra).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                //Natural: MOVE #TIAA-CNTRCT-NBR TO #GRA-CONTRACTS ( #GRA )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("IL")))                                                                                                 //Natural: IF #ISSUE-STATE-CDE EQ 'IL'
        {
            pnd_Grail.nadd(1);                                                                                                                                            //Natural: ASSIGN #GRAIL := #GRAIL + 1
            pnd_Output_Pnd_Gra_Tiaa_Contracts_Il.getValue(pnd_Grail).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                      //Natural: MOVE #TIAA-CNTRCT-NBR TO #GRA-TIAA-CONTRACTS-IL ( #GRAIL )
            pnd_Output_Pnd_Gra_Cref_Contracts_Il.getValue(pnd_Grail).setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                      //Natural: MOVE #CREF-CNTRCT-NBR TO #GRA-CREF-CONTRACTS-IL ( #GRAIL )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-GRA-CONTRACT-TO-ARRAY
    }
    private void sub_Add_Sra_Contract_To_Array() throws Exception                                                                                                         //Natural: ADD-SRA-CONTRACT-TO-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------
        pnd_Sra.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #SRA
        if (condition(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr.notEquals(" ")))                                                                                               //Natural: IF #CREF-CNTRCT-NBR NE ' '
        {
            pnd_Output_Pnd_Sra_Contracts.getValue(pnd_Sra).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,                 //Natural: COMPRESS #TIAA-CNTRCT-NBR '/' #CREF-CNTRCT-NBR TO #SRA-CONTRACTS ( #SRA ) LEAVING NO SPACE
                "/", pnd_Input_Record_Pnd_Cref_Cntrct_Nbr));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_Pnd_Sra_Contracts.getValue(pnd_Sra).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                //Natural: MOVE #TIAA-CNTRCT-NBR TO #SRA-CONTRACTS ( #SRA )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Record_Pnd_Issue_State_Cde.equals("IL")))                                                                                                 //Natural: IF #ISSUE-STATE-CDE EQ 'IL'
        {
            pnd_Srail.nadd(1);                                                                                                                                            //Natural: ASSIGN #SRAIL := #SRAIL + 1
            pnd_Output_Pnd_Sra_Tiaa_Contracts_Il.getValue(pnd_Srail).setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                      //Natural: MOVE #TIAA-CNTRCT-NBR TO #SRA-TIAA-CONTRACTS-IL ( #SRAIL )
            pnd_Output_Pnd_Sra_Cref_Contracts_Il1.getValue(pnd_Srail).setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                     //Natural: MOVE #CREF-CNTRCT-NBR TO #SRA-CREF-CONTRACTS-IL1 ( #SRAIL )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADD-SRA-CONTRACT-TO-ARRAY
    }
    private void sub_Check_Record_In_Cor() throws Exception                                                                                                               //Natural: CHECK-RECORD-IN-COR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                 //Natural: MOVE #TIAA-CNTRCT-NBR TO #MDMA211.#I-CONTRACT-NUMBER
        pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue("01");                                                                                                   //Natural: MOVE '01' TO #MDMA211.#I-PAYEE-CODE-A2
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA211.#O-RETURN-CODE NE '0000'
        {
            pnd_Remarks.setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Text());                                                                                          //Natural: ASSIGN #REMARKS := #MDMA211.#O-RETURN-TEXT
            //*  PINEXP
            getReports().write(1, new ColumnSpacing(1),pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) 1X #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Contract_Not_In_Cor.nadd(1);                                                                                                                       //Natural: ADD 1 TO #CTR-DA-CONTRACT-NOT-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals(" ") || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("H")         //Natural: IF NOT ( #MDMA211.#O-CONTRACT-STATUS-CODE = ' ' OR = 'H' OR = 'Y' )
            || pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code().equals("Y"))))
        {
            pnd_Remarks.setValue(DbsUtil.compress("CONTRACT INACTIVE IN COR - STATUS CODE:", pdaMdma211.getPnd_Mdma211_Pnd_O_Contract_Status_Code()));                    //Natural: COMPRESS 'CONTRACT INACTIVE IN COR - STATUS CODE:' #MDMA211.#O-CONTRACT-STATUS-CODE TO #REMARKS
            //*  PINEXP
            getReports().write(1, new ColumnSpacing(1),pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) 1X #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Ctr_Da_Inactive_In_Cor.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CTR-DA-INACTIVE-IN-COR
            pnd_Valid_Record.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #VALID-RECORD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PIN-STATUS
            sub_Determine_Pin_Status();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Accepted.getBoolean()))                                                                                                                     //Natural: IF #ACCEPTED
            {
                pnd_Valid_Record.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #VALID-RECORD
                pnd_Output_Pnd_Tiaa_Cntrct_Number.setValue(pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr);                                                                         //Natural: MOVE #TIAA-CNTRCT-NBR TO #TIAA-CNTRCT-NUMBER
                pnd_Output_Pnd_Cref_Cntrct_Number.setValue(pnd_Input_Record_Pnd_Cref_Cntrct_Nbr);                                                                         //Natural: MOVE #CREF-CNTRCT-NBR TO #CREF-CNTRCT-NUMBER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Valid_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #VALID-RECORD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CHECK-RECORD-IN-COR
    }
    private void sub_Determine_Pin_Status() throws Exception                                                                                                              //Natural: DETERMINE-PIN-STATUS
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------
        //*  PINEXP >>>
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        if (condition(pnd_Input_Record_Pnd_Pin_A12.notEquals(" ")))                                                                                                       //Natural: IF #INPUT-RECORD.#PIN-A12 NE ' '
        {
            if (condition(pnd_Input_Record_Pnd_Pin_N5.equals(getZero())))                                                                                                 //Natural: IF #INPUT-RECORD.#PIN-N5 = 0
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N7);                                                                          //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #INPUT-RECORD.#PIN-N7
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pnd_Input_Record_Pnd_Pin_N12);                                                                         //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #INPUT-RECORD.#PIN-N12
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  REJECT DECEASED PARTICIPANTS
        //*  PINEXP
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death().greater(getZero())))                                                                                //Natural: IF #MDMA101.#O-DATE-OF-DEATH GT 0
        {
            pnd_Ctr_Ph_Deceased.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CTR-PH-DECEASED
            pnd_Remarks.setValue(DbsUtil.compress("P/H DECEASED - DOD:", pdaMdma101.getPnd_Mdma101_Pnd_O_Date_Of_Death()));                                               //Natural: COMPRESS 'P/H DECEASED - DOD:' #MDMA101.#O-DATE-OF-DEATH TO #REMARKS
            getReports().write(1, new ColumnSpacing(1),pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new  //Natural: WRITE ( 1 ) 1X #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                ColumnSpacing(3),pnd_Remarks);
            if (Global.isEscape()) return;
            pnd_Accepted.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #ACCEPTED
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  REJECT PARTICIPANTS W/ STOP ALL MAILING SUPPRESSION CODE
            //*  PINEXP
            DbsUtil.examine(new ExamineSource(pdaMdma101.getPnd_Mdma101_Pnd_O_Mail_Code().getValue("*")), new ExamineSearch("01"), new ExamineGivingPosition(pnd_I));     //Natural: EXAMINE #MDMA101.#O-MAIL-CODE ( * ) FOR '01' GIVING POSITION #I
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I GT 0
            {
                pnd_Ctr_Ph_With_Mail_Suppression.nadd(1);                                                                                                                 //Natural: ADD 1 TO #CTR-PH-WITH-MAIL-SUPPRESSION
                pnd_Remarks.setValue(DbsUtil.compress("P/H MAIL SUPPRESSED - MAIL CODE: 01"));                                                                            //Natural: COMPRESS 'P/H MAIL SUPPRESSED - MAIL CODE: 01' TO #REMARKS
                getReports().write(1, new ColumnSpacing(1),pnd_Input_Record_Pnd_Pin_A12,new ColumnSpacing(3),pnd_Input_Record_Pnd_Tiaa_Cntrct_Nbr,new                     //Natural: WRITE ( 1 ) 1X #INPUT-RECORD.#PIN-A12 3X #TIAA-CNTRCT-NBR 5X #ISSUE-STATE-CDE 3X #REMARKS
                    ColumnSpacing(5),pnd_Input_Record_Pnd_Issue_State_Cde,new ColumnSpacing(3),pnd_Remarks);
                if (Global.isEscape()) return;
                pnd_Accepted.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #ACCEPTED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Accepted.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #ACCEPTED
                pnd_Output_Pnd_Full_Name.setValue(DbsUtil.compress(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name(), pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name(),           //Natural: COMPRESS #O-FIRST-NAME #O-MIDDLE-NAME #O-LAST-NAME INTO #FULL-NAME
                    pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name()));
                pnd_Output_Pnd_Last_Name.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                                                           //Natural: ASSIGN #LAST-NAME := #O-LAST-NAME
                pnd_Output_Pnd_Address_Line_Txt.getValue(1).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_1());                                              //Natural: ASSIGN #ADDRESS-LINE-TXT ( 1 ) := #O-BASE-ADDRESS-LINE-1
                pnd_Output_Pnd_Address_Line_Txt.getValue(2).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_2());                                              //Natural: ASSIGN #ADDRESS-LINE-TXT ( 2 ) := #O-BASE-ADDRESS-LINE-2
                pnd_Output_Pnd_Address_Line_Txt.getValue(3).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_3());                                              //Natural: ASSIGN #ADDRESS-LINE-TXT ( 3 ) := #O-BASE-ADDRESS-LINE-3
                pnd_Output_Pnd_Address_Line_Txt.getValue(4).setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Line_4());                                              //Natural: ASSIGN #ADDRESS-LINE-TXT ( 4 ) := #O-BASE-ADDRESS-LINE-4
                pnd_Output_Pnd_Postal_Data.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Postal_Data());                                                          //Natural: ASSIGN #POSTAL-DATA := #O-BASE-ADDRESS-POSTAL-DATA
                pnd_Output_Pnd_Address_Type_Cde.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Base_Address_Type_Code());                                                       //Natural: ASSIGN #ADDRESS-TYPE-CDE := #O-BASE-ADDRESS-TYPE-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE-PIN-STATUS
    }
    private void sub_Write_Data_To_File() throws Exception                                                                                                                //Natural: WRITE-DATA-TO-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        pnd_Output_Pnd_Package_Code.setValue("RDR2013A");                                                                                                                 //Natural: MOVE 'RDR2013A' TO #PACKAGE-CODE
        pnd_Ctr_File1.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #CTR-FILE1
        getWorkFiles().write(2, false, pnd_Output);                                                                                                                       //Natural: WRITE WORK FILE 2 #OUTPUT
        //*  WRITE-DATA-TO-FILE
    }
    private void sub_Write_Statistics_Report() throws Exception                                                                                                           //Natural: WRITE-STATISTICS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ctr_Contract_Rejected.compute(new ComputeParameters(false, pnd_Ctr_Contract_Rejected), pnd_Ctr_Da_Contract_Not_In_Cor.add(pnd_Ctr_Da_Inactive_In_Cor).add(pnd_Ctr_Ph_Deceased).add(pnd_Ctr_Ph_With_Mail_Suppression).add(pnd_Ctr_Out_Of_State).add(pnd_Ctr_Contract_Outof_Range).add(pnd_Ctr_Duplicate_Contract).add(pnd_Ctr_Ra_Reach_Max_Contracts).add(pnd_Ctr_Gra_Reach_Max_Contracts).add(pnd_Ctr_Sra_Reach_Max_Contracts)); //Natural: ASSIGN #CTR-CONTRACT-REJECTED := #CTR-DA-CONTRACT-NOT-IN-COR + #CTR-DA-INACTIVE-IN-COR + #CTR-PH-DECEASED + #CTR-PH-WITH-MAIL-SUPPRESSION + #CTR-OUT-OF-STATE + #CTR-CONTRACT-OUTOF-RANGE + #CTR-DUPLICATE-CONTRACT + #CTR-RA-REACH-MAX-CONTRACTS + #CTR-GRA-REACH-MAX-CONTRACTS + #CTR-SRA-REACH-MAX-CONTRACTS
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS READ         :",pnd_Ctr_Rpl_Contract_Read);                                                                              //Natural: WRITE 'NUMBER OF RECORDS READ         :' #CTR-RPL-CONTRACT-READ
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RA RECORDS ACCEPTED  :",pnd_Ctr_Ra_Contract_Accepted);                                                                           //Natural: WRITE 'NUMBER OF RA RECORDS ACCEPTED  :' #CTR-RA-CONTRACT-ACCEPTED
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF GRA RECORDS ACCEPTED :",pnd_Ctr_Gra_Contract_Accepted);                                                                          //Natural: WRITE 'NUMBER OF GRA RECORDS ACCEPTED :' #CTR-GRA-CONTRACT-ACCEPTED
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF SRA RECORDS ACCEPTED :",pnd_Ctr_Sra_Contract_Accepted);                                                                          //Natural: WRITE 'NUMBER OF SRA RECORDS ACCEPTED :' #CTR-SRA-CONTRACT-ACCEPTED
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF EOF RECORDS-99999999 :",pnd_Ctr_Contract_Read_Eof);                                                                              //Natural: WRITE 'NUMBER OF EOF RECORDS-99999999 :' #CTR-CONTRACT-READ-EOF
        if (Global.isEscape()) return;
        getReports().write(0, "NUMBER OF RECORDS REJECTED     :",pnd_Ctr_Contract_Rejected);                                                                              //Natural: WRITE 'NUMBER OF RECORDS REJECTED     :' #CTR-CONTRACT-REJECTED
        if (Global.isEscape()) return;
        getReports().write(0, "BREAKDOWN OF RECORDS REJECTED  :");                                                                                                        //Natural: WRITE 'BREAKDOWN OF RECORDS REJECTED  :'
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT NOT IN COR     :",pnd_Ctr_Da_Contract_Not_In_Cor);                                                                              //Natural: WRITE ' -CONTRACT NOT IN COR     :' #CTR-DA-CONTRACT-NOT-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT INACTIVE IN COR:",pnd_Ctr_Da_Inactive_In_Cor);                                                                                  //Natural: WRITE ' -CONTRACT INACTIVE IN COR:' #CTR-DA-INACTIVE-IN-COR
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H DECEASED            :",pnd_Ctr_Ph_Deceased);                                                                                         //Natural: WRITE ' -P/H DECEASED            :' #CTR-PH-DECEASED
        if (Global.isEscape()) return;
        getReports().write(0, " -P/H W/ MAIL SUPPRESSION :",pnd_Ctr_Ph_With_Mail_Suppression);                                                                            //Natural: WRITE ' -P/H W/ MAIL SUPPRESSION :' #CTR-PH-WITH-MAIL-SUPPRESSION
        if (Global.isEscape()) return;
        getReports().write(0, " -CONTRACT OUT OF RANGE   :",pnd_Ctr_Contract_Outof_Range);                                                                                //Natural: WRITE ' -CONTRACT OUT OF RANGE   :' #CTR-CONTRACT-OUTOF-RANGE
        if (Global.isEscape()) return;
        getReports().write(0, " -DUPLICATE CONTRACT      :",pnd_Ctr_Duplicate_Contract);                                                                                  //Natural: WRITE ' -DUPLICATE CONTRACT      :' #CTR-DUPLICATE-CONTRACT
        if (Global.isEscape()) return;
        getReports().write(0, " -RA REACHED MAX CONTR    :",pnd_Ctr_Ra_Reach_Max_Contracts);                                                                              //Natural: WRITE ' -RA REACHED MAX CONTR    :' #CTR-RA-REACH-MAX-CONTRACTS
        if (Global.isEscape()) return;
        getReports().write(0, " -GRA REACHED MAX CONTR   :",pnd_Ctr_Gra_Reach_Max_Contracts);                                                                             //Natural: WRITE ' -GRA REACHED MAX CONTR   :' #CTR-GRA-REACH-MAX-CONTRACTS
        if (Global.isEscape()) return;
        getReports().write(0, " -SRA REACHED MAX CONTR   :",pnd_Ctr_Sra_Reach_Max_Contracts);                                                                             //Natural: WRITE ' -SRA REACHED MAX CONTR   :' #CTR-SRA-REACH-MAX-CONTRACTS
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL BY STATE AND PRODUCT: ");                                                                                                            //Natural: WRITE 'TOTAL BY STATE AND PRODUCT: '
        if (Global.isEscape()) return;
        getReports().write(0, " - FL  - RA         :",pnd_Ctr_Total_Ra_Fl);                                                                                               //Natural: WRITE ' - FL  - RA         :' #CTR-TOTAL-RA-FL
        if (Global.isEscape()) return;
        getReports().write(0, " - TX  - RA         :",pnd_Ctr_Total_Ra_Tx);                                                                                               //Natural: WRITE ' - TX  - RA         :' #CTR-TOTAL-RA-TX
        if (Global.isEscape()) return;
        getReports().write(0, " - PR  - RA         :",pnd_Ctr_Total_Ra_Pr);                                                                                               //Natural: WRITE ' - PR  - RA         :' #CTR-TOTAL-RA-PR
        if (Global.isEscape()) return;
        getReports().write(0, " - IL  - RA         :",pnd_Ctr_Total_Ra_Il);                                                                                               //Natural: WRITE ' - IL  - RA         :' #CTR-TOTAL-RA-IL
        if (Global.isEscape()) return;
        getReports().write(0, " - IL1 - RA         :",pnd_Ctr_Total_Ra_Il1);                                                                                              //Natural: WRITE ' - IL1 - RA         :' #CTR-TOTAL-RA-IL1
        if (Global.isEscape()) return;
        getReports().write(0, " - PA  - RA         :",pnd_Ctr_Total_Ra_Pa);                                                                                               //Natural: WRITE ' - PA  - RA         :' #CTR-TOTAL-RA-PA
        if (Global.isEscape()) return;
        getReports().write(0, " - AR  - RA         :",pnd_Ctr_Total_Ra_Ar);                                                                                               //Natural: WRITE ' - AR  - RA         :' #CTR-TOTAL-RA-AR
        if (Global.isEscape()) return;
        getReports().write(0, " - ALL OTHER STATES :",pnd_Ctr_Total_Ra_All_Other);                                                                                        //Natural: WRITE ' - ALL OTHER STATES :' #CTR-TOTAL-RA-ALL-OTHER
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, " - FL  - GRA        :",pnd_Ctr_Total_Gra_Fl);                                                                                              //Natural: WRITE ' - FL  - GRA        :' #CTR-TOTAL-GRA-FL
        if (Global.isEscape()) return;
        getReports().write(0, " - MD  - GRA        :",pnd_Ctr_Total_Gra_Md);                                                                                              //Natural: WRITE ' - MD  - GRA        :' #CTR-TOTAL-GRA-MD
        if (Global.isEscape()) return;
        getReports().write(0, " - TX  - GRA        :",pnd_Ctr_Total_Gra_Tx);                                                                                              //Natural: WRITE ' - TX  - GRA        :' #CTR-TOTAL-GRA-TX
        if (Global.isEscape()) return;
        getReports().write(0, " - PR  - GRA        :",pnd_Ctr_Total_Gra_Pr);                                                                                              //Natural: WRITE ' - PR  - GRA        :' #CTR-TOTAL-GRA-PR
        if (Global.isEscape()) return;
        getReports().write(0, " - IL  - GRA        :",pnd_Ctr_Total_Gra_Il);                                                                                              //Natural: WRITE ' - IL  - GRA        :' #CTR-TOTAL-GRA-IL
        if (Global.isEscape()) return;
        getReports().write(0, " - ALL OTHER STATES :",pnd_Ctr_Total_Gra_All_Other);                                                                                       //Natural: WRITE ' - ALL OTHER STATES :' #CTR-TOTAL-GRA-ALL-OTHER
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, " - FL  - SRA        :",pnd_Ctr_Total_Sra_Fl);                                                                                              //Natural: WRITE ' - FL  - SRA        :' #CTR-TOTAL-SRA-FL
        if (Global.isEscape()) return;
        getReports().write(0, " - TX  - SRA        :",pnd_Ctr_Total_Sra_Tx);                                                                                              //Natural: WRITE ' - TX  - SRA        :' #CTR-TOTAL-SRA-TX
        if (Global.isEscape()) return;
        getReports().write(0, " - IL  - SRA        :",pnd_Ctr_Total_Sra_Il);                                                                                              //Natural: WRITE ' - IL  - SRA        :' #CTR-TOTAL-SRA-IL
        if (Global.isEscape()) return;
        getReports().write(0, " - IL1 - SRA        :",pnd_Ctr_Total_Sra_Il1);                                                                                             //Natural: WRITE ' - IL1 - SRA        :' #CTR-TOTAL-SRA-IL1
        if (Global.isEscape()) return;
        getReports().write(0, " - PA  - SRA        :",pnd_Ctr_Total_Sra_Pa);                                                                                              //Natural: WRITE ' - PA  - SRA        :' #CTR-TOTAL-SRA-PA
        if (Global.isEscape()) return;
        getReports().write(0, " - ALL OTHER STATES :",pnd_Ctr_Total_Sra_All_Other);                                                                                       //Natural: WRITE ' - ALL OTHER STATES :' #CTR-TOTAL-SRA-ALL-OTHER
        if (Global.isEscape()) return;
        getReports().write(0, " ----------------------------------");                                                                                                     //Natural: WRITE ' ----------------------------------'
        if (Global.isEscape()) return;
        //*  WRITE-STATISTICS-REPORT
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=120 PS=60");
        Global.format(0, "LS=80 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(43),"RPL LOAN   MAILING",new 
            TabSetting(84),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(43),"EXCEPTIONS LIST",new TabSetting(84),"TIME:",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,new TabSetting(84),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,new TabSetting(2),"PIN            TIAA NBR   STATE   REMARKS",NEWLINE,new 
            TabSetting(2),"------------------------------------------------------------",NEWLINE);
    }
}
