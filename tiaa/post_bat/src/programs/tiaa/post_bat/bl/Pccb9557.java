/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:16:55 PM
**        * FROM NATURAL PROGRAM : Pccb9557
************************************************************
**        * FILE NAME            : Pccb9557.java
**        * CLASS NAME           : Pccb9557
**        * INSTANCE NAME        : Pccb9557
************************************************************
************************************************************************
*  PROD.NDM.NY.PROCESS.LIB(DLNPDCN?) = PROD LIKE DLNPDCS1
*  CREATE DLNPDCN1 THRU DLNPDCN9 FOR 1 TO 9 COPY COMMANDS.
************************************************************************
* PROGRAM  : PCCB9557
* SYSTEM   : PROJPST
* TITLE    : POST / DCS NDM
* FUNCTION : THIS MODULE BUILDS CONNECT:DIRECT CONTROL CARDS FOR DCS.
*          : USED FOR CREATING MULTIPLE COPY COMMANDS FOR SENDING
*          : NUMEROUS XML FILES TO DCS SERVER.
* CREATED  : AUGUST, 2014 (CLONED FROM PCCB9556)
*          :
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
* J. CRUZ  08/2014  INITIAL CODING
* B.NEWSOM 07/2017  CHANGE PATH FROM DCS TO CCP             (PFDC)
* **********************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Pccb9557 extends BLNatBase
{
    // Data Areas
    private PdaPsta9670 pdaPsta9670;
    private PdaCwfpda_M pdaCwfpda_M;
    private LdaPstl9902 ldaPstl9902;
    private PdaPsta9200 pdaPsta9200;
    private PdaPsta9017 pdaPsta9017;
    private LdaPstl7212 ldaPstl7212;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Parameters;
    private DbsField pnd_Input_Parameters_Pnd_Input_Hlq;
    private DbsField pnd_Input_Parameters_Pnd_Input_Prev_Jobname;
    private DbsField pnd_Input_Parameters_Pnd_Input_Package_Cde;
    private DbsField pnd_Input_Parameters_Pnd_Input_Delivery_Cde;
    private DbsField pnd_Input_Parameters_Pnd_Input_Printer_Dest;
    private DbsField pnd_Input_Parameters_Pnd_Input_Parmlib;
    private DbsField pnd_Input_Parameters_Pnd_Input_Folder;
    private DbsField pnd_Input_Dsn_From;
    private DbsField pnd_Input_Dsn_To;
    private DbsField pnd_Dest_Path;
    private DbsField pnd_Sys_Subsys;
    private DbsField pnd_Dts;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_1;
    private DbsField pnd_Date_Pnd_Cc;
    private DbsField pnd_Date_Pnd_Yymmdd;
    private DbsField pnd_Time;
    private DbsField pnd_Date_5;
    private DbsField pnd_Pkg_Parm80;

    private DbsGroup pnd_Pkg_Parm80__R_Field_2;
    private DbsField pnd_Pkg_Parm80_Pnd_Pp80_Package;
    private DbsField pnd_Pkg_Parm80_Pnd_Pp80_Sys_Subsys;
    private DbsField pnd_Pkg_Parm80_Pnd_Pp80_Filler;
    private DbsField pnd_Pkg_Parm80_Pnd_Pp80_Lights_On;
    private DbsField pnd_Data_In;
    private DbsField pnd_Cis_Unique_Rec;

    private DbsGroup pnd_Cis_Unique_Rec__R_Field_3;
    private DbsField pnd_Cis_Unique_Rec_Pnd_Cis_Filler;
    private DbsField pnd_Cis_Unique_Rec_Pnd_Cis_Profile;
    private DbsField pnd_Netchg_App_Rec;

    private DbsGroup pnd_Netchg_App_Rec__R_Field_4;
    private DbsField pnd_Netchg_App_Rec_Pnd_Net_Profile;
    private DbsField pnd_Netchg_App_Rec_Pnd_Net_App_Id;
    private DbsField pnd_Paqtrly_App_Rec;

    private DbsGroup pnd_Paqtrly_App_Rec__R_Field_5;
    private DbsField pnd_Paqtrly_App_Rec_Pnd_Paq_Profile;
    private DbsField pnd_Paqtrly_App_Rec_Pnd_Paq_App_Id;
    private DbsField pnd_Paqtrly_App_Rec_Pnd_Paq_Sys_Subsys;

    private DbsGroup pnd_Profile_Data_In;
    private DbsField pnd_Profile_Data_In_Pnd_Profile_Id;
    private DbsField pnd_Profile_Data_In_Pnd_Profile_App;
    private DbsField pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys;
    private DbsField pnd_Ndm_Control_Card_Output;

    private DataAccessProgramView vw_mail_Item;
    private DbsField mail_Item_Pst_Rqst_Id;
    private DbsField mail_Item_Pckge_Cde;
    private DbsField mail_Item_Alt_Dlvry_Addr;
    private DbsField pnd_Prof_Id;
    private DbsField pnd_Prof_App;
    private DbsField pnd_Prof_Sys_Subsys;
    private DbsField pnd_Workfile;
    private DbsField pnd_In_Profile;
    private DbsField pnd_Job_Name;
    private DbsField pnd_Found_Pkg;
    private DbsField pnd_I;
    private DbsField pnd_P;

    private DbsGroup error_Handler_Fields;
    private DbsField error_Handler_Fields_Pnd_Error_Nr;
    private DbsField error_Handler_Fields_Pnd_Error_Line;
    private DbsField error_Handler_Fields_Pnd_Error_Status;
    private DbsField error_Handler_Fields_Pnd_Error_Program;
    private DbsField error_Handler_Fields_Pnd_Error_Level;
    private DbsField error_Handler_Fields_Pnd_Error_Appl;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta9670 = new PdaPsta9670(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        ldaPstl9902 = new LdaPstl9902();
        registerRecord(ldaPstl9902);
        pdaPsta9200 = new PdaPsta9200(localVariables);
        pdaPsta9017 = new PdaPsta9017(localVariables);
        ldaPstl7212 = new LdaPstl7212();
        registerRecord(ldaPstl7212);
        registerRecord(ldaPstl7212.getVw_pst_Stndrd_Pckge());

        // Local Variables

        pnd_Input_Parameters = localVariables.newGroupInRecord("pnd_Input_Parameters", "#INPUT-PARAMETERS");
        pnd_Input_Parameters_Pnd_Input_Hlq = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_Input_Hlq", "#INPUT-HLQ", FieldType.STRING, 
            15);
        pnd_Input_Parameters_Pnd_Input_Prev_Jobname = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_Input_Prev_Jobname", "#INPUT-PREV-JOBNAME", 
            FieldType.STRING, 8);
        pnd_Input_Parameters_Pnd_Input_Package_Cde = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_Input_Package_Cde", "#INPUT-PACKAGE-CDE", 
            FieldType.STRING, 8);
        pnd_Input_Parameters_Pnd_Input_Delivery_Cde = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_Input_Delivery_Cde", "#INPUT-DELIVERY-CDE", 
            FieldType.STRING, 3);
        pnd_Input_Parameters_Pnd_Input_Printer_Dest = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_Input_Printer_Dest", "#INPUT-PRINTER-DEST", 
            FieldType.STRING, 8);
        pnd_Input_Parameters_Pnd_Input_Parmlib = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_Input_Parmlib", "#INPUT-PARMLIB", FieldType.STRING, 
            52);
        pnd_Input_Parameters_Pnd_Input_Folder = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_Input_Folder", "#INPUT-FOLDER", FieldType.STRING, 
            10);
        pnd_Input_Dsn_From = localVariables.newFieldInRecord("pnd_Input_Dsn_From", "#INPUT-DSN-FROM", FieldType.STRING, 52);
        pnd_Input_Dsn_To = localVariables.newFieldInRecord("pnd_Input_Dsn_To", "#INPUT-DSN-TO", FieldType.STRING, 80);
        pnd_Dest_Path = localVariables.newFieldInRecord("pnd_Dest_Path", "#DEST-PATH", FieldType.STRING, 60);
        pnd_Sys_Subsys = localVariables.newFieldInRecord("pnd_Sys_Subsys", "#SYS-SUBSYS", FieldType.STRING, 6);
        pnd_Dts = localVariables.newFieldInRecord("pnd_Dts", "#DTS", FieldType.STRING, 12);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Date__R_Field_1", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Cc = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Cc", "#CC", FieldType.STRING, 2);
        pnd_Date_Pnd_Yymmdd = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Yymmdd", "#YYMMDD", FieldType.STRING, 6);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.STRING, 7);
        pnd_Date_5 = localVariables.newFieldInRecord("pnd_Date_5", "#DATE-5", FieldType.STRING, 5);
        pnd_Pkg_Parm80 = localVariables.newFieldInRecord("pnd_Pkg_Parm80", "#PKG-PARM80", FieldType.STRING, 80);

        pnd_Pkg_Parm80__R_Field_2 = localVariables.newGroupInRecord("pnd_Pkg_Parm80__R_Field_2", "REDEFINE", pnd_Pkg_Parm80);
        pnd_Pkg_Parm80_Pnd_Pp80_Package = pnd_Pkg_Parm80__R_Field_2.newFieldInGroup("pnd_Pkg_Parm80_Pnd_Pp80_Package", "#PP80-PACKAGE", FieldType.STRING, 
            8);
        pnd_Pkg_Parm80_Pnd_Pp80_Sys_Subsys = pnd_Pkg_Parm80__R_Field_2.newFieldInGroup("pnd_Pkg_Parm80_Pnd_Pp80_Sys_Subsys", "#PP80-SYS-SUBSYS", FieldType.STRING, 
            6);
        pnd_Pkg_Parm80_Pnd_Pp80_Filler = pnd_Pkg_Parm80__R_Field_2.newFieldInGroup("pnd_Pkg_Parm80_Pnd_Pp80_Filler", "#PP80-FILLER", FieldType.STRING, 
            15);
        pnd_Pkg_Parm80_Pnd_Pp80_Lights_On = pnd_Pkg_Parm80__R_Field_2.newFieldInGroup("pnd_Pkg_Parm80_Pnd_Pp80_Lights_On", "#PP80-LIGHTS-ON", FieldType.STRING, 
            1);
        pnd_Data_In = localVariables.newFieldInRecord("pnd_Data_In", "#DATA-IN", FieldType.STRING, 20);
        pnd_Cis_Unique_Rec = localVariables.newFieldInRecord("pnd_Cis_Unique_Rec", "#CIS-UNIQUE-REC", FieldType.STRING, 4);

        pnd_Cis_Unique_Rec__R_Field_3 = localVariables.newGroupInRecord("pnd_Cis_Unique_Rec__R_Field_3", "REDEFINE", pnd_Cis_Unique_Rec);
        pnd_Cis_Unique_Rec_Pnd_Cis_Filler = pnd_Cis_Unique_Rec__R_Field_3.newFieldInGroup("pnd_Cis_Unique_Rec_Pnd_Cis_Filler", "#CIS-FILLER", FieldType.STRING, 
            2);
        pnd_Cis_Unique_Rec_Pnd_Cis_Profile = pnd_Cis_Unique_Rec__R_Field_3.newFieldInGroup("pnd_Cis_Unique_Rec_Pnd_Cis_Profile", "#CIS-PROFILE", FieldType.STRING, 
            2);
        pnd_Netchg_App_Rec = localVariables.newFieldInRecord("pnd_Netchg_App_Rec", "#NETCHG-APP-REC", FieldType.STRING, 10);

        pnd_Netchg_App_Rec__R_Field_4 = localVariables.newGroupInRecord("pnd_Netchg_App_Rec__R_Field_4", "REDEFINE", pnd_Netchg_App_Rec);
        pnd_Netchg_App_Rec_Pnd_Net_Profile = pnd_Netchg_App_Rec__R_Field_4.newFieldInGroup("pnd_Netchg_App_Rec_Pnd_Net_Profile", "#NET-PROFILE", FieldType.STRING, 
            2);
        pnd_Netchg_App_Rec_Pnd_Net_App_Id = pnd_Netchg_App_Rec__R_Field_4.newFieldInGroup("pnd_Netchg_App_Rec_Pnd_Net_App_Id", "#NET-APP-ID", FieldType.STRING, 
            8);
        pnd_Paqtrly_App_Rec = localVariables.newFieldInRecord("pnd_Paqtrly_App_Rec", "#PAQTRLY-APP-REC", FieldType.STRING, 16);

        pnd_Paqtrly_App_Rec__R_Field_5 = localVariables.newGroupInRecord("pnd_Paqtrly_App_Rec__R_Field_5", "REDEFINE", pnd_Paqtrly_App_Rec);
        pnd_Paqtrly_App_Rec_Pnd_Paq_Profile = pnd_Paqtrly_App_Rec__R_Field_5.newFieldInGroup("pnd_Paqtrly_App_Rec_Pnd_Paq_Profile", "#PAQ-PROFILE", FieldType.STRING, 
            2);
        pnd_Paqtrly_App_Rec_Pnd_Paq_App_Id = pnd_Paqtrly_App_Rec__R_Field_5.newFieldInGroup("pnd_Paqtrly_App_Rec_Pnd_Paq_App_Id", "#PAQ-APP-ID", FieldType.STRING, 
            8);
        pnd_Paqtrly_App_Rec_Pnd_Paq_Sys_Subsys = pnd_Paqtrly_App_Rec__R_Field_5.newFieldInGroup("pnd_Paqtrly_App_Rec_Pnd_Paq_Sys_Subsys", "#PAQ-SYS-SUBSYS", 
            FieldType.STRING, 6);

        pnd_Profile_Data_In = localVariables.newGroupInRecord("pnd_Profile_Data_In", "#PROFILE-DATA-IN");
        pnd_Profile_Data_In_Pnd_Profile_Id = pnd_Profile_Data_In.newFieldInGroup("pnd_Profile_Data_In_Pnd_Profile_Id", "#PROFILE-ID", FieldType.STRING, 
            2);
        pnd_Profile_Data_In_Pnd_Profile_App = pnd_Profile_Data_In.newFieldInGroup("pnd_Profile_Data_In_Pnd_Profile_App", "#PROFILE-APP", FieldType.STRING, 
            8);
        pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys = pnd_Profile_Data_In.newFieldInGroup("pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys", "#PROFILE-SYS-SUBSYS", 
            FieldType.STRING, 6);
        pnd_Ndm_Control_Card_Output = localVariables.newFieldInRecord("pnd_Ndm_Control_Card_Output", "#NDM-CONTROL-CARD-OUTPUT", FieldType.STRING, 100);

        vw_mail_Item = new DataAccessProgramView(new NameInfo("vw_mail_Item", "MAIL-ITEM"), "PST_OUTGOING_MAIL_ITEM", "PST_REQUEST_DATA");
        mail_Item_Pst_Rqst_Id = vw_mail_Item.getRecord().newFieldInGroup("mail_Item_Pst_Rqst_Id", "PST-RQST-ID", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "PST_RQST_ID");
        mail_Item_Pst_Rqst_Id.setDdmHeader("EXTERNAL/REQUEST/ID");
        mail_Item_Pckge_Cde = vw_mail_Item.getRecord().newFieldInGroup("mail_Item_Pckge_Cde", "PCKGE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "PCKGE_CDE");
        mail_Item_Pckge_Cde.setDdmHeader("PACKAGE/ID");
        mail_Item_Alt_Dlvry_Addr = vw_mail_Item.getRecord().newFieldInGroup("mail_Item_Alt_Dlvry_Addr", "ALT-DLVRY-ADDR", FieldType.STRING, 100, RepeatingFieldStrategy.None, 
            "ALT_DLVRY_ADDR");
        registerRecord(vw_mail_Item);

        pnd_Prof_Id = localVariables.newFieldArrayInRecord("pnd_Prof_Id", "#PROF-ID", FieldType.STRING, 2, new DbsArrayController(1, 28));
        pnd_Prof_App = localVariables.newFieldArrayInRecord("pnd_Prof_App", "#PROF-APP", FieldType.STRING, 8, new DbsArrayController(1, 28));
        pnd_Prof_Sys_Subsys = localVariables.newFieldArrayInRecord("pnd_Prof_Sys_Subsys", "#PROF-SYS-SUBSYS", FieldType.STRING, 6, new DbsArrayController(1, 
            28));
        pnd_Workfile = localVariables.newFieldInRecord("pnd_Workfile", "#WORKFILE", FieldType.PACKED_DECIMAL, 2);
        pnd_In_Profile = localVariables.newFieldInRecord("pnd_In_Profile", "#IN-PROFILE", FieldType.STRING, 2);
        pnd_Job_Name = localVariables.newFieldInRecord("pnd_Job_Name", "#JOB-NAME", FieldType.STRING, 8);
        pnd_Found_Pkg = localVariables.newFieldInRecord("pnd_Found_Pkg", "#FOUND-PKG", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.PACKED_DECIMAL, 3);

        error_Handler_Fields = localVariables.newGroupInRecord("error_Handler_Fields", "ERROR-HANDLER-FIELDS");
        error_Handler_Fields_Pnd_Error_Nr = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Nr", "#ERROR-NR", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Line = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Line", "#ERROR-LINE", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Status = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Status", "#ERROR-STATUS", FieldType.STRING, 
            1);
        error_Handler_Fields_Pnd_Error_Program = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Program", "#ERROR-PROGRAM", FieldType.STRING, 
            8);
        error_Handler_Fields_Pnd_Error_Level = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Level", "#ERROR-LEVEL", FieldType.NUMERIC, 
            2);
        error_Handler_Fields_Pnd_Error_Appl = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Appl", "#ERROR-APPL", FieldType.STRING, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_mail_Item.reset();

        ldaPstl9902.initializeValues();
        ldaPstl7212.initializeValues();

        localVariables.reset();
        error_Handler_Fields_Pnd_Error_Status.setInitialValue("O");
        error_Handler_Fields_Pnd_Error_Program.setInitialValue(Global.getPROGRAM());
        error_Handler_Fields_Pnd_Error_Appl.setInitialValue("POST");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Pccb9557() throws Exception
    {
        super("Pccb9557");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 80 PS = 60;//Natural: FORMAT ( 0 ) PS = 60 LS = 132
                                                                                                                                                                          //Natural: PERFORM GET-INPUT-PARAMETERS
        sub_Get_Input_Parameters();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK 02 #PROFILE-DATA-IN
        while (condition(getWorkFiles().read(2, pnd_Profile_Data_In)))
        {
            pnd_P.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #P
            pnd_Prof_Id.getValue(pnd_P).setValue(pnd_Profile_Data_In_Pnd_Profile_Id);                                                                                     //Natural: ASSIGN #PROF-ID ( #P ) := #PROFILE-ID
            pnd_Prof_App.getValue(pnd_P).setValue(pnd_Profile_Data_In_Pnd_Profile_App);                                                                                   //Natural: ASSIGN #PROF-APP ( #P ) := #PROFILE-APP
            pnd_Prof_Sys_Subsys.getValue(pnd_P).setValue(pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys);                                                                     //Natural: ASSIGN #PROF-SYS-SUBSYS ( #P ) := #PROFILE-SYS-SUBSYS
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        ldaPstl7212.getVw_pst_Stndrd_Pckge().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) PST-STNDRD-PCKGE WITH PCKGE-CDE-VRSN-CDE = #INPUT-PACKAGE-CDE
        (
        "READ02",
        new Wc[] { new Wc("PCKGE_CDE_VRSN_CDE", ">=", pnd_Input_Parameters_Pnd_Input_Package_Cde, WcType.BY) },
        new Oc[] { new Oc("PCKGE_CDE_VRSN_CDE", "ASC") },
        1
        );
        READ02:
        while (condition(ldaPstl7212.getVw_pst_Stndrd_Pckge().readNextRow("READ02")))
        {
            pnd_Sys_Subsys.setValue(ldaPstl7212.getPst_Stndrd_Pckge_Pckge_Owner_Unit_Cde());                                                                              //Natural: ASSIGN #SYS-SUBSYS := PCKGE-OWNER-UNIT-CDE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM IMMEDIATE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Found_Pkg.setValue(false);                                                                                                                                    //Natural: ASSIGN #FOUND-PKG := FALSE
        READWORK03:                                                                                                                                                       //Natural: READ WORK 04 #PKG-PARM80
        while (condition(getWorkFiles().read(4, pnd_Pkg_Parm80)))
        {
            if (condition(pnd_Pkg_Parm80_Pnd_Pp80_Package.equals(pnd_Input_Parameters_Pnd_Input_Package_Cde)))                                                            //Natural: IF #PP80-PACKAGE = #INPUT-PACKAGE-CDE
            {
                pnd_Found_Pkg.setValue(true);                                                                                                                             //Natural: ASSIGN #FOUND-PKG := TRUE
                getReports().write(0, "**** FOUND PACKAGE IN POST2DCS ****");                                                                                             //Natural: WRITE '**** FOUND PACKAGE IN POST2DCS ****'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, pnd_Pkg_Parm80_Pnd_Pp80_Package,pnd_Pkg_Parm80_Pnd_Pp80_Sys_Subsys,pnd_Pkg_Parm80_Pnd_Pp80_Lights_On);                              //Natural: WRITE #PP80-PACKAGE #PP80-SYS-SUBSYS #PP80-LIGHTS-ON
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK03_Exit:
        if (Global.isEscape()) return;
        if (condition(! (pnd_Found_Pkg.getBoolean())))                                                                                                                    //Natural: IF NOT #FOUND-PKG
        {
            pnd_Pkg_Parm80_Pnd_Pp80_Lights_On.reset();                                                                                                                    //Natural: RESET #PP80-LIGHTS-ON
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GENERATE-CONNECT-DIRECT
        sub_Generate_Connect_Direct();
        if (condition(Global.isEscape())) {return;}
        //*  ---------------------------------------------------------------------
        //*                S T A R T    O F    S U B R O U T I N E S
        //*  ---------------------------------------------------------------------
        //*  --------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-INPUT-PARAMETERS
        //*  --------------------------------------
        //*  INPUT (AD=M) #INPUT-DELIVERY-CDE
        //*  INPUT (AD=M) #INPUT-PRINTER-DEST
        //*  INPUT (AD=M) #INPUT-FOLDER
        //*  WHEN #INPUT-DELIVERY-CDE EQ ' '
        //*    MSG-INFO-SUB.##MSG := '"#INPUT-DELIVERY-CDE" MUST BE SUPPLIED'
        //*  WHEN #INPUT-PRINTER-DEST EQ ' '
        //*    MSG-INFO-SUB.##MSG := '"#INPUT-PRINTER-DEST" MUST BE SUPPLIED'
        //*  WHEN #INPUT-FOLDER       EQ ' '
        //*    MSG-INFO-SUB.##MSG := '"#INPUT-FOLDER" MUST BE SUPPLIED'
        //*  -----------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-CONNECT-DIRECT
        //*  -----------------------------------------
        //*      "/ecs-app/dcs/dcs-hv-preprocessor-drop -"
        //*  -----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-OUTPUT-LINE
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-JOB
    }
    private void sub_Get_Input_Parameters() throws Exception                                                                                                              //Natural: GET-INPUT-PARAMETERS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("PCCB9557|sub_Get_Input_Parameters");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, new FieldAttributes ("AD=M"),pnd_Input_Parameters_Pnd_Input_Hlq);                                      //Natural: INPUT ( AD = M ) #INPUT-HLQ
                DbsUtil.invokeInput(setInputStatus(INPUT_2), this, new FieldAttributes ("AD=M"),pnd_Input_Parameters_Pnd_Input_Prev_Jobname);                             //Natural: INPUT ( AD = M ) #INPUT-PREV-JOBNAME
                DbsUtil.invokeInput(setInputStatus(INPUT_3), this, new FieldAttributes ("AD=M"),pnd_Input_Parameters_Pnd_Input_Parmlib);                                  //Natural: INPUT ( AD = M ) #INPUT-PARMLIB
                DbsUtil.invokeInput(setInputStatus(INPUT_4), this, new FieldAttributes ("AD=M"),pnd_Input_Parameters_Pnd_Input_Package_Cde);                              //Natural: INPUT ( AD = M ) #INPUT-PACKAGE-CDE
                pnd_Input_Parameters_Pnd_Input_Delivery_Cde.setValue("S");                                                                                                //Natural: ASSIGN #INPUT-DELIVERY-CDE := 'S'
                pnd_Input_Parameters_Pnd_Input_Printer_Dest.setValue("U1700");                                                                                            //Natural: ASSIGN #INPUT-PRINTER-DEST := 'U1700'
                pnd_Job_Name.setValue(Global.getINIT_USER(), MoveOption.LeftJustified);                                                                                   //Natural: MOVE LEFT JUSTIFIED *INIT-USER TO #JOB-NAME
                if (condition(pnd_Input_Parameters_Pnd_Input_Prev_Jobname.getSubstring(1,1).equals("T") || pnd_Input_Parameters_Pnd_Input_Prev_Jobname.getSubstring(1,1).equals("I")  //Natural: IF SUBSTR ( #INPUT-PREV-JOBNAME,1,1 ) = 'T' OR = 'I' OR = 'D'
                    || pnd_Input_Parameters_Pnd_Input_Prev_Jobname.getSubstring(1,1).equals("D")))
                {
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("TCS0"), new ExamineReplace("TDCS"));                    //Natural: EXAMINE #INPUT-PARMLIB FOR 'TCS0' AND REPLACE WITH 'TDCS'
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("SCS0"), new ExamineReplace("TDCS"));                    //Natural: EXAMINE #INPUT-PARMLIB FOR 'SCS0' AND REPLACE WITH 'TDCS'
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("TCS03"), new ExamineReplace("TDCS1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'TCS03' AND REPLACE WITH 'TDCS1'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("TCS03"), new ExamineReplace("TDCS1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'TCS03' AND REPLACE WITH 'TDCS1'
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("TCS01"), new ExamineReplace("TDCS1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'TCS01' AND REPLACE WITH 'TDCS1'
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("SCS0"), new ExamineReplace("SDCS"));                    //Natural: EXAMINE #INPUT-PARMLIB FOR 'SCS0' AND REPLACE WITH 'SDCS'
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("ECS0"), new ExamineReplace("PDCS"));                    //Natural: EXAMINE #INPUT-PARMLIB FOR 'ECS0' AND REPLACE WITH 'PDCS'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Job_Name.getSubstring(1,1).equals("S")))                                                                                                //Natural: IF SUBSTR ( #JOB-NAME,1,1 ) = 'S'
                {
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("SDCS1"), new ExamineReplace("RDCS1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'SDCS1' AND REPLACE WITH 'RDCS1'
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("TDCS1"), new ExamineReplace("RDCS1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'TDCS1' AND REPLACE WITH 'RDCS1'
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("SCCP1"), new ExamineReplace("RCCP1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'SCCP1' AND REPLACE WITH 'RCCP1'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Job_Name.getSubstring(1,1).equals("Y")))                                                                                                //Natural: IF SUBSTR ( #JOB-NAME,1,1 ) = 'Y'
                {
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("TDCS1"), new ExamineReplace("SDCS1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'TDCS1' AND REPLACE WITH 'SDCS1'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Job_Name.getSubstring(1,1).equals("F")))                                                                                                //Natural: IF SUBSTR ( #JOB-NAME,1,1 ) = 'F'
                {
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("SDCS1"), new ExamineReplace("FDCS1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'SDCS1' AND REPLACE WITH 'FDCS1'
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("TDCS1"), new ExamineReplace("FDCS1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'TDCS1' AND REPLACE WITH 'FDCS1'
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("SCCP1"), new ExamineReplace("FCCP1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'SCCP1' AND REPLACE WITH 'FCCP1'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Job_Name.getSubstring(1,1).equals("D") || pnd_Job_Name.getSubstring(1,1).equals("I") || pnd_Job_Name.getSubstring(1,                    //Natural: IF SUBSTR ( #JOB-NAME,1,1 ) = 'D' OR = 'I' OR = 'T'
                    1).equals("T")))
                {
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("SDCS1"), new ExamineReplace("DDCS1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'SDCS1' AND REPLACE WITH 'DDCS1'
                    DbsUtil.examine(new ExamineSource(pnd_Input_Parameters_Pnd_Input_Parmlib), new ExamineSearch("SCCP1"), new ExamineReplace("DCCP1"));                  //Natural: EXAMINE #INPUT-PARMLIB FOR 'SCCP1' AND REPLACE WITH 'DCCP1'
                }                                                                                                                                                         //Natural: END-IF
                //* *
                getReports().write(0, "=",pnd_Input_Parameters_Pnd_Input_Hlq);                                                                                            //Natural: WRITE '=' #INPUT-HLQ
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Input_Parameters_Pnd_Input_Prev_Jobname);                                                                                   //Natural: WRITE '=' #INPUT-PREV-JOBNAME
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Input_Parameters_Pnd_Input_Package_Cde);                                                                                    //Natural: WRITE '=' #INPUT-PACKAGE-CDE
                if (Global.isEscape()) return;
                //*  WRITE '=' #INPUT-DELIVERY-CDE
                //*  WRITE '=' #INPUT-PRINTER-DEST
                getReports().write(0, "=",pnd_Input_Parameters_Pnd_Input_Parmlib);                                                                                        //Natural: WRITE '=' #INPUT-PARMLIB
                if (Global.isEscape()) return;
                //*  WRITE '=' #INPUT-FOLDER
                short decideConditionsMet452 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #INPUT-HLQ EQ ' '
                if (condition(pnd_Input_Parameters_Pnd_Input_Hlq.equals(" ")))
                {
                    decideConditionsMet452++;
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("'INPUT-HLQ' must be supplied");                                                                   //Natural: ASSIGN MSG-INFO-SUB.##MSG := '"INPUT-HLQ" must be supplied'
                }                                                                                                                                                         //Natural: WHEN #INPUT-PREV-JOBNAME EQ ' '
                else if (condition(pnd_Input_Parameters_Pnd_Input_Prev_Jobname.equals(" ")))
                {
                    decideConditionsMet452++;
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("'INPUT-PREV-JOBNAME' must be supplied");                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG := '"INPUT-PREV-JOBNAME" must be supplied'
                }                                                                                                                                                         //Natural: WHEN #INPUT-PACKAGE-CDE EQ ' '
                else if (condition(pnd_Input_Parameters_Pnd_Input_Package_Cde.equals(" ")))
                {
                    decideConditionsMet452++;
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("'INPUT-PACKAGE-CDE' MUST BE SUPPLIED");                                                           //Natural: ASSIGN MSG-INFO-SUB.##MSG := '"INPUT-PACKAGE-CDE" MUST BE SUPPLIED'
                }                                                                                                                                                         //Natural: WHEN #INPUT-PARMLIB EQ ' '
                else if (condition(pnd_Input_Parameters_Pnd_Input_Parmlib.equals(" ")))
                {
                    decideConditionsMet452++;
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("'#INPUT-PARMLIB' must be supplied");                                                              //Natural: ASSIGN MSG-INFO-SUB.##MSG := '"#INPUT-PARMLIB" must be supplied'
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet452 > 0))
                {
                                                                                                                                                                          //Natural: PERFORM TERMINATE-JOB
                    sub_Terminate_Job();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Generate_Connect_Direct() throws Exception                                                                                                           //Natural: GENERATE-CONNECT-DIRECT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Workfile.setValue(1);                                                                                                                                         //Natural: ASSIGN #WORKFILE := 1
        getReports().write(0, "CONNECT:DIRECT CONTROL CARDS");                                                                                                            //Natural: WRITE 'CONNECT:DIRECT CONTROL CARDS'
        if (Global.isEscape()) return;
        pnd_Ndm_Control_Card_Output.setValue("   SIGNON");                                                                                                                //Natural: ASSIGN #NDM-CONTROL-CARD-OUTPUT := "   SIGNON"
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-LINE
        sub_Write_Output_Line();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //* *
        READWORK04:                                                                                                                                                       //Natural: READ WORK 03 #DATA-IN
        while (condition(getWorkFiles().read(3, pnd_Data_In)))
        {
            if (condition(DbsUtil.maskMatches(pnd_Data_In.getSubstring(1,4),"NNNN")))                                                                                     //Natural: IF SUBSTR ( #DATA-IN,1,4 ) = MASK ( NNNN )
            {
                pnd_Cis_Unique_Rec.setValue(pnd_Data_In);                                                                                                                 //Natural: ASSIGN #CIS-UNIQUE-REC := #DATA-IN
                pnd_In_Profile.setValue(pnd_Cis_Unique_Rec_Pnd_Cis_Profile);                                                                                              //Natural: ASSIGN #IN-PROFILE := #CIS-PROFILE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(DbsUtil.maskMatches(pnd_Data_In.getSubstring(1,3),"NN'P'")))                                                                                //Natural: IF SUBSTR ( #DATA-IN,1,3 ) = MASK ( NN'P' )
                {
                    pnd_Paqtrly_App_Rec.setValue(pnd_Data_In);                                                                                                            //Natural: ASSIGN #PAQTRLY-APP-REC := #DATA-IN
                    pnd_In_Profile.setValue(pnd_Paqtrly_App_Rec_Pnd_Paq_Profile);                                                                                         //Natural: ASSIGN #IN-PROFILE := #PAQ-PROFILE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Netchg_App_Rec.setValue(pnd_Data_In);                                                                                                             //Natural: ASSIGN #NETCHG-APP-REC := #DATA-IN
                    pnd_In_Profile.setValue(pnd_Netchg_App_Rec_Pnd_Net_Profile);                                                                                          //Natural: ASSIGN #IN-PROFILE := #NET-PROFILE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Prof_Id.getValue("*")), new ExamineSearch(pnd_In_Profile), new ExamineGivingIndex(pnd_I));                              //Natural: EXAMINE #PROF-ID ( * ) FOR #IN-PROFILE GIVING INDEX IN #I
            if (condition(pnd_I.equals(getZero())))                                                                                                                       //Natural: IF #I = 0
            {
                getReports().write(0, NEWLINE,NEWLINE,"**ERROR: PROFILE ID",pnd_In_Profile,"NOT IN PROFILE TABLE**");                                                     //Natural: WRITE // '**ERROR: PROFILE ID' #IN-PROFILE 'NOT IN PROFILE TABLE**'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(22);  if (true) return;                                                                                                                 //Natural: TERMINATE 0022
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sys_Subsys.setValue(pnd_Prof_Sys_Subsys.getValue(pnd_I));                                                                                                 //Natural: ASSIGN #SYS-SUBSYS := #PROF-SYS-SUBSYS ( #I )
            pnd_Input_Parameters_Pnd_Input_Package_Cde.setValue(pnd_Prof_App.getValue(pnd_I));                                                                            //Natural: ASSIGN #INPUT-PACKAGE-CDE := #PROF-APP ( #I )
            //*   SETUP THE PARMLIB THAT CONTAINS CONNECT:DIRECT CONTROL CARDS.
            pnd_Ndm_Control_Card_Output.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "   SUBMIT  DSN=", pnd_Input_Parameters_Pnd_Input_Parmlib,               //Natural: COMPRESS "   SUBMIT  DSN=" #INPUT-PARMLIB " MAXDELAY=UNLIMITED  -" INTO #NDM-CONTROL-CARD-OUTPUT LEAVING NO SPACE
                " MAXDELAY=UNLIMITED  -"));
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-LINE
            sub_Write_Output_Line();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            pnd_Ndm_Control_Card_Output.setValue("           CASE=YES -");                                                                                                //Natural: ASSIGN #NDM-CONTROL-CARD-OUTPUT := "           CASE=YES -"
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-LINE
            sub_Write_Output_Line();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            if (condition(pnd_Job_Name.getSubstring(2,6).equals("CC2650") || pnd_Input_Parameters_Pnd_Input_Package_Cde.getSubstring(1,3).equals("DMS")                   //Natural: IF SUBSTR ( #JOB-NAME,2,6 ) = 'CC2650' OR SUBSTR ( #INPUT-PACKAGE-CDE,1,3 ) = 'DMS' OR = 'SPI' OR = 'ATA' OR #INPUT-PACKAGE-CDE = 'PFRKNCOA' OR = 'PFRKBADL'
                || pnd_Input_Parameters_Pnd_Input_Package_Cde.getSubstring(1,3).equals("SPI") || pnd_Input_Parameters_Pnd_Input_Package_Cde.getSubstring(1,3).equals("ATA") 
                || pnd_Input_Parameters_Pnd_Input_Package_Cde.equals("PFRKNCOA") || pnd_Input_Parameters_Pnd_Input_Package_Cde.equals("PFRKBADL")))
            {
                pnd_Ndm_Control_Card_Output.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "           &RMTDIR=", "/ecs-app/ccp/event-processor/drop -"));      //Natural: COMPRESS "           &RMTDIR=" "/ecs-app/ccp/event-processor/drop -" INTO #NDM-CONTROL-CARD-OUTPUT LEAVING NO SPACE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  PFDC
                pnd_Ndm_Control_Card_Output.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "           &RMTDIR=", "/ecs-app/ccp/event-processor/drop -"));      //Natural: COMPRESS "           &RMTDIR=" "/ecs-app/ccp/event-processor/drop -" INTO #NDM-CONTROL-CARD-OUTPUT LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-LINE
            sub_Write_Output_Line();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            //*   SETUP THE SOURCE DATASET NAME - XML FILE
            //*  COMPRESS #INPUT-HLQ '.' #INPUT-PREV-JOBNAME '.XMLOUT.FILE'
            pnd_Input_Dsn_From.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Parameters_Pnd_Input_Hlq, ".", pnd_Prof_App.getValue(pnd_I),            //Natural: COMPRESS #INPUT-HLQ '.' #PROF-APP ( #I ) '.XMLOUT.FILE' INTO #INPUT-DSN-FROM LEAVING NO SPACE
                ".XMLOUT.FILE"));
            pnd_Ndm_Control_Card_Output.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "           &DSNIN=", pnd_Input_Dsn_From, " -"));                        //Natural: COMPRESS "           &DSNIN=" #INPUT-DSN-FROM " -" INTO #NDM-CONTROL-CARD-OUTPUT LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-LINE
            sub_Write_Output_Line();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            //*   SETUP THE DESTINATION FILENAME - XML FILE
            pnd_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                     //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE
            pnd_Date_5.setValue(pnd_Date.getSubstring(4,5));                                                                                                              //Natural: ASSIGN #DATE-5 := SUBSTR ( #DATE,4,5 )
            pnd_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                                      //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO #TIME
            //*  #TIME-6 := SUBSTR(#TIME,1,6)
            //*  COMPRESS #DATE #TIME-6 INTO #DTS LEAVING NO SPACE
            pnd_Dts.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Date_5, pnd_Time));                                                                      //Natural: COMPRESS #DATE-5 #TIME INTO #DTS LEAVING NO SPACE
            pnd_Input_Dsn_To.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Sys_Subsys, "-", pnd_Date_Pnd_Yymmdd, "-", pnd_Dts, "-", pnd_Input_Parameters_Pnd_Input_Delivery_Cde,  //Natural: COMPRESS #SYS-SUBSYS '-' #YYMMDD '-' #DTS '-' #INPUT-DELIVERY-CDE '-' #INPUT-PRINTER-DEST INTO #INPUT-DSN-TO LEAVING NO
                "-", pnd_Input_Parameters_Pnd_Input_Printer_Dest));
            pnd_Ndm_Control_Card_Output.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "           &FILNAM=/", pnd_Input_Dsn_To, ".xml -"));                    //Natural: COMPRESS "           &FILNAM=/" #INPUT-DSN-TO ".xml -" INTO #NDM-CONTROL-CARD-OUTPUT LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-LINE
            sub_Write_Output_Line();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            //*   SETUP THE DESTINATION FILENAME - TRG FILE
            if (condition(ldaPstl7212.getPst_Stndrd_Pckge_Pckge_New_Mrge_Mngr().getSubstring(4,1).equals("X") || pnd_Pkg_Parm80_Pnd_Pp80_Lights_On.equals("N")))          //Natural: IF SUBSTR ( PCKGE-NEW-MRGE-MNGR,4,1 ) = 'X' OR #PP80-LIGHTS-ON = 'N'
            {
                pnd_Ndm_Control_Card_Output.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "           &FILNAM2=/", pnd_Input_Dsn_To, ".xml -"));               //Natural: COMPRESS "           &FILNAM2=/" #INPUT-DSN-TO ".xml -" INTO #NDM-CONTROL-CARD-OUTPUT LEAVING NO SPACE
                //*  COMPRESS "           &FILNAM2=/X" #INPUT-DSN-TO " -"
                //*   INTO #NDM-CONTROL-CARD-OUTPUT LEAVING NO SPACE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ndm_Control_Card_Output.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "           &FILNAM2=/", pnd_Input_Dsn_To, ".trg -"));               //Natural: COMPRESS "           &FILNAM2=/" #INPUT-DSN-TO ".trg -" INTO #NDM-CONTROL-CARD-OUTPUT LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-LINE
            sub_Write_Output_Line();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            //*   SETUP THE SOURCE DATASET NAME - TRG FILE
            if (condition(ldaPstl7212.getPst_Stndrd_Pckge_Pckge_New_Mrge_Mngr().getSubstring(4,1).equals("X") || pnd_Pkg_Parm80_Pnd_Pp80_Lights_On.equals("N")))          //Natural: IF SUBSTR ( PCKGE-NEW-MRGE-MNGR,4,1 ) = 'X' OR #PP80-LIGHTS-ON = 'N'
            {
                pnd_Input_Dsn_From.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Parameters_Pnd_Input_Hlq, ".", pnd_Prof_App.getValue(pnd_I),        //Natural: COMPRESS #INPUT-HLQ '.' #PROF-APP ( #I ) '.XMLOUT.FILE' INTO #INPUT-DSN-FROM LEAVING NO SPACE
                    ".XMLOUT.FILE"));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Input_Dsn_From.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Parameters_Pnd_Input_Hlq, ".", pnd_Prof_App.getValue(pnd_I),        //Natural: COMPRESS #INPUT-HLQ '.' #PROF-APP ( #I ) '.TRGFILE' INTO #INPUT-DSN-FROM LEAVING NO SPACE
                    ".TRGFILE"));
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ndm_Control_Card_Output.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "           &DSNIN2=", pnd_Input_Dsn_From));                             //Natural: COMPRESS "           &DSNIN2=" #INPUT-DSN-FROM INTO #NDM-CONTROL-CARD-OUTPUT LEAVING NO SPACE
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-LINE
            sub_Write_Output_Line();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            //*  COMPRESS "           &FOLDR2=" #INPUT-FOLDER " -"
            //*    INTO #NDM-CONTROL-CARD-OUTPUT LEAVING NO SPACE
            //*  PERFORM WRITE-OUTPUT-LINE
            //* *
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK04_Exit:
        if (Global.isEscape()) return;
        pnd_Ndm_Control_Card_Output.setValue("   SIGNOFF");                                                                                                               //Natural: ASSIGN #NDM-CONTROL-CARD-OUTPUT := "   SIGNOFF"
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-LINE
        sub_Write_Output_Line();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Write_Output_Line() throws Exception                                                                                                                 //Natural: WRITE-OUTPUT-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------------
        getWorkFiles().write(1, false, pnd_Ndm_Control_Card_Output);                                                                                                      //Natural: WRITE WORK FILE 1 #NDM-CONTROL-CARD-OUTPUT
        getReports().write(0, pnd_Ndm_Control_Card_Output);                                                                                                               //Natural: WRITE ( 0 ) #NDM-CONTROL-CARD-OUTPUT
        if (Global.isEscape()) return;
    }
    private void sub_Terminate_Job() throws Exception                                                                                                                     //Natural: TERMINATE-JOB
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("PCCB9557|sub_Terminate_Job");
        while(true)
        {
            try
            {
                //*  -------------------------------
                //*     TERMINATE JOB DUE TO AN ERROR.
                //*     NOTE:  THERE IS NO RETURN FROM THIS SUBROUTINE.
                //*  EXPAND ##MSG TO MAKE IT READABLE
                DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
                if (condition(Global.isEscape())) return;
                DbsUtil.invokeInput(setInputStatus(INPUT_5), this, new FieldAttributes ("AD=O"),"-",new RepeatItem(131),NEWLINE,NEWLINE,"Job terminated due to following error(s):", //Natural: INPUT ( AD = O ) '-' ( 131 ) // 'Job terminated due to following error(s):' / MSG-INFO-SUB.##MSG // 'Check list of errors above.' / 'No Output has been produced' /
                    NEWLINE,pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),NEWLINE,NEWLINE,"Check list of errors above.",NEWLINE,"No Output has been produced",
                    NEWLINE);
                error_Handler_Fields_Pnd_Error_Nr.reset();                                                                                                                //Natural: RESET #ERROR-NR #ERROR-LINE #ERROR-LEVEL
                error_Handler_Fields_Pnd_Error_Line.reset();
                error_Handler_Fields_Pnd_Error_Level.reset();
                //*  WILL TERMINATE WITH COND CODE 4
                Global.getSTACK().pushData(StackOption.TOP, error_Handler_Fields_Pnd_Error_Nr, error_Handler_Fields_Pnd_Error_Line, error_Handler_Fields_Pnd_Error_Status,  //Natural: FETCH 'INFP9000' #ERROR-NR #ERROR-LINE #ERROR-STATUS #ERROR-PROGRAM #ERROR-LEVEL #ERROR-APPL
                    error_Handler_Fields_Pnd_Error_Program, error_Handler_Fields_Pnd_Error_Level, error_Handler_Fields_Pnd_Error_Appl);
                Global.setFetchProgram(DbsUtil.getBlType("INFP9000"));
                if (condition(Global.isEscape())) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=80 PS=60");
        Global.format(0, "PS=60 LS=132");
    }
}
