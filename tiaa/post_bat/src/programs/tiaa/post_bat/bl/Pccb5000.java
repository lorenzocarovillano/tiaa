/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:16:53 PM
**        * FROM NATURAL PROGRAM : Pccb5000
************************************************************
**        * FILE NAME            : Pccb5000.java
**        * CLASS NAME           : Pccb5000
**        * INSTANCE NAME        : Pccb5000
************************************************************
************************************************************************
* PROGRAM  : PCCB5000
* SYSTEM   : CLIENT COMMUNICATIONS
* TITLE    : SPLIT LARGE VOLUME FILES PRIOR TO SENDING DATA TO CCP
* FUNCTION : FOR LARGE-VOLUME REQUEST DATA FILES (PACKAGES PSLOAN AND
*          : PTRKMDOR), PROGRAM WILL SPLIT THE FILE INTO SMALLER BATCHES
*          : PER THE PACKAGE THRESHOLD VALUE. THE SPLITTING POINT
*          : SHOULD NOT CAUSE REQUESTS FOR THE SAME PIN TO BE IN
*          : DIFFERENT BATCHES.
*
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
* J. CRUZ  OCT 2015 INITIAL IMPLEMENTATION
* J. CRUZ  NOV 2015 INCREASE DAILY EXTRACT LIMIT TO 48,000 FOR PTRKMDOR
* B. JENA  NOV 2018 UPDATE THE LOGIC IN THE PROGRAM TO THROW RC=02 WHEN
*                   END-OF-FILE IS REACHED.           /* SEE:C493059
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Pccb5000 extends BLNatBase
{
    // Data Areas
    private LdaPldl520 ldaPldl520;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_All;

    private DbsGroup pnd_Input_All__R_Field_1;
    private DbsField pnd_Input_All_Pnd_Input_Req_Fixed;

    private DbsGroup pnd_Input_All__R_Field_2;
    private DbsField pnd_Input_All_Pnd_Key_Data;

    private DbsGroup pnd_Input_All__R_Field_3;
    private DbsField pnd_Input_All_Pnd_Kd_Pckg_Code;
    private DbsField pnd_Input_All_Pnd_Kd_Date_Time;
    private DbsField pnd_Input_All_Pnd_Kd_Univ_Id_Data;

    private DbsGroup pnd_Input_All__R_Field_4;
    private DbsField pnd_Input_All_Pnd_Kd_Univ_Id_Type;
    private DbsField pnd_Input_All_Pnd_Kd_Univ_Id;
    private DbsField pnd_Input_All_Pnd_Kd_Recipient_Seq;
    private DbsField pnd_Input_All_Pnd_Detail_Seq;
    private DbsField pnd_Input_All_Pnd_Record_Type;
    private DbsField pnd_Input_All_Pnd_Input_Req_Variable;

    private DbsGroup pnd_Input_All__R_Field_5;
    private DbsField pnd_Input_All_Pnd_Header_Data;

    private DbsGroup pnd_Input_All__R_Field_6;
    private DbsField pnd_Input_All_Pnd_Detail_Data;

    private DbsGroup pnd_Input_All__R_Field_7;
    private DbsField pnd_Input_All_Pnd_Trailer_Data;
    private DbsField pnd_Input_Del_Type;
    private DbsField pnd_Input_Dtl_Id;
    private DbsField pnd_Output_Req_Data;
    private DbsField pnd_Curr_Restart_Key;

    private DbsGroup pnd_Curr_Restart_Key__R_Field_8;
    private DbsField pnd_Curr_Restart_Key_Pnd_Crk_Filler;
    private DbsField pnd_Curr_Restart_Key_Pnd_Crk_Univ_Id_Data;
    private DbsField pnd_Prev_Key_Data;
    private DbsField pnd_Prev_Kd_Univ_Id;
    private DbsField pnd_Daily_Mdor_Complete;
    private DbsField pnd_User_Job;
    private DbsField pnd_App_Id;
    private DbsField pnd_Package_Id;
    private DbsField pnd_Curr_Package;
    private DbsField pnd_Batch_Limit;
    private DbsField pnd_Record_Cnt;
    private DbsField pnd_Req_Cnt;
    private DbsField pnd_Mdor_Cnt;
    private DbsField pnd_P;
    private DbsField pnd_Fill1;
    private DbsField pnd_Fill3;

    private DbsGroup pnd_Restart_Stuff;
    private DbsField pnd_Restart_Stuff_Pnd_Restart_Loan_No;
    private DbsField pnd_Restart_Stuff_Pnd_Restart_Seq_No;

    private DbsGroup pnd_Restart_Stuff__R_Field_9;
    private DbsField pnd_Restart_Stuff_Pnd_Restart_Data;
    private DbsField pnd_Seq_No;
    private DbsField pnd_Save_Restart_Loan_No;
    private DbsField pnd_Save_Loan_No;

    private DbsGroup pnd_Save_Loan_No__R_Field_10;
    private DbsField pnd_Save_Loan_No_Pnd_Save_Contract;
    private DbsField pnd_Save_Loan_No_Pnd_Save_Sequence;
    private DbsField pnd_Batch_Restart_Isn;

    private DbsGroup pnd_Batch_Restart_Key;
    private DbsField pnd_Batch_Restart_Key_Pnd_Brk_Active_Ind;
    private DbsField pnd_Batch_Restart_Key_Pnd_Brk_Job_Name;
    private DbsField pnd_Batch_Restart_Key_Pnd_Brk_Jobstep_Name;

    private DbsGroup pnd_Batch_Restart_Key__R_Field_11;
    private DbsField pnd_Batch_Restart_Key_Pnd_Sp_Actve_Job_Step;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaPldl520 = new LdaPldl520();
        registerRecord(ldaPldl520);
        registerRecord(ldaPldl520.getVw_batch_Restart());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Input_All = localVariables.newFieldArrayInRecord("pnd_Input_All", "#INPUT-ALL", FieldType.STRING, 1, new DbsArrayController(1, 5054));

        pnd_Input_All__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_All__R_Field_1", "REDEFINE", pnd_Input_All);
        pnd_Input_All_Pnd_Input_Req_Fixed = pnd_Input_All__R_Field_1.newFieldArrayInGroup("pnd_Input_All_Pnd_Input_Req_Fixed", "#INPUT-REQ-FIXED", FieldType.STRING, 
            1, new DbsArrayController(1, 52));

        pnd_Input_All__R_Field_2 = pnd_Input_All__R_Field_1.newGroupInGroup("pnd_Input_All__R_Field_2", "REDEFINE", pnd_Input_All_Pnd_Input_Req_Fixed);
        pnd_Input_All_Pnd_Key_Data = pnd_Input_All__R_Field_2.newFieldInGroup("pnd_Input_All_Pnd_Key_Data", "#KEY-DATA", FieldType.STRING, 44);

        pnd_Input_All__R_Field_3 = pnd_Input_All__R_Field_2.newGroupInGroup("pnd_Input_All__R_Field_3", "REDEFINE", pnd_Input_All_Pnd_Key_Data);
        pnd_Input_All_Pnd_Kd_Pckg_Code = pnd_Input_All__R_Field_3.newFieldInGroup("pnd_Input_All_Pnd_Kd_Pckg_Code", "#KD-PCKG-CODE", FieldType.STRING, 
            8);
        pnd_Input_All_Pnd_Kd_Date_Time = pnd_Input_All__R_Field_3.newFieldInGroup("pnd_Input_All_Pnd_Kd_Date_Time", "#KD-DATE-TIME", FieldType.STRING, 
            15);
        pnd_Input_All_Pnd_Kd_Univ_Id_Data = pnd_Input_All__R_Field_3.newFieldInGroup("pnd_Input_All_Pnd_Kd_Univ_Id_Data", "#KD-UNIV-ID-DATA", FieldType.STRING, 
            14);

        pnd_Input_All__R_Field_4 = pnd_Input_All__R_Field_3.newGroupInGroup("pnd_Input_All__R_Field_4", "REDEFINE", pnd_Input_All_Pnd_Kd_Univ_Id_Data);
        pnd_Input_All_Pnd_Kd_Univ_Id_Type = pnd_Input_All__R_Field_4.newFieldInGroup("pnd_Input_All_Pnd_Kd_Univ_Id_Type", "#KD-UNIV-ID-TYPE", FieldType.STRING, 
            1);
        pnd_Input_All_Pnd_Kd_Univ_Id = pnd_Input_All__R_Field_4.newFieldInGroup("pnd_Input_All_Pnd_Kd_Univ_Id", "#KD-UNIV-ID", FieldType.STRING, 13);
        pnd_Input_All_Pnd_Kd_Recipient_Seq = pnd_Input_All__R_Field_3.newFieldInGroup("pnd_Input_All_Pnd_Kd_Recipient_Seq", "#KD-RECIPIENT-SEQ", FieldType.NUMERIC, 
            7);
        pnd_Input_All_Pnd_Detail_Seq = pnd_Input_All__R_Field_2.newFieldInGroup("pnd_Input_All_Pnd_Detail_Seq", "#DETAIL-SEQ", FieldType.NUMERIC, 7);
        pnd_Input_All_Pnd_Record_Type = pnd_Input_All__R_Field_2.newFieldInGroup("pnd_Input_All_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 1);
        pnd_Input_All_Pnd_Input_Req_Variable = pnd_Input_All__R_Field_1.newFieldArrayInGroup("pnd_Input_All_Pnd_Input_Req_Variable", "#INPUT-REQ-VARIABLE", 
            FieldType.STRING, 1, new DbsArrayController(1, 5002));

        pnd_Input_All__R_Field_5 = pnd_Input_All__R_Field_1.newGroupInGroup("pnd_Input_All__R_Field_5", "REDEFINE", pnd_Input_All_Pnd_Input_Req_Variable);
        pnd_Input_All_Pnd_Header_Data = pnd_Input_All__R_Field_5.newFieldArrayInGroup("pnd_Input_All_Pnd_Header_Data", "#HEADER-DATA", FieldType.STRING, 
            1, new DbsArrayController(1, 501));

        pnd_Input_All__R_Field_6 = pnd_Input_All__R_Field_1.newGroupInGroup("pnd_Input_All__R_Field_6", "REDEFINE", pnd_Input_All_Pnd_Input_Req_Variable);
        pnd_Input_All_Pnd_Detail_Data = pnd_Input_All__R_Field_6.newFieldArrayInGroup("pnd_Input_All_Pnd_Detail_Data", "#DETAIL-DATA", FieldType.STRING, 
            1, new DbsArrayController(1, 5002));

        pnd_Input_All__R_Field_7 = pnd_Input_All__R_Field_1.newGroupInGroup("pnd_Input_All__R_Field_7", "REDEFINE", pnd_Input_All_Pnd_Input_Req_Variable);
        pnd_Input_All_Pnd_Trailer_Data = pnd_Input_All__R_Field_7.newFieldArrayInGroup("pnd_Input_All_Pnd_Trailer_Data", "#TRAILER-DATA", FieldType.STRING, 
            1, new DbsArrayController(1, 7));
        pnd_Input_Del_Type = localVariables.newFieldInRecord("pnd_Input_Del_Type", "#INPUT-DEL-TYPE", FieldType.STRING, 1);
        pnd_Input_Dtl_Id = localVariables.newFieldInRecord("pnd_Input_Dtl_Id", "#INPUT-DTL-ID", FieldType.STRING, 2);
        pnd_Output_Req_Data = localVariables.newFieldArrayInRecord("pnd_Output_Req_Data", "#OUTPUT-REQ-DATA", FieldType.STRING, 1, new DbsArrayController(1, 
            5002));
        pnd_Curr_Restart_Key = localVariables.newFieldInRecord("pnd_Curr_Restart_Key", "#CURR-RESTART-KEY", FieldType.STRING, 44);

        pnd_Curr_Restart_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Curr_Restart_Key__R_Field_8", "REDEFINE", pnd_Curr_Restart_Key);
        pnd_Curr_Restart_Key_Pnd_Crk_Filler = pnd_Curr_Restart_Key__R_Field_8.newFieldInGroup("pnd_Curr_Restart_Key_Pnd_Crk_Filler", "#CRK-FILLER", FieldType.STRING, 
            23);
        pnd_Curr_Restart_Key_Pnd_Crk_Univ_Id_Data = pnd_Curr_Restart_Key__R_Field_8.newFieldInGroup("pnd_Curr_Restart_Key_Pnd_Crk_Univ_Id_Data", "#CRK-UNIV-ID-DATA", 
            FieldType.STRING, 14);
        pnd_Prev_Key_Data = localVariables.newFieldInRecord("pnd_Prev_Key_Data", "#PREV-KEY-DATA", FieldType.STRING, 44);
        pnd_Prev_Kd_Univ_Id = localVariables.newFieldInRecord("pnd_Prev_Kd_Univ_Id", "#PREV-KD-UNIV-ID", FieldType.STRING, 13);
        pnd_Daily_Mdor_Complete = localVariables.newFieldInRecord("pnd_Daily_Mdor_Complete", "#DAILY-MDOR-COMPLETE", FieldType.BOOLEAN, 1);
        pnd_User_Job = localVariables.newFieldInRecord("pnd_User_Job", "#USER-JOB", FieldType.STRING, 8);
        pnd_App_Id = localVariables.newFieldInRecord("pnd_App_Id", "#APP-ID", FieldType.STRING, 8);
        pnd_Package_Id = localVariables.newFieldArrayInRecord("pnd_Package_Id", "#PACKAGE-ID", FieldType.STRING, 8, new DbsArrayController(1, 3));
        pnd_Curr_Package = localVariables.newFieldInRecord("pnd_Curr_Package", "#CURR-PACKAGE", FieldType.STRING, 8);
        pnd_Batch_Limit = localVariables.newFieldArrayInRecord("pnd_Batch_Limit", "#BATCH-LIMIT", FieldType.NUMERIC, 5, new DbsArrayController(1, 3));
        pnd_Record_Cnt = localVariables.newFieldInRecord("pnd_Record_Cnt", "#RECORD-CNT", FieldType.NUMERIC, 10);
        pnd_Req_Cnt = localVariables.newFieldInRecord("pnd_Req_Cnt", "#REQ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Mdor_Cnt = localVariables.newFieldInRecord("pnd_Mdor_Cnt", "#MDOR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.PACKED_DECIMAL, 3);
        pnd_Fill1 = localVariables.newFieldInRecord("pnd_Fill1", "#FILL1", FieldType.STRING, 4501);
        pnd_Fill3 = localVariables.newFieldInRecord("pnd_Fill3", "#FILL3", FieldType.STRING, 4995);

        pnd_Restart_Stuff = localVariables.newGroupInRecord("pnd_Restart_Stuff", "#RESTART-STUFF");
        pnd_Restart_Stuff_Pnd_Restart_Loan_No = pnd_Restart_Stuff.newFieldInGroup("pnd_Restart_Stuff_Pnd_Restart_Loan_No", "#RESTART-LOAN-NO", FieldType.STRING, 
            10);
        pnd_Restart_Stuff_Pnd_Restart_Seq_No = pnd_Restart_Stuff.newFieldInGroup("pnd_Restart_Stuff_Pnd_Restart_Seq_No", "#RESTART-SEQ-NO", FieldType.NUMERIC, 
            7);

        pnd_Restart_Stuff__R_Field_9 = localVariables.newGroupInRecord("pnd_Restart_Stuff__R_Field_9", "REDEFINE", pnd_Restart_Stuff);
        pnd_Restart_Stuff_Pnd_Restart_Data = pnd_Restart_Stuff__R_Field_9.newFieldInGroup("pnd_Restart_Stuff_Pnd_Restart_Data", "#RESTART-DATA", FieldType.STRING, 
            17);
        pnd_Seq_No = localVariables.newFieldInRecord("pnd_Seq_No", "#SEQ-NO", FieldType.NUMERIC, 7);
        pnd_Save_Restart_Loan_No = localVariables.newFieldInRecord("pnd_Save_Restart_Loan_No", "#SAVE-RESTART-LOAN-NO", FieldType.STRING, 10);
        pnd_Save_Loan_No = localVariables.newFieldInRecord("pnd_Save_Loan_No", "#SAVE-LOAN-NO", FieldType.STRING, 10);

        pnd_Save_Loan_No__R_Field_10 = localVariables.newGroupInRecord("pnd_Save_Loan_No__R_Field_10", "REDEFINE", pnd_Save_Loan_No);
        pnd_Save_Loan_No_Pnd_Save_Contract = pnd_Save_Loan_No__R_Field_10.newFieldInGroup("pnd_Save_Loan_No_Pnd_Save_Contract", "#SAVE-CONTRACT", FieldType.STRING, 
            8);
        pnd_Save_Loan_No_Pnd_Save_Sequence = pnd_Save_Loan_No__R_Field_10.newFieldInGroup("pnd_Save_Loan_No_Pnd_Save_Sequence", "#SAVE-SEQUENCE", FieldType.STRING, 
            2);
        pnd_Batch_Restart_Isn = localVariables.newFieldInRecord("pnd_Batch_Restart_Isn", "#BATCH-RESTART-ISN", FieldType.PACKED_DECIMAL, 10);

        pnd_Batch_Restart_Key = localVariables.newGroupInRecord("pnd_Batch_Restart_Key", "#BATCH-RESTART-KEY");
        pnd_Batch_Restart_Key_Pnd_Brk_Active_Ind = pnd_Batch_Restart_Key.newFieldInGroup("pnd_Batch_Restart_Key_Pnd_Brk_Active_Ind", "#BRK-ACTIVE-IND", 
            FieldType.STRING, 1);
        pnd_Batch_Restart_Key_Pnd_Brk_Job_Name = pnd_Batch_Restart_Key.newFieldInGroup("pnd_Batch_Restart_Key_Pnd_Brk_Job_Name", "#BRK-JOB-NAME", FieldType.STRING, 
            8);
        pnd_Batch_Restart_Key_Pnd_Brk_Jobstep_Name = pnd_Batch_Restart_Key.newFieldInGroup("pnd_Batch_Restart_Key_Pnd_Brk_Jobstep_Name", "#BRK-JOBSTEP-NAME", 
            FieldType.STRING, 8);

        pnd_Batch_Restart_Key__R_Field_11 = localVariables.newGroupInRecord("pnd_Batch_Restart_Key__R_Field_11", "REDEFINE", pnd_Batch_Restart_Key);
        pnd_Batch_Restart_Key_Pnd_Sp_Actve_Job_Step = pnd_Batch_Restart_Key__R_Field_11.newFieldInGroup("pnd_Batch_Restart_Key_Pnd_Sp_Actve_Job_Step", 
            "#SP-ACTVE-JOB-STEP", FieldType.STRING, 17);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaPldl520.initializeValues();

        localVariables.reset();
        pnd_Daily_Mdor_Complete.setInitialValue(false);
        pnd_Package_Id.getValue(1).setInitialValue("PSLOAN");
        pnd_Package_Id.getValue(2).setInitialValue("PTRKMDOR");
        pnd_Package_Id.getValue(3).setInitialValue(" ");
        pnd_Batch_Limit.getValue(1).setInitialValue(12000);
        pnd_Batch_Limit.getValue(2).setInitialValue(12000);
        pnd_Batch_Limit.getValue(3).setInitialValue(0);
        pnd_Record_Cnt.setInitialValue(0);
        pnd_Req_Cnt.setInitialValue(0);
        pnd_Mdor_Cnt.setInitialValue(0);
        pnd_P.setInitialValue(0);
        pnd_Restart_Stuff_Pnd_Restart_Loan_No.setInitialValue(" ");
        pnd_Restart_Stuff_Pnd_Restart_Seq_No.setInitialValue(0);
        pnd_Seq_No.setInitialValue(0);
        pnd_Save_Restart_Loan_No.setInitialValue(" ");
        pnd_Save_Loan_No.setInitialValue(" ");
        pnd_Batch_Restart_Isn.setInitialValue(0);
        pnd_Batch_Restart_Key_Pnd_Brk_Active_Ind.setInitialValue("A");
        pnd_Batch_Restart_Key_Pnd_Brk_Jobstep_Name.setInitialValue("FILSPLIT");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Pccb5000() throws Exception
    {
        super("Pccb5000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ***********************************************************************
        //* **                                                                                                                                                            //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
        //* **
        READWORK01:                                                                                                                                                       //Natural: READ WORK 01 #INPUT-REQ-FIXED ( * ) #INPUT-REQ-VARIABLE ( * )
        while (condition(getWorkFiles().read(1, pnd_Input_All_Pnd_Input_Req_Fixed.getValue("*"), pnd_Input_All_Pnd_Input_Req_Variable.getValue("*"))))
        {
            pnd_Record_Cnt.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #RECORD-CNT
            if (condition(pnd_Record_Cnt.equals(1)))                                                                                                                      //Natural: IF #RECORD-CNT = 1
            {
                pnd_Batch_Restart_Key_Pnd_Brk_Job_Name.setValue(pnd_Input_All_Pnd_Kd_Pckg_Code);                                                                          //Natural: ASSIGN #BRK-JOB-NAME := #KD-PCKG-CODE
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-RESTART
                sub_Check_For_Restart();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Input_All_Pnd_Kd_Pckg_Code.equals("PTRKMDOR")))                                                                                         //Natural: IF #KD-PCKG-CODE = 'PTRKMDOR'
                {
                    pnd_Mdor_Cnt.setValue(ldaPldl520.getBatch_Restart_Rst_Cnt());                                                                                         //Natural: ASSIGN #MDOR-CNT := RST-CNT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Curr_Package.setValue(pnd_Input_All_Pnd_Kd_Pckg_Code);                                                                                                //Natural: ASSIGN #CURR-PACKAGE := #KD-PCKG-CODE
                DbsUtil.examine(new ExamineSource(pnd_Package_Id.getValue("*")), new ExamineSearch(pnd_Input_All_Pnd_Kd_Pckg_Code), new ExamineGivingIndex(pnd_P));       //Natural: EXAMINE #PACKAGE-ID ( * ) FOR #KD-PCKG-CODE GIVING INDEX IN #P
                if (condition(pnd_P.equals(getZero())))                                                                                                                   //Natural: IF #P = 0
                {
                    pnd_P.setValue(3);                                                                                                                                    //Natural: ASSIGN #P := 3
                    pnd_Batch_Limit.getValue(3).setValue(12000);                                                                                                          //Natural: ASSIGN #BATCH-LIMIT ( 3 ) := 12000
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "=",pnd_Input_All_Pnd_Kd_Pckg_Code,pnd_P,pnd_Package_Id.getValue(pnd_P),"=",pnd_Batch_Limit.getValue(pnd_P));                       //Natural: WRITE '=' #KD-PCKG-CODE #P #PACKAGE-ID ( #P ) '=' #BATCH-LIMIT ( #P )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",pnd_Input_All_Pnd_Kd_Univ_Id_Data,"=",pnd_Curr_Restart_Key_Pnd_Crk_Univ_Id_Data);                                               //Natural: WRITE '=' #KD-UNIV-ID-DATA '=' #CRK-UNIV-ID-DATA
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* *
            if (condition(pnd_Input_All_Pnd_Kd_Univ_Id_Data.lessOrEqual(pnd_Curr_Restart_Key_Pnd_Crk_Univ_Id_Data)))                                                      //Natural: IF #KD-UNIV-ID-DATA LE #CRK-UNIV-ID-DATA
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* *
            pnd_Output_Req_Data.getValue(1,":",5002).setValue(" ");                                                                                                       //Natural: ASSIGN #OUTPUT-REQ-DATA ( 1:5002 ) := ' '
            //* *
            short decideConditionsMet159 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #RECORD-TYPE;//Natural: VALUE 'H'
            if (condition((pnd_Input_All_Pnd_Record_Type.equals("H"))))
            {
                decideConditionsMet159++;
                if (condition(pnd_Req_Cnt.greaterOrEqual(pnd_Batch_Limit.getValue(pnd_P))))                                                                               //Natural: IF #REQ-CNT GE #BATCH-LIMIT ( #P )
                {
                    if (condition(pnd_Input_All_Pnd_Kd_Univ_Id.notEquals(pnd_Prev_Kd_Univ_Id)))                                                                           //Natural: IF #KD-UNIV-ID NE #PREV-KD-UNIV-ID
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pnd_Req_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REQ-CNT
                pnd_Output_Req_Data.getValue(1,":",501).setValue(pnd_Input_All_Pnd_Header_Data.getValue(1,":",501));                                                      //Natural: MOVE #HEADER-DATA ( 1:501 ) TO #OUTPUT-REQ-DATA ( 1:501 )
                getWorkFiles().write(2, false, pnd_Input_All_Pnd_Input_Req_Fixed.getValue(1,":",52), pnd_Output_Req_Data.getValue("*"));                                  //Natural: WRITE WORK FILE 02 #INPUT-REQ-FIXED ( 1:52 ) #OUTPUT-REQ-DATA ( * )
                pnd_Prev_Key_Data.setValue(pnd_Input_All_Pnd_Key_Data);                                                                                                   //Natural: ASSIGN #PREV-KEY-DATA := #KEY-DATA
                pnd_Prev_Kd_Univ_Id.setValue(pnd_Input_All_Pnd_Kd_Univ_Id);                                                                                               //Natural: ASSIGN #PREV-KD-UNIV-ID := #KD-UNIV-ID
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pnd_Input_All_Pnd_Record_Type.equals("D"))))
            {
                decideConditionsMet159++;
                pnd_Output_Req_Data.getValue("*").setValue(pnd_Input_All_Pnd_Detail_Data.getValue("*"));                                                                  //Natural: MOVE #DETAIL-DATA ( * ) TO #OUTPUT-REQ-DATA ( * )
                getWorkFiles().write(2, false, pnd_Input_All_Pnd_Input_Req_Fixed.getValue(1,":",52), pnd_Output_Req_Data.getValue("*"));                                  //Natural: WRITE WORK FILE 02 #INPUT-REQ-FIXED ( 1:52 ) #OUTPUT-REQ-DATA ( * )
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: VALUE 'T'
            else if (condition((pnd_Input_All_Pnd_Record_Type.equals("T"))))
            {
                decideConditionsMet159++;
                pnd_Output_Req_Data.getValue(1,":",7).setValue(pnd_Input_All_Pnd_Trailer_Data.getValue(1,":",7));                                                         //Natural: MOVE #TRAILER-DATA ( 1:7 ) TO #OUTPUT-REQ-DATA ( 1:7 )
                getWorkFiles().write(2, false, pnd_Input_All_Pnd_Input_Req_Fixed.getValue(1,":",52), pnd_Output_Req_Data.getValue("*"));                                  //Natural: WRITE WORK FILE 02 #INPUT-REQ-FIXED ( 1:52 ) #OUTPUT-REQ-DATA ( * )
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                getReports().write(1, "*** ERROR> INVALID RECORD TYPE IN INPUT:",pnd_Input_All_Pnd_Record_Type);                                                          //Natural: WRITE ( 1 ) '*** ERROR> INVALID RECORD TYPE IN INPUT:' #RECORD-TYPE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *
            //* *
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *
        if (condition(pnd_Batch_Restart_Key_Pnd_Brk_Job_Name.equals(" ")))                                                                                                //Natural: IF #BRK-JOB-NAME = ' '
        {
            pnd_User_Job.setValue(Global.getINIT_USER());                                                                                                                 //Natural: ASSIGN #USER-JOB := *INIT-USER
            if (condition(pnd_User_Job.getSubstring(2,4).equals("4010")))                                                                                                 //Natural: IF SUBSTR ( #USER-JOB,2,4 ) = '4010'
            {
                pnd_Batch_Restart_Key_Pnd_Brk_Job_Name.setValue("PSLOAN");                                                                                                //Natural: ASSIGN #BRK-JOB-NAME := 'PSLOAN'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_User_Job.getSubstring(4,4).equals("3090")))                                                                                             //Natural: IF SUBSTR ( #USER-JOB,4,4 ) = '3090'
                {
                    pnd_Batch_Restart_Key_Pnd_Brk_Job_Name.setValue("PTRKMDOR");                                                                                          //Natural: ASSIGN #BRK-JOB-NAME := 'PTRKMDOR'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Batch_Restart_Key_Pnd_Brk_Job_Name.setValue("ZZZZZZZZ");                                                                                          //Natural: ASSIGN #BRK-JOB-NAME := 'ZZZZZZZZ'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(pnd_Record_Cnt.greater(getZero())))                                                                                                                 //Natural: IF #RECORD-CNT GT 0
        {
            if (condition((pnd_Input_All_Pnd_Kd_Pckg_Code.equals("PTRKMDOR") && ldaPldl520.getBatch_Restart_Rst_Cnt().less(48000)) && pnd_Req_Cnt.greaterOrEqual(pnd_Batch_Limit.getValue(pnd_P)))) //Natural: IF ( #KD-PCKG-CODE = 'PTRKMDOR' AND RST-CNT LT 48000 ) AND #REQ-CNT GE #BATCH-LIMIT ( #P )
            {
                pnd_Mdor_Cnt.nadd(pnd_Req_Cnt);                                                                                                                           //Natural: ADD #REQ-CNT TO #MDOR-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Req_Cnt.less(pnd_Batch_Limit.getValue(pnd_P))))                                                                                         //Natural: IF #REQ-CNT LT #BATCH-LIMIT ( #P )
                {
                    pnd_Input_All_Pnd_Key_Data.reset();                                                                                                                   //Natural: RESET #KEY-DATA #MDOR-CNT
                    pnd_Mdor_Cnt.reset();
                    if (condition(pnd_Req_Cnt.equals(getZero())))                                                                                                         //Natural: IF #REQ-CNT = 0
                    {
                        pnd_Prev_Key_Data.reset();                                                                                                                        //Natural: RESET #PREV-KEY-DATA
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-CURRENT-BATCH
            sub_End_Current_Batch();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, "*** NO RECORDS IN INPUT FILE; NOTHING TO PROCESS. ***");                                                                               //Natural: WRITE ( 1 ) '*** NO RECORDS IN INPUT FILE; NOTHING TO PROCESS. ***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* **
        if (condition(pnd_Req_Cnt.equals(getZero()) || pnd_Record_Cnt.equals(getZero())))                                                                                 //Natural: IF #REQ-CNT = 0 OR #RECORD-CNT = 0
        {
            getReports().write(1, NEWLINE,NEWLINE,"*** NO RECORDS IN INPUT DATA FILE FOR THIS RUN ***",NEWLINE,NEWLINE,"*** NO BATCH WILL BE CREATED AND SENT TO CCP   ***"); //Natural: WRITE ( 1 ) // '*** NO RECORDS IN INPUT DATA FILE FOR THIS RUN ***' // '*** NO BATCH WILL BE CREATED AND SENT TO CCP   ***'
            if (Global.isEscape()) return;
            if (condition(pnd_Batch_Restart_Key_Pnd_Sp_Actve_Job_Step.notEquals(" ")))                                                                                    //Natural: IF #SP-ACTVE-JOB-STEP NE ' '
            {
                ldaPldl520.getVw_batch_Restart().startDatabaseRead                                                                                                        //Natural: READ BATCH-RESTART WITH SP-ACTVE-JOB-STEP = #SP-ACTVE-JOB-STEP
                (
                "PND_PND_L1900",
                new Wc[] { new Wc("SP_ACTVE_JOB_STEP", ">=", pnd_Batch_Restart_Key_Pnd_Sp_Actve_Job_Step, WcType.BY) },
                new Oc[] { new Oc("SP_ACTVE_JOB_STEP", "ASC") }
                );
                PND_PND_L1900:
                while (condition(ldaPldl520.getVw_batch_Restart().readNextRow("PND_PND_L1900")))
                {
                    if (condition(ldaPldl520.getBatch_Restart_Rst_Actve_Ind().notEquals(pnd_Batch_Restart_Key_Pnd_Brk_Active_Ind) || ldaPldl520.getBatch_Restart_Rst_Job_Nme().notEquals(pnd_Batch_Restart_Key_Pnd_Brk_Job_Name)  //Natural: IF RST-ACTVE-IND NE #BRK-ACTIVE-IND OR RST-JOB-NME NE #BRK-JOB-NAME OR RST-JOBSTEP-NME NE #BRK-JOBSTEP-NAME
                        || ldaPldl520.getBatch_Restart_Rst_Jobstep_Nme().notEquals(pnd_Batch_Restart_Key_Pnd_Brk_Jobstep_Name)))
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                    ldaPldl520.getVw_batch_Restart().deleteDBRow("PND_PND_L1900");                                                                                        //Natural: DELETE ( ##L1900. )
                    getReports().write(1, NEWLINE,NEWLINE,"*** DELETING RESTART REC:",pnd_Batch_Restart_Key_Pnd_Sp_Actve_Job_Step,"***",ldaPldl520.getBatch_Restart_Rst_Pgm_Nme(), //Natural: WRITE ( 1 ) // '*** DELETING RESTART REC:' #SP-ACTVE-JOB-STEP '***' RST-PGM-NME RST-CURR-DTE RST-CURR-TME
                        ldaPldl520.getBatch_Restart_Rst_Curr_Dte(),ldaPldl520.getBatch_Restart_Rst_Curr_Tme());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1900"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1900"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            //*  TERMINATE 1                                         /* C493059
            //*  C493059
            DbsUtil.terminate(2);  if (true) return;                                                                                                                      //Natural: TERMINATE 2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, NEWLINE,NEWLINE,"*** RESTART DATA FOR NEXT RUN ***",NEWLINE,NEWLINE,"=",ldaPldl520.getBatch_Restart_Rst_Cnt(),"=",ldaPldl520.getBatch_Restart_Rst_Key()); //Natural: WRITE ( 1 ) // '*** RESTART DATA FOR NEXT RUN ***' // '=' RST-CNT '=' RST-KEY
            if (Global.isEscape()) return;
            if (condition(pnd_Daily_Mdor_Complete.getBoolean()))                                                                                                          //Natural: IF #DAILY-MDOR-COMPLETE
            {
                DbsUtil.terminate(1);  if (true) return;                                                                                                                  //Natural: TERMINATE 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ***
        //* *====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-RESTART
        //* *====================================
        //* *
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-CURRENT-BATCH
        //* **
        //* *
        //* *====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RESTART-INFO
        //* *====================================
        //* *
        //* *====================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-BATCH-RESTART
        //* *====================================
        //* ***
    }
    private void sub_Check_For_Restart() throws Exception                                                                                                                 //Natural: CHECK-FOR-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        ldaPldl520.getVw_batch_Restart().startDatabaseFind                                                                                                                //Natural: FIND ( 1 ) BATCH-RESTART WITH SP-ACTVE-JOB-STEP = #SP-ACTVE-JOB-STEP
        (
        "PND_PND_L2180",
        new Wc[] { new Wc("SP_ACTVE_JOB_STEP", "=", pnd_Batch_Restart_Key_Pnd_Sp_Actve_Job_Step, WcType.WITH) },
        1
        );
        PND_PND_L2180:
        while (condition(ldaPldl520.getVw_batch_Restart().readNextRow("PND_PND_L2180", true)))
        {
            ldaPldl520.getVw_batch_Restart().setIfNotFoundControlFlag(false);
            if (condition(ldaPldl520.getVw_batch_Restart().getAstCOUNTER().equals(0)))                                                                                    //Natural: IF NO RECORD FOUND
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-BATCH-RESTART
                sub_Create_Batch_Restart();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L2180"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2180"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Batch_Restart_Isn.setValue(ldaPldl520.getVw_batch_Restart().getAstISN("PND_PND_L2180"));                                                                  //Natural: ASSIGN #BATCH-RESTART-ISN := *ISN ( ##L2180. )
            pnd_Curr_Restart_Key.setValue(ldaPldl520.getBatch_Restart_Rst_Key());                                                                                         //Natural: ASSIGN #CURR-RESTART-KEY := RST-KEY
            ldaPldl520.getBatch_Restart_Rst_Start_Dte().setValue(Global.getDATN());                                                                                       //Natural: ASSIGN RST-START-DTE := *DATN
            ldaPldl520.getBatch_Restart_Rst_Start_Tme().setValue(Global.getTIMN());                                                                                       //Natural: ASSIGN RST-START-TME := *TIMN
            ldaPldl520.getVw_batch_Restart().updateDBRow("PND_PND_L2180");                                                                                                //Natural: UPDATE ( ##L2180. )
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_End_Current_Batch() throws Exception                                                                                                                 //Natural: END-CURRENT-BATCH
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
                                                                                                                                                                          //Natural: PERFORM UPDATE-RESTART-INFO
        sub_Update_Restart_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Req_Cnt.equals(getZero())))                                                                                                                     //Natural: IF #REQ-CNT = 0
        {
            pnd_Input_All_Pnd_Key_Data.reset();                                                                                                                           //Natural: RESET #KEY-DATA
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, NEWLINE,NEWLINE,"COMPLETED BATCH HAS",pnd_Req_Cnt, new ReportEditMask ("ZZZ,ZZ9"),"TOTAL REQUESTS",NEWLINE,"LAST UNIV ID INCLUDED:  ",      //Natural: WRITE ( 1 ) // 'COMPLETED BATCH HAS' #REQ-CNT ( EM = ZZZ,ZZ9 ) 'TOTAL REQUESTS' / 'LAST UNIV ID INCLUDED:  ' #PREV-KD-UNIV-ID '=' #PREV-KEY-DATA / 'NEXT UNIV ID TO PROCESS:' #KD-UNIV-ID
            pnd_Prev_Kd_Univ_Id,"=",pnd_Prev_Key_Data,NEWLINE,"NEXT UNIV ID TO PROCESS:",pnd_Input_All_Pnd_Kd_Univ_Id);
        if (Global.isEscape()) return;
    }
    private void sub_Update_Restart_Info() throws Exception                                                                                                               //Natural: UPDATE-RESTART-INFO
    {
        if (BLNatReinput.isReinput()) return;

        PND_PND_L2530:                                                                                                                                                    //Natural: GET BATCH-RESTART #BATCH-RESTART-ISN
        ldaPldl520.getVw_batch_Restart().readByID(pnd_Batch_Restart_Isn.getLong(), "PND_PND_L2530");
        if (condition(pnd_Req_Cnt.equals(getZero())))                                                                                                                     //Natural: IF #REQ-CNT = 0
        {
            if (condition(pnd_Input_All_Pnd_Kd_Pckg_Code.notEquals("PTRKMDOR") || (pnd_Input_All_Pnd_Kd_Pckg_Code.equals("PTRKMDOR") && ldaPldl520.getBatch_Restart_Rst_Cnt().greaterOrEqual(48000)))) //Natural: IF #KD-PCKG-CODE NE 'PTRKMDOR' OR ( #KD-PCKG-CODE = 'PTRKMDOR' AND RST-CNT GE 48000 )
            {
                ldaPldl520.getBatch_Restart_Rst_Key().reset();                                                                                                            //Natural: RESET RST-KEY RST-CNT
                ldaPldl520.getBatch_Restart_Rst_Cnt().reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPldl520.getBatch_Restart_Rst_Key().setValue(pnd_Prev_Key_Data);                                                                                            //Natural: ASSIGN RST-KEY := #PREV-KEY-DATA
            if (condition(pnd_Input_All_Pnd_Kd_Pckg_Code.equals("PTRKMDOR")))                                                                                             //Natural: IF #KD-PCKG-CODE = 'PTRKMDOR'
            {
                if (condition(ldaPldl520.getBatch_Restart_Rst_Cnt().greaterOrEqual(48000)))                                                                               //Natural: IF RST-CNT GE 48000
                {
                    ldaPldl520.getBatch_Restart_Rst_Cnt().reset();                                                                                                        //Natural: RESET RST-CNT
                    pnd_Daily_Mdor_Complete.setValue(true);                                                                                                               //Natural: ASSIGN #DAILY-MDOR-COMPLETE := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPldl520.getBatch_Restart_Rst_Cnt().setValue(pnd_Mdor_Cnt);                                                                                         //Natural: ASSIGN RST-CNT := #MDOR-CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaPldl520.getBatch_Restart_Rst_Last_Rstrt_Dte().setValue(Global.getDATN());                                                                                      //Natural: ASSIGN RST-LAST-RSTRT-DTE := *DATN
        ldaPldl520.getBatch_Restart_Rst_Last_Rstrt_Tme().setValue(Global.getTIMN());                                                                                      //Natural: ASSIGN RST-LAST-RSTRT-TME := *TIMN
        ldaPldl520.getVw_batch_Restart().updateDBRow("PND_PND_L2530");                                                                                                    //Natural: UPDATE ( ##L2530. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, NEWLINE,NEWLINE,"=",ldaPldl520.getBatch_Restart_Rst_Cnt(),"=",ldaPldl520.getBatch_Restart_Rst_Key());                                       //Natural: WRITE // '=' RST-CNT '=' RST-KEY
        if (Global.isEscape()) return;
    }
    private void sub_Create_Batch_Restart() throws Exception                                                                                                              //Natural: CREATE-BATCH-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        ldaPldl520.getBatch_Restart_Rst_Actve_Ind().setValue("A");                                                                                                        //Natural: ASSIGN RST-ACTVE-IND := 'A'
        ldaPldl520.getBatch_Restart_Rst_Job_Nme().setValue(pnd_Input_All_Pnd_Kd_Pckg_Code);                                                                               //Natural: ASSIGN RST-JOB-NME := #KD-PCKG-CODE
        ldaPldl520.getBatch_Restart_Rst_Jobstep_Nme().setValue("FILSPLIT");                                                                                               //Natural: ASSIGN RST-JOBSTEP-NME := 'FILSPLIT'
        ldaPldl520.getBatch_Restart_Rst_Pgm_Nme().setValue(Global.getPROGRAM());                                                                                          //Natural: ASSIGN RST-PGM-NME := *PROGRAM
        ldaPldl520.getBatch_Restart_Rst_Curr_Dte().setValue(Global.getDATN());                                                                                            //Natural: ASSIGN RST-CURR-DTE := *DATN
        ldaPldl520.getBatch_Restart_Rst_Curr_Tme().setValue(Global.getTIMN());                                                                                            //Natural: ASSIGN RST-CURR-TME := *TIMN
        ldaPldl520.getBatch_Restart_Rst_Start_Dte().setValue(Global.getDATN());                                                                                           //Natural: ASSIGN RST-START-DTE := *DATN
        ldaPldl520.getBatch_Restart_Rst_Start_Tme().setValue(Global.getTIMN());                                                                                           //Natural: ASSIGN RST-START-TME := *TIMN
        ldaPldl520.getBatch_Restart_Rst_Cnt().setValue(0);                                                                                                                //Natural: ASSIGN RST-CNT := 0
        ldaPldl520.getBatch_Restart_Rst_Key().setValue(" ");                                                                                                              //Natural: ASSIGN RST-KEY := ' '
        PND_PND_L2940:                                                                                                                                                    //Natural: STORE BATCH-RESTART
        ldaPldl520.getVw_batch_Restart().insertDBRow("PND_PND_L2940");
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_Batch_Restart_Isn.setValue(ldaPldl520.getVw_batch_Restart().getAstISN("PND_PND_L2940"));                                                                      //Natural: ASSIGN #BATCH-RESTART-ISN := *ISN ( ##L2940. )
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
    }
}
