/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:16:59 PM
**        * FROM NATURAL PROGRAM : Pccb9560
************************************************************
**        * FILE NAME            : Pccb9560.java
**        * CLASS NAME           : Pccb9560
**        * INSTANCE NAME        : Pccb9560
************************************************************
************************************************************************
* PROGRAM  : PCCB9560
* SYSTEM   : CCP
* TITLE    : BUILD MQFTE COMMANDS FOR MAINFRAME TO CCP DATA TRANSFER
* FUNCTION : THIS MODULE BUILDS MQFTE COMMANDS CONTROL CARDS FOR CCP.
*          : USED FOR CREATING MULTIPLE COPY COMMANDS FOR SENDING
*          : NUMEROUS XML FILES TO CCP SERVER.
* CREATED  : SEPTEMBER 2015
*          :
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
* J. CRUZ  09/2015  INITIAL CODING
* J. CRUZ  10/2015  ASSIGN 081000 AS SYS-SUBSYS FOR NULL DMSALIP FILES.
* J. CRUZ  11/2015  CHANGE LAST SECTION OF DATAFILE DSN TO "HDRDTLS".
* J. CRUZ  06/2016  CHANGES TO ACCOMMODATE TAX FORMS MASS MIGRATION.
* J. CRUZ  07/2016  CHANGES TO ACCOMMODATE CISMDO & NET CHANGE FILES.
*
* **********************************************************************
*
*
* OPTIONS TQMARK=OFF
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Pccb9560 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_curr_Environment;
    private DbsField curr_Environment_Env_Id;
    private DbsField curr_Environment_Env_Descp;

    private DbsGroup pnd_Input_Parameters;
    private DbsField pnd_Input_Parameters_Pnd_Jobname;
    private DbsField pnd_Input_Parameters_Pnd_Input_Package_Cde;

    private DbsGroup filecnts_Data;
    private DbsField filecnts_Data_Pnd_File_Num;
    private DbsField filecnts_Data_Pnd_Job_Name;
    private DbsField filecnts_Data_Pnd_Job_File;
    private DbsField pnd_File_Id;
    private DbsField pnd_Src_Filename;
    private DbsField pnd_Dst_Filename;
    private DbsField pnd_Src_Agentname;
    private DbsField pnd_Dst_Agentname;
    private DbsField pnd_Src_Hlq;
    private DbsField pnd_Input_Dsn_To;
    private DbsField pnd_Dest_Path;
    private DbsField pnd_Curr_Env;
    private DbsField pnd_Sys_Subsys;
    private DbsField pnd_Dts;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_1;
    private DbsField pnd_Date_Pnd_Cc;
    private DbsField pnd_Date_Pnd_Yymmdd;
    private DbsField pnd_Time;
    private DbsField pnd_Date_5;
    private DbsField pnd_Xml_Quote;
    private DbsField pnd_Txt_Quote;
    private DbsField pnd_Trg_Quote;
    private DbsField pnd_Pkg_Parm80;

    private DbsGroup pnd_Pkg_Parm80__R_Field_2;
    private DbsField pnd_Pkg_Parm80_Pnd_Pp80_Package;
    private DbsField pnd_Pkg_Parm80_Pnd_Pp80_Sys_Subsys;
    private DbsField pnd_Pkg_Parm80_Pnd_Pp80_Filler;
    private DbsField pnd_Pkg_Parm80_Pnd_Pp80_Lights_On;

    private DbsGroup pnd_Profile_Data_In;
    private DbsField pnd_Profile_Data_In_Pnd_Profile_Id;
    private DbsField pnd_Profile_Data_In_Pnd_Profile_App;
    private DbsField pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys;

    private DbsGroup pnd_Seq_File;
    private DbsField pnd_Seq_File_Pnd_Seq_Var;
    private DbsField pnd_Seq_File_Pnd_Seq_Filler;
    private DbsField pnd_Prof_Pckg;
    private DbsField pnd_Prof_Sys_Subsys;
    private DbsField pnd_Workfile;
    private DbsField pnd_In_Profile;
    private DbsField pnd_Found_Pkg;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_P;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Wk_Line;
    private DbsField pnd_Mqfte_Line;

    private DbsGroup pnd_Mqfte_Line__R_Field_3;
    private DbsField pnd_Mqfte_Line_Pnd_Mqfte_Parm80;
    private DbsField pnd_Mqfte_Line_Pnd_Mqfte_Filler;
    private DbsField pnd_Mqfte_Command;

    private DbsGroup error_Handler_Fields;
    private DbsField error_Handler_Fields_Pnd_Error_Nr;
    private DbsField error_Handler_Fields_Pnd_Error_Line;
    private DbsField error_Handler_Fields_Pnd_Error_Status;
    private DbsField error_Handler_Fields_Pnd_Error_Program;
    private DbsField error_Handler_Fields_Pnd_Error_Level;
    private DbsField error_Handler_Fields_Pnd_Error_Appl;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_curr_Environment = new DataAccessProgramView(new NameInfo("vw_curr_Environment", "CURR-ENVIRONMENT"), "ENV_CURR", "ENV_CURR");
        curr_Environment_Env_Id = vw_curr_Environment.getRecord().newFieldInGroup("curr_Environment_Env_Id", "ENV-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENV_ID");
        curr_Environment_Env_Descp = vw_curr_Environment.getRecord().newFieldInGroup("curr_Environment_Env_Descp", "ENV-DESCP", FieldType.STRING, 16, 
            RepeatingFieldStrategy.None, "ENV_DESCP");
        registerRecord(vw_curr_Environment);

        pnd_Input_Parameters = localVariables.newGroupInRecord("pnd_Input_Parameters", "#INPUT-PARAMETERS");
        pnd_Input_Parameters_Pnd_Jobname = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_Jobname", "#JOBNAME", FieldType.STRING, 8);
        pnd_Input_Parameters_Pnd_Input_Package_Cde = pnd_Input_Parameters.newFieldInGroup("pnd_Input_Parameters_Pnd_Input_Package_Cde", "#INPUT-PACKAGE-CDE", 
            FieldType.STRING, 8);

        filecnts_Data = localVariables.newGroupInRecord("filecnts_Data", "FILECNTS-DATA");
        filecnts_Data_Pnd_File_Num = filecnts_Data.newFieldInGroup("filecnts_Data_Pnd_File_Num", "#FILE-NUM", FieldType.STRING, 2);
        filecnts_Data_Pnd_Job_Name = filecnts_Data.newFieldInGroup("filecnts_Data_Pnd_Job_Name", "#JOB-NAME", FieldType.STRING, 8);
        filecnts_Data_Pnd_Job_File = filecnts_Data.newFieldInGroup("filecnts_Data_Pnd_Job_File", "#JOB-FILE", FieldType.STRING, 8);
        pnd_File_Id = localVariables.newFieldInRecord("pnd_File_Id", "#FILE-ID", FieldType.STRING, 8);
        pnd_Src_Filename = localVariables.newFieldInRecord("pnd_Src_Filename", "#SRC-FILENAME", FieldType.STRING, 44);
        pnd_Dst_Filename = localVariables.newFieldInRecord("pnd_Dst_Filename", "#DST-FILENAME", FieldType.STRING, 80);
        pnd_Src_Agentname = localVariables.newFieldInRecord("pnd_Src_Agentname", "#SRC-AGENTNAME", FieldType.STRING, 44);
        pnd_Dst_Agentname = localVariables.newFieldInRecord("pnd_Dst_Agentname", "#DST-AGENTNAME", FieldType.STRING, 44);
        pnd_Src_Hlq = localVariables.newFieldInRecord("pnd_Src_Hlq", "#SRC-HLQ", FieldType.STRING, 22);
        pnd_Input_Dsn_To = localVariables.newFieldInRecord("pnd_Input_Dsn_To", "#INPUT-DSN-TO", FieldType.STRING, 80);
        pnd_Dest_Path = localVariables.newFieldInRecord("pnd_Dest_Path", "#DEST-PATH", FieldType.STRING, 60);
        pnd_Curr_Env = localVariables.newFieldInRecord("pnd_Curr_Env", "#CURR-ENV", FieldType.STRING, 2);
        pnd_Sys_Subsys = localVariables.newFieldInRecord("pnd_Sys_Subsys", "#SYS-SUBSYS", FieldType.STRING, 6);
        pnd_Dts = localVariables.newFieldInRecord("pnd_Dts", "#DTS", FieldType.STRING, 12);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Date__R_Field_1", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Cc = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Cc", "#CC", FieldType.STRING, 2);
        pnd_Date_Pnd_Yymmdd = pnd_Date__R_Field_1.newFieldInGroup("pnd_Date_Pnd_Yymmdd", "#YYMMDD", FieldType.STRING, 6);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.STRING, 7);
        pnd_Date_5 = localVariables.newFieldInRecord("pnd_Date_5", "#DATE-5", FieldType.STRING, 5);
        pnd_Xml_Quote = localVariables.newFieldInRecord("pnd_Xml_Quote", "#XML-QUOTE", FieldType.STRING, 5);
        pnd_Txt_Quote = localVariables.newFieldInRecord("pnd_Txt_Quote", "#TXT-QUOTE", FieldType.STRING, 5);
        pnd_Trg_Quote = localVariables.newFieldInRecord("pnd_Trg_Quote", "#TRG-QUOTE", FieldType.STRING, 5);
        pnd_Pkg_Parm80 = localVariables.newFieldInRecord("pnd_Pkg_Parm80", "#PKG-PARM80", FieldType.STRING, 80);

        pnd_Pkg_Parm80__R_Field_2 = localVariables.newGroupInRecord("pnd_Pkg_Parm80__R_Field_2", "REDEFINE", pnd_Pkg_Parm80);
        pnd_Pkg_Parm80_Pnd_Pp80_Package = pnd_Pkg_Parm80__R_Field_2.newFieldInGroup("pnd_Pkg_Parm80_Pnd_Pp80_Package", "#PP80-PACKAGE", FieldType.STRING, 
            8);
        pnd_Pkg_Parm80_Pnd_Pp80_Sys_Subsys = pnd_Pkg_Parm80__R_Field_2.newFieldInGroup("pnd_Pkg_Parm80_Pnd_Pp80_Sys_Subsys", "#PP80-SYS-SUBSYS", FieldType.STRING, 
            6);
        pnd_Pkg_Parm80_Pnd_Pp80_Filler = pnd_Pkg_Parm80__R_Field_2.newFieldInGroup("pnd_Pkg_Parm80_Pnd_Pp80_Filler", "#PP80-FILLER", FieldType.STRING, 
            15);
        pnd_Pkg_Parm80_Pnd_Pp80_Lights_On = pnd_Pkg_Parm80__R_Field_2.newFieldInGroup("pnd_Pkg_Parm80_Pnd_Pp80_Lights_On", "#PP80-LIGHTS-ON", FieldType.STRING, 
            1);

        pnd_Profile_Data_In = localVariables.newGroupInRecord("pnd_Profile_Data_In", "#PROFILE-DATA-IN");
        pnd_Profile_Data_In_Pnd_Profile_Id = pnd_Profile_Data_In.newFieldInGroup("pnd_Profile_Data_In_Pnd_Profile_Id", "#PROFILE-ID", FieldType.STRING, 
            2);
        pnd_Profile_Data_In_Pnd_Profile_App = pnd_Profile_Data_In.newFieldInGroup("pnd_Profile_Data_In_Pnd_Profile_App", "#PROFILE-APP", FieldType.STRING, 
            8);
        pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys = pnd_Profile_Data_In.newFieldInGroup("pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys", "#PROFILE-SYS-SUBSYS", 
            FieldType.STRING, 6);

        pnd_Seq_File = localVariables.newGroupInRecord("pnd_Seq_File", "#SEQ-FILE");
        pnd_Seq_File_Pnd_Seq_Var = pnd_Seq_File.newFieldInGroup("pnd_Seq_File_Pnd_Seq_Var", "#SEQ-VAR", FieldType.STRING, 7);
        pnd_Seq_File_Pnd_Seq_Filler = pnd_Seq_File.newFieldInGroup("pnd_Seq_File_Pnd_Seq_Filler", "#SEQ-FILLER", FieldType.STRING, 73);
        pnd_Prof_Pckg = localVariables.newFieldArrayInRecord("pnd_Prof_Pckg", "#PROF-PCKG", FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Prof_Sys_Subsys = localVariables.newFieldArrayInRecord("pnd_Prof_Sys_Subsys", "#PROF-SYS-SUBSYS", FieldType.STRING, 6, new DbsArrayController(1, 
            10));
        pnd_Workfile = localVariables.newFieldInRecord("pnd_Workfile", "#WORKFILE", FieldType.PACKED_DECIMAL, 2);
        pnd_In_Profile = localVariables.newFieldInRecord("pnd_In_Profile", "#IN-PROFILE", FieldType.STRING, 2);
        pnd_Found_Pkg = localVariables.newFieldInRecord("pnd_Found_Pkg", "#FOUND-PKG", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.PACKED_DECIMAL, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 3);
        pnd_Wk_Line = localVariables.newFieldInRecord("pnd_Wk_Line", "#WK-LINE", FieldType.STRING, 80);
        pnd_Mqfte_Line = localVariables.newFieldInRecord("pnd_Mqfte_Line", "#MQFTE-LINE", FieldType.STRING, 200);

        pnd_Mqfte_Line__R_Field_3 = localVariables.newGroupInRecord("pnd_Mqfte_Line__R_Field_3", "REDEFINE", pnd_Mqfte_Line);
        pnd_Mqfte_Line_Pnd_Mqfte_Parm80 = pnd_Mqfte_Line__R_Field_3.newFieldInGroup("pnd_Mqfte_Line_Pnd_Mqfte_Parm80", "#MQFTE-PARM80", FieldType.STRING, 
            80);
        pnd_Mqfte_Line_Pnd_Mqfte_Filler = pnd_Mqfte_Line__R_Field_3.newFieldInGroup("pnd_Mqfte_Line_Pnd_Mqfte_Filler", "#MQFTE-FILLER", FieldType.STRING, 
            120);
        pnd_Mqfte_Command = localVariables.newFieldArrayInRecord("pnd_Mqfte_Command", "#MQFTE-COMMAND", FieldType.STRING, 120, new DbsArrayController(1, 
            9));

        error_Handler_Fields = localVariables.newGroupInRecord("error_Handler_Fields", "ERROR-HANDLER-FIELDS");
        error_Handler_Fields_Pnd_Error_Nr = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Nr", "#ERROR-NR", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Line = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Line", "#ERROR-LINE", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Status = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Status", "#ERROR-STATUS", FieldType.STRING, 
            1);
        error_Handler_Fields_Pnd_Error_Program = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Program", "#ERROR-PROGRAM", FieldType.STRING, 
            8);
        error_Handler_Fields_Pnd_Error_Level = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Level", "#ERROR-LEVEL", FieldType.NUMERIC, 
            2);
        error_Handler_Fields_Pnd_Error_Appl = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Appl", "#ERROR-APPL", FieldType.STRING, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_curr_Environment.reset();

        localVariables.reset();
        pnd_Xml_Quote.setInitialValue(".xml'");
        pnd_Txt_Quote.setInitialValue(".txt'");
        pnd_Trg_Quote.setInitialValue(".trg'");
        pnd_Mqfte_Command.getValue(1).setInitialValue("TIAA_WMQFTE_USERXFER_FUNCTION=FILECOPY");
        pnd_Mqfte_Command.getValue(2).setInitialValue("TIAA_WMQFTE_USERXFER_SRC_AGENTNAME=AGMVSDNDV_SND");
        pnd_Mqfte_Command.getValue(3).setInitialValue("TIAA_WMQFTE_USERXFER_SRC_FILENAME=");
        pnd_Mqfte_Command.getValue(4).setInitialValue("TIAA_WMQFTE_USERXFER_DST_AGENTNAME=AGS2DENS2B3WLCPE01_RCV");
        pnd_Mqfte_Command.getValue(5).setInitialValue("TIAA_WMQFTE_USERXFER_DST_FILENAME='/ecs-app/ccp/event-processor/drop/");
        pnd_Mqfte_Command.getValue(6).setInitialValue("TIAA_WMQFTE_USERXFER_JOBNAME=");
        pnd_Mqfte_Command.getValue(7).setInitialValue("TIAA_WMQFTE_USERXFER_OVERWRITE=FALSE");
        pnd_Mqfte_Command.getValue(8).setInitialValue("TIAA_WMQFTE_USERXFER_CONVERSION=TEXT");
        pnd_Mqfte_Command.getValue(9).setInitialValue("TIAA_WMQFTE_USERXFER_PRIORITY=3");
        error_Handler_Fields_Pnd_Error_Status.setInitialValue("O");
        error_Handler_Fields_Pnd_Error_Program.setInitialValue(Global.getPROGRAM());
        error_Handler_Fields_Pnd_Error_Appl.setInitialValue("POST");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Pccb9560() throws Exception
    {
        super("Pccb9560");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 80 PS = 60;//Natural: FORMAT ( 0 ) PS = 60 LS = 132
        READWORK01:                                                                                                                                                       //Natural: READ WORK 6 #SEQ-FILE
        while (condition(getWorkFiles().read(6, pnd_Seq_File)))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Input_Parameters_Pnd_Jobname.setValue(Global.getINIT_USER());                                                                                                 //Natural: ASSIGN #JOBNAME := *INIT-USER
        pnd_Input_Parameters_Pnd_Input_Package_Cde.setValue(Global.getETID());                                                                                            //Natural: ASSIGN #INPUT-PACKAGE-CDE := *ETID
        if (condition(pnd_Input_Parameters_Pnd_Input_Package_Cde.getSubstring(1,3).equals("PTX")))                                                                        //Natural: IF SUBSTR ( #INPUT-PACKAGE-CDE,1,3 ) = 'PTX'
        {
            READWORK02:                                                                                                                                                   //Natural: READ WORK 05 FILECNTS-DATA
            while (condition(getWorkFiles().read(5, filecnts_Data)))
            {
            }                                                                                                                                                             //Natural: END-WORK
            READWORK02_Exit:
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "=",pnd_Input_Parameters_Pnd_Input_Package_Cde,"=",filecnts_Data_Pnd_Job_File,"=",filecnts_Data_Pnd_File_Num);                              //Natural: WRITE '=' #INPUT-PACKAGE-CDE '=' #JOB-FILE '=' #FILE-NUM
        if (Global.isEscape()) return;
        if (condition(pnd_Input_Parameters_Pnd_Input_Package_Cde.getSubstring(1,3).equals("PTX")))                                                                        //Natural: IF SUBSTR ( #INPUT-PACKAGE-CDE,1,3 ) = 'PTX'
        {
            pnd_File_Id.setValue(filecnts_Data_Pnd_Job_File);                                                                                                             //Natural: ASSIGN #FILE-ID := #JOB-FILE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_File_Id.setValue(pnd_Input_Parameters_Pnd_Input_Package_Cde);                                                                                             //Natural: ASSIGN #FILE-ID := #INPUT-PACKAGE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "=",pnd_File_Id);                                                                                                                           //Natural: WRITE '=' #FILE-ID
        if (Global.isEscape()) return;
        pnd_Mqfte_Line.reset();                                                                                                                                           //Natural: RESET #MQFTE-LINE
        vw_curr_Environment.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) CURR-ENVIRONMENT WITH ENV-ID = ' '
        (
        "READ03",
        new Wc[] { new Wc("ENV_ID", ">=", " ", WcType.BY) },
        new Oc[] { new Oc("ENV_ID", "ASC") },
        1
        );
        READ03:
        while (condition(vw_curr_Environment.readNextRow("READ03")))
        {
            pnd_Curr_Env.setValue(curr_Environment_Env_Id);                                                                                                               //Natural: ASSIGN #CURR-ENV := ENV-ID
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        short decideConditionsMet144 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #CURR-ENV;//Natural: VALUE 'P','P1','P2'
        if (condition((pnd_Curr_Env.equals("P") || pnd_Curr_Env.equals("P1") || pnd_Curr_Env.equals("P2"))))
        {
            decideConditionsMet144++;
            pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "PPT.COR.", pnd_File_Id));                                                               //Natural: COMPRESS 'PPT.COR.' #FILE-ID INTO #SRC-HLQ LEAVING NO
        }                                                                                                                                                                 //Natural: VALUE 'PF'
        else if (condition((pnd_Curr_Env.equals("PF"))))
        {
            decideConditionsMet144++;
            pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TPFX.COR.", pnd_File_Id));                                                              //Natural: COMPRESS 'TPFX.COR.' #FILE-ID INTO #SRC-HLQ LEAVING NO
        }                                                                                                                                                                 //Natural: VALUE 'U'
        else if (condition((pnd_Curr_Env.equals("U"))))
        {
            decideConditionsMet144++;
            pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TEMP.COR.", pnd_File_Id));                                                              //Natural: COMPRESS 'TEMP.COR.' #FILE-ID INTO #SRC-HLQ LEAVING NO
        }                                                                                                                                                                 //Natural: VALUE 'I1'
        else if (condition((pnd_Curr_Env.equals("I1"))))
        {
            decideConditionsMet144++;
            pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TINT.P0F.COR.", pnd_File_Id));                                                          //Natural: COMPRESS 'TINT.P0F.COR.' #FILE-ID INTO #SRC-HLQ LEAVING NO
        }                                                                                                                                                                 //Natural: VALUE 'I2'
        else if (condition((pnd_Curr_Env.equals("I2"))))
        {
            decideConditionsMet144++;
            pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TINT.P0A.COR.", pnd_File_Id));                                                          //Natural: COMPRESS 'TINT.P0A.COR.' #FILE-ID INTO #SRC-HLQ LEAVING NO
        }                                                                                                                                                                 //Natural: VALUE 'S1'
        else if (condition((pnd_Curr_Env.equals("S1"))))
        {
            decideConditionsMet144++;
            pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TSYS.COR.", pnd_File_Id));                                                              //Natural: COMPRESS 'TSYS.COR.' #FILE-ID INTO #SRC-HLQ LEAVING NO
        }                                                                                                                                                                 //Natural: VALUE 'S2'
        else if (condition((pnd_Curr_Env.equals("S2"))))
        {
            decideConditionsMet144++;
            pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TSYS2.COR.", pnd_File_Id));                                                             //Natural: COMPRESS 'TSYS2.COR.' #FILE-ID INTO #SRC-HLQ LEAVING NO
        }                                                                                                                                                                 //Natural: VALUE 'S4'
        else if (condition((pnd_Curr_Env.equals("S4"))))
        {
            decideConditionsMet144++;
            //*    IF #JOBNAME = 'YCC265JX'
            //*      COMPRESS 'TSYS4.COR.' #FILE-ID INTO #SRC-HLQ
            //*        LEAVING NO
            //*    ELSE
            pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TSYS4.COR.", pnd_File_Id));                                                             //Natural: COMPRESS 'TSYS4.COR.' #FILE-ID INTO #SRC-HLQ LEAVING NO
            //*    END-IF
        }                                                                                                                                                                 //Natural: VALUE 'A'
        else if (condition((pnd_Curr_Env.equals("A"))))
        {
            decideConditionsMet144++;
            pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "TACT.COR.", pnd_File_Id));                                                              //Natural: COMPRESS 'TACT.COR.' #FILE-ID INTO #SRC-HLQ LEAVING NO
        }                                                                                                                                                                 //Natural: VALUE 'DR'
        else if (condition((pnd_Curr_Env.equals("DR"))))
        {
            decideConditionsMet144++;
            pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "PPT.COR.", pnd_File_Id));                                                               //Natural: COMPRESS 'PPT.COR.' #FILE-ID INTO #SRC-HLQ LEAVING NO
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Src_Hlq.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "PPT.COR.", pnd_File_Id));                                                               //Natural: COMPRESS 'PPT.COR.' #FILE-ID INTO #SRC-HLQ LEAVING NO
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Curr_Env.equals("U") || pnd_Curr_Env.equals("I1") || pnd_Curr_Env.equals("I2") || pnd_Curr_Env.equals("S1") || pnd_Curr_Env.equals("S2")        //Natural: IF #CURR-ENV = 'U' OR = 'I1' OR = 'I2' OR = 'S1' OR = 'S2' OR = 'S4' OR = 'A' OR = 'PF'
            || pnd_Curr_Env.equals("S4") || pnd_Curr_Env.equals("A") || pnd_Curr_Env.equals("PF")))
        {
            pnd_Src_Agentname.setValue("AGMVSDNDV_SND");                                                                                                                  //Natural: ASSIGN #SRC-AGENTNAME := 'AGMVSDNDV_SND'
            pnd_Dst_Agentname.setValue("AG@@DEN@@B3WLCPE01_RCV");                                                                                                         //Natural: ASSIGN #DST-AGENTNAME := 'AG@@DEN@@B3WLCPE01_RCV'
            DbsUtil.examine(new ExamineSource(pnd_Dst_Agentname), new ExamineSearch("@@"), new ExamineReplace(pnd_Curr_Env));                                             //Natural: EXAMINE #DST-AGENTNAME FOR '@@' AND REPLACE WITH #CURR-ENV
            if (condition(pnd_Curr_Env.equals("S1")))                                                                                                                     //Natural: IF #CURR-ENV = 'S1'
            {
                DbsUtil.examine(new ExamineSource(pnd_Dst_Agentname), new ExamineSearch("S1"), new ExamineReplace("D2"));                                                 //Natural: EXAMINE #DST-AGENTNAME FOR 'S1' AND REPLACE WITH 'D2'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Curr_Env.equals("I2")))                                                                                                                     //Natural: IF #CURR-ENV = 'I2'
            {
                DbsUtil.examine(new ExamineSource(pnd_Dst_Agentname), new ExamineSearch("I2"), new ExamineReplace("D2"));                                                 //Natural: EXAMINE #DST-AGENTNAME FOR 'I2' AND REPLACE WITH 'D2'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Curr_Env.equals("PF")))                                                                                                                     //Natural: IF #CURR-ENV = 'PF'
            {
                DbsUtil.examine(new ExamineSource(pnd_Dst_Agentname), new ExamineSearch("B3"), new ExamineReplace("A3"));                                                 //Natural: EXAMINE #DST-AGENTNAME FOR 'B3' AND REPLACE WITH 'A3'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Src_Agentname.setValue("AGMVSPROD1_SND");                                                                                                                 //Natural: ASSIGN #SRC-AGENTNAME := 'AGMVSPROD1_SND'
            if (condition(pnd_Curr_Env.equals("DR")))                                                                                                                     //Natural: IF #CURR-ENV = 'DR'
            {
                pnd_Dst_Agentname.setValue("AGDRDENDRA3WLCPE01_RCV");                                                                                                     //Natural: ASSIGN #DST-AGENTNAME := 'AGDRDENDRA3WLCPE01_RCV'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Dst_Agentname.setValue("AGPDCHAPDA3WLCPE01_RCV");                                                                                                     //Natural: ASSIGN #DST-AGENTNAME := 'AGPDCHAPDA3WLCPE01_RCV'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Found_Pkg.setValue(false);                                                                                                                                    //Natural: ASSIGN #FOUND-PKG := FALSE
        if (condition(pnd_Input_Parameters_Pnd_Input_Package_Cde.equals("PSLOAN") || pnd_Input_Parameters_Pnd_Input_Package_Cde.equals("PTRKMDOR") ||                     //Natural: IF #INPUT-PACKAGE-CDE = 'PSLOAN' OR = 'PTRKMDOR' OR SUBSTR ( #INPUT-PACKAGE-CDE,1,3 ) = 'PTX' OR SUBSTR ( #INPUT-PACKAGE-CDE,1,3 ) = 'PNT'
            pnd_Input_Parameters_Pnd_Input_Package_Cde.getSubstring(1,3).equals("PTX") || pnd_Input_Parameters_Pnd_Input_Package_Cde.getSubstring(1,3).equals("PNT")))
        {
            READWORK04:                                                                                                                                                   //Natural: READ WORK 01 #PKG-PARM80
            while (condition(getWorkFiles().read(1, pnd_Pkg_Parm80)))
            {
                if (condition(pnd_Pkg_Parm80_Pnd_Pp80_Package.equals(pnd_Input_Parameters_Pnd_Input_Package_Cde)))                                                        //Natural: IF #PP80-PACKAGE = #INPUT-PACKAGE-CDE
                {
                    pnd_Found_Pkg.setValue(true);                                                                                                                         //Natural: ASSIGN #FOUND-PKG := TRUE
                    getReports().write(0, "**** FOUND PACKAGE IN POST2DCS ****");                                                                                         //Natural: WRITE '**** FOUND PACKAGE IN POST2DCS ****'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, pnd_Pkg_Parm80_Pnd_Pp80_Package,pnd_Pkg_Parm80_Pnd_Pp80_Sys_Subsys,pnd_Pkg_Parm80_Pnd_Pp80_Lights_On);                          //Natural: WRITE #PP80-PACKAGE #PP80-SYS-SUBSYS #PP80-LIGHTS-ON
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Sys_Subsys.setValue(pnd_Pkg_Parm80_Pnd_Pp80_Sys_Subsys);                                                                                          //Natural: ASSIGN #SYS-SUBSYS := #PP80-SYS-SUBSYS
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-WORK
            READWORK04_Exit:
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            READWORK05:                                                                                                                                                   //Natural: READ WORK 05 #PROFILE-DATA-IN
            while (condition(getWorkFiles().read(5, pnd_Profile_Data_In)))
            {
                if (condition(pnd_Profile_Data_In_Pnd_Profile_App.equals(pnd_Input_Parameters_Pnd_Input_Package_Cde)))                                                    //Natural: IF #PROFILE-APP = #INPUT-PACKAGE-CDE
                {
                    getReports().write(0, "**** FOUND PACKAGE IN profile-app-data ****");                                                                                 //Natural: WRITE '**** FOUND PACKAGE IN profile-app-data ****'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, pnd_Profile_Data_In_Pnd_Profile_App,pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys);                                                //Natural: WRITE #PROFILE-APP #PROFILE-SYS-SUBSYS
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Sys_Subsys.setValue(pnd_Profile_Data_In_Pnd_Profile_Sys_Subsys);                                                                                  //Natural: ASSIGN #SYS-SUBSYS := #PROFILE-SYS-SUBSYS
                    pnd_Found_Pkg.setValue(true);                                                                                                                         //Natural: ASSIGN #FOUND-PKG := TRUE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-WORK
            READWORK05_Exit:
            if (Global.isEscape()) return;
            if (condition(! (pnd_Found_Pkg.getBoolean())))                                                                                                                //Natural: IF NOT #FOUND-PKG
            {
                pnd_Sys_Subsys.setValue("081000");                                                                                                                        //Natural: ASSIGN #SYS-SUBSYS := '081000'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Found_Pkg.getBoolean()))                                                                                                                        //Natural: IF #FOUND-PKG
        {
                                                                                                                                                                          //Natural: PERFORM BUILD-MQFTE-COMMANDS
            sub_Build_Mqfte_Commands();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pkg_Parm80_Pnd_Pp80_Lights_On.reset();                                                                                                                    //Natural: RESET #PP80-LIGHTS-ON
            DbsUtil.terminate(1);  if (true) return;                                                                                                                      //Natural: TERMINATE 1
        }                                                                                                                                                                 //Natural: END-IF
        //*  ---------------------------------------------------------------------
        //*                S T A R T    O F    S U B R O U T I N E S
        //*  ---------------------------------------------------------------------
        //*  -----------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-MQFTE-COMMANDS
    }
    private void sub_Build_Mqfte_Commands() throws Exception                                                                                                              //Natural: BUILD-MQFTE-COMMANDS
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------------------
        if (condition(pnd_Input_Parameters_Pnd_Input_Package_Cde.equals("PSLOAN") || pnd_Input_Parameters_Pnd_Input_Package_Cde.equals("PTRKMDOR")))                      //Natural: IF #INPUT-PACKAGE-CDE = 'PSLOAN' OR = 'PTRKMDOR'
        {
            pnd_Src_Filename.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Src_Hlq, ".", pnd_Seq_File_Pnd_Seq_Var, ".HDRDTLS"));                           //Natural: COMPRESS #SRC-HLQ '.' #SEQ-VAR '.HDRDTLS' INTO #SRC-FILENAME LEAVING NO
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Src_Filename.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Src_Hlq, ".", pnd_Seq_File_Pnd_Seq_Var, ".XMLOUT.FILE"));                       //Natural: COMPRESS #SRC-HLQ '.' #SEQ-VAR '.XMLOUT.FILE' INTO #SRC-FILENAME LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        //*   SETUP THE DESTINATION FILENAME
        pnd_Date.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                                         //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #DATE
        pnd_Date_5.setValue(pnd_Date.getSubstring(4,5));                                                                                                                  //Natural: ASSIGN #DATE-5 := SUBSTR ( #DATE,4,5 )
        pnd_Time.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                                                          //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO #TIME
        //*  #TIME-6 := SUBSTR(#TIME,1,6)
        //*  COMPRESS #DATE #TIME-6 INTO #DTS LEAVING NO
        pnd_Dts.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Date_5, pnd_Time));                                                                          //Natural: COMPRESS #DATE-5 #TIME INTO #DTS LEAVING NO
        pnd_Input_Dsn_To.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "event-processor/drop/", pnd_Sys_Subsys, "-", pnd_Date_Pnd_Yymmdd, "-",                 //Natural: COMPRESS 'event-processor/drop/' #SYS-SUBSYS '-' #YYMMDD '-' #DTS '-' #INPUT-PACKAGE-CDE INTO #INPUT-DSN-TO LEAVING NO
            pnd_Dts, "-", pnd_Input_Parameters_Pnd_Input_Package_Cde));
        if (condition(pnd_Input_Parameters_Pnd_Input_Package_Cde.equals("PSLOAN") || pnd_Input_Parameters_Pnd_Input_Package_Cde.equals("PTRKMDOR")))                      //Natural: IF #INPUT-PACKAGE-CDE = 'PSLOAN' OR = 'PTRKMDOR'
        {
            pnd_Dst_Filename.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Dsn_To, pnd_Txt_Quote));                                                  //Natural: COMPRESS #INPUT-DSN-TO #TXT-QUOTE INTO #DST-FILENAME LEAVING NO
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Dst_Filename.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Dsn_To, pnd_Xml_Quote));                                                  //Natural: COMPRESS #INPUT-DSN-TO #XML-QUOTE INTO #DST-FILENAME LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        READWORK06:                                                                                                                                                       //Natural: READ WORK 02 #MQFTE-PARM80
        while (condition(getWorkFiles().read(2, pnd_Mqfte_Line_Pnd_Mqfte_Parm80)))
        {
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@SRC-FILENAME"), new ExamineGivingPosition(pnd_X));                                 //Natural: EXAMINE #MQFTE-LINE FOR '@SRC-FILENAME' GIVING POSITION IN #X
                if (condition(pnd_X.greater(getZero())))                                                                                                                  //Natural: IF #X GT 0
                {
                    pnd_X.nsubtract(1);                                                                                                                                   //Natural: SUBTRACT 1 FROM #X
                    pnd_Wk_Line.setValue(pnd_Mqfte_Line.getSubstring(1,pnd_X.getInt()));                                                                                  //Natural: ASSIGN #WK-LINE := SUBSTR ( #MQFTE-LINE,1,#X )
                    pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wk_Line, pnd_Src_Filename, "'\\'"));                                      //Natural: COMPRESS #WK-LINE #SRC-FILENAME ""\"" INTO #MQFTE-LINE LEAVING NO
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@DST-FILENAME"), new ExamineGivingPosition(pnd_X));                                 //Natural: EXAMINE #MQFTE-LINE FOR '@DST-FILENAME' GIVING POSITION IN #X
                if (condition(pnd_X.greater(getZero())))                                                                                                                  //Natural: IF #X GT 0
                {
                    pnd_X.nsubtract(1);                                                                                                                                   //Natural: SUBTRACT 1 FROM #X
                    pnd_Wk_Line.setValue(pnd_Mqfte_Line.getSubstring(1,pnd_X.getInt()));                                                                                  //Natural: ASSIGN #WK-LINE := SUBSTR ( #MQFTE-LINE,1,#X )
                    pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wk_Line, pnd_Dst_Filename, "'"));                                         //Natural: COMPRESS #WK-LINE #DST-FILENAME "'" INTO #MQFTE-LINE LEAVING NO
                    if (condition(pnd_Curr_Env.equals("I2")))                                                                                                             //Natural: IF #CURR-ENV = 'I2'
                    {
                        DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("ccp/event-processor"), new ExamineReplace("ccp-dev-2/event-processor"));    //Natural: EXAMINE #MQFTE-LINE FOR 'ccp/event-processor' REPLACE WITH 'ccp-dev-2/event-processor'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@SRC-AGENTNAME"), new ExamineGivingPosition(pnd_X));                                //Natural: EXAMINE #MQFTE-LINE FOR '@SRC-AGENTNAME' GIVING POSITION IN #X
                if (condition(pnd_X.greater(getZero())))                                                                                                                  //Natural: IF #X GT 0
                {
                    pnd_X.nsubtract(1);                                                                                                                                   //Natural: SUBTRACT 1 FROM #X
                    pnd_Wk_Line.setValue(pnd_Mqfte_Line.getSubstring(1,pnd_X.getInt()));                                                                                  //Natural: ASSIGN #WK-LINE := SUBSTR ( #MQFTE-LINE,1,#X )
                    pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wk_Line, pnd_Src_Agentname, "'"));                                        //Natural: COMPRESS #WK-LINE #SRC-AGENTNAME "'" INTO #MQFTE-LINE LEAVING NO
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@DST-AGENTNAME"), new ExamineGivingPosition(pnd_X));                                //Natural: EXAMINE #MQFTE-LINE FOR '@DST-AGENTNAME' GIVING POSITION IN #X
                if (condition(pnd_X.greater(getZero())))                                                                                                                  //Natural: IF #X GT 0
                {
                    pnd_X.nsubtract(1);                                                                                                                                   //Natural: SUBTRACT 1 FROM #X
                    pnd_Wk_Line.setValue(pnd_Mqfte_Line.getSubstring(1,pnd_X.getInt()));                                                                                  //Natural: ASSIGN #WK-LINE := SUBSTR ( #MQFTE-LINE,1,#X )
                    pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wk_Line, pnd_Dst_Agentname, "'"));                                        //Natural: COMPRESS #WK-LINE #DST-AGENTNAME "'" INTO #MQFTE-LINE LEAVING NO
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@JOBNAME"), new ExamineReplace(pnd_Input_Parameters_Pnd_Jobname));                  //Natural: EXAMINE #MQFTE-LINE FOR '@JOBNAME' REPLACE WITH #JOBNAME
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-REPEAT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(3, false, pnd_Mqfte_Line);                                                                                                               //Natural: WRITE WORK FILE 03 #MQFTE-LINE
            pnd_Mqfte_Line.reset();                                                                                                                                       //Natural: RESET #MQFTE-LINE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK06_Exit:
        if (Global.isEscape()) return;
        getWorkFiles().close(2);                                                                                                                                          //Natural: CLOSE WORK FILE 02
        pnd_Src_Filename.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Src_Hlq, ".", pnd_Seq_File_Pnd_Seq_Var, ".TRGFILE"));                               //Natural: COMPRESS #SRC-HLQ '.' #SEQ-VAR '.TRGFILE' INTO #SRC-FILENAME LEAVING NO SPACE
        //*   SETUP THE DESTINATION FILENAME - TRG FILE
        pnd_Dst_Filename.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Dsn_To, pnd_Trg_Quote));                                                      //Natural: COMPRESS #INPUT-DSN-TO #TRG-QUOTE INTO #DST-FILENAME LEAVING NO
        READWORK07:                                                                                                                                                       //Natural: READ WORK 02 #MQFTE-PARM80
        while (condition(getWorkFiles().read(2, pnd_Mqfte_Line_Pnd_Mqfte_Parm80)))
        {
            REPEAT02:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@SRC-FILENAME"), new ExamineGivingPosition(pnd_X));                                 //Natural: EXAMINE #MQFTE-LINE FOR '@SRC-FILENAME' GIVING POSITION IN #X
                if (condition(pnd_X.greater(getZero())))                                                                                                                  //Natural: IF #X GT 0
                {
                    pnd_X.nsubtract(1);                                                                                                                                   //Natural: SUBTRACT 1 FROM #X
                    pnd_Wk_Line.setValue(pnd_Mqfte_Line.getSubstring(1,pnd_X.getInt()));                                                                                  //Natural: ASSIGN #WK-LINE := SUBSTR ( #MQFTE-LINE,1,#X )
                    pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wk_Line, pnd_Src_Filename, "'\\'"));                                      //Natural: COMPRESS #WK-LINE #SRC-FILENAME ""\"" INTO #MQFTE-LINE LEAVING NO
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@DST-FILENAME"), new ExamineGivingPosition(pnd_X));                                 //Natural: EXAMINE #MQFTE-LINE FOR '@DST-FILENAME' GIVING POSITION IN #X
                if (condition(pnd_X.greater(getZero())))                                                                                                                  //Natural: IF #X GT 0
                {
                    pnd_X.nsubtract(1);                                                                                                                                   //Natural: SUBTRACT 1 FROM #X
                    pnd_Wk_Line.setValue(pnd_Mqfte_Line.getSubstring(1,pnd_X.getInt()));                                                                                  //Natural: ASSIGN #WK-LINE := SUBSTR ( #MQFTE-LINE,1,#X )
                    pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wk_Line, pnd_Dst_Filename, "'"));                                         //Natural: COMPRESS #WK-LINE #DST-FILENAME "'" INTO #MQFTE-LINE LEAVING NO
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@SRC-AGENTNAME"), new ExamineGivingPosition(pnd_X));                                //Natural: EXAMINE #MQFTE-LINE FOR '@SRC-AGENTNAME' GIVING POSITION IN #X
                if (condition(pnd_X.greater(getZero())))                                                                                                                  //Natural: IF #X GT 0
                {
                    pnd_X.nsubtract(1);                                                                                                                                   //Natural: SUBTRACT 1 FROM #X
                    pnd_Wk_Line.setValue(pnd_Mqfte_Line.getSubstring(1,pnd_X.getInt()));                                                                                  //Natural: ASSIGN #WK-LINE := SUBSTR ( #MQFTE-LINE,1,#X )
                    pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wk_Line, pnd_Src_Agentname, "'"));                                        //Natural: COMPRESS #WK-LINE #SRC-AGENTNAME "'" INTO #MQFTE-LINE LEAVING NO
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@DST-AGENTNAME"), new ExamineGivingPosition(pnd_X));                                //Natural: EXAMINE #MQFTE-LINE FOR '@DST-AGENTNAME' GIVING POSITION IN #X
                if (condition(pnd_X.greater(getZero())))                                                                                                                  //Natural: IF #X GT 0
                {
                    pnd_X.nsubtract(1);                                                                                                                                   //Natural: SUBTRACT 1 FROM #X
                    pnd_Wk_Line.setValue(pnd_Mqfte_Line.getSubstring(1,pnd_X.getInt()));                                                                                  //Natural: ASSIGN #WK-LINE := SUBSTR ( #MQFTE-LINE,1,#X )
                    pnd_Mqfte_Line.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wk_Line, pnd_Dst_Agentname, "'"));                                        //Natural: COMPRESS #WK-LINE #DST-AGENTNAME "'" INTO #MQFTE-LINE LEAVING NO
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pnd_Mqfte_Line), new ExamineSearch("@JOBNAME"), new ExamineReplace(pnd_Input_Parameters_Pnd_Jobname));                  //Natural: EXAMINE #MQFTE-LINE FOR '@JOBNAME' REPLACE WITH #JOBNAME
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-REPEAT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(4, false, pnd_Mqfte_Line);                                                                                                               //Natural: WRITE WORK FILE 04 #MQFTE-LINE
            pnd_Mqfte_Line.reset();                                                                                                                                       //Natural: RESET #MQFTE-LINE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK07_Exit:
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=80 PS=60");
        Global.format(0, "PS=60 LS=132");
    }
}
