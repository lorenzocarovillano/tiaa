/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:21:49 PM
**        * FROM NATURAL PROGRAM : Ridb774
************************************************************
**        * FILE NAME            : Ridb774.java
**        * CLASS NAME           : Ridb774
**        * INSTANCE NAME        : Ridb774
************************************************************
************************************************************************
** PROGRAM  : RIDB774                                                 **
** SYSTEM   : RIDER                                                   **
** TITLE    : CCP DATA GENERATION MODULE                              **
** NARRATIVE: RIDER MAILING FOR RETIREMENT PLAN LOAN (RPL)            **
**          : ENDORSEMENTS FOR RA/GRA/SRA CONTRACTS.                  **
** PURPOSE  : THIS MODULE GETS THE PACKAGE CODE, START REQUEST ID,    **
**          : END REQUEST ID AND TRACE INFORMATION FROM A PARM FILE.  **
**          : IT ALSO RETRIEVES SIGNATURE INFORMATION AND CREATES     **
**          : A FLAT FILE FOR XML GENERATION THAT WILL BE SENT TO CCP.**
**          : IT ALSO GENERATES STATISTICS REPORTS                    **
**                                                                    **
** HISTORY                                                            **
**                                                                    **
*   WHO      WHEN              WHY                                    **
* -------- --------   ------------------------------------------------**
* NEWSOM   03/25/2019 NEW                                             **
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridb774 extends BLNatBase
{
    // Data Areas
    private LdaPstl9902 ldaPstl9902;
    private PdaPsta9200 pdaPsta9200;
    private LdaRidl4040 ldaRidl4040;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Post_Data;
    private DbsField pnd_Post_Data_Pnd_Pst_Rqst_Id;
    private DbsField pnd_Post_Data_Pnd_Pin_Number;

    private DbsGroup pnd_Post_Data__R_Field_1;
    private DbsField pnd_Post_Data_Pnd_Pin_N7;
    private DbsField pnd_Post_Data_Pnd_Pin_A5;
    private DbsField pnd_Post_Data_Pnd_Tiaa_Cntrct_Number;
    private DbsField pnd_Post_Data_Pnd_Cref_Cntrct_Number;
    private DbsField pnd_Post_Data_Pnd_Full_Name;
    private DbsField pnd_Post_Data_Pnd_Last_Name;
    private DbsField pnd_Post_Data_Pnd_Address_Line_Txt;
    private DbsField pnd_Post_Data_Pnd_Postal_Data;
    private DbsField pnd_Post_Data_Pnd_Address_Type_Cde;
    private DbsField pnd_Post_Data_Pnd_Package_Code;
    private DbsField pnd_Post_Data_Pnd_Letter_Type;

    private DbsGroup pnd_Post_Data_Pnd_Retirement_Annuity;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Tx;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pr;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Il;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pa;
    private DbsField pnd_Post_Data_Pnd_C_Ra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Ar;
    private DbsField pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Pr;
    private DbsField pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Il1;

    private DbsGroup pnd_Post_Data_Pnd_Group_Retirement_Annuity;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Md;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Tx;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Pr;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Il;
    private DbsField pnd_Post_Data_Pnd_C_Gra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Tx;
    private DbsField pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Pr;
    private DbsField pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Il;

    private DbsGroup pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity;
    private DbsField pnd_Post_Data_Pnd_T_Sra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Tx;
    private DbsField pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Il;
    private DbsField pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Pa;
    private DbsField pnd_Post_Data_Pnd_C_Sra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Il1;
    private DbsField pnd_Post_Data_Pnd_Rider_Date;

    private DbsGroup pnd_Post_Data__R_Field_2;
    private DbsField pnd_Post_Data_Pnd_Rider_Month;
    private DbsField pnd_Post_Data_Pnd_Rider_Year;

    private DbsGroup pnd_Post_Data__R_Field_3;
    private DbsField pnd_Post_Data_Pnd_Rider_Year_A;
    private DbsField pnd_Post_Data_Pnd_Ra_Contracts;
    private DbsField pnd_Post_Data_Pnd_Gra_Contracts;
    private DbsField pnd_Post_Data_Pnd_Sra_Contracts;
    private DbsField pnd_Post_Data_Pnd_Ra_Tiaa_Contracts_Il;
    private DbsField pnd_Post_Data_Pnd_Ra_Cref_Contracts_Il1;
    private DbsField pnd_Post_Data_Pnd_Gra_Tiaa_Contracts_Il;
    private DbsField pnd_Post_Data_Pnd_Gra_Cref_Contracts_Il;
    private DbsField pnd_Post_Data_Pnd_Sra_Tiaa_Contracts_Il;
    private DbsField pnd_Post_Data_Pnd_Sra_Cref_Contracts_Il1;
    private DbsField pnd_File_No_Ctr;

    private DbsGroup constants;
    private DbsField constants_Pnd_Blank;
    private DbsField constants_Pnd_Error_Ind;

    private DbsGroup counters;
    private DbsField counters_Pnd_Cnt_Record_Read;
    private DbsField counters_Pnd_Cnt_Record_Write_Max;
    private DbsField counters_Pnd_Cnt_Print;
    private DbsField counters_Pnd_Cnt_Divert;
    private DbsField counters_Pnd_Cnt_Total;
    private DbsField counters_Pnd_L;
    private DbsField counters_Pnd_I;
    private DbsField counters_Pnd_J;

    private DbsGroup counters__R_Field_4;
    private DbsField counters_Pnd_J1;
    private DbsField counters_Pnd_M;
    private DbsField counters_Pnd_N;
    private DbsField counters_Pnd_Work_Idx;
    private DbsField counters_Pnd_Wk8_Count;
    private DbsField pnd_Mail_Item_Ctr;
    private DbsField pnd_Already_Done;
    private DbsField pnd_First_Call;
    private DbsField pnd_Pst_Tbl_Data_Field2;

    private DbsGroup pnd_Pst_Tbl_Data_Field2__R_Field_5;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4;
    private DbsField pls_Current_Mail_Id;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaPstl9902 = new LdaPstl9902();
        registerRecord(ldaPstl9902);
        localVariables = new DbsRecord();
        pdaPsta9200 = new PdaPsta9200(localVariables);
        ldaRidl4040 = new LdaRidl4040();
        registerRecord(ldaRidl4040);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);

        // Local Variables

        pnd_Post_Data = localVariables.newGroupInRecord("pnd_Post_Data", "#POST-DATA");
        pnd_Post_Data_Pnd_Pst_Rqst_Id = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pst_Rqst_Id", "#PST-RQST-ID", FieldType.STRING, 11);
        pnd_Post_Data_Pnd_Pin_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);

        pnd_Post_Data__R_Field_1 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data__R_Field_1", "REDEFINE", pnd_Post_Data_Pnd_Pin_Number);
        pnd_Post_Data_Pnd_Pin_N7 = pnd_Post_Data__R_Field_1.newFieldInGroup("pnd_Post_Data_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);
        pnd_Post_Data_Pnd_Pin_A5 = pnd_Post_Data__R_Field_1.newFieldInGroup("pnd_Post_Data_Pnd_Pin_A5", "#PIN-A5", FieldType.STRING, 5);
        pnd_Post_Data_Pnd_Tiaa_Cntrct_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Tiaa_Cntrct_Number", "#TIAA-CNTRCT-NUMBER", FieldType.STRING, 
            8);
        pnd_Post_Data_Pnd_Cref_Cntrct_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Cref_Cntrct_Number", "#CREF-CNTRCT-NUMBER", FieldType.STRING, 
            8);
        pnd_Post_Data_Pnd_Full_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Post_Data_Pnd_Last_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 35);
        pnd_Post_Data_Pnd_Address_Line_Txt = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 6));
        pnd_Post_Data_Pnd_Postal_Data = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 32);
        pnd_Post_Data_Pnd_Address_Type_Cde = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Package_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Post_Data_Pnd_Letter_Type = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);

        pnd_Post_Data_Pnd_Retirement_Annuity = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Retirement_Annuity", "#RETIREMENT-ANNUITY");
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_All_Other", "#T-RA-RPL-E1-ALL-OTHER", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Fl", "#T-RA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Tx = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Tx", "#T-RA-RPL-E1-TX", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pr = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pr", "#T-RA-RPL-E1-PR", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Il = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Il", "#T-RA-RPL-E1-IL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pa = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pa", "#T-RA-RPL-E1-PA", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Ra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Ra_Rpl_E1_All_Other", "#C-RA-RPL-E1-ALL-OTHER", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Ar = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Ar", "#C-RA-RPL-E1-AR", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Fl", "#C-RA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Pr = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Pr", "#C-RA-RPL-E1-PR", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Il1 = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Il1", "#C-RA-RPL-E1-IL1", 
            FieldType.NUMERIC, 8);

        pnd_Post_Data_Pnd_Group_Retirement_Annuity = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Group_Retirement_Annuity", "#GROUP-RETIREMENT-ANNUITY");
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_All_Other", 
            "#T-GRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Fl", "#T-GRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Md = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Md", "#T-GRA-RPL-E1-MD", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Tx = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Tx", "#T-GRA-RPL-E1-TX", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Pr = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Pr", "#T-GRA-RPL-E1-PR", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Il = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Il", "#T-GRA-RPL-E1-IL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Gra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Gra_Rpl_E1_All_Other", 
            "#C-GRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Fl", "#C-GRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Tx = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Tx", "#C-GRA-RPL-E1-TX", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Pr = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Pr", "#C-GRA-RPL-E1-PR", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Il = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Il", "#C-GRA-RPL-E1-IL", 
            FieldType.NUMERIC, 8);

        pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity", "#SUPPLEMENTAL-RETIREMENT-ANNUITY");
        pnd_Post_Data_Pnd_T_Sra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Sra_Rpl_E1_All_Other", 
            "#T-SRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Fl", "#T-SRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Tx = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Tx", "#T-SRA-RPL-E1-TX", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Il = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Il", "#T-SRA-RPL-E1-IL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Pa = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Pa", "#T-SRA-RPL-E1-PA", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Sra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Sra_Rpl_E1_All_Other", 
            "#C-SRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Fl", "#C-SRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Il1 = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Il1", "#C-SRA-RPL-E1-IL1", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_Rider_Date = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Date", "#RIDER-DATE", FieldType.NUMERIC, 6);

        pnd_Post_Data__R_Field_2 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data__R_Field_2", "REDEFINE", pnd_Post_Data_Pnd_Rider_Date);
        pnd_Post_Data_Pnd_Rider_Month = pnd_Post_Data__R_Field_2.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Month", "#RIDER-MONTH", FieldType.NUMERIC, 2);
        pnd_Post_Data_Pnd_Rider_Year = pnd_Post_Data__R_Field_2.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year", "#RIDER-YEAR", FieldType.NUMERIC, 4);

        pnd_Post_Data__R_Field_3 = pnd_Post_Data__R_Field_2.newGroupInGroup("pnd_Post_Data__R_Field_3", "REDEFINE", pnd_Post_Data_Pnd_Rider_Year);
        pnd_Post_Data_Pnd_Rider_Year_A = pnd_Post_Data__R_Field_3.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year_A", "#RIDER-YEAR-A", FieldType.STRING, 
            4);
        pnd_Post_Data_Pnd_Ra_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Ra_Contracts", "#RA-CONTRACTS", FieldType.STRING, 17, new 
            DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Gra_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Gra_Contracts", "#GRA-CONTRACTS", FieldType.STRING, 17, 
            new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Sra_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Sra_Contracts", "#SRA-CONTRACTS", FieldType.STRING, 17, 
            new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Ra_Tiaa_Contracts_Il = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Ra_Tiaa_Contracts_Il", "#RA-TIAA-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Ra_Cref_Contracts_Il1 = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Ra_Cref_Contracts_Il1", "#RA-CREF-CONTRACTS-IL1", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Gra_Tiaa_Contracts_Il = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Gra_Tiaa_Contracts_Il", "#GRA-TIAA-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Gra_Cref_Contracts_Il = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Gra_Cref_Contracts_Il", "#GRA-CREF-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Sra_Tiaa_Contracts_Il = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Sra_Tiaa_Contracts_Il", "#SRA-TIAA-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Sra_Cref_Contracts_Il1 = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Sra_Cref_Contracts_Il1", "#SRA-CREF-CONTRACTS-IL1", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pnd_File_No_Ctr = localVariables.newFieldInRecord("pnd_File_No_Ctr", "#FILE-NO-CTR", FieldType.PACKED_DECIMAL, 7);

        constants = localVariables.newGroupInRecord("constants", "CONSTANTS");
        constants_Pnd_Blank = constants.newFieldInGroup("constants_Pnd_Blank", "#BLANK", FieldType.STRING, 1);
        constants_Pnd_Error_Ind = constants.newFieldInGroup("constants_Pnd_Error_Ind", "#ERROR-IND", FieldType.STRING, 1);

        counters = localVariables.newGroupInRecord("counters", "COUNTERS");
        counters_Pnd_Cnt_Record_Read = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Read", "#CNT-RECORD-READ", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Write_Max = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Write_Max", "#CNT-RECORD-WRITE-MAX", FieldType.PACKED_DECIMAL, 
            7);
        counters_Pnd_Cnt_Print = counters.newFieldInGroup("counters_Pnd_Cnt_Print", "#CNT-PRINT", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Divert = counters.newFieldInGroup("counters_Pnd_Cnt_Divert", "#CNT-DIVERT", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Total = counters.newFieldInGroup("counters_Pnd_Cnt_Total", "#CNT-TOTAL", FieldType.NUMERIC, 7);
        counters_Pnd_L = counters.newFieldInGroup("counters_Pnd_L", "#L", FieldType.NUMERIC, 2);
        counters_Pnd_I = counters.newFieldInGroup("counters_Pnd_I", "#I", FieldType.NUMERIC, 2);
        counters_Pnd_J = counters.newFieldInGroup("counters_Pnd_J", "#J", FieldType.NUMERIC, 2);

        counters__R_Field_4 = counters.newGroupInGroup("counters__R_Field_4", "REDEFINE", counters_Pnd_J);
        counters_Pnd_J1 = counters__R_Field_4.newFieldInGroup("counters_Pnd_J1", "#J1", FieldType.STRING, 2);
        counters_Pnd_M = counters.newFieldInGroup("counters_Pnd_M", "#M", FieldType.PACKED_DECIMAL, 3);
        counters_Pnd_N = counters.newFieldInGroup("counters_Pnd_N", "#N", FieldType.PACKED_DECIMAL, 3);
        counters_Pnd_Work_Idx = counters.newFieldInGroup("counters_Pnd_Work_Idx", "#WORK-IDX", FieldType.INTEGER, 2);
        counters_Pnd_Wk8_Count = counters.newFieldInGroup("counters_Pnd_Wk8_Count", "#WK8-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Mail_Item_Ctr = localVariables.newFieldInRecord("pnd_Mail_Item_Ctr", "#MAIL-ITEM-CTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Already_Done = localVariables.newFieldInRecord("pnd_Already_Done", "#ALREADY-DONE", FieldType.BOOLEAN, 1);
        pnd_First_Call = localVariables.newFieldInRecord("pnd_First_Call", "#FIRST-CALL", FieldType.BOOLEAN, 1);
        pnd_Pst_Tbl_Data_Field2 = localVariables.newFieldInRecord("pnd_Pst_Tbl_Data_Field2", "#PST-TBL-DATA-FIELD2", FieldType.STRING, 253);

        pnd_Pst_Tbl_Data_Field2__R_Field_5 = localVariables.newGroupInRecord("pnd_Pst_Tbl_Data_Field2__R_Field_5", "REDEFINE", pnd_Pst_Tbl_Data_Field2);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory = pnd_Pst_Tbl_Data_Field2__R_Field_5.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory", "#MM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1 = pnd_Pst_Tbl_Data_Field2__R_Field_5.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1", "#MM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2 = pnd_Pst_Tbl_Data_Field2__R_Field_5.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2", "#MM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3 = pnd_Pst_Tbl_Data_Field2__R_Field_5.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3", "#MM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4 = pnd_Pst_Tbl_Data_Field2__R_Field_5.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4", "#MM-TITLE4", 
            FieldType.STRING, 50);
        pls_Current_Mail_Id = WsIndependent.getInstance().newFieldInRecord("pls_Current_Mail_Id", "+CURRENT-MAIL-ID", FieldType.STRING, 11);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaPstl9902.initializeValues();
        ldaRidl4040.initializeValues();

        localVariables.reset();
        pnd_File_No_Ctr.setInitialValue(1);
        constants_Pnd_Blank.setInitialValue(" ");
        constants_Pnd_Error_Ind.setInitialValue("E");
        pnd_Already_Done.setInitialValue(false);
        pls_Trace.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridb774() throws Exception
    {
        super("Ridb774");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDB774", onError);
        setupReports();
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
        //*  ----------------------------------------------------------------------                                                                                       //Natural: ON ERROR
        pnd_File_No_Ctr.setValue(1);                                                                                                                                      //Natural: ASSIGN #FILE-NO-CTR := 1
                                                                                                                                                                          //Natural: PERFORM GET-SIGNATORY
        sub_Get_Signatory();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-INPUT-PARMS
        sub_Get_Input_Parms();
        if (condition(Global.isEscape())) {return;}
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 19T 'RETIREMENT PLAN LOAN ENDORSEMENT MAILING' 65T 'DATE:' *DATU / 30T 'BATCH CONTROL TOTALS' 65T 'TIME:' *TIME ( EM = XXXXXXXX ) / 65T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT 'PROGRAM:' *PROGRAM 19T 'GSRA LOAN ENDORSEMENT MAILING' 65T 'DATE:' *DATU / 30T 'BATCH CONTROL DETAILS' 65T 'TIME:' *TIME ( EM = XXXXXXXX ) / 65T 'PAGE:' 3X *PAGE-NUMBER ( 2 ) // 27T 'PACKAGE CODE :  ' INPUT-PARMS.#PCKGE-CDE / / 10T '                                                            ' 1X 'ADDR' / 10T ' MAIL ITEM              FULL NAME                 PIN       ' 1X 'TYPE' / 10T '----------- ----------------------------------- ------------' 1X '----'
        pnd_First_Call.setValue(true);                                                                                                                                    //Natural: ASSIGN #FIRST-CALL := TRUE
        //*  ----------------------------------------------------------------------
        //*                  S T A R T    O F    M A I N    L O O P
        //*  ----------------------------------------------------------------------
        pnd_Mail_Item_Ctr.reset();                                                                                                                                        //Natural: RESET #MAIL-ITEM-CTR
        //*  RIDL772
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #POST-DATA
        while (condition(getWorkFiles().read(1, pnd_Post_Data)))
        {
            //*  FIRST AND LAST PST PER FILE.
            //*  ADDED FIRST ARRAR
            if (condition(pnd_First_Call.getBoolean()))                                                                                                                   //Natural: IF #FIRST-CALL
            {
                ldaRidl4040.getPnd_First_Pst().getValue(1).setValue(pnd_Post_Data_Pnd_Pst_Rqst_Id);                                                                       //Natural: ASSIGN #FIRST-PST ( 1 ) := #POST-DATA.#PST-RQST-ID
                pnd_First_Call.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-CALL := FALSE
                //*  ADDED FIRST ARRAY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaRidl4040.getPnd_Last_Pst().getValue(1).setValue(pnd_Post_Data_Pnd_Pst_Rqst_Id);                                                                        //Natural: ASSIGN #LAST-PST ( 1 ) := #POST-DATA.#PST-RQST-ID
            }                                                                                                                                                             //Natural: END-IF
            counters_Pnd_Cnt_Record_Read.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CNT-RECORD-READ
            //*  IF +TRACE
            //*    WRITE (0) '-' (131) /
            //*      *PROGRAM 'READ ITEM RQST:' #POST-DATA.#PST-RQST-ID
            //*  END-IF
            //*  MAKE CALL TO GENERATION MODULE TO GENERATE DATA
                                                                                                                                                                          //Natural: PERFORM CALL-GEN-MODULE
            sub_Call_Gen_Module();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, new TabSetting(10),pnd_Post_Data_Pnd_Pst_Rqst_Id,new TabSetting(22),pnd_Post_Data_Pnd_Full_Name, new AlphanumericLength                 //Natural: WRITE ( 2 ) 10T #POST-DATA.#PST-RQST-ID 22T #POST-DATA.#FULL-NAME ( AL = 33 ) 57T #POST-DATA.#PIN-NUMBER 73T #POST-DATA.#ADDRESS-TYPE-CDE
                (33),new TabSetting(57),pnd_Post_Data_Pnd_Pin_Number,new TabSetting(73),pnd_Post_Data_Pnd_Address_Type_Cde);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Mail_Item_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #MAIL-ITEM-CTR
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        counters_Pnd_Cnt_Total.compute(new ComputeParameters(false, counters_Pnd_Cnt_Total), counters_Pnd_Cnt_Total.add(counters_Pnd_Cnt_Print).add(counters_Pnd_Cnt_Divert)); //Natural: ADD #CNT-PRINT #CNT-DIVERT TO #CNT-TOTAL
        if (condition(counters_Pnd_Cnt_Total.notEquals(getZero())))                                                                                                       //Natural: IF #CNT-TOTAL NE 0
        {
            getReports().write(1, "RECORDS READ        : ",counters_Pnd_Cnt_Record_Read, new ReportEditMask ("Z,ZZZ,ZZ9"));                                               //Natural: WRITE ( 1 ) 'RECORDS READ        : ' #CNT-RECORD-READ ( EM = Z,ZZZ,ZZ9 )
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE," ");                                                                                                                           //Natural: WRITE ( 1 ) / ' '
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE,"PACKAGE CODE    : ",ldaRidl4040.getInput_Parms_Pnd_Pckge_Cde(),new TabSetting(45),"FIRST MAIL ITEM : ",ldaRidl4040.getPnd_First_Pst().getValue(1)); //Natural: WRITE ( 1 ) / 'PACKAGE CODE    : ' #PCKGE-CDE 45T 'FIRST MAIL ITEM : ' #FIRST-PST ( 1 )
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE,"PACKAGE         : RETIREMENT PLAN LOAN",new TabSetting(45),"LAST MAIL ITEM  : ",ldaRidl4040.getPnd_Last_Pst().getValue(1));    //Natural: WRITE ( 1 ) / 'PACKAGE         : RETIREMENT PLAN LOAN' 45T 'LAST MAIL ITEM  : ' #LAST-PST ( 1 )
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE," ");                                                                                                                           //Natural: WRITE ( 1 ) / ' '
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE," ");                                                                                                                           //Natural: WRITE ( 1 ) / ' '
            if (Global.isEscape()) return;
            getReports().write(1, new TabSetting(18),"SENT FOR PRINTING          : ",counters_Pnd_Cnt_Print, new ReportEditMask ("ZZZ,ZZ9"));                             //Natural: WRITE ( 1 ) 18T 'SENT FOR PRINTING          : ' #CNT-PRINT ( EM = ZZZ,ZZ9 )
            if (Global.isEscape()) return;
            getReports().write(1, new TabSetting(18),"SENT,DIVERT,FOREIGN ADDRESS: ",counters_Pnd_Cnt_Divert, new ReportEditMask ("ZZZ,ZZ9"));                            //Natural: WRITE ( 1 ) 18T 'SENT,DIVERT,FOREIGN ADDRESS: ' #CNT-DIVERT ( EM = ZZZ,ZZ9 )
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE," ");                                                                                                                           //Natural: WRITE ( 1 ) / ' '
            if (Global.isEscape()) return;
            getReports().write(1, NEWLINE," ");                                                                                                                           //Natural: WRITE ( 1 ) / ' '
            if (Global.isEscape()) return;
            getReports().write(1, new TabSetting(18),"TOTAL NUMBER OF MAIL ITEMS : ",counters_Pnd_Cnt_Total, new ReportEditMask ("ZZZ,ZZ9"));                             //Natural: WRITE ( 1 ) 18T 'TOTAL NUMBER OF MAIL ITEMS : ' #CNT-TOTAL ( EM = ZZZ,ZZ9 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-FOOTER
        sub_Write_Footer();
        if (condition(Global.isEscape())) {return;}
        //*  ----------------------------------------------------------------------
        //*                    E N D    O F    P R O C E S S I N G
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*                 S T A R T    O F    S U B R O U T I N E S
        //*  ----------------------------------------------------------------------
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SIGNATORY
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-INPUT-PARMS
        //*  ---------------------------------
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-GEN-MODULE
        //*  ------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADING
        //*  ------------------------------
        //*  -----------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FOOTER
        //*  -----------------------------
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-JOB
        //*  ----------------------------------------------------------------------
        //*                   E N D    O F    S U B R O U T I N E S
        //*  ----------------------------------------------------------------------
    }
    private void sub_Get_Signatory() throws Exception                                                                                                                     //Natural: GET-SIGNATORY
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 3 #PST-TBL-DATA-FIELD2
        while (condition(getWorkFiles().read(3, pnd_Pst_Tbl_Data_Field2)))
        {
            getReports().write(0, "READING SIGNATORY WORK FILE");                                                                                                         //Natural: WRITE 'READING SIGNATORY WORK FILE'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory);                                                                                          //Natural: WRITE '=' #MM-SIGNATORY
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1);                                                                                             //Natural: WRITE '=' #MM-TITLE1
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2);                                                                                             //Natural: WRITE '=' #MM-TITLE2
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3);                                                                                             //Natural: WRITE '=' #MM-TITLE3
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4);                                                                                             //Natural: WRITE '=' #MM-TITLE4
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*  GET-SIGNATORY
    }
    //*  FROM  RIDL4040
    private void sub_Get_Input_Parms() throws Exception                                                                                                                   //Natural: GET-INPUT-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("RIDB774|sub_Get_Input_Parms");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, ldaRidl4040.getInput_Parms_Pnd_Pckge_Cde());                                                           //Natural: INPUT INPUT-PARMS.#PCKGE-CDE
                DbsUtil.invokeInput(setInputStatus(INPUT_2), this, ldaRidl4040.getInput_Parms_Pnd_Start_Rqst_Id());                                                       //Natural: INPUT INPUT-PARMS.#START-RQST-ID
                DbsUtil.invokeInput(setInputStatus(INPUT_3), this, ldaRidl4040.getInput_Parms_Pnd_End_Rqst_Id());                                                         //Natural: INPUT INPUT-PARMS.#END-RQST-ID
                DbsUtil.invokeInput(setInputStatus(INPUT_4), this, ldaRidl4040.getInput_Parms_Pnd_Trace());                                                               //Natural: INPUT INPUT-PARMS.#TRACE
                //*  WRITE OUT PARAMTETERS
                getReports().write(0, "Input Parameters for",Global.getPROGRAM(),NEWLINE,NEWLINE,"    Package Code: ",ldaRidl4040.getInput_Parms_Pnd_Pckge_Cde(),NEWLINE,"Start Request ID: ",ldaRidl4040.getInput_Parms_Pnd_Start_Rqst_Id(),NEWLINE,"  End Request ID: ",ldaRidl4040.getInput_Parms_Pnd_End_Rqst_Id(),NEWLINE,NEWLINE,"  VOLSER: __________________________",NEWLINE,"   Trace Program: ",ldaRidl4040.getInput_Parms_Pnd_Trace(),  //Natural: WRITE ( 0 ) 'Input Parameters for' *PROGRAM // '    Package Code: ' INPUT-PARMS.#PCKGE-CDE / 'Start Request ID: ' INPUT-PARMS.#START-RQST-ID / '  End Request ID: ' INPUT-PARMS.#END-RQST-ID // '  VOLSER: __________________________' / '   Trace Program: ' INPUT-PARMS.#TRACE ( EM = NO/YES ) /
                    new ReportEditMask ("NO/YES"),NEWLINE);
                if (Global.isEscape()) return;
                if (condition(ldaRidl4040.getInput_Parms_Pnd_Pckge_Cde().equals(" ") && ldaRidl4040.getInput_Parms_Pnd_Start_Rqst_Id().equals(" ")))                      //Natural: IF INPUT-PARMS.#PCKGE-CDE = ' ' AND INPUT-PARMS.#START-RQST-ID = ' '
                {
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("PACKAGE CODE OR START/END REQUEST ID MUST BE SUPPLIED");                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG := 'PACKAGE CODE OR START/END REQUEST ID MUST BE SUPPLIED'
                }                                                                                                                                                         //Natural: END-IF
                pls_Trace.setValue(ldaRidl4040.getInput_Parms_Pnd_Trace().getBoolean());                                                                                  //Natural: ASSIGN +TRACE := INPUT-PARMS.#TRACE
                //*  GET-INPUT-PARMS
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Call_Gen_Module() throws Exception                                                                                                                   //Natural: CALL-GEN-MODULE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        DbsUtil.callnat(Ridn774.class , getCurrentProcessState(), pnd_Post_Data, pdaCwfpda_M.getMsg_Info_Sub(), pnd_Pst_Tbl_Data_Field2);                                 //Natural: CALLNAT 'RIDN774' #POST-DATA MSG-INFO-SUB #PST-TBL-DATA-FIELD2
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals(constants_Pnd_Error_Ind)))                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = #ERROR-IND
        {
                                                                                                                                                                          //Natural: PERFORM TERMINATE-JOB
            sub_Terminate_Job();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        counters_Pnd_Cnt_Print.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #CNT-PRINT
    }
    private void sub_Write_Heading() throws Exception                                                                                                                     //Natural: WRITE-HEADING
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------
        getReports().write(2, "PROGRAM:",Global.getPROGRAM(),new TabSetting(20),"RETIREMENT PLAN LOAN ENDORSEMENT MAILING",new TabSetting(65),"DATE:",Global.getDATU(),NEWLINE,new  //Natural: WRITE ( 2 ) 'PROGRAM:' *PROGRAM 20T 'RETIREMENT PLAN LOAN ENDORSEMENT MAILING' 65T 'DATE:' *DATU / 33T 'BATCH DETAILS' 65T 'TIME:' *TIME ( EM = XXXXXXXX ) / 65T 'PAGE:' 3X *PAGE-NUMBER ( 2 ) /
            TabSetting(33),"BATCH DETAILS",new TabSetting(65),"TIME:",Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,new TabSetting(65),"PAGE:",new 
            ColumnSpacing(3),getReports().getPageNumberDbs(2),NEWLINE);
        if (Global.isEscape()) return;
        //*  ------------------------------
        //*  WRITE-HEADING
    }
    private void sub_Write_Footer() throws Exception                                                                                                                      //Natural: WRITE-FOOTER
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------
        getReports().write(2, NEWLINE," ",NEWLINE);                                                                                                                       //Natural: WRITE ( 2 ) / ' ' /
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(10),"Total Number of Mail Items : ",pnd_Mail_Item_Ctr, new ReportEditMask ("ZZZ,ZZ9"));                                      //Natural: WRITE ( 2 ) 10T 'Total Number of Mail Items : ' #MAIL-ITEM-CTR ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* **                                           #CNT-TOTAL(#I)(EM=ZZZ,ZZ9)
        //*  -----------------------------
        //*  WRITE-FOOTER
    }
    private void sub_Terminate_Job() throws Exception                                                                                                                     //Natural: TERMINATE-JOB
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("RIDB774|sub_Terminate_Job");
        while(true)
        {
            try
            {
                //*  -------------------------------
                //*  TERMINATE JOB DUE TO AN ERROR.
                //*  NOTE:  THERE IS NO RETURN FROM THIS SUBROUTINE
                //*  EXPAND ##MSG TO MAKE IT READABLE.
                DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
                if (condition(Global.isEscape())) return;
                DbsUtil.invokeInput(setInputStatus(INPUT_5), this, new FieldAttributes ("AD=O"),"-",new RepeatItem(131),NEWLINE,NEWLINE,"Job terminated due to the following error(s):", //Natural: INPUT ( AD = O ) '-' ( 131 ) // 'Job terminated due to the following error(s):' / MSG-INFO-SUB.##MSG // 'Check list of errors above.' / 'No output has been produced.' /
                    NEWLINE,pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),NEWLINE,NEWLINE,"Check list of errors above.",NEWLINE,"No output has been produced.",
                    NEWLINE);
                ldaRidl4040.getError_Handler_Fields_Pnd_Error_Nr().reset();                                                                                               //Natural: RESET #ERROR-NR #ERROR-LINE #ERROR-LEVEL
                ldaRidl4040.getError_Handler_Fields_Pnd_Error_Line().reset();
                ldaRidl4040.getError_Handler_Fields_Pnd_Error_Level().reset();
                //*  WILL TERMINATE WITH COND CODE 4
                Global.getSTACK().pushData(StackOption.TOP, ldaRidl4040.getError_Handler_Fields_Pnd_Error_Nr(), ldaRidl4040.getError_Handler_Fields_Pnd_Error_Line(),     //Natural: FETCH 'INFP9000' #ERROR-NR #ERROR-LINE #ERROR-STATUS #ERROR-PROGRAM #ERROR-LEVEL #ERROR-APPL
                    ldaRidl4040.getError_Handler_Fields_Pnd_Error_Status(), ldaRidl4040.getError_Handler_Fields_Pnd_Error_Program(), ldaRidl4040.getError_Handler_Fields_Pnd_Error_Level(), 
                    ldaRidl4040.getError_Handler_Fields_Pnd_Error_Appl());
                Global.setFetchProgram(DbsUtil.getBlType("INFP9000"));
                if (condition(Global.isEscape())) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(19),"RETIREMENT PLAN LOAN ENDORSEMENT MAILING",new 
            TabSetting(65),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(30),"BATCH CONTROL TOTALS",new TabSetting(65),"TIME:",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,new TabSetting(65),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(19),"GSRA LOAN ENDORSEMENT MAILING",new 
            TabSetting(65),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(30),"BATCH CONTROL DETAILS",new TabSetting(65),"TIME:",Global.getTIME(), new 
            ReportEditMask ("XXXXXXXX"),NEWLINE,new TabSetting(65),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(2),NEWLINE,NEWLINE,new TabSetting(27),"PACKAGE CODE :  ",ldaRidl4040.getInput_Parms_Pnd_Pckge_Cde(),NEWLINE,NEWLINE,new 
            TabSetting(10),"                                                            ",new ColumnSpacing(1),"ADDR",NEWLINE,new TabSetting(10)," MAIL ITEM              FULL NAME                 PIN       ",new 
            ColumnSpacing(1),"TYPE",NEWLINE,new TabSetting(10),"----------- ----------------------------------- ------------",new ColumnSpacing(1),"----");
    }
}
