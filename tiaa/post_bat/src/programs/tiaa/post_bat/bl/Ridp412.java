/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:21:59 PM
**        * FROM NATURAL PROGRAM : Ridp412
************************************************************
**        * FILE NAME            : Ridp412.java
**        * CLASS NAME           : Ridp412
**        * INSTANCE NAME        : Ridp412
************************************************************
************************************************************************
** PROGRAM     : RIDP412                                              **
** AUTHOR      : JOANNES C. AVE                                       **
** DATE        : 10/11/2006                                           **
** DESCRIPTION : READS THE RIDP410 OUTPUT FILE, GETS NAME AND         **
**               ADDRESS DETAILS BY CALLING THE SUBPROGRAM PSTN3029,  **
**               ASSIGN A RID NUMBER BY CALLING THE SUBPROGRAM        **
**               PSTN9650, WRITE COMPUSET STATEMENTS TO GENERATE THE  **
**               ADDRESS PAGE AND COVER LETTER, AND PRINT REPORTS     **
************************************************************************
** HISTORY:                                                           **
** 10/11/2006  : INITIAL IMPLEMENTATION                               **
** 06/01/2007  : ADD #CONTRACTS ARRAY IN THE #PH-DATA LAYOUT          **
** 09/15/2009  : CONVERT TO ECS AND ADD PR AND NV STATE SPECIFIC      **
** 04/13/2011  : ADD E-DELIVERY CODES                                 **
** 07/19/2016  : REMARK CALL TO COR/NAAD TO GET NAME AND ADDRESS.     **
**               THIS WILL BE HANDLED BY CCP.                         **
**               REMARK CALL TO POST MODULE TO GET RID #. REPLACE     **
**               THIS WITH COUNTER GENERATED NUMBER.                  **
** 09/09/2017  : PIN EXPANSION PROJECT                     PINEXP     **
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridp412 extends BLNatBase
{
    // Data Areas
    private LdaRidl412 ldaRidl412;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ph_Data;
    private DbsField pnd_Ph_Data_Pnd_Pin_Number;
    private DbsField pnd_Ph_Data_Pnd_Letter_Type;
    private DbsField pnd_Ph_Data_Pnd_Institution_Name;
    private DbsField pnd_Ph_Data_Pnd_Level;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_St;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Rp;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Sv;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_St;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Rp;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Sv;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_St;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Sv;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_St;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Sv;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Nj;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Nj;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Nj;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Nj;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Pa;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Pa;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Pa;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Pa;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Ct;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Ct;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Ct;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Ct;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Tx;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Tx;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Tx;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Tx;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Va;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Va;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Va;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Va;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Mi;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Mo;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Mo;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Mo;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Mo;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Fl;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Fl;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Fl;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Fl;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Ok;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Ok;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Ok;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Ok;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Or;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Or;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Or;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Or;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Wi;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Wi;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Wi;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Wi;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Sc;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Sc;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Sc;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Sc;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Ne;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Ne;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Ne;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Ne;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Ar;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Hi;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Ky;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Mt;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Mt;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Mt;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Mt;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Nc;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Tn;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Ut;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Ut;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Vt;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Vt;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Pr;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Pr;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Ra_Nh;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Sr_Nh;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Nh;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Nh;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gr_Njabp;
    private DbsField pnd_Ph_Data_Pnd_Ctr_Gs_Suny;
    private DbsField pnd_Ph_Data_Pnd_Package_Code;
    private DbsField pnd_Ph_Data_Pnd_Contracts;
    private DbsField pnd_Ph_Data_Pnd_Email_Address;
    private DbsField pnd_Current_Date;

    private DbsGroup pnd_Current_Date__R_Field_1;
    private DbsField pnd_Current_Date_Pnd_Current_Year;
    private DbsField pnd_Current_Date_Pnd_Current_Month;
    private DbsField pnd_Current_Date_Pnd_Current_Day;
    private DbsField pnd_Rdr_Date;

    private DbsGroup pnd_Rdr_Date__R_Field_2;
    private DbsField pnd_Rdr_Date_Pnd_Rdr_Month;
    private DbsField pnd_Rdr_Date_Pnd_Rdr_Year;

    private DbsGroup counters;
    private DbsField counters_Pnd_Cnt_Record_Read;
    private DbsField counters_Pnd_Cnt_Record_Stored;
    private DbsField counters_Pnd_Cnt_Record_Addr_B;
    private DbsField counters_Pnd_Cnt_Record_Addr_C;
    private DbsField counters_Pnd_Cnt_Record_Addr_F;
    private DbsField counters_Pnd_Cnt_Record_Addr_U;
    private DbsField counters_Pnd_Cnt_Ph_Deceased;
    private DbsField pnd_Blank;
    private DbsField pnd_Batch_Id_Found;
    private DbsField pnd_First_Pst_Nbr_Allocated;
    private DbsField pnd_Error_Ind;

    private DbsGroup control_Variables;
    private DbsField control_Variables_Run_Type;
    private DbsField control_Variables_Batch_Number;
    private DbsField control_Variables_Current_Pst_Rqst_Id;

    private DbsGroup error_Handler_Fields;
    private DbsField error_Handler_Fields_Pnd_Error_Nr;
    private DbsField error_Handler_Fields_Pnd_Error_Line;
    private DbsField error_Handler_Fields_Pnd_Error_Status;
    private DbsField error_Handler_Fields_Pnd_Error_Program;
    private DbsField error_Handler_Fields_Pnd_Error_Level;
    private DbsField error_Handler_Fields_Pnd_Error_Appl;
    private DbsField pnd_Msg_Parts;

    private DbsGroup pnd_Package_Variables;
    private DbsField pnd_Package_Variables_Pnd_Package_Cdes;
    private DbsField pnd_Package_Variables_Pnd_Cnt_Record_By_Package;
    private DbsField pnd_Package_Variables_Pnd_First_Pst;
    private DbsField pnd_Package_Variables_Pnd_Last_Pst;
    private DbsField pnd_Prev_Package_Code;
    private DbsField pnd_Prev_Pst;
    private DbsField pnd_K;
    private DbsField pnd_First_Record;
    private DbsField pnd_First_Bad_Record;
    private DbsField pnd_Cnt_Et;

    private DbsGroup pnd_Package_Table;
    private DbsField pnd_Package_Table_Pnd_Package_Code;
    private DbsField pnd_Package_Table_Pnd_File_Desc;
    private DbsField pnd_Ndx;
    private DbsField pnd_Deceased;
    private DbsField pnd_Rem;
    private DbsField pnd_Bad_Address;
    private DbsField pnd_With_Il_Fl;
    private DbsField pnd_Rid_Ctr;
    private DbsField pnd_Rid_Nbr;

    private DbsGroup pnd_Rid_Nbr__R_Field_3;
    private DbsField pnd_Rid_Nbr_Pnd_Rid_Nbr_Prefix;
    private DbsField pnd_Rid_Nbr_Pnd_Rid_Nbr_A;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaRidl412 = new LdaRidl412();
        registerRecord(ldaRidl412);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ph_Data = localVariables.newGroupInRecord("pnd_Ph_Data", "#PH-DATA");
        pnd_Ph_Data_Pnd_Pin_Number = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);
        pnd_Ph_Data_Pnd_Letter_Type = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Ph_Data_Pnd_Institution_Name = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 120);
        pnd_Ph_Data_Pnd_Level = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Level", "#LEVEL", FieldType.STRING, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_St = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_St", "#CTR-RA-ST", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Rp = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Rp", "#CTR-RA-RP", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Sv = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Sv", "#CTR-RA-SV", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_St = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_St", "#CTR-SR-ST", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Rp = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Rp", "#CTR-SR-RP", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Sv = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Sv", "#CTR-SR-SV", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_St = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_St", "#CTR-GR-ST", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Sv = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Sv", "#CTR-GR-SV", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_St = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_St", "#CTR-GS-ST", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Sv = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Sv", "#CTR-GS-SV", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Nj = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Nj", "#CTR-RA-NJ", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Nj = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Nj", "#CTR-SR-NJ", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Nj = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Nj", "#CTR-GR-NJ", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Nj = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Nj", "#CTR-GS-NJ", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Pa = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Pa", "#CTR-RA-PA", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Pa = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Pa", "#CTR-SR-PA", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Pa = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Pa", "#CTR-GR-PA", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Pa = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Pa", "#CTR-GS-PA", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Ct = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Ct", "#CTR-RA-CT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Ct = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Ct", "#CTR-SR-CT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Ct = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Ct", "#CTR-GR-CT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Ct = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Ct", "#CTR-GS-CT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Tx = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Tx", "#CTR-RA-TX", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Tx = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Tx", "#CTR-SR-TX", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Tx = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Tx", "#CTR-GR-TX", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Tx = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Tx", "#CTR-GS-TX", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Va = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Va", "#CTR-RA-VA", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Va = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Va", "#CTR-SR-VA", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Va = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Va", "#CTR-GR-VA", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Va = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Va", "#CTR-GS-VA", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Mi = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Mi", "#CTR-GS-MI", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Mo = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Mo", "#CTR-RA-MO", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Mo = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Mo", "#CTR-SR-MO", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Mo = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Mo", "#CTR-GR-MO", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Mo = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Mo", "#CTR-GS-MO", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Fl = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Fl", "#CTR-RA-FL", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Fl = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Fl", "#CTR-SR-FL", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Fl = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Fl", "#CTR-GR-FL", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Fl = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Fl", "#CTR-GS-FL", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Ok = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Ok", "#CTR-RA-OK", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Ok = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Ok", "#CTR-SR-OK", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Ok = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Ok", "#CTR-GR-OK", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Ok = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Ok", "#CTR-GS-OK", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Or = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Or", "#CTR-RA-OR", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Or = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Or", "#CTR-SR-OR", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Or = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Or", "#CTR-GR-OR", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Or = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Or", "#CTR-GS-OR", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Wi = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Wi", "#CTR-RA-WI", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Wi = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Wi", "#CTR-SR-WI", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Wi = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Wi", "#CTR-GR-WI", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Wi = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Wi", "#CTR-GS-WI", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Sc = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Sc", "#CTR-RA-SC", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Sc = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Sc", "#CTR-SR-SC", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Sc = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Sc", "#CTR-GR-SC", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Sc = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Sc", "#CTR-GS-SC", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Ne = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Ne", "#CTR-RA-NE", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Ne = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Ne", "#CTR-SR-NE", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Ne = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Ne", "#CTR-GR-NE", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Ne = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Ne", "#CTR-GS-NE", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Ar = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Ar", "#CTR-GS-AR", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Hi = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Hi", "#CTR-GS-HI", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Ky = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Ky", "#CTR-GS-KY", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Mt = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Mt", "#CTR-RA-MT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Mt = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Mt", "#CTR-SR-MT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Mt = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Mt", "#CTR-GR-MT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Mt = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Mt", "#CTR-GS-MT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Nc = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Nc", "#CTR-GR-NC", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Tn = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Tn", "#CTR-GS-TN", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Ut = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Ut", "#CTR-RA-UT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Ut = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Ut", "#CTR-SR-UT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Vt = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Vt", "#CTR-SR-VT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Vt = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Vt", "#CTR-GS-VT", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Pr = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Pr", "#CTR-RA-PR", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Pr = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Pr", "#CTR-GR-PR", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Ra_Nh = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Ra_Nh", "#CTR-RA-NH", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Sr_Nh = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Sr_Nh", "#CTR-SR-NH", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Nh = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Nh", "#CTR-GR-NH", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Nh = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Nh", "#CTR-GS-NH", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gr_Njabp = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gr_Njabp", "#CTR-GR-NJABP", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Ctr_Gs_Suny = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Ctr_Gs_Suny", "#CTR-GS-SUNY", FieldType.NUMERIC, 1);
        pnd_Ph_Data_Pnd_Package_Code = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Ph_Data_Pnd_Contracts = pnd_Ph_Data.newFieldArrayInGroup("pnd_Ph_Data_Pnd_Contracts", "#CONTRACTS", FieldType.STRING, 8, new DbsArrayController(1, 
            20));
        pnd_Ph_Data_Pnd_Email_Address = pnd_Ph_Data.newFieldInGroup("pnd_Ph_Data_Pnd_Email_Address", "#EMAIL-ADDRESS", FieldType.STRING, 100);
        pnd_Current_Date = localVariables.newFieldInRecord("pnd_Current_Date", "#CURRENT-DATE", FieldType.NUMERIC, 8);

        pnd_Current_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Current_Date__R_Field_1", "REDEFINE", pnd_Current_Date);
        pnd_Current_Date_Pnd_Current_Year = pnd_Current_Date__R_Field_1.newFieldInGroup("pnd_Current_Date_Pnd_Current_Year", "#CURRENT-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Current_Date_Pnd_Current_Month = pnd_Current_Date__R_Field_1.newFieldInGroup("pnd_Current_Date_Pnd_Current_Month", "#CURRENT-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Current_Date_Pnd_Current_Day = pnd_Current_Date__R_Field_1.newFieldInGroup("pnd_Current_Date_Pnd_Current_Day", "#CURRENT-DAY", FieldType.NUMERIC, 
            2);
        pnd_Rdr_Date = localVariables.newFieldInRecord("pnd_Rdr_Date", "#RDR-DATE", FieldType.NUMERIC, 6);

        pnd_Rdr_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Rdr_Date__R_Field_2", "REDEFINE", pnd_Rdr_Date);
        pnd_Rdr_Date_Pnd_Rdr_Month = pnd_Rdr_Date__R_Field_2.newFieldInGroup("pnd_Rdr_Date_Pnd_Rdr_Month", "#RDR-MONTH", FieldType.NUMERIC, 2);
        pnd_Rdr_Date_Pnd_Rdr_Year = pnd_Rdr_Date__R_Field_2.newFieldInGroup("pnd_Rdr_Date_Pnd_Rdr_Year", "#RDR-YEAR", FieldType.NUMERIC, 4);

        counters = localVariables.newGroupInRecord("counters", "COUNTERS");
        counters_Pnd_Cnt_Record_Read = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Read", "#CNT-RECORD-READ", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Stored = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Stored", "#CNT-RECORD-STORED", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Addr_B = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Addr_B", "#CNT-RECORD-ADDR-B", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Addr_C = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Addr_C", "#CNT-RECORD-ADDR-C", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Addr_F = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Addr_F", "#CNT-RECORD-ADDR-F", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Record_Addr_U = counters.newFieldInGroup("counters_Pnd_Cnt_Record_Addr_U", "#CNT-RECORD-ADDR-U", FieldType.NUMERIC, 7);
        counters_Pnd_Cnt_Ph_Deceased = counters.newFieldInGroup("counters_Pnd_Cnt_Ph_Deceased", "#CNT-PH-DECEASED", FieldType.NUMERIC, 7);
        pnd_Blank = localVariables.newFieldInRecord("pnd_Blank", "#BLANK", FieldType.STRING, 1);
        pnd_Batch_Id_Found = localVariables.newFieldInRecord("pnd_Batch_Id_Found", "#BATCH-ID-FOUND", FieldType.BOOLEAN, 1);
        pnd_First_Pst_Nbr_Allocated = localVariables.newFieldInRecord("pnd_First_Pst_Nbr_Allocated", "#FIRST-PST-NBR-ALLOCATED", FieldType.BOOLEAN, 1);
        pnd_Error_Ind = localVariables.newFieldInRecord("pnd_Error_Ind", "#ERROR-IND", FieldType.STRING, 1);

        control_Variables = localVariables.newGroupInRecord("control_Variables", "CONTROL-VARIABLES");
        control_Variables_Run_Type = control_Variables.newFieldInGroup("control_Variables_Run_Type", "RUN-TYPE", FieldType.PACKED_DECIMAL, 1);
        control_Variables_Batch_Number = control_Variables.newFieldInGroup("control_Variables_Batch_Number", "BATCH-NUMBER", FieldType.STRING, 11);
        control_Variables_Current_Pst_Rqst_Id = control_Variables.newFieldInGroup("control_Variables_Current_Pst_Rqst_Id", "CURRENT-PST-RQST-ID", FieldType.STRING, 
            11);

        error_Handler_Fields = localVariables.newGroupInRecord("error_Handler_Fields", "ERROR-HANDLER-FIELDS");
        error_Handler_Fields_Pnd_Error_Nr = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Nr", "#ERROR-NR", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Line = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Line", "#ERROR-LINE", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Status = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Status", "#ERROR-STATUS", FieldType.STRING, 
            1);
        error_Handler_Fields_Pnd_Error_Program = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Program", "#ERROR-PROGRAM", FieldType.STRING, 
            8);
        error_Handler_Fields_Pnd_Error_Level = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Level", "#ERROR-LEVEL", FieldType.NUMERIC, 
            2);
        error_Handler_Fields_Pnd_Error_Appl = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Appl", "#ERROR-APPL", FieldType.STRING, 
            8);
        pnd_Msg_Parts = localVariables.newFieldArrayInRecord("pnd_Msg_Parts", "#MSG-PARTS", FieldType.STRING, 80, new DbsArrayController(1, 10));

        pnd_Package_Variables = localVariables.newGroupInRecord("pnd_Package_Variables", "#PACKAGE-VARIABLES");
        pnd_Package_Variables_Pnd_Package_Cdes = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_Package_Cdes", "#PACKAGE-CDES", FieldType.STRING, 
            8);
        pnd_Package_Variables_Pnd_Cnt_Record_By_Package = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_Cnt_Record_By_Package", "#CNT-RECORD-BY-PACKAGE", 
            FieldType.NUMERIC, 6);
        pnd_Package_Variables_Pnd_First_Pst = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_First_Pst", "#FIRST-PST", FieldType.STRING, 
            11);
        pnd_Package_Variables_Pnd_Last_Pst = pnd_Package_Variables.newFieldInGroup("pnd_Package_Variables_Pnd_Last_Pst", "#LAST-PST", FieldType.STRING, 
            11);
        pnd_Prev_Package_Code = localVariables.newFieldInRecord("pnd_Prev_Package_Code", "#PREV-PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Prev_Pst = localVariables.newFieldInRecord("pnd_Prev_Pst", "#PREV-PST", FieldType.STRING, 11);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_First_Record = localVariables.newFieldInRecord("pnd_First_Record", "#FIRST-RECORD", FieldType.BOOLEAN, 1);
        pnd_First_Bad_Record = localVariables.newFieldInRecord("pnd_First_Bad_Record", "#FIRST-BAD-RECORD", FieldType.BOOLEAN, 1);
        pnd_Cnt_Et = localVariables.newFieldInRecord("pnd_Cnt_Et", "#CNT-ET", FieldType.NUMERIC, 3);

        pnd_Package_Table = localVariables.newGroupArrayInRecord("pnd_Package_Table", "#PACKAGE-TABLE", new DbsArrayController(1, 14));
        pnd_Package_Table_Pnd_Package_Code = pnd_Package_Table.newFieldInGroup("pnd_Package_Table_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 
            8);
        pnd_Package_Table_Pnd_File_Desc = pnd_Package_Table.newFieldInGroup("pnd_Package_Table_Pnd_File_Desc", "#FILE-DESC", FieldType.STRING, 80);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 2);
        pnd_Deceased = localVariables.newFieldInRecord("pnd_Deceased", "#DECEASED", FieldType.BOOLEAN, 1);
        pnd_Rem = localVariables.newFieldInRecord("pnd_Rem", "#REM", FieldType.STRING, 2);
        pnd_Bad_Address = localVariables.newFieldInRecord("pnd_Bad_Address", "#BAD-ADDRESS", FieldType.BOOLEAN, 1);
        pnd_With_Il_Fl = localVariables.newFieldInRecord("pnd_With_Il_Fl", "#WITH-IL-FL", FieldType.STRING, 1);
        pnd_Rid_Ctr = localVariables.newFieldInRecord("pnd_Rid_Ctr", "#RID-CTR", FieldType.NUMERIC, 8);
        pnd_Rid_Nbr = localVariables.newFieldInRecord("pnd_Rid_Nbr", "#RID-NBR", FieldType.NUMERIC, 9);

        pnd_Rid_Nbr__R_Field_3 = localVariables.newGroupInRecord("pnd_Rid_Nbr__R_Field_3", "REDEFINE", pnd_Rid_Nbr);
        pnd_Rid_Nbr_Pnd_Rid_Nbr_Prefix = pnd_Rid_Nbr__R_Field_3.newFieldInGroup("pnd_Rid_Nbr_Pnd_Rid_Nbr_Prefix", "#RID-NBR-PREFIX", FieldType.STRING, 
            1);
        pnd_Rid_Nbr_Pnd_Rid_Nbr_A = pnd_Rid_Nbr__R_Field_3.newFieldInGroup("pnd_Rid_Nbr_Pnd_Rid_Nbr_A", "#RID-NBR-A", FieldType.STRING, 8);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaRidl412.initializeValues();

        localVariables.reset();
        pnd_Blank.setInitialValue(" ");
        pnd_Error_Ind.setInitialValue("E");
        error_Handler_Fields_Pnd_Error_Status.setInitialValue("O");
        error_Handler_Fields_Pnd_Error_Program.setInitialValue("PROGRAM");
        error_Handler_Fields_Pnd_Error_Appl.setInitialValue("RIDER");
        pnd_K.setInitialValue(1);
        pnd_First_Record.setInitialValue(true);
        pnd_First_Bad_Record.setInitialValue(true);
        pnd_Cnt_Et.setInitialValue(0);
        pnd_Package_Table_Pnd_Package_Code.getValue(1).setInitialValue("RDR2009D");
        pnd_Package_Table_Pnd_Package_Code.getValue(2).setInitialValue("RDR2009E");
        pnd_Package_Table_Pnd_Package_Code.getValue(3).setInitialValue("RDR2009F");
        pnd_Package_Table_Pnd_Package_Code.getValue(4).setInitialValue("RDR2009G");
        pnd_Package_Table_Pnd_Package_Code.getValue(5).setInitialValue("RDR2009H");
        pnd_Package_Table_Pnd_Package_Code.getValue(6).setInitialValue("RDR2009I");
        pnd_Package_Table_Pnd_Package_Code.getValue(7).setInitialValue("RDR2009J");
        pnd_Package_Table_Pnd_Package_Code.getValue(8).setInitialValue("RDR2009K");
        pnd_Package_Table_Pnd_Package_Code.getValue(9).setInitialValue("RDR2009L");
        pnd_Package_Table_Pnd_Package_Code.getValue(10).setInitialValue("RDR2009M");
        pnd_Package_Table_Pnd_Package_Code.getValue(11).setInitialValue("RDR2009N");
        pnd_Package_Table_Pnd_Package_Code.getValue(12).setInitialValue("RDR2009O");
        pnd_Package_Table_Pnd_Package_Code.getValue(13).setInitialValue("RDR2009P");
        pnd_Package_Table_Pnd_Package_Code.getValue(14).setInitialValue("RDR2009Q");
        pnd_Package_Table_Pnd_File_Desc.getValue(1).setInitialValue("Between  8 to 14 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(2).setInitialValue("Between 15 to 20 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(3).setInitialValue("Between 21 to 26 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(4).setInitialValue("Between 27 to 32 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(5).setInitialValue("Between 33 to 39 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(6).setInitialValue("Between 40 to 45 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(7).setInitialValue("Between 46 to 51 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(8).setInitialValue("Between 52 to 57 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(9).setInitialValue("Between 58 to 64 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(10).setInitialValue("Between 65 to 70 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(11).setInitialValue("Between 71 to 76 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(12).setInitialValue("Between 77 to 82 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(13).setInitialValue("More than 82 pages");
        pnd_Package_Table_Pnd_File_Desc.getValue(14).setInitialValue("e-delivery");
        pnd_Bad_Address.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Ridp412() throws Exception
    {
        super("Ridp412");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDP412", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60;//Natural: FORMAT LS = 132 PS = 60
        //*  ----------------------------------------------------------------------
        //*                      E R R O R    H A N D L I N G
        //*  ----------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  GET RIDER DATE
        pnd_Current_Date.setValue(Global.getDATN());                                                                                                                      //Natural: ASSIGN #CURRENT-DATE = *DATN
        pnd_Rdr_Date_Pnd_Rdr_Year.setValue(pnd_Current_Date_Pnd_Current_Year);                                                                                            //Natural: ASSIGN #RDR-YEAR = #CURRENT-YEAR
        pnd_Rdr_Date_Pnd_Rdr_Month.setValue(pnd_Current_Date_Pnd_Current_Month);                                                                                          //Natural: ASSIGN #RDR-MONTH = #CURRENT-MONTH
        //*  WRITE REPORT HEADINGS
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'PROGRAM:' *PROGRAM 40T '2007 TIAA ACCESS MAILING POST LOAD PROCESSING STATISTICS' 118T 'DATE:' *DATU / 118T 'TIME:' *TIME ( EM = XXXXXXXX ) / 118T 'PAGE:' 3X *PAGE-NUMBER ( 1 ) /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT 'PROGRAM:' *PROGRAM 51T '2007 TIAA ACCESS RIDER MAILING' 116T 'DATE:' *DATU / 57T 'BAD ADDRESS REPORT' 116T 'TIME:' *TIME ( EM = XXXXXXXX ) / 116T 'PAGE:' 3X *PAGE-NUMBER ( 2 ) // '---------------------------------------------------------------------------------------------------------------------------------'/ '                               |---------------------------------------- CONTRACT COUNTS ---------------------------------------|'/ '                               RA RA RA SR SR SR GR GR GS GS RA SR GR GS GS RA SR GR GS RA SR GR GS RA SR GR GS RA SR GR' / '  PIN    NAME                  ST RP SV ST RP SV ST SV ST SV NJ NJ NJ NJ PA PA PA PA CT CT CT CT TX TX TX TX VA VA VA' / '                               GS GS RA SR GR GS RA SR GR GS RA SR GR GS RA SR GR GS RA SR GR GS RA SR GR GS RA SR GR GS' / '                               VA MI MO MO MO MO FL FL FL FL OK OK OK OK OR OR OR OR WI WI WI WI SC SC SC SC NE NE NE NE' / '                               GS GS GS RA SR GR GS GR GS RA SR SR GS RA GR RA GR SR GS GR GS' / '                               AR HI KY MT MT MT MT NC TN UT UT VT VT PR PR NH NH NH NH NA SN' / '---------------------------------------------------------------------------------------------------------------------------------'/
        //*  ----------------------------------------------------------------------
        //*                           M A I N    L O O P
        //*  ----------------------------------------------------------------------
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #PH-DATA
        while (condition(getWorkFiles().read(1, pnd_Ph_Data)))
        {
            counters_Pnd_Cnt_Record_Read.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CNT-RECORD-READ
            ldaRidl412.getPnd_Post_Data().reset();                                                                                                                        //Natural: RESET #POST-DATA
            ldaRidl412.getPnd_Post_Data().setValuesByName(pnd_Ph_Data);                                                                                                   //Natural: MOVE BY NAME #PH-DATA TO #POST-DATA
            ldaRidl412.getPnd_Post_Data_Pnd_Rider_Date().setValue(pnd_Rdr_Date);                                                                                          //Natural: ASSIGN #POST-DATA.#RIDER-DATE = #RDR-DATE
            //*    PERFORM GET-NAME-ADDRESS-DETAILS
            //*    IF #BAD-ADDRESS
            //*  041311 JAVE
            if (condition(pnd_Bad_Address.getBoolean() && ldaRidl412.getPnd_Post_Data_Pnd_Email_Address().equals(" ")))                                                   //Natural: IF #BAD-ADDRESS AND #POST-DATA.#EMAIL-ADDRESS EQ ' '
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-BAD-ADDRESS-REPORT
                sub_Write_Bad_Address_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM GET-PST-NUMBER
                sub_Get_Pst_Number();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* **   PERFORM DISPLAY-POST-DATA
                if (condition(pnd_First_Record.getBoolean()))                                                                                                             //Natural: IF #FIRST-RECORD
                {
                    pnd_Prev_Package_Code.setValue(ldaRidl412.getPnd_Post_Data_Pnd_Package_Code());                                                                       //Natural: ASSIGN #PREV-PACKAGE-CODE := #POST-DATA.#PACKAGE-CODE
                    pnd_First_Record.setValue(false);                                                                                                                     //Natural: ASSIGN #FIRST-RECORD := FALSE
                    pnd_Package_Variables_Pnd_First_Pst.setValue(ldaRidl412.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                          //Natural: ASSIGN #FIRST-PST := #POST-DATA.#PST-RQST-ID
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Prev_Package_Code.equals(ldaRidl412.getPnd_Post_Data_Pnd_Package_Code())))                                                              //Natural: IF #PREV-PACKAGE-CODE EQ #POST-DATA.#PACKAGE-CODE
                {
                    pnd_Prev_Pst.setValue(ldaRidl412.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                                                 //Natural: ASSIGN #PREV-PST := #POST-DATA.#PST-RQST-ID
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Package_Variables_Pnd_Last_Pst.setValue(pnd_Prev_Pst);                                                                                            //Natural: ASSIGN #LAST-PST := #PREV-PST
                    pnd_Package_Variables_Pnd_First_Pst.setValue(ldaRidl412.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                          //Natural: ASSIGN #FIRST-PST := #POST-DATA.#PST-RQST-ID
                    pnd_Prev_Package_Code.setValue(ldaRidl412.getPnd_Post_Data_Pnd_Package_Code());                                                                       //Natural: ASSIGN #PREV-PACKAGE-CODE := #POST-DATA.#PACKAGE-CODE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM STORE-POST-DATA
                sub_Store_Post_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_Package_Variables_Pnd_Last_Pst.setValue(pnd_Prev_Pst);                                                                                                        //Natural: ASSIGN #LAST-PST := #PREV-PST
                                                                                                                                                                          //Natural: PERFORM PRINT-PROCESSING-REPORT
        sub_Print_Processing_Report();
        if (condition(Global.isEscape())) {return;}
        //*  ----------------------------------------------------------------------
        //*                   E N D    O F    P R O C E S S I N G
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
        //*                 S T A R T    O F   S U B R O U T I N E S
        //*  ----------------------------------------------------------------------
        //*  ------------------------------------------
        //*  DEFINE SUBROUTINE GET-NAME-ADDRESS-DETAILS
        //*  ------------------------------------------
        //*  CALL PSTN3029 AND USE RETURNED ADDRESS
        //*    PSTA3029.PIN-NBR := #POST-DATA.#PIN-NUMBER
        //*    CALLNAT 'PSTN3029' PSTA3029
        //*      PSTA3029-MAIL-ADDRSS
        //*      PSTA3029-PRMNNT-ADDRSS
        //*      DIALOG-INFO-SUB
        //*      MSG-INFO-SUB
        //*      PASS-SUB
        //*   #POST-DATA.#ADDRESS-LINE-TXT(*) := PSTA3029-MAIL-ADDRSS.ADDRSS-LNE(*)
        //*   #POST-DATA.#POSTAL-DATA    := PSTA3029-MAIL-ADDRSS.ADDRSS-POSTAL-DATA
        //*   #POST-DATA.#ADDRESS-TYPE-CDE := PSTA3029-MAIL-ADDRSS.ADDRSS-TYPE-CDE
        //*   IF NOT (#POST-DATA.#ADDRESS-LINE-TXT(*) NE #BLANK)
        //*     #ADDRESS-TYPE-CDE := 'B'      /* CAUSE ITEM TO BE DIVERTED
        //*   END-IF
        //*   RESET INITIAL #BAD-ADDRESS
        //*   DECIDE ON FIRST VALUE OF #ADDRESS-TYPE-CDE
        //*     VALUE 'B'
        //*       ADD 1 TO #CNT-RECORD-ADDR-B
        //*       MOVE TRUE TO #BAD-ADDRESS
        //*     VALUE 'C'
        //*       ADD 1 TO #CNT-RECORD-ADDR-C
        //*     VALUE 'F'
        //*       ADD 1 TO #CNT-RECORD-ADDR-F
        //*     VALUE 'U'
        //*       ADD 1 TO #CNT-RECORD-ADDR-U
        //*     NONE
        //*       IGNORE
        //*   END-DECIDE
        //*  GET NAME OF PARTICIPANT
        //*     #POST-DATA.#FULL-NAME := PSTA3029.FULL-NAME
        //*     IF #POST-DATA.#FULL-NAME = #BLANK
        //* **   WRITE / 'PIN NUMBER ' #POST-DATA.#PIN-NUMBER
        //* **           'HAS NO FULLNAME IN NAME & ADDRESS FILE'
        //*       #COR-KEY.PIN := #POST-DATA.#PIN-NUMBER
        //*       #COR-KEY.REC := 01
        //*       FIND (1) COR-PH-V WITH COR-SUPER-PIN-RCDTYPE = #COR-KEY
        //*         COMPRESS COR-PH-V.PH-FIRST-NME COR-PH-V.PH-LAST-NME
        //*           INTO #POST-DATA.#FULL-NAME
        //*       END-FIND
        //*    END-IF
        //*  ASSIGN RIDER DATE TO POST DATA
        //*  ASSIGN #POST-DATA.#RIDER-DATE = #RDR-DATE
        //*  END-SUBROUTINE     /* GET-NAME-ADDRESS-DETAILS
        //*  --------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PST-NUMBER
        //*  --------------------------------
        //*   GET NEXT MUNBER FOR PST-OUTGOING-MAIL-ITEM. ON THE FIRST CALL,
        //*   ENSURE THAT THE NUMBER ALLOCATED IS THE HIGHEST ON THE FILE.
        //*  MOVE 'RID' TO CURRENT-PST-RQST-ID
        //*  REPEAT
        //*    CALLNAT 'PSTN9650' CURRENT-PST-RQST-ID MSG-INFO-SUB
        //*    IF MSG-INFO-SUB.##RETURN-CODE = #ERROR-IND
        //*      PERFORM TERMINATE-JOB
        //*    END-IF
        //*    #BATCH-ID-FOUND := FALSE
        //*    IF NOT #FIRST-PST-NBR-ALLOCATED
        //*      HISTOGRAM (1) HIST2-MAIL-ITEM FOR RQST-HDR-IND STARTING FROM
        //*          CURRENT-PST-RQST-ID
        //*        IF HIST2-MAIL-ITEM.#PST-RQST-ID >= CURRENT-PST-RQST-ID
        //*          #BATCH-ID-FOUND := TRUE
        //*        END-IF
        //*      END-HISTOGRAM
        //* **   WRITE *PROGRAM 'PST-RQST-ID:' CURRENT-PST-RQST-ID 'is'
        //* **      #BATCH-ID-FOUND (EM='   '/'NOT') 'highest on file. FOUND'
        //* **      HIST2-MAIL-ITEM.#PST-RQST-ID
        //*    END-IF
        //*  WHILE #BATCH-ID-FOUND
        //*  END-REPEAT
        //*  #FIRST-PST-NBR-ALLOCATED := TRUE
        //*  #POST-DATA.#PST-RQST-ID  := CURRENT-PST-RQST-ID
        //* **  IF +TRACE
        //* **   WRITE (0) *PROGRAM 'MAIL ITEM nbr' CURRENT-PST-RQST-ID 'allocated'
        //* **  END-IF
        //*  IF #CNT-ET = 100
        //*    END TRANSACTION
        //*    RESET #CNT-ET
        //*  ELSE
        //*    ADD 1 TO #CNT-ET
        //*  END-IF
        //*  ---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-POST-DATA
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-PROCESSING-REPORT
        //*  -----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-POST-DATA
        //*  ------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-BAD-ADDRESS-REPORT
        //*  ----------------------------------------------------------------------
        //*                    E N D    O F    S U B R O U T I N E S
        //*  ----------------------------------------------------------------------
    }
    private void sub_Get_Pst_Number() throws Exception                                                                                                                    //Natural: GET-PST-NUMBER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rid_Ctr.nadd(1);                                                                                                                                              //Natural: ASSIGN #RID-CTR := #RID-CTR + 1
        pnd_Rid_Nbr.compute(new ComputeParameters(false, pnd_Rid_Nbr), DbsField.add(100000000,pnd_Rid_Ctr));                                                              //Natural: ASSIGN #RID-NBR := 100000000 + #RID-CTR
        ldaRidl412.getPnd_Post_Data_Pnd_Pst_Rqst_Id().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "RID", pnd_Rid_Nbr_Pnd_Rid_Nbr_A));                        //Natural: COMPRESS 'RID' #RID-NBR-A TO #POST-DATA.#PST-RQST-ID LEAVING NO
        //*  GET-PST-NUMBER
    }
    private void sub_Store_Post_Data() throws Exception                                                                                                                   //Natural: STORE-POST-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        getWorkFiles().write(2, false, ldaRidl412.getPnd_Post_Data());                                                                                                    //Natural: WRITE WORK FILE 2 #POST-DATA
        ldaRidl412.getPnd_Post_Data().reset();                                                                                                                            //Natural: RESET #POST-DATA
        counters_Pnd_Cnt_Record_Stored.nadd(1);                                                                                                                           //Natural: ADD 1 TO #CNT-RECORD-STORED
        //*  STORE-POST-DATA
    }
    private void sub_Print_Processing_Report() throws Exception                                                                                                           //Natural: PRINT-PROCESSING-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------
        getReports().write(1, NEWLINE,"NUMBER OF RECORDS READ                       :",counters_Pnd_Cnt_Record_Read, new ReportEditMask ("ZZZ,ZZ9"));                     //Natural: WRITE ( 1 ) / 'NUMBER OF RECORDS READ                       :' #CNT-RECORD-READ ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"NUMBER OF RECORDS WRITTEN FOR ECS GENERATION :",counters_Pnd_Cnt_Record_Stored, new ReportEditMask ("ZZZ,ZZ9"));                   //Natural: WRITE ( 1 ) / 'NUMBER OF RECORDS WRITTEN FOR ECS GENERATION :' #CNT-RECORD-STORED ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, "BREAKDOWN OF RECORDS WRITTEN");                                                                                                            //Natural: WRITE ( 1 ) 'BREAKDOWN OF RECORDS WRITTEN'
        if (Global.isEscape()) return;
        getReports().write(1, "   - DOMESTIC ADDRESS (U)                    :",counters_Pnd_Cnt_Record_Addr_U, new ReportEditMask ("ZZZ,ZZ9"));                           //Natural: WRITE ( 1 ) '   - DOMESTIC ADDRESS (U)                    :' #CNT-RECORD-ADDR-U ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, "   - FOREIGN  ADDRESS (F)                    :",counters_Pnd_Cnt_Record_Addr_F, new ReportEditMask ("ZZZ,ZZ9"));                           //Natural: WRITE ( 1 ) '   - FOREIGN  ADDRESS (F)                    :' #CNT-RECORD-ADDR-F ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, "   - CANADIAN ADDRESS (C)                    :",counters_Pnd_Cnt_Record_Addr_C, new ReportEditMask ("ZZZ,ZZ9"));                           //Natural: WRITE ( 1 ) '   - CANADIAN ADDRESS (C)                    :' #CNT-RECORD-ADDR-C ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, "   - BAD      ADDRESS (B)                    :",counters_Pnd_Cnt_Record_Addr_B, new ReportEditMask ("ZZZ,ZZ9"));                           //Natural: WRITE ( 1 ) '   - BAD      ADDRESS (B)                    :' #CNT-RECORD-ADDR-B ( EM = ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE);                                                                                                                           //Natural: WRITE ( 1 ) //
        if (Global.isEscape()) return;
        DbsUtil.examine(new ExamineSource(pnd_Package_Table_Pnd_Package_Code.getValue("*")), new ExamineSearch(pnd_Ph_Data_Pnd_Package_Code), new ExamineGivingIndex(pnd_K)); //Natural: EXAMINE #PACKAGE-TABLE.#PACKAGE-CODE ( * ) FOR #PH-DATA.#PACKAGE-CODE GIVING INDEX #K
        getReports().write(0, "=",pnd_K,"=",pnd_Ph_Data_Pnd_Package_Code);                                                                                                //Natural: WRITE '=' #K '=' #PH-DATA.#PACKAGE-CODE
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"PACKAGE CODE     : ",pnd_Ph_Data_Pnd_Package_Code," - ",pnd_Package_Table_Pnd_File_Desc.getValue(pnd_K));                          //Natural: WRITE ( 1 ) / 'PACKAGE CODE     : ' #PH-DATA.#PACKAGE-CODE ' - ' #FILE-DESC ( #K )
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"FIRST PST NUMBER : ",pnd_Package_Variables_Pnd_First_Pst);                                                                         //Natural: WRITE ( 1 ) / 'FIRST PST NUMBER : ' #FIRST-PST
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"LAST  PST NUMBER : ",pnd_Package_Variables_Pnd_Last_Pst);                                                                          //Natural: WRITE ( 1 ) / 'LAST  PST NUMBER : ' #LAST-PST
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE," ",NEWLINE,NEWLINE);                                                                                                               //Natural: WRITE / ' ' //
        if (Global.isEscape()) return;
        //*  PRINT-PROCESSING-REPORT
    }
    private void sub_Display_Post_Data() throws Exception                                                                                                                 //Natural: DISPLAY-POST-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------------
        getReports().write(0, NEWLINE,"PST : ",ldaRidl412.getPnd_Post_Data_Pnd_Pst_Rqst_Id(),"PIN : ",ldaRidl412.getPnd_Post_Data_Pnd_Pin_Number());                      //Natural: WRITE ( 0 ) / 'PST : ' #POST-DATA.#PST-RQST-ID 'PIN : ' #POST-DATA.#PIN-NUMBER
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl412.getPnd_Post_Data_Pnd_Full_Name());                                                                                   //Natural: WRITE ( 0 ) / '=' #POST-DATA.#FULL-NAME
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl412.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(1));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 1 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl412.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(2));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 2 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl412.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(3));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 3 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl412.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(4));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 4 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl412.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(5));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 5 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl412.getPnd_Post_Data_Pnd_Address_Line_Txt().getValue(6));                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-LINE-TXT ( 6 )
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl412.getPnd_Post_Data_Pnd_Postal_Data());                                                                                 //Natural: WRITE ( 0 ) / '=' #POST-DATA.#POSTAL-DATA
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl412.getPnd_Post_Data_Pnd_Address_Type_Cde());                                                                            //Natural: WRITE ( 0 ) / '=' #POST-DATA.#ADDRESS-TYPE-CDE
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",ldaRidl412.getPnd_Post_Data_Pnd_Package_Code());                                                                                //Natural: WRITE ( 0 ) / '=' #POST-DATA.#PACKAGE-CODE
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE," ");                                                                                                                               //Natural: WRITE ( 0 ) / ' '
        if (Global.isEscape()) return;
        //*  DISPLAY-POST-DATA
    }
    private void sub_Write_Bad_Address_Report() throws Exception                                                                                                          //Natural: WRITE-BAD-ADDRESS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------
        if (condition(pnd_First_Bad_Record.getBoolean()))                                                                                                                 //Natural: IF #FIRST-BAD-RECORD
        {
            getReports().write(2, "INSTITUTION : ",pnd_Ph_Data_Pnd_Institution_Name,NEWLINE);                                                                             //Natural: WRITE ( 2 ) 'INSTITUTION : ' #PH-DATA.#INSTITUTION-NAME /
            if (Global.isEscape()) return;
            pnd_First_Bad_Record.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #FIRST-BAD-RECORD
        }                                                                                                                                                                 //Natural: END-IF
        //*  PINEXP >>>
        getReports().write(2, pnd_Ph_Data_Pnd_Pin_Number,new TabSetting(15),ldaRidl412.getPnd_Post_Data_Pnd_Full_Name(), new AlphanumericLength (20),new                  //Natural: WRITE ( 2 ) #PH-DATA.#PIN-NUMBER 15T #POST-DATA.#FULL-NAME ( AL = 20 ) 37T #PH-DATA.#CTR-RA-ST 40T #PH-DATA.#CTR-RA-RP 43T #PH-DATA.#CTR-RA-SV 46T #PH-DATA.#CTR-SR-ST 49T #PH-DATA.#CTR-SR-RP 52T #PH-DATA.#CTR-SR-SV 55T #PH-DATA.#CTR-GR-ST 58T #PH-DATA.#CTR-GR-SV 61T #PH-DATA.#CTR-GS-ST 64T #PH-DATA.#CTR-GS-SV 67T #PH-DATA.#CTR-RA-NJ 70T #PH-DATA.#CTR-SR-NJ 73T #PH-DATA.#CTR-GR-NJ 76T #PH-DATA.#CTR-GS-NJ 79T #PH-DATA.#CTR-RA-PA 82T #PH-DATA.#CTR-SR-PA 85T #PH-DATA.#CTR-GR-PA 88T #PH-DATA.#CTR-GS-PA 91T #PH-DATA.#CTR-RA-CT 94T #PH-DATA.#CTR-SR-CT 97T #PH-DATA.#CTR-GR-CT 100T #PH-DATA.#CTR-GS-CT 103T #PH-DATA.#CTR-RA-TX 106T #PH-DATA.#CTR-SR-TX 109T #PH-DATA.#CTR-GR-TX 112T #PH-DATA.#CTR-GS-TX 115T #PH-DATA.#CTR-RA-VA 118T #PH-DATA.#CTR-SR-VA 121T #PH-DATA.#CTR-GR-VA
            TabSetting(37),pnd_Ph_Data_Pnd_Ctr_Ra_St,new TabSetting(40),pnd_Ph_Data_Pnd_Ctr_Ra_Rp,new TabSetting(43),pnd_Ph_Data_Pnd_Ctr_Ra_Sv,new TabSetting(46),pnd_Ph_Data_Pnd_Ctr_Sr_St,new 
            TabSetting(49),pnd_Ph_Data_Pnd_Ctr_Sr_Rp,new TabSetting(52),pnd_Ph_Data_Pnd_Ctr_Sr_Sv,new TabSetting(55),pnd_Ph_Data_Pnd_Ctr_Gr_St,new TabSetting(58),pnd_Ph_Data_Pnd_Ctr_Gr_Sv,new 
            TabSetting(61),pnd_Ph_Data_Pnd_Ctr_Gs_St,new TabSetting(64),pnd_Ph_Data_Pnd_Ctr_Gs_Sv,new TabSetting(67),pnd_Ph_Data_Pnd_Ctr_Ra_Nj,new TabSetting(70),pnd_Ph_Data_Pnd_Ctr_Sr_Nj,new 
            TabSetting(73),pnd_Ph_Data_Pnd_Ctr_Gr_Nj,new TabSetting(76),pnd_Ph_Data_Pnd_Ctr_Gs_Nj,new TabSetting(79),pnd_Ph_Data_Pnd_Ctr_Ra_Pa,new TabSetting(82),pnd_Ph_Data_Pnd_Ctr_Sr_Pa,new 
            TabSetting(85),pnd_Ph_Data_Pnd_Ctr_Gr_Pa,new TabSetting(88),pnd_Ph_Data_Pnd_Ctr_Gs_Pa,new TabSetting(91),pnd_Ph_Data_Pnd_Ctr_Ra_Ct,new TabSetting(94),pnd_Ph_Data_Pnd_Ctr_Sr_Ct,new 
            TabSetting(97),pnd_Ph_Data_Pnd_Ctr_Gr_Ct,new TabSetting(100),pnd_Ph_Data_Pnd_Ctr_Gs_Ct,new TabSetting(103),pnd_Ph_Data_Pnd_Ctr_Ra_Tx,new TabSetting(106),pnd_Ph_Data_Pnd_Ctr_Sr_Tx,new 
            TabSetting(109),pnd_Ph_Data_Pnd_Ctr_Gr_Tx,new TabSetting(112),pnd_Ph_Data_Pnd_Ctr_Gs_Tx,new TabSetting(115),pnd_Ph_Data_Pnd_Ctr_Ra_Va,new TabSetting(118),pnd_Ph_Data_Pnd_Ctr_Sr_Va,new 
            TabSetting(121),pnd_Ph_Data_Pnd_Ctr_Gr_Va);
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(37),pnd_Ph_Data_Pnd_Ctr_Gs_Va,new TabSetting(40),pnd_Ph_Data_Pnd_Ctr_Gs_Mi,new TabSetting(43),pnd_Ph_Data_Pnd_Ctr_Ra_Mo,new  //Natural: WRITE ( 2 ) 37T #PH-DATA.#CTR-GS-VA 40T #PH-DATA.#CTR-GS-MI 43T #PH-DATA.#CTR-RA-MO 46T #PH-DATA.#CTR-SR-MO 49T #PH-DATA.#CTR-GR-MO 52T #PH-DATA.#CTR-GS-MO 55T #PH-DATA.#CTR-RA-FL 58T #PH-DATA.#CTR-SR-FL 61T #PH-DATA.#CTR-GR-FL 64T #PH-DATA.#CTR-GS-FL 67T #PH-DATA.#CTR-RA-OK 70T #PH-DATA.#CTR-SR-OK 73T #PH-DATA.#CTR-GR-OK 76T #PH-DATA.#CTR-GS-OK 79T #PH-DATA.#CTR-RA-OR 82T #PH-DATA.#CTR-SR-OR 85T #PH-DATA.#CTR-GR-OR 88T #PH-DATA.#CTR-GS-OR 91T #PH-DATA.#CTR-RA-WI 94T #PH-DATA.#CTR-SR-WI 97T #PH-DATA.#CTR-GR-WI 100T #PH-DATA.#CTR-GS-WI 103T #PH-DATA.#CTR-RA-SC 106T #PH-DATA.#CTR-SR-SC 109T #PH-DATA.#CTR-GR-SC 112T #PH-DATA.#CTR-GS-SC 115T #PH-DATA.#CTR-RA-NE 118T #PH-DATA.#CTR-SR-NE 121T #PH-DATA.#CTR-GR-NE 124T #PH-DATA.#CTR-GS-NE
            TabSetting(46),pnd_Ph_Data_Pnd_Ctr_Sr_Mo,new TabSetting(49),pnd_Ph_Data_Pnd_Ctr_Gr_Mo,new TabSetting(52),pnd_Ph_Data_Pnd_Ctr_Gs_Mo,new TabSetting(55),pnd_Ph_Data_Pnd_Ctr_Ra_Fl,new 
            TabSetting(58),pnd_Ph_Data_Pnd_Ctr_Sr_Fl,new TabSetting(61),pnd_Ph_Data_Pnd_Ctr_Gr_Fl,new TabSetting(64),pnd_Ph_Data_Pnd_Ctr_Gs_Fl,new TabSetting(67),pnd_Ph_Data_Pnd_Ctr_Ra_Ok,new 
            TabSetting(70),pnd_Ph_Data_Pnd_Ctr_Sr_Ok,new TabSetting(73),pnd_Ph_Data_Pnd_Ctr_Gr_Ok,new TabSetting(76),pnd_Ph_Data_Pnd_Ctr_Gs_Ok,new TabSetting(79),pnd_Ph_Data_Pnd_Ctr_Ra_Or,new 
            TabSetting(82),pnd_Ph_Data_Pnd_Ctr_Sr_Or,new TabSetting(85),pnd_Ph_Data_Pnd_Ctr_Gr_Or,new TabSetting(88),pnd_Ph_Data_Pnd_Ctr_Gs_Or,new TabSetting(91),pnd_Ph_Data_Pnd_Ctr_Ra_Wi,new 
            TabSetting(94),pnd_Ph_Data_Pnd_Ctr_Sr_Wi,new TabSetting(97),pnd_Ph_Data_Pnd_Ctr_Gr_Wi,new TabSetting(100),pnd_Ph_Data_Pnd_Ctr_Gs_Wi,new TabSetting(103),pnd_Ph_Data_Pnd_Ctr_Ra_Sc,new 
            TabSetting(106),pnd_Ph_Data_Pnd_Ctr_Sr_Sc,new TabSetting(109),pnd_Ph_Data_Pnd_Ctr_Gr_Sc,new TabSetting(112),pnd_Ph_Data_Pnd_Ctr_Gs_Sc,new TabSetting(115),pnd_Ph_Data_Pnd_Ctr_Ra_Ne,new 
            TabSetting(118),pnd_Ph_Data_Pnd_Ctr_Sr_Ne,new TabSetting(121),pnd_Ph_Data_Pnd_Ctr_Gr_Ne,new TabSetting(124),pnd_Ph_Data_Pnd_Ctr_Gs_Ne);
        if (Global.isEscape()) return;
        //*  PINEXP <<<
        getReports().write(2, new TabSetting(37),pnd_Ph_Data_Pnd_Ctr_Gs_Ar,new TabSetting(40),pnd_Ph_Data_Pnd_Ctr_Gs_Hi,new TabSetting(43),pnd_Ph_Data_Pnd_Ctr_Gs_Ky,new  //Natural: WRITE ( 2 ) 37T #PH-DATA.#CTR-GS-AR 40T #PH-DATA.#CTR-GS-HI 43T #PH-DATA.#CTR-GS-KY 46T #PH-DATA.#CTR-RA-MT 49T #PH-DATA.#CTR-SR-MT 52T #PH-DATA.#CTR-GR-MT 55T #PH-DATA.#CTR-GS-MT 58T #PH-DATA.#CTR-GR-NC 61T #PH-DATA.#CTR-GS-TN 64T #PH-DATA.#CTR-RA-UT 67T #PH-DATA.#CTR-SR-UT 70T #PH-DATA.#CTR-SR-VT 73T #PH-DATA.#CTR-GS-VT 76T #PH-DATA.#CTR-RA-PR 79T #PH-DATA.#CTR-GR-PR 82T #PH-DATA.#CTR-SR-OR 85T #PH-DATA.#CTR-RA-NH 88T #PH-DATA.#CTR-GR-NH 91T #PH-DATA.#CTR-SR-NH 94T #PH-DATA.#CTR-GS-NH 97T #PH-DATA.#CTR-GR-NJABP 100T #PH-DATA.#CTR-GS-SUNY
            TabSetting(46),pnd_Ph_Data_Pnd_Ctr_Ra_Mt,new TabSetting(49),pnd_Ph_Data_Pnd_Ctr_Sr_Mt,new TabSetting(52),pnd_Ph_Data_Pnd_Ctr_Gr_Mt,new TabSetting(55),pnd_Ph_Data_Pnd_Ctr_Gs_Mt,new 
            TabSetting(58),pnd_Ph_Data_Pnd_Ctr_Gr_Nc,new TabSetting(61),pnd_Ph_Data_Pnd_Ctr_Gs_Tn,new TabSetting(64),pnd_Ph_Data_Pnd_Ctr_Ra_Ut,new TabSetting(67),pnd_Ph_Data_Pnd_Ctr_Sr_Ut,new 
            TabSetting(70),pnd_Ph_Data_Pnd_Ctr_Sr_Vt,new TabSetting(73),pnd_Ph_Data_Pnd_Ctr_Gs_Vt,new TabSetting(76),pnd_Ph_Data_Pnd_Ctr_Ra_Pr,new TabSetting(79),pnd_Ph_Data_Pnd_Ctr_Gr_Pr,new 
            TabSetting(82),pnd_Ph_Data_Pnd_Ctr_Sr_Or,new TabSetting(85),pnd_Ph_Data_Pnd_Ctr_Ra_Nh,new TabSetting(88),pnd_Ph_Data_Pnd_Ctr_Gr_Nh,new TabSetting(91),pnd_Ph_Data_Pnd_Ctr_Sr_Nh,new 
            TabSetting(94),pnd_Ph_Data_Pnd_Ctr_Gs_Nh,new TabSetting(97),pnd_Ph_Data_Pnd_Ctr_Gr_Njabp,new TabSetting(100),pnd_Ph_Data_Pnd_Ctr_Gs_Suny);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");
        Global.format(0, "LS=132 PS=60");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(40),"2007 TIAA ACCESS MAILING POST LOAD PROCESSING STATISTICS",new 
            TabSetting(118),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(118),"TIME:",Global.getTIME(), new ReportEditMask ("XXXXXXXX"),NEWLINE,new TabSetting(118),"PAGE:",new 
            ColumnSpacing(3),getReports().getPageNumberDbs(1),NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"PROGRAM:",Global.getPROGRAM(),new TabSetting(51),"2007 TIAA ACCESS RIDER MAILING",new 
            TabSetting(116),"DATE:",Global.getDATU(),NEWLINE,new TabSetting(57),"BAD ADDRESS REPORT",new TabSetting(116),"TIME:",Global.getTIME(), new ReportEditMask 
            ("XXXXXXXX"),NEWLINE,new TabSetting(116),"PAGE:",new ColumnSpacing(3),getReports().getPageNumberDbs(2),NEWLINE,NEWLINE,"---------------------------------------------------------------------------------------------------------------------------------",
            NEWLINE,"                               |---------------------------------------- CONTRACT COUNTS ---------------------------------------|",
            NEWLINE,"                               RA RA RA SR SR SR GR GR GS GS RA SR GR GS GS RA SR GR GS RA SR GR GS RA SR GR GS RA SR GR",NEWLINE,"  PIN    NAME                  ST RP SV ST RP SV ST SV ST SV NJ NJ NJ NJ PA PA PA PA CT CT CT CT TX TX TX TX VA VA VA",
            NEWLINE,"                               GS GS RA SR GR GS RA SR GR GS RA SR GR GS RA SR GR GS RA SR GR GS RA SR GR GS RA SR GR GS",NEWLINE,"                               VA MI MO MO MO MO FL FL FL FL OK OK OK OK OR OR OR OR WI WI WI WI SC SC SC SC NE NE NE NE",
            NEWLINE,"                               GS GS GS RA SR GR GS GR GS RA SR SR GS RA GR RA GR SR GS GR GS",NEWLINE,"                               AR HI KY MT MT MT MT NC TN UT UT VT VT PR PR NH NH NH NH NA SN",
            NEWLINE,"---------------------------------------------------------------------------------------------------------------------------------",
            NEWLINE);
    }
}
