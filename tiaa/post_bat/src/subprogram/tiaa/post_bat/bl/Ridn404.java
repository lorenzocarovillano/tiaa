/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:07:16 AM
**        * FROM NATURAL SUBPROGRAM : Ridn404
************************************************************
**        * FILE NAME            : Ridn404.java
**        * CLASS NAME           : Ridn404
**        * INSTANCE NAME        : Ridn404
************************************************************
************************************************************************
* PROGRAM  : RIDN404
* AUTHOR   : JOANNES AVE
* SYSTEM   : RIDER MAILINGS
* TITLE    : 2006 ROTH 403(B)/401(K) ENDORSEMENT
* FUNCTION : THIS MODULE GENERATES XICS STATEMENTS FOR A BATCH OF
*          : REQUESTS TO POST. THE OUTPUT IS USED AS INPUT TO
*          : COMPUST COMPOSITION SOFTWARE.
* DETAILS  : THIS MODULE READS A FILE FROM RIDP402 AND REFORMATS THE
*          : WORK FILE AS INPUT TO COMPUSET. THE COMPOSITION SOFTWARE
*          : CREATES PCL TO BE ROUTED TO A HIGH SPEED PRINTER IN MAP.
*          :
*          : POST PACKAGE ID:
*          :        RDR2006T, RDR2006U
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
* J.AVE    12/09/06 INITIAL IMPLEMENTATION
* J.AVE    01/07/09 ECS CONVERSION
* J.AVE    08/31/11 EXCLUDE RC/RCP CONTRACTS
*                   ADD FLORIDA SPECIFIC CREF ENDORSEMENT
*                   ADD GSRA 457 (B) ENDORSEMENTS
*                   SEND 1 ENDORSEMENT PER LINE OF BUSINESS
* J.AVE    01/17/12 ADD THE FOLLOWING STATE SPECIFIC ENDORSEMENTS:
*                            TIAA 457(B) FOR AR/HI, AND NH
*                            CREF 457(B) FOR IL
* J.AVE    07/15/16 REMARK PASSING OF NAME AND ADDRESS LINES. CCP WILL
*                   GET THIS FROM MDM AS PART OF DCS ELIMINATION.
* L.SHU    05/26/17 PIN EXPANSION PROJECT                     PINEXP
*                   RESTOW FOR RIDA404
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridn404 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaRida404 pdaRida404;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaRidl504 ldaRidl504;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Pst_Tbl_Data_Field2;

    private DbsGroup pnd_Pst_Tbl_Data_Field2__R_Field_1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4;
    private DbsField pnd_Appl_Title;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Date_D;
    private DbsField pnd_Work_Month;
    private DbsField pnd_Work_Day;
    private DbsField pnd_Work_Ccyy;
    private DbsField pnd_Work_Next_Day_Date;
    private DbsField pnd_Work_Formatted_Date;
    private DbsField pnd_Work_Day_Temp;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_K;
    private DbsField pnd_Ndx;
    private DbsField pnd_Contract_Combo;
    private DbsField pnd_Pst_Tbl_Data_Field1;

    private DbsGroup pnd_Array;
    private DbsField pnd_Array_Pnd_Ltr_Type;
    private DbsField pnd_Array_Pnd_Roth_Plan_Type2;
    private DbsField pnd_Array_Pnd_Cover_Letter;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaRidl504 = new LdaRidl504();
        registerRecord(ldaRidl504);

        // parameters
        parameters = new DbsRecord();
        pdaRida404 = new PdaRida404(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pnd_Pst_Tbl_Data_Field2 = parameters.newFieldInRecord("pnd_Pst_Tbl_Data_Field2", "#PST-TBL-DATA-FIELD2", FieldType.STRING, 253);
        pnd_Pst_Tbl_Data_Field2.setParameterOption(ParameterOption.ByReference);

        pnd_Pst_Tbl_Data_Field2__R_Field_1 = parameters.newGroupInRecord("pnd_Pst_Tbl_Data_Field2__R_Field_1", "REDEFINE", pnd_Pst_Tbl_Data_Field2);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory", "#MM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1", "#MM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2", "#MM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3", "#MM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4", "#MM-TITLE4", 
            FieldType.STRING, 50);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Appl_Title = localVariables.newFieldInRecord("pnd_Appl_Title", "#APPL-TITLE", FieldType.STRING, 15);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Date_D = pnd_Work.newFieldInGroup("pnd_Work_Date_D", "DATE-D", FieldType.DATE);
        pnd_Work_Month = pnd_Work.newFieldInGroup("pnd_Work_Month", "MONTH", FieldType.STRING, 11);
        pnd_Work_Day = pnd_Work.newFieldInGroup("pnd_Work_Day", "DAY", FieldType.STRING, 2);
        pnd_Work_Ccyy = pnd_Work.newFieldInGroup("pnd_Work_Ccyy", "CCYY", FieldType.STRING, 4);
        pnd_Work_Next_Day_Date = pnd_Work.newFieldInGroup("pnd_Work_Next_Day_Date", "NEXT-DAY-DATE", FieldType.DATE);
        pnd_Work_Formatted_Date = pnd_Work.newFieldInGroup("pnd_Work_Formatted_Date", "FORMATTED-DATE", FieldType.STRING, 19);
        pnd_Work_Day_Temp = pnd_Work.newFieldInGroup("pnd_Work_Day_Temp", "DAY-TEMP", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 2);
        pnd_Contract_Combo = localVariables.newFieldInRecord("pnd_Contract_Combo", "#CONTRACT-COMBO", FieldType.STRING, 60);
        pnd_Pst_Tbl_Data_Field1 = localVariables.newFieldInRecord("pnd_Pst_Tbl_Data_Field1", "#PST-TBL-DATA-FIELD1", FieldType.STRING, 253);

        pnd_Array = localVariables.newGroupArrayInRecord("pnd_Array", "#ARRAY", new DbsArrayController(1, 16));
        pnd_Array_Pnd_Ltr_Type = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Ltr_Type", "#LTR-TYPE", FieldType.STRING, 1);
        pnd_Array_Pnd_Roth_Plan_Type2 = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Roth_Plan_Type2", "#ROTH-PLAN-TYPE2", FieldType.STRING, 50);
        pnd_Array_Pnd_Cover_Letter = pnd_Array.newFieldInGroup("pnd_Array_Pnd_Cover_Letter", "#COVER-LETTER", FieldType.STRING, 8);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaRidl504.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Appl_Title.setInitialValue("RIDER MAILINGS");
        pnd_Array_Pnd_Ltr_Type.getValue(1).setInitialValue("1");
        pnd_Array_Pnd_Ltr_Type.getValue(2).setInitialValue("2");
        pnd_Array_Pnd_Ltr_Type.getValue(3).setInitialValue("3");
        pnd_Array_Pnd_Ltr_Type.getValue(4).setInitialValue("4");
        pnd_Array_Pnd_Ltr_Type.getValue(5).setInitialValue("5");
        pnd_Array_Pnd_Ltr_Type.getValue(6).setInitialValue("6");
        pnd_Array_Pnd_Ltr_Type.getValue(7).setInitialValue("7");
        pnd_Array_Pnd_Ltr_Type.getValue(8).setInitialValue("8");
        pnd_Array_Pnd_Ltr_Type.getValue(9).setInitialValue("A");
        pnd_Array_Pnd_Ltr_Type.getValue(10).setInitialValue("B");
        pnd_Array_Pnd_Ltr_Type.getValue(11).setInitialValue("C");
        pnd_Array_Pnd_Ltr_Type.getValue(12).setInitialValue("D");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(1).setInitialValue("retirement plan");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(2).setInitialValue("retirement plan");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(3).setInitialValue("retirement planb) plans");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(4).setInitialValue("403(b) and 457(b) plans");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(5).setInitialValue("retirement plan");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(6).setInitialValue("retirement plannnuity 403(b)");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(7).setInitialValue("Tax-Sheltered Annuity 403(b) Program");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(8).setInitialValue("403(b) retirement plan");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(9).setInitialValue("403(b)/457(b) retirement plans");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(10).setInitialValue("403(b)/457(b) retirement plans");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(11).setInitialValue("403(b)/457(b) retirement plans");
        pnd_Array_Pnd_Roth_Plan_Type2.getValue(12).setInitialValue("403(b) and 457(b) retirement plans");
        pnd_Array_Pnd_Cover_Letter.getValue(1).setInitialValue("ROTHMM");
        pnd_Array_Pnd_Cover_Letter.getValue(2).setInitialValue("ROTHWM");
        pnd_Array_Pnd_Cover_Letter.getValue(3).setInitialValue("ROTHMM");
        pnd_Array_Pnd_Cover_Letter.getValue(4).setInitialValue("ROTHWWMM");
        pnd_Array_Pnd_Cover_Letter.getValue(5).setInitialValue("ROTHMM");
        pnd_Array_Pnd_Cover_Letter.getValue(6).setInitialValue("ROTHWM");
        pnd_Array_Pnd_Cover_Letter.getValue(7).setInitialValue("ROTHWWMM");
        pnd_Array_Pnd_Cover_Letter.getValue(8).setInitialValue("ROTHWWWM");
        pnd_Array_Pnd_Cover_Letter.getValue(9).setInitialValue("ROTHWAMM");
        pnd_Array_Pnd_Cover_Letter.getValue(10).setInitialValue("ROTHWBMM");
        pnd_Array_Pnd_Cover_Letter.getValue(11).setInitialValue("ROTHWCMM");
        pnd_Array_Pnd_Cover_Letter.getValue(12).setInitialValue("ROTHWDMM");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Ridn404() throws Exception
    {
        super("Ridn404");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDN404", onError);
        setupReports();
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
        pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: RESET MSG-INFO-SUB
        pnd_Work_Date_D.setValue(Global.getDATX());                                                                                                                       //Natural: ASSIGN DATE-D := *DATX
        //*  PERFORM GET-SIGNATORY-TITLE
        //*  PERFORM MOVE-VALUES-TO-ARRAY
        pnd_Contract_Combo.reset();                                                                                                                                       //Natural: RESET #CONTRACT-COMBO
        pnd_Contract_Combo.setValue("CTR");                                                                                                                               //Natural: MOVE 'CTR' TO #CONTRACT-COMBO
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Gn().notEquals(getZero())))                                                                              //Natural: IF #CTR-RA-SRA-GN NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "-RAGN:", pdaRida404.getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Gn()));  //Natural: COMPRESS #CONTRACT-COMBO '-RAGN:' #CTR-RA-SRA-GN INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Gn().notEquals(getZero())))                                                                         //Natural: IF #CTR-GRA-GSRA-RS-GN NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "-GRGN:", pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Gn())); //Natural: COMPRESS #CONTRACT-COMBO '-GRGN:' #CTR-GRA-GSRA-RS-GN INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Il().notEquals(getZero())))                                                                              //Natural: IF #CTR-RA-SRA-IL NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "-RAIL:", pdaRida404.getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Il()));  //Natural: COMPRESS #CONTRACT-COMBO '-RAIL:' #CTR-RA-SRA-IL INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Il().notEquals(getZero())))                                                                         //Natural: IF #CTR-GRA-GSRA-RS-IL NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "-GRIL:", pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Il())); //Natural: COMPRESS #CONTRACT-COMBO '-GRIL:' #CTR-GRA-GSRA-RS-IL INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Cref_Fl().notEquals(getZero())))                                                                         //Natural: IF #CTR-RA-SRA-CREF-FL NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "-RAFL:", pdaRida404.getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Cref_Fl())); //Natural: COMPRESS #CONTRACT-COMBO '-RAFL:' #CTR-RA-SRA-CREF-FL INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl().notEquals(getZero())))                                                                    //Natural: IF #CTR-GRA-GSRA-RS-CREF-FL NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "-GRFL:", pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl())); //Natural: COMPRESS #CONTRACT-COMBO '-GRFL:' #CTR-GRA-GSRA-RS-CREF-FL INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gsra_457b().notEquals(getZero())))                                                                              //Natural: IF #CTR-GSRA-457B NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "-G457:", pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gsra_457b()));  //Natural: COMPRESS #CONTRACT-COMBO '-G457:' #CTR-GSRA-457B INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        //*  MAIL ITEM
                                                                                                                                                                          //Natural: PERFORM CREATE-MI-RECORD
        sub_Create_Mi_Record();
        if (condition(Global.isEscape())) {return;}
        //*  RIDER MAIL COVER LETTER
                                                                                                                                                                          //Natural: PERFORM CREATE-RM-RECORD
        sub_Create_Rm_Record();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Package_Code().equals("RDR2006U")))                                                                                 //Natural: IF #PACKAGE-CODE EQ 'RDR2006U'
        {
            //*  ADDRESS PAGE
                                                                                                                                                                          //Natural: PERFORM CREATE-AP-RECORD
            sub_Create_Ap_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CONTRACT DETAILS
                                                                                                                                                                          //Natural: PERFORM CREATE-CD-RECORD
        sub_Create_Cd_Record();
        if (condition(Global.isEscape())) {return;}
        //*  EXTRACT PDF
                                                                                                                                                                          //Natural: PERFORM CREATE-EP-RECORD
        sub_Create_Ep_Record();
        if (condition(Global.isEscape())) {return;}
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-MI-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-RM-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-AP-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CD-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-EP-RECORD
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ECS-OUT-DATA
        //*  -------------------------------------
        //*  DEFINE SUBROUTINE GET-SIGNATORY-TITLE
        //*  -------------------------------------
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'WM-SIGNATORY-TITLE'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD1 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND 1'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'MM-SIGNATORY-TITLE'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD2 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND 2'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'ROTH-PLAN-TYPE-1'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD3 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND 3'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'ROTH-PLAN-TYPE-2'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD4 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND 4'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'ROTH-PLAN-TYPE-3'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD5 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND 5'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'ROTH-PLAN-TYPE-4'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD6 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*  END-SUBROUTINE /* GET-SIGNATORY-TITLE
        //*  --------------------------------------
        //*  DEFINE SUBROUTINE MOVE-VALUES-TO-ARRAY
        //*  --------------------------------------
        //*    RESET #I
        //*    FOR #A = 1 TO 4
        //*      ADD 1 TO #I
        //*      MOVE #LTYPE1(#A)      TO #LTR-TYPE(#I)
        //*      MOVE #ROTH-TYPE1(#A)  TO #ROTH-PLAN-TYPE2(#I)
        //*      MOVE #COV-LETTER1(#A) TO #COVER-LETTER(#I)
        //*    END-FOR
        //*    FOR #A = 1 TO 4
        //*      ADD 1 TO #I
        //*      MOVE #LTYPE2(#A)      TO #LTR-TYPE(#I)
        //*      MOVE #ROTH-TYPE2(#A)  TO #ROTH-PLAN-TYPE2(#I)
        //*      MOVE #COV-LETTER2(#A) TO #COVER-LETTER(#I)
        //*    END-FOR
        //*    FOR #A = 1 TO 4
        //*      ADD 1 TO #I
        //*      MOVE #LTYPE3(#A)      TO #LTR-TYPE(#I)
        //*      MOVE #ROTH-TYPE3(#A)  TO #ROTH-PLAN-TYPE2(#I)
        //*      MOVE #COV-LETTER3(#A) TO #COVER-LETTER(#I)
        //*    END-FOR
        //*    FOR #A = 1 TO 4
        //*      ADD 1 TO #I
        //*      MOVE #LTYPE4(#A)      TO #LTR-TYPE(#I)
        //*      MOVE #ROTH-TYPE4(#A)  TO #ROTH-PLAN-TYPE2(#I)
        //*      MOVE #COV-LETTER4(#A) TO #COVER-LETTER(#I)
        //*    END-FOR
        //*  END-SUBROUTINE
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //*  -----------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DATE
        //*  ---------------------------------------
        //*  DEFINE SUBROUTINE SETUP-POSTNET-BARCODE
        //*  ---------------------------------------
        //*    PSTA9532.ADDRSS-PSTL-DTA := #POST-DATA.#POSTAL-DATA
        //*    CALLNAT 'PSTN9532' PSTA9532   /* CONVERT POSTAL DATA TO CORRECT
        //*    MSG-INFO-SUB                /* POSTNET BARCODE FORMAT
        //*  END-SUBROUTINE  /* SETUP-POSTNET-BARCDOE
    }
    private void sub_Create_Mi_Record() throws Exception                                                                                                                  //Natural: CREATE-MI-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Rec_Id().setValue("MI");                                                                                                                 //Natural: MOVE 'MI' TO ECS-RECORD.REC-ID
        ldaRidl504.getEcs_Record_Pin_Nbr().setValue(pdaRida404.getPnd_Post_Data_Pnd_Pin_Number());                                                                        //Natural: MOVE #POST-DATA.#PIN-NUMBER TO ECS-RECORD.PIN-NBR
        ldaRidl504.getEcs_Record_Pst_Rqst_Id().setValue(pdaRida404.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                                   //Natural: MOVE #POST-DATA.#PST-RQST-ID TO ECS-RECORD.PST-RQST-ID
        //*    MOVE #POST-DATA.#FULL-NAME             TO ECS-RECORD.FULL-NME
        //*    MOVE #POST-DATA.#ADDRESS-TYPE-CDE      TO ECS-RECORD.ADDRSS-TYP-CDE
        //*    IF PSTA9670.ENVIRONMENT NE 'PROD'    /* SCRAMBLE ADDRESS LINES
        //*      CALLNAT 'PSTN9990' USING #POST-DATA.#ADDRESS-LINE-TXT(*)
        //*    END-IF
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(1) TO
        //*         ECS-RECORD.ADDRSS-LINE-TXT(1)
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(2) TO
        //*         ECS-RECORD.ADDRSS-LINE-TXT(2)
        //*    #A := 2
        //*    FOR #I = 3 TO 6
        //*      ADD 1 TO #A
        //*      IF #POST-DATA.#ADDRESS-LINE-TXT(#I) NE ' '
        //*        MOVE #POST-DATA.#ADDRESS-LINE-TXT(#I) TO
        //*             ECS-RECORD.ADDRSS-LINE-TXT(#I)
        //*      ELSE
        //*        IF PSTA9670.ENVIRONMENT NE  'PROD'
        //*          MOVE 'TEST   ...DO NOT MAIL...' TO
        //*               ECS-RECORD.ADDRSS-LINE-TXT(#A)
        //*        END-IF
        //*      END-IF
        //*    END-FOR
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(1)   TO ECS-RECORD.ADDRSS-LINE-1
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(2)   TO ECS-RECORD.ADDRSS-LINE-2
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(3)   TO ECS-RECORD.ADDRSS-LINE-3
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(4)   TO ECS-RECORD.ADDRSS-LINE-4
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(5)   TO ECS-RECORD.ADDRSS-LINE-5
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(6)   TO ECS-RECORD.ADDRSS-LINE-6
        //*    PERFORM SETUP-POSTNET-BARCODE
        //*    EXAMINE PSTA9532.FULL-POSTNET-BAR-CDE 'B' REPLACE WITH '*'
        //*    EXAMINE PSTA9532.FULL-POSTNET-BAR-CDE 'E' REPLACE WITH '*'
        //*    MOVE PSTA9532.FULL-POSTNET-BAR-CDE     TO ECS-RECORD.POSTNET-BARCODE
        ldaRidl504.getEcs_Record_Pckge_Cde().setValue(pdaRida404.getPnd_Post_Data_Pnd_Package_Code());                                                                    //Natural: MOVE #POST-DATA.#PACKAGE-CODE TO ECS-RECORD.PCKGE-CDE
        ldaRidl504.getEcs_Record_Letter_Dte().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO ECS-RECORD.LETTER-DTE
        //*    IF #POST-DATA.#ADDRESS-TYPE-CDE NE 'U'  /* OTHER THAN DOMESTIC
        //*      MOVE '8' TO ECS-RECORD.BYTE2
        //*    ELSE
        //*      MOVE '0' TO ECS-RECORD.BYTE2
        //*    END-IF
        ldaRidl504.getEcs_Record_Byte4().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE4
        ldaRidl504.getEcs_Record_Byte5().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE5
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-MI-RECORD
    }
    private void sub_Create_Rm_Record() throws Exception                                                                                                                  //Natural: CREATE-RM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Rm_Record_Type().setValue("RM");                                                                                                         //Natural: MOVE 'RM' TO ECS-RECORD.RM-RECORD-TYPE
        DbsUtil.examine(new ExamineSource(pnd_Array_Pnd_Ltr_Type.getValue("*")), new ExamineSearch(pdaRida404.getPnd_Post_Data_Pnd_Letter_Type()), new                    //Natural: EXAMINE #LTR-TYPE ( * ) FOR #LETTER-TYPE GIVING INDEX #NDX
            ExamineGivingIndex(pnd_Ndx));
        ldaRidl504.getEcs_Record_Rm_Cover_Letter_Code().setValue(pnd_Array_Pnd_Cover_Letter.getValue(pnd_Ndx));                                                           //Natural: MOVE #COVER-LETTER ( #NDX ) TO ECS-RECORD.RM-COVER-LETTER-CODE
        //*    IF ECS-RECORD.RM-COVER-LETTER-CODE EQ 'ROTHWWMM' OR EQ 'ROTHMM'
        ldaRidl504.getEcs_Record_Rm_Signatory_Name().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory);                                                                  //Natural: MOVE #MM-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
        ldaRidl504.getEcs_Record_Rm_Signatory_Title1().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1);                                                                   //Natural: MOVE #MM-TITLE1 TO ECS-RECORD.RM-SIGNATORY-TITLE1
        ldaRidl504.getEcs_Record_Rm_Signatory_Title2().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2);                                                                   //Natural: MOVE #MM-TITLE2 TO ECS-RECORD.RM-SIGNATORY-TITLE2
        ldaRidl504.getEcs_Record_Rm_Signatory_Title3().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3);                                                                   //Natural: MOVE #MM-TITLE3 TO ECS-RECORD.RM-SIGNATORY-TITLE3
        ldaRidl504.getEcs_Record_Rm_Signatory_Title4().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4);                                                                   //Natural: MOVE #MM-TITLE4 TO ECS-RECORD.RM-SIGNATORY-TITLE4
        //*     ELSE
        //*       MOVE #WM-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
        //*       MOVE #WM-TITLE1    TO ECS-RECORD.RM-SIGNATORY-TITLE1
        //*       MOVE #WM-TITLE2    TO ECS-RECORD.RM-SIGNATORY-TITLE2
        //*       MOVE #WM-TITLE3    TO ECS-RECORD.RM-SIGNATORY-TITLE3
        //*       MOVE #WM-TITLE4    TO ECS-RECORD.RM-SIGNATORY-TITLE4
        //*    END-IF
        ldaRidl504.getEcs_Record_Rm_Institution_Name().setValue(pdaRida404.getPnd_Post_Data_Pnd_Institution_Name());                                                      //Natural: MOVE #POST-DATA.#INSTITUTION-NAME TO ECS-RECORD.RM-INSTITUTION-NAME
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Package_Code().equals("RDR2006T")))                                                                                 //Natural: IF #PACKAGE-CODE EQ 'RDR2006T'
        {
            ldaRidl504.getEcs_Record_Rm_Cont_Num().setValue(pnd_Contract_Combo);                                                                                          //Natural: MOVE #CONTRACT-COMBO TO ECS-RECORD.RM-CONT-NUM
        }                                                                                                                                                                 //Natural: END-IF
        ldaRidl504.getEcs_Record_Rm_Roth_Plan_Type2().setValue(pnd_Array_Pnd_Roth_Plan_Type2.getValue(pnd_Ndx));                                                          //Natural: MOVE #ROTH-PLAN-TYPE2 ( #NDX ) TO ECS-RECORD.RM-ROTH-PLAN-TYPE2
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-RM-RECORD
    }
    private void sub_Create_Ap_Record() throws Exception                                                                                                                  //Natural: CREATE-AP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Ap_Record_Type().setValue("AP");                                                                                                         //Natural: MOVE 'AP' TO ECS-RECORD.AP-RECORD-TYPE
        ldaRidl504.getEcs_Record_Ap_Cont_Num().setValue(pnd_Contract_Combo);                                                                                              //Natural: MOVE #CONTRACT-COMBO TO ECS-RECORD.AP-CONT-NUM
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-AP-RECORD
    }
    private void sub_Create_Cd_Record() throws Exception                                                                                                                  //Natural: CREATE-CD-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Cd_Record_Type().setValue("CD");                                                                                                         //Natural: MOVE 'CD' TO ECS-RECORD.CD-RECORD-TYPE
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaRida404.getPnd_Post_Data_Pnd_Contracts().getValue(pnd_I).notEquals(" ")))                                                                    //Natural: IF #CONTRACTS ( #I ) NE ' '
            {
                ldaRidl504.getEcs_Record_Cd_Contract_Number().setValue(pdaRida404.getPnd_Post_Data_Pnd_Contracts().getValue(pnd_I));                                      //Natural: MOVE #CONTRACTS ( #I ) TO ECS-RECORD.CD-CONTRACT-NUMBER
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CREATE-CD-RECORD
    }
    private void sub_Create_Ep_Record() throws Exception                                                                                                                  //Natural: CREATE-EP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Ep_Record_Type().setValue("EP");                                                                                                         //Natural: MOVE 'EP' TO ECS-RECORD.EP-RECORD-TYPE
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Gn().equals(1)))                                                                                         //Natural: IF #CTR-RA-SRA-GN EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TIAA_ROTH_IND");                                                                                         //Natural: MOVE 'RDR_TIAA_ROTH_IND' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CREF_ROTH_IND");                                                                                         //Natural: MOVE 'RDR_CREF_ROTH_IND' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Gn().equals(1)))                                                                                    //Natural: IF #CTR-GRA-GSRA-RS-GN EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TIAA_ROTH_GRP");                                                                                         //Natural: MOVE 'RDR_TIAA_ROTH_GRP' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CREF_ROTH_GRP");                                                                                         //Natural: MOVE 'RDR_CREF_ROTH_GRP' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Il().equals(1)))                                                                                         //Natural: IF #CTR-RA-SRA-IL EQ 1
        {
            if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Gn().notEquals(1)))                                                                                  //Natural: IF #CTR-RA-SRA-GN NE 1
            {
                ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TIAA_ROTH_IND");                                                                                     //Natural: MOVE 'RDR_TIAA_ROTH_IND' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CREF_ROTH_IND_IL");                                                                                      //Natural: MOVE 'RDR_CREF_ROTH_IND_IL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Il().equals(1)))                                                                                    //Natural: IF #CTR-GRA-GSRA-RS-IL EQ 1
        {
            if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Gn().notEquals(1)))                                                                             //Natural: IF #CTR-GRA-GSRA-RS-GN NE 1
            {
                ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TIAA_ROTH_GRP");                                                                                     //Natural: MOVE 'RDR_TIAA_ROTH_GRP' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CREF_ROTH_GRP_IL");                                                                                      //Natural: MOVE 'RDR_CREF_ROTH_GRP_IL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Cref_Fl().equals(1)))                                                                                    //Natural: IF #CTR-RA-SRA-CREF-FL EQ 1
        {
            if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Gn().notEquals(1)))                                                                                  //Natural: IF #CTR-RA-SRA-GN NE 1
            {
                ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TIAA_ROTH_IND");                                                                                     //Natural: MOVE 'RDR_TIAA_ROTH_IND' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CREF_ROTH_IND_FL");                                                                                      //Natural: MOVE 'RDR_CREF_ROTH_IND_FL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl().equals(1)))                                                                               //Natural: IF #CTR-GRA-GSRA-RS-CREF-FL EQ 1
        {
            if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Gn().notEquals(1)))                                                                             //Natural: IF #CTR-GRA-GSRA-RS-GN NE 1
            {
                ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TIAA_ROTH_GRP");                                                                                     //Natural: MOVE 'RDR_TIAA_ROTH_GRP' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CREF_ROTH_GRP_FL");                                                                                      //Natural: MOVE 'RDR_CREF_ROTH_GRP_FL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gsra_457b().equals(1)))                                                                                         //Natural: IF #CTR-GSRA-457B EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TGSRA-457b-E3");                                                                                         //Natural: MOVE 'RDR_TGSRA-457b-E3' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref().equals(1)))                                                                                    //Natural: IF #CTR-GSRA-457B-CREF EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CGSRA-457b-E3");                                                                                         //Natural: MOVE 'RDR_CGSRA-457b-E3' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gsra_457b_Nl().equals(1)))                                                                                      //Natural: IF #CTR-GSRA-457B-NL EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TGSRA-457b-E3-NL");                                                                                      //Natural: MOVE 'RDR_TGSRA-457b-E3-NL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gsra_457b_Nh().equals(1)))                                                                                      //Natural: IF #CTR-GSRA-457B-NH EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TGSRA-457b-E3-NH");                                                                                      //Natural: MOVE 'RDR_TGSRA-457b-E3-NH' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida404.getPnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref_Il().equals(1)))                                                                                 //Natural: IF #CTR-GSRA-457B-CREF-IL EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CGSRA-457b-E3-IL");                                                                                      //Natural: MOVE 'RDR_CGSRA-457b-E3-IL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-EP-RECORD
    }
    private void sub_Write_Ecs_Out_Data() throws Exception                                                                                                                //Natural: WRITE-ECS-OUT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        getWorkFiles().write(2, false, ldaRidl504.getEcs_Record());                                                                                                       //Natural: WRITE WORK FILE 2 ECS-RECORD
        //*  WRITE-ECS-OUT-DATA
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, Global.getPROGRAM(),"IN PROCESS-ERROR","SUBROUTINE",pdaRida404.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                     //Natural: WRITE ( 0 ) *PROGRAM 'IN PROCESS-ERROR' 'SUBROUTINE' #POST-DATA.#PST-RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  PSTA9531.#POST-DATA-STATUS := PSTL9620.GEN-FAILED
        //*  PSTA9531.TERMINATE-IND    := TRUE
        getReports().print(0, Global.getPROGRAM(),"JOB TERMINATED! NO",pnd_Appl_Title,"CREATED.",NEWLINE,"SEE MESSAGES ABOVE FOR FURTHER INFORMATION");                   //Natural: PRINT ( 0 ) *PROGRAM 'JOB TERMINATED! NO' #APPL-TITLE 'CREATED.' / 'SEE MESSAGES ABOVE FOR FURTHER INFORMATION'
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
        //*  PROCESS-ERROR
    }
    private void sub_Format_Date() throws Exception                                                                                                                       //Natural: FORMAT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------
        pnd_Work_Month.setValueEdited(pnd_Work_Date_D,new ReportEditMask("LLLLLLLLLLL"));                                                                                 //Natural: MOVE EDITED #WORK.DATE-D ( EM = L ( 11 ) ) TO #WORK.MONTH
        pnd_Work_Day_Temp.setValueEdited(pnd_Work_Date_D,new ReportEditMask("ZD"));                                                                                       //Natural: MOVE EDITED #WORK.DATE-D ( EM = ZD ) TO #WORK.DAY-TEMP
        pnd_Work_Day.setValue(pnd_Work_Day_Temp, MoveOption.LeftJustified);                                                                                               //Natural: MOVE LEFT #WORK.DAY-TEMP TO #WORK.DAY
        pnd_Work_Ccyy.setValueEdited(pnd_Work_Date_D,new ReportEditMask("YYYY"));                                                                                         //Natural: MOVE EDITED #WORK.DATE-D ( EM = YYYY ) TO #WORK.CCYY
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Month));                                                                //Natural: COMPRESS #WORK.MONTH INTO #WORK.FORMATTED-DATE LEAVING NO
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(pnd_Work_Formatted_Date, pnd_Work_Ccyy));                                                                       //Natural: COMPRESS #WORK.FORMATTED-DATE #WORK.CCYY INTO #WORK.FORMATTED-DATE
        //*  FORMAT-DATE
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
    }
}
