/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:07:27 AM
**        * FROM NATURAL SUBPROGRAM : Ridn644
************************************************************
**        * FILE NAME            : Ridn644.java
**        * CLASS NAME           : Ridn644
**        * INSTANCE NAME        : Ridn644
************************************************************
************************************************************************
* PROGRAM  : RIDN644
* AUTHOR   : JOANNES AVE
* SYSTEM   : PROJCNY
* TITLE    : 2016 CREF REDESIGN SHARE CLASS ENDORSEMENT MAILING
* FUNCTION : THIS MODULE GENERATES DATA FOR CONVERSION TO XML AS INPUT
*          : TO DCS DIALOGUE TO GENERATE THE ENDORSEMENTS.
* CREATED  : NOV 2016.
* DETAILS  : THIS MODULE READS A FILE FROM RIDP642 AND FORMATS THE
*          : WORK FILE AS INPUT TO DCS.
*          :
*          : POST PACKAGE ID: RDR2016C (PAPER), RDR2016D (EMAIL)
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
* J.AVE    11/04/15 INITIAL CODING
* J.AVE    07/18/16 REMARK PASSING OF NAME AND ADDRESS. CCP WILL HANDLE
*                   THIS AS PART OF DCS ELIMINATION
* L SHU    05/22/17 PIN EXPANSION                              PINEXP
*                   RESTOW FOR RIDA644
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridn644 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaRida644 pdaRida644;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaRidl634 ldaRidl634;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Pst_Tbl_Data_Field2;

    private DbsGroup pnd_Pst_Tbl_Data_Field2__R_Field_1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4;
    private DbsField pnd_Appl_Title;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Date_D;
    private DbsField pnd_Work_Month;
    private DbsField pnd_Work_Day;
    private DbsField pnd_Work_Ccyy;
    private DbsField pnd_Work_Next_Day_Date;
    private DbsField pnd_Work_Formatted_Date;
    private DbsField pnd_Work_Day_Temp;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_K;
    private DbsField pnd_J;
    private DbsField pnd_P;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaRidl634 = new LdaRidl634();
        registerRecord(ldaRidl634);

        // parameters
        parameters = new DbsRecord();
        pdaRida644 = new PdaRida644(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pnd_Pst_Tbl_Data_Field2 = parameters.newFieldInRecord("pnd_Pst_Tbl_Data_Field2", "#PST-TBL-DATA-FIELD2", FieldType.STRING, 253);
        pnd_Pst_Tbl_Data_Field2.setParameterOption(ParameterOption.ByReference);

        pnd_Pst_Tbl_Data_Field2__R_Field_1 = parameters.newGroupInRecord("pnd_Pst_Tbl_Data_Field2__R_Field_1", "REDEFINE", pnd_Pst_Tbl_Data_Field2);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory", "#MM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1", "#MM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2", "#MM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3", "#MM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4", "#MM-TITLE4", 
            FieldType.STRING, 50);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Appl_Title = localVariables.newFieldInRecord("pnd_Appl_Title", "#APPL-TITLE", FieldType.STRING, 15);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Date_D = pnd_Work.newFieldInGroup("pnd_Work_Date_D", "DATE-D", FieldType.DATE);
        pnd_Work_Month = pnd_Work.newFieldInGroup("pnd_Work_Month", "MONTH", FieldType.STRING, 11);
        pnd_Work_Day = pnd_Work.newFieldInGroup("pnd_Work_Day", "DAY", FieldType.STRING, 2);
        pnd_Work_Ccyy = pnd_Work.newFieldInGroup("pnd_Work_Ccyy", "CCYY", FieldType.STRING, 4);
        pnd_Work_Next_Day_Date = pnd_Work.newFieldInGroup("pnd_Work_Next_Day_Date", "NEXT-DAY-DATE", FieldType.DATE);
        pnd_Work_Formatted_Date = pnd_Work.newFieldInGroup("pnd_Work_Formatted_Date", "FORMATTED-DATE", FieldType.STRING, 19);
        pnd_Work_Day_Temp = pnd_Work.newFieldInGroup("pnd_Work_Day_Temp", "DAY-TEMP", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.NUMERIC, 2);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaRidl634.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Appl_Title.setInitialValue("RIDER MAILINGS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Ridn644() throws Exception
    {
        super("Ridn644");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDN644", onError);
        setupReports();
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
        pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: RESET MSG-INFO-SUB
        pnd_Work_Date_D.setValue(Global.getDATX());                                                                                                                       //Natural: ASSIGN DATE-D := *DATX
        //*  MAIL ITEM
                                                                                                                                                                          //Natural: PERFORM CREATE-MI-RECORD
        sub_Create_Mi_Record();
        if (condition(Global.isEscape())) {return;}
        //*  RIDER MAIL COVER LETTER
                                                                                                                                                                          //Natural: PERFORM CREATE-RM-RECORD
        sub_Create_Rm_Record();
        if (condition(Global.isEscape())) {return;}
        //*  DIALOGUE TEMPLATE ENDORSEMENT
                                                                                                                                                                          //Natural: PERFORM CREATE-EN-RECORD
        sub_Create_En_Record();
        if (condition(Global.isEscape())) {return;}
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-MI-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-RM-RECORD
        //*  ----------------------------------
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-EN-RECORD
        //*  GRA / GSRA / RC / RCP CONTRACTS - GENERIC
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ECS-OUT-DATA
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //*  -----------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DATE
        //*  ---------------------------------------
        //*  DEFINE SUBROUTINE SETUP-POSTNET-BARCODE
        //*  ---------------------------------------
        //*    PSTA9532.ADDRSS-PSTL-DTA := #POST-DATA.#POSTAL-DATA
        //*    CALLNAT 'PSTN9532' PSTA9532   /* CONVERT POSTAL DATA TO CORRECT
        //*    MSG-INFO-SUB                /* POSTNET BARCODE FORMAT
        //*  END-SUBROUTINE  /* SETUP-POSTNET-BARCDOE
    }
    private void sub_Create_Mi_Record() throws Exception                                                                                                                  //Natural: CREATE-MI-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_Rec_Id().setValue("MI");                                                                                                                 //Natural: MOVE 'MI' TO ECS-RECORD.REC-ID
        ldaRidl634.getEcs_Record_Pin_Nbr().setValue(pdaRida644.getPnd_Post_Data_Pnd_Pin_Number());                                                                        //Natural: MOVE #POST-DATA.#PIN-NUMBER TO ECS-RECORD.PIN-NBR
        ldaRidl634.getEcs_Record_Pst_Rqst_Id().setValue(pdaRida644.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                                   //Natural: MOVE #POST-DATA.#PST-RQST-ID TO ECS-RECORD.PST-RQST-ID
        //*    MOVE #POST-DATA.#FULL-NAME             TO ECS-RECORD.FULL-NME
        //*    MOVE #POST-DATA.#ADDRESS-TYPE-CDE      TO ECS-RECORD.ADDRSS-TYP-CDE
        //*    MOVE #POST-DATA.#EMAIL-ADDRESS         TO ECS-RECORD.ALT-DLVRY-ADDR
        //*    MOVE 'N'                               TO ECS-RECORD.APPROVED-IND
        //*    IF #PACKAGE-CODE = 'RDR2016D'
        //*      MOVE 'EML'                           TO ECS-RECORD.PCKGE-DLVRY-TYP
        //*    END-IF
        //*    IF PSTA9670.ENVIRONMENT NE 'PROD'    /* SCRAMBLE ADDRESS LINES
        //*      CALLNAT 'PSTN9990' USING #POST-DATA.#ADDRESS-LINE-TXT(*)
        //*    END-IF
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(1) TO
        //*         ECS-RECORD.ADDRSS-LINE-TXT(1)
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(2) TO
        //*         ECS-RECORD.ADDRSS-LINE-TXT(2)
        //*    #A := 2
        //*    FOR #I = 3 TO 6
        //*      ADD 1 TO #A
        //*      IF #POST-DATA.#ADDRESS-LINE-TXT(#I) NE ' '
        //*        MOVE #POST-DATA.#ADDRESS-LINE-TXT(#I) TO
        //*             ECS-RECORD.ADDRSS-LINE-TXT(#I)
        //*      ELSE
        //*        IF PSTA9670.ENVIRONMENT NE  'PROD'
        //*          MOVE 'TEST   ...DO NOT MAIL...' TO
        //*               ECS-RECORD.ADDRSS-LINE-TXT(#A)
        //*        END-IF
        //*      END-IF
        //*    END-FOR
        //*    PERFORM SETUP-POSTNET-BARCODE
        //*    EXAMINE PSTA9532.FULL-POSTNET-BAR-CDE 'B' REPLACE WITH '*'
        //*    EXAMINE PSTA9532.FULL-POSTNET-BAR-CDE 'E' REPLACE WITH '*'
        //*    MOVE PSTA9532.FULL-POSTNET-BAR-CDE     TO ECS-RECORD.POSTNET-BARCODE
        ldaRidl634.getEcs_Record_Pckge_Cde().setValue(pdaRida644.getPnd_Post_Data_Pnd_Package_Code());                                                                    //Natural: MOVE #POST-DATA.#PACKAGE-CODE TO ECS-RECORD.PCKGE-CDE
        ldaRidl634.getEcs_Record_Letter_Dte().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO ECS-RECORD.LETTER-DTE
        //*    IF #POST-DATA.#ADDRESS-TYPE-CDE NE 'U'  /* OTHER THAN DOMESTIC
        //*      MOVE '8' TO ECS-RECORD.BYTE2
        //*    ELSE
        //*      MOVE '0' TO ECS-RECORD.BYTE2
        //*    END-IF
        ldaRidl634.getEcs_Record_Byte4().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE4
        ldaRidl634.getEcs_Record_Byte5().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE5
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-MI-RECORD
    }
    private void sub_Create_Rm_Record() throws Exception                                                                                                                  //Natural: CREATE-RM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_Rm_Record_Type().setValue("RM");                                                                                                         //Natural: MOVE 'RM' TO ECS-RECORD.RM-RECORD-TYPE
        ldaRidl634.getEcs_Record_Rm_Institution_Name().setValue(pdaRida644.getPnd_Post_Data_Pnd_Institution_Name());                                                      //Natural: MOVE #INSTITUTION-NAME TO ECS-RECORD.RM-INSTITUTION-NAME
        ldaRidl634.getEcs_Record_Rm_Share_Class_Effect_Date().setValue(pdaRida644.getPnd_Post_Data_Pnd_Effective_Date());                                                 //Natural: MOVE #EFFECTIVE-DATE TO ECS-RECORD.RM-SHARE-CLASS-EFFECT-DATE
        //*    WRITE '=' #INSTITUTION-NAME (AL=50)
        //*    WRITE '=' #EFFECTIVE-DATE
        if (condition(pdaRida644.getPnd_Post_Data_Pnd_Letter_Type().equals("1")))                                                                                         //Natural: IF #LETTER-TYPE EQ '1'
        {
            ldaRidl634.getEcs_Record_Rm_Cover_Letter_Code().setValue("CREFSCMM");                                                                                         //Natural: MOVE 'CREFSCMM' TO ECS-RECORD.RM-COVER-LETTER-CODE
            ldaRidl634.getEcs_Record_Rm_Signatory_Name().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory);                                                              //Natural: MOVE #MM-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
            ldaRidl634.getEcs_Record_Rm_Signatory_Title1().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1);                                                               //Natural: MOVE #MM-TITLE1 TO ECS-RECORD.RM-SIGNATORY-TITLE1
            ldaRidl634.getEcs_Record_Rm_Signatory_Title2().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2);                                                               //Natural: MOVE #MM-TITLE2 TO ECS-RECORD.RM-SIGNATORY-TITLE2
            ldaRidl634.getEcs_Record_Rm_Signatory_Title3().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3);                                                               //Natural: MOVE #MM-TITLE3 TO ECS-RECORD.RM-SIGNATORY-TITLE3
            ldaRidl634.getEcs_Record_Rm_Signatory_Title4().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4);                                                               //Natural: MOVE #MM-TITLE4 TO ECS-RECORD.RM-SIGNATORY-TITLE4
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-RM-RECORD
    }
    private void sub_Create_En_Record() throws Exception                                                                                                                  //Natural: CREATE-EN-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_En_Record_Type().setValue("EN");                                                                                                         //Natural: MOVE 'EN' TO ECS-RECORD.EN-RECORD-TYPE
        FOR01:                                                                                                                                                            //Natural: FOR #K = 1 TO #CTR-CREF-CONTRACT
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida644.getPnd_Post_Data_Pnd_Ctr_Cref_Contract())); pnd_K.nadd(1))
        {
            if (condition(pdaRida644.getPnd_Post_Data_Pnd_Cref_Contracts().getValue(pnd_K).notEquals(" ")))                                                               //Natural: IF #CREF-CONTRACTS ( #K ) NE ' '
            {
                if (condition(pdaRida644.getPnd_Post_Data_Pnd_Contract_Type().getValue(pnd_K).equals("GR") && pdaRida644.getPnd_Post_Data_Pnd_Contract_State().getValue(pnd_K).equals("GN"))) //Natural: IF #CONTRACT-TYPE ( #K ) EQ 'GR' AND #CONTRACT-STATE ( #K ) EQ 'GN'
                {
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CRSCGRGN");                                                                                      //Natural: MOVE 'CRSCGRGN' TO ECS-RECORD.EN-ENDORSE-TYPE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaRida644.getPnd_Post_Data_Pnd_Contract_Type().getValue(pnd_K).equals("GR") && pdaRida644.getPnd_Post_Data_Pnd_Contract_State().getValue(pnd_K).equals("FL"))) //Natural: IF #CONTRACT-TYPE ( #K ) EQ 'GR' AND #CONTRACT-STATE ( #K ) EQ 'FL'
                {
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CRSCGRFL");                                                                                      //Natural: MOVE 'CRSCGRFL' TO ECS-RECORD.EN-ENDORSE-TYPE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaRida644.getPnd_Post_Data_Pnd_Contract_Type().getValue(pnd_K).equals("GR") && pdaRida644.getPnd_Post_Data_Pnd_Contract_State().getValue(pnd_K).equals("IL"))) //Natural: IF #CONTRACT-TYPE ( #K ) EQ 'GR' AND #CONTRACT-STATE ( #K ) EQ 'IL'
                {
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CRSCGRIL");                                                                                      //Natural: MOVE 'CRSCGRIL' TO ECS-RECORD.EN-ENDORSE-TYPE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaRida644.getPnd_Post_Data_Pnd_Contract_Type().getValue(pnd_K).equals("IN") && pdaRida644.getPnd_Post_Data_Pnd_Contract_State().getValue(pnd_K).equals("GN"))) //Natural: IF #CONTRACT-TYPE ( #K ) EQ 'IN' AND #CONTRACT-STATE ( #K ) EQ 'GN'
                {
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CRSCINGN");                                                                                      //Natural: MOVE 'CRSCINGN' TO ECS-RECORD.EN-ENDORSE-TYPE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaRida644.getPnd_Post_Data_Pnd_Contract_Type().getValue(pnd_K).equals("IN") && pdaRida644.getPnd_Post_Data_Pnd_Contract_State().getValue(pnd_K).equals("FL"))) //Natural: IF #CONTRACT-TYPE ( #K ) EQ 'IN' AND #CONTRACT-STATE ( #K ) EQ 'FL'
                {
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CRSCINFL");                                                                                      //Natural: MOVE 'CRSCINFL' TO ECS-RECORD.EN-ENDORSE-TYPE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaRida644.getPnd_Post_Data_Pnd_Contract_Type().getValue(pnd_K).equals("IN") && pdaRida644.getPnd_Post_Data_Pnd_Contract_State().getValue(pnd_K).equals("IL"))) //Natural: IF #CONTRACT-TYPE ( #K ) EQ 'IN' AND #CONTRACT-STATE ( #K ) EQ 'IL'
                {
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CRSCINIL");                                                                                      //Natural: MOVE 'CRSCINIL' TO ECS-RECORD.EN-ENDORSE-TYPE
                }                                                                                                                                                         //Natural: END-IF
                ldaRidl634.getEcs_Record_En_Contract_Number().setValue(pdaRida644.getPnd_Post_Data_Pnd_Cref_Contracts().getValue(pnd_K));                                 //Natural: MOVE #CREF-CONTRACTS ( #K ) TO ECS-RECORD.EN-CONTRACT-NUMBER
                if (condition(pdaRida644.getPnd_Post_Data_Pnd_Multi_Plan_Ctr().getValue(pnd_K).greater(1)))                                                               //Natural: IF #MULTI-PLAN-CTR ( #K ) GT 1
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("Y");                                                                                           //Natural: MOVE 'Y' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("N");                                                                                           //Natural: MOVE 'N' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: END-IF
                FOR02:                                                                                                                                                    //Natural: FOR #J = 1 TO 10
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(10)); pnd_J.nadd(1))
                {
                    if (condition(pdaRida644.getPnd_Post_Data_Pnd_Plan_Name().getValue(pnd_K,pnd_J).notEquals(" ")))                                                      //Natural: IF #PLAN-NAME ( #K,#J ) NE ' '
                    {
                        ldaRidl634.getEcs_Record_En_Plan_Name().getValue(pnd_J).setValue(pdaRida644.getPnd_Post_Data_Pnd_Plan_Name().getValue(pnd_K,pnd_J));              //Natural: MOVE #PLAN-NAME ( #K,#J ) TO ECS-RECORD.EN-PLAN-NAME ( #J )
                        getReports().write(0, "=",pdaRida644.getPnd_Post_Data_Pnd_Plan_Name().getValue(pnd_K,pnd_J), new AlphanumericLength (50));                        //Natural: WRITE '=' #PLAN-NAME ( #K,#J ) ( AL = 50 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaRidl634.getEcs_Record_En_Plan_Share_Class().getValue(pnd_J).setValue(pdaRida644.getPnd_Post_Data_Pnd_Share_Class().getValue(pnd_K,             //Natural: MOVE #SHARE-CLASS ( #K,#J ) TO ECS-RECORD.EN-PLAN-SHARE-CLASS ( #J )
                            pnd_J));
                        getReports().write(0, "=",pdaRida644.getPnd_Post_Data_Pnd_Share_Class().getValue(pnd_K,pnd_J));                                                   //Natural: WRITE '=' #SHARE-CLASS ( #K,#J )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CREATE-EN-RECORD
    }
    private void sub_Write_Ecs_Out_Data() throws Exception                                                                                                                //Natural: WRITE-ECS-OUT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        getWorkFiles().write(2, false, ldaRidl634.getEcs_Record());                                                                                                       //Natural: WRITE WORK FILE 2 ECS-RECORD
        //*  WRITE-ECS-OUT-DATA
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, Global.getPROGRAM(),"IN PROCESS-ERROR","SUBROUTINE",pdaRida644.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                     //Natural: WRITE ( 0 ) *PROGRAM 'IN PROCESS-ERROR' 'SUBROUTINE' #POST-DATA.#PST-RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  PSTA9531.#POST-DATA-STATUS := PSTL9620.GEN-FAILED
        //*  PSTA9531.TERMINATE-IND    := TRUE
        getReports().print(0, Global.getPROGRAM(),"JOB TERMINATED! NO",pnd_Appl_Title,"CREATED.",NEWLINE,"SEE MESSAGES ABOVE FOR FURTHER INFORMATION");                   //Natural: PRINT ( 0 ) *PROGRAM 'JOB TERMINATED! NO' #APPL-TITLE 'CREATED.' / 'SEE MESSAGES ABOVE FOR FURTHER INFORMATION'
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
        //*  PROCESS-ERROR
    }
    private void sub_Format_Date() throws Exception                                                                                                                       //Natural: FORMAT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------
        pnd_Work_Month.setValueEdited(pnd_Work_Date_D,new ReportEditMask("LLLLLLLLLLL"));                                                                                 //Natural: MOVE EDITED #WORK.DATE-D ( EM = L ( 11 ) ) TO #WORK.MONTH
        pnd_Work_Day_Temp.setValueEdited(pnd_Work_Date_D,new ReportEditMask("ZD"));                                                                                       //Natural: MOVE EDITED #WORK.DATE-D ( EM = ZD ) TO #WORK.DAY-TEMP
        pnd_Work_Day.setValue(pnd_Work_Day_Temp, MoveOption.LeftJustified);                                                                                               //Natural: MOVE LEFT #WORK.DAY-TEMP TO #WORK.DAY
        pnd_Work_Ccyy.setValueEdited(pnd_Work_Date_D,new ReportEditMask("YYYY"));                                                                                         //Natural: MOVE EDITED #WORK.DATE-D ( EM = YYYY ) TO #WORK.CCYY
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Month));                                                                //Natural: COMPRESS #WORK.MONTH INTO #WORK.FORMATTED-DATE LEAVING NO
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(pnd_Work_Formatted_Date, pnd_Work_Ccyy));                                                                       //Natural: COMPRESS #WORK.FORMATTED-DATE #WORK.CCYY INTO #WORK.FORMATTED-DATE
        //*  FORMAT-DATE
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
    }
}
