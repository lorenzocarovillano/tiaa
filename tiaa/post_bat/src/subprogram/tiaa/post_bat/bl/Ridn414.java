/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:07:17 AM
**        * FROM NATURAL SUBPROGRAM : Ridn414
************************************************************
**        * FILE NAME            : Ridn414.java
**        * CLASS NAME           : Ridn414
**        * INSTANCE NAME        : Ridn414
************************************************************
************************************************************************
* PROGRAM  : RIDN414
* AUTHOR   : JOANNES AVE
* SYSTEM   : RIDER MAILINGS
* TITLE    : TIAA ACCESS ENDORSEMENT
* FUNCTION : THIS MODULE GENERATES A FLAT FILE FOR A BATCH OF
*          : REQUESTS TO POST. THE OUTPUT IS USED AS INPUT TO
*          : ECS EXSTREAM DIALOGUE COMPOSITION SOFTWARE.
* DETAILS  : THIS MODULE READS A FILE FROM RIDP412 AND REFORMATS THE
*          : WORK FILE AS INPUT TO ECS. THE COMPOSITION SOFTWARE
*          : CREATES A POSTSCRIPT FILE ROUTED TO A XEROX PRINTER.
*          :
*          : POST PACKAGE ID:
*          : RDR2009D, RDR2009E, RDR2009F, RDR2009G, RDR2009H, RDR2009I,
*          : RDR2009J, RDR2009K, RDR2009L, RDR2009M, RDR2009N, RDR2009O,
*          : RDR2009P, RDR2009Q
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
* J.AVE    12/09/06 INITIAL IMPLEMENTATION
* J.AVE    10/06/09 ECS CONVERSION
* J.AVE    04/13/11 ADD E-DELIVERY CODES (RDR2009Q)
* L.SHU    09/10/17 PIN EXPANSION PROJECT                     PINEXP
*                   RESTOW FOR RIDA414
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridn414 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaRida414 pdaRida414;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaRidl554 ldaRidl554;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Pst_Tbl_Data_Field2;

    private DbsGroup pnd_Pst_Tbl_Data_Field2__R_Field_1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4;
    private DbsField pnd_Appl_Title;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Date_D;
    private DbsField pnd_Work_Month;
    private DbsField pnd_Work_Day;
    private DbsField pnd_Work_Ccyy;
    private DbsField pnd_Work_Next_Day_Date;
    private DbsField pnd_Work_Formatted_Date;
    private DbsField pnd_Work_Day_Temp;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_K;
    private DbsField pnd_Contract_Combo;
    private DbsField pnd_Pst_Tbl_Data_Field1;

    private DbsGroup pnd_Pst_Tbl_Data_Field1__R_Field_2;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title4;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaRidl554 = new LdaRidl554();
        registerRecord(ldaRidl554);

        // parameters
        parameters = new DbsRecord();
        pdaRida414 = new PdaRida414(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pnd_Pst_Tbl_Data_Field2 = parameters.newFieldInRecord("pnd_Pst_Tbl_Data_Field2", "#PST-TBL-DATA-FIELD2", FieldType.STRING, 253);
        pnd_Pst_Tbl_Data_Field2.setParameterOption(ParameterOption.ByReference);

        pnd_Pst_Tbl_Data_Field2__R_Field_1 = parameters.newGroupInRecord("pnd_Pst_Tbl_Data_Field2__R_Field_1", "REDEFINE", pnd_Pst_Tbl_Data_Field2);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory", "#MM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1", "#MM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2", "#MM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3", "#MM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4", "#MM-TITLE4", 
            FieldType.STRING, 50);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Appl_Title = localVariables.newFieldInRecord("pnd_Appl_Title", "#APPL-TITLE", FieldType.STRING, 15);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Date_D = pnd_Work.newFieldInGroup("pnd_Work_Date_D", "DATE-D", FieldType.DATE);
        pnd_Work_Month = pnd_Work.newFieldInGroup("pnd_Work_Month", "MONTH", FieldType.STRING, 11);
        pnd_Work_Day = pnd_Work.newFieldInGroup("pnd_Work_Day", "DAY", FieldType.STRING, 2);
        pnd_Work_Ccyy = pnd_Work.newFieldInGroup("pnd_Work_Ccyy", "CCYY", FieldType.STRING, 4);
        pnd_Work_Next_Day_Date = pnd_Work.newFieldInGroup("pnd_Work_Next_Day_Date", "NEXT-DAY-DATE", FieldType.DATE);
        pnd_Work_Formatted_Date = pnd_Work.newFieldInGroup("pnd_Work_Formatted_Date", "FORMATTED-DATE", FieldType.STRING, 19);
        pnd_Work_Day_Temp = pnd_Work.newFieldInGroup("pnd_Work_Day_Temp", "DAY-TEMP", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_Contract_Combo = localVariables.newFieldInRecord("pnd_Contract_Combo", "#CONTRACT-COMBO", FieldType.STRING, 60);
        pnd_Pst_Tbl_Data_Field1 = localVariables.newFieldInRecord("pnd_Pst_Tbl_Data_Field1", "#PST-TBL-DATA-FIELD1", FieldType.STRING, 253);

        pnd_Pst_Tbl_Data_Field1__R_Field_2 = localVariables.newGroupInRecord("pnd_Pst_Tbl_Data_Field1__R_Field_2", "REDEFINE", pnd_Pst_Tbl_Data_Field1);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Signatory = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Signatory", "#WM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title1 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title1", "#WM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title2 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title2", "#WM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title3 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title3", "#WM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title4 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title4", "#WM-TITLE4", 
            FieldType.STRING, 50);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaRidl554.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Appl_Title.setInitialValue("RIDER MAILINGS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Ridn414() throws Exception
    {
        super("Ridn414");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDN414", onError);
        setupReports();
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
        pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: RESET MSG-INFO-SUB
        pnd_Work_Date_D.setValue(Global.getDATX());                                                                                                                       //Natural: ASSIGN DATE-D := *DATX
        //*  PERFORM GET-SIGNATORY-TITLE
        pnd_Contract_Combo.reset();                                                                                                                                       //Natural: RESET #CONTRACT-COMBO
        pnd_Contract_Combo.setValue("ENDR-");                                                                                                                             //Natural: MOVE 'ENDR-' TO #CONTRACT-COMBO
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-ST NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RAST:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RAST:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Rp().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-RP NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RARP:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RARP:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-ST NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRST:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRST:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Rp().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-RP NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRRP:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRRP:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-ST NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRST:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRST:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-ST NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSST:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSST:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-NJ NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RANJ:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RANJ:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-NJ NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRNJ:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRNJ:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NJ NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRNJ:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRNJ:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-NJ NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSNJ:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSNJ:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-PA NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RAPA:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RAPA:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-PA NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRPA:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRPA:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-PA NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRPA:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRPA:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-PA NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSPA:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSPA:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-CT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RACT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RACT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-CT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRCT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRCT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-CT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRCT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRCT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-CT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSCT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSCT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-TX NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RATX:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RATX:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-TX NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRTX:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRTX:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-TX NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRTX:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRTX:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-TX NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSTX:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSTX:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-VA NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RAVA:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RAVA:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-VA NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRVA:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRVA:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-VA NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRVA:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRVA:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-VA NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSVA:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSVA:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Mi().notEquals(getZero()) || pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Tn().notEquals(getZero())                //Natural: IF #CTR-GS-MI NE 0 OR #CTR-GS-TN NE 0 OR #CTR-GS-KY NE 0
            || pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ky().notEquals(getZero())))
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSMI:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSMI:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-MO NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RAMO:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RAMO:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-MO NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRMO:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRMO:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-MO NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRMO:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRMO:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-MO NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSMO:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSMO:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-FL NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RAFL:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RAFL:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-FL NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRFL:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRFL:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-FL NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRFL:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRFL:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-FL NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSFL:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSFL:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-OK NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RAOK:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RAOK:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-OK NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SROK:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SROK:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-OK NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GROK:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GROK:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-OK NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSOK:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSOK:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-OR NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RAOR:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RAOR:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-OR NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SROR:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SROR:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-OR NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GROR:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GROR:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-OR NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSOR:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSOR:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-WI NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RAWI:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RAWI:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-WI NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRWI:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRWI:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-WI NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRWI:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRWI:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-WI NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSWI:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSWI:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-SC NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RASC:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RASC:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-SC NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRSC:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRSC:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-SC NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRSC:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRSC:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-SC NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSSC:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSSC:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-NE NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RANE:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RANE:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-NE NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRNE:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRNE:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NE NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRNE:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRNE:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-NE NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSNE:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSNE:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ar().notEquals(getZero()) || pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Hi().notEquals(getZero())))              //Natural: IF #CTR-GS-AR NE 0 OR #CTR-GS-HI NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSAR:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSAR:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-MT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RAMT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RAMT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-MT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRMT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRMT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-MT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRMT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRMT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-MT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSMT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSMT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Nc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NC NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRNC:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRNC:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ut().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-UT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RAUT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RAUT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ut().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-UT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRUT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRUT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Vt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-VT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRVT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRVT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Vt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-VT NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSVT:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSVT:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Pr().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-PR NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RAPR:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RAPR:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Pr().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-PR NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRPR:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRPR:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-NH NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "RANH:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'RANH:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-NH NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "SRNH:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'SRNH:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NH NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRNH:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRNH:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-NH NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSNH:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSNH:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Njabp().notEquals(getZero())))                                                                               //Natural: IF #CTR-GR-NJABP NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GRNA:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GRNA:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Suny().notEquals(getZero())))                                                                                //Natural: IF #CTR-GS-SUNY NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "GSSU:"));                                                    //Natural: COMPRESS #CONTRACT-COMBO 'GSSU:' INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        //*  MAIL ITEM
                                                                                                                                                                          //Natural: PERFORM CREATE-MI-RECORD
        sub_Create_Mi_Record();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Package_Code().notEquals("RDR2009Q")))                                                                              //Natural: IF #PACKAGE-CODE <> 'RDR2009Q'
        {
            //*  ADDRESS PAGE
                                                                                                                                                                          //Natural: PERFORM CREATE-AP-RECORD
            sub_Create_Ap_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  RIDER MAIL COVER LETTER
                                                                                                                                                                          //Natural: PERFORM CREATE-RM-RECORD
        sub_Create_Rm_Record();
        if (condition(Global.isEscape())) {return;}
        //*  STAPLING INDICATOR
                                                                                                                                                                          //Natural: PERFORM CREATE-SP-RECORD
        sub_Create_Sp_Record();
        if (condition(Global.isEscape())) {return;}
        //*  CONTRACT DETAILS
                                                                                                                                                                          //Natural: PERFORM CREATE-CD-RECORD
        sub_Create_Cd_Record();
        if (condition(Global.isEscape())) {return;}
        //*  PERFORM CREATE-EP-RECORD       /* EXTRACT PDF
        //*  -- START -- 04/13/11 J.AVE
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Package_Code().notEquals("RDR2009Q")))                                                                              //Natural: IF #PACKAGE-CODE <> 'RDR2009Q'
        {
            //*  EXTRACT PDF
                                                                                                                                                                          //Natural: PERFORM CREATE-EP-RECORD
            sub_Create_Ep_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ASSIGN LINK FOR E-MAIL
                                                                                                                                                                          //Natural: PERFORM CREATE-LK-RECORD
            sub_Create_Lk_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  -- END -- 04/13/11 J.AVE
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-MI-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-RM-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-SP-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-AP-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CD-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-EP-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-LK-RECORD
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ECS-OUT-DATA
        //*  -------------------------------------
        //*  DEFINE SUBROUTINE GET-SIGNATORY-TITLE
        //*  -------------------------------------
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'WM-SIGNATORY-TITLE'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD1 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'MM-SIGNATORY-TITLE'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD2 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*  END-SUBROUTINE /* GET-SIGNATORY-TITLE
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //*  -----------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DATE
        //*  ---------------------------------------
        //*  DEFINE SUBROUTINE SETUP-POSTNET-BARCODE
        //*  ---------------------------------------
        //*    PSTA9532.ADDRSS-PSTL-DTA := #POST-DATA.#POSTAL-DATA
        //*    CALLNAT 'PSTN9532' PSTA9532   /* CONVERT POSTAL DATA TO CORRECT
        //*    MSG-INFO-SUB                /* POSTNET BARCODE FORMAT
        //*  END-SUBROUTINE  /* SETUP-POSTNET-BARCDOE
    }
    private void sub_Create_Mi_Record() throws Exception                                                                                                                  //Natural: CREATE-MI-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl554.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl554.getEcs_Record_Rec_Id().setValue("MI");                                                                                                                 //Natural: MOVE 'MI' TO ECS-RECORD.REC-ID
        ldaRidl554.getEcs_Record_Pin_Nbr().setValue(pdaRida414.getPnd_Post_Data_Pnd_Pin_Number());                                                                        //Natural: MOVE #POST-DATA.#PIN-NUMBER TO ECS-RECORD.PIN-NBR
        ldaRidl554.getEcs_Record_Pst_Rqst_Id().setValue(pdaRida414.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                                   //Natural: MOVE #POST-DATA.#PST-RQST-ID TO ECS-RECORD.PST-RQST-ID
        //*    MOVE #POST-DATA.#FULL-NAME             TO ECS-RECORD.FULL-NME
        //*    MOVE #POST-DATA.#ADDRESS-TYPE-CDE      TO ECS-RECORD.ADDRSS-TYP-CDE
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(1)   TO ECS-RECORD.ADDRSS-LINE-1
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(2)   TO ECS-RECORD.ADDRSS-LINE-2
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(3)   TO ECS-RECORD.ADDRSS-LINE-3
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(4)   TO ECS-RECORD.ADDRSS-LINE-4
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(5)   TO ECS-RECORD.ADDRSS-LINE-5
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(6)   TO ECS-RECORD.ADDRSS-LINE-6
        //*    MOVE #POST-DATA.#EMAIL-ADDRESS         TO ECS-RECORD.ALT-DLVRY-ADDR
        //*  -- START -- 04/13/11 J.AVE
        //*    IF #PACKAGE-CODE = 'RDR2009Q'
        //*      MOVE 'EML'                           TO ECS-RECORD.PCKGE-DLVRY-TYP
        //*    END-IF
        //*  -- END -- 04/13/11 J.AVE
        //*    PERFORM SETUP-POSTNET-BARCODE
        //*    EXAMINE PSTA9532.FULL-POSTNET-BAR-CDE 'B' REPLACE WITH '*'
        //*    EXAMINE PSTA9532.FULL-POSTNET-BAR-CDE 'E' REPLACE WITH '*'
        //*    MOVE PSTA9532.FULL-POSTNET-BAR-CDE     TO ECS-RECORD.POSTNET-BARCODE
        ldaRidl554.getEcs_Record_Pckge_Cde().setValue(pdaRida414.getPnd_Post_Data_Pnd_Package_Code());                                                                    //Natural: MOVE #POST-DATA.#PACKAGE-CODE TO ECS-RECORD.PCKGE-CDE
        ldaRidl554.getEcs_Record_Letter_Dte().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO ECS-RECORD.LETTER-DTE
        //*    IF #POST-DATA.#ADDRESS-TYPE-CDE NE 'U'       /* OTHER THAN DOMESTIC
        //*      MOVE '8' TO ECS-RECORD.BYTE2
        //*    ELSE
        //*      MOVE '0' TO ECS-RECORD.BYTE2
        //*    END-IF
        ldaRidl554.getEcs_Record_Byte4().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE4
        ldaRidl554.getEcs_Record_Byte5().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE5
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-MI-RECORD
    }
    //*  JCA ITEMS ADDED 04/13/2011
    private void sub_Create_Rm_Record() throws Exception                                                                                                                  //Natural: CREATE-RM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl554.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl554.getEcs_Record_Rm_Record_Type().setValue("RM");                                                                                                         //Natural: MOVE 'RM' TO ECS-RECORD.RM-RECORD-TYPE
        //*  NON-RESPONDERS
        short decideConditionsMet825 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #LETTER-TYPE;//Natural: VALUE '7'
        if (condition((pdaRida414.getPnd_Post_Data_Pnd_Letter_Type().equals("7"))))
        {
            decideConditionsMet825++;
            //*  JCA
            if (condition(pdaRida414.getPnd_Post_Data_Pnd_Package_Code().equals("RDR2009Q")))                                                                             //Natural: IF #PACKAGE-CODE = 'RDR2009Q'
            {
                //*  JCA
                ldaRidl554.getEcs_Record_Rm_Cover_Letter_Code().setValue("ACCNEMM");                                                                                      //Natural: MOVE 'ACCNEMM' TO ECS-RECORD.RM-COVER-LETTER-CODE
                //*  JCA
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaRidl554.getEcs_Record_Rm_Cover_Letter_Code().setValue("ACCNOMM");                                                                                      //Natural: MOVE 'ACCNOMM' TO ECS-RECORD.RM-COVER-LETTER-CODE
                //*  JCA
                //*  RESPONDERS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE '9'
        else if (condition((pdaRida414.getPnd_Post_Data_Pnd_Letter_Type().equals("9"))))
        {
            decideConditionsMet825++;
            //*  JCA
            if (condition(pdaRida414.getPnd_Post_Data_Pnd_Package_Code().equals("RDR2009Q")))                                                                             //Natural: IF #PACKAGE-CODE = 'RDR2009Q'
            {
                //*  JCA
                ldaRidl554.getEcs_Record_Rm_Cover_Letter_Code().setValue("ACCREMM");                                                                                      //Natural: MOVE 'ACCREMM' TO ECS-RECORD.RM-COVER-LETTER-CODE
                //*  JCA
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaRidl554.getEcs_Record_Rm_Cover_Letter_Code().setValue("ACCRSMM");                                                                                      //Natural: MOVE 'ACCRSMM' TO ECS-RECORD.RM-COVER-LETTER-CODE
                //*  JCA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaRidl554.getEcs_Record_Rm_Signatory_Name().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory);                                                                  //Natural: MOVE #MM-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
        ldaRidl554.getEcs_Record_Rm_Signatory_Title1().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1);                                                                   //Natural: MOVE #MM-TITLE1 TO ECS-RECORD.RM-SIGNATORY-TITLE1
        ldaRidl554.getEcs_Record_Rm_Signatory_Title2().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2);                                                                   //Natural: MOVE #MM-TITLE2 TO ECS-RECORD.RM-SIGNATORY-TITLE2
        ldaRidl554.getEcs_Record_Rm_Signatory_Title3().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3);                                                                   //Natural: MOVE #MM-TITLE3 TO ECS-RECORD.RM-SIGNATORY-TITLE3
        ldaRidl554.getEcs_Record_Rm_Signatory_Title4().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4);                                                                   //Natural: MOVE #MM-TITLE4 TO ECS-RECORD.RM-SIGNATORY-TITLE4
        ldaRidl554.getEcs_Record_Rm_Institution_Name().setValue(pdaRida414.getPnd_Post_Data_Pnd_Institution_Name());                                                      //Natural: MOVE #POST-DATA.#INSTITUTION-NAME TO ECS-RECORD.RM-INSTITUTION-NAME
        ldaRidl554.getEcs_Record_Rm_Access_Lvl().setValue(pdaRida414.getPnd_Post_Data_Pnd_Level());                                                                       //Natural: MOVE #LEVEL TO ECS-RECORD.RM-ACCESS-LVL
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-RM-RECORD
    }
    private void sub_Create_Sp_Record() throws Exception                                                                                                                  //Natural: CREATE-SP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl554.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl554.getEcs_Record_Sp_Record_Type().setValue("SP");                                                                                                         //Natural: MOVE 'SP' TO ECS-RECORD.SP-RECORD-TYPE
        ldaRidl554.getEcs_Record_Sp_Stapling_Ind().setValue("Y");                                                                                                         //Natural: MOVE 'Y' TO ECS-RECORD.SP-STAPLING-IND
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-AP-RECORD
    }
    private void sub_Create_Ap_Record() throws Exception                                                                                                                  //Natural: CREATE-AP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl554.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl554.getEcs_Record_Ap_Record_Type().setValue("AP");                                                                                                         //Natural: MOVE 'AP' TO ECS-RECORD.AP-RECORD-TYPE
        ldaRidl554.getEcs_Record_Ap_Cont_Num().setValue(pnd_Contract_Combo);                                                                                              //Natural: MOVE #CONTRACT-COMBO TO ECS-RECORD.AP-CONT-NUM
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-AP-RECORD
    }
    private void sub_Create_Cd_Record() throws Exception                                                                                                                  //Natural: CREATE-CD-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl554.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl554.getEcs_Record_Cd_Record_Type().setValue("CD");                                                                                                         //Natural: MOVE 'CD' TO ECS-RECORD.CD-RECORD-TYPE
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaRida414.getPnd_Post_Data_Pnd_Contracts().getValue(pnd_I).notEquals(" ")))                                                                    //Natural: IF #CONTRACTS ( #I ) NE ' '
            {
                ldaRidl554.getEcs_Record_Cd_Contract_Number().setValue(pdaRida414.getPnd_Post_Data_Pnd_Contracts().getValue(pnd_I));                                      //Natural: MOVE #CONTRACTS ( #I ) TO ECS-RECORD.CD-CONTRACT-NUMBER
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CREATE-CD-RECORD
    }
    private void sub_Create_Ep_Record() throws Exception                                                                                                                  //Natural: CREATE-EP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl554.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl554.getEcs_Record_Ep_Record_Type().setValue("ES");                                                                                                         //Natural: MOVE 'ES' TO ECS-RECORD.EP-RECORD-TYPE
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-ST NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-ST
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));         //Natural: COMPRESS 'END-1000.24-ACC Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Rp().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-RP NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-RP
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-ROP Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));     //Natural: COMPRESS 'END-1000.24-ACC-ROP Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-ST NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-ST
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));          //Natural: COMPRESS 'END-1200.8-ACC Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Rp().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-RP NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-RP
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-ROP Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1200.8-ACC-ROP Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-ST NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-ST
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));         //Natural: COMPRESS 'END-G1000.6-ACC Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-ST NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-ST
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));         //Natural: COMPRESS 'END-G1250.1-ACC Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-NJ NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-NJ
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-NJ Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1000.24-ACC-NJ Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-NJ NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-NJ
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-NJ Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));       //Natural: COMPRESS 'END-1200.8-ACC-NJ Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NJ NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-NJ
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-NJ Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-NJ Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-NJ NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-NJ
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-UL18-NJ Level", pdaRida414.getPnd_Post_Data_Pnd_Level(),                    //Natural: COMPRESS 'END-G1250.1-ACC-UL18-NJ Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                "for Marketing"));
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-PA NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-PA
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-PA Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1000.24-ACC-PA Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-PA NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-PA
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-PA Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));       //Natural: COMPRESS 'END-1200.8-ACC-PA Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-PA NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-PA
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-PA Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-PA Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-PA NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-PA
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-PA Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-PA Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-CT NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-CT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-CT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1000.24-ACC-CT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-CT NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-CT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-CT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));       //Natural: COMPRESS 'END-1200.8-ACC-CT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-CT NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-CT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-CT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-CT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-CT NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-CT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-CT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-CT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-TX NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-TX
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-TX Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1000.24-ACC-TX Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-TX NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-TX
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-TX Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));       //Natural: COMPRESS 'END-1200.8-ACC-TX Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-TX NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-TX
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-TX Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-TX Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-TX NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-TX
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-TX Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-TX Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-VA NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-VA
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-VA Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1000.24-ACC-VA Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-VA NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-VA
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-VA Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));       //Natural: COMPRESS 'END-1200.8-ACC-VA Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-VA NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-VA
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-VA Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-VA Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-VA NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-VA
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-VA Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-VA Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Mi().notEquals(getZero()) || pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Tn().notEquals(getZero())                //Natural: IF #CTR-GS-MI NE 0 OR #CTR-GS-TN NE 0 OR #CTR-GS-KY NE 0
            || pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ky().notEquals(getZero())))
        {
            //*      FOR #I = 1 TO #CTR-GS-MI
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-UL18 Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));    //Natural: COMPRESS 'END-G1250.1-ACC-UL18 Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-MO NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-MO
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-ROP-MO Level", pdaRida414.getPnd_Post_Data_Pnd_Level(),                     //Natural: COMPRESS 'END-1000.24-ACC-ROP-MO Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                "for Marketing"));
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-MO NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-MO
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-ROP-MO Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));   //Natural: COMPRESS 'END-1200.8-ACC-ROP-MO Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-MO NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-MO
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-MO Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-MO Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-MO NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-MO
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-MO Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-MO Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-FL NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-FL
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-FL Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1000.24-ACC-FL Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-FL NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-FL
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-FL Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));       //Natural: COMPRESS 'END-1200.8-ACC-FL Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-FL NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-FL
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-FL Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-FL Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-FL NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-FL
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-FL Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-FL Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-OK NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-OK
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-ROP-OK Level", pdaRida414.getPnd_Post_Data_Pnd_Level(),                     //Natural: COMPRESS 'END-1000.24-ACC-ROP-OK Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                "for Marketing"));
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-OK NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-OK
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-ROP-OK Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));   //Natural: COMPRESS 'END-1200.8-ACC-ROP-OK Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-OK NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-OK
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-OK Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-OK Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-OK NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-OK
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-OK Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-OK Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-OR NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-OR
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-OR Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1000.24-ACC-OR Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-OR NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-OR
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-OR Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));       //Natural: COMPRESS 'END-1200.8-ACC-OR Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-OR NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-OR
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.7-ACC-OR Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.7-ACC-OR Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-OR NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-OR
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-OR Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-OR Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-WI NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-WI
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-WI Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1000.24-ACC-WI Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-WI NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-WI
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-WI Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));       //Natural: COMPRESS 'END-1200.8-ACC-WI Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-WI NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-WI
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-WI Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-WI Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-WI NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-WI
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-WI Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-WI Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-SC NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-SC
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-ROP-SC Level", pdaRida414.getPnd_Post_Data_Pnd_Level(),                     //Natural: COMPRESS 'END-1000.24-ACC-ROP-SC Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                "for Marketing"));
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-SC NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-SC
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-ROP-SC Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));   //Natural: COMPRESS 'END-1200.8-ACC-ROP-SC Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-SC NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-SC
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-SC Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-SC Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-SC NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-SC
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-SC Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-SC Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-NE NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-NE
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-ROP-NE Level", pdaRida414.getPnd_Post_Data_Pnd_Level(),                     //Natural: COMPRESS 'END-1000.24-ACC-ROP-NE Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                "for Marketing"));
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-NE NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-NE
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-ROP-NE Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));   //Natural: COMPRESS 'END-1200.8-ACC-ROP-NE Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NE NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-NE
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-NE Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-NE Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-NE NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-NE
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-NE Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-NE Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ar().notEquals(getZero()) || pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Hi().notEquals(getZero())))              //Natural: IF #CTR-GS-AR NE 0 OR #CTR-GS-HI NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-AR
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-AR Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-AR Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-MT NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-MT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-MT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1000.24-ACC-MT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-MT NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-MT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-MT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));       //Natural: COMPRESS 'END-1200.8-ACC-MT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-MT NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-MT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-MT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-MT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-MT NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-MT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-MT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-MT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Nc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NC NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-NC
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-NC Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-NC Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ut().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-UT NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-UT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-ROP-UT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(),                     //Natural: COMPRESS 'END-1000.24-ACC-ROP-UT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                "for Marketing"));
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ut().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-UT NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-UT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-ROP-UT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));   //Natural: COMPRESS 'END-1200.8-ACC-ROP-UT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Vt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-VT NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-VT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-VT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));       //Natural: COMPRESS 'END-1200.8-ACC-VT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Vt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-VT NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-VT
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-VT Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-VT Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Pr().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-PR NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-PR
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-PR Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1000.24-ACC-PR Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Pr().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-PR NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-PR
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.7-ACC-PR Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.7-ACC-PR Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-NH NE 0
        {
            //*      FOR #I = 1 TO #CTR-RA-NH
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1000.24-ACC-NH Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-1000.24-ACC-NH Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-NH NE 0
        {
            //*      FOR #I = 1 TO #CTR-SR-NH
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-1200.8-ACC-NH Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));       //Natural: COMPRESS 'END-1200.8-ACC-NH Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NH NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-NH
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.6-ACC-NH Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1000.6-ACC-NH Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-NH NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-NH
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1250.1-ACC-NH Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));      //Natural: COMPRESS 'END-G1250.1-ACC-NH Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Njabp().notEquals(getZero())))                                                                               //Natural: IF #CTR-GR-NJABP NE 0
        {
            //*      FOR #I = 1 TO #CTR-GR-NJABP
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1000.7-ACC Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));         //Natural: COMPRESS 'END-G1000.7-ACC Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Suny().notEquals(getZero())))                                                                                //Natural: IF #CTR-GS-SUNY NE 0
        {
            //*      FOR #I = 1 TO #CTR-GS-SUNY
            ldaRidl554.getEcs_Record_Ep_Pdf_Name().setValue(DbsUtil.compress("END-G1251.1-ACC Level", pdaRida414.getPnd_Post_Data_Pnd_Level(), "for Marketing"));         //Natural: COMPRESS 'END-G1251.1-ACC Level' #LEVEL 'for Marketing' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
            //*      END-FOR
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-EP-RECORD
    }
    //*  JCA ADDED 04/13/2011
    private void sub_Create_Lk_Record() throws Exception                                                                                                                  //Natural: CREATE-LK-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl554.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl554.getEcs_Record_Lk_Record_Type().setValue("LK");                                                                                                         //Natural: MOVE 'LK' TO ECS-RECORD.LK-RECORD-TYPE
        ldaRidl554.getEcs_Record_Lk_Access_Lvl().setValue(pdaRida414.getPnd_Post_Data_Pnd_Level());                                                                       //Natural: MOVE #LEVEL TO ECS-RECORD.LK-ACCESS-LVL
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-ST NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RAST");                                                                                                      //Natural: MOVE 'RAST' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Rp().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-RP NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RARP");                                                                                                      //Natural: MOVE 'RARP' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-ST NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRST");                                                                                                      //Natural: MOVE 'SRST' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Rp().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-RP NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRRP");                                                                                                      //Natural: MOVE 'SRRP' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-ST NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRST");                                                                                                      //Natural: MOVE 'GRST' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_St().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-ST NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSST");                                                                                                      //Natural: MOVE 'GSST' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-NJ NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RANJ");                                                                                                      //Natural: MOVE 'RANJ' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-NJ NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRNJ");                                                                                                      //Natural: MOVE 'SRNJ' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NJ NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRNJ");                                                                                                      //Natural: MOVE 'GRNJ' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Nj().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-NJ NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSNJ");                                                                                                      //Natural: MOVE 'GSNJ' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-PA NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RAPA");                                                                                                      //Natural: MOVE 'RAPA' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-PA NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRPA");                                                                                                      //Natural: MOVE 'SRPA' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-PA NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRPA");                                                                                                      //Natural: MOVE 'GRPA' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Pa().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-PA NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSPA");                                                                                                      //Natural: MOVE 'GSPA' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-CT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RACT");                                                                                                      //Natural: MOVE 'RACT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-CT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRCT");                                                                                                      //Natural: MOVE 'SRCT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-CT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRCT");                                                                                                      //Natural: MOVE 'GRCT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ct().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-CT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSCT");                                                                                                      //Natural: MOVE 'GSCT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-TX NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RATX");                                                                                                      //Natural: MOVE 'RATX' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-TX NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRTX");                                                                                                      //Natural: MOVE 'SRTX' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-TX NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRTX");                                                                                                      //Natural: MOVE 'GRTX' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Tx().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-TX NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSTX");                                                                                                      //Natural: MOVE 'GSTX' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-VA NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RAVA");                                                                                                      //Natural: MOVE 'RAVA' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-VA NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRVA");                                                                                                      //Natural: MOVE 'SRVA' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-VA NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRVA");                                                                                                      //Natural: MOVE 'GRVA' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Va().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-VA NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSVA");                                                                                                      //Natural: MOVE 'GSVA' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Mi().notEquals(getZero()) || pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Tn().notEquals(getZero())                //Natural: IF #CTR-GS-MI NE 0 OR #CTR-GS-TN NE 0 OR #CTR-GS-KY NE 0
            || pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ky().notEquals(getZero())))
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSKY");                                                                                                      //Natural: MOVE 'GSKY' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-MO NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RAMO");                                                                                                      //Natural: MOVE 'RAMO' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-MO NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRMO");                                                                                                      //Natural: MOVE 'SRMO' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-MO NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRMO");                                                                                                      //Natural: MOVE 'GRMO' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Mo().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-MO NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSMO");                                                                                                      //Natural: MOVE 'GSMO' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-FL NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RAFL");                                                                                                      //Natural: MOVE 'RAFL' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-FL NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRFL");                                                                                                      //Natural: MOVE 'SRFL' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-FL NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRFL");                                                                                                      //Natural: MOVE 'GRFL' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Fl().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-FL NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSFL");                                                                                                      //Natural: MOVE 'GSFL' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-OK NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RAOK");                                                                                                      //Natural: MOVE 'RAOK' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-OK NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SROK");                                                                                                      //Natural: MOVE 'SROK' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-OK NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GROK");                                                                                                      //Natural: MOVE 'GROK' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ok().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-OK NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSOK");                                                                                                      //Natural: MOVE 'GSOK' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-OR NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RAOR");                                                                                                      //Natural: MOVE 'RAOR' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-OR NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SROR");                                                                                                      //Natural: MOVE 'SROR' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-OR NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GROR");                                                                                                      //Natural: MOVE 'GROR' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Or().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-OR NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSOR");                                                                                                      //Natural: MOVE 'GSOR' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-WI NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RAWI");                                                                                                      //Natural: MOVE 'RAWI' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-WI NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRWI");                                                                                                      //Natural: MOVE 'SRWI' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-WI NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRWI");                                                                                                      //Natural: MOVE 'GRWI' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Wi().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-WI NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSWI");                                                                                                      //Natural: MOVE 'GSWI' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-SC NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RASC");                                                                                                      //Natural: MOVE 'RASC' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-SC NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRSC");                                                                                                      //Natural: MOVE 'SRSC' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-SC NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRSC");                                                                                                      //Natural: MOVE 'GRSC' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Sc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-SC NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSSC");                                                                                                      //Natural: MOVE 'GSSC' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-NE NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RANE");                                                                                                      //Natural: MOVE 'RANE' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-NE NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRNE");                                                                                                      //Natural: MOVE 'SRNE' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NE NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRNE");                                                                                                      //Natural: MOVE 'GRNE' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ne().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-NE NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSNE");                                                                                                      //Natural: MOVE 'GSNE' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Ar().notEquals(getZero()) || pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Hi().notEquals(getZero())))              //Natural: IF #CTR-GS-AR NE 0 OR #CTR-GS-HI NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSAR");                                                                                                      //Natural: MOVE 'GSAR' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-MT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RAMT");                                                                                                      //Natural: MOVE 'RAMT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-MT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRMT");                                                                                                      //Natural: MOVE 'SRMT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-MT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRMT");                                                                                                      //Natural: MOVE 'GRMT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Mt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-MT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSMT");                                                                                                      //Natural: MOVE 'GSMT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Nc().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NC NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRNC");                                                                                                      //Natural: MOVE 'GRNC' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Ut().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-UT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RAUT");                                                                                                      //Natural: MOVE 'RAUT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Ut().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-UT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRUT");                                                                                                      //Natural: MOVE 'SRUT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Vt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-VT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRVT");                                                                                                      //Natural: MOVE 'SRVT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Vt().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-VT NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSVT");                                                                                                      //Natural: MOVE 'GSVT' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Pr().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-PR NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RAPR");                                                                                                      //Natural: MOVE 'RAPR' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Pr().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-PR NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRPR");                                                                                                      //Natural: MOVE 'GRPR' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Ra_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-RA-NH NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("RANH");                                                                                                      //Natural: MOVE 'RANH' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Sr_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-SR-NH NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("SRNH");                                                                                                      //Natural: MOVE 'SRNH' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GR-NH NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRNH");                                                                                                      //Natural: MOVE 'GRNH' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Nh().notEquals(getZero())))                                                                                  //Natural: IF #CTR-GS-NH NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSNH");                                                                                                      //Natural: MOVE 'GSNH' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gr_Njabp().notEquals(getZero())))                                                                               //Natural: IF #CTR-GR-NJABP NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GRNA");                                                                                                      //Natural: MOVE 'GRNA' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida414.getPnd_Post_Data_Pnd_Ctr_Gs_Suny().notEquals(getZero())))                                                                                //Natural: IF #CTR-GS-SUNY NE 0
        {
            ldaRidl554.getEcs_Record_Lk_Url_Link().setValue("GSSU");                                                                                                      //Natural: MOVE 'GSSU' TO ECS-RECORD.LK-URL-LINK
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-LK-RECORD
    }
    private void sub_Write_Ecs_Out_Data() throws Exception                                                                                                                //Natural: WRITE-ECS-OUT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        getWorkFiles().write(2, false, ldaRidl554.getEcs_Record());                                                                                                       //Natural: WRITE WORK FILE 2 ECS-RECORD
        //*  WRITE-ECS-OUT-DATA
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, Global.getPROGRAM(),"IN PROCESS-ERROR","SUBROUTINE",pdaRida414.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                     //Natural: WRITE ( 0 ) *PROGRAM 'IN PROCESS-ERROR' 'SUBROUTINE' #POST-DATA.#PST-RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  PSTA9531.#POST-DATA-STATUS := PSTL9620.GEN-FAILED
        //*  PSTA9531.TERMINATE-IND    := TRUE
        getReports().print(0, Global.getPROGRAM(),"JOB TERMINATED! NO",pnd_Appl_Title,"CREATED.",NEWLINE,"SEE MESSAGES ABOVE FOR FURTHER INFORMATION");                   //Natural: PRINT ( 0 ) *PROGRAM 'JOB TERMINATED! NO' #APPL-TITLE 'CREATED.' / 'SEE MESSAGES ABOVE FOR FURTHER INFORMATION'
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
        //*  PROCESS-ERROR
    }
    private void sub_Format_Date() throws Exception                                                                                                                       //Natural: FORMAT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------
        pnd_Work_Month.setValueEdited(pnd_Work_Date_D,new ReportEditMask("LLLLLLLLLLL"));                                                                                 //Natural: MOVE EDITED #WORK.DATE-D ( EM = L ( 11 ) ) TO #WORK.MONTH
        pnd_Work_Day_Temp.setValueEdited(pnd_Work_Date_D,new ReportEditMask("ZD"));                                                                                       //Natural: MOVE EDITED #WORK.DATE-D ( EM = ZD ) TO #WORK.DAY-TEMP
        pnd_Work_Day.setValue(pnd_Work_Day_Temp, MoveOption.LeftJustified);                                                                                               //Natural: MOVE LEFT #WORK.DAY-TEMP TO #WORK.DAY
        pnd_Work_Ccyy.setValueEdited(pnd_Work_Date_D,new ReportEditMask("YYYY"));                                                                                         //Natural: MOVE EDITED #WORK.DATE-D ( EM = YYYY ) TO #WORK.CCYY
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Month));                                                                //Natural: COMPRESS #WORK.MONTH INTO #WORK.FORMATTED-DATE LEAVING NO
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(pnd_Work_Formatted_Date, pnd_Work_Ccyy));                                                                       //Natural: COMPRESS #WORK.FORMATTED-DATE #WORK.CCYY INTO #WORK.FORMATTED-DATE
        //*  FORMAT-DATE
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
    }
}
