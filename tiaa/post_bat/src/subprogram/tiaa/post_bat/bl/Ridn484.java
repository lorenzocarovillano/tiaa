/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:07:21 AM
**        * FROM NATURAL SUBPROGRAM : Ridn484
************************************************************
**        * FILE NAME            : Ridn484.java
**        * CLASS NAME           : Ridn484
**        * INSTANCE NAME        : Ridn484
************************************************************
************************************************************************
* PROGRAM  : RIDN484
* AUTHOR   : JOANNES AVE
* SYSTEM   : PROJCNY
* TITLE    : 2008 FORCED CASH ENDORSEMENT MAILING
* FUNCTION : THIS MODULE GENERATES XICS STATEMENTS FOR A BATCH OF
*          : REQUESTS TO POST. THE OUTPUT IS USED AS INPUT TO
*          : COMPUST COMPOSITION SOFTWARE.
* CREATED  : MAY 2008.
* DETAILS  : THIS MODULE READS A FILE FROM RIDP482 AND FORMATS THE
*          : WORK FILE AS INPUT TO COMPUSET. THE COMPOSITION SOFTWARE
*          : CREATES PCL TO BE ROUTED TO A HIGH SPEED PRINTER IN MAP.
*          :
*          : POST PACKAGE ID: RDR2008U, RDR2008V
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
* J.AVE    06/06/08 INITIAL IMPLEMENTATION
* J.AVE    01/26/09 ECS CONVERSION
* J.AVE    06/14/10 ADD GSRA TIAA/CREF ENDORSEMENTS
* J.AVE    07/15/16 REMARK PASSING OF NAME AND ADDRESS. CCP WILL OBTAIN
*                   IT FROM MDM AS PART OF DCS ELIMINATION
* L.SHU    05/26/17 PIN EXPANSION - RESTOW FOR RIDA484         PINEXP
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridn484 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaRida484 pdaRida484;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaRidl504 ldaRidl504;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Pst_Tbl_Data_Field2;

    private DbsGroup pnd_Pst_Tbl_Data_Field2__R_Field_1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4;
    private DbsField pnd_Appl_Title;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Date_D;
    private DbsField pnd_Work_Month;
    private DbsField pnd_Work_Day;
    private DbsField pnd_Work_Ccyy;
    private DbsField pnd_Work_Next_Day_Date;
    private DbsField pnd_Work_Formatted_Date;
    private DbsField pnd_Work_Day_Temp;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_K;
    private DbsField pnd_Contract_Combo;
    private DbsField pnd_Pst_Tbl_Data_Field1;

    private DbsGroup pnd_Pst_Tbl_Data_Field1__R_Field_2;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title4;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaRidl504 = new LdaRidl504();
        registerRecord(ldaRidl504);

        // parameters
        parameters = new DbsRecord();
        pdaRida484 = new PdaRida484(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pnd_Pst_Tbl_Data_Field2 = parameters.newFieldInRecord("pnd_Pst_Tbl_Data_Field2", "#PST-TBL-DATA-FIELD2", FieldType.STRING, 253);
        pnd_Pst_Tbl_Data_Field2.setParameterOption(ParameterOption.ByReference);

        pnd_Pst_Tbl_Data_Field2__R_Field_1 = parameters.newGroupInRecord("pnd_Pst_Tbl_Data_Field2__R_Field_1", "REDEFINE", pnd_Pst_Tbl_Data_Field2);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory", "#MM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1", "#MM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2", "#MM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3", "#MM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4", "#MM-TITLE4", 
            FieldType.STRING, 50);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Appl_Title = localVariables.newFieldInRecord("pnd_Appl_Title", "#APPL-TITLE", FieldType.STRING, 15);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Date_D = pnd_Work.newFieldInGroup("pnd_Work_Date_D", "DATE-D", FieldType.DATE);
        pnd_Work_Month = pnd_Work.newFieldInGroup("pnd_Work_Month", "MONTH", FieldType.STRING, 11);
        pnd_Work_Day = pnd_Work.newFieldInGroup("pnd_Work_Day", "DAY", FieldType.STRING, 2);
        pnd_Work_Ccyy = pnd_Work.newFieldInGroup("pnd_Work_Ccyy", "CCYY", FieldType.STRING, 4);
        pnd_Work_Next_Day_Date = pnd_Work.newFieldInGroup("pnd_Work_Next_Day_Date", "NEXT-DAY-DATE", FieldType.DATE);
        pnd_Work_Formatted_Date = pnd_Work.newFieldInGroup("pnd_Work_Formatted_Date", "FORMATTED-DATE", FieldType.STRING, 19);
        pnd_Work_Day_Temp = pnd_Work.newFieldInGroup("pnd_Work_Day_Temp", "DAY-TEMP", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_Contract_Combo = localVariables.newFieldInRecord("pnd_Contract_Combo", "#CONTRACT-COMBO", FieldType.STRING, 60);
        pnd_Pst_Tbl_Data_Field1 = localVariables.newFieldInRecord("pnd_Pst_Tbl_Data_Field1", "#PST-TBL-DATA-FIELD1", FieldType.STRING, 253);

        pnd_Pst_Tbl_Data_Field1__R_Field_2 = localVariables.newGroupInRecord("pnd_Pst_Tbl_Data_Field1__R_Field_2", "REDEFINE", pnd_Pst_Tbl_Data_Field1);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Signatory = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Signatory", "#WM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title1 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title1", "#WM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title2 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title2", "#WM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title3 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title3", "#WM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title4 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title4", "#WM-TITLE4", 
            FieldType.STRING, 50);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaRidl504.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Appl_Title.setInitialValue("RIDER MAILINGS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Ridn484() throws Exception
    {
        super("Ridn484");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDN484", onError);
        setupReports();
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
        pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: RESET MSG-INFO-SUB
        pnd_Work_Date_D.setValue(Global.getDATX());                                                                                                                       //Natural: ASSIGN DATE-D := *DATX
        //*  PERFORM GET-SIGNATORY-TITLE
        pnd_Contract_Combo.reset();                                                                                                                                       //Natural: RESET #CONTRACT-COMBO
        pnd_Contract_Combo.setValue("CTR");                                                                                                                               //Natural: MOVE 'CTR' TO #CONTRACT-COMBO
        if (condition(pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gra_Tiaa().notEquals(getZero())))                                                                               //Natural: IF #CTR-GRA-TIAA NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "-GRT:", pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gra_Tiaa()));    //Natural: COMPRESS #CONTRACT-COMBO '-GRT:' #CTR-GRA-TIAA INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gra_Cref().notEquals(getZero())))                                                                               //Natural: IF #CTR-GRA-CREF NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "-GRC:", pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gra_Cref()));    //Natural: COMPRESS #CONTRACT-COMBO '-GRC:' #CTR-GRA-CREF INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gsra_Tiaa().notEquals(getZero())))                                                                              //Natural: IF #CTR-GSRA-TIAA NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "-GST:", pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gsra_Tiaa()));   //Natural: COMPRESS #CONTRACT-COMBO '-GST:' #CTR-GSRA-TIAA INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gsra_Cref().notEquals(getZero())))                                                                              //Natural: IF #CTR-GSRA-CREF NE 0
        {
            pnd_Contract_Combo.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Contract_Combo, "-GSC:", pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gsra_Cref()));   //Natural: COMPRESS #CONTRACT-COMBO '-GSC:' #CTR-GSRA-CREF INTO #CONTRACT-COMBO LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        //*  MAIL ITEM
                                                                                                                                                                          //Natural: PERFORM CREATE-MI-RECORD
        sub_Create_Mi_Record();
        if (condition(Global.isEscape())) {return;}
        //*  RIDER MAIL COVER LETTER
                                                                                                                                                                          //Natural: PERFORM CREATE-RM-RECORD
        sub_Create_Rm_Record();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaRida484.getPnd_Post_Data_Pnd_Package_Code().equals("RDR2008V")))                                                                                 //Natural: IF #PACKAGE-CODE EQ 'RDR2008V'
        {
            //*  ADDRESS PAGE
                                                                                                                                                                          //Natural: PERFORM CREATE-AP-RECORD
            sub_Create_Ap_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CONTRACT DETAILS
                                                                                                                                                                          //Natural: PERFORM CREATE-CD-RECORD
        sub_Create_Cd_Record();
        if (condition(Global.isEscape())) {return;}
        //*  EXTRACT PDF
                                                                                                                                                                          //Natural: PERFORM CREATE-EP-RECORD
        sub_Create_Ep_Record();
        if (condition(Global.isEscape())) {return;}
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-MI-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-RM-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-AP-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CD-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-EP-RECORD
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ECS-OUT-DATA
        //*  -------------------------------------
        //*  DEFINE SUBROUTINE GET-SIGNATORY-TITLE
        //*  -------------------------------------
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'WM-SIGNATORY-TITLE'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD1 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'MM-SIGNATORY-TITLE'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD2 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*  END-SUBROUTINE /* GET-SIGNATORY-TITLE
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //*  -----------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DATE
        //*  ---------------------------------------
        //*  DEFINE SUBROUTINE SETUP-POSTNET-BARCODE
        //*  ---------------------------------------
        //*    PSTA9532.ADDRSS-PSTL-DTA := #POST-DATA.#POSTAL-DATA
        //*    CALLNAT 'PSTN9532' PSTA9532   /* CONVERT POSTAL DATA TO CORRECT
        //*    MSG-INFO-SUB                /* POSTNET BARCODE FORMAT
        //*  END-SUBROUTINE  /* SETUP-POSTNET-BARCDOE
    }
    private void sub_Create_Mi_Record() throws Exception                                                                                                                  //Natural: CREATE-MI-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Rec_Id().setValue("MI");                                                                                                                 //Natural: MOVE 'MI' TO ECS-RECORD.REC-ID
        ldaRidl504.getEcs_Record_Pin_Nbr().setValue(pdaRida484.getPnd_Post_Data_Pnd_Pin_Number());                                                                        //Natural: MOVE #POST-DATA.#PIN-NUMBER TO ECS-RECORD.PIN-NBR
        ldaRidl504.getEcs_Record_Pst_Rqst_Id().setValue(pdaRida484.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                                   //Natural: MOVE #POST-DATA.#PST-RQST-ID TO ECS-RECORD.PST-RQST-ID
        //*    MOVE #POST-DATA.#FULL-NAME             TO ECS-RECORD.FULL-NME
        //*    MOVE #POST-DATA.#ADDRESS-TYPE-CDE      TO ECS-RECORD.ADDRSS-TYP-CDE
        //*    IF PSTA9670.ENVIRONMENT NE 'PROD'    /* SCRAMBLE ADDRESS LINES
        //*      CALLNAT 'PSTN9990' USING #POST-DATA.#ADDRESS-LINE-TXT(*)
        //*    END-IF
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(1) TO
        //*         ECS-RECORD.ADDRSS-LINE-TXT(1)
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(2) TO
        //*         ECS-RECORD.ADDRSS-LINE-TXT(2)
        //*    #A := 2
        //*    FOR #I = 3 TO 6
        //*      ADD 1 TO #A
        //*      IF #POST-DATA.#ADDRESS-LINE-TXT(#I) NE ' '
        //*        MOVE #POST-DATA.#ADDRESS-LINE-TXT(#I) TO
        //*             ECS-RECORD.ADDRSS-LINE-TXT(#I)
        //*      ELSE
        //*        IF PSTA9670.ENVIRONMENT NE  'PROD'
        //*          MOVE 'TEST   ...DO NOT MAIL...' TO
        //*               ECS-RECORD.ADDRSS-LINE-TXT(#A)
        //*        END-IF
        //*      END-IF
        //*    END-FOR
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(1)   TO ECS-RECORD.ADDRSS-LINE-1
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(2)   TO ECS-RECORD.ADDRSS-LINE-2
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(3)   TO ECS-RECORD.ADDRSS-LINE-3
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(4)   TO ECS-RECORD.ADDRSS-LINE-4
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(5)   TO ECS-RECORD.ADDRSS-LINE-5
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(6)   TO ECS-RECORD.ADDRSS-LINE-6
        //*    PERFORM SETUP-POSTNET-BARCODE
        //*    EXAMINE PSTA9532.FULL-POSTNET-BAR-CDE 'B' REPLACE WITH '*'
        //*    EXAMINE PSTA9532.FULL-POSTNET-BAR-CDE 'E' REPLACE WITH '*'
        //*    MOVE PSTA9532.FULL-POSTNET-BAR-CDE     TO ECS-RECORD.POSTNET-BARCODE
        ldaRidl504.getEcs_Record_Pckge_Cde().setValue(pdaRida484.getPnd_Post_Data_Pnd_Package_Code());                                                                    //Natural: MOVE #POST-DATA.#PACKAGE-CODE TO ECS-RECORD.PCKGE-CDE
        ldaRidl504.getEcs_Record_Letter_Dte().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO ECS-RECORD.LETTER-DTE
        //*    IF #POST-DATA.#ADDRESS-TYPE-CDE NE 'U'  /* OTHER THAN DOMESTIC
        //*      MOVE '8' TO ECS-RECORD.BYTE2
        //*    ELSE
        //*      MOVE '0' TO ECS-RECORD.BYTE2
        //*    END-IF
        ldaRidl504.getEcs_Record_Byte4().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE4
        ldaRidl504.getEcs_Record_Byte5().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE5
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-MI-RECORD
    }
    private void sub_Create_Rm_Record() throws Exception                                                                                                                  //Natural: CREATE-RM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Rm_Record_Type().setValue("RM");                                                                                                         //Natural: MOVE 'RM' TO ECS-RECORD.RM-RECORD-TYPE
        ldaRidl504.getEcs_Record_Rm_Institution_Name().setValue(pdaRida484.getPnd_Post_Data_Pnd_Institution_Name());                                                      //Natural: MOVE #INSTITUTION-NAME TO ECS-RECORD.RM-INSTITUTION-NAME
        //*    IF #LETTER-TYPE EQ '1'
        ldaRidl504.getEcs_Record_Rm_Cover_Letter_Code().setValue("FCASHMM");                                                                                              //Natural: MOVE 'FCASHMM' TO ECS-RECORD.RM-COVER-LETTER-CODE
        ldaRidl504.getEcs_Record_Rm_Signatory_Name().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory);                                                                  //Natural: MOVE #MM-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
        ldaRidl504.getEcs_Record_Rm_Signatory_Title1().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1);                                                                   //Natural: MOVE #MM-TITLE1 TO ECS-RECORD.RM-SIGNATORY-TITLE1
        ldaRidl504.getEcs_Record_Rm_Signatory_Title2().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2);                                                                   //Natural: MOVE #MM-TITLE2 TO ECS-RECORD.RM-SIGNATORY-TITLE2
        ldaRidl504.getEcs_Record_Rm_Signatory_Title3().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3);                                                                   //Natural: MOVE #MM-TITLE3 TO ECS-RECORD.RM-SIGNATORY-TITLE3
        ldaRidl504.getEcs_Record_Rm_Signatory_Title4().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4);                                                                   //Natural: MOVE #MM-TITLE4 TO ECS-RECORD.RM-SIGNATORY-TITLE4
        //*    ELSE
        //*      MOVE 'FCASHWM'     TO ECS-RECORD.RM-COVER-LETTER-CODE
        //*      MOVE #WM-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
        //*      MOVE #WM-TITLE1    TO ECS-RECORD.RM-SIGNATORY-TITLE1
        //*      MOVE #WM-TITLE2    TO ECS-RECORD.RM-SIGNATORY-TITLE2
        //*      MOVE #WM-TITLE3    TO ECS-RECORD.RM-SIGNATORY-TITLE3
        //*      MOVE #WM-TITLE4    TO ECS-RECORD.RM-SIGNATORY-TITLE4
        //*    END-IF
        if (condition(pdaRida484.getPnd_Post_Data_Pnd_Package_Code().equals("RDR2008U")))                                                                                 //Natural: IF #PACKAGE-CODE EQ 'RDR2008U'
        {
            ldaRidl504.getEcs_Record_Rm_Cont_Num().setValue(pnd_Contract_Combo);                                                                                          //Natural: MOVE #CONTRACT-COMBO TO ECS-RECORD.RM-CONT-NUM
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-RM-RECORD
    }
    private void sub_Create_Ap_Record() throws Exception                                                                                                                  //Natural: CREATE-AP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Ap_Record_Type().setValue("AP");                                                                                                         //Natural: MOVE 'AP' TO ECS-RECORD.AP-RECORD-TYPE
        ldaRidl504.getEcs_Record_Ap_Cont_Num().setValue(pnd_Contract_Combo);                                                                                              //Natural: MOVE #CONTRACT-COMBO TO ECS-RECORD.AP-CONT-NUM
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-AP-RECORD
    }
    private void sub_Create_Cd_Record() throws Exception                                                                                                                  //Natural: CREATE-CD-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Cd_Record_Type().setValue("CD");                                                                                                         //Natural: MOVE 'CD' TO ECS-RECORD.CD-RECORD-TYPE
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaRida484.getPnd_Post_Data_Pnd_Tiaa_Contracts().getValue(pnd_I).notEquals(" ")))                                                               //Natural: IF #TIAA-CONTRACTS ( #I ) NE ' '
            {
                ldaRidl504.getEcs_Record_Cd_Contract_Number().setValue(DbsUtil.compress(pdaRida484.getPnd_Post_Data_Pnd_Tiaa_Contracts().getValue(pnd_I)));               //Natural: COMPRESS #TIAA-CONTRACTS ( #I ) INTO ECS-RECORD.CD-CONTRACT-NUMBER
                if (condition(pdaRida484.getPnd_Post_Data_Pnd_Cref_Contracts().getValue(pnd_I).notEquals(" ")))                                                           //Natural: IF #CREF-CONTRACTS ( #I ) NE ' '
                {
                    ldaRidl504.getEcs_Record_Cd_Contract_Number().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaRidl504.getEcs_Record_Cd_Contract_Number(), //Natural: COMPRESS ECS-RECORD.CD-CONTRACT-NUMBER '/' #CREF-CONTRACTS ( #I ) INTO ECS-RECORD.CD-CONTRACT-NUMBER LEAVING NO
                        "/", pdaRida484.getPnd_Post_Data_Pnd_Cref_Contracts().getValue(pnd_I)));
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CREATE-CD-RECORD
    }
    private void sub_Create_Ep_Record() throws Exception                                                                                                                  //Natural: CREATE-EP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Ep_Record_Type().setValue("EP");                                                                                                         //Natural: MOVE 'EP' TO ECS-RECORD.EP-RECORD-TYPE
        if (condition(pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gra_Tiaa().greater(getZero())))                                                                                 //Natural: IF #CTR-GRA-TIAA GT 0
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_G912_GRA");                                                                                              //Natural: MOVE 'RDR_G912_GRA' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gra_Cref().greater(getZero())))                                                                                 //Natural: IF #CTR-GRA-CREF GT 0
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CG912_GRA");                                                                                             //Natural: MOVE 'RDR_CG912_GRA' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gsra_Tiaa().greater(getZero())))                                                                                //Natural: IF #CTR-GSRA-TIAA GT 0
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TIAA_GSRA_FORCED_CASHOUT");                                                                              //Natural: MOVE 'RDR_TIAA_GSRA_FORCED_CASHOUT' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida484.getPnd_Post_Data_Pnd_Ctr_Gsra_Cref().greater(getZero())))                                                                                //Natural: IF #CTR-GSRA-CREF GT 0
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CREF_GSRA_FORCED_CASHOUT");                                                                              //Natural: MOVE 'RDR_CREF_GSRA_FORCED_CASHOUT' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-EP-RECORD
    }
    private void sub_Write_Ecs_Out_Data() throws Exception                                                                                                                //Natural: WRITE-ECS-OUT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        getWorkFiles().write(2, false, ldaRidl504.getEcs_Record());                                                                                                       //Natural: WRITE WORK FILE 2 ECS-RECORD
        //*  WRITE-ECS-OUT-DATA
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, Global.getPROGRAM(),"IN PROCESS-ERROR","SUBROUTINE",pdaRida484.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                     //Natural: WRITE ( 0 ) *PROGRAM 'IN PROCESS-ERROR' 'SUBROUTINE' #POST-DATA.#PST-RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  PSTA9531.#POST-DATA-STATUS := PSTL9620.GEN-FAILED
        //*  PSTA9531.TERMINATE-IND    := TRUE
        getReports().print(0, Global.getPROGRAM(),"JOB TERMINATED! NO",pnd_Appl_Title,"CREATED.",NEWLINE,"SEE MESSAGES ABOVE FOR FURTHER INFORMATION");                   //Natural: PRINT ( 0 ) *PROGRAM 'JOB TERMINATED! NO' #APPL-TITLE 'CREATED.' / 'SEE MESSAGES ABOVE FOR FURTHER INFORMATION'
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
        //*  PROCESS-ERROR
    }
    private void sub_Format_Date() throws Exception                                                                                                                       //Natural: FORMAT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------
        pnd_Work_Month.setValueEdited(pnd_Work_Date_D,new ReportEditMask("LLLLLLLLLLL"));                                                                                 //Natural: MOVE EDITED #WORK.DATE-D ( EM = L ( 11 ) ) TO #WORK.MONTH
        pnd_Work_Day_Temp.setValueEdited(pnd_Work_Date_D,new ReportEditMask("ZD"));                                                                                       //Natural: MOVE EDITED #WORK.DATE-D ( EM = ZD ) TO #WORK.DAY-TEMP
        pnd_Work_Day.setValue(pnd_Work_Day_Temp, MoveOption.LeftJustified);                                                                                               //Natural: MOVE LEFT #WORK.DAY-TEMP TO #WORK.DAY
        pnd_Work_Ccyy.setValueEdited(pnd_Work_Date_D,new ReportEditMask("YYYY"));                                                                                         //Natural: MOVE EDITED #WORK.DATE-D ( EM = YYYY ) TO #WORK.CCYY
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Month));                                                                //Natural: COMPRESS #WORK.MONTH INTO #WORK.FORMATTED-DATE LEAVING NO
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(pnd_Work_Formatted_Date, pnd_Work_Ccyy));                                                                       //Natural: COMPRESS #WORK.FORMATTED-DATE #WORK.CCYY INTO #WORK.FORMATTED-DATE
        //*  FORMAT-DATE
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
    }
}
