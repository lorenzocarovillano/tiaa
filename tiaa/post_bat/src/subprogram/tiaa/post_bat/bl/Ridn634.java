/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:07:25 AM
**        * FROM NATURAL SUBPROGRAM : Ridn634
************************************************************
**        * FILE NAME            : Ridn634.java
**        * CLASS NAME           : Ridn634
**        * INSTANCE NAME        : Ridn634
************************************************************
************************************************************************
* PROGRAM  : RIDN634
* AUTHOR   : JOANNES AVE
* SYSTEM   : PROJCNY
* TITLE    : 2016 CUSTOM PORTFOLIO ENDORSEMENT MAILING
* FUNCTION : THIS MODULE GENERATES DATA FOR CONVERSION TO XML AS INPUT
*          : TO DCS DIALOGUE TO GENERATE THE ENDORSEMENTS.
* CREATED  : NOV 2016.
* DETAILS  : THIS MODULE READS A FILE FROM RIDP632 AND FORMATS THE
*          : WORK FILE AS INPUT TO DCS.
*          :
*          : POST PACKAGE ID: RDR2016A, RDR2016B
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
* J.AVE    11/04/15 INITIAL CODING
* J.AVE    07/18/16 REMARK PASSING OF NAME AND ADDRESS. CCP WILL HANDLE
*                   THIS AS PART OF DCS ELIMINATION
* L.SHU    05/26/17 PIN EXPANSION - RESTOW FOR RIDA634         PINEXP
* L.SHU    05/03/19 RETIREPLUS WITH TSV MODIFICATION            CPTSV
*                   ADDING AK, FL AND NY FOR STATE VARIATION
* L.SHU    09/06/19 REMOVE LETTER CODE = 1, NOT IN USE PRE-2019 CPTSV2
*                   RETIREPLUS PRO WITH TSV MODIFICATION
*                   ADDING AK, FL AND NY FOR STATE VARIATION
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridn634 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaRida634 pdaRida634;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaRidl634 ldaRidl634;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Pst_Tbl_Data_Field2;

    private DbsGroup pnd_Pst_Tbl_Data_Field2__R_Field_1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4;
    private DbsField pnd_Appl_Title;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Date_D;
    private DbsField pnd_Work_Month;
    private DbsField pnd_Work_Day;
    private DbsField pnd_Work_Ccyy;
    private DbsField pnd_Work_Next_Day_Date;
    private DbsField pnd_Work_Formatted_Date;
    private DbsField pnd_Work_Day_Temp;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_K;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaRidl634 = new LdaRidl634();
        registerRecord(ldaRidl634);

        // parameters
        parameters = new DbsRecord();
        pdaRida634 = new PdaRida634(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pnd_Pst_Tbl_Data_Field2 = parameters.newFieldInRecord("pnd_Pst_Tbl_Data_Field2", "#PST-TBL-DATA-FIELD2", FieldType.STRING, 253);
        pnd_Pst_Tbl_Data_Field2.setParameterOption(ParameterOption.ByReference);

        pnd_Pst_Tbl_Data_Field2__R_Field_1 = parameters.newGroupInRecord("pnd_Pst_Tbl_Data_Field2__R_Field_1", "REDEFINE", pnd_Pst_Tbl_Data_Field2);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory", "#MM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1", "#MM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2", "#MM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3", "#MM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4", "#MM-TITLE4", 
            FieldType.STRING, 50);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Appl_Title = localVariables.newFieldInRecord("pnd_Appl_Title", "#APPL-TITLE", FieldType.STRING, 15);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Date_D = pnd_Work.newFieldInGroup("pnd_Work_Date_D", "DATE-D", FieldType.DATE);
        pnd_Work_Month = pnd_Work.newFieldInGroup("pnd_Work_Month", "MONTH", FieldType.STRING, 11);
        pnd_Work_Day = pnd_Work.newFieldInGroup("pnd_Work_Day", "DAY", FieldType.STRING, 2);
        pnd_Work_Ccyy = pnd_Work.newFieldInGroup("pnd_Work_Ccyy", "CCYY", FieldType.STRING, 4);
        pnd_Work_Next_Day_Date = pnd_Work.newFieldInGroup("pnd_Work_Next_Day_Date", "NEXT-DAY-DATE", FieldType.DATE);
        pnd_Work_Formatted_Date = pnd_Work.newFieldInGroup("pnd_Work_Formatted_Date", "FORMATTED-DATE", FieldType.STRING, 19);
        pnd_Work_Day_Temp = pnd_Work.newFieldInGroup("pnd_Work_Day_Temp", "DAY-TEMP", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaRidl634.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Appl_Title.setInitialValue("RIDER MAILINGS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Ridn634() throws Exception
    {
        super("Ridn634");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDN634", onError);
        setupReports();
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
        pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: RESET MSG-INFO-SUB
        pnd_Work_Date_D.setValue(Global.getDATX());                                                                                                                       //Natural: ASSIGN DATE-D := *DATX
        //*  MAIL ITEM
                                                                                                                                                                          //Natural: PERFORM CREATE-MI-RECORD
        sub_Create_Mi_Record();
        if (condition(Global.isEscape())) {return;}
        //*  RIDER MAIL COVER LETTER
                                                                                                                                                                          //Natural: PERFORM CREATE-RM-RECORD
        sub_Create_Rm_Record();
        if (condition(Global.isEscape())) {return;}
        //*  CONTRACT DETAILS
                                                                                                                                                                          //Natural: PERFORM CREATE-CD-RECORD
        sub_Create_Cd_Record();
        if (condition(Global.isEscape())) {return;}
        //*  PERFORM CREATE-EP-RECORD         /* EXTERNAL PDF ENDORSEMENT
        //*  DIALOGUE TEMPLATE ENDORSEMENT
                                                                                                                                                                          //Natural: PERFORM CREATE-EN-RECORD
        sub_Create_En_Record();
        if (condition(Global.isEscape())) {return;}
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-MI-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-RM-RECORD
        //*       VALUE '1'                   NO LONGER IN USE
        //*          MOVE 'CUSTPFMM' TO ECS-RECORD.RM-COVER-LETTER-CODE
        //*  ----------------------------------
        //*  DEFINE SUBROUTINE CREATE-AP-RECORD   /* NOT REFERENCED
        //*  ----------------------------------
        //*    RESET ECS-OUT-DATA(*)
        //*    MOVE 'AP'            TO ECS-RECORD.AP-RECORD-TYPE
        //*    MOVE #CONTRACT-COMBO TO ECS-RECORD.AP-CONT-NUM
        //*    PERFORM WRITE-ECS-OUT-DATA
        //*  END-SUBROUTINE /* CREATE-AP-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CD-RECORD
        //*  ----------------------------------
        //*  DEFINE SUBROUTINE CREATE-EP-RECORD
        //*  ----------------------------------
        //*    RESET ECS-OUT-DATA(*)
        //*    MOVE 'EP'            TO ECS-RECORD.EP-RECORD-TYPE
        //*    IF #CTR-RC-RCP-GN GT 0
        //*      FOR #K = 1 TO #CTR-RC-RCP-GN
        //*        IF #TIAA-CONTRACTS-GN(#K) NE ' '
        //*          MOVE 'RDR_IGRS-CRT-CPMS-E1' TO ECS-RECORD.EP-PDF-NAME
        //*          PERFORM WRITE-ECS-OUT-DATA
        //*        END-IF
        //*        IF #CREF-CONTRACTS-GN(#K) NE ' '
        //*          MOVE 'RDR_CIGRS-CRT-CPMS-E1' TO ECS-RECORD.EP-PDF-NAME
        //*          PERFORM WRITE-ECS-OUT-DATA
        //*        END-IF
        //*      END-FOR
        //*    END-IF
        //*  END-SUBROUTINE /* CREATE-EP-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-EN-RECORD
        //*      WHEN #LETTER-TYPE EQ '1'
        //*        IF #CTR-RC-RCP-GN GT 0
        //*            FOR #K = 1 TO #CTR-RC-RCP-GN
        //*              IF #TIAA-CONTRACTS-GN(#K) NE ' '
        //*                MOVE 'CPTIAAGN' TO ECS-RECORD.EN-ENDORSE-TYPE
        //*                PERFORM WRITE-ECS-OUT-DATA
        //*              END-IF
        //*              IF #CREF-CONTRACTS-GN(#K) NE ' '
        //*                MOVE 'CPCREFGN' TO ECS-RECORD.EN-ENDORSE-TYPE
        //*                PERFORM WRITE-ECS-OUT-DATA
        //*              END-IF
        //*            END-FOR
        //*        END-IF
        //*        IF #CTR-RC-RCP-IL  GT 0
        //*            FOR #K = 1 TO #CTR-RC-RCP-IL
        //*              IF #TIAA-CONTRACTS-IL(#K) NE ' '
        //*                MOVE 'CPTIAAIL' TO ECS-RECORD.EN-ENDORSE-TYPE
        //*                MOVE #TIAA-CONTRACTS-IL(#K) TO
        //*                                   ECS-RECORD.EN-CONTRACT-NUMBER
        //*                PERFORM WRITE-ECS-OUT-DATA
        //*              END-IF
        //*              IF #CREF-CONTRACTS-IL(#K) NE ' '
        //*                MOVE 'CPCREFIL' TO ECS-RECORD.EN-ENDORSE-TYPE
        //*                MOVE #CREF-CONTRACTS-IL(#K) TO
        //*                                   ECS-RECORD.EN-CONTRACT-NUMBER
        //*                PERFORM WRITE-ECS-OUT-DATA
        //*              END-IF
        //*            END-FOR
        //*        END-IF
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ECS-OUT-DATA
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //*  -----------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DATE
    }
    private void sub_Create_Mi_Record() throws Exception                                                                                                                  //Natural: CREATE-MI-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_Rec_Id().setValue("MI");                                                                                                                 //Natural: MOVE 'MI' TO ECS-RECORD.REC-ID
        ldaRidl634.getEcs_Record_Pin_Nbr().setValue(pdaRida634.getPnd_Post_Data_Pnd_Pin_Number());                                                                        //Natural: MOVE #POST-DATA.#PIN-NUMBER TO ECS-RECORD.PIN-NBR
        ldaRidl634.getEcs_Record_Pst_Rqst_Id().setValue(pdaRida634.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                                   //Natural: MOVE #POST-DATA.#PST-RQST-ID TO ECS-RECORD.PST-RQST-ID
        ldaRidl634.getEcs_Record_Pckge_Cde().setValue(pdaRida634.getPnd_Post_Data_Pnd_Package_Code());                                                                    //Natural: MOVE #POST-DATA.#PACKAGE-CODE TO ECS-RECORD.PCKGE-CDE
        ldaRidl634.getEcs_Record_Letter_Dte().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO ECS-RECORD.LETTER-DTE
        ldaRidl634.getEcs_Record_Byte4().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE4
        ldaRidl634.getEcs_Record_Byte5().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE5
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-MI-RECORD
    }
    private void sub_Create_Rm_Record() throws Exception                                                                                                                  //Natural: CREATE-RM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_Rm_Record_Type().setValue("RM");                                                                                                         //Natural: MOVE 'RM' TO ECS-RECORD.RM-RECORD-TYPE
        ldaRidl634.getEcs_Record_Rm_Institution_Name().setValue(pdaRida634.getPnd_Post_Data_Pnd_Institution_Name());                                                      //Natural: MOVE #INSTITUTION-NAME TO ECS-RECORD.RM-INSTITUTION-NAME
        //*    IF #LETTER-TYPE EQ '1'                             /* CPTSV >>>
        //*      MOVE 'CUSTPFMM'    TO ECS-RECORD.RM-COVER-LETTER-CODE
        //*      MOVE #MM-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
        //*      MOVE #MM-TITLE1    TO ECS-RECORD.RM-SIGNATORY-TITLE1
        //*      MOVE #MM-TITLE2    TO ECS-RECORD.RM-SIGNATORY-TITLE2
        //*      MOVE #MM-TITLE3    TO ECS-RECORD.RM-SIGNATORY-TITLE3
        //*      MOVE #MM-TITLE4    TO ECS-RECORD.RM-SIGNATORY-TITLE4
        //*    END-IF
        short decideConditionsMet443 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #LETTER-TYPE;//Natural: VALUE '2'
        if (condition((pdaRida634.getPnd_Post_Data_Pnd_Letter_Type().equals("2"))))
        {
            decideConditionsMet443++;
            ldaRidl634.getEcs_Record_Rm_Cover_Letter_Code().setValue("CUSTBULK");                                                                                         //Natural: MOVE 'CUSTBULK' TO ECS-RECORD.RM-COVER-LETTER-CODE
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((pdaRida634.getPnd_Post_Data_Pnd_Letter_Type().equals("3"))))
        {
            decideConditionsMet443++;
            ldaRidl634.getEcs_Record_Rm_Cover_Letter_Code().setValue("CUSTE2");                                                                                           //Natural: MOVE 'CUSTE2' TO ECS-RECORD.RM-COVER-LETTER-CODE
        }                                                                                                                                                                 //Natural: VALUE '4'
        else if (condition((pdaRida634.getPnd_Post_Data_Pnd_Letter_Type().equals("4"))))
        {
            decideConditionsMet443++;
            ldaRidl634.getEcs_Record_Rm_Cover_Letter_Code().setValue("CUSTE2SV");                                                                                         //Natural: MOVE 'CUSTE2SV' TO ECS-RECORD.RM-COVER-LETTER-CODE
        }                                                                                                                                                                 //Natural: VALUE '5'
        else if (condition((pdaRida634.getPnd_Post_Data_Pnd_Letter_Type().equals("5"))))
        {
            decideConditionsMet443++;
            ldaRidl634.getEcs_Record_Rm_Cover_Letter_Code().setValue("PLUSE3");                                                                                           //Natural: MOVE 'PLUSE3' TO ECS-RECORD.RM-COVER-LETTER-CODE
        }                                                                                                                                                                 //Natural: VALUE '6'
        else if (condition((pdaRida634.getPnd_Post_Data_Pnd_Letter_Type().equals("6"))))
        {
            decideConditionsMet443++;
            ldaRidl634.getEcs_Record_Rm_Cover_Letter_Code().setValue("PLUSE3SV");                                                                                         //Natural: MOVE 'PLUSE3SV' TO ECS-RECORD.RM-COVER-LETTER-CODE
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet443 > 0))
        {
            ldaRidl634.getEcs_Record_Rm_Signatory_Name().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory);                                                              //Natural: MOVE #MM-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
            ldaRidl634.getEcs_Record_Rm_Signatory_Title1().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1);                                                               //Natural: MOVE #MM-TITLE1 TO ECS-RECORD.RM-SIGNATORY-TITLE1
            ldaRidl634.getEcs_Record_Rm_Signatory_Title2().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2);                                                               //Natural: MOVE #MM-TITLE2 TO ECS-RECORD.RM-SIGNATORY-TITLE2
            ldaRidl634.getEcs_Record_Rm_Signatory_Title3().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3);                                                               //Natural: MOVE #MM-TITLE3 TO ECS-RECORD.RM-SIGNATORY-TITLE3
            ldaRidl634.getEcs_Record_Rm_Signatory_Title4().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4);                                                               //Natural: MOVE #MM-TITLE4 TO ECS-RECORD.RM-SIGNATORY-TITLE4
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
            //*  CPTSV  <<<
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-RM-RECORD
    }
    private void sub_Create_Cd_Record() throws Exception                                                                                                                  //Natural: CREATE-CD-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_Cd_Record_Type().setValue("CD");                                                                                                         //Natural: MOVE 'CD' TO ECS-RECORD.CD-RECORD-TYPE
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 10
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
        {
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tiaa_Contracts().getValue(pnd_I).notEquals(" ")))                                                               //Natural: IF #TIAA-CONTRACTS ( #I ) NE ' '
            {
                ldaRidl634.getEcs_Record_Cd_Contract_Number().setValue(DbsUtil.compress(pdaRida634.getPnd_Post_Data_Pnd_Tiaa_Contracts().getValue(pnd_I)));               //Natural: COMPRESS #TIAA-CONTRACTS ( #I ) INTO ECS-RECORD.CD-CONTRACT-NUMBER
                if (condition(pdaRida634.getPnd_Post_Data_Pnd_Cref_Contracts().getValue(pnd_I).notEquals(" ")))                                                           //Natural: IF #CREF-CONTRACTS ( #I ) NE ' '
                {
                    ldaRidl634.getEcs_Record_Cd_Contract_Number().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaRidl634.getEcs_Record_Cd_Contract_Number(), //Natural: COMPRESS ECS-RECORD.CD-CONTRACT-NUMBER '/' #CREF-CONTRACTS ( #I ) INTO ECS-RECORD.CD-CONTRACT-NUMBER LEAVING NO
                        "/", pdaRida634.getPnd_Post_Data_Pnd_Cref_Contracts().getValue(pnd_I)));
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CREATE-CD-RECORD
    }
    private void sub_Create_En_Record() throws Exception                                                                                                                  //Natural: CREATE-EN-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_En_Record_Type().setValue("EN");                                                                                                         //Natural: MOVE 'EN' TO ECS-RECORD.EN-RECORD-TYPE
        //*    IF #CTR-RC-RCP-GN GT 0
        //*      FOR #K = 1 TO #CTR-RC-RCP-GN
        //*        IF #TIAA-CONTRACTS-GN(#K) NE ' '
        //*          MOVE 'CPTIAAGN' TO ECS-RECORD.EN-ENDORSE-TYPE
        //*          PERFORM WRITE-ECS-OUT-DATA
        //*        END-IF
        //*        IF #CREF-CONTRACTS-GN(#K) NE ' '
        //*          MOVE 'CPCREFGN' TO ECS-RECORD.EN-ENDORSE-TYPE
        //*          PERFORM WRITE-ECS-OUT-DATA
        //*        END-IF
        //*      END-FOR
        //*    END-IF
        //*    IF #CTR-RC-RCP-IL GT 0
        //*      FOR #K = 1 TO #CTR-RC-RCP-IL
        //*        IF #TIAA-CONTRACTS-IL(#K) NE ' '
        //*          MOVE 'CPTIAAIL' TO ECS-RECORD.EN-ENDORSE-TYPE
        //*          MOVE #TIAA-CONTRACTS-IL(#K) TO ECS-RECORD.EN-CONTRACT-NUMBER
        //*          PERFORM WRITE-ECS-OUT-DATA
        //*        END-IF
        //*        IF #CREF-CONTRACTS-IL(#K) NE ' '
        //*          MOVE 'CPCREFIL' TO ECS-RECORD.EN-ENDORSE-TYPE
        //*          MOVE #CREF-CONTRACTS-IL(#K) TO ECS-RECORD.EN-CONTRACT-NUMBER
        //*          PERFORM WRITE-ECS-OUT-DATA
        //*        END-IF
        //*      END-FOR
        //*    END-IF
        short decideConditionsMet520 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( #LETTER-TYPE EQ '2' OR #LETTER-TYPE EQ '3' )
        if (condition(pdaRida634.getPnd_Post_Data_Pnd_Letter_Type().equals("2") || pdaRida634.getPnd_Post_Data_Pnd_Letter_Type().equals("3")))
        {
            decideConditionsMet520++;
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Gn().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-GN GT 0
            {
                FOR02:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-GN
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Gn())); pnd_K.nadd(1))
                {
                    //*             MOVE 'IGRS-CRT-CPMS-E2'
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTIAGN2");                                                                                      //Natural: MOVE 'CPTIAGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*             MOVE 'CIGRS-CRT-CPMS-E2'
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPCREGN2");                                                                                      //Natural: MOVE 'CPCREGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tsv_Ind().equals("Y")))                                                                                 //Natural: IF #TSV-IND = 'Y'
                    {
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVGN3");                                                                                  //Natural: MOVE 'CPTSVGN3' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  (2660)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ak().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-AK GT 0
            {
                FOR03:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-AK
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ak())); pnd_K.nadd(1))
                {
                    //*             MOVE 'IGRS-CRT-CPMS-E2' TO ECS-RECORD.EN-ENDORSE-TYPE
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTIAGN2");                                                                                      //Natural: MOVE 'CPTIAGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*             MOVE 'CIGRS-CRT-CPMS-E2'
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPCREGN2");                                                                                      //Natural: MOVE 'CPCREGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tsv_Ind().equals("Y")))                                                                                 //Natural: IF #TSV-IND = 'Y'
                    {
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVAK3");                                                                                  //Natural: MOVE 'CPTSVAK3' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  (2800)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Fl().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-FL GT 0
            {
                FOR04:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-FL
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Fl())); pnd_K.nadd(1))
                {
                    //*             MOVE 'IGRS-CRT-CPMS-E2' TO ECS-RECORD.EN-ENDORSE-TYPE
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTIAGN2");                                                                                      //Natural: MOVE 'CPTIAGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*             MOVE 'CIGRS-CRT-CPMS-E2-FL' TO
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPCREFL2");                                                                                      //Natural: MOVE 'CPCREFL2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tsv_Ind().equals("Y")))                                                                                 //Natural: IF #TSV-IND = 'Y'
                    {
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVGN3");                                                                                  //Natural: MOVE 'CPTSVGN3' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  (2940)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ny().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-NY GT 0
            {
                FOR05:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-NY
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ny())); pnd_K.nadd(1))
                {
                    //*             MOVE 'IGRS-CRT-CPMS-E2' TO ECS-RECORD.EN-ENDORSE-TYPE
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTIAGN2");                                                                                      //Natural: MOVE 'CPTIAGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*             MOVE 'CIGRS-CRT-CPMS-E2'
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPCREGN2");                                                                                      //Natural: MOVE 'CPCREGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tsv_Ind().equals("Y")))                                                                                 //Natural: IF #TSV-IND = 'Y'
                    {
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVNY3");                                                                                  //Natural: MOVE 'CPTSVNY3' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  (3080) NY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN ( #LETTER-TYPE EQ '4' AND #TSV-IND = 'S' )
        else if (condition(pdaRida634.getPnd_Post_Data_Pnd_Letter_Type().equals("4") && pdaRida634.getPnd_Post_Data_Pnd_Tsv_Ind().equals("S")))
        {
            decideConditionsMet520++;
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Gn().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-GN GT 0
            {
                FOR06:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-GN
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Gn())); pnd_K.nadd(1))
                {
                    //*  CPTSV2
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVGN3");                                                                                      //Natural: MOVE 'CPTSVGN3' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Fl().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-FL GT 0
            {
                FOR07:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-FL
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Fl())); pnd_K.nadd(1))
                {
                    //*  CPTSV2
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVGN3");                                                                                      //Natural: MOVE 'CPTSVGN3' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ak().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-AK GT 0
            {
                FOR08:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-AK
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ak())); pnd_K.nadd(1))
                {
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tiaa_Contracts_Ak().getValue(pnd_K).notEquals(" ")))                                                    //Natural: IF #TIAA-CONTRACTS-AK ( #K ) NE ' '
                    {
                        //*  CPTSV2
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVAK3");                                                                                  //Natural: MOVE 'CPTSVAK3' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  (3350)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ny().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-NY GT 0
            {
                FOR09:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-NY
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ny())); pnd_K.nadd(1))
                {
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tiaa_Contracts_Ny().getValue(pnd_K).notEquals(" ")))                                                    //Natural: IF #TIAA-CONTRACTS-NY ( #K ) NE ' '
                    {
                        //*  CPTSV2
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVNY3");                                                                                  //Natural: MOVE 'CPTSVNY3' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #LETTER-TYPE EQ '5'
        else if (condition(pdaRida634.getPnd_Post_Data_Pnd_Letter_Type().equals("5")))
        {
            decideConditionsMet520++;
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Gn().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-GN GT 0
            {
                FOR10:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-GN
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Gn())); pnd_K.nadd(1))
                {
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTIAGN2");                                                                                      //Natural: MOVE 'CPTIAGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPCREGN2");                                                                                      //Natural: MOVE 'CPCREGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tsv_Ind().equals("Y")))                                                                                 //Natural: IF #TSV-IND = 'Y'
                    {
                        //*  CPTSV2
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVGN4");                                                                                  //Natural: MOVE 'CPTSVGN4' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  (2660)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ak().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-AK GT 0
            {
                FOR11:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-AK
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ak())); pnd_K.nadd(1))
                {
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTIAGN2");                                                                                      //Natural: MOVE 'CPTIAGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPCREGN2");                                                                                      //Natural: MOVE 'CPCREGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tsv_Ind().equals("Y")))                                                                                 //Natural: IF #TSV-IND = 'Y'
                    {
                        //*  CPTSV2
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVAK4");                                                                                  //Natural: MOVE 'CPTSVAK4' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  (2800)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Fl().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-FL GT 0
            {
                FOR12:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-FL
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Fl())); pnd_K.nadd(1))
                {
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTIAGN2");                                                                                      //Natural: MOVE 'CPTIAGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPCREFL2");                                                                                      //Natural: MOVE 'CPCREFL2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tsv_Ind().equals("Y")))                                                                                 //Natural: IF #TSV-IND = 'Y'
                    {
                        //*  CPTSV2
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVGN4");                                                                                  //Natural: MOVE 'CPTSVGN4' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  (2940)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ny().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-NY GT 0
            {
                FOR13:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-NY
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ny())); pnd_K.nadd(1))
                {
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTIAGN2");                                                                                      //Natural: MOVE 'CPTIAGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPCREGN2");                                                                                      //Natural: MOVE 'CPCREGN2' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tsv_Ind().equals("Y")))                                                                                 //Natural: IF #TSV-IND = 'Y'
                    {
                        //*  CPTSV2
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVNY4");                                                                                  //Natural: MOVE 'CPTSVNY4' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  (3080) NY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN ( #LETTER-TYPE EQ '6' AND #TSV-IND = 'S' )
        else if (condition(pdaRida634.getPnd_Post_Data_Pnd_Letter_Type().equals("6") && pdaRida634.getPnd_Post_Data_Pnd_Tsv_Ind().equals("S")))
        {
            decideConditionsMet520++;
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Gn().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-GN GT 0
            {
                FOR14:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-GN
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Gn())); pnd_K.nadd(1))
                {
                    //*  CPTSV2
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVGN4");                                                                                      //Natural: MOVE 'CPTSVGN4' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Fl().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-FL GT 0
            {
                FOR15:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-FL
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Fl())); pnd_K.nadd(1))
                {
                    //*  CPTSV2
                    ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVGN4");                                                                                      //Natural: MOVE 'CPTSVGN4' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                    sub_Write_Ecs_Out_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ak().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-AK GT 0
            {
                FOR16:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-AK
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ak())); pnd_K.nadd(1))
                {
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tiaa_Contracts_Ak().getValue(pnd_K).notEquals(" ")))                                                    //Natural: IF #TIAA-CONTRACTS-AK ( #K ) NE ' '
                    {
                        //*  CPTSV2
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVAK4");                                                                                  //Natural: MOVE 'CPTSVAK4' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  (3350)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ny().greater(getZero())))                                                                            //Natural: IF #CTR-RC-RCP-NY GT 0
            {
                FOR17:                                                                                                                                                    //Natural: FOR #K = 1 TO #CTR-RC-RCP-NY
                for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pdaRida634.getPnd_Post_Data_Pnd_Ctr_Rc_Rcp_Ny())); pnd_K.nadd(1))
                {
                    if (condition(pdaRida634.getPnd_Post_Data_Pnd_Tiaa_Contracts_Ny().getValue(pnd_K).notEquals(" ")))                                                    //Natural: IF #TIAA-CONTRACTS-NY ( #K ) NE ' '
                    {
                        //*  CPTSV2
                        ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CPTSVNY4");                                                                                  //Natural: MOVE 'CPTSVNY4' TO ECS-RECORD.EN-ENDORSE-TYPE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                        sub_Write_Ecs_Out_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CREATE-EN-RECORD
    }
    private void sub_Write_Ecs_Out_Data() throws Exception                                                                                                                //Natural: WRITE-ECS-OUT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        getWorkFiles().write(2, false, ldaRidl634.getEcs_Record());                                                                                                       //Natural: WRITE WORK FILE 2 ECS-RECORD
        //*  WRITE-ECS-OUT-DATA
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, Global.getPROGRAM(),"IN PROCESS-ERROR","SUBROUTINE",pdaRida634.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                     //Natural: WRITE ( 0 ) *PROGRAM 'IN PROCESS-ERROR' 'SUBROUTINE' #POST-DATA.#PST-RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().print(0, Global.getPROGRAM(),"JOB TERMINATED! NO",pnd_Appl_Title,"CREATED.",NEWLINE,"SEE MESSAGES ABOVE FOR FURTHER INFORMATION");                   //Natural: PRINT ( 0 ) *PROGRAM 'JOB TERMINATED! NO' #APPL-TITLE 'CREATED.' / 'SEE MESSAGES ABOVE FOR FURTHER INFORMATION'
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
        //*  PROCESS-ERROR
    }
    private void sub_Format_Date() throws Exception                                                                                                                       //Natural: FORMAT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------
        pnd_Work_Month.setValueEdited(pnd_Work_Date_D,new ReportEditMask("LLLLLLLLLLL"));                                                                                 //Natural: MOVE EDITED #WORK.DATE-D ( EM = L ( 11 ) ) TO #WORK.MONTH
        pnd_Work_Day_Temp.setValueEdited(pnd_Work_Date_D,new ReportEditMask("ZD"));                                                                                       //Natural: MOVE EDITED #WORK.DATE-D ( EM = ZD ) TO #WORK.DAY-TEMP
        pnd_Work_Day.setValue(pnd_Work_Day_Temp, MoveOption.LeftJustified);                                                                                               //Natural: MOVE LEFT #WORK.DAY-TEMP TO #WORK.DAY
        pnd_Work_Ccyy.setValueEdited(pnd_Work_Date_D,new ReportEditMask("YYYY"));                                                                                         //Natural: MOVE EDITED #WORK.DATE-D ( EM = YYYY ) TO #WORK.CCYY
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Month));                                                                //Natural: COMPRESS #WORK.MONTH INTO #WORK.FORMATTED-DATE LEAVING NO
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(pnd_Work_Formatted_Date, pnd_Work_Ccyy));                                                                       //Natural: COMPRESS #WORK.FORMATTED-DATE #WORK.CCYY INTO #WORK.FORMATTED-DATE
        //*  FORMAT-DATE
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
    }
}
