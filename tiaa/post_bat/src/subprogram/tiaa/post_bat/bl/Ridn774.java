/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:07:29 AM
**        * FROM NATURAL SUBPROGRAM : Ridn774
************************************************************
**        * FILE NAME            : Ridn774.java
**        * CLASS NAME           : Ridn774
**        * INSTANCE NAME        : Ridn774
************************************************************
************************************************************************
* PROGRAM  : RIDN774
* SYSTEM   : RIDER MAILING
* TITLE    : LOAN RA/GRA/SRA ENDORSEMENT MAILING
* FUNCTION : THIS MODULE GENERATES DATA FOR CONVERSION TO XML AS INPUT
*          : TO CCP DIALOGUE TO GENERATE THE ENDORSEMENTS.
* CREATED  : MARCH 2020.
* DETAILS  : THIS MODULE READS A FILE FROM RIDP772 AND FORMATS THE
*          : WORK FILE AS INPUT TO CCP.
*          :
*          : POST PACKAGE ID: RDR2013A
* HISTORY
*
*   WHO      WHEN              WHY
* -------- --------   --------------------------------------------------
* NEWSOM   03/24/2020 NEW
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridn774 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaRidl634 ldaRidl634;

    // Local Variables
    public DbsRecord localVariables;

    // parameters

    private DbsGroup pnd_Post_Data;
    private DbsField pnd_Post_Data_Pnd_Pst_Rqst_Id;
    private DbsField pnd_Post_Data_Pnd_Pin_Number;

    private DbsGroup pnd_Post_Data__R_Field_1;
    private DbsField pnd_Post_Data_Pnd_Pin_N7;
    private DbsField pnd_Post_Data_Pnd_Pin_A5;
    private DbsField pnd_Post_Data_Pnd_Tiaa_Cntrct_Number;
    private DbsField pnd_Post_Data_Pnd_Cref_Cntrct_Number;
    private DbsField pnd_Post_Data_Pnd_Full_Name;
    private DbsField pnd_Post_Data_Pnd_Last_Name;
    private DbsField pnd_Post_Data_Pnd_Address_Line_Txt;
    private DbsField pnd_Post_Data_Pnd_Postal_Data;
    private DbsField pnd_Post_Data_Pnd_Address_Type_Cde;
    private DbsField pnd_Post_Data_Pnd_Package_Code;
    private DbsField pnd_Post_Data_Pnd_Letter_Type;

    private DbsGroup pnd_Post_Data_Pnd_Retirement_Annuity;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Tx;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pr;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Il;
    private DbsField pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pa;
    private DbsField pnd_Post_Data_Pnd_C_Ra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Ar;
    private DbsField pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Pr;
    private DbsField pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Il1;

    private DbsGroup pnd_Post_Data_Pnd_Group_Retirement_Annuity;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Md;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Tx;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Pr;
    private DbsField pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Il;
    private DbsField pnd_Post_Data_Pnd_C_Gra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Tx;
    private DbsField pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Pr;
    private DbsField pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Il;

    private DbsGroup pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity;
    private DbsField pnd_Post_Data_Pnd_T_Sra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Tx;
    private DbsField pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Il;
    private DbsField pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Pa;
    private DbsField pnd_Post_Data_Pnd_C_Sra_Rpl_E1_All_Other;
    private DbsField pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Fl;
    private DbsField pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Il1;
    private DbsField pnd_Post_Data_Pnd_Rider_Date;

    private DbsGroup pnd_Post_Data__R_Field_2;
    private DbsField pnd_Post_Data_Pnd_Rider_Month;
    private DbsField pnd_Post_Data_Pnd_Rider_Year;

    private DbsGroup pnd_Post_Data__R_Field_3;
    private DbsField pnd_Post_Data_Pnd_Rider_Year_A;
    private DbsField pnd_Post_Data_Pnd_Ra_Contracts;
    private DbsField pnd_Post_Data_Pnd_Gra_Contracts;
    private DbsField pnd_Post_Data_Pnd_Sra_Contracts;
    private DbsField pnd_Post_Data_Pnd_Ra_Tiaa_Contracts_Il;
    private DbsField pnd_Post_Data_Pnd_Ra_Cref_Contracts_Il1;
    private DbsField pnd_Post_Data_Pnd_Gra_Tiaa_Contracts_Il;
    private DbsField pnd_Post_Data_Pnd_Gra_Cref_Contracts_Il;
    private DbsField pnd_Post_Data_Pnd_Sra_Tiaa_Contracts_Il;
    private DbsField pnd_Post_Data_Pnd_Sra_Cref_Contracts_Il1;
    private DbsField pnd_Pst_Tbl_Data_Field2;

    private DbsGroup pnd_Pst_Tbl_Data_Field2__R_Field_4;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4;
    private DbsField pnd_Appl_Title;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Date_D;
    private DbsField pnd_Work_Month;
    private DbsField pnd_Work_Day;
    private DbsField pnd_Work_Ccyy;
    private DbsField pnd_Work_Next_Day_Date;
    private DbsField pnd_Work_Formatted_Date;
    private DbsField pnd_Work_Day_Temp;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_K;
    private DbsField pnd_Contract_Combo;
    private DbsField pnd_Multi_Plan_Ctr;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaRidl634 = new LdaRidl634();
        registerRecord(ldaRidl634);

        // parameters
        parameters = new DbsRecord();

        pnd_Post_Data = parameters.newGroupInRecord("pnd_Post_Data", "#POST-DATA");
        pnd_Post_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Post_Data_Pnd_Pst_Rqst_Id = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pst_Rqst_Id", "#PST-RQST-ID", FieldType.STRING, 11);
        pnd_Post_Data_Pnd_Pin_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);

        pnd_Post_Data__R_Field_1 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data__R_Field_1", "REDEFINE", pnd_Post_Data_Pnd_Pin_Number);
        pnd_Post_Data_Pnd_Pin_N7 = pnd_Post_Data__R_Field_1.newFieldInGroup("pnd_Post_Data_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);
        pnd_Post_Data_Pnd_Pin_A5 = pnd_Post_Data__R_Field_1.newFieldInGroup("pnd_Post_Data_Pnd_Pin_A5", "#PIN-A5", FieldType.STRING, 5);
        pnd_Post_Data_Pnd_Tiaa_Cntrct_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Tiaa_Cntrct_Number", "#TIAA-CNTRCT-NUMBER", FieldType.STRING, 
            8);
        pnd_Post_Data_Pnd_Cref_Cntrct_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Cref_Cntrct_Number", "#CREF-CNTRCT-NUMBER", FieldType.STRING, 
            8);
        pnd_Post_Data_Pnd_Full_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Post_Data_Pnd_Last_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 35);
        pnd_Post_Data_Pnd_Address_Line_Txt = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 6));
        pnd_Post_Data_Pnd_Postal_Data = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 32);
        pnd_Post_Data_Pnd_Address_Type_Cde = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Package_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Post_Data_Pnd_Letter_Type = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);

        pnd_Post_Data_Pnd_Retirement_Annuity = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Retirement_Annuity", "#RETIREMENT-ANNUITY");
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_All_Other", "#T-RA-RPL-E1-ALL-OTHER", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Fl", "#T-RA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Tx = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Tx", "#T-RA-RPL-E1-TX", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pr = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pr", "#T-RA-RPL-E1-PR", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Il = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Il", "#T-RA-RPL-E1-IL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pa = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pa", "#T-RA-RPL-E1-PA", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Ra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Ra_Rpl_E1_All_Other", "#C-RA-RPL-E1-ALL-OTHER", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Ar = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Ar", "#C-RA-RPL-E1-AR", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Fl", "#C-RA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Pr = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Pr", "#C-RA-RPL-E1-PR", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Il1 = pnd_Post_Data_Pnd_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Il1", "#C-RA-RPL-E1-IL1", 
            FieldType.NUMERIC, 8);

        pnd_Post_Data_Pnd_Group_Retirement_Annuity = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Group_Retirement_Annuity", "#GROUP-RETIREMENT-ANNUITY");
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_All_Other", 
            "#T-GRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Fl", "#T-GRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Md = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Md", "#T-GRA-RPL-E1-MD", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Tx = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Tx", "#T-GRA-RPL-E1-TX", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Pr = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Pr", "#T-GRA-RPL-E1-PR", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Il = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Il", "#T-GRA-RPL-E1-IL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Gra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Gra_Rpl_E1_All_Other", 
            "#C-GRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Fl", "#C-GRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Tx = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Tx", "#C-GRA-RPL-E1-TX", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Pr = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Pr", "#C-GRA-RPL-E1-PR", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Il = pnd_Post_Data_Pnd_Group_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Il", "#C-GRA-RPL-E1-IL", 
            FieldType.NUMERIC, 8);

        pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity", "#SUPPLEMENTAL-RETIREMENT-ANNUITY");
        pnd_Post_Data_Pnd_T_Sra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Sra_Rpl_E1_All_Other", 
            "#T-SRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Fl", "#T-SRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Tx = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Tx", "#T-SRA-RPL-E1-TX", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Il = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Il", "#T-SRA-RPL-E1-IL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Pa = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Pa", "#T-SRA-RPL-E1-PA", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Sra_Rpl_E1_All_Other = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Sra_Rpl_E1_All_Other", 
            "#C-SRA-RPL-E1-ALL-OTHER", FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Fl = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Fl", "#C-SRA-RPL-E1-FL", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Il1 = pnd_Post_Data_Pnd_Supplemental_Retirement_Annuity.newFieldInGroup("pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Il1", "#C-SRA-RPL-E1-IL1", 
            FieldType.NUMERIC, 8);
        pnd_Post_Data_Pnd_Rider_Date = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Date", "#RIDER-DATE", FieldType.NUMERIC, 6);

        pnd_Post_Data__R_Field_2 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data__R_Field_2", "REDEFINE", pnd_Post_Data_Pnd_Rider_Date);
        pnd_Post_Data_Pnd_Rider_Month = pnd_Post_Data__R_Field_2.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Month", "#RIDER-MONTH", FieldType.NUMERIC, 2);
        pnd_Post_Data_Pnd_Rider_Year = pnd_Post_Data__R_Field_2.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year", "#RIDER-YEAR", FieldType.NUMERIC, 4);

        pnd_Post_Data__R_Field_3 = pnd_Post_Data__R_Field_2.newGroupInGroup("pnd_Post_Data__R_Field_3", "REDEFINE", pnd_Post_Data_Pnd_Rider_Year);
        pnd_Post_Data_Pnd_Rider_Year_A = pnd_Post_Data__R_Field_3.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year_A", "#RIDER-YEAR-A", FieldType.STRING, 
            4);
        pnd_Post_Data_Pnd_Ra_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Ra_Contracts", "#RA-CONTRACTS", FieldType.STRING, 17, new 
            DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Gra_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Gra_Contracts", "#GRA-CONTRACTS", FieldType.STRING, 17, 
            new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Sra_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Sra_Contracts", "#SRA-CONTRACTS", FieldType.STRING, 17, 
            new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Ra_Tiaa_Contracts_Il = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Ra_Tiaa_Contracts_Il", "#RA-TIAA-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Ra_Cref_Contracts_Il1 = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Ra_Cref_Contracts_Il1", "#RA-CREF-CONTRACTS-IL1", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Gra_Tiaa_Contracts_Il = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Gra_Tiaa_Contracts_Il", "#GRA-TIAA-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Gra_Cref_Contracts_Il = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Gra_Cref_Contracts_Il", "#GRA-CREF-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Sra_Tiaa_Contracts_Il = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Sra_Tiaa_Contracts_Il", "#SRA-TIAA-CONTRACTS-IL", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pnd_Post_Data_Pnd_Sra_Cref_Contracts_Il1 = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Sra_Cref_Contracts_Il1", "#SRA-CREF-CONTRACTS-IL1", 
            FieldType.STRING, 8, new DbsArrayController(1, 50));
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pnd_Pst_Tbl_Data_Field2 = parameters.newFieldInRecord("pnd_Pst_Tbl_Data_Field2", "#PST-TBL-DATA-FIELD2", FieldType.STRING, 253);
        pnd_Pst_Tbl_Data_Field2.setParameterOption(ParameterOption.ByReference);

        pnd_Pst_Tbl_Data_Field2__R_Field_4 = parameters.newGroupInRecord("pnd_Pst_Tbl_Data_Field2__R_Field_4", "REDEFINE", pnd_Pst_Tbl_Data_Field2);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory = pnd_Pst_Tbl_Data_Field2__R_Field_4.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory", "#MM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1 = pnd_Pst_Tbl_Data_Field2__R_Field_4.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1", "#MM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2 = pnd_Pst_Tbl_Data_Field2__R_Field_4.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2", "#MM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3 = pnd_Pst_Tbl_Data_Field2__R_Field_4.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3", "#MM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4 = pnd_Pst_Tbl_Data_Field2__R_Field_4.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4", "#MM-TITLE4", 
            FieldType.STRING, 50);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Appl_Title = localVariables.newFieldInRecord("pnd_Appl_Title", "#APPL-TITLE", FieldType.STRING, 15);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Date_D = pnd_Work.newFieldInGroup("pnd_Work_Date_D", "DATE-D", FieldType.DATE);
        pnd_Work_Month = pnd_Work.newFieldInGroup("pnd_Work_Month", "MONTH", FieldType.STRING, 11);
        pnd_Work_Day = pnd_Work.newFieldInGroup("pnd_Work_Day", "DAY", FieldType.STRING, 2);
        pnd_Work_Ccyy = pnd_Work.newFieldInGroup("pnd_Work_Ccyy", "CCYY", FieldType.STRING, 4);
        pnd_Work_Next_Day_Date = pnd_Work.newFieldInGroup("pnd_Work_Next_Day_Date", "NEXT-DAY-DATE", FieldType.DATE);
        pnd_Work_Formatted_Date = pnd_Work.newFieldInGroup("pnd_Work_Formatted_Date", "FORMATTED-DATE", FieldType.STRING, 19);
        pnd_Work_Day_Temp = pnd_Work.newFieldInGroup("pnd_Work_Day_Temp", "DAY-TEMP", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 8);
        pnd_Contract_Combo = localVariables.newFieldInRecord("pnd_Contract_Combo", "#CONTRACT-COMBO", FieldType.STRING, 60);
        pnd_Multi_Plan_Ctr = localVariables.newFieldInRecord("pnd_Multi_Plan_Ctr", "#MULTI-PLAN-CTR", FieldType.NUMERIC, 2);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaRidl634.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Appl_Title.setInitialValue("RIDER MAILINGS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Ridn774() throws Exception
    {
        super("Ridn774");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDN774", onError);
        setupReports();
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
        pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: RESET MSG-INFO-SUB
        pnd_Work_Date_D.setValue(Global.getDATX());                                                                                                                       //Natural: ASSIGN DATE-D := *DATX
        //*  MAIL ITEM
                                                                                                                                                                          //Natural: PERFORM CREATE-MI-RECORD
        sub_Create_Mi_Record();
        if (condition(Global.isEscape())) {return;}
        //*  RIDER MAIL COVER LETTER
                                                                                                                                                                          //Natural: PERFORM CREATE-RM-RECORD
        sub_Create_Rm_Record();
        if (condition(Global.isEscape())) {return;}
        //*  CONTRACT DETAILS
                                                                                                                                                                          //Natural: PERFORM CREATE-CD-RECORD
        sub_Create_Cd_Record();
        if (condition(Global.isEscape())) {return;}
        //*  DIALOGUE TEMPLATE ENDORSEMENT
                                                                                                                                                                          //Natural: PERFORM CREATE-EN-RECORD
        sub_Create_En_Record();
        if (condition(Global.isEscape())) {return;}
        //*  ENDORSEMENT
                                                                                                                                                                          //Natural: PERFORM CREATE-EP-RECORD
        sub_Create_Ep_Record();
        if (condition(Global.isEscape())) {return;}
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-MI-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-RM-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CD-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-EP-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-EN-RECORD
        //*  ----------------------------------
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ECS-OUT-DATA
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //*  -----------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DATE
    }
    private void sub_Create_Mi_Record() throws Exception                                                                                                                  //Natural: CREATE-MI-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_Rec_Id().setValue("MI");                                                                                                                 //Natural: MOVE 'MI' TO ECS-RECORD.REC-ID
        ldaRidl634.getEcs_Record_Pin_Nbr().setValue(pnd_Post_Data_Pnd_Pin_Number);                                                                                        //Natural: MOVE #POST-DATA.#PIN-NUMBER TO ECS-RECORD.PIN-NBR
        ldaRidl634.getEcs_Record_Pst_Rqst_Id().setValue(pnd_Post_Data_Pnd_Pst_Rqst_Id);                                                                                   //Natural: MOVE #POST-DATA.#PST-RQST-ID TO ECS-RECORD.PST-RQST-ID
        ldaRidl634.getEcs_Record_Pckge_Cde().setValue(pnd_Post_Data_Pnd_Package_Code);                                                                                    //Natural: MOVE #POST-DATA.#PACKAGE-CODE TO ECS-RECORD.PCKGE-CDE
        ldaRidl634.getEcs_Record_Full_Nme().setValue(pnd_Post_Data_Pnd_Full_Name);                                                                                        //Natural: MOVE #POST-DATA.#FULL-NAME TO ECS-RECORD.FULL-NME
        ldaRidl634.getEcs_Record_Last_Nme().setValue(pnd_Post_Data_Pnd_Last_Name);                                                                                        //Natural: MOVE #POST-DATA.#LAST-NAME TO ECS-RECORD.LAST-NME
        ldaRidl634.getEcs_Record_Tiaa_Cntrct_Nbr().setValue(pnd_Post_Data_Pnd_Tiaa_Cntrct_Number);                                                                        //Natural: MOVE #POST-DATA.#TIAA-CNTRCT-NUMBER TO ECS-RECORD.TIAA-CNTRCT-NBR
        ldaRidl634.getEcs_Record_Addrss_Typ_Cde().setValue(pnd_Post_Data_Pnd_Address_Type_Cde);                                                                           //Natural: MOVE #POST-DATA.#ADDRESS-TYPE-CDE TO ECS-RECORD.ADDRSS-TYP-CDE
        ldaRidl634.getEcs_Record_Addrss_Line_Txt().getValue("*").setValue(pnd_Post_Data_Pnd_Address_Line_Txt.getValue("*"));                                              //Natural: MOVE #POST-DATA.#ADDRESS-LINE-TXT ( * ) TO ECS-RECORD.ADDRSS-LINE-TXT ( * )
        ldaRidl634.getEcs_Record_Letter_Dte().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO ECS-RECORD.LETTER-DTE
        ldaRidl634.getEcs_Record_Byte4().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE4
        ldaRidl634.getEcs_Record_Byte5().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE5
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-MI-RECORD
    }
    private void sub_Create_Rm_Record() throws Exception                                                                                                                  //Natural: CREATE-RM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_Rm_Record_Type().setValue("RM");                                                                                                         //Natural: MOVE 'RM' TO ECS-RECORD.RM-RECORD-TYPE
        ldaRidl634.getEcs_Record_Rm_Signatory_Name().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory);                                                                  //Natural: MOVE #MM-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
        ldaRidl634.getEcs_Record_Rm_Signatory_Title1().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1);                                                                   //Natural: MOVE #MM-TITLE1 TO ECS-RECORD.RM-SIGNATORY-TITLE1
        ldaRidl634.getEcs_Record_Rm_Signatory_Title2().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2);                                                                   //Natural: MOVE #MM-TITLE2 TO ECS-RECORD.RM-SIGNATORY-TITLE2
        ldaRidl634.getEcs_Record_Rm_Signatory_Title3().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3);                                                                   //Natural: MOVE #MM-TITLE3 TO ECS-RECORD.RM-SIGNATORY-TITLE3
        ldaRidl634.getEcs_Record_Rm_Signatory_Title4().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4);                                                                   //Natural: MOVE #MM-TITLE4 TO ECS-RECORD.RM-SIGNATORY-TITLE4
        ldaRidl634.getEcs_Record_Rm_Cont_Num().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Post_Data_Pnd_Tiaa_Cntrct_Number, ",", pnd_Post_Data_Pnd_Cref_Cntrct_Number)); //Natural: COMPRESS #POST-DATA.#TIAA-CNTRCT-NUMBER ',' #POST-DATA.#CREF-CNTRCT-NUMBER INTO ECS-RECORD.RM-CONT-NUM LEAVING NO SPACE
        short decideConditionsMet393 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #LETTER-TYPE;//Natural: VALUE '7'
        if (condition((pnd_Post_Data_Pnd_Letter_Type.equals("7"))))
        {
            decideConditionsMet393++;
            ldaRidl634.getEcs_Record_Rm_Cover_Letter_Code().setValue("RPL-7");                                                                                            //Natural: MOVE 'RPL-7' TO ECS-RECORD.RM-COVER-LETTER-CODE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-RM-RECORD
    }
    private void sub_Create_Cd_Record() throws Exception                                                                                                                  //Natural: CREATE-CD-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_Cd_Record_Type().setValue("CD");                                                                                                         //Natural: MOVE 'CD' TO ECS-RECORD.CD-RECORD-TYPE
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 50
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(50)); pnd_I.nadd(1))
        {
            if (condition(pnd_Post_Data_Pnd_Ra_Contracts.getValue(pnd_I).notEquals(" ")))                                                                                 //Natural: IF #RA-CONTRACTS ( #I ) NE ' '
            {
                ldaRidl634.getEcs_Record_Cd_Contract_Number().setValue(DbsUtil.compress(pnd_Post_Data_Pnd_Ra_Contracts.getValue(pnd_I)));                                 //Natural: COMPRESS #RA-CONTRACTS ( #I ) INTO ECS-RECORD.CD-CONTRACT-NUMBER
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 50
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(50)); pnd_I.nadd(1))
        {
            if (condition(pnd_Post_Data_Pnd_Gra_Contracts.getValue(pnd_I).notEquals(" ")))                                                                                //Natural: IF #GRA-CONTRACTS ( #I ) NE ' '
            {
                ldaRidl634.getEcs_Record_Cd_Contract_Number().setValue(DbsUtil.compress(pnd_Post_Data_Pnd_Gra_Contracts.getValue(pnd_I)));                                //Natural: COMPRESS #GRA-CONTRACTS ( #I ) INTO ECS-RECORD.CD-CONTRACT-NUMBER
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 50
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(50)); pnd_I.nadd(1))
        {
            if (condition(pnd_Post_Data_Pnd_Sra_Contracts.getValue(pnd_I).notEquals(" ")))                                                                                //Natural: IF #SRA-CONTRACTS ( #I ) NE ' '
            {
                ldaRidl634.getEcs_Record_Cd_Contract_Number().setValue(DbsUtil.compress(pnd_Post_Data_Pnd_Sra_Contracts.getValue(pnd_I)));                                //Natural: COMPRESS #SRA-CONTRACTS ( #I ) INTO ECS-RECORD.CD-CONTRACT-NUMBER
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CREATE-CD-RECORD
    }
    private void sub_Create_Ep_Record() throws Exception                                                                                                                  //Natural: CREATE-EP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_Ep_Record_Type().setValue("EP");                                                                                                         //Natural: MOVE 'EP' TO ECS-RECORD.EP-RECORD-TYPE
        short decideConditionsMet438 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #T-RA-RPL-E1-ALL-OTHER > 0
        if (condition(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_All_Other.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR04:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-RA-RPL-E1-ALL-OTHER
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_All_Other)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-RA-RPL-E1");                                                                                           //Natural: MOVE 'T-RA-RPL-E1' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-RA-RPL-E1-FL > 0
        if (condition(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Fl.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR05:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-RA-RPL-E1-FL
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Fl)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-RA-RPL-E1-FL");                                                                                        //Natural: MOVE 'T-RA-RPL-E1-FL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-RA-RPL-E1-TX > 0
        if (condition(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Tx.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR06:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-RA-RPL-E1-TX
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Tx)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-RA-RPL-E1-TX");                                                                                        //Natural: MOVE 'T-RA-RPL-E1-TX' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-RA-RPL-E1-PR > 0
        if (condition(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pr.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR07:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-RA-RPL-E1-PR
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pr)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-RA-RPL-E1-PR");                                                                                        //Natural: MOVE 'T-RA-RPL-E1-PR' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-RA-RPL-E1-PA > 0
        if (condition(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pa.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR08:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-RA-RPL-E1-PA
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Pa)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-RA-RPL-E1-PA");                                                                                        //Natural: MOVE 'T-RA-RPL-E1-PA' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-RA-RPL-E1-ALL-OTHER > 0
        if (condition(pnd_Post_Data_Pnd_C_Ra_Rpl_E1_All_Other.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR09:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-RA-RPL-E1-ALL-OTHER
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Ra_Rpl_E1_All_Other)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("C-RA-RPL-E1");                                                                                           //Natural: MOVE 'C-RA-RPL-E1' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-RA-RPL-E1-AR > 0
        if (condition(pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Ar.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR10:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-RA-RPL-E1-AR
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Ar)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("C-RA-RPL-E1-AR");                                                                                        //Natural: MOVE 'C-RA-RPL-E1-AR' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-RA-RPL-E1-FL > 0
        if (condition(pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Fl.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR11:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-RA-RPL-E1-FL
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Fl)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("C-RA-RPL-E1-FL");                                                                                        //Natural: MOVE 'C-RA-RPL-E1-FL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-RA-RPL-E1-PR > 0
        if (condition(pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Pr.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR12:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-RA-RPL-E1-PR
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Pr)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("C-RA-RPL-E1-PR");                                                                                        //Natural: MOVE 'C-RA-RPL-E1-PR' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-GRA-RPL-E1-ALL-OTHER > 0
        if (condition(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_All_Other.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR13:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-GRA-RPL-E1-ALL-OTHER
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_All_Other)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-GRA-RPL-E1");                                                                                          //Natural: MOVE 'T-GRA-RPL-E1' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-GRA-RPL-E1-FL > 0
        if (condition(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Fl.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR14:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-GRA-RPL-E1-FL
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Fl)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-GRA-RPL-E1-FL");                                                                                       //Natural: MOVE 'T-GRA-RPL-E1-FL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-GRA-RPL-E1-MD > 0
        if (condition(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Md.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR15:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-GRA-RPL-E1-MD
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Md)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-GRA-RPL-E1-MD");                                                                                       //Natural: MOVE 'T-GRA-RPL-E1-MD' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-GRA-RPL-E1-TX > 0
        if (condition(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Tx.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR16:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-GRA-RPL-E1-TX
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Tx)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-GRA-RPL-E1-TX");                                                                                       //Natural: MOVE 'T-GRA-RPL-E1-TX' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-GRA-RPL-E1-PR > 0
        if (condition(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Pr.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR17:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-GRA-RPL-E1-PR
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Pr)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-GRA-RPL-E1-PR");                                                                                       //Natural: MOVE 'T-GRA-RPL-E1-PR' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-GRA-RPL-E1-ALL-OTHER > 0
        if (condition(pnd_Post_Data_Pnd_C_Gra_Rpl_E1_All_Other.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR18:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-GRA-RPL-E1-ALL-OTHER
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Gra_Rpl_E1_All_Other)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("C-GRA-RPL-E1");                                                                                          //Natural: MOVE 'C-GRA-RPL-E1' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-GRA-RPL-E1-FL > 0
        if (condition(pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Fl.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR19:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-GRA-RPL-E1-FL
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Fl)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("C-GRA-RPL-E1-FL");                                                                                       //Natural: MOVE 'C-GRA-RPL-E1-FL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-GRA-RPL-E1-TX > 0
        if (condition(pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Tx.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR20:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-GRA-RPL-E1-TX
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Tx)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("C-GRA-RPL-E1-TX");                                                                                       //Natural: MOVE 'C-GRA-RPL-E1-TX' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-GRA-RPL-E1-PR > 0
        if (condition(pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Pr.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR21:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-GRA-RPL-E1-PR
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Pr)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("C-GRA-RPL-E1-PR");                                                                                       //Natural: MOVE 'C-GRA-RPL-E1-PR' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-SRA-RPL-E1-ALL-OTHER > 0
        if (condition(pnd_Post_Data_Pnd_T_Sra_Rpl_E1_All_Other.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR22:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-SRA-RPL-E1-ALL-OTHER
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Sra_Rpl_E1_All_Other)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-SRA-RPL-E1");                                                                                          //Natural: MOVE 'T-SRA-RPL-E1' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-SRA-RPL-E1-FL > 0
        if (condition(pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Fl.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR23:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-SRA-RPL-E1-FL
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Fl)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-SRA-RPL-E1-FL");                                                                                       //Natural: MOVE 'T-SRA-RPL-E1-FL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-SRA-RPL-E1-TX > 0
        if (condition(pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Tx.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR24:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-SRA-RPL-E1-TX
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Tx)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-SRA-RPL-E1-TX");                                                                                       //Natural: MOVE 'T-SRA-RPL-E1-TX' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-SRA-RPL-E1-PA > 0
        if (condition(pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Pa.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR25:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-SRA-RPL-E1-PA
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Pa)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("T-SRA-RPL-E1-PA");                                                                                       //Natural: MOVE 'T-SRA-RPL-E1-PA' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-SRA-RPL-E1-ALL-OTHER > 0
        if (condition(pnd_Post_Data_Pnd_C_Sra_Rpl_E1_All_Other.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR26:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-SRA-RPL-E1-ALL-OTHER
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Sra_Rpl_E1_All_Other)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("C-SRA-RPL-E1");                                                                                          //Natural: MOVE 'C-SRA-RPL-E1' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-SRA-RPL-E1-FL > 0
        if (condition(pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Fl.greater(getZero())))
        {
            decideConditionsMet438++;
            FOR27:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-SRA-RPL-E1-FL
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Fl)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_Ep_Pdf_Name().setValue("C-SRA-RPL-E1-FL");                                                                                       //Natural: MOVE 'C-SRA-RPL-E1-FL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet438 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CREATE-EP-RECORD
    }
    private void sub_Create_En_Record() throws Exception                                                                                                                  //Natural: CREATE-EN-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl634.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl634.getEcs_Record_Ep_Record_Type().setValue("EN");                                                                                                         //Natural: MOVE 'EN' TO ECS-RECORD.EP-RECORD-TYPE
        short decideConditionsMet572 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #T-RA-RPL-E1-IL > 0
        if (condition(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Il.greater(getZero())))
        {
            decideConditionsMet572++;
            FOR28:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-RA-RPL-E1-IL
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Ra_Rpl_E1_Il)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("TRARPLIL");                                                                                          //Natural: MOVE 'TRARPLIL' TO ECS-RECORD.EN-ENDORSE-TYPE
                ldaRidl634.getEcs_Record_En_Contract_Number().setValue(pnd_Post_Data_Pnd_Ra_Tiaa_Contracts_Il.getValue(pnd_K));                                           //Natural: MOVE #RA-TIAA-CONTRACTS-IL ( #K ) TO ECS-RECORD.EN-CONTRACT-NUMBER
                if (condition(pnd_Multi_Plan_Ctr.greater(1)))                                                                                                             //Natural: IF #MULTI-PLAN-CTR GT 1
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("Y");                                                                                           //Natural: MOVE 'Y' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("N");                                                                                           //Natural: MOVE 'N' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-RA-RPL-E1-IL1 > 0
        if (condition(pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Il1.greater(getZero())))
        {
            decideConditionsMet572++;
            FOR29:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-RA-RPL-E1-IL1
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Ra_Rpl_E1_Il1)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CRARPLL1");                                                                                          //Natural: MOVE 'CRARPLL1' TO ECS-RECORD.EN-ENDORSE-TYPE
                ldaRidl634.getEcs_Record_En_Contract_Number().setValue(pnd_Post_Data_Pnd_Ra_Cref_Contracts_Il1.getValue(pnd_K));                                          //Natural: MOVE #RA-CREF-CONTRACTS-IL1 ( #K ) TO ECS-RECORD.EN-CONTRACT-NUMBER
                if (condition(pnd_Multi_Plan_Ctr.greater(1)))                                                                                                             //Natural: IF #MULTI-PLAN-CTR GT 1
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("Y");                                                                                           //Natural: MOVE 'Y' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("N");                                                                                           //Natural: MOVE 'N' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-GRA-RPL-E1-IL > 0
        if (condition(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Il.greater(getZero())))
        {
            decideConditionsMet572++;
            getReports().write(0, "=",pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Il);                                                                                                 //Natural: WRITE '=' #T-GRA-RPL-E1-IL
            if (Global.isEscape()) return;
            FOR30:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-GRA-RPL-E1-IL
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Gra_Rpl_E1_Il)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("TGRARPLI");                                                                                          //Natural: MOVE 'TGRARPLI' TO ECS-RECORD.EN-ENDORSE-TYPE
                getReports().write(0, "=",pnd_K,"=",pnd_Post_Data_Pnd_Gra_Tiaa_Contracts_Il.getValue("*"));                                                               //Natural: WRITE '=' #K '=' #GRA-TIAA-CONTRACTS-IL ( * )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaRidl634.getEcs_Record_En_Contract_Number().setValue(pnd_Post_Data_Pnd_Gra_Tiaa_Contracts_Il.getValue(pnd_K));                                          //Natural: MOVE #GRA-TIAA-CONTRACTS-IL ( #K ) TO ECS-RECORD.EN-CONTRACT-NUMBER
                if (condition(pnd_Multi_Plan_Ctr.greater(1)))                                                                                                             //Natural: IF #MULTI-PLAN-CTR GT 1
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("Y");                                                                                           //Natural: MOVE 'Y' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("N");                                                                                           //Natural: MOVE 'N' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-GRA-RPL-E1-IL > 0
        if (condition(pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Il.greater(getZero())))
        {
            decideConditionsMet572++;
            FOR31:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-GRA-RPL-E1-IL
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Gra_Rpl_E1_Il)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CGRARPLI");                                                                                          //Natural: MOVE 'CGRARPLI' TO ECS-RECORD.EN-ENDORSE-TYPE
                ldaRidl634.getEcs_Record_En_Contract_Number().setValue(pnd_Post_Data_Pnd_Gra_Cref_Contracts_Il.getValue(pnd_K));                                          //Natural: MOVE #GRA-CREF-CONTRACTS-IL ( #K ) TO ECS-RECORD.EN-CONTRACT-NUMBER
                if (condition(pnd_Multi_Plan_Ctr.greater(1)))                                                                                                             //Natural: IF #MULTI-PLAN-CTR GT 1
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("Y");                                                                                           //Natural: MOVE 'Y' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("N");                                                                                           //Natural: MOVE 'N' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #T-SRA-RPL-E1-IL > 0
        if (condition(pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Il.greater(getZero())))
        {
            decideConditionsMet572++;
            FOR32:                                                                                                                                                        //Natural: FOR #K = 1 TO #T-SRA-RPL-E1-IL
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_T_Sra_Rpl_E1_Il)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("TSRARPLI");                                                                                          //Natural: MOVE 'TSRARPLI' TO ECS-RECORD.EN-ENDORSE-TYPE
                ldaRidl634.getEcs_Record_En_Contract_Number().setValue(pnd_Post_Data_Pnd_Sra_Tiaa_Contracts_Il.getValue(pnd_K));                                          //Natural: MOVE #SRA-TIAA-CONTRACTS-IL ( #K ) TO ECS-RECORD.EN-CONTRACT-NUMBER
                if (condition(pnd_Multi_Plan_Ctr.greater(1)))                                                                                                             //Natural: IF #MULTI-PLAN-CTR GT 1
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("Y");                                                                                           //Natural: MOVE 'Y' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("N");                                                                                           //Natural: MOVE 'N' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #C-SRA-RPL-E1-IL1 > 0
        if (condition(pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Il1.greater(getZero())))
        {
            decideConditionsMet572++;
            FOR33:                                                                                                                                                        //Natural: FOR #K = 1 TO #C-SRA-RPL-E1-IL1
            for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_Post_Data_Pnd_C_Sra_Rpl_E1_Il1)); pnd_K.nadd(1))
            {
                ldaRidl634.getEcs_Record_En_Endorse_Type().setValue("CSRARPL1");                                                                                          //Natural: MOVE 'CSRARPL1' TO ECS-RECORD.EN-ENDORSE-TYPE
                ldaRidl634.getEcs_Record_En_Contract_Number().setValue(pnd_Post_Data_Pnd_Sra_Cref_Contracts_Il1.getValue(pnd_K));                                         //Natural: MOVE #SRA-CREF-CONTRACTS-IL1 ( #K ) TO ECS-RECORD.EN-CONTRACT-NUMBER
                if (condition(pnd_Multi_Plan_Ctr.greater(1)))                                                                                                             //Natural: IF #MULTI-PLAN-CTR GT 1
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("Y");                                                                                           //Natural: MOVE 'Y' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaRidl634.getEcs_Record_En_Multi_Plan_Ind().setValue("N");                                                                                           //Natural: MOVE 'N' TO ECS-RECORD.EN-MULTI-PLAN-IND
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet572 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CREATE-EN-RECORD
    }
    private void sub_Write_Ecs_Out_Data() throws Exception                                                                                                                //Natural: WRITE-ECS-OUT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        getWorkFiles().write(2, false, ldaRidl634.getEcs_Record());                                                                                                       //Natural: WRITE WORK FILE 2 ECS-RECORD
        //*  WRITE-ECS-OUT-DATA
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, Global.getPROGRAM(),"IN PROCESS-ERROR","SUBROUTINE",pnd_Post_Data_Pnd_Pst_Rqst_Id);                                                     //Natural: WRITE ( 0 ) *PROGRAM 'IN PROCESS-ERROR' 'SUBROUTINE' #POST-DATA.#PST-RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  PSTA9531.#POST-DATA-STATUS := PSTL9620.GEN-FAILED
        //*  PSTA9531.TERMINATE-IND    := TRUE
        getReports().print(0, Global.getPROGRAM(),"JOB TERMINATED! NO",pnd_Appl_Title,"CREATED.",NEWLINE,"SEE MESSAGES ABOVE FOR FURTHER INFORMATION");                   //Natural: PRINT ( 0 ) *PROGRAM 'JOB TERMINATED! NO' #APPL-TITLE 'CREATED.' / 'SEE MESSAGES ABOVE FOR FURTHER INFORMATION'
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
        //*  PROCESS-ERROR
    }
    private void sub_Format_Date() throws Exception                                                                                                                       //Natural: FORMAT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------
        pnd_Work_Month.setValueEdited(pnd_Work_Date_D,new ReportEditMask("LLLLLLLLLLL"));                                                                                 //Natural: MOVE EDITED #WORK.DATE-D ( EM = L ( 11 ) ) TO #WORK.MONTH
        pnd_Work_Day_Temp.setValueEdited(pnd_Work_Date_D,new ReportEditMask("ZD"));                                                                                       //Natural: MOVE EDITED #WORK.DATE-D ( EM = ZD ) TO #WORK.DAY-TEMP
        pnd_Work_Day.setValue(pnd_Work_Day_Temp, MoveOption.LeftJustified);                                                                                               //Natural: MOVE LEFT #WORK.DAY-TEMP TO #WORK.DAY
        pnd_Work_Ccyy.setValueEdited(pnd_Work_Date_D,new ReportEditMask("YYYY"));                                                                                         //Natural: MOVE EDITED #WORK.DATE-D ( EM = YYYY ) TO #WORK.CCYY
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Month));                                                                //Natural: COMPRESS #WORK.MONTH INTO #WORK.FORMATTED-DATE LEAVING NO
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(pnd_Work_Formatted_Date, pnd_Work_Ccyy));                                                                       //Natural: COMPRESS #WORK.FORMATTED-DATE #WORK.CCYY INTO #WORK.FORMATTED-DATE
        //*  FORMAT-DATE
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
    }
}
