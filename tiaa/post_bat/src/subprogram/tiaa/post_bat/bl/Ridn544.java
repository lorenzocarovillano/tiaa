/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:07:22 AM
**        * FROM NATURAL SUBPROGRAM : Ridn544
************************************************************
**        * FILE NAME            : Ridn544.java
**        * CLASS NAME           : Ridn544
**        * INSTANCE NAME        : Ridn544
************************************************************
************************************************************************
* PROGRAM  : RIDN544
* AUTHOR   : JOANNES AVE
* SYSTEM   : RIDER MAILINGS
* TITLE    : TIAA STABLE VALUE ENDORSEMENT
* FUNCTION : THIS MODULE GENERATES ECS DATA FOR A BATCH OF
*          : REQUESTS TO POST. THE OUTPUT IS USED AS INPUT TO
*          : DIALOGUE COMPOSITION SOFTWARE.
* DETAILS  : THIS MODULE READS A FILE FROM RIDP542 AND REFORMATS THE
*          : WORK FILE AS INPUT TO ECS. THE DIALOGUE SOFTWARE
*          : CREATES PS TO BE ROUTED TO XEROX.
*          :
*          : POST PACKAGE ID: RDR2010C
* HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
* J.AVE    11/26/10 INITIAL IMPLEMENTATION
* J.AVE    01/03/11 ADD LA,MN,MT,NE,VA CERTS AND IL CREF ENDORSEMENT
* J.AVE    06/01/12 ADD THE FOLLOWING:
*                   -  FL CREF ENDORSEMENTS FOR RA AND GRA/RC
*                   -  NH TIAA ENDORSEMENTS FOR RA AND GRA/RC
*                   -  NH AND FL CERTIFICATES
* J.AVE    07/15/16 REMARK PASSING OF NAME AND ADDRESS. CCP WILL OBTAIN
*                   THIS FROM MDM AS PART OF DCS ELIMINATION
* L.SHU    02/16/17 CHANGE NH TO GET GENERIC CERTIFICATE     /* LS1
*                   CHANGE OR TO GET OR CERTIFICATE
*                   ADD NM, NY, PR CERTIFICATE
*                   CHANGE LETTER TYPE CODE SVEQBWMM TO SVRCV2MM
* L.SHU    05/26/17 PIN EXPANSION - RESTOW FOR RIDA544         PINEXP
************************************************************************
*

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Ridn544 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaRida544 pdaRida544;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_P pdaCwfpda_P;
    private LdaRidl504 ldaRidl504;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Pst_Tbl_Data_Field2;

    private DbsGroup pnd_Pst_Tbl_Data_Field2__R_Field_1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4;
    private DbsField pnd_Appl_Title;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Date_D;
    private DbsField pnd_Work_Month;
    private DbsField pnd_Work_Day;
    private DbsField pnd_Work_Ccyy;
    private DbsField pnd_Work_Next_Day_Date;
    private DbsField pnd_Work_Formatted_Date;
    private DbsField pnd_Work_Day_Temp;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_K;
    private DbsField pnd_Contract_Combo;
    private DbsField pnd_Pst_Tbl_Data_Field1;

    private DbsGroup pnd_Pst_Tbl_Data_Field1__R_Field_2;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title4;
    private DbsField pnd_Pst_Tbl_Data_Field3;

    private DbsGroup pnd_Pst_Tbl_Data_Field3__R_Field_3;
    private DbsField pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Signatory;
    private DbsField pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title1;
    private DbsField pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title2;
    private DbsField pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title3;
    private DbsField pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title4;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        ldaRidl504 = new LdaRidl504();
        registerRecord(ldaRidl504);

        // parameters
        parameters = new DbsRecord();
        pdaRida544 = new PdaRida544(parameters);
        pdaCwfpda_M = new PdaCwfpda_M(parameters);
        pnd_Pst_Tbl_Data_Field2 = parameters.newFieldInRecord("pnd_Pst_Tbl_Data_Field2", "#PST-TBL-DATA-FIELD2", FieldType.STRING, 253);
        pnd_Pst_Tbl_Data_Field2.setParameterOption(ParameterOption.ByReference);

        pnd_Pst_Tbl_Data_Field2__R_Field_1 = parameters.newGroupInRecord("pnd_Pst_Tbl_Data_Field2__R_Field_1", "REDEFINE", pnd_Pst_Tbl_Data_Field2);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory", "#MM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1", "#MM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2", "#MM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3", "#MM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4 = pnd_Pst_Tbl_Data_Field2__R_Field_1.newFieldInGroup("pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4", "#MM-TITLE4", 
            FieldType.STRING, 50);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Appl_Title = localVariables.newFieldInRecord("pnd_Appl_Title", "#APPL-TITLE", FieldType.STRING, 15);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Date_D = pnd_Work.newFieldInGroup("pnd_Work_Date_D", "DATE-D", FieldType.DATE);
        pnd_Work_Month = pnd_Work.newFieldInGroup("pnd_Work_Month", "MONTH", FieldType.STRING, 11);
        pnd_Work_Day = pnd_Work.newFieldInGroup("pnd_Work_Day", "DAY", FieldType.STRING, 2);
        pnd_Work_Ccyy = pnd_Work.newFieldInGroup("pnd_Work_Ccyy", "CCYY", FieldType.STRING, 4);
        pnd_Work_Next_Day_Date = pnd_Work.newFieldInGroup("pnd_Work_Next_Day_Date", "NEXT-DAY-DATE", FieldType.DATE);
        pnd_Work_Formatted_Date = pnd_Work.newFieldInGroup("pnd_Work_Formatted_Date", "FORMATTED-DATE", FieldType.STRING, 19);
        pnd_Work_Day_Temp = pnd_Work.newFieldInGroup("pnd_Work_Day_Temp", "DAY-TEMP", FieldType.STRING, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 1);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 2);
        pnd_Contract_Combo = localVariables.newFieldInRecord("pnd_Contract_Combo", "#CONTRACT-COMBO", FieldType.STRING, 60);
        pnd_Pst_Tbl_Data_Field1 = localVariables.newFieldInRecord("pnd_Pst_Tbl_Data_Field1", "#PST-TBL-DATA-FIELD1", FieldType.STRING, 253);

        pnd_Pst_Tbl_Data_Field1__R_Field_2 = localVariables.newGroupInRecord("pnd_Pst_Tbl_Data_Field1__R_Field_2", "REDEFINE", pnd_Pst_Tbl_Data_Field1);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Signatory = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Signatory", "#WM-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title1 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title1", "#WM-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title2 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title2", "#WM-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title3 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title3", "#WM-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title4 = pnd_Pst_Tbl_Data_Field1__R_Field_2.newFieldInGroup("pnd_Pst_Tbl_Data_Field1_Pnd_Wm_Title4", "#WM-TITLE4", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field3 = localVariables.newFieldInRecord("pnd_Pst_Tbl_Data_Field3", "#PST-TBL-DATA-FIELD3", FieldType.STRING, 253);

        pnd_Pst_Tbl_Data_Field3__R_Field_3 = localVariables.newGroupInRecord("pnd_Pst_Tbl_Data_Field3__R_Field_3", "REDEFINE", pnd_Pst_Tbl_Data_Field3);
        pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Signatory = pnd_Pst_Tbl_Data_Field3__R_Field_3.newFieldInGroup("pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Signatory", "#SV-SIGNATORY", 
            FieldType.STRING, 40);
        pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title1 = pnd_Pst_Tbl_Data_Field3__R_Field_3.newFieldInGroup("pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title1", "#SV-TITLE1", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title2 = pnd_Pst_Tbl_Data_Field3__R_Field_3.newFieldInGroup("pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title2", "#SV-TITLE2", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title3 = pnd_Pst_Tbl_Data_Field3__R_Field_3.newFieldInGroup("pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title3", "#SV-TITLE3", 
            FieldType.STRING, 50);
        pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title4 = pnd_Pst_Tbl_Data_Field3__R_Field_3.newFieldInGroup("pnd_Pst_Tbl_Data_Field3_Pnd_Sv_Title4", "#SV-TITLE4", 
            FieldType.STRING, 50);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaRidl504.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Appl_Title.setInitialValue("RIDER MAILINGS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Ridn544() throws Exception
    {
        super("Ridn544");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("RIDN544", onError);
        setupReports();
        //* ***********************************************************************
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
        pdaCwfpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: FORMAT ( 0 ) PS = 60 LS = 132;//Natural: RESET MSG-INFO-SUB
        pnd_Work_Date_D.setValue(Global.getDATX());                                                                                                                       //Natural: ASSIGN DATE-D := *DATX
        //*  PERFORM GET-SIGNATORY-TITLE
        //*  RESET #CONTRACT-COMBO
        //*  MOVE 'CTR' TO #CONTRACT-COMBO
        //*  IF #CTR-RA-SRA-GN NE 0
        //*    COMPRESS #CONTRACT-COMBO '-RAGN:' #CTR-RA-SRA-GN-A INTO
        //*             #CONTRACT-COMBO LEAVING NO
        //*  END-IF
        //*  IF #CTR-GRA-GSRA-RS-GN NE 0
        //*    COMPRESS #CONTRACT-COMBO '-GRGN:' #CTR-GRA-GSRA-RS-GN-A INTO
        //*             #CONTRACT-COMBO LEAVING NO
        //*  END-IF
        //*  IF #CTR-RC-GN NE 0
        //*    COMPRESS #CONTRACT-COMBO '-RCGN:' #CTR-RC-GN-A INTO
        //*             #CONTRACT-COMBO LEAVING NO
        //*  END-IF
        //*  IF #CTR-RA-SRA-IL NE 0
        //*   COMPRESS #CONTRACT-COMBO '-RAIL:' #CTR-RA-SRA-IL-A INTO
        //*            #CONTRACT-COMBO LEAVING NO
        //*  END-IF
        //*  IF #CTR-GRA-GSRA-RS-IL NE 0
        //*    COMPRESS #CONTRACT-COMBO '-GRIL:' #CTR-GRA-GSRA-RS-IL-A INTO
        //*             #CONTRACT-COMBO LEAVING NO
        //*  END-IF
        //*  IF #CTR-RC-IL NE 0
        //*    COMPRESS #CONTRACT-COMBO '-RCIL:' #CTR-RC-IL-A INTO
        //*             #CONTRACT-COMBO LEAVING NO
        //*  END-IF
        //*  MAIL ITEM
                                                                                                                                                                          //Natural: PERFORM CREATE-MI-RECORD
        sub_Create_Mi_Record();
        if (condition(Global.isEscape())) {return;}
        //*  RIDER MAIL COVER LETTER
                                                                                                                                                                          //Natural: PERFORM CREATE-RM-RECORD
        sub_Create_Rm_Record();
        if (condition(Global.isEscape())) {return;}
        //*  ADDRESS PAGE
                                                                                                                                                                          //Natural: PERFORM CREATE-AP-RECORD
        sub_Create_Ap_Record();
        if (condition(Global.isEscape())) {return;}
        //*  STAPLING
                                                                                                                                                                          //Natural: PERFORM CREATE-SP-RECORD
        sub_Create_Sp_Record();
        if (condition(Global.isEscape())) {return;}
        //*  CONTRACT DETAILS
                                                                                                                                                                          //Natural: PERFORM CREATE-CD-RECORD
        sub_Create_Cd_Record();
        if (condition(Global.isEscape())) {return;}
        //*  NOT APPLICABLE TO RC            /* LS1
        if (condition(pdaRida544.getPnd_Post_Data_Pnd_Inst_Sv_Lob().notEquals("A")))                                                                                      //Natural: IF #INST-SV-LOB NOT = 'A'
        {
            //*  EQUITY WASH ENDORSEMENT PDF
                                                                                                                                                                          //Natural: PERFORM CREATE-EP-RECORD
            sub_Create_Ep_Record();
            if (condition(Global.isEscape())) {return;}
            //*  LS1
        }                                                                                                                                                                 //Natural: END-IF
        //*  CERTIFICATE PDF
                                                                                                                                                                          //Natural: PERFORM CREATE-ES-RECORD
        sub_Create_Es_Record();
        if (condition(Global.isEscape())) {return;}
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-MI-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-RM-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-AP-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-SP-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CD-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-EP-RECORD
        //*  ----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-ES-RECORD
        //*  ------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ECS-OUT-DATA
        //*  -------------------------------------
        //*  DEFINE SUBROUTINE GET-SIGNATORY-TITLE
        //*  -------------------------------------
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'WM-SIGNATORY-TITLE'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD1 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'MM-SIGNATORY-TITLE'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD2 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*    PSTA9017.TBL-TABLE-NME  := 'RIDER-MISCELLANEOUS'
        //*    PSTA9017.TBL-KEY-FIELD  := 'SV-SIGNATORY-TITLE'
        //*    PSTA9017.TBL-SCRTY-LEVEL-IND := 'A'
        //*    CALLNAT 'PSTN9017' PSTA9017
        //*    IF PSTA9017.VALID-ENTRY
        //*      #PST-TBL-DATA-FIELD3 := PSTA9017.TBL-DATA-FIELD
        //*    ELSE
        //*      WRITE 'TABLE ENTRY NOT FOUND'
        //*      ESCAPE ROUTINE
        //*    END-IF
        //*  END-SUBROUTINE /* GET-SIGNATORY-TITLE
        //*  -------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        //*  -----------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DATE
        //*  ---------------------------------------
        //*  DEFINE SUBROUTINE SETUP-POSTNET-BARCODE
        //*  ---------------------------------------
        //*    PSTA9532.ADDRSS-PSTL-DTA := #POST-DATA.#POSTAL-DATA
        //*    CALLNAT 'PSTN9532' PSTA9532   /* CONVERT POSTAL DATA TO CORRECT
        //*    MSG-INFO-SUB                /* POSTNET BARCODE FORMAT
        //*  END-SUBROUTINE  /* SETUP-POSTNET-BARCDOE
    }
    private void sub_Create_Mi_Record() throws Exception                                                                                                                  //Natural: CREATE-MI-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Rec_Id().setValue("MI");                                                                                                                 //Natural: MOVE 'MI' TO ECS-RECORD.REC-ID
        ldaRidl504.getEcs_Record_Pin_Nbr().setValue(pdaRida544.getPnd_Post_Data_Pnd_Pin_Number());                                                                        //Natural: MOVE #POST-DATA.#PIN-NUMBER TO ECS-RECORD.PIN-NBR
        ldaRidl504.getEcs_Record_Pst_Rqst_Id().setValue(pdaRida544.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                                                   //Natural: MOVE #POST-DATA.#PST-RQST-ID TO ECS-RECORD.PST-RQST-ID
        //*    MOVE #POST-DATA.#FULL-NAME             TO ECS-RECORD.FULL-NME
        //*    MOVE #POST-DATA.#ADDRESS-TYPE-CDE      TO ECS-RECORD.ADDRSS-TYP-CDE
        //*    IF PSTA9670.ENVIRONMENT NE 'PROD'    /* SCRAMBLE ADDRESS LINES
        //*      CALLNAT 'PSTN9990' USING #POST-DATA.#ADDRESS-LINE-TXT(*)
        //*    END-IF
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(1) TO
        //*         ECS-RECORD.ADDRSS-LINE-TXT(1)
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(2) TO
        //*         ECS-RECORD.ADDRSS-LINE-TXT(2)
        //*    #A := 2
        //*    FOR #I = 3 TO 6
        //*      ADD 1 TO #A
        //*      IF #POST-DATA.#ADDRESS-LINE-TXT(#I) NE ' '
        //*        MOVE #POST-DATA.#ADDRESS-LINE-TXT(#I) TO
        //*             ECS-RECORD.ADDRSS-LINE-TXT(#I)
        //*      ELSE
        //*        IF PSTA9670.ENVIRONMENT NE  'PROD'
        //*          MOVE 'TEST   ...DO NOT MAIL...' TO
        //*               ECS-RECORD.ADDRSS-LINE-TXT(#A)
        //*        END-IF
        //*      END-IF
        //*    END-FOR
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(1)   TO ECS-RECORD.ADDRSS-LINE-1
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(2)   TO ECS-RECORD.ADDRSS-LINE-2
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(3)   TO ECS-RECORD.ADDRSS-LINE-3
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(4)   TO ECS-RECORD.ADDRSS-LINE-4
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(5)   TO ECS-RECORD.ADDRSS-LINE-5
        //*    MOVE #POST-DATA.#ADDRESS-LINE-TXT(6)   TO ECS-RECORD.ADDRSS-LINE-6
        //*    PERFORM SETUP-POSTNET-BARCODE
        //*    EXAMINE PSTA9532.FULL-POSTNET-BAR-CDE 'B' REPLACE WITH '*'
        //*    EXAMINE PSTA9532.FULL-POSTNET-BAR-CDE 'E' REPLACE WITH '*'
        //*    MOVE PSTA9532.FULL-POSTNET-BAR-CDE     TO ECS-RECORD.POSTNET-BARCODE
        ldaRidl504.getEcs_Record_Pckge_Cde().setValue(pdaRida544.getPnd_Post_Data_Pnd_Package_Code());                                                                    //Natural: MOVE #POST-DATA.#PACKAGE-CODE TO ECS-RECORD.PCKGE-CDE
        ldaRidl504.getEcs_Record_Letter_Dte().setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO ECS-RECORD.LETTER-DTE
        //*    IF #POST-DATA.#ADDRESS-TYPE-CDE NE 'U'  /* OTHER THAN DOMESTIC
        //*      MOVE '8' TO ECS-RECORD.BYTE2
        //*    ELSE
        //*      MOVE '0' TO ECS-RECORD.BYTE2
        //*    END-IF
        ldaRidl504.getEcs_Record_Byte4().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE4
        ldaRidl504.getEcs_Record_Byte5().setValue("0");                                                                                                                   //Natural: MOVE '0' TO ECS-RECORD.BYTE5
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-MI-RECORD
    }
    private void sub_Create_Rm_Record() throws Exception                                                                                                                  //Natural: CREATE-RM-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Rm_Record_Type().setValue("RM");                                                                                                         //Natural: MOVE 'RM' TO ECS-RECORD.RM-RECORD-TYPE
        short decideConditionsMet554 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #LETTER-TYPE;//Natural: VALUE '1'
        if (condition((pdaRida544.getPnd_Post_Data_Pnd_Letter_Type().equals("1"))))
        {
            decideConditionsMet554++;
            if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Oregon().equals(1)))                                                                                        //Natural: IF #CTR-OREGON = 1
            {
                ldaRidl504.getEcs_Record_Rm_Cover_Letter_Code().setValue("SVCOMM");                                                                                       //Natural: MOVE 'SVCOMM' TO ECS-RECORD.RM-COVER-LETTER-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaRidl504.getEcs_Record_Rm_Cover_Letter_Code().setValue("SVEQWMM");                                                                                      //Natural: MOVE 'SVEQWMM' TO ECS-RECORD.RM-COVER-LETTER-CODE
            }                                                                                                                                                             //Natural: END-IF
            ldaRidl504.getEcs_Record_Rm_Signatory_Name().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory);                                                              //Natural: MOVE #MM-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
            ldaRidl504.getEcs_Record_Rm_Signatory_Title1().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1);                                                               //Natural: MOVE #MM-TITLE1 TO ECS-RECORD.RM-SIGNATORY-TITLE1
            ldaRidl504.getEcs_Record_Rm_Signatory_Title2().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2);                                                               //Natural: MOVE #MM-TITLE2 TO ECS-RECORD.RM-SIGNATORY-TITLE2
            ldaRidl504.getEcs_Record_Rm_Signatory_Title3().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3);                                                               //Natural: MOVE #MM-TITLE3 TO ECS-RECORD.RM-SIGNATORY-TITLE3
            ldaRidl504.getEcs_Record_Rm_Signatory_Title4().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4);                                                               //Natural: MOVE #MM-TITLE4 TO ECS-RECORD.RM-SIGNATORY-TITLE4
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((pdaRida544.getPnd_Post_Data_Pnd_Letter_Type().equals("2"))))
        {
            decideConditionsMet554++;
            //*        IF #CTR-OREGON = 1                     /* START  >>>  LS1
            //*          MOVE 'SVCOBMM'  TO ECS-RECORD.RM-COVER-LETTER-CODE
            //*        ELSE
            //* *        MOVE 'SVEQBWMM' TO ECS-RECORD.RM-COVER-LETTER-CODE
            //*        END-IF
            //*        MOVE #SV-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
            //*        MOVE #SV-TITLE1    TO ECS-RECORD.RM-SIGNATORY-TITLE1
            //*        MOVE #SV-TITLE2    TO ECS-RECORD.RM-SIGNATORY-TITLE2
            //*        MOVE #SV-TITLE3    TO ECS-RECORD.RM-SIGNATORY-TITLE3
            //*        MOVE #SV-TITLE4    TO ECS-RECORD.RM-SIGNATORY-TITLE4
            ldaRidl504.getEcs_Record_Rm_Cover_Letter_Code().setValue("SVRCV2MM");                                                                                         //Natural: MOVE 'SVRCV2MM' TO ECS-RECORD.RM-COVER-LETTER-CODE
            ldaRidl504.getEcs_Record_Rm_Signatory_Name().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Signatory);                                                              //Natural: MOVE #MM-SIGNATORY TO ECS-RECORD.RM-SIGNATORY-NAME
            ldaRidl504.getEcs_Record_Rm_Signatory_Title1().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title1);                                                               //Natural: MOVE #MM-TITLE1 TO ECS-RECORD.RM-SIGNATORY-TITLE1
            ldaRidl504.getEcs_Record_Rm_Signatory_Title2().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title2);                                                               //Natural: MOVE #MM-TITLE2 TO ECS-RECORD.RM-SIGNATORY-TITLE2
            ldaRidl504.getEcs_Record_Rm_Signatory_Title3().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title3);                                                               //Natural: MOVE #MM-TITLE3 TO ECS-RECORD.RM-SIGNATORY-TITLE3
            //*  << LS1
            ldaRidl504.getEcs_Record_Rm_Signatory_Title4().setValue(pnd_Pst_Tbl_Data_Field2_Pnd_Mm_Title4);                                                               //Natural: MOVE #MM-TITLE4 TO ECS-RECORD.RM-SIGNATORY-TITLE4
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaRidl504.getEcs_Record_Rm_Stable_Value_Ind().setValue(pdaRida544.getPnd_Post_Data_Pnd_Inst_Sv_Lob());                                                           //Natural: MOVE #INST-SV-LOB TO ECS-RECORD.RM-STABLE-VALUE-IND
        ldaRidl504.getEcs_Record_Rm_Institution_Name().setValue(pdaRida544.getPnd_Post_Data_Pnd_Institution_Name());                                                      //Natural: MOVE #POST-DATA.#INSTITUTION-NAME TO ECS-RECORD.RM-INSTITUTION-NAME
        ldaRidl504.getEcs_Record_Rm_Cont_Num().setValue(pnd_Contract_Combo);                                                                                              //Natural: MOVE #CONTRACT-COMBO TO ECS-RECORD.RM-CONT-NUM
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-RM-RECORD
    }
    private void sub_Create_Ap_Record() throws Exception                                                                                                                  //Natural: CREATE-AP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Ap_Record_Type().setValue("AP");                                                                                                         //Natural: MOVE 'AP' TO ECS-RECORD.AP-RECORD-TYPE
        ldaRidl504.getEcs_Record_Ap_Cont_Num().setValue(pnd_Contract_Combo);                                                                                              //Natural: MOVE #CONTRACT-COMBO TO ECS-RECORD.AP-CONT-NUM
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-AP-RECORD
    }
    private void sub_Create_Sp_Record() throws Exception                                                                                                                  //Natural: CREATE-SP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Sp_Record_Type().setValue("SP");                                                                                                         //Natural: MOVE 'SP' TO ECS-RECORD.SP-RECORD-TYPE
        ldaRidl504.getEcs_Record_Sp_Stapling_Ind().setValue("Y");                                                                                                         //Natural: MOVE 'Y' TO ECS-RECORD.SP-STAPLING-IND
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
        sub_Write_Ecs_Out_Data();
        if (condition(Global.isEscape())) {return;}
        //*  CREATE-AP-RECORD
    }
    private void sub_Create_Cd_Record() throws Exception                                                                                                                  //Natural: CREATE-CD-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Cd_Record_Type().setValue("CD");                                                                                                         //Natural: MOVE 'CD' TO ECS-RECORD.CD-RECORD-TYPE
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaRida544.getPnd_Post_Data_Pnd_Contracts().getValue(pnd_I).notEquals(" ")))                                                                    //Natural: IF #CONTRACTS ( #I ) NE ' '
            {
                ldaRidl504.getEcs_Record_Cd_Contract_Number().setValue(pdaRida544.getPnd_Post_Data_Pnd_Contracts().getValue(pnd_I));                                      //Natural: MOVE #CONTRACTS ( #I ) TO ECS-RECORD.CD-CONTRACT-NUMBER
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CREATE-CD-RECORD
    }
    private void sub_Create_Ep_Record() throws Exception                                                                                                                  //Natural: CREATE-EP-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Ep_Record_Type().setValue("EP");                                                                                                         //Natural: MOVE 'EP' TO ECS-RECORD.EP-RECORD-TYPE
        if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Tiaa_Ra().equals(1)))                                                                                           //Natural: IF #CTR-TIAA-RA EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TEW_E1");                                                                                                //Natural: MOVE 'RDR_TEW_E1' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc().equals(1)))                                                                                       //Natural: IF #CTR-TIAA-GRA-RC EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TEW_CRT_E1");                                                                                            //Natural: MOVE 'RDR_TEW_CRT_E1' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh().equals(1)))                                                                                        //Natural: IF #CTR-TIAA-RA-NH EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TEW_E1_NH");                                                                                             //Natural: MOVE 'RDR_TEW_E1_NH' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh().equals(1)))                                                                                    //Natural: IF #CTR-TIAA-GRA-RC-NH EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_TEW_CRT_E1_NH");                                                                                         //Natural: MOVE 'RDR_TEW_CRT_E1_NH' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cref().equals(1)))                                                                                              //Natural: IF #CTR-CREF EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CEW_CRT_E1");                                                                                            //Natural: MOVE 'RDR_CEW_CRT_E1' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cref_Il().equals(1)))                                                                                           //Natural: IF #CTR-CREF-IL EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CEW_CRT_E1_IL");                                                                                         //Natural: MOVE 'RDR_CEW_CRT_E1_IL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl().equals(1)))                                                                                        //Natural: IF #CTR-CREF-RA-FL EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_CEW_CRT_E1_FL");                                                                                         //Natural: MOVE 'RDR_CEW_CRT_E1_FL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl().equals(1)))                                                                                    //Natural: IF #CTR-CREF-GRA-RC-FL EQ 1
        {
            ldaRidl504.getEcs_Record_Ep_Pdf_Name().setValue("RDR_GCEW_CRT_E1_FL");                                                                                        //Natural: MOVE 'RDR_GCEW_CRT_E1_FL' TO ECS-RECORD.EP-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
            sub_Write_Ecs_Out_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-EP-RECORD
    }
    private void sub_Create_Es_Record() throws Exception                                                                                                                  //Natural: CREATE-ES-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------
        ldaRidl504.getEcs_Record_Ecs_Out_Data().getValue("*").reset();                                                                                                    //Natural: RESET ECS-OUT-DATA ( * )
        ldaRidl504.getEcs_Record_Es_Record_Type().setValue("ES");                                                                                                         //Natural: MOVE 'ES' TO ECS-RECORD.ES-RECORD-TYPE
        //*  START >>> LS1
        if (condition(pdaRida544.getPnd_Post_Data_Pnd_Inst_Sv_Lob().equals("A")))                                                                                         //Natural: IF #INST-SV-LOB = 'A'
        {
            short decideConditionsMet685 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CTR-CERT-GN EQ 1
            if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Gn().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2");                                                                                          //Natural: MOVE 'RDR_SV_CERT2' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-CA EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Ca().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_CA");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_CA' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-OK EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Ok().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_OK");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_OK' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-NC EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Nc().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_NC");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_NC' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-LA EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_La().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_LA");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_LA' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-MN EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Mn().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_MN");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_MN' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-MT EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Mt().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_MT");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_MT' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-NE EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Ne().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_NE");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_NE' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-VA EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Va().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_VA");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_VA' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-NH EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Nh().equals(1)))
            {
                decideConditionsMet685++;
                //*  LS1
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2");                                                                                          //Natural: MOVE 'RDR_SV_CERT2' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-FL EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Fl().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_FL");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_FL' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-NM EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Nm().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_NM");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_NM' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-NY EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Ny().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_NY");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_NY' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-PR EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Pr().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_PR");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_PR' TO ECS-RECORD.ES-PDF-NAME
                //*  LS1A >>>
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-OR EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Or().equals(1)))
            {
                decideConditionsMet685++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT2_OR");                                                                                       //Natural: MOVE 'RDR_SV_CERT2_OR' TO ECS-RECORD.ES-PDF-NAME
                //*  LS1A  <<<
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet738 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CTR-CERT-GN EQ 1
            if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Gn().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1");                                                                                          //Natural: MOVE 'RDR_SV_CERT1' TO ECS-RECORD.ES-PDF-NAME
                //*  LS1A >>>
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-OR EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Or().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1");                                                                                          //Natural: MOVE 'RDR_SV_CERT1' TO ECS-RECORD.ES-PDF-NAME
                //*  LS1A <<<
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-CA EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Ca().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1_CA");                                                                                       //Natural: MOVE 'RDR_SV_CERT1_CA' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-OK EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Ok().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1_OK");                                                                                       //Natural: MOVE 'RDR_SV_CERT1_OK' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-NC EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Nc().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1_NC");                                                                                       //Natural: MOVE 'RDR_SV_CERT1_NC' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-LA EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_La().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1_LA");                                                                                       //Natural: MOVE 'RDR_SV_CERT1_LA' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-MN EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Mn().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1_MN");                                                                                       //Natural: MOVE 'RDR_SV_CERT1_MN' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-MT EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Mt().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1_MT");                                                                                       //Natural: MOVE 'RDR_SV_CERT1_MT' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-NE EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Ne().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1_NE");                                                                                       //Natural: MOVE 'RDR_SV_CERT1_NE' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-VA EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Va().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1_VA");                                                                                       //Natural: MOVE 'RDR_SV_CERT1_VA' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-NH EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Nh().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1_NH");                                                                                       //Natural: MOVE 'RDR_SV_CERT1_NH' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-FL EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Fl().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1_FL");                                                                                       //Natural: MOVE 'RDR_SV_CERT1_FL' TO ECS-RECORD.ES-PDF-NAME
                //*  GENERIC
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-NM EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Nm().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1");                                                                                          //Natural: MOVE 'RDR_SV_CERT1' TO ECS-RECORD.ES-PDF-NAME
                //*  GENERIC
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-NY EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Ny().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1");                                                                                          //Natural: MOVE 'RDR_SV_CERT1' TO ECS-RECORD.ES-PDF-NAME
                //*  GENERIC
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN #CTR-CERT-PR EQ 1
            else if (condition(pdaRida544.getPnd_Post_Data_Pnd_Ctr_Cert_Pr().equals(1)))
            {
                decideConditionsMet738++;
                ldaRidl504.getEcs_Record_Es_Pdf_Name().setValue("RDR_SV_CERT1");                                                                                          //Natural: MOVE 'RDR_SV_CERT1' TO ECS-RECORD.ES-PDF-NAME
                                                                                                                                                                          //Natural: PERFORM WRITE-ECS-OUT-DATA
                sub_Write_Ecs_Out_Data();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  IF #INST-SV-LOB =  'A'
        }                                                                                                                                                                 //Natural: END-IF
        //*       IF #CTR-CERT-GN EQ 1
        //*          MOVE 'RDR_SV_CERT1' TO ECS-RECORD.ES-PDF-NAME
        //*          PERFORM WRITE-ECS-OUT-DATA
        //*        END-IF
        //*       IF #CTR-CERT-CA EQ 1
        //*          MOVE 'RDR_SV_CERT1_CA' TO ECS-RECORD.ES-PDF-NAME
        //*          PERFORM WRITE-ECS-OUT-DATA
        //*       END-IF
        //*       IF #CTR-CERT-OK EQ 1
        //*          MOVE 'RDR_SV_CERT1_OK' TO ECS-RECORD.ES-PDF-NAME
        //*          PERFORM WRITE-ECS-OUT-DATA
        //*        END-IF
        //*        IF #CTR-CERT-NC EQ 1
        //*           MOVE 'RDR_SV_CERT1_NC' TO ECS-RECORD.ES-PDF-NAME
        //*           PERFORM WRITE-ECS-OUT-DATA
        //*        END-IF
        //*        IF #CTR-CERT-LA EQ 1
        //*           MOVE 'RDR_SV_CERT1_LA' TO ECS-RECORD.ES-PDF-NAME
        //*           PERFORM WRITE-ECS-OUT-DATA
        //*        END-IF
        //*    IF #CTR-CERT-MN EQ 1
        //*      MOVE 'RDR_SV_CERT1_MN' TO ECS-RECORD.ES-PDF-NAME
        //*      PERFORM WRITE-ECS-OUT-DATA
        //*    END-IF
        //*    IF #CTR-CERT-MT EQ 1
        //*      MOVE 'RDR_SV_CERT1_MT' TO ECS-RECORD.ES-PDF-NAME
        //*      PERFORM WRITE-ECS-OUT-DATA
        //*    END-IF
        //*    IF #CTR-CERT-NE EQ 1
        //*      MOVE 'RDR_SV_CERT1_NE' TO ECS-RECORD.ES-PDF-NAME
        //*      PERFORM WRITE-ECS-OUT-DATA
        //*    END-IF
        //*    IF #CTR-CERT-VA EQ 1
        //*      MOVE 'RDR_SV_CERT1_VA' TO ECS-RECORD.ES-PDF-NAME
        //*      PERFORM WRITE-ECS-OUT-DATA
        //*    END-IF
        //*    IF #CTR-CERT-FL EQ 1
        //*      MOVE 'RDR_SV_CERT1_FL' TO ECS-RECORD.ES-PDF-NAME
        //*      PERFORM WRITE-ECS-OUT-DATA
        //*    END-IF
        //*    IF #CTR-CERT-NH EQ 1
        //*      MOVE 'RDR_SV_CERT1_NH' TO ECS-RECORD.ES-PDF-NAME
        //*      PERFORM WRITE-ECS-OUT-DATA
        //*    END-IF                                     /* END <<<     LS1
        //*  CREATE-ES-RECORD
    }
    private void sub_Write_Ecs_Out_Data() throws Exception                                                                                                                //Natural: WRITE-ECS-OUT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        getWorkFiles().write(2, false, ldaRidl504.getEcs_Record());                                                                                                       //Natural: WRITE WORK FILE 2 ECS-RECORD
        //*  WRITE-ECS-OUT-DATA
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, Global.getPROGRAM(),"IN PROCESS-ERROR","SUBROUTINE",pdaRida544.getPnd_Post_Data_Pnd_Pst_Rqst_Id());                                     //Natural: WRITE ( 0 ) *PROGRAM 'IN PROCESS-ERROR' 'SUBROUTINE' #POST-DATA.#PST-RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  PSTA9531.#POST-DATA-STATUS := PSTL9620.GEN-FAILED
        //*  PSTA9531.TERMINATE-IND    := TRUE
        getReports().print(0, Global.getPROGRAM(),"JOB TERMINATED! NO",pnd_Appl_Title,"CREATED.",NEWLINE,"SEE MESSAGES ABOVE FOR FURTHER INFORMATION");                   //Natural: PRINT ( 0 ) *PROGRAM 'JOB TERMINATED! NO' #APPL-TITLE 'CREATED.' / 'SEE MESSAGES ABOVE FOR FURTHER INFORMATION'
        Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                                  //Natural: ESCAPE BOTTOM
        if (true) return;
        //*  PROCESS-ERROR
    }
    private void sub_Format_Date() throws Exception                                                                                                                       //Natural: FORMAT-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -----------------------------
        pnd_Work_Month.setValueEdited(pnd_Work_Date_D,new ReportEditMask("LLLLLLLLLLL"));                                                                                 //Natural: MOVE EDITED #WORK.DATE-D ( EM = L ( 11 ) ) TO #WORK.MONTH
        pnd_Work_Day_Temp.setValueEdited(pnd_Work_Date_D,new ReportEditMask("ZD"));                                                                                       //Natural: MOVE EDITED #WORK.DATE-D ( EM = ZD ) TO #WORK.DAY-TEMP
        pnd_Work_Day.setValue(pnd_Work_Day_Temp, MoveOption.LeftJustified);                                                                                               //Natural: MOVE LEFT #WORK.DAY-TEMP TO #WORK.DAY
        pnd_Work_Ccyy.setValueEdited(pnd_Work_Date_D,new ReportEditMask("YYYY"));                                                                                         //Natural: MOVE EDITED #WORK.DATE-D ( EM = YYYY ) TO #WORK.CCYY
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Month));                                                                //Natural: COMPRESS #WORK.MONTH INTO #WORK.FORMATTED-DATE LEAVING NO
        pnd_Work_Formatted_Date.setValue(DbsUtil.compress(pnd_Work_Formatted_Date, pnd_Work_Ccyy));                                                                       //Natural: COMPRESS #WORK.FORMATTED-DATE #WORK.CCYY INTO #WORK.FORMATTED-DATE
        //*  FORMAT-DATE
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, " AN ERROR OCCURRED AND PROGRAM IS TERMINATED",NEWLINE,NEWLINE,NEWLINE,"PROGRAM = ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER = ",          //Natural: WRITE ' AN ERROR OCCURRED AND PROGRAM IS TERMINATED' // / 'PROGRAM = ' *PROGRAM / 'ERROR NUMBER = ' *ERROR-NR / 'ERROR LINE =' *ERROR-LINE
            Global.getERROR_NR(),NEWLINE,"ERROR LINE =",Global.getERROR_LINE());
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
    }
}
