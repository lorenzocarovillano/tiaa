/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:01:58 AM
**        * FROM NATURAL SUBPROGRAM : Pstn7225
************************************************************
**        * FILE NAME            : Pstn7225.java
**        * CLASS NAME           : Pstn7225
**        * INSTANCE NAME        : Pstn7225
************************************************************
**SAG GENERATOR: OBJECT-SUBP-TIAA                 VERSION: 3.2.2
**SAG TITLE: MAINTAIN PACKAGES
**SAG SYSTEM: PST
**SAG OBJECT-DESC: PACKAGE
**SAG PRIME-FILE: PST-STNDRD-PCKGE
**SAG PRIME-KEY: PCKGE-CDE-VRSN-CDE
**SAG HOLD-FIELD: PCKGE-UPDTE-DTE-TME
**SAG OBJECT-PDA: PSTA7220
**SAG RESTRICTED-PDA: PSTA7221
**SAG DESCS(1): THIS SUBPROGRAM IS USED TO PERFORM OBJECT MAINTENANCE
**SAG DESCS(2): FOR POST PACKAGES
**SAG DESCS(3): THIS MODULE SUPERCEEDES PSTN7200
************************************************************************
* PROGRAM  : PSTN7225
* SYSTEM   : PST
* TITLE    : MAINTAIN PACKAGES
* GENERATED: MAY 5, 1999
* FUNCTION : THIS SUBPROGRAM IS USED TO PERFORM OBJECT MAINTENANCE
*            FOR POST PACKAGES
*            THIS MODULE SUPERCEEDES CLONED FORM PSTN7210 USED IN POST
*            PURGE PSTB5151 VIA CALLNAT TO PSTN7220
*
* HISTORY
**SAG DEFINE EXIT CHANGE-HISTORY
*
*   WHO      WHEN              WHY
* -------- -------- ----------------------------------------------------
*
* GRAF     05/99    NEW MODULE.
*                   ADDED CODE TO HANDLE EDITS OF PURGE DAYS PARMS
*
* LAURENC  04/01    ADDED 'M' PRINT FACILITY CODE.
*
* MCKEEVE  11/04     ADD 'T' FACILITY CODE FOR COMPUSET PCL TOPS.
* AVE      02/05     RE-STOW CHANGES MADE BY T. MCKEEVER
*
* LAURENC  10/08    ADDED NEW ECS CODE 'E'
*
**SAG END-EXIT
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Pstn7225 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaPsta7220 pdaPsta7220;
    private PdaPsta7221 pdaPsta7221;
    private PdaCdaobj pdaCdaobj;
    private PdaPstpda_D pdaPstpda_D;
    private PdaPstpda_M pdaPstpda_M;
    private PdaPstpda_P pdaPstpda_P;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Current_Field;
    private DbsField pnd_Db_Call;
    private DbsField pnd_D1;
    private DbsField pnd_D2;
    private DbsField pnd_Object;
    private DbsField pnd_Old_Rec;
    private DbsField pnd_Save_Rec;
    private DbsField pnd_Update_Performed;

    private DataAccessProgramView vw_next_View;
    private DbsField next_View_Pckge_Cde_Vrsn_Cde;

    private DataAccessProgramView vw_pst_Stndrd_Pckge;
    private DbsField pst_Stndrd_Pckge_Pckge_Cde;
    private DbsField pst_Stndrd_Pckge_Pckge_Vrsn_Cde;
    private DbsField pst_Stndrd_Pckge_Pckge_Short_Nme;
    private DbsField pst_Stndrd_Pckge_Pckge_Owner_Unit_Cde;
    private DbsGroup pst_Stndrd_Pckge_Pckge_Dscrptn_TxtMuGroup;
    private DbsField pst_Stndrd_Pckge_Pckge_Dscrptn_Txt;
    private DbsField pst_Stndrd_Pckge_Pckge_Finish_Cde;
    private DbsField pst_Stndrd_Pckge_Pckge_Post_Vrsn_Cde;
    private DbsField pst_Stndrd_Pckge_Pckge_Updte_Dte_Tme;
    private DbsField pst_Stndrd_Pckge_Pckge_Updte_Oprtr_Cde;
    private DbsField pst_Stndrd_Pckge_Pckge_File_Copy_Ind;
    private DbsField pst_Stndrd_Pckge_Pckge_Mit_Ctgry;
    private DbsField pst_Stndrd_Pckge_Pckge_Mit_Clss;
    private DbsField pst_Stndrd_Pckge_Pckge_Mit_Spcfc;
    private DbsField pst_Stndrd_Pckge_Pckge_Csf_Tble_Gnrtn_Mdle;
    private DbsField pst_Stndrd_Pckge_Pckge_Csf_Dta_Gnrtn_Mdle;
    private DbsField pst_Stndrd_Pckge_Pckge_Csf_File_Name;
    private DbsField pst_Stndrd_Pckge_Pckge_Cmbne_Ind;
    private DbsField pst_Stndrd_Pckge_Pckge_Immdte_Prnt_Ind;
    private DbsField pst_Stndrd_Pckge_Pckge_Image_Ind;
    private DbsField pst_Stndrd_Pckge_Pckge_Image_Ctgry;
    private DbsField pst_Stndrd_Pckge_Pckge_Image_Clss;
    private DbsField pst_Stndrd_Pckge_Pckge_Image_Spcfc;
    private DbsField pst_Stndrd_Pckge_Pckge_Dflt_Dlvry_Typ;
    private DbsField pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde;

    private DbsGroup pst_Stndrd_Pckge_Purge_Dtls;
    private DbsField pst_Stndrd_Pckge_Purge_Status_Cde;
    private DbsField pst_Stndrd_Pckge_Purge_Days;
    private DbsField pst_Stndrd_Pckge_Gnrt_Jcl_Ovrrde;
    private DbsField pst_Stndrd_Pckge_Rprnt_Jcl_Ovrrde;
    private DbsField pst_Stndrd_Pckge_Rimge_Jcl_Override;

    private DbsGroup pst_Stndrd_Pckge_Deliveryprofiles;
    private DbsField pst_Stndrd_Pckge_Pckge_Delivery_Type;
    private DbsField pst_Stndrd_Pckge_Fnsh_Lctn;
    private DbsField pst_Stndrd_Pckge_Dflt_Dlvry_Ind;
    private DbsField pst_Stndrd_Pckge_Fnsh_Prfle;

    private DataAccessProgramView vw_pst_Finish_Profile_Pckge_Rl;

    private DbsGroup efsn9015;
    private DbsField efsn9015_Pnd_Dcmnt_Dscrptn;
    private DbsField efsn9015_Pnd_Default_Retention;
    private DbsField efsn9015_Pnd_Actual_Retention;
    private DbsField pnd_Nbr;
    private DbsField pnd_A;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaPsta7220 = new PdaPsta7220(parameters);
        pdaPsta7221 = new PdaPsta7221(parameters);
        pdaCdaobj = new PdaCdaobj(parameters);
        pdaPstpda_D = new PdaPstpda_D(parameters);
        pdaPstpda_M = new PdaPstpda_M(parameters);
        pdaPstpda_P = new PdaPstpda_P(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Current_Field = localVariables.newFieldInRecord("pnd_Current_Field", "#CURRENT-FIELD", FieldType.STRING, 32);
        pnd_Db_Call = localVariables.newFieldInRecord("pnd_Db_Call", "#DB-CALL", FieldType.STRING, 1);
        pnd_D1 = localVariables.newFieldInRecord("pnd_D1", "#D1", FieldType.PACKED_DECIMAL, 3);
        pnd_D2 = localVariables.newFieldInRecord("pnd_D2", "#D2", FieldType.PACKED_DECIMAL, 3);
        pnd_Object = localVariables.newFieldInRecord("pnd_Object", "#OBJECT", FieldType.STRING, 20);
        pnd_Old_Rec = localVariables.newFieldInRecord("pnd_Old_Rec", "#OLD-REC", FieldType.BOOLEAN, 1);
        pnd_Save_Rec = localVariables.newFieldInRecord("pnd_Save_Rec", "#SAVE-REC", FieldType.BOOLEAN, 1);
        pnd_Update_Performed = localVariables.newFieldInRecord("pnd_Update_Performed", "#UPDATE-PERFORMED", FieldType.BOOLEAN, 1);

        vw_next_View = new DataAccessProgramView(new NameInfo("vw_next_View", "NEXT-VIEW"), "PST_STNDRD_PCKGE", "PST_STNDRD_PCKGE");
        next_View_Pckge_Cde_Vrsn_Cde = vw_next_View.getRecord().newFieldInGroup("next_View_Pckge_Cde_Vrsn_Cde", "PCKGE-CDE-VRSN-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "PCKGE_CDE_VRSN_CDE");
        next_View_Pckge_Cde_Vrsn_Cde.setSuperDescriptor(true);
        registerRecord(vw_next_View);

        vw_pst_Stndrd_Pckge = new DataAccessProgramView(new NameInfo("vw_pst_Stndrd_Pckge", "PST-STNDRD-PCKGE"), "PST_STNDRD_PCKGE", "PST_STNDRD_PCKGE", 
            DdmPeriodicGroups.getInstance().getGroups("PST_STNDRD_PCKGE"));
        pst_Stndrd_Pckge_Pckge_Cde = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Cde", "PCKGE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "PCKGE_CDE");
        pst_Stndrd_Pckge_Pckge_Cde.setDdmHeader("PACKAGE/CODE");
        pst_Stndrd_Pckge_Pckge_Vrsn_Cde = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Vrsn_Cde", "PCKGE-VRSN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "PCKGE_VRSN_CDE");
        pst_Stndrd_Pckge_Pckge_Vrsn_Cde.setDdmHeader("PACKAGE/TYPE/CODE");
        pst_Stndrd_Pckge_Pckge_Short_Nme = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Short_Nme", "PCKGE-SHORT-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "PCKGE_SHORT_NME");
        pst_Stndrd_Pckge_Pckge_Short_Nme.setDdmHeader("PACKAGE/NAME");
        pst_Stndrd_Pckge_Pckge_Owner_Unit_Cde = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Owner_Unit_Cde", "PCKGE-OWNER-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PCKGE_OWNER_UNIT_CDE");
        pst_Stndrd_Pckge_Pckge_Owner_Unit_Cde.setDdmHeader("OWNER/UNIT");
        pst_Stndrd_Pckge_Pckge_Dscrptn_TxtMuGroup = vw_pst_Stndrd_Pckge.getRecord().newGroupInGroup("PST_STNDRD_PCKGE_PCKGE_DSCRPTN_TXTMuGroup", "PCKGE_DSCRPTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "PST_STNDRD_PCKGE_PCKGE_DSCRPTN_TXT");
        pst_Stndrd_Pckge_Pckge_Dscrptn_Txt = pst_Stndrd_Pckge_Pckge_Dscrptn_TxtMuGroup.newFieldArrayInGroup("pst_Stndrd_Pckge_Pckge_Dscrptn_Txt", "PCKGE-DSCRPTN-TXT", 
            FieldType.STRING, 30, new DbsArrayController(1, 6), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "PCKGE_DSCRPTN_TXT");
        pst_Stndrd_Pckge_Pckge_Dscrptn_Txt.setDdmHeader("PACKAGE/DESCRIPTION");
        pst_Stndrd_Pckge_Pckge_Finish_Cde = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Finish_Cde", "PCKGE-FINISH-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "PCKGE_FINISH_CDE");
        pst_Stndrd_Pckge_Pckge_Post_Vrsn_Cde = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Post_Vrsn_Cde", "PCKGE-POST-VRSN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "PCKGE_POST_VRSN_CDE");
        pst_Stndrd_Pckge_Pckge_Updte_Dte_Tme = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Updte_Dte_Tme", "PCKGE-UPDTE-DTE-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "PCKGE_UPDTE_DTE_TME");
        pst_Stndrd_Pckge_Pckge_Updte_Dte_Tme.setDdmHeader("LAST/UPDATED");
        pst_Stndrd_Pckge_Pckge_Updte_Oprtr_Cde = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Updte_Oprtr_Cde", "PCKGE-UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PCKGE_UPDTE_OPRTR_CDE");
        pst_Stndrd_Pckge_Pckge_Updte_Oprtr_Cde.setDdmHeader("UPDATED/BY");
        pst_Stndrd_Pckge_Pckge_File_Copy_Ind = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_File_Copy_Ind", "PCKGE-FILE-COPY-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "PCKGE_FILE_COPY_IND");
        pst_Stndrd_Pckge_Pckge_File_Copy_Ind.setDdmHeader("FILE/COPY/IND");
        pst_Stndrd_Pckge_Pckge_Mit_Ctgry = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Mit_Ctgry", "PCKGE-MIT-CTGRY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PCKGE_MIT_CTGRY");
        pst_Stndrd_Pckge_Pckge_Mit_Ctgry.setDdmHeader("MIT/CAT");
        pst_Stndrd_Pckge_Pckge_Mit_Clss = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Mit_Clss", "PCKGE-MIT-CLSS", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PCKGE_MIT_CLSS");
        pst_Stndrd_Pckge_Pckge_Mit_Clss.setDdmHeader("MIT/CLASS");
        pst_Stndrd_Pckge_Pckge_Mit_Spcfc = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Mit_Spcfc", "PCKGE-MIT-SPCFC", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "PCKGE_MIT_SPCFC");
        pst_Stndrd_Pckge_Pckge_Mit_Spcfc.setDdmHeader("MIT/SPECIFIC");
        pst_Stndrd_Pckge_Pckge_Csf_Tble_Gnrtn_Mdle = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Csf_Tble_Gnrtn_Mdle", "PCKGE-CSF-TBLE-GNRTN-MDLE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PCKGE_CSF_TBLE_GNRTN_MDLE");
        pst_Stndrd_Pckge_Pckge_Csf_Tble_Gnrtn_Mdle.setDdmHeader("CSF TABLE/GENERATE/MODULE");
        pst_Stndrd_Pckge_Pckge_Csf_Dta_Gnrtn_Mdle = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Csf_Dta_Gnrtn_Mdle", "PCKGE-CSF-DTA-GNRTN-MDLE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "PCKGE_CSF_DTA_GNRTN_MDLE");
        pst_Stndrd_Pckge_Pckge_Csf_Dta_Gnrtn_Mdle.setDdmHeader("CSF DATA/GENERATE/MODULE");
        pst_Stndrd_Pckge_Pckge_Csf_File_Name = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Csf_File_Name", "PCKGE-CSF-FILE-NAME", 
            FieldType.STRING, 16, RepeatingFieldStrategy.None, "PCKGE_CSF_FILE_NAME");
        pst_Stndrd_Pckge_Pckge_Csf_File_Name.setDdmHeader("PRINT/FCLTY/CDE");
        pst_Stndrd_Pckge_Pckge_Cmbne_Ind = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Cmbne_Ind", "PCKGE-CMBNE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PCKGE_CMBNE_IND");
        pst_Stndrd_Pckge_Pckge_Immdte_Prnt_Ind = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Immdte_Prnt_Ind", "PCKGE-IMMDTE-PRNT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PCKGE_IMMDTE_PRNT_IND");
        pst_Stndrd_Pckge_Pckge_Image_Ind = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Image_Ind", "PCKGE-IMAGE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PCKGE_IMAGE_IND");
        pst_Stndrd_Pckge_Pckge_Image_Ctgry = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Image_Ctgry", "PCKGE-IMAGE-CTGRY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PCKGE_IMAGE_CTGRY");
        pst_Stndrd_Pckge_Pckge_Image_Ctgry.setDdmHeader("PCKG/IMAGE/CAT");
        pst_Stndrd_Pckge_Pckge_Image_Clss = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Image_Clss", "PCKGE-IMAGE-CLSS", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PCKGE_IMAGE_CLSS");
        pst_Stndrd_Pckge_Pckge_Image_Clss.setDdmHeader("PACKAGE/IMAGE CLASS");
        pst_Stndrd_Pckge_Pckge_Image_Spcfc = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Image_Spcfc", "PCKGE-IMAGE-SPCFC", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "PCKGE_IMAGE_SPCFC");
        pst_Stndrd_Pckge_Pckge_Image_Spcfc.setDdmHeader("PACKAGE/IMAGE/SPECIFIC");
        pst_Stndrd_Pckge_Pckge_Dflt_Dlvry_Typ = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Dflt_Dlvry_Typ", "PCKGE-DFLT-DLVRY-TYP", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "PCKGE_DFLT_DLVRY_TYP");
        pst_Stndrd_Pckge_Pckge_Dflt_Dlvry_Typ.setDdmHeader("DEFAULT/DELIVERY/TYPE");
        pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde", "PCKGE-DFLT-PRINT-FCLTY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PCKGE_DFLT_PRINT_FCLTY_CDE");
        pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde.setDdmHeader("DEFAULT/FACILITY/CODE");

        pst_Stndrd_Pckge_Purge_Dtls = vw_pst_Stndrd_Pckge.getRecord().newGroupArrayInGroup("pst_Stndrd_Pckge_Purge_Dtls", "PURGE-DTLS", new DbsArrayController(1, 
            2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PST_STNDRD_PCKGE_PURGE_DTLS");
        pst_Stndrd_Pckge_Purge_Dtls.setDdmHeader("PURGE/DETAILS");
        pst_Stndrd_Pckge_Purge_Status_Cde = pst_Stndrd_Pckge_Purge_Dtls.newFieldInGroup("pst_Stndrd_Pckge_Purge_Status_Cde", "PURGE-STATUS-CDE", FieldType.STRING, 
            8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PURGE_STATUS_CDE", "PST_STNDRD_PCKGE_PURGE_DTLS");
        pst_Stndrd_Pckge_Purge_Status_Cde.setDdmHeader("PURGE/STATUS/CODE");
        pst_Stndrd_Pckge_Purge_Days = pst_Stndrd_Pckge_Purge_Dtls.newFieldInGroup("pst_Stndrd_Pckge_Purge_Days", "PURGE-DAYS", FieldType.PACKED_DECIMAL, 
            3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PURGE_DAYS", "PST_STNDRD_PCKGE_PURGE_DTLS");
        pst_Stndrd_Pckge_Purge_Days.setDdmHeader("DAYS/ON/FILE");
        pst_Stndrd_Pckge_Gnrt_Jcl_Ovrrde = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Gnrt_Jcl_Ovrrde", "GNRT-JCL-OVRRDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "GNRT_JCL_OVRRDE");
        pst_Stndrd_Pckge_Rprnt_Jcl_Ovrrde = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Rprnt_Jcl_Ovrrde", "RPRNT-JCL-OVRRDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RPRNT_JCL_OVRRDE");
        pst_Stndrd_Pckge_Rimge_Jcl_Override = vw_pst_Stndrd_Pckge.getRecord().newFieldInGroup("pst_Stndrd_Pckge_Rimge_Jcl_Override", "RIMGE-JCL-OVERRIDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "RIMGE_JCL_OVERRIDE");

        pst_Stndrd_Pckge_Deliveryprofiles = vw_pst_Stndrd_Pckge.getRecord().newGroupArrayInGroup("pst_Stndrd_Pckge_Deliveryprofiles", "DELIVERYPROFILES", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PST_STNDRD_PCKGE_DELIVERYPROFILES");
        pst_Stndrd_Pckge_Pckge_Delivery_Type = pst_Stndrd_Pckge_Deliveryprofiles.newFieldInGroup("pst_Stndrd_Pckge_Pckge_Delivery_Type", "PCKGE-DELIVERY-TYPE", 
            FieldType.STRING, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PCKGE_DELIVERY_TYPE", "PST_STNDRD_PCKGE_DELIVERYPROFILES");
        pst_Stndrd_Pckge_Fnsh_Lctn = pst_Stndrd_Pckge_Deliveryprofiles.newFieldInGroup("pst_Stndrd_Pckge_Fnsh_Lctn", "FNSH-LCTN", FieldType.STRING, 8, 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "FNSH_LCTN", "PST_STNDRD_PCKGE_DELIVERYPROFILES");
        pst_Stndrd_Pckge_Dflt_Dlvry_Ind = pst_Stndrd_Pckge_Deliveryprofiles.newFieldInGroup("pst_Stndrd_Pckge_Dflt_Dlvry_Ind", "DFLT-DLVRY-IND", FieldType.BOOLEAN, 
            1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "DFLT_DLVRY_IND", "PST_STNDRD_PCKGE_DELIVERYPROFILES");
        pst_Stndrd_Pckge_Fnsh_Prfle = pst_Stndrd_Pckge_Deliveryprofiles.newFieldInGroup("pst_Stndrd_Pckge_Fnsh_Prfle", "FNSH-PRFLE", FieldType.STRING, 
            8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "FNSH_PRFLE", "PST_STNDRD_PCKGE_DELIVERYPROFILES");
        registerRecord(vw_pst_Stndrd_Pckge);

        vw_pst_Finish_Profile_Pckge_Rl = new DataAccessProgramView(new NameInfo("vw_pst_Finish_Profile_Pckge_Rl", "PST-FINISH-PROFILE-PCKGE-RL"), "PST_FINISHING_PROFILE", 
            "PST_FNSHNG_PRFL");
        registerRecord(vw_pst_Finish_Profile_Pckge_Rl);

        efsn9015 = localVariables.newGroupInRecord("efsn9015", "EFSN9015");
        efsn9015_Pnd_Dcmnt_Dscrptn = efsn9015.newFieldInGroup("efsn9015_Pnd_Dcmnt_Dscrptn", "#DCMNT-DSCRPTN", FieldType.STRING, 30);
        efsn9015_Pnd_Default_Retention = efsn9015.newFieldInGroup("efsn9015_Pnd_Default_Retention", "#DEFAULT-RETENTION", FieldType.STRING, 1);
        efsn9015_Pnd_Actual_Retention = efsn9015.newFieldInGroup("efsn9015_Pnd_Actual_Retention", "#ACTUAL-RETENTION", FieldType.STRING, 1);
        pnd_Nbr = localVariables.newFieldInRecord("pnd_Nbr", "#NBR", FieldType.NUMERIC, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_next_View.reset();
        vw_pst_Stndrd_Pckge.reset();
        vw_pst_Finish_Profile_Pckge_Rl.reset();

        localVariables.reset();
        pnd_Object.setInitialValue("Package");
        pnd_Update_Performed.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Pstn7225() throws Exception
    {
        super("Pstn7225");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("PSTN7225", onError);
        //* *SAG DEFINE EXIT START-OF-PROGRAM
        //*  SPECIFY CODE TO BE EXECUTED AT THE BEGINNING OF THE OBJECT SUBPROGRAM.
        //*  THIS MIGHT INCLUDE SECURITY CHECKING LOGIC.
        //* *SAG END-EXIT                                                                                                                                                 //Natural: ON ERROR
        //*  INITIALIZE OUTPUT FIELDS.
        //*  CJS
        pdaCdaobj.getCdaobj_Outputs().reset();                                                                                                                            //Natural: RESET CDAOBJ.OUTPUTS MSG-INFO-SUB
        pdaPstpda_M.getMsg_Info_Sub().reset();
        //*  BUILD UP PRIME KEY OF THE OBJECT.
        pdaPsta7220.getPsta7220_Id_Structure().setValuesByName(pdaPsta7220.getPsta7220());                                                                                //Natural: MOVE BY NAME PSTA7220 TO PSTA7220-ID.STRUCTURE
        //*  SET UP REPEAT TO ALLOW ESCAPE FROM WITHIN A SUBROUTINE.
        PROG:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  DECIDE WHAT TO DO BASED ON FUNCTION SUPPLIED.
            //*  READ THE OBJECT
            short decideConditionsMet361 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE CDAOBJ.#FUNCTION;//Natural: VALUE 'GET'
            if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("GET"))))
            {
                decideConditionsMet361++;
                                                                                                                                                                          //Natural: PERFORM GET-OBJECT
                sub_Get_Object();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SET-OBJECT-ID
                sub_Set_Object_Id();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  READ THE NEXT HIGHER OBJECT
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:displayed successfully");                                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:displayed successfully'
            }                                                                                                                                                             //Natural: VALUE 'NEXT'
            else if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("NEXT"))))
            {
                decideConditionsMet361++;
                vw_next_View.createHistogram                                                                                                                              //Natural: HISTOGRAM ( 2 ) NEXT-VIEW FOR PCKGE-CDE-VRSN-CDE STARTING FROM PSTA7220-ID
                (
                "HIST01",
                "PCKGE_CDE_VRSN_CDE",
                new Wc[] { new Wc("PCKGE_CDE_VRSN_CDE", ">=", pdaPsta7220.getPsta7220_Id(), WcType.WITH) },
                2
                );
                HIST01:
                while (condition(vw_next_View.readNextRow("HIST01")))
                {
                    if (condition(next_View_Pckge_Cde_Vrsn_Cde.notEquals(pdaPsta7220.getPsta7220_Id())))                                                                  //Natural: IF NEXT-VIEW.PCKGE-CDE-VRSN-CDE NE PSTA7220-ID THEN
                    {
                        pdaPsta7220.getPsta7220_Id().setValue(next_View_Pckge_Cde_Vrsn_Cde);                                                                              //Natural: ASSIGN PSTA7220-ID = NEXT-VIEW.PCKGE-CDE-VRSN-CDE
                        pdaPsta7220.getPsta7220().setValuesByName(pdaPsta7220.getPsta7220_Id_Structure());                                                                //Natural: MOVE BY NAME PSTA7220-ID.STRUCTURE TO PSTA7220
                                                                                                                                                                          //Natural: PERFORM GET-OBJECT
                        sub_Get_Object();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(pnd_Object);                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = #OBJECT
                        pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Next:1:displayed");                                                                           //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Next:1:displayed'
                        if (true) break PROG;                                                                                                                             //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-HISTOGRAM
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  NO RECORDS BEYOND CURRENT RECORD.
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("End of data reached");                                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'End of data reached'
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-CDE-VRSN-CDE");                                                                         //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-CDE-VRSN-CDE'
                //*  WARNING.
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("W");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'W'
                //*  MODIFY OR PURGE CURRENT OBJECT
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE IMMEDIATE
            }                                                                                                                                                             //Natural: VALUE 'UPDATE','DELETE'
            else if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("DELETE"))))
            {
                decideConditionsMet361++;
                //*  MAKE SURE THE RECORD WAS HELD PRIOR TO UPDATING.
                if (condition(pdaPsta7220.getPsta7220_Id().notEquals(pdaPsta7221.getPsta7221_Held_Id())))                                                                 //Natural: IF PSTA7220-ID NE PSTA7221.HELD-ID THEN
                {
                    pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Attempted to update/delete:1:that was not in held status");                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Attempted to update/delete:1:that was not in held status'
                    pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("#FUNCTION");                                                                              //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = '#FUNCTION'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                    sub_Process_Error();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM HOLD-OBJECT
                sub_Hold_Object();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SET-OBJECT-ID
                sub_Set_Object_Id();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE")))                                                                                       //Natural: IF CDAOBJ.#FUNCTION = 'UPDATE'
                {
                                                                                                                                                                          //Natural: PERFORM CLEAR
                    sub_Clear();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(! (pdaCdaobj.getCdaobj_Pnd_Clear_After_Update().getBoolean())))                                                                         //Natural: IF NOT CDAOBJ.#CLEAR-AFTER-UPDATE THEN
                    {
                        //*  GET NEW COPY OF OBJECT IF CLEAR NOT REQ.
                                                                                                                                                                          //Natural: PERFORM GET-OBJECT
                        sub_Get_Object();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PROG"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Update_Performed.setValue(true);                                                                                                                  //Natural: ASSIGN #UPDATE-PERFORMED = TRUE
                    pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:modified successfully");                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:modified successfully'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaCdaobj.getCdaobj_Pnd_Clear_After_Update().getBoolean()))                                                                             //Natural: IF CDAOBJ.#CLEAR-AFTER-UPDATE THEN
                    {
                        //*  CLEAR SCREEN AFTER PURGE IF REQUESTED.
                                                                                                                                                                          //Natural: PERFORM CLEAR
                        sub_Clear();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PROG"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Update_Performed.setValue(true);                                                                                                                  //Natural: ASSIGN #UPDATE-PERFORMED = TRUE
                    pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:purged successfully");                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:purged successfully'
                    //*  EXISTENCE CHECK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'EXISTS'
            else if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("EXISTS"))))
            {
                decideConditionsMet361++;
                                                                                                                                                                          //Natural: PERFORM CHECK-EXISTENCE
                sub_Check_Existence();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ADD NEW OBJECT
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE IMMEDIATE
            }                                                                                                                                                             //Natural: VALUE 'STORE'
            else if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE"))))
            {
                decideConditionsMet361++;
                                                                                                                                                                          //Natural: PERFORM CREATE-OBJECT
                sub_Create_Object();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SET-OBJECT-ID
                sub_Set_Object_Id();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CLEAR
                sub_Clear();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(! (pdaCdaobj.getCdaobj_Pnd_Clear_After_Update().getBoolean())))                                                                             //Natural: IF NOT CDAOBJ.#CLEAR-AFTER-UPDATE THEN
                {
                    //*  GET NEW COPY OF OBJECT IF CLEAR NOT REQ.
                                                                                                                                                                          //Natural: PERFORM GET-OBJECT
                    sub_Get_Object();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PROG"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Update_Performed.setValue(true);                                                                                                                      //Natural: ASSIGN #UPDATE-PERFORMED = TRUE
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:added successfully");                                                                              //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:added successfully'
            }                                                                                                                                                             //Natural: VALUE 'INITIALIZE'
            else if (condition((pdaCdaobj.getCdaobj_Pnd_Function().equals("INITIALIZE"))))
            {
                decideConditionsMet361++;
                                                                                                                                                                          //Natural: PERFORM CLEAR
                sub_Clear();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *SAG DEFINE EXIT USER-DEFINED-FUNCTIONS
                //* *SAG END-EXIT
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet361 > 0))
            {
                //*  ALL DONE.
                //*  UNKNOWN ACTION CODE
                if (true) break PROG;                                                                                                                                     //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(pnd_Object);                                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = #OBJECT
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2).setValue(pdaCdaobj.getCdaobj_Pnd_Function());                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 2 ) = CDAOBJ.#FUNCTION
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Invalid:1:function specified::2:");                                                                   //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Invalid:1:function specified::2:'
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("#FUNCTION");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = '#FUNCTION'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PROG"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PROG"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
            //* ***********************************************************************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ERROR
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*                   GET-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-OBJECT
        //* *SAG DEFINE EXIT BEFORE-PRIMARY-STORE
        //*  USE THIS EXIT FOR CODE TO BE EXECUTED IMMEDIATELY PRIOR TO STOREING
        //*  A PRIMARY FILE RECORD. (E.G. ESCAPE ROUTINE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HOLD-OBJECT
        //* *SAG DEFINE EXIT BEFORE-PRIMARY-UPDATE-DELETE
        //*  PLACE CODE HERE PRIOR TO UPDATE OR DELETE OF PRIMARY FILE:
        //*  (E.G. ESCAPE ROUTINE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-AND-UPDATE-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EDIT-OBJECT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-EXISTENCE
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CLEAR
        //* *SAG DEFINE EXIT UPDATE-EDITS
        //*  SPECIFY SUBROUTINES OF THE FORM V0-ENTITY-NAME (OR V1- OR V2-) USED
        //*  TO VALIDATE OR MANIPULATE DATA BEFORE AN UPDATE/STORE OPERATION WHICH
        //*  MOVE DATA FROM THE OBJECT TO THE ENTITY-NAME RECORD. DATA MANIPULATION
        //*  MUST BE MADE AGAINST THE RECORD. IF ANY REFERENCE TO THE OBJECT IS
        //*  REQUIRED BECAUSE THE RELEVANT FIELDS HAVE DIFFERENT NAMES ON THE
        //*  OBJECT AND THE FILE, APPROPRIATE INDICES (#L2),(#L2,#L3),(#L2,#L3,#L4)
        //*  SHOULD BE USED. THE V2- ROUTINE IS PERFORMED FOR AN ENTITY AFTER ALL
        //*  OF ITS CHILDREN's processing while the V0-/V1- are invoked before
        //*  ITS CHILDREN's, and before/after its PREDICT rules respectively.
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: V0-PST-STNDRD-PCKGE
        //*   WHEN PST-STNDRD-PCKGE.PCKGE-MIT-CLSS EQ ' '
        //*       AND PST-STNDRD-PCKGE.PCKGE-MIT-CTGRY �= ' '
        //*     ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
        //*     ASSIGN MSG-INFO-SUB.##MSG-DATA(1) = 'MIT Class'
        //*     ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-MIT-CLSS'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: V1-PST-STNDRD-PCKGE
        //* *SAG END-EXIT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DB-CALL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-OBJECT-ID
        //* *SAG DEFINE EXIT MISCELLANEOUS-SUBROUTINES
        //* *SAG END-EXIT
    }
    private void sub_Process_Error() throws Exception                                                                                                                     //Natural: PROCESS-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  SET DEFAULT FOR :1:.
        if (condition(pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                            //Natural: IF MSG-INFO-SUB.##MSG-DATA ( 1 ) = ' ' THEN
        {
                                                                                                                                                                          //Natural: PERFORM SET-OBJECT-ID
            sub_Set_Object_Id();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  SET DEFAULT ERROR FIELD.
        if (condition(pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().equals(" ")))                                                                                     //Natural: IF MSG-INFO-SUB.##ERROR-FIELD = ' ' THEN
        {
            if (condition(pnd_Current_Field.notEquals(" ")))                                                                                                              //Natural: IF #CURRENT-FIELD NE ' '
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue(pnd_Current_Field);                                                                            //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = #CURRENT-FIELD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-CDE-VRSN-CDE");                                                                         //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-CDE-VRSN-CDE'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ERROR.
        pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'E'
        //* *SAG DEFINE EXIT PROCESS-ERROR-EXIT
        //* * CODE TO PRECEED BT
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
        Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
        if (true) return;
        //* *
        //* *SAG END-EXIT
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        Global.setEscape(true);                                                                                                                                           //Natural: ESCAPE BOTTOM ( PROG. ) IMMEDIATE
        Global.setEscapeCode(EscapeType.BottomImmediate, "PROG");
        if (true) return;
        //*  PROCESS-ERROR
    }
    private void sub_Create_Object() throws Exception                                                                                                                     //Natural: CREATE-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  DON't allow the key to be null.
        //*  COMPARE WITH NULL
        if (condition(pdaPsta7220.getPsta7220_Id().equals(next_View_Pckge_Cde_Vrsn_Cde)))                                                                                 //Natural: IF PSTA7220-ID = NEXT-VIEW.PCKGE-CDE-VRSN-CDE
        {
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(pnd_Object);                                                                              //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = #OBJECT
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:identifier is required");                                                                              //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:identifier is required'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF OBJECT ALREADY EXIST, RETURN WITH ERROR.
                                                                                                                                                                          //Natural: PERFORM CHECK-EXISTENCE
        sub_Check_Existence();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaCdaobj.getCdaobj_Pnd_Exists().getBoolean()))                                                                                                     //Natural: IF CDAOBJ.#EXISTS THEN
        {
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:already exists");                                                                                      //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:already exists'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
            sub_Process_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  PRE-EDIT OBJECT HEADER
                                                                                                                                                                          //Natural: PERFORM EDIT-OBJECT
        sub_Edit_Object();
        if (condition(Global.isEscape())) {return;}
        pst_Stndrd_Pckge_Pckge_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                                  //Natural: ASSIGN PST-STNDRD-PCKGE.PCKGE-UPDTE-DTE-TME = *TIMX
        //*  CHECK AND UPDATE CHILDREN
                                                                                                                                                                          //Natural: PERFORM CHECK-AND-UPDATE-OBJECT
        sub_Check_And_Update_Object();
        if (condition(Global.isEscape())) {return;}
        pst_Stndrd_Pckge_Pckge_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                           //Natural: ASSIGN PST-STNDRD-PCKGE.PCKGE-UPDTE-OPRTR-CDE := *INIT-USER
        //* *SAG END-EXIT
        vw_pst_Stndrd_Pckge.insertDBRow();                                                                                                                                //Natural: STORE PST-STNDRD-PCKGE
        //*  CREATE-OBJECT
    }
    private void sub_Get_Object() throws Exception                                                                                                                        //Natural: GET-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  GET THE PRIMARY OBJECT RECORD.
        vw_pst_Stndrd_Pckge.startDatabaseFind                                                                                                                             //Natural: FIND PST-STNDRD-PCKGE WITH PCKGE-CDE-VRSN-CDE = PSTA7220-ID
        (
        "FIND01",
        new Wc[] { new Wc("PCKGE_CDE_VRSN_CDE", "=", pdaPsta7220.getPsta7220_Id(), WcType.WITH) }
        );
        FIND01:
        while (condition(vw_pst_Stndrd_Pckge.readNextRow("FIND01", true)))
        {
            vw_pst_Stndrd_Pckge.setIfNotFoundControlFlag(false);
            if (condition(vw_pst_Stndrd_Pckge.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORDS FOUND
            {
                //*  RETURN THE FACT THAT THE OBJECT DOES NOT EXIST.
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:does not exist");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:does not exist'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM CLEAR
            sub_Clear();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaCdaobj.getCdaobj_Pnd_Exists().setValue(true);                                                                                                              //Natural: ASSIGN CDAOBJ.#EXISTS = TRUE
            //*  "Hold" THE RECORD BY CAPTURING THE VALUE OF THE UPDATE FLAG.
            pdaPsta7221.getPsta7221_Intervening_Upd_Fld().setValue(pst_Stndrd_Pckge_Pckge_Updte_Dte_Tme);                                                                 //Natural: ASSIGN PSTA7221.INTERVENING-UPD-FLD = PST-STNDRD-PCKGE.PCKGE-UPDTE-DTE-TME
            //*  SHIFT PRIMARY ENTITY INFORMATION TO OBJECT
            pdaPsta7220.getPsta7220().setValuesByName(vw_pst_Stndrd_Pckge);                                                                                               //Natural: MOVE BY NAME PST-STNDRD-PCKGE TO PSTA7220
            pdaPsta7221.getPsta7221_Held_Id().setValue(pdaPsta7220.getPsta7220_Id());                                                                                     //Natural: ASSIGN PSTA7221.HELD-ID = PSTA7220-ID
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  GET-OBJECT
    }
    private void sub_Hold_Object() throws Exception                                                                                                                       //Natural: HOLD-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  GET PRIMARY ENTITY AND PROCESS IT.
        vw_pst_Stndrd_Pckge.startDatabaseFind                                                                                                                             //Natural: FIND PST-STNDRD-PCKGE WITH PCKGE-CDE-VRSN-CDE = PSTA7220-ID
        (
        "HOLD_PRIME",
        new Wc[] { new Wc("PCKGE_CDE_VRSN_CDE", "=", pdaPsta7220.getPsta7220_Id(), WcType.WITH) }
        );
        HOLD_PRIME:
        while (condition(vw_pst_Stndrd_Pckge.readNextRow("HOLD_PRIME", true)))
        {
            vw_pst_Stndrd_Pckge.setIfNotFoundControlFlag(false);
            if (condition(vw_pst_Stndrd_Pckge.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORDS FOUND
            {
                //*  TRYING TO PROCESS AN OBJECT THAT DOESN't exist.
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Intervening purge, please try again");                                                                //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Intervening purge, please try again'
                pdaPsta7221.getPsta7221().reset();                                                                                                                        //Natural: RESET PSTA7221
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(pdaPsta7221.getPsta7221_Intervening_Upd_Fld().notEquals(pst_Stndrd_Pckge_Pckge_Updte_Dte_Tme)))                                                 //Natural: IF PSTA7221.INTERVENING-UPD-FLD NE PST-STNDRD-PCKGE.PCKGE-UPDTE-DTE-TME THEN
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Intervening update, please try again");                                                               //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Intervening update, please try again'
                pdaPsta7221.getPsta7221().reset();                                                                                                                        //Natural: RESET PSTA7221
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("HOLD_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaCdaobj.getCdaobj_Pnd_Exists().setValue(true);                                                                                                              //Natural: ASSIGN CDAOBJ.#EXISTS = TRUE
            //*  PRE-EDIT OBJECT HEADER
                                                                                                                                                                          //Natural: PERFORM EDIT-OBJECT
            sub_Edit_Object();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("HOLD_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("HOLD_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CHECK AND UPDATE CHILDREN
                                                                                                                                                                          //Natural: PERFORM CHECK-AND-UPDATE-OBJECT
            sub_Check_And_Update_Object();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("HOLD_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("HOLD_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pst_Stndrd_Pckge_Pckge_Updte_Oprtr_Cde.setValue(Global.getINIT_USER());                                                                                       //Natural: ASSIGN PST-STNDRD-PCKGE.PCKGE-UPDTE-OPRTR-CDE := *INIT-USER
            //* *SAG END-EXIT
            short decideConditionsMet632 = 0;                                                                                                                             //Natural: DECIDE ON EVERY VALUE CDAOBJ.#FUNCTION;//Natural: VALUE 'UPDATE'
            if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE")))
            {
                decideConditionsMet632++;
                pst_Stndrd_Pckge_Pckge_Updte_Dte_Tme.setValue(Global.getTIMX());                                                                                          //Natural: ASSIGN PST-STNDRD-PCKGE.PCKGE-UPDTE-DTE-TME = *TIMX
                vw_pst_Stndrd_Pckge.updateDBRow("HOLD_PRIME");                                                                                                            //Natural: UPDATE ( HOLD-PRIME. )
            }                                                                                                                                                             //Natural: VALUE 'DELETE'
            if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("DELETE")))
            {
                decideConditionsMet632++;
                vw_pst_Stndrd_Pckge.deleteDBRow("HOLD_PRIME");                                                                                                            //Natural: DELETE ( HOLD-PRIME. )
                pdaPsta7221.getPsta7221_Held_Id().reset();                                                                                                                //Natural: RESET PSTA7221.HELD-ID
            }                                                                                                                                                             //Natural: NONE
            if (condition(decideConditionsMet632 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  HOLD-OBJECT
    }
    //*  PROCESS ALL SUB-ENTITIES
    private void sub_Check_And_Update_Object() throws Exception                                                                                                           //Natural: CHECK-AND-UPDATE-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  NO SUB-ENTITIES
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  CHECK-AND-UPDATE-OBJECT
    }
    //*  PRE-EDITING OBJECT HEADER
    private void sub_Edit_Object() throws Exception                                                                                                                       //Natural: EDIT-OBJECT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") || pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE")))                                         //Natural: IF CDAOBJ.#FUNCTION = 'UPDATE' OR = 'STORE'
        {
            //*  BUILD UP THE HEADER RECORD FOR EDITING OR UPDATING
            vw_pst_Stndrd_Pckge.setValuesByName(pdaPsta7220.getPsta7220());                                                                                               //Natural: MOVE BY NAME PSTA7220 TO PST-STNDRD-PCKGE
                                                                                                                                                                          //Natural: PERFORM V0-PST-STNDRD-PCKGE
            sub_V0_Pst_Stndrd_Pckge();
            if (condition(Global.isEscape())) {return;}
            pdaPsta7220.getPsta7220().setValuesByName(vw_pst_Stndrd_Pckge);                                                                                               //Natural: MOVE BY NAME PST-STNDRD-PCKGE TO PSTA7220
            if (condition(pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ")))                                                                                      //Natural: IF MSG-INFO-SUB.##MSG NE ' ' THEN
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM V1-PST-STNDRD-PCKGE
            sub_V1_Pst_Stndrd_Pckge();
            if (condition(Global.isEscape())) {return;}
            pdaPsta7220.getPsta7220().setValuesByName(vw_pst_Stndrd_Pckge);                                                                                               //Natural: MOVE BY NAME PST-STNDRD-PCKGE TO PSTA7220
            if (condition(pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ")))                                                                                      //Natural: IF MSG-INFO-SUB.##MSG NE ' ' THEN
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  ENFORCE UPDATE CONSTRAINT FOR INSERT RULE
            vw_pst_Finish_Profile_Pckge_Rl.getTotalRowCount                                                                                                               //Natural: FIND NUMBER PST-FINISH-PROFILE-PCKGE-RL WITH FINISH-CDE = PST-STNDRD-PCKGE.PCKGE-FINISH-CDE
            (
            new Wc[] { new Wc("FINISH_CDE", "=", pst_Stndrd_Pckge_Pckge_Finish_Cde, WcType.WITH) }
            );
            if (condition(Global.getAstNUMBER().equals(getZero())))                                                                                                       //Natural: IF *NUMBER = 0
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Pckge Finish Cde");                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Pckge Finish Cde'
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-FINISH-CDE");                                                                           //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-FINISH-CDE'
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2).setValue(pst_Stndrd_Pckge_Pckge_Finish_Cde);                                                   //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 2 ) = PST-STNDRD-PCKGE.PCKGE-FINISH-CDE
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(3).setValue("Pst Finishing Profile");                                                             //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 3 ) = 'Pst Finishing Profile'
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1::2:not found on file:3:");                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1::2:not found on file:3:'
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  EDIT-OBJECT
    }
    private void sub_Check_Existence() throws Exception                                                                                                                   //Natural: CHECK-EXISTENCE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CHECK WHETHER OBJECT CURRENTLY EXISTS.
        vw_next_View.createHistogram                                                                                                                                      //Natural: HISTOGRAM ( 1 ) NEXT-VIEW FOR PCKGE-CDE-VRSN-CDE FROM PSTA7220-ID THRU PSTA7220-ID
        (
        "EXISTENCE_CHECK",
        "PCKGE_CDE_VRSN_CDE",
        new Wc[] { new Wc("PCKGE_CDE_VRSN_CDE", ">=", pdaPsta7220.getPsta7220_Id(), "And", WcType.WITH) ,
        new Wc("PCKGE_CDE_VRSN_CDE", "<=", pdaPsta7220.getPsta7220_Id(), WcType.WITH) },
        1
        );
        EXISTENCE_CHECK:
        while (condition(vw_next_View.readNextRow("EXISTENCE_CHECK")))
        {
            //*  OBJECT ID NOT UNIQUE.
            if (condition(vw_next_View.getAstNUMBER().greater(1)))                                                                                                        //Natural: IF *NUMBER ( EXISTENCE-CHECK. ) GT 1 THEN
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("Data integrity error,:2:occurrences of:1:");                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG = 'Data integrity error,:2:occurrences of:1:'
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(2).setValue(vw_next_View.getAstNUMBER());                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 2 ) = *NUMBER ( EXISTENCE-CHECK. )
                pdaPsta7221.getPsta7221().reset();                                                                                                                        //Natural: RESET PSTA7221
                                                                                                                                                                          //Natural: PERFORM PROCESS-ERROR
                sub_Process_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("EXISTENCE_CHECK"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("EXISTENCE_CHECK"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaCdaobj.getCdaobj_Pnd_Exists().setValue(true);                                                                                                              //Natural: ASSIGN CDAOBJ.#EXISTS = TRUE
        }                                                                                                                                                                 //Natural: END-HISTOGRAM
        if (Global.isEscape()) return;
        //*  CHECK-EXISTENCE
    }
    private void sub_Clear() throws Exception                                                                                                                             //Natural: CLEAR
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        //*  CAPTURE KEY.
        pdaPsta7220.getPsta7220_Id_Structure().setValuesByName(pdaPsta7220.getPsta7220());                                                                                //Natural: MOVE BY NAME PSTA7220 TO PSTA7220-ID.STRUCTURE
        pdaPsta7220.getPsta7220().reset();                                                                                                                                //Natural: RESET PSTA7220 PSTA7221
        pdaPsta7221.getPsta7221().reset();
        //*  RESTORE KEY.
        pdaPsta7220.getPsta7220().setValuesByName(pdaPsta7220.getPsta7220_Id_Structure());                                                                                //Natural: MOVE BY NAME PSTA7220-ID.STRUCTURE TO PSTA7220
        //*  CLEAR
    }
    private void sub_V0_Pst_Stndrd_Pckge() throws Exception                                                                                                               //Natural: V0-PST-STNDRD-PCKGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INSERT CODE WHICH IS NECESSARY TO VALIDATE/MANIPULATE THE DATA FROM
        //*  PST-STNDRD-PCKGE RECORD BEFORE AN UPDATE/STORE OPERATION,
        //*  BEFORE ITS CHILDREN's processing and before its PREDICT rules if any.
        short decideConditionsMet719 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PST-STNDRD-PCKGE.PCKGE-CDE EQ ' '
        if (condition(pst_Stndrd_Pckge_Pckge_Cde.equals(" ")))
        {
            decideConditionsMet719++;
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:is required");                                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Package Code");                                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Package Code'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-CDE");                                                                                      //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-CDE'
        }                                                                                                                                                                 //Natural: WHEN PST-STNDRD-PCKGE.PCKGE-VRSN-CDE EQ ' '
        else if (condition(pst_Stndrd_Pckge_Pckge_Vrsn_Cde.equals(" ")))
        {
            decideConditionsMet719++;
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:is required");                                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Package Type Code");                                                                     //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Package Type Code'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-VRSN-CDE");                                                                                 //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-VRSN-CDE'
        }                                                                                                                                                                 //Natural: WHEN PST-STNDRD-PCKGE.PCKGE-SHORT-NME EQ ' '
        else if (condition(pst_Stndrd_Pckge_Pckge_Short_Nme.equals(" ")))
        {
            decideConditionsMet719++;
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:is required");                                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Package Name");                                                                          //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Package Name'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-SHORT-NME");                                                                                //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-SHORT-NME'
        }                                                                                                                                                                 //Natural: WHEN PST-STNDRD-PCKGE.PCKGE-OWNER-UNIT-CDE EQ ' '
        else if (condition(pst_Stndrd_Pckge_Pckge_Owner_Unit_Cde.equals(" ")))
        {
            decideConditionsMet719++;
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:is required");                                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Owner Unit");                                                                            //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Owner Unit'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-OWNER-UNIT-CDE");                                                                           //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-OWNER-UNIT-CDE'
        }                                                                                                                                                                 //Natural: WHEN NOT PST-STNDRD-PCKGE.PCKGE-DSCRPTN-TXT ( * ) <> ' '
        else if (condition(! (pst_Stndrd_Pckge_Pckge_Dscrptn_Txt.getValue("*").notEquals(" "))))
        {
            decideConditionsMet719++;
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:is required");                                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Package Description");                                                                   //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Package Description'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-DSCRPTN-TXT");                                                                              //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-DSCRPTN-TXT'
            //* GRAF9702
            //*  RPL1008
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field_Index1().setValue(1);                                                                                         //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD-INDEX1 = 1
        }                                                                                                                                                                 //Natural: WHEN NOT PST-STNDRD-PCKGE.PCKGE-DFLT-PRINT-FCLTY-CDE EQ 'S' OR = 'F' OR = 'P' OR = 'X' OR = 'D' OR = 'M' OR = 'T' OR = 'E'
        else if (condition(! ((pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde.equals("S") || pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde.equals("F") || pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde.equals("P") 
            || pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde.equals("X") || pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde.equals("D") || pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde.equals("M") 
            || pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde.equals("T") || pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde.equals("E")))))
        {
            decideConditionsMet719++;
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:MUST BE 'S'(CSF) OR 'T'(TOP) OR 'P'(PDL) OR 'X'(XICS) OR 'M'(METACODE)");                              //Natural: ASSIGN MSG-INFO-SUB.##MSG := ':1:MUST BE "S"(CSF) OR "T"(TOP) OR "P"(PDL) OR "X"(XICS) OR "M"(METACODE)'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Pckge dflt print fctly code");                                                           //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Pckge dflt print fctly code'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-DFLT-PRINT-FCLTY-CDE");                                                                     //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-DFLT-PRINT-FCLTY-CDE'
        }                                                                                                                                                                 //Natural: WHEN PST-STNDRD-PCKGE.PCKGE-FINISH-CDE EQ ' '
        else if (condition(pst_Stndrd_Pckge_Pckge_Finish_Cde.equals(" ")))
        {
            decideConditionsMet719++;
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:is required");                                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:is required'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Pckge Finish Cde");                                                                      //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Pckge Finish Cde'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-FINISH-CDE");                                                                               //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-FINISH-CDE'
        }                                                                                                                                                                 //Natural: WHEN NOT PST-STNDRD-PCKGE.PCKGE-CMBNE-IND = 'Y' OR = 'N'
        else if (condition(! ((pst_Stndrd_Pckge_Pckge_Cmbne_Ind.equals("Y") || pst_Stndrd_Pckge_Pckge_Cmbne_Ind.equals("N")))))
        {
            decideConditionsMet719++;
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:must be 'Y' OR 'N'");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:must be "Y" OR "N"'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Pckge combine ind");                                                                     //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Pckge combine ind'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-CMBNE-IND");                                                                                //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-CMBNE-IND'
        }                                                                                                                                                                 //Natural: WHEN NOT PST-STNDRD-PCKGE.PCKGE-IMMDTE-PRNT-IND = 'Y' OR = 'N'
        else if (condition(! ((pst_Stndrd_Pckge_Pckge_Immdte_Prnt_Ind.equals("Y") || pst_Stndrd_Pckge_Pckge_Immdte_Prnt_Ind.equals("N")))))
        {
            decideConditionsMet719++;
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:must be 'Y' OR 'N'");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:must be "Y" OR "N"'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Pckge immediate print ind");                                                             //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Pckge immediate print ind'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-IMMDTE-PRNT-IND");                                                                          //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-IMMDTE-PRNT-IND'
        }                                                                                                                                                                 //Natural: WHEN NOT PST-STNDRD-PCKGE.PCKGE-IMAGE-IND = 'Y' OR = 'N'
        else if (condition(! ((pst_Stndrd_Pckge_Pckge_Image_Ind.equals("Y") || pst_Stndrd_Pckge_Pckge_Image_Ind.equals("N")))))
        {
            decideConditionsMet719++;
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:must be 'Y' OR 'N'");                                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:must be "Y" OR "N"'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Pckge image ind");                                                                       //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Pckge image ind'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-IMAGE-IND");                                                                                //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-IMAGE-IND'
        }                                                                                                                                                                 //Natural: WHEN NOT PST-STNDRD-PCKGE.PURGE-DAYS ( 1 ) = 0
        else if (condition(! ((pst_Stndrd_Pckge_Purge_Days.getValue(1).equals(getZero())))))
        {
            decideConditionsMet719++;
            if (condition(pst_Stndrd_Pckge_Purge_Days.getValue(2).equals(getZero())))                                                                                     //Natural: IF PST-STNDRD-PCKGE.PURGE-DAYS ( 2 ) = 0
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:enter both 'Purge Days' values");                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:enter both "Purge Days" values'
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Purge Bad Days, ");                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Purge Bad Days, '
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PURGE-DAYS");                                                                                 //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PURGE-DAYS'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pst_Stndrd_Pckge_Purge_Days.getValue(2).greater(getZero()) && pst_Stndrd_Pckge_Purge_Days.getValue(2).less(7)))                                 //Natural: IF PST-STNDRD-PCKGE.PURGE-DAYS ( 2 ) > 0 AND PST-STNDRD-PCKGE.PURGE-DAYS ( 2 ) < 7
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:valid value must be > 6 days ");                                                                   //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:valid value must be > 6 days '
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("for Purge Bad Days, ");                                                              //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'for Purge Bad Days, '
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PURGE-DAYS");                                                                                 //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PURGE-DAYS'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NOT PST-STNDRD-PCKGE.PURGE-DAYS ( 2 ) = 0
        else if (condition(! ((pst_Stndrd_Pckge_Purge_Days.getValue(2).equals(getZero())))))
        {
            decideConditionsMet719++;
            if (condition(pst_Stndrd_Pckge_Purge_Days.getValue(1).equals(getZero())))                                                                                     //Natural: IF PST-STNDRD-PCKGE.PURGE-DAYS ( 1 ) = 0
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:enter both 'Purge Days' values");                                                                  //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:enter both "Purge Days" values'
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Purge Good Days, ");                                                                 //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Purge Good Days, '
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PURGE-DAYS");                                                                                 //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PURGE-DAYS'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NOT PST-STNDRD-PCKGE.PCKGE-POST-VRSN-CDE = '  ' OR = '01' OR = '02'
        else if (condition(! ((pst_Stndrd_Pckge_Pckge_Post_Vrsn_Cde.equals("  ") || pst_Stndrd_Pckge_Pckge_Post_Vrsn_Cde.equals("01") || pst_Stndrd_Pckge_Pckge_Post_Vrsn_Cde.equals("02")))))
        {
            decideConditionsMet719++;
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:must be '  ','01' OR '02'");                                                                           //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:must be "  ","01" OR "02"'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Pckge POST Vrsn Cde");                                                                   //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Pckge POST Vrsn Cde'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-POST-VRSN-CDE");                                                                            //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-POST-VRSN-CDE'
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet719 > 0))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pst_Stndrd_Pckge_Pckge_Post_Vrsn_Cde.equals("02")))                                                                                                 //Natural: IF PST-STNDRD-PCKGE.PCKGE-POST-VRSN-CDE = '02'
        {
            if (condition(pst_Stndrd_Pckge_Pckge_Dflt_Print_Fclty_Cde.equals("S")))                                                                                       //Natural: IF PST-STNDRD-PCKGE.PCKGE-DFLT-PRINT-FCLTY-CDE = 'S'
            {
                short decideConditionsMet793 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PST-STNDRD-PCKGE.PCKGE-CSF-DTA-GNRTN-MDLE = ' '
                if (condition(pst_Stndrd_Pckge_Pckge_Csf_Dta_Gnrtn_Mdle.equals(" ")))
                {
                    decideConditionsMet793++;
                    pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:is required for CSF");                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:is required for CSF'
                    pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("Data Generation Module");                                                        //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'Data Generation Module'
                    pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-CSF-DTA-GNRTN-MDLE");                                                               //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-CSF-DTA-GNRTN-MDLE'
                }                                                                                                                                                         //Natural: WHEN PST-STNDRD-PCKGE.PCKGE-CSF-FILE-NAME = ' '
                else if (condition(pst_Stndrd_Pckge_Pckge_Csf_File_Name.equals(" ")))
                {
                    decideConditionsMet793++;
                    pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:is required for CSF");                                                                         //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:is required for CSF'
                    pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue("CSF File Name");                                                                 //Natural: ASSIGN MSG-INFO-SUB.##MSG-DATA ( 1 ) = 'CSF File Name'
                    pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-CSF-FILE-NAME");                                                                    //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-CSF-FILE-NAME'
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet793 > 0))
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   VALIDATE ENTERED DCMNT CTGRY/CLSS/SPCFC
        if (condition(pst_Stndrd_Pckge_Pckge_Mit_Ctgry.notEquals(" ") || pst_Stndrd_Pckge_Pckge_Mit_Clss.notEquals(" ") || pst_Stndrd_Pckge_Pckge_Mit_Spcfc.notEquals(" "))) //Natural: IF PST-STNDRD-PCKGE.PCKGE-MIT-CTGRY <> ' ' OR PST-STNDRD-PCKGE.PCKGE-MIT-CLSS <> ' ' OR PST-STNDRD-PCKGE.PCKGE-MIT-SPCFC <> ' '
        {
            DbsUtil.callnat(Efsn9015.class , getCurrentProcessState(), pst_Stndrd_Pckge_Pckge_Mit_Ctgry, pst_Stndrd_Pckge_Pckge_Mit_Clss, pst_Stndrd_Pckge_Pckge_Mit_Spcfc,  //Natural: CALLNAT 'EFSN9015' PST-STNDRD-PCKGE.PCKGE-MIT-CTGRY PST-STNDRD-PCKGE.PCKGE-MIT-CLSS PST-STNDRD-PCKGE.PCKGE-MIT-SPCFC #DCMNT-DSCRPTN #DEFAULT-RETENTION #ACTUAL-RETENTION CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                efsn9015_Pnd_Dcmnt_Dscrptn, efsn9015_Pnd_Default_Retention, efsn9015_Pnd_Actual_Retention, pdaCdaobj.getCdaobj(), pdaPstpda_D.getDialog_Info_Sub(), 
                pdaPstpda_M.getMsg_Info_Sub(), pdaPstpda_P.getPass_Sub());
            if (condition(Global.isEscape())) return;
            if (condition(pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().notEquals(getZero())))                                                                             //Natural: IF MSG-INFO-SUB.##MSG-NR <> 0
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-MIT-CTGRY");                                                                            //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-MIT-CTGRY'
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'E'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   VALIDATE ENTERED IMAGE CTGRY/CLSS/SPCFC
        if (condition(pst_Stndrd_Pckge_Pckge_Image_Ctgry.notEquals(" ") || pst_Stndrd_Pckge_Pckge_Image_Clss.notEquals(" ") || pst_Stndrd_Pckge_Pckge_Image_Spcfc.notEquals(" "))) //Natural: IF PST-STNDRD-PCKGE.PCKGE-IMAGE-CTGRY <> ' ' OR PST-STNDRD-PCKGE.PCKGE-IMAGE-CLSS <> ' ' OR PST-STNDRD-PCKGE.PCKGE-IMAGE-SPCFC <> ' '
        {
            DbsUtil.callnat(Efsn9015.class , getCurrentProcessState(), pst_Stndrd_Pckge_Pckge_Image_Ctgry, pst_Stndrd_Pckge_Pckge_Image_Clss, pst_Stndrd_Pckge_Pckge_Image_Spcfc,  //Natural: CALLNAT 'EFSN9015' PST-STNDRD-PCKGE.PCKGE-IMAGE-CTGRY PST-STNDRD-PCKGE.PCKGE-IMAGE-CLSS PST-STNDRD-PCKGE.PCKGE-IMAGE-SPCFC #DCMNT-DSCRPTN #DEFAULT-RETENTION #ACTUAL-RETENTION CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                efsn9015_Pnd_Dcmnt_Dscrptn, efsn9015_Pnd_Default_Retention, efsn9015_Pnd_Actual_Retention, pdaCdaobj.getCdaobj(), pdaPstpda_D.getDialog_Info_Sub(), 
                pdaPstpda_M.getMsg_Info_Sub(), pdaPstpda_P.getPass_Sub());
            if (condition(Global.isEscape())) return;
            if (condition(pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().notEquals(getZero())))                                                                             //Natural: IF MSG-INFO-SUB.##MSG-NR <> 0
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-IMAGE-CTGRY");                                                                          //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-IMAGE-CTGRY'
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'E'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   VALIDATE IF IMAGE IND IS 'Y', IMAGE CLSS, CTGRY AND SPCFC ARE THERE
        //*    PST-STNDRD-PCKGE.
        if (condition(pst_Stndrd_Pckge_Pckge_Image_Ind.equals("Y")))                                                                                                      //Natural: IF PST-STNDRD-PCKGE.PCKGE-IMAGE-IND = 'Y'
        {
            if (condition(pst_Stndrd_Pckge_Pckge_Image_Clss.equals(" ") && pst_Stndrd_Pckge_Pckge_Image_Ctgry.equals(" ") && pst_Stndrd_Pckge_Pckge_Image_Spcfc.equals(" "))) //Natural: IF PST-STNDRD-PCKGE.PCKGE-IMAGE-CLSS = ' ' AND PST-STNDRD-PCKGE.PCKGE-IMAGE-CTGRY = ' ' AND PST-STNDRD-PCKGE.PCKGE-IMAGE-SPCFC = ' '
            {
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1:is 'N' if other image data is not specified ");                                                    //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1:is "N" if other image data is not specified '
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("PCKGE-IMAGE-IND");                                                                            //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'PCKGE-IMAGE-IND'
                pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'E'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   VALIDATE THAT ONE DEFAULT PRINT IND IS SET FOR EACH DLVRY TYPE
        pnd_Nbr.reset();                                                                                                                                                  //Natural: RESET #NBR
        FOR01:                                                                                                                                                            //Natural: FOR #A 1 TO 20
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(20)); pnd_A.nadd(1))
        {
            if (condition(pst_Stndrd_Pckge_Pckge_Delivery_Type.getValue(pnd_A).equals(" ") && pst_Stndrd_Pckge_Fnsh_Prfle.getValue(pnd_A).equals(" ")))                   //Natural: IF PST-STNDRD-PCKGE.PCKGE-DELIVERY-TYPE ( #A ) = ' ' AND PST-STNDRD-PCKGE.FNSH-PRFLE ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_A.greater(1)))                                                                                                                              //Natural: IF #A > 1
            {
                if (condition(pst_Stndrd_Pckge_Pckge_Delivery_Type.getValue(pnd_A).equals(pst_Stndrd_Pckge_Pckge_Delivery_Type.getValue(pnd_A.getDec().subtract(1)))))    //Natural: IF PST-STNDRD-PCKGE.PCKGE-DELIVERY-TYPE ( #A ) = PST-STNDRD-PCKGE.PCKGE-DELIVERY-TYPE ( #A - 1 )
                {
                    if (condition(pst_Stndrd_Pckge_Dflt_Dlvry_Ind.getValue(pnd_A).getBoolean() && pst_Stndrd_Pckge_Dflt_Dlvry_Ind.getValue(pnd_A.getDec().subtract(1)).getBoolean())) //Natural: IF PST-STNDRD-PCKGE.DFLT-DLVRY-IND ( #A ) AND PST-STNDRD-PCKGE.DFLT-DLVRY-IND ( #A - 1 )
                    {
                        pnd_Nbr.setValue(1);                                                                                                                              //Natural: ASSIGN #NBR := 1
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(! (pst_Stndrd_Pckge_Dflt_Dlvry_Ind.getValue(pnd_A).equals(true)) && ! (pst_Stndrd_Pckge_Dflt_Dlvry_Ind.getValue(pnd_A.getDec().subtract(1)).equals(true)))) //Natural: IF NOT PST-STNDRD-PCKGE.DFLT-DLVRY-IND ( #A ) AND NOT PST-STNDRD-PCKGE.DFLT-DLVRY-IND ( #A - 1 )
                    {
                        pnd_Nbr.setValue(1);                                                                                                                              //Natural: ASSIGN #NBR := 1
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Nbr.equals(1)))                                                                                                                                 //Natural: IF #NBR = 1
        {
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(":1: select 1 dflt per dlvry typ");                                                                        //Natural: ASSIGN MSG-INFO-SUB.##MSG = ':1: select 1 dflt per dlvry typ'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("DFLT-DLVRY-IND");                                                                                 //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD = 'DFLT-DLVRY-IND'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE = 'E'
            pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field_Index1().setValue(1);                                                                                         //Natural: ASSIGN MSG-INFO-SUB.##ERROR-FIELD-INDEX1 = 1
        }                                                                                                                                                                 //Natural: END-IF
        //*  V0-PST-STNDRD-PCKGE
    }
    private void sub_V1_Pst_Stndrd_Pckge() throws Exception                                                                                                               //Natural: V1-PST-STNDRD-PCKGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INSERT CODE WHICH IS NECESSARY TO VALIDATE/MANIPULATE THE DATA FROM
        //*  PST-STNDRD-PCKGE RECORD BEFORE AN UPDATE/STORE OPERATION,
        //*  BEFORE ITS CHILDREN's processing and after its PREDICT rules if any.
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  V1-PST-STNDRD-PCKGE
    }
    private void sub_Get_Db_Call() throws Exception                                                                                                                       //Natural: GET-DB-CALL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet885 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #OLD-REC AND ( CDAOBJ.#FUNCTION = 'DELETE' OR ( CDAOBJ.#FUNCTION = 'UPDATE' AND NOT #SAVE-REC ) )
        if (condition((pnd_Old_Rec.getBoolean() && (pdaCdaobj.getCdaobj_Pnd_Function().equals("DELETE") || (pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") 
            && ! (pnd_Save_Rec.getBoolean()))))))
        {
            decideConditionsMet885++;
            //*  DELETE
            pnd_Db_Call.setValue("D");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'D'
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'UPDATE' AND #SAVE-REC AND #OLD-REC
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") && pnd_Save_Rec.getBoolean() && pnd_Old_Rec.getBoolean()))
        {
            decideConditionsMet885++;
            //*  UPDATE
            pnd_Db_Call.setValue("U");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'U'
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'UPDATE' AND #SAVE-REC AND NOT #OLD-REC
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("UPDATE") && pnd_Save_Rec.getBoolean() && ! (pnd_Old_Rec.getBoolean())))
        {
            decideConditionsMet885++;
            pnd_Db_Call.setValue("S");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'S'
        }                                                                                                                                                                 //Natural: WHEN CDAOBJ.#FUNCTION = 'STORE' AND #SAVE-REC
        else if (condition(pdaCdaobj.getCdaobj_Pnd_Function().equals("STORE") && pnd_Save_Rec.getBoolean()))
        {
            decideConditionsMet885++;
            pnd_Db_Call.setValue("S");                                                                                                                                    //Natural: ASSIGN #DB-CALL = 'S'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Db_Call.reset();                                                                                                                                          //Natural: RESET #DB-CALL
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  GET-DB-CALL
    }
    private void sub_Set_Object_Id() throws Exception                                                                                                                     //Natural: SET-OBJECT-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaPsta7220.getPsta7220_Pckge_Cde(),          //Natural: COMPRESS PSTA7220.PCKGE-CDE '-' PSTA7220.PCKGE-VRSN-CDE TO MSG-INFO-SUB.##MSG-DATA ( 1 ) LEAVING NO
            "-", pdaPsta7220.getPsta7220_Pckge_Vrsn_Cde()));
        pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(DbsUtil.compress(pnd_Object, pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1)));    //Natural: COMPRESS #OBJECT MSG-INFO-SUB.##MSG-DATA ( 1 ) TO MSG-INFO-SUB.##MSG-DATA ( 1 )
        //* *SAG DEFINE EXIT ADJUST-OBJECT-ID-IN-MSG
        pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "CDE '", pdaPsta7220.getPsta7220_Pckge_Cde(), //Natural: COMPRESS 'CDE "' PSTA7220.PCKGE-CDE '"' ' VRSN "' PSTA7220.PCKGE-VRSN-CDE '"' TO MSG-INFO-SUB.##MSG-DATA ( 1 ) LEAVING NO
            "'", " VRSN '", pdaPsta7220.getPsta7220_Pckge_Vrsn_Cde(), "'"));
        pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1).setValue(DbsUtil.compress(pnd_Object, pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Data().getValue(1)));    //Natural: COMPRESS #OBJECT MSG-INFO-SUB.##MSG-DATA ( 1 ) TO MSG-INFO-SUB.##MSG-DATA ( 1 )
        //* *SAG END-EXIT
        //*  SET-OBJECT-ID
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //*   GENERAL CATCH ALL ERROR HANDLER.
        pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Natural error", Global.getERROR_NR(), "in", Global.getPROGRAM(), "at line",                  //Natural: COMPRESS 'Natural error' *ERROR-NR 'in' *PROGRAM 'at line' *ERROR-LINE INTO MSG-INFO-SUB.##MSG
            Global.getERROR_LINE()));
        pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field().setValue("SYSTEM");                                                                                             //Natural: MOVE 'SYSTEM' TO MSG-INFO-SUB.##ERROR-FIELD
        pdaPstpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                                  //Natural: MOVE 'E' TO MSG-INFO-SUB.##RETURN-CODE
    };                                                                                                                                                                    //Natural: END-ERROR
}
