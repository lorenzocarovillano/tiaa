/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 02:01:57 AM
**        * FROM NATURAL SUBPROGRAM : Pstn7220
************************************************************
**        * FILE NAME            : Pstn7220.java
**        * CLASS NAME           : Pstn7220
**        * INSTANCE NAME        : Pstn7220
************************************************************
**SAG GENERATOR: OBJECT-SUBP-TIAA                 VERSION: 3.2.2
**SAG TITLE: MAINTAIN PACKAGES
**SAG SYSTEM: PST
**SAG OBJECT-DESC: PACKAGE
**SAG PRIME-FILE: PST-STNDRD-PCKGE
**SAG PRIME-KEY: PCKGE-CDE-VRSN-CDE
**SAG HOLD-FIELD: PCKGE-UPDTE-DTE-TME
**SAG OBJECT-PDA: PSTA7220
**SAG RESTRICTED-PDA: PSTA7221
**SAG DESCS(1): THIS SUBPROGRAM IS USED TO PERFORM OBJECT MAINTENANCE
**SAG DESCS(2): FOR POST PACKAGES
************************************************************************
* PROGRAM  : PSTN7220
* SYSTEM   : PST
* TITLE    : MAINTAIN PACKAGES
* GENERATED: MAY 05, 1999
* FUNCTION : THIS SUBPROGRAM IS USED TO CALL PSTN7225 WHICH IS THE
*            OBJECT MAINTENANCE MODULE FOR POST PACKAGES
*
* HISTORY
*
*   WHO    WHEN       WHAT
* -------- ------ ------------------------------------------------------
* GRAF     5//5/99  CLONED FROM PSTN7205 TO BE USED IN THE POST PURGE
*                   MODULE PSTB5151.
*
************************************************************************

************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Pstn7220 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaPsta7205 pdaPsta7205;
    private PdaPsta7206 pdaPsta7206;
    private PdaCdaobj pdaCdaobj;
    private PdaPstpda_D pdaPstpda_D;
    private PdaPstpda_M pdaPstpda_M;
    private PdaPstpda_P pdaPstpda_P;
    private PdaPsta7220 pdaPsta7220;
    private PdaPsta7221 pdaPsta7221;

    // Local Variables
    public DbsRecord localVariables;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta7220 = new PdaPsta7220(localVariables);
        pdaPsta7221 = new PdaPsta7221(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaPsta7205 = new PdaPsta7205(parameters);
        pdaPsta7206 = new PdaPsta7206(parameters);
        pdaCdaobj = new PdaCdaobj(parameters);
        pdaPstpda_D = new PdaPstpda_D(parameters);
        pdaPstpda_M = new PdaPstpda_M(parameters);
        pdaPstpda_P = new PdaPstpda_P(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Pstn7220() throws Exception
    {
        super("Pstn7220");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaPsta7220.getPsta7220().setValuesByName(pdaPsta7205.getPsta7205());                                                                                             //Natural: MOVE BY NAME PSTA7205 TO PSTA7220
        pdaPsta7221.getPsta7221().setValuesByName(pdaPsta7206.getPsta7206());                                                                                             //Natural: MOVE BY NAME PSTA7206 TO PSTA7221
        //*  INVOKE SUBPROGRAM TO PROCESS OBJECT
        DbsUtil.callnat(Pstn7225.class , getCurrentProcessState(), pdaPsta7220.getPsta7220(), pdaPsta7220.getPsta7220_Id(), pdaPsta7221.getPsta7221(),                    //Natural: CALLNAT 'PSTN7225' PSTA7220 PSTA7220-ID PSTA7221 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaPstpda_D.getDialog_Info_Sub(), pdaPstpda_M.getMsg_Info_Sub(), pdaPstpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        pdaPsta7205.getPsta7205().setValuesByName(pdaPsta7220.getPsta7220());                                                                                             //Natural: MOVE BY NAME PSTA7220 TO PSTA7205
        pdaPsta7206.getPsta7206().setValuesByName(pdaPsta7221.getPsta7221());                                                                                             //Natural: MOVE BY NAME PSTA7221 TO PSTA7206
    }

    //
}
