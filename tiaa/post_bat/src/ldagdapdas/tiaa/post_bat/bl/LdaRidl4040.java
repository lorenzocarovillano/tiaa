/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:11:38 PM
**        * FROM NATURAL LDA     : RIDL4040
************************************************************
**        * FILE NAME            : LdaRidl4040.java
**        * CLASS NAME           : LdaRidl4040
**        * INSTANCE NAME        : LdaRidl4040
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaRidl4040 extends DbsRecord
{
    // Properties
    private DbsGroup error_Handler_Fields;
    private DbsField error_Handler_Fields_Pnd_Error_Nr;
    private DbsField error_Handler_Fields_Pnd_Error_Line;
    private DbsField error_Handler_Fields_Pnd_Error_Status;
    private DbsField error_Handler_Fields_Pnd_Error_Program;
    private DbsField error_Handler_Fields_Pnd_Error_Level;
    private DbsField error_Handler_Fields_Pnd_Error_Appl;
    private DbsGroup input_Parms;
    private DbsField input_Parms_Pnd_Pckge_Cde;
    private DbsField input_Parms_Pnd_Start_Rqst_Id;
    private DbsField input_Parms_Pnd_End_Rqst_Id;
    private DbsField input_Parms_Pnd_Run_Mode;
    private DbsField input_Parms_Pnd_Trace;
    private DbsField pnd_First_Pst;
    private DbsField pnd_Last_Pst;

    public DbsGroup getError_Handler_Fields() { return error_Handler_Fields; }

    public DbsField getError_Handler_Fields_Pnd_Error_Nr() { return error_Handler_Fields_Pnd_Error_Nr; }

    public DbsField getError_Handler_Fields_Pnd_Error_Line() { return error_Handler_Fields_Pnd_Error_Line; }

    public DbsField getError_Handler_Fields_Pnd_Error_Status() { return error_Handler_Fields_Pnd_Error_Status; }

    public DbsField getError_Handler_Fields_Pnd_Error_Program() { return error_Handler_Fields_Pnd_Error_Program; }

    public DbsField getError_Handler_Fields_Pnd_Error_Level() { return error_Handler_Fields_Pnd_Error_Level; }

    public DbsField getError_Handler_Fields_Pnd_Error_Appl() { return error_Handler_Fields_Pnd_Error_Appl; }

    public DbsGroup getInput_Parms() { return input_Parms; }

    public DbsField getInput_Parms_Pnd_Pckge_Cde() { return input_Parms_Pnd_Pckge_Cde; }

    public DbsField getInput_Parms_Pnd_Start_Rqst_Id() { return input_Parms_Pnd_Start_Rqst_Id; }

    public DbsField getInput_Parms_Pnd_End_Rqst_Id() { return input_Parms_Pnd_End_Rqst_Id; }

    public DbsField getInput_Parms_Pnd_Run_Mode() { return input_Parms_Pnd_Run_Mode; }

    public DbsField getInput_Parms_Pnd_Trace() { return input_Parms_Pnd_Trace; }

    public DbsField getPnd_First_Pst() { return pnd_First_Pst; }

    public DbsField getPnd_Last_Pst() { return pnd_Last_Pst; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        error_Handler_Fields = newGroupInRecord("error_Handler_Fields", "ERROR-HANDLER-FIELDS");
        error_Handler_Fields_Pnd_Error_Nr = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Nr", "#ERROR-NR", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Line = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Line", "#ERROR-LINE", FieldType.NUMERIC, 
            4);
        error_Handler_Fields_Pnd_Error_Status = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Status", "#ERROR-STATUS", FieldType.STRING, 
            1);
        error_Handler_Fields_Pnd_Error_Program = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Program", "#ERROR-PROGRAM", FieldType.STRING, 
            8);
        error_Handler_Fields_Pnd_Error_Level = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Level", "#ERROR-LEVEL", FieldType.NUMERIC, 
            2);
        error_Handler_Fields_Pnd_Error_Appl = error_Handler_Fields.newFieldInGroup("error_Handler_Fields_Pnd_Error_Appl", "#ERROR-APPL", FieldType.STRING, 
            8);

        input_Parms = newGroupInRecord("input_Parms", "INPUT-PARMS");
        input_Parms_Pnd_Pckge_Cde = input_Parms.newFieldInGroup("input_Parms_Pnd_Pckge_Cde", "#PCKGE-CDE", FieldType.STRING, 11);
        input_Parms_Pnd_Start_Rqst_Id = input_Parms.newFieldInGroup("input_Parms_Pnd_Start_Rqst_Id", "#START-RQST-ID", FieldType.STRING, 11);
        input_Parms_Pnd_End_Rqst_Id = input_Parms.newFieldInGroup("input_Parms_Pnd_End_Rqst_Id", "#END-RQST-ID", FieldType.STRING, 11);
        input_Parms_Pnd_Run_Mode = input_Parms.newFieldInGroup("input_Parms_Pnd_Run_Mode", "#RUN-MODE", FieldType.STRING, 3);
        input_Parms_Pnd_Trace = input_Parms.newFieldInGroup("input_Parms_Pnd_Trace", "#TRACE", FieldType.BOOLEAN);

        pnd_First_Pst = newFieldArrayInRecord("pnd_First_Pst", "#FIRST-PST", FieldType.STRING, 11, new DbsArrayController(1,10));

        pnd_Last_Pst = newFieldArrayInRecord("pnd_Last_Pst", "#LAST-PST", FieldType.STRING, 11, new DbsArrayController(1,10));

        this.setRecordName("LdaRidl4040");
    }

    public void initializeValues() throws Exception
    {
        reset();
        error_Handler_Fields_Pnd_Error_Status.setInitialValue("O");
        error_Handler_Fields_Pnd_Error_Program.setInitialValue(Global.getPROGRAM());
        error_Handler_Fields_Pnd_Error_Appl.setInitialValue("RIDER");
    }

    // Constructor
    public LdaRidl4040() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
