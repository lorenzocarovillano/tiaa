/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:11:38 PM
**        * FROM NATURAL LDA     : RIDL402
************************************************************
**        * FILE NAME            : LdaRidl402.java
**        * CLASS NAME           : LdaRidl402
**        * INSTANCE NAME        : LdaRidl402
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaRidl402 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Post_Data;
    private DbsField pnd_Post_Data_Pnd_Pst_Rqst_Id;
    private DbsField pnd_Post_Data_Pnd_Pin_Number;
    private DbsField pnd_Post_Data_Pnd_Full_Name;
    private DbsField pnd_Post_Data_Pnd_Address_Line_Txt;
    private DbsField pnd_Post_Data_Pnd_Postal_Data;
    private DbsField pnd_Post_Data_Pnd_Address_Type_Cde;
    private DbsField pnd_Post_Data_Pnd_Test_Code;
    private DbsField pnd_Post_Data_Pnd_Package_Code;
    private DbsField pnd_Post_Data_Pnd_Letter_Type;
    private DbsField pnd_Post_Data_Pnd_Institution_Name;
    private DbsField pnd_Post_Data_Pnd_Ctr_Combo;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_ComboRedef1;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Sra_Gn;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Gn;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Sra_Il;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Il;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Sra_Cref_Fl;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gsra_457b;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gsra_457b_Nl;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gsra_457b_Nh;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref_Il;
    private DbsField pnd_Post_Data_Pnd_Rider_Date;
    private DbsGroup pnd_Post_Data_Pnd_Rider_DateRedef2;
    private DbsField pnd_Post_Data_Pnd_Rider_Month;
    private DbsField pnd_Post_Data_Pnd_Rider_Year;
    private DbsGroup pnd_Post_Data_Pnd_Rider_YearRedef3;
    private DbsField pnd_Post_Data_Pnd_Rider_Year_A;
    private DbsField pnd_Post_Data_Pnd_Contracts;

    public DbsGroup getPnd_Post_Data() { return pnd_Post_Data; }

    public DbsField getPnd_Post_Data_Pnd_Pst_Rqst_Id() { return pnd_Post_Data_Pnd_Pst_Rqst_Id; }

    public DbsField getPnd_Post_Data_Pnd_Pin_Number() { return pnd_Post_Data_Pnd_Pin_Number; }

    public DbsField getPnd_Post_Data_Pnd_Full_Name() { return pnd_Post_Data_Pnd_Full_Name; }

    public DbsField getPnd_Post_Data_Pnd_Address_Line_Txt() { return pnd_Post_Data_Pnd_Address_Line_Txt; }

    public DbsField getPnd_Post_Data_Pnd_Postal_Data() { return pnd_Post_Data_Pnd_Postal_Data; }

    public DbsField getPnd_Post_Data_Pnd_Address_Type_Cde() { return pnd_Post_Data_Pnd_Address_Type_Cde; }

    public DbsField getPnd_Post_Data_Pnd_Test_Code() { return pnd_Post_Data_Pnd_Test_Code; }

    public DbsField getPnd_Post_Data_Pnd_Package_Code() { return pnd_Post_Data_Pnd_Package_Code; }

    public DbsField getPnd_Post_Data_Pnd_Letter_Type() { return pnd_Post_Data_Pnd_Letter_Type; }

    public DbsField getPnd_Post_Data_Pnd_Institution_Name() { return pnd_Post_Data_Pnd_Institution_Name; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Combo() { return pnd_Post_Data_Pnd_Ctr_Combo; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_ComboRedef1() { return pnd_Post_Data_Pnd_Ctr_ComboRedef1; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Gn() { return pnd_Post_Data_Pnd_Ctr_Ra_Sra_Gn; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Gn() { return pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Gn; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Il() { return pnd_Post_Data_Pnd_Ctr_Ra_Sra_Il; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Il() { return pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Il; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Sra_Cref_Fl() { return pnd_Post_Data_Pnd_Ctr_Ra_Sra_Cref_Fl; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl() { return pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gsra_457b() { return pnd_Post_Data_Pnd_Ctr_Gsra_457b; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref() { return pnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gsra_457b_Nl() { return pnd_Post_Data_Pnd_Ctr_Gsra_457b_Nl; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gsra_457b_Nh() { return pnd_Post_Data_Pnd_Ctr_Gsra_457b_Nh; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref_Il() { return pnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref_Il; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Date() { return pnd_Post_Data_Pnd_Rider_Date; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_DateRedef2() { return pnd_Post_Data_Pnd_Rider_DateRedef2; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Month() { return pnd_Post_Data_Pnd_Rider_Month; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year() { return pnd_Post_Data_Pnd_Rider_Year; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_YearRedef3() { return pnd_Post_Data_Pnd_Rider_YearRedef3; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year_A() { return pnd_Post_Data_Pnd_Rider_Year_A; }

    public DbsField getPnd_Post_Data_Pnd_Contracts() { return pnd_Post_Data_Pnd_Contracts; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Post_Data = newGroupInRecord("pnd_Post_Data", "#POST-DATA");
        pnd_Post_Data_Pnd_Pst_Rqst_Id = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pst_Rqst_Id", "#PST-RQST-ID", FieldType.STRING, 11);
        pnd_Post_Data_Pnd_Pin_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);
        pnd_Post_Data_Pnd_Full_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Post_Data_Pnd_Address_Line_Txt = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        pnd_Post_Data_Pnd_Postal_Data = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 32);
        pnd_Post_Data_Pnd_Address_Type_Cde = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Test_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Test_Code", "#TEST-CODE", FieldType.STRING, 5);
        pnd_Post_Data_Pnd_Package_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Post_Data_Pnd_Letter_Type = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Institution_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 
            60);
        pnd_Post_Data_Pnd_Ctr_Combo = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Combo", "#CTR-COMBO", FieldType.STRING, 11);
        pnd_Post_Data_Pnd_Ctr_ComboRedef1 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_ComboRedef1", "Redefines", pnd_Post_Data_Pnd_Ctr_Combo);
        pnd_Post_Data_Pnd_Ctr_Ra_Sra_Gn = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Sra_Gn", "#CTR-RA-SRA-GN", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Gn = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Gn", "#CTR-GRA-GSRA-RS-GN", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Ctr_Ra_Sra_Il = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Sra_Il", "#CTR-RA-SRA-IL", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Il = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Il", "#CTR-GRA-GSRA-RS-IL", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Ctr_Ra_Sra_Cref_Fl = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Sra_Cref_Fl", "#CTR-RA-SRA-CREF-FL", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gra_Gsra_Rs_Cref_Fl", "#CTR-GRA-GSRA-RS-CREF-FL", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Ctr_Gsra_457b = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gsra_457b", "#CTR-GSRA-457B", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref", "#CTR-GSRA-457B-CREF", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Ctr_Gsra_457b_Nl = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gsra_457b_Nl", "#CTR-GSRA-457B-NL", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Ctr_Gsra_457b_Nh = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gsra_457b_Nh", "#CTR-GSRA-457B-NH", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref_Il = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gsra_457b_Cref_Il", "#CTR-GSRA-457B-CREF-IL", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Rider_Date = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Date", "#RIDER-DATE", FieldType.NUMERIC, 6);
        pnd_Post_Data_Pnd_Rider_DateRedef2 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Rider_DateRedef2", "Redefines", pnd_Post_Data_Pnd_Rider_Date);
        pnd_Post_Data_Pnd_Rider_Month = pnd_Post_Data_Pnd_Rider_DateRedef2.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Month", "#RIDER-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Post_Data_Pnd_Rider_Year = pnd_Post_Data_Pnd_Rider_DateRedef2.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year", "#RIDER-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Post_Data_Pnd_Rider_YearRedef3 = pnd_Post_Data_Pnd_Rider_DateRedef2.newGroupInGroup("pnd_Post_Data_Pnd_Rider_YearRedef3", "Redefines", pnd_Post_Data_Pnd_Rider_Year);
        pnd_Post_Data_Pnd_Rider_Year_A = pnd_Post_Data_Pnd_Rider_YearRedef3.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year_A", "#RIDER-YEAR-A", FieldType.STRING, 
            4);
        pnd_Post_Data_Pnd_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Contracts", "#CONTRACTS", FieldType.STRING, 17, new DbsArrayController(1,
            20));

        this.setRecordName("LdaRidl402");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaRidl402() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
