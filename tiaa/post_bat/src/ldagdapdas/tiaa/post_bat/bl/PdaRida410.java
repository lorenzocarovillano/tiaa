/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:05 PM
**        * FROM NATURAL PDA     : RIDA410
************************************************************
**        * FILE NAME            : PdaRida410.java
**        * CLASS NAME           : PdaRida410
**        * INSTANCE NAME        : PdaRida410
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaRida410 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Ctrs;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_St;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Rp;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Sv;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_St;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Rp;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Sv;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_St;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Sv;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_St;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Sv;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Nj;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Nj;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Nj;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Nj;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Pa;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Pa;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Pa;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Pa;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Ct;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Ct;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Ct;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Ct;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Tx;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Tx;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Tx;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Tx;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Va;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Va;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Va;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Va;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Mi;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Mo;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Mo;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Mo;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Mo;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Fl;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Fl;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Fl;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Fl;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Ok;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Ok;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Ok;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Ok;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Or;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Or;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Or;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Or;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Wi;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Wi;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Wi;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Wi;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Sc;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Sc;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Sc;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Sc;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Ne;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Ne;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Ne;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Ne;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Ar;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Hi;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Ky;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Mt;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Mt;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Mt;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Mt;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Nc;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Tn;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Ut;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Ut;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Vt;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Vt;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Pr;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Pr;
    private DbsField pnd_Ctrs_Pnd_Ctr_Ra_Nh;
    private DbsField pnd_Ctrs_Pnd_Ctr_Sr_Nh;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Nh;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Nh;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gr_Njabp;
    private DbsField pnd_Ctrs_Pnd_Ctr_Gs_Suny;
    private DbsGroup pnd_Output;
    private DbsField pnd_Output_Pnd_Pin_A12;
    private DbsGroup pnd_Output_Pnd_Pin_A12Redef1;
    private DbsField pnd_Output_Pnd_Pin_N7;
    private DbsField pnd_Output_Pnd_Pin_A5;
    private DbsField pnd_Output_Pnd_Letter_Type;
    private DbsField pnd_Output_Pnd_Institution_Name;
    private DbsField pnd_Output_Pnd_Level;
    private DbsField pnd_Output_Pnd_Ctr_Ra_St;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Rp;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Sv;
    private DbsField pnd_Output_Pnd_Ctr_Sr_St;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Rp;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Sv;
    private DbsField pnd_Output_Pnd_Ctr_Gr_St;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Sv;
    private DbsField pnd_Output_Pnd_Ctr_Gs_St;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Sv;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Nj;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Nj;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Nj;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Nj;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Pa;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Pa;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Pa;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Pa;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Ct;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Ct;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Ct;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Ct;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Tx;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Tx;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Tx;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Tx;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Va;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Va;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Va;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Va;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Mi;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Mo;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Mo;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Mo;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Mo;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Fl;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Fl;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Fl;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Fl;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Ok;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Ok;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Ok;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Ok;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Or;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Or;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Or;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Or;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Wi;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Wi;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Wi;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Wi;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Sc;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Sc;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Sc;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Sc;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Ne;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Ne;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Ne;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Ne;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Ar;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Hi;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Ky;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Mt;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Mt;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Mt;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Mt;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Nc;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Tn;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Ut;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Ut;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Vt;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Vt;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Pr;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Pr;
    private DbsField pnd_Output_Pnd_Ctr_Ra_Nh;
    private DbsField pnd_Output_Pnd_Ctr_Sr_Nh;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Nh;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Nh;
    private DbsField pnd_Output_Pnd_Ctr_Gr_Njabp;
    private DbsField pnd_Output_Pnd_Ctr_Gs_Suny;
    private DbsField pnd_Output_Pnd_Package_Code;
    private DbsField pnd_Output_Pnd_Contracts;
    private DbsField pnd_Output_Pnd_Email_Address;
    private DbsGroup pnd_Miscellaneous;
    private DbsField pnd_Miscellaneous_Pnd_Suny;
    private DbsField pnd_Miscellaneous_Pnd_Cuny;
    private DbsField pnd_Miscellaneous_Pnd_Njabp;
    private DbsField pnd_Miscellaneous_Pnd_Mo;
    private DbsField pnd_Miscellaneous_Pnd_Mi;
    private DbsField pnd_Miscellaneous_Pnd_Va;
    private DbsField pnd_Miscellaneous_Pnd_Tx;
    private DbsField pnd_Miscellaneous_Pnd_Ca;
    private DbsField pnd_Miscellaneous_Pnd_Co;
    private DbsField pnd_Miscellaneous_Pnd_Dc;
    private DbsField pnd_Miscellaneous_Pnd_De;
    private DbsField pnd_Miscellaneous_Pnd_Fl;
    private DbsField pnd_Miscellaneous_Pnd_Fo;
    private DbsField pnd_Miscellaneous_Pnd_Ga;
    private DbsField pnd_Miscellaneous_Pnd_Ia;
    private DbsField pnd_Miscellaneous_Pnd_Id;
    private DbsField pnd_Miscellaneous_Pnd_Il;
    private DbsField pnd_Miscellaneous_Pnd_In;
    private DbsField pnd_Miscellaneous_Pnd_Me;
    private DbsField pnd_Miscellaneous_Pnd_Nh;
    private DbsField pnd_Miscellaneous_Pnd_Ma;
    private DbsField pnd_Miscellaneous_Pnd_Ri;
    private DbsField pnd_Miscellaneous_Pnd_Vt;
    private DbsField pnd_Miscellaneous_Pnd_Ny;
    private DbsField pnd_Miscellaneous_Pnd_Pa;
    private DbsField pnd_Miscellaneous_Pnd_Md;
    private DbsField pnd_Miscellaneous_Pnd_Wv;
    private DbsField pnd_Miscellaneous_Pnd_Oh;
    private DbsField pnd_Miscellaneous_Pnd_Nc;
    private DbsField pnd_Miscellaneous_Pnd_Sc;
    private DbsField pnd_Miscellaneous_Pnd_Tn;
    private DbsField pnd_Miscellaneous_Pnd_Ky;
    private DbsField pnd_Miscellaneous_Pnd_Ut;
    private DbsField pnd_Miscellaneous_Pnd_Ok;
    private DbsField pnd_Miscellaneous_Pnd_Wi;
    private DbsField pnd_Miscellaneous_Pnd_Ne;
    private DbsField pnd_Miscellaneous_Pnd_Ak;
    private DbsField pnd_Miscellaneous_Pnd_Hi;
    private DbsField pnd_Miscellaneous_Pnd_Ar;
    private DbsField pnd_Miscellaneous_Pnd_Az;
    private DbsField pnd_Miscellaneous_Pnd_Nv;
    private DbsField pnd_Miscellaneous_Pnd_Nm;
    private DbsField pnd_Miscellaneous_Pnd_Mt;
    private DbsField pnd_Miscellaneous_Pnd_Vi;
    private DbsField pnd_Miscellaneous_Pnd_Al;
    private DbsField pnd_Miscellaneous_Pnd_Ms;
    private DbsField pnd_Miscellaneous_Pnd_La;
    private DbsField pnd_Miscellaneous_Pnd_Mn;
    private DbsField pnd_Miscellaneous_Pnd_Nd;
    private DbsField pnd_Miscellaneous_Pnd_Sd;
    private DbsField pnd_Miscellaneous_Pnd_Wy;
    private DbsField pnd_Miscellaneous_Pnd_Ks;
    private DbsField pnd_Miscellaneous_Pnd_Or;
    private DbsField pnd_Miscellaneous_Pnd_Ct;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_St_Rp_Sv;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File1;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File2;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File3;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File4;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File5;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File6;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File7;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File8;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File9;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File10;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File11;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File12;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File13;
    private DbsField pnd_Miscellaneous_Pnd_Ctr_File14;
    private DbsField pnd_Miscellaneous_Pnd_State_Ra_Sv;
    private DbsField pnd_Miscellaneous_Pnd_State_Sra_Sv;
    private DbsField pnd_Miscellaneous_Pnd_State_Gra_Sv;
    private DbsField pnd_Miscellaneous_Pnd_State_Gsra_Sv;

    public DbsGroup getPnd_Ctrs() { return pnd_Ctrs; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_St() { return pnd_Ctrs_Pnd_Ctr_Ra_St; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Rp() { return pnd_Ctrs_Pnd_Ctr_Ra_Rp; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Sv() { return pnd_Ctrs_Pnd_Ctr_Ra_Sv; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_St() { return pnd_Ctrs_Pnd_Ctr_Sr_St; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Rp() { return pnd_Ctrs_Pnd_Ctr_Sr_Rp; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Sv() { return pnd_Ctrs_Pnd_Ctr_Sr_Sv; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_St() { return pnd_Ctrs_Pnd_Ctr_Gr_St; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Sv() { return pnd_Ctrs_Pnd_Ctr_Gr_Sv; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_St() { return pnd_Ctrs_Pnd_Ctr_Gs_St; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Sv() { return pnd_Ctrs_Pnd_Ctr_Gs_Sv; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Nj() { return pnd_Ctrs_Pnd_Ctr_Ra_Nj; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Nj() { return pnd_Ctrs_Pnd_Ctr_Sr_Nj; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Nj() { return pnd_Ctrs_Pnd_Ctr_Gr_Nj; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Nj() { return pnd_Ctrs_Pnd_Ctr_Gs_Nj; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Pa() { return pnd_Ctrs_Pnd_Ctr_Ra_Pa; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Pa() { return pnd_Ctrs_Pnd_Ctr_Sr_Pa; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Pa() { return pnd_Ctrs_Pnd_Ctr_Gr_Pa; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Pa() { return pnd_Ctrs_Pnd_Ctr_Gs_Pa; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Ct() { return pnd_Ctrs_Pnd_Ctr_Ra_Ct; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Ct() { return pnd_Ctrs_Pnd_Ctr_Sr_Ct; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Ct() { return pnd_Ctrs_Pnd_Ctr_Gr_Ct; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Ct() { return pnd_Ctrs_Pnd_Ctr_Gs_Ct; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Tx() { return pnd_Ctrs_Pnd_Ctr_Ra_Tx; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Tx() { return pnd_Ctrs_Pnd_Ctr_Sr_Tx; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Tx() { return pnd_Ctrs_Pnd_Ctr_Gr_Tx; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Tx() { return pnd_Ctrs_Pnd_Ctr_Gs_Tx; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Va() { return pnd_Ctrs_Pnd_Ctr_Ra_Va; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Va() { return pnd_Ctrs_Pnd_Ctr_Sr_Va; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Va() { return pnd_Ctrs_Pnd_Ctr_Gr_Va; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Va() { return pnd_Ctrs_Pnd_Ctr_Gs_Va; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Mi() { return pnd_Ctrs_Pnd_Ctr_Gs_Mi; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Mo() { return pnd_Ctrs_Pnd_Ctr_Ra_Mo; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Mo() { return pnd_Ctrs_Pnd_Ctr_Sr_Mo; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Mo() { return pnd_Ctrs_Pnd_Ctr_Gr_Mo; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Mo() { return pnd_Ctrs_Pnd_Ctr_Gs_Mo; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Fl() { return pnd_Ctrs_Pnd_Ctr_Ra_Fl; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Fl() { return pnd_Ctrs_Pnd_Ctr_Sr_Fl; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Fl() { return pnd_Ctrs_Pnd_Ctr_Gr_Fl; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Fl() { return pnd_Ctrs_Pnd_Ctr_Gs_Fl; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Ok() { return pnd_Ctrs_Pnd_Ctr_Ra_Ok; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Ok() { return pnd_Ctrs_Pnd_Ctr_Sr_Ok; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Ok() { return pnd_Ctrs_Pnd_Ctr_Gr_Ok; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Ok() { return pnd_Ctrs_Pnd_Ctr_Gs_Ok; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Or() { return pnd_Ctrs_Pnd_Ctr_Ra_Or; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Or() { return pnd_Ctrs_Pnd_Ctr_Sr_Or; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Or() { return pnd_Ctrs_Pnd_Ctr_Gr_Or; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Or() { return pnd_Ctrs_Pnd_Ctr_Gs_Or; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Wi() { return pnd_Ctrs_Pnd_Ctr_Ra_Wi; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Wi() { return pnd_Ctrs_Pnd_Ctr_Sr_Wi; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Wi() { return pnd_Ctrs_Pnd_Ctr_Gr_Wi; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Wi() { return pnd_Ctrs_Pnd_Ctr_Gs_Wi; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Sc() { return pnd_Ctrs_Pnd_Ctr_Ra_Sc; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Sc() { return pnd_Ctrs_Pnd_Ctr_Sr_Sc; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Sc() { return pnd_Ctrs_Pnd_Ctr_Gr_Sc; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Sc() { return pnd_Ctrs_Pnd_Ctr_Gs_Sc; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Ne() { return pnd_Ctrs_Pnd_Ctr_Ra_Ne; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Ne() { return pnd_Ctrs_Pnd_Ctr_Sr_Ne; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Ne() { return pnd_Ctrs_Pnd_Ctr_Gr_Ne; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Ne() { return pnd_Ctrs_Pnd_Ctr_Gs_Ne; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Ar() { return pnd_Ctrs_Pnd_Ctr_Gs_Ar; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Hi() { return pnd_Ctrs_Pnd_Ctr_Gs_Hi; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Ky() { return pnd_Ctrs_Pnd_Ctr_Gs_Ky; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Mt() { return pnd_Ctrs_Pnd_Ctr_Ra_Mt; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Mt() { return pnd_Ctrs_Pnd_Ctr_Sr_Mt; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Mt() { return pnd_Ctrs_Pnd_Ctr_Gr_Mt; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Mt() { return pnd_Ctrs_Pnd_Ctr_Gs_Mt; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Nc() { return pnd_Ctrs_Pnd_Ctr_Gr_Nc; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Tn() { return pnd_Ctrs_Pnd_Ctr_Gs_Tn; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Ut() { return pnd_Ctrs_Pnd_Ctr_Ra_Ut; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Ut() { return pnd_Ctrs_Pnd_Ctr_Sr_Ut; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Vt() { return pnd_Ctrs_Pnd_Ctr_Sr_Vt; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Vt() { return pnd_Ctrs_Pnd_Ctr_Gs_Vt; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Pr() { return pnd_Ctrs_Pnd_Ctr_Ra_Pr; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Pr() { return pnd_Ctrs_Pnd_Ctr_Gr_Pr; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Ra_Nh() { return pnd_Ctrs_Pnd_Ctr_Ra_Nh; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Sr_Nh() { return pnd_Ctrs_Pnd_Ctr_Sr_Nh; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Nh() { return pnd_Ctrs_Pnd_Ctr_Gr_Nh; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Nh() { return pnd_Ctrs_Pnd_Ctr_Gs_Nh; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gr_Njabp() { return pnd_Ctrs_Pnd_Ctr_Gr_Njabp; }

    public DbsField getPnd_Ctrs_Pnd_Ctr_Gs_Suny() { return pnd_Ctrs_Pnd_Ctr_Gs_Suny; }

    public DbsGroup getPnd_Output() { return pnd_Output; }

    public DbsField getPnd_Output_Pnd_Pin_A12() { return pnd_Output_Pnd_Pin_A12; }

    public DbsGroup getPnd_Output_Pnd_Pin_A12Redef1() { return pnd_Output_Pnd_Pin_A12Redef1; }

    public DbsField getPnd_Output_Pnd_Pin_N7() { return pnd_Output_Pnd_Pin_N7; }

    public DbsField getPnd_Output_Pnd_Pin_A5() { return pnd_Output_Pnd_Pin_A5; }

    public DbsField getPnd_Output_Pnd_Letter_Type() { return pnd_Output_Pnd_Letter_Type; }

    public DbsField getPnd_Output_Pnd_Institution_Name() { return pnd_Output_Pnd_Institution_Name; }

    public DbsField getPnd_Output_Pnd_Level() { return pnd_Output_Pnd_Level; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_St() { return pnd_Output_Pnd_Ctr_Ra_St; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Rp() { return pnd_Output_Pnd_Ctr_Ra_Rp; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Sv() { return pnd_Output_Pnd_Ctr_Ra_Sv; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_St() { return pnd_Output_Pnd_Ctr_Sr_St; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Rp() { return pnd_Output_Pnd_Ctr_Sr_Rp; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Sv() { return pnd_Output_Pnd_Ctr_Sr_Sv; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_St() { return pnd_Output_Pnd_Ctr_Gr_St; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Sv() { return pnd_Output_Pnd_Ctr_Gr_Sv; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_St() { return pnd_Output_Pnd_Ctr_Gs_St; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Sv() { return pnd_Output_Pnd_Ctr_Gs_Sv; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Nj() { return pnd_Output_Pnd_Ctr_Ra_Nj; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Nj() { return pnd_Output_Pnd_Ctr_Sr_Nj; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Nj() { return pnd_Output_Pnd_Ctr_Gr_Nj; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Nj() { return pnd_Output_Pnd_Ctr_Gs_Nj; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Pa() { return pnd_Output_Pnd_Ctr_Ra_Pa; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Pa() { return pnd_Output_Pnd_Ctr_Sr_Pa; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Pa() { return pnd_Output_Pnd_Ctr_Gr_Pa; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Pa() { return pnd_Output_Pnd_Ctr_Gs_Pa; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Ct() { return pnd_Output_Pnd_Ctr_Ra_Ct; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Ct() { return pnd_Output_Pnd_Ctr_Sr_Ct; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Ct() { return pnd_Output_Pnd_Ctr_Gr_Ct; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Ct() { return pnd_Output_Pnd_Ctr_Gs_Ct; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Tx() { return pnd_Output_Pnd_Ctr_Ra_Tx; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Tx() { return pnd_Output_Pnd_Ctr_Sr_Tx; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Tx() { return pnd_Output_Pnd_Ctr_Gr_Tx; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Tx() { return pnd_Output_Pnd_Ctr_Gs_Tx; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Va() { return pnd_Output_Pnd_Ctr_Ra_Va; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Va() { return pnd_Output_Pnd_Ctr_Sr_Va; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Va() { return pnd_Output_Pnd_Ctr_Gr_Va; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Va() { return pnd_Output_Pnd_Ctr_Gs_Va; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Mi() { return pnd_Output_Pnd_Ctr_Gs_Mi; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Mo() { return pnd_Output_Pnd_Ctr_Ra_Mo; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Mo() { return pnd_Output_Pnd_Ctr_Sr_Mo; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Mo() { return pnd_Output_Pnd_Ctr_Gr_Mo; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Mo() { return pnd_Output_Pnd_Ctr_Gs_Mo; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Fl() { return pnd_Output_Pnd_Ctr_Ra_Fl; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Fl() { return pnd_Output_Pnd_Ctr_Sr_Fl; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Fl() { return pnd_Output_Pnd_Ctr_Gr_Fl; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Fl() { return pnd_Output_Pnd_Ctr_Gs_Fl; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Ok() { return pnd_Output_Pnd_Ctr_Ra_Ok; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Ok() { return pnd_Output_Pnd_Ctr_Sr_Ok; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Ok() { return pnd_Output_Pnd_Ctr_Gr_Ok; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Ok() { return pnd_Output_Pnd_Ctr_Gs_Ok; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Or() { return pnd_Output_Pnd_Ctr_Ra_Or; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Or() { return pnd_Output_Pnd_Ctr_Sr_Or; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Or() { return pnd_Output_Pnd_Ctr_Gr_Or; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Or() { return pnd_Output_Pnd_Ctr_Gs_Or; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Wi() { return pnd_Output_Pnd_Ctr_Ra_Wi; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Wi() { return pnd_Output_Pnd_Ctr_Sr_Wi; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Wi() { return pnd_Output_Pnd_Ctr_Gr_Wi; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Wi() { return pnd_Output_Pnd_Ctr_Gs_Wi; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Sc() { return pnd_Output_Pnd_Ctr_Ra_Sc; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Sc() { return pnd_Output_Pnd_Ctr_Sr_Sc; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Sc() { return pnd_Output_Pnd_Ctr_Gr_Sc; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Sc() { return pnd_Output_Pnd_Ctr_Gs_Sc; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Ne() { return pnd_Output_Pnd_Ctr_Ra_Ne; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Ne() { return pnd_Output_Pnd_Ctr_Sr_Ne; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Ne() { return pnd_Output_Pnd_Ctr_Gr_Ne; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Ne() { return pnd_Output_Pnd_Ctr_Gs_Ne; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Ar() { return pnd_Output_Pnd_Ctr_Gs_Ar; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Hi() { return pnd_Output_Pnd_Ctr_Gs_Hi; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Ky() { return pnd_Output_Pnd_Ctr_Gs_Ky; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Mt() { return pnd_Output_Pnd_Ctr_Ra_Mt; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Mt() { return pnd_Output_Pnd_Ctr_Sr_Mt; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Mt() { return pnd_Output_Pnd_Ctr_Gr_Mt; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Mt() { return pnd_Output_Pnd_Ctr_Gs_Mt; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Nc() { return pnd_Output_Pnd_Ctr_Gr_Nc; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Tn() { return pnd_Output_Pnd_Ctr_Gs_Tn; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Ut() { return pnd_Output_Pnd_Ctr_Ra_Ut; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Ut() { return pnd_Output_Pnd_Ctr_Sr_Ut; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Vt() { return pnd_Output_Pnd_Ctr_Sr_Vt; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Vt() { return pnd_Output_Pnd_Ctr_Gs_Vt; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Pr() { return pnd_Output_Pnd_Ctr_Ra_Pr; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Pr() { return pnd_Output_Pnd_Ctr_Gr_Pr; }

    public DbsField getPnd_Output_Pnd_Ctr_Ra_Nh() { return pnd_Output_Pnd_Ctr_Ra_Nh; }

    public DbsField getPnd_Output_Pnd_Ctr_Sr_Nh() { return pnd_Output_Pnd_Ctr_Sr_Nh; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Nh() { return pnd_Output_Pnd_Ctr_Gr_Nh; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Nh() { return pnd_Output_Pnd_Ctr_Gs_Nh; }

    public DbsField getPnd_Output_Pnd_Ctr_Gr_Njabp() { return pnd_Output_Pnd_Ctr_Gr_Njabp; }

    public DbsField getPnd_Output_Pnd_Ctr_Gs_Suny() { return pnd_Output_Pnd_Ctr_Gs_Suny; }

    public DbsField getPnd_Output_Pnd_Package_Code() { return pnd_Output_Pnd_Package_Code; }

    public DbsField getPnd_Output_Pnd_Contracts() { return pnd_Output_Pnd_Contracts; }

    public DbsField getPnd_Output_Pnd_Email_Address() { return pnd_Output_Pnd_Email_Address; }

    public DbsGroup getPnd_Miscellaneous() { return pnd_Miscellaneous; }

    public DbsField getPnd_Miscellaneous_Pnd_Suny() { return pnd_Miscellaneous_Pnd_Suny; }

    public DbsField getPnd_Miscellaneous_Pnd_Cuny() { return pnd_Miscellaneous_Pnd_Cuny; }

    public DbsField getPnd_Miscellaneous_Pnd_Njabp() { return pnd_Miscellaneous_Pnd_Njabp; }

    public DbsField getPnd_Miscellaneous_Pnd_Mo() { return pnd_Miscellaneous_Pnd_Mo; }

    public DbsField getPnd_Miscellaneous_Pnd_Mi() { return pnd_Miscellaneous_Pnd_Mi; }

    public DbsField getPnd_Miscellaneous_Pnd_Va() { return pnd_Miscellaneous_Pnd_Va; }

    public DbsField getPnd_Miscellaneous_Pnd_Tx() { return pnd_Miscellaneous_Pnd_Tx; }

    public DbsField getPnd_Miscellaneous_Pnd_Ca() { return pnd_Miscellaneous_Pnd_Ca; }

    public DbsField getPnd_Miscellaneous_Pnd_Co() { return pnd_Miscellaneous_Pnd_Co; }

    public DbsField getPnd_Miscellaneous_Pnd_Dc() { return pnd_Miscellaneous_Pnd_Dc; }

    public DbsField getPnd_Miscellaneous_Pnd_De() { return pnd_Miscellaneous_Pnd_De; }

    public DbsField getPnd_Miscellaneous_Pnd_Fl() { return pnd_Miscellaneous_Pnd_Fl; }

    public DbsField getPnd_Miscellaneous_Pnd_Fo() { return pnd_Miscellaneous_Pnd_Fo; }

    public DbsField getPnd_Miscellaneous_Pnd_Ga() { return pnd_Miscellaneous_Pnd_Ga; }

    public DbsField getPnd_Miscellaneous_Pnd_Ia() { return pnd_Miscellaneous_Pnd_Ia; }

    public DbsField getPnd_Miscellaneous_Pnd_Id() { return pnd_Miscellaneous_Pnd_Id; }

    public DbsField getPnd_Miscellaneous_Pnd_Il() { return pnd_Miscellaneous_Pnd_Il; }

    public DbsField getPnd_Miscellaneous_Pnd_In() { return pnd_Miscellaneous_Pnd_In; }

    public DbsField getPnd_Miscellaneous_Pnd_Me() { return pnd_Miscellaneous_Pnd_Me; }

    public DbsField getPnd_Miscellaneous_Pnd_Nh() { return pnd_Miscellaneous_Pnd_Nh; }

    public DbsField getPnd_Miscellaneous_Pnd_Ma() { return pnd_Miscellaneous_Pnd_Ma; }

    public DbsField getPnd_Miscellaneous_Pnd_Ri() { return pnd_Miscellaneous_Pnd_Ri; }

    public DbsField getPnd_Miscellaneous_Pnd_Vt() { return pnd_Miscellaneous_Pnd_Vt; }

    public DbsField getPnd_Miscellaneous_Pnd_Ny() { return pnd_Miscellaneous_Pnd_Ny; }

    public DbsField getPnd_Miscellaneous_Pnd_Pa() { return pnd_Miscellaneous_Pnd_Pa; }

    public DbsField getPnd_Miscellaneous_Pnd_Md() { return pnd_Miscellaneous_Pnd_Md; }

    public DbsField getPnd_Miscellaneous_Pnd_Wv() { return pnd_Miscellaneous_Pnd_Wv; }

    public DbsField getPnd_Miscellaneous_Pnd_Oh() { return pnd_Miscellaneous_Pnd_Oh; }

    public DbsField getPnd_Miscellaneous_Pnd_Nc() { return pnd_Miscellaneous_Pnd_Nc; }

    public DbsField getPnd_Miscellaneous_Pnd_Sc() { return pnd_Miscellaneous_Pnd_Sc; }

    public DbsField getPnd_Miscellaneous_Pnd_Tn() { return pnd_Miscellaneous_Pnd_Tn; }

    public DbsField getPnd_Miscellaneous_Pnd_Ky() { return pnd_Miscellaneous_Pnd_Ky; }

    public DbsField getPnd_Miscellaneous_Pnd_Ut() { return pnd_Miscellaneous_Pnd_Ut; }

    public DbsField getPnd_Miscellaneous_Pnd_Ok() { return pnd_Miscellaneous_Pnd_Ok; }

    public DbsField getPnd_Miscellaneous_Pnd_Wi() { return pnd_Miscellaneous_Pnd_Wi; }

    public DbsField getPnd_Miscellaneous_Pnd_Ne() { return pnd_Miscellaneous_Pnd_Ne; }

    public DbsField getPnd_Miscellaneous_Pnd_Ak() { return pnd_Miscellaneous_Pnd_Ak; }

    public DbsField getPnd_Miscellaneous_Pnd_Hi() { return pnd_Miscellaneous_Pnd_Hi; }

    public DbsField getPnd_Miscellaneous_Pnd_Ar() { return pnd_Miscellaneous_Pnd_Ar; }

    public DbsField getPnd_Miscellaneous_Pnd_Az() { return pnd_Miscellaneous_Pnd_Az; }

    public DbsField getPnd_Miscellaneous_Pnd_Nv() { return pnd_Miscellaneous_Pnd_Nv; }

    public DbsField getPnd_Miscellaneous_Pnd_Nm() { return pnd_Miscellaneous_Pnd_Nm; }

    public DbsField getPnd_Miscellaneous_Pnd_Mt() { return pnd_Miscellaneous_Pnd_Mt; }

    public DbsField getPnd_Miscellaneous_Pnd_Vi() { return pnd_Miscellaneous_Pnd_Vi; }

    public DbsField getPnd_Miscellaneous_Pnd_Al() { return pnd_Miscellaneous_Pnd_Al; }

    public DbsField getPnd_Miscellaneous_Pnd_Ms() { return pnd_Miscellaneous_Pnd_Ms; }

    public DbsField getPnd_Miscellaneous_Pnd_La() { return pnd_Miscellaneous_Pnd_La; }

    public DbsField getPnd_Miscellaneous_Pnd_Mn() { return pnd_Miscellaneous_Pnd_Mn; }

    public DbsField getPnd_Miscellaneous_Pnd_Nd() { return pnd_Miscellaneous_Pnd_Nd; }

    public DbsField getPnd_Miscellaneous_Pnd_Sd() { return pnd_Miscellaneous_Pnd_Sd; }

    public DbsField getPnd_Miscellaneous_Pnd_Wy() { return pnd_Miscellaneous_Pnd_Wy; }

    public DbsField getPnd_Miscellaneous_Pnd_Ks() { return pnd_Miscellaneous_Pnd_Ks; }

    public DbsField getPnd_Miscellaneous_Pnd_Or() { return pnd_Miscellaneous_Pnd_Or; }

    public DbsField getPnd_Miscellaneous_Pnd_Ct() { return pnd_Miscellaneous_Pnd_Ct; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_St_Rp_Sv() { return pnd_Miscellaneous_Pnd_Ctr_St_Rp_Sv; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File1() { return pnd_Miscellaneous_Pnd_Ctr_File1; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File2() { return pnd_Miscellaneous_Pnd_Ctr_File2; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File3() { return pnd_Miscellaneous_Pnd_Ctr_File3; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File4() { return pnd_Miscellaneous_Pnd_Ctr_File4; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File5() { return pnd_Miscellaneous_Pnd_Ctr_File5; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File6() { return pnd_Miscellaneous_Pnd_Ctr_File6; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File7() { return pnd_Miscellaneous_Pnd_Ctr_File7; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File8() { return pnd_Miscellaneous_Pnd_Ctr_File8; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File9() { return pnd_Miscellaneous_Pnd_Ctr_File9; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File10() { return pnd_Miscellaneous_Pnd_Ctr_File10; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File11() { return pnd_Miscellaneous_Pnd_Ctr_File11; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File12() { return pnd_Miscellaneous_Pnd_Ctr_File12; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File13() { return pnd_Miscellaneous_Pnd_Ctr_File13; }

    public DbsField getPnd_Miscellaneous_Pnd_Ctr_File14() { return pnd_Miscellaneous_Pnd_Ctr_File14; }

    public DbsField getPnd_Miscellaneous_Pnd_State_Ra_Sv() { return pnd_Miscellaneous_Pnd_State_Ra_Sv; }

    public DbsField getPnd_Miscellaneous_Pnd_State_Sra_Sv() { return pnd_Miscellaneous_Pnd_State_Sra_Sv; }

    public DbsField getPnd_Miscellaneous_Pnd_State_Gra_Sv() { return pnd_Miscellaneous_Pnd_State_Gra_Sv; }

    public DbsField getPnd_Miscellaneous_Pnd_State_Gsra_Sv() { return pnd_Miscellaneous_Pnd_State_Gsra_Sv; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Ctrs = dbsRecord.newGroupInRecord("pnd_Ctrs", "#CTRS");
        pnd_Ctrs.setParameterOption(ParameterOption.ByReference);
        pnd_Ctrs_Pnd_Ctr_Ra_St = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_St", "#CTR-RA-ST", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Rp = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Rp", "#CTR-RA-RP", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Sv = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Sv", "#CTR-RA-SV", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_St = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_St", "#CTR-SR-ST", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Rp = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Rp", "#CTR-SR-RP", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Sv = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Sv", "#CTR-SR-SV", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_St = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_St", "#CTR-GR-ST", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Sv = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Sv", "#CTR-GR-SV", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_St = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_St", "#CTR-GS-ST", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Sv = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Sv", "#CTR-GS-SV", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Nj = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Nj", "#CTR-RA-NJ", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Nj = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Nj", "#CTR-SR-NJ", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Nj = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Nj", "#CTR-GR-NJ", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Nj = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Nj", "#CTR-GS-NJ", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Pa = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Pa", "#CTR-RA-PA", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Pa = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Pa", "#CTR-SR-PA", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Pa = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Pa", "#CTR-GR-PA", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Pa = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Pa", "#CTR-GS-PA", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Ct = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Ct", "#CTR-RA-CT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Ct = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Ct", "#CTR-SR-CT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Ct = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Ct", "#CTR-GR-CT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Ct = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Ct", "#CTR-GS-CT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Tx = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Tx", "#CTR-RA-TX", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Tx = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Tx", "#CTR-SR-TX", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Tx = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Tx", "#CTR-GR-TX", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Tx = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Tx", "#CTR-GS-TX", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Va = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Va", "#CTR-RA-VA", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Va = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Va", "#CTR-SR-VA", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Va = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Va", "#CTR-GR-VA", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Va = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Va", "#CTR-GS-VA", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Mi = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Mi", "#CTR-GS-MI", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Mo = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Mo", "#CTR-RA-MO", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Mo = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Mo", "#CTR-SR-MO", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Mo = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Mo", "#CTR-GR-MO", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Mo = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Mo", "#CTR-GS-MO", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Fl", "#CTR-RA-FL", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Fl", "#CTR-SR-FL", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Fl", "#CTR-GR-FL", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Fl = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Fl", "#CTR-GS-FL", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Ok = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Ok", "#CTR-RA-OK", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Ok = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Ok", "#CTR-SR-OK", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Ok = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Ok", "#CTR-GR-OK", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Ok = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Ok", "#CTR-GS-OK", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Or = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Or", "#CTR-RA-OR", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Or = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Or", "#CTR-SR-OR", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Or = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Or", "#CTR-GR-OR", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Or = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Or", "#CTR-GS-OR", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Wi = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Wi", "#CTR-RA-WI", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Wi = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Wi", "#CTR-SR-WI", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Wi = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Wi", "#CTR-GR-WI", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Wi = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Wi", "#CTR-GS-WI", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Sc = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Sc", "#CTR-RA-SC", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Sc = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Sc", "#CTR-SR-SC", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Sc = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Sc", "#CTR-GR-SC", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Sc = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Sc", "#CTR-GS-SC", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Ne = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Ne", "#CTR-RA-NE", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Ne = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Ne", "#CTR-SR-NE", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Ne = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Ne", "#CTR-GR-NE", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Ne = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Ne", "#CTR-GS-NE", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Ar = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Ar", "#CTR-GS-AR", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Hi = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Hi", "#CTR-GS-HI", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Ky = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Ky", "#CTR-GS-KY", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Mt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Mt", "#CTR-RA-MT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Mt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Mt", "#CTR-SR-MT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Mt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Mt", "#CTR-GR-MT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Mt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Mt", "#CTR-GS-MT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Nc = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Nc", "#CTR-GR-NC", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Tn = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Tn", "#CTR-GS-TN", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Ut = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Ut", "#CTR-RA-UT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Ut = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Ut", "#CTR-SR-UT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Vt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Vt", "#CTR-SR-VT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Vt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Vt", "#CTR-GS-VT", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Pr = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Pr", "#CTR-RA-PR", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Pr = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Pr", "#CTR-GR-PR", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Ra_Nh = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Ra_Nh", "#CTR-RA-NH", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Sr_Nh = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Sr_Nh", "#CTR-SR-NH", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Nh = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Nh", "#CTR-GR-NH", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Nh = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Nh", "#CTR-GS-NH", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gr_Njabp = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gr_Njabp", "#CTR-GR-NJABP", FieldType.NUMERIC, 1);
        pnd_Ctrs_Pnd_Ctr_Gs_Suny = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_Ctr_Gs_Suny", "#CTR-GS-SUNY", FieldType.NUMERIC, 1);

        pnd_Output = dbsRecord.newGroupInRecord("pnd_Output", "#OUTPUT");
        pnd_Output.setParameterOption(ParameterOption.ByReference);
        pnd_Output_Pnd_Pin_A12 = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);
        pnd_Output_Pnd_Pin_A12Redef1 = pnd_Output.newGroupInGroup("pnd_Output_Pnd_Pin_A12Redef1", "Redefines", pnd_Output_Pnd_Pin_A12);
        pnd_Output_Pnd_Pin_N7 = pnd_Output_Pnd_Pin_A12Redef1.newFieldInGroup("pnd_Output_Pnd_Pin_N7", "#PIN-N7", FieldType.NUMERIC, 7);
        pnd_Output_Pnd_Pin_A5 = pnd_Output_Pnd_Pin_A12Redef1.newFieldInGroup("pnd_Output_Pnd_Pin_A5", "#PIN-A5", FieldType.STRING, 5);
        pnd_Output_Pnd_Letter_Type = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Output_Pnd_Institution_Name = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 120);
        pnd_Output_Pnd_Level = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Level", "#LEVEL", FieldType.STRING, 1);
        pnd_Output_Pnd_Ctr_Ra_St = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_St", "#CTR-RA-ST", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Rp = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Rp", "#CTR-RA-RP", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Sv = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Sv", "#CTR-RA-SV", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_St = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_St", "#CTR-SR-ST", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Rp = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Rp", "#CTR-SR-RP", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Sv = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Sv", "#CTR-SR-SV", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_St = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_St", "#CTR-GR-ST", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Sv = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Sv", "#CTR-GR-SV", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_St = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_St", "#CTR-GS-ST", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Sv = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Sv", "#CTR-GS-SV", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Nj = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Nj", "#CTR-RA-NJ", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Nj = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Nj", "#CTR-SR-NJ", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Nj = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Nj", "#CTR-GR-NJ", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Nj = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Nj", "#CTR-GS-NJ", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Pa = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Pa", "#CTR-RA-PA", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Pa = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Pa", "#CTR-SR-PA", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Pa = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Pa", "#CTR-GR-PA", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Pa = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Pa", "#CTR-GS-PA", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Ct = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Ct", "#CTR-RA-CT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Ct = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Ct", "#CTR-SR-CT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Ct = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Ct", "#CTR-GR-CT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Ct = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Ct", "#CTR-GS-CT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Tx = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Tx", "#CTR-RA-TX", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Tx = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Tx", "#CTR-SR-TX", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Tx = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Tx", "#CTR-GR-TX", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Tx = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Tx", "#CTR-GS-TX", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Va = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Va", "#CTR-RA-VA", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Va = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Va", "#CTR-SR-VA", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Va = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Va", "#CTR-GR-VA", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Va = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Va", "#CTR-GS-VA", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Mi = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Mi", "#CTR-GS-MI", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Mo = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Mo", "#CTR-RA-MO", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Mo = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Mo", "#CTR-SR-MO", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Mo = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Mo", "#CTR-GR-MO", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Mo = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Mo", "#CTR-GS-MO", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Fl = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Fl", "#CTR-RA-FL", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Fl = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Fl", "#CTR-SR-FL", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Fl = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Fl", "#CTR-GR-FL", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Fl = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Fl", "#CTR-GS-FL", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Ok = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Ok", "#CTR-RA-OK", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Ok = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Ok", "#CTR-SR-OK", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Ok = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Ok", "#CTR-GR-OK", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Ok = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Ok", "#CTR-GS-OK", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Or = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Or", "#CTR-RA-OR", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Or = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Or", "#CTR-SR-OR", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Or = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Or", "#CTR-GR-OR", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Or = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Or", "#CTR-GS-OR", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Wi = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Wi", "#CTR-RA-WI", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Wi = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Wi", "#CTR-SR-WI", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Wi = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Wi", "#CTR-GR-WI", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Wi = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Wi", "#CTR-GS-WI", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Sc = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Sc", "#CTR-RA-SC", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Sc = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Sc", "#CTR-SR-SC", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Sc = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Sc", "#CTR-GR-SC", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Sc = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Sc", "#CTR-GS-SC", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Ne = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Ne", "#CTR-RA-NE", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Ne = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Ne", "#CTR-SR-NE", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Ne = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Ne", "#CTR-GR-NE", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Ne = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Ne", "#CTR-GS-NE", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Ar = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Ar", "#CTR-GS-AR", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Hi = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Hi", "#CTR-GS-HI", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Ky = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Ky", "#CTR-GS-KY", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Mt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Mt", "#CTR-RA-MT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Mt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Mt", "#CTR-SR-MT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Mt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Mt", "#CTR-GR-MT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Mt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Mt", "#CTR-GS-MT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Nc = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Nc", "#CTR-GR-NC", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Tn = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Tn", "#CTR-GS-TN", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Ut = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Ut", "#CTR-RA-UT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Ut = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Ut", "#CTR-SR-UT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Vt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Vt", "#CTR-SR-VT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Vt = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Vt", "#CTR-GS-VT", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Pr = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Pr", "#CTR-RA-PR", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Pr = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Pr", "#CTR-GR-PR", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Ra_Nh = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Ra_Nh", "#CTR-RA-NH", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Sr_Nh = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Sr_Nh", "#CTR-SR-NH", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Nh = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Nh", "#CTR-GR-NH", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Nh = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Nh", "#CTR-GS-NH", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gr_Njabp = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gr_Njabp", "#CTR-GR-NJABP", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Ctr_Gs_Suny = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Ctr_Gs_Suny", "#CTR-GS-SUNY", FieldType.NUMERIC, 1);
        pnd_Output_Pnd_Package_Code = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Output_Pnd_Contracts = pnd_Output.newFieldArrayInGroup("pnd_Output_Pnd_Contracts", "#CONTRACTS", FieldType.STRING, 8, new DbsArrayController(1,
            20));
        pnd_Output_Pnd_Email_Address = pnd_Output.newFieldInGroup("pnd_Output_Pnd_Email_Address", "#EMAIL-ADDRESS", FieldType.STRING, 100);

        pnd_Miscellaneous = dbsRecord.newGroupInRecord("pnd_Miscellaneous", "#MISCELLANEOUS");
        pnd_Miscellaneous.setParameterOption(ParameterOption.ByReference);
        pnd_Miscellaneous_Pnd_Suny = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Suny", "#SUNY", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Cuny = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Cuny", "#CUNY", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Njabp = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Njabp", "#NJABP", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Mo = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Mo", "#MO", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Mi = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Mi", "#MI", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Va = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Va", "#VA", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Tx = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Tx", "#TX", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ca = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ca", "#CA", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Co = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Co", "#CO", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Dc = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Dc", "#DC", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_De = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_De", "#DE", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Fl = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Fl", "#FL", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Fo = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Fo", "#FO", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ga = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ga", "#GA", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ia = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ia", "#IA", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Id = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Id", "#ID", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Il = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Il", "#IL", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_In = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_In", "#IN", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Me = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Me", "#ME", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Nh = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Nh", "#NH", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ma = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ma", "#MA", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ri = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ri", "#RI", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Vt = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Vt", "#VT", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ny = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ny", "#NY", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Pa = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Pa", "#PA", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Md = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Md", "#MD", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Wv = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Wv", "#WV", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Oh = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Oh", "#OH", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Nc = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Nc", "#NC", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Sc = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Sc", "#SC", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Tn = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Tn", "#TN", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ky = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ky", "#KY", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ut = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ut", "#UT", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ok = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ok", "#OK", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Wi = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Wi", "#WI", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ne = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ne", "#NE", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ak = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ak", "#AK", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Hi = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Hi", "#HI", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ar = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ar", "#AR", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Az = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Az", "#AZ", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Nv = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Nv", "#NV", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Nm = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Nm", "#NM", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Mt = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Mt", "#MT", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Vi = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Vi", "#VI", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Al = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Al", "#AL", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ms = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ms", "#MS", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_La = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_La", "#LA", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Mn = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Mn", "#MN", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Nd = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Nd", "#ND", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Sd = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Sd", "#SD", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Wy = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Wy", "#WY", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ks = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ks", "#KS", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Or = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Or", "#OR", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ct = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ct", "#CT", FieldType.BOOLEAN);
        pnd_Miscellaneous_Pnd_Ctr_St_Rp_Sv = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_St_Rp_Sv", "#CTR-ST-RP-SV", FieldType.NUMERIC, 
            7);
        pnd_Miscellaneous_Pnd_Ctr_File1 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File1", "#CTR-FILE1", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File2 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File2", "#CTR-FILE2", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File3 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File3", "#CTR-FILE3", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File4 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File4", "#CTR-FILE4", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File5 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File5", "#CTR-FILE5", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File6 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File6", "#CTR-FILE6", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File7 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File7", "#CTR-FILE7", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File8 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File8", "#CTR-FILE8", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File9 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File9", "#CTR-FILE9", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File10 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File10", "#CTR-FILE10", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File11 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File11", "#CTR-FILE11", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File12 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File12", "#CTR-FILE12", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File13 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File13", "#CTR-FILE13", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_Ctr_File14 = pnd_Miscellaneous.newFieldInGroup("pnd_Miscellaneous_Pnd_Ctr_File14", "#CTR-FILE14", FieldType.NUMERIC, 7);
        pnd_Miscellaneous_Pnd_State_Ra_Sv = pnd_Miscellaneous.newFieldArrayInGroup("pnd_Miscellaneous_Pnd_State_Ra_Sv", "#STATE-RA-SV", FieldType.NUMERIC, 
            7, new DbsArrayController(1,52));
        pnd_Miscellaneous_Pnd_State_Sra_Sv = pnd_Miscellaneous.newFieldArrayInGroup("pnd_Miscellaneous_Pnd_State_Sra_Sv", "#STATE-SRA-SV", FieldType.NUMERIC, 
            7, new DbsArrayController(1,52));
        pnd_Miscellaneous_Pnd_State_Gra_Sv = pnd_Miscellaneous.newFieldArrayInGroup("pnd_Miscellaneous_Pnd_State_Gra_Sv", "#STATE-GRA-SV", FieldType.NUMERIC, 
            7, new DbsArrayController(1,52));
        pnd_Miscellaneous_Pnd_State_Gsra_Sv = pnd_Miscellaneous.newFieldArrayInGroup("pnd_Miscellaneous_Pnd_State_Gsra_Sv", "#STATE-GSRA-SV", FieldType.NUMERIC, 
            7, new DbsArrayController(1,52));

        dbsRecord.reset();
    }

    // Constructors
    public PdaRida410(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

