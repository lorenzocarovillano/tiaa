/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:11:40 PM
**        * FROM NATURAL LDA     : RIDL634
************************************************************
**        * FILE NAME            : LdaRidl634.java
**        * CLASS NAME           : LdaRidl634
**        * INSTANCE NAME        : LdaRidl634
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaRidl634 extends DbsRecord
{
    // Properties
    private DbsGroup ecs_Record;
    private DbsField ecs_Record_Ecs_Out_Data;
    private DbsGroup ecs_Record_Ecs_Out_DataRedef1;
    private DbsGroup ecs_Record_Mi_Record;
    private DbsField ecs_Record_Rec_Id;
    private DbsField ecs_Record_Pst_Rqst_Id;
    private DbsField ecs_Record_Pin_Nbr;
    private DbsField ecs_Record_Ssn_Nbr;
    private DbsField ecs_Record_Full_Nme;
    private DbsField ecs_Record_Last_Nme;
    private DbsField ecs_Record_Tiaa_Cntrct_Nbr;
    private DbsField ecs_Record_Addrss_Typ_Cde;
    private DbsField ecs_Record_Addrss_Line_Txt;
    private DbsGroup ecs_Record_Addrss_Line_TxtRedef2;
    private DbsField ecs_Record_Addrss_Line_1;
    private DbsField ecs_Record_Addrss_Line_2;
    private DbsField ecs_Record_Addrss_Line_3;
    private DbsField ecs_Record_Addrss_Line_4;
    private DbsField ecs_Record_Addrss_Line_5;
    private DbsField ecs_Record_Addrss_Line_6;
    private DbsField ecs_Record_Postnet_Barcode;
    private DbsField ecs_Record_Prntr_Id_Cde;
    private DbsField ecs_Record_Batch_Id_Cde;
    private DbsField ecs_Record_Pckge_Cde;
    private DbsField ecs_Record_Pckge_Dlvry_Typ;
    private DbsField ecs_Record_Pckge_Image_Ind;
    private DbsField ecs_Record_Image_Srce_Id;
    private DbsField ecs_Record_Image_Id;
    private DbsField ecs_Record_Alt_Dlvry_Addr;
    private DbsField ecs_Record_Letter_Dte;
    private DbsField ecs_Record_Lttr_Slttn_Txt;
    private DbsField ecs_Record_Empl_Sgntry_Nme;
    private DbsField ecs_Record_Empl_Unit_Work_Nme;
    private DbsField ecs_Record_Pckge_Immdte_Prnt_Ind;
    private DbsField ecs_Record_Byte2;
    private DbsField ecs_Record_Byte4;
    private DbsField ecs_Record_Byte5;
    private DbsField ecs_Record_App_Date;
    private DbsField ecs_Record_Approved_Ind;
    private DbsGroup ecs_Record_Ecs_Out_DataRedef3;
    private DbsGroup ecs_Record_Rm_Record;
    private DbsField ecs_Record_Rm_Record_Type;
    private DbsField ecs_Record_Rm_Cover_Letter_Code;
    private DbsField ecs_Record_Rm_Institution_Name;
    private DbsField ecs_Record_Rm_Signatory_Name;
    private DbsField ecs_Record_Rm_Signatory_Title1;
    private DbsField ecs_Record_Rm_Signatory_Title2;
    private DbsField ecs_Record_Rm_Signatory_Title3;
    private DbsField ecs_Record_Rm_Signatory_Title4;
    private DbsField ecs_Record_Rm_Access_Lvl;
    private DbsField ecs_Record_Rm_Cont_Num;
    private DbsField ecs_Record_Rm_Roth_Plan_Type;
    private DbsField ecs_Record_Rm_Prospectus_Pdf_Website;
    private DbsField ecs_Record_Rm_Stable_Value_Ind;
    private DbsField ecs_Record_Rm_Filler;
    private DbsGroup ecs_Record_Rm_FillerRedef4;
    private DbsField ecs_Record_Rm_Roth_Plan_Type2;
    private DbsField ecs_Record_Rm_Share_Class_Effect_Date;
    private DbsGroup ecs_Record_Ecs_Out_DataRedef5;
    private DbsGroup ecs_Record_Ap_Record;
    private DbsField ecs_Record_Ap_Record_Type;
    private DbsField ecs_Record_Ap_Cont_Num;
    private DbsGroup ecs_Record_Ecs_Out_DataRedef6;
    private DbsGroup ecs_Record_Cd_Record;
    private DbsField ecs_Record_Cd_Record_Type;
    private DbsField ecs_Record_Cd_Contract_Number;
    private DbsGroup ecs_Record_Ecs_Out_DataRedef7;
    private DbsGroup ecs_Record_Sp_Record;
    private DbsField ecs_Record_Sp_Record_Type;
    private DbsField ecs_Record_Sp_Stapling_Ind;
    private DbsGroup ecs_Record_Ecs_Out_DataRedef8;
    private DbsGroup ecs_Record_Ep_Record;
    private DbsField ecs_Record_Ep_Record_Type;
    private DbsField ecs_Record_Ep_Pdf_Name;
    private DbsGroup ecs_Record_Ecs_Out_DataRedef9;
    private DbsGroup ecs_Record_Es_Record;
    private DbsField ecs_Record_Es_Record_Type;
    private DbsField ecs_Record_Es_Pdf_Name;
    private DbsGroup ecs_Record_Ecs_Out_DataRedef10;
    private DbsGroup ecs_Record_En_Record;
    private DbsField ecs_Record_En_Record_Type;
    private DbsField ecs_Record_En_Endorse_Type;
    private DbsField ecs_Record_En_Contract_Number;
    private DbsField ecs_Record_En_Multi_Plan_Ind;
    private DbsGroup ecs_Record_En_Plan_Detail;
    private DbsField ecs_Record_En_Plan_Name;
    private DbsField ecs_Record_En_Plan_Share_Class;

    public DbsGroup getEcs_Record() { return ecs_Record; }

    public DbsField getEcs_Record_Ecs_Out_Data() { return ecs_Record_Ecs_Out_Data; }

    public DbsGroup getEcs_Record_Ecs_Out_DataRedef1() { return ecs_Record_Ecs_Out_DataRedef1; }

    public DbsGroup getEcs_Record_Mi_Record() { return ecs_Record_Mi_Record; }

    public DbsField getEcs_Record_Rec_Id() { return ecs_Record_Rec_Id; }

    public DbsField getEcs_Record_Pst_Rqst_Id() { return ecs_Record_Pst_Rqst_Id; }

    public DbsField getEcs_Record_Pin_Nbr() { return ecs_Record_Pin_Nbr; }

    public DbsField getEcs_Record_Ssn_Nbr() { return ecs_Record_Ssn_Nbr; }

    public DbsField getEcs_Record_Full_Nme() { return ecs_Record_Full_Nme; }

    public DbsField getEcs_Record_Last_Nme() { return ecs_Record_Last_Nme; }

    public DbsField getEcs_Record_Tiaa_Cntrct_Nbr() { return ecs_Record_Tiaa_Cntrct_Nbr; }

    public DbsField getEcs_Record_Addrss_Typ_Cde() { return ecs_Record_Addrss_Typ_Cde; }

    public DbsField getEcs_Record_Addrss_Line_Txt() { return ecs_Record_Addrss_Line_Txt; }

    public DbsGroup getEcs_Record_Addrss_Line_TxtRedef2() { return ecs_Record_Addrss_Line_TxtRedef2; }

    public DbsField getEcs_Record_Addrss_Line_1() { return ecs_Record_Addrss_Line_1; }

    public DbsField getEcs_Record_Addrss_Line_2() { return ecs_Record_Addrss_Line_2; }

    public DbsField getEcs_Record_Addrss_Line_3() { return ecs_Record_Addrss_Line_3; }

    public DbsField getEcs_Record_Addrss_Line_4() { return ecs_Record_Addrss_Line_4; }

    public DbsField getEcs_Record_Addrss_Line_5() { return ecs_Record_Addrss_Line_5; }

    public DbsField getEcs_Record_Addrss_Line_6() { return ecs_Record_Addrss_Line_6; }

    public DbsField getEcs_Record_Postnet_Barcode() { return ecs_Record_Postnet_Barcode; }

    public DbsField getEcs_Record_Prntr_Id_Cde() { return ecs_Record_Prntr_Id_Cde; }

    public DbsField getEcs_Record_Batch_Id_Cde() { return ecs_Record_Batch_Id_Cde; }

    public DbsField getEcs_Record_Pckge_Cde() { return ecs_Record_Pckge_Cde; }

    public DbsField getEcs_Record_Pckge_Dlvry_Typ() { return ecs_Record_Pckge_Dlvry_Typ; }

    public DbsField getEcs_Record_Pckge_Image_Ind() { return ecs_Record_Pckge_Image_Ind; }

    public DbsField getEcs_Record_Image_Srce_Id() { return ecs_Record_Image_Srce_Id; }

    public DbsField getEcs_Record_Image_Id() { return ecs_Record_Image_Id; }

    public DbsField getEcs_Record_Alt_Dlvry_Addr() { return ecs_Record_Alt_Dlvry_Addr; }

    public DbsField getEcs_Record_Letter_Dte() { return ecs_Record_Letter_Dte; }

    public DbsField getEcs_Record_Lttr_Slttn_Txt() { return ecs_Record_Lttr_Slttn_Txt; }

    public DbsField getEcs_Record_Empl_Sgntry_Nme() { return ecs_Record_Empl_Sgntry_Nme; }

    public DbsField getEcs_Record_Empl_Unit_Work_Nme() { return ecs_Record_Empl_Unit_Work_Nme; }

    public DbsField getEcs_Record_Pckge_Immdte_Prnt_Ind() { return ecs_Record_Pckge_Immdte_Prnt_Ind; }

    public DbsField getEcs_Record_Byte2() { return ecs_Record_Byte2; }

    public DbsField getEcs_Record_Byte4() { return ecs_Record_Byte4; }

    public DbsField getEcs_Record_Byte5() { return ecs_Record_Byte5; }

    public DbsField getEcs_Record_App_Date() { return ecs_Record_App_Date; }

    public DbsField getEcs_Record_Approved_Ind() { return ecs_Record_Approved_Ind; }

    public DbsGroup getEcs_Record_Ecs_Out_DataRedef3() { return ecs_Record_Ecs_Out_DataRedef3; }

    public DbsGroup getEcs_Record_Rm_Record() { return ecs_Record_Rm_Record; }

    public DbsField getEcs_Record_Rm_Record_Type() { return ecs_Record_Rm_Record_Type; }

    public DbsField getEcs_Record_Rm_Cover_Letter_Code() { return ecs_Record_Rm_Cover_Letter_Code; }

    public DbsField getEcs_Record_Rm_Institution_Name() { return ecs_Record_Rm_Institution_Name; }

    public DbsField getEcs_Record_Rm_Signatory_Name() { return ecs_Record_Rm_Signatory_Name; }

    public DbsField getEcs_Record_Rm_Signatory_Title1() { return ecs_Record_Rm_Signatory_Title1; }

    public DbsField getEcs_Record_Rm_Signatory_Title2() { return ecs_Record_Rm_Signatory_Title2; }

    public DbsField getEcs_Record_Rm_Signatory_Title3() { return ecs_Record_Rm_Signatory_Title3; }

    public DbsField getEcs_Record_Rm_Signatory_Title4() { return ecs_Record_Rm_Signatory_Title4; }

    public DbsField getEcs_Record_Rm_Access_Lvl() { return ecs_Record_Rm_Access_Lvl; }

    public DbsField getEcs_Record_Rm_Cont_Num() { return ecs_Record_Rm_Cont_Num; }

    public DbsField getEcs_Record_Rm_Roth_Plan_Type() { return ecs_Record_Rm_Roth_Plan_Type; }

    public DbsField getEcs_Record_Rm_Prospectus_Pdf_Website() { return ecs_Record_Rm_Prospectus_Pdf_Website; }

    public DbsField getEcs_Record_Rm_Stable_Value_Ind() { return ecs_Record_Rm_Stable_Value_Ind; }

    public DbsField getEcs_Record_Rm_Filler() { return ecs_Record_Rm_Filler; }

    public DbsGroup getEcs_Record_Rm_FillerRedef4() { return ecs_Record_Rm_FillerRedef4; }

    public DbsField getEcs_Record_Rm_Roth_Plan_Type2() { return ecs_Record_Rm_Roth_Plan_Type2; }

    public DbsField getEcs_Record_Rm_Share_Class_Effect_Date() { return ecs_Record_Rm_Share_Class_Effect_Date; }

    public DbsGroup getEcs_Record_Ecs_Out_DataRedef5() { return ecs_Record_Ecs_Out_DataRedef5; }

    public DbsGroup getEcs_Record_Ap_Record() { return ecs_Record_Ap_Record; }

    public DbsField getEcs_Record_Ap_Record_Type() { return ecs_Record_Ap_Record_Type; }

    public DbsField getEcs_Record_Ap_Cont_Num() { return ecs_Record_Ap_Cont_Num; }

    public DbsGroup getEcs_Record_Ecs_Out_DataRedef6() { return ecs_Record_Ecs_Out_DataRedef6; }

    public DbsGroup getEcs_Record_Cd_Record() { return ecs_Record_Cd_Record; }

    public DbsField getEcs_Record_Cd_Record_Type() { return ecs_Record_Cd_Record_Type; }

    public DbsField getEcs_Record_Cd_Contract_Number() { return ecs_Record_Cd_Contract_Number; }

    public DbsGroup getEcs_Record_Ecs_Out_DataRedef7() { return ecs_Record_Ecs_Out_DataRedef7; }

    public DbsGroup getEcs_Record_Sp_Record() { return ecs_Record_Sp_Record; }

    public DbsField getEcs_Record_Sp_Record_Type() { return ecs_Record_Sp_Record_Type; }

    public DbsField getEcs_Record_Sp_Stapling_Ind() { return ecs_Record_Sp_Stapling_Ind; }

    public DbsGroup getEcs_Record_Ecs_Out_DataRedef8() { return ecs_Record_Ecs_Out_DataRedef8; }

    public DbsGroup getEcs_Record_Ep_Record() { return ecs_Record_Ep_Record; }

    public DbsField getEcs_Record_Ep_Record_Type() { return ecs_Record_Ep_Record_Type; }

    public DbsField getEcs_Record_Ep_Pdf_Name() { return ecs_Record_Ep_Pdf_Name; }

    public DbsGroup getEcs_Record_Ecs_Out_DataRedef9() { return ecs_Record_Ecs_Out_DataRedef9; }

    public DbsGroup getEcs_Record_Es_Record() { return ecs_Record_Es_Record; }

    public DbsField getEcs_Record_Es_Record_Type() { return ecs_Record_Es_Record_Type; }

    public DbsField getEcs_Record_Es_Pdf_Name() { return ecs_Record_Es_Pdf_Name; }

    public DbsGroup getEcs_Record_Ecs_Out_DataRedef10() { return ecs_Record_Ecs_Out_DataRedef10; }

    public DbsGroup getEcs_Record_En_Record() { return ecs_Record_En_Record; }

    public DbsField getEcs_Record_En_Record_Type() { return ecs_Record_En_Record_Type; }

    public DbsField getEcs_Record_En_Endorse_Type() { return ecs_Record_En_Endorse_Type; }

    public DbsField getEcs_Record_En_Contract_Number() { return ecs_Record_En_Contract_Number; }

    public DbsField getEcs_Record_En_Multi_Plan_Ind() { return ecs_Record_En_Multi_Plan_Ind; }

    public DbsGroup getEcs_Record_En_Plan_Detail() { return ecs_Record_En_Plan_Detail; }

    public DbsField getEcs_Record_En_Plan_Name() { return ecs_Record_En_Plan_Name; }

    public DbsField getEcs_Record_En_Plan_Share_Class() { return ecs_Record_En_Plan_Share_Class; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ecs_Record = newGroupInRecord("ecs_Record", "ECS-RECORD");
        ecs_Record_Ecs_Out_Data = ecs_Record.newFieldArrayInGroup("ecs_Record_Ecs_Out_Data", "ECS-OUT-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            850));
        ecs_Record_Ecs_Out_DataRedef1 = ecs_Record.newGroupInGroup("ecs_Record_Ecs_Out_DataRedef1", "Redefines", ecs_Record_Ecs_Out_Data);
        ecs_Record_Mi_Record = ecs_Record_Ecs_Out_DataRedef1.newGroupInGroup("ecs_Record_Mi_Record", "MI-RECORD");
        ecs_Record_Rec_Id = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Rec_Id", "REC-ID", FieldType.STRING, 2);
        ecs_Record_Pst_Rqst_Id = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Pst_Rqst_Id", "PST-RQST-ID", FieldType.STRING, 11);
        ecs_Record_Pin_Nbr = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Pin_Nbr", "PIN-NBR", FieldType.STRING, 13);
        ecs_Record_Ssn_Nbr = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Ssn_Nbr", "SSN-NBR", FieldType.STRING, 11);
        ecs_Record_Full_Nme = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Full_Nme", "FULL-NME", FieldType.STRING, 35);
        ecs_Record_Last_Nme = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Last_Nme", "LAST-NME", FieldType.STRING, 35);
        ecs_Record_Tiaa_Cntrct_Nbr = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Tiaa_Cntrct_Nbr", "TIAA-CNTRCT-NBR", FieldType.STRING, 9);
        ecs_Record_Addrss_Typ_Cde = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Addrss_Typ_Cde", "ADDRSS-TYP-CDE", FieldType.STRING, 1);
        ecs_Record_Addrss_Line_Txt = ecs_Record_Mi_Record.newFieldArrayInGroup("ecs_Record_Addrss_Line_Txt", "ADDRSS-LINE-TXT", FieldType.STRING, 35, 
            new DbsArrayController(1,6));
        ecs_Record_Addrss_Line_TxtRedef2 = ecs_Record_Mi_Record.newGroupInGroup("ecs_Record_Addrss_Line_TxtRedef2", "Redefines", ecs_Record_Addrss_Line_Txt);
        ecs_Record_Addrss_Line_1 = ecs_Record_Addrss_Line_TxtRedef2.newFieldInGroup("ecs_Record_Addrss_Line_1", "ADDRSS-LINE-1", FieldType.STRING, 35);
        ecs_Record_Addrss_Line_2 = ecs_Record_Addrss_Line_TxtRedef2.newFieldInGroup("ecs_Record_Addrss_Line_2", "ADDRSS-LINE-2", FieldType.STRING, 35);
        ecs_Record_Addrss_Line_3 = ecs_Record_Addrss_Line_TxtRedef2.newFieldInGroup("ecs_Record_Addrss_Line_3", "ADDRSS-LINE-3", FieldType.STRING, 35);
        ecs_Record_Addrss_Line_4 = ecs_Record_Addrss_Line_TxtRedef2.newFieldInGroup("ecs_Record_Addrss_Line_4", "ADDRSS-LINE-4", FieldType.STRING, 35);
        ecs_Record_Addrss_Line_5 = ecs_Record_Addrss_Line_TxtRedef2.newFieldInGroup("ecs_Record_Addrss_Line_5", "ADDRSS-LINE-5", FieldType.STRING, 35);
        ecs_Record_Addrss_Line_6 = ecs_Record_Addrss_Line_TxtRedef2.newFieldInGroup("ecs_Record_Addrss_Line_6", "ADDRSS-LINE-6", FieldType.STRING, 35);
        ecs_Record_Postnet_Barcode = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Postnet_Barcode", "POSTNET-BARCODE", FieldType.STRING, 15);
        ecs_Record_Prntr_Id_Cde = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Prntr_Id_Cde", "PRNTR-ID-CDE", FieldType.STRING, 8);
        ecs_Record_Batch_Id_Cde = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Batch_Id_Cde", "BATCH-ID-CDE", FieldType.STRING, 12);
        ecs_Record_Pckge_Cde = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Pckge_Cde", "PCKGE-CDE", FieldType.STRING, 8);
        ecs_Record_Pckge_Dlvry_Typ = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Pckge_Dlvry_Typ", "PCKGE-DLVRY-TYP", FieldType.STRING, 3);
        ecs_Record_Pckge_Image_Ind = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Pckge_Image_Ind", "PCKGE-IMAGE-IND", FieldType.STRING, 1);
        ecs_Record_Image_Srce_Id = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Image_Srce_Id", "IMAGE-SRCE-ID", FieldType.STRING, 6);
        ecs_Record_Image_Id = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Image_Id", "IMAGE-ID", FieldType.STRING, 11);
        ecs_Record_Alt_Dlvry_Addr = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Alt_Dlvry_Addr", "ALT-DLVRY-ADDR", FieldType.STRING, 100);
        ecs_Record_Letter_Dte = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Letter_Dte", "LETTER-DTE", FieldType.STRING, 8);
        ecs_Record_Lttr_Slttn_Txt = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Lttr_Slttn_Txt", "LTTR-SLTTN-TXT", FieldType.STRING, 40);
        ecs_Record_Empl_Sgntry_Nme = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Empl_Sgntry_Nme", "EMPL-SGNTRY-NME", FieldType.STRING, 35);
        ecs_Record_Empl_Unit_Work_Nme = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Empl_Unit_Work_Nme", "EMPL-UNIT-WORK-NME", FieldType.STRING, 
            45);
        ecs_Record_Pckge_Immdte_Prnt_Ind = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Pckge_Immdte_Prnt_Ind", "PCKGE-IMMDTE-PRNT-IND", FieldType.STRING, 
            1);
        ecs_Record_Byte2 = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Byte2", "BYTE2", FieldType.STRING, 1);
        ecs_Record_Byte4 = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Byte4", "BYTE4", FieldType.STRING, 1);
        ecs_Record_Byte5 = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Byte5", "BYTE5", FieldType.STRING, 1);
        ecs_Record_App_Date = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_App_Date", "APP-DATE", FieldType.STRING, 8);
        ecs_Record_Approved_Ind = ecs_Record_Mi_Record.newFieldInGroup("ecs_Record_Approved_Ind", "APPROVED-IND", FieldType.STRING, 1);
        ecs_Record_Ecs_Out_DataRedef3 = ecs_Record.newGroupInGroup("ecs_Record_Ecs_Out_DataRedef3", "Redefines", ecs_Record_Ecs_Out_Data);
        ecs_Record_Rm_Record = ecs_Record_Ecs_Out_DataRedef3.newGroupInGroup("ecs_Record_Rm_Record", "RM-RECORD");
        ecs_Record_Rm_Record_Type = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Record_Type", "RM-RECORD-TYPE", FieldType.STRING, 2);
        ecs_Record_Rm_Cover_Letter_Code = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Cover_Letter_Code", "RM-COVER-LETTER-CODE", FieldType.STRING, 
            10);
        ecs_Record_Rm_Institution_Name = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Institution_Name", "RM-INSTITUTION-NAME", FieldType.STRING, 
            64);
        ecs_Record_Rm_Signatory_Name = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Signatory_Name", "RM-SIGNATORY-NAME", FieldType.STRING, 40);
        ecs_Record_Rm_Signatory_Title1 = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Signatory_Title1", "RM-SIGNATORY-TITLE1", FieldType.STRING, 
            50);
        ecs_Record_Rm_Signatory_Title2 = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Signatory_Title2", "RM-SIGNATORY-TITLE2", FieldType.STRING, 
            50);
        ecs_Record_Rm_Signatory_Title3 = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Signatory_Title3", "RM-SIGNATORY-TITLE3", FieldType.STRING, 
            50);
        ecs_Record_Rm_Signatory_Title4 = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Signatory_Title4", "RM-SIGNATORY-TITLE4", FieldType.STRING, 
            50);
        ecs_Record_Rm_Access_Lvl = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Access_Lvl", "RM-ACCESS-LVL", FieldType.STRING, 1);
        ecs_Record_Rm_Cont_Num = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Cont_Num", "RM-CONT-NUM", FieldType.STRING, 60);
        ecs_Record_Rm_Roth_Plan_Type = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Roth_Plan_Type", "RM-ROTH-PLAN-TYPE", FieldType.STRING, 30);
        ecs_Record_Rm_Prospectus_Pdf_Website = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Prospectus_Pdf_Website", "RM-PROSPECTUS-PDF-WEBSITE", 
            FieldType.STRING, 1);
        ecs_Record_Rm_Stable_Value_Ind = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Stable_Value_Ind", "RM-STABLE-VALUE-IND", FieldType.STRING, 
            1);
        ecs_Record_Rm_Filler = ecs_Record_Rm_Record.newFieldInGroup("ecs_Record_Rm_Filler", "RM-FILLER", FieldType.STRING, 100);
        ecs_Record_Rm_FillerRedef4 = ecs_Record_Rm_Record.newGroupInGroup("ecs_Record_Rm_FillerRedef4", "Redefines", ecs_Record_Rm_Filler);
        ecs_Record_Rm_Roth_Plan_Type2 = ecs_Record_Rm_FillerRedef4.newFieldInGroup("ecs_Record_Rm_Roth_Plan_Type2", "RM-ROTH-PLAN-TYPE2", FieldType.STRING, 
            50);
        ecs_Record_Rm_Share_Class_Effect_Date = ecs_Record_Rm_FillerRedef4.newFieldInGroup("ecs_Record_Rm_Share_Class_Effect_Date", "RM-SHARE-CLASS-EFFECT-DATE", 
            FieldType.STRING, 8);
        ecs_Record_Ecs_Out_DataRedef5 = ecs_Record.newGroupInGroup("ecs_Record_Ecs_Out_DataRedef5", "Redefines", ecs_Record_Ecs_Out_Data);
        ecs_Record_Ap_Record = ecs_Record_Ecs_Out_DataRedef5.newGroupInGroup("ecs_Record_Ap_Record", "AP-RECORD");
        ecs_Record_Ap_Record_Type = ecs_Record_Ap_Record.newFieldInGroup("ecs_Record_Ap_Record_Type", "AP-RECORD-TYPE", FieldType.STRING, 2);
        ecs_Record_Ap_Cont_Num = ecs_Record_Ap_Record.newFieldInGroup("ecs_Record_Ap_Cont_Num", "AP-CONT-NUM", FieldType.STRING, 60);
        ecs_Record_Ecs_Out_DataRedef6 = ecs_Record.newGroupInGroup("ecs_Record_Ecs_Out_DataRedef6", "Redefines", ecs_Record_Ecs_Out_Data);
        ecs_Record_Cd_Record = ecs_Record_Ecs_Out_DataRedef6.newGroupInGroup("ecs_Record_Cd_Record", "CD-RECORD");
        ecs_Record_Cd_Record_Type = ecs_Record_Cd_Record.newFieldInGroup("ecs_Record_Cd_Record_Type", "CD-RECORD-TYPE", FieldType.STRING, 2);
        ecs_Record_Cd_Contract_Number = ecs_Record_Cd_Record.newFieldInGroup("ecs_Record_Cd_Contract_Number", "CD-CONTRACT-NUMBER", FieldType.STRING, 
            21);
        ecs_Record_Ecs_Out_DataRedef7 = ecs_Record.newGroupInGroup("ecs_Record_Ecs_Out_DataRedef7", "Redefines", ecs_Record_Ecs_Out_Data);
        ecs_Record_Sp_Record = ecs_Record_Ecs_Out_DataRedef7.newGroupInGroup("ecs_Record_Sp_Record", "SP-RECORD");
        ecs_Record_Sp_Record_Type = ecs_Record_Sp_Record.newFieldInGroup("ecs_Record_Sp_Record_Type", "SP-RECORD-TYPE", FieldType.STRING, 2);
        ecs_Record_Sp_Stapling_Ind = ecs_Record_Sp_Record.newFieldInGroup("ecs_Record_Sp_Stapling_Ind", "SP-STAPLING-IND", FieldType.STRING, 1);
        ecs_Record_Ecs_Out_DataRedef8 = ecs_Record.newGroupInGroup("ecs_Record_Ecs_Out_DataRedef8", "Redefines", ecs_Record_Ecs_Out_Data);
        ecs_Record_Ep_Record = ecs_Record_Ecs_Out_DataRedef8.newGroupInGroup("ecs_Record_Ep_Record", "EP-RECORD");
        ecs_Record_Ep_Record_Type = ecs_Record_Ep_Record.newFieldInGroup("ecs_Record_Ep_Record_Type", "EP-RECORD-TYPE", FieldType.STRING, 2);
        ecs_Record_Ep_Pdf_Name = ecs_Record_Ep_Record.newFieldInGroup("ecs_Record_Ep_Pdf_Name", "EP-PDF-NAME", FieldType.STRING, 30);
        ecs_Record_Ecs_Out_DataRedef9 = ecs_Record.newGroupInGroup("ecs_Record_Ecs_Out_DataRedef9", "Redefines", ecs_Record_Ecs_Out_Data);
        ecs_Record_Es_Record = ecs_Record_Ecs_Out_DataRedef9.newGroupInGroup("ecs_Record_Es_Record", "ES-RECORD");
        ecs_Record_Es_Record_Type = ecs_Record_Es_Record.newFieldInGroup("ecs_Record_Es_Record_Type", "ES-RECORD-TYPE", FieldType.STRING, 2);
        ecs_Record_Es_Pdf_Name = ecs_Record_Es_Record.newFieldInGroup("ecs_Record_Es_Pdf_Name", "ES-PDF-NAME", FieldType.STRING, 30);
        ecs_Record_Ecs_Out_DataRedef10 = ecs_Record.newGroupInGroup("ecs_Record_Ecs_Out_DataRedef10", "Redefines", ecs_Record_Ecs_Out_Data);
        ecs_Record_En_Record = ecs_Record_Ecs_Out_DataRedef10.newGroupInGroup("ecs_Record_En_Record", "EN-RECORD");
        ecs_Record_En_Record_Type = ecs_Record_En_Record.newFieldInGroup("ecs_Record_En_Record_Type", "EN-RECORD-TYPE", FieldType.STRING, 2);
        ecs_Record_En_Endorse_Type = ecs_Record_En_Record.newFieldInGroup("ecs_Record_En_Endorse_Type", "EN-ENDORSE-TYPE", FieldType.STRING, 8);
        ecs_Record_En_Contract_Number = ecs_Record_En_Record.newFieldInGroup("ecs_Record_En_Contract_Number", "EN-CONTRACT-NUMBER", FieldType.STRING, 
            8);
        ecs_Record_En_Multi_Plan_Ind = ecs_Record_En_Record.newFieldInGroup("ecs_Record_En_Multi_Plan_Ind", "EN-MULTI-PLAN-IND", FieldType.STRING, 1);
        ecs_Record_En_Plan_Detail = ecs_Record_En_Record.newGroupArrayInGroup("ecs_Record_En_Plan_Detail", "EN-PLAN-DETAIL", new DbsArrayController(1,
            10));
        ecs_Record_En_Plan_Name = ecs_Record_En_Plan_Detail.newFieldInGroup("ecs_Record_En_Plan_Name", "EN-PLAN-NAME", FieldType.STRING, 80);
        ecs_Record_En_Plan_Share_Class = ecs_Record_En_Plan_Detail.newFieldInGroup("ecs_Record_En_Plan_Share_Class", "EN-PLAN-SHARE-CLASS", FieldType.STRING, 
            2);

        this.setRecordName("LdaRidl634");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaRidl634() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
