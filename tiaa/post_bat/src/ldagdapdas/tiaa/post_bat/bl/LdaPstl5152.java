/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:10:18 PM
**        * FROM NATURAL LDA     : PSTL5152
************************************************************
**        * FILE NAME            : LdaPstl5152.java
**        * CLASS NAME           : LdaPstl5152
**        * INSTANCE NAME        : LdaPstl5152
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaPstl5152 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_mail_Header;
    private DbsField mail_Header_Mail_Id;
    private DbsField mail_Header_Otgoing_Mail_Item_Id;
    private DbsField mail_Header_Entry_Dte_Tme;
    private DbsField mail_Header_Entry_Oprtr_Cde;
    private DbsField mail_Header_Updte_Dte_Tme;
    private DbsField mail_Header_Updte_Oprtr_Cde;
    private DbsField mail_Header_Pckge_Stts;
    private DataAccessProgramView vw_mail_Data;
    private DbsField mail_Data_Mail_Id;
    private DbsField mail_Data_Data_Status;
    private DataAccessProgramView vw_mail_Item;
    private DbsField mail_Item_Pst_Rqst_Id;
    private DbsField mail_Item_Hdr_Ind;
    private DbsField mail_Item_Updte_Dte_Tme;
    private DbsField mail_Item_Updte_Oprtr_Cde;
    private DbsField mail_Item_Pckge_Stts;
    private DbsField mail_Item_Pckge_Cde;
    private DbsField mail_Item_Pckge_Vrsn_Cde;
    private DataAccessProgramView vw_rqst_Header;
    private DbsField rqst_Header_Pst_Rqst_Id;
    private DbsField rqst_Header_Pst_Record_Typ;
    private DbsField rqst_Header_Updte_Dte_Tme;
    private DbsField rqst_Header_Updte_Oprtr_Cde;
    private DbsField rqst_Header_Hdr_Ind;
    private DbsField rqst_Header_Pckge_Stts;
    private DbsField rqst_Header_Pckge_Cde;
    private DbsField pnd_Error_Nr;
    private DbsField pnd_Error_Line;
    private DbsField pnd_Error_Status;
    private DbsField pnd_Error_Program;
    private DbsField pnd_Error_Level;
    private DbsField pnd_Error_Appl;

    public DataAccessProgramView getVw_mail_Header() { return vw_mail_Header; }

    public DbsField getMail_Header_Mail_Id() { return mail_Header_Mail_Id; }

    public DbsField getMail_Header_Otgoing_Mail_Item_Id() { return mail_Header_Otgoing_Mail_Item_Id; }

    public DbsField getMail_Header_Entry_Dte_Tme() { return mail_Header_Entry_Dte_Tme; }

    public DbsField getMail_Header_Entry_Oprtr_Cde() { return mail_Header_Entry_Oprtr_Cde; }

    public DbsField getMail_Header_Updte_Dte_Tme() { return mail_Header_Updte_Dte_Tme; }

    public DbsField getMail_Header_Updte_Oprtr_Cde() { return mail_Header_Updte_Oprtr_Cde; }

    public DbsField getMail_Header_Pckge_Stts() { return mail_Header_Pckge_Stts; }

    public DataAccessProgramView getVw_mail_Data() { return vw_mail_Data; }

    public DbsField getMail_Data_Mail_Id() { return mail_Data_Mail_Id; }

    public DbsField getMail_Data_Data_Status() { return mail_Data_Data_Status; }

    public DataAccessProgramView getVw_mail_Item() { return vw_mail_Item; }

    public DbsField getMail_Item_Pst_Rqst_Id() { return mail_Item_Pst_Rqst_Id; }

    public DbsField getMail_Item_Hdr_Ind() { return mail_Item_Hdr_Ind; }

    public DbsField getMail_Item_Updte_Dte_Tme() { return mail_Item_Updte_Dte_Tme; }

    public DbsField getMail_Item_Updte_Oprtr_Cde() { return mail_Item_Updte_Oprtr_Cde; }

    public DbsField getMail_Item_Pckge_Stts() { return mail_Item_Pckge_Stts; }

    public DbsField getMail_Item_Pckge_Cde() { return mail_Item_Pckge_Cde; }

    public DbsField getMail_Item_Pckge_Vrsn_Cde() { return mail_Item_Pckge_Vrsn_Cde; }

    public DataAccessProgramView getVw_rqst_Header() { return vw_rqst_Header; }

    public DbsField getRqst_Header_Pst_Rqst_Id() { return rqst_Header_Pst_Rqst_Id; }

    public DbsField getRqst_Header_Pst_Record_Typ() { return rqst_Header_Pst_Record_Typ; }

    public DbsField getRqst_Header_Updte_Dte_Tme() { return rqst_Header_Updte_Dte_Tme; }

    public DbsField getRqst_Header_Updte_Oprtr_Cde() { return rqst_Header_Updte_Oprtr_Cde; }

    public DbsField getRqst_Header_Hdr_Ind() { return rqst_Header_Hdr_Ind; }

    public DbsField getRqst_Header_Pckge_Stts() { return rqst_Header_Pckge_Stts; }

    public DbsField getRqst_Header_Pckge_Cde() { return rqst_Header_Pckge_Cde; }

    public DbsField getPnd_Error_Nr() { return pnd_Error_Nr; }

    public DbsField getPnd_Error_Line() { return pnd_Error_Line; }

    public DbsField getPnd_Error_Status() { return pnd_Error_Status; }

    public DbsField getPnd_Error_Program() { return pnd_Error_Program; }

    public DbsField getPnd_Error_Level() { return pnd_Error_Level; }

    public DbsField getPnd_Error_Appl() { return pnd_Error_Appl; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_mail_Header = new DataAccessProgramView(new NameInfo("vw_mail_Header", "MAIL-HEADER"), "PST_MAIL_HEADER", "PST_REQUEST_DATA");
        mail_Header_Mail_Id = vw_mail_Header.getRecord().newFieldInGroup("mail_Header_Mail_Id", "MAIL-ID", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "MAIL_ID");
        mail_Header_Mail_Id.setDdmHeader("OUTGOING/REQUEST/ID");
        mail_Header_Otgoing_Mail_Item_Id = vw_mail_Header.getRecord().newFieldInGroup("mail_Header_Otgoing_Mail_Item_Id", "OTGOING-MAIL-ITEM-ID", FieldType.STRING, 
            11, RepeatingFieldStrategy.None, "OTGOING_MAIL_ITEM_ID");
        mail_Header_Otgoing_Mail_Item_Id.setDdmHeader("OUTGOING/POST/REQUEST ID");
        mail_Header_Entry_Dte_Tme = vw_mail_Header.getRecord().newFieldInGroup("mail_Header_Entry_Dte_Tme", "ENTRY-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ENTRY_DTE_TME");
        mail_Header_Entry_Dte_Tme.setDdmHeader("CREATED/DATE/TIME");
        mail_Header_Entry_Oprtr_Cde = vw_mail_Header.getRecord().newFieldInGroup("mail_Header_Entry_Oprtr_Cde", "ENTRY-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "ENTRY_OPRTR_CDE");
        mail_Header_Updte_Dte_Tme = vw_mail_Header.getRecord().newFieldInGroup("mail_Header_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "UPDTE_DTE_TME");
        mail_Header_Updte_Dte_Tme.setDdmHeader("UPDATE/DATE //TIME");
        mail_Header_Updte_Oprtr_Cde = vw_mail_Header.getRecord().newFieldInGroup("mail_Header_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        mail_Header_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        mail_Header_Pckge_Stts = vw_mail_Header.getRecord().newFieldInGroup("mail_Header_Pckge_Stts", "PCKGE-STTS", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PCKGE_STTS");
        mail_Header_Pckge_Stts.setDdmHeader("PCKGE/STAT");

        vw_mail_Data = new DataAccessProgramView(new NameInfo("vw_mail_Data", "MAIL-DATA"), "PST_MAIL_DATA", "PST_REQUEST_DATA");
        mail_Data_Mail_Id = vw_mail_Data.getRecord().newFieldInGroup("mail_Data_Mail_Id", "MAIL-ID", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "MAIL_ID");
        mail_Data_Mail_Id.setDdmHeader("OUTGOING/REQUEST/ID");
        mail_Data_Data_Status = vw_mail_Data.getRecord().newFieldInGroup("mail_Data_Data_Status", "DATA-STATUS", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "DATA_STATUS");

        vw_mail_Item = new DataAccessProgramView(new NameInfo("vw_mail_Item", "MAIL-ITEM"), "PST_OUTGOING_MAIL_ITEM", "PST_REQUEST_DATA");
        mail_Item_Pst_Rqst_Id = vw_mail_Item.getRecord().newFieldInGroup("mail_Item_Pst_Rqst_Id", "PST-RQST-ID", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "PST_RQST_ID");
        mail_Item_Pst_Rqst_Id.setDdmHeader("EXTERNAL/REQUEST/ID");
        mail_Item_Hdr_Ind = vw_mail_Item.getRecord().newFieldInGroup("mail_Item_Hdr_Ind", "HDR-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "HDR_IND");
        mail_Item_Hdr_Ind.setDdmHeader("HDR/IND");
        mail_Item_Updte_Dte_Tme = vw_mail_Item.getRecord().newFieldInGroup("mail_Item_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "UPDTE_DTE_TME");
        mail_Item_Updte_Dte_Tme.setDdmHeader("UPDATE/DATE //TIME");
        mail_Item_Updte_Oprtr_Cde = vw_mail_Item.getRecord().newFieldInGroup("mail_Item_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UPDTE_OPRTR_CDE");
        mail_Item_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        mail_Item_Pckge_Stts = vw_mail_Item.getRecord().newFieldInGroup("mail_Item_Pckge_Stts", "PCKGE-STTS", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PCKGE_STTS");
        mail_Item_Pckge_Stts.setDdmHeader("PCKGE/STAT");
        mail_Item_Pckge_Cde = vw_mail_Item.getRecord().newFieldInGroup("mail_Item_Pckge_Cde", "PCKGE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "PCKGE_CDE");
        mail_Item_Pckge_Cde.setDdmHeader("PACKAGE/ID");
        mail_Item_Pckge_Vrsn_Cde = vw_mail_Item.getRecord().newFieldInGroup("mail_Item_Pckge_Vrsn_Cde", "PCKGE-VRSN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PCKGE_VRSN_CDE");
        mail_Item_Pckge_Vrsn_Cde.setDdmHeader("PCKGE/VRSN/CODE");

        vw_rqst_Header = new DataAccessProgramView(new NameInfo("vw_rqst_Header", "RQST-HEADER"), "PST_REQUEST_HEADER", "PST_REQUEST_DATA");
        rqst_Header_Pst_Rqst_Id = vw_rqst_Header.getRecord().newFieldInGroup("rqst_Header_Pst_Rqst_Id", "PST-RQST-ID", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "PST_RQST_ID");
        rqst_Header_Pst_Rqst_Id.setDdmHeader("EXTERNAL/REQUEST/ID");
        rqst_Header_Pst_Record_Typ = vw_rqst_Header.getRecord().newFieldInGroup("rqst_Header_Pst_Record_Typ", "PST-RECORD-TYP", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "PST_RECORD_TYP");
        rqst_Header_Updte_Dte_Tme = vw_rqst_Header.getRecord().newFieldInGroup("rqst_Header_Updte_Dte_Tme", "UPDTE-DTE-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "UPDTE_DTE_TME");
        rqst_Header_Updte_Dte_Tme.setDdmHeader("UPDATE/DATE //TIME");
        rqst_Header_Updte_Oprtr_Cde = vw_rqst_Header.getRecord().newFieldInGroup("rqst_Header_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        rqst_Header_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        rqst_Header_Hdr_Ind = vw_rqst_Header.getRecord().newFieldInGroup("rqst_Header_Hdr_Ind", "HDR-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "HDR_IND");
        rqst_Header_Hdr_Ind.setDdmHeader("HDR/IND");
        rqst_Header_Pckge_Stts = vw_rqst_Header.getRecord().newFieldInGroup("rqst_Header_Pckge_Stts", "PCKGE-STTS", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PCKGE_STTS");
        rqst_Header_Pckge_Stts.setDdmHeader("PCKGE/STAT");
        rqst_Header_Pckge_Cde = vw_rqst_Header.getRecord().newFieldInGroup("rqst_Header_Pckge_Cde", "PCKGE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "PCKGE_CDE");
        rqst_Header_Pckge_Cde.setDdmHeader("PACKAGE/ID");

        pnd_Error_Nr = newFieldInRecord("pnd_Error_Nr", "#ERROR-NR", FieldType.NUMERIC, 4);

        pnd_Error_Line = newFieldInRecord("pnd_Error_Line", "#ERROR-LINE", FieldType.NUMERIC, 4);

        pnd_Error_Status = newFieldInRecord("pnd_Error_Status", "#ERROR-STATUS", FieldType.STRING, 1);

        pnd_Error_Program = newFieldInRecord("pnd_Error_Program", "#ERROR-PROGRAM", FieldType.STRING, 8);

        pnd_Error_Level = newFieldInRecord("pnd_Error_Level", "#ERROR-LEVEL", FieldType.NUMERIC, 2);

        pnd_Error_Appl = newFieldInRecord("pnd_Error_Appl", "#ERROR-APPL", FieldType.STRING, 8);

        this.setRecordName("LdaPstl5152");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_mail_Header.reset();
        vw_mail_Data.reset();
        vw_mail_Item.reset();
        vw_rqst_Header.reset();
        pnd_Error_Status.setInitialValue("O");
        pnd_Error_Program.setInitialValue(Global.getPROGRAM());
        pnd_Error_Appl.setInitialValue("POST");
    }

    // Constructor
    public LdaPstl5152() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
