/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:10:13 PM
**        * FROM NATURAL LDA     : PSTL300X
************************************************************
**        * FILE NAME            : LdaPstl300x.java
**        * CLASS NAME           : LdaPstl300x
**        * INSTANCE NAME        : LdaPstl300x
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaPstl300x extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Work_Record_10;
    private DbsField pnd_Work_Record_10_Pnd_Work_Record_10_Data;
    private DbsGroup pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1;
    private DbsField pnd_Work_Record_10_Pnd_W10_Ppcn_Nbr;
    private DbsField pnd_Work_Record_10_Pnd_W10_Payee;
    private DbsField pnd_Work_Record_10_Pnd_W10_Record_Code;
    private DbsField pnd_Work_Record_10_Pnd_W10_Optn_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_Orgn_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_Acctng_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_Issue_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_First_Pymnt_Due_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_First_Pymnt_Pd_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_Crrncy_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_Type_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_Pymnt_Mthd;
    private DbsField pnd_Work_Record_10_Pnd_W10_Pnsn_Pln_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_Joint_Cnvrt_Rcrcd_Ind;
    private DbsField pnd_Work_Record_10_Pnd_W10_Orig_Da_Cntrct_Nbr;
    private DbsField pnd_Work_Record_10_Pnd_W10_Rsdncy_At_Issue_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_First_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_10_Pnd_W10_First_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_First_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_First_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_First_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_10_Pnd_W10_First_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Xref_Ind;
    private DbsField pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Dob_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Dod_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Life_Cnt;
    private DbsField pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Ssn;
    private DbsField pnd_Work_Record_10_Pnd_W10_Div_Payee_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_Div_Coll_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_Inst_Iss_Cde;
    private DbsField pnd_Work_Record_10_Pnd_W10_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_Cntrct_Type;
    private DbsField pnd_Work_Record_10_Pnd_W10_Cntrct_Rsdncy_At_Iss_Re;
    private DbsField pnd_Work_Record_10_Pnd_W10_Cntrct_Fnl_Prm_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_Cntrct_Mtch_Ppcn;
    private DbsField pnd_Work_Record_10_Pnd_W10_Cntrct_Annty_Strt_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_Cntrct_Issue_Dte_Dd;
    private DbsField pnd_Work_Record_10_Pnd_W10_Cntrct_Fp_Due_Dte_Dd;
    private DbsField pnd_Work_Record_10_Pnd_W10_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField pnd_Work_Record_10_Pnd_W10_Roth_Frst_Cntrb_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_Roth_Ssnng_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_Cntrct_Ssnng_Dte;
    private DbsField pnd_Work_Record_10_Pnd_W10_Plan_Nmbr;
    private DbsField pnd_Work_Record_10_Pnd_W10_Tax_Exmpt_Ind;
    private DbsField pnd_Work_Record_10_Pnd_W10_Orig_Ownr_Dob;
    private DbsField pnd_Work_Record_10_Pnd_W10_Orig_Ownr_Dod;
    private DbsField pnd_Work_Record_10_Pnd_W10_Sub_Plan_Nmbr;
    private DbsField pnd_Work_Record_10_Pnd_W10_Orgntng_Sub_Plan_Nmbr;
    private DbsField pnd_Work_Record_10_Pnd_W10_Orgntng_Cntrct_Nmbr;
    private DbsField pnd_Work_Record_10_Pnd_W10_Filler;
    private DbsGroup pnd_Work_Record_20;
    private DbsField pnd_Work_Record_20_Pnd_Work_Record_20_Data;
    private DbsGroup pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2;
    private DbsField pnd_Work_Record_20_Pnd_W20_Part_Ppcn_Nbr;
    private DbsField pnd_Work_Record_20_Pnd_W20_Part_Payee_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Record_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Cpr_Id_Nbr;
    private DbsField pnd_Work_Record_20_Pnd_W20_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_20_Pnd_W20_Prtcpnt_Ctznshp_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Prtcpnt_Rsdncy_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Prtcpnt_Rsdncy_Sw;
    private DbsField pnd_Work_Record_20_Pnd_W20_Prtcpnt_Tax_Id_Nbr;
    private DbsField pnd_Work_Record_20_Pnd_W20_Prtcpnt_Tax_Id_Typ;
    private DbsField pnd_Work_Record_20_Pnd_W20_Actvty_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Trmnte_Rsn;
    private DbsField pnd_Work_Record_20_Pnd_W20_Rwrttn_Ind;
    private DbsField pnd_Work_Record_20_Pnd_W20_Cash_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Emplymnt_Trmnt_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Company_Cd;
    private DbsField pnd_Work_Record_20_Pnd_W20_Rcvry_Type_Ind;
    private DbsField pnd_Work_Record_20_Pnd_W20_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_20_Pnd_W20_Resdl_Ivc_Amt;
    private DbsField pnd_Work_Record_20_Pnd_W20_Ivc_Amt;
    private DbsField pnd_Work_Record_20_Pnd_W20_Ivc_Used_Amt;
    private DbsField pnd_Work_Record_20_Pnd_W20_Rtb_Amt;
    private DbsField pnd_Work_Record_20_Pnd_W20_Rtb_Percent;
    private DbsField pnd_Work_Record_20_Pnd_W20_Mode_Ind;
    private DbsField pnd_Work_Record_20_Pnd_W20_Wthdrwl_Dte;
    private DbsField pnd_Work_Record_20_Pnd_W20_Final_Per_Pay_Dte;
    private DbsField pnd_Work_Record_20_Pnd_W20_Final_Pay_Dte;
    private DbsField pnd_Work_Record_20_Pnd_W20_Bnfcry_Xref_Ind;
    private DbsField pnd_Work_Record_20_Pnd_W20_Bnfcry_Dod_Dte;
    private DbsField pnd_Work_Record_20_Pnd_W20_Pend_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Hold_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Pend_Dte;
    private DbsField pnd_Work_Record_20_Pnd_W20_Prev_Dist_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Curr_Dist_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Cmbne_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Spirt_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Spirt_Amt;
    private DbsField pnd_Work_Record_20_Pnd_W20_Spirt_Srce;
    private DbsField pnd_Work_Record_20_Pnd_W20_Spirt_Arr_Dte;
    private DbsField pnd_Work_Record_20_Pnd_W20_Spirt_Prcss_Dte;
    private DbsField pnd_Work_Record_20_Pnd_W20_Fed_Tax_Amt;
    private DbsField pnd_Work_Record_20_Pnd_W20_State_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_State_Tax_Amt;
    private DbsField pnd_Work_Record_20_Pnd_W20_Local_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Local_Tax_Amt;
    private DbsField pnd_Work_Record_20_Pnd_W20_Lst_Chnge_Dte;
    private DbsField pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Term_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Cpr_Lgl_Res_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Iss_Dte;
    private DbsField pnd_Work_Record_20_Pnd_W20_Rllvr_Cntrct_Nbr;
    private DbsField pnd_Work_Record_20_Pnd_W20_Rllvr_Ivc_Ind;
    private DbsField pnd_Work_Record_20_Pnd_W20_Rllvr_Elgble_Ind;
    private DbsField pnd_Work_Record_20_Pnd_W20_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Rllvr_Accptng_Irc_Cde;
    private DbsField pnd_Work_Record_20_Pnd_W20_Rllvr_Pln_Admn_Ind;
    private DbsField pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Iss_Dte_N8;
    private DbsField pnd_Work_Record_20_Pnd_W20_Roth_Dsblty_Dte;
    private DbsGroup pnd_Work_Record_30;
    private DbsField pnd_Work_Record_30_Pnd_Work_Record_30_Data;
    private DbsGroup pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3;
    private DbsField pnd_Work_Record_30_Pnd_W30_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Work_Record_30_Pnd_W30_Cntrct_Payee_Cde;
    private DbsField pnd_Work_Record_30_Pnd_W30_Record_Code;
    private DbsField pnd_Work_Record_30_Pnd_W30_Cmpny_Cde;
    private DbsField pnd_Work_Record_30_Pnd_W30_Fund_Cde;
    private DbsField pnd_Work_Record_30_Pnd_W30_Per_Ivc_Amt;
    private DbsField pnd_Work_Record_30_Pnd_W30_Rtb_Amt;
    private DbsField pnd_Work_Record_30_Pnd_W30_Tot_Per_Amt;
    private DbsField pnd_Work_Record_30_Pnd_W30_Tot_Div_Amt;
    private DbsField pnd_Work_Record_30_Pnd_W30_Old_Per_Amt;
    private DbsField pnd_Work_Record_30_Pnd_W30_Old_Div_Amt;
    private DbsField pnd_Work_Record_30_Pnd_W30_Rate_Cde;
    private DbsField pnd_Work_Record_30_Pnd_W30_Rate_Dte;
    private DbsField pnd_Work_Record_30_Pnd_W30_Per_Pay_Amt;
    private DbsField pnd_Work_Record_30_Pnd_W30_Per_Div_Amt;
    private DbsField pnd_Work_Record_30_Pnd_W30_Units_Cnt;
    private DbsField pnd_Work_Record_30_Pnd_W30_Rate_Final_Pay_Amt;
    private DbsField pnd_Work_Record_30_Pnd_W30_Rate_Final_Div_Amt;
    private DbsField pnd_Work_Record_30_Pnd_W30_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_30_Pnd_W30_Filler;
    private DbsField pnd_Work_Record_30_Pnd_W30_Filler2;
    private DbsField pnd_Work_Record_30_Pnd_W30_Filler3;
    private DbsGroup pnd_Work_Record_40;
    private DbsField pnd_Work_Record_40_Pnd_Work_Record_40_Data;
    private DbsGroup pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Ppcn_Nbr;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Payee_Cde;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Record_Cde;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Id_Nbr;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Cde;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Seq_Nbr;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Payee;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Per_Amt;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Ytd_Amt;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Pd_To_Dte;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Tot_Amt;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Intent_Cde;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Strt_Dte;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Stp_Dte;
    private DbsField pnd_Work_Record_40_Pnd_W40_Ddctn_Final_Dte;
    private DbsField pnd_Work_Record_40_Pnd_W40_Lst_Trans_Dte;
    private DbsField pnd_Work_Record_40_Pnd_W40_Filler;
    private DbsField pnd_Work_Record_40_Pnd_W40_Filler2;
    private DbsField pnd_Work_Record_40_Pnd_W40_Filler3;
    private DbsField pnd_Work_Record_40_Pnd_W40_Filler4;

    public DbsGroup getPnd_Work_Record_10() { return pnd_Work_Record_10; }

    public DbsField getPnd_Work_Record_10_Pnd_Work_Record_10_Data() { return pnd_Work_Record_10_Pnd_Work_Record_10_Data; }

    public DbsGroup getPnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1() { return pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Ppcn_Nbr() { return pnd_Work_Record_10_Pnd_W10_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Payee() { return pnd_Work_Record_10_Pnd_W10_Payee; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Record_Code() { return pnd_Work_Record_10_Pnd_W10_Record_Code; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Optn_Cde() { return pnd_Work_Record_10_Pnd_W10_Optn_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Orgn_Cde() { return pnd_Work_Record_10_Pnd_W10_Orgn_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Acctng_Cde() { return pnd_Work_Record_10_Pnd_W10_Acctng_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Issue_Dte() { return pnd_Work_Record_10_Pnd_W10_Issue_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_First_Pymnt_Due_Dte() { return pnd_Work_Record_10_Pnd_W10_First_Pymnt_Due_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_First_Pymnt_Pd_Dte() { return pnd_Work_Record_10_Pnd_W10_First_Pymnt_Pd_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Crrncy_Cde() { return pnd_Work_Record_10_Pnd_W10_Crrncy_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Type_Cde() { return pnd_Work_Record_10_Pnd_W10_Type_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Pymnt_Mthd() { return pnd_Work_Record_10_Pnd_W10_Pymnt_Mthd; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Pnsn_Pln_Cde() { return pnd_Work_Record_10_Pnd_W10_Pnsn_Pln_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Joint_Cnvrt_Rcrcd_Ind() { return pnd_Work_Record_10_Pnd_W10_Joint_Cnvrt_Rcrcd_Ind; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Orig_Da_Cntrct_Nbr() { return pnd_Work_Record_10_Pnd_W10_Orig_Da_Cntrct_Nbr; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Rsdncy_At_Issue_Cde() { return pnd_Work_Record_10_Pnd_W10_Rsdncy_At_Issue_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_First_Annt_Xref_Ind() { return pnd_Work_Record_10_Pnd_W10_First_Annt_Xref_Ind; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_First_Annt_Dob_Dte() { return pnd_Work_Record_10_Pnd_W10_First_Annt_Dob_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_First_Annt_Mrtlty_Yob_Dte() { return pnd_Work_Record_10_Pnd_W10_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_First_Annt_Sex_Cde() { return pnd_Work_Record_10_Pnd_W10_First_Annt_Sex_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_First_Annt_Life_Cnt() { return pnd_Work_Record_10_Pnd_W10_First_Annt_Life_Cnt; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_First_Annt_Dod_Dte() { return pnd_Work_Record_10_Pnd_W10_First_Annt_Dod_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Scnd_Annt_Xref_Ind() { return pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Xref_Ind; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Scnd_Annt_Dob_Dte() { return pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Dob_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Scnd_Annt_Mrtlty_Yob_Dte() { return pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Scnd_Annt_Sex_Cde() { return pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Sex_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Scnd_Annt_Dod_Dte() { return pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Dod_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Scnd_Annt_Life_Cnt() { return pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Life_Cnt; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Scnd_Annt_Ssn() { return pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Ssn; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Div_Payee_Cde() { return pnd_Work_Record_10_Pnd_W10_Div_Payee_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Div_Coll_Cde() { return pnd_Work_Record_10_Pnd_W10_Div_Coll_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Inst_Iss_Cde() { return pnd_Work_Record_10_Pnd_W10_Inst_Iss_Cde; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Lst_Trans_Dte() { return pnd_Work_Record_10_Pnd_W10_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Cntrct_Type() { return pnd_Work_Record_10_Pnd_W10_Cntrct_Type; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Cntrct_Rsdncy_At_Iss_Re() { return pnd_Work_Record_10_Pnd_W10_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Cntrct_Fnl_Prm_Dte() { return pnd_Work_Record_10_Pnd_W10_Cntrct_Fnl_Prm_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Cntrct_Mtch_Ppcn() { return pnd_Work_Record_10_Pnd_W10_Cntrct_Mtch_Ppcn; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Cntrct_Annty_Strt_Dte() { return pnd_Work_Record_10_Pnd_W10_Cntrct_Annty_Strt_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Cntrct_Issue_Dte_Dd() { return pnd_Work_Record_10_Pnd_W10_Cntrct_Issue_Dte_Dd; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Cntrct_Fp_Due_Dte_Dd() { return pnd_Work_Record_10_Pnd_W10_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Cntrct_Fp_Pd_Dte_Dd() { return pnd_Work_Record_10_Pnd_W10_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Roth_Frst_Cntrb_Dte() { return pnd_Work_Record_10_Pnd_W10_Roth_Frst_Cntrb_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Roth_Ssnng_Dte() { return pnd_Work_Record_10_Pnd_W10_Roth_Ssnng_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Cntrct_Ssnng_Dte() { return pnd_Work_Record_10_Pnd_W10_Cntrct_Ssnng_Dte; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Plan_Nmbr() { return pnd_Work_Record_10_Pnd_W10_Plan_Nmbr; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Tax_Exmpt_Ind() { return pnd_Work_Record_10_Pnd_W10_Tax_Exmpt_Ind; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Orig_Ownr_Dob() { return pnd_Work_Record_10_Pnd_W10_Orig_Ownr_Dob; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Orig_Ownr_Dod() { return pnd_Work_Record_10_Pnd_W10_Orig_Ownr_Dod; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Sub_Plan_Nmbr() { return pnd_Work_Record_10_Pnd_W10_Sub_Plan_Nmbr; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Orgntng_Sub_Plan_Nmbr() { return pnd_Work_Record_10_Pnd_W10_Orgntng_Sub_Plan_Nmbr; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Orgntng_Cntrct_Nmbr() { return pnd_Work_Record_10_Pnd_W10_Orgntng_Cntrct_Nmbr; }

    public DbsField getPnd_Work_Record_10_Pnd_W10_Filler() { return pnd_Work_Record_10_Pnd_W10_Filler; }

    public DbsGroup getPnd_Work_Record_20() { return pnd_Work_Record_20; }

    public DbsField getPnd_Work_Record_20_Pnd_Work_Record_20_Data() { return pnd_Work_Record_20_Pnd_Work_Record_20_Data; }

    public DbsGroup getPnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2() { return pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Part_Ppcn_Nbr() { return pnd_Work_Record_20_Pnd_W20_Part_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Part_Payee_Cde() { return pnd_Work_Record_20_Pnd_W20_Part_Payee_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Record_Cde() { return pnd_Work_Record_20_Pnd_W20_Record_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Cpr_Id_Nbr() { return pnd_Work_Record_20_Pnd_W20_Cpr_Id_Nbr; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Lst_Trans_Dte() { return pnd_Work_Record_20_Pnd_W20_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Prtcpnt_Ctznshp_Cde() { return pnd_Work_Record_20_Pnd_W20_Prtcpnt_Ctznshp_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Prtcpnt_Rsdncy_Cde() { return pnd_Work_Record_20_Pnd_W20_Prtcpnt_Rsdncy_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Prtcpnt_Rsdncy_Sw() { return pnd_Work_Record_20_Pnd_W20_Prtcpnt_Rsdncy_Sw; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Prtcpnt_Tax_Id_Nbr() { return pnd_Work_Record_20_Pnd_W20_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Prtcpnt_Tax_Id_Typ() { return pnd_Work_Record_20_Pnd_W20_Prtcpnt_Tax_Id_Typ; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Actvty_Cde() { return pnd_Work_Record_20_Pnd_W20_Actvty_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Trmnte_Rsn() { return pnd_Work_Record_20_Pnd_W20_Trmnte_Rsn; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Rwrttn_Ind() { return pnd_Work_Record_20_Pnd_W20_Rwrttn_Ind; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Cash_Cde() { return pnd_Work_Record_20_Pnd_W20_Cash_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Emplymnt_Trmnt_Cde() { return pnd_Work_Record_20_Pnd_W20_Emplymnt_Trmnt_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Company_Cd() { return pnd_Work_Record_20_Pnd_W20_Company_Cd; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Rcvry_Type_Ind() { return pnd_Work_Record_20_Pnd_W20_Rcvry_Type_Ind; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Per_Ivc_Amt() { return pnd_Work_Record_20_Pnd_W20_Per_Ivc_Amt; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Resdl_Ivc_Amt() { return pnd_Work_Record_20_Pnd_W20_Resdl_Ivc_Amt; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Ivc_Amt() { return pnd_Work_Record_20_Pnd_W20_Ivc_Amt; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Ivc_Used_Amt() { return pnd_Work_Record_20_Pnd_W20_Ivc_Used_Amt; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Rtb_Amt() { return pnd_Work_Record_20_Pnd_W20_Rtb_Amt; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Rtb_Percent() { return pnd_Work_Record_20_Pnd_W20_Rtb_Percent; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Mode_Ind() { return pnd_Work_Record_20_Pnd_W20_Mode_Ind; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Wthdrwl_Dte() { return pnd_Work_Record_20_Pnd_W20_Wthdrwl_Dte; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Final_Per_Pay_Dte() { return pnd_Work_Record_20_Pnd_W20_Final_Per_Pay_Dte; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Final_Pay_Dte() { return pnd_Work_Record_20_Pnd_W20_Final_Pay_Dte; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Bnfcry_Xref_Ind() { return pnd_Work_Record_20_Pnd_W20_Bnfcry_Xref_Ind; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Bnfcry_Dod_Dte() { return pnd_Work_Record_20_Pnd_W20_Bnfcry_Dod_Dte; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Pend_Cde() { return pnd_Work_Record_20_Pnd_W20_Pend_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Hold_Cde() { return pnd_Work_Record_20_Pnd_W20_Hold_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Pend_Dte() { return pnd_Work_Record_20_Pnd_W20_Pend_Dte; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Prev_Dist_Cde() { return pnd_Work_Record_20_Pnd_W20_Prev_Dist_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Curr_Dist_Cde() { return pnd_Work_Record_20_Pnd_W20_Curr_Dist_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Cmbne_Cde() { return pnd_Work_Record_20_Pnd_W20_Cmbne_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Spirt_Cde() { return pnd_Work_Record_20_Pnd_W20_Spirt_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Spirt_Amt() { return pnd_Work_Record_20_Pnd_W20_Spirt_Amt; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Spirt_Srce() { return pnd_Work_Record_20_Pnd_W20_Spirt_Srce; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Spirt_Arr_Dte() { return pnd_Work_Record_20_Pnd_W20_Spirt_Arr_Dte; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Spirt_Prcss_Dte() { return pnd_Work_Record_20_Pnd_W20_Spirt_Prcss_Dte; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Fed_Tax_Amt() { return pnd_Work_Record_20_Pnd_W20_Fed_Tax_Amt; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_State_Cde() { return pnd_Work_Record_20_Pnd_W20_State_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_State_Tax_Amt() { return pnd_Work_Record_20_Pnd_W20_State_Tax_Amt; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Local_Cde() { return pnd_Work_Record_20_Pnd_W20_Local_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Local_Tax_Amt() { return pnd_Work_Record_20_Pnd_W20_Local_Tax_Amt; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Lst_Chnge_Dte() { return pnd_Work_Record_20_Pnd_W20_Lst_Chnge_Dte; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Term_Cde() { return pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Term_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Cpr_Lgl_Res_Cde() { return pnd_Work_Record_20_Pnd_W20_Cpr_Lgl_Res_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Iss_Dte() { return pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Iss_Dte; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Rllvr_Cntrct_Nbr() { return pnd_Work_Record_20_Pnd_W20_Rllvr_Cntrct_Nbr; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Rllvr_Ivc_Ind() { return pnd_Work_Record_20_Pnd_W20_Rllvr_Ivc_Ind; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Rllvr_Elgble_Ind() { return pnd_Work_Record_20_Pnd_W20_Rllvr_Elgble_Ind; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Rllvr_Dstrbtng_Irc_Cde() { return pnd_Work_Record_20_Pnd_W20_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Rllvr_Accptng_Irc_Cde() { return pnd_Work_Record_20_Pnd_W20_Rllvr_Accptng_Irc_Cde; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Rllvr_Pln_Admn_Ind() { return pnd_Work_Record_20_Pnd_W20_Rllvr_Pln_Admn_Ind; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Iss_Dte_N8() { return pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Iss_Dte_N8; }

    public DbsField getPnd_Work_Record_20_Pnd_W20_Roth_Dsblty_Dte() { return pnd_Work_Record_20_Pnd_W20_Roth_Dsblty_Dte; }

    public DbsGroup getPnd_Work_Record_30() { return pnd_Work_Record_30; }

    public DbsField getPnd_Work_Record_30_Pnd_Work_Record_30_Data() { return pnd_Work_Record_30_Pnd_Work_Record_30_Data; }

    public DbsGroup getPnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3() { return pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Cntrct_Ppcn_Nbr() { return pnd_Work_Record_30_Pnd_W30_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Cntrct_Payee_Cde() { return pnd_Work_Record_30_Pnd_W30_Cntrct_Payee_Cde; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Record_Code() { return pnd_Work_Record_30_Pnd_W30_Record_Code; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Cmpny_Cde() { return pnd_Work_Record_30_Pnd_W30_Cmpny_Cde; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Fund_Cde() { return pnd_Work_Record_30_Pnd_W30_Fund_Cde; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Per_Ivc_Amt() { return pnd_Work_Record_30_Pnd_W30_Per_Ivc_Amt; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Rtb_Amt() { return pnd_Work_Record_30_Pnd_W30_Rtb_Amt; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Tot_Per_Amt() { return pnd_Work_Record_30_Pnd_W30_Tot_Per_Amt; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Tot_Div_Amt() { return pnd_Work_Record_30_Pnd_W30_Tot_Div_Amt; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Old_Per_Amt() { return pnd_Work_Record_30_Pnd_W30_Old_Per_Amt; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Old_Div_Amt() { return pnd_Work_Record_30_Pnd_W30_Old_Div_Amt; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Rate_Cde() { return pnd_Work_Record_30_Pnd_W30_Rate_Cde; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Rate_Dte() { return pnd_Work_Record_30_Pnd_W30_Rate_Dte; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Per_Pay_Amt() { return pnd_Work_Record_30_Pnd_W30_Per_Pay_Amt; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Per_Div_Amt() { return pnd_Work_Record_30_Pnd_W30_Per_Div_Amt; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Units_Cnt() { return pnd_Work_Record_30_Pnd_W30_Units_Cnt; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Rate_Final_Pay_Amt() { return pnd_Work_Record_30_Pnd_W30_Rate_Final_Pay_Amt; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Rate_Final_Div_Amt() { return pnd_Work_Record_30_Pnd_W30_Rate_Final_Div_Amt; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Lst_Trans_Dte() { return pnd_Work_Record_30_Pnd_W30_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Filler() { return pnd_Work_Record_30_Pnd_W30_Filler; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Filler2() { return pnd_Work_Record_30_Pnd_W30_Filler2; }

    public DbsField getPnd_Work_Record_30_Pnd_W30_Filler3() { return pnd_Work_Record_30_Pnd_W30_Filler3; }

    public DbsGroup getPnd_Work_Record_40() { return pnd_Work_Record_40; }

    public DbsField getPnd_Work_Record_40_Pnd_Work_Record_40_Data() { return pnd_Work_Record_40_Pnd_Work_Record_40_Data; }

    public DbsGroup getPnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4() { return pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Ppcn_Nbr() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Ppcn_Nbr; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Payee_Cde() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Payee_Cde; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Record_Cde() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Record_Cde; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Id_Nbr() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Id_Nbr; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Cde() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Cde; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Seq_Nbr() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Seq_Nbr; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Payee() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Payee; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Per_Amt() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Per_Amt; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Ytd_Amt() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Ytd_Amt; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Pd_To_Dte() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Pd_To_Dte; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Tot_Amt() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Tot_Amt; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Intent_Cde() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Intent_Cde; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Strt_Dte() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Strt_Dte; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Stp_Dte() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Stp_Dte; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Ddctn_Final_Dte() { return pnd_Work_Record_40_Pnd_W40_Ddctn_Final_Dte; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Lst_Trans_Dte() { return pnd_Work_Record_40_Pnd_W40_Lst_Trans_Dte; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Filler() { return pnd_Work_Record_40_Pnd_W40_Filler; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Filler2() { return pnd_Work_Record_40_Pnd_W40_Filler2; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Filler3() { return pnd_Work_Record_40_Pnd_W40_Filler3; }

    public DbsField getPnd_Work_Record_40_Pnd_W40_Filler4() { return pnd_Work_Record_40_Pnd_W40_Filler4; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Work_Record_10 = newGroupInRecord("pnd_Work_Record_10", "#WORK-RECORD-10");
        pnd_Work_Record_10_Pnd_Work_Record_10_Data = pnd_Work_Record_10.newFieldInGroup("pnd_Work_Record_10_Pnd_Work_Record_10_Data", "#WORK-RECORD-10-DATA", 
            FieldType.STRING, 367);
        pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1 = pnd_Work_Record_10.newGroupInGroup("pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1", "Redefines", 
            pnd_Work_Record_10_Pnd_Work_Record_10_Data);
        pnd_Work_Record_10_Pnd_W10_Ppcn_Nbr = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Ppcn_Nbr", 
            "#W10-PPCN-NBR", FieldType.STRING, 10);
        pnd_Work_Record_10_Pnd_W10_Payee = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Payee", "#W10-PAYEE", 
            FieldType.NUMERIC, 2);
        pnd_Work_Record_10_Pnd_W10_Record_Code = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Record_Code", 
            "#W10-RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Work_Record_10_Pnd_W10_Optn_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Optn_Cde", 
            "#W10-OPTN-CDE", FieldType.NUMERIC, 2);
        pnd_Work_Record_10_Pnd_W10_Orgn_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Orgn_Cde", 
            "#W10-ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_Work_Record_10_Pnd_W10_Acctng_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Acctng_Cde", 
            "#W10-ACCTNG-CDE", FieldType.STRING, 2);
        pnd_Work_Record_10_Pnd_W10_Issue_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Issue_Dte", 
            "#W10-ISSUE-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_10_Pnd_W10_First_Pymnt_Due_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_First_Pymnt_Due_Dte", 
            "#W10-FIRST-PYMNT-DUE-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_10_Pnd_W10_First_Pymnt_Pd_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_First_Pymnt_Pd_Dte", 
            "#W10-FIRST-PYMNT-PD-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_10_Pnd_W10_Crrncy_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Crrncy_Cde", 
            "#W10-CRRNCY-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_10_Pnd_W10_Type_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Type_Cde", 
            "#W10-TYPE-CDE", FieldType.STRING, 1);
        pnd_Work_Record_10_Pnd_W10_Pymnt_Mthd = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Pymnt_Mthd", 
            "#W10-PYMNT-MTHD", FieldType.STRING, 1);
        pnd_Work_Record_10_Pnd_W10_Pnsn_Pln_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Pnsn_Pln_Cde", 
            "#W10-PNSN-PLN-CDE", FieldType.STRING, 1);
        pnd_Work_Record_10_Pnd_W10_Joint_Cnvrt_Rcrcd_Ind = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Joint_Cnvrt_Rcrcd_Ind", 
            "#W10-JOINT-CNVRT-RCRCD-IND", FieldType.STRING, 1);
        pnd_Work_Record_10_Pnd_W10_Orig_Da_Cntrct_Nbr = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Orig_Da_Cntrct_Nbr", 
            "#W10-ORIG-DA-CNTRCT-NBR", FieldType.STRING, 8);
        pnd_Work_Record_10_Pnd_W10_Rsdncy_At_Issue_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Rsdncy_At_Issue_Cde", 
            "#W10-RSDNCY-AT-ISSUE-CDE", FieldType.STRING, 3);
        pnd_Work_Record_10_Pnd_W10_First_Annt_Xref_Ind = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_First_Annt_Xref_Ind", 
            "#W10-FIRST-ANNT-XREF-IND", FieldType.STRING, 9);
        pnd_Work_Record_10_Pnd_W10_First_Annt_Dob_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_First_Annt_Dob_Dte", 
            "#W10-FIRST-ANNT-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_10_Pnd_W10_First_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_First_Annt_Mrtlty_Yob_Dte", 
            "#W10-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_10_Pnd_W10_First_Annt_Sex_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_First_Annt_Sex_Cde", 
            "#W10-FIRST-ANNT-SEX-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_10_Pnd_W10_First_Annt_Life_Cnt = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_First_Annt_Life_Cnt", 
            "#W10-FIRST-ANNT-LIFE-CNT", FieldType.NUMERIC, 1);
        pnd_Work_Record_10_Pnd_W10_First_Annt_Dod_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_First_Annt_Dod_Dte", 
            "#W10-FIRST-ANNT-DOD-DTE", FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Xref_Ind = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Xref_Ind", 
            "#W10-SCND-ANNT-XREF-IND", FieldType.STRING, 9);
        pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Dob_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Dob_Dte", 
            "#W10-SCND-ANNT-DOB-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Mrtlty_Yob_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Mrtlty_Yob_Dte", 
            "#W10-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Sex_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Sex_Cde", 
            "#W10-SCND-ANNT-SEX-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Dod_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Dod_Dte", 
            "#W10-SCND-ANNT-DOD-DTE", FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Life_Cnt = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Life_Cnt", 
            "#W10-SCND-ANNT-LIFE-CNT", FieldType.NUMERIC, 1);
        pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Ssn = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Scnd_Annt_Ssn", 
            "#W10-SCND-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Work_Record_10_Pnd_W10_Div_Payee_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Div_Payee_Cde", 
            "#W10-DIV-PAYEE-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_10_Pnd_W10_Div_Coll_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Div_Coll_Cde", 
            "#W10-DIV-COLL-CDE", FieldType.STRING, 5);
        pnd_Work_Record_10_Pnd_W10_Inst_Iss_Cde = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Inst_Iss_Cde", 
            "#W10-INST-ISS-CDE", FieldType.STRING, 5);
        pnd_Work_Record_10_Pnd_W10_Lst_Trans_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Lst_Trans_Dte", 
            "#W10-LST-TRANS-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_10_Pnd_W10_Cntrct_Type = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Cntrct_Type", 
            "#W10-CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Work_Record_10_Pnd_W10_Cntrct_Rsdncy_At_Iss_Re = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Cntrct_Rsdncy_At_Iss_Re", 
            "#W10-CNTRCT-RSDNCY-AT-ISS-RE", FieldType.STRING, 3);
        pnd_Work_Record_10_Pnd_W10_Cntrct_Fnl_Prm_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldArrayInGroup("pnd_Work_Record_10_Pnd_W10_Cntrct_Fnl_Prm_Dte", 
            "#W10-CNTRCT-FNL-PRM-DTE", FieldType.NUMERIC, 8, new DbsArrayController(1,5));
        pnd_Work_Record_10_Pnd_W10_Cntrct_Mtch_Ppcn = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Cntrct_Mtch_Ppcn", 
            "#W10-CNTRCT-MTCH-PPCN", FieldType.STRING, 10);
        pnd_Work_Record_10_Pnd_W10_Cntrct_Annty_Strt_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Cntrct_Annty_Strt_Dte", 
            "#W10-CNTRCT-ANNTY-STRT-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_10_Pnd_W10_Cntrct_Issue_Dte_Dd = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Cntrct_Issue_Dte_Dd", 
            "#W10-CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Record_10_Pnd_W10_Cntrct_Fp_Due_Dte_Dd = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Cntrct_Fp_Due_Dte_Dd", 
            "#W10-CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Record_10_Pnd_W10_Cntrct_Fp_Pd_Dte_Dd = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Cntrct_Fp_Pd_Dte_Dd", 
            "#W10-CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Record_10_Pnd_W10_Roth_Frst_Cntrb_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Roth_Frst_Cntrb_Dte", 
            "#W10-ROTH-FRST-CNTRB-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_10_Pnd_W10_Roth_Ssnng_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Roth_Ssnng_Dte", 
            "#W10-ROTH-SSNNG-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_10_Pnd_W10_Cntrct_Ssnng_Dte = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Cntrct_Ssnng_Dte", 
            "#W10-CNTRCT-SSNNG-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_10_Pnd_W10_Plan_Nmbr = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Plan_Nmbr", 
            "#W10-PLAN-NMBR", FieldType.STRING, 6);
        pnd_Work_Record_10_Pnd_W10_Tax_Exmpt_Ind = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Tax_Exmpt_Ind", 
            "#W10-TAX-EXMPT-IND", FieldType.STRING, 1);
        pnd_Work_Record_10_Pnd_W10_Orig_Ownr_Dob = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Orig_Ownr_Dob", 
            "#W10-ORIG-OWNR-DOB", FieldType.NUMERIC, 8);
        pnd_Work_Record_10_Pnd_W10_Orig_Ownr_Dod = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Orig_Ownr_Dod", 
            "#W10-ORIG-OWNR-DOD", FieldType.NUMERIC, 8);
        pnd_Work_Record_10_Pnd_W10_Sub_Plan_Nmbr = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Sub_Plan_Nmbr", 
            "#W10-SUB-PLAN-NMBR", FieldType.STRING, 6);
        pnd_Work_Record_10_Pnd_W10_Orgntng_Sub_Plan_Nmbr = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Orgntng_Sub_Plan_Nmbr", 
            "#W10-ORGNTNG-SUB-PLAN-NMBR", FieldType.STRING, 6);
        pnd_Work_Record_10_Pnd_W10_Orgntng_Cntrct_Nmbr = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Orgntng_Cntrct_Nmbr", 
            "#W10-ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 10);
        pnd_Work_Record_10_Pnd_W10_Filler = pnd_Work_Record_10_Pnd_Work_Record_10_DataRedef1.newFieldInGroup("pnd_Work_Record_10_Pnd_W10_Filler", "#W10-FILLER", 
            FieldType.STRING, 94);

        pnd_Work_Record_20 = newGroupInRecord("pnd_Work_Record_20", "#WORK-RECORD-20");
        pnd_Work_Record_20_Pnd_Work_Record_20_Data = pnd_Work_Record_20.newFieldInGroup("pnd_Work_Record_20_Pnd_Work_Record_20_Data", "#WORK-RECORD-20-DATA", 
            FieldType.STRING, 367);
        pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2 = pnd_Work_Record_20.newGroupInGroup("pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2", "Redefines", 
            pnd_Work_Record_20_Pnd_Work_Record_20_Data);
        pnd_Work_Record_20_Pnd_W20_Part_Ppcn_Nbr = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Part_Ppcn_Nbr", 
            "#W20-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Work_Record_20_Pnd_W20_Part_Payee_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Part_Payee_Cde", 
            "#W20-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Work_Record_20_Pnd_W20_Record_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Record_Cde", 
            "#W20-RECORD-CDE", FieldType.NUMERIC, 2);
        pnd_Work_Record_20_Pnd_W20_Cpr_Id_Nbr = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Cpr_Id_Nbr", 
            "#W20-CPR-ID-NBR", FieldType.NUMERIC, 7);
        pnd_Work_Record_20_Pnd_W20_Lst_Trans_Dte = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Lst_Trans_Dte", 
            "#W20-LST-TRANS-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_20_Pnd_W20_Prtcpnt_Ctznshp_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Prtcpnt_Ctznshp_Cde", 
            "#W20-PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3);
        pnd_Work_Record_20_Pnd_W20_Prtcpnt_Rsdncy_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Prtcpnt_Rsdncy_Cde", 
            "#W20-PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3);
        pnd_Work_Record_20_Pnd_W20_Prtcpnt_Rsdncy_Sw = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Prtcpnt_Rsdncy_Sw", 
            "#W20-PRTCPNT-RSDNCY-SW", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Prtcpnt_Tax_Id_Nbr = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Prtcpnt_Tax_Id_Nbr", 
            "#W20-PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9);
        pnd_Work_Record_20_Pnd_W20_Prtcpnt_Tax_Id_Typ = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Prtcpnt_Tax_Id_Typ", 
            "#W20-PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Actvty_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Actvty_Cde", 
            "#W20-ACTVTY-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_20_Pnd_W20_Trmnte_Rsn = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Trmnte_Rsn", 
            "#W20-TRMNTE-RSN", FieldType.STRING, 2);
        pnd_Work_Record_20_Pnd_W20_Rwrttn_Ind = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Rwrttn_Ind", 
            "#W20-RWRTTN-IND", FieldType.NUMERIC, 1);
        pnd_Work_Record_20_Pnd_W20_Cash_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Cash_Cde", 
            "#W20-CASH-CDE", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Emplymnt_Trmnt_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Emplymnt_Trmnt_Cde", 
            "#W20-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Company_Cd = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldArrayInGroup("pnd_Work_Record_20_Pnd_W20_Company_Cd", 
            "#W20-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1,5));
        pnd_Work_Record_20_Pnd_W20_Rcvry_Type_Ind = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldArrayInGroup("pnd_Work_Record_20_Pnd_W20_Rcvry_Type_Ind", 
            "#W20-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1,5));
        pnd_Work_Record_20_Pnd_W20_Per_Ivc_Amt = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldArrayInGroup("pnd_Work_Record_20_Pnd_W20_Per_Ivc_Amt", 
            "#W20-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_20_Pnd_W20_Resdl_Ivc_Amt = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldArrayInGroup("pnd_Work_Record_20_Pnd_W20_Resdl_Ivc_Amt", 
            "#W20-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_20_Pnd_W20_Ivc_Amt = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldArrayInGroup("pnd_Work_Record_20_Pnd_W20_Ivc_Amt", 
            "#W20-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_20_Pnd_W20_Ivc_Used_Amt = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldArrayInGroup("pnd_Work_Record_20_Pnd_W20_Ivc_Used_Amt", 
            "#W20-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_20_Pnd_W20_Rtb_Amt = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldArrayInGroup("pnd_Work_Record_20_Pnd_W20_Rtb_Amt", 
            "#W20-RTB-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,5));
        pnd_Work_Record_20_Pnd_W20_Rtb_Percent = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldArrayInGroup("pnd_Work_Record_20_Pnd_W20_Rtb_Percent", 
            "#W20-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7,4, new DbsArrayController(1,5));
        pnd_Work_Record_20_Pnd_W20_Mode_Ind = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Mode_Ind", 
            "#W20-MODE-IND", FieldType.NUMERIC, 3);
        pnd_Work_Record_20_Pnd_W20_Wthdrwl_Dte = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Wthdrwl_Dte", 
            "#W20-WTHDRWL-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_20_Pnd_W20_Final_Per_Pay_Dte = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Final_Per_Pay_Dte", 
            "#W20-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_20_Pnd_W20_Final_Pay_Dte = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Final_Pay_Dte", 
            "#W20-FINAL-PAY-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_20_Pnd_W20_Bnfcry_Xref_Ind = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Bnfcry_Xref_Ind", 
            "#W20-BNFCRY-XREF-IND", FieldType.STRING, 9);
        pnd_Work_Record_20_Pnd_W20_Bnfcry_Dod_Dte = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Bnfcry_Dod_Dte", 
            "#W20-BNFCRY-DOD-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_20_Pnd_W20_Pend_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Pend_Cde", 
            "#W20-PEND-CDE", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Hold_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Hold_Cde", 
            "#W20-HOLD-CDE", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Pend_Dte = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Pend_Dte", 
            "#W20-PEND-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_20_Pnd_W20_Prev_Dist_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Prev_Dist_Cde", 
            "#W20-PREV-DIST-CDE", FieldType.STRING, 4);
        pnd_Work_Record_20_Pnd_W20_Curr_Dist_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Curr_Dist_Cde", 
            "#W20-CURR-DIST-CDE", FieldType.STRING, 4);
        pnd_Work_Record_20_Pnd_W20_Cmbne_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Cmbne_Cde", 
            "#W20-CMBNE-CDE", FieldType.STRING, 12);
        pnd_Work_Record_20_Pnd_W20_Spirt_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Spirt_Cde", 
            "#W20-SPIRT-CDE", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Spirt_Amt = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Spirt_Amt", 
            "#W20-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7,2);
        pnd_Work_Record_20_Pnd_W20_Spirt_Srce = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Spirt_Srce", 
            "#W20-SPIRT-SRCE", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Spirt_Arr_Dte = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Spirt_Arr_Dte", 
            "#W20-SPIRT-ARR-DTE", FieldType.NUMERIC, 4);
        pnd_Work_Record_20_Pnd_W20_Spirt_Prcss_Dte = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Spirt_Prcss_Dte", 
            "#W20-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_20_Pnd_W20_Fed_Tax_Amt = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Fed_Tax_Amt", 
            "#W20-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_20_Pnd_W20_State_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_State_Cde", 
            "#W20-STATE-CDE", FieldType.STRING, 3);
        pnd_Work_Record_20_Pnd_W20_State_Tax_Amt = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_State_Tax_Amt", 
            "#W20-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_20_Pnd_W20_Local_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Local_Cde", 
            "#W20-LOCAL-CDE", FieldType.STRING, 3);
        pnd_Work_Record_20_Pnd_W20_Local_Tax_Amt = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Local_Tax_Amt", 
            "#W20-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_20_Pnd_W20_Lst_Chnge_Dte = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Lst_Chnge_Dte", 
            "#W20-LST-CHNGE-DTE", FieldType.NUMERIC, 6);
        pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Term_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Term_Cde", 
            "#W20-CPR-XFR-TERM-CDE", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Cpr_Lgl_Res_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Cpr_Lgl_Res_Cde", 
            "#W20-CPR-LGL-RES-CDE", FieldType.STRING, 3);
        pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Iss_Dte = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Iss_Dte", 
            "#W20-CPR-XFR-ISS-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_20_Pnd_W20_Rllvr_Cntrct_Nbr = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Rllvr_Cntrct_Nbr", 
            "#W20-RLLVR-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Work_Record_20_Pnd_W20_Rllvr_Ivc_Ind = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Rllvr_Ivc_Ind", 
            "#W20-RLLVR-IVC-IND", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Rllvr_Elgble_Ind = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Rllvr_Elgble_Ind", 
            "#W20-RLLVR-ELGBLE-IND", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Rllvr_Dstrbtng_Irc_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldArrayInGroup("pnd_Work_Record_20_Pnd_W20_Rllvr_Dstrbtng_Irc_Cde", 
            "#W20-RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1,4));
        pnd_Work_Record_20_Pnd_W20_Rllvr_Accptng_Irc_Cde = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Rllvr_Accptng_Irc_Cde", 
            "#W20-RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2);
        pnd_Work_Record_20_Pnd_W20_Rllvr_Pln_Admn_Ind = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Rllvr_Pln_Admn_Ind", 
            "#W20-RLLVR-PLN-ADMN-IND", FieldType.STRING, 1);
        pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Iss_Dte_N8 = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Cpr_Xfr_Iss_Dte_N8", 
            "#W20-CPR-XFR-ISS-DTE-N8", FieldType.NUMERIC, 8);
        pnd_Work_Record_20_Pnd_W20_Roth_Dsblty_Dte = pnd_Work_Record_20_Pnd_Work_Record_20_DataRedef2.newFieldInGroup("pnd_Work_Record_20_Pnd_W20_Roth_Dsblty_Dte", 
            "#W20-ROTH-DSBLTY-DTE", FieldType.NUMERIC, 8);

        pnd_Work_Record_30 = newGroupInRecord("pnd_Work_Record_30", "#WORK-RECORD-30");
        pnd_Work_Record_30_Pnd_Work_Record_30_Data = pnd_Work_Record_30.newFieldInGroup("pnd_Work_Record_30_Pnd_Work_Record_30_Data", "#WORK-RECORD-30-DATA", 
            FieldType.STRING, 367);
        pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3 = pnd_Work_Record_30.newGroupInGroup("pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3", "Redefines", 
            pnd_Work_Record_30_Pnd_Work_Record_30_Data);
        pnd_Work_Record_30_Pnd_W30_Cntrct_Ppcn_Nbr = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Cntrct_Ppcn_Nbr", 
            "#W30-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Work_Record_30_Pnd_W30_Cntrct_Payee_Cde = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Cntrct_Payee_Cde", 
            "#W30-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Work_Record_30_Pnd_W30_Record_Code = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Record_Code", 
            "#W30-RECORD-CODE", FieldType.NUMERIC, 2);
        pnd_Work_Record_30_Pnd_W30_Cmpny_Cde = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Cmpny_Cde", 
            "#W30-CMPNY-CDE", FieldType.STRING, 1);
        pnd_Work_Record_30_Pnd_W30_Fund_Cde = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Fund_Cde", 
            "#W30-FUND-CDE", FieldType.STRING, 2);
        pnd_Work_Record_30_Pnd_W30_Per_Ivc_Amt = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Per_Ivc_Amt", 
            "#W30-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_30_Pnd_W30_Rtb_Amt = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Rtb_Amt", "#W30-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_30_Pnd_W30_Tot_Per_Amt = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Tot_Per_Amt", 
            "#W30-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_30_Pnd_W30_Tot_Div_Amt = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Tot_Div_Amt", 
            "#W30-TOT-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_30_Pnd_W30_Old_Per_Amt = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Old_Per_Amt", 
            "#W30-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_30_Pnd_W30_Old_Div_Amt = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Old_Div_Amt", 
            "#W30-OLD-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_30_Pnd_W30_Rate_Cde = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Rate_Cde", 
            "#W30-RATE-CDE", FieldType.STRING, 2);
        pnd_Work_Record_30_Pnd_W30_Rate_Dte = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Rate_Dte", 
            "#W30-RATE-DTE", FieldType.PACKED_DECIMAL, 6);
        pnd_Work_Record_30_Pnd_W30_Per_Pay_Amt = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Per_Pay_Amt", 
            "#W30-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_30_Pnd_W30_Per_Div_Amt = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Per_Div_Amt", 
            "#W30-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_30_Pnd_W30_Units_Cnt = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Units_Cnt", 
            "#W30-UNITS-CNT", FieldType.PACKED_DECIMAL, 9,3);
        pnd_Work_Record_30_Pnd_W30_Rate_Final_Pay_Amt = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Rate_Final_Pay_Amt", 
            "#W30-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_30_Pnd_W30_Rate_Final_Div_Amt = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Rate_Final_Div_Amt", 
            "#W30-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_30_Pnd_W30_Lst_Trans_Dte = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Lst_Trans_Dte", 
            "#W30-LST-TRANS-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_30_Pnd_W30_Filler = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Filler", "#W30-FILLER", 
            FieldType.STRING, 231);
        pnd_Work_Record_30_Pnd_W30_Filler2 = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Filler2", "#W30-FILLER2", 
            FieldType.STRING, 31);
        pnd_Work_Record_30_Pnd_W30_Filler3 = pnd_Work_Record_30_Pnd_Work_Record_30_DataRedef3.newFieldInGroup("pnd_Work_Record_30_Pnd_W30_Filler3", "#W30-FILLER3", 
            FieldType.STRING, 15);

        pnd_Work_Record_40 = newGroupInRecord("pnd_Work_Record_40", "#WORK-RECORD-40");
        pnd_Work_Record_40_Pnd_Work_Record_40_Data = pnd_Work_Record_40.newFieldInGroup("pnd_Work_Record_40_Pnd_Work_Record_40_Data", "#WORK-RECORD-40-DATA", 
            FieldType.STRING, 367);
        pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4 = pnd_Work_Record_40.newGroupInGroup("pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4", "Redefines", 
            pnd_Work_Record_40_Pnd_Work_Record_40_Data);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Ppcn_Nbr = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Ppcn_Nbr", 
            "#W40-DDCTN-PPCN-NBR", FieldType.STRING, 10);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Payee_Cde = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Payee_Cde", 
            "#W40-DDCTN-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Record_Cde = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Record_Cde", 
            "#W40-DDCTN-RECORD-CDE", FieldType.NUMERIC, 2);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Id_Nbr = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Id_Nbr", 
            "#W40-DDCTN-ID-NBR", FieldType.NUMERIC, 7);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Cde = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Cde", 
            "#W40-DDCTN-CDE", FieldType.STRING, 3);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Seq_Nbr = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Seq_Nbr", 
            "#W40-DDCTN-SEQ-NBR", FieldType.NUMERIC, 3);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Payee = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Payee", 
            "#W40-DDCTN-PAYEE", FieldType.STRING, 5);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Per_Amt = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Per_Amt", 
            "#W40-DDCTN-PER-AMT", FieldType.DECIMAL, 7,2);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Ytd_Amt = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Ytd_Amt", 
            "#W40-DDCTN-YTD-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Pd_To_Dte = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Pd_To_Dte", 
            "#W40-DDCTN-PD-TO-DTE", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Tot_Amt = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Tot_Amt", 
            "#W40-DDCTN-TOT-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Intent_Cde = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Intent_Cde", 
            "#W40-DDCTN-INTENT-CDE", FieldType.NUMERIC, 1);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Strt_Dte = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Strt_Dte", 
            "#W40-DDCTN-STRT-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Stp_Dte = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Stp_Dte", 
            "#W40-DDCTN-STP-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_40_Pnd_W40_Ddctn_Final_Dte = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Ddctn_Final_Dte", 
            "#W40-DDCTN-FINAL-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_40_Pnd_W40_Lst_Trans_Dte = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Lst_Trans_Dte", 
            "#W40-LST-TRANS-DTE", FieldType.NUMERIC, 8);
        pnd_Work_Record_40_Pnd_W40_Filler = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Filler", "#W40-FILLER", 
            FieldType.STRING, 250);
        pnd_Work_Record_40_Pnd_W40_Filler2 = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Filler2", "#W40-FILLER2", 
            FieldType.STRING, 10);
        pnd_Work_Record_40_Pnd_W40_Filler3 = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Filler3", "#W40-FILLER3", 
            FieldType.STRING, 8);
        pnd_Work_Record_40_Pnd_W40_Filler4 = pnd_Work_Record_40_Pnd_Work_Record_40_DataRedef4.newFieldInGroup("pnd_Work_Record_40_Pnd_W40_Filler4", "#W40-FILLER4", 
            FieldType.STRING, 8);

        this.setRecordName("LdaPstl300x");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaPstl300x() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
