/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:22:52 PM
**        * FROM NATURAL PDA     : PSTA7221
************************************************************
**        * FILE NAME            : PdaPsta7221.java
**        * CLASS NAME           : PdaPsta7221
**        * INSTANCE NAME        : PdaPsta7221
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaPsta7221 extends PdaBase
{
    // Properties
    private DbsGroup psta7221;
    private DbsField psta7221_Held_Id;
    private DbsField psta7221_Intervening_Upd_Fld;

    public DbsGroup getPsta7221() { return psta7221; }

    public DbsField getPsta7221_Held_Id() { return psta7221_Held_Id; }

    public DbsField getPsta7221_Intervening_Upd_Fld() { return psta7221_Intervening_Upd_Fld; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        psta7221 = dbsRecord.newGroupInRecord("psta7221", "PSTA7221");
        psta7221.setParameterOption(ParameterOption.ByReference);
        psta7221_Held_Id = psta7221.newFieldInGroup("psta7221_Held_Id", "HELD-ID", FieldType.STRING, 10);
        psta7221_Intervening_Upd_Fld = psta7221.newFieldInGroup("psta7221_Intervening_Upd_Fld", "INTERVENING-UPD-FLD", FieldType.TIME);

        dbsRecord.reset();
    }

    // Constructors
    public PdaPsta7221(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

