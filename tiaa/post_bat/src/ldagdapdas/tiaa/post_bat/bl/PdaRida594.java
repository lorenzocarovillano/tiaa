/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:07 PM
**        * FROM NATURAL PDA     : RIDA594
************************************************************
**        * FILE NAME            : PdaRida594.java
**        * CLASS NAME           : PdaRida594
**        * INSTANCE NAME        : PdaRida594
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaRida594 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Post_Data;
    private DbsField pnd_Post_Data_Pnd_Pst_Rqst_Id;
    private DbsField pnd_Post_Data_Pnd_Pin_Number;
    private DbsField pnd_Post_Data_Pnd_Full_Name;
    private DbsField pnd_Post_Data_Pnd_Address_Line_Txt;
    private DbsField pnd_Post_Data_Pnd_Postal_Data;
    private DbsField pnd_Post_Data_Pnd_Address_Type_Cde;
    private DbsField pnd_Post_Data_Pnd_Test_Code;
    private DbsField pnd_Post_Data_Pnd_Package_Code;
    private DbsField pnd_Post_Data_Pnd_Level;
    private DbsField pnd_Post_Data_Pnd_Institution_Name;
    private DbsField pnd_Post_Data_Pnd_Ctr_Combo;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_ComboRedef1;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Gra_Mdo;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ira_Sra_Gsra_Mdo;
    private DbsField pnd_Post_Data_Pnd_Rider_Date;
    private DbsGroup pnd_Post_Data_Pnd_Rider_DateRedef2;
    private DbsField pnd_Post_Data_Pnd_Rider_Month;
    private DbsField pnd_Post_Data_Pnd_Rider_Year;
    private DbsGroup pnd_Post_Data_Pnd_Rider_YearRedef3;
    private DbsField pnd_Post_Data_Pnd_Rider_Year_A;
    private DbsField pnd_Post_Data_Pnd_Contracts;
    private DbsField pnd_Post_Data_Pnd_Email_Address;

    public DbsGroup getPnd_Post_Data() { return pnd_Post_Data; }

    public DbsField getPnd_Post_Data_Pnd_Pst_Rqst_Id() { return pnd_Post_Data_Pnd_Pst_Rqst_Id; }

    public DbsField getPnd_Post_Data_Pnd_Pin_Number() { return pnd_Post_Data_Pnd_Pin_Number; }

    public DbsField getPnd_Post_Data_Pnd_Full_Name() { return pnd_Post_Data_Pnd_Full_Name; }

    public DbsField getPnd_Post_Data_Pnd_Address_Line_Txt() { return pnd_Post_Data_Pnd_Address_Line_Txt; }

    public DbsField getPnd_Post_Data_Pnd_Postal_Data() { return pnd_Post_Data_Pnd_Postal_Data; }

    public DbsField getPnd_Post_Data_Pnd_Address_Type_Cde() { return pnd_Post_Data_Pnd_Address_Type_Cde; }

    public DbsField getPnd_Post_Data_Pnd_Test_Code() { return pnd_Post_Data_Pnd_Test_Code; }

    public DbsField getPnd_Post_Data_Pnd_Package_Code() { return pnd_Post_Data_Pnd_Package_Code; }

    public DbsField getPnd_Post_Data_Pnd_Level() { return pnd_Post_Data_Pnd_Level; }

    public DbsField getPnd_Post_Data_Pnd_Institution_Name() { return pnd_Post_Data_Pnd_Institution_Name; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Combo() { return pnd_Post_Data_Pnd_Ctr_Combo; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_ComboRedef1() { return pnd_Post_Data_Pnd_Ctr_ComboRedef1; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Gra_Mdo() { return pnd_Post_Data_Pnd_Ctr_Ra_Gra_Mdo; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ira_Sra_Gsra_Mdo() { return pnd_Post_Data_Pnd_Ctr_Ira_Sra_Gsra_Mdo; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Date() { return pnd_Post_Data_Pnd_Rider_Date; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_DateRedef2() { return pnd_Post_Data_Pnd_Rider_DateRedef2; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Month() { return pnd_Post_Data_Pnd_Rider_Month; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year() { return pnd_Post_Data_Pnd_Rider_Year; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_YearRedef3() { return pnd_Post_Data_Pnd_Rider_YearRedef3; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year_A() { return pnd_Post_Data_Pnd_Rider_Year_A; }

    public DbsField getPnd_Post_Data_Pnd_Contracts() { return pnd_Post_Data_Pnd_Contracts; }

    public DbsField getPnd_Post_Data_Pnd_Email_Address() { return pnd_Post_Data_Pnd_Email_Address; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Post_Data = dbsRecord.newGroupInRecord("pnd_Post_Data", "#POST-DATA");
        pnd_Post_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Post_Data_Pnd_Pst_Rqst_Id = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pst_Rqst_Id", "#PST-RQST-ID", FieldType.STRING, 11);
        pnd_Post_Data_Pnd_Pin_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);
        pnd_Post_Data_Pnd_Full_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Post_Data_Pnd_Address_Line_Txt = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        pnd_Post_Data_Pnd_Postal_Data = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 32);
        pnd_Post_Data_Pnd_Address_Type_Cde = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Test_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Test_Code", "#TEST-CODE", FieldType.STRING, 5);
        pnd_Post_Data_Pnd_Package_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Post_Data_Pnd_Level = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Level", "#LEVEL", FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Institution_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 
            60);
        pnd_Post_Data_Pnd_Ctr_Combo = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Combo", "#CTR-COMBO", FieldType.STRING, 2);
        pnd_Post_Data_Pnd_Ctr_ComboRedef1 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_ComboRedef1", "Redefines", pnd_Post_Data_Pnd_Ctr_Combo);
        pnd_Post_Data_Pnd_Ctr_Ra_Gra_Mdo = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Gra_Mdo", "#CTR-RA-GRA-MDO", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ira_Sra_Gsra_Mdo = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ira_Sra_Gsra_Mdo", "#CTR-IRA-SRA-GSRA-MDO", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Rider_Date = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Date", "#RIDER-DATE", FieldType.NUMERIC, 6);
        pnd_Post_Data_Pnd_Rider_DateRedef2 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Rider_DateRedef2", "Redefines", pnd_Post_Data_Pnd_Rider_Date);
        pnd_Post_Data_Pnd_Rider_Month = pnd_Post_Data_Pnd_Rider_DateRedef2.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Month", "#RIDER-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Post_Data_Pnd_Rider_Year = pnd_Post_Data_Pnd_Rider_DateRedef2.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year", "#RIDER-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Post_Data_Pnd_Rider_YearRedef3 = pnd_Post_Data_Pnd_Rider_DateRedef2.newGroupInGroup("pnd_Post_Data_Pnd_Rider_YearRedef3", "Redefines", pnd_Post_Data_Pnd_Rider_Year);
        pnd_Post_Data_Pnd_Rider_Year_A = pnd_Post_Data_Pnd_Rider_YearRedef3.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year_A", "#RIDER-YEAR-A", FieldType.STRING, 
            4);
        pnd_Post_Data_Pnd_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Contracts", "#CONTRACTS", FieldType.STRING, 8, new DbsArrayController(1,
            20));
        pnd_Post_Data_Pnd_Email_Address = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Email_Address", "#EMAIL-ADDRESS", FieldType.STRING, 100);

        dbsRecord.reset();
    }

    // Constructors
    public PdaRida594(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

