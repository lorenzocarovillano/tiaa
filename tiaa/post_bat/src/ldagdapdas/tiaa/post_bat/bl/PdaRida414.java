/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:23:06 PM
**        * FROM NATURAL PDA     : RIDA414
************************************************************
**        * FILE NAME            : PdaRida414.java
**        * CLASS NAME           : PdaRida414
**        * INSTANCE NAME        : PdaRida414
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaRida414 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Post_Data;
    private DbsField pnd_Post_Data_Pnd_Pst_Rqst_Id;
    private DbsField pnd_Post_Data_Pnd_Pin_Number;
    private DbsField pnd_Post_Data_Pnd_Full_Name;
    private DbsField pnd_Post_Data_Pnd_Address_Line_Txt;
    private DbsField pnd_Post_Data_Pnd_Postal_Data;
    private DbsField pnd_Post_Data_Pnd_Address_Type_Cde;
    private DbsField pnd_Post_Data_Pnd_Package_Code;
    private DbsField pnd_Post_Data_Pnd_Letter_Type;
    private DbsField pnd_Post_Data_Pnd_Institution_Name;
    private DbsField pnd_Post_Data_Pnd_Level;
    private DbsField pnd_Post_Data_Pnd_Ctr_Combo;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_ComboRedef1;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_St;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Rp;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Sv;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_St;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Rp;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Sv;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_St;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Sv;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_St;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Sv;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Nj;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Nj;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Nj;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Nj;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Pa;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Pa;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Pa;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Pa;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Ct;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Ct;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Ct;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Ct;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Tx;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Tx;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Tx;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Tx;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Va;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Va;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Va;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Va;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Mi;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Mo;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Mo;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Mo;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Mo;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Fl;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Fl;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Fl;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Fl;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Ok;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Ok;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Ok;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Ok;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Or;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Or;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Or;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Or;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Wi;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Wi;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Wi;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Wi;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Sc;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Sc;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Sc;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Sc;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Ne;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Ne;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Ne;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Ne;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Ar;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Hi;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Ky;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Mt;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Mt;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Mt;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Mt;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Nc;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Tn;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Ut;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Ut;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Vt;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Vt;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Pr;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Pr;
    private DbsField pnd_Post_Data_Pnd_Ctr_Ra_Nh;
    private DbsField pnd_Post_Data_Pnd_Ctr_Sr_Nh;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Nh;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Nh;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gr_Njabp;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gs_Suny;
    private DbsField pnd_Post_Data_Pnd_Rider_Date;
    private DbsGroup pnd_Post_Data_Pnd_Rider_DateRedef2;
    private DbsField pnd_Post_Data_Pnd_Rider_Month;
    private DbsField pnd_Post_Data_Pnd_Rider_Year;
    private DbsGroup pnd_Post_Data_Pnd_Rider_YearRedef3;
    private DbsField pnd_Post_Data_Pnd_Rider_Year_A;
    private DbsField pnd_Post_Data_Pnd_Contracts;
    private DbsField pnd_Post_Data_Pnd_Email_Address;

    public DbsGroup getPnd_Post_Data() { return pnd_Post_Data; }

    public DbsField getPnd_Post_Data_Pnd_Pst_Rqst_Id() { return pnd_Post_Data_Pnd_Pst_Rqst_Id; }

    public DbsField getPnd_Post_Data_Pnd_Pin_Number() { return pnd_Post_Data_Pnd_Pin_Number; }

    public DbsField getPnd_Post_Data_Pnd_Full_Name() { return pnd_Post_Data_Pnd_Full_Name; }

    public DbsField getPnd_Post_Data_Pnd_Address_Line_Txt() { return pnd_Post_Data_Pnd_Address_Line_Txt; }

    public DbsField getPnd_Post_Data_Pnd_Postal_Data() { return pnd_Post_Data_Pnd_Postal_Data; }

    public DbsField getPnd_Post_Data_Pnd_Address_Type_Cde() { return pnd_Post_Data_Pnd_Address_Type_Cde; }

    public DbsField getPnd_Post_Data_Pnd_Package_Code() { return pnd_Post_Data_Pnd_Package_Code; }

    public DbsField getPnd_Post_Data_Pnd_Letter_Type() { return pnd_Post_Data_Pnd_Letter_Type; }

    public DbsField getPnd_Post_Data_Pnd_Institution_Name() { return pnd_Post_Data_Pnd_Institution_Name; }

    public DbsField getPnd_Post_Data_Pnd_Level() { return pnd_Post_Data_Pnd_Level; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Combo() { return pnd_Post_Data_Pnd_Ctr_Combo; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_ComboRedef1() { return pnd_Post_Data_Pnd_Ctr_ComboRedef1; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_St() { return pnd_Post_Data_Pnd_Ctr_Ra_St; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Rp() { return pnd_Post_Data_Pnd_Ctr_Ra_Rp; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Sv() { return pnd_Post_Data_Pnd_Ctr_Ra_Sv; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_St() { return pnd_Post_Data_Pnd_Ctr_Sr_St; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Rp() { return pnd_Post_Data_Pnd_Ctr_Sr_Rp; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Sv() { return pnd_Post_Data_Pnd_Ctr_Sr_Sv; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_St() { return pnd_Post_Data_Pnd_Ctr_Gr_St; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Sv() { return pnd_Post_Data_Pnd_Ctr_Gr_Sv; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_St() { return pnd_Post_Data_Pnd_Ctr_Gs_St; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Sv() { return pnd_Post_Data_Pnd_Ctr_Gs_Sv; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Nj() { return pnd_Post_Data_Pnd_Ctr_Ra_Nj; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Nj() { return pnd_Post_Data_Pnd_Ctr_Sr_Nj; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Nj() { return pnd_Post_Data_Pnd_Ctr_Gr_Nj; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Nj() { return pnd_Post_Data_Pnd_Ctr_Gs_Nj; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Pa() { return pnd_Post_Data_Pnd_Ctr_Ra_Pa; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Pa() { return pnd_Post_Data_Pnd_Ctr_Sr_Pa; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Pa() { return pnd_Post_Data_Pnd_Ctr_Gr_Pa; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Pa() { return pnd_Post_Data_Pnd_Ctr_Gs_Pa; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Ct() { return pnd_Post_Data_Pnd_Ctr_Ra_Ct; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Ct() { return pnd_Post_Data_Pnd_Ctr_Sr_Ct; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Ct() { return pnd_Post_Data_Pnd_Ctr_Gr_Ct; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Ct() { return pnd_Post_Data_Pnd_Ctr_Gs_Ct; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Tx() { return pnd_Post_Data_Pnd_Ctr_Ra_Tx; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Tx() { return pnd_Post_Data_Pnd_Ctr_Sr_Tx; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Tx() { return pnd_Post_Data_Pnd_Ctr_Gr_Tx; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Tx() { return pnd_Post_Data_Pnd_Ctr_Gs_Tx; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Va() { return pnd_Post_Data_Pnd_Ctr_Ra_Va; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Va() { return pnd_Post_Data_Pnd_Ctr_Sr_Va; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Va() { return pnd_Post_Data_Pnd_Ctr_Gr_Va; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Va() { return pnd_Post_Data_Pnd_Ctr_Gs_Va; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Mi() { return pnd_Post_Data_Pnd_Ctr_Gs_Mi; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Mo() { return pnd_Post_Data_Pnd_Ctr_Ra_Mo; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Mo() { return pnd_Post_Data_Pnd_Ctr_Sr_Mo; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Mo() { return pnd_Post_Data_Pnd_Ctr_Gr_Mo; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Mo() { return pnd_Post_Data_Pnd_Ctr_Gs_Mo; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Fl() { return pnd_Post_Data_Pnd_Ctr_Ra_Fl; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Fl() { return pnd_Post_Data_Pnd_Ctr_Sr_Fl; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Fl() { return pnd_Post_Data_Pnd_Ctr_Gr_Fl; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Fl() { return pnd_Post_Data_Pnd_Ctr_Gs_Fl; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Ok() { return pnd_Post_Data_Pnd_Ctr_Ra_Ok; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Ok() { return pnd_Post_Data_Pnd_Ctr_Sr_Ok; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Ok() { return pnd_Post_Data_Pnd_Ctr_Gr_Ok; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Ok() { return pnd_Post_Data_Pnd_Ctr_Gs_Ok; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Or() { return pnd_Post_Data_Pnd_Ctr_Ra_Or; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Or() { return pnd_Post_Data_Pnd_Ctr_Sr_Or; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Or() { return pnd_Post_Data_Pnd_Ctr_Gr_Or; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Or() { return pnd_Post_Data_Pnd_Ctr_Gs_Or; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Wi() { return pnd_Post_Data_Pnd_Ctr_Ra_Wi; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Wi() { return pnd_Post_Data_Pnd_Ctr_Sr_Wi; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Wi() { return pnd_Post_Data_Pnd_Ctr_Gr_Wi; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Wi() { return pnd_Post_Data_Pnd_Ctr_Gs_Wi; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Sc() { return pnd_Post_Data_Pnd_Ctr_Ra_Sc; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Sc() { return pnd_Post_Data_Pnd_Ctr_Sr_Sc; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Sc() { return pnd_Post_Data_Pnd_Ctr_Gr_Sc; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Sc() { return pnd_Post_Data_Pnd_Ctr_Gs_Sc; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Ne() { return pnd_Post_Data_Pnd_Ctr_Ra_Ne; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Ne() { return pnd_Post_Data_Pnd_Ctr_Sr_Ne; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Ne() { return pnd_Post_Data_Pnd_Ctr_Gr_Ne; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Ne() { return pnd_Post_Data_Pnd_Ctr_Gs_Ne; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Ar() { return pnd_Post_Data_Pnd_Ctr_Gs_Ar; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Hi() { return pnd_Post_Data_Pnd_Ctr_Gs_Hi; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Ky() { return pnd_Post_Data_Pnd_Ctr_Gs_Ky; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Mt() { return pnd_Post_Data_Pnd_Ctr_Ra_Mt; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Mt() { return pnd_Post_Data_Pnd_Ctr_Sr_Mt; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Mt() { return pnd_Post_Data_Pnd_Ctr_Gr_Mt; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Mt() { return pnd_Post_Data_Pnd_Ctr_Gs_Mt; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Nc() { return pnd_Post_Data_Pnd_Ctr_Gr_Nc; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Tn() { return pnd_Post_Data_Pnd_Ctr_Gs_Tn; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Ut() { return pnd_Post_Data_Pnd_Ctr_Ra_Ut; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Ut() { return pnd_Post_Data_Pnd_Ctr_Sr_Ut; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Vt() { return pnd_Post_Data_Pnd_Ctr_Sr_Vt; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Vt() { return pnd_Post_Data_Pnd_Ctr_Gs_Vt; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Pr() { return pnd_Post_Data_Pnd_Ctr_Ra_Pr; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Pr() { return pnd_Post_Data_Pnd_Ctr_Gr_Pr; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Ra_Nh() { return pnd_Post_Data_Pnd_Ctr_Ra_Nh; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Sr_Nh() { return pnd_Post_Data_Pnd_Ctr_Sr_Nh; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Nh() { return pnd_Post_Data_Pnd_Ctr_Gr_Nh; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Nh() { return pnd_Post_Data_Pnd_Ctr_Gs_Nh; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gr_Njabp() { return pnd_Post_Data_Pnd_Ctr_Gr_Njabp; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gs_Suny() { return pnd_Post_Data_Pnd_Ctr_Gs_Suny; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Date() { return pnd_Post_Data_Pnd_Rider_Date; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_DateRedef2() { return pnd_Post_Data_Pnd_Rider_DateRedef2; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Month() { return pnd_Post_Data_Pnd_Rider_Month; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year() { return pnd_Post_Data_Pnd_Rider_Year; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_YearRedef3() { return pnd_Post_Data_Pnd_Rider_YearRedef3; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year_A() { return pnd_Post_Data_Pnd_Rider_Year_A; }

    public DbsField getPnd_Post_Data_Pnd_Contracts() { return pnd_Post_Data_Pnd_Contracts; }

    public DbsField getPnd_Post_Data_Pnd_Email_Address() { return pnd_Post_Data_Pnd_Email_Address; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Post_Data = dbsRecord.newGroupInRecord("pnd_Post_Data", "#POST-DATA");
        pnd_Post_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Post_Data_Pnd_Pst_Rqst_Id = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pst_Rqst_Id", "#PST-RQST-ID", FieldType.STRING, 11);
        pnd_Post_Data_Pnd_Pin_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);
        pnd_Post_Data_Pnd_Full_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Post_Data_Pnd_Address_Line_Txt = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        pnd_Post_Data_Pnd_Postal_Data = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 32);
        pnd_Post_Data_Pnd_Address_Type_Cde = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Package_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Post_Data_Pnd_Letter_Type = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Institution_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 
            120);
        pnd_Post_Data_Pnd_Level = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Level", "#LEVEL", FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Ctr_Combo = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Combo", "#CTR-COMBO", FieldType.STRING, 80);
        pnd_Post_Data_Pnd_Ctr_ComboRedef1 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_ComboRedef1", "Redefines", pnd_Post_Data_Pnd_Ctr_Combo);
        pnd_Post_Data_Pnd_Ctr_Ra_St = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_St", "#CTR-RA-ST", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Rp = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Rp", "#CTR-RA-RP", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Sv = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Sv", "#CTR-RA-SV", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_St = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_St", "#CTR-SR-ST", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Rp = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Rp", "#CTR-SR-RP", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Sv = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Sv", "#CTR-SR-SV", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_St = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_St", "#CTR-GR-ST", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Sv = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Sv", "#CTR-GR-SV", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_St = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_St", "#CTR-GS-ST", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Sv = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Sv", "#CTR-GS-SV", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Nj = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Nj", "#CTR-RA-NJ", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Nj = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Nj", "#CTR-SR-NJ", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Nj = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Nj", "#CTR-GR-NJ", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Nj = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Nj", "#CTR-GS-NJ", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Pa = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Pa", "#CTR-RA-PA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Pa = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Pa", "#CTR-SR-PA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Pa = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Pa", "#CTR-GR-PA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Pa = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Pa", "#CTR-GS-PA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Ct = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Ct", "#CTR-RA-CT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Ct = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Ct", "#CTR-SR-CT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Ct = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Ct", "#CTR-GR-CT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Ct = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Ct", "#CTR-GS-CT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Tx = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Tx", "#CTR-RA-TX", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Tx = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Tx", "#CTR-SR-TX", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Tx = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Tx", "#CTR-GR-TX", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Tx = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Tx", "#CTR-GS-TX", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Va = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Va", "#CTR-RA-VA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Va = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Va", "#CTR-SR-VA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Va = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Va", "#CTR-GR-VA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Va = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Va", "#CTR-GS-VA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Mi = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Mi", "#CTR-GS-MI", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Mo = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Mo", "#CTR-RA-MO", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Mo = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Mo", "#CTR-SR-MO", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Mo = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Mo", "#CTR-GR-MO", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Mo = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Mo", "#CTR-GS-MO", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Fl = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Fl", "#CTR-RA-FL", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Fl = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Fl", "#CTR-SR-FL", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Fl = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Fl", "#CTR-GR-FL", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Fl = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Fl", "#CTR-GS-FL", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Ok = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Ok", "#CTR-RA-OK", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Ok = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Ok", "#CTR-SR-OK", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Ok = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Ok", "#CTR-GR-OK", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Ok = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Ok", "#CTR-GS-OK", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Or = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Or", "#CTR-RA-OR", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Or = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Or", "#CTR-SR-OR", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Or = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Or", "#CTR-GR-OR", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Or = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Or", "#CTR-GS-OR", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Wi = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Wi", "#CTR-RA-WI", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Wi = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Wi", "#CTR-SR-WI", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Wi = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Wi", "#CTR-GR-WI", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Wi = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Wi", "#CTR-GS-WI", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Sc = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Sc", "#CTR-RA-SC", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Sc = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Sc", "#CTR-SR-SC", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Sc = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Sc", "#CTR-GR-SC", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Sc = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Sc", "#CTR-GS-SC", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Ne = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Ne", "#CTR-RA-NE", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Ne = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Ne", "#CTR-SR-NE", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Ne = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Ne", "#CTR-GR-NE", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Ne = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Ne", "#CTR-GS-NE", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Ar = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Ar", "#CTR-GS-AR", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Hi = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Hi", "#CTR-GS-HI", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Ky = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Ky", "#CTR-GS-KY", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Mt = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Mt", "#CTR-RA-MT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Mt = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Mt", "#CTR-SR-MT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Mt = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Mt", "#CTR-GR-MT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Mt = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Mt", "#CTR-GS-MT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Nc = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Nc", "#CTR-GR-NC", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Tn = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Tn", "#CTR-GS-TN", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Ut = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Ut", "#CTR-RA-UT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Ut = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Ut", "#CTR-SR-UT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Vt = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Vt", "#CTR-SR-VT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Vt = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Vt", "#CTR-GS-VT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Pr = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Pr", "#CTR-RA-PR", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Pr = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Pr", "#CTR-GR-PR", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Ra_Nh = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Ra_Nh", "#CTR-RA-NH", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Sr_Nh = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Sr_Nh", "#CTR-SR-NH", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Nh = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Nh", "#CTR-GR-NH", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Nh = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Nh", "#CTR-GS-NH", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gr_Njabp = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gr_Njabp", "#CTR-GR-NJABP", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gs_Suny = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gs_Suny", "#CTR-GS-SUNY", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Rider_Date = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Date", "#RIDER-DATE", FieldType.NUMERIC, 6);
        pnd_Post_Data_Pnd_Rider_DateRedef2 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Rider_DateRedef2", "Redefines", pnd_Post_Data_Pnd_Rider_Date);
        pnd_Post_Data_Pnd_Rider_Month = pnd_Post_Data_Pnd_Rider_DateRedef2.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Month", "#RIDER-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Post_Data_Pnd_Rider_Year = pnd_Post_Data_Pnd_Rider_DateRedef2.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year", "#RIDER-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Post_Data_Pnd_Rider_YearRedef3 = pnd_Post_Data_Pnd_Rider_DateRedef2.newGroupInGroup("pnd_Post_Data_Pnd_Rider_YearRedef3", "Redefines", pnd_Post_Data_Pnd_Rider_Year);
        pnd_Post_Data_Pnd_Rider_Year_A = pnd_Post_Data_Pnd_Rider_YearRedef3.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year_A", "#RIDER-YEAR-A", FieldType.STRING, 
            4);
        pnd_Post_Data_Pnd_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Contracts", "#CONTRACTS", FieldType.STRING, 8, new DbsArrayController(1,
            20));
        pnd_Post_Data_Pnd_Email_Address = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Email_Address", "#EMAIL-ADDRESS", FieldType.STRING, 100);

        dbsRecord.reset();
    }

    // Constructors
    public PdaRida414(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

