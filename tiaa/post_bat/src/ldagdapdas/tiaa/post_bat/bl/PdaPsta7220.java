/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:22:51 PM
**        * FROM NATURAL PDA     : PSTA7220
************************************************************
**        * FILE NAME            : PdaPsta7220.java
**        * CLASS NAME           : PdaPsta7220
**        * INSTANCE NAME        : PdaPsta7220
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaPsta7220 extends PdaBase
{
    // Properties
    private DbsGroup psta7220;
    private DbsField psta7220_Pckge_Cde;
    private DbsField psta7220_Pckge_Vrsn_Cde;
    private DbsField psta7220_Pckge_Short_Nme;
    private DbsField psta7220_Pckge_Owner_Unit_Cde;
    private DbsField psta7220_Pckge_Dscrptn_Txt;
    private DbsField psta7220_Pckge_Finish_Cde;
    private DbsField psta7220_Pckge_Post_Vrsn_Cde;
    private DbsField psta7220_Pckge_Updte_Dte_Tme;
    private DbsField psta7220_Pckge_Updte_Oprtr_Cde;
    private DbsField psta7220_Pckge_File_Copy_Ind;
    private DbsField psta7220_Pckge_Mit_Ctgry;
    private DbsField psta7220_Pckge_Mit_Clss;
    private DbsField psta7220_Pckge_Mit_Spcfc;
    private DbsField psta7220_Pckge_Csf_Tble_Gnrtn_Mdle;
    private DbsField psta7220_Pckge_Csf_Dta_Gnrtn_Mdle;
    private DbsField psta7220_Pckge_Csf_File_Name;
    private DbsField psta7220_Pckge_Cmbne_Ind;
    private DbsField psta7220_Pckge_Immdte_Prnt_Ind;
    private DbsField psta7220_Pckge_Image_Ind;
    private DbsField psta7220_Pckge_Image_Ctgry;
    private DbsField psta7220_Pckge_Image_Clss;
    private DbsField psta7220_Pckge_Image_Spcfc;
    private DbsField psta7220_Pckge_Dflt_Dlvry_Typ;
    private DbsField psta7220_Pckge_Dflt_Print_Fclty_Cde;
    private DbsGroup psta7220_Purge_Dtls;
    private DbsField psta7220_Purge_Status_Cde;
    private DbsField psta7220_Purge_Days;
    private DbsGroup psta7220_Applctn_Dtls;
    private DbsField psta7220_Systm_Nme;
    private DbsField psta7220_Days_On_File;
    private DbsField psta7220_Actn_On_Dltn;
    private DbsField psta7220_Gnrt_Jcl_Ovrrde;
    private DbsField psta7220_Rprnt_Jcl_Ovrrde;
    private DbsField psta7220_Rimge_Jcl_Override;
    private DbsGroup psta7220_Deliveryprofiles;
    private DbsField psta7220_Pckge_Delivery_Type;
    private DbsField psta7220_Fnsh_Lctn;
    private DbsField psta7220_Dflt_Dlvry_Ind;
    private DbsField psta7220_Fnsh_Prfle;
    private DbsField psta7220_Dlvry_Prfl_Unvlbl_Ind;
    private DbsField psta7220_Id;
    private DbsGroup psta7220_IdRedef1;
    private DbsGroup psta7220_Id_Structure;
    private DbsField psta7220_Id_Pckge_Cde;
    private DbsField psta7220_Id_Pckge_Vrsn_Cde;

    public DbsGroup getPsta7220() { return psta7220; }

    public DbsField getPsta7220_Pckge_Cde() { return psta7220_Pckge_Cde; }

    public DbsField getPsta7220_Pckge_Vrsn_Cde() { return psta7220_Pckge_Vrsn_Cde; }

    public DbsField getPsta7220_Pckge_Short_Nme() { return psta7220_Pckge_Short_Nme; }

    public DbsField getPsta7220_Pckge_Owner_Unit_Cde() { return psta7220_Pckge_Owner_Unit_Cde; }

    public DbsField getPsta7220_Pckge_Dscrptn_Txt() { return psta7220_Pckge_Dscrptn_Txt; }

    public DbsField getPsta7220_Pckge_Finish_Cde() { return psta7220_Pckge_Finish_Cde; }

    public DbsField getPsta7220_Pckge_Post_Vrsn_Cde() { return psta7220_Pckge_Post_Vrsn_Cde; }

    public DbsField getPsta7220_Pckge_Updte_Dte_Tme() { return psta7220_Pckge_Updte_Dte_Tme; }

    public DbsField getPsta7220_Pckge_Updte_Oprtr_Cde() { return psta7220_Pckge_Updte_Oprtr_Cde; }

    public DbsField getPsta7220_Pckge_File_Copy_Ind() { return psta7220_Pckge_File_Copy_Ind; }

    public DbsField getPsta7220_Pckge_Mit_Ctgry() { return psta7220_Pckge_Mit_Ctgry; }

    public DbsField getPsta7220_Pckge_Mit_Clss() { return psta7220_Pckge_Mit_Clss; }

    public DbsField getPsta7220_Pckge_Mit_Spcfc() { return psta7220_Pckge_Mit_Spcfc; }

    public DbsField getPsta7220_Pckge_Csf_Tble_Gnrtn_Mdle() { return psta7220_Pckge_Csf_Tble_Gnrtn_Mdle; }

    public DbsField getPsta7220_Pckge_Csf_Dta_Gnrtn_Mdle() { return psta7220_Pckge_Csf_Dta_Gnrtn_Mdle; }

    public DbsField getPsta7220_Pckge_Csf_File_Name() { return psta7220_Pckge_Csf_File_Name; }

    public DbsField getPsta7220_Pckge_Cmbne_Ind() { return psta7220_Pckge_Cmbne_Ind; }

    public DbsField getPsta7220_Pckge_Immdte_Prnt_Ind() { return psta7220_Pckge_Immdte_Prnt_Ind; }

    public DbsField getPsta7220_Pckge_Image_Ind() { return psta7220_Pckge_Image_Ind; }

    public DbsField getPsta7220_Pckge_Image_Ctgry() { return psta7220_Pckge_Image_Ctgry; }

    public DbsField getPsta7220_Pckge_Image_Clss() { return psta7220_Pckge_Image_Clss; }

    public DbsField getPsta7220_Pckge_Image_Spcfc() { return psta7220_Pckge_Image_Spcfc; }

    public DbsField getPsta7220_Pckge_Dflt_Dlvry_Typ() { return psta7220_Pckge_Dflt_Dlvry_Typ; }

    public DbsField getPsta7220_Pckge_Dflt_Print_Fclty_Cde() { return psta7220_Pckge_Dflt_Print_Fclty_Cde; }

    public DbsGroup getPsta7220_Purge_Dtls() { return psta7220_Purge_Dtls; }

    public DbsField getPsta7220_Purge_Status_Cde() { return psta7220_Purge_Status_Cde; }

    public DbsField getPsta7220_Purge_Days() { return psta7220_Purge_Days; }

    public DbsGroup getPsta7220_Applctn_Dtls() { return psta7220_Applctn_Dtls; }

    public DbsField getPsta7220_Systm_Nme() { return psta7220_Systm_Nme; }

    public DbsField getPsta7220_Days_On_File() { return psta7220_Days_On_File; }

    public DbsField getPsta7220_Actn_On_Dltn() { return psta7220_Actn_On_Dltn; }

    public DbsField getPsta7220_Gnrt_Jcl_Ovrrde() { return psta7220_Gnrt_Jcl_Ovrrde; }

    public DbsField getPsta7220_Rprnt_Jcl_Ovrrde() { return psta7220_Rprnt_Jcl_Ovrrde; }

    public DbsField getPsta7220_Rimge_Jcl_Override() { return psta7220_Rimge_Jcl_Override; }

    public DbsGroup getPsta7220_Deliveryprofiles() { return psta7220_Deliveryprofiles; }

    public DbsField getPsta7220_Pckge_Delivery_Type() { return psta7220_Pckge_Delivery_Type; }

    public DbsField getPsta7220_Fnsh_Lctn() { return psta7220_Fnsh_Lctn; }

    public DbsField getPsta7220_Dflt_Dlvry_Ind() { return psta7220_Dflt_Dlvry_Ind; }

    public DbsField getPsta7220_Fnsh_Prfle() { return psta7220_Fnsh_Prfle; }

    public DbsField getPsta7220_Dlvry_Prfl_Unvlbl_Ind() { return psta7220_Dlvry_Prfl_Unvlbl_Ind; }

    public DbsField getPsta7220_Id() { return psta7220_Id; }

    public DbsGroup getPsta7220_IdRedef1() { return psta7220_IdRedef1; }

    public DbsGroup getPsta7220_Id_Structure() { return psta7220_Id_Structure; }

    public DbsField getPsta7220_Id_Pckge_Cde() { return psta7220_Id_Pckge_Cde; }

    public DbsField getPsta7220_Id_Pckge_Vrsn_Cde() { return psta7220_Id_Pckge_Vrsn_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        psta7220 = dbsRecord.newGroupInRecord("psta7220", "PSTA7220");
        psta7220.setParameterOption(ParameterOption.ByReference);
        psta7220_Pckge_Cde = psta7220.newFieldInGroup("psta7220_Pckge_Cde", "PCKGE-CDE", FieldType.STRING, 8);
        psta7220_Pckge_Vrsn_Cde = psta7220.newFieldInGroup("psta7220_Pckge_Vrsn_Cde", "PCKGE-VRSN-CDE", FieldType.STRING, 2);
        psta7220_Pckge_Short_Nme = psta7220.newFieldInGroup("psta7220_Pckge_Short_Nme", "PCKGE-SHORT-NME", FieldType.STRING, 30);
        psta7220_Pckge_Owner_Unit_Cde = psta7220.newFieldInGroup("psta7220_Pckge_Owner_Unit_Cde", "PCKGE-OWNER-UNIT-CDE", FieldType.STRING, 8);
        psta7220_Pckge_Dscrptn_Txt = psta7220.newFieldArrayInGroup("psta7220_Pckge_Dscrptn_Txt", "PCKGE-DSCRPTN-TXT", FieldType.STRING, 30, new DbsArrayController(1,
            6));
        psta7220_Pckge_Finish_Cde = psta7220.newFieldInGroup("psta7220_Pckge_Finish_Cde", "PCKGE-FINISH-CDE", FieldType.STRING, 8);
        psta7220_Pckge_Post_Vrsn_Cde = psta7220.newFieldInGroup("psta7220_Pckge_Post_Vrsn_Cde", "PCKGE-POST-VRSN-CDE", FieldType.STRING, 2);
        psta7220_Pckge_Updte_Dte_Tme = psta7220.newFieldInGroup("psta7220_Pckge_Updte_Dte_Tme", "PCKGE-UPDTE-DTE-TME", FieldType.TIME);
        psta7220_Pckge_Updte_Oprtr_Cde = psta7220.newFieldInGroup("psta7220_Pckge_Updte_Oprtr_Cde", "PCKGE-UPDTE-OPRTR-CDE", FieldType.STRING, 8);
        psta7220_Pckge_File_Copy_Ind = psta7220.newFieldInGroup("psta7220_Pckge_File_Copy_Ind", "PCKGE-FILE-COPY-IND", FieldType.BOOLEAN);
        psta7220_Pckge_Mit_Ctgry = psta7220.newFieldInGroup("psta7220_Pckge_Mit_Ctgry", "PCKGE-MIT-CTGRY", FieldType.STRING, 1);
        psta7220_Pckge_Mit_Clss = psta7220.newFieldInGroup("psta7220_Pckge_Mit_Clss", "PCKGE-MIT-CLSS", FieldType.STRING, 3);
        psta7220_Pckge_Mit_Spcfc = psta7220.newFieldInGroup("psta7220_Pckge_Mit_Spcfc", "PCKGE-MIT-SPCFC", FieldType.STRING, 4);
        psta7220_Pckge_Csf_Tble_Gnrtn_Mdle = psta7220.newFieldInGroup("psta7220_Pckge_Csf_Tble_Gnrtn_Mdle", "PCKGE-CSF-TBLE-GNRTN-MDLE", FieldType.STRING, 
            8);
        psta7220_Pckge_Csf_Dta_Gnrtn_Mdle = psta7220.newFieldInGroup("psta7220_Pckge_Csf_Dta_Gnrtn_Mdle", "PCKGE-CSF-DTA-GNRTN-MDLE", FieldType.STRING, 
            8);
        psta7220_Pckge_Csf_File_Name = psta7220.newFieldInGroup("psta7220_Pckge_Csf_File_Name", "PCKGE-CSF-FILE-NAME", FieldType.STRING, 16);
        psta7220_Pckge_Cmbne_Ind = psta7220.newFieldInGroup("psta7220_Pckge_Cmbne_Ind", "PCKGE-CMBNE-IND", FieldType.STRING, 1);
        psta7220_Pckge_Immdte_Prnt_Ind = psta7220.newFieldInGroup("psta7220_Pckge_Immdte_Prnt_Ind", "PCKGE-IMMDTE-PRNT-IND", FieldType.STRING, 1);
        psta7220_Pckge_Image_Ind = psta7220.newFieldInGroup("psta7220_Pckge_Image_Ind", "PCKGE-IMAGE-IND", FieldType.STRING, 1);
        psta7220_Pckge_Image_Ctgry = psta7220.newFieldInGroup("psta7220_Pckge_Image_Ctgry", "PCKGE-IMAGE-CTGRY", FieldType.STRING, 1);
        psta7220_Pckge_Image_Clss = psta7220.newFieldInGroup("psta7220_Pckge_Image_Clss", "PCKGE-IMAGE-CLSS", FieldType.STRING, 3);
        psta7220_Pckge_Image_Spcfc = psta7220.newFieldInGroup("psta7220_Pckge_Image_Spcfc", "PCKGE-IMAGE-SPCFC", FieldType.STRING, 4);
        psta7220_Pckge_Dflt_Dlvry_Typ = psta7220.newFieldInGroup("psta7220_Pckge_Dflt_Dlvry_Typ", "PCKGE-DFLT-DLVRY-TYP", FieldType.STRING, 3);
        psta7220_Pckge_Dflt_Print_Fclty_Cde = psta7220.newFieldInGroup("psta7220_Pckge_Dflt_Print_Fclty_Cde", "PCKGE-DFLT-PRINT-FCLTY-CDE", FieldType.STRING, 
            1);
        psta7220_Purge_Dtls = psta7220.newGroupArrayInGroup("psta7220_Purge_Dtls", "PURGE-DTLS", new DbsArrayController(1,2));
        psta7220_Purge_Status_Cde = psta7220_Purge_Dtls.newFieldInGroup("psta7220_Purge_Status_Cde", "PURGE-STATUS-CDE", FieldType.STRING, 8);
        psta7220_Purge_Days = psta7220_Purge_Dtls.newFieldInGroup("psta7220_Purge_Days", "PURGE-DAYS", FieldType.PACKED_DECIMAL, 3);
        psta7220_Applctn_Dtls = psta7220.newGroupArrayInGroup("psta7220_Applctn_Dtls", "APPLCTN-DTLS", new DbsArrayController(1,20));
        psta7220_Systm_Nme = psta7220_Applctn_Dtls.newFieldInGroup("psta7220_Systm_Nme", "SYSTM-NME", FieldType.STRING, 8);
        psta7220_Days_On_File = psta7220_Applctn_Dtls.newFieldInGroup("psta7220_Days_On_File", "DAYS-ON-FILE", FieldType.PACKED_DECIMAL, 3);
        psta7220_Actn_On_Dltn = psta7220_Applctn_Dtls.newFieldInGroup("psta7220_Actn_On_Dltn", "ACTN-ON-DLTN", FieldType.STRING, 1);
        psta7220_Gnrt_Jcl_Ovrrde = psta7220.newFieldInGroup("psta7220_Gnrt_Jcl_Ovrrde", "GNRT-JCL-OVRRDE", FieldType.STRING, 8);
        psta7220_Rprnt_Jcl_Ovrrde = psta7220.newFieldInGroup("psta7220_Rprnt_Jcl_Ovrrde", "RPRNT-JCL-OVRRDE", FieldType.STRING, 8);
        psta7220_Rimge_Jcl_Override = psta7220.newFieldInGroup("psta7220_Rimge_Jcl_Override", "RIMGE-JCL-OVERRIDE", FieldType.STRING, 8);
        psta7220_Deliveryprofiles = psta7220.newGroupArrayInGroup("psta7220_Deliveryprofiles", "DELIVERYPROFILES", new DbsArrayController(1,20));
        psta7220_Pckge_Delivery_Type = psta7220_Deliveryprofiles.newFieldInGroup("psta7220_Pckge_Delivery_Type", "PCKGE-DELIVERY-TYPE", FieldType.STRING, 
            3);
        psta7220_Fnsh_Lctn = psta7220_Deliveryprofiles.newFieldInGroup("psta7220_Fnsh_Lctn", "FNSH-LCTN", FieldType.STRING, 8);
        psta7220_Dflt_Dlvry_Ind = psta7220_Deliveryprofiles.newFieldInGroup("psta7220_Dflt_Dlvry_Ind", "DFLT-DLVRY-IND", FieldType.BOOLEAN);
        psta7220_Fnsh_Prfle = psta7220_Deliveryprofiles.newFieldInGroup("psta7220_Fnsh_Prfle", "FNSH-PRFLE", FieldType.STRING, 8);
        psta7220_Dlvry_Prfl_Unvlbl_Ind = psta7220_Deliveryprofiles.newFieldInGroup("psta7220_Dlvry_Prfl_Unvlbl_Ind", "DLVRY-PRFL-UNVLBL-IND", FieldType.BOOLEAN);

        psta7220_Id = dbsRecord.newFieldInRecord("psta7220_Id", "PSTA7220-ID", FieldType.STRING, 10);
        psta7220_Id.setParameterOption(ParameterOption.ByReference);
        psta7220_IdRedef1 = dbsRecord.newGroupInRecord("psta7220_IdRedef1", "Redefines", psta7220_Id);
        psta7220_Id_Structure = psta7220_IdRedef1.newGroupInGroup("psta7220_Id_Structure", "STRUCTURE");
        psta7220_Id_Pckge_Cde = psta7220_Id_Structure.newFieldInGroup("psta7220_Id_Pckge_Cde", "PCKGE-CDE", FieldType.STRING, 8);
        psta7220_Id_Pckge_Vrsn_Cde = psta7220_Id_Structure.newFieldInGroup("psta7220_Id_Pckge_Vrsn_Cde", "PCKGE-VRSN-CDE", FieldType.STRING, 2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaPsta7220(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

