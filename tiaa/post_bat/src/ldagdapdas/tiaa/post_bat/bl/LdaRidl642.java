/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:11:40 PM
**        * FROM NATURAL LDA     : RIDL642
************************************************************
**        * FILE NAME            : LdaRidl642.java
**        * CLASS NAME           : LdaRidl642
**        * INSTANCE NAME        : LdaRidl642
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaRidl642 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Post_Data;
    private DbsField pnd_Post_Data_Pnd_Pst_Rqst_Id;
    private DbsField pnd_Post_Data_Pnd_Pin_Number;
    private DbsField pnd_Post_Data_Pnd_Full_Name;
    private DbsField pnd_Post_Data_Pnd_Address_Line_Txt;
    private DbsField pnd_Post_Data_Pnd_Postal_Data;
    private DbsField pnd_Post_Data_Pnd_Address_Type_Cde;
    private DbsField pnd_Post_Data_Pnd_Package_Code;
    private DbsField pnd_Post_Data_Pnd_Letter_Type;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cref_Contract;
    private DbsGroup pnd_Post_Data_Pnd_Contract_Details;
    private DbsField pnd_Post_Data_Pnd_Cref_Contracts;
    private DbsField pnd_Post_Data_Pnd_Contract_Type;
    private DbsField pnd_Post_Data_Pnd_Contract_State;
    private DbsField pnd_Post_Data_Pnd_Multi_Plan_Ctr;
    private DbsField pnd_Post_Data_Pnd_Plan_Name;
    private DbsField pnd_Post_Data_Pnd_Share_Class;
    private DbsField pnd_Post_Data_Pnd_Rider_Date;
    private DbsGroup pnd_Post_Data_Pnd_Rider_DateRedef1;
    private DbsField pnd_Post_Data_Pnd_Rider_Month;
    private DbsField pnd_Post_Data_Pnd_Rider_Year;
    private DbsGroup pnd_Post_Data_Pnd_Rider_YearRedef2;
    private DbsField pnd_Post_Data_Pnd_Rider_Year_A;
    private DbsField pnd_Post_Data_Pnd_Institution_Name;
    private DbsField pnd_Post_Data_Pnd_Effective_Date;
    private DbsField pnd_Post_Data_Pnd_Email_Address;

    public DbsGroup getPnd_Post_Data() { return pnd_Post_Data; }

    public DbsField getPnd_Post_Data_Pnd_Pst_Rqst_Id() { return pnd_Post_Data_Pnd_Pst_Rqst_Id; }

    public DbsField getPnd_Post_Data_Pnd_Pin_Number() { return pnd_Post_Data_Pnd_Pin_Number; }

    public DbsField getPnd_Post_Data_Pnd_Full_Name() { return pnd_Post_Data_Pnd_Full_Name; }

    public DbsField getPnd_Post_Data_Pnd_Address_Line_Txt() { return pnd_Post_Data_Pnd_Address_Line_Txt; }

    public DbsField getPnd_Post_Data_Pnd_Postal_Data() { return pnd_Post_Data_Pnd_Postal_Data; }

    public DbsField getPnd_Post_Data_Pnd_Address_Type_Cde() { return pnd_Post_Data_Pnd_Address_Type_Cde; }

    public DbsField getPnd_Post_Data_Pnd_Package_Code() { return pnd_Post_Data_Pnd_Package_Code; }

    public DbsField getPnd_Post_Data_Pnd_Letter_Type() { return pnd_Post_Data_Pnd_Letter_Type; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cref_Contract() { return pnd_Post_Data_Pnd_Ctr_Cref_Contract; }

    public DbsGroup getPnd_Post_Data_Pnd_Contract_Details() { return pnd_Post_Data_Pnd_Contract_Details; }

    public DbsField getPnd_Post_Data_Pnd_Cref_Contracts() { return pnd_Post_Data_Pnd_Cref_Contracts; }

    public DbsField getPnd_Post_Data_Pnd_Contract_Type() { return pnd_Post_Data_Pnd_Contract_Type; }

    public DbsField getPnd_Post_Data_Pnd_Contract_State() { return pnd_Post_Data_Pnd_Contract_State; }

    public DbsField getPnd_Post_Data_Pnd_Multi_Plan_Ctr() { return pnd_Post_Data_Pnd_Multi_Plan_Ctr; }

    public DbsField getPnd_Post_Data_Pnd_Plan_Name() { return pnd_Post_Data_Pnd_Plan_Name; }

    public DbsField getPnd_Post_Data_Pnd_Share_Class() { return pnd_Post_Data_Pnd_Share_Class; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Date() { return pnd_Post_Data_Pnd_Rider_Date; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_DateRedef1() { return pnd_Post_Data_Pnd_Rider_DateRedef1; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Month() { return pnd_Post_Data_Pnd_Rider_Month; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year() { return pnd_Post_Data_Pnd_Rider_Year; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_YearRedef2() { return pnd_Post_Data_Pnd_Rider_YearRedef2; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year_A() { return pnd_Post_Data_Pnd_Rider_Year_A; }

    public DbsField getPnd_Post_Data_Pnd_Institution_Name() { return pnd_Post_Data_Pnd_Institution_Name; }

    public DbsField getPnd_Post_Data_Pnd_Effective_Date() { return pnd_Post_Data_Pnd_Effective_Date; }

    public DbsField getPnd_Post_Data_Pnd_Email_Address() { return pnd_Post_Data_Pnd_Email_Address; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Post_Data = newGroupInRecord("pnd_Post_Data", "#POST-DATA");
        pnd_Post_Data_Pnd_Pst_Rqst_Id = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pst_Rqst_Id", "#PST-RQST-ID", FieldType.STRING, 11);
        pnd_Post_Data_Pnd_Pin_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);
        pnd_Post_Data_Pnd_Full_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Post_Data_Pnd_Address_Line_Txt = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        pnd_Post_Data_Pnd_Postal_Data = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 32);
        pnd_Post_Data_Pnd_Address_Type_Cde = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Package_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Post_Data_Pnd_Letter_Type = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Ctr_Cref_Contract = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cref_Contract", "#CTR-CREF-CONTRACT", FieldType.NUMERIC, 
            2);
        pnd_Post_Data_Pnd_Contract_Details = pnd_Post_Data.newGroupArrayInGroup("pnd_Post_Data_Pnd_Contract_Details", "#CONTRACT-DETAILS", new DbsArrayController(1,
            10));
        pnd_Post_Data_Pnd_Cref_Contracts = pnd_Post_Data_Pnd_Contract_Details.newFieldInGroup("pnd_Post_Data_Pnd_Cref_Contracts", "#CREF-CONTRACTS", FieldType.STRING, 
            8);
        pnd_Post_Data_Pnd_Contract_Type = pnd_Post_Data_Pnd_Contract_Details.newFieldInGroup("pnd_Post_Data_Pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 
            2);
        pnd_Post_Data_Pnd_Contract_State = pnd_Post_Data_Pnd_Contract_Details.newFieldInGroup("pnd_Post_Data_Pnd_Contract_State", "#CONTRACT-STATE", FieldType.STRING, 
            2);
        pnd_Post_Data_Pnd_Multi_Plan_Ctr = pnd_Post_Data_Pnd_Contract_Details.newFieldInGroup("pnd_Post_Data_Pnd_Multi_Plan_Ctr", "#MULTI-PLAN-CTR", FieldType.NUMERIC, 
            2);
        pnd_Post_Data_Pnd_Plan_Name = pnd_Post_Data_Pnd_Contract_Details.newFieldArrayInGroup("pnd_Post_Data_Pnd_Plan_Name", "#PLAN-NAME", FieldType.STRING, 
            80, new DbsArrayController(1,10));
        pnd_Post_Data_Pnd_Share_Class = pnd_Post_Data_Pnd_Contract_Details.newFieldArrayInGroup("pnd_Post_Data_Pnd_Share_Class", "#SHARE-CLASS", FieldType.STRING, 
            2, new DbsArrayController(1,10));
        pnd_Post_Data_Pnd_Rider_Date = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Date", "#RIDER-DATE", FieldType.NUMERIC, 6);
        pnd_Post_Data_Pnd_Rider_DateRedef1 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Rider_DateRedef1", "Redefines", pnd_Post_Data_Pnd_Rider_Date);
        pnd_Post_Data_Pnd_Rider_Month = pnd_Post_Data_Pnd_Rider_DateRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Month", "#RIDER-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Post_Data_Pnd_Rider_Year = pnd_Post_Data_Pnd_Rider_DateRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year", "#RIDER-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Post_Data_Pnd_Rider_YearRedef2 = pnd_Post_Data_Pnd_Rider_DateRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Rider_YearRedef2", "Redefines", pnd_Post_Data_Pnd_Rider_Year);
        pnd_Post_Data_Pnd_Rider_Year_A = pnd_Post_Data_Pnd_Rider_YearRedef2.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year_A", "#RIDER-YEAR-A", FieldType.STRING, 
            4);
        pnd_Post_Data_Pnd_Institution_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 
            120);
        pnd_Post_Data_Pnd_Effective_Date = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.STRING, 8);
        pnd_Post_Data_Pnd_Email_Address = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Email_Address", "#EMAIL-ADDRESS", FieldType.STRING, 100);

        this.setRecordName("LdaRidl642");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaRidl642() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
