/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:11:39 PM
**        * FROM NATURAL LDA     : RIDL482
************************************************************
**        * FILE NAME            : LdaRidl482.java
**        * CLASS NAME           : LdaRidl482
**        * INSTANCE NAME        : LdaRidl482
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaRidl482 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Post_Data;
    private DbsField pnd_Post_Data_Pnd_Pst_Rqst_Id;
    private DbsField pnd_Post_Data_Pnd_Pin_Number;
    private DbsField pnd_Post_Data_Pnd_Full_Name;
    private DbsField pnd_Post_Data_Pnd_Address_Line_Txt;
    private DbsField pnd_Post_Data_Pnd_Postal_Data;
    private DbsField pnd_Post_Data_Pnd_Address_Type_Cde;
    private DbsField pnd_Post_Data_Pnd_Test_Code;
    private DbsField pnd_Post_Data_Pnd_Package_Code;
    private DbsField pnd_Post_Data_Pnd_Letter_Type;
    private DbsField pnd_Post_Data_Pnd_Ctr_Combo;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_ComboRedef1;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gra_Tiaa;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_Gra_TiaaRedef2;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gra_Tiaa_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gra_Cref;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_Gra_CrefRedef3;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gra_Cref_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gsra_Tiaa;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_Gsra_TiaaRedef4;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gsra_Tiaa_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gsra_Cref;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_Gsra_CrefRedef5;
    private DbsField pnd_Post_Data_Pnd_Ctr_Gsra_Cref_A;
    private DbsField pnd_Post_Data_Pnd_Rider_Date;
    private DbsGroup pnd_Post_Data_Pnd_Rider_DateRedef6;
    private DbsField pnd_Post_Data_Pnd_Rider_Month;
    private DbsField pnd_Post_Data_Pnd_Rider_Year;
    private DbsGroup pnd_Post_Data_Pnd_Rider_YearRedef7;
    private DbsField pnd_Post_Data_Pnd_Rider_Year_A;
    private DbsField pnd_Post_Data_Pnd_Tiaa_Contracts;
    private DbsField pnd_Post_Data_Pnd_Cref_Contracts;
    private DbsField pnd_Post_Data_Pnd_Institution_Name;

    public DbsGroup getPnd_Post_Data() { return pnd_Post_Data; }

    public DbsField getPnd_Post_Data_Pnd_Pst_Rqst_Id() { return pnd_Post_Data_Pnd_Pst_Rqst_Id; }

    public DbsField getPnd_Post_Data_Pnd_Pin_Number() { return pnd_Post_Data_Pnd_Pin_Number; }

    public DbsField getPnd_Post_Data_Pnd_Full_Name() { return pnd_Post_Data_Pnd_Full_Name; }

    public DbsField getPnd_Post_Data_Pnd_Address_Line_Txt() { return pnd_Post_Data_Pnd_Address_Line_Txt; }

    public DbsField getPnd_Post_Data_Pnd_Postal_Data() { return pnd_Post_Data_Pnd_Postal_Data; }

    public DbsField getPnd_Post_Data_Pnd_Address_Type_Cde() { return pnd_Post_Data_Pnd_Address_Type_Cde; }

    public DbsField getPnd_Post_Data_Pnd_Test_Code() { return pnd_Post_Data_Pnd_Test_Code; }

    public DbsField getPnd_Post_Data_Pnd_Package_Code() { return pnd_Post_Data_Pnd_Package_Code; }

    public DbsField getPnd_Post_Data_Pnd_Letter_Type() { return pnd_Post_Data_Pnd_Letter_Type; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Combo() { return pnd_Post_Data_Pnd_Ctr_Combo; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_ComboRedef1() { return pnd_Post_Data_Pnd_Ctr_ComboRedef1; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gra_Tiaa() { return pnd_Post_Data_Pnd_Ctr_Gra_Tiaa; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_Gra_TiaaRedef2() { return pnd_Post_Data_Pnd_Ctr_Gra_TiaaRedef2; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gra_Tiaa_A() { return pnd_Post_Data_Pnd_Ctr_Gra_Tiaa_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gra_Cref() { return pnd_Post_Data_Pnd_Ctr_Gra_Cref; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_Gra_CrefRedef3() { return pnd_Post_Data_Pnd_Ctr_Gra_CrefRedef3; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gra_Cref_A() { return pnd_Post_Data_Pnd_Ctr_Gra_Cref_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gsra_Tiaa() { return pnd_Post_Data_Pnd_Ctr_Gsra_Tiaa; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_Gsra_TiaaRedef4() { return pnd_Post_Data_Pnd_Ctr_Gsra_TiaaRedef4; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gsra_Tiaa_A() { return pnd_Post_Data_Pnd_Ctr_Gsra_Tiaa_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gsra_Cref() { return pnd_Post_Data_Pnd_Ctr_Gsra_Cref; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_Gsra_CrefRedef5() { return pnd_Post_Data_Pnd_Ctr_Gsra_CrefRedef5; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Gsra_Cref_A() { return pnd_Post_Data_Pnd_Ctr_Gsra_Cref_A; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Date() { return pnd_Post_Data_Pnd_Rider_Date; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_DateRedef6() { return pnd_Post_Data_Pnd_Rider_DateRedef6; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Month() { return pnd_Post_Data_Pnd_Rider_Month; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year() { return pnd_Post_Data_Pnd_Rider_Year; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_YearRedef7() { return pnd_Post_Data_Pnd_Rider_YearRedef7; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year_A() { return pnd_Post_Data_Pnd_Rider_Year_A; }

    public DbsField getPnd_Post_Data_Pnd_Tiaa_Contracts() { return pnd_Post_Data_Pnd_Tiaa_Contracts; }

    public DbsField getPnd_Post_Data_Pnd_Cref_Contracts() { return pnd_Post_Data_Pnd_Cref_Contracts; }

    public DbsField getPnd_Post_Data_Pnd_Institution_Name() { return pnd_Post_Data_Pnd_Institution_Name; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Post_Data = newGroupInRecord("pnd_Post_Data", "#POST-DATA");
        pnd_Post_Data_Pnd_Pst_Rqst_Id = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pst_Rqst_Id", "#PST-RQST-ID", FieldType.STRING, 11);
        pnd_Post_Data_Pnd_Pin_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);
        pnd_Post_Data_Pnd_Full_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Post_Data_Pnd_Address_Line_Txt = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        pnd_Post_Data_Pnd_Postal_Data = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 32);
        pnd_Post_Data_Pnd_Address_Type_Cde = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Test_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Test_Code", "#TEST-CODE", FieldType.STRING, 5);
        pnd_Post_Data_Pnd_Package_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Post_Data_Pnd_Letter_Type = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Ctr_Combo = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Combo", "#CTR-COMBO", FieldType.STRING, 4);
        pnd_Post_Data_Pnd_Ctr_ComboRedef1 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_ComboRedef1", "Redefines", pnd_Post_Data_Pnd_Ctr_Combo);
        pnd_Post_Data_Pnd_Ctr_Gra_Tiaa = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gra_Tiaa", "#CTR-GRA-TIAA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gra_TiaaRedef2 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_Gra_TiaaRedef2", "Redefines", 
            pnd_Post_Data_Pnd_Ctr_Gra_Tiaa);
        pnd_Post_Data_Pnd_Ctr_Gra_Tiaa_A = pnd_Post_Data_Pnd_Ctr_Gra_TiaaRedef2.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gra_Tiaa_A", "#CTR-GRA-TIAA-A", 
            FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Ctr_Gra_Cref = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gra_Cref", "#CTR-GRA-CREF", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gra_CrefRedef3 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_Gra_CrefRedef3", "Redefines", 
            pnd_Post_Data_Pnd_Ctr_Gra_Cref);
        pnd_Post_Data_Pnd_Ctr_Gra_Cref_A = pnd_Post_Data_Pnd_Ctr_Gra_CrefRedef3.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gra_Cref_A", "#CTR-GRA-CREF-A", 
            FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Ctr_Gsra_Tiaa = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gsra_Tiaa", "#CTR-GSRA-TIAA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gsra_TiaaRedef4 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_Gsra_TiaaRedef4", "Redefines", 
            pnd_Post_Data_Pnd_Ctr_Gsra_Tiaa);
        pnd_Post_Data_Pnd_Ctr_Gsra_Tiaa_A = pnd_Post_Data_Pnd_Ctr_Gsra_TiaaRedef4.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gsra_Tiaa_A", "#CTR-GSRA-TIAA-A", 
            FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Ctr_Gsra_Cref = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gsra_Cref", "#CTR-GSRA-CREF", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Gsra_CrefRedef5 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_Gsra_CrefRedef5", "Redefines", 
            pnd_Post_Data_Pnd_Ctr_Gsra_Cref);
        pnd_Post_Data_Pnd_Ctr_Gsra_Cref_A = pnd_Post_Data_Pnd_Ctr_Gsra_CrefRedef5.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Gsra_Cref_A", "#CTR-GSRA-CREF-A", 
            FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Rider_Date = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Date", "#RIDER-DATE", FieldType.NUMERIC, 6);
        pnd_Post_Data_Pnd_Rider_DateRedef6 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Rider_DateRedef6", "Redefines", pnd_Post_Data_Pnd_Rider_Date);
        pnd_Post_Data_Pnd_Rider_Month = pnd_Post_Data_Pnd_Rider_DateRedef6.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Month", "#RIDER-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Post_Data_Pnd_Rider_Year = pnd_Post_Data_Pnd_Rider_DateRedef6.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year", "#RIDER-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Post_Data_Pnd_Rider_YearRedef7 = pnd_Post_Data_Pnd_Rider_DateRedef6.newGroupInGroup("pnd_Post_Data_Pnd_Rider_YearRedef7", "Redefines", pnd_Post_Data_Pnd_Rider_Year);
        pnd_Post_Data_Pnd_Rider_Year_A = pnd_Post_Data_Pnd_Rider_YearRedef7.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year_A", "#RIDER-YEAR-A", FieldType.STRING, 
            4);
        pnd_Post_Data_Pnd_Tiaa_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Tiaa_Contracts", "#TIAA-CONTRACTS", FieldType.STRING, 
            8, new DbsArrayController(1,20));
        pnd_Post_Data_Pnd_Cref_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Cref_Contracts", "#CREF-CONTRACTS", FieldType.STRING, 
            8, new DbsArrayController(1,20));
        pnd_Post_Data_Pnd_Institution_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 
            120);

        this.setRecordName("LdaRidl482");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaRidl482() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
