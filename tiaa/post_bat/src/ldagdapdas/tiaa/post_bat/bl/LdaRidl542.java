/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:11:39 PM
**        * FROM NATURAL LDA     : RIDL542
************************************************************
**        * FILE NAME            : LdaRidl542.java
**        * CLASS NAME           : LdaRidl542
**        * INSTANCE NAME        : LdaRidl542
************************************************************ */

package tiaa.post_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaRidl542 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Post_Data;
    private DbsField pnd_Post_Data_Pnd_Pst_Rqst_Id;
    private DbsField pnd_Post_Data_Pnd_Pin_Number;
    private DbsField pnd_Post_Data_Pnd_Full_Name;
    private DbsField pnd_Post_Data_Pnd_Address_Line_Txt;
    private DbsField pnd_Post_Data_Pnd_Postal_Data;
    private DbsField pnd_Post_Data_Pnd_Address_Type_Cde;
    private DbsField pnd_Post_Data_Pnd_Test_Code;
    private DbsField pnd_Post_Data_Pnd_Package_Code;
    private DbsField pnd_Post_Data_Pnd_Letter_Type;
    private DbsField pnd_Post_Data_Pnd_Inst_Sv_Lob;
    private DbsField pnd_Post_Data_Pnd_Institution_Name;
    private DbsField pnd_Post_Data_Pnd_Ctr_Combo;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_ComboRedef1;
    private DbsField pnd_Post_Data_Pnd_Ctr_Tiaa_Ra;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_Tiaa_RaRedef2;
    private DbsField pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_RcRedef3;
    private DbsField pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_NhRedef4;
    private DbsField pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_NhRedef5;
    private DbsField pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cref;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_CrefRedef6;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cref_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cre_Il;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_Cre_IlRedef7;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cref_Il_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_Cref_Ra_FlRedef8;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_FlRedef9;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Gn;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Ca;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Ok;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Nc;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_La;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Mn;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Mt;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Ne;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Va;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Nh;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Fl;
    private DbsField pnd_Post_Data_Pnd_Ctr_Oregon;
    private DbsGroup pnd_Post_Data_Pnd_Ctr_OregonRedef10;
    private DbsField pnd_Post_Data_Pnd_Ctr_Oregon_A;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Or;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Nm;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Ny;
    private DbsField pnd_Post_Data_Pnd_Ctr_Cert_Pr;
    private DbsField pnd_Post_Data_Pnd_Rider_Date;
    private DbsGroup pnd_Post_Data_Pnd_Rider_DateRedef11;
    private DbsField pnd_Post_Data_Pnd_Rider_Month;
    private DbsField pnd_Post_Data_Pnd_Rider_Year;
    private DbsGroup pnd_Post_Data_Pnd_Rider_YearRedef12;
    private DbsField pnd_Post_Data_Pnd_Rider_Year_A;
    private DbsField pnd_Post_Data_Pnd_Contracts;

    public DbsGroup getPnd_Post_Data() { return pnd_Post_Data; }

    public DbsField getPnd_Post_Data_Pnd_Pst_Rqst_Id() { return pnd_Post_Data_Pnd_Pst_Rqst_Id; }

    public DbsField getPnd_Post_Data_Pnd_Pin_Number() { return pnd_Post_Data_Pnd_Pin_Number; }

    public DbsField getPnd_Post_Data_Pnd_Full_Name() { return pnd_Post_Data_Pnd_Full_Name; }

    public DbsField getPnd_Post_Data_Pnd_Address_Line_Txt() { return pnd_Post_Data_Pnd_Address_Line_Txt; }

    public DbsField getPnd_Post_Data_Pnd_Postal_Data() { return pnd_Post_Data_Pnd_Postal_Data; }

    public DbsField getPnd_Post_Data_Pnd_Address_Type_Cde() { return pnd_Post_Data_Pnd_Address_Type_Cde; }

    public DbsField getPnd_Post_Data_Pnd_Test_Code() { return pnd_Post_Data_Pnd_Test_Code; }

    public DbsField getPnd_Post_Data_Pnd_Package_Code() { return pnd_Post_Data_Pnd_Package_Code; }

    public DbsField getPnd_Post_Data_Pnd_Letter_Type() { return pnd_Post_Data_Pnd_Letter_Type; }

    public DbsField getPnd_Post_Data_Pnd_Inst_Sv_Lob() { return pnd_Post_Data_Pnd_Inst_Sv_Lob; }

    public DbsField getPnd_Post_Data_Pnd_Institution_Name() { return pnd_Post_Data_Pnd_Institution_Name; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Combo() { return pnd_Post_Data_Pnd_Ctr_Combo; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_ComboRedef1() { return pnd_Post_Data_Pnd_Ctr_ComboRedef1; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Tiaa_Ra() { return pnd_Post_Data_Pnd_Ctr_Tiaa_Ra; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_Tiaa_RaRedef2() { return pnd_Post_Data_Pnd_Ctr_Tiaa_RaRedef2; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Tiaa_Ra_A() { return pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc() { return pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_Tiaa_Gra_RcRedef3() { return pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_RcRedef3; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_A() { return pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh() { return pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_Tiaa_Ra_NhRedef4() { return pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_NhRedef4; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh_A() { return pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh() { return pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_NhRedef5() { return pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_NhRedef5; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A() { return pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cref() { return pnd_Post_Data_Pnd_Ctr_Cref; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_CrefRedef6() { return pnd_Post_Data_Pnd_Ctr_CrefRedef6; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cref_A() { return pnd_Post_Data_Pnd_Ctr_Cref_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cre_Il() { return pnd_Post_Data_Pnd_Ctr_Cre_Il; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_Cre_IlRedef7() { return pnd_Post_Data_Pnd_Ctr_Cre_IlRedef7; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cref_Il_A() { return pnd_Post_Data_Pnd_Ctr_Cref_Il_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl() { return pnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_Cref_Ra_FlRedef8() { return pnd_Post_Data_Pnd_Ctr_Cref_Ra_FlRedef8; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl_A() { return pnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl() { return pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_FlRedef9() { return pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_FlRedef9; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl_A() { return pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Gn() { return pnd_Post_Data_Pnd_Ctr_Cert_Gn; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Ca() { return pnd_Post_Data_Pnd_Ctr_Cert_Ca; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Ok() { return pnd_Post_Data_Pnd_Ctr_Cert_Ok; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Nc() { return pnd_Post_Data_Pnd_Ctr_Cert_Nc; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_La() { return pnd_Post_Data_Pnd_Ctr_Cert_La; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Mn() { return pnd_Post_Data_Pnd_Ctr_Cert_Mn; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Mt() { return pnd_Post_Data_Pnd_Ctr_Cert_Mt; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Ne() { return pnd_Post_Data_Pnd_Ctr_Cert_Ne; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Va() { return pnd_Post_Data_Pnd_Ctr_Cert_Va; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Nh() { return pnd_Post_Data_Pnd_Ctr_Cert_Nh; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Fl() { return pnd_Post_Data_Pnd_Ctr_Cert_Fl; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Oregon() { return pnd_Post_Data_Pnd_Ctr_Oregon; }

    public DbsGroup getPnd_Post_Data_Pnd_Ctr_OregonRedef10() { return pnd_Post_Data_Pnd_Ctr_OregonRedef10; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Oregon_A() { return pnd_Post_Data_Pnd_Ctr_Oregon_A; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Or() { return pnd_Post_Data_Pnd_Ctr_Cert_Or; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Nm() { return pnd_Post_Data_Pnd_Ctr_Cert_Nm; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Ny() { return pnd_Post_Data_Pnd_Ctr_Cert_Ny; }

    public DbsField getPnd_Post_Data_Pnd_Ctr_Cert_Pr() { return pnd_Post_Data_Pnd_Ctr_Cert_Pr; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Date() { return pnd_Post_Data_Pnd_Rider_Date; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_DateRedef11() { return pnd_Post_Data_Pnd_Rider_DateRedef11; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Month() { return pnd_Post_Data_Pnd_Rider_Month; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year() { return pnd_Post_Data_Pnd_Rider_Year; }

    public DbsGroup getPnd_Post_Data_Pnd_Rider_YearRedef12() { return pnd_Post_Data_Pnd_Rider_YearRedef12; }

    public DbsField getPnd_Post_Data_Pnd_Rider_Year_A() { return pnd_Post_Data_Pnd_Rider_Year_A; }

    public DbsField getPnd_Post_Data_Pnd_Contracts() { return pnd_Post_Data_Pnd_Contracts; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Post_Data = newGroupInRecord("pnd_Post_Data", "#POST-DATA");
        pnd_Post_Data_Pnd_Pst_Rqst_Id = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pst_Rqst_Id", "#PST-RQST-ID", FieldType.STRING, 11);
        pnd_Post_Data_Pnd_Pin_Number = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);
        pnd_Post_Data_Pnd_Full_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_Post_Data_Pnd_Address_Line_Txt = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Address_Line_Txt", "#ADDRESS-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        pnd_Post_Data_Pnd_Postal_Data = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Postal_Data", "#POSTAL-DATA", FieldType.STRING, 32);
        pnd_Post_Data_Pnd_Address_Type_Cde = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Address_Type_Cde", "#ADDRESS-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Test_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Test_Code", "#TEST-CODE", FieldType.STRING, 5);
        pnd_Post_Data_Pnd_Package_Code = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Package_Code", "#PACKAGE-CODE", FieldType.STRING, 8);
        pnd_Post_Data_Pnd_Letter_Type = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Letter_Type", "#LETTER-TYPE", FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Inst_Sv_Lob = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Inst_Sv_Lob", "#INST-SV-LOB", FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Institution_Name = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Institution_Name", "#INSTITUTION-NAME", FieldType.STRING, 
            120);
        pnd_Post_Data_Pnd_Ctr_Combo = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Combo", "#CTR-COMBO", FieldType.STRING, 24);
        pnd_Post_Data_Pnd_Ctr_ComboRedef1 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_ComboRedef1", "Redefines", pnd_Post_Data_Pnd_Ctr_Combo);
        pnd_Post_Data_Pnd_Ctr_Tiaa_Ra = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_Ra", "#CTR-TIAA-RA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Tiaa_RaRedef2 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_RaRedef2", "Redefines", pnd_Post_Data_Pnd_Ctr_Tiaa_Ra);
        pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_A = pnd_Post_Data_Pnd_Ctr_Tiaa_RaRedef2.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_A", "#CTR-TIAA-RA-A", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc", "#CTR-TIAA-GRA-RC", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_RcRedef3 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_RcRedef3", "Redefines", 
            pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc);
        pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_A = pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_RcRedef3.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_A", "#CTR-TIAA-GRA-RC-A", 
            FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh", "#CTR-TIAA-RA-NH", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_NhRedef4 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_NhRedef4", "Redefines", 
            pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh);
        pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh_A = pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_NhRedef4.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_Ra_Nh_A", "#CTR-TIAA-RA-NH-A", 
            FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh", "#CTR-TIAA-GRA-RC-NH", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_NhRedef5 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_NhRedef5", "Redefines", 
            pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh);
        pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A = pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_NhRedef5.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Tiaa_Gra_Rc_Nh_A", 
            "#CTR-TIAA-GRA-RC-NH-A", FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Ctr_Cref = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cref", "#CTR-CREF", FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Ctr_CrefRedef6 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_CrefRedef6", "Redefines", pnd_Post_Data_Pnd_Ctr_Cref);
        pnd_Post_Data_Pnd_Ctr_Cref_A = pnd_Post_Data_Pnd_Ctr_CrefRedef6.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cref_A", "#CTR-CREF-A", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cre_Il = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cre_Il", "#CTR-CRE-IL", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cre_IlRedef7 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_Cre_IlRedef7", "Redefines", pnd_Post_Data_Pnd_Ctr_Cre_Il);
        pnd_Post_Data_Pnd_Ctr_Cref_Il_A = pnd_Post_Data_Pnd_Ctr_Cre_IlRedef7.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cref_Il_A", "#CTR-CREF-IL-A", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl", "#CTR-CREF-RA-FL", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cref_Ra_FlRedef8 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_Cref_Ra_FlRedef8", "Redefines", 
            pnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl);
        pnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl_A = pnd_Post_Data_Pnd_Ctr_Cref_Ra_FlRedef8.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cref_Ra_Fl_A", "#CTR-CREF-RA-FL-A", 
            FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl", "#CTR-CREF-GRA-RC-FL", 
            FieldType.NUMERIC, 1);
        pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_FlRedef9 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_FlRedef9", "Redefines", 
            pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl);
        pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl_A = pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_FlRedef9.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cref_Gra_Rc_Fl_A", 
            "#CTR-CREF-GRA-RC-FL-A", FieldType.STRING, 1);
        pnd_Post_Data_Pnd_Ctr_Cert_Gn = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Gn", "#CTR-CERT-GN", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Ca = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Ca", "#CTR-CERT-CA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Ok = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Ok", "#CTR-CERT-OK", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Nc = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Nc", "#CTR-CERT-NC", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_La = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_La", "#CTR-CERT-LA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Mn = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Mn", "#CTR-CERT-MN", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Mt = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Mt", "#CTR-CERT-MT", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Ne = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Ne", "#CTR-CERT-NE", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Va = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Va", "#CTR-CERT-VA", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Nh = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Nh", "#CTR-CERT-NH", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Fl = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Fl", "#CTR-CERT-FL", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Oregon = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Oregon", "#CTR-OREGON", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_OregonRedef10 = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newGroupInGroup("pnd_Post_Data_Pnd_Ctr_OregonRedef10", "Redefines", pnd_Post_Data_Pnd_Ctr_Oregon);
        pnd_Post_Data_Pnd_Ctr_Oregon_A = pnd_Post_Data_Pnd_Ctr_OregonRedef10.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Oregon_A", "#CTR-OREGON-A", FieldType.STRING, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Or = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Or", "#CTR-CERT-OR", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Nm = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Nm", "#CTR-CERT-NM", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Ny = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Ny", "#CTR-CERT-NY", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Ctr_Cert_Pr = pnd_Post_Data_Pnd_Ctr_ComboRedef1.newFieldInGroup("pnd_Post_Data_Pnd_Ctr_Cert_Pr", "#CTR-CERT-PR", FieldType.NUMERIC, 
            1);
        pnd_Post_Data_Pnd_Rider_Date = pnd_Post_Data.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Date", "#RIDER-DATE", FieldType.NUMERIC, 6);
        pnd_Post_Data_Pnd_Rider_DateRedef11 = pnd_Post_Data.newGroupInGroup("pnd_Post_Data_Pnd_Rider_DateRedef11", "Redefines", pnd_Post_Data_Pnd_Rider_Date);
        pnd_Post_Data_Pnd_Rider_Month = pnd_Post_Data_Pnd_Rider_DateRedef11.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Month", "#RIDER-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Post_Data_Pnd_Rider_Year = pnd_Post_Data_Pnd_Rider_DateRedef11.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year", "#RIDER-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Post_Data_Pnd_Rider_YearRedef12 = pnd_Post_Data_Pnd_Rider_DateRedef11.newGroupInGroup("pnd_Post_Data_Pnd_Rider_YearRedef12", "Redefines", 
            pnd_Post_Data_Pnd_Rider_Year);
        pnd_Post_Data_Pnd_Rider_Year_A = pnd_Post_Data_Pnd_Rider_YearRedef12.newFieldInGroup("pnd_Post_Data_Pnd_Rider_Year_A", "#RIDER-YEAR-A", FieldType.STRING, 
            4);
        pnd_Post_Data_Pnd_Contracts = pnd_Post_Data.newFieldArrayInGroup("pnd_Post_Data_Pnd_Contracts", "#CONTRACTS", FieldType.STRING, 17, new DbsArrayController(1,
            20));

        this.setRecordName("LdaRidl542");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaRidl542() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
