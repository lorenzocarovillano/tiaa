/*******************************************************************************
 * Confidential Information - Limited distribution to authorized persons only.
 * This software is protected as an unpublished work under the U.S. Copyright
 * Act of 1976.
 *
 * Copyright (c) 2004-2018, ModSys International Ltd. All rights reserved.
 *******************************************************************************/

package com.bphx.dsl;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

public final class IdCamsErrors {
    public static final IdCamsErrors OK = new IdCamsErrors(0); // command executed with no errors
    public static final IdCamsErrors WARNING =new IdCamsErrors(4); //- execution may go successful
    public static final IdCamsErrors ERROR_MAY_FAIL = new IdCamsErrors(8); //serious error - execution may fail
    public static final IdCamsErrors ERROR_EXEC_IMPOSSIBLE =new IdCamsErrors(12); //serious error - execution impossible
    public static final IdCamsErrors FATAL = new IdCamsErrors(16);//job step terminates
    
    //@formatter:off
    private static ImmutableList<IdCamsErrors> values =ImmutableList.<IdCamsErrors>builder()
    		.add(OK)
    		.add(new IdCamsErrors(1))
    		.add(new IdCamsErrors(2))
    		.add(new IdCamsErrors(3))
    		.add(WARNING)
    		.add(new IdCamsErrors(5))
    		.add(new IdCamsErrors(6))
    		.add(new IdCamsErrors(7))
    		.add(ERROR_MAY_FAIL)
    		.add(new IdCamsErrors(9))
    		.add(new IdCamsErrors(10))
    		.add(new IdCamsErrors(11))
    		.add(ERROR_EXEC_IMPOSSIBLE)
    		.add(new IdCamsErrors(13))
    		.add(new IdCamsErrors(14))
    		.add(new IdCamsErrors(15))
    		.add(FATAL)
    		.build();
    //@formatter:on
    private int value;

    private IdCamsErrors(){}
    private IdCamsErrors(int value) {
    	if( value == 4 ) value=0;
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    
    public static IdCamsErrors get(int value){
    	Preconditions.checkState(value>=0);
    	if(value>=16){
    		return values.get(16);
    	}else{
    		return values.get(value);
    	}
    }
    
    
    
    
}
