/*******************************************************************************
 * Confidential Information - Limited distribution to authorized persons only.
 * This software is protected as an unpublished work under the U.S. Copyright
 * Act of 1976.
 *
 * Copyright (c) 2004-2018, ModSys International Ltd. All rights reserved.
 *******************************************************************************/

package bphx.batch.log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Properties;

import com.bphx.ctu.core.data.util.FixedStrings;
import com.google.common.base.Strings;

import bphx.batch.batchlets.ReturnCode;
import bphx.batch.context.ReturnCodeInfos;
import bphx.batch.step.DDCardInfo;
import bphx.batch.step.EFileFormat;
import bphx.batch.step.RecordInfo;
import bphx.batch.step.SpecialProperty;

public abstract class BaseJobLogger implements JobLogger {

	protected abstract void write(String text);

	@Override
	public void properties(Properties props, Properties jobProps) {
        Enumeration<?> names = jobProps.propertyNames();
        while(names.hasMoreElements()) {
        	String prop = (String) names.nextElement();
        	if(!SpecialProperty.isSpecialProperty(prop)) {
        		String dflt = jobProps.getProperty(prop);
        		String actual = props.getProperty(prop);
        		if(dflt.equals(actual)) {
        			write("//    "+prop+"="+actual);
        		} else {
        			write("//    "+prop+"="+actual+ "    (default="+dflt+")");
        		}
        	}
        }
	}

	@Override
	public void jobStart(String jobName) {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    String strDate = sdf.format(cal.getTime());
    write(strDate + "//" + jobName + " JOB");
	}

	@Override
	public void jobEnd(String jobName, ReturnCodeInfos result, long duration) {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    String strDate = sdf.format(cal.getTime());
    write(strDate + "//" + jobName + " JOB ENDED  STATUS: " + (result.isAbend() ? "ABEND" : "OK") + " - RETURN CODE: " + result
        
        .getMaxCode() + " - ELAPSED TIME: " + BatchLogUtils.formatDuration(duration));
	}

	@Override
	public void procStart(String jobName, String procName) {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    String strDate = sdf.format(cal.getTime());
    write(System.lineSeparator() + strDate + "//" + jobName + " EXEC " + procName);
	}

	@Override
	public void startStep(String step, String pgm, String param) {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    String strDate = sdf.format(cal.getTime());
    String message = strDate + "=====STEP START: " + step + " PROGRAM=" + pgm;
    if (!Strings.isNullOrEmpty(param))
      message = message + " PARM=\"" + param + "\""; 
    	write(System.lineSeparator()+message);
	}

	@Override
	public void skipStep(String step) {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    String strDate = sdf.format(cal.getTime());
    write(strDate + "=====STEP SKIPPED: " + step);
	}

	@Override
	public void endStep(String step, ReturnCode result, long duration) {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    String strDate = sdf.format(cal.getTime());
    write(strDate + "=====STEP END: " + step + " STATUS: " + success(result) + " - RETURN CODE: " + result
        .getVal() + " - ELAPSED TIME: " + BatchLogUtils.formatDuration(duration));
	}

    private String success(ReturnCode rc) {
    	if(rc.isAbend()) {
    		return "ABEND";
    	}
    	return "OK";
    }

    @Override
    public void ddCard(DDCardInfo dci) {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    String strDate = sdf.format(cal.getTime());
    String prefix = strDate + "===" + dci.getDdName() + "=";
		if(dci.isConcatenated()) {
			boolean first=true;
			String plus = FixedStrings.get("", prefix.length()-1)+"+";
			for(DDCardInfo cdci:dci.getConcat()) {
				writeDdMapping(first?prefix:plus, cdci);
				first=false;
			}
		} else {
			writeDdMapping(prefix, dci);
		}
    }

    private void writeDdMapping(String prefix, DDCardInfo dci) {
        RecordInfo ri = dci.getRecordInfo();
		String absoluteFile = ri.getAbsoluteFile();
        if (absoluteFile == null && dci.getInlineData() != null) {
            absoluteFile = dci.getInlineDsn();
        }

		StringBuilder ddMapping = new StringBuilder(prefix);
		ddMapping.append(absoluteFile);
		ddMapping.append("  RECFM=").append(ri.getFormat().getVal());
		if (EFileFormat.LS != ri.getFormat()) {
			ddMapping.append("  LRECL=").append(ri.getReclen());
		}
		write(ddMapping.toString());
    }
}
