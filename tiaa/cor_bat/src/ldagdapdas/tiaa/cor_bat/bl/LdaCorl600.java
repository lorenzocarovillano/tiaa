/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:01 PM
**        * FROM NATURAL LDA     : CORL600
************************************************************
**        * FILE NAME            : LdaCorl600.java
**        * CLASS NAME           : LdaCorl600
**        * INSTANCE NAME        : LdaCorl600
************************************************************ */

package tiaa.cor_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCorl600 extends DbsRecord
{
    // Properties
    private DbsGroup sort_Key;
    private DbsField sort_Key_Sort_Key_Lgth;
    private DbsField sort_Key_Sort_Key_Array;
    private DbsGroup corl600_Data;
    private DbsField corl600_Data_Corl600_Rec_Id;
    private DbsField corl600_Data_Corl600_Data_Lgth;
    private DbsField corl600_Data_Corl600_Data_Occurs;
    private DbsField corl600_Data_Corl600_Data_Array;
    private DbsGroup corl600_Data_Corl600_Data_ArrayRedef1;
    private DbsGroup corl600_Data_Cor_Change_Data;
    private DbsField corl600_Data_Name_Prefix;
    private DbsField corl600_Data_Name_Last;
    private DbsField corl600_Data_Name_First;
    private DbsField corl600_Data_Name_Middle;
    private DbsField corl600_Data_Name_Suffix;
    private DbsField corl600_Data_Ssn;
    private DbsField corl600_Data_Date_Of_Birth;
    private DbsField corl600_Data_Name_Prefix_Old;
    private DbsField corl600_Data_Name_Last_Old;
    private DbsField corl600_Data_Name_First_Old;
    private DbsField corl600_Data_Name_Middle_Old;
    private DbsField corl600_Data_Name_Suffix_Old;
    private DbsField corl600_Data_Ssn_Old;
    private DbsField corl600_Data_Date_Of_Birth_Old;
    private DbsField corl600_Data_Name_Corr_Flag;

    public DbsGroup getSort_Key() { return sort_Key; }

    public DbsField getSort_Key_Sort_Key_Lgth() { return sort_Key_Sort_Key_Lgth; }

    public DbsField getSort_Key_Sort_Key_Array() { return sort_Key_Sort_Key_Array; }

    public DbsGroup getCorl600_Data() { return corl600_Data; }

    public DbsField getCorl600_Data_Corl600_Rec_Id() { return corl600_Data_Corl600_Rec_Id; }

    public DbsField getCorl600_Data_Corl600_Data_Lgth() { return corl600_Data_Corl600_Data_Lgth; }

    public DbsField getCorl600_Data_Corl600_Data_Occurs() { return corl600_Data_Corl600_Data_Occurs; }

    public DbsField getCorl600_Data_Corl600_Data_Array() { return corl600_Data_Corl600_Data_Array; }

    public DbsGroup getCorl600_Data_Corl600_Data_ArrayRedef1() { return corl600_Data_Corl600_Data_ArrayRedef1; }

    public DbsGroup getCorl600_Data_Cor_Change_Data() { return corl600_Data_Cor_Change_Data; }

    public DbsField getCorl600_Data_Name_Prefix() { return corl600_Data_Name_Prefix; }

    public DbsField getCorl600_Data_Name_Last() { return corl600_Data_Name_Last; }

    public DbsField getCorl600_Data_Name_First() { return corl600_Data_Name_First; }

    public DbsField getCorl600_Data_Name_Middle() { return corl600_Data_Name_Middle; }

    public DbsField getCorl600_Data_Name_Suffix() { return corl600_Data_Name_Suffix; }

    public DbsField getCorl600_Data_Ssn() { return corl600_Data_Ssn; }

    public DbsField getCorl600_Data_Date_Of_Birth() { return corl600_Data_Date_Of_Birth; }

    public DbsField getCorl600_Data_Name_Prefix_Old() { return corl600_Data_Name_Prefix_Old; }

    public DbsField getCorl600_Data_Name_Last_Old() { return corl600_Data_Name_Last_Old; }

    public DbsField getCorl600_Data_Name_First_Old() { return corl600_Data_Name_First_Old; }

    public DbsField getCorl600_Data_Name_Middle_Old() { return corl600_Data_Name_Middle_Old; }

    public DbsField getCorl600_Data_Name_Suffix_Old() { return corl600_Data_Name_Suffix_Old; }

    public DbsField getCorl600_Data_Ssn_Old() { return corl600_Data_Ssn_Old; }

    public DbsField getCorl600_Data_Date_Of_Birth_Old() { return corl600_Data_Date_Of_Birth_Old; }

    public DbsField getCorl600_Data_Name_Corr_Flag() { return corl600_Data_Name_Corr_Flag; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        sort_Key = newGroupInRecord("sort_Key", "SORT-KEY");
        sort_Key_Sort_Key_Lgth = sort_Key.newFieldInGroup("sort_Key_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key_Sort_Key_Array = sort_Key.newFieldArrayInGroup("sort_Key_Sort_Key_Array", "SORT-KEY-ARRAY", FieldType.STRING, 1, new DbsArrayController(1,
            1));

        corl600_Data = newGroupInRecord("corl600_Data", "CORL600-DATA");
        corl600_Data_Corl600_Rec_Id = corl600_Data.newFieldInGroup("corl600_Data_Corl600_Rec_Id", "CORL600-REC-ID", FieldType.STRING, 2);
        corl600_Data_Corl600_Data_Lgth = corl600_Data.newFieldInGroup("corl600_Data_Corl600_Data_Lgth", "CORL600-DATA-LGTH", FieldType.NUMERIC, 5);
        corl600_Data_Corl600_Data_Occurs = corl600_Data.newFieldInGroup("corl600_Data_Corl600_Data_Occurs", "CORL600-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        corl600_Data_Corl600_Data_Array = corl600_Data.newFieldArrayInGroup("corl600_Data_Corl600_Data_Array", "CORL600-DATA-ARRAY", FieldType.STRING, 
            1, new DbsArrayController(1,246));
        corl600_Data_Corl600_Data_ArrayRedef1 = corl600_Data.newGroupInGroup("corl600_Data_Corl600_Data_ArrayRedef1", "Redefines", corl600_Data_Corl600_Data_Array);
        corl600_Data_Cor_Change_Data = corl600_Data_Corl600_Data_ArrayRedef1.newGroupInGroup("corl600_Data_Cor_Change_Data", "COR-CHANGE-DATA");
        corl600_Data_Name_Prefix = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Name_Prefix", "NAME-PREFIX", FieldType.STRING, 8);
        corl600_Data_Name_Last = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Name_Last", "NAME-LAST", FieldType.STRING, 30);
        corl600_Data_Name_First = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Name_First", "NAME-FIRST", FieldType.STRING, 30);
        corl600_Data_Name_Middle = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Name_Middle", "NAME-MIDDLE", FieldType.STRING, 30);
        corl600_Data_Name_Suffix = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Name_Suffix", "NAME-SUFFIX", FieldType.STRING, 8);
        corl600_Data_Ssn = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Ssn", "SSN", FieldType.NUMERIC, 9);
        corl600_Data_Date_Of_Birth = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Date_Of_Birth", "DATE-OF-BIRTH", FieldType.DATE);
        corl600_Data_Name_Prefix_Old = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Name_Prefix_Old", "NAME-PREFIX-OLD", FieldType.STRING, 
            8);
        corl600_Data_Name_Last_Old = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Name_Last_Old", "NAME-LAST-OLD", FieldType.STRING, 30);
        corl600_Data_Name_First_Old = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Name_First_Old", "NAME-FIRST-OLD", FieldType.STRING, 
            30);
        corl600_Data_Name_Middle_Old = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Name_Middle_Old", "NAME-MIDDLE-OLD", FieldType.STRING, 
            30);
        corl600_Data_Name_Suffix_Old = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Name_Suffix_Old", "NAME-SUFFIX-OLD", FieldType.STRING, 
            8);
        corl600_Data_Ssn_Old = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Ssn_Old", "SSN-OLD", FieldType.NUMERIC, 9);
        corl600_Data_Date_Of_Birth_Old = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Date_Of_Birth_Old", "DATE-OF-BIRTH-OLD", FieldType.DATE);
        corl600_Data_Name_Corr_Flag = corl600_Data_Cor_Change_Data.newFieldInGroup("corl600_Data_Name_Corr_Flag", "NAME-CORR-FLAG", FieldType.STRING, 
            1);

        this.setRecordName("LdaCorl600");
    }

    public void initializeValues() throws Exception
    {
        reset();
        sort_Key_Sort_Key_Lgth.setInitialValue(1);
        corl600_Data_Corl600_Rec_Id.setInitialValue("a");
        corl600_Data_Corl600_Data_Lgth.setInitialValue(246);
        corl600_Data_Corl600_Data_Occurs.setInitialValue(1);
    }

    // Constructor
    public LdaCorl600() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
