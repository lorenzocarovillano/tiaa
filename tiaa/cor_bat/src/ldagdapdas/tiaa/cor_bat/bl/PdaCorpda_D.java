/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:25 PM
**        * FROM NATURAL PDA     : CORPDA_D
************************************************************
**        * FILE NAME            : PdaCorpda_D.java
**        * CLASS NAME           : PdaCorpda_D
**        * INSTANCE NAME        : PdaCorpda_D
************************************************************ */

package tiaa.cor_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCorpda_D extends PdaBase
{
    // Properties
    private DbsGroup dialog_Info_Sub;
    private DbsField dialog_Info_Sub_Pnd_Pnd_Command;
    private DbsGroup dialog_Info_Sub_Pnd_Pnd_CommandRedef1;
    private DbsField dialog_Info_Sub_Pnd_Pnd_Gda_Command_Char;
    private DbsField dialog_Info_Sub_Pnd_Pnd_Main;
    private DbsField dialog_Info_Sub_Pnd_Pnd_Quit;

    public DbsGroup getDialog_Info_Sub() { return dialog_Info_Sub; }

    public DbsField getDialog_Info_Sub_Pnd_Pnd_Command() { return dialog_Info_Sub_Pnd_Pnd_Command; }

    public DbsGroup getDialog_Info_Sub_Pnd_Pnd_CommandRedef1() { return dialog_Info_Sub_Pnd_Pnd_CommandRedef1; }

    public DbsField getDialog_Info_Sub_Pnd_Pnd_Gda_Command_Char() { return dialog_Info_Sub_Pnd_Pnd_Gda_Command_Char; }

    public DbsField getDialog_Info_Sub_Pnd_Pnd_Main() { return dialog_Info_Sub_Pnd_Pnd_Main; }

    public DbsField getDialog_Info_Sub_Pnd_Pnd_Quit() { return dialog_Info_Sub_Pnd_Pnd_Quit; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        dialog_Info_Sub = dbsRecord.newGroupInRecord("dialog_Info_Sub", "DIALOG-INFO-SUB");
        dialog_Info_Sub.setParameterOption(ParameterOption.ByReference);
        dialog_Info_Sub_Pnd_Pnd_Command = dialog_Info_Sub.newFieldInGroup("dialog_Info_Sub_Pnd_Pnd_Command", "##COMMAND", FieldType.STRING, 60);
        dialog_Info_Sub_Pnd_Pnd_CommandRedef1 = dialog_Info_Sub.newGroupInGroup("dialog_Info_Sub_Pnd_Pnd_CommandRedef1", "Redefines", dialog_Info_Sub_Pnd_Pnd_Command);
        dialog_Info_Sub_Pnd_Pnd_Gda_Command_Char = dialog_Info_Sub_Pnd_Pnd_CommandRedef1.newFieldArrayInGroup("dialog_Info_Sub_Pnd_Pnd_Gda_Command_Char", 
            "##GDA-COMMAND-CHAR", FieldType.STRING, 1, new DbsArrayController(1,60));
        dialog_Info_Sub_Pnd_Pnd_Main = dialog_Info_Sub.newFieldInGroup("dialog_Info_Sub_Pnd_Pnd_Main", "##MAIN", FieldType.STRING, 8);
        dialog_Info_Sub_Pnd_Pnd_Quit = dialog_Info_Sub.newFieldInGroup("dialog_Info_Sub_Pnd_Pnd_Quit", "##QUIT", FieldType.STRING, 8);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCorpda_D(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

