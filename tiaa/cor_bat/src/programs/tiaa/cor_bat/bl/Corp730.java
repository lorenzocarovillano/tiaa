/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:23:38 PM
**        * FROM NATURAL PROGRAM : Corp730
************************************************************
**        * FILE NAME            : Corp730.java
**        * CLASS NAME           : Corp730
**        * INSTANCE NAME        : Corp730
************************************************************
************************************************************************
* PROGRAM      : CORP730
* AUTHOR       : MONETTE V. CABIG
* DATE WRITTEN : JULY 29, 2009
* DESCRIPTION  : CREATE LETTER VIA POST. BATCH VERSION OF CORN730 ONLINE
*                PROCESS. INPUT COMING FROM MDM.
* GOPAZ   10/24/2009 CALL CORL600 INSTEAD OF CORA730
* MEADED  1/17 : PIN EXPANSION FROM 7 TO 12 BYTES. INPUT LRECL NOW 292
*                SEE "DM 1/17"
* MEADED  2/17 : MAKE PIN AN ALPHA FIELD AS 7-BYTE PIN's will have
*                TRAILING BLANKS. SEE "DM 2/17"
* MEADED  7/17 : RESTOW FOR PIN EXPANSION CWF/EFS PDA's
* MEADED  9/17 : USE FULL 12-BYTE PIN's. Replace call to PSTN9610 with
*                PSTN9612 AND CALL TO PSTN9680 WITH PSTN9685.
*                SEE '12-byte' COMMENTS.
* MEADED  1/19 : CALL PSTN9685 INSTEAD OF PSTN9680 IN BACKOUT ROUTINE.
*                DISPLAY ERROR MESSAGES FROM CALLED SUBPROGRAMS.
*                SEE DM 1-19
************************************************************************

************************************************************ */

package tiaa.cor_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Corp730 extends BLNatBase
{
    // Data Areas
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaCorpda_M pdaCorpda_M;
    private LdaCorl600 ldaCorl600;
    private PdaCorpda_D pdaCorpda_D;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Post_Cor;

    private DbsGroup pnd_Post_Cor__R_Field_1;
    private DbsField pnd_Post_Cor_Pnd_Pc_Pin;

    private DbsGroup pnd_Post_Cor__R_Field_2;
    private DbsField pnd_Post_Cor_Pnd_Pc_Pin_Num;
    private DbsField pnd_Post_Cor_Pnd_Pc_Name_Prefix;
    private DbsField pnd_Post_Cor_Pnd_Pc_Name_Last;
    private DbsField pnd_Post_Cor_Pnd_Pc_Name_First;
    private DbsField pnd_Post_Cor_Pnd_Pc_Name_Middle;
    private DbsField pnd_Post_Cor_Pnd_Pc_Name_Suffix;
    private DbsField pnd_Post_Cor_Pnd_Pc_Ssn;

    private DbsGroup pnd_Post_Cor__R_Field_3;
    private DbsField pnd_Post_Cor_Pnd_Pc_Ssn_A;
    private DbsField pnd_Post_Cor_Pnd_Pc_Dob;

    private DbsGroup pnd_Post_Cor__R_Field_4;
    private DbsField pnd_Post_Cor_Pnd_L_Dob;
    private DbsField pnd_Post_Cor_Pnd_Pc_Name_Prefix_Old;
    private DbsField pnd_Post_Cor_Pnd_Pc_Name_Last_Old;
    private DbsField pnd_Post_Cor_Pnd_Pc_Name_First_Old;
    private DbsField pnd_Post_Cor_Pnd_Pc_Name_Middle_Old;
    private DbsField pnd_Post_Cor_Pnd_Pc_Name_Suffix_Old;
    private DbsField pnd_Post_Cor_Pnd_Pc_Ssn_Old;

    private DbsGroup pnd_Post_Cor__R_Field_5;
    private DbsField pnd_Post_Cor_Pnd_Pc_Ssn_Old_A;
    private DbsField pnd_Post_Cor_Pnd_Pc_Dob_Old;

    private DbsGroup pnd_Post_Cor__R_Field_6;
    private DbsField pnd_Post_Cor_Pnd_L_Dob_Old;
    private DbsField pnd_Post_Cor_Pnd_Pc_Name_Corr_Flag;
    private DbsField pnd_Post_Cor_Pnd_Pc_Mit_Wpid;
    private DbsField pnd_Post_Cor_Pnd_Pc_Mit_Unit_Cde;
    private DbsField pnd_Post_Cor_Pnd_Pc_Mit_Log_Date_Time;
    private DbsField pnd_Post_Cor_Pnd_Pc_Mit_Status_Cde;

    private DbsGroup cwf_Pass_Sub;
    private DbsField cwf_Pass_Sub_Pnd_Pnd_User;
    private DbsField cwf_Pass_Sub_Pnd_Pnd_Help_Nme;
    private DbsField cwf_Pass_Sub_Pnd_Pnd_Map_Nme;
    private DbsField cwf_Pass_Sub_Pnd_Pnd_Upd_User_Ind;
    private DbsField pnd_Efsn9120_Line;

    private DbsGroup pnd_Efsn9120_Line__R_Field_7;
    private DbsField pnd_Efsn9120_Line_Pnd_Prompt;
    private DbsField pnd_Efsn9120_Line_Pnd_Field1;
    private DbsField pnd_Efsn9120_Line_Pnd_Field2;
    private DbsField pnd_L_Date;

    private DataAccessProgramView vw_restart;
    private DbsField restart_Rst_Actve_Ind;
    private DbsField restart_Rst_Job_Nme;
    private DbsField restart_Rst_Jobstep_Nme;
    private DbsField restart_Rst_Pgm_Nme;
    private DbsField restart_Rst_Curr_Dte;
    private DbsField restart_Rst_Curr_Tme;
    private DbsField restart_Rst_Start_Dte;
    private DbsField restart_Rst_Start_Tme;
    private DbsField restart_Rst_Key;
    private DbsField restart_Rst_Frst_Run_Dte;
    private DbsField restart_Rst_Last_Rstrt_Dte;
    private DbsField restart_Rst_Last_Rstrt_Tme;
    private DbsField pnd_L_Restart_Isn;
    private DbsField pnd_L_Restart_Key;

    private DbsGroup pnd_L_Restart_Key__R_Field_8;
    private DbsField pnd_L_Restart_Key_Pnd_L_Act_Ind;
    private DbsField pnd_L_Restart_Key_Pnd_L_Job_Name;
    private DbsField pnd_L_Restart_Key_Pnd_L_Step_Name;
    private DbsField pnd_L_Restart_Data;

    private DbsGroup pnd_L_Restart_Data__R_Field_9;
    private DbsField pnd_L_Restart_Data_Pnd_L_Restart_Pin;
    private DbsField pnd_L_Restart_Data_Pnd_L_Restart_Ssn;
    private DbsField pnd_L_Et_Data;

    private DbsGroup pnd_L_Et_Data__R_Field_10;
    private DbsField pnd_L_Et_Data_Pnd_L_Et_Pin;
    private DbsField pnd_L_Et_Data_Pnd_L_Et_Ssn;
    private DbsField pnd_L_Ctr;
    private DbsField pnd_L_Bot_Ctr;
    private DbsField pnd_L_Upd_Ctr;
    private DbsField pnd_L_Byp_Ctr;
    private DbsField pnd_L_New_Ssn;
    private DbsField pnd_L_Old_Ssn;
    private DbsField pnd_L_New_Dob;
    private DbsField pnd_L_Old_Dob;
    private DbsField pnd_X;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaCorpda_M = new PdaCorpda_M(localVariables);
        ldaCorl600 = new LdaCorl600();
        registerRecord(ldaCorl600);
        pdaCorpda_D = new PdaCorpda_D(localVariables);

        // Local Variables
        pnd_Post_Cor = localVariables.newFieldInRecord("pnd_Post_Cor", "#POST-COR", FieldType.STRING, 292);

        pnd_Post_Cor__R_Field_1 = localVariables.newGroupInRecord("pnd_Post_Cor__R_Field_1", "REDEFINE", pnd_Post_Cor);
        pnd_Post_Cor_Pnd_Pc_Pin = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Pin", "#PC-PIN", FieldType.STRING, 12);

        pnd_Post_Cor__R_Field_2 = pnd_Post_Cor__R_Field_1.newGroupInGroup("pnd_Post_Cor__R_Field_2", "REDEFINE", pnd_Post_Cor_Pnd_Pc_Pin);
        pnd_Post_Cor_Pnd_Pc_Pin_Num = pnd_Post_Cor__R_Field_2.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Pin_Num", "#PC-PIN-NUM", FieldType.NUMERIC, 12);
        pnd_Post_Cor_Pnd_Pc_Name_Prefix = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Name_Prefix", "#PC-NAME-PREFIX", FieldType.STRING, 
            8);
        pnd_Post_Cor_Pnd_Pc_Name_Last = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Name_Last", "#PC-NAME-LAST", FieldType.STRING, 30);
        pnd_Post_Cor_Pnd_Pc_Name_First = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Name_First", "#PC-NAME-FIRST", FieldType.STRING, 
            30);
        pnd_Post_Cor_Pnd_Pc_Name_Middle = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Name_Middle", "#PC-NAME-MIDDLE", FieldType.STRING, 
            30);
        pnd_Post_Cor_Pnd_Pc_Name_Suffix = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Name_Suffix", "#PC-NAME-SUFFIX", FieldType.STRING, 
            8);
        pnd_Post_Cor_Pnd_Pc_Ssn = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Ssn", "#PC-SSN", FieldType.NUMERIC, 9);

        pnd_Post_Cor__R_Field_3 = pnd_Post_Cor__R_Field_1.newGroupInGroup("pnd_Post_Cor__R_Field_3", "REDEFINE", pnd_Post_Cor_Pnd_Pc_Ssn);
        pnd_Post_Cor_Pnd_Pc_Ssn_A = pnd_Post_Cor__R_Field_3.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Ssn_A", "#PC-SSN-A", FieldType.STRING, 9);
        pnd_Post_Cor_Pnd_Pc_Dob = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Dob", "#PC-DOB", FieldType.NUMERIC, 8);

        pnd_Post_Cor__R_Field_4 = pnd_Post_Cor__R_Field_1.newGroupInGroup("pnd_Post_Cor__R_Field_4", "REDEFINE", pnd_Post_Cor_Pnd_Pc_Dob);
        pnd_Post_Cor_Pnd_L_Dob = pnd_Post_Cor__R_Field_4.newFieldInGroup("pnd_Post_Cor_Pnd_L_Dob", "#L-DOB", FieldType.STRING, 8);
        pnd_Post_Cor_Pnd_Pc_Name_Prefix_Old = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Name_Prefix_Old", "#PC-NAME-PREFIX-OLD", FieldType.STRING, 
            8);
        pnd_Post_Cor_Pnd_Pc_Name_Last_Old = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Name_Last_Old", "#PC-NAME-LAST-OLD", FieldType.STRING, 
            30);
        pnd_Post_Cor_Pnd_Pc_Name_First_Old = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Name_First_Old", "#PC-NAME-FIRST-OLD", FieldType.STRING, 
            30);
        pnd_Post_Cor_Pnd_Pc_Name_Middle_Old = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Name_Middle_Old", "#PC-NAME-MIDDLE-OLD", FieldType.STRING, 
            30);
        pnd_Post_Cor_Pnd_Pc_Name_Suffix_Old = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Name_Suffix_Old", "#PC-NAME-SUFFIX-OLD", FieldType.STRING, 
            8);
        pnd_Post_Cor_Pnd_Pc_Ssn_Old = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Ssn_Old", "#PC-SSN-OLD", FieldType.NUMERIC, 9);

        pnd_Post_Cor__R_Field_5 = pnd_Post_Cor__R_Field_1.newGroupInGroup("pnd_Post_Cor__R_Field_5", "REDEFINE", pnd_Post_Cor_Pnd_Pc_Ssn_Old);
        pnd_Post_Cor_Pnd_Pc_Ssn_Old_A = pnd_Post_Cor__R_Field_5.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Ssn_Old_A", "#PC-SSN-OLD-A", FieldType.STRING, 9);
        pnd_Post_Cor_Pnd_Pc_Dob_Old = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Dob_Old", "#PC-DOB-OLD", FieldType.NUMERIC, 8);

        pnd_Post_Cor__R_Field_6 = pnd_Post_Cor__R_Field_1.newGroupInGroup("pnd_Post_Cor__R_Field_6", "REDEFINE", pnd_Post_Cor_Pnd_Pc_Dob_Old);
        pnd_Post_Cor_Pnd_L_Dob_Old = pnd_Post_Cor__R_Field_6.newFieldInGroup("pnd_Post_Cor_Pnd_L_Dob_Old", "#L-DOB-OLD", FieldType.STRING, 8);
        pnd_Post_Cor_Pnd_Pc_Name_Corr_Flag = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Name_Corr_Flag", "#PC-NAME-CORR-FLAG", FieldType.STRING, 
            1);
        pnd_Post_Cor_Pnd_Pc_Mit_Wpid = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Mit_Wpid", "#PC-MIT-WPID", FieldType.STRING, 6);
        pnd_Post_Cor_Pnd_Pc_Mit_Unit_Cde = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Mit_Unit_Cde", "#PC-MIT-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Post_Cor_Pnd_Pc_Mit_Log_Date_Time = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Mit_Log_Date_Time", "#PC-MIT-LOG-DATE-TIME", 
            FieldType.STRING, 15);
        pnd_Post_Cor_Pnd_Pc_Mit_Status_Cde = pnd_Post_Cor__R_Field_1.newFieldInGroup("pnd_Post_Cor_Pnd_Pc_Mit_Status_Cde", "#PC-MIT-STATUS-CDE", FieldType.STRING, 
            4);

        cwf_Pass_Sub = localVariables.newGroupInRecord("cwf_Pass_Sub", "CWF-PASS-SUB");
        cwf_Pass_Sub_Pnd_Pnd_User = cwf_Pass_Sub.newFieldInGroup("cwf_Pass_Sub_Pnd_Pnd_User", "##USER", FieldType.STRING, 8);
        cwf_Pass_Sub_Pnd_Pnd_Help_Nme = cwf_Pass_Sub.newFieldInGroup("cwf_Pass_Sub_Pnd_Pnd_Help_Nme", "##HELP-NME", FieldType.STRING, 8);
        cwf_Pass_Sub_Pnd_Pnd_Map_Nme = cwf_Pass_Sub.newFieldInGroup("cwf_Pass_Sub_Pnd_Pnd_Map_Nme", "##MAP-NME", FieldType.STRING, 8);
        cwf_Pass_Sub_Pnd_Pnd_Upd_User_Ind = cwf_Pass_Sub.newFieldInGroup("cwf_Pass_Sub_Pnd_Pnd_Upd_User_Ind", "##UPD-USER-IND", FieldType.STRING, 1);
        pnd_Efsn9120_Line = localVariables.newFieldInRecord("pnd_Efsn9120_Line", "#EFSN9120-LINE", FieldType.STRING, 79);

        pnd_Efsn9120_Line__R_Field_7 = localVariables.newGroupInRecord("pnd_Efsn9120_Line__R_Field_7", "REDEFINE", pnd_Efsn9120_Line);
        pnd_Efsn9120_Line_Pnd_Prompt = pnd_Efsn9120_Line__R_Field_7.newFieldInGroup("pnd_Efsn9120_Line_Pnd_Prompt", "#PROMPT", FieldType.STRING, 15);
        pnd_Efsn9120_Line_Pnd_Field1 = pnd_Efsn9120_Line__R_Field_7.newFieldInGroup("pnd_Efsn9120_Line_Pnd_Field1", "#FIELD1", FieldType.STRING, 30);
        pnd_Efsn9120_Line_Pnd_Field2 = pnd_Efsn9120_Line__R_Field_7.newFieldInGroup("pnd_Efsn9120_Line_Pnd_Field2", "#FIELD2", FieldType.STRING, 30);
        pnd_L_Date = localVariables.newFieldInRecord("pnd_L_Date", "#L-DATE", FieldType.DATE);

        vw_restart = new DataAccessProgramView(new NameInfo("vw_restart", "RESTART"), "BATCH_RESTART", "BATCH_RESTART");
        restart_Rst_Actve_Ind = vw_restart.getRecord().newFieldInGroup("restart_Rst_Actve_Ind", "RST-ACTVE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RST_ACTVE_IND");
        restart_Rst_Actve_Ind.setDdmHeader("ACTIVE IND");
        restart_Rst_Job_Nme = vw_restart.getRecord().newFieldInGroup("restart_Rst_Job_Nme", "RST-JOB-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_JOB_NME");
        restart_Rst_Job_Nme.setDdmHeader("JOB NAME");
        restart_Rst_Jobstep_Nme = vw_restart.getRecord().newFieldInGroup("restart_Rst_Jobstep_Nme", "RST-JOBSTEP-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_JOBSTEP_NME");
        restart_Rst_Jobstep_Nme.setDdmHeader("STEP NAME");
        restart_Rst_Pgm_Nme = vw_restart.getRecord().newFieldInGroup("restart_Rst_Pgm_Nme", "RST-PGM-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_PGM_NME");
        restart_Rst_Pgm_Nme.setDdmHeader("PROGRAM NAME");
        restart_Rst_Curr_Dte = vw_restart.getRecord().newFieldInGroup("restart_Rst_Curr_Dte", "RST-CURR-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_CURR_DTE");
        restart_Rst_Curr_Tme = vw_restart.getRecord().newFieldInGroup("restart_Rst_Curr_Tme", "RST-CURR-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_CURR_TME");
        restart_Rst_Start_Dte = vw_restart.getRecord().newFieldInGroup("restart_Rst_Start_Dte", "RST-START-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_START_DTE");
        restart_Rst_Start_Dte.setDdmHeader("START/DATE");
        restart_Rst_Start_Tme = vw_restart.getRecord().newFieldInGroup("restart_Rst_Start_Tme", "RST-START-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_START_TME");
        restart_Rst_Start_Tme.setDdmHeader("START/TIME");
        restart_Rst_Key = vw_restart.getRecord().newFieldInGroup("restart_Rst_Key", "RST-KEY", FieldType.STRING, 50, RepeatingFieldStrategy.None, "RST_KEY");
        restart_Rst_Frst_Run_Dte = vw_restart.getRecord().newFieldInGroup("restart_Rst_Frst_Run_Dte", "RST-FRST-RUN-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_FRST_RUN_DTE");
        restart_Rst_Frst_Run_Dte.setDdmHeader("FIRST/RUN DTE");
        restart_Rst_Last_Rstrt_Dte = vw_restart.getRecord().newFieldInGroup("restart_Rst_Last_Rstrt_Dte", "RST-LAST-RSTRT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_LAST_RSTRT_DTE");
        restart_Rst_Last_Rstrt_Dte.setDdmHeader("LAST RSTRT/DATE");
        restart_Rst_Last_Rstrt_Tme = vw_restart.getRecord().newFieldInGroup("restart_Rst_Last_Rstrt_Tme", "RST-LAST-RSTRT-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_LAST_RSTRT_TME");
        restart_Rst_Last_Rstrt_Tme.setDdmHeader("LAST RSTRT/TIME");
        registerRecord(vw_restart);

        pnd_L_Restart_Isn = localVariables.newFieldInRecord("pnd_L_Restart_Isn", "#L-RESTART-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_L_Restart_Key = localVariables.newFieldInRecord("pnd_L_Restart_Key", "#L-RESTART-KEY", FieldType.STRING, 17);

        pnd_L_Restart_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_L_Restart_Key__R_Field_8", "REDEFINE", pnd_L_Restart_Key);
        pnd_L_Restart_Key_Pnd_L_Act_Ind = pnd_L_Restart_Key__R_Field_8.newFieldInGroup("pnd_L_Restart_Key_Pnd_L_Act_Ind", "#L-ACT-IND", FieldType.STRING, 
            1);
        pnd_L_Restart_Key_Pnd_L_Job_Name = pnd_L_Restart_Key__R_Field_8.newFieldInGroup("pnd_L_Restart_Key_Pnd_L_Job_Name", "#L-JOB-NAME", FieldType.STRING, 
            8);
        pnd_L_Restart_Key_Pnd_L_Step_Name = pnd_L_Restart_Key__R_Field_8.newFieldInGroup("pnd_L_Restart_Key_Pnd_L_Step_Name", "#L-STEP-NAME", FieldType.STRING, 
            8);
        pnd_L_Restart_Data = localVariables.newFieldInRecord("pnd_L_Restart_Data", "#L-RESTART-DATA", FieldType.STRING, 21);

        pnd_L_Restart_Data__R_Field_9 = localVariables.newGroupInRecord("pnd_L_Restart_Data__R_Field_9", "REDEFINE", pnd_L_Restart_Data);
        pnd_L_Restart_Data_Pnd_L_Restart_Pin = pnd_L_Restart_Data__R_Field_9.newFieldInGroup("pnd_L_Restart_Data_Pnd_L_Restart_Pin", "#L-RESTART-PIN", 
            FieldType.STRING, 12);
        pnd_L_Restart_Data_Pnd_L_Restart_Ssn = pnd_L_Restart_Data__R_Field_9.newFieldInGroup("pnd_L_Restart_Data_Pnd_L_Restart_Ssn", "#L-RESTART-SSN", 
            FieldType.STRING, 9);
        pnd_L_Et_Data = localVariables.newFieldInRecord("pnd_L_Et_Data", "#L-ET-DATA", FieldType.STRING, 21);

        pnd_L_Et_Data__R_Field_10 = localVariables.newGroupInRecord("pnd_L_Et_Data__R_Field_10", "REDEFINE", pnd_L_Et_Data);
        pnd_L_Et_Data_Pnd_L_Et_Pin = pnd_L_Et_Data__R_Field_10.newFieldInGroup("pnd_L_Et_Data_Pnd_L_Et_Pin", "#L-ET-PIN", FieldType.STRING, 12);
        pnd_L_Et_Data_Pnd_L_Et_Ssn = pnd_L_Et_Data__R_Field_10.newFieldInGroup("pnd_L_Et_Data_Pnd_L_Et_Ssn", "#L-ET-SSN", FieldType.STRING, 9);
        pnd_L_Ctr = localVariables.newFieldInRecord("pnd_L_Ctr", "#L-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_L_Bot_Ctr = localVariables.newFieldInRecord("pnd_L_Bot_Ctr", "#L-BOT-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_L_Upd_Ctr = localVariables.newFieldInRecord("pnd_L_Upd_Ctr", "#L-UPD-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_L_Byp_Ctr = localVariables.newFieldInRecord("pnd_L_Byp_Ctr", "#L-BYP-CTR", FieldType.PACKED_DECIMAL, 9);
        pnd_L_New_Ssn = localVariables.newFieldInRecord("pnd_L_New_Ssn", "#L-NEW-SSN", FieldType.STRING, 11);
        pnd_L_Old_Ssn = localVariables.newFieldInRecord("pnd_L_Old_Ssn", "#L-OLD-SSN", FieldType.STRING, 11);
        pnd_L_New_Dob = localVariables.newFieldInRecord("pnd_L_New_Dob", "#L-NEW-DOB", FieldType.STRING, 10);
        pnd_L_Old_Dob = localVariables.newFieldInRecord("pnd_L_Old_Dob", "#L-OLD-DOB", FieldType.STRING, 10);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_restart.reset();

        ldaCorl600.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Corp730() throws Exception
    {
        super("Corp730");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 55 LS = 133;//Natural: FORMAT ( 2 ) PS = 55 LS = 133;//Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        pnd_L_Restart_Key_Pnd_L_Act_Ind.setValue("A");                                                                                                                    //Natural: ASSIGN #L-ACT-IND := 'A'
        pnd_L_Restart_Key_Pnd_L_Job_Name.setValue("PCR2245D");                                                                                                            //Natural: ASSIGN #L-JOB-NAME := 'PCR2245D'
        pnd_L_Restart_Key_Pnd_L_Step_Name.setValue(Global.getPROGRAM());                                                                                                  //Natural: ASSIGN #L-STEP-NAME := *PROGRAM
        //*  PERFORM CHECK-RESTART
        cwf_Pass_Sub_Pnd_Pnd_User.setValue("BATCHCOR");                                                                                                                   //Natural: ASSIGN CWF-PASS-SUB.##USER := 'BATCHCOR'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #POST-COR
        while (condition(getWorkFiles().read(1, pnd_Post_Cor)))
        {
            ldaCorl600.getCorl600_Data_Cor_Change_Data().reset();                                                                                                         //Natural: RESET CORL600-DATA.COR-CHANGE-DATA
            pnd_L_Ctr.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #L-CTR
            if (condition(pnd_Post_Cor_Pnd_Pc_Mit_Wpid.equals("TA MS")))                                                                                                  //Natural: IF #PC-MIT-WPID = 'TA MS'
            {
                if (condition(pnd_Post_Cor_Pnd_Pc_Mit_Status_Cde.equals("8030")))                                                                                         //Natural: IF #PC-MIT-STATUS-CDE = '8030'
                {
                    pnd_Post_Cor_Pnd_Pc_Mit_Status_Cde.setValue("8040");                                                                                                  //Natural: ASSIGN #PC-MIT-STATUS-CDE := '8040'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Post_Cor_Pnd_Pc_Mit_Wpid.equals("TA MDB")))                                                                                                 //Natural: IF #PC-MIT-WPID = 'TA MDB'
            {
                if (condition(pnd_Post_Cor_Pnd_Pc_Mit_Status_Cde.equals("8020")))                                                                                         //Natural: IF #PC-MIT-STATUS-CDE = '8020'
                {
                    pnd_Post_Cor_Pnd_Pc_Mit_Status_Cde.setValue("8030");                                                                                                  //Natural: ASSIGN #PC-MIT-STATUS-CDE := '8030'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Post_Cor_Pnd_Pc_Mit_Wpid.equals("TA MN")))                                                                                                  //Natural: IF #PC-MIT-WPID = 'TA MN'
            {
                if (condition(pnd_Post_Cor_Pnd_Pc_Mit_Status_Cde.equals("8040")))                                                                                         //Natural: IF #PC-MIT-STATUS-CDE = '8040'
                {
                    pnd_Post_Cor_Pnd_Pc_Mit_Status_Cde.setValue("8060");                                                                                                  //Natural: ASSIGN #PC-MIT-STATUS-CDE := '8060'
                }                                                                                                                                                         //Natural: END-IF
                //*  12-BYTE
            }                                                                                                                                                             //Natural: END-IF
            pnd_L_Et_Data_Pnd_L_Et_Pin.setValue(pnd_Post_Cor_Pnd_Pc_Pin);                                                                                                 //Natural: ASSIGN #L-ET-PIN := #POST-COR.#PC-PIN
            pnd_L_Et_Data_Pnd_L_Et_Ssn.setValue(pnd_Post_Cor_Pnd_Pc_Ssn_A);                                                                                               //Natural: ASSIGN #L-ET-SSN := #POST-COR.#PC-SSN-A
            if (condition(pnd_L_Et_Data.lessOrEqual(pnd_L_Restart_Data)))                                                                                                 //Natural: IF #L-ET-DATA LE #L-RESTART-DATA
            {
                pnd_L_Byp_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #L-BYP-CTR
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CORA730-VALUES
            sub_Cora730_Values();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM POST-OPEN-INIT-CALL
            sub_Post_Open_Init_Call();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  GTD. GET RESTART #L-RESTART-ISN
        //*  DELETE (GTD.)
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().skip(1, 4);                                                                                                                                          //Natural: SKIP ( 1 ) 4
        getReports().write(1, ReportOption.NOTITLE,"RECORD READ     :",pnd_L_Ctr,NEWLINE,"RECORD CREATED  :",pnd_L_Upd_Ctr,NEWLINE,"RECORD BYPASSED :",                   //Natural: WRITE ( 1 ) 'RECORD READ     :' #L-CTR / 'RECORD CREATED  :' #L-UPD-CTR / 'RECORD BYPASSED :' #L-BYP-CTR
            pnd_L_Byp_Ctr);
        if (Global.isEscape()) return;
        getReports().skip(1, 8);                                                                                                                                          //Natural: SKIP ( 1 ) 8
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(35),"* * * * *   E  N  D    O  F    R  E  P  O  R  T  * * * * *");                                      //Natural: WRITE ( 1 ) 35T '* * * * *   E  N  D    O  F    R  E  P  O  R  T  * * * * *'
        if (Global.isEscape()) return;
        getReports().skip(2, 4);                                                                                                                                          //Natural: SKIP ( 2 ) 4
        getReports().write(2, ReportOption.NOTITLE,"RECORD REVERSED :",pnd_L_Bot_Ctr);                                                                                    //Natural: WRITE ( 2 ) 'RECORD REVERSED :' #L-BOT-CTR
        if (Global.isEscape()) return;
        getReports().skip(2, 8);                                                                                                                                          //Natural: SKIP ( 2 ) 8
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(35),"* * * * *   E  N  D    O  F    R  E  P  O  R  T  * * * * *");                                      //Natural: WRITE ( 2 ) 35T '* * * * *   E  N  D    O  F    R  E  P  O  R  T  * * * * *'
        if (Global.isEscape()) return;
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CORA730-VALUES
        //* ******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-OPEN-INIT-CALL
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-OUTGOING
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CLOSE-POST
        //*   REPLACE WITH CORRECT CODE WHEN IT HAS BEEN ALLOCATED.
        //*  NOW FORMAT THE DOCUMENT ENTRY
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REVERSAL-ERROR-ROUTINE
        //*  ---------------------------------------------- *
        //*  TELL POST TO REVERSE ANY WORK THAT HAS BEEN DONE
        //*  ---------------------------------------------- *
        //* * ------------------------------ **
        //*   RECOVER ORIGINAL ERROR MESSAGE
        //* * ------------------------------ **
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RESTART
        //* ******************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-BATCH-RESTART
        //* *************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-BATCH-RESTART
        //* *************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-UPDATE-REPORT
    }
    private void sub_Cora730_Values() throws Exception                                                                                                                    //Natural: CORA730-VALUES
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pdaCorpda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: RESET MSG-INFO-SUB
        ldaCorl600.getCorl600_Data_Name_Prefix().setValue(pnd_Post_Cor_Pnd_Pc_Name_Prefix);                                                                               //Natural: ASSIGN NAME-PREFIX := #POST-COR.#PC-NAME-PREFIX
        ldaCorl600.getCorl600_Data_Name_Last().setValue(pnd_Post_Cor_Pnd_Pc_Name_Last);                                                                                   //Natural: ASSIGN NAME-LAST := #POST-COR.#PC-NAME-LAST
        ldaCorl600.getCorl600_Data_Name_First().setValue(pnd_Post_Cor_Pnd_Pc_Name_First);                                                                                 //Natural: ASSIGN NAME-FIRST := #POST-COR.#PC-NAME-FIRST
        ldaCorl600.getCorl600_Data_Name_Middle().setValue(pnd_Post_Cor_Pnd_Pc_Name_Middle);                                                                               //Natural: ASSIGN NAME-MIDDLE := #POST-COR.#PC-NAME-MIDDLE
        ldaCorl600.getCorl600_Data_Name_Suffix().setValue(pnd_Post_Cor_Pnd_Pc_Name_Suffix);                                                                               //Natural: ASSIGN NAME-SUFFIX := #POST-COR.#PC-NAME-SUFFIX
        ldaCorl600.getCorl600_Data_Ssn().setValue(pnd_Post_Cor_Pnd_Pc_Ssn);                                                                                               //Natural: ASSIGN SSN := #POST-COR.#PC-SSN
        ldaCorl600.getCorl600_Data_Name_Prefix_Old().setValue(pnd_Post_Cor_Pnd_Pc_Name_Prefix_Old);                                                                       //Natural: ASSIGN NAME-PREFIX-OLD := #POST-COR.#PC-NAME-PREFIX-OLD
        ldaCorl600.getCorl600_Data_Name_Last_Old().setValue(pnd_Post_Cor_Pnd_Pc_Name_Last_Old);                                                                           //Natural: ASSIGN NAME-LAST-OLD := #POST-COR.#PC-NAME-LAST-OLD
        ldaCorl600.getCorl600_Data_Name_First_Old().setValue(pnd_Post_Cor_Pnd_Pc_Name_First_Old);                                                                         //Natural: ASSIGN NAME-FIRST-OLD := #POST-COR.#PC-NAME-FIRST-OLD
        ldaCorl600.getCorl600_Data_Name_Middle_Old().setValue(pnd_Post_Cor_Pnd_Pc_Name_Middle_Old);                                                                       //Natural: ASSIGN NAME-MIDDLE-OLD := #POST-COR.#PC-NAME-MIDDLE-OLD
        ldaCorl600.getCorl600_Data_Name_Suffix_Old().setValue(pnd_Post_Cor_Pnd_Pc_Name_Suffix_Old);                                                                       //Natural: ASSIGN NAME-SUFFIX-OLD := #POST-COR.#PC-NAME-SUFFIX-OLD
        ldaCorl600.getCorl600_Data_Ssn_Old().setValue(pnd_Post_Cor_Pnd_Pc_Ssn_Old);                                                                                       //Natural: ASSIGN SSN-OLD := #POST-COR.#PC-SSN-OLD
        ldaCorl600.getCorl600_Data_Name_Corr_Flag().setValue(pnd_Post_Cor_Pnd_Pc_Name_Corr_Flag);                                                                         //Natural: ASSIGN NAME-CORR-FLAG := #POST-COR.#PC-NAME-CORR-FLAG
        if (condition(DbsUtil.maskMatches(pnd_Post_Cor_Pnd_L_Dob,"MMDDYYYY")))                                                                                            //Natural: IF #POST-COR.#L-DOB = MASK ( MMDDYYYY )
        {
            ldaCorl600.getCorl600_Data_Date_Of_Birth().setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Post_Cor_Pnd_L_Dob);                                             //Natural: MOVE EDITED #POST-COR.#L-DOB TO DATE-OF-BIRTH ( EM = MMDDYYYY )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(DbsUtil.maskMatches(pnd_Post_Cor_Pnd_L_Dob_Old,"MMDDYYYY")))                                                                                        //Natural: IF #POST-COR.#L-DOB-OLD = MASK ( MMDDYYYY )
        {
            ldaCorl600.getCorl600_Data_Date_Of_Birth_Old().setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Post_Cor_Pnd_L_Dob_Old);                                     //Natural: MOVE EDITED #POST-COR.#L-DOB-OLD TO DATE-OF-BIRTH-OLD ( EM = MMDDYYYY )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Post_Open_Init_Call() throws Exception                                                                                                               //Natural: POST-OPEN-INIT-CALL
    {
        if (BLNatReinput.isReinput()) return;

        //*  12-BYTE: REPLACED CALL TO PSTN9610 WITH CALL TO PSTN9612
        //*  AS THE LATTER HAS 13-BYTE UNIV-ID FIELD TO HOLD 12-BYTE PIN.
        //*  NEED TO POPULATE UNIV-ID-TYP - IF PIN NUMERIC, IT's a participant.
        //* *******************************************************************
        pdaPsta9610.getPsta9610().reset();                                                                                                                                //Natural: RESET PSTA9610 PSTA9612
        pdaPsta9612.getPsta9612().reset();
        //*  12-BYTE
        pdaPsta9612.getPsta9612_Full_Nme().setValue(DbsUtil.compress(pnd_Post_Cor_Pnd_Pc_Name_Prefix, pnd_Post_Cor_Pnd_Pc_Name_First, pnd_Post_Cor_Pnd_Pc_Name_Middle,    //Natural: COMPRESS #POST-COR.#PC-NAME-PREFIX #POST-COR.#PC-NAME-FIRST #POST-COR.#PC-NAME-MIDDLE #POST-COR.#PC-NAME-LAST #POST-COR.#PC-NAME-SUFFIX INTO PSTA9612.FULL-NME
            pnd_Post_Cor_Pnd_Pc_Name_Last, pnd_Post_Cor_Pnd_Pc_Name_Suffix));
        pdaPsta9612.getPsta9612_Univ_Id().setValue(pnd_Post_Cor_Pnd_Pc_Pin);                                                                                              //Natural: ASSIGN PSTA9612.UNIV-ID := #POST-COR.#PC-PIN
        if (condition(DbsUtil.is(pnd_Post_Cor_Pnd_Pc_Pin.getText(),"N12")))                                                                                               //Natural: IF #POST-COR.#PC-PIN IS ( N12 )
        {
            pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                          //Natural: ASSIGN PSTA9612.UNIV-ID-TYP := 'P'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("N");                                                                                                          //Natural: ASSIGN PSTA9612.UNIV-ID-TYP := 'N'
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9612.getPsta9612_Ssn_Nbr().setValue(pnd_Post_Cor_Pnd_Pc_Ssn);                                                                                              //Natural: ASSIGN PSTA9612.SSN-NBR := #POST-COR.#PC-SSN
        pdaPsta9612.getPsta9612_Empl_Sgntry_Nme().setValue("   ");                                                                                                        //Natural: ASSIGN PSTA9612.EMPL-SGNTRY-NME := '   '
        pdaPsta9612.getPsta9612_Empl_Unit_Work_Nme().setValue("   ");                                                                                                     //Natural: ASSIGN PSTA9612.EMPL-UNIT-WORK-NME := '   '
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("COR");                                                                                                           //Natural: ASSIGN PSTA9612.SYSTM-ID-CDE := 'COR'
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PFCORM  ");                                                                                                         //Natural: ASSIGN PSTA9612.PCKGE-CDE := 'PFCORM  '
        pdaPsta9612.getPsta9612_Rqst_Log_Oprtr_Cde().setValue(cwf_Pass_Sub_Pnd_Pnd_User);                                                                                 //Natural: ASSIGN PSTA9612.RQST-LOG-OPRTR-CDE := CWF-PASS-SUB.##USER
        pdaPsta9612.getPsta9612_Pckge_Image_Ind().setValue("Y");                                                                                                          //Natural: ASSIGN PSTA9612.PCKGE-IMAGE-IND := 'Y'
        if (condition(pnd_Post_Cor_Pnd_Pc_Mit_Wpid.notEquals(" ")))                                                                                                       //Natural: IF #POST-COR.#PC-MIT-WPID NE ' '
        {
            pdaPsta9612.getPsta9612_Rqst_Origin_Unit_Cde().setValue(pnd_Post_Cor_Pnd_Pc_Mit_Unit_Cde);                                                                    //Natural: ASSIGN PSTA9612.RQST-ORIGIN-UNIT-CDE := #POST-COR.#PC-MIT-UNIT-CDE
            pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(1).setValue(pnd_Post_Cor_Pnd_Pc_Mit_Log_Date_Time);                                                       //Natural: ASSIGN PSTA9612.RQST-LOG-DTE-TME ( 1 ) := #POST-COR.#PC-MIT-LOG-DATE-TIME
            pdaPsta9612.getPsta9612_Work_Prcss_Id().getValue(1).setValue(pnd_Post_Cor_Pnd_Pc_Mit_Wpid);                                                                   //Natural: ASSIGN PSTA9612.WORK-PRCSS-ID ( 1 ) := #POST-COR.#PC-MIT-WPID
            pdaPsta9612.getPsta9612_Status_Cde().getValue(1).setValue(pnd_Post_Cor_Pnd_Pc_Mit_Status_Cde);                                                                //Natural: ASSIGN PSTA9612.STATUS-CDE ( 1 ) := #POST-COR.#PC-MIT-STATUS-CDE
            pdaPsta9612.getPsta9612_Unit_Cde().getValue(1).setValue("RSSMP");                                                                                             //Natural: ASSIGN PSTA9612.UNIT-CDE ( 1 ) := 'RSSMP'
            pdaPsta9612.getPsta9612_Wpid_Vldte_Ind().getValue(1).setValue("Y");                                                                                           //Natural: ASSIGN PSTA9612.WPID-VLDTE-IND ( 1 ) := 'Y'
            pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(false);                                                                                                //Natural: ASSIGN NO-UPDTE-TO-MIT-IND := FALSE
            pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(false);                                                                                                //Natural: ASSIGN NO-UPDTE-TO-EFM-IND := FALSE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(true);                                                                                                 //Natural: ASSIGN NO-UPDTE-TO-MIT-IND := TRUE
            pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(true);                                                                                                 //Natural: ASSIGN NO-UPDTE-TO-EFM-IND := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCorpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        //*  DM 1-19
        if (condition(pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
            getReports().write(0, "OPEN GRC :",pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code(),"MSG:",pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                       //Natural: WRITE 'OPEN ''RC :'MSG-INFO-SUB.##RETURN-CODE 'MSG:'MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM REVERSAL-ERROR-ROUTINE
            sub_Reversal_Error_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM POST-OUTGOING
            sub_Post_Outgoing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Post_Outgoing() throws Exception                                                                                                                     //Natural: POST-OUTGOING
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pdaPsta9500.getPsta9500().reset();                                                                                                                                //Natural: RESET PSTA9500 PSTA9501
        pdaPsta9501.getPsta9501().reset();
        ldaCorl600.getSort_Key_Sort_Key_Array().getValue("*").moveAll("H'00'");                                                                                           //Natural: MOVE ALL H'00' TO SORT-KEY-ARRAY ( * )
        pdaPsta9500.getPsta9500_Data_Typ().setValue("M");                                                                                                                 //Natural: ASSIGN PSTA9500.DATA-TYP := 'M'
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB CWF-PASS-SUB PSTA9610 SORT-KEY CORL600-DATA
            pdaCdaobj.getCdaobj(), pdaCorpda_D.getDialog_Info_Sub(), pdaCorpda_M.getMsg_Info_Sub(), cwf_Pass_Sub, pdaPsta9610.getPsta9610(), ldaCorl600.getSort_Key(), 
            ldaCorl600.getCorl600_Data());
        if (condition(Global.isEscape())) return;
        //* * ------------------------------------ **
        //*  CHECK FOR ERROR IN POST POST-OUTGOING
        //* * ------------------------------------ **
        //*  DM 1-19
        if (condition(pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
            getReports().write(0, "OUTGOING :","RC :",pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code(),"MSG :",pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());               //Natural: WRITE 'OUTGOING :' 'RC :'MSG-INFO-SUB.##RETURN-CODE 'MSG :'MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM REVERSAL-ERROR-ROUTINE
            sub_Reversal_Error_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-CLOSE-POST
            sub_Update_Close_Post();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Close_Post() throws Exception                                                                                                                 //Natural: UPDATE-CLOSE-POST
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*   CALL EFSN9120 TO STORE AN AUDIT TRAIL IN CWF.
        //*   IF EFSN9120 IS SUCCESFUL, CALL POST CLOSE
        //*    SETUP THE STATIC PARAMETERS TO EFSN9020
        pdaEfsa9120.getEfsa9120().reset();                                                                                                                                //Natural: RESET EFSA9120 EFSA9120-ID EFSA902R
        pdaEfsa9120.getEfsa9120_Id().reset();
        pdaEfsa902r.getEfsa902r().reset();
        pdaEfsa9120.getEfsa9120_Rqst_Log_Dte_Tme().getValue(1).setValue(pnd_Post_Cor_Pnd_Pc_Mit_Log_Date_Time);                                                           //Natural: ASSIGN EFSA9120.RQST-LOG-DTE-TME ( 1 ) := #POST-COR.#PC-MIT-LOG-DATE-TIME
        //*  WRITE '=' #POST-COR.#PC-PIN
        //*    '='PSTA9612.SYSTM-ID-CDE
        //*    '=' #POST-COR.#PC-MIT-UNIT-CDE
        //*    '=' CWF-PASS-SUB.##USER
        //*  12-BYTE
        if (condition(pdaEfsa9120.getEfsa9120_Rqst_Log_Dte_Tme().getValue(1).notEquals(" ")))                                                                             //Natural: IF EFSA9120.RQST-LOG-DTE-TME ( 1 ) NE ' '
        {
            pdaEfsa9120.getEfsa9120_Doc_Direction().setValue("N");                                                                                                        //Natural: ASSIGN EFSA9120.DOC-DIRECTION := 'N'
            pdaEfsa9120.getEfsa9120_Action().setValue("AD");                                                                                                              //Natural: ASSIGN EFSA9120.ACTION := 'AD'
            pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                       //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
            pdaEfsa9120.getEfsa9120_Pin_Nbr().compute(new ComputeParameters(false, pdaEfsa9120.getEfsa9120_Pin_Nbr()), pnd_Post_Cor_Pnd_Pc_Pin.val());                    //Natural: ASSIGN EFSA9120.PIN-NBR := VAL ( #POST-COR.#PC-PIN )
            pdaEfsa9120.getEfsa9120_System().setValue(pdaPsta9612.getPsta9612_Systm_Id_Cde());                                                                            //Natural: ASSIGN EFSA9120.SYSTEM := PSTA9612.SYSTM-ID-CDE
            pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(pnd_Post_Cor_Pnd_Pc_Mit_Unit_Cde);                                                                    //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := #POST-COR.#PC-MIT-UNIT-CDE
            pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue(cwf_Pass_Sub_Pnd_Pnd_User);                                                                              //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := CWF-PASS-SUB.##USER
            pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue("T");                                                                                                       //Natural: ASSIGN EFSA9120.DOC-FORMAT-CDE := 'T'
            pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("M");                                                                                                      //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'M'
            pdaEfsa9120.getEfsa9120_Doc_Category().setValue("I");                                                                                                         //Natural: ASSIGN EFSA9120.DOC-CATEGORY := 'I'
            pdaEfsa9120.getEfsa9120_Doc_Class().setValue("CHG");                                                                                                          //Natural: ASSIGN EFSA9120.DOC-CLASS := 'CHG'
            pdaEfsa9120.getEfsa9120_Doc_Specific().setValue("AUDT");                                                                                                      //Natural: ASSIGN EFSA9120.DOC-SPECIFIC := 'AUDT'
            pdaEfsa9120.getEfsa9120_Last_Page_Flag().setValue("Y");                                                                                                       //Natural: ASSIGN EFSA9120.LAST-PAGE-FLAG := 'Y'
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(1).setValue("COR Details Change");                                                                                //Natural: ASSIGN EFSA9120.DOC-TEXT ( 1 ) := 'COR Details Change'
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(3).setValue("               Before Image                  After Image");                                          //Natural: ASSIGN EFSA9120.DOC-TEXT ( 3 ) := '               Before Image                  After Image'
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(4).setValue("               ==========================================================");                         //Natural: ASSIGN EFSA9120.DOC-TEXT ( 4 ) := '               =========================================================='
            pnd_Efsn9120_Line_Pnd_Prompt.setValue("       Prefix:");                                                                                                      //Natural: ASSIGN #PROMPT := '       Prefix:'
            pnd_Efsn9120_Line_Pnd_Field1.setValue(pnd_Post_Cor_Pnd_Pc_Name_Prefix_Old);                                                                                   //Natural: ASSIGN #FIELD1 := #POST-COR.#PC-NAME-PREFIX-OLD
            pnd_Efsn9120_Line_Pnd_Field2.setValue(pnd_Post_Cor_Pnd_Pc_Name_Prefix);                                                                                       //Natural: ASSIGN #FIELD2 := #POST-COR.#PC-NAME-PREFIX
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(5).setValue(pnd_Efsn9120_Line);                                                                                   //Natural: ASSIGN EFSA9120.DOC-TEXT ( 5 ) := #EFSN9120-LINE
            pnd_Efsn9120_Line_Pnd_Prompt.setValue("   First Name:");                                                                                                      //Natural: ASSIGN #PROMPT := '   First Name:'
            pnd_Efsn9120_Line_Pnd_Field1.setValue(pnd_Post_Cor_Pnd_Pc_Name_First_Old);                                                                                    //Natural: ASSIGN #FIELD1 := #POST-COR.#PC-NAME-FIRST-OLD
            pnd_Efsn9120_Line_Pnd_Field2.setValue(pnd_Post_Cor_Pnd_Pc_Name_First);                                                                                        //Natural: ASSIGN #FIELD2 := #POST-COR.#PC-NAME-FIRST
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(6).setValue(pnd_Efsn9120_Line);                                                                                   //Natural: ASSIGN EFSA9120.DOC-TEXT ( 6 ) := #EFSN9120-LINE
            pnd_Efsn9120_Line_Pnd_Prompt.setValue("  Middle Name:");                                                                                                      //Natural: ASSIGN #PROMPT := '  Middle Name:'
            pnd_Efsn9120_Line_Pnd_Field1.setValue(pnd_Post_Cor_Pnd_Pc_Name_Middle_Old);                                                                                   //Natural: ASSIGN #FIELD1 := #POST-COR.#PC-NAME-MIDDLE-OLD
            pnd_Efsn9120_Line_Pnd_Field2.setValue(pnd_Post_Cor_Pnd_Pc_Name_Middle);                                                                                       //Natural: ASSIGN #FIELD2 := #POST-COR.#PC-NAME-MIDDLE
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(7).setValue(pnd_Efsn9120_Line);                                                                                   //Natural: ASSIGN EFSA9120.DOC-TEXT ( 7 ) := #EFSN9120-LINE
            pnd_Efsn9120_Line_Pnd_Prompt.setValue("    Last Name:");                                                                                                      //Natural: ASSIGN #PROMPT := '    Last Name:'
            pnd_Efsn9120_Line_Pnd_Field1.setValue(pnd_Post_Cor_Pnd_Pc_Name_Last_Old);                                                                                     //Natural: ASSIGN #FIELD1 := #POST-COR.#PC-NAME-LAST-OLD
            pnd_Efsn9120_Line_Pnd_Field2.setValue(pnd_Post_Cor_Pnd_Pc_Name_Last);                                                                                         //Natural: ASSIGN #FIELD2 := #POST-COR.#PC-NAME-LAST
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(8).setValue(pnd_Efsn9120_Line);                                                                                   //Natural: ASSIGN EFSA9120.DOC-TEXT ( 8 ) := #EFSN9120-LINE
            pnd_Efsn9120_Line_Pnd_Prompt.setValue("       Suffix:");                                                                                                      //Natural: ASSIGN #PROMPT := '       Suffix:'
            pnd_Efsn9120_Line_Pnd_Field1.setValue(pnd_Post_Cor_Pnd_Pc_Name_Suffix_Old);                                                                                   //Natural: ASSIGN #FIELD1 := #POST-COR.#PC-NAME-SUFFIX-OLD
            pnd_Efsn9120_Line_Pnd_Field2.setValue(pnd_Post_Cor_Pnd_Pc_Name_Suffix);                                                                                       //Natural: ASSIGN #FIELD2 := #POST-COR.#PC-NAME-SUFFIX
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(9).setValue(pnd_Efsn9120_Line);                                                                                   //Natural: ASSIGN EFSA9120.DOC-TEXT ( 9 ) := #EFSN9120-LINE
            pnd_Efsn9120_Line.reset();                                                                                                                                    //Natural: RESET #EFSN9120-LINE
            pnd_Efsn9120_Line_Pnd_Prompt.setValue("          SSN:");                                                                                                      //Natural: ASSIGN #PROMPT := '          SSN:'
            if (condition(pnd_Post_Cor_Pnd_Pc_Ssn_Old.greater(getZero())))                                                                                                //Natural: IF #POST-COR.#PC-SSN-OLD > 0
            {
                pnd_Efsn9120_Line_Pnd_Field1.setValueEdited(pnd_Post_Cor_Pnd_Pc_Ssn_Old,new ReportEditMask("999-99-9999"));                                               //Natural: MOVE EDITED #POST-COR.#PC-SSN-OLD ( EM = 999-99-9999 ) TO #FIELD1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Post_Cor_Pnd_Pc_Ssn.greater(getZero())))                                                                                                    //Natural: IF #POST-COR.#PC-SSN > 0
            {
                pnd_Efsn9120_Line_Pnd_Field2.setValueEdited(pnd_Post_Cor_Pnd_Pc_Ssn,new ReportEditMask("999-99-9999"));                                                   //Natural: MOVE EDITED #POST-COR.#PC-SSN ( EM = 999-99-9999 ) TO #FIELD2
            }                                                                                                                                                             //Natural: END-IF
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(10).setValue(pnd_Efsn9120_Line);                                                                                  //Natural: ASSIGN EFSA9120.DOC-TEXT ( 10 ) := #EFSN9120-LINE
            pnd_Efsn9120_Line.reset();                                                                                                                                    //Natural: RESET #EFSN9120-LINE
            pnd_Efsn9120_Line_Pnd_Prompt.setValue("Date of Birth:");                                                                                                      //Natural: ASSIGN #PROMPT := 'Date of Birth:'
            if (condition(pnd_Post_Cor_Pnd_Pc_Dob_Old.notEquals(getZero())))                                                                                              //Natural: IF #POST-COR.#PC-DOB-OLD NE 0
            {
                pnd_L_Date.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Post_Cor_Pnd_L_Dob_Old);                                                                     //Natural: MOVE EDITED #POST-COR.#L-DOB-OLD TO #L-DATE ( EM = MMDDYYYY )
                pnd_Efsn9120_Line_Pnd_Field1.setValueEdited(pnd_L_Date,new ReportEditMask("MM/DD/YYYY"));                                                                 //Natural: MOVE EDITED #L-DATE ( EM = MM/DD/YYYY ) TO #FIELD1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Post_Cor_Pnd_Pc_Dob.notEquals(getZero())))                                                                                                  //Natural: IF #POST-COR.#PC-DOB NE 0
            {
                pnd_L_Date.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Post_Cor_Pnd_L_Dob);                                                                         //Natural: MOVE EDITED #POST-COR.#L-DOB TO #L-DATE ( EM = MMDDYYYY )
                pnd_Efsn9120_Line_Pnd_Field2.setValueEdited(pnd_L_Date,new ReportEditMask("MM/DD/YYYY"));                                                                 //Natural: MOVE EDITED #L-DATE ( EM = MM/DD/YYYY ) TO #FIELD2
            }                                                                                                                                                             //Natural: END-IF
            pdaEfsa9120.getEfsa9120_Doc_Text().getValue(11).setValue(pnd_Efsn9120_Line);                                                                                  //Natural: ASSIGN EFSA9120.DOC-TEXT ( 11 ) := #EFSN9120-LINE
            //* ** FOR TESTING PURPOSES ONLY ****
            //*   FOR #X 1 55
            //*     IF EFSA9120.DOC-TEXT(#X) NE ' '
            //*       EXAMINE EFSA9120.DOC-TEXT(#X) AND TRANSLATE INTO UPPER
            //*       WRITE WORK FILE 2 EFSA9120.DOC-TEXT(#X)
            //*     END-IF
            //*   END-FOR
            //* ** FOR TESTING PURPOSES ONLY ****
            //*   ADD AUDIT TRAIL DOCUMENT
            DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB CWF-PASS-SUB
                pdaCdaobj.getCdaobj(), pdaCorpda_D.getDialog_Info_Sub(), pdaCorpda_M.getMsg_Info_Sub(), cwf_Pass_Sub);
            if (condition(Global.isEscape())) return;
            //*  DM 1-19
            //*  EFSN9120 SETS MSG-NR INSTEAD
            if (condition(pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().notEquals(getZero())))                                                                             //Natural: IF MSG-INFO-SUB.##MSG-NR NE 0
            {
                getReports().write(0, "CLOSE :GRC :",pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr(),"MSG :",pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                     //Natural: WRITE 'CLOSE :''RC :'MSG-INFO-SUB.##MSG-NR 'MSG :'MSG-INFO-SUB.##MSG
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM REVERSAL-ERROR-ROUTINE
                sub_Reversal_Error_Routine();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  12-BYTE
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCorpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        //* * ---------------------------------------- **
        //*  CHECK FOR ERROR IN POST UPDATE-CLOSE-POST
        //* * ---------------------------------------- **
        //*  DM 1-19
        if (condition(pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
            getReports().write(0, "UPDATE :","RC :",pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code(),"MSG :",pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                 //Natural: WRITE 'UPDATE :' 'RC :'MSG-INFO-SUB.##RETURN-CODE 'MSG :'MSG-INFO-SUB.##MSG
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM REVERSAL-ERROR-ROUTINE
            sub_Reversal_Error_Routine();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  PERFORM UPDATE-BATCH-RESTART
        //*  WRITE 'RECORD PROCESSED:'#L-ET-PIN '='#L-ET-SSN
        pnd_L_Upd_Ctr.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #L-UPD-CTR
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM CREATE-UPDATE-REPORT
        sub_Create_Update_Report();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Reversal_Error_Routine() throws Exception                                                                                                            //Natural: REVERSAL-ERROR-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        //*  FORMAT ##MSG
        DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCorpda_M.getMsg_Info_Sub());                                                                        //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        pdaPsta9610.getPsta9610_Pst_Backout_Ind().setValue(true);                                                                                                         //Natural: ASSIGN PSTA9610.PST-BACKOUT-IND := TRUE
        //*  DM 1-19
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCorpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        //* * --------------------------------------------- **
        //*  IF POST BACKOUT IS OK, THEN COMMIT THE CHANGES.
        //* * --------------------------------------------- **
        if (condition(pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals(" ")))                                                                                             //Natural: IF MSG-INFO-SUB.##MSG = ' '
        {
            pnd_L_Bot_Ctr.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #L-BOT-CTR
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,pnd_Post_Cor_Pnd_Pc_Pin,new TabSetting(15),pnd_Post_Cor_Pnd_Pc_Ssn,new TabSetting(30),pnd_Post_Cor_Pnd_Pc_Ssn_Old,new  //Natural: WRITE ( 2 ) #POST-COR.#PC-PIN 15T#POST-COR.#PC-SSN 30T#POST-COR.#PC-SSN-OLD 45TMSG-INFO-SUB.##MSG
            TabSetting(45),pdaCorpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());
        if (Global.isEscape()) return;
    }
    private void sub_Check_Restart() throws Exception                                                                                                                     //Natural: CHECK-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        vw_restart.startDatabaseFind                                                                                                                                      //Natural: FIND ( 1 ) RESTART WITH SP-ACTVE-JOB-STEP = #L-RESTART-KEY
        (
        "FIND01",
        new Wc[] { new Wc("SP_ACTVE_JOB_STEP", "=", pnd_L_Restart_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_restart.readNextRow("FIND01", true)))
        {
            vw_restart.setIfNotFoundControlFlag(false);
            if (condition(vw_restart.getAstCOUNTER().equals(0)))                                                                                                          //Natural: IF NO RECORD FOUND
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-BATCH-RESTART
                sub_Create_Batch_Restart();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_L_Restart_Isn.setValue(vw_restart.getAstISN("Find01"));                                                                                                   //Natural: ASSIGN #L-RESTART-ISN := *ISN
            pnd_L_Restart_Data.setValue(restart_Rst_Key);                                                                                                                 //Natural: ASSIGN #L-RESTART-DATA := RESTART.RST-KEY
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Create_Batch_Restart() throws Exception                                                                                                              //Natural: CREATE-BATCH-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        restart_Rst_Actve_Ind.setValue(pnd_L_Restart_Key_Pnd_L_Act_Ind);                                                                                                  //Natural: ASSIGN RESTART.RST-ACTVE-IND := #L-ACT-IND
        restart_Rst_Job_Nme.setValue(pnd_L_Restart_Key_Pnd_L_Job_Name);                                                                                                   //Natural: ASSIGN RESTART.RST-JOB-NME := #L-JOB-NAME
        restart_Rst_Jobstep_Nme.setValue(pnd_L_Restart_Key_Pnd_L_Step_Name);                                                                                              //Natural: ASSIGN RESTART.RST-JOBSTEP-NME := #L-STEP-NAME
        restart_Rst_Pgm_Nme.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN RESTART.RST-PGM-NME := *PROGRAM
        restart_Rst_Frst_Run_Dte.setValue(Global.getDATN());                                                                                                              //Natural: ASSIGN RESTART.RST-FRST-RUN-DTE := *DATN
        restart_Rst_Pgm_Nme.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN RESTART.RST-PGM-NME := *PROGRAM
        restart_Rst_Frst_Run_Dte.setValue(Global.getDATN());                                                                                                              //Natural: ASSIGN RESTART.RST-FRST-RUN-DTE := *DATN
        restart_Rst_Start_Dte.setValue(Global.getDATN());                                                                                                                 //Natural: ASSIGN RESTART.RST-START-DTE := RST-CURR-DTE := *DATN
        restart_Rst_Curr_Dte.setValue(Global.getDATN());
        restart_Rst_Curr_Tme.setValue(Global.getTIMN());                                                                                                                  //Natural: ASSIGN RESTART.RST-CURR-TME := RST-START-TME := *TIMN
        restart_Rst_Start_Tme.setValue(Global.getTIMN());
        ST:                                                                                                                                                               //Natural: STORE RESTART
        vw_restart.insertDBRow("ST");
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_L_Restart_Isn.setValue(vw_restart.getAstISN("ST"));                                                                                                           //Natural: ASSIGN #L-RESTART-ISN := *ISN ( ST. )
    }
    private void sub_Update_Batch_Restart() throws Exception                                                                                                              //Natural: UPDATE-BATCH-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        GTU:                                                                                                                                                              //Natural: GET RESTART #L-RESTART-ISN
        vw_restart.readByID(pnd_L_Restart_Isn.getLong(), "GTU");
        restart_Rst_Key.setValue(pnd_L_Et_Data);                                                                                                                          //Natural: ASSIGN RESTART.RST-KEY := #L-ET-DATA
        restart_Rst_Last_Rstrt_Dte.setValue(Global.getDATN());                                                                                                            //Natural: ASSIGN RESTART.RST-LAST-RSTRT-DTE := *DATN
        restart_Rst_Last_Rstrt_Tme.setValue(Global.getTIMN());                                                                                                            //Natural: ASSIGN RESTART.RST-LAST-RSTRT-TME := *TIMN
        vw_restart.updateDBRow("GTU");                                                                                                                                    //Natural: UPDATE ( GTU. )
    }
    private void sub_Create_Update_Report() throws Exception                                                                                                              //Natural: CREATE-UPDATE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_L_New_Ssn.setValueEdited(pnd_Post_Cor_Pnd_Pc_Ssn_A,new ReportEditMask("XXX-XX-XXXX"));                                                                        //Natural: MOVE EDITED #PC-SSN-A ( EM = XXX-XX-XXXX ) TO #L-NEW-SSN
        if (condition(pnd_Post_Cor_Pnd_Pc_Ssn.equals(getZero())))                                                                                                         //Natural: IF #PC-SSN = 0
        {
            pnd_L_New_Ssn.setValue("000-00-0000");                                                                                                                        //Natural: ASSIGN #L-NEW-SSN := '000-00-0000'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_L_Old_Ssn.setValueEdited(pnd_Post_Cor_Pnd_Pc_Ssn_Old_A,new ReportEditMask("XXX-XX-XXXX"));                                                                    //Natural: MOVE EDITED #PC-SSN-OLD-A ( EM = XXX-XX-XXXX ) TO #L-OLD-SSN
        if (condition(pnd_Post_Cor_Pnd_Pc_Ssn_Old.equals(getZero())))                                                                                                     //Natural: IF #PC-SSN-OLD = 0
        {
            pnd_L_Old_Ssn.setValue("000-00-0000");                                                                                                                        //Natural: ASSIGN #L-OLD-SSN := '000-00-0000'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Post_Cor_Pnd_Pc_Dob.notEquals(getZero())))                                                                                                      //Natural: IF #POST-COR.#PC-DOB NE 0
        {
            pnd_L_Date.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Post_Cor_Pnd_L_Dob);                                                                             //Natural: MOVE EDITED #POST-COR.#L-DOB TO #L-DATE ( EM = MMDDYYYY )
            pnd_L_New_Dob.setValueEdited(pnd_L_Date,new ReportEditMask("MM/DD/YYYY"));                                                                                    //Natural: MOVE EDITED #L-DATE ( EM = MM/DD/YYYY ) TO #L-NEW-DOB
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Post_Cor_Pnd_Pc_Dob_Old.notEquals(getZero())))                                                                                                  //Natural: IF #POST-COR.#PC-DOB-OLD NE 0
        {
            pnd_L_Date.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Post_Cor_Pnd_L_Dob_Old);                                                                         //Natural: MOVE EDITED #POST-COR.#L-DOB-OLD TO #L-DATE ( EM = MMDDYYYY )
            pnd_L_Old_Dob.setValueEdited(pnd_L_Date,new ReportEditMask("MM/DD/YYYY"));                                                                                    //Natural: MOVE EDITED #L-DATE ( EM = MM/DD/YYYY ) TO #L-OLD-DOB
            //*  DM 2/17
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,pnd_Post_Cor_Pnd_Pc_Pin,new TabSetting(15),pnd_L_New_Ssn,new TabSetting(28),pnd_Post_Cor_Pnd_Pc_Name_First,            //Natural: WRITE ( 1 ) #PC-PIN 15T#L-NEW-SSN 28T#PC-NAME-FIRST ( AL = 15 ) 46T#PC-NAME-LAST ( AL = 15 ) 63T#L-NEW-DOB 78T#L-OLD-SSN 92T#PC-NAME-FIRST-OLD ( AL = 15 ) 110T#PC-NAME-LAST-OLD ( AL = 15 ) 127T#L-OLD-DOB
            new AlphanumericLength (15),new TabSetting(46),pnd_Post_Cor_Pnd_Pc_Name_Last, new AlphanumericLength (15),new TabSetting(63),pnd_L_New_Dob,new 
            TabSetting(78),pnd_L_Old_Ssn,new TabSetting(92),pnd_Post_Cor_Pnd_Pc_Name_First_Old, new AlphanumericLength (15),new TabSetting(110),pnd_Post_Cor_Pnd_Pc_Name_Last_Old, 
            new AlphanumericLength (15),new TabSetting(127),pnd_L_Old_Dob);
        if (Global.isEscape()) return;
        pnd_L_New_Ssn.reset();                                                                                                                                            //Natural: RESET #L-NEW-SSN #L-NEW-DOB #L-OLD-SSN #L-OLD-DOB
        pnd_L_New_Dob.reset();
        pnd_L_Old_Ssn.reset();
        pnd_L_Old_Dob.reset();
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new TabSetting(45),"CONFIRMATION LETTER THRU POST",new              //Natural: WRITE ( 1 ) NOTITLE NOHDR *PROGRAM 45T 'CONFIRMATION LETTER THRU POST' 112T'DATE :'*DATX ( EM = L ( 3 ) ' 'DD','YYYY ) / 52T'CREATED RECORD' 112T'PAGE :'*PAGE-NUMBER ( 1 ) / '*' ( 132 ) / 23T'N  E  W     D  E  T  A  I  L  S' 88T'O  L  D     D  E  T  A  I  L  S' / 10T'-' ( 58 ) 73T'-' ( 60 ) / 2T 'PIN NO' 14T'SSN' 23T'FIRST NAME' 41T'LAST NAME' 58T'BIRTH DATE' 77T'SSN' 87T'FIRST NAME' 105T'LAST NAME' 122T'BIRTH DATE'
                        TabSetting(112),"DATE :",Global.getDATX(), new ReportEditMask ("LLL' 'DD','YYYY"),NEWLINE,new TabSetting(52),"CREATED RECORD",new 
                        TabSetting(112),"PAGE :",getReports().getPageNumberDbs(1),NEWLINE,"*",new RepeatItem(132),NEWLINE,new TabSetting(23),"N  E  W     D  E  T  A  I  L  S",new 
                        TabSetting(88),"O  L  D     D  E  T  A  I  L  S",NEWLINE,new TabSetting(10),"-",new RepeatItem(58),new TabSetting(73),"-",new RepeatItem(60),NEWLINE,new 
                        TabSetting(2),"PIN NO",new TabSetting(14),"SSN",new TabSetting(23),"FIRST NAME",new TabSetting(41),"LAST NAME",new TabSetting(58),"BIRTH DATE",new 
                        TabSetting(77),"SSN",new TabSetting(87),"FIRST NAME",new TabSetting(105),"LAST NAME",new TabSetting(122),"BIRTH DATE");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getPROGRAM(),new TabSetting(45),"CONFIRMATION LETTER THRU POST",new              //Natural: WRITE ( 2 ) NOTITLE NOHDR *PROGRAM 45T 'CONFIRMATION LETTER THRU POST' 112T'DATE :'*DATX ( EM = L ( 3 ) ' 'DD','YYYY ) / 52T'ERROR REPORT' 112T'PAGE :'*PAGE-NUMBER ( 2 ) / '*' ( 132 ) / 'PIN NO' 12T'NEW SSN' 27T'OLD SSN' 40T'ERROR MESSAGE' / '-' ( 7 ) 10T'-' ( 11 ) 25T'-' ( 11 ) 40T'-' ( 90 )
                        TabSetting(112),"DATE :",Global.getDATX(), new ReportEditMask ("LLL' 'DD','YYYY"),NEWLINE,new TabSetting(52),"ERROR REPORT",new 
                        TabSetting(112),"PAGE :",getReports().getPageNumberDbs(2),NEWLINE,"*",new RepeatItem(132),NEWLINE,"PIN NO",new TabSetting(12),"NEW SSN",new 
                        TabSetting(27),"OLD SSN",new TabSetting(40),"ERROR MESSAGE",NEWLINE,"-",new RepeatItem(7),new TabSetting(10),"-",new RepeatItem(11),new 
                        TabSetting(25),"-",new RepeatItem(11),new TabSetting(40),"-",new RepeatItem(90));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=55 LS=133");
        Global.format(2, "PS=55 LS=133");
    }
}
