/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:23:41 PM
**        * FROM NATURAL PROGRAM : Corp850
************************************************************
**        * FILE NAME            : Corp850.java
**        * CLASS NAME           : Corp850
**        * INSTANCE NAME        : Corp850
************************************************************
* ---------------------------------------------------------------------
* PROGRAM: CORP850
*  AUTHOR: R. GO-PAZ
*    DATE: 02/27/2008
* PURPOSE: THIS PROGRAM CREATES WORK FILE OF SECURITY TABLE FOR
*          ENTITLEMENTS
* ---------------------------------------------------------------------

************************************************************ */

package tiaa.cor_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Corp850 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_security;
    private DbsField security_Cor_Sec_Super_Data;

    private DbsGroup security__R_Field_1;
    private DbsField security_Pnd_Table_Name;
    private DbsField security_Pnd_Table_Userid;
    private DbsField security_Pnd_Security_Level;
    private DbsField security_Pnd_Add_Access;
    private DbsField pnd_Cor_Cntrl_Super_Data;

    private DbsGroup pnd_Cor_Cntrl_Super_Data__R_Field_2;
    private DbsField pnd_Cor_Cntrl_Super_Data_Pnd_Table;
    private DbsField pnd_Cor_Cntrl_Super_Data_Pnd_Userid;
    private DbsField pnd_Level;
    private DbsField pnd_Level1;
    private DbsField pnd_Level2;
    private DbsField pnd_Level3;
    private DbsField pnd_Level4;
    private DbsField pnd_Level5;
    private DbsField pnd_Filler;
    private DbsField pnd_Filler2;
    private DbsField pnd_Write;
    private DbsField pnd_Entitle;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_security = new DataAccessProgramView(new NameInfo("vw_security", "SECURITY"), "COR_SECURITY_FILE", "COR_SECURITY");
        security_Cor_Sec_Super_Data = vw_security.getRecord().newFieldInGroup("security_Cor_Sec_Super_Data", "COR-SEC-SUPER-DATA", FieldType.STRING, 60, 
            RepeatingFieldStrategy.None, "COR_SEC_SUPER_DATA");
        security_Cor_Sec_Super_Data.setSuperDescriptor(true);

        security__R_Field_1 = vw_security.getRecord().newGroupInGroup("security__R_Field_1", "REDEFINE", security_Cor_Sec_Super_Data);
        security_Pnd_Table_Name = security__R_Field_1.newFieldInGroup("security_Pnd_Table_Name", "#TABLE-NAME", FieldType.STRING, 10);
        security_Pnd_Table_Userid = security__R_Field_1.newFieldInGroup("security_Pnd_Table_Userid", "#TABLE-USERID", FieldType.STRING, 8);
        security_Pnd_Security_Level = security__R_Field_1.newFieldInGroup("security_Pnd_Security_Level", "#SECURITY-LEVEL", FieldType.NUMERIC, 1);
        security_Pnd_Add_Access = security__R_Field_1.newFieldInGroup("security_Pnd_Add_Access", "#ADD-ACCESS", FieldType.STRING, 1);
        registerRecord(vw_security);

        pnd_Cor_Cntrl_Super_Data = localVariables.newFieldInRecord("pnd_Cor_Cntrl_Super_Data", "#COR-CNTRL-SUPER-DATA", FieldType.STRING, 60);

        pnd_Cor_Cntrl_Super_Data__R_Field_2 = localVariables.newGroupInRecord("pnd_Cor_Cntrl_Super_Data__R_Field_2", "REDEFINE", pnd_Cor_Cntrl_Super_Data);
        pnd_Cor_Cntrl_Super_Data_Pnd_Table = pnd_Cor_Cntrl_Super_Data__R_Field_2.newFieldInGroup("pnd_Cor_Cntrl_Super_Data_Pnd_Table", "#TABLE", FieldType.STRING, 
            10);
        pnd_Cor_Cntrl_Super_Data_Pnd_Userid = pnd_Cor_Cntrl_Super_Data__R_Field_2.newFieldInGroup("pnd_Cor_Cntrl_Super_Data_Pnd_Userid", "#USERID", FieldType.STRING, 
            8);
        pnd_Level = localVariables.newFieldInRecord("pnd_Level", "#LEVEL", FieldType.STRING, 50);
        pnd_Level1 = localVariables.newFieldInRecord("pnd_Level1", "#LEVEL1", FieldType.STRING, 50);
        pnd_Level2 = localVariables.newFieldInRecord("pnd_Level2", "#LEVEL2", FieldType.STRING, 50);
        pnd_Level3 = localVariables.newFieldInRecord("pnd_Level3", "#LEVEL3", FieldType.STRING, 50);
        pnd_Level4 = localVariables.newFieldInRecord("pnd_Level4", "#LEVEL4", FieldType.STRING, 50);
        pnd_Level5 = localVariables.newFieldInRecord("pnd_Level5", "#LEVEL5", FieldType.STRING, 50);
        pnd_Filler = localVariables.newFieldInRecord("pnd_Filler", "#FILLER", FieldType.STRING, 1);
        pnd_Filler2 = localVariables.newFieldInRecord("pnd_Filler2", "#FILLER2", FieldType.STRING, 1);
        pnd_Write = localVariables.newFieldInRecord("pnd_Write", "#WRITE", FieldType.NUMERIC, 7);
        pnd_Entitle = localVariables.newFieldInRecord("pnd_Entitle", "#ENTITLE", FieldType.STRING, 130);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_security.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Corp850() throws Exception
    {
        super("Corp850");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        pnd_Filler.setValue("H'7F'");                                                                                                                                     //Natural: FORMAT PS = 60 LS = 133;//Natural: ASSIGN #FILLER := H'7F'
        pnd_Filler2.setValue(",");                                                                                                                                        //Natural: ASSIGN #FILLER2 := ','
        pnd_Level1.setValue("IC,AP,AC,UP,BA,BP,RT");                                                                                                                      //Natural: ASSIGN #LEVEL1 := 'IC,AP,AC,UP,BA,BP,RT'
        pnd_Level2.setValue("IC,AP,AC,UP,BA,RP,CP,DC,PF,BP,RT");                                                                                                          //Natural: ASSIGN #LEVEL2 := 'IC,AP,AC,UP,BA,RP,CP,DC,PF,BP,RT'
        pnd_Level3.setValue("IC,AP,AC,UP,BA,DP,RP,CP,DC,PF,RO,MS,BP,RT");                                                                                                 //Natural: ASSIGN #LEVEL3 := 'IC,AP,AC,UP,BA,DP,RP,CP,DC,PF,RO,MS,BP,RT'
        pnd_Level4.setValue("IC,UF");                                                                                                                                     //Natural: ASSIGN #LEVEL4 := 'IC,UF'
        pnd_Level5.setValue("IC,BA,US");                                                                                                                                  //Natural: ASSIGN #LEVEL5 := 'IC,BA,US'
        vw_security.startDatabaseRead                                                                                                                                     //Natural: READ SECURITY BY COR-SEC-SUPER-DATA STARTING FROM 'SECURITY'
        (
        "READ01",
        new Wc[] { new Wc("COR_SEC_SUPER_DATA", ">=", "SECURITY", WcType.BY) },
        new Oc[] { new Oc("COR_SEC_SUPER_DATA", "ASC") }
        );
        READ01:
        while (condition(vw_security.readNextRow("READ01")))
        {
            short decideConditionsMet45 = 0;                                                                                                                              //Natural: DECIDE ON FIRST VALUE OF #SECURITY-LEVEL;//Natural: VALUE 1
            if (condition((security_Pnd_Security_Level.equals(1))))
            {
                decideConditionsMet45++;
                pnd_Level.setValue(pnd_Level1);                                                                                                                           //Natural: ASSIGN #LEVEL := #LEVEL1
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((security_Pnd_Security_Level.equals(2))))
            {
                decideConditionsMet45++;
                pnd_Level.setValue(pnd_Level2);                                                                                                                           //Natural: ASSIGN #LEVEL := #LEVEL2
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((security_Pnd_Security_Level.equals(3))))
            {
                decideConditionsMet45++;
                pnd_Level.setValue(pnd_Level3);                                                                                                                           //Natural: ASSIGN #LEVEL := #LEVEL3
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((security_Pnd_Security_Level.equals(4))))
            {
                decideConditionsMet45++;
                pnd_Level.setValue(pnd_Level4);                                                                                                                           //Natural: ASSIGN #LEVEL := #LEVEL4
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((security_Pnd_Security_Level.equals(5))))
            {
                decideConditionsMet45++;
                pnd_Level.setValue(pnd_Level5);                                                                                                                           //Natural: ASSIGN #LEVEL := #LEVEL5
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(security_Pnd_Add_Access.greater(" ")))                                                                                                          //Natural: IF #ADD-ACCESS GT ' '
            {
                pnd_Entitle.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, security_Pnd_Table_Userid, ",", security_Pnd_Security_Level, ",",                    //Natural: COMPRESS #TABLE-USERID ',' #SECURITY-LEVEL',' #ADD-ACCESS ',' #FILLER #LEVEL #FILLER #FILLER2 INTO #ENTITLE LEAVING NO
                    security_Pnd_Add_Access, ",", pnd_Filler, pnd_Level, pnd_Filler, pnd_Filler2));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Entitle.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, security_Pnd_Table_Userid, ",", security_Pnd_Security_Level, ",,",                   //Natural: COMPRESS #TABLE-USERID ',' #SECURITY-LEVEL',,' #FILLER #LEVEL #FILLER #FILLER2 INTO #ENTITLE LEAVING NO
                    pnd_Filler, pnd_Level, pnd_Filler, pnd_Filler2));
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Entitle);                                                                                                                  //Natural: WRITE WORK FILE 1 #ENTITLE
            pnd_Write.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #WRITE
            pnd_Entitle.reset();                                                                                                                                          //Natural: RESET #ENTITLE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "Total records written in the work file ",pnd_Write, new ReportEditMask ("Z,ZZZ,Z99"));                                                     //Natural: WRITE 'Total records written in the work file ' #WRITE ( EM = Z,ZZZ,Z99 )
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
    }
}
