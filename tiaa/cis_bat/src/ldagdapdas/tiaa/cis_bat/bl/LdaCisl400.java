/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:35 PM
**        * FROM NATURAL LDA     : CISL400
************************************************************
**        * FILE NAME            : LdaCisl400.java
**        * CLASS NAME           : LdaCisl400
**        * INSTANCE NAME        : LdaCisl400
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl400 extends DbsRecord
{
    // Properties
    private DbsGroup cisl400;
    private DbsField cisl400_Cis_Rqst_Id;
    private DbsField cisl400_Cis_Function_Cde;
    private DbsField cisl400_Cis_Pin_Nbr;
    private DbsField cisl400_Cis_Annt_Ssn;
    private DbsField cisl400_Cis_Existing_Tiaa_Nbr;
    private DbsField cisl400_Cis_Payee_Cde;
    private DbsField cisl400_Cis_Annt_Prfx;
    private DbsField cisl400_Cis_Annt_Lst_Nme;
    private DbsField cisl400_Cis_Annt_Frst_Nme;
    private DbsField cisl400_Cis_Annt_Mid_Nme;
    private DbsField cisl400_Cis_Annt_Sffx;
    private DbsField cisl400_Cis_Annt_Dob;
    private DbsField cisl400_Cis_Foriegn_Ssn;
    private DbsField cisl400_Cis_Annt_Sex_Cde;
    private DbsField cisl400_Cis_Annt_Dod;
    private DbsField cisl400_Cis_Tlc_Category;
    private DbsField cisl400_Cis_Area_Of_Origin;
    private DbsField cisl400_Cis_Tiaa_Nbr;
    private DbsGroup cisl400_Cis_Tiaa_NbrRedef1;
    private DbsField cisl400_Pnd_Cis_Tiaa_Nbr_1_7;
    private DbsField cisl400_Cis_Doi;
    private DbsField cisl400_Cis_Cor_Cntrct_Status_Cde;
    private DbsField cisl400_Cis_Cor_Cntrct_Status_Yr_Dte;
    private DbsField cisl400_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField cisl400_Cis_Cert_Nbr;
    private DbsGroup cisl400_Cis_Cert_NbrRedef2;
    private DbsField cisl400_Pnd_Cis_Cert_Nbr_1_7;
    private DbsField cisl400_Cis_Ownership_Cd;
    private DbsField cisl400_Cis_Product_Code;
    private DbsField cisl400_Cis_Annty_Option;
    private DbsField cisl400_Cis_Tax_Id;
    private DbsField cisl400_Cis_Plan_Code;
    private DbsField cisl400_Addr_Line1;
    private DbsField cisl400_Addr_Line2;
    private DbsField cisl400_Addr_Line3;
    private DbsField cisl400_Addr_Line4;
    private DbsField cisl400_Addr_Line5;
    private DbsField cisl400_Addr_Line6;
    private DbsField cisl400_Cis_Zip_Code_Ia;

    public DbsGroup getCisl400() { return cisl400; }

    public DbsField getCisl400_Cis_Rqst_Id() { return cisl400_Cis_Rqst_Id; }

    public DbsField getCisl400_Cis_Function_Cde() { return cisl400_Cis_Function_Cde; }

    public DbsField getCisl400_Cis_Pin_Nbr() { return cisl400_Cis_Pin_Nbr; }

    public DbsField getCisl400_Cis_Annt_Ssn() { return cisl400_Cis_Annt_Ssn; }

    public DbsField getCisl400_Cis_Existing_Tiaa_Nbr() { return cisl400_Cis_Existing_Tiaa_Nbr; }

    public DbsField getCisl400_Cis_Payee_Cde() { return cisl400_Cis_Payee_Cde; }

    public DbsField getCisl400_Cis_Annt_Prfx() { return cisl400_Cis_Annt_Prfx; }

    public DbsField getCisl400_Cis_Annt_Lst_Nme() { return cisl400_Cis_Annt_Lst_Nme; }

    public DbsField getCisl400_Cis_Annt_Frst_Nme() { return cisl400_Cis_Annt_Frst_Nme; }

    public DbsField getCisl400_Cis_Annt_Mid_Nme() { return cisl400_Cis_Annt_Mid_Nme; }

    public DbsField getCisl400_Cis_Annt_Sffx() { return cisl400_Cis_Annt_Sffx; }

    public DbsField getCisl400_Cis_Annt_Dob() { return cisl400_Cis_Annt_Dob; }

    public DbsField getCisl400_Cis_Foriegn_Ssn() { return cisl400_Cis_Foriegn_Ssn; }

    public DbsField getCisl400_Cis_Annt_Sex_Cde() { return cisl400_Cis_Annt_Sex_Cde; }

    public DbsField getCisl400_Cis_Annt_Dod() { return cisl400_Cis_Annt_Dod; }

    public DbsField getCisl400_Cis_Tlc_Category() { return cisl400_Cis_Tlc_Category; }

    public DbsField getCisl400_Cis_Area_Of_Origin() { return cisl400_Cis_Area_Of_Origin; }

    public DbsField getCisl400_Cis_Tiaa_Nbr() { return cisl400_Cis_Tiaa_Nbr; }

    public DbsGroup getCisl400_Cis_Tiaa_NbrRedef1() { return cisl400_Cis_Tiaa_NbrRedef1; }

    public DbsField getCisl400_Pnd_Cis_Tiaa_Nbr_1_7() { return cisl400_Pnd_Cis_Tiaa_Nbr_1_7; }

    public DbsField getCisl400_Cis_Doi() { return cisl400_Cis_Doi; }

    public DbsField getCisl400_Cis_Cor_Cntrct_Status_Cde() { return cisl400_Cis_Cor_Cntrct_Status_Cde; }

    public DbsField getCisl400_Cis_Cor_Cntrct_Status_Yr_Dte() { return cisl400_Cis_Cor_Cntrct_Status_Yr_Dte; }

    public DbsField getCisl400_Cis_Cor_Cntrct_Payee_Cde() { return cisl400_Cis_Cor_Cntrct_Payee_Cde; }

    public DbsField getCisl400_Cis_Cert_Nbr() { return cisl400_Cis_Cert_Nbr; }

    public DbsGroup getCisl400_Cis_Cert_NbrRedef2() { return cisl400_Cis_Cert_NbrRedef2; }

    public DbsField getCisl400_Pnd_Cis_Cert_Nbr_1_7() { return cisl400_Pnd_Cis_Cert_Nbr_1_7; }

    public DbsField getCisl400_Cis_Ownership_Cd() { return cisl400_Cis_Ownership_Cd; }

    public DbsField getCisl400_Cis_Product_Code() { return cisl400_Cis_Product_Code; }

    public DbsField getCisl400_Cis_Annty_Option() { return cisl400_Cis_Annty_Option; }

    public DbsField getCisl400_Cis_Tax_Id() { return cisl400_Cis_Tax_Id; }

    public DbsField getCisl400_Cis_Plan_Code() { return cisl400_Cis_Plan_Code; }

    public DbsField getCisl400_Addr_Line1() { return cisl400_Addr_Line1; }

    public DbsField getCisl400_Addr_Line2() { return cisl400_Addr_Line2; }

    public DbsField getCisl400_Addr_Line3() { return cisl400_Addr_Line3; }

    public DbsField getCisl400_Addr_Line4() { return cisl400_Addr_Line4; }

    public DbsField getCisl400_Addr_Line5() { return cisl400_Addr_Line5; }

    public DbsField getCisl400_Addr_Line6() { return cisl400_Addr_Line6; }

    public DbsField getCisl400_Cis_Zip_Code_Ia() { return cisl400_Cis_Zip_Code_Ia; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cisl400 = newGroupInRecord("cisl400", "CISL400");
        cisl400_Cis_Rqst_Id = cisl400.newFieldInGroup("cisl400_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8);
        cisl400_Cis_Function_Cde = cisl400.newFieldInGroup("cisl400_Cis_Function_Cde", "CIS-FUNCTION-CDE", FieldType.STRING, 3);
        cisl400_Cis_Pin_Nbr = cisl400.newFieldInGroup("cisl400_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.STRING, 12);
        cisl400_Cis_Annt_Ssn = cisl400.newFieldInGroup("cisl400_Cis_Annt_Ssn", "CIS-ANNT-SSN", FieldType.NUMERIC, 9);
        cisl400_Cis_Existing_Tiaa_Nbr = cisl400.newFieldInGroup("cisl400_Cis_Existing_Tiaa_Nbr", "CIS-EXISTING-TIAA-NBR", FieldType.STRING, 10);
        cisl400_Cis_Payee_Cde = cisl400.newFieldInGroup("cisl400_Cis_Payee_Cde", "CIS-PAYEE-CDE", FieldType.STRING, 2);
        cisl400_Cis_Annt_Prfx = cisl400.newFieldInGroup("cisl400_Cis_Annt_Prfx", "CIS-ANNT-PRFX", FieldType.STRING, 8);
        cisl400_Cis_Annt_Lst_Nme = cisl400.newFieldInGroup("cisl400_Cis_Annt_Lst_Nme", "CIS-ANNT-LST-NME", FieldType.STRING, 30);
        cisl400_Cis_Annt_Frst_Nme = cisl400.newFieldInGroup("cisl400_Cis_Annt_Frst_Nme", "CIS-ANNT-FRST-NME", FieldType.STRING, 30);
        cisl400_Cis_Annt_Mid_Nme = cisl400.newFieldInGroup("cisl400_Cis_Annt_Mid_Nme", "CIS-ANNT-MID-NME", FieldType.STRING, 30);
        cisl400_Cis_Annt_Sffx = cisl400.newFieldInGroup("cisl400_Cis_Annt_Sffx", "CIS-ANNT-SFFX", FieldType.STRING, 8);
        cisl400_Cis_Annt_Dob = cisl400.newFieldInGroup("cisl400_Cis_Annt_Dob", "CIS-ANNT-DOB", FieldType.NUMERIC, 8);
        cisl400_Cis_Foriegn_Ssn = cisl400.newFieldInGroup("cisl400_Cis_Foriegn_Ssn", "CIS-FORIEGN-SSN", FieldType.NUMERIC, 9);
        cisl400_Cis_Annt_Sex_Cde = cisl400.newFieldInGroup("cisl400_Cis_Annt_Sex_Cde", "CIS-ANNT-SEX-CDE", FieldType.STRING, 1);
        cisl400_Cis_Annt_Dod = cisl400.newFieldInGroup("cisl400_Cis_Annt_Dod", "CIS-ANNT-DOD", FieldType.NUMERIC, 8);
        cisl400_Cis_Tlc_Category = cisl400.newFieldInGroup("cisl400_Cis_Tlc_Category", "CIS-TLC-CATEGORY", FieldType.STRING, 2);
        cisl400_Cis_Area_Of_Origin = cisl400.newFieldInGroup("cisl400_Cis_Area_Of_Origin", "CIS-AREA-OF-ORIGIN", FieldType.STRING, 3);
        cisl400_Cis_Tiaa_Nbr = cisl400.newFieldInGroup("cisl400_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10);
        cisl400_Cis_Tiaa_NbrRedef1 = cisl400.newGroupInGroup("cisl400_Cis_Tiaa_NbrRedef1", "Redefines", cisl400_Cis_Tiaa_Nbr);
        cisl400_Pnd_Cis_Tiaa_Nbr_1_7 = cisl400_Cis_Tiaa_NbrRedef1.newFieldInGroup("cisl400_Pnd_Cis_Tiaa_Nbr_1_7", "#CIS-TIAA-NBR-1-7", FieldType.STRING, 
            7);
        cisl400_Cis_Doi = cisl400.newFieldInGroup("cisl400_Cis_Doi", "CIS-DOI", FieldType.NUMERIC, 8);
        cisl400_Cis_Cor_Cntrct_Status_Cde = cisl400.newFieldInGroup("cisl400_Cis_Cor_Cntrct_Status_Cde", "CIS-COR-CNTRCT-STATUS-CDE", FieldType.STRING, 
            1);
        cisl400_Cis_Cor_Cntrct_Status_Yr_Dte = cisl400.newFieldInGroup("cisl400_Cis_Cor_Cntrct_Status_Yr_Dte", "CIS-COR-CNTRCT-STATUS-YR-DTE", FieldType.NUMERIC, 
            4);
        cisl400_Cis_Cor_Cntrct_Payee_Cde = cisl400.newFieldInGroup("cisl400_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", FieldType.STRING, 2);
        cisl400_Cis_Cert_Nbr = cisl400.newFieldInGroup("cisl400_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10);
        cisl400_Cis_Cert_NbrRedef2 = cisl400.newGroupInGroup("cisl400_Cis_Cert_NbrRedef2", "Redefines", cisl400_Cis_Cert_Nbr);
        cisl400_Pnd_Cis_Cert_Nbr_1_7 = cisl400_Cis_Cert_NbrRedef2.newFieldInGroup("cisl400_Pnd_Cis_Cert_Nbr_1_7", "#CIS-CERT-NBR-1-7", FieldType.STRING, 
            7);
        cisl400_Cis_Ownership_Cd = cisl400.newFieldInGroup("cisl400_Cis_Ownership_Cd", "CIS-OWNERSHIP-CD", FieldType.STRING, 1);
        cisl400_Cis_Product_Code = cisl400.newFieldInGroup("cisl400_Cis_Product_Code", "CIS-PRODUCT-CODE", FieldType.STRING, 1);
        cisl400_Cis_Annty_Option = cisl400.newFieldInGroup("cisl400_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 2);
        cisl400_Cis_Tax_Id = cisl400.newFieldInGroup("cisl400_Cis_Tax_Id", "CIS-TAX-ID", FieldType.STRING, 9);
        cisl400_Cis_Plan_Code = cisl400.newFieldInGroup("cisl400_Cis_Plan_Code", "CIS-PLAN-CODE", FieldType.STRING, 5);
        cisl400_Addr_Line1 = cisl400.newFieldInGroup("cisl400_Addr_Line1", "ADDR-LINE1", FieldType.STRING, 35);
        cisl400_Addr_Line2 = cisl400.newFieldInGroup("cisl400_Addr_Line2", "ADDR-LINE2", FieldType.STRING, 35);
        cisl400_Addr_Line3 = cisl400.newFieldInGroup("cisl400_Addr_Line3", "ADDR-LINE3", FieldType.STRING, 35);
        cisl400_Addr_Line4 = cisl400.newFieldInGroup("cisl400_Addr_Line4", "ADDR-LINE4", FieldType.STRING, 35);
        cisl400_Addr_Line5 = cisl400.newFieldInGroup("cisl400_Addr_Line5", "ADDR-LINE5", FieldType.STRING, 35);
        cisl400_Addr_Line6 = cisl400.newFieldInGroup("cisl400_Addr_Line6", "ADDR-LINE6", FieldType.STRING, 35);
        cisl400_Cis_Zip_Code_Ia = cisl400.newFieldInGroup("cisl400_Cis_Zip_Code_Ia", "CIS-ZIP-CODE-IA", FieldType.STRING, 9);

        this.setRecordName("LdaCisl400");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl400() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
