/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:43 PM
**        * FROM NATURAL LDA     : CISL608
************************************************************
**        * FILE NAME            : LdaCisl608.java
**        * CLASS NAME           : LdaCisl608
**        * INSTANCE NAME        : LdaCisl608
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl608 extends DbsRecord
{
    // Properties
    private DbsGroup ra;
    private DbsField ra_Record_Id;
    private DbsGroup ra_Record_IdRedef1;
    private DbsField ra_Record_Type;
    private DbsField ra_Output_Profile;
    private DbsField ra_Reprint_Ind;
    private DbsField ra_Contract_Type;
    private DbsField ra_Rqst_Id;
    private DbsField ra_Tiaa_Numb;
    private DbsField ra_Cref_Cert_Numb;
    private DbsField ra_Da_Rea_Numb;
    private DbsField ra_Da_Rea_Amt;
    private DbsField ra_Last_Record;

    public DbsGroup getRa() { return ra; }

    public DbsField getRa_Record_Id() { return ra_Record_Id; }

    public DbsGroup getRa_Record_IdRedef1() { return ra_Record_IdRedef1; }

    public DbsField getRa_Record_Type() { return ra_Record_Type; }

    public DbsField getRa_Output_Profile() { return ra_Output_Profile; }

    public DbsField getRa_Reprint_Ind() { return ra_Reprint_Ind; }

    public DbsField getRa_Contract_Type() { return ra_Contract_Type; }

    public DbsField getRa_Rqst_Id() { return ra_Rqst_Id; }

    public DbsField getRa_Tiaa_Numb() { return ra_Tiaa_Numb; }

    public DbsField getRa_Cref_Cert_Numb() { return ra_Cref_Cert_Numb; }

    public DbsField getRa_Da_Rea_Numb() { return ra_Da_Rea_Numb; }

    public DbsField getRa_Da_Rea_Amt() { return ra_Da_Rea_Amt; }

    public DbsField getRa_Last_Record() { return ra_Last_Record; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ra = newGroupInRecord("ra", "RA");
        ra_Record_Id = ra.newFieldInGroup("ra_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        ra_Record_IdRedef1 = ra.newGroupInGroup("ra_Record_IdRedef1", "Redefines", ra_Record_Id);
        ra_Record_Type = ra_Record_IdRedef1.newFieldInGroup("ra_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        ra_Output_Profile = ra_Record_IdRedef1.newFieldInGroup("ra_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        ra_Reprint_Ind = ra_Record_IdRedef1.newFieldInGroup("ra_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        ra_Contract_Type = ra_Record_IdRedef1.newFieldInGroup("ra_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        ra_Rqst_Id = ra_Record_IdRedef1.newFieldInGroup("ra_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        ra_Tiaa_Numb = ra_Record_IdRedef1.newFieldInGroup("ra_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        ra_Cref_Cert_Numb = ra_Record_IdRedef1.newFieldInGroup("ra_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        ra_Da_Rea_Numb = ra.newFieldInGroup("ra_Da_Rea_Numb", "DA-REA-NUMB", FieldType.STRING, 12);
        ra_Da_Rea_Amt = ra.newFieldInGroup("ra_Da_Rea_Amt", "DA-REA-AMT", FieldType.STRING, 15);
        ra_Last_Record = ra.newFieldInGroup("ra_Last_Record", "LAST-RECORD", FieldType.STRING, 1);

        this.setRecordName("LdaCisl608");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl608() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
