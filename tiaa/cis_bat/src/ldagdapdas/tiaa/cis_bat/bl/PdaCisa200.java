/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:19 PM
**        * FROM NATURAL PDA     : CISA200
************************************************************
**        * FILE NAME            : PdaCisa200.java
**        * CLASS NAME           : PdaCisa200
**        * INSTANCE NAME        : PdaCisa200
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCisa200 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Parm_Data;
    private DbsField pnd_Parm_Data_Pnd_Cis_Type_Call;
    private DbsField pnd_Parm_Data_Pnd_Cis_Code;
    private DbsField pnd_Parm_Data_Pnd_Cis_Desc;

    public DbsGroup getPnd_Parm_Data() { return pnd_Parm_Data; }

    public DbsField getPnd_Parm_Data_Pnd_Cis_Type_Call() { return pnd_Parm_Data_Pnd_Cis_Type_Call; }

    public DbsField getPnd_Parm_Data_Pnd_Cis_Code() { return pnd_Parm_Data_Pnd_Cis_Code; }

    public DbsField getPnd_Parm_Data_Pnd_Cis_Desc() { return pnd_Parm_Data_Pnd_Cis_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Parm_Data = dbsRecord.newGroupInRecord("pnd_Parm_Data", "#PARM-DATA");
        pnd_Parm_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Parm_Data_Pnd_Cis_Type_Call = pnd_Parm_Data.newFieldInGroup("pnd_Parm_Data_Pnd_Cis_Type_Call", "#CIS-TYPE-CALL", FieldType.STRING, 1);
        pnd_Parm_Data_Pnd_Cis_Code = pnd_Parm_Data.newFieldInGroup("pnd_Parm_Data_Pnd_Cis_Code", "#CIS-CODE", FieldType.STRING, 2);
        pnd_Parm_Data_Pnd_Cis_Desc = pnd_Parm_Data.newFieldInGroup("pnd_Parm_Data_Pnd_Cis_Desc", "#CIS-DESC", FieldType.STRING, 20);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCisa200(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

