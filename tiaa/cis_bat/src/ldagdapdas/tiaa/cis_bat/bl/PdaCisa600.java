/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:22 PM
**        * FROM NATURAL PDA     : CISA600
************************************************************
**        * FILE NAME            : PdaCisa600.java
**        * CLASS NAME           : PdaCisa600
**        * INSTANCE NAME        : PdaCisa600
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCisa600 extends PdaBase
{
    // Properties
    private DbsGroup cisa600;
    private DbsField cisa600_Pnd_Cis_Rqst_Id;
    private DbsField cisa600_Pnd_Cis_Annty_Option;
    private DbsField cisa600_Pnd_Cis_Mdo_Contract_Cash_Status;
    private DbsField cisa600_Pnd_Cis_Mdo_Contract_Type;
    private DbsField cisa600_Pnd_Cis_Tiaa_Cntrct_Type;
    private DbsField cisa600_Pnd_Cis_Rea_Cntrct_Type;
    private DbsField cisa600_Pnd_Cis_Cref_Cntrct_Type;
    private DbsField cisa600_Pnd_Tiaa_Cntrct_Header_1;
    private DbsField cisa600_Pnd_Tiaa_Cntrct_Header_2;
    private DbsField cisa600_Pnd_Cref_Cntrct_Header_1;
    private DbsField cisa600_Pnd_Cref_Cntrct_Header_2;
    private DbsField cisa600_Pnd_Tiaa_Rea_Cntrct_Header_1;
    private DbsField cisa600_Pnd_Tiaa_Rea_Cntrct_Header_2;
    private DbsField cisa600_Pnd_Tiaa_Income_Option;
    private DbsField cisa600_Pnd_Cref_Income_Option;
    private DbsField cisa600_Pnd_Tiaa_Form_Num_Pg3_4;
    private DbsField cisa600_Pnd_Tiaa_Form_Num_Pg5_6;
    private DbsField cisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4;
    private DbsField cisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6;
    private DbsField cisa600_Pnd_Cref_Form_Num_Pg3_4;
    private DbsField cisa600_Pnd_Cref_Form_Num_Pg5_6;
    private DbsField cisa600_Pnd_Tiaa_Product_Cde_3_4;
    private DbsField cisa600_Pnd_Tiaa_Product_Cde_5_6;
    private DbsField cisa600_Pnd_Tiaa_Rea_Product_Cde_3_4;
    private DbsField cisa600_Pnd_Tiaa_Rea_Product_Cde_5_6;
    private DbsField cisa600_Pnd_Cref_Product_Cde_3_4;
    private DbsField cisa600_Pnd_Cref_Product_Cde_5_6;
    private DbsField cisa600_Pnd_Tiaa_Edition_Num_Pg3_4;
    private DbsField cisa600_Pnd_Tiaa_Edition_Num_Pg5_6;
    private DbsField cisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4;
    private DbsField cisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6;
    private DbsField cisa600_Pnd_Cref_Edition_Num_Pg3_4;
    private DbsField cisa600_Pnd_Cref_Edition_Num_Pg5_6;

    public DbsGroup getCisa600() { return cisa600; }

    public DbsField getCisa600_Pnd_Cis_Rqst_Id() { return cisa600_Pnd_Cis_Rqst_Id; }

    public DbsField getCisa600_Pnd_Cis_Annty_Option() { return cisa600_Pnd_Cis_Annty_Option; }

    public DbsField getCisa600_Pnd_Cis_Mdo_Contract_Cash_Status() { return cisa600_Pnd_Cis_Mdo_Contract_Cash_Status; }

    public DbsField getCisa600_Pnd_Cis_Mdo_Contract_Type() { return cisa600_Pnd_Cis_Mdo_Contract_Type; }

    public DbsField getCisa600_Pnd_Cis_Tiaa_Cntrct_Type() { return cisa600_Pnd_Cis_Tiaa_Cntrct_Type; }

    public DbsField getCisa600_Pnd_Cis_Rea_Cntrct_Type() { return cisa600_Pnd_Cis_Rea_Cntrct_Type; }

    public DbsField getCisa600_Pnd_Cis_Cref_Cntrct_Type() { return cisa600_Pnd_Cis_Cref_Cntrct_Type; }

    public DbsField getCisa600_Pnd_Tiaa_Cntrct_Header_1() { return cisa600_Pnd_Tiaa_Cntrct_Header_1; }

    public DbsField getCisa600_Pnd_Tiaa_Cntrct_Header_2() { return cisa600_Pnd_Tiaa_Cntrct_Header_2; }

    public DbsField getCisa600_Pnd_Cref_Cntrct_Header_1() { return cisa600_Pnd_Cref_Cntrct_Header_1; }

    public DbsField getCisa600_Pnd_Cref_Cntrct_Header_2() { return cisa600_Pnd_Cref_Cntrct_Header_2; }

    public DbsField getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_1() { return cisa600_Pnd_Tiaa_Rea_Cntrct_Header_1; }

    public DbsField getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2() { return cisa600_Pnd_Tiaa_Rea_Cntrct_Header_2; }

    public DbsField getCisa600_Pnd_Tiaa_Income_Option() { return cisa600_Pnd_Tiaa_Income_Option; }

    public DbsField getCisa600_Pnd_Cref_Income_Option() { return cisa600_Pnd_Cref_Income_Option; }

    public DbsField getCisa600_Pnd_Tiaa_Form_Num_Pg3_4() { return cisa600_Pnd_Tiaa_Form_Num_Pg3_4; }

    public DbsField getCisa600_Pnd_Tiaa_Form_Num_Pg5_6() { return cisa600_Pnd_Tiaa_Form_Num_Pg5_6; }

    public DbsField getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4() { return cisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4; }

    public DbsField getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6() { return cisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6; }

    public DbsField getCisa600_Pnd_Cref_Form_Num_Pg3_4() { return cisa600_Pnd_Cref_Form_Num_Pg3_4; }

    public DbsField getCisa600_Pnd_Cref_Form_Num_Pg5_6() { return cisa600_Pnd_Cref_Form_Num_Pg5_6; }

    public DbsField getCisa600_Pnd_Tiaa_Product_Cde_3_4() { return cisa600_Pnd_Tiaa_Product_Cde_3_4; }

    public DbsField getCisa600_Pnd_Tiaa_Product_Cde_5_6() { return cisa600_Pnd_Tiaa_Product_Cde_5_6; }

    public DbsField getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4() { return cisa600_Pnd_Tiaa_Rea_Product_Cde_3_4; }

    public DbsField getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6() { return cisa600_Pnd_Tiaa_Rea_Product_Cde_5_6; }

    public DbsField getCisa600_Pnd_Cref_Product_Cde_3_4() { return cisa600_Pnd_Cref_Product_Cde_3_4; }

    public DbsField getCisa600_Pnd_Cref_Product_Cde_5_6() { return cisa600_Pnd_Cref_Product_Cde_5_6; }

    public DbsField getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4() { return cisa600_Pnd_Tiaa_Edition_Num_Pg3_4; }

    public DbsField getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6() { return cisa600_Pnd_Tiaa_Edition_Num_Pg5_6; }

    public DbsField getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4() { return cisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4; }

    public DbsField getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6() { return cisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6; }

    public DbsField getCisa600_Pnd_Cref_Edition_Num_Pg3_4() { return cisa600_Pnd_Cref_Edition_Num_Pg3_4; }

    public DbsField getCisa600_Pnd_Cref_Edition_Num_Pg5_6() { return cisa600_Pnd_Cref_Edition_Num_Pg5_6; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cisa600 = dbsRecord.newGroupInRecord("cisa600", "CISA600");
        cisa600.setParameterOption(ParameterOption.ByReference);
        cisa600_Pnd_Cis_Rqst_Id = cisa600.newFieldInGroup("cisa600_Pnd_Cis_Rqst_Id", "#CIS-RQST-ID", FieldType.STRING, 8);
        cisa600_Pnd_Cis_Annty_Option = cisa600.newFieldInGroup("cisa600_Pnd_Cis_Annty_Option", "#CIS-ANNTY-OPTION", FieldType.STRING, 10);
        cisa600_Pnd_Cis_Mdo_Contract_Cash_Status = cisa600.newFieldInGroup("cisa600_Pnd_Cis_Mdo_Contract_Cash_Status", "#CIS-MDO-CONTRACT-CASH-STATUS", 
            FieldType.STRING, 1);
        cisa600_Pnd_Cis_Mdo_Contract_Type = cisa600.newFieldInGroup("cisa600_Pnd_Cis_Mdo_Contract_Type", "#CIS-MDO-CONTRACT-TYPE", FieldType.STRING, 1);
        cisa600_Pnd_Cis_Tiaa_Cntrct_Type = cisa600.newFieldInGroup("cisa600_Pnd_Cis_Tiaa_Cntrct_Type", "#CIS-TIAA-CNTRCT-TYPE", FieldType.STRING, 1);
        cisa600_Pnd_Cis_Rea_Cntrct_Type = cisa600.newFieldInGroup("cisa600_Pnd_Cis_Rea_Cntrct_Type", "#CIS-REA-CNTRCT-TYPE", FieldType.STRING, 1);
        cisa600_Pnd_Cis_Cref_Cntrct_Type = cisa600.newFieldInGroup("cisa600_Pnd_Cis_Cref_Cntrct_Type", "#CIS-CREF-CNTRCT-TYPE", FieldType.STRING, 1);
        cisa600_Pnd_Tiaa_Cntrct_Header_1 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Cntrct_Header_1", "#TIAA-CNTRCT-HEADER-1", FieldType.STRING, 85);
        cisa600_Pnd_Tiaa_Cntrct_Header_2 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Cntrct_Header_2", "#TIAA-CNTRCT-HEADER-2", FieldType.STRING, 85);
        cisa600_Pnd_Cref_Cntrct_Header_1 = cisa600.newFieldInGroup("cisa600_Pnd_Cref_Cntrct_Header_1", "#CREF-CNTRCT-HEADER-1", FieldType.STRING, 85);
        cisa600_Pnd_Cref_Cntrct_Header_2 = cisa600.newFieldInGroup("cisa600_Pnd_Cref_Cntrct_Header_2", "#CREF-CNTRCT-HEADER-2", FieldType.STRING, 85);
        cisa600_Pnd_Tiaa_Rea_Cntrct_Header_1 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Rea_Cntrct_Header_1", "#TIAA-REA-CNTRCT-HEADER-1", FieldType.STRING, 
            85);
        cisa600_Pnd_Tiaa_Rea_Cntrct_Header_2 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Rea_Cntrct_Header_2", "#TIAA-REA-CNTRCT-HEADER-2", FieldType.STRING, 
            85);
        cisa600_Pnd_Tiaa_Income_Option = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Income_Option", "#TIAA-INCOME-OPTION", FieldType.STRING, 85);
        cisa600_Pnd_Cref_Income_Option = cisa600.newFieldInGroup("cisa600_Pnd_Cref_Income_Option", "#CREF-INCOME-OPTION", FieldType.STRING, 85);
        cisa600_Pnd_Tiaa_Form_Num_Pg3_4 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Form_Num_Pg3_4", "#TIAA-FORM-NUM-PG3-4", FieldType.STRING, 20);
        cisa600_Pnd_Tiaa_Form_Num_Pg5_6 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Form_Num_Pg5_6", "#TIAA-FORM-NUM-PG5-6", FieldType.STRING, 20);
        cisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4", "#TIAA-REA-FORM-NUM-PG3-4", FieldType.STRING, 
            20);
        cisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6", "#TIAA-REA-FORM-NUM-PG5-6", FieldType.STRING, 
            20);
        cisa600_Pnd_Cref_Form_Num_Pg3_4 = cisa600.newFieldInGroup("cisa600_Pnd_Cref_Form_Num_Pg3_4", "#CREF-FORM-NUM-PG3-4", FieldType.STRING, 20);
        cisa600_Pnd_Cref_Form_Num_Pg5_6 = cisa600.newFieldInGroup("cisa600_Pnd_Cref_Form_Num_Pg5_6", "#CREF-FORM-NUM-PG5-6", FieldType.STRING, 20);
        cisa600_Pnd_Tiaa_Product_Cde_3_4 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Product_Cde_3_4", "#TIAA-PRODUCT-CDE-3-4", FieldType.STRING, 35);
        cisa600_Pnd_Tiaa_Product_Cde_5_6 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Product_Cde_5_6", "#TIAA-PRODUCT-CDE-5-6", FieldType.STRING, 35);
        cisa600_Pnd_Tiaa_Rea_Product_Cde_3_4 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Rea_Product_Cde_3_4", "#TIAA-REA-PRODUCT-CDE-3-4", FieldType.STRING, 
            35);
        cisa600_Pnd_Tiaa_Rea_Product_Cde_5_6 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Rea_Product_Cde_5_6", "#TIAA-REA-PRODUCT-CDE-5-6", FieldType.STRING, 
            35);
        cisa600_Pnd_Cref_Product_Cde_3_4 = cisa600.newFieldInGroup("cisa600_Pnd_Cref_Product_Cde_3_4", "#CREF-PRODUCT-CDE-3-4", FieldType.STRING, 35);
        cisa600_Pnd_Cref_Product_Cde_5_6 = cisa600.newFieldInGroup("cisa600_Pnd_Cref_Product_Cde_5_6", "#CREF-PRODUCT-CDE-5-6", FieldType.STRING, 35);
        cisa600_Pnd_Tiaa_Edition_Num_Pg3_4 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Edition_Num_Pg3_4", "#TIAA-EDITION-NUM-PG3-4", FieldType.STRING, 
            15);
        cisa600_Pnd_Tiaa_Edition_Num_Pg5_6 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Edition_Num_Pg5_6", "#TIAA-EDITION-NUM-PG5-6", FieldType.STRING, 
            15);
        cisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4", "#TIAA-REA-EDITION-NUM-PG3-4", FieldType.STRING, 
            15);
        cisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6 = cisa600.newFieldInGroup("cisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6", "#TIAA-REA-EDITION-NUM-PG5-6", FieldType.STRING, 
            15);
        cisa600_Pnd_Cref_Edition_Num_Pg3_4 = cisa600.newFieldInGroup("cisa600_Pnd_Cref_Edition_Num_Pg3_4", "#CREF-EDITION-NUM-PG3-4", FieldType.STRING, 
            15);
        cisa600_Pnd_Cref_Edition_Num_Pg5_6 = cisa600.newFieldInGroup("cisa600_Pnd_Cref_Edition_Num_Pg5_6", "#CREF-EDITION-NUM-PG5-6", FieldType.STRING, 
            15);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCisa600(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

