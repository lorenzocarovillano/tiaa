/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:43 PM
**        * FROM NATURAL LDA     : CISL612
************************************************************
**        * FILE NAME            : LdaCisl612.java
**        * CLASS NAME           : LdaCisl612
**        * INSTANCE NAME        : LdaCisl612
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl612 extends DbsRecord
{
    // Properties
    private DbsGroup fr;
    private DbsField fr_Record_Id;
    private DbsGroup fr_Record_IdRedef1;
    private DbsField fr_Record_Type;
    private DbsField fr_Output_Profile;
    private DbsField fr_Reprint_Ind;
    private DbsField fr_Contract_Type;
    private DbsField fr_Rqst_Id;
    private DbsField fr_Tiaa_Numb;
    private DbsField fr_Cref_Cert_Numb;
    private DbsGroup fr_From_Invest;
    private DbsField fr_Invest_Ind;
    private DbsField fr_From_Invest_Header;
    private DbsGroup fr_From_Invest_HeaderRedef2;
    private DbsField fr_Plan_Name;
    private DbsField fr_Invest_Tiaa_Num;
    private DbsField fr_Invest_Cref_Num;
    private DbsField fr_Invest_Orig_Amt;
    private DbsField fr_Invest_Process_Dt;
    private DbsField fr_From_Invest_Detail;
    private DbsGroup fr_From_Invest_DetailRedef3;
    private DbsField fr_Invest_Fund_Name_From;
    private DbsField fr_Invest_Fund_Amt;
    private DbsField fr_Invest_Unit_Price;
    private DbsField fr_Invest_Units;

    public DbsGroup getFr() { return fr; }

    public DbsField getFr_Record_Id() { return fr_Record_Id; }

    public DbsGroup getFr_Record_IdRedef1() { return fr_Record_IdRedef1; }

    public DbsField getFr_Record_Type() { return fr_Record_Type; }

    public DbsField getFr_Output_Profile() { return fr_Output_Profile; }

    public DbsField getFr_Reprint_Ind() { return fr_Reprint_Ind; }

    public DbsField getFr_Contract_Type() { return fr_Contract_Type; }

    public DbsField getFr_Rqst_Id() { return fr_Rqst_Id; }

    public DbsField getFr_Tiaa_Numb() { return fr_Tiaa_Numb; }

    public DbsField getFr_Cref_Cert_Numb() { return fr_Cref_Cert_Numb; }

    public DbsGroup getFr_From_Invest() { return fr_From_Invest; }

    public DbsField getFr_Invest_Ind() { return fr_Invest_Ind; }

    public DbsField getFr_From_Invest_Header() { return fr_From_Invest_Header; }

    public DbsGroup getFr_From_Invest_HeaderRedef2() { return fr_From_Invest_HeaderRedef2; }

    public DbsField getFr_Plan_Name() { return fr_Plan_Name; }

    public DbsField getFr_Invest_Tiaa_Num() { return fr_Invest_Tiaa_Num; }

    public DbsField getFr_Invest_Cref_Num() { return fr_Invest_Cref_Num; }

    public DbsField getFr_Invest_Orig_Amt() { return fr_Invest_Orig_Amt; }

    public DbsField getFr_Invest_Process_Dt() { return fr_Invest_Process_Dt; }

    public DbsField getFr_From_Invest_Detail() { return fr_From_Invest_Detail; }

    public DbsGroup getFr_From_Invest_DetailRedef3() { return fr_From_Invest_DetailRedef3; }

    public DbsField getFr_Invest_Fund_Name_From() { return fr_Invest_Fund_Name_From; }

    public DbsField getFr_Invest_Fund_Amt() { return fr_Invest_Fund_Amt; }

    public DbsField getFr_Invest_Unit_Price() { return fr_Invest_Unit_Price; }

    public DbsField getFr_Invest_Units() { return fr_Invest_Units; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        fr = newGroupInRecord("fr", "FR");
        fr_Record_Id = fr.newFieldInGroup("fr_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        fr_Record_IdRedef1 = fr.newGroupInGroup("fr_Record_IdRedef1", "Redefines", fr_Record_Id);
        fr_Record_Type = fr_Record_IdRedef1.newFieldInGroup("fr_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        fr_Output_Profile = fr_Record_IdRedef1.newFieldInGroup("fr_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        fr_Reprint_Ind = fr_Record_IdRedef1.newFieldInGroup("fr_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        fr_Contract_Type = fr_Record_IdRedef1.newFieldInGroup("fr_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        fr_Rqst_Id = fr_Record_IdRedef1.newFieldInGroup("fr_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        fr_Tiaa_Numb = fr_Record_IdRedef1.newFieldInGroup("fr_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        fr_Cref_Cert_Numb = fr_Record_IdRedef1.newFieldInGroup("fr_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        fr_From_Invest = fr.newGroupInGroup("fr_From_Invest", "FROM-INVEST");
        fr_Invest_Ind = fr_From_Invest.newFieldInGroup("fr_Invest_Ind", "INVEST-IND", FieldType.STRING, 2);
        fr_From_Invest_Header = fr_From_Invest.newFieldInGroup("fr_From_Invest_Header", "FROM-INVEST-HEADER", FieldType.STRING, 113);
        fr_From_Invest_HeaderRedef2 = fr_From_Invest.newGroupInGroup("fr_From_Invest_HeaderRedef2", "Redefines", fr_From_Invest_Header);
        fr_Plan_Name = fr_From_Invest_HeaderRedef2.newFieldInGroup("fr_Plan_Name", "PLAN-NAME", FieldType.STRING, 64);
        fr_Invest_Tiaa_Num = fr_From_Invest_HeaderRedef2.newFieldInGroup("fr_Invest_Tiaa_Num", "INVEST-TIAA-NUM", FieldType.STRING, 11);
        fr_Invest_Cref_Num = fr_From_Invest_HeaderRedef2.newFieldInGroup("fr_Invest_Cref_Num", "INVEST-CREF-NUM", FieldType.STRING, 11);
        fr_Invest_Orig_Amt = fr_From_Invest_HeaderRedef2.newFieldInGroup("fr_Invest_Orig_Amt", "INVEST-ORIG-AMT", FieldType.STRING, 19);
        fr_Invest_Process_Dt = fr_From_Invest_HeaderRedef2.newFieldInGroup("fr_Invest_Process_Dt", "INVEST-PROCESS-DT", FieldType.STRING, 8);
        fr_From_Invest_Detail = fr_From_Invest.newFieldInGroup("fr_From_Invest_Detail", "FROM-INVEST-DETAIL", FieldType.STRING, 81);
        fr_From_Invest_DetailRedef3 = fr_From_Invest.newGroupInGroup("fr_From_Invest_DetailRedef3", "Redefines", fr_From_Invest_Detail);
        fr_Invest_Fund_Name_From = fr_From_Invest_DetailRedef3.newFieldInGroup("fr_Invest_Fund_Name_From", "INVEST-FUND-NAME-FROM", FieldType.STRING, 
            30);
        fr_Invest_Fund_Amt = fr_From_Invest_DetailRedef3.newFieldInGroup("fr_Invest_Fund_Amt", "INVEST-FUND-AMT", FieldType.STRING, 19);
        fr_Invest_Unit_Price = fr_From_Invest_DetailRedef3.newFieldInGroup("fr_Invest_Unit_Price", "INVEST-UNIT-PRICE", FieldType.STRING, 17);
        fr_Invest_Units = fr_From_Invest_DetailRedef3.newFieldInGroup("fr_Invest_Units", "INVEST-UNITS", FieldType.STRING, 15);

        this.setRecordName("LdaCisl612");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl612() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
