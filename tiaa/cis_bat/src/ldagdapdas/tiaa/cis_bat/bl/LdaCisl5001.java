/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:41 PM
**        * FROM NATURAL LDA     : CISL5001
************************************************************
**        * FILE NAME            : LdaCisl5001.java
**        * CLASS NAME           : LdaCisl5001
**        * INSTANCE NAME        : LdaCisl5001
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl5001 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cis_Bene_File_02;
    private DbsField cis_Bene_File_02_Cis_Rcrd_Type_Cde;
    private DbsField cis_Bene_File_02_Cis_Bene_Tiaa_Nbr;
    private DbsField cis_Bene_File_02_Cis_Bene_Rqst_Id;
    private DbsField cis_Bene_File_02_Cis_Bene_Unique_Id_Nbr;
    private DbsField cis_Bene_File_02_Cis_Bene_Rqst_Id_Key;
    private DbsGroup cis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup;
    private DbsField cis_Bene_File_02_Cis_Bene_Dsgntn_Txt;

    public DataAccessProgramView getVw_cis_Bene_File_02() { return vw_cis_Bene_File_02; }

    public DbsField getCis_Bene_File_02_Cis_Rcrd_Type_Cde() { return cis_Bene_File_02_Cis_Rcrd_Type_Cde; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Tiaa_Nbr() { return cis_Bene_File_02_Cis_Bene_Tiaa_Nbr; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Rqst_Id() { return cis_Bene_File_02_Cis_Bene_Rqst_Id; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Unique_Id_Nbr() { return cis_Bene_File_02_Cis_Bene_Unique_Id_Nbr; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Rqst_Id_Key() { return cis_Bene_File_02_Cis_Bene_Rqst_Id_Key; }

    public DbsGroup getCis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup() { return cis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Dsgntn_Txt() { return cis_Bene_File_02_Cis_Bene_Dsgntn_Txt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cis_Bene_File_02 = new DataAccessProgramView(new NameInfo("vw_cis_Bene_File_02", "CIS-BENE-FILE-02"), "CIS_BENE_FILE_02_12", "CIS_BENE_FILE");
        cis_Bene_File_02_Cis_Rcrd_Type_Cde = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Rcrd_Type_Cde", "CIS-RCRD-TYPE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_RCRD_TYPE_CDE");
        cis_Bene_File_02_Cis_Bene_Tiaa_Nbr = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Tiaa_Nbr", "CIS-BENE-TIAA-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_BENE_TIAA_NBR");
        cis_Bene_File_02_Cis_Bene_Rqst_Id = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Rqst_Id", "CIS-BENE-RQST-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_BENE_RQST_ID");
        cis_Bene_File_02_Cis_Bene_Unique_Id_Nbr = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Unique_Id_Nbr", "CIS-BENE-UNIQUE-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CIS_BENE_UNIQUE_ID_NBR");
        cis_Bene_File_02_Cis_Bene_Rqst_Id_Key = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Rqst_Id_Key", "CIS-BENE-RQST-ID-KEY", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "CIS_BENE_RQST_ID_KEY");
        cis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup = vw_cis_Bene_File_02.getRecord().newGroupInGroup("cis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup", "CIS_BENE_DSGNTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CIS_BENE_FILE_CIS_BENE_DSGNTN_TXT");        cis_Bene_File_02_Cis_Bene_Dsgntn_Txt = cis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup.newFieldArrayInGroup("cis_Bene_File_02_Cis_Bene_Dsgntn_Txt", 
            "CIS-BENE-DSGNTN-TXT", FieldType.STRING, 72, new DbsArrayController(1,60), RepeatingFieldStrategy.SubTableFieldArray, "CIS_BENE_DSGNTN_TXT");

        this.setRecordName("LdaCisl5001");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cis_Bene_File_02.reset();
    }

    // Constructor
    public LdaCisl5001() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
