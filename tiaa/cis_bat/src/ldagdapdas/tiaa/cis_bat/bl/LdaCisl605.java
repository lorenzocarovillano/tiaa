/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:43 PM
**        * FROM NATURAL LDA     : CISL605
************************************************************
**        * FILE NAME            : LdaCisl605.java
**        * CLASS NAME           : LdaCisl605
**        * INSTANCE NAME        : LdaCisl605
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl605 extends DbsRecord
{
    // Properties
    private DbsGroup cm;
    private DbsField cm_Record_Id;
    private DbsGroup cm_Record_IdRedef1;
    private DbsField cm_Record_Type;
    private DbsField cm_Output_Profile;
    private DbsField cm_Reprint_Ind;
    private DbsField cm_Contract_Type;
    private DbsField cm_Rqst_Id;
    private DbsField cm_Tiaa_Numb;
    private DbsField cm_Cref_Cert_Numb;
    private DbsField cm_Comut_Grnted_Amt;
    private DbsField cm_Comut_Intr_Rate;
    private DbsField cm_Comut_Pymt_Method;
    private DbsField cm_Comut_Mort_Basis;

    public DbsGroup getCm() { return cm; }

    public DbsField getCm_Record_Id() { return cm_Record_Id; }

    public DbsGroup getCm_Record_IdRedef1() { return cm_Record_IdRedef1; }

    public DbsField getCm_Record_Type() { return cm_Record_Type; }

    public DbsField getCm_Output_Profile() { return cm_Output_Profile; }

    public DbsField getCm_Reprint_Ind() { return cm_Reprint_Ind; }

    public DbsField getCm_Contract_Type() { return cm_Contract_Type; }

    public DbsField getCm_Rqst_Id() { return cm_Rqst_Id; }

    public DbsField getCm_Tiaa_Numb() { return cm_Tiaa_Numb; }

    public DbsField getCm_Cref_Cert_Numb() { return cm_Cref_Cert_Numb; }

    public DbsField getCm_Comut_Grnted_Amt() { return cm_Comut_Grnted_Amt; }

    public DbsField getCm_Comut_Intr_Rate() { return cm_Comut_Intr_Rate; }

    public DbsField getCm_Comut_Pymt_Method() { return cm_Comut_Pymt_Method; }

    public DbsField getCm_Comut_Mort_Basis() { return cm_Comut_Mort_Basis; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cm = newGroupInRecord("cm", "CM");
        cm_Record_Id = cm.newFieldInGroup("cm_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        cm_Record_IdRedef1 = cm.newGroupInGroup("cm_Record_IdRedef1", "Redefines", cm_Record_Id);
        cm_Record_Type = cm_Record_IdRedef1.newFieldInGroup("cm_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        cm_Output_Profile = cm_Record_IdRedef1.newFieldInGroup("cm_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        cm_Reprint_Ind = cm_Record_IdRedef1.newFieldInGroup("cm_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        cm_Contract_Type = cm_Record_IdRedef1.newFieldInGroup("cm_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        cm_Rqst_Id = cm_Record_IdRedef1.newFieldInGroup("cm_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        cm_Tiaa_Numb = cm_Record_IdRedef1.newFieldInGroup("cm_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        cm_Cref_Cert_Numb = cm_Record_IdRedef1.newFieldInGroup("cm_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        cm_Comut_Grnted_Amt = cm.newFieldInGroup("cm_Comut_Grnted_Amt", "COMUT-GRNTED-AMT", FieldType.STRING, 15);
        cm_Comut_Intr_Rate = cm.newFieldInGroup("cm_Comut_Intr_Rate", "COMUT-INTR-RATE", FieldType.STRING, 5);
        cm_Comut_Pymt_Method = cm.newFieldInGroup("cm_Comut_Pymt_Method", "COMUT-PYMT-METHOD", FieldType.STRING, 8);
        cm_Comut_Mort_Basis = cm.newFieldInGroup("cm_Comut_Mort_Basis", "COMUT-MORT-BASIS", FieldType.STRING, 30);

        this.setRecordName("LdaCisl605");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl605() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
