/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:44 PM
**        * FROM NATURAL LDA     : CISL613
************************************************************
**        * FILE NAME            : LdaCisl613.java
**        * CLASS NAME           : LdaCisl613
**        * INSTANCE NAME        : LdaCisl613
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl613 extends DbsRecord
{
    // Properties
    private DbsGroup to_View;
    private DbsField to_View_Record_Id;
    private DbsGroup to_View_Record_IdRedef1;
    private DbsField to_View_Record_Type;
    private DbsField to_View_Output_Profile;
    private DbsField to_View_Reprint_Ind;
    private DbsField to_View_Contract_Type;
    private DbsField to_View_Rqst_Id;
    private DbsField to_View_Tiaa_Numb;
    private DbsField to_View_Cref_Cert_Numb;
    private DbsGroup to_View_To_Invest;
    private DbsField to_View_Invest_Ind;
    private DbsField to_View_To_Invest_Header;
    private DbsGroup to_View_To_Invest_HeaderRedef2;
    private DbsField to_View_Invest_Tiaa_Num_To;
    private DbsField to_View_Invest_Cref_Num_To;
    private DbsField to_View_Invest_Amt_Total;
    private DbsField to_View_Invest_Process_Dt;
    private DbsField to_View_To_Invest_Detail;
    private DbsGroup to_View_To_Invest_DetailRedef3;
    private DbsField to_View_Invest_Fund_Name_To;
    private DbsField to_View_Invest_Fund_Amt_To;
    private DbsField to_View_Invest_Unit_Price_To;
    private DbsField to_View_Invest_Units_To;

    public DbsGroup getTo_View() { return to_View; }

    public DbsField getTo_View_Record_Id() { return to_View_Record_Id; }

    public DbsGroup getTo_View_Record_IdRedef1() { return to_View_Record_IdRedef1; }

    public DbsField getTo_View_Record_Type() { return to_View_Record_Type; }

    public DbsField getTo_View_Output_Profile() { return to_View_Output_Profile; }

    public DbsField getTo_View_Reprint_Ind() { return to_View_Reprint_Ind; }

    public DbsField getTo_View_Contract_Type() { return to_View_Contract_Type; }

    public DbsField getTo_View_Rqst_Id() { return to_View_Rqst_Id; }

    public DbsField getTo_View_Tiaa_Numb() { return to_View_Tiaa_Numb; }

    public DbsField getTo_View_Cref_Cert_Numb() { return to_View_Cref_Cert_Numb; }

    public DbsGroup getTo_View_To_Invest() { return to_View_To_Invest; }

    public DbsField getTo_View_Invest_Ind() { return to_View_Invest_Ind; }

    public DbsField getTo_View_To_Invest_Header() { return to_View_To_Invest_Header; }

    public DbsGroup getTo_View_To_Invest_HeaderRedef2() { return to_View_To_Invest_HeaderRedef2; }

    public DbsField getTo_View_Invest_Tiaa_Num_To() { return to_View_Invest_Tiaa_Num_To; }

    public DbsField getTo_View_Invest_Cref_Num_To() { return to_View_Invest_Cref_Num_To; }

    public DbsField getTo_View_Invest_Amt_Total() { return to_View_Invest_Amt_Total; }

    public DbsField getTo_View_Invest_Process_Dt() { return to_View_Invest_Process_Dt; }

    public DbsField getTo_View_To_Invest_Detail() { return to_View_To_Invest_Detail; }

    public DbsGroup getTo_View_To_Invest_DetailRedef3() { return to_View_To_Invest_DetailRedef3; }

    public DbsField getTo_View_Invest_Fund_Name_To() { return to_View_Invest_Fund_Name_To; }

    public DbsField getTo_View_Invest_Fund_Amt_To() { return to_View_Invest_Fund_Amt_To; }

    public DbsField getTo_View_Invest_Unit_Price_To() { return to_View_Invest_Unit_Price_To; }

    public DbsField getTo_View_Invest_Units_To() { return to_View_Invest_Units_To; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        to_View = newGroupInRecord("to_View", "TO-VIEW");
        to_View_Record_Id = to_View.newFieldInGroup("to_View_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        to_View_Record_IdRedef1 = to_View.newGroupInGroup("to_View_Record_IdRedef1", "Redefines", to_View_Record_Id);
        to_View_Record_Type = to_View_Record_IdRedef1.newFieldInGroup("to_View_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        to_View_Output_Profile = to_View_Record_IdRedef1.newFieldInGroup("to_View_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        to_View_Reprint_Ind = to_View_Record_IdRedef1.newFieldInGroup("to_View_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        to_View_Contract_Type = to_View_Record_IdRedef1.newFieldInGroup("to_View_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        to_View_Rqst_Id = to_View_Record_IdRedef1.newFieldInGroup("to_View_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        to_View_Tiaa_Numb = to_View_Record_IdRedef1.newFieldInGroup("to_View_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        to_View_Cref_Cert_Numb = to_View_Record_IdRedef1.newFieldInGroup("to_View_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        to_View_To_Invest = to_View.newGroupInGroup("to_View_To_Invest", "TO-INVEST");
        to_View_Invest_Ind = to_View_To_Invest.newFieldInGroup("to_View_Invest_Ind", "INVEST-IND", FieldType.STRING, 2);
        to_View_To_Invest_Header = to_View_To_Invest.newFieldInGroup("to_View_To_Invest_Header", "TO-INVEST-HEADER", FieldType.STRING, 49);
        to_View_To_Invest_HeaderRedef2 = to_View_To_Invest.newGroupInGroup("to_View_To_Invest_HeaderRedef2", "Redefines", to_View_To_Invest_Header);
        to_View_Invest_Tiaa_Num_To = to_View_To_Invest_HeaderRedef2.newFieldInGroup("to_View_Invest_Tiaa_Num_To", "INVEST-TIAA-NUM-TO", FieldType.STRING, 
            11);
        to_View_Invest_Cref_Num_To = to_View_To_Invest_HeaderRedef2.newFieldInGroup("to_View_Invest_Cref_Num_To", "INVEST-CREF-NUM-TO", FieldType.STRING, 
            11);
        to_View_Invest_Amt_Total = to_View_To_Invest_HeaderRedef2.newFieldInGroup("to_View_Invest_Amt_Total", "INVEST-AMT-TOTAL", FieldType.STRING, 19);
        to_View_Invest_Process_Dt = to_View_To_Invest_HeaderRedef2.newFieldInGroup("to_View_Invest_Process_Dt", "INVEST-PROCESS-DT", FieldType.STRING, 
            8);
        to_View_To_Invest_Detail = to_View_To_Invest.newFieldInGroup("to_View_To_Invest_Detail", "TO-INVEST-DETAIL", FieldType.STRING, 81);
        to_View_To_Invest_DetailRedef3 = to_View_To_Invest.newGroupInGroup("to_View_To_Invest_DetailRedef3", "Redefines", to_View_To_Invest_Detail);
        to_View_Invest_Fund_Name_To = to_View_To_Invest_DetailRedef3.newFieldInGroup("to_View_Invest_Fund_Name_To", "INVEST-FUND-NAME-TO", FieldType.STRING, 
            30);
        to_View_Invest_Fund_Amt_To = to_View_To_Invest_DetailRedef3.newFieldInGroup("to_View_Invest_Fund_Amt_To", "INVEST-FUND-AMT-TO", FieldType.STRING, 
            19);
        to_View_Invest_Unit_Price_To = to_View_To_Invest_DetailRedef3.newFieldInGroup("to_View_Invest_Unit_Price_To", "INVEST-UNIT-PRICE-TO", FieldType.STRING, 
            17);
        to_View_Invest_Units_To = to_View_To_Invest_DetailRedef3.newFieldInGroup("to_View_Invest_Units_To", "INVEST-UNITS-TO", FieldType.STRING, 15);

        this.setRecordName("LdaCisl613");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl613() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
