/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:41 PM
**        * FROM NATURAL LDA     : CISL601I
************************************************************
**        * FILE NAME            : LdaCisl601i.java
**        * CLASS NAME           : LdaCisl601i
**        * INSTANCE NAME        : LdaCisl601i
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl601i extends DbsRecord
{
    // Properties
    private DbsGroup ia;
    private DbsField ia_Record_Id;
    private DbsGroup ia_Record_IdRedef1;
    private DbsField ia_Record_Type;
    private DbsField ia_Output_Profile;
    private DbsField ia_Reprint_Ind;
    private DbsField ia_Contract_Type;
    private DbsField ia_Rqst_Id;
    private DbsField ia_Tiaa_Numb;
    private DbsField ia_Cref_Cert_Numb;
    private DbsField ia_Rea_Numb;
    private DbsField ia_Tiaa_Issue_Date;
    private DbsField ia_Cref_Issue_Date;
    private DbsField ia_First_Payt_Date;
    private DbsField ia_Last_Payt_Date;
    private DbsField ia_Payt_Frequency;
    private DbsField ia_Guar_Period;
    private DbsField ia_Guar_Prd_Beg_Date;
    private DbsField ia_Guar_Prd_End_Date;
    private DbsField ia_Annt_Dob;
    private DbsField ia_Annt_Contract_Name;
    private DbsField ia_Annt_Name_Last;
    private DbsField ia_Annt_Name_First;
    private DbsField ia_Annt_Name_Middle;
    private DbsField ia_Annt_Name_Prefix;
    private DbsField ia_Annt_Name_Suffix;
    private DbsField ia_Annt_Citizenship;
    private DbsField ia_Annt_Soc_Numb;
    private DbsField ia_Address_Name;
    private DbsField ia_Welcome_Name;
    private DbsField ia_Directory_Name;
    private DbsField ia_Address_Line;
    private DbsField ia_Zip;
    private DbsField ia_Post_Zip;
    private DbsField ia_Pin_Numb;
    private DbsField ia_Sec_Annt_Contract_Name;
    private DbsField ia_Sec_Annt_Ssn;
    private DbsField ia_Sec_Annt_Dob;
    private DbsField ia_Survivor_Reduction;
    private DbsField ia_Tiaa_Graded_Amt;
    private DbsField ia_Tiaa_Standard_Amt;
    private DbsField ia_Tiaa_Total_Amt;
    private DbsField ia_Guar_Intr_Rate;
    private DbsField ia_Guar;
    private DbsGroup ia_GuarRedef2;
    private DbsGroup ia_Pnd_Redefine_Guar;
    private DbsField ia_Guar_Payt;
    private DbsField ia_Guar_Intr_Rte;
    private DbsField ia_Guar_Payt_Met;
    private DbsField ia_Da_Tiaa_Total_Amt;
    private DbsField ia_Da_Rea_Total_Amt;
    private DbsField ia_Real_Estate_Amt;
    private DbsField ia_Rea_Monthly_Units;
    private DbsField ia_Rea_Annualy_Units;
    private DbsField ia_Rea_Surv_Units;
    private DbsField ia_Seprate_Accnt_Chrg;
    private DbsField ia_Da_Cref_Total_Amt;
    private DbsField ia_Tiaa_Ed_Numb;
    private DbsField ia_Rea_Ed_Numb;
    private DbsField ia_Cref_Ed_Numb;
    private DbsField ia_Tiaa_Form_Numb;
    private DbsField ia_Rea_Form_Numb;
    private DbsField ia_Cref_Form_Numb;
    private DbsField ia_Tiaa_Prod_Cdes;
    private DbsField ia_Rea_Prod_Cdes;
    private DbsField ia_Cref_Prod_Cdes;
    private DbsField ia_Tiaa_Headers;
    private DbsField ia_Rea_Headers;
    private DbsField ia_Cref_Headers;
    private DbsField ia_Income_Option;
    private DbsField ia_State_Name;
    private DbsField ia_Inst_Name;
    private DbsField ia_Res_Issue_State;
    private DbsField ia_Lob;
    private DbsField ia_Lob_Type;
    private DbsField ia_Region;
    private DbsField ia_Branch;
    private DbsField ia_Need;
    private DbsField ia_Ppg_Code;
    private DbsField ia_Rtb_Percent;
    private DbsField ia_Rtb_Amount;
    private DbsField ia_Check_Mail_Addr;
    private DbsField ia_Bank_Accnt_No;
    private DbsField ia_Bank_Transit_Code;
    private DbsField ia_Last_Pymt_Amt;
    private DbsField ia_Cntrct_Apprvl;
    private DbsField ia_Cntrct_Option;
    private DbsField ia_Cntrct_Cash_Status;
    private DbsField ia_Cntrct_Retr_Surv;
    private DbsField ia_Guar_Period_Flag;
    private DbsField ia_Multiple_Intr_Rate;
    private DbsField ia_Tiaa_Cntr_Settled;
    private DbsField ia_Rea_Cntr_Settled;
    private DbsField ia_Cref_Cntr_Settled;
    private DbsField ia_Cref_Accnt_Number;
    private DbsField ia_Internal_Trnsf;
    private DbsField ia_Revaluation_Cde;
    private DbsField ia_Orig_Issue_State;
    private DbsField ia_Ownership_Code;
    private DbsField ia_Rtb_Request;
    private DbsField ia_Issue_Eq_Frst_Pymt;
    private DbsField ia_Estate_Cntgnt;
    private DbsField ia_Freeform_Ben;
    private DbsField ia_Prmry_Ben_Stnd_Txt;
    private DbsField ia_Prmry_Ben_Lump_Sum;
    private DbsField ia_Prmry_Ben_Auto;
    private DbsField ia_Prmry_Ben_Child_Prvsn;
    private DbsField ia_Prmry_Ben_Numb;
    private DbsField ia_Cntgnt_Ben_Stnd_Txt;
    private DbsField ia_Cntgnt_Ben_Lump_Sum;
    private DbsField ia_Cntgnt_Ben_Auto;
    private DbsField ia_Cntgnt_Ben_Child_Prvsn;
    private DbsField ia_Cntgnt_Ben_Numb;
    private DbsField ia_Gsra_Ira_Surr_Chrg;
    private DbsField ia_Da_Death_Sur_Right;
    private DbsField ia_Gra_Surr_Right;
    private DbsField ia_Personal_Annuity;
    private DbsField ia_Mit_Orgnl_Unit_Cde;
    private DbsField ia_Graded_Ind;
    private DbsField ia_Frst_Pymt_After_Iss;
    private DbsField ia_Tiaa_Cntrct_Type;
    private DbsField ia_Rea_Cntrct_Type;
    private DbsField ia_Cref_Cntrct_Type;
    private DbsField ia_Comut_Intr_Num;
    private DbsField ia_Trnsf_Cntr_Settled;
    private DbsField ia_Spec_Cntrct_Type;
    private DbsField ia_Orig_Issue_Date;
    private DbsField ia_Access_Ind;
    private DbsField ia_Access_Amt;
    private DbsField ia_Access_Account;
    private DbsField ia_Access_Mthly_Unit;
    private DbsField ia_Access_Annual_Unit;
    private DbsField ia_Trnsf_Units;
    private DbsField ia_Trnsf_Payout_Cntr;
    private DbsField ia_Trnsf_Fund;
    private DbsField ia_Trnsf_Mode;
    private DbsField ia_Roth_Ind;
    private DbsField ia_Payee_Ind;
    private DbsField ia_Surr_Charge;
    private DbsField ia_Four_Fifty_Seven_Ind;
    private DbsField ia_Trnsf_Cnt_Tiaa;
    private DbsField ia_Trnsf_Cnt_Sepa;
    private DbsField ia_Guid;
    private DbsField ia_Ia_Shr_Class;
    private DbsField ia_Multi_Plan_Ind;
    private DbsField ia_First_Tpa_Or_Ipro_Ind;
    private DbsGroup iaRedef3;
    private DbsField ia_Contract_1;
    private DbsField ia_Contract_2;
    private DbsField ia_Contract_3;
    private DbsField ia_Contract_4;
    private DbsField ia_Contract_5;
    private DbsField ia_Contract_6;
    private DbsField ia_Contract_7;
    private DbsField ia_Contract_8;
    private DbsField ia_Contract_9;
    private DbsField ia_Contract_10;
    private DbsField ia_Contract_11;
    private DbsField ia_Contract_12;

    public DbsGroup getIa() { return ia; }

    public DbsField getIa_Record_Id() { return ia_Record_Id; }

    public DbsGroup getIa_Record_IdRedef1() { return ia_Record_IdRedef1; }

    public DbsField getIa_Record_Type() { return ia_Record_Type; }

    public DbsField getIa_Output_Profile() { return ia_Output_Profile; }

    public DbsField getIa_Reprint_Ind() { return ia_Reprint_Ind; }

    public DbsField getIa_Contract_Type() { return ia_Contract_Type; }

    public DbsField getIa_Rqst_Id() { return ia_Rqst_Id; }

    public DbsField getIa_Tiaa_Numb() { return ia_Tiaa_Numb; }

    public DbsField getIa_Cref_Cert_Numb() { return ia_Cref_Cert_Numb; }

    public DbsField getIa_Rea_Numb() { return ia_Rea_Numb; }

    public DbsField getIa_Tiaa_Issue_Date() { return ia_Tiaa_Issue_Date; }

    public DbsField getIa_Cref_Issue_Date() { return ia_Cref_Issue_Date; }

    public DbsField getIa_First_Payt_Date() { return ia_First_Payt_Date; }

    public DbsField getIa_Last_Payt_Date() { return ia_Last_Payt_Date; }

    public DbsField getIa_Payt_Frequency() { return ia_Payt_Frequency; }

    public DbsField getIa_Guar_Period() { return ia_Guar_Period; }

    public DbsField getIa_Guar_Prd_Beg_Date() { return ia_Guar_Prd_Beg_Date; }

    public DbsField getIa_Guar_Prd_End_Date() { return ia_Guar_Prd_End_Date; }

    public DbsField getIa_Annt_Dob() { return ia_Annt_Dob; }

    public DbsField getIa_Annt_Contract_Name() { return ia_Annt_Contract_Name; }

    public DbsField getIa_Annt_Name_Last() { return ia_Annt_Name_Last; }

    public DbsField getIa_Annt_Name_First() { return ia_Annt_Name_First; }

    public DbsField getIa_Annt_Name_Middle() { return ia_Annt_Name_Middle; }

    public DbsField getIa_Annt_Name_Prefix() { return ia_Annt_Name_Prefix; }

    public DbsField getIa_Annt_Name_Suffix() { return ia_Annt_Name_Suffix; }

    public DbsField getIa_Annt_Citizenship() { return ia_Annt_Citizenship; }

    public DbsField getIa_Annt_Soc_Numb() { return ia_Annt_Soc_Numb; }

    public DbsField getIa_Address_Name() { return ia_Address_Name; }

    public DbsField getIa_Welcome_Name() { return ia_Welcome_Name; }

    public DbsField getIa_Directory_Name() { return ia_Directory_Name; }

    public DbsField getIa_Address_Line() { return ia_Address_Line; }

    public DbsField getIa_Zip() { return ia_Zip; }

    public DbsField getIa_Post_Zip() { return ia_Post_Zip; }

    public DbsField getIa_Pin_Numb() { return ia_Pin_Numb; }

    public DbsField getIa_Sec_Annt_Contract_Name() { return ia_Sec_Annt_Contract_Name; }

    public DbsField getIa_Sec_Annt_Ssn() { return ia_Sec_Annt_Ssn; }

    public DbsField getIa_Sec_Annt_Dob() { return ia_Sec_Annt_Dob; }

    public DbsField getIa_Survivor_Reduction() { return ia_Survivor_Reduction; }

    public DbsField getIa_Tiaa_Graded_Amt() { return ia_Tiaa_Graded_Amt; }

    public DbsField getIa_Tiaa_Standard_Amt() { return ia_Tiaa_Standard_Amt; }

    public DbsField getIa_Tiaa_Total_Amt() { return ia_Tiaa_Total_Amt; }

    public DbsField getIa_Guar_Intr_Rate() { return ia_Guar_Intr_Rate; }

    public DbsField getIa_Guar() { return ia_Guar; }

    public DbsGroup getIa_GuarRedef2() { return ia_GuarRedef2; }

    public DbsGroup getIa_Pnd_Redefine_Guar() { return ia_Pnd_Redefine_Guar; }

    public DbsField getIa_Guar_Payt() { return ia_Guar_Payt; }

    public DbsField getIa_Guar_Intr_Rte() { return ia_Guar_Intr_Rte; }

    public DbsField getIa_Guar_Payt_Met() { return ia_Guar_Payt_Met; }

    public DbsField getIa_Da_Tiaa_Total_Amt() { return ia_Da_Tiaa_Total_Amt; }

    public DbsField getIa_Da_Rea_Total_Amt() { return ia_Da_Rea_Total_Amt; }

    public DbsField getIa_Real_Estate_Amt() { return ia_Real_Estate_Amt; }

    public DbsField getIa_Rea_Monthly_Units() { return ia_Rea_Monthly_Units; }

    public DbsField getIa_Rea_Annualy_Units() { return ia_Rea_Annualy_Units; }

    public DbsField getIa_Rea_Surv_Units() { return ia_Rea_Surv_Units; }

    public DbsField getIa_Seprate_Accnt_Chrg() { return ia_Seprate_Accnt_Chrg; }

    public DbsField getIa_Da_Cref_Total_Amt() { return ia_Da_Cref_Total_Amt; }

    public DbsField getIa_Tiaa_Ed_Numb() { return ia_Tiaa_Ed_Numb; }

    public DbsField getIa_Rea_Ed_Numb() { return ia_Rea_Ed_Numb; }

    public DbsField getIa_Cref_Ed_Numb() { return ia_Cref_Ed_Numb; }

    public DbsField getIa_Tiaa_Form_Numb() { return ia_Tiaa_Form_Numb; }

    public DbsField getIa_Rea_Form_Numb() { return ia_Rea_Form_Numb; }

    public DbsField getIa_Cref_Form_Numb() { return ia_Cref_Form_Numb; }

    public DbsField getIa_Tiaa_Prod_Cdes() { return ia_Tiaa_Prod_Cdes; }

    public DbsField getIa_Rea_Prod_Cdes() { return ia_Rea_Prod_Cdes; }

    public DbsField getIa_Cref_Prod_Cdes() { return ia_Cref_Prod_Cdes; }

    public DbsField getIa_Tiaa_Headers() { return ia_Tiaa_Headers; }

    public DbsField getIa_Rea_Headers() { return ia_Rea_Headers; }

    public DbsField getIa_Cref_Headers() { return ia_Cref_Headers; }

    public DbsField getIa_Income_Option() { return ia_Income_Option; }

    public DbsField getIa_State_Name() { return ia_State_Name; }

    public DbsField getIa_Inst_Name() { return ia_Inst_Name; }

    public DbsField getIa_Res_Issue_State() { return ia_Res_Issue_State; }

    public DbsField getIa_Lob() { return ia_Lob; }

    public DbsField getIa_Lob_Type() { return ia_Lob_Type; }

    public DbsField getIa_Region() { return ia_Region; }

    public DbsField getIa_Branch() { return ia_Branch; }

    public DbsField getIa_Need() { return ia_Need; }

    public DbsField getIa_Ppg_Code() { return ia_Ppg_Code; }

    public DbsField getIa_Rtb_Percent() { return ia_Rtb_Percent; }

    public DbsField getIa_Rtb_Amount() { return ia_Rtb_Amount; }

    public DbsField getIa_Check_Mail_Addr() { return ia_Check_Mail_Addr; }

    public DbsField getIa_Bank_Accnt_No() { return ia_Bank_Accnt_No; }

    public DbsField getIa_Bank_Transit_Code() { return ia_Bank_Transit_Code; }

    public DbsField getIa_Last_Pymt_Amt() { return ia_Last_Pymt_Amt; }

    public DbsField getIa_Cntrct_Apprvl() { return ia_Cntrct_Apprvl; }

    public DbsField getIa_Cntrct_Option() { return ia_Cntrct_Option; }

    public DbsField getIa_Cntrct_Cash_Status() { return ia_Cntrct_Cash_Status; }

    public DbsField getIa_Cntrct_Retr_Surv() { return ia_Cntrct_Retr_Surv; }

    public DbsField getIa_Guar_Period_Flag() { return ia_Guar_Period_Flag; }

    public DbsField getIa_Multiple_Intr_Rate() { return ia_Multiple_Intr_Rate; }

    public DbsField getIa_Tiaa_Cntr_Settled() { return ia_Tiaa_Cntr_Settled; }

    public DbsField getIa_Rea_Cntr_Settled() { return ia_Rea_Cntr_Settled; }

    public DbsField getIa_Cref_Cntr_Settled() { return ia_Cref_Cntr_Settled; }

    public DbsField getIa_Cref_Accnt_Number() { return ia_Cref_Accnt_Number; }

    public DbsField getIa_Internal_Trnsf() { return ia_Internal_Trnsf; }

    public DbsField getIa_Revaluation_Cde() { return ia_Revaluation_Cde; }

    public DbsField getIa_Orig_Issue_State() { return ia_Orig_Issue_State; }

    public DbsField getIa_Ownership_Code() { return ia_Ownership_Code; }

    public DbsField getIa_Rtb_Request() { return ia_Rtb_Request; }

    public DbsField getIa_Issue_Eq_Frst_Pymt() { return ia_Issue_Eq_Frst_Pymt; }

    public DbsField getIa_Estate_Cntgnt() { return ia_Estate_Cntgnt; }

    public DbsField getIa_Freeform_Ben() { return ia_Freeform_Ben; }

    public DbsField getIa_Prmry_Ben_Stnd_Txt() { return ia_Prmry_Ben_Stnd_Txt; }

    public DbsField getIa_Prmry_Ben_Lump_Sum() { return ia_Prmry_Ben_Lump_Sum; }

    public DbsField getIa_Prmry_Ben_Auto() { return ia_Prmry_Ben_Auto; }

    public DbsField getIa_Prmry_Ben_Child_Prvsn() { return ia_Prmry_Ben_Child_Prvsn; }

    public DbsField getIa_Prmry_Ben_Numb() { return ia_Prmry_Ben_Numb; }

    public DbsField getIa_Cntgnt_Ben_Stnd_Txt() { return ia_Cntgnt_Ben_Stnd_Txt; }

    public DbsField getIa_Cntgnt_Ben_Lump_Sum() { return ia_Cntgnt_Ben_Lump_Sum; }

    public DbsField getIa_Cntgnt_Ben_Auto() { return ia_Cntgnt_Ben_Auto; }

    public DbsField getIa_Cntgnt_Ben_Child_Prvsn() { return ia_Cntgnt_Ben_Child_Prvsn; }

    public DbsField getIa_Cntgnt_Ben_Numb() { return ia_Cntgnt_Ben_Numb; }

    public DbsField getIa_Gsra_Ira_Surr_Chrg() { return ia_Gsra_Ira_Surr_Chrg; }

    public DbsField getIa_Da_Death_Sur_Right() { return ia_Da_Death_Sur_Right; }

    public DbsField getIa_Gra_Surr_Right() { return ia_Gra_Surr_Right; }

    public DbsField getIa_Personal_Annuity() { return ia_Personal_Annuity; }

    public DbsField getIa_Mit_Orgnl_Unit_Cde() { return ia_Mit_Orgnl_Unit_Cde; }

    public DbsField getIa_Graded_Ind() { return ia_Graded_Ind; }

    public DbsField getIa_Frst_Pymt_After_Iss() { return ia_Frst_Pymt_After_Iss; }

    public DbsField getIa_Tiaa_Cntrct_Type() { return ia_Tiaa_Cntrct_Type; }

    public DbsField getIa_Rea_Cntrct_Type() { return ia_Rea_Cntrct_Type; }

    public DbsField getIa_Cref_Cntrct_Type() { return ia_Cref_Cntrct_Type; }

    public DbsField getIa_Comut_Intr_Num() { return ia_Comut_Intr_Num; }

    public DbsField getIa_Trnsf_Cntr_Settled() { return ia_Trnsf_Cntr_Settled; }

    public DbsField getIa_Spec_Cntrct_Type() { return ia_Spec_Cntrct_Type; }

    public DbsField getIa_Orig_Issue_Date() { return ia_Orig_Issue_Date; }

    public DbsField getIa_Access_Ind() { return ia_Access_Ind; }

    public DbsField getIa_Access_Amt() { return ia_Access_Amt; }

    public DbsField getIa_Access_Account() { return ia_Access_Account; }

    public DbsField getIa_Access_Mthly_Unit() { return ia_Access_Mthly_Unit; }

    public DbsField getIa_Access_Annual_Unit() { return ia_Access_Annual_Unit; }

    public DbsField getIa_Trnsf_Units() { return ia_Trnsf_Units; }

    public DbsField getIa_Trnsf_Payout_Cntr() { return ia_Trnsf_Payout_Cntr; }

    public DbsField getIa_Trnsf_Fund() { return ia_Trnsf_Fund; }

    public DbsField getIa_Trnsf_Mode() { return ia_Trnsf_Mode; }

    public DbsField getIa_Roth_Ind() { return ia_Roth_Ind; }

    public DbsField getIa_Payee_Ind() { return ia_Payee_Ind; }

    public DbsField getIa_Surr_Charge() { return ia_Surr_Charge; }

    public DbsField getIa_Four_Fifty_Seven_Ind() { return ia_Four_Fifty_Seven_Ind; }

    public DbsField getIa_Trnsf_Cnt_Tiaa() { return ia_Trnsf_Cnt_Tiaa; }

    public DbsField getIa_Trnsf_Cnt_Sepa() { return ia_Trnsf_Cnt_Sepa; }

    public DbsField getIa_Guid() { return ia_Guid; }

    public DbsField getIa_Ia_Shr_Class() { return ia_Ia_Shr_Class; }

    public DbsField getIa_Multi_Plan_Ind() { return ia_Multi_Plan_Ind; }

    public DbsField getIa_First_Tpa_Or_Ipro_Ind() { return ia_First_Tpa_Or_Ipro_Ind; }

    public DbsGroup getIaRedef3() { return iaRedef3; }

    public DbsField getIa_Contract_1() { return ia_Contract_1; }

    public DbsField getIa_Contract_2() { return ia_Contract_2; }

    public DbsField getIa_Contract_3() { return ia_Contract_3; }

    public DbsField getIa_Contract_4() { return ia_Contract_4; }

    public DbsField getIa_Contract_5() { return ia_Contract_5; }

    public DbsField getIa_Contract_6() { return ia_Contract_6; }

    public DbsField getIa_Contract_7() { return ia_Contract_7; }

    public DbsField getIa_Contract_8() { return ia_Contract_8; }

    public DbsField getIa_Contract_9() { return ia_Contract_9; }

    public DbsField getIa_Contract_10() { return ia_Contract_10; }

    public DbsField getIa_Contract_11() { return ia_Contract_11; }

    public DbsField getIa_Contract_12() { return ia_Contract_12; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ia = newGroupInRecord("ia", "IA");
        ia_Record_Id = ia.newFieldInGroup("ia_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        ia_Record_IdRedef1 = ia.newGroupInGroup("ia_Record_IdRedef1", "Redefines", ia_Record_Id);
        ia_Record_Type = ia_Record_IdRedef1.newFieldInGroup("ia_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        ia_Output_Profile = ia_Record_IdRedef1.newFieldInGroup("ia_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        ia_Reprint_Ind = ia_Record_IdRedef1.newFieldInGroup("ia_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        ia_Contract_Type = ia_Record_IdRedef1.newFieldInGroup("ia_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        ia_Rqst_Id = ia_Record_IdRedef1.newFieldInGroup("ia_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        ia_Tiaa_Numb = ia_Record_IdRedef1.newFieldInGroup("ia_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        ia_Cref_Cert_Numb = ia_Record_IdRedef1.newFieldInGroup("ia_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        ia_Rea_Numb = ia.newFieldInGroup("ia_Rea_Numb", "REA-NUMB", FieldType.STRING, 16);
        ia_Tiaa_Issue_Date = ia.newFieldInGroup("ia_Tiaa_Issue_Date", "TIAA-ISSUE-DATE", FieldType.STRING, 8);
        ia_Cref_Issue_Date = ia.newFieldInGroup("ia_Cref_Issue_Date", "CREF-ISSUE-DATE", FieldType.STRING, 8);
        ia_First_Payt_Date = ia.newFieldInGroup("ia_First_Payt_Date", "FIRST-PAYT-DATE", FieldType.STRING, 8);
        ia_Last_Payt_Date = ia.newFieldInGroup("ia_Last_Payt_Date", "LAST-PAYT-DATE", FieldType.STRING, 8);
        ia_Payt_Frequency = ia.newFieldInGroup("ia_Payt_Frequency", "PAYT-FREQUENCY", FieldType.STRING, 13);
        ia_Guar_Period = ia.newFieldInGroup("ia_Guar_Period", "GUAR-PERIOD", FieldType.STRING, 18);
        ia_Guar_Prd_Beg_Date = ia.newFieldInGroup("ia_Guar_Prd_Beg_Date", "GUAR-PRD-BEG-DATE", FieldType.STRING, 14);
        ia_Guar_Prd_End_Date = ia.newFieldInGroup("ia_Guar_Prd_End_Date", "GUAR-PRD-END-DATE", FieldType.STRING, 14);
        ia_Annt_Dob = ia.newFieldInGroup("ia_Annt_Dob", "ANNT-DOB", FieldType.STRING, 8);
        ia_Annt_Contract_Name = ia.newFieldInGroup("ia_Annt_Contract_Name", "ANNT-CONTRACT-NAME", FieldType.STRING, 72);
        ia_Annt_Name_Last = ia.newFieldInGroup("ia_Annt_Name_Last", "ANNT-NAME-LAST", FieldType.STRING, 30);
        ia_Annt_Name_First = ia.newFieldInGroup("ia_Annt_Name_First", "ANNT-NAME-FIRST", FieldType.STRING, 30);
        ia_Annt_Name_Middle = ia.newFieldInGroup("ia_Annt_Name_Middle", "ANNT-NAME-MIDDLE", FieldType.STRING, 30);
        ia_Annt_Name_Prefix = ia.newFieldInGroup("ia_Annt_Name_Prefix", "ANNT-NAME-PREFIX", FieldType.STRING, 8);
        ia_Annt_Name_Suffix = ia.newFieldInGroup("ia_Annt_Name_Suffix", "ANNT-NAME-SUFFIX", FieldType.STRING, 8);
        ia_Annt_Citizenship = ia.newFieldInGroup("ia_Annt_Citizenship", "ANNT-CITIZENSHIP", FieldType.STRING, 25);
        ia_Annt_Soc_Numb = ia.newFieldInGroup("ia_Annt_Soc_Numb", "ANNT-SOC-NUMB", FieldType.STRING, 11);
        ia_Address_Name = ia.newFieldInGroup("ia_Address_Name", "ADDRESS-NAME", FieldType.STRING, 81);
        ia_Welcome_Name = ia.newFieldInGroup("ia_Welcome_Name", "WELCOME-NAME", FieldType.STRING, 39);
        ia_Directory_Name = ia.newFieldInGroup("ia_Directory_Name", "DIRECTORY-NAME", FieldType.STRING, 63);
        ia_Address_Line = ia.newFieldArrayInGroup("ia_Address_Line", "ADDRESS-LINE", FieldType.STRING, 35, new DbsArrayController(1,5));
        ia_Zip = ia.newFieldInGroup("ia_Zip", "ZIP", FieldType.STRING, 9);
        ia_Post_Zip = ia.newFieldInGroup("ia_Post_Zip", "POST-ZIP", FieldType.STRING, 12);
        ia_Pin_Numb = ia.newFieldInGroup("ia_Pin_Numb", "PIN-NUMB", FieldType.STRING, 12);
        ia_Sec_Annt_Contract_Name = ia.newFieldInGroup("ia_Sec_Annt_Contract_Name", "SEC-ANNT-CONTRACT-NAME", FieldType.STRING, 63);
        ia_Sec_Annt_Ssn = ia.newFieldInGroup("ia_Sec_Annt_Ssn", "SEC-ANNT-SSN", FieldType.STRING, 11);
        ia_Sec_Annt_Dob = ia.newFieldInGroup("ia_Sec_Annt_Dob", "SEC-ANNT-DOB", FieldType.STRING, 8);
        ia_Survivor_Reduction = ia.newFieldInGroup("ia_Survivor_Reduction", "SURVIVOR-REDUCTION", FieldType.STRING, 10);
        ia_Tiaa_Graded_Amt = ia.newFieldInGroup("ia_Tiaa_Graded_Amt", "TIAA-GRADED-AMT", FieldType.STRING, 15);
        ia_Tiaa_Standard_Amt = ia.newFieldInGroup("ia_Tiaa_Standard_Amt", "TIAA-STANDARD-AMT", FieldType.STRING, 15);
        ia_Tiaa_Total_Amt = ia.newFieldInGroup("ia_Tiaa_Total_Amt", "TIAA-TOTAL-AMT", FieldType.STRING, 15);
        ia_Guar_Intr_Rate = ia.newFieldInGroup("ia_Guar_Intr_Rate", "GUAR-INTR-RATE", FieldType.STRING, 6);
        ia_Guar = ia.newFieldArrayInGroup("ia_Guar", "GUAR", FieldType.STRING, 28, new DbsArrayController(1,5));
        ia_GuarRedef2 = ia.newGroupInGroup("ia_GuarRedef2", "Redefines", ia_Guar);
        ia_Pnd_Redefine_Guar = ia_GuarRedef2.newGroupArrayInGroup("ia_Pnd_Redefine_Guar", "#REDEFINE-GUAR", new DbsArrayController(1,5));
        ia_Guar_Payt = ia_Pnd_Redefine_Guar.newFieldInGroup("ia_Guar_Payt", "GUAR-PAYT", FieldType.STRING, 15);
        ia_Guar_Intr_Rte = ia_Pnd_Redefine_Guar.newFieldInGroup("ia_Guar_Intr_Rte", "GUAR-INTR-RTE", FieldType.STRING, 5);
        ia_Guar_Payt_Met = ia_Pnd_Redefine_Guar.newFieldInGroup("ia_Guar_Payt_Met", "GUAR-PAYT-MET", FieldType.STRING, 8);
        ia_Da_Tiaa_Total_Amt = ia.newFieldInGroup("ia_Da_Tiaa_Total_Amt", "DA-TIAA-TOTAL-AMT", FieldType.STRING, 15);
        ia_Da_Rea_Total_Amt = ia.newFieldInGroup("ia_Da_Rea_Total_Amt", "DA-REA-TOTAL-AMT", FieldType.STRING, 15);
        ia_Real_Estate_Amt = ia.newFieldInGroup("ia_Real_Estate_Amt", "REAL-ESTATE-AMT", FieldType.STRING, 15);
        ia_Rea_Monthly_Units = ia.newFieldInGroup("ia_Rea_Monthly_Units", "REA-MONTHLY-UNITS", FieldType.STRING, 12);
        ia_Rea_Annualy_Units = ia.newFieldInGroup("ia_Rea_Annualy_Units", "REA-ANNUALY-UNITS", FieldType.STRING, 12);
        ia_Rea_Surv_Units = ia.newFieldInGroup("ia_Rea_Surv_Units", "REA-SURV-UNITS", FieldType.STRING, 12);
        ia_Seprate_Accnt_Chrg = ia.newFieldInGroup("ia_Seprate_Accnt_Chrg", "SEPRATE-ACCNT-CHRG", FieldType.STRING, 6);
        ia_Da_Cref_Total_Amt = ia.newFieldInGroup("ia_Da_Cref_Total_Amt", "DA-CREF-TOTAL-AMT", FieldType.STRING, 15);
        ia_Tiaa_Ed_Numb = ia.newFieldArrayInGroup("ia_Tiaa_Ed_Numb", "TIAA-ED-NUMB", FieldType.STRING, 15, new DbsArrayController(1,2));
        ia_Rea_Ed_Numb = ia.newFieldArrayInGroup("ia_Rea_Ed_Numb", "REA-ED-NUMB", FieldType.STRING, 15, new DbsArrayController(1,2));
        ia_Cref_Ed_Numb = ia.newFieldArrayInGroup("ia_Cref_Ed_Numb", "CREF-ED-NUMB", FieldType.STRING, 15, new DbsArrayController(1,2));
        ia_Tiaa_Form_Numb = ia.newFieldArrayInGroup("ia_Tiaa_Form_Numb", "TIAA-FORM-NUMB", FieldType.STRING, 20, new DbsArrayController(1,2));
        ia_Rea_Form_Numb = ia.newFieldArrayInGroup("ia_Rea_Form_Numb", "REA-FORM-NUMB", FieldType.STRING, 20, new DbsArrayController(1,2));
        ia_Cref_Form_Numb = ia.newFieldArrayInGroup("ia_Cref_Form_Numb", "CREF-FORM-NUMB", FieldType.STRING, 20, new DbsArrayController(1,2));
        ia_Tiaa_Prod_Cdes = ia.newFieldArrayInGroup("ia_Tiaa_Prod_Cdes", "TIAA-PROD-CDES", FieldType.STRING, 35, new DbsArrayController(1,2));
        ia_Rea_Prod_Cdes = ia.newFieldArrayInGroup("ia_Rea_Prod_Cdes", "REA-PROD-CDES", FieldType.STRING, 35, new DbsArrayController(1,2));
        ia_Cref_Prod_Cdes = ia.newFieldArrayInGroup("ia_Cref_Prod_Cdes", "CREF-PROD-CDES", FieldType.STRING, 35, new DbsArrayController(1,2));
        ia_Tiaa_Headers = ia.newFieldArrayInGroup("ia_Tiaa_Headers", "TIAA-HEADERS", FieldType.STRING, 85, new DbsArrayController(1,2));
        ia_Rea_Headers = ia.newFieldArrayInGroup("ia_Rea_Headers", "REA-HEADERS", FieldType.STRING, 85, new DbsArrayController(1,2));
        ia_Cref_Headers = ia.newFieldArrayInGroup("ia_Cref_Headers", "CREF-HEADERS", FieldType.STRING, 85, new DbsArrayController(1,2));
        ia_Income_Option = ia.newFieldArrayInGroup("ia_Income_Option", "INCOME-OPTION", FieldType.STRING, 85, new DbsArrayController(1,2));
        ia_State_Name = ia.newFieldInGroup("ia_State_Name", "STATE-NAME", FieldType.STRING, 15);
        ia_Inst_Name = ia.newFieldInGroup("ia_Inst_Name", "INST-NAME", FieldType.STRING, 76);
        ia_Res_Issue_State = ia.newFieldInGroup("ia_Res_Issue_State", "RES-ISSUE-STATE", FieldType.STRING, 2);
        ia_Lob = ia.newFieldInGroup("ia_Lob", "LOB", FieldType.STRING, 1);
        ia_Lob_Type = ia.newFieldInGroup("ia_Lob_Type", "LOB-TYPE", FieldType.STRING, 1);
        ia_Region = ia.newFieldInGroup("ia_Region", "REGION", FieldType.STRING, 1);
        ia_Branch = ia.newFieldInGroup("ia_Branch", "BRANCH", FieldType.STRING, 1);
        ia_Need = ia.newFieldInGroup("ia_Need", "NEED", FieldType.STRING, 1);
        ia_Ppg_Code = ia.newFieldInGroup("ia_Ppg_Code", "PPG-CODE", FieldType.STRING, 6);
        ia_Rtb_Percent = ia.newFieldInGroup("ia_Rtb_Percent", "RTB-PERCENT", FieldType.STRING, 3);
        ia_Rtb_Amount = ia.newFieldInGroup("ia_Rtb_Amount", "RTB-AMOUNT", FieldType.STRING, 15);
        ia_Check_Mail_Addr = ia.newFieldArrayInGroup("ia_Check_Mail_Addr", "CHECK-MAIL-ADDR", FieldType.STRING, 35, new DbsArrayController(1,5));
        ia_Bank_Accnt_No = ia.newFieldInGroup("ia_Bank_Accnt_No", "BANK-ACCNT-NO", FieldType.STRING, 21);
        ia_Bank_Transit_Code = ia.newFieldInGroup("ia_Bank_Transit_Code", "BANK-TRANSIT-CODE", FieldType.STRING, 9);
        ia_Last_Pymt_Amt = ia.newFieldInGroup("ia_Last_Pymt_Amt", "LAST-PYMT-AMT", FieldType.STRING, 15);
        ia_Cntrct_Apprvl = ia.newFieldInGroup("ia_Cntrct_Apprvl", "CNTRCT-APPRVL", FieldType.STRING, 1);
        ia_Cntrct_Option = ia.newFieldInGroup("ia_Cntrct_Option", "CNTRCT-OPTION", FieldType.STRING, 10);
        ia_Cntrct_Cash_Status = ia.newFieldInGroup("ia_Cntrct_Cash_Status", "CNTRCT-CASH-STATUS", FieldType.STRING, 1);
        ia_Cntrct_Retr_Surv = ia.newFieldInGroup("ia_Cntrct_Retr_Surv", "CNTRCT-RETR-SURV", FieldType.STRING, 1);
        ia_Guar_Period_Flag = ia.newFieldInGroup("ia_Guar_Period_Flag", "GUAR-PERIOD-FLAG", FieldType.STRING, 1);
        ia_Multiple_Intr_Rate = ia.newFieldInGroup("ia_Multiple_Intr_Rate", "MULTIPLE-INTR-RATE", FieldType.STRING, 1);
        ia_Tiaa_Cntr_Settled = ia.newFieldInGroup("ia_Tiaa_Cntr_Settled", "TIAA-CNTR-SETTLED", FieldType.STRING, 2);
        ia_Rea_Cntr_Settled = ia.newFieldInGroup("ia_Rea_Cntr_Settled", "REA-CNTR-SETTLED", FieldType.STRING, 2);
        ia_Cref_Cntr_Settled = ia.newFieldInGroup("ia_Cref_Cntr_Settled", "CREF-CNTR-SETTLED", FieldType.STRING, 2);
        ia_Cref_Accnt_Number = ia.newFieldInGroup("ia_Cref_Accnt_Number", "CREF-ACCNT-NUMBER", FieldType.STRING, 2);
        ia_Internal_Trnsf = ia.newFieldInGroup("ia_Internal_Trnsf", "INTERNAL-TRNSF", FieldType.STRING, 1);
        ia_Revaluation_Cde = ia.newFieldInGroup("ia_Revaluation_Cde", "REVALUATION-CDE", FieldType.STRING, 1);
        ia_Orig_Issue_State = ia.newFieldInGroup("ia_Orig_Issue_State", "ORIG-ISSUE-STATE", FieldType.STRING, 2);
        ia_Ownership_Code = ia.newFieldInGroup("ia_Ownership_Code", "OWNERSHIP-CODE", FieldType.STRING, 1);
        ia_Rtb_Request = ia.newFieldInGroup("ia_Rtb_Request", "RTB-REQUEST", FieldType.STRING, 1);
        ia_Issue_Eq_Frst_Pymt = ia.newFieldInGroup("ia_Issue_Eq_Frst_Pymt", "ISSUE-EQ-FRST-PYMT", FieldType.STRING, 1);
        ia_Estate_Cntgnt = ia.newFieldInGroup("ia_Estate_Cntgnt", "ESTATE-CNTGNT", FieldType.STRING, 1);
        ia_Freeform_Ben = ia.newFieldInGroup("ia_Freeform_Ben", "FREEFORM-BEN", FieldType.STRING, 1);
        ia_Prmry_Ben_Stnd_Txt = ia.newFieldInGroup("ia_Prmry_Ben_Stnd_Txt", "PRMRY-BEN-STND-TXT", FieldType.STRING, 1);
        ia_Prmry_Ben_Lump_Sum = ia.newFieldInGroup("ia_Prmry_Ben_Lump_Sum", "PRMRY-BEN-LUMP-SUM", FieldType.STRING, 1);
        ia_Prmry_Ben_Auto = ia.newFieldInGroup("ia_Prmry_Ben_Auto", "PRMRY-BEN-AUTO", FieldType.STRING, 1);
        ia_Prmry_Ben_Child_Prvsn = ia.newFieldInGroup("ia_Prmry_Ben_Child_Prvsn", "PRMRY-BEN-CHILD-PRVSN", FieldType.STRING, 1);
        ia_Prmry_Ben_Numb = ia.newFieldInGroup("ia_Prmry_Ben_Numb", "PRMRY-BEN-NUMB", FieldType.STRING, 2);
        ia_Cntgnt_Ben_Stnd_Txt = ia.newFieldInGroup("ia_Cntgnt_Ben_Stnd_Txt", "CNTGNT-BEN-STND-TXT", FieldType.STRING, 1);
        ia_Cntgnt_Ben_Lump_Sum = ia.newFieldInGroup("ia_Cntgnt_Ben_Lump_Sum", "CNTGNT-BEN-LUMP-SUM", FieldType.STRING, 1);
        ia_Cntgnt_Ben_Auto = ia.newFieldInGroup("ia_Cntgnt_Ben_Auto", "CNTGNT-BEN-AUTO", FieldType.STRING, 1);
        ia_Cntgnt_Ben_Child_Prvsn = ia.newFieldInGroup("ia_Cntgnt_Ben_Child_Prvsn", "CNTGNT-BEN-CHILD-PRVSN", FieldType.STRING, 1);
        ia_Cntgnt_Ben_Numb = ia.newFieldInGroup("ia_Cntgnt_Ben_Numb", "CNTGNT-BEN-NUMB", FieldType.STRING, 2);
        ia_Gsra_Ira_Surr_Chrg = ia.newFieldInGroup("ia_Gsra_Ira_Surr_Chrg", "GSRA-IRA-SURR-CHRG", FieldType.STRING, 1);
        ia_Da_Death_Sur_Right = ia.newFieldInGroup("ia_Da_Death_Sur_Right", "DA-DEATH-SUR-RIGHT", FieldType.STRING, 1);
        ia_Gra_Surr_Right = ia.newFieldInGroup("ia_Gra_Surr_Right", "GRA-SURR-RIGHT", FieldType.STRING, 1);
        ia_Personal_Annuity = ia.newFieldInGroup("ia_Personal_Annuity", "PERSONAL-ANNUITY", FieldType.STRING, 1);
        ia_Mit_Orgnl_Unit_Cde = ia.newFieldInGroup("ia_Mit_Orgnl_Unit_Cde", "MIT-ORGNL-UNIT-CDE", FieldType.STRING, 8);
        ia_Graded_Ind = ia.newFieldInGroup("ia_Graded_Ind", "GRADED-IND", FieldType.STRING, 1);
        ia_Frst_Pymt_After_Iss = ia.newFieldInGroup("ia_Frst_Pymt_After_Iss", "FRST-PYMT-AFTER-ISS", FieldType.STRING, 1);
        ia_Tiaa_Cntrct_Type = ia.newFieldInGroup("ia_Tiaa_Cntrct_Type", "TIAA-CNTRCT-TYPE", FieldType.STRING, 1);
        ia_Rea_Cntrct_Type = ia.newFieldInGroup("ia_Rea_Cntrct_Type", "REA-CNTRCT-TYPE", FieldType.STRING, 1);
        ia_Cref_Cntrct_Type = ia.newFieldInGroup("ia_Cref_Cntrct_Type", "CREF-CNTRCT-TYPE", FieldType.STRING, 1);
        ia_Comut_Intr_Num = ia.newFieldInGroup("ia_Comut_Intr_Num", "COMUT-INTR-NUM", FieldType.STRING, 2);
        ia_Trnsf_Cntr_Settled = ia.newFieldInGroup("ia_Trnsf_Cntr_Settled", "TRNSF-CNTR-SETTLED", FieldType.STRING, 2);
        ia_Spec_Cntrct_Type = ia.newFieldInGroup("ia_Spec_Cntrct_Type", "SPEC-CNTRCT-TYPE", FieldType.STRING, 1);
        ia_Orig_Issue_Date = ia.newFieldInGroup("ia_Orig_Issue_Date", "ORIG-ISSUE-DATE", FieldType.STRING, 8);
        ia_Access_Ind = ia.newFieldInGroup("ia_Access_Ind", "ACCESS-IND", FieldType.STRING, 4);
        ia_Access_Amt = ia.newFieldInGroup("ia_Access_Amt", "ACCESS-AMT", FieldType.STRING, 14);
        ia_Access_Account = ia.newFieldInGroup("ia_Access_Account", "ACCESS-ACCOUNT", FieldType.STRING, 30);
        ia_Access_Mthly_Unit = ia.newFieldInGroup("ia_Access_Mthly_Unit", "ACCESS-MTHLY-UNIT", FieldType.STRING, 12);
        ia_Access_Annual_Unit = ia.newFieldInGroup("ia_Access_Annual_Unit", "ACCESS-ANNUAL-UNIT", FieldType.STRING, 12);
        ia_Trnsf_Units = ia.newFieldInGroup("ia_Trnsf_Units", "TRNSF-UNITS", FieldType.STRING, 9);
        ia_Trnsf_Payout_Cntr = ia.newFieldInGroup("ia_Trnsf_Payout_Cntr", "TRNSF-PAYOUT-CNTR", FieldType.STRING, 12);
        ia_Trnsf_Fund = ia.newFieldInGroup("ia_Trnsf_Fund", "TRNSF-FUND", FieldType.STRING, 30);
        ia_Trnsf_Mode = ia.newFieldInGroup("ia_Trnsf_Mode", "TRNSF-MODE", FieldType.STRING, 13);
        ia_Roth_Ind = ia.newFieldInGroup("ia_Roth_Ind", "ROTH-IND", FieldType.STRING, 1);
        ia_Payee_Ind = ia.newFieldInGroup("ia_Payee_Ind", "PAYEE-IND", FieldType.STRING, 1);
        ia_Surr_Charge = ia.newFieldInGroup("ia_Surr_Charge", "SURR-CHARGE", FieldType.STRING, 4);
        ia_Four_Fifty_Seven_Ind = ia.newFieldInGroup("ia_Four_Fifty_Seven_Ind", "FOUR-FIFTY-SEVEN-IND", FieldType.STRING, 1);
        ia_Trnsf_Cnt_Tiaa = ia.newFieldInGroup("ia_Trnsf_Cnt_Tiaa", "TRNSF-CNT-TIAA", FieldType.STRING, 2);
        ia_Trnsf_Cnt_Sepa = ia.newFieldInGroup("ia_Trnsf_Cnt_Sepa", "TRNSF-CNT-SEPA", FieldType.STRING, 2);
        ia_Guid = ia.newFieldInGroup("ia_Guid", "GUID", FieldType.NUMERIC, 5);
        ia_Ia_Shr_Class = ia.newFieldInGroup("ia_Ia_Shr_Class", "IA-SHR-CLASS", FieldType.STRING, 2);
        ia_Multi_Plan_Ind = ia.newFieldInGroup("ia_Multi_Plan_Ind", "MULTI-PLAN-IND", FieldType.STRING, 1);
        ia_First_Tpa_Or_Ipro_Ind = ia.newFieldInGroup("ia_First_Tpa_Or_Ipro_Ind", "FIRST-TPA-OR-IPRO-IND", FieldType.STRING, 1);
        iaRedef3 = newGroupInRecord("iaRedef3", "Redefines", ia);
        ia_Contract_1 = iaRedef3.newFieldInGroup("ia_Contract_1", "CONTRACT-1", FieldType.STRING, 250);
        ia_Contract_2 = iaRedef3.newFieldInGroup("ia_Contract_2", "CONTRACT-2", FieldType.STRING, 250);
        ia_Contract_3 = iaRedef3.newFieldInGroup("ia_Contract_3", "CONTRACT-3", FieldType.STRING, 250);
        ia_Contract_4 = iaRedef3.newFieldInGroup("ia_Contract_4", "CONTRACT-4", FieldType.STRING, 250);
        ia_Contract_5 = iaRedef3.newFieldInGroup("ia_Contract_5", "CONTRACT-5", FieldType.STRING, 250);
        ia_Contract_6 = iaRedef3.newFieldInGroup("ia_Contract_6", "CONTRACT-6", FieldType.STRING, 250);
        ia_Contract_7 = iaRedef3.newFieldInGroup("ia_Contract_7", "CONTRACT-7", FieldType.STRING, 250);
        ia_Contract_8 = iaRedef3.newFieldInGroup("ia_Contract_8", "CONTRACT-8", FieldType.STRING, 250);
        ia_Contract_9 = iaRedef3.newFieldInGroup("ia_Contract_9", "CONTRACT-9", FieldType.STRING, 250);
        ia_Contract_10 = iaRedef3.newFieldInGroup("ia_Contract_10", "CONTRACT-10", FieldType.STRING, 250);
        ia_Contract_11 = iaRedef3.newFieldInGroup("ia_Contract_11", "CONTRACT-11", FieldType.STRING, 250);
        ia_Contract_12 = iaRedef3.newFieldInGroup("ia_Contract_12", "CONTRACT-12", FieldType.STRING, 59);

        this.setRecordName("LdaCisl601i");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl601i() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
