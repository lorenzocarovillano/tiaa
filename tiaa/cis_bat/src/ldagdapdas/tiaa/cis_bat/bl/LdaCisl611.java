/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:43 PM
**        * FROM NATURAL LDA     : CISL611
************************************************************
**        * FILE NAME            : LdaCisl611.java
**        * CLASS NAME           : LdaCisl611
**        * INSTANCE NAME        : LdaCisl611
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl611 extends DbsRecord
{
    // Properties
    private DbsGroup st;
    private DbsField st_Record_Id;
    private DbsGroup st_Record_IdRedef1;
    private DbsField st_Record_Type;
    private DbsField st_Output_Profile;
    private DbsField st_Reprint_Ind;
    private DbsField st_Contract_Type;
    private DbsField st_Rqst_Id;
    private DbsField st_Tiaa_Numb;
    private DbsField st_Cref_Cert_Numb;
    private DbsField st_Sepa_Cref_Units;
    private DbsField st_Sepa_Ticker_Name;
    private DbsField st_Sepa_Reval_Type;

    public DbsGroup getSt() { return st; }

    public DbsField getSt_Record_Id() { return st_Record_Id; }

    public DbsGroup getSt_Record_IdRedef1() { return st_Record_IdRedef1; }

    public DbsField getSt_Record_Type() { return st_Record_Type; }

    public DbsField getSt_Output_Profile() { return st_Output_Profile; }

    public DbsField getSt_Reprint_Ind() { return st_Reprint_Ind; }

    public DbsField getSt_Contract_Type() { return st_Contract_Type; }

    public DbsField getSt_Rqst_Id() { return st_Rqst_Id; }

    public DbsField getSt_Tiaa_Numb() { return st_Tiaa_Numb; }

    public DbsField getSt_Cref_Cert_Numb() { return st_Cref_Cert_Numb; }

    public DbsField getSt_Sepa_Cref_Units() { return st_Sepa_Cref_Units; }

    public DbsField getSt_Sepa_Ticker_Name() { return st_Sepa_Ticker_Name; }

    public DbsField getSt_Sepa_Reval_Type() { return st_Sepa_Reval_Type; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        st = newGroupInRecord("st", "ST");
        st_Record_Id = st.newFieldInGroup("st_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        st_Record_IdRedef1 = st.newGroupInGroup("st_Record_IdRedef1", "Redefines", st_Record_Id);
        st_Record_Type = st_Record_IdRedef1.newFieldInGroup("st_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        st_Output_Profile = st_Record_IdRedef1.newFieldInGroup("st_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        st_Reprint_Ind = st_Record_IdRedef1.newFieldInGroup("st_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        st_Contract_Type = st_Record_IdRedef1.newFieldInGroup("st_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        st_Rqst_Id = st_Record_IdRedef1.newFieldInGroup("st_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        st_Tiaa_Numb = st_Record_IdRedef1.newFieldInGroup("st_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        st_Cref_Cert_Numb = st_Record_IdRedef1.newFieldInGroup("st_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        st_Sepa_Cref_Units = st.newFieldInGroup("st_Sepa_Cref_Units", "SEPA-CREF-UNITS", FieldType.STRING, 8);
        st_Sepa_Ticker_Name = st.newFieldInGroup("st_Sepa_Ticker_Name", "SEPA-TICKER-NAME", FieldType.STRING, 27);
        st_Sepa_Reval_Type = st.newFieldInGroup("st_Sepa_Reval_Type", "SEPA-REVAL-TYPE", FieldType.STRING, 15);

        this.setRecordName("LdaCisl611");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl611() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
