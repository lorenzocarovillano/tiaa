/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:42 PM
**        * FROM NATURAL LDA     : CISL602
************************************************************
**        * FILE NAME            : LdaCisl602.java
**        * CLASS NAME           : LdaCisl602
**        * INSTANCE NAME        : LdaCisl602
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl602 extends DbsRecord
{
    // Properties
    private DbsGroup pb;
    private DbsField pb_Record_Id;
    private DbsGroup pb_Record_IdRedef1;
    private DbsField pb_Record_Type;
    private DbsField pb_Output_Profile;
    private DbsField pb_Reprint_Ind;
    private DbsField pb_Contract_Type;
    private DbsField pb_Rqst_Id;
    private DbsField pb_Tiaa_Numb;
    private DbsField pb_Cref_Cert_Numb;
    private DbsField pb_Primary_Bene_Line;

    public DbsGroup getPb() { return pb; }

    public DbsField getPb_Record_Id() { return pb_Record_Id; }

    public DbsGroup getPb_Record_IdRedef1() { return pb_Record_IdRedef1; }

    public DbsField getPb_Record_Type() { return pb_Record_Type; }

    public DbsField getPb_Output_Profile() { return pb_Output_Profile; }

    public DbsField getPb_Reprint_Ind() { return pb_Reprint_Ind; }

    public DbsField getPb_Contract_Type() { return pb_Contract_Type; }

    public DbsField getPb_Rqst_Id() { return pb_Rqst_Id; }

    public DbsField getPb_Tiaa_Numb() { return pb_Tiaa_Numb; }

    public DbsField getPb_Cref_Cert_Numb() { return pb_Cref_Cert_Numb; }

    public DbsField getPb_Primary_Bene_Line() { return pb_Primary_Bene_Line; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pb = newGroupInRecord("pb", "PB");
        pb_Record_Id = pb.newFieldInGroup("pb_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        pb_Record_IdRedef1 = pb.newGroupInGroup("pb_Record_IdRedef1", "Redefines", pb_Record_Id);
        pb_Record_Type = pb_Record_IdRedef1.newFieldInGroup("pb_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        pb_Output_Profile = pb_Record_IdRedef1.newFieldInGroup("pb_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        pb_Reprint_Ind = pb_Record_IdRedef1.newFieldInGroup("pb_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        pb_Contract_Type = pb_Record_IdRedef1.newFieldInGroup("pb_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        pb_Rqst_Id = pb_Record_IdRedef1.newFieldInGroup("pb_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        pb_Tiaa_Numb = pb_Record_IdRedef1.newFieldInGroup("pb_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        pb_Cref_Cert_Numb = pb_Record_IdRedef1.newFieldInGroup("pb_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        pb_Primary_Bene_Line = pb.newFieldInGroup("pb_Primary_Bene_Line", "PRIMARY-BENE-LINE", FieldType.STRING, 85);

        this.setRecordName("LdaCisl602");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl602() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
