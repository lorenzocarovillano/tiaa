/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:23 PM
**        * FROM NATURAL PDA     : CISA601
************************************************************
**        * FILE NAME            : PdaCisa601.java
**        * CLASS NAME           : PdaCisa601
**        * INSTANCE NAME        : PdaCisa601
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCisa601 extends PdaBase
{
    // Properties
    private DbsGroup cisa601;
    private DbsField cisa601_Pnd_Todays_Date;
    private DbsField cisa601_Pnd_Eff_Date;
    private DbsField cisa601_Pnd_Pin;
    private DbsField cisa601_Pnd_Tiaa_Cntrct;
    private DbsField cisa601_Pnd_Bene_Name;
    private DbsField cisa601_Pnd_Total_Bene_Contract;
    private DbsField cisa601_Pnd_Total_Bene_Mos;
    private DbsField cisa601_Pnd_Total_Bene_Dest;
    private DbsField cisa601_Pnd_Error_Code;

    public DbsGroup getCisa601() { return cisa601; }

    public DbsField getCisa601_Pnd_Todays_Date() { return cisa601_Pnd_Todays_Date; }

    public DbsField getCisa601_Pnd_Eff_Date() { return cisa601_Pnd_Eff_Date; }

    public DbsField getCisa601_Pnd_Pin() { return cisa601_Pnd_Pin; }

    public DbsField getCisa601_Pnd_Tiaa_Cntrct() { return cisa601_Pnd_Tiaa_Cntrct; }

    public DbsField getCisa601_Pnd_Bene_Name() { return cisa601_Pnd_Bene_Name; }

    public DbsField getCisa601_Pnd_Total_Bene_Contract() { return cisa601_Pnd_Total_Bene_Contract; }

    public DbsField getCisa601_Pnd_Total_Bene_Mos() { return cisa601_Pnd_Total_Bene_Mos; }

    public DbsField getCisa601_Pnd_Total_Bene_Dest() { return cisa601_Pnd_Total_Bene_Dest; }

    public DbsField getCisa601_Pnd_Error_Code() { return cisa601_Pnd_Error_Code; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cisa601 = dbsRecord.newGroupInRecord("cisa601", "CISA601");
        cisa601.setParameterOption(ParameterOption.ByReference);
        cisa601_Pnd_Todays_Date = cisa601.newFieldInGroup("cisa601_Pnd_Todays_Date", "#TODAYS-DATE", FieldType.STRING, 8);
        cisa601_Pnd_Eff_Date = cisa601.newFieldInGroup("cisa601_Pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);
        cisa601_Pnd_Pin = cisa601.newFieldInGroup("cisa601_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        cisa601_Pnd_Tiaa_Cntrct = cisa601.newFieldInGroup("cisa601_Pnd_Tiaa_Cntrct", "#TIAA-CNTRCT", FieldType.STRING, 10);
        cisa601_Pnd_Bene_Name = cisa601.newFieldInGroup("cisa601_Pnd_Bene_Name", "#BENE-NAME", FieldType.STRING, 35);
        cisa601_Pnd_Total_Bene_Contract = cisa601.newFieldInGroup("cisa601_Pnd_Total_Bene_Contract", "#TOTAL-BENE-CONTRACT", FieldType.NUMERIC, 10);
        cisa601_Pnd_Total_Bene_Mos = cisa601.newFieldInGroup("cisa601_Pnd_Total_Bene_Mos", "#TOTAL-BENE-MOS", FieldType.NUMERIC, 10);
        cisa601_Pnd_Total_Bene_Dest = cisa601.newFieldInGroup("cisa601_Pnd_Total_Bene_Dest", "#TOTAL-BENE-DEST", FieldType.NUMERIC, 10);
        cisa601_Pnd_Error_Code = cisa601.newFieldInGroup("cisa601_Pnd_Error_Code", "#ERROR-CODE", FieldType.STRING, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCisa601(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

