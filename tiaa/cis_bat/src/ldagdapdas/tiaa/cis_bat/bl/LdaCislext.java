/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:44 PM
**        * FROM NATURAL LDA     : CISLEXT
************************************************************
**        * FILE NAME            : LdaCislext.java
**        * CLASS NAME           : LdaCislext
**        * INSTANCE NAME        : LdaCislext
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCislext extends DbsRecord
{
    // Properties
    private DbsGroup ex;
    private DbsField ex_Print_Jobname;
    private DbsField ex_Job_Description;
    private DbsField ex_Contract_Type;
    private DbsField ex_Cntrct_Apprvl;
    private DbsField ex_Rqst_Id;
    private DbsField ex_Cntrct_Option;
    private DbsField ex_Cntrct_Cash_Status;
    private DbsField ex_Cntrct_Retr_Surv;
    private DbsField ex_Tiaa_Headers;
    private DbsField ex_Cref_Headers;
    private DbsField ex_Rea_Headers;
    private DbsField ex_Income_Option;
    private DbsField ex_Eop_Mergeset_Id;
    private DbsField ex_Tiaa_Numb;
    private DbsField ex_Rea_Numb;
    private DbsField ex_Cref_Cert_Numb;
    private DbsField ex_Tiaa_Issue_Date;
    private DbsField ex_Cref_Issue_Date;
    private DbsField ex_Payt_Frequency;
    private DbsField ex_First_Payt_Date;
    private DbsField ex_Last_Payt_Date;
    private DbsField ex_Guar_Period;
    private DbsField ex_Guar_Prd_Beg_Date;
    private DbsField ex_Guar_Prd_End_Date;
    private DbsField ex_Tiaa_Form_Numb;
    private DbsField ex_Rea_Form_Numb;
    private DbsField ex_Cref_Form_Numb;
    private DbsField ex_Tiaa_Prod_Cdes;
    private DbsField ex_Rea_Prod_Cdes;
    private DbsField ex_Cref_Prod_Cdes;
    private DbsField ex_Tiaa_Ed_Numb;
    private DbsField ex_Rea_Ed_Numb;
    private DbsField ex_Cref_Ed_Numb;
    private DbsField ex_Pin_Numb;
    private DbsField ex_Annt_Contract_Name;
    private DbsField ex_Annt_Name_Last;
    private DbsField ex_Annt_Name_First;
    private DbsField ex_Annt_Name_Middle;
    private DbsField ex_Annt_Name_Prefix;
    private DbsField ex_Annt_Name_Suffix;
    private DbsField ex_Annt_Citizenship;
    private DbsField ex_Annt_Soc_Numb;
    private DbsField ex_Annt_Dob;
    private DbsField ex_First_Annu_Res_State;
    private DbsField ex_Address_Name;
    private DbsField ex_Welcome_Name;
    private DbsField ex_Directory_Name;
    private DbsField ex_Annt_Calc_Method;
    private DbsField ex_Annt_Calc_Met_2;
    private DbsField ex_Calc_Participant;
    private DbsField ex_Address_Line;
    private DbsField ex_Zip;
    private DbsGroup ex_ZipRedef1;
    private DbsField ex_Zip_5;
    private DbsField ex_Zip_4;
    private DbsField ex_Sec_Annt_Contract_Name;
    private DbsField ex_Sec_Annt_Ssn;
    private DbsField ex_Sec_Annt_Dob;
    private DbsField ex_Tiaa_Graded_Amt;
    private DbsField ex_Tiaa_Standard_Amt;
    private DbsField ex_Tiaa_Total_Amt;
    private DbsField ex_Guar_Period_Flag;
    private DbsField ex_Multiple_Intr_Rate;
    private DbsField ex_Issue_Eq_Frst_Pymt;
    private DbsField ex_Guar;
    private DbsGroup ex_GuarRedef2;
    private DbsGroup ex_Pnd_Redefine_Guar;
    private DbsField ex_Guar_Payt;
    private DbsField ex_Guar_Intr_Rte;
    private DbsField ex_Guar_Payt_Met;
    private DbsField ex_Survivor_Reduction;
    private DbsField ex_Tiaa_Cntr_Settled;
    private DbsField ex_Da_Tiaa_Total_Amt;
    private DbsField ex_Tiaa_Payin;
    private DbsGroup ex_Tiaa_PayinRedef3;
    private DbsGroup ex_Pnd_Redefine_Tiaa_Payin;
    private DbsField ex_Da_Tiaa_Numb;
    private DbsField ex_Da_Tiaa_Amt;
    private DbsField ex_Internal_Trnsf;
    private DbsField ex_Trnsf_Units;
    private DbsField ex_Trnsf_Payout_Cntr;
    private DbsField ex_Trnsf_Fund;
    private DbsField ex_Eop_Trnsf_Company;
    private DbsField ex_Trnsf_Mode;
    private DbsField ex_Cref_Accnt_Number;
    private DbsField ex_Cref_Payout_Info;
    private DbsGroup ex_Cref_Payout_InfoRedef4;
    private DbsGroup ex_Pnd_Redefine_Cref_Payout_Info;
    private DbsField ex_Cref_Fund;
    private DbsField ex_Cref_Mnthly_Units;
    private DbsField ex_Cref_Annual_Units;
    private DbsField ex_Cref_Cntr_Settled;
    private DbsField ex_Da_Cref_Total_Amt;
    private DbsField ex_Cref_Payin;
    private DbsGroup ex_Cref_PayinRedef5;
    private DbsGroup ex_Pnd_Redefine_Cref_Payin;
    private DbsField ex_Da_Cref_Numb;
    private DbsField ex_Da_Cref_Amt;
    private DbsField ex_Rea_Cntr_Settled;
    private DbsField ex_Da_Rea_Total_Amt;
    private DbsField ex_Rea_Payin;
    private DbsGroup ex_Rea_PayinRedef6;
    private DbsGroup ex_Pnd_Redefine_Rea_Payin;
    private DbsField ex_Da_Rea_Numb;
    private DbsField ex_Da_Rea_Amt;
    private DbsField ex_Real_Estate_Amt;
    private DbsField ex_Rea_Monthly_Units;
    private DbsField ex_Rea_Annualy_Units;
    private DbsField ex_Rea_Surv_Units;
    private DbsField ex_Traditional_Amt;
    private DbsField ex_Tiaa_Initial_Payt_Amt;
    private DbsField ex_Cref_Initial_Payt_Amt;
    private DbsField ex_Tiaa_Excluded_Amt;
    private DbsField ex_Cref_Excluded_Amt;
    private DbsField ex_Guar_Intr_Rate;
    private DbsField ex_Surrender_Chrg;
    private DbsField ex_Contigent_Chrg;
    private DbsField ex_Seprate_Accnt_Chrg;
    private DbsField ex_Orgnl_Prtcpnt_Name;
    private DbsField ex_Orgnl_Prtcpnt_Ssn;
    private DbsField ex_Orgnl_Prtcpnt_Dob;
    private DbsField ex_Orgnl_Prtcpnt_Dod;
    private DbsField ex_Rtb_Request;
    private DbsField ex_Rtb_Percent;
    private DbsField ex_Rtb_Amount;
    private DbsField ex_Gsra_Ira_Surr_Chrg;
    private DbsField ex_Da_Death_Sur_Right;
    private DbsField ex_Gra_Surr_Right;
    private DbsField ex_Tiaa_Cntrct_Num;
    private DbsField ex_Cref_Cert_Num;
    private DbsField ex_Rqst_Id_Key;
    private DbsField ex_Mit_Log_Dte_Tme;
    private DbsField ex_Mit_Log_Oprtr_Cde;
    private DbsField ex_Mit_Orgnl_Unit_Cde;
    private DbsField ex_Mit_Wpid;
    private DbsField ex_Mit_Step_Id;
    private DbsField ex_Mit_Status_Cde;
    private DbsField ex_Extract_Date;
    private DbsField ex_Inst_Name;
    private DbsField ex_Mailing_Instructions;
    private DbsField ex_Pullout_Code;
    private DbsField ex_State_Name;
    private DbsField ex_Res_Issue_State;
    private DbsField ex_Orig_Issue_State;
    private DbsField ex_Ppg_Code;
    private DbsField ex_Lob;
    private DbsField ex_Lob_Type;
    private DbsField ex_Region;
    private DbsField ex_Branch;
    private DbsField ex_Need;
    private DbsField ex_Personal_Annuity;
    private DbsField ex_Graded_Ind;
    private DbsField ex_Revaluation_Cde;
    private DbsField ex_Frst_Pymt_After_Iss;
    private DbsField ex_Annual_Req_Dist_Amt;
    private DbsField ex_Frst_Req_Pymt_Year;
    private DbsField ex_Frst_Req_Pymt_Amt;
    private DbsField ex_Ownership_Code;
    private DbsField ex_Tiaa_Cntrct_Type;
    private DbsField ex_Rea_Cntrct_Type;
    private DbsField ex_Cref_Cntrct_Type;
    private DbsField ex_Check_Mail_Addr;
    private DbsField ex_Bank_Accnt_No;
    private DbsField ex_Bank_Transit_Code;
    private DbsField ex_Comut_Intr_Num;
    private DbsField ex_Comut_Table;
    private DbsGroup ex_Comut_TableRedef7;
    private DbsGroup ex_Pnd_Redefine_Comut_Table;
    private DbsField ex_Comut_Grnted_Amt;
    private DbsField ex_Comut_Intr_Rate;
    private DbsField ex_Comut_Pymt_Method;
    private DbsField ex_Comut_Mort_Basis;
    private DbsField ex_Trnsf_Cntr_Settled;
    private DbsField ex_Spec_Cntrct_Type;
    private DbsField ex_Orig_Issue_Date;
    private DbsField ex_Access_Amt;
    private DbsField ex_Access_Ind;
    private DbsField ex_Access_Account;
    private DbsField ex_Access_Mthly_Unit;
    private DbsField ex_Access_Annual_Unit;
    private DbsField ex_Roth_Ind;
    private DbsField ex_Payee_Ind;
    private DbsField ex_Four_Fifty_Seven_Ind;
    private DbsField ex_Trnsf_Table;
    private DbsGroup ex_Trnsf_TableRedef8;
    private DbsGroup ex_Pnd_Redefine_Trnsf_Table;
    private DbsField ex_Trnsf_Ticker;
    private DbsField ex_Trnsf_Reval_Type;
    private DbsField ex_Trnsf_Cref_Units;
    private DbsField ex_Reval_Type_Name;
    private DbsField ex_Trnsf_Ticker_Name;
    private DbsField ex_Trnsf_Rece_Company;
    private DbsField ex_Trnsf_Cnt_Sepa;
    private DbsField ex_Trnsf_Cnt_Tiaa;
    private DbsField ex_Invest_Process_Dt;
    private DbsField ex_Invest_Amt_Total;
    private DbsField ex_Invest_Num_Tot;
    private DbsField ex_Invest_Tbl_1;
    private DbsGroup ex_Invest_Tbl_1Redef9;
    private DbsGroup ex_Invest_Tbl;
    private DbsField ex_Invest_Num;
    private DbsField ex_Invest_Orig_Amt;
    private DbsField ex_Invest_Tiaa_Num_1;
    private DbsField ex_Invest_Cref_Num_1;
    private DbsField ex_Invest_Fund_Amt;
    private DbsField ex_Invest_Unit_Price;
    private DbsField ex_Invest_Units;
    private DbsField ex_Invest_Fund_Name_From;
    private DbsField ex_Invest_Tiaa_Num_To;
    private DbsField ex_Invest_Cref_Num_To;
    private DbsField ex_Invest_Tbl_3;
    private DbsGroup ex_Invest_Tbl_3Redef10;
    private DbsGroup ex_Invest_Table_3;
    private DbsField ex_Invest_Fund_Amt_To;
    private DbsField ex_Invest_Unit_Price_To;
    private DbsField ex_Invest_Units_To;
    private DbsField ex_Invest_Fund_Name_To;
    private DbsField ex_Plan_Name;
    private DbsField ex_Ia_Shr_Class;
    private DbsField ex_Multi_Plan_Ind;
    private DbsField ex_Plan_Num;
    private DbsField ex_Multi_Plan_Tbl;
    private DbsGroup ex_Multi_Plan_TblRedef11;
    private DbsGroup ex_Multi_Plan_Info;
    private DbsField ex_Multi_Plan_No;
    private DbsField ex_Multi_Sub_Plan;
    private DbsField ex_First_Tpa_Or_Ipro_Ind;
    private DbsField filler01;
    private DbsGroup exRedef12;
    private DbsField ex_Contract_1;
    private DbsField ex_Contract_2;
    private DbsField ex_Contract_3;
    private DbsField ex_Contract_4;
    private DbsField ex_Contract_5;
    private DbsField ex_Contract_6;
    private DbsField ex_Contract_7;
    private DbsField ex_Contract_8;
    private DbsField ex_Contract_9;
    private DbsField ex_Contract_10;
    private DbsField ex_Contract_11;
    private DbsField ex_Contract_12;
    private DbsField ex_Contract_13;
    private DbsField ex_Contract_14;
    private DbsField ex_Contract_15;
    private DbsField ex_Contract_16;
    private DbsField ex_Contract_17;
    private DbsField ex_Contract_18;
    private DbsField ex_Contract_19;
    private DbsField ex_Contract_20;
    private DbsField ex_Contract_21;
    private DbsField ex_Contract_22;
    private DbsField ex_Contract_23;
    private DbsField ex_Contract_24;
    private DbsField ex_Contract_25;
    private DbsField ex_Contract_26;
    private DbsField ex_Contract_27;
    private DbsField ex_Contract_28;
    private DbsField ex_Contract_29;
    private DbsField ex_Contract_30;
    private DbsField ex_Contract_31;
    private DbsField ex_Contract_32;
    private DbsField ex_Contract_33;
    private DbsField ex_Contract_34;
    private DbsField ex_Contract_35;
    private DbsField ex_Contract_36;
    private DbsField ex_Contract_37;
    private DbsField ex_Contract_38;
    private DbsField ex_Contract_39;
    private DbsField ex_Contract_40;
    private DbsField ex_Contract_41;
    private DbsField ex_Contract_42;
    private DbsField ex_Contract_43;
    private DbsField ex_Contract_44;
    private DbsField ex_Contract_45;
    private DbsField ex_Contract_46;
    private DbsField ex_Contract_47;
    private DbsField ex_Contract_48;
    private DbsField ex_Contract_49;
    private DbsField ex_Contract_50;
    private DbsField ex_Contract_51;
    private DbsField ex_Contract_52;
    private DbsField ex_Contract_53;
    private DbsField ex_Contract_54;
    private DbsField ex_Contract_55;
    private DbsField ex_Contract_56;
    private DbsField ex_Contract_57;
    private DbsField ex_Contract_58;
    private DbsField ex_Contract_59;
    private DbsField ex_Contract_60;
    private DbsField ex_Contract_61;
    private DbsField ex_Contract_62;
    private DbsField ex_Contract_63;
    private DbsField ex_Contract_64;
    private DbsField ex_Contract_65;
    private DbsField ex_Contract_66;
    private DbsField ex_Contract_67;
    private DbsField ex_Contract_68;
    private DbsField ex_Contract_69;
    private DbsField ex_Contract_70;
    private DbsField ex_Contract_71;

    public DbsGroup getEx() { return ex; }

    public DbsField getEx_Print_Jobname() { return ex_Print_Jobname; }

    public DbsField getEx_Job_Description() { return ex_Job_Description; }

    public DbsField getEx_Contract_Type() { return ex_Contract_Type; }

    public DbsField getEx_Cntrct_Apprvl() { return ex_Cntrct_Apprvl; }

    public DbsField getEx_Rqst_Id() { return ex_Rqst_Id; }

    public DbsField getEx_Cntrct_Option() { return ex_Cntrct_Option; }

    public DbsField getEx_Cntrct_Cash_Status() { return ex_Cntrct_Cash_Status; }

    public DbsField getEx_Cntrct_Retr_Surv() { return ex_Cntrct_Retr_Surv; }

    public DbsField getEx_Tiaa_Headers() { return ex_Tiaa_Headers; }

    public DbsField getEx_Cref_Headers() { return ex_Cref_Headers; }

    public DbsField getEx_Rea_Headers() { return ex_Rea_Headers; }

    public DbsField getEx_Income_Option() { return ex_Income_Option; }

    public DbsField getEx_Eop_Mergeset_Id() { return ex_Eop_Mergeset_Id; }

    public DbsField getEx_Tiaa_Numb() { return ex_Tiaa_Numb; }

    public DbsField getEx_Rea_Numb() { return ex_Rea_Numb; }

    public DbsField getEx_Cref_Cert_Numb() { return ex_Cref_Cert_Numb; }

    public DbsField getEx_Tiaa_Issue_Date() { return ex_Tiaa_Issue_Date; }

    public DbsField getEx_Cref_Issue_Date() { return ex_Cref_Issue_Date; }

    public DbsField getEx_Payt_Frequency() { return ex_Payt_Frequency; }

    public DbsField getEx_First_Payt_Date() { return ex_First_Payt_Date; }

    public DbsField getEx_Last_Payt_Date() { return ex_Last_Payt_Date; }

    public DbsField getEx_Guar_Period() { return ex_Guar_Period; }

    public DbsField getEx_Guar_Prd_Beg_Date() { return ex_Guar_Prd_Beg_Date; }

    public DbsField getEx_Guar_Prd_End_Date() { return ex_Guar_Prd_End_Date; }

    public DbsField getEx_Tiaa_Form_Numb() { return ex_Tiaa_Form_Numb; }

    public DbsField getEx_Rea_Form_Numb() { return ex_Rea_Form_Numb; }

    public DbsField getEx_Cref_Form_Numb() { return ex_Cref_Form_Numb; }

    public DbsField getEx_Tiaa_Prod_Cdes() { return ex_Tiaa_Prod_Cdes; }

    public DbsField getEx_Rea_Prod_Cdes() { return ex_Rea_Prod_Cdes; }

    public DbsField getEx_Cref_Prod_Cdes() { return ex_Cref_Prod_Cdes; }

    public DbsField getEx_Tiaa_Ed_Numb() { return ex_Tiaa_Ed_Numb; }

    public DbsField getEx_Rea_Ed_Numb() { return ex_Rea_Ed_Numb; }

    public DbsField getEx_Cref_Ed_Numb() { return ex_Cref_Ed_Numb; }

    public DbsField getEx_Pin_Numb() { return ex_Pin_Numb; }

    public DbsField getEx_Annt_Contract_Name() { return ex_Annt_Contract_Name; }

    public DbsField getEx_Annt_Name_Last() { return ex_Annt_Name_Last; }

    public DbsField getEx_Annt_Name_First() { return ex_Annt_Name_First; }

    public DbsField getEx_Annt_Name_Middle() { return ex_Annt_Name_Middle; }

    public DbsField getEx_Annt_Name_Prefix() { return ex_Annt_Name_Prefix; }

    public DbsField getEx_Annt_Name_Suffix() { return ex_Annt_Name_Suffix; }

    public DbsField getEx_Annt_Citizenship() { return ex_Annt_Citizenship; }

    public DbsField getEx_Annt_Soc_Numb() { return ex_Annt_Soc_Numb; }

    public DbsField getEx_Annt_Dob() { return ex_Annt_Dob; }

    public DbsField getEx_First_Annu_Res_State() { return ex_First_Annu_Res_State; }

    public DbsField getEx_Address_Name() { return ex_Address_Name; }

    public DbsField getEx_Welcome_Name() { return ex_Welcome_Name; }

    public DbsField getEx_Directory_Name() { return ex_Directory_Name; }

    public DbsField getEx_Annt_Calc_Method() { return ex_Annt_Calc_Method; }

    public DbsField getEx_Annt_Calc_Met_2() { return ex_Annt_Calc_Met_2; }

    public DbsField getEx_Calc_Participant() { return ex_Calc_Participant; }

    public DbsField getEx_Address_Line() { return ex_Address_Line; }

    public DbsField getEx_Zip() { return ex_Zip; }

    public DbsGroup getEx_ZipRedef1() { return ex_ZipRedef1; }

    public DbsField getEx_Zip_5() { return ex_Zip_5; }

    public DbsField getEx_Zip_4() { return ex_Zip_4; }

    public DbsField getEx_Sec_Annt_Contract_Name() { return ex_Sec_Annt_Contract_Name; }

    public DbsField getEx_Sec_Annt_Ssn() { return ex_Sec_Annt_Ssn; }

    public DbsField getEx_Sec_Annt_Dob() { return ex_Sec_Annt_Dob; }

    public DbsField getEx_Tiaa_Graded_Amt() { return ex_Tiaa_Graded_Amt; }

    public DbsField getEx_Tiaa_Standard_Amt() { return ex_Tiaa_Standard_Amt; }

    public DbsField getEx_Tiaa_Total_Amt() { return ex_Tiaa_Total_Amt; }

    public DbsField getEx_Guar_Period_Flag() { return ex_Guar_Period_Flag; }

    public DbsField getEx_Multiple_Intr_Rate() { return ex_Multiple_Intr_Rate; }

    public DbsField getEx_Issue_Eq_Frst_Pymt() { return ex_Issue_Eq_Frst_Pymt; }

    public DbsField getEx_Guar() { return ex_Guar; }

    public DbsGroup getEx_GuarRedef2() { return ex_GuarRedef2; }

    public DbsGroup getEx_Pnd_Redefine_Guar() { return ex_Pnd_Redefine_Guar; }

    public DbsField getEx_Guar_Payt() { return ex_Guar_Payt; }

    public DbsField getEx_Guar_Intr_Rte() { return ex_Guar_Intr_Rte; }

    public DbsField getEx_Guar_Payt_Met() { return ex_Guar_Payt_Met; }

    public DbsField getEx_Survivor_Reduction() { return ex_Survivor_Reduction; }

    public DbsField getEx_Tiaa_Cntr_Settled() { return ex_Tiaa_Cntr_Settled; }

    public DbsField getEx_Da_Tiaa_Total_Amt() { return ex_Da_Tiaa_Total_Amt; }

    public DbsField getEx_Tiaa_Payin() { return ex_Tiaa_Payin; }

    public DbsGroup getEx_Tiaa_PayinRedef3() { return ex_Tiaa_PayinRedef3; }

    public DbsGroup getEx_Pnd_Redefine_Tiaa_Payin() { return ex_Pnd_Redefine_Tiaa_Payin; }

    public DbsField getEx_Da_Tiaa_Numb() { return ex_Da_Tiaa_Numb; }

    public DbsField getEx_Da_Tiaa_Amt() { return ex_Da_Tiaa_Amt; }

    public DbsField getEx_Internal_Trnsf() { return ex_Internal_Trnsf; }

    public DbsField getEx_Trnsf_Units() { return ex_Trnsf_Units; }

    public DbsField getEx_Trnsf_Payout_Cntr() { return ex_Trnsf_Payout_Cntr; }

    public DbsField getEx_Trnsf_Fund() { return ex_Trnsf_Fund; }

    public DbsField getEx_Eop_Trnsf_Company() { return ex_Eop_Trnsf_Company; }

    public DbsField getEx_Trnsf_Mode() { return ex_Trnsf_Mode; }

    public DbsField getEx_Cref_Accnt_Number() { return ex_Cref_Accnt_Number; }

    public DbsField getEx_Cref_Payout_Info() { return ex_Cref_Payout_Info; }

    public DbsGroup getEx_Cref_Payout_InfoRedef4() { return ex_Cref_Payout_InfoRedef4; }

    public DbsGroup getEx_Pnd_Redefine_Cref_Payout_Info() { return ex_Pnd_Redefine_Cref_Payout_Info; }

    public DbsField getEx_Cref_Fund() { return ex_Cref_Fund; }

    public DbsField getEx_Cref_Mnthly_Units() { return ex_Cref_Mnthly_Units; }

    public DbsField getEx_Cref_Annual_Units() { return ex_Cref_Annual_Units; }

    public DbsField getEx_Cref_Cntr_Settled() { return ex_Cref_Cntr_Settled; }

    public DbsField getEx_Da_Cref_Total_Amt() { return ex_Da_Cref_Total_Amt; }

    public DbsField getEx_Cref_Payin() { return ex_Cref_Payin; }

    public DbsGroup getEx_Cref_PayinRedef5() { return ex_Cref_PayinRedef5; }

    public DbsGroup getEx_Pnd_Redefine_Cref_Payin() { return ex_Pnd_Redefine_Cref_Payin; }

    public DbsField getEx_Da_Cref_Numb() { return ex_Da_Cref_Numb; }

    public DbsField getEx_Da_Cref_Amt() { return ex_Da_Cref_Amt; }

    public DbsField getEx_Rea_Cntr_Settled() { return ex_Rea_Cntr_Settled; }

    public DbsField getEx_Da_Rea_Total_Amt() { return ex_Da_Rea_Total_Amt; }

    public DbsField getEx_Rea_Payin() { return ex_Rea_Payin; }

    public DbsGroup getEx_Rea_PayinRedef6() { return ex_Rea_PayinRedef6; }

    public DbsGroup getEx_Pnd_Redefine_Rea_Payin() { return ex_Pnd_Redefine_Rea_Payin; }

    public DbsField getEx_Da_Rea_Numb() { return ex_Da_Rea_Numb; }

    public DbsField getEx_Da_Rea_Amt() { return ex_Da_Rea_Amt; }

    public DbsField getEx_Real_Estate_Amt() { return ex_Real_Estate_Amt; }

    public DbsField getEx_Rea_Monthly_Units() { return ex_Rea_Monthly_Units; }

    public DbsField getEx_Rea_Annualy_Units() { return ex_Rea_Annualy_Units; }

    public DbsField getEx_Rea_Surv_Units() { return ex_Rea_Surv_Units; }

    public DbsField getEx_Traditional_Amt() { return ex_Traditional_Amt; }

    public DbsField getEx_Tiaa_Initial_Payt_Amt() { return ex_Tiaa_Initial_Payt_Amt; }

    public DbsField getEx_Cref_Initial_Payt_Amt() { return ex_Cref_Initial_Payt_Amt; }

    public DbsField getEx_Tiaa_Excluded_Amt() { return ex_Tiaa_Excluded_Amt; }

    public DbsField getEx_Cref_Excluded_Amt() { return ex_Cref_Excluded_Amt; }

    public DbsField getEx_Guar_Intr_Rate() { return ex_Guar_Intr_Rate; }

    public DbsField getEx_Surrender_Chrg() { return ex_Surrender_Chrg; }

    public DbsField getEx_Contigent_Chrg() { return ex_Contigent_Chrg; }

    public DbsField getEx_Seprate_Accnt_Chrg() { return ex_Seprate_Accnt_Chrg; }

    public DbsField getEx_Orgnl_Prtcpnt_Name() { return ex_Orgnl_Prtcpnt_Name; }

    public DbsField getEx_Orgnl_Prtcpnt_Ssn() { return ex_Orgnl_Prtcpnt_Ssn; }

    public DbsField getEx_Orgnl_Prtcpnt_Dob() { return ex_Orgnl_Prtcpnt_Dob; }

    public DbsField getEx_Orgnl_Prtcpnt_Dod() { return ex_Orgnl_Prtcpnt_Dod; }

    public DbsField getEx_Rtb_Request() { return ex_Rtb_Request; }

    public DbsField getEx_Rtb_Percent() { return ex_Rtb_Percent; }

    public DbsField getEx_Rtb_Amount() { return ex_Rtb_Amount; }

    public DbsField getEx_Gsra_Ira_Surr_Chrg() { return ex_Gsra_Ira_Surr_Chrg; }

    public DbsField getEx_Da_Death_Sur_Right() { return ex_Da_Death_Sur_Right; }

    public DbsField getEx_Gra_Surr_Right() { return ex_Gra_Surr_Right; }

    public DbsField getEx_Tiaa_Cntrct_Num() { return ex_Tiaa_Cntrct_Num; }

    public DbsField getEx_Cref_Cert_Num() { return ex_Cref_Cert_Num; }

    public DbsField getEx_Rqst_Id_Key() { return ex_Rqst_Id_Key; }

    public DbsField getEx_Mit_Log_Dte_Tme() { return ex_Mit_Log_Dte_Tme; }

    public DbsField getEx_Mit_Log_Oprtr_Cde() { return ex_Mit_Log_Oprtr_Cde; }

    public DbsField getEx_Mit_Orgnl_Unit_Cde() { return ex_Mit_Orgnl_Unit_Cde; }

    public DbsField getEx_Mit_Wpid() { return ex_Mit_Wpid; }

    public DbsField getEx_Mit_Step_Id() { return ex_Mit_Step_Id; }

    public DbsField getEx_Mit_Status_Cde() { return ex_Mit_Status_Cde; }

    public DbsField getEx_Extract_Date() { return ex_Extract_Date; }

    public DbsField getEx_Inst_Name() { return ex_Inst_Name; }

    public DbsField getEx_Mailing_Instructions() { return ex_Mailing_Instructions; }

    public DbsField getEx_Pullout_Code() { return ex_Pullout_Code; }

    public DbsField getEx_State_Name() { return ex_State_Name; }

    public DbsField getEx_Res_Issue_State() { return ex_Res_Issue_State; }

    public DbsField getEx_Orig_Issue_State() { return ex_Orig_Issue_State; }

    public DbsField getEx_Ppg_Code() { return ex_Ppg_Code; }

    public DbsField getEx_Lob() { return ex_Lob; }

    public DbsField getEx_Lob_Type() { return ex_Lob_Type; }

    public DbsField getEx_Region() { return ex_Region; }

    public DbsField getEx_Branch() { return ex_Branch; }

    public DbsField getEx_Need() { return ex_Need; }

    public DbsField getEx_Personal_Annuity() { return ex_Personal_Annuity; }

    public DbsField getEx_Graded_Ind() { return ex_Graded_Ind; }

    public DbsField getEx_Revaluation_Cde() { return ex_Revaluation_Cde; }

    public DbsField getEx_Frst_Pymt_After_Iss() { return ex_Frst_Pymt_After_Iss; }

    public DbsField getEx_Annual_Req_Dist_Amt() { return ex_Annual_Req_Dist_Amt; }

    public DbsField getEx_Frst_Req_Pymt_Year() { return ex_Frst_Req_Pymt_Year; }

    public DbsField getEx_Frst_Req_Pymt_Amt() { return ex_Frst_Req_Pymt_Amt; }

    public DbsField getEx_Ownership_Code() { return ex_Ownership_Code; }

    public DbsField getEx_Tiaa_Cntrct_Type() { return ex_Tiaa_Cntrct_Type; }

    public DbsField getEx_Rea_Cntrct_Type() { return ex_Rea_Cntrct_Type; }

    public DbsField getEx_Cref_Cntrct_Type() { return ex_Cref_Cntrct_Type; }

    public DbsField getEx_Check_Mail_Addr() { return ex_Check_Mail_Addr; }

    public DbsField getEx_Bank_Accnt_No() { return ex_Bank_Accnt_No; }

    public DbsField getEx_Bank_Transit_Code() { return ex_Bank_Transit_Code; }

    public DbsField getEx_Comut_Intr_Num() { return ex_Comut_Intr_Num; }

    public DbsField getEx_Comut_Table() { return ex_Comut_Table; }

    public DbsGroup getEx_Comut_TableRedef7() { return ex_Comut_TableRedef7; }

    public DbsGroup getEx_Pnd_Redefine_Comut_Table() { return ex_Pnd_Redefine_Comut_Table; }

    public DbsField getEx_Comut_Grnted_Amt() { return ex_Comut_Grnted_Amt; }

    public DbsField getEx_Comut_Intr_Rate() { return ex_Comut_Intr_Rate; }

    public DbsField getEx_Comut_Pymt_Method() { return ex_Comut_Pymt_Method; }

    public DbsField getEx_Comut_Mort_Basis() { return ex_Comut_Mort_Basis; }

    public DbsField getEx_Trnsf_Cntr_Settled() { return ex_Trnsf_Cntr_Settled; }

    public DbsField getEx_Spec_Cntrct_Type() { return ex_Spec_Cntrct_Type; }

    public DbsField getEx_Orig_Issue_Date() { return ex_Orig_Issue_Date; }

    public DbsField getEx_Access_Amt() { return ex_Access_Amt; }

    public DbsField getEx_Access_Ind() { return ex_Access_Ind; }

    public DbsField getEx_Access_Account() { return ex_Access_Account; }

    public DbsField getEx_Access_Mthly_Unit() { return ex_Access_Mthly_Unit; }

    public DbsField getEx_Access_Annual_Unit() { return ex_Access_Annual_Unit; }

    public DbsField getEx_Roth_Ind() { return ex_Roth_Ind; }

    public DbsField getEx_Payee_Ind() { return ex_Payee_Ind; }

    public DbsField getEx_Four_Fifty_Seven_Ind() { return ex_Four_Fifty_Seven_Ind; }

    public DbsField getEx_Trnsf_Table() { return ex_Trnsf_Table; }

    public DbsGroup getEx_Trnsf_TableRedef8() { return ex_Trnsf_TableRedef8; }

    public DbsGroup getEx_Pnd_Redefine_Trnsf_Table() { return ex_Pnd_Redefine_Trnsf_Table; }

    public DbsField getEx_Trnsf_Ticker() { return ex_Trnsf_Ticker; }

    public DbsField getEx_Trnsf_Reval_Type() { return ex_Trnsf_Reval_Type; }

    public DbsField getEx_Trnsf_Cref_Units() { return ex_Trnsf_Cref_Units; }

    public DbsField getEx_Reval_Type_Name() { return ex_Reval_Type_Name; }

    public DbsField getEx_Trnsf_Ticker_Name() { return ex_Trnsf_Ticker_Name; }

    public DbsField getEx_Trnsf_Rece_Company() { return ex_Trnsf_Rece_Company; }

    public DbsField getEx_Trnsf_Cnt_Sepa() { return ex_Trnsf_Cnt_Sepa; }

    public DbsField getEx_Trnsf_Cnt_Tiaa() { return ex_Trnsf_Cnt_Tiaa; }

    public DbsField getEx_Invest_Process_Dt() { return ex_Invest_Process_Dt; }

    public DbsField getEx_Invest_Amt_Total() { return ex_Invest_Amt_Total; }

    public DbsField getEx_Invest_Num_Tot() { return ex_Invest_Num_Tot; }

    public DbsField getEx_Invest_Tbl_1() { return ex_Invest_Tbl_1; }

    public DbsGroup getEx_Invest_Tbl_1Redef9() { return ex_Invest_Tbl_1Redef9; }

    public DbsGroup getEx_Invest_Tbl() { return ex_Invest_Tbl; }

    public DbsField getEx_Invest_Num() { return ex_Invest_Num; }

    public DbsField getEx_Invest_Orig_Amt() { return ex_Invest_Orig_Amt; }

    public DbsField getEx_Invest_Tiaa_Num_1() { return ex_Invest_Tiaa_Num_1; }

    public DbsField getEx_Invest_Cref_Num_1() { return ex_Invest_Cref_Num_1; }

    public DbsField getEx_Invest_Fund_Amt() { return ex_Invest_Fund_Amt; }

    public DbsField getEx_Invest_Unit_Price() { return ex_Invest_Unit_Price; }

    public DbsField getEx_Invest_Units() { return ex_Invest_Units; }

    public DbsField getEx_Invest_Fund_Name_From() { return ex_Invest_Fund_Name_From; }

    public DbsField getEx_Invest_Tiaa_Num_To() { return ex_Invest_Tiaa_Num_To; }

    public DbsField getEx_Invest_Cref_Num_To() { return ex_Invest_Cref_Num_To; }

    public DbsField getEx_Invest_Tbl_3() { return ex_Invest_Tbl_3; }

    public DbsGroup getEx_Invest_Tbl_3Redef10() { return ex_Invest_Tbl_3Redef10; }

    public DbsGroup getEx_Invest_Table_3() { return ex_Invest_Table_3; }

    public DbsField getEx_Invest_Fund_Amt_To() { return ex_Invest_Fund_Amt_To; }

    public DbsField getEx_Invest_Unit_Price_To() { return ex_Invest_Unit_Price_To; }

    public DbsField getEx_Invest_Units_To() { return ex_Invest_Units_To; }

    public DbsField getEx_Invest_Fund_Name_To() { return ex_Invest_Fund_Name_To; }

    public DbsField getEx_Plan_Name() { return ex_Plan_Name; }

    public DbsField getEx_Ia_Shr_Class() { return ex_Ia_Shr_Class; }

    public DbsField getEx_Multi_Plan_Ind() { return ex_Multi_Plan_Ind; }

    public DbsField getEx_Plan_Num() { return ex_Plan_Num; }

    public DbsField getEx_Multi_Plan_Tbl() { return ex_Multi_Plan_Tbl; }

    public DbsGroup getEx_Multi_Plan_TblRedef11() { return ex_Multi_Plan_TblRedef11; }

    public DbsGroup getEx_Multi_Plan_Info() { return ex_Multi_Plan_Info; }

    public DbsField getEx_Multi_Plan_No() { return ex_Multi_Plan_No; }

    public DbsField getEx_Multi_Sub_Plan() { return ex_Multi_Sub_Plan; }

    public DbsField getEx_First_Tpa_Or_Ipro_Ind() { return ex_First_Tpa_Or_Ipro_Ind; }

    public DbsField getFiller01() { return filler01; }

    public DbsGroup getExRedef12() { return exRedef12; }

    public DbsField getEx_Contract_1() { return ex_Contract_1; }

    public DbsField getEx_Contract_2() { return ex_Contract_2; }

    public DbsField getEx_Contract_3() { return ex_Contract_3; }

    public DbsField getEx_Contract_4() { return ex_Contract_4; }

    public DbsField getEx_Contract_5() { return ex_Contract_5; }

    public DbsField getEx_Contract_6() { return ex_Contract_6; }

    public DbsField getEx_Contract_7() { return ex_Contract_7; }

    public DbsField getEx_Contract_8() { return ex_Contract_8; }

    public DbsField getEx_Contract_9() { return ex_Contract_9; }

    public DbsField getEx_Contract_10() { return ex_Contract_10; }

    public DbsField getEx_Contract_11() { return ex_Contract_11; }

    public DbsField getEx_Contract_12() { return ex_Contract_12; }

    public DbsField getEx_Contract_13() { return ex_Contract_13; }

    public DbsField getEx_Contract_14() { return ex_Contract_14; }

    public DbsField getEx_Contract_15() { return ex_Contract_15; }

    public DbsField getEx_Contract_16() { return ex_Contract_16; }

    public DbsField getEx_Contract_17() { return ex_Contract_17; }

    public DbsField getEx_Contract_18() { return ex_Contract_18; }

    public DbsField getEx_Contract_19() { return ex_Contract_19; }

    public DbsField getEx_Contract_20() { return ex_Contract_20; }

    public DbsField getEx_Contract_21() { return ex_Contract_21; }

    public DbsField getEx_Contract_22() { return ex_Contract_22; }

    public DbsField getEx_Contract_23() { return ex_Contract_23; }

    public DbsField getEx_Contract_24() { return ex_Contract_24; }

    public DbsField getEx_Contract_25() { return ex_Contract_25; }

    public DbsField getEx_Contract_26() { return ex_Contract_26; }

    public DbsField getEx_Contract_27() { return ex_Contract_27; }

    public DbsField getEx_Contract_28() { return ex_Contract_28; }

    public DbsField getEx_Contract_29() { return ex_Contract_29; }

    public DbsField getEx_Contract_30() { return ex_Contract_30; }

    public DbsField getEx_Contract_31() { return ex_Contract_31; }

    public DbsField getEx_Contract_32() { return ex_Contract_32; }

    public DbsField getEx_Contract_33() { return ex_Contract_33; }

    public DbsField getEx_Contract_34() { return ex_Contract_34; }

    public DbsField getEx_Contract_35() { return ex_Contract_35; }

    public DbsField getEx_Contract_36() { return ex_Contract_36; }

    public DbsField getEx_Contract_37() { return ex_Contract_37; }

    public DbsField getEx_Contract_38() { return ex_Contract_38; }

    public DbsField getEx_Contract_39() { return ex_Contract_39; }

    public DbsField getEx_Contract_40() { return ex_Contract_40; }

    public DbsField getEx_Contract_41() { return ex_Contract_41; }

    public DbsField getEx_Contract_42() { return ex_Contract_42; }

    public DbsField getEx_Contract_43() { return ex_Contract_43; }

    public DbsField getEx_Contract_44() { return ex_Contract_44; }

    public DbsField getEx_Contract_45() { return ex_Contract_45; }

    public DbsField getEx_Contract_46() { return ex_Contract_46; }

    public DbsField getEx_Contract_47() { return ex_Contract_47; }

    public DbsField getEx_Contract_48() { return ex_Contract_48; }

    public DbsField getEx_Contract_49() { return ex_Contract_49; }

    public DbsField getEx_Contract_50() { return ex_Contract_50; }

    public DbsField getEx_Contract_51() { return ex_Contract_51; }

    public DbsField getEx_Contract_52() { return ex_Contract_52; }

    public DbsField getEx_Contract_53() { return ex_Contract_53; }

    public DbsField getEx_Contract_54() { return ex_Contract_54; }

    public DbsField getEx_Contract_55() { return ex_Contract_55; }

    public DbsField getEx_Contract_56() { return ex_Contract_56; }

    public DbsField getEx_Contract_57() { return ex_Contract_57; }

    public DbsField getEx_Contract_58() { return ex_Contract_58; }

    public DbsField getEx_Contract_59() { return ex_Contract_59; }

    public DbsField getEx_Contract_60() { return ex_Contract_60; }

    public DbsField getEx_Contract_61() { return ex_Contract_61; }

    public DbsField getEx_Contract_62() { return ex_Contract_62; }

    public DbsField getEx_Contract_63() { return ex_Contract_63; }

    public DbsField getEx_Contract_64() { return ex_Contract_64; }

    public DbsField getEx_Contract_65() { return ex_Contract_65; }

    public DbsField getEx_Contract_66() { return ex_Contract_66; }

    public DbsField getEx_Contract_67() { return ex_Contract_67; }

    public DbsField getEx_Contract_68() { return ex_Contract_68; }

    public DbsField getEx_Contract_69() { return ex_Contract_69; }

    public DbsField getEx_Contract_70() { return ex_Contract_70; }

    public DbsField getEx_Contract_71() { return ex_Contract_71; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ex = newGroupInRecord("ex", "EX");
        ex_Print_Jobname = ex.newFieldInGroup("ex_Print_Jobname", "PRINT-JOBNAME", FieldType.STRING, 8);
        ex_Job_Description = ex.newFieldInGroup("ex_Job_Description", "JOB-DESCRIPTION", FieldType.STRING, 32);
        ex_Contract_Type = ex.newFieldInGroup("ex_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        ex_Cntrct_Apprvl = ex.newFieldInGroup("ex_Cntrct_Apprvl", "CNTRCT-APPRVL", FieldType.STRING, 1);
        ex_Rqst_Id = ex.newFieldInGroup("ex_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        ex_Cntrct_Option = ex.newFieldInGroup("ex_Cntrct_Option", "CNTRCT-OPTION", FieldType.STRING, 10);
        ex_Cntrct_Cash_Status = ex.newFieldInGroup("ex_Cntrct_Cash_Status", "CNTRCT-CASH-STATUS", FieldType.STRING, 1);
        ex_Cntrct_Retr_Surv = ex.newFieldInGroup("ex_Cntrct_Retr_Surv", "CNTRCT-RETR-SURV", FieldType.STRING, 1);
        ex_Tiaa_Headers = ex.newFieldArrayInGroup("ex_Tiaa_Headers", "TIAA-HEADERS", FieldType.STRING, 85, new DbsArrayController(1,2));
        ex_Cref_Headers = ex.newFieldArrayInGroup("ex_Cref_Headers", "CREF-HEADERS", FieldType.STRING, 85, new DbsArrayController(1,2));
        ex_Rea_Headers = ex.newFieldArrayInGroup("ex_Rea_Headers", "REA-HEADERS", FieldType.STRING, 85, new DbsArrayController(1,2));
        ex_Income_Option = ex.newFieldArrayInGroup("ex_Income_Option", "INCOME-OPTION", FieldType.STRING, 85, new DbsArrayController(1,2));
        ex_Eop_Mergeset_Id = ex.newFieldInGroup("ex_Eop_Mergeset_Id", "EOP-MERGESET-ID", FieldType.STRING, 18);
        ex_Tiaa_Numb = ex.newFieldInGroup("ex_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        ex_Rea_Numb = ex.newFieldInGroup("ex_Rea_Numb", "REA-NUMB", FieldType.STRING, 16);
        ex_Cref_Cert_Numb = ex.newFieldInGroup("ex_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        ex_Tiaa_Issue_Date = ex.newFieldInGroup("ex_Tiaa_Issue_Date", "TIAA-ISSUE-DATE", FieldType.STRING, 8);
        ex_Cref_Issue_Date = ex.newFieldInGroup("ex_Cref_Issue_Date", "CREF-ISSUE-DATE", FieldType.STRING, 8);
        ex_Payt_Frequency = ex.newFieldInGroup("ex_Payt_Frequency", "PAYT-FREQUENCY", FieldType.STRING, 13);
        ex_First_Payt_Date = ex.newFieldInGroup("ex_First_Payt_Date", "FIRST-PAYT-DATE", FieldType.STRING, 8);
        ex_Last_Payt_Date = ex.newFieldInGroup("ex_Last_Payt_Date", "LAST-PAYT-DATE", FieldType.STRING, 8);
        ex_Guar_Period = ex.newFieldInGroup("ex_Guar_Period", "GUAR-PERIOD", FieldType.STRING, 18);
        ex_Guar_Prd_Beg_Date = ex.newFieldInGroup("ex_Guar_Prd_Beg_Date", "GUAR-PRD-BEG-DATE", FieldType.STRING, 14);
        ex_Guar_Prd_End_Date = ex.newFieldInGroup("ex_Guar_Prd_End_Date", "GUAR-PRD-END-DATE", FieldType.STRING, 14);
        ex_Tiaa_Form_Numb = ex.newFieldArrayInGroup("ex_Tiaa_Form_Numb", "TIAA-FORM-NUMB", FieldType.STRING, 20, new DbsArrayController(1,2));
        ex_Rea_Form_Numb = ex.newFieldArrayInGroup("ex_Rea_Form_Numb", "REA-FORM-NUMB", FieldType.STRING, 20, new DbsArrayController(1,2));
        ex_Cref_Form_Numb = ex.newFieldArrayInGroup("ex_Cref_Form_Numb", "CREF-FORM-NUMB", FieldType.STRING, 20, new DbsArrayController(1,2));
        ex_Tiaa_Prod_Cdes = ex.newFieldArrayInGroup("ex_Tiaa_Prod_Cdes", "TIAA-PROD-CDES", FieldType.STRING, 35, new DbsArrayController(1,2));
        ex_Rea_Prod_Cdes = ex.newFieldArrayInGroup("ex_Rea_Prod_Cdes", "REA-PROD-CDES", FieldType.STRING, 35, new DbsArrayController(1,2));
        ex_Cref_Prod_Cdes = ex.newFieldArrayInGroup("ex_Cref_Prod_Cdes", "CREF-PROD-CDES", FieldType.STRING, 35, new DbsArrayController(1,2));
        ex_Tiaa_Ed_Numb = ex.newFieldArrayInGroup("ex_Tiaa_Ed_Numb", "TIAA-ED-NUMB", FieldType.STRING, 15, new DbsArrayController(1,2));
        ex_Rea_Ed_Numb = ex.newFieldArrayInGroup("ex_Rea_Ed_Numb", "REA-ED-NUMB", FieldType.STRING, 15, new DbsArrayController(1,2));
        ex_Cref_Ed_Numb = ex.newFieldArrayInGroup("ex_Cref_Ed_Numb", "CREF-ED-NUMB", FieldType.STRING, 15, new DbsArrayController(1,2));
        ex_Pin_Numb = ex.newFieldInGroup("ex_Pin_Numb", "PIN-NUMB", FieldType.STRING, 12);
        ex_Annt_Contract_Name = ex.newFieldInGroup("ex_Annt_Contract_Name", "ANNT-CONTRACT-NAME", FieldType.STRING, 72);
        ex_Annt_Name_Last = ex.newFieldInGroup("ex_Annt_Name_Last", "ANNT-NAME-LAST", FieldType.STRING, 30);
        ex_Annt_Name_First = ex.newFieldInGroup("ex_Annt_Name_First", "ANNT-NAME-FIRST", FieldType.STRING, 30);
        ex_Annt_Name_Middle = ex.newFieldInGroup("ex_Annt_Name_Middle", "ANNT-NAME-MIDDLE", FieldType.STRING, 30);
        ex_Annt_Name_Prefix = ex.newFieldInGroup("ex_Annt_Name_Prefix", "ANNT-NAME-PREFIX", FieldType.STRING, 8);
        ex_Annt_Name_Suffix = ex.newFieldInGroup("ex_Annt_Name_Suffix", "ANNT-NAME-SUFFIX", FieldType.STRING, 8);
        ex_Annt_Citizenship = ex.newFieldInGroup("ex_Annt_Citizenship", "ANNT-CITIZENSHIP", FieldType.STRING, 25);
        ex_Annt_Soc_Numb = ex.newFieldInGroup("ex_Annt_Soc_Numb", "ANNT-SOC-NUMB", FieldType.STRING, 11);
        ex_Annt_Dob = ex.newFieldInGroup("ex_Annt_Dob", "ANNT-DOB", FieldType.STRING, 8);
        ex_First_Annu_Res_State = ex.newFieldInGroup("ex_First_Annu_Res_State", "FIRST-ANNU-RES-STATE", FieldType.STRING, 2);
        ex_Address_Name = ex.newFieldInGroup("ex_Address_Name", "ADDRESS-NAME", FieldType.STRING, 81);
        ex_Welcome_Name = ex.newFieldInGroup("ex_Welcome_Name", "WELCOME-NAME", FieldType.STRING, 39);
        ex_Directory_Name = ex.newFieldInGroup("ex_Directory_Name", "DIRECTORY-NAME", FieldType.STRING, 63);
        ex_Annt_Calc_Method = ex.newFieldInGroup("ex_Annt_Calc_Method", "ANNT-CALC-METHOD", FieldType.STRING, 1);
        ex_Annt_Calc_Met_2 = ex.newFieldInGroup("ex_Annt_Calc_Met_2", "ANNT-CALC-MET-2", FieldType.STRING, 15);
        ex_Calc_Participant = ex.newFieldInGroup("ex_Calc_Participant", "CALC-PARTICIPANT", FieldType.STRING, 13);
        ex_Address_Line = ex.newFieldArrayInGroup("ex_Address_Line", "ADDRESS-LINE", FieldType.STRING, 35, new DbsArrayController(1,5));
        ex_Zip = ex.newFieldInGroup("ex_Zip", "ZIP", FieldType.STRING, 9);
        ex_ZipRedef1 = ex.newGroupInGroup("ex_ZipRedef1", "Redefines", ex_Zip);
        ex_Zip_5 = ex_ZipRedef1.newFieldInGroup("ex_Zip_5", "ZIP-5", FieldType.STRING, 5);
        ex_Zip_4 = ex_ZipRedef1.newFieldInGroup("ex_Zip_4", "ZIP-4", FieldType.STRING, 4);
        ex_Sec_Annt_Contract_Name = ex.newFieldInGroup("ex_Sec_Annt_Contract_Name", "SEC-ANNT-CONTRACT-NAME", FieldType.STRING, 63);
        ex_Sec_Annt_Ssn = ex.newFieldInGroup("ex_Sec_Annt_Ssn", "SEC-ANNT-SSN", FieldType.STRING, 11);
        ex_Sec_Annt_Dob = ex.newFieldInGroup("ex_Sec_Annt_Dob", "SEC-ANNT-DOB", FieldType.STRING, 8);
        ex_Tiaa_Graded_Amt = ex.newFieldInGroup("ex_Tiaa_Graded_Amt", "TIAA-GRADED-AMT", FieldType.STRING, 15);
        ex_Tiaa_Standard_Amt = ex.newFieldInGroup("ex_Tiaa_Standard_Amt", "TIAA-STANDARD-AMT", FieldType.STRING, 15);
        ex_Tiaa_Total_Amt = ex.newFieldInGroup("ex_Tiaa_Total_Amt", "TIAA-TOTAL-AMT", FieldType.STRING, 15);
        ex_Guar_Period_Flag = ex.newFieldInGroup("ex_Guar_Period_Flag", "GUAR-PERIOD-FLAG", FieldType.STRING, 1);
        ex_Multiple_Intr_Rate = ex.newFieldInGroup("ex_Multiple_Intr_Rate", "MULTIPLE-INTR-RATE", FieldType.STRING, 1);
        ex_Issue_Eq_Frst_Pymt = ex.newFieldInGroup("ex_Issue_Eq_Frst_Pymt", "ISSUE-EQ-FRST-PYMT", FieldType.STRING, 1);
        ex_Guar = ex.newFieldArrayInGroup("ex_Guar", "GUAR", FieldType.STRING, 28, new DbsArrayController(1,5));
        ex_GuarRedef2 = ex.newGroupInGroup("ex_GuarRedef2", "Redefines", ex_Guar);
        ex_Pnd_Redefine_Guar = ex_GuarRedef2.newGroupArrayInGroup("ex_Pnd_Redefine_Guar", "#REDEFINE-GUAR", new DbsArrayController(1,5));
        ex_Guar_Payt = ex_Pnd_Redefine_Guar.newFieldInGroup("ex_Guar_Payt", "GUAR-PAYT", FieldType.STRING, 15);
        ex_Guar_Intr_Rte = ex_Pnd_Redefine_Guar.newFieldInGroup("ex_Guar_Intr_Rte", "GUAR-INTR-RTE", FieldType.STRING, 5);
        ex_Guar_Payt_Met = ex_Pnd_Redefine_Guar.newFieldInGroup("ex_Guar_Payt_Met", "GUAR-PAYT-MET", FieldType.STRING, 8);
        ex_Survivor_Reduction = ex.newFieldInGroup("ex_Survivor_Reduction", "SURVIVOR-REDUCTION", FieldType.STRING, 10);
        ex_Tiaa_Cntr_Settled = ex.newFieldInGroup("ex_Tiaa_Cntr_Settled", "TIAA-CNTR-SETTLED", FieldType.STRING, 2);
        ex_Da_Tiaa_Total_Amt = ex.newFieldInGroup("ex_Da_Tiaa_Total_Amt", "DA-TIAA-TOTAL-AMT", FieldType.STRING, 15);
        ex_Tiaa_Payin = ex.newFieldArrayInGroup("ex_Tiaa_Payin", "TIAA-PAYIN", FieldType.STRING, 27, new DbsArrayController(1,20));
        ex_Tiaa_PayinRedef3 = ex.newGroupInGroup("ex_Tiaa_PayinRedef3", "Redefines", ex_Tiaa_Payin);
        ex_Pnd_Redefine_Tiaa_Payin = ex_Tiaa_PayinRedef3.newGroupArrayInGroup("ex_Pnd_Redefine_Tiaa_Payin", "#REDEFINE-TIAA-PAYIN", new DbsArrayController(1,
            20));
        ex_Da_Tiaa_Numb = ex_Pnd_Redefine_Tiaa_Payin.newFieldInGroup("ex_Da_Tiaa_Numb", "DA-TIAA-NUMB", FieldType.STRING, 12);
        ex_Da_Tiaa_Amt = ex_Pnd_Redefine_Tiaa_Payin.newFieldInGroup("ex_Da_Tiaa_Amt", "DA-TIAA-AMT", FieldType.STRING, 15);
        ex_Internal_Trnsf = ex.newFieldInGroup("ex_Internal_Trnsf", "INTERNAL-TRNSF", FieldType.STRING, 1);
        ex_Trnsf_Units = ex.newFieldInGroup("ex_Trnsf_Units", "TRNSF-UNITS", FieldType.STRING, 9);
        ex_Trnsf_Payout_Cntr = ex.newFieldInGroup("ex_Trnsf_Payout_Cntr", "TRNSF-PAYOUT-CNTR", FieldType.STRING, 12);
        ex_Trnsf_Fund = ex.newFieldInGroup("ex_Trnsf_Fund", "TRNSF-FUND", FieldType.STRING, 30);
        ex_Eop_Trnsf_Company = ex.newFieldInGroup("ex_Eop_Trnsf_Company", "EOP-TRNSF-COMPANY", FieldType.STRING, 4);
        ex_Trnsf_Mode = ex.newFieldInGroup("ex_Trnsf_Mode", "TRNSF-MODE", FieldType.STRING, 13);
        ex_Cref_Accnt_Number = ex.newFieldInGroup("ex_Cref_Accnt_Number", "CREF-ACCNT-NUMBER", FieldType.STRING, 2);
        ex_Cref_Payout_Info = ex.newFieldArrayInGroup("ex_Cref_Payout_Info", "CREF-PAYOUT-INFO", FieldType.STRING, 54, new DbsArrayController(1,20));
        ex_Cref_Payout_InfoRedef4 = ex.newGroupInGroup("ex_Cref_Payout_InfoRedef4", "Redefines", ex_Cref_Payout_Info);
        ex_Pnd_Redefine_Cref_Payout_Info = ex_Cref_Payout_InfoRedef4.newGroupArrayInGroup("ex_Pnd_Redefine_Cref_Payout_Info", "#REDEFINE-CREF-PAYOUT-INFO", 
            new DbsArrayController(1,20));
        ex_Cref_Fund = ex_Pnd_Redefine_Cref_Payout_Info.newFieldInGroup("ex_Cref_Fund", "CREF-FUND", FieldType.STRING, 30);
        ex_Cref_Mnthly_Units = ex_Pnd_Redefine_Cref_Payout_Info.newFieldInGroup("ex_Cref_Mnthly_Units", "CREF-MNTHLY-UNITS", FieldType.STRING, 12);
        ex_Cref_Annual_Units = ex_Pnd_Redefine_Cref_Payout_Info.newFieldInGroup("ex_Cref_Annual_Units", "CREF-ANNUAL-UNITS", FieldType.STRING, 12);
        ex_Cref_Cntr_Settled = ex.newFieldInGroup("ex_Cref_Cntr_Settled", "CREF-CNTR-SETTLED", FieldType.STRING, 2);
        ex_Da_Cref_Total_Amt = ex.newFieldInGroup("ex_Da_Cref_Total_Amt", "DA-CREF-TOTAL-AMT", FieldType.STRING, 15);
        ex_Cref_Payin = ex.newFieldArrayInGroup("ex_Cref_Payin", "CREF-PAYIN", FieldType.STRING, 27, new DbsArrayController(1,20));
        ex_Cref_PayinRedef5 = ex.newGroupInGroup("ex_Cref_PayinRedef5", "Redefines", ex_Cref_Payin);
        ex_Pnd_Redefine_Cref_Payin = ex_Cref_PayinRedef5.newGroupArrayInGroup("ex_Pnd_Redefine_Cref_Payin", "#REDEFINE-CREF-PAYIN", new DbsArrayController(1,
            20));
        ex_Da_Cref_Numb = ex_Pnd_Redefine_Cref_Payin.newFieldInGroup("ex_Da_Cref_Numb", "DA-CREF-NUMB", FieldType.STRING, 12);
        ex_Da_Cref_Amt = ex_Pnd_Redefine_Cref_Payin.newFieldInGroup("ex_Da_Cref_Amt", "DA-CREF-AMT", FieldType.STRING, 15);
        ex_Rea_Cntr_Settled = ex.newFieldInGroup("ex_Rea_Cntr_Settled", "REA-CNTR-SETTLED", FieldType.STRING, 2);
        ex_Da_Rea_Total_Amt = ex.newFieldInGroup("ex_Da_Rea_Total_Amt", "DA-REA-TOTAL-AMT", FieldType.STRING, 15);
        ex_Rea_Payin = ex.newFieldArrayInGroup("ex_Rea_Payin", "REA-PAYIN", FieldType.STRING, 27, new DbsArrayController(1,20));
        ex_Rea_PayinRedef6 = ex.newGroupInGroup("ex_Rea_PayinRedef6", "Redefines", ex_Rea_Payin);
        ex_Pnd_Redefine_Rea_Payin = ex_Rea_PayinRedef6.newGroupArrayInGroup("ex_Pnd_Redefine_Rea_Payin", "#REDEFINE-REA-PAYIN", new DbsArrayController(1,
            20));
        ex_Da_Rea_Numb = ex_Pnd_Redefine_Rea_Payin.newFieldInGroup("ex_Da_Rea_Numb", "DA-REA-NUMB", FieldType.STRING, 12);
        ex_Da_Rea_Amt = ex_Pnd_Redefine_Rea_Payin.newFieldInGroup("ex_Da_Rea_Amt", "DA-REA-AMT", FieldType.STRING, 15);
        ex_Real_Estate_Amt = ex.newFieldInGroup("ex_Real_Estate_Amt", "REAL-ESTATE-AMT", FieldType.STRING, 15);
        ex_Rea_Monthly_Units = ex.newFieldInGroup("ex_Rea_Monthly_Units", "REA-MONTHLY-UNITS", FieldType.STRING, 12);
        ex_Rea_Annualy_Units = ex.newFieldInGroup("ex_Rea_Annualy_Units", "REA-ANNUALY-UNITS", FieldType.STRING, 12);
        ex_Rea_Surv_Units = ex.newFieldInGroup("ex_Rea_Surv_Units", "REA-SURV-UNITS", FieldType.STRING, 12);
        ex_Traditional_Amt = ex.newFieldInGroup("ex_Traditional_Amt", "TRADITIONAL-AMT", FieldType.STRING, 15);
        ex_Tiaa_Initial_Payt_Amt = ex.newFieldInGroup("ex_Tiaa_Initial_Payt_Amt", "TIAA-INITIAL-PAYT-AMT", FieldType.STRING, 15);
        ex_Cref_Initial_Payt_Amt = ex.newFieldInGroup("ex_Cref_Initial_Payt_Amt", "CREF-INITIAL-PAYT-AMT", FieldType.STRING, 15);
        ex_Tiaa_Excluded_Amt = ex.newFieldInGroup("ex_Tiaa_Excluded_Amt", "TIAA-EXCLUDED-AMT", FieldType.STRING, 15);
        ex_Cref_Excluded_Amt = ex.newFieldInGroup("ex_Cref_Excluded_Amt", "CREF-EXCLUDED-AMT", FieldType.STRING, 15);
        ex_Guar_Intr_Rate = ex.newFieldInGroup("ex_Guar_Intr_Rate", "GUAR-INTR-RATE", FieldType.STRING, 6);
        ex_Surrender_Chrg = ex.newFieldInGroup("ex_Surrender_Chrg", "SURRENDER-CHRG", FieldType.STRING, 6);
        ex_Contigent_Chrg = ex.newFieldInGroup("ex_Contigent_Chrg", "CONTIGENT-CHRG", FieldType.STRING, 6);
        ex_Seprate_Accnt_Chrg = ex.newFieldInGroup("ex_Seprate_Accnt_Chrg", "SEPRATE_ACCNT-CHRG", FieldType.STRING, 5);
        ex_Orgnl_Prtcpnt_Name = ex.newFieldInGroup("ex_Orgnl_Prtcpnt_Name", "ORGNL-PRTCPNT-NAME", FieldType.STRING, 72);
        ex_Orgnl_Prtcpnt_Ssn = ex.newFieldInGroup("ex_Orgnl_Prtcpnt_Ssn", "ORGNL-PRTCPNT-SSN", FieldType.STRING, 11);
        ex_Orgnl_Prtcpnt_Dob = ex.newFieldInGroup("ex_Orgnl_Prtcpnt_Dob", "ORGNL-PRTCPNT-DOB", FieldType.STRING, 8);
        ex_Orgnl_Prtcpnt_Dod = ex.newFieldInGroup("ex_Orgnl_Prtcpnt_Dod", "ORGNL-PRTCPNT-DOD", FieldType.STRING, 8);
        ex_Rtb_Request = ex.newFieldInGroup("ex_Rtb_Request", "RTB-REQUEST", FieldType.STRING, 1);
        ex_Rtb_Percent = ex.newFieldInGroup("ex_Rtb_Percent", "RTB-PERCENT", FieldType.STRING, 3);
        ex_Rtb_Amount = ex.newFieldInGroup("ex_Rtb_Amount", "RTB-AMOUNT", FieldType.STRING, 15);
        ex_Gsra_Ira_Surr_Chrg = ex.newFieldInGroup("ex_Gsra_Ira_Surr_Chrg", "GSRA-IRA-SURR-CHRG", FieldType.STRING, 1);
        ex_Da_Death_Sur_Right = ex.newFieldInGroup("ex_Da_Death_Sur_Right", "DA-DEATH-SUR-RIGHT", FieldType.STRING, 1);
        ex_Gra_Surr_Right = ex.newFieldInGroup("ex_Gra_Surr_Right", "GRA-SURR-RIGHT", FieldType.STRING, 1);
        ex_Tiaa_Cntrct_Num = ex.newFieldInGroup("ex_Tiaa_Cntrct_Num", "TIAA-CNTRCT-NUM", FieldType.STRING, 10);
        ex_Cref_Cert_Num = ex.newFieldInGroup("ex_Cref_Cert_Num", "CREF-CERT-NUM", FieldType.STRING, 10);
        ex_Rqst_Id_Key = ex.newFieldInGroup("ex_Rqst_Id_Key", "RQST-ID-KEY", FieldType.STRING, 35);
        ex_Mit_Log_Dte_Tme = ex.newFieldInGroup("ex_Mit_Log_Dte_Tme", "MIT-LOG-DTE-TME", FieldType.STRING, 15);
        ex_Mit_Log_Oprtr_Cde = ex.newFieldInGroup("ex_Mit_Log_Oprtr_Cde", "MIT-LOG-OPRTR-CDE", FieldType.STRING, 8);
        ex_Mit_Orgnl_Unit_Cde = ex.newFieldInGroup("ex_Mit_Orgnl_Unit_Cde", "MIT-ORGNL-UNIT-CDE", FieldType.STRING, 8);
        ex_Mit_Wpid = ex.newFieldInGroup("ex_Mit_Wpid", "MIT-WPID", FieldType.STRING, 6);
        ex_Mit_Step_Id = ex.newFieldInGroup("ex_Mit_Step_Id", "MIT-STEP-ID", FieldType.STRING, 6);
        ex_Mit_Status_Cde = ex.newFieldInGroup("ex_Mit_Status_Cde", "MIT-STATUS-CDE", FieldType.STRING, 4);
        ex_Extract_Date = ex.newFieldInGroup("ex_Extract_Date", "EXTRACT-DATE", FieldType.STRING, 8);
        ex_Inst_Name = ex.newFieldInGroup("ex_Inst_Name", "INST-NAME", FieldType.STRING, 76);
        ex_Mailing_Instructions = ex.newFieldInGroup("ex_Mailing_Instructions", "MAILING-INSTRUCTIONS", FieldType.STRING, 1);
        ex_Pullout_Code = ex.newFieldInGroup("ex_Pullout_Code", "PULLOUT-CODE", FieldType.STRING, 4);
        ex_State_Name = ex.newFieldInGroup("ex_State_Name", "STATE-NAME", FieldType.STRING, 15);
        ex_Res_Issue_State = ex.newFieldInGroup("ex_Res_Issue_State", "RES-ISSUE-STATE", FieldType.STRING, 2);
        ex_Orig_Issue_State = ex.newFieldInGroup("ex_Orig_Issue_State", "ORIG-ISSUE-STATE", FieldType.STRING, 2);
        ex_Ppg_Code = ex.newFieldInGroup("ex_Ppg_Code", "PPG-CODE", FieldType.STRING, 6);
        ex_Lob = ex.newFieldInGroup("ex_Lob", "LOB", FieldType.STRING, 1);
        ex_Lob_Type = ex.newFieldInGroup("ex_Lob_Type", "LOB-TYPE", FieldType.STRING, 1);
        ex_Region = ex.newFieldInGroup("ex_Region", "REGION", FieldType.STRING, 1);
        ex_Branch = ex.newFieldInGroup("ex_Branch", "BRANCH", FieldType.STRING, 1);
        ex_Need = ex.newFieldInGroup("ex_Need", "NEED", FieldType.STRING, 1);
        ex_Personal_Annuity = ex.newFieldInGroup("ex_Personal_Annuity", "PERSONAL-ANNUITY", FieldType.STRING, 1);
        ex_Graded_Ind = ex.newFieldInGroup("ex_Graded_Ind", "GRADED-IND", FieldType.STRING, 1);
        ex_Revaluation_Cde = ex.newFieldInGroup("ex_Revaluation_Cde", "REVALUATION-CDE", FieldType.STRING, 1);
        ex_Frst_Pymt_After_Iss = ex.newFieldInGroup("ex_Frst_Pymt_After_Iss", "FRST-PYMT-AFTER-ISS", FieldType.STRING, 1);
        ex_Annual_Req_Dist_Amt = ex.newFieldInGroup("ex_Annual_Req_Dist_Amt", "ANNUAL-REQ-DIST-AMT", FieldType.STRING, 15);
        ex_Frst_Req_Pymt_Year = ex.newFieldInGroup("ex_Frst_Req_Pymt_Year", "FRST-REQ-PYMT-YEAR", FieldType.STRING, 8);
        ex_Frst_Req_Pymt_Amt = ex.newFieldInGroup("ex_Frst_Req_Pymt_Amt", "FRST-REQ-PYMT-AMT", FieldType.STRING, 15);
        ex_Ownership_Code = ex.newFieldInGroup("ex_Ownership_Code", "OWNERSHIP-CODE", FieldType.STRING, 1);
        ex_Tiaa_Cntrct_Type = ex.newFieldInGroup("ex_Tiaa_Cntrct_Type", "TIAA-CNTRCT-TYPE", FieldType.STRING, 1);
        ex_Rea_Cntrct_Type = ex.newFieldInGroup("ex_Rea_Cntrct_Type", "REA-CNTRCT-TYPE", FieldType.STRING, 1);
        ex_Cref_Cntrct_Type = ex.newFieldInGroup("ex_Cref_Cntrct_Type", "CREF-CNTRCT-TYPE", FieldType.STRING, 1);
        ex_Check_Mail_Addr = ex.newFieldArrayInGroup("ex_Check_Mail_Addr", "CHECK-MAIL-ADDR", FieldType.STRING, 35, new DbsArrayController(1,5));
        ex_Bank_Accnt_No = ex.newFieldInGroup("ex_Bank_Accnt_No", "BANK-ACCNT-NO", FieldType.STRING, 21);
        ex_Bank_Transit_Code = ex.newFieldInGroup("ex_Bank_Transit_Code", "BANK-TRANSIT-CODE", FieldType.STRING, 9);
        ex_Comut_Intr_Num = ex.newFieldInGroup("ex_Comut_Intr_Num", "COMUT-INTR-NUM", FieldType.STRING, 2);
        ex_Comut_Table = ex.newFieldArrayInGroup("ex_Comut_Table", "COMUT-TABLE", FieldType.STRING, 58, new DbsArrayController(1,20));
        ex_Comut_TableRedef7 = ex.newGroupInGroup("ex_Comut_TableRedef7", "Redefines", ex_Comut_Table);
        ex_Pnd_Redefine_Comut_Table = ex_Comut_TableRedef7.newGroupArrayInGroup("ex_Pnd_Redefine_Comut_Table", "#REDEFINE-COMUT-TABLE", new DbsArrayController(1,
            20));
        ex_Comut_Grnted_Amt = ex_Pnd_Redefine_Comut_Table.newFieldInGroup("ex_Comut_Grnted_Amt", "COMUT-GRNTED-AMT", FieldType.STRING, 15);
        ex_Comut_Intr_Rate = ex_Pnd_Redefine_Comut_Table.newFieldInGroup("ex_Comut_Intr_Rate", "COMUT-INTR-RATE", FieldType.STRING, 5);
        ex_Comut_Pymt_Method = ex_Pnd_Redefine_Comut_Table.newFieldInGroup("ex_Comut_Pymt_Method", "COMUT-PYMT-METHOD", FieldType.STRING, 8);
        ex_Comut_Mort_Basis = ex_Pnd_Redefine_Comut_Table.newFieldInGroup("ex_Comut_Mort_Basis", "COMUT-MORT-BASIS", FieldType.STRING, 30);
        ex_Trnsf_Cntr_Settled = ex.newFieldInGroup("ex_Trnsf_Cntr_Settled", "TRNSF-CNTR-SETTLED", FieldType.STRING, 2);
        ex_Spec_Cntrct_Type = ex.newFieldInGroup("ex_Spec_Cntrct_Type", "SPEC-CNTRCT-TYPE", FieldType.STRING, 1);
        ex_Orig_Issue_Date = ex.newFieldInGroup("ex_Orig_Issue_Date", "ORIG-ISSUE-DATE", FieldType.STRING, 8);
        ex_Access_Amt = ex.newFieldInGroup("ex_Access_Amt", "ACCESS-AMT", FieldType.STRING, 14);
        ex_Access_Ind = ex.newFieldInGroup("ex_Access_Ind", "ACCESS-IND", FieldType.STRING, 4);
        ex_Access_Account = ex.newFieldInGroup("ex_Access_Account", "ACCESS-ACCOUNT", FieldType.STRING, 30);
        ex_Access_Mthly_Unit = ex.newFieldInGroup("ex_Access_Mthly_Unit", "ACCESS-MTHLY-UNIT", FieldType.STRING, 12);
        ex_Access_Annual_Unit = ex.newFieldInGroup("ex_Access_Annual_Unit", "ACCESS-ANNUAL-UNIT", FieldType.STRING, 12);
        ex_Roth_Ind = ex.newFieldInGroup("ex_Roth_Ind", "ROTH-IND", FieldType.STRING, 1);
        ex_Payee_Ind = ex.newFieldInGroup("ex_Payee_Ind", "PAYEE-IND", FieldType.STRING, 1);
        ex_Four_Fifty_Seven_Ind = ex.newFieldInGroup("ex_Four_Fifty_Seven_Ind", "FOUR-FIFTY-SEVEN-IND", FieldType.STRING, 1);
        ex_Trnsf_Table = ex.newFieldArrayInGroup("ex_Trnsf_Table", "TRNSF-TABLE", FieldType.STRING, 65, new DbsArrayController(1,20));
        ex_Trnsf_TableRedef8 = ex.newGroupInGroup("ex_Trnsf_TableRedef8", "Redefines", ex_Trnsf_Table);
        ex_Pnd_Redefine_Trnsf_Table = ex_Trnsf_TableRedef8.newGroupArrayInGroup("ex_Pnd_Redefine_Trnsf_Table", "#REDEFINE-TRNSF-TABLE", new DbsArrayController(1,
            20));
        ex_Trnsf_Ticker = ex_Pnd_Redefine_Trnsf_Table.newFieldInGroup("ex_Trnsf_Ticker", "TRNSF-TICKER", FieldType.STRING, 10);
        ex_Trnsf_Reval_Type = ex_Pnd_Redefine_Trnsf_Table.newFieldInGroup("ex_Trnsf_Reval_Type", "TRNSF-REVAL-TYPE", FieldType.STRING, 1);
        ex_Trnsf_Cref_Units = ex_Pnd_Redefine_Trnsf_Table.newFieldInGroup("ex_Trnsf_Cref_Units", "TRNSF-CREF-UNITS", FieldType.STRING, 8);
        ex_Reval_Type_Name = ex_Pnd_Redefine_Trnsf_Table.newFieldInGroup("ex_Reval_Type_Name", "REVAL-TYPE-NAME", FieldType.STRING, 15);
        ex_Trnsf_Ticker_Name = ex_Pnd_Redefine_Trnsf_Table.newFieldInGroup("ex_Trnsf_Ticker_Name", "TRNSF-TICKER-NAME", FieldType.STRING, 27);
        ex_Trnsf_Rece_Company = ex_Pnd_Redefine_Trnsf_Table.newFieldInGroup("ex_Trnsf_Rece_Company", "TRNSF-RECE-COMPANY", FieldType.STRING, 4);
        ex_Trnsf_Cnt_Sepa = ex.newFieldInGroup("ex_Trnsf_Cnt_Sepa", "TRNSF-CNT-SEPA", FieldType.STRING, 2);
        ex_Trnsf_Cnt_Tiaa = ex.newFieldInGroup("ex_Trnsf_Cnt_Tiaa", "TRNSF-CNT-TIAA", FieldType.STRING, 2);
        ex_Invest_Process_Dt = ex.newFieldInGroup("ex_Invest_Process_Dt", "INVEST-PROCESS-DT", FieldType.STRING, 8);
        ex_Invest_Amt_Total = ex.newFieldInGroup("ex_Invest_Amt_Total", "INVEST-AMT-TOTAL", FieldType.STRING, 19);
        ex_Invest_Num_Tot = ex.newFieldInGroup("ex_Invest_Num_Tot", "INVEST-NUM-TOT", FieldType.NUMERIC, 2);
        ex_Invest_Tbl_1 = ex.newFieldArrayInGroup("ex_Invest_Tbl_1", "INVEST-TBL-1", FieldType.STRING, 43, new DbsArrayController(1,3));
        ex_Invest_Tbl_1Redef9 = ex.newGroupInGroup("ex_Invest_Tbl_1Redef9", "Redefines", ex_Invest_Tbl_1);
        ex_Invest_Tbl = ex_Invest_Tbl_1Redef9.newGroupArrayInGroup("ex_Invest_Tbl", "INVEST-TBL", new DbsArrayController(1,3));
        ex_Invest_Num = ex_Invest_Tbl.newFieldInGroup("ex_Invest_Num", "INVEST-NUM", FieldType.NUMERIC, 2);
        ex_Invest_Orig_Amt = ex_Invest_Tbl.newFieldInGroup("ex_Invest_Orig_Amt", "INVEST-ORIG-AMT", FieldType.STRING, 19);
        ex_Invest_Tiaa_Num_1 = ex_Invest_Tbl.newFieldInGroup("ex_Invest_Tiaa_Num_1", "INVEST-TIAA-NUM-1", FieldType.STRING, 11);
        ex_Invest_Cref_Num_1 = ex_Invest_Tbl.newFieldInGroup("ex_Invest_Cref_Num_1", "INVEST-CREF-NUM-1", FieldType.STRING, 11);
        ex_Invest_Fund_Amt = ex.newFieldArrayInGroup("ex_Invest_Fund_Amt", "INVEST-FUND-AMT", FieldType.STRING, 19, new DbsArrayController(1,3,1,25));
        ex_Invest_Unit_Price = ex.newFieldArrayInGroup("ex_Invest_Unit_Price", "INVEST-UNIT-PRICE", FieldType.STRING, 17, new DbsArrayController(1,3,1,
            25));
        ex_Invest_Units = ex.newFieldArrayInGroup("ex_Invest_Units", "INVEST-UNITS", FieldType.STRING, 15, new DbsArrayController(1,3,1,25));
        ex_Invest_Fund_Name_From = ex.newFieldArrayInGroup("ex_Invest_Fund_Name_From", "INVEST-FUND-NAME-FROM", FieldType.STRING, 30, new DbsArrayController(1,
            3,1,25));
        ex_Invest_Tiaa_Num_To = ex.newFieldInGroup("ex_Invest_Tiaa_Num_To", "INVEST-TIAA-NUM-TO", FieldType.STRING, 11);
        ex_Invest_Cref_Num_To = ex.newFieldInGroup("ex_Invest_Cref_Num_To", "INVEST-CREF-NUM-TO", FieldType.STRING, 11);
        ex_Invest_Tbl_3 = ex.newFieldArrayInGroup("ex_Invest_Tbl_3", "INVEST-TBL-3", FieldType.STRING, 81, new DbsArrayController(1,25));
        ex_Invest_Tbl_3Redef10 = ex.newGroupInGroup("ex_Invest_Tbl_3Redef10", "Redefines", ex_Invest_Tbl_3);
        ex_Invest_Table_3 = ex_Invest_Tbl_3Redef10.newGroupArrayInGroup("ex_Invest_Table_3", "INVEST-TABLE-3", new DbsArrayController(1,25));
        ex_Invest_Fund_Amt_To = ex_Invest_Table_3.newFieldInGroup("ex_Invest_Fund_Amt_To", "INVEST-FUND-AMT-TO", FieldType.STRING, 19);
        ex_Invest_Unit_Price_To = ex_Invest_Table_3.newFieldInGroup("ex_Invest_Unit_Price_To", "INVEST-UNIT-PRICE-TO", FieldType.STRING, 17);
        ex_Invest_Units_To = ex_Invest_Table_3.newFieldInGroup("ex_Invest_Units_To", "INVEST-UNITS-TO", FieldType.STRING, 15);
        ex_Invest_Fund_Name_To = ex_Invest_Table_3.newFieldInGroup("ex_Invest_Fund_Name_To", "INVEST-FUND-NAME-TO", FieldType.STRING, 30);
        ex_Plan_Name = ex.newFieldArrayInGroup("ex_Plan_Name", "PLAN-NAME", FieldType.STRING, 64, new DbsArrayController(1,3));
        ex_Ia_Shr_Class = ex.newFieldInGroup("ex_Ia_Shr_Class", "IA-SHR-CLASS", FieldType.STRING, 2);
        ex_Multi_Plan_Ind = ex.newFieldInGroup("ex_Multi_Plan_Ind", "MULTI-PLAN-IND", FieldType.STRING, 1);
        ex_Plan_Num = ex.newFieldInGroup("ex_Plan_Num", "PLAN-NUM", FieldType.STRING, 6);
        ex_Multi_Plan_Tbl = ex.newFieldArrayInGroup("ex_Multi_Plan_Tbl", "MULTI-PLAN-TBL", FieldType.STRING, 12, new DbsArrayController(1,15));
        ex_Multi_Plan_TblRedef11 = ex.newGroupInGroup("ex_Multi_Plan_TblRedef11", "Redefines", ex_Multi_Plan_Tbl);
        ex_Multi_Plan_Info = ex_Multi_Plan_TblRedef11.newGroupArrayInGroup("ex_Multi_Plan_Info", "MULTI-PLAN-INFO", new DbsArrayController(1,15));
        ex_Multi_Plan_No = ex_Multi_Plan_Info.newFieldInGroup("ex_Multi_Plan_No", "MULTI-PLAN-NO", FieldType.STRING, 6);
        ex_Multi_Sub_Plan = ex_Multi_Plan_Info.newFieldInGroup("ex_Multi_Sub_Plan", "MULTI-SUB-PLAN", FieldType.STRING, 6);
        ex_First_Tpa_Or_Ipro_Ind = ex.newFieldInGroup("ex_First_Tpa_Or_Ipro_Ind", "FIRST-TPA-OR-IPRO-IND", FieldType.STRING, 1);
        filler01 = ex.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 708);
        exRedef12 = newGroupInRecord("exRedef12", "Redefines", ex);
        ex_Contract_1 = exRedef12.newFieldInGroup("ex_Contract_1", "CONTRACT-1", FieldType.STRING, 250);
        ex_Contract_2 = exRedef12.newFieldInGroup("ex_Contract_2", "CONTRACT-2", FieldType.STRING, 250);
        ex_Contract_3 = exRedef12.newFieldInGroup("ex_Contract_3", "CONTRACT-3", FieldType.STRING, 250);
        ex_Contract_4 = exRedef12.newFieldInGroup("ex_Contract_4", "CONTRACT-4", FieldType.STRING, 250);
        ex_Contract_5 = exRedef12.newFieldInGroup("ex_Contract_5", "CONTRACT-5", FieldType.STRING, 250);
        ex_Contract_6 = exRedef12.newFieldInGroup("ex_Contract_6", "CONTRACT-6", FieldType.STRING, 250);
        ex_Contract_7 = exRedef12.newFieldInGroup("ex_Contract_7", "CONTRACT-7", FieldType.STRING, 250);
        ex_Contract_8 = exRedef12.newFieldInGroup("ex_Contract_8", "CONTRACT-8", FieldType.STRING, 250);
        ex_Contract_9 = exRedef12.newFieldInGroup("ex_Contract_9", "CONTRACT-9", FieldType.STRING, 250);
        ex_Contract_10 = exRedef12.newFieldInGroup("ex_Contract_10", "CONTRACT-10", FieldType.STRING, 250);
        ex_Contract_11 = exRedef12.newFieldInGroup("ex_Contract_11", "CONTRACT-11", FieldType.STRING, 250);
        ex_Contract_12 = exRedef12.newFieldInGroup("ex_Contract_12", "CONTRACT-12", FieldType.STRING, 250);
        ex_Contract_13 = exRedef12.newFieldInGroup("ex_Contract_13", "CONTRACT-13", FieldType.STRING, 250);
        ex_Contract_14 = exRedef12.newFieldInGroup("ex_Contract_14", "CONTRACT-14", FieldType.STRING, 250);
        ex_Contract_15 = exRedef12.newFieldInGroup("ex_Contract_15", "CONTRACT-15", FieldType.STRING, 250);
        ex_Contract_16 = exRedef12.newFieldInGroup("ex_Contract_16", "CONTRACT-16", FieldType.STRING, 250);
        ex_Contract_17 = exRedef12.newFieldInGroup("ex_Contract_17", "CONTRACT-17", FieldType.STRING, 250);
        ex_Contract_18 = exRedef12.newFieldInGroup("ex_Contract_18", "CONTRACT-18", FieldType.STRING, 250);
        ex_Contract_19 = exRedef12.newFieldInGroup("ex_Contract_19", "CONTRACT-19", FieldType.STRING, 250);
        ex_Contract_20 = exRedef12.newFieldInGroup("ex_Contract_20", "CONTRACT-20", FieldType.STRING, 250);
        ex_Contract_21 = exRedef12.newFieldInGroup("ex_Contract_21", "CONTRACT-21", FieldType.STRING, 250);
        ex_Contract_22 = exRedef12.newFieldInGroup("ex_Contract_22", "CONTRACT-22", FieldType.STRING, 250);
        ex_Contract_23 = exRedef12.newFieldInGroup("ex_Contract_23", "CONTRACT-23", FieldType.STRING, 250);
        ex_Contract_24 = exRedef12.newFieldInGroup("ex_Contract_24", "CONTRACT-24", FieldType.STRING, 250);
        ex_Contract_25 = exRedef12.newFieldInGroup("ex_Contract_25", "CONTRACT-25", FieldType.STRING, 250);
        ex_Contract_26 = exRedef12.newFieldInGroup("ex_Contract_26", "CONTRACT-26", FieldType.STRING, 250);
        ex_Contract_27 = exRedef12.newFieldInGroup("ex_Contract_27", "CONTRACT-27", FieldType.STRING, 250);
        ex_Contract_28 = exRedef12.newFieldInGroup("ex_Contract_28", "CONTRACT-28", FieldType.STRING, 250);
        ex_Contract_29 = exRedef12.newFieldInGroup("ex_Contract_29", "CONTRACT-29", FieldType.STRING, 250);
        ex_Contract_30 = exRedef12.newFieldInGroup("ex_Contract_30", "CONTRACT-30", FieldType.STRING, 250);
        ex_Contract_31 = exRedef12.newFieldInGroup("ex_Contract_31", "CONTRACT-31", FieldType.STRING, 250);
        ex_Contract_32 = exRedef12.newFieldInGroup("ex_Contract_32", "CONTRACT-32", FieldType.STRING, 250);
        ex_Contract_33 = exRedef12.newFieldInGroup("ex_Contract_33", "CONTRACT-33", FieldType.STRING, 250);
        ex_Contract_34 = exRedef12.newFieldInGroup("ex_Contract_34", "CONTRACT-34", FieldType.STRING, 250);
        ex_Contract_35 = exRedef12.newFieldInGroup("ex_Contract_35", "CONTRACT-35", FieldType.STRING, 250);
        ex_Contract_36 = exRedef12.newFieldInGroup("ex_Contract_36", "CONTRACT-36", FieldType.STRING, 250);
        ex_Contract_37 = exRedef12.newFieldInGroup("ex_Contract_37", "CONTRACT-37", FieldType.STRING, 250);
        ex_Contract_38 = exRedef12.newFieldInGroup("ex_Contract_38", "CONTRACT-38", FieldType.STRING, 250);
        ex_Contract_39 = exRedef12.newFieldInGroup("ex_Contract_39", "CONTRACT-39", FieldType.STRING, 250);
        ex_Contract_40 = exRedef12.newFieldInGroup("ex_Contract_40", "CONTRACT-40", FieldType.STRING, 250);
        ex_Contract_41 = exRedef12.newFieldInGroup("ex_Contract_41", "CONTRACT-41", FieldType.STRING, 250);
        ex_Contract_42 = exRedef12.newFieldInGroup("ex_Contract_42", "CONTRACT-42", FieldType.STRING, 250);
        ex_Contract_43 = exRedef12.newFieldInGroup("ex_Contract_43", "CONTRACT-43", FieldType.STRING, 250);
        ex_Contract_44 = exRedef12.newFieldInGroup("ex_Contract_44", "CONTRACT-44", FieldType.STRING, 250);
        ex_Contract_45 = exRedef12.newFieldInGroup("ex_Contract_45", "CONTRACT-45", FieldType.STRING, 250);
        ex_Contract_46 = exRedef12.newFieldInGroup("ex_Contract_46", "CONTRACT-46", FieldType.STRING, 250);
        ex_Contract_47 = exRedef12.newFieldInGroup("ex_Contract_47", "CONTRACT-47", FieldType.STRING, 250);
        ex_Contract_48 = exRedef12.newFieldInGroup("ex_Contract_48", "CONTRACT-48", FieldType.STRING, 250);
        ex_Contract_49 = exRedef12.newFieldInGroup("ex_Contract_49", "CONTRACT-49", FieldType.STRING, 250);
        ex_Contract_50 = exRedef12.newFieldInGroup("ex_Contract_50", "CONTRACT-50", FieldType.STRING, 250);
        ex_Contract_51 = exRedef12.newFieldInGroup("ex_Contract_51", "CONTRACT-51", FieldType.STRING, 250);
        ex_Contract_52 = exRedef12.newFieldInGroup("ex_Contract_52", "CONTRACT-52", FieldType.STRING, 250);
        ex_Contract_53 = exRedef12.newFieldInGroup("ex_Contract_53", "CONTRACT-53", FieldType.STRING, 250);
        ex_Contract_54 = exRedef12.newFieldInGroup("ex_Contract_54", "CONTRACT-54", FieldType.STRING, 250);
        ex_Contract_55 = exRedef12.newFieldInGroup("ex_Contract_55", "CONTRACT-55", FieldType.STRING, 250);
        ex_Contract_56 = exRedef12.newFieldInGroup("ex_Contract_56", "CONTRACT-56", FieldType.STRING, 250);
        ex_Contract_57 = exRedef12.newFieldInGroup("ex_Contract_57", "CONTRACT-57", FieldType.STRING, 250);
        ex_Contract_58 = exRedef12.newFieldInGroup("ex_Contract_58", "CONTRACT-58", FieldType.STRING, 250);
        ex_Contract_59 = exRedef12.newFieldInGroup("ex_Contract_59", "CONTRACT-59", FieldType.STRING, 250);
        ex_Contract_60 = exRedef12.newFieldInGroup("ex_Contract_60", "CONTRACT-60", FieldType.STRING, 250);
        ex_Contract_61 = exRedef12.newFieldInGroup("ex_Contract_61", "CONTRACT-61", FieldType.STRING, 250);
        ex_Contract_62 = exRedef12.newFieldInGroup("ex_Contract_62", "CONTRACT-62", FieldType.STRING, 250);
        ex_Contract_63 = exRedef12.newFieldInGroup("ex_Contract_63", "CONTRACT-63", FieldType.STRING, 250);
        ex_Contract_64 = exRedef12.newFieldInGroup("ex_Contract_64", "CONTRACT-64", FieldType.STRING, 250);
        ex_Contract_65 = exRedef12.newFieldInGroup("ex_Contract_65", "CONTRACT-65", FieldType.STRING, 250);
        ex_Contract_66 = exRedef12.newFieldInGroup("ex_Contract_66", "CONTRACT-66", FieldType.STRING, 250);
        ex_Contract_67 = exRedef12.newFieldInGroup("ex_Contract_67", "CONTRACT-67", FieldType.STRING, 250);
        ex_Contract_68 = exRedef12.newFieldInGroup("ex_Contract_68", "CONTRACT-68", FieldType.STRING, 250);
        ex_Contract_69 = exRedef12.newFieldInGroup("ex_Contract_69", "CONTRACT-69", FieldType.STRING, 250);
        ex_Contract_70 = exRedef12.newFieldInGroup("ex_Contract_70", "CONTRACT-70", FieldType.STRING, 250);
        ex_Contract_71 = exRedef12.newFieldInGroup("ex_Contract_71", "CONTRACT-71", FieldType.STRING, 199);

        this.setRecordName("LdaCislext");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCislext() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
