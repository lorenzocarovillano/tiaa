/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:23 PM
**        * FROM NATURAL PDA     : CISA620
************************************************************
**        * FILE NAME            : PdaCisa620.java
**        * CLASS NAME           : PdaCisa620
**        * INSTANCE NAME        : PdaCisa620
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCisa620 extends PdaBase
{
    // Properties
    private DbsGroup cisa620;
    private DbsField cisa620_Pnd_Cisa_Bene_Tiaa_Nbr;
    private DbsField cisa620_Pnd_Cisa_Bene_Cref_Nbr;
    private DbsField cisa620_Pnd_Cisa_Bene_Rqst_Id;
    private DbsField cisa620_Pnd_Cisa_Bene_Unique_Id_Nbr;
    private DbsField cisa620_Pnd_Cisa_Bene_Rqst_Id_Key;
    private DbsField cisa620_Pnd_Cisa_Resid_Issue_State;
    private DbsField cisa620_Pnd_Cisa_Bene_Std_Free;
    private DbsField cisa620_Pnd_Cisa_Bene_Estate;
    private DbsField cisa620_Pnd_Cisa_Bene_Trust;
    private DbsField cisa620_Pnd_Cisa_Bene_Category;
    private DbsField cisa620_Pnd_Cisa_Prmry_Bene_Child_Prvsn;
    private DbsField cisa620_Pnd_Cisa_Cntgnt_Bene_Child_Prvsn;
    private DbsField cisa620_Pnd_Cisa_Clcltn_Bene_Dod_A;
    private DbsField cisa620_Pnd_Cisa_Clcltn_Bene_Crossover;
    private DbsField cisa620_Pnd_Cisa_Clcltn_Bene_Name;
    private DbsField cisa620_Pnd_Cisa_Clcltn_Bene_Rltnshp_Cde;
    private DbsField cisa620_Pnd_Cisa_Clcltn_Bene_Ssn_Nbr;
    private DbsField cisa620_Pnd_Cisa_Clcltn_Bene_Dob_A;
    private DbsField cisa620_Pnd_Cisa_Clcltn_Bene_Sex_Cde;
    private DbsField cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Method;
    private DbsField cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Desc;
    private DbsField cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Bene;
    private DbsField cisa620_Pnd_Cisa_Prmry_Std_Or_Text_Ind;
    private DbsField cisa620_Pnd_Cisa_Prmry_Lump_Sum_Ind;
    private DbsField cisa620_Pnd_Cisa_Prmry_Auto_Cmnt_Val_Ind;
    private DbsField cisa620_Pnd_Cisa_Prmry_Cnt;
    private DbsGroup cisa620_Pnd_Cisa_Prmry_Table;
    private DbsField cisa620_Pnd_Cisa_Prmry_Print_Line;
    private DbsField cisa620_Pnd_Cisa_Cntgnt_Std_Or_Text_Ind;
    private DbsField cisa620_Pnd_Cisa_Cntgnt_Lump_Sum_Ind;
    private DbsField cisa620_Pnd_Cisa_Cntgnt_Auto_Cmnt_Val_Ind;
    private DbsField cisa620_Pnd_Cisa_Cntgnt_Cnt;
    private DbsGroup cisa620_Pnd_Cisa_Cntgnt_Table;
    private DbsField cisa620_Pnd_Cisa_Cntgnt_Print_Line;
    private DbsField cisa620_Pnd_Cisa_Cntgnt_Contain_Estate;
    private DbsField cisa620_Pnd_Cisa_Return_Code;
    private DbsField cisa620_Pnd_Cisa_Msg;

    public DbsGroup getCisa620() { return cisa620; }

    public DbsField getCisa620_Pnd_Cisa_Bene_Tiaa_Nbr() { return cisa620_Pnd_Cisa_Bene_Tiaa_Nbr; }

    public DbsField getCisa620_Pnd_Cisa_Bene_Cref_Nbr() { return cisa620_Pnd_Cisa_Bene_Cref_Nbr; }

    public DbsField getCisa620_Pnd_Cisa_Bene_Rqst_Id() { return cisa620_Pnd_Cisa_Bene_Rqst_Id; }

    public DbsField getCisa620_Pnd_Cisa_Bene_Unique_Id_Nbr() { return cisa620_Pnd_Cisa_Bene_Unique_Id_Nbr; }

    public DbsField getCisa620_Pnd_Cisa_Bene_Rqst_Id_Key() { return cisa620_Pnd_Cisa_Bene_Rqst_Id_Key; }

    public DbsField getCisa620_Pnd_Cisa_Resid_Issue_State() { return cisa620_Pnd_Cisa_Resid_Issue_State; }

    public DbsField getCisa620_Pnd_Cisa_Bene_Std_Free() { return cisa620_Pnd_Cisa_Bene_Std_Free; }

    public DbsField getCisa620_Pnd_Cisa_Bene_Estate() { return cisa620_Pnd_Cisa_Bene_Estate; }

    public DbsField getCisa620_Pnd_Cisa_Bene_Trust() { return cisa620_Pnd_Cisa_Bene_Trust; }

    public DbsField getCisa620_Pnd_Cisa_Bene_Category() { return cisa620_Pnd_Cisa_Bene_Category; }

    public DbsField getCisa620_Pnd_Cisa_Prmry_Bene_Child_Prvsn() { return cisa620_Pnd_Cisa_Prmry_Bene_Child_Prvsn; }

    public DbsField getCisa620_Pnd_Cisa_Cntgnt_Bene_Child_Prvsn() { return cisa620_Pnd_Cisa_Cntgnt_Bene_Child_Prvsn; }

    public DbsField getCisa620_Pnd_Cisa_Clcltn_Bene_Dod_A() { return cisa620_Pnd_Cisa_Clcltn_Bene_Dod_A; }

    public DbsField getCisa620_Pnd_Cisa_Clcltn_Bene_Crossover() { return cisa620_Pnd_Cisa_Clcltn_Bene_Crossover; }

    public DbsField getCisa620_Pnd_Cisa_Clcltn_Bene_Name() { return cisa620_Pnd_Cisa_Clcltn_Bene_Name; }

    public DbsField getCisa620_Pnd_Cisa_Clcltn_Bene_Rltnshp_Cde() { return cisa620_Pnd_Cisa_Clcltn_Bene_Rltnshp_Cde; }

    public DbsField getCisa620_Pnd_Cisa_Clcltn_Bene_Ssn_Nbr() { return cisa620_Pnd_Cisa_Clcltn_Bene_Ssn_Nbr; }

    public DbsField getCisa620_Pnd_Cisa_Clcltn_Bene_Dob_A() { return cisa620_Pnd_Cisa_Clcltn_Bene_Dob_A; }

    public DbsField getCisa620_Pnd_Cisa_Clcltn_Bene_Sex_Cde() { return cisa620_Pnd_Cisa_Clcltn_Bene_Sex_Cde; }

    public DbsField getCisa620_Pnd_Cisa_Clcltn_Bene_Calc_Method() { return cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Method; }

    public DbsField getCisa620_Pnd_Cisa_Clcltn_Bene_Calc_Desc() { return cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Desc; }

    public DbsField getCisa620_Pnd_Cisa_Clcltn_Bene_Calc_Bene() { return cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Bene; }

    public DbsField getCisa620_Pnd_Cisa_Prmry_Std_Or_Text_Ind() { return cisa620_Pnd_Cisa_Prmry_Std_Or_Text_Ind; }

    public DbsField getCisa620_Pnd_Cisa_Prmry_Lump_Sum_Ind() { return cisa620_Pnd_Cisa_Prmry_Lump_Sum_Ind; }

    public DbsField getCisa620_Pnd_Cisa_Prmry_Auto_Cmnt_Val_Ind() { return cisa620_Pnd_Cisa_Prmry_Auto_Cmnt_Val_Ind; }

    public DbsField getCisa620_Pnd_Cisa_Prmry_Cnt() { return cisa620_Pnd_Cisa_Prmry_Cnt; }

    public DbsGroup getCisa620_Pnd_Cisa_Prmry_Table() { return cisa620_Pnd_Cisa_Prmry_Table; }

    public DbsField getCisa620_Pnd_Cisa_Prmry_Print_Line() { return cisa620_Pnd_Cisa_Prmry_Print_Line; }

    public DbsField getCisa620_Pnd_Cisa_Cntgnt_Std_Or_Text_Ind() { return cisa620_Pnd_Cisa_Cntgnt_Std_Or_Text_Ind; }

    public DbsField getCisa620_Pnd_Cisa_Cntgnt_Lump_Sum_Ind() { return cisa620_Pnd_Cisa_Cntgnt_Lump_Sum_Ind; }

    public DbsField getCisa620_Pnd_Cisa_Cntgnt_Auto_Cmnt_Val_Ind() { return cisa620_Pnd_Cisa_Cntgnt_Auto_Cmnt_Val_Ind; }

    public DbsField getCisa620_Pnd_Cisa_Cntgnt_Cnt() { return cisa620_Pnd_Cisa_Cntgnt_Cnt; }

    public DbsGroup getCisa620_Pnd_Cisa_Cntgnt_Table() { return cisa620_Pnd_Cisa_Cntgnt_Table; }

    public DbsField getCisa620_Pnd_Cisa_Cntgnt_Print_Line() { return cisa620_Pnd_Cisa_Cntgnt_Print_Line; }

    public DbsField getCisa620_Pnd_Cisa_Cntgnt_Contain_Estate() { return cisa620_Pnd_Cisa_Cntgnt_Contain_Estate; }

    public DbsField getCisa620_Pnd_Cisa_Return_Code() { return cisa620_Pnd_Cisa_Return_Code; }

    public DbsField getCisa620_Pnd_Cisa_Msg() { return cisa620_Pnd_Cisa_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cisa620 = dbsRecord.newGroupInRecord("cisa620", "CISA620");
        cisa620.setParameterOption(ParameterOption.ByReference);
        cisa620_Pnd_Cisa_Bene_Tiaa_Nbr = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Bene_Tiaa_Nbr", "#CISA-BENE-TIAA-NBR", FieldType.STRING, 10);
        cisa620_Pnd_Cisa_Bene_Cref_Nbr = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Bene_Cref_Nbr", "#CISA-BENE-CREF-NBR", FieldType.STRING, 10);
        cisa620_Pnd_Cisa_Bene_Rqst_Id = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Bene_Rqst_Id", "#CISA-BENE-RQST-ID", FieldType.STRING, 8);
        cisa620_Pnd_Cisa_Bene_Unique_Id_Nbr = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Bene_Unique_Id_Nbr", "#CISA-BENE-UNIQUE-ID-NBR", FieldType.NUMERIC, 
            12);
        cisa620_Pnd_Cisa_Bene_Rqst_Id_Key = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Bene_Rqst_Id_Key", "#CISA-BENE-RQST-ID-KEY", FieldType.STRING, 35);
        cisa620_Pnd_Cisa_Resid_Issue_State = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Resid_Issue_State", "#CISA-RESID-ISSUE-STATE", FieldType.STRING, 
            2);
        cisa620_Pnd_Cisa_Bene_Std_Free = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Bene_Std_Free", "#CISA-BENE-STD-FREE", FieldType.STRING, 1);
        cisa620_Pnd_Cisa_Bene_Estate = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Bene_Estate", "#CISA-BENE-ESTATE", FieldType.STRING, 1);
        cisa620_Pnd_Cisa_Bene_Trust = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Bene_Trust", "#CISA-BENE-TRUST", FieldType.STRING, 1);
        cisa620_Pnd_Cisa_Bene_Category = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Bene_Category", "#CISA-BENE-CATEGORY", FieldType.STRING, 1);
        cisa620_Pnd_Cisa_Prmry_Bene_Child_Prvsn = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Prmry_Bene_Child_Prvsn", "#CISA-PRMRY-BENE-CHILD-PRVSN", FieldType.STRING, 
            1);
        cisa620_Pnd_Cisa_Cntgnt_Bene_Child_Prvsn = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Cntgnt_Bene_Child_Prvsn", "#CISA-CNTGNT-BENE-CHILD-PRVSN", 
            FieldType.STRING, 1);
        cisa620_Pnd_Cisa_Clcltn_Bene_Dod_A = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Clcltn_Bene_Dod_A", "#CISA-CLCLTN-BENE-DOD-A", FieldType.STRING, 
            8);
        cisa620_Pnd_Cisa_Clcltn_Bene_Crossover = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Clcltn_Bene_Crossover", "#CISA-CLCLTN-BENE-CROSSOVER", FieldType.STRING, 
            1);
        cisa620_Pnd_Cisa_Clcltn_Bene_Name = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Clcltn_Bene_Name", "#CISA-CLCLTN-BENE-NAME", FieldType.STRING, 35);
        cisa620_Pnd_Cisa_Clcltn_Bene_Rltnshp_Cde = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Clcltn_Bene_Rltnshp_Cde", "#CISA-CLCLTN-BENE-RLTNSHP-CDE", 
            FieldType.STRING, 2);
        cisa620_Pnd_Cisa_Clcltn_Bene_Ssn_Nbr = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Clcltn_Bene_Ssn_Nbr", "#CISA-CLCLTN-BENE-SSN-NBR", FieldType.NUMERIC, 
            9);
        cisa620_Pnd_Cisa_Clcltn_Bene_Dob_A = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Clcltn_Bene_Dob_A", "#CISA-CLCLTN-BENE-DOB-A", FieldType.STRING, 
            8);
        cisa620_Pnd_Cisa_Clcltn_Bene_Sex_Cde = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Clcltn_Bene_Sex_Cde", "#CISA-CLCLTN-BENE-SEX-CDE", FieldType.STRING, 
            1);
        cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Method = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Method", "#CISA-CLCLTN-BENE-CALC-METHOD", 
            FieldType.STRING, 1);
        cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Desc = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Desc", "#CISA-CLCLTN-BENE-CALC-DESC", FieldType.STRING, 
            15);
        cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Bene = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Clcltn_Bene_Calc_Bene", "#CISA-CLCLTN-BENE-CALC-BENE", FieldType.STRING, 
            25);
        cisa620_Pnd_Cisa_Prmry_Std_Or_Text_Ind = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Prmry_Std_Or_Text_Ind", "#CISA-PRMRY-STD-OR-TEXT-IND", FieldType.STRING, 
            1);
        cisa620_Pnd_Cisa_Prmry_Lump_Sum_Ind = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Prmry_Lump_Sum_Ind", "#CISA-PRMRY-LUMP-SUM-IND", FieldType.STRING, 
            1);
        cisa620_Pnd_Cisa_Prmry_Auto_Cmnt_Val_Ind = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Prmry_Auto_Cmnt_Val_Ind", "#CISA-PRMRY-AUTO-CMNT-VAL-IND", 
            FieldType.STRING, 1);
        cisa620_Pnd_Cisa_Prmry_Cnt = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Prmry_Cnt", "#CISA-PRMRY-CNT", FieldType.NUMERIC, 2);
        cisa620_Pnd_Cisa_Prmry_Table = cisa620.newGroupArrayInGroup("cisa620_Pnd_Cisa_Prmry_Table", "#CISA-PRMRY-TABLE", new DbsArrayController(1,80));
        cisa620_Pnd_Cisa_Prmry_Print_Line = cisa620_Pnd_Cisa_Prmry_Table.newFieldInGroup("cisa620_Pnd_Cisa_Prmry_Print_Line", "#CISA-PRMRY-PRINT-LINE", 
            FieldType.STRING, 85);
        cisa620_Pnd_Cisa_Cntgnt_Std_Or_Text_Ind = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Cntgnt_Std_Or_Text_Ind", "#CISA-CNTGNT-STD-OR-TEXT-IND", FieldType.STRING, 
            1);
        cisa620_Pnd_Cisa_Cntgnt_Lump_Sum_Ind = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Cntgnt_Lump_Sum_Ind", "#CISA-CNTGNT-LUMP-SUM-IND", FieldType.STRING, 
            1);
        cisa620_Pnd_Cisa_Cntgnt_Auto_Cmnt_Val_Ind = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Cntgnt_Auto_Cmnt_Val_Ind", "#CISA-CNTGNT-AUTO-CMNT-VAL-IND", 
            FieldType.STRING, 1);
        cisa620_Pnd_Cisa_Cntgnt_Cnt = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Cntgnt_Cnt", "#CISA-CNTGNT-CNT", FieldType.NUMERIC, 2);
        cisa620_Pnd_Cisa_Cntgnt_Table = cisa620.newGroupArrayInGroup("cisa620_Pnd_Cisa_Cntgnt_Table", "#CISA-CNTGNT-TABLE", new DbsArrayController(1,80));
        cisa620_Pnd_Cisa_Cntgnt_Print_Line = cisa620_Pnd_Cisa_Cntgnt_Table.newFieldInGroup("cisa620_Pnd_Cisa_Cntgnt_Print_Line", "#CISA-CNTGNT-PRINT-LINE", 
            FieldType.STRING, 85);
        cisa620_Pnd_Cisa_Cntgnt_Contain_Estate = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Cntgnt_Contain_Estate", "#CISA-CNTGNT-CONTAIN-ESTATE", FieldType.STRING, 
            1);
        cisa620_Pnd_Cisa_Return_Code = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Return_Code", "#CISA-RETURN-CODE", FieldType.NUMERIC, 2);
        cisa620_Pnd_Cisa_Msg = cisa620.newFieldInGroup("cisa620_Pnd_Cisa_Msg", "#CISA-MSG", FieldType.STRING, 79);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCisa620(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

