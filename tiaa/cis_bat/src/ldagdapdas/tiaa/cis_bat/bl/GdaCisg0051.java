/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:48:27 PM
**        * FROM NATURAL GDA     : CISG0051
************************************************************
**        * FILE NAME            : GdaCisg0051.java
**        * CLASS NAME           : GdaCisg0051
**        * INSTANCE NAME        : GdaCisg0051
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import java.util.ArrayList;
import java.util.List;

import tiaa.tiaacommon.bl.*;

public final class GdaCisg0051 extends DbsRecord
{
    private static ThreadLocal<List<GdaCisg0051>> _instance = new ThreadLocal<List<GdaCisg0051>>();

    // Properties
    private DbsGroup pnd_Pnd_Cisg0051;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_Data;
    private DbsGroup pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Cis_Guid;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Ping_Ind;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Sender_Appl_Id;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Appl_Id;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Module_Code;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Module_Name;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Pda_Ctr;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Pda_Length;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Date_Sent;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Time_Sent;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Header_Filler;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Data;
    private DbsGroup pnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef2;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Data_In;
    private DbsGroup pnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef3;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Data_Response;
    private DbsField pnd_Pnd_Cisg0051_Pnd_Pnd_Data_Out;

    public DbsGroup getPnd_Pnd_Cisg0051() { return pnd_Pnd_Cisg0051; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_Data() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_Data; }

    public DbsGroup getPnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Cis_Guid() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Cis_Guid; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Ping_Ind() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Ping_Ind; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Sender_Appl_Id() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Sender_Appl_Id; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Appl_Id() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Appl_Id; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Module_Code() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Module_Code; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Module_Name() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Module_Name; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Pda_Ctr() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Pda_Ctr; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Pda_Length() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Pda_Length; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Date_Sent() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Date_Sent; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Time_Sent() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Time_Sent; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Header_Filler() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Header_Filler; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Data() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Data; }

    public DbsGroup getPnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef2() { return pnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef2; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Data_In() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Data_In; }

    public DbsGroup getPnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef3() { return pnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef3; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Data_Response() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Data_Response; }

    public DbsField getPnd_Pnd_Cisg0051_Pnd_Pnd_Data_Out() { return pnd_Pnd_Cisg0051_Pnd_Pnd_Data_Out; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Pnd_Cisg0051 = newGroupInRecord("pnd_Pnd_Cisg0051", "##CISG0051");
        pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_Data = pnd_Pnd_Cisg0051.newFieldArrayInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_Data", "##CISG0051-DATA", FieldType.STRING, 
            1, new DbsArrayController(1,32760));
        pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1 = pnd_Pnd_Cisg0051.newGroupInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1", "Redefines", pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_Data);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Cis_Guid = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Cis_Guid", "##CIS-GUID", 
            FieldType.STRING, 60);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Ping_Ind = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Ping_Ind", 
            "##MSG-PING-IND", FieldType.STRING, 1);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Sender_Appl_Id = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Sender_Appl_Id", 
            "##SENDER-APPL-ID", FieldType.STRING, 8);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Appl_Id = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Appl_Id", "##TGT-APPL-ID", 
            FieldType.STRING, 8);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Module_Code = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Module_Code", 
            "##TGT-MODULE-CODE", FieldType.STRING, 1);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Module_Name = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Tgt_Module_Name", 
            "##TGT-MODULE-NAME", FieldType.STRING, 8);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Pda_Ctr = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Pda_Ctr", "##PDA-CTR", 
            FieldType.NUMERIC, 2);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Pda_Length = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Pda_Length", "##PDA-LENGTH", 
            FieldType.NUMERIC, 5);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Date_Sent = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Date_Sent", 
            "##MSG-DATE-SENT", FieldType.STRING, 10);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Time_Sent = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Msg_Time_Sent", 
            "##MSG-TIME-SENT", FieldType.STRING, 8);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Header_Filler = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Header_Filler", 
            "##HEADER-FILLER", FieldType.STRING, 89);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Data = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newFieldArrayInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Data", "##DATA", FieldType.STRING, 
            1, new DbsArrayController(1,32560));
        pnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef2 = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newGroupInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef2", "Redefines", 
            pnd_Pnd_Cisg0051_Pnd_Pnd_Data);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Data_In = pnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef2.newFieldArrayInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Data_In", "##DATA-IN", FieldType.STRING, 
            1, new DbsArrayController(1,32560));
        pnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef3 = pnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_DataRedef1.newGroupInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef3", "Redefines", 
            pnd_Pnd_Cisg0051_Pnd_Pnd_Data);
        pnd_Pnd_Cisg0051_Pnd_Pnd_Data_Response = pnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef3.newFieldArrayInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Data_Response", "##DATA-RESPONSE", 
            FieldType.STRING, 1, new DbsArrayController(1,200));
        pnd_Pnd_Cisg0051_Pnd_Pnd_Data_Out = pnd_Pnd_Cisg0051_Pnd_Pnd_DataRedef3.newFieldArrayInGroup("pnd_Pnd_Cisg0051_Pnd_Pnd_Data_Out", "##DATA-OUT", 
            FieldType.STRING, 1, new DbsArrayController(1,32360));

        this.setRecordName("GdaCisg0051");
    }
    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    private GdaCisg0051() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }

    // Instance Property
    public static GdaCisg0051 getInstance(int callnatLevel) throws Exception
    {
        if (_instance.get() == null)
            _instance.set(new ArrayList<GdaCisg0051>());

        if (_instance.get().size() < callnatLevel)
        {
            while (_instance.get().size() < callnatLevel)
            {
                _instance.get().add(new GdaCisg0051());
            }
        }
        else if (_instance.get().size() > callnatLevel)
        {
            while(_instance.get().size() > callnatLevel)
            _instance.get().remove(_instance.get().size() - 1);
        }

        return _instance.get().get(callnatLevel - 1);
    }
}

