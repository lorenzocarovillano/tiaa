/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:43 PM
**        * FROM NATURAL LDA     : CISL610
************************************************************
**        * FILE NAME            : LdaCisl610.java
**        * CLASS NAME           : LdaCisl610
**        * INSTANCE NAME        : LdaCisl610
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl610 extends DbsRecord
{
    // Properties
    private DbsGroup tt;
    private DbsField tt_Record_Id;
    private DbsGroup tt_Record_IdRedef1;
    private DbsField tt_Record_Type;
    private DbsField tt_Output_Profile;
    private DbsField tt_Reprint_Ind;
    private DbsField tt_Contract_Type;
    private DbsField tt_Rqst_Id;
    private DbsField tt_Tiaa_Numb;
    private DbsField tt_Cref_Cert_Numb;
    private DbsField tt_Tiaa_Cref_Units;
    private DbsField tt_Tiaa_Ticker_Name;
    private DbsField tt_Tiaa_Reval_Type;

    public DbsGroup getTt() { return tt; }

    public DbsField getTt_Record_Id() { return tt_Record_Id; }

    public DbsGroup getTt_Record_IdRedef1() { return tt_Record_IdRedef1; }

    public DbsField getTt_Record_Type() { return tt_Record_Type; }

    public DbsField getTt_Output_Profile() { return tt_Output_Profile; }

    public DbsField getTt_Reprint_Ind() { return tt_Reprint_Ind; }

    public DbsField getTt_Contract_Type() { return tt_Contract_Type; }

    public DbsField getTt_Rqst_Id() { return tt_Rqst_Id; }

    public DbsField getTt_Tiaa_Numb() { return tt_Tiaa_Numb; }

    public DbsField getTt_Cref_Cert_Numb() { return tt_Cref_Cert_Numb; }

    public DbsField getTt_Tiaa_Cref_Units() { return tt_Tiaa_Cref_Units; }

    public DbsField getTt_Tiaa_Ticker_Name() { return tt_Tiaa_Ticker_Name; }

    public DbsField getTt_Tiaa_Reval_Type() { return tt_Tiaa_Reval_Type; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        tt = newGroupInRecord("tt", "TT");
        tt_Record_Id = tt.newFieldInGroup("tt_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        tt_Record_IdRedef1 = tt.newGroupInGroup("tt_Record_IdRedef1", "Redefines", tt_Record_Id);
        tt_Record_Type = tt_Record_IdRedef1.newFieldInGroup("tt_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        tt_Output_Profile = tt_Record_IdRedef1.newFieldInGroup("tt_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        tt_Reprint_Ind = tt_Record_IdRedef1.newFieldInGroup("tt_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        tt_Contract_Type = tt_Record_IdRedef1.newFieldInGroup("tt_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        tt_Rqst_Id = tt_Record_IdRedef1.newFieldInGroup("tt_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        tt_Tiaa_Numb = tt_Record_IdRedef1.newFieldInGroup("tt_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        tt_Cref_Cert_Numb = tt_Record_IdRedef1.newFieldInGroup("tt_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        tt_Tiaa_Cref_Units = tt.newFieldInGroup("tt_Tiaa_Cref_Units", "TIAA-CREF-UNITS", FieldType.STRING, 8);
        tt_Tiaa_Ticker_Name = tt.newFieldInGroup("tt_Tiaa_Ticker_Name", "TIAA-TICKER-NAME", FieldType.STRING, 27);
        tt_Tiaa_Reval_Type = tt.newFieldInGroup("tt_Tiaa_Reval_Type", "TIAA-REVAL-TYPE", FieldType.STRING, 15);

        this.setRecordName("LdaCisl610");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl610() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
