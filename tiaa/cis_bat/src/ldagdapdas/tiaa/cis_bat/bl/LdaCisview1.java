/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:00 PM
**        * FROM NATURAL LDA     : CISVIEW1
************************************************************
**        * FILE NAME            : LdaCisview1.java
**        * CLASS NAME           : LdaCisview1
**        * INSTANCE NAME        : LdaCisview1
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisview1 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cis_Prt_View;
    private DbsGroup cis_Prt_View_R_Reprint;
    private DbsField cis_Prt_View_R_Pin_Num;
    private DbsField cis_Prt_View_R_Sys_Id_Cde;
    private DbsField cis_Prt_View_R_Package_Cde;
    private DbsField cis_Prt_View_R_Rqst_Unit_Cde;
    private DbsField cis_Prt_View_R_Payee_Cde;
    private DbsField cis_Prt_View_R_Req_Id_Key;
    private DbsField cis_Prt_View_R_Cref_Cntrct;
    private DbsField cis_Prt_View_R_Name;
    private DbsField cis_Prt_View_R_Mit_Log_Date_Time;
    private DbsField cis_Prt_View_R_Mit_Wpid;
    private DbsField cis_Prt_View_R_Mit_Status_Code;
    private DbsField cis_Prt_View_R_Element_Max;
    private DbsField cis_Prt_View_Count_Castr_Edl_Elements;
    private DbsGroup cis_Prt_View_R_Edl_ElementsMuGroup;
    private DbsField cis_Prt_View_R_Edl_Elements;
    private DbsField cis_Prt_View_Count_Castr_Edl_Elements_2;
    private DbsGroup cis_Prt_View_R_Edl_Elements_2MuGroup;
    private DbsField cis_Prt_View_R_Edl_Elements_2;
    private DbsField cis_Prt_View_R_Date;
    private DbsField cis_Prt_View_R_Status_Ind;
    private DbsField cis_Prt_View_R_Tiaa_Cntrct;

    public DataAccessProgramView getVw_cis_Prt_View() { return vw_cis_Prt_View; }

    public DbsGroup getCis_Prt_View_R_Reprint() { return cis_Prt_View_R_Reprint; }

    public DbsField getCis_Prt_View_R_Pin_Num() { return cis_Prt_View_R_Pin_Num; }

    public DbsField getCis_Prt_View_R_Sys_Id_Cde() { return cis_Prt_View_R_Sys_Id_Cde; }

    public DbsField getCis_Prt_View_R_Package_Cde() { return cis_Prt_View_R_Package_Cde; }

    public DbsField getCis_Prt_View_R_Rqst_Unit_Cde() { return cis_Prt_View_R_Rqst_Unit_Cde; }

    public DbsField getCis_Prt_View_R_Payee_Cde() { return cis_Prt_View_R_Payee_Cde; }

    public DbsField getCis_Prt_View_R_Req_Id_Key() { return cis_Prt_View_R_Req_Id_Key; }

    public DbsField getCis_Prt_View_R_Cref_Cntrct() { return cis_Prt_View_R_Cref_Cntrct; }

    public DbsField getCis_Prt_View_R_Name() { return cis_Prt_View_R_Name; }

    public DbsField getCis_Prt_View_R_Mit_Log_Date_Time() { return cis_Prt_View_R_Mit_Log_Date_Time; }

    public DbsField getCis_Prt_View_R_Mit_Wpid() { return cis_Prt_View_R_Mit_Wpid; }

    public DbsField getCis_Prt_View_R_Mit_Status_Code() { return cis_Prt_View_R_Mit_Status_Code; }

    public DbsField getCis_Prt_View_R_Element_Max() { return cis_Prt_View_R_Element_Max; }

    public DbsField getCis_Prt_View_Count_Castr_Edl_Elements() { return cis_Prt_View_Count_Castr_Edl_Elements; }

    public DbsGroup getCis_Prt_View_R_Edl_ElementsMuGroup() { return cis_Prt_View_R_Edl_ElementsMuGroup; }

    public DbsField getCis_Prt_View_R_Edl_Elements() { return cis_Prt_View_R_Edl_Elements; }

    public DbsField getCis_Prt_View_Count_Castr_Edl_Elements_2() { return cis_Prt_View_Count_Castr_Edl_Elements_2; }

    public DbsGroup getCis_Prt_View_R_Edl_Elements_2MuGroup() { return cis_Prt_View_R_Edl_Elements_2MuGroup; }

    public DbsField getCis_Prt_View_R_Edl_Elements_2() { return cis_Prt_View_R_Edl_Elements_2; }

    public DbsField getCis_Prt_View_R_Date() { return cis_Prt_View_R_Date; }

    public DbsField getCis_Prt_View_R_Status_Ind() { return cis_Prt_View_R_Status_Ind; }

    public DbsField getCis_Prt_View_R_Tiaa_Cntrct() { return cis_Prt_View_R_Tiaa_Cntrct; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cis_Prt_View = new DataAccessProgramView(new NameInfo("vw_cis_Prt_View", "CIS-PRT-VIEW"), "CIS_REPRT_FILE_12", "CIS_REPRT_FILE");
        cis_Prt_View_R_Reprint = vw_cis_Prt_View.getRecord().newGroupInGroup("cis_Prt_View_R_Reprint", "R-REPRINT");
        cis_Prt_View_R_Pin_Num = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Pin_Num", "R-PIN-NUM", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "R_PIN_NUM");
        cis_Prt_View_R_Sys_Id_Cde = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Sys_Id_Cde", "R-SYS-ID-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "R_SYS_ID_CDE");
        cis_Prt_View_R_Package_Cde = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Package_Cde", "R-PACKAGE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "R_PACKAGE_CDE");
        cis_Prt_View_R_Rqst_Unit_Cde = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Rqst_Unit_Cde", "R-RQST-UNIT-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "R_RQST_UNIT_CDE");
        cis_Prt_View_R_Payee_Cde = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Payee_Cde", "R-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "R_PAYEE_CDE");
        cis_Prt_View_R_Req_Id_Key = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Req_Id_Key", "R-REQ-ID-KEY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "R_REQ_ID_KEY");
        cis_Prt_View_R_Cref_Cntrct = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Cref_Cntrct", "R-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "R_CREF_CNTRCT");
        cis_Prt_View_R_Name = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Name", "R-NAME", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "R_NAME");
        cis_Prt_View_R_Mit_Log_Date_Time = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Mit_Log_Date_Time", "R-MIT-LOG-DATE-TIME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "R_MIT_LOG_DATE_TIME");
        cis_Prt_View_R_Mit_Wpid = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Mit_Wpid", "R-MIT-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "R_MIT_WPID");
        cis_Prt_View_R_Mit_Status_Code = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Mit_Status_Code", "R-MIT-STATUS-CODE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "R_MIT_STATUS_CODE");
        cis_Prt_View_R_Element_Max = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Element_Max", "R-ELEMENT-MAX", FieldType.PACKED_DECIMAL, 3, 
            RepeatingFieldStrategy.None, "R_ELEMENT_MAX");
        cis_Prt_View_Count_Castr_Edl_Elements = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_Count_Castr_Edl_Elements", "C*R-EDL-ELEMENTS", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_REPRT_FILE_R_EDL_ELEMENTS");
        cis_Prt_View_R_Edl_ElementsMuGroup = cis_Prt_View_R_Reprint.newGroupInGroup("cis_Prt_View_R_Edl_ElementsMuGroup", "R_EDL_ELEMENTSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CIS_REPRT_FILE_R_EDL_ELEMENTS");
        cis_Prt_View_R_Edl_Elements = cis_Prt_View_R_Edl_ElementsMuGroup.newFieldArrayInGroup("cis_Prt_View_R_Edl_Elements", "R-EDL-ELEMENTS", FieldType.STRING, 
            6, new DbsArrayController(1,191), RepeatingFieldStrategy.SubTableFieldArray, "R_EDL_ELEMENTS");
        cis_Prt_View_Count_Castr_Edl_Elements_2 = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_Count_Castr_Edl_Elements_2", "C*R-EDL-ELEMENTS-2", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_REPRT_FILE_R_EDL_ELEMENTS_2");
        cis_Prt_View_R_Edl_Elements_2MuGroup = cis_Prt_View_R_Reprint.newGroupInGroup("cis_Prt_View_R_Edl_Elements_2MuGroup", "R_EDL_ELEMENTS_2MuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CIS_REPRT_FILE_R_EDL_ELEMENTS_2");
        cis_Prt_View_R_Edl_Elements_2 = cis_Prt_View_R_Edl_Elements_2MuGroup.newFieldArrayInGroup("cis_Prt_View_R_Edl_Elements_2", "R-EDL-ELEMENTS-2", 
            FieldType.STRING, 6, new DbsArrayController(1,34), RepeatingFieldStrategy.SubTableFieldArray, "R_EDL_ELEMENTS_2");
        cis_Prt_View_R_Date = vw_cis_Prt_View.getRecord().newFieldInGroup("cis_Prt_View_R_Date", "R-DATE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "R_DATE");
        cis_Prt_View_R_Status_Ind = vw_cis_Prt_View.getRecord().newFieldInGroup("cis_Prt_View_R_Status_Ind", "R-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "R_STATUS_IND");
        cis_Prt_View_R_Tiaa_Cntrct = vw_cis_Prt_View.getRecord().newFieldInGroup("cis_Prt_View_R_Tiaa_Cntrct", "R-TIAA-CNTRCT", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "R_TIAA_CNTRCT");

        this.setRecordName("LdaCisview1");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cis_Prt_View.reset();
    }

    // Constructor
    public LdaCisview1() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
