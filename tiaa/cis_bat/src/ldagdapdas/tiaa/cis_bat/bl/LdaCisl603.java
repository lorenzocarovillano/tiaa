/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:42 PM
**        * FROM NATURAL LDA     : CISL603
************************************************************
**        * FILE NAME            : LdaCisl603.java
**        * CLASS NAME           : LdaCisl603
**        * INSTANCE NAME        : LdaCisl603
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl603 extends DbsRecord
{
    // Properties
    private DbsGroup cb;
    private DbsField cb_Record_Id;
    private DbsGroup cb_Record_IdRedef1;
    private DbsField cb_Record_Type;
    private DbsField cb_Output_Profile;
    private DbsField cb_Reprint_Ind;
    private DbsField cb_Contract_Type;
    private DbsField cb_Rqst_Id;
    private DbsField cb_Tiaa_Numb;
    private DbsField cb_Cref_Cert_Numb;
    private DbsField cb_Cntgnt_Bene_Line;

    public DbsGroup getCb() { return cb; }

    public DbsField getCb_Record_Id() { return cb_Record_Id; }

    public DbsGroup getCb_Record_IdRedef1() { return cb_Record_IdRedef1; }

    public DbsField getCb_Record_Type() { return cb_Record_Type; }

    public DbsField getCb_Output_Profile() { return cb_Output_Profile; }

    public DbsField getCb_Reprint_Ind() { return cb_Reprint_Ind; }

    public DbsField getCb_Contract_Type() { return cb_Contract_Type; }

    public DbsField getCb_Rqst_Id() { return cb_Rqst_Id; }

    public DbsField getCb_Tiaa_Numb() { return cb_Tiaa_Numb; }

    public DbsField getCb_Cref_Cert_Numb() { return cb_Cref_Cert_Numb; }

    public DbsField getCb_Cntgnt_Bene_Line() { return cb_Cntgnt_Bene_Line; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cb = newGroupInRecord("cb", "CB");
        cb_Record_Id = cb.newFieldInGroup("cb_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        cb_Record_IdRedef1 = cb.newGroupInGroup("cb_Record_IdRedef1", "Redefines", cb_Record_Id);
        cb_Record_Type = cb_Record_IdRedef1.newFieldInGroup("cb_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        cb_Output_Profile = cb_Record_IdRedef1.newFieldInGroup("cb_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        cb_Reprint_Ind = cb_Record_IdRedef1.newFieldInGroup("cb_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        cb_Contract_Type = cb_Record_IdRedef1.newFieldInGroup("cb_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        cb_Rqst_Id = cb_Record_IdRedef1.newFieldInGroup("cb_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        cb_Tiaa_Numb = cb_Record_IdRedef1.newFieldInGroup("cb_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        cb_Cref_Cert_Numb = cb_Record_IdRedef1.newFieldInGroup("cb_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        cb_Cntgnt_Bene_Line = cb.newFieldInGroup("cb_Cntgnt_Bene_Line", "CNTGNT-BENE-LINE", FieldType.STRING, 85);

        this.setRecordName("LdaCisl603");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl603() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
