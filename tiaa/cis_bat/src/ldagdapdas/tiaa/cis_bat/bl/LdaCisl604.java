/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:42 PM
**        * FROM NATURAL LDA     : CISL604
************************************************************
**        * FILE NAME            : LdaCisl604.java
**        * CLASS NAME           : LdaCisl604
**        * INSTANCE NAME        : LdaCisl604
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl604 extends DbsRecord
{
    // Properties
    private DbsGroup cf;
    private DbsField cf_Record_Id;
    private DbsGroup cf_Record_IdRedef1;
    private DbsField cf_Record_Type;
    private DbsField cf_Output_Profile;
    private DbsField cf_Reprint_Ind;
    private DbsField cf_Contract_Type;
    private DbsField cf_Rqst_Id;
    private DbsField cf_Tiaa_Numb;
    private DbsField cf_Cref_Cert_Numb;
    private DbsField cf_Cref_Fund;
    private DbsField cf_Cref_Mnthly_Units;
    private DbsField cf_Cref_Annual_Units;

    public DbsGroup getCf() { return cf; }

    public DbsField getCf_Record_Id() { return cf_Record_Id; }

    public DbsGroup getCf_Record_IdRedef1() { return cf_Record_IdRedef1; }

    public DbsField getCf_Record_Type() { return cf_Record_Type; }

    public DbsField getCf_Output_Profile() { return cf_Output_Profile; }

    public DbsField getCf_Reprint_Ind() { return cf_Reprint_Ind; }

    public DbsField getCf_Contract_Type() { return cf_Contract_Type; }

    public DbsField getCf_Rqst_Id() { return cf_Rqst_Id; }

    public DbsField getCf_Tiaa_Numb() { return cf_Tiaa_Numb; }

    public DbsField getCf_Cref_Cert_Numb() { return cf_Cref_Cert_Numb; }

    public DbsField getCf_Cref_Fund() { return cf_Cref_Fund; }

    public DbsField getCf_Cref_Mnthly_Units() { return cf_Cref_Mnthly_Units; }

    public DbsField getCf_Cref_Annual_Units() { return cf_Cref_Annual_Units; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cf = newGroupInRecord("cf", "CF");
        cf_Record_Id = cf.newFieldInGroup("cf_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        cf_Record_IdRedef1 = cf.newGroupInGroup("cf_Record_IdRedef1", "Redefines", cf_Record_Id);
        cf_Record_Type = cf_Record_IdRedef1.newFieldInGroup("cf_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        cf_Output_Profile = cf_Record_IdRedef1.newFieldInGroup("cf_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        cf_Reprint_Ind = cf_Record_IdRedef1.newFieldInGroup("cf_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        cf_Contract_Type = cf_Record_IdRedef1.newFieldInGroup("cf_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        cf_Rqst_Id = cf_Record_IdRedef1.newFieldInGroup("cf_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        cf_Tiaa_Numb = cf_Record_IdRedef1.newFieldInGroup("cf_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        cf_Cref_Cert_Numb = cf_Record_IdRedef1.newFieldInGroup("cf_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        cf_Cref_Fund = cf.newFieldInGroup("cf_Cref_Fund", "CREF-FUND", FieldType.STRING, 30);
        cf_Cref_Mnthly_Units = cf.newFieldInGroup("cf_Cref_Mnthly_Units", "CREF-MNTHLY-UNITS", FieldType.STRING, 12);
        cf_Cref_Annual_Units = cf.newFieldInGroup("cf_Cref_Annual_Units", "CREF-ANNUAL-UNITS", FieldType.STRING, 12);

        this.setRecordName("LdaCisl604");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl604() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
