/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:41 PM
**        * FROM NATURAL LDA     : CISL501
************************************************************
**        * FILE NAME            : LdaCisl501.java
**        * CLASS NAME           : LdaCisl501
**        * INSTANCE NAME        : LdaCisl501
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl501 extends DbsRecord
{
    // Properties
    private DbsGroup cisl501;
    private DbsField cisl501_Cis_Addr_Src_Cd;
    private DbsField cisl501_Cis_Input_Business_Dt;
    private DbsField cisl501_Cis_Contract_Nbr;
    private DbsField cisl501_Cis_Dob;
    private DbsField cisl501_Cis_Ssn;
    private DbsField cisl501_Cis_Cntrct_Payee_Cd;
    private DbsField cisl501_Cis_Trans_Cd;
    private DbsField cisl501_Cis_Ph_Unque_Id_Nbr;
    private DbsField cisl501_Cis_Cntrct_Name_Free;
    private DbsField cisl501_Cis_Address_Line;
    private DbsField cisl501_Cis_Zip_Code;
    private DbsField cisl501_Cis_Addr_Usage_Cd;
    private DbsField cisl501_Cis_Cntrct_Ownership;
    private DbsField cisl501_Cis_Ph_Bank_Pymnt_Acct_Nbr;
    private DbsField cisl501_Cis_Checking_Saving_Cd;
    private DbsField cisl501_Cis_Bank_Aba_Acct_Nbr;
    private DbsField cisl501_Cis_Vacation_Start_Dt;
    private DbsField cisl501_Cis_Vacation_End_Dt;
    private DbsField cisl501_Cis_Annual_Vac_Cycle_Ind;
    private DbsField cisl501_Cis_Cntrct_Name_Prefix;
    private DbsField cisl501_Cis_Cntrct_Name_First;
    private DbsField cisl501_Cis_Cntrct_Name_Middle;
    private DbsField cisl501_Cis_Cntrct_Name_Last;
    private DbsField cisl501_Cis_Cntrct_Name_Suffix;
    private DbsField cisl501_Cis_Eft_Prenote_Ind;
    private DbsField cisl501_Cis_Eft_Status_Ind;
    private DbsField cisl501_Filler1;
    private DbsField cisl501_Cis_Stnd_Rtn_Cd;
    private DbsField cisl501_Cis_Finalist_Reason_Codes;
    private DbsField cisl501_Cis_Addr_Stnd_Cd;
    private DbsField cisl501_Cis_Stnd_Overide;
    private DbsField cisl501_Cis_Postal_Data_Fields;
    private DbsField cisl501_Cis_Addr_Geographic_Cd;
    private DbsField cisl501_Cis_Res_Address_Line;
    private DbsField cisl501_Cis_Res_Zip;
    private DbsField cisl501_Cis_Res_Country_Code;
    private DbsField cisl501_Cis_Res_Country_Code_Type;
    private DbsField cisl501_Filler2;

    public DbsGroup getCisl501() { return cisl501; }

    public DbsField getCisl501_Cis_Addr_Src_Cd() { return cisl501_Cis_Addr_Src_Cd; }

    public DbsField getCisl501_Cis_Input_Business_Dt() { return cisl501_Cis_Input_Business_Dt; }

    public DbsField getCisl501_Cis_Contract_Nbr() { return cisl501_Cis_Contract_Nbr; }

    public DbsField getCisl501_Cis_Dob() { return cisl501_Cis_Dob; }

    public DbsField getCisl501_Cis_Ssn() { return cisl501_Cis_Ssn; }

    public DbsField getCisl501_Cis_Cntrct_Payee_Cd() { return cisl501_Cis_Cntrct_Payee_Cd; }

    public DbsField getCisl501_Cis_Trans_Cd() { return cisl501_Cis_Trans_Cd; }

    public DbsField getCisl501_Cis_Ph_Unque_Id_Nbr() { return cisl501_Cis_Ph_Unque_Id_Nbr; }

    public DbsField getCisl501_Cis_Cntrct_Name_Free() { return cisl501_Cis_Cntrct_Name_Free; }

    public DbsField getCisl501_Cis_Address_Line() { return cisl501_Cis_Address_Line; }

    public DbsField getCisl501_Cis_Zip_Code() { return cisl501_Cis_Zip_Code; }

    public DbsField getCisl501_Cis_Addr_Usage_Cd() { return cisl501_Cis_Addr_Usage_Cd; }

    public DbsField getCisl501_Cis_Cntrct_Ownership() { return cisl501_Cis_Cntrct_Ownership; }

    public DbsField getCisl501_Cis_Ph_Bank_Pymnt_Acct_Nbr() { return cisl501_Cis_Ph_Bank_Pymnt_Acct_Nbr; }

    public DbsField getCisl501_Cis_Checking_Saving_Cd() { return cisl501_Cis_Checking_Saving_Cd; }

    public DbsField getCisl501_Cis_Bank_Aba_Acct_Nbr() { return cisl501_Cis_Bank_Aba_Acct_Nbr; }

    public DbsField getCisl501_Cis_Vacation_Start_Dt() { return cisl501_Cis_Vacation_Start_Dt; }

    public DbsField getCisl501_Cis_Vacation_End_Dt() { return cisl501_Cis_Vacation_End_Dt; }

    public DbsField getCisl501_Cis_Annual_Vac_Cycle_Ind() { return cisl501_Cis_Annual_Vac_Cycle_Ind; }

    public DbsField getCisl501_Cis_Cntrct_Name_Prefix() { return cisl501_Cis_Cntrct_Name_Prefix; }

    public DbsField getCisl501_Cis_Cntrct_Name_First() { return cisl501_Cis_Cntrct_Name_First; }

    public DbsField getCisl501_Cis_Cntrct_Name_Middle() { return cisl501_Cis_Cntrct_Name_Middle; }

    public DbsField getCisl501_Cis_Cntrct_Name_Last() { return cisl501_Cis_Cntrct_Name_Last; }

    public DbsField getCisl501_Cis_Cntrct_Name_Suffix() { return cisl501_Cis_Cntrct_Name_Suffix; }

    public DbsField getCisl501_Cis_Eft_Prenote_Ind() { return cisl501_Cis_Eft_Prenote_Ind; }

    public DbsField getCisl501_Cis_Eft_Status_Ind() { return cisl501_Cis_Eft_Status_Ind; }

    public DbsField getCisl501_Filler1() { return cisl501_Filler1; }

    public DbsField getCisl501_Cis_Stnd_Rtn_Cd() { return cisl501_Cis_Stnd_Rtn_Cd; }

    public DbsField getCisl501_Cis_Finalist_Reason_Codes() { return cisl501_Cis_Finalist_Reason_Codes; }

    public DbsField getCisl501_Cis_Addr_Stnd_Cd() { return cisl501_Cis_Addr_Stnd_Cd; }

    public DbsField getCisl501_Cis_Stnd_Overide() { return cisl501_Cis_Stnd_Overide; }

    public DbsField getCisl501_Cis_Postal_Data_Fields() { return cisl501_Cis_Postal_Data_Fields; }

    public DbsField getCisl501_Cis_Addr_Geographic_Cd() { return cisl501_Cis_Addr_Geographic_Cd; }

    public DbsField getCisl501_Cis_Res_Address_Line() { return cisl501_Cis_Res_Address_Line; }

    public DbsField getCisl501_Cis_Res_Zip() { return cisl501_Cis_Res_Zip; }

    public DbsField getCisl501_Cis_Res_Country_Code() { return cisl501_Cis_Res_Country_Code; }

    public DbsField getCisl501_Cis_Res_Country_Code_Type() { return cisl501_Cis_Res_Country_Code_Type; }

    public DbsField getCisl501_Filler2() { return cisl501_Filler2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cisl501 = newGroupInRecord("cisl501", "CISL501");
        cisl501_Cis_Addr_Src_Cd = cisl501.newFieldInGroup("cisl501_Cis_Addr_Src_Cd", "CIS-ADDR-SRC-CD", FieldType.STRING, 2);
        cisl501_Cis_Input_Business_Dt = cisl501.newFieldInGroup("cisl501_Cis_Input_Business_Dt", "CIS-INPUT-BUSINESS-DT", FieldType.NUMERIC, 8);
        cisl501_Cis_Contract_Nbr = cisl501.newFieldInGroup("cisl501_Cis_Contract_Nbr", "CIS-CONTRACT-NBR", FieldType.STRING, 10);
        cisl501_Cis_Dob = cisl501.newFieldInGroup("cisl501_Cis_Dob", "CIS-DOB", FieldType.NUMERIC, 6);
        cisl501_Cis_Ssn = cisl501.newFieldInGroup("cisl501_Cis_Ssn", "CIS-SSN", FieldType.NUMERIC, 9);
        cisl501_Cis_Cntrct_Payee_Cd = cisl501.newFieldInGroup("cisl501_Cis_Cntrct_Payee_Cd", "CIS-CNTRCT-PAYEE-CD", FieldType.STRING, 2);
        cisl501_Cis_Trans_Cd = cisl501.newFieldInGroup("cisl501_Cis_Trans_Cd", "CIS-TRANS-CD", FieldType.STRING, 2);
        cisl501_Cis_Ph_Unque_Id_Nbr = cisl501.newFieldInGroup("cisl501_Cis_Ph_Unque_Id_Nbr", "CIS-PH-UNQUE-ID-NBR", FieldType.STRING, 12);
        cisl501_Cis_Cntrct_Name_Free = cisl501.newFieldInGroup("cisl501_Cis_Cntrct_Name_Free", "CIS-CNTRCT-NAME-FREE", FieldType.STRING, 35);
        cisl501_Cis_Address_Line = cisl501.newFieldArrayInGroup("cisl501_Cis_Address_Line", "CIS-ADDRESS-LINE", FieldType.STRING, 35, new DbsArrayController(1,
            5));
        cisl501_Cis_Zip_Code = cisl501.newFieldInGroup("cisl501_Cis_Zip_Code", "CIS-ZIP-CODE", FieldType.STRING, 5);
        cisl501_Cis_Addr_Usage_Cd = cisl501.newFieldInGroup("cisl501_Cis_Addr_Usage_Cd", "CIS-ADDR-USAGE-CD", FieldType.STRING, 1);
        cisl501_Cis_Cntrct_Ownership = cisl501.newFieldInGroup("cisl501_Cis_Cntrct_Ownership", "CIS-CNTRCT-OWNERSHIP", FieldType.STRING, 1);
        cisl501_Cis_Ph_Bank_Pymnt_Acct_Nbr = cisl501.newFieldInGroup("cisl501_Cis_Ph_Bank_Pymnt_Acct_Nbr", "CIS-PH-BANK-PYMNT-ACCT-NBR", FieldType.STRING, 
            21);
        cisl501_Cis_Checking_Saving_Cd = cisl501.newFieldInGroup("cisl501_Cis_Checking_Saving_Cd", "CIS-CHECKING-SAVING-CD", FieldType.STRING, 1);
        cisl501_Cis_Bank_Aba_Acct_Nbr = cisl501.newFieldInGroup("cisl501_Cis_Bank_Aba_Acct_Nbr", "CIS-BANK-ABA-ACCT-NBR", FieldType.STRING, 9);
        cisl501_Cis_Vacation_Start_Dt = cisl501.newFieldInGroup("cisl501_Cis_Vacation_Start_Dt", "CIS-VACATION-START-DT", FieldType.NUMERIC, 6);
        cisl501_Cis_Vacation_End_Dt = cisl501.newFieldInGroup("cisl501_Cis_Vacation_End_Dt", "CIS-VACATION-END-DT", FieldType.NUMERIC, 6);
        cisl501_Cis_Annual_Vac_Cycle_Ind = cisl501.newFieldInGroup("cisl501_Cis_Annual_Vac_Cycle_Ind", "CIS-ANNUAL-VAC-CYCLE-IND", FieldType.STRING, 1);
        cisl501_Cis_Cntrct_Name_Prefix = cisl501.newFieldInGroup("cisl501_Cis_Cntrct_Name_Prefix", "CIS-CNTRCT-NAME-PREFIX", FieldType.STRING, 8);
        cisl501_Cis_Cntrct_Name_First = cisl501.newFieldInGroup("cisl501_Cis_Cntrct_Name_First", "CIS-CNTRCT-NAME-FIRST", FieldType.STRING, 30);
        cisl501_Cis_Cntrct_Name_Middle = cisl501.newFieldInGroup("cisl501_Cis_Cntrct_Name_Middle", "CIS-CNTRCT-NAME-MIDDLE", FieldType.STRING, 30);
        cisl501_Cis_Cntrct_Name_Last = cisl501.newFieldInGroup("cisl501_Cis_Cntrct_Name_Last", "CIS-CNTRCT-NAME-LAST", FieldType.STRING, 30);
        cisl501_Cis_Cntrct_Name_Suffix = cisl501.newFieldInGroup("cisl501_Cis_Cntrct_Name_Suffix", "CIS-CNTRCT-NAME-SUFFIX", FieldType.STRING, 8);
        cisl501_Cis_Eft_Prenote_Ind = cisl501.newFieldInGroup("cisl501_Cis_Eft_Prenote_Ind", "CIS-EFT-PRENOTE-IND", FieldType.STRING, 1);
        cisl501_Cis_Eft_Status_Ind = cisl501.newFieldInGroup("cisl501_Cis_Eft_Status_Ind", "CIS-EFT-STATUS-IND", FieldType.STRING, 1);
        cisl501_Filler1 = cisl501.newFieldInGroup("cisl501_Filler1", "FILLER1", FieldType.STRING, 18);
        cisl501_Cis_Stnd_Rtn_Cd = cisl501.newFieldInGroup("cisl501_Cis_Stnd_Rtn_Cd", "CIS-STND-RTN-CD", FieldType.STRING, 2);
        cisl501_Cis_Finalist_Reason_Codes = cisl501.newFieldInGroup("cisl501_Cis_Finalist_Reason_Codes", "CIS-FINALIST-REASON-CODES", FieldType.STRING, 
            10);
        cisl501_Cis_Addr_Stnd_Cd = cisl501.newFieldInGroup("cisl501_Cis_Addr_Stnd_Cd", "CIS-ADDR-STND-CD", FieldType.STRING, 1);
        cisl501_Cis_Stnd_Overide = cisl501.newFieldInGroup("cisl501_Cis_Stnd_Overide", "CIS-STND-OVERIDE", FieldType.STRING, 1);
        cisl501_Cis_Postal_Data_Fields = cisl501.newFieldInGroup("cisl501_Cis_Postal_Data_Fields", "CIS-POSTAL-DATA-FIELDS", FieldType.STRING, 44);
        cisl501_Cis_Addr_Geographic_Cd = cisl501.newFieldInGroup("cisl501_Cis_Addr_Geographic_Cd", "CIS-ADDR-GEOGRAPHIC-CD", FieldType.STRING, 2);
        cisl501_Cis_Res_Address_Line = cisl501.newFieldArrayInGroup("cisl501_Cis_Res_Address_Line", "CIS-RES-ADDRESS-LINE", FieldType.STRING, 35, new 
            DbsArrayController(1,5));
        cisl501_Cis_Res_Zip = cisl501.newFieldInGroup("cisl501_Cis_Res_Zip", "CIS-RES-ZIP", FieldType.STRING, 9);
        cisl501_Cis_Res_Country_Code = cisl501.newFieldInGroup("cisl501_Cis_Res_Country_Code", "CIS-RES-COUNTRY-CODE", FieldType.STRING, 3);
        cisl501_Cis_Res_Country_Code_Type = cisl501.newFieldInGroup("cisl501_Cis_Res_Country_Code_Type", "CIS-RES-COUNTRY-CODE-TYPE", FieldType.STRING, 
            1);
        cisl501_Filler2 = cisl501.newFieldInGroup("cisl501_Filler2", "FILLER2", FieldType.STRING, 26);

        this.setRecordName("LdaCisl501");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl501() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
