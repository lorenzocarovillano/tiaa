/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:42 PM
**        * FROM NATURAL LDA     : CISL601M
************************************************************
**        * FILE NAME            : LdaCisl601m.java
**        * CLASS NAME           : LdaCisl601m
**        * INSTANCE NAME        : LdaCisl601m
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl601m extends DbsRecord
{
    // Properties
    private DbsGroup md;
    private DbsField md_Record_Id;
    private DbsGroup md_Record_IdRedef1;
    private DbsField md_Record_Type;
    private DbsField md_Output_Profile;
    private DbsField md_Reprint_Ind;
    private DbsField md_Contract_Type;
    private DbsField md_Rqst_Id;
    private DbsField md_Tiaa_Numb;
    private DbsField md_Cref_Cert_Numb;
    private DbsField md_Tiaa_Issue_Date;
    private DbsField md_Cref_Issue_Date;
    private DbsField md_First_Payt_Date;
    private DbsField md_Payt_Frequency;
    private DbsField md_Annt_Calc_Method;
    private DbsField md_Annt_Calc_Met_2;
    private DbsField md_Annt_Dob;
    private DbsField md_Annt_Contract_Name;
    private DbsField md_Annt_Name_Last;
    private DbsField md_Annt_Name_First;
    private DbsField md_Annt_Name_Middle;
    private DbsField md_Annt_Name_Prefix;
    private DbsField md_Annt_Name_Suffix;
    private DbsField md_Annt_Citizenship;
    private DbsField md_Annt_Soc_Numb;
    private DbsField md_Calc_Participant;
    private DbsField md_Clcltn_Beneficiary;
    private DbsField md_Clcltn_Bene_Surv;
    private DbsField md_Clcltn_Bene_Name;
    private DbsField md_Clcltn_Bene_Method;
    private DbsField md_Clcltn_Bene_Method_2;
    private DbsField md_Clcltn_Bene_Dob;
    private DbsField md_Orgnl_Prtcpnt_Name;
    private DbsField md_Orgnl_Prtcpnt_Dob;
    private DbsField md_Orgnl_Prtcpnt_Dod;
    private DbsField md_Orgnl_Prtcpnt_Ssn;
    private DbsField md_Address_Name;
    private DbsField md_Welcome_Name;
    private DbsField md_Directory_Name;
    private DbsField md_Address_Line;
    private DbsField md_Zip;
    private DbsField md_Post_Zip;
    private DbsField md_Pin_Numb;
    private DbsField md_Guar_Intr_Rate;
    private DbsField md_Traditional_Amt;
    private DbsField md_Real_Estate_Units;
    private DbsField md_Tiaa_Initial_Payt_Amt;
    private DbsField md_Tiaa_Excluded_Amt;
    private DbsField md_Cref_Initial_Payt_Amt;
    private DbsField md_Cref_Excluded_Amt;
    private DbsField md_Contigent_Chrg;
    private DbsField md_Surrender_Chrg;
    private DbsField md_Da_Tiaa_Total_Amt;
    private DbsField md_Real_Estate_Amt;
    private DbsField md_Seprate_Accnt_Chrg;
    private DbsField md_Da_Cref_Total_Amt;
    private DbsField md_Tiaa_Ed_Numb;
    private DbsField md_Cref_Ed_Numb;
    private DbsField md_Tiaa_Form_Numb;
    private DbsField md_Cref_Form_Numb;
    private DbsField md_Tiaa_Prod_Cdes;
    private DbsField md_Cref_Prod_Cdes;
    private DbsField md_Tiaa_Headers;
    private DbsField md_Cref_Headers;
    private DbsField md_State_Name;
    private DbsField md_Inst_Name;
    private DbsField md_Res_Issue_State;
    private DbsField md_Lob;
    private DbsField md_Lob_Type;
    private DbsField md_Region;
    private DbsField md_Branch;
    private DbsField md_Need;
    private DbsField md_Ppg_Code;
    private DbsField md_Annual_Req_Dist_Amt;
    private DbsField md_Frst_Req_Pymt_Year;
    private DbsField md_Frst_Req_Pymt_Amt;
    private DbsField md_Cntrct_Apprvl;
    private DbsField md_Cntrct_Option;
    private DbsField md_Cntrct_Cash_Status;
    private DbsField md_Cntrct_Retr_Surv;
    private DbsField md_Guar_Period_Flag;
    private DbsField md_Multiple_Intr_Rate;
    private DbsField md_Tiaa_Cntr_Settled;
    private DbsField md_Rea_Cntr_Settled;
    private DbsField md_Cref_Cntr_Settled;
    private DbsField md_Cref_Accnt_Number;
    private DbsField md_Internal_Trnsf;
    private DbsField md_Revaluation_Cde;
    private DbsField md_Orig_Issue_State;
    private DbsField md_Ownership_Code;
    private DbsField md_Rtb_Request;
    private DbsField md_Issue_Eq_Frst_Pymt;
    private DbsField md_Estate_Cntgnt;
    private DbsField md_Freeform_Ben;
    private DbsField md_Prmry_Ben_Stnd_Txt;
    private DbsField md_Prmry_Ben_Lump_Sum;
    private DbsField md_Prmry_Ben_Auto;
    private DbsField md_Prmry_Ben_Child_Prvsn;
    private DbsField md_Prmry_Ben_Numb;
    private DbsField md_Cntgnt_Ben_Stnd_Txt;
    private DbsField md_Cntgnt_Ben_Lump_Sum;
    private DbsField md_Cntgnt_Ben_Auto;
    private DbsField md_Cntgnt_Ben_Child_Prvsn;
    private DbsField md_Cntgnt_Ben_Numb;
    private DbsField md_Gsra_Ira_Surr_Chrg;
    private DbsField md_Da_Death_Sur_Right;
    private DbsField md_Gra_Surr_Right;
    private DbsField md_Personal_Annuity;
    private DbsField md_Mit_Orgnl_Unit_Cde;
    private DbsField md_Graded_Ind;
    private DbsField md_Frst_Pymt_After_Iss;
    private DbsField md_Tiaa_Cntrct_Type;
    private DbsField md_Rea_Cntrct_Type;
    private DbsField md_Cref_Cntrct_Type;
    private DbsField md_Comut_Intr_Num;
    private DbsField md_Trnsf_Cntr_Settled;
    private DbsField md_Spec_Cntrct_Type;
    private DbsField md_Orig_Issue_Date;
    private DbsField md_Access_Ind;
    private DbsField md_Roth_Ind;
    private DbsField md_Payee_Ind;
    private DbsField md_Guid;
    private DbsField md_Invest_Num_Tot;
    private DbsField md_Multi_Plan_Ind;
    private DbsField md_Plan_Num;
    private DbsField md_Multi_Plan_Tbl;
    private DbsGroup md_Multi_Plan_TblRedef2;
    private DbsGroup md_Multi_Plan_Info;
    private DbsField md_Multi_Plan_No;
    private DbsField md_Multi_Sub_Plan;
    private DbsGroup mdRedef3;
    private DbsField md_Contract_1;
    private DbsField md_Contract_2;
    private DbsField md_Contract_3;
    private DbsField md_Contract_4;
    private DbsField md_Contract_5;
    private DbsField md_Contract_6;
    private DbsField md_Contract_7;
    private DbsField md_Contract_8;
    private DbsField md_Contract_9;

    public DbsGroup getMd() { return md; }

    public DbsField getMd_Record_Id() { return md_Record_Id; }

    public DbsGroup getMd_Record_IdRedef1() { return md_Record_IdRedef1; }

    public DbsField getMd_Record_Type() { return md_Record_Type; }

    public DbsField getMd_Output_Profile() { return md_Output_Profile; }

    public DbsField getMd_Reprint_Ind() { return md_Reprint_Ind; }

    public DbsField getMd_Contract_Type() { return md_Contract_Type; }

    public DbsField getMd_Rqst_Id() { return md_Rqst_Id; }

    public DbsField getMd_Tiaa_Numb() { return md_Tiaa_Numb; }

    public DbsField getMd_Cref_Cert_Numb() { return md_Cref_Cert_Numb; }

    public DbsField getMd_Tiaa_Issue_Date() { return md_Tiaa_Issue_Date; }

    public DbsField getMd_Cref_Issue_Date() { return md_Cref_Issue_Date; }

    public DbsField getMd_First_Payt_Date() { return md_First_Payt_Date; }

    public DbsField getMd_Payt_Frequency() { return md_Payt_Frequency; }

    public DbsField getMd_Annt_Calc_Method() { return md_Annt_Calc_Method; }

    public DbsField getMd_Annt_Calc_Met_2() { return md_Annt_Calc_Met_2; }

    public DbsField getMd_Annt_Dob() { return md_Annt_Dob; }

    public DbsField getMd_Annt_Contract_Name() { return md_Annt_Contract_Name; }

    public DbsField getMd_Annt_Name_Last() { return md_Annt_Name_Last; }

    public DbsField getMd_Annt_Name_First() { return md_Annt_Name_First; }

    public DbsField getMd_Annt_Name_Middle() { return md_Annt_Name_Middle; }

    public DbsField getMd_Annt_Name_Prefix() { return md_Annt_Name_Prefix; }

    public DbsField getMd_Annt_Name_Suffix() { return md_Annt_Name_Suffix; }

    public DbsField getMd_Annt_Citizenship() { return md_Annt_Citizenship; }

    public DbsField getMd_Annt_Soc_Numb() { return md_Annt_Soc_Numb; }

    public DbsField getMd_Calc_Participant() { return md_Calc_Participant; }

    public DbsField getMd_Clcltn_Beneficiary() { return md_Clcltn_Beneficiary; }

    public DbsField getMd_Clcltn_Bene_Surv() { return md_Clcltn_Bene_Surv; }

    public DbsField getMd_Clcltn_Bene_Name() { return md_Clcltn_Bene_Name; }

    public DbsField getMd_Clcltn_Bene_Method() { return md_Clcltn_Bene_Method; }

    public DbsField getMd_Clcltn_Bene_Method_2() { return md_Clcltn_Bene_Method_2; }

    public DbsField getMd_Clcltn_Bene_Dob() { return md_Clcltn_Bene_Dob; }

    public DbsField getMd_Orgnl_Prtcpnt_Name() { return md_Orgnl_Prtcpnt_Name; }

    public DbsField getMd_Orgnl_Prtcpnt_Dob() { return md_Orgnl_Prtcpnt_Dob; }

    public DbsField getMd_Orgnl_Prtcpnt_Dod() { return md_Orgnl_Prtcpnt_Dod; }

    public DbsField getMd_Orgnl_Prtcpnt_Ssn() { return md_Orgnl_Prtcpnt_Ssn; }

    public DbsField getMd_Address_Name() { return md_Address_Name; }

    public DbsField getMd_Welcome_Name() { return md_Welcome_Name; }

    public DbsField getMd_Directory_Name() { return md_Directory_Name; }

    public DbsField getMd_Address_Line() { return md_Address_Line; }

    public DbsField getMd_Zip() { return md_Zip; }

    public DbsField getMd_Post_Zip() { return md_Post_Zip; }

    public DbsField getMd_Pin_Numb() { return md_Pin_Numb; }

    public DbsField getMd_Guar_Intr_Rate() { return md_Guar_Intr_Rate; }

    public DbsField getMd_Traditional_Amt() { return md_Traditional_Amt; }

    public DbsField getMd_Real_Estate_Units() { return md_Real_Estate_Units; }

    public DbsField getMd_Tiaa_Initial_Payt_Amt() { return md_Tiaa_Initial_Payt_Amt; }

    public DbsField getMd_Tiaa_Excluded_Amt() { return md_Tiaa_Excluded_Amt; }

    public DbsField getMd_Cref_Initial_Payt_Amt() { return md_Cref_Initial_Payt_Amt; }

    public DbsField getMd_Cref_Excluded_Amt() { return md_Cref_Excluded_Amt; }

    public DbsField getMd_Contigent_Chrg() { return md_Contigent_Chrg; }

    public DbsField getMd_Surrender_Chrg() { return md_Surrender_Chrg; }

    public DbsField getMd_Da_Tiaa_Total_Amt() { return md_Da_Tiaa_Total_Amt; }

    public DbsField getMd_Real_Estate_Amt() { return md_Real_Estate_Amt; }

    public DbsField getMd_Seprate_Accnt_Chrg() { return md_Seprate_Accnt_Chrg; }

    public DbsField getMd_Da_Cref_Total_Amt() { return md_Da_Cref_Total_Amt; }

    public DbsField getMd_Tiaa_Ed_Numb() { return md_Tiaa_Ed_Numb; }

    public DbsField getMd_Cref_Ed_Numb() { return md_Cref_Ed_Numb; }

    public DbsField getMd_Tiaa_Form_Numb() { return md_Tiaa_Form_Numb; }

    public DbsField getMd_Cref_Form_Numb() { return md_Cref_Form_Numb; }

    public DbsField getMd_Tiaa_Prod_Cdes() { return md_Tiaa_Prod_Cdes; }

    public DbsField getMd_Cref_Prod_Cdes() { return md_Cref_Prod_Cdes; }

    public DbsField getMd_Tiaa_Headers() { return md_Tiaa_Headers; }

    public DbsField getMd_Cref_Headers() { return md_Cref_Headers; }

    public DbsField getMd_State_Name() { return md_State_Name; }

    public DbsField getMd_Inst_Name() { return md_Inst_Name; }

    public DbsField getMd_Res_Issue_State() { return md_Res_Issue_State; }

    public DbsField getMd_Lob() { return md_Lob; }

    public DbsField getMd_Lob_Type() { return md_Lob_Type; }

    public DbsField getMd_Region() { return md_Region; }

    public DbsField getMd_Branch() { return md_Branch; }

    public DbsField getMd_Need() { return md_Need; }

    public DbsField getMd_Ppg_Code() { return md_Ppg_Code; }

    public DbsField getMd_Annual_Req_Dist_Amt() { return md_Annual_Req_Dist_Amt; }

    public DbsField getMd_Frst_Req_Pymt_Year() { return md_Frst_Req_Pymt_Year; }

    public DbsField getMd_Frst_Req_Pymt_Amt() { return md_Frst_Req_Pymt_Amt; }

    public DbsField getMd_Cntrct_Apprvl() { return md_Cntrct_Apprvl; }

    public DbsField getMd_Cntrct_Option() { return md_Cntrct_Option; }

    public DbsField getMd_Cntrct_Cash_Status() { return md_Cntrct_Cash_Status; }

    public DbsField getMd_Cntrct_Retr_Surv() { return md_Cntrct_Retr_Surv; }

    public DbsField getMd_Guar_Period_Flag() { return md_Guar_Period_Flag; }

    public DbsField getMd_Multiple_Intr_Rate() { return md_Multiple_Intr_Rate; }

    public DbsField getMd_Tiaa_Cntr_Settled() { return md_Tiaa_Cntr_Settled; }

    public DbsField getMd_Rea_Cntr_Settled() { return md_Rea_Cntr_Settled; }

    public DbsField getMd_Cref_Cntr_Settled() { return md_Cref_Cntr_Settled; }

    public DbsField getMd_Cref_Accnt_Number() { return md_Cref_Accnt_Number; }

    public DbsField getMd_Internal_Trnsf() { return md_Internal_Trnsf; }

    public DbsField getMd_Revaluation_Cde() { return md_Revaluation_Cde; }

    public DbsField getMd_Orig_Issue_State() { return md_Orig_Issue_State; }

    public DbsField getMd_Ownership_Code() { return md_Ownership_Code; }

    public DbsField getMd_Rtb_Request() { return md_Rtb_Request; }

    public DbsField getMd_Issue_Eq_Frst_Pymt() { return md_Issue_Eq_Frst_Pymt; }

    public DbsField getMd_Estate_Cntgnt() { return md_Estate_Cntgnt; }

    public DbsField getMd_Freeform_Ben() { return md_Freeform_Ben; }

    public DbsField getMd_Prmry_Ben_Stnd_Txt() { return md_Prmry_Ben_Stnd_Txt; }

    public DbsField getMd_Prmry_Ben_Lump_Sum() { return md_Prmry_Ben_Lump_Sum; }

    public DbsField getMd_Prmry_Ben_Auto() { return md_Prmry_Ben_Auto; }

    public DbsField getMd_Prmry_Ben_Child_Prvsn() { return md_Prmry_Ben_Child_Prvsn; }

    public DbsField getMd_Prmry_Ben_Numb() { return md_Prmry_Ben_Numb; }

    public DbsField getMd_Cntgnt_Ben_Stnd_Txt() { return md_Cntgnt_Ben_Stnd_Txt; }

    public DbsField getMd_Cntgnt_Ben_Lump_Sum() { return md_Cntgnt_Ben_Lump_Sum; }

    public DbsField getMd_Cntgnt_Ben_Auto() { return md_Cntgnt_Ben_Auto; }

    public DbsField getMd_Cntgnt_Ben_Child_Prvsn() { return md_Cntgnt_Ben_Child_Prvsn; }

    public DbsField getMd_Cntgnt_Ben_Numb() { return md_Cntgnt_Ben_Numb; }

    public DbsField getMd_Gsra_Ira_Surr_Chrg() { return md_Gsra_Ira_Surr_Chrg; }

    public DbsField getMd_Da_Death_Sur_Right() { return md_Da_Death_Sur_Right; }

    public DbsField getMd_Gra_Surr_Right() { return md_Gra_Surr_Right; }

    public DbsField getMd_Personal_Annuity() { return md_Personal_Annuity; }

    public DbsField getMd_Mit_Orgnl_Unit_Cde() { return md_Mit_Orgnl_Unit_Cde; }

    public DbsField getMd_Graded_Ind() { return md_Graded_Ind; }

    public DbsField getMd_Frst_Pymt_After_Iss() { return md_Frst_Pymt_After_Iss; }

    public DbsField getMd_Tiaa_Cntrct_Type() { return md_Tiaa_Cntrct_Type; }

    public DbsField getMd_Rea_Cntrct_Type() { return md_Rea_Cntrct_Type; }

    public DbsField getMd_Cref_Cntrct_Type() { return md_Cref_Cntrct_Type; }

    public DbsField getMd_Comut_Intr_Num() { return md_Comut_Intr_Num; }

    public DbsField getMd_Trnsf_Cntr_Settled() { return md_Trnsf_Cntr_Settled; }

    public DbsField getMd_Spec_Cntrct_Type() { return md_Spec_Cntrct_Type; }

    public DbsField getMd_Orig_Issue_Date() { return md_Orig_Issue_Date; }

    public DbsField getMd_Access_Ind() { return md_Access_Ind; }

    public DbsField getMd_Roth_Ind() { return md_Roth_Ind; }

    public DbsField getMd_Payee_Ind() { return md_Payee_Ind; }

    public DbsField getMd_Guid() { return md_Guid; }

    public DbsField getMd_Invest_Num_Tot() { return md_Invest_Num_Tot; }

    public DbsField getMd_Multi_Plan_Ind() { return md_Multi_Plan_Ind; }

    public DbsField getMd_Plan_Num() { return md_Plan_Num; }

    public DbsField getMd_Multi_Plan_Tbl() { return md_Multi_Plan_Tbl; }

    public DbsGroup getMd_Multi_Plan_TblRedef2() { return md_Multi_Plan_TblRedef2; }

    public DbsGroup getMd_Multi_Plan_Info() { return md_Multi_Plan_Info; }

    public DbsField getMd_Multi_Plan_No() { return md_Multi_Plan_No; }

    public DbsField getMd_Multi_Sub_Plan() { return md_Multi_Sub_Plan; }

    public DbsGroup getMdRedef3() { return mdRedef3; }

    public DbsField getMd_Contract_1() { return md_Contract_1; }

    public DbsField getMd_Contract_2() { return md_Contract_2; }

    public DbsField getMd_Contract_3() { return md_Contract_3; }

    public DbsField getMd_Contract_4() { return md_Contract_4; }

    public DbsField getMd_Contract_5() { return md_Contract_5; }

    public DbsField getMd_Contract_6() { return md_Contract_6; }

    public DbsField getMd_Contract_7() { return md_Contract_7; }

    public DbsField getMd_Contract_8() { return md_Contract_8; }

    public DbsField getMd_Contract_9() { return md_Contract_9; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        md = newGroupInRecord("md", "MD");
        md_Record_Id = md.newFieldInGroup("md_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        md_Record_IdRedef1 = md.newGroupInGroup("md_Record_IdRedef1", "Redefines", md_Record_Id);
        md_Record_Type = md_Record_IdRedef1.newFieldInGroup("md_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        md_Output_Profile = md_Record_IdRedef1.newFieldInGroup("md_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        md_Reprint_Ind = md_Record_IdRedef1.newFieldInGroup("md_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        md_Contract_Type = md_Record_IdRedef1.newFieldInGroup("md_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        md_Rqst_Id = md_Record_IdRedef1.newFieldInGroup("md_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        md_Tiaa_Numb = md_Record_IdRedef1.newFieldInGroup("md_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        md_Cref_Cert_Numb = md_Record_IdRedef1.newFieldInGroup("md_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        md_Tiaa_Issue_Date = md.newFieldInGroup("md_Tiaa_Issue_Date", "TIAA-ISSUE-DATE", FieldType.STRING, 8);
        md_Cref_Issue_Date = md.newFieldInGroup("md_Cref_Issue_Date", "CREF-ISSUE-DATE", FieldType.STRING, 8);
        md_First_Payt_Date = md.newFieldInGroup("md_First_Payt_Date", "FIRST-PAYT-DATE", FieldType.STRING, 8);
        md_Payt_Frequency = md.newFieldInGroup("md_Payt_Frequency", "PAYT-FREQUENCY", FieldType.STRING, 13);
        md_Annt_Calc_Method = md.newFieldInGroup("md_Annt_Calc_Method", "ANNT-CALC-METHOD", FieldType.STRING, 1);
        md_Annt_Calc_Met_2 = md.newFieldInGroup("md_Annt_Calc_Met_2", "ANNT-CALC-MET-2", FieldType.STRING, 15);
        md_Annt_Dob = md.newFieldInGroup("md_Annt_Dob", "ANNT-DOB", FieldType.STRING, 8);
        md_Annt_Contract_Name = md.newFieldInGroup("md_Annt_Contract_Name", "ANNT-CONTRACT-NAME", FieldType.STRING, 72);
        md_Annt_Name_Last = md.newFieldInGroup("md_Annt_Name_Last", "ANNT-NAME-LAST", FieldType.STRING, 30);
        md_Annt_Name_First = md.newFieldInGroup("md_Annt_Name_First", "ANNT-NAME-FIRST", FieldType.STRING, 30);
        md_Annt_Name_Middle = md.newFieldInGroup("md_Annt_Name_Middle", "ANNT-NAME-MIDDLE", FieldType.STRING, 30);
        md_Annt_Name_Prefix = md.newFieldInGroup("md_Annt_Name_Prefix", "ANNT-NAME-PREFIX", FieldType.STRING, 8);
        md_Annt_Name_Suffix = md.newFieldInGroup("md_Annt_Name_Suffix", "ANNT-NAME-SUFFIX", FieldType.STRING, 8);
        md_Annt_Citizenship = md.newFieldInGroup("md_Annt_Citizenship", "ANNT-CITIZENSHIP", FieldType.STRING, 25);
        md_Annt_Soc_Numb = md.newFieldInGroup("md_Annt_Soc_Numb", "ANNT-SOC-NUMB", FieldType.STRING, 11);
        md_Calc_Participant = md.newFieldInGroup("md_Calc_Participant", "CALC-PARTICIPANT", FieldType.STRING, 13);
        md_Clcltn_Beneficiary = md.newFieldInGroup("md_Clcltn_Beneficiary", "CLCLTN-BENEFICIARY", FieldType.STRING, 25);
        md_Clcltn_Bene_Surv = md.newFieldInGroup("md_Clcltn_Bene_Surv", "CLCLTN-BENE-SURV", FieldType.STRING, 35);
        md_Clcltn_Bene_Name = md.newFieldInGroup("md_Clcltn_Bene_Name", "CLCLTN-BENE-NAME", FieldType.STRING, 35);
        md_Clcltn_Bene_Method = md.newFieldInGroup("md_Clcltn_Bene_Method", "CLCLTN-BENE-METHOD", FieldType.STRING, 1);
        md_Clcltn_Bene_Method_2 = md.newFieldInGroup("md_Clcltn_Bene_Method_2", "CLCLTN-BENE-METHOD-2", FieldType.STRING, 15);
        md_Clcltn_Bene_Dob = md.newFieldInGroup("md_Clcltn_Bene_Dob", "CLCLTN-BENE-DOB", FieldType.STRING, 8);
        md_Orgnl_Prtcpnt_Name = md.newFieldInGroup("md_Orgnl_Prtcpnt_Name", "ORGNL-PRTCPNT-NAME", FieldType.STRING, 72);
        md_Orgnl_Prtcpnt_Dob = md.newFieldInGroup("md_Orgnl_Prtcpnt_Dob", "ORGNL-PRTCPNT-DOB", FieldType.STRING, 8);
        md_Orgnl_Prtcpnt_Dod = md.newFieldInGroup("md_Orgnl_Prtcpnt_Dod", "ORGNL-PRTCPNT-DOD", FieldType.STRING, 8);
        md_Orgnl_Prtcpnt_Ssn = md.newFieldInGroup("md_Orgnl_Prtcpnt_Ssn", "ORGNL-PRTCPNT-SSN", FieldType.STRING, 11);
        md_Address_Name = md.newFieldInGroup("md_Address_Name", "ADDRESS-NAME", FieldType.STRING, 81);
        md_Welcome_Name = md.newFieldInGroup("md_Welcome_Name", "WELCOME-NAME", FieldType.STRING, 39);
        md_Directory_Name = md.newFieldInGroup("md_Directory_Name", "DIRECTORY-NAME", FieldType.STRING, 63);
        md_Address_Line = md.newFieldArrayInGroup("md_Address_Line", "ADDRESS-LINE", FieldType.STRING, 35, new DbsArrayController(1,5));
        md_Zip = md.newFieldInGroup("md_Zip", "ZIP", FieldType.STRING, 9);
        md_Post_Zip = md.newFieldInGroup("md_Post_Zip", "POST-ZIP", FieldType.STRING, 12);
        md_Pin_Numb = md.newFieldInGroup("md_Pin_Numb", "PIN-NUMB", FieldType.STRING, 12);
        md_Guar_Intr_Rate = md.newFieldInGroup("md_Guar_Intr_Rate", "GUAR-INTR-RATE", FieldType.STRING, 6);
        md_Traditional_Amt = md.newFieldInGroup("md_Traditional_Amt", "TRADITIONAL-AMT", FieldType.STRING, 15);
        md_Real_Estate_Units = md.newFieldInGroup("md_Real_Estate_Units", "REAL-ESTATE-UNITS", FieldType.STRING, 12);
        md_Tiaa_Initial_Payt_Amt = md.newFieldInGroup("md_Tiaa_Initial_Payt_Amt", "TIAA-INITIAL-PAYT-AMT", FieldType.STRING, 15);
        md_Tiaa_Excluded_Amt = md.newFieldInGroup("md_Tiaa_Excluded_Amt", "TIAA-EXCLUDED-AMT", FieldType.STRING, 15);
        md_Cref_Initial_Payt_Amt = md.newFieldInGroup("md_Cref_Initial_Payt_Amt", "CREF-INITIAL-PAYT-AMT", FieldType.STRING, 15);
        md_Cref_Excluded_Amt = md.newFieldInGroup("md_Cref_Excluded_Amt", "CREF-EXCLUDED-AMT", FieldType.STRING, 15);
        md_Contigent_Chrg = md.newFieldInGroup("md_Contigent_Chrg", "CONTIGENT-CHRG", FieldType.STRING, 6);
        md_Surrender_Chrg = md.newFieldInGroup("md_Surrender_Chrg", "SURRENDER-CHRG", FieldType.STRING, 6);
        md_Da_Tiaa_Total_Amt = md.newFieldInGroup("md_Da_Tiaa_Total_Amt", "DA-TIAA-TOTAL-AMT", FieldType.STRING, 15);
        md_Real_Estate_Amt = md.newFieldInGroup("md_Real_Estate_Amt", "REAL-ESTATE-AMT", FieldType.STRING, 15);
        md_Seprate_Accnt_Chrg = md.newFieldInGroup("md_Seprate_Accnt_Chrg", "SEPRATE-ACCNT-CHRG", FieldType.STRING, 6);
        md_Da_Cref_Total_Amt = md.newFieldInGroup("md_Da_Cref_Total_Amt", "DA-CREF-TOTAL-AMT", FieldType.STRING, 15);
        md_Tiaa_Ed_Numb = md.newFieldArrayInGroup("md_Tiaa_Ed_Numb", "TIAA-ED-NUMB", FieldType.STRING, 9, new DbsArrayController(1,2));
        md_Cref_Ed_Numb = md.newFieldArrayInGroup("md_Cref_Ed_Numb", "CREF-ED-NUMB", FieldType.STRING, 9, new DbsArrayController(1,2));
        md_Tiaa_Form_Numb = md.newFieldArrayInGroup("md_Tiaa_Form_Numb", "TIAA-FORM-NUMB", FieldType.STRING, 15, new DbsArrayController(1,2));
        md_Cref_Form_Numb = md.newFieldArrayInGroup("md_Cref_Form_Numb", "CREF-FORM-NUMB", FieldType.STRING, 15, new DbsArrayController(1,2));
        md_Tiaa_Prod_Cdes = md.newFieldArrayInGroup("md_Tiaa_Prod_Cdes", "TIAA-PROD-CDES", FieldType.STRING, 20, new DbsArrayController(1,2));
        md_Cref_Prod_Cdes = md.newFieldArrayInGroup("md_Cref_Prod_Cdes", "CREF-PROD-CDES", FieldType.STRING, 20, new DbsArrayController(1,2));
        md_Tiaa_Headers = md.newFieldArrayInGroup("md_Tiaa_Headers", "TIAA-HEADERS", FieldType.STRING, 85, new DbsArrayController(1,2));
        md_Cref_Headers = md.newFieldArrayInGroup("md_Cref_Headers", "CREF-HEADERS", FieldType.STRING, 85, new DbsArrayController(1,2));
        md_State_Name = md.newFieldInGroup("md_State_Name", "STATE-NAME", FieldType.STRING, 15);
        md_Inst_Name = md.newFieldInGroup("md_Inst_Name", "INST-NAME", FieldType.STRING, 76);
        md_Res_Issue_State = md.newFieldInGroup("md_Res_Issue_State", "RES-ISSUE-STATE", FieldType.STRING, 2);
        md_Lob = md.newFieldInGroup("md_Lob", "LOB", FieldType.STRING, 1);
        md_Lob_Type = md.newFieldInGroup("md_Lob_Type", "LOB-TYPE", FieldType.STRING, 1);
        md_Region = md.newFieldInGroup("md_Region", "REGION", FieldType.STRING, 1);
        md_Branch = md.newFieldInGroup("md_Branch", "BRANCH", FieldType.STRING, 1);
        md_Need = md.newFieldInGroup("md_Need", "NEED", FieldType.STRING, 1);
        md_Ppg_Code = md.newFieldInGroup("md_Ppg_Code", "PPG-CODE", FieldType.STRING, 6);
        md_Annual_Req_Dist_Amt = md.newFieldInGroup("md_Annual_Req_Dist_Amt", "ANNUAL-REQ-DIST-AMT", FieldType.STRING, 15);
        md_Frst_Req_Pymt_Year = md.newFieldInGroup("md_Frst_Req_Pymt_Year", "FRST-REQ-PYMT-YEAR", FieldType.STRING, 4);
        md_Frst_Req_Pymt_Amt = md.newFieldInGroup("md_Frst_Req_Pymt_Amt", "FRST-REQ-PYMT-AMT", FieldType.STRING, 15);
        md_Cntrct_Apprvl = md.newFieldInGroup("md_Cntrct_Apprvl", "CNTRCT-APPRVL", FieldType.STRING, 1);
        md_Cntrct_Option = md.newFieldInGroup("md_Cntrct_Option", "CNTRCT-OPTION", FieldType.STRING, 10);
        md_Cntrct_Cash_Status = md.newFieldInGroup("md_Cntrct_Cash_Status", "CNTRCT-CASH-STATUS", FieldType.STRING, 1);
        md_Cntrct_Retr_Surv = md.newFieldInGroup("md_Cntrct_Retr_Surv", "CNTRCT-RETR-SURV", FieldType.STRING, 1);
        md_Guar_Period_Flag = md.newFieldInGroup("md_Guar_Period_Flag", "GUAR-PERIOD-FLAG", FieldType.STRING, 1);
        md_Multiple_Intr_Rate = md.newFieldInGroup("md_Multiple_Intr_Rate", "MULTIPLE-INTR-RATE", FieldType.STRING, 1);
        md_Tiaa_Cntr_Settled = md.newFieldInGroup("md_Tiaa_Cntr_Settled", "TIAA-CNTR-SETTLED", FieldType.STRING, 2);
        md_Rea_Cntr_Settled = md.newFieldInGroup("md_Rea_Cntr_Settled", "REA-CNTR-SETTLED", FieldType.STRING, 2);
        md_Cref_Cntr_Settled = md.newFieldInGroup("md_Cref_Cntr_Settled", "CREF-CNTR-SETTLED", FieldType.STRING, 2);
        md_Cref_Accnt_Number = md.newFieldInGroup("md_Cref_Accnt_Number", "CREF-ACCNT-NUMBER", FieldType.STRING, 2);
        md_Internal_Trnsf = md.newFieldInGroup("md_Internal_Trnsf", "INTERNAL-TRNSF", FieldType.STRING, 1);
        md_Revaluation_Cde = md.newFieldInGroup("md_Revaluation_Cde", "REVALUATION-CDE", FieldType.STRING, 1);
        md_Orig_Issue_State = md.newFieldInGroup("md_Orig_Issue_State", "ORIG-ISSUE-STATE", FieldType.STRING, 2);
        md_Ownership_Code = md.newFieldInGroup("md_Ownership_Code", "OWNERSHIP-CODE", FieldType.STRING, 1);
        md_Rtb_Request = md.newFieldInGroup("md_Rtb_Request", "RTB-REQUEST", FieldType.STRING, 1);
        md_Issue_Eq_Frst_Pymt = md.newFieldInGroup("md_Issue_Eq_Frst_Pymt", "ISSUE-EQ-FRST-PYMT", FieldType.STRING, 1);
        md_Estate_Cntgnt = md.newFieldInGroup("md_Estate_Cntgnt", "ESTATE-CNTGNT", FieldType.STRING, 1);
        md_Freeform_Ben = md.newFieldInGroup("md_Freeform_Ben", "FREEFORM-BEN", FieldType.STRING, 1);
        md_Prmry_Ben_Stnd_Txt = md.newFieldInGroup("md_Prmry_Ben_Stnd_Txt", "PRMRY-BEN-STND-TXT", FieldType.STRING, 1);
        md_Prmry_Ben_Lump_Sum = md.newFieldInGroup("md_Prmry_Ben_Lump_Sum", "PRMRY-BEN-LUMP-SUM", FieldType.STRING, 1);
        md_Prmry_Ben_Auto = md.newFieldInGroup("md_Prmry_Ben_Auto", "PRMRY-BEN-AUTO", FieldType.STRING, 1);
        md_Prmry_Ben_Child_Prvsn = md.newFieldInGroup("md_Prmry_Ben_Child_Prvsn", "PRMRY-BEN-CHILD-PRVSN", FieldType.STRING, 1);
        md_Prmry_Ben_Numb = md.newFieldInGroup("md_Prmry_Ben_Numb", "PRMRY-BEN-NUMB", FieldType.STRING, 2);
        md_Cntgnt_Ben_Stnd_Txt = md.newFieldInGroup("md_Cntgnt_Ben_Stnd_Txt", "CNTGNT-BEN-STND-TXT", FieldType.STRING, 1);
        md_Cntgnt_Ben_Lump_Sum = md.newFieldInGroup("md_Cntgnt_Ben_Lump_Sum", "CNTGNT-BEN-LUMP-SUM", FieldType.STRING, 1);
        md_Cntgnt_Ben_Auto = md.newFieldInGroup("md_Cntgnt_Ben_Auto", "CNTGNT-BEN-AUTO", FieldType.STRING, 1);
        md_Cntgnt_Ben_Child_Prvsn = md.newFieldInGroup("md_Cntgnt_Ben_Child_Prvsn", "CNTGNT-BEN-CHILD-PRVSN", FieldType.STRING, 1);
        md_Cntgnt_Ben_Numb = md.newFieldInGroup("md_Cntgnt_Ben_Numb", "CNTGNT-BEN-NUMB", FieldType.STRING, 2);
        md_Gsra_Ira_Surr_Chrg = md.newFieldInGroup("md_Gsra_Ira_Surr_Chrg", "GSRA-IRA-SURR-CHRG", FieldType.STRING, 1);
        md_Da_Death_Sur_Right = md.newFieldInGroup("md_Da_Death_Sur_Right", "DA-DEATH-SUR-RIGHT", FieldType.STRING, 1);
        md_Gra_Surr_Right = md.newFieldInGroup("md_Gra_Surr_Right", "GRA-SURR-RIGHT", FieldType.STRING, 1);
        md_Personal_Annuity = md.newFieldInGroup("md_Personal_Annuity", "PERSONAL-ANNUITY", FieldType.STRING, 1);
        md_Mit_Orgnl_Unit_Cde = md.newFieldInGroup("md_Mit_Orgnl_Unit_Cde", "MIT-ORGNL-UNIT-CDE", FieldType.STRING, 8);
        md_Graded_Ind = md.newFieldInGroup("md_Graded_Ind", "GRADED-IND", FieldType.STRING, 1);
        md_Frst_Pymt_After_Iss = md.newFieldInGroup("md_Frst_Pymt_After_Iss", "FRST-PYMT-AFTER-ISS", FieldType.STRING, 1);
        md_Tiaa_Cntrct_Type = md.newFieldInGroup("md_Tiaa_Cntrct_Type", "TIAA-CNTRCT-TYPE", FieldType.STRING, 1);
        md_Rea_Cntrct_Type = md.newFieldInGroup("md_Rea_Cntrct_Type", "REA-CNTRCT-TYPE", FieldType.STRING, 1);
        md_Cref_Cntrct_Type = md.newFieldInGroup("md_Cref_Cntrct_Type", "CREF-CNTRCT-TYPE", FieldType.STRING, 1);
        md_Comut_Intr_Num = md.newFieldInGroup("md_Comut_Intr_Num", "COMUT-INTR-NUM", FieldType.STRING, 2);
        md_Trnsf_Cntr_Settled = md.newFieldInGroup("md_Trnsf_Cntr_Settled", "TRNSF-CNTR-SETTLED", FieldType.STRING, 2);
        md_Spec_Cntrct_Type = md.newFieldInGroup("md_Spec_Cntrct_Type", "SPEC-CNTRCT-TYPE", FieldType.STRING, 1);
        md_Orig_Issue_Date = md.newFieldInGroup("md_Orig_Issue_Date", "ORIG-ISSUE-DATE", FieldType.STRING, 8);
        md_Access_Ind = md.newFieldInGroup("md_Access_Ind", "ACCESS-IND", FieldType.STRING, 4);
        md_Roth_Ind = md.newFieldInGroup("md_Roth_Ind", "ROTH-IND", FieldType.STRING, 1);
        md_Payee_Ind = md.newFieldInGroup("md_Payee_Ind", "PAYEE-IND", FieldType.STRING, 1);
        md_Guid = md.newFieldInGroup("md_Guid", "GUID", FieldType.NUMERIC, 5);
        md_Invest_Num_Tot = md.newFieldInGroup("md_Invest_Num_Tot", "INVEST-NUM-TOT", FieldType.NUMERIC, 2);
        md_Multi_Plan_Ind = md.newFieldInGroup("md_Multi_Plan_Ind", "MULTI-PLAN-IND", FieldType.STRING, 1);
        md_Plan_Num = md.newFieldInGroup("md_Plan_Num", "PLAN-NUM", FieldType.STRING, 6);
        md_Multi_Plan_Tbl = md.newFieldArrayInGroup("md_Multi_Plan_Tbl", "MULTI-PLAN-TBL", FieldType.STRING, 12, new DbsArrayController(1,15));
        md_Multi_Plan_TblRedef2 = md.newGroupInGroup("md_Multi_Plan_TblRedef2", "Redefines", md_Multi_Plan_Tbl);
        md_Multi_Plan_Info = md_Multi_Plan_TblRedef2.newGroupArrayInGroup("md_Multi_Plan_Info", "MULTI-PLAN-INFO", new DbsArrayController(1,15));
        md_Multi_Plan_No = md_Multi_Plan_Info.newFieldInGroup("md_Multi_Plan_No", "MULTI-PLAN-NO", FieldType.STRING, 6);
        md_Multi_Sub_Plan = md_Multi_Plan_Info.newFieldInGroup("md_Multi_Sub_Plan", "MULTI-SUB-PLAN", FieldType.STRING, 6);
        mdRedef3 = newGroupInRecord("mdRedef3", "Redefines", md);
        md_Contract_1 = mdRedef3.newFieldInGroup("md_Contract_1", "CONTRACT-1", FieldType.STRING, 250);
        md_Contract_2 = mdRedef3.newFieldInGroup("md_Contract_2", "CONTRACT-2", FieldType.STRING, 250);
        md_Contract_3 = mdRedef3.newFieldInGroup("md_Contract_3", "CONTRACT-3", FieldType.STRING, 250);
        md_Contract_4 = mdRedef3.newFieldInGroup("md_Contract_4", "CONTRACT-4", FieldType.STRING, 250);
        md_Contract_5 = mdRedef3.newFieldInGroup("md_Contract_5", "CONTRACT-5", FieldType.STRING, 250);
        md_Contract_6 = mdRedef3.newFieldInGroup("md_Contract_6", "CONTRACT-6", FieldType.STRING, 250);
        md_Contract_7 = mdRedef3.newFieldInGroup("md_Contract_7", "CONTRACT-7", FieldType.STRING, 250);
        md_Contract_8 = mdRedef3.newFieldInGroup("md_Contract_8", "CONTRACT-8", FieldType.STRING, 250);
        md_Contract_9 = mdRedef3.newFieldInGroup("md_Contract_9", "CONTRACT-9", FieldType.STRING, 14);

        this.setRecordName("LdaCisl601m");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl601m() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
