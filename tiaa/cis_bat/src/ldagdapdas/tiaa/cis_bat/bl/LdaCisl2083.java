/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:34 PM
**        * FROM NATURAL LDA     : CISL2083
************************************************************
**        * FILE NAME            : LdaCisl2083.java
**        * CLASS NAME           : LdaCisl2083
**        * INSTANCE NAME        : LdaCisl2083
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl2083 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cis_Prtcpnt_File;
    private DbsField cis_Prtcpnt_File_Cis_Pin_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Rqst_Id_Key;
    private DbsField cis_Prtcpnt_File_Cis_Tiaa_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Cert_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Tiaa_Doi;
    private DbsField cis_Prtcpnt_File_Cis_Cref_Doi;
    private DbsField cis_Prtcpnt_File_Cis_Opn_Clsd_Ind;
    private DbsField cis_Prtcpnt_File_Cis_Status_Cd;
    private DbsField cis_Prtcpnt_File_Cis_Cntrct_Type;
    private DbsField cis_Prtcpnt_File_Cis_Rqst_Id;
    private DbsField cis_Prtcpnt_File_Cis_Cntrct_Print_Dte;
    private DbsField cis_Prtcpnt_File_Cis_Extract_Date;
    private DbsGroup cis_Prtcpnt_File_Cis_Mit_Request;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Dte_Tme;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Oprtr_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Original_Unit_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Work_Process_Id;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Step_Id;
    private DbsField cis_Prtcpnt_File_Cis_Mit_Status_Cde;
    private DbsField cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme;

    public DataAccessProgramView getVw_cis_Prtcpnt_File() { return vw_cis_Prtcpnt_File; }

    public DbsField getCis_Prtcpnt_File_Cis_Pin_Nbr() { return cis_Prtcpnt_File_Cis_Pin_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Rqst_Id_Key() { return cis_Prtcpnt_File_Cis_Rqst_Id_Key; }

    public DbsField getCis_Prtcpnt_File_Cis_Tiaa_Nbr() { return cis_Prtcpnt_File_Cis_Tiaa_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Cert_Nbr() { return cis_Prtcpnt_File_Cis_Cert_Nbr; }

    public DbsField getCis_Prtcpnt_File_Cis_Tiaa_Doi() { return cis_Prtcpnt_File_Cis_Tiaa_Doi; }

    public DbsField getCis_Prtcpnt_File_Cis_Cref_Doi() { return cis_Prtcpnt_File_Cis_Cref_Doi; }

    public DbsField getCis_Prtcpnt_File_Cis_Opn_Clsd_Ind() { return cis_Prtcpnt_File_Cis_Opn_Clsd_Ind; }

    public DbsField getCis_Prtcpnt_File_Cis_Status_Cd() { return cis_Prtcpnt_File_Cis_Status_Cd; }

    public DbsField getCis_Prtcpnt_File_Cis_Cntrct_Type() { return cis_Prtcpnt_File_Cis_Cntrct_Type; }

    public DbsField getCis_Prtcpnt_File_Cis_Rqst_Id() { return cis_Prtcpnt_File_Cis_Rqst_Id; }

    public DbsField getCis_Prtcpnt_File_Cis_Cntrct_Print_Dte() { return cis_Prtcpnt_File_Cis_Cntrct_Print_Dte; }

    public DbsField getCis_Prtcpnt_File_Cis_Extract_Date() { return cis_Prtcpnt_File_Cis_Extract_Date; }

    public DbsGroup getCis_Prtcpnt_File_Cis_Mit_Request() { return cis_Prtcpnt_File_Cis_Mit_Request; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Rqst_Log_Dte_Tme() { return cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Dte_Tme; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Rqst_Log_Oprtr_Cde() { return cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Oprtr_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Original_Unit_Cde() { return cis_Prtcpnt_File_Cis_Mit_Original_Unit_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Work_Process_Id() { return cis_Prtcpnt_File_Cis_Mit_Work_Process_Id; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Step_Id() { return cis_Prtcpnt_File_Cis_Mit_Step_Id; }

    public DbsField getCis_Prtcpnt_File_Cis_Mit_Status_Cde() { return cis_Prtcpnt_File_Cis_Mit_Status_Cde; }

    public DbsField getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme() { return cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cis_Prtcpnt_File = new DataAccessProgramView(new NameInfo("vw_cis_Prtcpnt_File", "CIS-PRTCPNT-FILE"), "CIS_PRTCPNT_FILE_12", "CIS_PRTCPNT_FILE");
        cis_Prtcpnt_File_Cis_Pin_Nbr = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CIS_PIN_NBR");
        cis_Prtcpnt_File_Cis_Rqst_Id_Key = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "CIS_RQST_ID_KEY");
        cis_Prtcpnt_File_Cis_Tiaa_Nbr = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_TIAA_NBR");
        cis_Prtcpnt_File_Cis_Cert_Nbr = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_CERT_NBR");
        cis_Prtcpnt_File_Cis_Tiaa_Doi = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_TIAA_DOI");
        cis_Prtcpnt_File_Cis_Cref_Doi = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_CREF_DOI");
        cis_Prtcpnt_File_Cis_Opn_Clsd_Ind = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Opn_Clsd_Ind", "CIS-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_OPN_CLSD_IND");
        cis_Prtcpnt_File_Cis_Status_Cd = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Status_Cd", "CIS-STATUS-CD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_STATUS_CD");
        cis_Prtcpnt_File_Cis_Cntrct_Type = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_CNTRCT_TYPE");
        cis_Prtcpnt_File_Cis_Rqst_Id = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_RQST_ID");
        cis_Prtcpnt_File_Cis_Cntrct_Print_Dte = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Cntrct_Print_Dte", "CIS-CNTRCT-PRINT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CNTRCT_PRINT_DTE");
        cis_Prtcpnt_File_Cis_Extract_Date = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Extract_Date", "CIS-EXTRACT-DATE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_EXTRACT_DATE");
        cis_Prtcpnt_File_Cis_Mit_Request = vw_cis_Prtcpnt_File.getRecord().newGroupInGroup("cis_Prtcpnt_File_Cis_Mit_Request", "CIS-MIT-REQUEST");
        cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Dte_Tme = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Dte_Tme", "CIS-MIT-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CIS_MIT_RQST_LOG_DTE_TME");
        cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Oprtr_Cde = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Rqst_Log_Oprtr_Cde", 
            "CIS-MIT-RQST-LOG-OPRTR-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_MIT_RQST_LOG_OPRTR_CDE");
        cis_Prtcpnt_File_Cis_Mit_Original_Unit_Cde = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Original_Unit_Cde", "CIS-MIT-ORIGINAL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_MIT_ORIGINAL_UNIT_CDE");
        cis_Prtcpnt_File_Cis_Mit_Work_Process_Id = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Work_Process_Id", "CIS-MIT-WORK-PROCESS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "CIS_MIT_WORK_PROCESS_ID");
        cis_Prtcpnt_File_Cis_Mit_Step_Id = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Step_Id", "CIS-MIT-STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "CIS_MIT_STEP_ID");
        cis_Prtcpnt_File_Cis_Mit_Status_Cde = cis_Prtcpnt_File_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_File_Cis_Mit_Status_Cde", "CIS-MIT-STATUS-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_MIT_STATUS_CDE");
        cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme", 
            "CIS-RQST-SYS-MIT-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CIS_RQST_SYS_MIT_LOG_DTE_TME");

        this.setRecordName("LdaCisl2083");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cis_Prtcpnt_File.reset();
    }

    // Constructor
    public LdaCisl2083() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
