/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:43 PM
**        * FROM NATURAL LDA     : CISL609
************************************************************
**        * FILE NAME            : LdaCisl609.java
**        * CLASS NAME           : LdaCisl609
**        * INSTANCE NAME        : LdaCisl609
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl609 extends DbsRecord
{
    // Properties
    private DbsGroup ca;
    private DbsField ca_Record_Id;
    private DbsGroup ca_Record_IdRedef1;
    private DbsField ca_Record_Type;
    private DbsField ca_Output_Profile;
    private DbsField ca_Reprint_Ind;
    private DbsField ca_Contract_Type;
    private DbsField ca_Rqst_Id;
    private DbsField ca_Tiaa_Numb;
    private DbsField ca_Cref_Cert_Numb;
    private DbsField ca_Da_Cref_Numb;
    private DbsField ca_Da_Cref_Amt;
    private DbsField ca_Last_Record;

    public DbsGroup getCa() { return ca; }

    public DbsField getCa_Record_Id() { return ca_Record_Id; }

    public DbsGroup getCa_Record_IdRedef1() { return ca_Record_IdRedef1; }

    public DbsField getCa_Record_Type() { return ca_Record_Type; }

    public DbsField getCa_Output_Profile() { return ca_Output_Profile; }

    public DbsField getCa_Reprint_Ind() { return ca_Reprint_Ind; }

    public DbsField getCa_Contract_Type() { return ca_Contract_Type; }

    public DbsField getCa_Rqst_Id() { return ca_Rqst_Id; }

    public DbsField getCa_Tiaa_Numb() { return ca_Tiaa_Numb; }

    public DbsField getCa_Cref_Cert_Numb() { return ca_Cref_Cert_Numb; }

    public DbsField getCa_Da_Cref_Numb() { return ca_Da_Cref_Numb; }

    public DbsField getCa_Da_Cref_Amt() { return ca_Da_Cref_Amt; }

    public DbsField getCa_Last_Record() { return ca_Last_Record; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ca = newGroupInRecord("ca", "CA");
        ca_Record_Id = ca.newFieldInGroup("ca_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        ca_Record_IdRedef1 = ca.newGroupInGroup("ca_Record_IdRedef1", "Redefines", ca_Record_Id);
        ca_Record_Type = ca_Record_IdRedef1.newFieldInGroup("ca_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        ca_Output_Profile = ca_Record_IdRedef1.newFieldInGroup("ca_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        ca_Reprint_Ind = ca_Record_IdRedef1.newFieldInGroup("ca_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        ca_Contract_Type = ca_Record_IdRedef1.newFieldInGroup("ca_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        ca_Rqst_Id = ca_Record_IdRedef1.newFieldInGroup("ca_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        ca_Tiaa_Numb = ca_Record_IdRedef1.newFieldInGroup("ca_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        ca_Cref_Cert_Numb = ca_Record_IdRedef1.newFieldInGroup("ca_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        ca_Da_Cref_Numb = ca.newFieldInGroup("ca_Da_Cref_Numb", "DA-CREF-NUMB", FieldType.STRING, 12);
        ca_Da_Cref_Amt = ca.newFieldInGroup("ca_Da_Cref_Amt", "DA-CREF-AMT", FieldType.STRING, 15);
        ca_Last_Record = ca.newFieldInGroup("ca_Last_Record", "LAST-RECORD", FieldType.STRING, 1);

        this.setRecordName("LdaCisl609");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl609() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
