/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:20 PM
**        * FROM NATURAL PDA     : CISA2081
************************************************************
**        * FILE NAME            : PdaCisa2081.java
**        * CLASS NAME           : PdaCisa2081
**        * INSTANCE NAME        : PdaCisa2081
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCisa2081 extends PdaBase
{
    // Properties
    private DbsGroup cisa2081;
    private DbsField cisa2081_Cis_Bene_Rqst_Id_Key;
    private DbsField cisa2081_Cis_Bene_Tiaa_Nbr;
    private DbsField cisa2081_Cis_Isn_01;
    private DbsField cisa2081_Cis_Isn_02;
    private DbsField cisa2081_Cis_Error_Code;
    private DbsField cisa2081_Pnd_Db_Up;
    private DbsField cisa2081_Cis_Bene_Sync_Ind;
    private DbsField cisa2081_Cis_Bene_Sync_Upd_Date;

    public DbsGroup getCisa2081() { return cisa2081; }

    public DbsField getCisa2081_Cis_Bene_Rqst_Id_Key() { return cisa2081_Cis_Bene_Rqst_Id_Key; }

    public DbsField getCisa2081_Cis_Bene_Tiaa_Nbr() { return cisa2081_Cis_Bene_Tiaa_Nbr; }

    public DbsField getCisa2081_Cis_Isn_01() { return cisa2081_Cis_Isn_01; }

    public DbsField getCisa2081_Cis_Isn_02() { return cisa2081_Cis_Isn_02; }

    public DbsField getCisa2081_Cis_Error_Code() { return cisa2081_Cis_Error_Code; }

    public DbsField getCisa2081_Pnd_Db_Up() { return cisa2081_Pnd_Db_Up; }

    public DbsField getCisa2081_Cis_Bene_Sync_Ind() { return cisa2081_Cis_Bene_Sync_Ind; }

    public DbsField getCisa2081_Cis_Bene_Sync_Upd_Date() { return cisa2081_Cis_Bene_Sync_Upd_Date; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cisa2081 = dbsRecord.newGroupInRecord("cisa2081", "CISA2081");
        cisa2081.setParameterOption(ParameterOption.ByReference);
        cisa2081_Cis_Bene_Rqst_Id_Key = cisa2081.newFieldInGroup("cisa2081_Cis_Bene_Rqst_Id_Key", "CIS-BENE-RQST-ID-KEY", FieldType.STRING, 35);
        cisa2081_Cis_Bene_Tiaa_Nbr = cisa2081.newFieldInGroup("cisa2081_Cis_Bene_Tiaa_Nbr", "CIS-BENE-TIAA-NBR", FieldType.STRING, 10);
        cisa2081_Cis_Isn_01 = cisa2081.newFieldInGroup("cisa2081_Cis_Isn_01", "CIS-ISN-01", FieldType.PACKED_DECIMAL, 10);
        cisa2081_Cis_Isn_02 = cisa2081.newFieldInGroup("cisa2081_Cis_Isn_02", "CIS-ISN-02", FieldType.PACKED_DECIMAL, 10);
        cisa2081_Cis_Error_Code = cisa2081.newFieldInGroup("cisa2081_Cis_Error_Code", "CIS-ERROR-CODE", FieldType.STRING, 2);
        cisa2081_Pnd_Db_Up = cisa2081.newFieldInGroup("cisa2081_Pnd_Db_Up", "#DB-UP", FieldType.BOOLEAN);
        cisa2081_Cis_Bene_Sync_Ind = cisa2081.newFieldInGroup("cisa2081_Cis_Bene_Sync_Ind", "CIS-BENE-SYNC-IND", FieldType.STRING, 1);
        cisa2081_Cis_Bene_Sync_Upd_Date = cisa2081.newFieldInGroup("cisa2081_Cis_Bene_Sync_Upd_Date", "CIS-BENE-SYNC-UPD-DATE", FieldType.DATE);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCisa2081(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

