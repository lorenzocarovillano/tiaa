/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:33 PM
**        * FROM NATURAL LDA     : CISL2082
************************************************************
**        * FILE NAME            : LdaCisl2082.java
**        * CLASS NAME           : LdaCisl2082
**        * INSTANCE NAME        : LdaCisl2082
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl2082 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cis_Bene_File_02;
    private DbsField cis_Bene_File_02_Cis_Bene_Sync_Ind;
    private DbsField cis_Bene_File_02_Cis_Bene_Sync_Upd_Date;
    private DbsField cis_Bene_File_02_Cis_Rcrd_Type_Cde;
    private DbsField cis_Bene_File_02_Cis_Bene_Tiaa_Nbr;
    private DbsField cis_Bene_File_02_Cis_Bene_Cref_Nbr;
    private DbsField cis_Bene_File_02_Cis_Bene_Rqst_Id;
    private DbsField cis_Bene_File_02_Cis_Bene_Unique_Id_Nbr;
    private DbsField cis_Bene_File_02_Cis_Bene_Annt_Ssn;
    private DbsField cis_Bene_File_02_Cis_Bene_Annt_Prfx;
    private DbsField cis_Bene_File_02_Cis_Bene_Annt_Frst_Nme;
    private DbsField cis_Bene_File_02_Cis_Bene_Annt_Mid_Nme;
    private DbsField cis_Bene_File_02_Cis_Bene_Annt_Lst_Nme;
    private DbsField cis_Bene_File_02_Cis_Bene_Annt_Sffx;
    private DbsField cis_Bene_File_02_Cis_Bene_Rqst_Id_Key;
    private DbsField cis_Bene_File_02_Cis_Bene_Std_Free;
    private DbsField cis_Bene_File_02_Cis_Bene_Estate;
    private DbsField cis_Bene_File_02_Cis_Bene_Trust;
    private DbsField cis_Bene_File_02_Cis_Bene_Category;
    private DbsField cis_Bene_File_02_Cis_Prmry_Bene_Child_Prvsn;
    private DbsField cis_Bene_File_02_Cis_Cntgnt_Bene_Child_Prvsn;
    private DbsGroup cis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup;
    private DbsField cis_Bene_File_02_Cis_Bene_Dsgntn_Txt;

    public DataAccessProgramView getVw_cis_Bene_File_02() { return vw_cis_Bene_File_02; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Sync_Ind() { return cis_Bene_File_02_Cis_Bene_Sync_Ind; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Sync_Upd_Date() { return cis_Bene_File_02_Cis_Bene_Sync_Upd_Date; }

    public DbsField getCis_Bene_File_02_Cis_Rcrd_Type_Cde() { return cis_Bene_File_02_Cis_Rcrd_Type_Cde; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Tiaa_Nbr() { return cis_Bene_File_02_Cis_Bene_Tiaa_Nbr; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Cref_Nbr() { return cis_Bene_File_02_Cis_Bene_Cref_Nbr; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Rqst_Id() { return cis_Bene_File_02_Cis_Bene_Rqst_Id; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Unique_Id_Nbr() { return cis_Bene_File_02_Cis_Bene_Unique_Id_Nbr; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Annt_Ssn() { return cis_Bene_File_02_Cis_Bene_Annt_Ssn; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Annt_Prfx() { return cis_Bene_File_02_Cis_Bene_Annt_Prfx; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Annt_Frst_Nme() { return cis_Bene_File_02_Cis_Bene_Annt_Frst_Nme; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Annt_Mid_Nme() { return cis_Bene_File_02_Cis_Bene_Annt_Mid_Nme; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Annt_Lst_Nme() { return cis_Bene_File_02_Cis_Bene_Annt_Lst_Nme; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Annt_Sffx() { return cis_Bene_File_02_Cis_Bene_Annt_Sffx; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Rqst_Id_Key() { return cis_Bene_File_02_Cis_Bene_Rqst_Id_Key; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Std_Free() { return cis_Bene_File_02_Cis_Bene_Std_Free; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Estate() { return cis_Bene_File_02_Cis_Bene_Estate; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Trust() { return cis_Bene_File_02_Cis_Bene_Trust; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Category() { return cis_Bene_File_02_Cis_Bene_Category; }

    public DbsField getCis_Bene_File_02_Cis_Prmry_Bene_Child_Prvsn() { return cis_Bene_File_02_Cis_Prmry_Bene_Child_Prvsn; }

    public DbsField getCis_Bene_File_02_Cis_Cntgnt_Bene_Child_Prvsn() { return cis_Bene_File_02_Cis_Cntgnt_Bene_Child_Prvsn; }

    public DbsGroup getCis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup() { return cis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup; }

    public DbsField getCis_Bene_File_02_Cis_Bene_Dsgntn_Txt() { return cis_Bene_File_02_Cis_Bene_Dsgntn_Txt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cis_Bene_File_02 = new DataAccessProgramView(new NameInfo("vw_cis_Bene_File_02", "CIS-BENE-FILE-02"), "CIS_BENE_FILE_02_12", "CIS_BENE_FILE");
        cis_Bene_File_02_Cis_Bene_Sync_Ind = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Sync_Ind", "CIS-BENE-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_BENE_SYNC_IND");
        cis_Bene_File_02_Cis_Bene_Sync_Upd_Date = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Sync_Upd_Date", "CIS-BENE-SYNC-UPD-DATE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_BENE_SYNC_UPD_DATE");
        cis_Bene_File_02_Cis_Rcrd_Type_Cde = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Rcrd_Type_Cde", "CIS-RCRD-TYPE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_RCRD_TYPE_CDE");
        cis_Bene_File_02_Cis_Bene_Tiaa_Nbr = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Tiaa_Nbr", "CIS-BENE-TIAA-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_BENE_TIAA_NBR");
        cis_Bene_File_02_Cis_Bene_Cref_Nbr = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Cref_Nbr", "CIS-BENE-CREF-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_BENE_CREF_NBR");
        cis_Bene_File_02_Cis_Bene_Rqst_Id = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Rqst_Id", "CIS-BENE-RQST-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_BENE_RQST_ID");
        cis_Bene_File_02_Cis_Bene_Unique_Id_Nbr = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Unique_Id_Nbr", "CIS-BENE-UNIQUE-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CIS_BENE_UNIQUE_ID_NBR");
        cis_Bene_File_02_Cis_Bene_Annt_Ssn = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Annt_Ssn", "CIS-BENE-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_SSN");
        cis_Bene_File_02_Cis_Bene_Annt_Prfx = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Annt_Prfx", "CIS-BENE-ANNT-PRFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_PRFX");
        cis_Bene_File_02_Cis_Bene_Annt_Frst_Nme = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Annt_Frst_Nme", "CIS-BENE-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_FRST_NME");
        cis_Bene_File_02_Cis_Bene_Annt_Mid_Nme = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Annt_Mid_Nme", "CIS-BENE-ANNT-MID-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_MID_NME");
        cis_Bene_File_02_Cis_Bene_Annt_Lst_Nme = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Annt_Lst_Nme", "CIS-BENE-ANNT-LST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_LST_NME");
        cis_Bene_File_02_Cis_Bene_Annt_Sffx = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Annt_Sffx", "CIS-BENE-ANNT-SFFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_SFFX");
        cis_Bene_File_02_Cis_Bene_Rqst_Id_Key = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Rqst_Id_Key", "CIS-BENE-RQST-ID-KEY", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "CIS_BENE_RQST_ID_KEY");
        cis_Bene_File_02_Cis_Bene_Std_Free = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Std_Free", "CIS-BENE-STD-FREE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_BENE_STD_FREE");
        cis_Bene_File_02_Cis_Bene_Estate = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Estate", "CIS-BENE-ESTATE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_BENE_ESTATE");
        cis_Bene_File_02_Cis_Bene_Trust = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Trust", "CIS-BENE-TRUST", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_BENE_TRUST");
        cis_Bene_File_02_Cis_Bene_Category = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Bene_Category", "CIS-BENE-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_BENE_CATEGORY");
        cis_Bene_File_02_Cis_Prmry_Bene_Child_Prvsn = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Prmry_Bene_Child_Prvsn", "CIS-PRMRY-BENE-CHILD-PRVSN", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_PRMRY_BENE_CHILD_PRVSN");
        cis_Bene_File_02_Cis_Cntgnt_Bene_Child_Prvsn = vw_cis_Bene_File_02.getRecord().newFieldInGroup("cis_Bene_File_02_Cis_Cntgnt_Bene_Child_Prvsn", 
            "CIS-CNTGNT-BENE-CHILD-PRVSN", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CNTGNT_BENE_CHILD_PRVSN");
        cis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup = vw_cis_Bene_File_02.getRecord().newGroupInGroup("cis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup", "CIS_BENE_DSGNTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CIS_BENE_FILE_CIS_BENE_DSGNTN_TXT");        cis_Bene_File_02_Cis_Bene_Dsgntn_Txt = cis_Bene_File_02_Cis_Bene_Dsgntn_TxtMuGroup.newFieldArrayInGroup("cis_Bene_File_02_Cis_Bene_Dsgntn_Txt", 
            "CIS-BENE-DSGNTN-TXT", FieldType.STRING, 72, new DbsArrayController(1,60), RepeatingFieldStrategy.SubTableFieldArray, "CIS_BENE_DSGNTN_TXT");

        this.setRecordName("LdaCisl2082");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cis_Bene_File_02.reset();
    }

    // Constructor
    public LdaCisl2082() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
