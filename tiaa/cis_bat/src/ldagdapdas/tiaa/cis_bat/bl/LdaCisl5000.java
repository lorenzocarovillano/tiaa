/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:38 PM
**        * FROM NATURAL LDA     : CISL5000
************************************************************
**        * FILE NAME            : LdaCisl5000.java
**        * CLASS NAME           : LdaCisl5000
**        * INSTANCE NAME        : LdaCisl5000
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl5000 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cis_Bene_File_01;
    private DbsField cis_Bene_File_01_Cis_Rcrd_Type_Cde;
    private DbsField cis_Bene_File_01_Cis_Bene_Tiaa_Nbr;
    private DbsField cis_Bene_File_01_Cis_Bene_Cref_Nbr;
    private DbsField cis_Bene_File_01_Cis_Bene_Rqst_Id;
    private DbsField cis_Bene_File_01_Cis_Bene_Unique_Id_Nbr;
    private DbsField cis_Bene_File_01_Cis_Bene_Annt_Ssn;
    private DbsField cis_Bene_File_01_Cis_Bene_Annt_Prfx;
    private DbsField cis_Bene_File_01_Cis_Bene_Annt_Frst_Nme;
    private DbsField cis_Bene_File_01_Cis_Bene_Annt_Mid_Nme;
    private DbsField cis_Bene_File_01_Cis_Bene_Annt_Lst_Nme;
    private DbsField cis_Bene_File_01_Cis_Bene_Annt_Sffx;
    private DbsField cis_Bene_File_01_Cis_Bene_Rqst_Id_Key;
    private DbsField cis_Bene_File_01_Cis_Bene_Std_Free;
    private DbsField cis_Bene_File_01_Cis_Bene_Estate;
    private DbsField cis_Bene_File_01_Cis_Bene_Trust;
    private DbsField cis_Bene_File_01_Cis_Bene_Category;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn;
    private DbsGroup cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta;
    private DbsField cis_Bene_File_01_Cis_Clcltn_Bene_Dod;
    private DbsField cis_Bene_File_01_Cis_Clcltn_Bene_Crossover;
    private DbsField cis_Bene_File_01_Cis_Clcltn_Bene_Name;
    private DbsField cis_Bene_File_01_Cis_Clcltn_Bene_Rltnshp_Cde;
    private DbsField cis_Bene_File_01_Cis_Clcltn_Bene_Ssn_Nbr;
    private DbsField cis_Bene_File_01_Cis_Clcltn_Bene_Dob;
    private DbsField cis_Bene_File_01_Cis_Clcltn_Bene_Sex_Cde;
    private DbsField cis_Bene_File_01_Cis_Clcltn_Bene_Calc_Method;
    private DbsField cis_Bene_File_01_Count_Castcis_Prmry_Bnfcry_Dta;
    private DbsGroup cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Std_Txt;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Lump_Sum;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Auto_Cmnt_Val;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Name;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Rltn;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Dob;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Allctn_Pct;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr;
    private DbsGroup cis_Bene_File_01_Cis_Prmry_Bene_Spcl_TxtMuGroup;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt;
    private DbsField cis_Bene_File_01_Count_Castcis_Cntgnt_Bnfcry_Dta;
    private DbsGroup cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Std_Txt;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Lump_Sum;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Auto_Cmnt_Val;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Name;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Rltn;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Dob;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Allctn_Pct;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr;
    private DbsGroup cis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_TxtMuGroup;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt;
    private DbsField pnd_Cis_Ben_Super_1;
    private DbsGroup pnd_Cis_Ben_Super_1Redef1;
    private DbsField pnd_Cis_Ben_Super_1_Pnd_S_Cis_Rcrd_Type_Cde;
    private DbsField pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Rqst_Id;
    private DbsField pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Unique_Id_Nbr;
    private DbsField pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Tiaa_Nbr;

    public DataAccessProgramView getVw_cis_Bene_File_01() { return vw_cis_Bene_File_01; }

    public DbsField getCis_Bene_File_01_Cis_Rcrd_Type_Cde() { return cis_Bene_File_01_Cis_Rcrd_Type_Cde; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr() { return cis_Bene_File_01_Cis_Bene_Tiaa_Nbr; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Cref_Nbr() { return cis_Bene_File_01_Cis_Bene_Cref_Nbr; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Rqst_Id() { return cis_Bene_File_01_Cis_Bene_Rqst_Id; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Unique_Id_Nbr() { return cis_Bene_File_01_Cis_Bene_Unique_Id_Nbr; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Annt_Ssn() { return cis_Bene_File_01_Cis_Bene_Annt_Ssn; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Annt_Prfx() { return cis_Bene_File_01_Cis_Bene_Annt_Prfx; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Annt_Frst_Nme() { return cis_Bene_File_01_Cis_Bene_Annt_Frst_Nme; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Annt_Mid_Nme() { return cis_Bene_File_01_Cis_Bene_Annt_Mid_Nme; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Annt_Lst_Nme() { return cis_Bene_File_01_Cis_Bene_Annt_Lst_Nme; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Annt_Sffx() { return cis_Bene_File_01_Cis_Bene_Annt_Sffx; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Rqst_Id_Key() { return cis_Bene_File_01_Cis_Bene_Rqst_Id_Key; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Std_Free() { return cis_Bene_File_01_Cis_Bene_Std_Free; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Estate() { return cis_Bene_File_01_Cis_Bene_Estate; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Trust() { return cis_Bene_File_01_Cis_Bene_Trust; }

    public DbsField getCis_Bene_File_01_Cis_Bene_Category() { return cis_Bene_File_01_Cis_Bene_Category; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn() { return cis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn; }

    public DbsGroup getCis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta() { return cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta; }

    public DbsField getCis_Bene_File_01_Cis_Clcltn_Bene_Dod() { return cis_Bene_File_01_Cis_Clcltn_Bene_Dod; }

    public DbsField getCis_Bene_File_01_Cis_Clcltn_Bene_Crossover() { return cis_Bene_File_01_Cis_Clcltn_Bene_Crossover; }

    public DbsField getCis_Bene_File_01_Cis_Clcltn_Bene_Name() { return cis_Bene_File_01_Cis_Clcltn_Bene_Name; }

    public DbsField getCis_Bene_File_01_Cis_Clcltn_Bene_Rltnshp_Cde() { return cis_Bene_File_01_Cis_Clcltn_Bene_Rltnshp_Cde; }

    public DbsField getCis_Bene_File_01_Cis_Clcltn_Bene_Ssn_Nbr() { return cis_Bene_File_01_Cis_Clcltn_Bene_Ssn_Nbr; }

    public DbsField getCis_Bene_File_01_Cis_Clcltn_Bene_Dob() { return cis_Bene_File_01_Cis_Clcltn_Bene_Dob; }

    public DbsField getCis_Bene_File_01_Cis_Clcltn_Bene_Sex_Cde() { return cis_Bene_File_01_Cis_Clcltn_Bene_Sex_Cde; }

    public DbsField getCis_Bene_File_01_Cis_Clcltn_Bene_Calc_Method() { return cis_Bene_File_01_Cis_Clcltn_Bene_Calc_Method; }

    public DbsField getCis_Bene_File_01_Count_Castcis_Prmry_Bnfcry_Dta() { return cis_Bene_File_01_Count_Castcis_Prmry_Bnfcry_Dta; }

    public DbsGroup getCis_Bene_File_01_Cis_Prmry_Bnfcry_Dta() { return cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Std_Txt() { return cis_Bene_File_01_Cis_Prmry_Bene_Std_Txt; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Lump_Sum() { return cis_Bene_File_01_Cis_Prmry_Bene_Lump_Sum; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Auto_Cmnt_Val() { return cis_Bene_File_01_Cis_Prmry_Bene_Auto_Cmnt_Val; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Name() { return cis_Bene_File_01_Cis_Prmry_Bene_Name; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Rltn() { return cis_Bene_File_01_Cis_Prmry_Bene_Rltn; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde() { return cis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr() { return cis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Dob() { return cis_Bene_File_01_Cis_Prmry_Bene_Dob; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Allctn_Pct() { return cis_Bene_File_01_Cis_Prmry_Bene_Allctn_Pct; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr() { return cis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr() { return cis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr; }

    public DbsGroup getCis_Bene_File_01_Cis_Prmry_Bene_Spcl_TxtMuGroup() { return cis_Bene_File_01_Cis_Prmry_Bene_Spcl_TxtMuGroup; }

    public DbsField getCis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt() { return cis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt; }

    public DbsField getCis_Bene_File_01_Count_Castcis_Cntgnt_Bnfcry_Dta() { return cis_Bene_File_01_Count_Castcis_Cntgnt_Bnfcry_Dta; }

    public DbsGroup getCis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta() { return cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Std_Txt() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Std_Txt; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Lump_Sum() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Lump_Sum; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Auto_Cmnt_Val() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Auto_Cmnt_Val; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Name() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Name; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Rltn; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Dob() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Dob; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Allctn_Pct() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Allctn_Pct; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr; }

    public DbsGroup getCis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_TxtMuGroup() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_TxtMuGroup; }

    public DbsField getCis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt() { return cis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt; }

    public DbsField getPnd_Cis_Ben_Super_1() { return pnd_Cis_Ben_Super_1; }

    public DbsGroup getPnd_Cis_Ben_Super_1Redef1() { return pnd_Cis_Ben_Super_1Redef1; }

    public DbsField getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Rcrd_Type_Cde() { return pnd_Cis_Ben_Super_1_Pnd_S_Cis_Rcrd_Type_Cde; }

    public DbsField getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Rqst_Id() { return pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Rqst_Id; }

    public DbsField getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Unique_Id_Nbr() { return pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Unique_Id_Nbr; }

    public DbsField getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Tiaa_Nbr() { return pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Tiaa_Nbr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cis_Bene_File_01 = new DataAccessProgramView(new NameInfo("vw_cis_Bene_File_01", "CIS-BENE-FILE-01"), "CIS_BENE_FILE_01_12", "CIS_BENE_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("CIS_BENE_FILE_01_12"));
        cis_Bene_File_01_Cis_Rcrd_Type_Cde = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Rcrd_Type_Cde", "CIS-RCRD-TYPE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_RCRD_TYPE_CDE");
        cis_Bene_File_01_Cis_Bene_Tiaa_Nbr = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Tiaa_Nbr", "CIS-BENE-TIAA-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_BENE_TIAA_NBR");
        cis_Bene_File_01_Cis_Bene_Cref_Nbr = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Cref_Nbr", "CIS-BENE-CREF-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_BENE_CREF_NBR");
        cis_Bene_File_01_Cis_Bene_Rqst_Id = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Rqst_Id", "CIS-BENE-RQST-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_BENE_RQST_ID");
        cis_Bene_File_01_Cis_Bene_Unique_Id_Nbr = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Unique_Id_Nbr", "CIS-BENE-UNIQUE-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CIS_BENE_UNIQUE_ID_NBR");
        cis_Bene_File_01_Cis_Bene_Annt_Ssn = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Annt_Ssn", "CIS-BENE-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_SSN");
        cis_Bene_File_01_Cis_Bene_Annt_Prfx = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Annt_Prfx", "CIS-BENE-ANNT-PRFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_PRFX");
        cis_Bene_File_01_Cis_Bene_Annt_Frst_Nme = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Annt_Frst_Nme", "CIS-BENE-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_FRST_NME");
        cis_Bene_File_01_Cis_Bene_Annt_Mid_Nme = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Annt_Mid_Nme", "CIS-BENE-ANNT-MID-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_MID_NME");
        cis_Bene_File_01_Cis_Bene_Annt_Lst_Nme = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Annt_Lst_Nme", "CIS-BENE-ANNT-LST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_LST_NME");
        cis_Bene_File_01_Cis_Bene_Annt_Sffx = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Annt_Sffx", "CIS-BENE-ANNT-SFFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_SFFX");
        cis_Bene_File_01_Cis_Bene_Rqst_Id_Key = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Rqst_Id_Key", "CIS-BENE-RQST-ID-KEY", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "CIS_BENE_RQST_ID_KEY");
        cis_Bene_File_01_Cis_Bene_Std_Free = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Std_Free", "CIS-BENE-STD-FREE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_BENE_STD_FREE");
        cis_Bene_File_01_Cis_Bene_Estate = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Estate", "CIS-BENE-ESTATE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_BENE_ESTATE");
        cis_Bene_File_01_Cis_Bene_Trust = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Trust", "CIS-BENE-TRUST", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_BENE_TRUST");
        cis_Bene_File_01_Cis_Bene_Category = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Category", "CIS-BENE-CATEGORY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_BENE_CATEGORY");
        cis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn", "CIS-PRMRY-BENE-CHILD-PRVSN", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_PRMRY_BENE_CHILD_PRVSN");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn", 
            "CIS-CNTGNT-BENE-CHILD-PRVSN", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CNTGNT_BENE_CHILD_PRVSN");
        cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta = vw_cis_Bene_File_01.getRecord().newGroupInGroup("cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta", "CIS-CLCLTN-BNFCRY-DTA");
        cis_Bene_File_01_Cis_Clcltn_Bene_Dod = cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_01_Cis_Clcltn_Bene_Dod", "CIS-CLCLTN-BENE-DOD", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_DOD");
        cis_Bene_File_01_Cis_Clcltn_Bene_Crossover = cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_01_Cis_Clcltn_Bene_Crossover", 
            "CIS-CLCLTN-BENE-CROSSOVER", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_CROSSOVER");
        cis_Bene_File_01_Cis_Clcltn_Bene_Name = cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_01_Cis_Clcltn_Bene_Name", "CIS-CLCLTN-BENE-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_NAME");
        cis_Bene_File_01_Cis_Clcltn_Bene_Rltnshp_Cde = cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_01_Cis_Clcltn_Bene_Rltnshp_Cde", 
            "CIS-CLCLTN-BENE-RLTNSHP-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_RLTNSHP_CDE");
        cis_Bene_File_01_Cis_Clcltn_Bene_Ssn_Nbr = cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_01_Cis_Clcltn_Bene_Ssn_Nbr", 
            "CIS-CLCLTN-BENE-SSN-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_SSN_NBR");
        cis_Bene_File_01_Cis_Clcltn_Bene_Dob = cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_01_Cis_Clcltn_Bene_Dob", "CIS-CLCLTN-BENE-DOB", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_DOB");
        cis_Bene_File_01_Cis_Clcltn_Bene_Sex_Cde = cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_01_Cis_Clcltn_Bene_Sex_Cde", 
            "CIS-CLCLTN-BENE-SEX-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_SEX_CDE");
        cis_Bene_File_01_Cis_Clcltn_Bene_Calc_Method = cis_Bene_File_01_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_01_Cis_Clcltn_Bene_Calc_Method", 
            "CIS-CLCLTN-BENE-CALC-METHOD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_CALC_METHOD");
        cis_Bene_File_01_Count_Castcis_Prmry_Bnfcry_Dta = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Count_Castcis_Prmry_Bnfcry_Dta", 
            "C*CIS-PRMRY-BNFCRY-DTA", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta = vw_cis_Bene_File_01.getRecord().newGroupInGroup("cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta", "CIS-PRMRY-BNFCRY-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Std_Txt = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Std_Txt", 
            "CIS-PRMRY-BENE-STD-TXT", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_STD_TXT", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Lump_Sum = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Lump_Sum", 
            "CIS-PRMRY-BENE-LUMP-SUM", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_LUMP_SUM", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Auto_Cmnt_Val = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Auto_Cmnt_Val", 
            "CIS-PRMRY-BENE-AUTO-CMNT-VAL", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_AUTO_CMNT_VAL", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Name = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Name", "CIS-PRMRY-BENE-NAME", 
            FieldType.STRING, 35, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_NAME", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Rltn = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Rltn", "CIS-PRMRY-BENE-RLTN", 
            FieldType.STRING, 10, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_RLTN", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde", 
            "CIS-PRMRY-BENE-RLTN-CDE", FieldType.STRING, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_RLTN_CDE", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr", 
            "CIS-PRMRY-BENE-SSN-NBR", FieldType.NUMERIC, 9, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_SSN_NBR", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Dob = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Dob", "CIS-PRMRY-BENE-DOB", 
            FieldType.DATE, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_DOB", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Allctn_Pct = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Allctn_Pct", 
            "CIS-PRMRY-BENE-ALLCTN-PCT", FieldType.PACKED_DECIMAL, 5, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_ALLCTN_PCT", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr", 
            "CIS-PRMRY-BENE-NMRTR-NBR", FieldType.NUMERIC, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_NMRTR_NBR", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr", 
            "CIS-PRMRY-BENE-DNMNTR-NBR", FieldType.NUMERIC, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_DNMNTR_NBR", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Spcl_TxtMuGroup = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newGroupInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Spcl_TxtMuGroup", 
            "CIS_PRMRY_BENE_SPCL_TXTMuGroup", RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_BENE_FILE_CIS_PRMRY_BENE_SPCL_TXT", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt = cis_Bene_File_01_Cis_Prmry_Bene_Spcl_TxtMuGroup.newFieldArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt", 
            "CIS-PRMRY-BENE-SPCL-TXT", FieldType.STRING, 72, new DbsArrayController(1,20,1,3) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_PRMRY_BENE_SPCL_TXT", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Count_Castcis_Cntgnt_Bnfcry_Dta = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Count_Castcis_Cntgnt_Bnfcry_Dta", 
            "C*CIS-CNTGNT-BNFCRY-DTA", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta = vw_cis_Bene_File_01.getRecord().newGroupInGroup("cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta", "CIS-CNTGNT-BNFCRY-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Std_Txt = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Std_Txt", 
            "CIS-CNTGNT-BENE-STD-TXT", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_STD_TXT", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Lump_Sum = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Lump_Sum", 
            "CIS-CNTGNT-BENE-LUMP-SUM", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_LUMP_SUM", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Auto_Cmnt_Val = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Auto_Cmnt_Val", 
            "CIS-CNTGNT-BENE-AUTO-CMNT-VAL", FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_AUTO_CMNT_VAL", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Name = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Name", "CIS-CNTGNT-BENE-NAME", 
            FieldType.STRING, 35, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_NAME", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Rltn = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Rltn", "CIS-CNTGNT-BENE-RLTN", 
            FieldType.STRING, 10, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_RLTN", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde", 
            "CIS-CNTGNT-BENE-RLTN-CDE", FieldType.STRING, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_RLTN_CDE", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr", 
            "CIS-CNTGNT-BENE-SSN-NBR", FieldType.NUMERIC, 9, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_SSN_NBR", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Dob = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Dob", "CIS-CNTGNT-BENE-DOB", 
            FieldType.DATE, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_DOB", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Allctn_Pct = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Allctn_Pct", 
            "CIS-CNTGNT-BENE-ALLCTN-PCT", FieldType.PACKED_DECIMAL, 5, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_CNTGNT_BENE_ALLCTN_PCT", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr", 
            "CIS-CNTGNT-BENE-NMRTR-NBR", FieldType.NUMERIC, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_NMRTR_NBR", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr", 
            "CIS-CNTGNT-BENE-DNMNTR-NBR", FieldType.NUMERIC, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_DNMNTR_NBR", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_TxtMuGroup = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newGroupInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_TxtMuGroup", 
            "CIS_CNTGNT_BENE_SPCL_TXTMuGroup", RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_BENE_FILE_CIS_CNTGNT_BENE_SPCL_TXT", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt = cis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_TxtMuGroup.newFieldArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt", 
            "CIS-CNTGNT-BENE-SPCL-TXT", FieldType.STRING, 72, new DbsArrayController(1,20,1,3) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_CNTGNT_BENE_SPCL_TXT", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");

        pnd_Cis_Ben_Super_1 = newFieldInRecord("pnd_Cis_Ben_Super_1", "#CIS-BEN-SUPER-1", FieldType.STRING, 31);
        pnd_Cis_Ben_Super_1Redef1 = newGroupInRecord("pnd_Cis_Ben_Super_1Redef1", "Redefines", pnd_Cis_Ben_Super_1);
        pnd_Cis_Ben_Super_1_Pnd_S_Cis_Rcrd_Type_Cde = pnd_Cis_Ben_Super_1Redef1.newFieldInGroup("pnd_Cis_Ben_Super_1_Pnd_S_Cis_Rcrd_Type_Cde", "#S-CIS-RCRD-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Rqst_Id = pnd_Cis_Ben_Super_1Redef1.newFieldInGroup("pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Rqst_Id", "#S-CIS-BENE-RQST-ID", 
            FieldType.STRING, 8);
        pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Unique_Id_Nbr = pnd_Cis_Ben_Super_1Redef1.newFieldInGroup("pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Unique_Id_Nbr", 
            "#S-CIS-BENE-UNIQUE-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Tiaa_Nbr = pnd_Cis_Ben_Super_1Redef1.newFieldInGroup("pnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Tiaa_Nbr", "#S-CIS-BENE-TIAA-NBR", 
            FieldType.STRING, 10);
        vw_cis_Bene_File_01.setUniquePeList();

        this.setRecordName("LdaCisl5000");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cis_Bene_File_01.reset();
    }

    // Constructor
    public LdaCisl5000() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
