/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:21:50 PM
**        * FROM NATURAL PDA     : MDMA371
************************************************************
**        * FILE NAME            : PdaMdma371.java
**        * CLASS NAME           : PdaMdma371
**        * INSTANCE NAME        : PdaMdma371
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaMdma371 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Mdma371;
    private DbsField pnd_Mdma371_Pnd_Input_Section;
    private DbsGroup pnd_Mdma371_Pnd_Input_SectionRedef1;
    private DbsField pnd_Mdma371_Pnd_Requestor;
    private DbsField pnd_Mdma371_Pnd_Function_Code;
    private DbsField pnd_Mdma371_Pnd_Pin_A7;
    private DbsGroup pnd_Mdma371_Pnd_Pin_A7Redef2;
    private DbsField pnd_Mdma371_Pnd_Pin;
    private DbsField pnd_Mdma371_Pnd_Record_Type;
    private DbsField pnd_Mdma371_Pnd_Soc_Sec_Nbr_A9;
    private DbsGroup pnd_Mdma371_Pnd_Soc_Sec_Nbr_A9Redef3;
    private DbsField pnd_Mdma371_Pnd_Soc_Sec_Nbr;
    private DbsField pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9;
    private DbsGroup pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9Redef4;
    private DbsField pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr;
    private DbsField pnd_Mdma371_Pnd_Negative_Election_Code;
    private DbsField pnd_Mdma371_Pnd_Date_Of_Birth;
    private DbsField pnd_Mdma371_Pnd_Date_Of_Death;
    private DbsField pnd_Mdma371_Pnd_Sex_Code;
    private DbsField pnd_Mdma371_Pnd_Occupation;
    private DbsField pnd_Mdma371_Pnd_Prefix;
    private DbsField pnd_Mdma371_Pnd_Last_Name;
    private DbsGroup pnd_Mdma371_Pnd_Last_NameRedef5;
    private DbsField pnd_Mdma371_Pnd_Last_Name_Char;
    private DbsField pnd_Mdma371_Pnd_First_Name;
    private DbsGroup pnd_Mdma371_Pnd_First_NameRedef6;
    private DbsField pnd_Mdma371_Pnd_First_Name_Char;
    private DbsField pnd_Mdma371_Pnd_Middle_Name;
    private DbsGroup pnd_Mdma371_Pnd_Middle_NameRedef7;
    private DbsField pnd_Mdma371_Pnd_Middle_Name_Char;
    private DbsField pnd_Mdma371_Pnd_Suffix;
    private DbsField pnd_Mdma371_Pnd_Tlc_Table_Count;
    private DbsField pnd_Mdma371_Pnd_Tlc_Category;
    private DbsField pnd_Mdma371_Pnd_Tlc_Area_Of_Origin;
    private DbsField pnd_Mdma371_Pnd_Mail_Table_Count;
    private DbsField pnd_Mdma371_Pnd_Mail_Code;
    private DbsField pnd_Mdma371_Pnd_Mail_Area_Of_Origin;
    private DbsField pnd_Mdma371_Pnd_Institution;
    private DbsField pnd_Mdma371_Pnd_Institution_Last_Date;
    private DbsField pnd_Mdma371_Pnd_Institution_Ind;
    private DbsField pnd_Mdma371_Pnd_Institution_Paidup_Ind;
    private DbsField pnd_Mdma371_Pnd_Institution_Number_Range_Code;
    private DbsField pnd_Mdma371_Pnd_Mf_Ownr_Cde;
    private DbsField pnd_Mdma371_Pnd_Ph_Universal_Data;
    private DbsField pnd_Mdma371_Pnd_Ssn_Or_Tin_Ind;
    private DbsField pnd_Mdma371_Pnd_Contract_Table_Count;
    private DbsField pnd_Mdma371_Pnd_Contract;
    private DbsGroup pnd_Mdma371_Pnd_ContractRedef8;
    private DbsGroup pnd_Mdma371_Pnd_Contract_Data;
    private DbsField pnd_Mdma371_Pnd_Contract_Char;
    private DbsGroup pnd_Mdma371_Pnd_ContractRedef9;
    private DbsGroup pnd_Mdma371_Pnd_Contract_Data_8;
    private DbsField pnd_Mdma371_Pnd_Contract_8;
    private DbsGroup pnd_Mdma371_Pnd_Contract_8Redef10;
    private DbsField pnd_Mdma371_Pnd_Contract_Base;
    private DbsGroup pnd_Mdma371_Pnd_Contract_BaseRedef11;
    private DbsField pnd_Mdma371_Pnd_Contract_Prefix_1;
    private DbsField pnd_Mdma371_Pnd_Contract_Nbr_1;
    private DbsGroup pnd_Mdma371_Pnd_Contract_BaseRedef12;
    private DbsField pnd_Mdma371_Pnd_Contract_Prefix_2;
    private DbsField pnd_Mdma371_Pnd_Contract_Nbr_2;
    private DbsField pnd_Mdma371_Pnd_Contract_Digit;
    private DbsField pnd_Mdma371_Pnd_Contract_Filler;
    private DbsField pnd_Mdma371_Pnd_Issue_Date;
    private DbsField pnd_Mdma371_Pnd_Status_Code;
    private DbsField pnd_Mdma371_Pnd_Status_Year;
    private DbsField pnd_Mdma371_Pnd_Payee_Code;
    private DbsField pnd_Mdma371_Pnd_Cref_Contract;
    private DbsGroup pnd_Mdma371_Pnd_Cref_ContractRedef13;
    private DbsGroup pnd_Mdma371_Pnd_Cref_Contract_Data;
    private DbsField pnd_Mdma371_Pnd_Cref_Prefix;
    private DbsField pnd_Mdma371_Pnd_Cref_Nbr;
    private DbsField pnd_Mdma371_Pnd_Cref_Digit;
    private DbsField pnd_Mdma371_Pnd_Cref_Filler;
    private DbsField pnd_Mdma371_Pnd_Cref_Issued_Ind;
    private DbsField pnd_Mdma371_Pnd_Da_Ownership_Code;
    private DbsField pnd_Mdma371_Pnd_Ia_Product_Code;
    private DbsField pnd_Mdma371_Pnd_Ia_Option_Code;
    private DbsField pnd_Mdma371_Pnd_Ia_Tax_Id;
    private DbsField pnd_Mdma371_Pnd_Ins_Plan_Code;
    private DbsField pnd_Mdma371_Pnd_Mf_Social_Cde;
    private DbsField pnd_Mdma371_Pnd_Mf_Invstr_Nbr;
    private DbsField pnd_Mdma371_Pnd_Cntrct_Universal_Data;
    private DbsField pnd_Mdma371_Pnd_Decedent_Contract;
    private DbsField pnd_Mdma371_Pnd_Acceptance_Ind;
    private DbsField pnd_Mdma371_Pnd_Relationship_To_Decedent;
    private DbsField pnd_Mdma371_Pnd_T_Pin;
    private DbsField pnd_Mdma371_Pnd_T_Record_Type;
    private DbsField pnd_Mdma371_Pnd_T_Soc_Sec_Nbr;
    private DbsField pnd_Mdma371_Pnd_T_Foreign_Soc_Sec_Nbr;
    private DbsField pnd_Mdma371_Pnd_T_Negative_Election_Code;
    private DbsField pnd_Mdma371_Pnd_T_Date_Of_Birth;
    private DbsField pnd_Mdma371_Pnd_T_Date_Of_Death;
    private DbsField pnd_Mdma371_Pnd_T_Sex_Code;
    private DbsField pnd_Mdma371_Pnd_T_Occupation;
    private DbsField pnd_Mdma371_Pnd_T_Prefix;
    private DbsField pnd_Mdma371_Pnd_T_Last_Name;
    private DbsGroup pnd_Mdma371_Pnd_T_Last_NameRedef14;
    private DbsField pnd_Mdma371_Pnd_T_Last_Name_Char;
    private DbsField pnd_Mdma371_Pnd_T_First_Name;
    private DbsGroup pnd_Mdma371_Pnd_T_First_NameRedef15;
    private DbsField pnd_Mdma371_Pnd_T_First_Name_Char;
    private DbsField pnd_Mdma371_Pnd_T_Middle_Name;
    private DbsGroup pnd_Mdma371_Pnd_T_Middle_NameRedef16;
    private DbsField pnd_Mdma371_Pnd_T_Middle_Name_Char;
    private DbsField pnd_Mdma371_Pnd_T_Suffix;
    private DbsField pnd_Mdma371_Pnd_T_Tlc_Table_Count;
    private DbsField pnd_Mdma371_Pnd_T_Tlc_Category;
    private DbsField pnd_Mdma371_Pnd_T_Tlc_Area_Of_Origin;
    private DbsField pnd_Mdma371_Pnd_T_Mail_Table_Count;
    private DbsField pnd_Mdma371_Pnd_T_Mail_Code;
    private DbsField pnd_Mdma371_Pnd_T_Mail_Area_Of_Origin;
    private DbsField pnd_Mdma371_Pnd_T_Institution;
    private DbsField pnd_Mdma371_Pnd_T_Institution_Last_Date;
    private DbsField pnd_Mdma371_Pnd_T_Institution_Ind;
    private DbsField pnd_Mdma371_Pnd_T_Institution_Paidup_Ind;
    private DbsField pnd_Mdma371_Pnd_T_Institution_Number_Range_Code;
    private DbsField pnd_Mdma371_Pnd_T_Mf_Ownr_Cde;
    private DbsField pnd_Mdma371_Pnd_T_Universal_Data;
    private DbsField pnd_Mdma371_Pnd_Continuation_Code;
    private DbsGroup pnd_Mdma371_Pnd_Continuation_CodeRedef17;
    private DbsField pnd_Mdma371_Pnd_Continuation_Order;
    private DbsField pnd_Mdma371_Pnd_Continuation_End;
    private DbsField pnd_Mdma371_Pnd_Pin_A12;
    private DbsGroup pnd_Mdma371_Pnd_Pin_A12Redef18;
    private DbsField pnd_Mdma371_Pnd_Pin_N12;
    private DbsGroup pnd_Mdma371_Pnd_O_Return_Section;
    private DbsField pnd_Mdma371_Pnd_Return_Code;
    private DbsField pnd_Mdma371_Pnd_Return_Text;
    private DbsField pnd_Mdma371_Pnd_O_Data_Section;
    private DbsGroup pnd_Mdma371_Pnd_O_Data_SectionRedef19;
    private DbsField pnd_Mdma371_Pnd_New_Pin;
    private DbsField pnd_Mdma371_Pnd_New_Record_Type;
    private DbsField pnd_Mdma371_Pnd_Current_Policyholder_Count;
    private DbsField pnd_Mdma371_Pnd_Current_Pin;
    private DbsField pnd_Mdma371_Pnd_Current_Record_Type;
    private DbsField pnd_Mdma371_Pnd_Current_Soc_Sec_Nbr;
    private DbsField pnd_Mdma371_Pnd_Current_Prefix;
    private DbsField pnd_Mdma371_Pnd_Current_Last_Name;
    private DbsField pnd_Mdma371_Pnd_Current_First_Name;
    private DbsField pnd_Mdma371_Pnd_Current_Middle_Name;
    private DbsField pnd_Mdma371_Pnd_Current_Suffix;
    private DbsField pnd_Mdma371_Pnd_Current_Date_Of_Birth;
    private DbsField pnd_Mdma371_Pnd_Current_Sex_Code;
    private DbsField pnd_Mdma371_Pnd_Contract_Error_Occurrence;

    public DbsGroup getPnd_Mdma371() { return pnd_Mdma371; }

    public DbsField getPnd_Mdma371_Pnd_Input_Section() { return pnd_Mdma371_Pnd_Input_Section; }

    public DbsGroup getPnd_Mdma371_Pnd_Input_SectionRedef1() { return pnd_Mdma371_Pnd_Input_SectionRedef1; }

    public DbsField getPnd_Mdma371_Pnd_Requestor() { return pnd_Mdma371_Pnd_Requestor; }

    public DbsField getPnd_Mdma371_Pnd_Function_Code() { return pnd_Mdma371_Pnd_Function_Code; }

    public DbsField getPnd_Mdma371_Pnd_Pin_A7() { return pnd_Mdma371_Pnd_Pin_A7; }

    public DbsGroup getPnd_Mdma371_Pnd_Pin_A7Redef2() { return pnd_Mdma371_Pnd_Pin_A7Redef2; }

    public DbsField getPnd_Mdma371_Pnd_Pin() { return pnd_Mdma371_Pnd_Pin; }

    public DbsField getPnd_Mdma371_Pnd_Record_Type() { return pnd_Mdma371_Pnd_Record_Type; }

    public DbsField getPnd_Mdma371_Pnd_Soc_Sec_Nbr_A9() { return pnd_Mdma371_Pnd_Soc_Sec_Nbr_A9; }

    public DbsGroup getPnd_Mdma371_Pnd_Soc_Sec_Nbr_A9Redef3() { return pnd_Mdma371_Pnd_Soc_Sec_Nbr_A9Redef3; }

    public DbsField getPnd_Mdma371_Pnd_Soc_Sec_Nbr() { return pnd_Mdma371_Pnd_Soc_Sec_Nbr; }

    public DbsField getPnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9() { return pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9; }

    public DbsGroup getPnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9Redef4() { return pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9Redef4; }

    public DbsField getPnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr() { return pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr; }

    public DbsField getPnd_Mdma371_Pnd_Negative_Election_Code() { return pnd_Mdma371_Pnd_Negative_Election_Code; }

    public DbsField getPnd_Mdma371_Pnd_Date_Of_Birth() { return pnd_Mdma371_Pnd_Date_Of_Birth; }

    public DbsField getPnd_Mdma371_Pnd_Date_Of_Death() { return pnd_Mdma371_Pnd_Date_Of_Death; }

    public DbsField getPnd_Mdma371_Pnd_Sex_Code() { return pnd_Mdma371_Pnd_Sex_Code; }

    public DbsField getPnd_Mdma371_Pnd_Occupation() { return pnd_Mdma371_Pnd_Occupation; }

    public DbsField getPnd_Mdma371_Pnd_Prefix() { return pnd_Mdma371_Pnd_Prefix; }

    public DbsField getPnd_Mdma371_Pnd_Last_Name() { return pnd_Mdma371_Pnd_Last_Name; }

    public DbsGroup getPnd_Mdma371_Pnd_Last_NameRedef5() { return pnd_Mdma371_Pnd_Last_NameRedef5; }

    public DbsField getPnd_Mdma371_Pnd_Last_Name_Char() { return pnd_Mdma371_Pnd_Last_Name_Char; }

    public DbsField getPnd_Mdma371_Pnd_First_Name() { return pnd_Mdma371_Pnd_First_Name; }

    public DbsGroup getPnd_Mdma371_Pnd_First_NameRedef6() { return pnd_Mdma371_Pnd_First_NameRedef6; }

    public DbsField getPnd_Mdma371_Pnd_First_Name_Char() { return pnd_Mdma371_Pnd_First_Name_Char; }

    public DbsField getPnd_Mdma371_Pnd_Middle_Name() { return pnd_Mdma371_Pnd_Middle_Name; }

    public DbsGroup getPnd_Mdma371_Pnd_Middle_NameRedef7() { return pnd_Mdma371_Pnd_Middle_NameRedef7; }

    public DbsField getPnd_Mdma371_Pnd_Middle_Name_Char() { return pnd_Mdma371_Pnd_Middle_Name_Char; }

    public DbsField getPnd_Mdma371_Pnd_Suffix() { return pnd_Mdma371_Pnd_Suffix; }

    public DbsField getPnd_Mdma371_Pnd_Tlc_Table_Count() { return pnd_Mdma371_Pnd_Tlc_Table_Count; }

    public DbsField getPnd_Mdma371_Pnd_Tlc_Category() { return pnd_Mdma371_Pnd_Tlc_Category; }

    public DbsField getPnd_Mdma371_Pnd_Tlc_Area_Of_Origin() { return pnd_Mdma371_Pnd_Tlc_Area_Of_Origin; }

    public DbsField getPnd_Mdma371_Pnd_Mail_Table_Count() { return pnd_Mdma371_Pnd_Mail_Table_Count; }

    public DbsField getPnd_Mdma371_Pnd_Mail_Code() { return pnd_Mdma371_Pnd_Mail_Code; }

    public DbsField getPnd_Mdma371_Pnd_Mail_Area_Of_Origin() { return pnd_Mdma371_Pnd_Mail_Area_Of_Origin; }

    public DbsField getPnd_Mdma371_Pnd_Institution() { return pnd_Mdma371_Pnd_Institution; }

    public DbsField getPnd_Mdma371_Pnd_Institution_Last_Date() { return pnd_Mdma371_Pnd_Institution_Last_Date; }

    public DbsField getPnd_Mdma371_Pnd_Institution_Ind() { return pnd_Mdma371_Pnd_Institution_Ind; }

    public DbsField getPnd_Mdma371_Pnd_Institution_Paidup_Ind() { return pnd_Mdma371_Pnd_Institution_Paidup_Ind; }

    public DbsField getPnd_Mdma371_Pnd_Institution_Number_Range_Code() { return pnd_Mdma371_Pnd_Institution_Number_Range_Code; }

    public DbsField getPnd_Mdma371_Pnd_Mf_Ownr_Cde() { return pnd_Mdma371_Pnd_Mf_Ownr_Cde; }

    public DbsField getPnd_Mdma371_Pnd_Ph_Universal_Data() { return pnd_Mdma371_Pnd_Ph_Universal_Data; }

    public DbsField getPnd_Mdma371_Pnd_Ssn_Or_Tin_Ind() { return pnd_Mdma371_Pnd_Ssn_Or_Tin_Ind; }

    public DbsField getPnd_Mdma371_Pnd_Contract_Table_Count() { return pnd_Mdma371_Pnd_Contract_Table_Count; }

    public DbsField getPnd_Mdma371_Pnd_Contract() { return pnd_Mdma371_Pnd_Contract; }

    public DbsGroup getPnd_Mdma371_Pnd_ContractRedef8() { return pnd_Mdma371_Pnd_ContractRedef8; }

    public DbsGroup getPnd_Mdma371_Pnd_Contract_Data() { return pnd_Mdma371_Pnd_Contract_Data; }

    public DbsField getPnd_Mdma371_Pnd_Contract_Char() { return pnd_Mdma371_Pnd_Contract_Char; }

    public DbsGroup getPnd_Mdma371_Pnd_ContractRedef9() { return pnd_Mdma371_Pnd_ContractRedef9; }

    public DbsGroup getPnd_Mdma371_Pnd_Contract_Data_8() { return pnd_Mdma371_Pnd_Contract_Data_8; }

    public DbsField getPnd_Mdma371_Pnd_Contract_8() { return pnd_Mdma371_Pnd_Contract_8; }

    public DbsGroup getPnd_Mdma371_Pnd_Contract_8Redef10() { return pnd_Mdma371_Pnd_Contract_8Redef10; }

    public DbsField getPnd_Mdma371_Pnd_Contract_Base() { return pnd_Mdma371_Pnd_Contract_Base; }

    public DbsGroup getPnd_Mdma371_Pnd_Contract_BaseRedef11() { return pnd_Mdma371_Pnd_Contract_BaseRedef11; }

    public DbsField getPnd_Mdma371_Pnd_Contract_Prefix_1() { return pnd_Mdma371_Pnd_Contract_Prefix_1; }

    public DbsField getPnd_Mdma371_Pnd_Contract_Nbr_1() { return pnd_Mdma371_Pnd_Contract_Nbr_1; }

    public DbsGroup getPnd_Mdma371_Pnd_Contract_BaseRedef12() { return pnd_Mdma371_Pnd_Contract_BaseRedef12; }

    public DbsField getPnd_Mdma371_Pnd_Contract_Prefix_2() { return pnd_Mdma371_Pnd_Contract_Prefix_2; }

    public DbsField getPnd_Mdma371_Pnd_Contract_Nbr_2() { return pnd_Mdma371_Pnd_Contract_Nbr_2; }

    public DbsField getPnd_Mdma371_Pnd_Contract_Digit() { return pnd_Mdma371_Pnd_Contract_Digit; }

    public DbsField getPnd_Mdma371_Pnd_Contract_Filler() { return pnd_Mdma371_Pnd_Contract_Filler; }

    public DbsField getPnd_Mdma371_Pnd_Issue_Date() { return pnd_Mdma371_Pnd_Issue_Date; }

    public DbsField getPnd_Mdma371_Pnd_Status_Code() { return pnd_Mdma371_Pnd_Status_Code; }

    public DbsField getPnd_Mdma371_Pnd_Status_Year() { return pnd_Mdma371_Pnd_Status_Year; }

    public DbsField getPnd_Mdma371_Pnd_Payee_Code() { return pnd_Mdma371_Pnd_Payee_Code; }

    public DbsField getPnd_Mdma371_Pnd_Cref_Contract() { return pnd_Mdma371_Pnd_Cref_Contract; }

    public DbsGroup getPnd_Mdma371_Pnd_Cref_ContractRedef13() { return pnd_Mdma371_Pnd_Cref_ContractRedef13; }

    public DbsGroup getPnd_Mdma371_Pnd_Cref_Contract_Data() { return pnd_Mdma371_Pnd_Cref_Contract_Data; }

    public DbsField getPnd_Mdma371_Pnd_Cref_Prefix() { return pnd_Mdma371_Pnd_Cref_Prefix; }

    public DbsField getPnd_Mdma371_Pnd_Cref_Nbr() { return pnd_Mdma371_Pnd_Cref_Nbr; }

    public DbsField getPnd_Mdma371_Pnd_Cref_Digit() { return pnd_Mdma371_Pnd_Cref_Digit; }

    public DbsField getPnd_Mdma371_Pnd_Cref_Filler() { return pnd_Mdma371_Pnd_Cref_Filler; }

    public DbsField getPnd_Mdma371_Pnd_Cref_Issued_Ind() { return pnd_Mdma371_Pnd_Cref_Issued_Ind; }

    public DbsField getPnd_Mdma371_Pnd_Da_Ownership_Code() { return pnd_Mdma371_Pnd_Da_Ownership_Code; }

    public DbsField getPnd_Mdma371_Pnd_Ia_Product_Code() { return pnd_Mdma371_Pnd_Ia_Product_Code; }

    public DbsField getPnd_Mdma371_Pnd_Ia_Option_Code() { return pnd_Mdma371_Pnd_Ia_Option_Code; }

    public DbsField getPnd_Mdma371_Pnd_Ia_Tax_Id() { return pnd_Mdma371_Pnd_Ia_Tax_Id; }

    public DbsField getPnd_Mdma371_Pnd_Ins_Plan_Code() { return pnd_Mdma371_Pnd_Ins_Plan_Code; }

    public DbsField getPnd_Mdma371_Pnd_Mf_Social_Cde() { return pnd_Mdma371_Pnd_Mf_Social_Cde; }

    public DbsField getPnd_Mdma371_Pnd_Mf_Invstr_Nbr() { return pnd_Mdma371_Pnd_Mf_Invstr_Nbr; }

    public DbsField getPnd_Mdma371_Pnd_Cntrct_Universal_Data() { return pnd_Mdma371_Pnd_Cntrct_Universal_Data; }

    public DbsField getPnd_Mdma371_Pnd_Decedent_Contract() { return pnd_Mdma371_Pnd_Decedent_Contract; }

    public DbsField getPnd_Mdma371_Pnd_Acceptance_Ind() { return pnd_Mdma371_Pnd_Acceptance_Ind; }

    public DbsField getPnd_Mdma371_Pnd_Relationship_To_Decedent() { return pnd_Mdma371_Pnd_Relationship_To_Decedent; }

    public DbsField getPnd_Mdma371_Pnd_T_Pin() { return pnd_Mdma371_Pnd_T_Pin; }

    public DbsField getPnd_Mdma371_Pnd_T_Record_Type() { return pnd_Mdma371_Pnd_T_Record_Type; }

    public DbsField getPnd_Mdma371_Pnd_T_Soc_Sec_Nbr() { return pnd_Mdma371_Pnd_T_Soc_Sec_Nbr; }

    public DbsField getPnd_Mdma371_Pnd_T_Foreign_Soc_Sec_Nbr() { return pnd_Mdma371_Pnd_T_Foreign_Soc_Sec_Nbr; }

    public DbsField getPnd_Mdma371_Pnd_T_Negative_Election_Code() { return pnd_Mdma371_Pnd_T_Negative_Election_Code; }

    public DbsField getPnd_Mdma371_Pnd_T_Date_Of_Birth() { return pnd_Mdma371_Pnd_T_Date_Of_Birth; }

    public DbsField getPnd_Mdma371_Pnd_T_Date_Of_Death() { return pnd_Mdma371_Pnd_T_Date_Of_Death; }

    public DbsField getPnd_Mdma371_Pnd_T_Sex_Code() { return pnd_Mdma371_Pnd_T_Sex_Code; }

    public DbsField getPnd_Mdma371_Pnd_T_Occupation() { return pnd_Mdma371_Pnd_T_Occupation; }

    public DbsField getPnd_Mdma371_Pnd_T_Prefix() { return pnd_Mdma371_Pnd_T_Prefix; }

    public DbsField getPnd_Mdma371_Pnd_T_Last_Name() { return pnd_Mdma371_Pnd_T_Last_Name; }

    public DbsGroup getPnd_Mdma371_Pnd_T_Last_NameRedef14() { return pnd_Mdma371_Pnd_T_Last_NameRedef14; }

    public DbsField getPnd_Mdma371_Pnd_T_Last_Name_Char() { return pnd_Mdma371_Pnd_T_Last_Name_Char; }

    public DbsField getPnd_Mdma371_Pnd_T_First_Name() { return pnd_Mdma371_Pnd_T_First_Name; }

    public DbsGroup getPnd_Mdma371_Pnd_T_First_NameRedef15() { return pnd_Mdma371_Pnd_T_First_NameRedef15; }

    public DbsField getPnd_Mdma371_Pnd_T_First_Name_Char() { return pnd_Mdma371_Pnd_T_First_Name_Char; }

    public DbsField getPnd_Mdma371_Pnd_T_Middle_Name() { return pnd_Mdma371_Pnd_T_Middle_Name; }

    public DbsGroup getPnd_Mdma371_Pnd_T_Middle_NameRedef16() { return pnd_Mdma371_Pnd_T_Middle_NameRedef16; }

    public DbsField getPnd_Mdma371_Pnd_T_Middle_Name_Char() { return pnd_Mdma371_Pnd_T_Middle_Name_Char; }

    public DbsField getPnd_Mdma371_Pnd_T_Suffix() { return pnd_Mdma371_Pnd_T_Suffix; }

    public DbsField getPnd_Mdma371_Pnd_T_Tlc_Table_Count() { return pnd_Mdma371_Pnd_T_Tlc_Table_Count; }

    public DbsField getPnd_Mdma371_Pnd_T_Tlc_Category() { return pnd_Mdma371_Pnd_T_Tlc_Category; }

    public DbsField getPnd_Mdma371_Pnd_T_Tlc_Area_Of_Origin() { return pnd_Mdma371_Pnd_T_Tlc_Area_Of_Origin; }

    public DbsField getPnd_Mdma371_Pnd_T_Mail_Table_Count() { return pnd_Mdma371_Pnd_T_Mail_Table_Count; }

    public DbsField getPnd_Mdma371_Pnd_T_Mail_Code() { return pnd_Mdma371_Pnd_T_Mail_Code; }

    public DbsField getPnd_Mdma371_Pnd_T_Mail_Area_Of_Origin() { return pnd_Mdma371_Pnd_T_Mail_Area_Of_Origin; }

    public DbsField getPnd_Mdma371_Pnd_T_Institution() { return pnd_Mdma371_Pnd_T_Institution; }

    public DbsField getPnd_Mdma371_Pnd_T_Institution_Last_Date() { return pnd_Mdma371_Pnd_T_Institution_Last_Date; }

    public DbsField getPnd_Mdma371_Pnd_T_Institution_Ind() { return pnd_Mdma371_Pnd_T_Institution_Ind; }

    public DbsField getPnd_Mdma371_Pnd_T_Institution_Paidup_Ind() { return pnd_Mdma371_Pnd_T_Institution_Paidup_Ind; }

    public DbsField getPnd_Mdma371_Pnd_T_Institution_Number_Range_Code() { return pnd_Mdma371_Pnd_T_Institution_Number_Range_Code; }

    public DbsField getPnd_Mdma371_Pnd_T_Mf_Ownr_Cde() { return pnd_Mdma371_Pnd_T_Mf_Ownr_Cde; }

    public DbsField getPnd_Mdma371_Pnd_T_Universal_Data() { return pnd_Mdma371_Pnd_T_Universal_Data; }

    public DbsField getPnd_Mdma371_Pnd_Continuation_Code() { return pnd_Mdma371_Pnd_Continuation_Code; }

    public DbsGroup getPnd_Mdma371_Pnd_Continuation_CodeRedef17() { return pnd_Mdma371_Pnd_Continuation_CodeRedef17; }

    public DbsField getPnd_Mdma371_Pnd_Continuation_Order() { return pnd_Mdma371_Pnd_Continuation_Order; }

    public DbsField getPnd_Mdma371_Pnd_Continuation_End() { return pnd_Mdma371_Pnd_Continuation_End; }

    public DbsField getPnd_Mdma371_Pnd_Pin_A12() { return pnd_Mdma371_Pnd_Pin_A12; }

    public DbsGroup getPnd_Mdma371_Pnd_Pin_A12Redef18() { return pnd_Mdma371_Pnd_Pin_A12Redef18; }

    public DbsField getPnd_Mdma371_Pnd_Pin_N12() { return pnd_Mdma371_Pnd_Pin_N12; }

    public DbsGroup getPnd_Mdma371_Pnd_O_Return_Section() { return pnd_Mdma371_Pnd_O_Return_Section; }

    public DbsField getPnd_Mdma371_Pnd_Return_Code() { return pnd_Mdma371_Pnd_Return_Code; }

    public DbsField getPnd_Mdma371_Pnd_Return_Text() { return pnd_Mdma371_Pnd_Return_Text; }

    public DbsField getPnd_Mdma371_Pnd_O_Data_Section() { return pnd_Mdma371_Pnd_O_Data_Section; }

    public DbsGroup getPnd_Mdma371_Pnd_O_Data_SectionRedef19() { return pnd_Mdma371_Pnd_O_Data_SectionRedef19; }

    public DbsField getPnd_Mdma371_Pnd_New_Pin() { return pnd_Mdma371_Pnd_New_Pin; }

    public DbsField getPnd_Mdma371_Pnd_New_Record_Type() { return pnd_Mdma371_Pnd_New_Record_Type; }

    public DbsField getPnd_Mdma371_Pnd_Current_Policyholder_Count() { return pnd_Mdma371_Pnd_Current_Policyholder_Count; }

    public DbsField getPnd_Mdma371_Pnd_Current_Pin() { return pnd_Mdma371_Pnd_Current_Pin; }

    public DbsField getPnd_Mdma371_Pnd_Current_Record_Type() { return pnd_Mdma371_Pnd_Current_Record_Type; }

    public DbsField getPnd_Mdma371_Pnd_Current_Soc_Sec_Nbr() { return pnd_Mdma371_Pnd_Current_Soc_Sec_Nbr; }

    public DbsField getPnd_Mdma371_Pnd_Current_Prefix() { return pnd_Mdma371_Pnd_Current_Prefix; }

    public DbsField getPnd_Mdma371_Pnd_Current_Last_Name() { return pnd_Mdma371_Pnd_Current_Last_Name; }

    public DbsField getPnd_Mdma371_Pnd_Current_First_Name() { return pnd_Mdma371_Pnd_Current_First_Name; }

    public DbsField getPnd_Mdma371_Pnd_Current_Middle_Name() { return pnd_Mdma371_Pnd_Current_Middle_Name; }

    public DbsField getPnd_Mdma371_Pnd_Current_Suffix() { return pnd_Mdma371_Pnd_Current_Suffix; }

    public DbsField getPnd_Mdma371_Pnd_Current_Date_Of_Birth() { return pnd_Mdma371_Pnd_Current_Date_Of_Birth; }

    public DbsField getPnd_Mdma371_Pnd_Current_Sex_Code() { return pnd_Mdma371_Pnd_Current_Sex_Code; }

    public DbsField getPnd_Mdma371_Pnd_Contract_Error_Occurrence() { return pnd_Mdma371_Pnd_Contract_Error_Occurrence; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Mdma371 = dbsRecord.newGroupInRecord("pnd_Mdma371", "#MDMA371");
        pnd_Mdma371.setParameterOption(ParameterOption.ByReference);
        pnd_Mdma371_Pnd_Input_Section = pnd_Mdma371.newFieldArrayInGroup("pnd_Mdma371_Pnd_Input_Section", "#INPUT-SECTION", FieldType.STRING, 1, new DbsArrayController(1,
            5588));
        pnd_Mdma371_Pnd_Input_SectionRedef1 = pnd_Mdma371.newGroupInGroup("pnd_Mdma371_Pnd_Input_SectionRedef1", "Redefines", pnd_Mdma371_Pnd_Input_Section);
        pnd_Mdma371_Pnd_Requestor = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Requestor", "#REQUESTOR", FieldType.STRING, 8);
        pnd_Mdma371_Pnd_Function_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Function_Code", "#FUNCTION-CODE", FieldType.STRING, 
            3);
        pnd_Mdma371_Pnd_Pin_A7 = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Pin_A7", "#PIN-A7", FieldType.STRING, 7);
        pnd_Mdma371_Pnd_Pin_A7Redef2 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_Pin_A7Redef2", "Redefines", pnd_Mdma371_Pnd_Pin_A7);
        pnd_Mdma371_Pnd_Pin = pnd_Mdma371_Pnd_Pin_A7Redef2.newFieldInGroup("pnd_Mdma371_Pnd_Pin", "#PIN", FieldType.NUMERIC, 7);
        pnd_Mdma371_Pnd_Record_Type = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Record_Type", "#RECORD-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Mdma371_Pnd_Soc_Sec_Nbr_A9 = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Soc_Sec_Nbr_A9", "#SOC-SEC-NBR-A9", FieldType.STRING, 
            9);
        pnd_Mdma371_Pnd_Soc_Sec_Nbr_A9Redef3 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_Soc_Sec_Nbr_A9Redef3", "Redefines", 
            pnd_Mdma371_Pnd_Soc_Sec_Nbr_A9);
        pnd_Mdma371_Pnd_Soc_Sec_Nbr = pnd_Mdma371_Pnd_Soc_Sec_Nbr_A9Redef3.newFieldInGroup("pnd_Mdma371_Pnd_Soc_Sec_Nbr", "#SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9 = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9", "#FOREIGN-SOC-SEC-NBR-A9", 
            FieldType.STRING, 9);
        pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9Redef4 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9Redef4", 
            "Redefines", pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9);
        pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr = pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9Redef4.newFieldInGroup("pnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr", "#FOREIGN-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Mdma371_Pnd_Negative_Election_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Negative_Election_Code", "#NEGATIVE-ELECTION-CODE", 
            FieldType.NUMERIC, 3);
        pnd_Mdma371_Pnd_Date_Of_Birth = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Date_Of_Birth", "#DATE-OF-BIRTH", FieldType.NUMERIC, 
            8);
        pnd_Mdma371_Pnd_Date_Of_Death = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Date_Of_Death", "#DATE-OF-DEATH", FieldType.NUMERIC, 
            8);
        pnd_Mdma371_Pnd_Sex_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Sex_Code", "#SEX-CODE", FieldType.STRING, 1);
        pnd_Mdma371_Pnd_Occupation = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Occupation", "#OCCUPATION", FieldType.STRING, 
            10);
        pnd_Mdma371_Pnd_Prefix = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Prefix", "#PREFIX", FieldType.STRING, 8);
        pnd_Mdma371_Pnd_Last_Name = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 30);
        pnd_Mdma371_Pnd_Last_NameRedef5 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_Last_NameRedef5", "Redefines", pnd_Mdma371_Pnd_Last_Name);
        pnd_Mdma371_Pnd_Last_Name_Char = pnd_Mdma371_Pnd_Last_NameRedef5.newFieldArrayInGroup("pnd_Mdma371_Pnd_Last_Name_Char", "#LAST-NAME-CHAR", FieldType.STRING, 
            1, new DbsArrayController(1,30));
        pnd_Mdma371_Pnd_First_Name = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 
            30);
        pnd_Mdma371_Pnd_First_NameRedef6 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_First_NameRedef6", "Redefines", pnd_Mdma371_Pnd_First_Name);
        pnd_Mdma371_Pnd_First_Name_Char = pnd_Mdma371_Pnd_First_NameRedef6.newFieldArrayInGroup("pnd_Mdma371_Pnd_First_Name_Char", "#FIRST-NAME-CHAR", 
            FieldType.STRING, 1, new DbsArrayController(1,30));
        pnd_Mdma371_Pnd_Middle_Name = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Middle_Name", "#MIDDLE-NAME", FieldType.STRING, 
            30);
        pnd_Mdma371_Pnd_Middle_NameRedef7 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_Middle_NameRedef7", "Redefines", pnd_Mdma371_Pnd_Middle_Name);
        pnd_Mdma371_Pnd_Middle_Name_Char = pnd_Mdma371_Pnd_Middle_NameRedef7.newFieldArrayInGroup("pnd_Mdma371_Pnd_Middle_Name_Char", "#MIDDLE-NAME-CHAR", 
            FieldType.STRING, 1, new DbsArrayController(1,30));
        pnd_Mdma371_Pnd_Suffix = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Suffix", "#SUFFIX", FieldType.STRING, 8);
        pnd_Mdma371_Pnd_Tlc_Table_Count = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Tlc_Table_Count", "#TLC-TABLE-COUNT", FieldType.NUMERIC, 
            2);
        pnd_Mdma371_Pnd_Tlc_Category = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Tlc_Category", "#TLC-CATEGORY", FieldType.STRING, 
            2, new DbsArrayController(1,8));
        pnd_Mdma371_Pnd_Tlc_Area_Of_Origin = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Tlc_Area_Of_Origin", "#TLC-AREA-OF-ORIGIN", 
            FieldType.STRING, 3, new DbsArrayController(1,8));
        pnd_Mdma371_Pnd_Mail_Table_Count = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Mail_Table_Count", "#MAIL-TABLE-COUNT", 
            FieldType.NUMERIC, 2);
        pnd_Mdma371_Pnd_Mail_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Mail_Code", "#MAIL-CODE", FieldType.STRING, 
            2, new DbsArrayController(1,25));
        pnd_Mdma371_Pnd_Mail_Area_Of_Origin = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Mail_Area_Of_Origin", "#MAIL-AREA-OF-ORIGIN", 
            FieldType.STRING, 3, new DbsArrayController(1,25));
        pnd_Mdma371_Pnd_Institution = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Institution", "#INSTITUTION", FieldType.NUMERIC, 
            5);
        pnd_Mdma371_Pnd_Institution_Last_Date = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Institution_Last_Date", "#INSTITUTION-LAST-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Mdma371_Pnd_Institution_Ind = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Institution_Ind", "#INSTITUTION-IND", FieldType.STRING, 
            1);
        pnd_Mdma371_Pnd_Institution_Paidup_Ind = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Institution_Paidup_Ind", "#INSTITUTION-PAIDUP-IND", 
            FieldType.STRING, 1);
        pnd_Mdma371_Pnd_Institution_Number_Range_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Institution_Number_Range_Code", 
            "#INSTITUTION-NUMBER-RANGE-CODE", FieldType.STRING, 2);
        pnd_Mdma371_Pnd_Mf_Ownr_Cde = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Mf_Ownr_Cde", "#MF-OWNR-CDE", FieldType.STRING, 
            1);
        pnd_Mdma371_Pnd_Ph_Universal_Data = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Ph_Universal_Data", "#PH-UNIVERSAL-DATA", 
            FieldType.STRING, 150);
        pnd_Mdma371_Pnd_Ssn_Or_Tin_Ind = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Ssn_Or_Tin_Ind", "#SSN-OR-TIN-IND", FieldType.STRING, 
            1);
        pnd_Mdma371_Pnd_Contract_Table_Count = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Contract_Table_Count", "#CONTRACT-TABLE-COUNT", 
            FieldType.NUMERIC, 2);
        pnd_Mdma371_Pnd_Contract = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Contract", "#CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_ContractRedef8 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_ContractRedef8", "Redefines", pnd_Mdma371_Pnd_Contract);
        pnd_Mdma371_Pnd_Contract_Data = pnd_Mdma371_Pnd_ContractRedef8.newGroupArrayInGroup("pnd_Mdma371_Pnd_Contract_Data", "#CONTRACT-DATA", new DbsArrayController(1,
            20));
        pnd_Mdma371_Pnd_Contract_Char = pnd_Mdma371_Pnd_Contract_Data.newFieldArrayInGroup("pnd_Mdma371_Pnd_Contract_Char", "#CONTRACT-CHAR", FieldType.STRING, 
            1, new DbsArrayController(1,10));
        pnd_Mdma371_Pnd_ContractRedef9 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_ContractRedef9", "Redefines", pnd_Mdma371_Pnd_Contract);
        pnd_Mdma371_Pnd_Contract_Data_8 = pnd_Mdma371_Pnd_ContractRedef9.newGroupArrayInGroup("pnd_Mdma371_Pnd_Contract_Data_8", "#CONTRACT-DATA-8", new 
            DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Contract_8 = pnd_Mdma371_Pnd_Contract_Data_8.newFieldInGroup("pnd_Mdma371_Pnd_Contract_8", "#CONTRACT-8", FieldType.STRING, 8);
        pnd_Mdma371_Pnd_Contract_8Redef10 = pnd_Mdma371_Pnd_Contract_Data_8.newGroupInGroup("pnd_Mdma371_Pnd_Contract_8Redef10", "Redefines", pnd_Mdma371_Pnd_Contract_8);
        pnd_Mdma371_Pnd_Contract_Base = pnd_Mdma371_Pnd_Contract_8Redef10.newFieldInGroup("pnd_Mdma371_Pnd_Contract_Base", "#CONTRACT-BASE", FieldType.STRING, 
            7);
        pnd_Mdma371_Pnd_Contract_BaseRedef11 = pnd_Mdma371_Pnd_Contract_8Redef10.newGroupInGroup("pnd_Mdma371_Pnd_Contract_BaseRedef11", "Redefines", 
            pnd_Mdma371_Pnd_Contract_Base);
        pnd_Mdma371_Pnd_Contract_Prefix_1 = pnd_Mdma371_Pnd_Contract_BaseRedef11.newFieldInGroup("pnd_Mdma371_Pnd_Contract_Prefix_1", "#CONTRACT-PREFIX-1", 
            FieldType.STRING, 1);
        pnd_Mdma371_Pnd_Contract_Nbr_1 = pnd_Mdma371_Pnd_Contract_BaseRedef11.newFieldInGroup("pnd_Mdma371_Pnd_Contract_Nbr_1", "#CONTRACT-NBR-1", FieldType.STRING, 
            6);
        pnd_Mdma371_Pnd_Contract_BaseRedef12 = pnd_Mdma371_Pnd_Contract_8Redef10.newGroupInGroup("pnd_Mdma371_Pnd_Contract_BaseRedef12", "Redefines", 
            pnd_Mdma371_Pnd_Contract_Base);
        pnd_Mdma371_Pnd_Contract_Prefix_2 = pnd_Mdma371_Pnd_Contract_BaseRedef12.newFieldInGroup("pnd_Mdma371_Pnd_Contract_Prefix_2", "#CONTRACT-PREFIX-2", 
            FieldType.STRING, 2);
        pnd_Mdma371_Pnd_Contract_Nbr_2 = pnd_Mdma371_Pnd_Contract_BaseRedef12.newFieldInGroup("pnd_Mdma371_Pnd_Contract_Nbr_2", "#CONTRACT-NBR-2", FieldType.STRING, 
            5);
        pnd_Mdma371_Pnd_Contract_Digit = pnd_Mdma371_Pnd_Contract_8Redef10.newFieldInGroup("pnd_Mdma371_Pnd_Contract_Digit", "#CONTRACT-DIGIT", FieldType.STRING, 
            1);
        pnd_Mdma371_Pnd_Contract_Filler = pnd_Mdma371_Pnd_Contract_Data_8.newFieldInGroup("pnd_Mdma371_Pnd_Contract_Filler", "#CONTRACT-FILLER", FieldType.STRING, 
            2);
        pnd_Mdma371_Pnd_Issue_Date = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Issue_Date", "#ISSUE-DATE", FieldType.NUMERIC, 
            8, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Status_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Status_Code", "#STATUS-CODE", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Status_Year = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Status_Year", "#STATUS-YEAR", FieldType.NUMERIC, 
            4, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Payee_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Payee_Code", "#PAYEE-CODE", FieldType.STRING, 
            2, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Cref_Contract = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Cref_Contract", "#CREF-CONTRACT", FieldType.STRING, 
            10, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Cref_ContractRedef13 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_Cref_ContractRedef13", "Redefines", 
            pnd_Mdma371_Pnd_Cref_Contract);
        pnd_Mdma371_Pnd_Cref_Contract_Data = pnd_Mdma371_Pnd_Cref_ContractRedef13.newGroupArrayInGroup("pnd_Mdma371_Pnd_Cref_Contract_Data", "#CREF-CONTRACT-DATA", 
            new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Cref_Prefix = pnd_Mdma371_Pnd_Cref_Contract_Data.newFieldInGroup("pnd_Mdma371_Pnd_Cref_Prefix", "#CREF-PREFIX", FieldType.STRING, 
            1);
        pnd_Mdma371_Pnd_Cref_Nbr = pnd_Mdma371_Pnd_Cref_Contract_Data.newFieldInGroup("pnd_Mdma371_Pnd_Cref_Nbr", "#CREF-NBR", FieldType.STRING, 6);
        pnd_Mdma371_Pnd_Cref_Digit = pnd_Mdma371_Pnd_Cref_Contract_Data.newFieldInGroup("pnd_Mdma371_Pnd_Cref_Digit", "#CREF-DIGIT", FieldType.STRING, 
            1);
        pnd_Mdma371_Pnd_Cref_Filler = pnd_Mdma371_Pnd_Cref_Contract_Data.newFieldInGroup("pnd_Mdma371_Pnd_Cref_Filler", "#CREF-FILLER", FieldType.STRING, 
            2);
        pnd_Mdma371_Pnd_Cref_Issued_Ind = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Cref_Issued_Ind", "#CREF-ISSUED-IND", 
            FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Da_Ownership_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Da_Ownership_Code", "#DA-OWNERSHIP-CODE", 
            FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Ia_Product_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Ia_Product_Code", "#IA-PRODUCT-CODE", 
            FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Ia_Option_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Ia_Option_Code", "#IA-OPTION-CODE", 
            FieldType.STRING, 2, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Ia_Tax_Id = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Ia_Tax_Id", "#IA-TAX-ID", FieldType.NUMERIC, 
            9, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Ins_Plan_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Ins_Plan_Code", "#INS-PLAN-CODE", FieldType.STRING, 
            5, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Mf_Social_Cde = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Mf_Social_Cde", "#MF-SOCIAL-CDE", FieldType.STRING, 
            3, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Mf_Invstr_Nbr = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Mf_Invstr_Nbr", "#MF-INVSTR-NBR", FieldType.STRING, 
            9, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Cntrct_Universal_Data = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Cntrct_Universal_Data", "#CNTRCT-UNIVERSAL-DATA", 
            FieldType.STRING, 150, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Decedent_Contract = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Decedent_Contract", "#DECEDENT-CONTRACT", 
            FieldType.STRING, 10, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Acceptance_Ind = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Acceptance_Ind", "#ACCEPTANCE-IND", 
            FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_Relationship_To_Decedent = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_Relationship_To_Decedent", 
            "#RELATIONSHIP-TO-DECEDENT", FieldType.STRING, 1, new DbsArrayController(1,20));
        pnd_Mdma371_Pnd_T_Pin = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Pin", "#T-PIN", FieldType.NUMERIC, 7);
        pnd_Mdma371_Pnd_T_Record_Type = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Record_Type", "#T-RECORD-TYPE", FieldType.NUMERIC, 
            2);
        pnd_Mdma371_Pnd_T_Soc_Sec_Nbr = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Soc_Sec_Nbr", "#T-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Mdma371_Pnd_T_Foreign_Soc_Sec_Nbr = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Foreign_Soc_Sec_Nbr", "#T-FOREIGN-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Mdma371_Pnd_T_Negative_Election_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Negative_Election_Code", "#T-NEGATIVE-ELECTION-CODE", 
            FieldType.NUMERIC, 3);
        pnd_Mdma371_Pnd_T_Date_Of_Birth = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Date_Of_Birth", "#T-DATE-OF-BIRTH", FieldType.NUMERIC, 
            8);
        pnd_Mdma371_Pnd_T_Date_Of_Death = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Date_Of_Death", "#T-DATE-OF-DEATH", FieldType.NUMERIC, 
            8);
        pnd_Mdma371_Pnd_T_Sex_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Sex_Code", "#T-SEX-CODE", FieldType.STRING, 
            1);
        pnd_Mdma371_Pnd_T_Occupation = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Occupation", "#T-OCCUPATION", FieldType.STRING, 
            10);
        pnd_Mdma371_Pnd_T_Prefix = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Prefix", "#T-PREFIX", FieldType.STRING, 8);
        pnd_Mdma371_Pnd_T_Last_Name = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Last_Name", "#T-LAST-NAME", FieldType.STRING, 
            30);
        pnd_Mdma371_Pnd_T_Last_NameRedef14 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_T_Last_NameRedef14", "Redefines", pnd_Mdma371_Pnd_T_Last_Name);
        pnd_Mdma371_Pnd_T_Last_Name_Char = pnd_Mdma371_Pnd_T_Last_NameRedef14.newFieldArrayInGroup("pnd_Mdma371_Pnd_T_Last_Name_Char", "#T-LAST-NAME-CHAR", 
            FieldType.STRING, 1, new DbsArrayController(1,30));
        pnd_Mdma371_Pnd_T_First_Name = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_First_Name", "#T-FIRST-NAME", FieldType.STRING, 
            30);
        pnd_Mdma371_Pnd_T_First_NameRedef15 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_T_First_NameRedef15", "Redefines", 
            pnd_Mdma371_Pnd_T_First_Name);
        pnd_Mdma371_Pnd_T_First_Name_Char = pnd_Mdma371_Pnd_T_First_NameRedef15.newFieldArrayInGroup("pnd_Mdma371_Pnd_T_First_Name_Char", "#T-FIRST-NAME-CHAR", 
            FieldType.STRING, 1, new DbsArrayController(1,30));
        pnd_Mdma371_Pnd_T_Middle_Name = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Middle_Name", "#T-MIDDLE-NAME", FieldType.STRING, 
            30);
        pnd_Mdma371_Pnd_T_Middle_NameRedef16 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_T_Middle_NameRedef16", "Redefines", 
            pnd_Mdma371_Pnd_T_Middle_Name);
        pnd_Mdma371_Pnd_T_Middle_Name_Char = pnd_Mdma371_Pnd_T_Middle_NameRedef16.newFieldArrayInGroup("pnd_Mdma371_Pnd_T_Middle_Name_Char", "#T-MIDDLE-NAME-CHAR", 
            FieldType.STRING, 1, new DbsArrayController(1,30));
        pnd_Mdma371_Pnd_T_Suffix = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Suffix", "#T-SUFFIX", FieldType.STRING, 8);
        pnd_Mdma371_Pnd_T_Tlc_Table_Count = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Tlc_Table_Count", "#T-TLC-TABLE-COUNT", 
            FieldType.NUMERIC, 2);
        pnd_Mdma371_Pnd_T_Tlc_Category = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_T_Tlc_Category", "#T-TLC-CATEGORY", 
            FieldType.STRING, 2, new DbsArrayController(1,8));
        pnd_Mdma371_Pnd_T_Tlc_Area_Of_Origin = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_T_Tlc_Area_Of_Origin", "#T-TLC-AREA-OF-ORIGIN", 
            FieldType.STRING, 3, new DbsArrayController(1,8));
        pnd_Mdma371_Pnd_T_Mail_Table_Count = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Mail_Table_Count", "#T-MAIL-TABLE-COUNT", 
            FieldType.NUMERIC, 2);
        pnd_Mdma371_Pnd_T_Mail_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_T_Mail_Code", "#T-MAIL-CODE", FieldType.STRING, 
            2, new DbsArrayController(1,25));
        pnd_Mdma371_Pnd_T_Mail_Area_Of_Origin = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldArrayInGroup("pnd_Mdma371_Pnd_T_Mail_Area_Of_Origin", "#T-MAIL-AREA-OF-ORIGIN", 
            FieldType.STRING, 3, new DbsArrayController(1,25));
        pnd_Mdma371_Pnd_T_Institution = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Institution", "#T-INSTITUTION", FieldType.NUMERIC, 
            5);
        pnd_Mdma371_Pnd_T_Institution_Last_Date = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Institution_Last_Date", "#T-INSTITUTION-LAST-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Mdma371_Pnd_T_Institution_Ind = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Institution_Ind", "#T-INSTITUTION-IND", 
            FieldType.STRING, 1);
        pnd_Mdma371_Pnd_T_Institution_Paidup_Ind = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Institution_Paidup_Ind", "#T-INSTITUTION-PAIDUP-IND", 
            FieldType.STRING, 1);
        pnd_Mdma371_Pnd_T_Institution_Number_Range_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Institution_Number_Range_Code", 
            "#T-INSTITUTION-NUMBER-RANGE-CODE", FieldType.STRING, 2);
        pnd_Mdma371_Pnd_T_Mf_Ownr_Cde = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Mf_Ownr_Cde", "#T-MF-OWNR-CDE", FieldType.STRING, 
            1);
        pnd_Mdma371_Pnd_T_Universal_Data = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_T_Universal_Data", "#T-UNIVERSAL-DATA", 
            FieldType.STRING, 150);
        pnd_Mdma371_Pnd_Continuation_Code = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Continuation_Code", "#CONTINUATION-CODE", 
            FieldType.STRING, 2);
        pnd_Mdma371_Pnd_Continuation_CodeRedef17 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_Continuation_CodeRedef17", "Redefines", 
            pnd_Mdma371_Pnd_Continuation_Code);
        pnd_Mdma371_Pnd_Continuation_Order = pnd_Mdma371_Pnd_Continuation_CodeRedef17.newFieldInGroup("pnd_Mdma371_Pnd_Continuation_Order", "#CONTINUATION-ORDER", 
            FieldType.STRING, 1);
        pnd_Mdma371_Pnd_Continuation_End = pnd_Mdma371_Pnd_Continuation_CodeRedef17.newFieldInGroup("pnd_Mdma371_Pnd_Continuation_End", "#CONTINUATION-END", 
            FieldType.STRING, 1);
        pnd_Mdma371_Pnd_Pin_A12 = pnd_Mdma371_Pnd_Input_SectionRedef1.newFieldInGroup("pnd_Mdma371_Pnd_Pin_A12", "#PIN-A12", FieldType.STRING, 12);
        pnd_Mdma371_Pnd_Pin_A12Redef18 = pnd_Mdma371_Pnd_Input_SectionRedef1.newGroupInGroup("pnd_Mdma371_Pnd_Pin_A12Redef18", "Redefines", pnd_Mdma371_Pnd_Pin_A12);
        pnd_Mdma371_Pnd_Pin_N12 = pnd_Mdma371_Pnd_Pin_A12Redef18.newFieldInGroup("pnd_Mdma371_Pnd_Pin_N12", "#PIN-N12", FieldType.NUMERIC, 12);
        pnd_Mdma371_Pnd_O_Return_Section = pnd_Mdma371.newGroupInGroup("pnd_Mdma371_Pnd_O_Return_Section", "#O-RETURN-SECTION");
        pnd_Mdma371_Pnd_Return_Code = pnd_Mdma371_Pnd_O_Return_Section.newFieldInGroup("pnd_Mdma371_Pnd_Return_Code", "#RETURN-CODE", FieldType.NUMERIC, 
            4);
        pnd_Mdma371_Pnd_Return_Text = pnd_Mdma371_Pnd_O_Return_Section.newFieldInGroup("pnd_Mdma371_Pnd_Return_Text", "#RETURN-TEXT", FieldType.STRING, 
            79);
        pnd_Mdma371_Pnd_O_Data_Section = pnd_Mdma371.newFieldArrayInGroup("pnd_Mdma371_Pnd_O_Data_Section", "#O-DATA-SECTION", FieldType.STRING, 1, new 
            DbsArrayController(1,1398));
        pnd_Mdma371_Pnd_O_Data_SectionRedef19 = pnd_Mdma371.newGroupInGroup("pnd_Mdma371_Pnd_O_Data_SectionRedef19", "Redefines", pnd_Mdma371_Pnd_O_Data_Section);
        pnd_Mdma371_Pnd_New_Pin = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldInGroup("pnd_Mdma371_Pnd_New_Pin", "#NEW-PIN", FieldType.NUMERIC, 12);
        pnd_Mdma371_Pnd_New_Record_Type = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldInGroup("pnd_Mdma371_Pnd_New_Record_Type", "#NEW-RECORD-TYPE", 
            FieldType.NUMERIC, 2);
        pnd_Mdma371_Pnd_Current_Policyholder_Count = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldInGroup("pnd_Mdma371_Pnd_Current_Policyholder_Count", 
            "#CURRENT-POLICYHOLDER-COUNT", FieldType.NUMERIC, 2);
        pnd_Mdma371_Pnd_Current_Pin = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldArrayInGroup("pnd_Mdma371_Pnd_Current_Pin", "#CURRENT-PIN", FieldType.NUMERIC, 
            12, new DbsArrayController(1,10));
        pnd_Mdma371_Pnd_Current_Record_Type = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldArrayInGroup("pnd_Mdma371_Pnd_Current_Record_Type", "#CURRENT-RECORD-TYPE", 
            FieldType.NUMERIC, 2, new DbsArrayController(1,10));
        pnd_Mdma371_Pnd_Current_Soc_Sec_Nbr = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldArrayInGroup("pnd_Mdma371_Pnd_Current_Soc_Sec_Nbr", "#CURRENT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9, new DbsArrayController(1,10));
        pnd_Mdma371_Pnd_Current_Prefix = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldArrayInGroup("pnd_Mdma371_Pnd_Current_Prefix", "#CURRENT-PREFIX", 
            FieldType.STRING, 8, new DbsArrayController(1,10));
        pnd_Mdma371_Pnd_Current_Last_Name = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldArrayInGroup("pnd_Mdma371_Pnd_Current_Last_Name", "#CURRENT-LAST-NAME", 
            FieldType.STRING, 30, new DbsArrayController(1,10));
        pnd_Mdma371_Pnd_Current_First_Name = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldArrayInGroup("pnd_Mdma371_Pnd_Current_First_Name", "#CURRENT-FIRST-NAME", 
            FieldType.STRING, 30, new DbsArrayController(1,10));
        pnd_Mdma371_Pnd_Current_Middle_Name = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldArrayInGroup("pnd_Mdma371_Pnd_Current_Middle_Name", "#CURRENT-MIDDLE-NAME", 
            FieldType.STRING, 30, new DbsArrayController(1,10));
        pnd_Mdma371_Pnd_Current_Suffix = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldArrayInGroup("pnd_Mdma371_Pnd_Current_Suffix", "#CURRENT-SUFFIX", 
            FieldType.STRING, 8, new DbsArrayController(1,10));
        pnd_Mdma371_Pnd_Current_Date_Of_Birth = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldArrayInGroup("pnd_Mdma371_Pnd_Current_Date_Of_Birth", "#CURRENT-DATE-OF-BIRTH", 
            FieldType.NUMERIC, 8, new DbsArrayController(1,10));
        pnd_Mdma371_Pnd_Current_Sex_Code = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldArrayInGroup("pnd_Mdma371_Pnd_Current_Sex_Code", "#CURRENT-SEX-CODE", 
            FieldType.STRING, 1, new DbsArrayController(1,10));
        pnd_Mdma371_Pnd_Contract_Error_Occurrence = pnd_Mdma371_Pnd_O_Data_SectionRedef19.newFieldInGroup("pnd_Mdma371_Pnd_Contract_Error_Occurrence", 
            "#CONTRACT-ERROR-OCCURRENCE", FieldType.NUMERIC, 2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaMdma371(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

