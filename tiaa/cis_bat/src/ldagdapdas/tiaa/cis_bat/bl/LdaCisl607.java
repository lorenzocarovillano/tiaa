/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:52:43 PM
**        * FROM NATURAL LDA     : CISL607
************************************************************
**        * FILE NAME            : LdaCisl607.java
**        * CLASS NAME           : LdaCisl607
**        * INSTANCE NAME        : LdaCisl607
************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCisl607 extends DbsRecord
{
    // Properties
    private DbsGroup ta;
    private DbsField ta_Record_Id;
    private DbsGroup ta_Record_IdRedef1;
    private DbsField ta_Record_Type;
    private DbsField ta_Output_Profile;
    private DbsField ta_Reprint_Ind;
    private DbsField ta_Contract_Type;
    private DbsField ta_Rqst_Id;
    private DbsField ta_Tiaa_Numb;
    private DbsField ta_Cref_Cert_Numb;
    private DbsField ta_Da_Tiaa_Numb;
    private DbsField ta_Da_Tiaa_Amt;
    private DbsField ta_Last_Record;

    public DbsGroup getTa() { return ta; }

    public DbsField getTa_Record_Id() { return ta_Record_Id; }

    public DbsGroup getTa_Record_IdRedef1() { return ta_Record_IdRedef1; }

    public DbsField getTa_Record_Type() { return ta_Record_Type; }

    public DbsField getTa_Output_Profile() { return ta_Output_Profile; }

    public DbsField getTa_Reprint_Ind() { return ta_Reprint_Ind; }

    public DbsField getTa_Contract_Type() { return ta_Contract_Type; }

    public DbsField getTa_Rqst_Id() { return ta_Rqst_Id; }

    public DbsField getTa_Tiaa_Numb() { return ta_Tiaa_Numb; }

    public DbsField getTa_Cref_Cert_Numb() { return ta_Cref_Cert_Numb; }

    public DbsField getTa_Da_Tiaa_Numb() { return ta_Da_Tiaa_Numb; }

    public DbsField getTa_Da_Tiaa_Amt() { return ta_Da_Tiaa_Amt; }

    public DbsField getTa_Last_Record() { return ta_Last_Record; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ta = newGroupInRecord("ta", "TA");
        ta_Record_Id = ta.newFieldInGroup("ta_Record_Id", "RECORD-ID", FieldType.STRING, 38);
        ta_Record_IdRedef1 = ta.newGroupInGroup("ta_Record_IdRedef1", "Redefines", ta_Record_Id);
        ta_Record_Type = ta_Record_IdRedef1.newFieldInGroup("ta_Record_Type", "RECORD-TYPE", FieldType.STRING, 2);
        ta_Output_Profile = ta_Record_IdRedef1.newFieldInGroup("ta_Output_Profile", "OUTPUT-PROFILE", FieldType.STRING, 2);
        ta_Reprint_Ind = ta_Record_IdRedef1.newFieldInGroup("ta_Reprint_Ind", "REPRINT-IND", FieldType.STRING, 1);
        ta_Contract_Type = ta_Record_IdRedef1.newFieldInGroup("ta_Contract_Type", "CONTRACT-TYPE", FieldType.STRING, 1);
        ta_Rqst_Id = ta_Record_IdRedef1.newFieldInGroup("ta_Rqst_Id", "RQST-ID", FieldType.STRING, 8);
        ta_Tiaa_Numb = ta_Record_IdRedef1.newFieldInGroup("ta_Tiaa_Numb", "TIAA-NUMB", FieldType.STRING, 12);
        ta_Cref_Cert_Numb = ta_Record_IdRedef1.newFieldInGroup("ta_Cref_Cert_Numb", "CREF-CERT-NUMB", FieldType.STRING, 12);
        ta_Da_Tiaa_Numb = ta.newFieldInGroup("ta_Da_Tiaa_Numb", "DA-TIAA-NUMB", FieldType.STRING, 12);
        ta_Da_Tiaa_Amt = ta.newFieldInGroup("ta_Da_Tiaa_Amt", "DA-TIAA-AMT", FieldType.STRING, 15);
        ta_Last_Record = ta.newFieldInGroup("ta_Last_Record", "LAST-RECORD", FieldType.STRING, 1);

        this.setRecordName("LdaCisl607");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCisl607() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
