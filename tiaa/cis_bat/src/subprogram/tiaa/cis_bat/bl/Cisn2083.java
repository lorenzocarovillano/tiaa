/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:09:02 PM
**        * FROM NATURAL SUBPROGRAM : Cisn2083
************************************************************
**        * FILE NAME            : Cisn2083.java
**        * CLASS NAME           : Cisn2083
**        * INSTANCE NAME        : Cisn2083
************************************************************
**************************************************************
*  PROGRAM:   CISN2000
*   SYSTEM:   CIS
*     DATE:   JANUARY 28, 1997
*
* DESCRIPTION
* -----------
*   THE PURPOSE OF THE SUBPROGRAM IS TO READ THE FILE CIS-BENE-FILE
*   AND LOAD TO PDA CISA2020.
*
* 05/10/17  (BABRE)   PIN EXPANSION CHANGES. (C420007)         PINE
*
********************************************************************

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisn2083 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCisa2020 pdaCisa2020;
    private PdaCisa1000 pdaCisa1000;
    private LdaCisl2020 ldaCisl2020;
    private LdaCisl2021 ldaCisl2021;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Rcd_Type;
    private DbsField pnd_R;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;
    private DbsField pnd_Key_Pnd_Rqst_Id;
    private DbsField pnd_Key_Pnd_Unq_Id;
    private DbsField pnd_Key_Pnd_Tiaa_Nbr;
    private DbsField pnd_Cis_Ben_Super_3;

    private DbsGroup pnd_Cis_Ben_Super_3__R_Field_2;
    private DbsField pnd_Cis_Ben_Super_3_Pnd_Cis_Rcrd_Type_Cde;
    private DbsField pnd_Cis_Ben_Super_3_Pnd_Cis_Bene_Rqst_Id_Key;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCisl2020 = new LdaCisl2020();
        registerRecord(ldaCisl2020);
        registerRecord(ldaCisl2020.getVw_cis_Bene_File_01());
        ldaCisl2021 = new LdaCisl2021();
        registerRecord(ldaCisl2021);
        registerRecord(ldaCisl2021.getVw_cis_Bene_File_02());

        // parameters
        parameters = new DbsRecord();
        pdaCisa2020 = new PdaCisa2020(parameters);
        pdaCisa1000 = new PdaCisa1000(parameters);
        pnd_Rcd_Type = parameters.newFieldInRecord("pnd_Rcd_Type", "#RCD-TYPE", FieldType.STRING, 1);
        pnd_Rcd_Type.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.NUMERIC, 2);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 25);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);
        pnd_Key_Pnd_Rqst_Id = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 8);
        pnd_Key_Pnd_Unq_Id = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Unq_Id", "#UNQ-ID", FieldType.NUMERIC, 7);
        pnd_Key_Pnd_Tiaa_Nbr = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Tiaa_Nbr", "#TIAA-NBR", FieldType.STRING, 10);
        pnd_Cis_Ben_Super_3 = localVariables.newFieldInRecord("pnd_Cis_Ben_Super_3", "#CIS-BEN-SUPER-3", FieldType.STRING, 36);

        pnd_Cis_Ben_Super_3__R_Field_2 = localVariables.newGroupInRecord("pnd_Cis_Ben_Super_3__R_Field_2", "REDEFINE", pnd_Cis_Ben_Super_3);
        pnd_Cis_Ben_Super_3_Pnd_Cis_Rcrd_Type_Cde = pnd_Cis_Ben_Super_3__R_Field_2.newFieldInGroup("pnd_Cis_Ben_Super_3_Pnd_Cis_Rcrd_Type_Cde", "#CIS-RCRD-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Cis_Ben_Super_3_Pnd_Cis_Bene_Rqst_Id_Key = pnd_Cis_Ben_Super_3__R_Field_2.newFieldInGroup("pnd_Cis_Ben_Super_3_Pnd_Cis_Bene_Rqst_Id_Key", 
            "#CIS-BENE-RQST-ID-KEY", FieldType.STRING, 35);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCisl2020.initializeValues();
        ldaCisl2021.initializeValues();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cisn2083() throws Exception
    {
        super("Cisn2083");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CISN2083", onError);
        pnd_Cis_Ben_Super_3_Pnd_Cis_Rcrd_Type_Cde.setValue(pnd_Rcd_Type);                                                                                                 //Natural: ASSIGN #CIS-RCRD-TYPE-CDE := #RCD-TYPE
        pnd_Cis_Ben_Super_3_Pnd_Cis_Bene_Rqst_Id_Key.setValue(pdaCisa2020.getCisa2020_Cis_Bene_Rqst_Id_Key());                                                            //Natural: ASSIGN #CIS-BENE-RQST-ID-KEY := CISA2020.CIS-BENE-RQST-ID-KEY
        ldaCisl2020.getVw_cis_Bene_File_01().startDatabaseRead                                                                                                            //Natural: READ CIS-BENE-FILE-01 BY CIS-BEN-SUPER-3 EQ #CIS-BEN-SUPER-3
        (
        "R1",
        new Wc[] { new Wc("CIS_BEN_SUPER_3", ">=", pnd_Cis_Ben_Super_3, WcType.BY) },
        new Oc[] { new Oc("CIS_BEN_SUPER_3", "ASC") }
        );
        R1:
        while (condition(ldaCisl2020.getVw_cis_Bene_File_01().readNextRow("R1")))
        {
            if (condition(pnd_Cis_Ben_Super_3_Pnd_Cis_Bene_Rqst_Id_Key.notEquals(ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Rqst_Id_Key())))                                //Natural: IF #CIS-BENE-RQST-ID-KEY NE CIS-BENE-FILE-01.CIS-BENE-RQST-ID-KEY
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pdaCisa2020.getCisa2020_Cis_Rcrd_Type_Cde_01().setValue("1");                                                                                                 //Natural: ASSIGN CISA2020.CIS-RCRD-TYPE-CDE-01 := '1'
            pdaCisa2020.getCisa2020().setValuesByName(ldaCisl2020.getVw_cis_Bene_File_01());                                                                              //Natural: MOVE BY NAME CIS-BENE-FILE-01 TO CISA2020
            pdaCisa2020.getCisa2020_Cis_Isn_01().setValue(ldaCisl2020.getVw_cis_Bene_File_01().getAstISN("R1"));                                                          //Natural: ASSIGN CISA2020.CIS-ISN-01 := *ISN ( R1. )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *
        if (condition(pdaCisa2020.getCisa2020_Cis_Bene_Std_Free().equals("Y")))                                                                                           //Natural: IF CISA2020.CIS-BENE-STD-FREE EQ 'Y'
        {
            pnd_Cis_Ben_Super_3_Pnd_Cis_Rcrd_Type_Cde.setValue(2);                                                                                                        //Natural: ASSIGN #CIS-RCRD-TYPE-CDE := 2
            pnd_Cis_Ben_Super_3_Pnd_Cis_Bene_Rqst_Id_Key.setValue(pdaCisa2020.getCisa2020_Cis_Bene_Rqst_Id_Key());                                                        //Natural: ASSIGN #CIS-BENE-RQST-ID-KEY := CISA2020.CIS-BENE-RQST-ID-KEY
            ldaCisl2021.getVw_cis_Bene_File_02().startDatabaseRead                                                                                                        //Natural: READ CIS-BENE-FILE-02 BY CIS-BEN-SUPER-3 EQ #CIS-BEN-SUPER-3
            (
            "R2",
            new Wc[] { new Wc("CIS_BEN_SUPER_3", ">=", pnd_Cis_Ben_Super_3, WcType.BY) },
            new Oc[] { new Oc("CIS_BEN_SUPER_3", "ASC") }
            );
            R2:
            while (condition(ldaCisl2021.getVw_cis_Bene_File_02().readNextRow("R2")))
            {
                if (condition(pnd_Cis_Ben_Super_3_Pnd_Cis_Bene_Rqst_Id_Key.notEquals(ldaCisl2021.getCis_Bene_File_02_Cis_Bene_Rqst_Id_Key())))                            //Natural: IF #CIS-BENE-RQST-ID-KEY NE CIS-BENE-FILE-02.CIS-BENE-RQST-ID-KEY
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pdaCisa2020.getCisa2020_Cis_Rcrd_Type_Cde_02().setValue("2");                                                                                             //Natural: ASSIGN CISA2020.CIS-RCRD-TYPE-CDE-02 := '2'
                pdaCisa2020.getCisa2020().setValuesByName(ldaCisl2021.getVw_cis_Bene_File_02());                                                                          //Natural: MOVE BY NAME CIS-BENE-FILE-02 TO CISA2020
                pdaCisa2020.getCisa2020_Cis_Isn_02().setValue(ldaCisl2021.getVw_cis_Bene_File_02().getAstISN("R2"));                                                      //Natural: ASSIGN CISA2020.CIS-ISN-02 := *ISN ( R2. )
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }                                                                                                                                                                     //Natural: ON ERROR

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pdaCisa2020.getCisa2020_Cis_Error_Code().setValue("99");                                                                                                          //Natural: ASSIGN CISA2020.CIS-ERROR-CODE := '99'
    };                                                                                                                                                                    //Natural: END-ERROR
}
