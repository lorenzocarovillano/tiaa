/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:09:29 PM
**        * FROM NATURAL SUBPROGRAM : Cisn5001
************************************************************
**        * FILE NAME            : Cisn5001.java
**        * CLASS NAME           : Cisn5001
**        * INSTANCE NAME        : Cisn5001
************************************************************
**********************************************************************
* 05/11/17  (BABRE)   PIN EXPANSION CHANGES. (C420007)         PINE
*
**********************************************************************

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisn5001 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Cis_Ben_Super_1;

    private DbsGroup pnd_Cis_Ben_Super_1__R_Field_1;
    private DbsField pnd_Cis_Ben_Super_1_Pnd_Cisa_Cis_Rcrd_Type_Cde;
    private DbsField pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Rqst_Id;
    private DbsField pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Unique_Id_Nbr;
    private DbsField pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Tiaa_Nbr;
    private DbsField pnd_Rqst_Id_Key;
    private DbsField pnd_Prmry_Extended_Name;
    private DbsField pnd_Cntgnt_Extended_Name;
    private DbsField pnd_Occurence;
    private DbsField pnd_Found_Extended;

    private DataAccessProgramView vw_cis_Bene_File_01;
    private DbsField cis_Bene_File_01_Cis_Rcrd_Type_Cde;
    private DbsField cis_Bene_File_01_Cis_Bene_Tiaa_Nbr;
    private DbsField cis_Bene_File_01_Cis_Bene_Cref_Nbr;
    private DbsField cis_Bene_File_01_Cis_Bene_Rqst_Id;
    private DbsField cis_Bene_File_01_Cis_Bene_Unique_Id_Nbr;
    private DbsField cis_Bene_File_01_Cis_Bene_Rqst_Id_Key;
    private DbsField cis_Bene_File_01_Count_Castcis_Prmry_Bnfcry_Dta;

    private DbsGroup cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta;
    private DbsField cis_Bene_File_01_Cis_Prmry_Bene_Extndd_Nme;
    private DbsField cis_Bene_File_01_Count_Castcis_Cntgnt_Bnfcry_Dta;

    private DbsGroup cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta;
    private DbsField cis_Bene_File_01_Cis_Cntgnt_Bene_Extndd_Nme;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Cis_Ben_Super_1 = parameters.newFieldInRecord("pnd_Cis_Ben_Super_1", "#CIS-BEN-SUPER-1", FieldType.STRING, 31);
        pnd_Cis_Ben_Super_1.setParameterOption(ParameterOption.ByReference);

        pnd_Cis_Ben_Super_1__R_Field_1 = parameters.newGroupInRecord("pnd_Cis_Ben_Super_1__R_Field_1", "REDEFINE", pnd_Cis_Ben_Super_1);
        pnd_Cis_Ben_Super_1_Pnd_Cisa_Cis_Rcrd_Type_Cde = pnd_Cis_Ben_Super_1__R_Field_1.newFieldInGroup("pnd_Cis_Ben_Super_1_Pnd_Cisa_Cis_Rcrd_Type_Cde", 
            "#CISA-CIS-RCRD-TYPE-CDE", FieldType.STRING, 1);
        pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Rqst_Id = pnd_Cis_Ben_Super_1__R_Field_1.newFieldInGroup("pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Rqst_Id", "#CISA-BENE-RQST-ID", 
            FieldType.STRING, 8);
        pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Unique_Id_Nbr = pnd_Cis_Ben_Super_1__R_Field_1.newFieldInGroup("pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Unique_Id_Nbr", 
            "#CISA-BENE-UNIQUE-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Tiaa_Nbr = pnd_Cis_Ben_Super_1__R_Field_1.newFieldInGroup("pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Tiaa_Nbr", "#CISA-BENE-TIAA-NBR", 
            FieldType.STRING, 10);
        pnd_Rqst_Id_Key = parameters.newFieldInRecord("pnd_Rqst_Id_Key", "#RQST-ID-KEY", FieldType.STRING, 30);
        pnd_Rqst_Id_Key.setParameterOption(ParameterOption.ByReference);
        pnd_Prmry_Extended_Name = parameters.newFieldInRecord("pnd_Prmry_Extended_Name", "#PRMRY-EXTENDED-NAME", FieldType.STRING, 35);
        pnd_Prmry_Extended_Name.setParameterOption(ParameterOption.ByReference);
        pnd_Cntgnt_Extended_Name = parameters.newFieldInRecord("pnd_Cntgnt_Extended_Name", "#CNTGNT-EXTENDED-NAME", FieldType.STRING, 35);
        pnd_Cntgnt_Extended_Name.setParameterOption(ParameterOption.ByReference);
        pnd_Occurence = parameters.newFieldInRecord("pnd_Occurence", "#OCCURENCE", FieldType.NUMERIC, 3);
        pnd_Occurence.setParameterOption(ParameterOption.ByReference);
        pnd_Found_Extended = parameters.newFieldInRecord("pnd_Found_Extended", "#FOUND-EXTENDED", FieldType.BOOLEAN, 1);
        pnd_Found_Extended.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_cis_Bene_File_01 = new DataAccessProgramView(new NameInfo("vw_cis_Bene_File_01", "CIS-BENE-FILE-01"), "CIS_BENE_FILE_01_12", "CIS_BENE_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("CIS_BENE_FILE_01_12"));
        cis_Bene_File_01_Cis_Rcrd_Type_Cde = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Rcrd_Type_Cde", "CIS-RCRD-TYPE-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_RCRD_TYPE_CDE");
        cis_Bene_File_01_Cis_Bene_Tiaa_Nbr = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Tiaa_Nbr", "CIS-BENE-TIAA-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_BENE_TIAA_NBR");
        cis_Bene_File_01_Cis_Bene_Cref_Nbr = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Cref_Nbr", "CIS-BENE-CREF-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_BENE_CREF_NBR");
        cis_Bene_File_01_Cis_Bene_Rqst_Id = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Rqst_Id", "CIS-BENE-RQST-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_BENE_RQST_ID");
        cis_Bene_File_01_Cis_Bene_Unique_Id_Nbr = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Unique_Id_Nbr", "CIS-BENE-UNIQUE-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CIS_BENE_UNIQUE_ID_NBR");
        cis_Bene_File_01_Cis_Bene_Rqst_Id_Key = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Cis_Bene_Rqst_Id_Key", "CIS-BENE-RQST-ID-KEY", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "CIS_BENE_RQST_ID_KEY");
        cis_Bene_File_01_Count_Castcis_Prmry_Bnfcry_Dta = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Count_Castcis_Prmry_Bnfcry_Dta", 
            "C*CIS-PRMRY-BNFCRY-DTA", RepeatingFieldStrategy.CAsteriskVariable, "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");

        cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta = vw_cis_Bene_File_01.getRecord().newGroupArrayInGroup("cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta", "CIS-PRMRY-BNFCRY-DTA", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Prmry_Bene_Extndd_Nme = cis_Bene_File_01_Cis_Prmry_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_01_Cis_Prmry_Bene_Extndd_Nme", 
            "CIS-PRMRY-BENE-EXTNDD-NME", FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_EXTNDD_NME", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_01_Count_Castcis_Cntgnt_Bnfcry_Dta = vw_cis_Bene_File_01.getRecord().newFieldInGroup("cis_Bene_File_01_Count_Castcis_Cntgnt_Bnfcry_Dta", 
            "C*CIS-CNTGNT-BNFCRY-DTA", RepeatingFieldStrategy.CAsteriskVariable, "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");

        cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta = vw_cis_Bene_File_01.getRecord().newGroupArrayInGroup("cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta", "CIS-CNTGNT-BNFCRY-DTA", 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_01_Cis_Cntgnt_Bene_Extndd_Nme = cis_Bene_File_01_Cis_Cntgnt_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_01_Cis_Cntgnt_Bene_Extndd_Nme", 
            "CIS-CNTGNT-BENE-EXTNDD-NME", FieldType.STRING, 35, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_EXTNDD_NME", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cis_Bene_File_01.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cisn5001() throws Exception
    {
        super("Cisn5001");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        vw_cis_Bene_File_01.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) CIS-BENE-FILE-01 WITH CIS-BEN-SUPER-1 = #CIS-BEN-SUPER-1
        (
        "READ01",
        new Wc[] { new Wc("CIS_BEN_SUPER_1", ">=", pnd_Cis_Ben_Super_1, WcType.BY) },
        new Oc[] { new Oc("CIS_BEN_SUPER_1", "ASC") },
        1
        );
        READ01:
        while (condition(vw_cis_Bene_File_01.readNextRow("READ01")))
        {
            if (condition(cis_Bene_File_01_Cis_Bene_Rqst_Id.notEquals(pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Rqst_Id) || cis_Bene_File_01_Cis_Bene_Unique_Id_Nbr.notEquals(pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Unique_Id_Nbr)  //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID NE #CISA-BENE-RQST-ID OR CIS-BENE-FILE-01.CIS-BENE-UNIQUE-ID-NBR NE #CISA-BENE-UNIQUE-ID-NBR OR CIS-BENE-FILE-01.CIS-BENE-TIAA-NBR NE #CISA-BENE-TIAA-NBR
                || cis_Bene_File_01_Cis_Bene_Tiaa_Nbr.notEquals(pnd_Cis_Ben_Super_1_Pnd_Cisa_Bene_Tiaa_Nbr)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Found_Extended.setValue(true);                                                                                                                            //Natural: ASSIGN #FOUND-EXTENDED := TRUE
            if (condition(pnd_Occurence.greater(getZero())))                                                                                                              //Natural: IF #OCCURENCE GT 0
            {
                pnd_Prmry_Extended_Name.setValue(cis_Bene_File_01_Cis_Prmry_Bene_Extndd_Nme.getValue(pnd_Occurence));                                                     //Natural: ASSIGN #PRMRY-EXTENDED-NAME := CIS-PRMRY-BENE-EXTNDD-NME ( #OCCURENCE )
                pnd_Cntgnt_Extended_Name.setValue(cis_Bene_File_01_Cis_Cntgnt_Bene_Extndd_Nme.getValue(pnd_Occurence));                                                   //Natural: ASSIGN #CNTGNT-EXTENDED-NAME := CIS-CNTGNT-BENE-EXTNDD-NME ( #OCCURENCE )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
