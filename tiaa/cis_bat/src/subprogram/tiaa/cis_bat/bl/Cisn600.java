/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:09:30 PM
**        * FROM NATURAL SUBPROGRAM : Cisn600
************************************************************
**        * FILE NAME            : Cisn600.java
**        * CLASS NAME           : Cisn600
**        * INSTANCE NAME        : Cisn600
************************************************************
************************************************************************
** SYSTEM       : CIS SYSTEM                                           *
** AUTHOR       : KATHLEEN ROOY                                        *
** DESCRIPTION  : THIS SUBPROGRAM WILL WRITE AND MOVE THE INFORMATION  *
**              : TO A WORK FILE IN CISB600                            *
** OUTPUT FILE  : NONE                                                 *
** PROGRAM      : CISN600                                              *
** GENERATED    :                                                      *
* ----------  -------------  ----------------------------------------- *
*   DATE      MODIFIED BY    CHANGE DESCRIPTION                        *
* ----------  -------------  ----------------------------------------- *
* 06/07/2006  O. SOTTO       CHANGED FOR MDO LEGAL SPLIT SC 060706     *
* 07/31/2007  O. SOTTO       NEW ANNUITY (75%) OPTION CHANGE SC 073107 *
* 04/07/2011  J. BERGHEISER  CHANGE SPELLING OF INSTALLMENT FOR HEADER *
************************************************************************
*

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisn600 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCisa600 pdaCisa600;

    // Local Variables
    public DbsRecord localVariables;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaCisa600 = new PdaCisa600(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cisn600() throws Exception
    {
        super("Cisn600");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* *---------------------------MAIN LOGIC---------------------------------
        //*  ZP=ON
        //*  ZP=ON
        //* *                                                                                                                                                             //Natural: FORMAT ( 1 ) PS = 62 LS = 130;//Natural: FORMAT ( 2 ) PS = 62 LS = 75 ZP = ON;//Natural: FORMAT ( 3 ) PS = 58 LS = 130
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id().equals("IA")))                                                                                              //Natural: IF #CIS-RQST-ID = 'IA'
        {
            short decideConditionsMet65 = 0;                                                                                                                              //Natural: DECIDE ON FIRST VALUE OF #CIS-ANNTY-OPTION;//Natural: VALUE 'OL'
            if (condition((pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("OL"))))
            {
                decideConditionsMet65++;
                                                                                                                                                                          //Natural: PERFORM IA-OL
                sub_Ia_Ol();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'AC'
            else if (condition((pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("AC"))))
            {
                decideConditionsMet65++;
                                                                                                                                                                          //Natural: PERFORM IA-AC
                sub_Ia_Ac();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'LSF'
            else if (condition((pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("LSF"))))
            {
                decideConditionsMet65++;
                                                                                                                                                                          //Natural: PERFORM IA-LSF
                sub_Ia_Lsf();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'JS'
            else if (condition((pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("JS"))))
            {
                decideConditionsMet65++;
                                                                                                                                                                          //Natural: PERFORM IA-JS
                sub_Ia_Js();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'LS'
            else if (condition((pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("LS"))))
            {
                decideConditionsMet65++;
                //*  073107
                                                                                                                                                                          //Natural: PERFORM IA-LS
                sub_Ia_Ls();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'LST'
            else if (condition((pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("LST"))))
            {
                decideConditionsMet65++;
                //*  073107
                                                                                                                                                                          //Natural: PERFORM IA-LST
                sub_Ia_Lst();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'IR'
            else if (condition((pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("IR"))))
            {
                decideConditionsMet65++;
                                                                                                                                                                          //Natural: PERFORM IA-IR
                sub_Ia_Ir();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  060706
        if (condition(DbsUtil.maskMatches(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id(),"'MDO'")))                                                                              //Natural: IF #CIS-RQST-ID = MASK ( 'MDO' )
        {
                                                                                                                                                                          //Natural: PERFORM MOVE-MDO
            sub_Move_Mdo();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id().equals("TPA")))                                                                                             //Natural: IF #CIS-RQST-ID = 'TPA'
        {
                                                                                                                                                                          //Natural: PERFORM MOVE-TPA
            sub_Move_Tpa();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id().equals("IPRO")))                                                                                            //Natural: IF #CIS-RQST-ID = 'IPRO'
        {
                                                                                                                                                                          //Natural: PERFORM MOVE-IPRO
            sub_Move_Ipro();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-OL
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-AC
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-LSF
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-JS
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-LS
        //* *******************************************************073107 START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-LST
        //* *************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-IR
        //* ***
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-MDO
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TPA
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-IPRO
    }
    private void sub_Ia_Ol() throws Exception                                                                                                                             //Natural: IA-OL
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("A") ||                     //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M' OR = 'A' OR = 'S'
            pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("S")))
        {
            pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF One-Life Unit-Annuity Certificate");                                                     //Natural: MOVE 'Your CREF One-Life Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M")))                                                                                  //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1005.6");                                                                                      //Natural: MOVE 'C1005.6' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1005.6");
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF OL");                                                                                     //Natural: MOVE 'CREF OL' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF OL");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 4-98' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1005.5");                                                                                      //Natural: MOVE 'C1005.5' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1005.5");
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF OL");                                                                                     //Natural: MOVE 'CREF OL' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF OL");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 7-97' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("T")))                                                                                  //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'T'
            {
                pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF Equities One-Life Unit-Annuity Certificate");                                        //Natural: MOVE 'Your CREF Equities One-Life Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("CE1005");                                                                                       //Natural: MOVE 'CE1005' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("CE1005");
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Equities One-Life");                                                                      //Natural: MOVE 'CREF Equities One-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Equities One-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 10-1999", MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT 'Ed. 10-1999' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("A")))                        //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M' OR = 'A'
        {
            pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue("Your TIAA Real Estate Account One-Life Unit-Annuity Contract");                                //Natural: MOVE 'Your TIAA Real Estate Account One-Life Unit-Annuity Contract' TO #TIAA-REA-CNTRCT-HEADER-2
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M")))                                                                                   //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("1009.1-REA");                                                                               //Natural: MOVE '1009.1-REA' TO #TIAA-REA-FORM-NUM-PG3-4 #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("1009.1-REA");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA OL");                                                                             //Natural: MOVE 'TIAA REA OL' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA OL");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                   //Natural: MOVE RIGHT 'Ed. 4-98' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("1009 - REA");                                                                               //Natural: MOVE '1009 - REA' TO #TIAA-REA-FORM-NUM-PG3-4 #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("1009 - REA");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA OLA");                                                                            //Natural: MOVE 'TIAA REA OLA' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA OLA");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 10-95", MoveOption.RightJustified);                                                  //Natural: MOVE RIGHT 'Ed. 10-95' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("A") || pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("S") ||                     //Natural: IF #CIS-TIAA-CNTRCT-TYPE = 'A' OR = 'S' OR = 'M'
            pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))
        {
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))                                                                                  //Natural: IF #CIS-TIAA-CNTRCT-TYPE EQ 'M'
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA One-Life Traditional Payout Annuity Contract");                                      //Natural: MOVE 'Your TIAA One-Life Traditional Payout Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1009.9");                                                                                       //Natural: MOVE '1009.9' TO #TIAA-FORM-NUM-PG3-4 #TIAA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1009.9");
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA One-Life");                                                                               //Natural: MOVE 'TIAA One-Life' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA One-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 10-1999", MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT 'Ed. 10-1999' TO #TIAA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                          //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA One-Life Annuity Contract");                                                         //Natural: MOVE 'Your TIAA One-Life Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1009.8");                                                                                       //Natural: MOVE '1009.8' TO #TIAA-FORM-NUM-PG3-4 #TIAA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1009.8");
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA OL");                                                                                     //Natural: MOVE 'TIAA OL' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA OL");
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 7-97' TO #TIAA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                          //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ia_Ac() throws Exception                                                                                                                             //Natural: IA-AC
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("A") ||                     //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M' OR = 'A' OR = 'S'
            pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("S")))
        {
            pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF Fixed-Period Unit-Annuity Certificate");                                                 //Natural: MOVE 'Your CREF Fixed-Period Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M")))                                                                                  //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1008.6");                                                                                      //Natural: MOVE 'C1008.6' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1008.6");
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF AC");                                                                                     //Natural: MOVE 'CREF AC' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF AC");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 4-98' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1008.5");                                                                                      //Natural: MOVE 'C1008.5' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1008.5");
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF AC");                                                                                     //Natural: MOVE 'CREF AC' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF AC");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 7-97' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("A")))                        //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M' OR = 'A'
        {
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M")))                                                                                   //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue("Your TIAA Real Estate Account Fixed-Period Unit-Annuity Contract");                        //Natural: MOVE 'Your TIAA Real Estate Account Fixed-Period Unit-Annuity Contract' TO #TIAA-REA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("901.1-REA");                                                                                //Natural: MOVE '901.1-REA' TO #TIAA-REA-FORM-NUM-PG3-4 #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("901.1-REA");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA AC");                                                                             //Natural: MOVE 'TIAA REA AC' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA AC");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                   //Natural: MOVE RIGHT 'Ed. 4-98' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue("Your TIAA Real Estate Account Unit-Annuity Certain Contract");                             //Natural: MOVE 'Your TIAA Real Estate Account Unit-Annuity Certain Contract' TO #TIAA-REA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("901 - REA");                                                                                //Natural: MOVE '901 - REA' TO #TIAA-REA-FORM-NUM-PG3-4 #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("901 - REA");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA AC");                                                                             //Natural: MOVE 'TIAA REA AC' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA AC");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 10-95", MoveOption.RightJustified);                                                  //Natural: MOVE RIGHT 'Ed. 10-95' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("A") || pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("S") ||                     //Natural: IF #CIS-TIAA-CNTRCT-TYPE = 'A' OR = 'S' OR = 'M'
            pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))
        {
            pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA Fixed-Period Annuity Contract");                                                         //Natural: MOVE 'Your TIAA Fixed-Period Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
            pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                           //Natural: MOVE RIGHT 'Ed. 7-97' TO #TIAA-EDITION-NUM-PG3-4
            pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                              //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("901.9");                                                                                            //Natural: MOVE '901.9' TO #TIAA-FORM-NUM-PG3-4 #TIAA-FORM-NUM-PG5-6
            pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("901.9");
            pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA AC");                                                                                         //Natural: MOVE 'TIAA AC' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
            pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA AC");
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ia_Lsf() throws Exception                                                                                                                            //Natural: IA-LSF
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("A") ||                     //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M' OR = 'A' OR = 'S'
            pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("S")))
        {
            pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF Two Life Unit-Annuity Certificate");                                                     //Natural: MOVE 'Your CREF Two Life Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
            pdaCisa600.getCisa600_Pnd_Cref_Income_Option().setValue("Two-Life Unit-Annuity With Full Benefit to Survivor");                                               //Natural: MOVE 'Two-Life Unit-Annuity With Full Benefit to Survivor' TO #CREF-INCOME-OPTION
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M")))                                                                                  //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1033.1-LSF");                                                                                  //Natural: MOVE 'C1033.1-LSF' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1033.1");                                                                                      //Natural: MOVE 'C1033.1' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Two-Life");                                                                               //Natural: MOVE 'CREF Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 4-98' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1033-LSF");                                                                                    //Natural: MOVE 'C1033-LSF' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1033");                                                                                        //Natural: MOVE 'C1033' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Two-Life");                                                                               //Natural: MOVE 'CREF Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 7-97' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("T")))                                                                                  //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'T'
            {
                pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF Equities Two Life Unit-Annuity Certificate");                                        //Natural: MOVE 'Your CREF Equities Two Life Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Cref_Income_Option().setValue("Two-Life Unit-Annuity With Full Benefit to Survivor");                                           //Natural: MOVE 'Two-Life Unit-Annuity With Full Benefit to Survivor' TO #CREF-INCOME-OPTION
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("CE1033-LSF");                                                                                   //Natural: MOVE 'CE1033-LSF' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("CE1033");                                                                                       //Natural: MOVE 'CE1033' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Equities Two-Life");                                                                      //Natural: MOVE 'CREF Equities Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Equities Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 10-1999", MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT 'Ed. 10-1999' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("A")))                        //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M' OR = 'A'
        {
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M")))                                                                                   //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue("Your TIAA Real Estate Account Two-Life Unit-Annuity Contract");                            //Natural: MOVE 'Your TIAA Real Estate Account Two-Life Unit-Annuity Contract' TO #TIAA-REA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("1033-REA-LSF");                                                                             //Natural: MOVE '1033-REA-LSF' TO #TIAA-REA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("1033-REA");                                                                                 //Natural: MOVE '1033-REA' TO #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA Two-Life");                                                                       //Natural: MOVE 'TIAA REA Two-Life' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                   //Natural: MOVE RIGHT 'Ed. 4-98' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue("Your TIAA Real Estate Account Last Survivor Life Unit-Annuity Contract");                  //Natural: MOVE 'Your TIAA Real Estate Account Last Survivor Life Unit-Annuity Contract' TO #TIAA-REA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("1024 - REA");                                                                               //Natural: MOVE '1024 - REA' TO #TIAA-REA-FORM-NUM-PG3-4 #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("1024 - REA");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA LS");                                                                             //Natural: MOVE 'TIAA REA LS' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA LS");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 10-95", MoveOption.RightJustified);                                                  //Natural: MOVE RIGHT 'Ed. 10-95' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("A") || pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("S") ||                     //Natural: IF #CIS-TIAA-CNTRCT-TYPE = 'A' OR = 'S' OR = 'M'
            pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))
        {
            pdaCisa600.getCisa600_Pnd_Tiaa_Income_Option().setValue("Two-Life Annuity With Full Benefit to Survivor");                                                    //Natural: MOVE 'Two-Life Annuity With Full Benefit to Survivor' TO #TIAA-INCOME-OPTION
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))                                                                                  //Natural: IF #CIS-TIAA-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA Two-Life Traditional Annuity Contract");                                             //Natural: MOVE 'Your TIAA Two-Life Traditional Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1033.1-LSF");                                                                                   //Natural: MOVE '1033.1-LSF' TO #TIAA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1033.1");                                                                                       //Natural: MOVE '1033.1' TO #TIAA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA Two-Life");                                                                               //Natural: MOVE 'TIAA Two-Life' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 10-1999", MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT 'Ed. 10-1999' TO #TIAA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                          //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA Two-Life Annuity Contract");                                                         //Natural: MOVE 'Your TIAA Two-Life Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1033-LSF");                                                                                     //Natural: MOVE '1033-LSF' TO #TIAA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1033");                                                                                         //Natural: MOVE '1033' TO #TIAA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA Two-Life");                                                                               //Natural: MOVE 'TIAA Two-Life' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 7-97' TO #TIAA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                          //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ia_Js() throws Exception                                                                                                                             //Natural: IA-JS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("S") ||                     //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M' OR = 'S' OR = 'A'
            pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("A")))
        {
            pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF Two-Life Unit-Annuity Certificate");                                                     //Natural: MOVE 'Your CREF Two-Life Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
            pdaCisa600.getCisa600_Pnd_Cref_Income_Option().setValue("Two-Life Unit-Annuity With Two-Thirds Benefit to Survivor");                                         //Natural: MOVE 'Two-Life Unit-Annuity With Two-Thirds Benefit to Survivor' TO #CREF-INCOME-OPTION
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M")))                                                                                  //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1033.1-J&S");                                                                                  //Natural: MOVE 'C1033.1-J&S' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1033.1");                                                                                      //Natural: MOVE 'C1033.1' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Two-Life");                                                                               //Natural: MOVE 'CREF Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 4-98' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1033-J&S");                                                                                    //Natural: MOVE 'C1033-J&S' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1033");                                                                                        //Natural: MOVE 'C1033' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Two-Life");                                                                               //Natural: MOVE 'CREF Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 7-97' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("T")))                                                                                  //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'T'
            {
                pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF Equities Two-Life Unit-Annuity Certificate");                                        //Natural: MOVE 'Your CREF Equities Two-Life Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Cref_Income_Option().setValue("Two-Life Unit-Annuity With Two-Thirds Benefit to Survivor");                                     //Natural: MOVE 'Two-Life Unit-Annuity With Two-Thirds Benefit to Survivor' TO #CREF-INCOME-OPTION
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("CE1033-J&S");                                                                                   //Natural: MOVE 'CE1033-J&S' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("CE1033");                                                                                       //Natural: MOVE 'CE1033' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Equities Two-Life");                                                                      //Natural: MOVE 'CREF Equities Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Equities Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 10-1999", MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT 'Ed. 10-1999' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("A")))                        //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M' OR = 'A'
        {
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M")))                                                                                   //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue("Your TIAA Real Estate Account Two-Life Unit-Annuity Contract");                            //Natural: MOVE 'Your TIAA Real Estate Account Two-Life Unit-Annuity Contract' TO #TIAA-REA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("1033-REA-J&S");                                                                             //Natural: MOVE '1033-REA-J&S' TO #TIAA-REA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("1033-REA");                                                                                 //Natural: MOVE '1033-REA' TO #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA Two-Life");                                                                       //Natural: MOVE 'TIAA REA Two-Life' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                   //Natural: MOVE RIGHT 'Ed. 4-98' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue("Your TIAA Real Estate Account Joint and Survivor Life Unit-Annuity Contract");             //Natural: MOVE 'Your TIAA Real Estate Account Joint and Survivor Life Unit-Annuity Contract' TO #TIAA-REA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("1019 - REA");                                                                               //Natural: MOVE '1019 - REA' TO #TIAA-REA-FORM-NUM-PG3-4 #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("1019 - REA");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA JS");                                                                             //Natural: MOVE 'TIAA REA JS' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA JS");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 10-95", MoveOption.RightJustified);                                                  //Natural: MOVE RIGHT 'Ed. 10-95' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("A") || pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("S") ||                     //Natural: IF #CIS-TIAA-CNTRCT-TYPE = 'A' OR = 'S' OR = 'M'
            pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))
        {
            pdaCisa600.getCisa600_Pnd_Tiaa_Income_Option().setValue("Two-Life Annuity With Two-Thirds Benefit to Survivor");                                              //Natural: MOVE 'Two-Life Annuity With Two-Thirds Benefit to Survivor' TO #TIAA-INCOME-OPTION
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))                                                                                  //Natural: IF #CIS-TIAA-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA Two-Life Traditional Annuity Contract");                                             //Natural: MOVE 'Your TIAA Two-Life Traditional Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1033.1-J&S");                                                                                   //Natural: MOVE '1033.1-J&S' TO #TIAA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1033.1");                                                                                       //Natural: MOVE '1033.1' TO #TIAA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA Two-Life");                                                                               //Natural: MOVE 'TIAA Two-Life' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 10-1999", MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT 'Ed. 10-1999' TO #TIAA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                          //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA Two-Life Annuity Contract");                                                         //Natural: MOVE 'Your TIAA Two-Life Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1033-J&S");                                                                                     //Natural: MOVE '1033-J&S' TO #TIAA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1033");                                                                                         //Natural: MOVE '1033' TO #TIAA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA Two-Life");                                                                               //Natural: MOVE 'TIAA Two-Life' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 7-97' TO #TIAA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                          //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ia_Ls() throws Exception                                                                                                                             //Natural: IA-LS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("A") ||                     //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M' OR = 'A' OR = 'S'
            pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("S")))
        {
            pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF Two-Life Unit-Annuity Certificate");                                                     //Natural: MOVE 'Your CREF Two-Life Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
            pdaCisa600.getCisa600_Pnd_Cref_Income_Option().setValue("Two-Life Unit-Annuity With One-Half Benefit to Surviving Second Participant");                       //Natural: MOVE 'Two-Life Unit-Annuity With One-Half Benefit to Surviving Second Participant' TO #CREF-INCOME-OPTION
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M")))                                                                                  //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1033.1-LSH");                                                                                  //Natural: MOVE 'C1033.1-LSH' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1033.1");                                                                                      //Natural: MOVE 'C1033.1' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Two-Life");                                                                               //Natural: MOVE 'CREF Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 4-98' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1033-LSH");                                                                                    //Natural: MOVE 'C1033-LSH' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1033");                                                                                        //Natural: MOVE 'C1033' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Two-Life");                                                                               //Natural: MOVE 'CREF Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 7-97' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("T")))                                                                                  //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'T'
            {
                pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF Equities Two-Life Unit-Annuity Certificate");                                        //Natural: MOVE 'Your CREF Equities Two-Life Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Cref_Income_Option().setValue("Two-Life Unit-Annuity With One-Half Benefit to Surviving Second Participant");                   //Natural: MOVE 'Two-Life Unit-Annuity With One-Half Benefit to Surviving Second Participant' TO #CREF-INCOME-OPTION
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("CE1033-LSH");                                                                                   //Natural: MOVE 'CE1033-LSH' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("CE1033");                                                                                       //Natural: MOVE 'CE1033' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Equities Two-Life");                                                                      //Natural: MOVE 'CREF Equities Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Equities Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 10-1999", MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT 'Ed. 10-1999' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("A")))                        //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M' OR = 'A'
        {
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M")))                                                                                   //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue("Your TIAA Real Estate Account Two-Life Unit-Annuity Contract");                            //Natural: MOVE 'Your TIAA Real Estate Account Two-Life Unit-Annuity Contract' TO #TIAA-REA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("1033-REA-LSH");                                                                             //Natural: MOVE '1033-REA-LSH' TO #TIAA-REA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("1033-REA");                                                                                 //Natural: MOVE '1033-REA' TO #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA Two-Life");                                                                       //Natural: MOVE 'TIAA REA Two-Life' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                   //Natural: MOVE RIGHT 'Ed. 4-98' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue("Your TIAA Real Estate Account Last Survivor Life Unit-Annuity Contract");                  //Natural: MOVE 'Your TIAA Real Estate Account Last Survivor Life Unit-Annuity Contract' TO #TIAA-REA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("1024 - REA");                                                                               //Natural: MOVE '1024 - REA' TO #TIAA-REA-FORM-NUM-PG3-4 #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("1024 - REA");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA LS");                                                                             //Natural: MOVE 'TIAA REA LS' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA LS");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 10-95", MoveOption.RightJustified);                                                  //Natural: MOVE RIGHT 'Ed. 10-95' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("A") || pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("S") ||                     //Natural: IF #CIS-TIAA-CNTRCT-TYPE = 'A' OR = 'S' OR = 'M'
            pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))
        {
            pdaCisa600.getCisa600_Pnd_Tiaa_Income_Option().setValue("Two-Life Annuity With One-Half Benefit to Surviving Second Annuitant");                              //Natural: MOVE 'Two-Life Annuity With One-Half Benefit to Surviving Second Annuitant' TO #TIAA-INCOME-OPTION
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))                                                                                  //Natural: IF #CIS-TIAA-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA Two-Life Traditional Annuity Contract");                                             //Natural: MOVE 'Your TIAA Two-Life Traditional Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1033.1-LSH");                                                                                   //Natural: MOVE '1033.1-LSH' TO #TIAA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1033.1");                                                                                       //Natural: MOVE '1033.1' TO #TIAA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA Two-Life");                                                                               //Natural: MOVE 'TIAA Two-Life' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 10-1999", MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT 'Ed. 10-1999' TO #TIAA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                          //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA Two-Life Annuity Contract");                                                         //Natural: MOVE 'Your TIAA Two-Life Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1033-LSH");                                                                                     //Natural: MOVE '1033-LSH' TO #TIAA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1033");                                                                                         //Natural: MOVE '1033' TO #TIAA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA Two-Life");                                                                               //Natural: MOVE 'TIAA Two-Life' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 7-97' TO #TIAA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                          //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ia_Lst() throws Exception                                                                                                                            //Natural: IA-LST
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("A") ||                     //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M' OR = 'A' OR = 'S'
            pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("S")))
        {
            pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF Two-Life Unit-Annuity Certificate");                                                     //Natural: MOVE 'Your CREF Two-Life Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
            pdaCisa600.getCisa600_Pnd_Cref_Income_Option().setValue("Two-Life Unit-Annuity With 75% Benefit to Surviving Second Participant");                            //Natural: MOVE 'Two-Life Unit-Annuity With 75% Benefit to Surviving Second Participant' TO #CREF-INCOME-OPTION
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("M")))                                                                                  //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1033.1-LSH");                                                                                  //Natural: MOVE 'C1033.1-LSH' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1033.1");                                                                                      //Natural: MOVE 'C1033.1' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Two-Life");                                                                               //Natural: MOVE 'CREF Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 4-98' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1033-LSH");                                                                                    //Natural: MOVE 'C1033-LSH' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1033");                                                                                        //Natural: MOVE 'C1033' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Two-Life");                                                                               //Natural: MOVE 'CREF Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 7-97' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("T")))                                                                                  //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'T'
            {
                pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF Equities Two-Life Unit-Annuity Certificate");                                        //Natural: MOVE 'Your CREF Equities Two-Life Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Cref_Income_Option().setValue("Two-Life Unit-Annuity With 75% Benefit to Surviving Second Participant");                        //Natural: MOVE 'Two-Life Unit-Annuity With 75% Benefit to Surviving Second Participant' TO #CREF-INCOME-OPTION
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("CE1033-LSH");                                                                                   //Natural: MOVE 'CE1033-LSH' TO #CREF-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("CE1033");                                                                                       //Natural: MOVE 'CE1033' TO #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Equities Two-Life");                                                                      //Natural: MOVE 'CREF Equities Two-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Equities Two-Life");
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 10-1999", MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT 'Ed. 10-1999' TO #CREF-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                          //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M") || pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("A")))                        //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M' OR = 'A'
        {
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().equals("M")))                                                                                   //Natural: IF #CIS-REA-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue("Your TIAA Real Estate Account Two-Life Unit-Annuity Contract");                            //Natural: MOVE 'Your TIAA Real Estate Account Two-Life Unit-Annuity Contract' TO #TIAA-REA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("1033-REA-LSH");                                                                             //Natural: MOVE '1033-REA-LSH' TO #TIAA-REA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("1033-REA");                                                                                 //Natural: MOVE '1033-REA' TO #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA Two-Life");                                                                       //Natural: MOVE 'TIAA REA Two-Life' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 4-98", MoveOption.RightJustified);                                                   //Natural: MOVE RIGHT 'Ed. 4-98' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue("Your TIAA Real Estate Account Last Survivor Life Unit-Annuity Contract");                  //Natural: MOVE 'Your TIAA Real Estate Account Last Survivor Life Unit-Annuity Contract' TO #TIAA-REA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue("1024 - REA");                                                                               //Natural: MOVE '1024 - REA' TO #TIAA-REA-FORM-NUM-PG3-4 #TIAA-REA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue("1024 - REA");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue("TIAA REA LS");                                                                             //Natural: MOVE 'TIAA REA LS' TO #TIAA-REA-PRODUCT-CDE-3-4 #TIAA-REA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue("TIAA REA LS");
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue("Ed. 10-95", MoveOption.RightJustified);                                                  //Natural: MOVE RIGHT 'Ed. 10-95' TO #TIAA-REA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                                  //Natural: MOVE #TIAA-REA-EDITION-NUM-PG3-4 TO #TIAA-REA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("A") || pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("S") ||                     //Natural: IF #CIS-TIAA-CNTRCT-TYPE = 'A' OR = 'S' OR = 'M'
            pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))
        {
            pdaCisa600.getCisa600_Pnd_Tiaa_Income_Option().setValue("Two-Life Annuity With 75% Benefit to Surviving Second Annuitant");                                   //Natural: MOVE 'Two-Life Annuity With 75% Benefit to Surviving Second Annuitant' TO #TIAA-INCOME-OPTION
            if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))                                                                                  //Natural: IF #CIS-TIAA-CNTRCT-TYPE = 'M'
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA Two-Life Traditional Annuity Contract");                                             //Natural: MOVE 'Your TIAA Two-Life Traditional Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1033.1-LSH");                                                                                   //Natural: MOVE '1033.1-LSH' TO #TIAA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1033.1");                                                                                       //Natural: MOVE '1033.1' TO #TIAA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA Two-Life");                                                                               //Natural: MOVE 'TIAA Two-Life' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 10-1999", MoveOption.RightJustified);                                                    //Natural: MOVE RIGHT 'Ed. 10-1999' TO #TIAA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                          //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA Two-Life Annuity Contract");                                                         //Natural: MOVE 'Your TIAA Two-Life Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1033-LSH");                                                                                     //Natural: MOVE '1033-LSH' TO #TIAA-FORM-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1033");                                                                                         //Natural: MOVE '1033' TO #TIAA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA Two-Life");                                                                               //Natural: MOVE 'TIAA Two-Life' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA Two-Life");
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                       //Natural: MOVE RIGHT 'Ed. 7-97' TO #TIAA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                          //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  073107 END
    }
    private void sub_Ia_Ir() throws Exception                                                                                                                             //Natural: IA-IR
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************************************
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("A") || pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("S") ||                     //Natural: IF #CIS-TIAA-CNTRCT-TYPE = 'A' OR = 'S' OR = 'M'
            pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().equals("M")))
        {
            //*  JRB
            pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA Instalment Refund Life Annuity Contract");                                               //Natural: MOVE 'Your TIAA Instalment Refund Life Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
            pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue(" ");                                                                                               //Natural: MOVE ' ' TO #CREF-CNTRCT-HEADER-2
            pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue(" ");                                                                                           //Natural: MOVE ' ' TO #TIAA-REA-CNTRCT-HEADER-2
            pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 7-97", MoveOption.RightJustified);                                                           //Natural: MOVE RIGHT 'Ed. 7-97' TO #TIAA-EDITION-NUM-PG3-4
            pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                              //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
            pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1011.9");                                                                                           //Natural: MOVE '1011.9' TO #TIAA-FORM-NUM-PG3-4 #TIAA-FORM-NUM-PG5-6
            pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1011.9");
            pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA IR");                                                                                         //Natural: MOVE 'TIAA IR' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
            pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA IR");
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().equals("T")))                                                                                      //Natural: IF #CIS-CREF-CNTRCT-TYPE = 'T'
        {
            pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Your CREF Equities One-Life Unit-Annuity Certificate");                                            //Natural: MOVE 'Your CREF Equities One-Life Unit-Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
            pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("CE1005");                                                                                           //Natural: MOVE 'CE1005' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
            pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("CE1005");
            pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF Equities One-Life");                                                                          //Natural: MOVE 'CREF Equities One-Life' TO #CREF-PRODUCT-CDE-3-4 #CREF-PRODUCT-CDE-5-6
            pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF Equities One-Life");
            pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue("Ed. 10-1999", MoveOption.RightJustified);                                                        //Natural: MOVE RIGHT 'Ed. 10-1999' TO #CREF-EDITION-NUM-PG3-4
            pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                                              //Natural: MOVE #CREF-EDITION-NUM-PG3-4 TO #CREF-EDITION-NUM-PG5-6
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Mdo() throws Exception                                                                                                                          //Natural: MOVE-MDO
    {
        if (BLNatReinput.isReinput()) return;

        pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_1().setValue("Your TIAA Minimum Distribution");                                                                      //Natural: MOVE 'Your TIAA Minimum Distribution' TO #TIAA-CNTRCT-HEADER-1
        pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_1().setValue("Your CREF Minimum Distribution");                                                                      //Natural: MOVE 'Your CREF Minimum Distribution' TO #CREF-CNTRCT-HEADER-1
        pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Annuity Contract");                                                                                    //Natural: MOVE 'Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
        pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2().setValue("Annuity Certificate");                                                                                 //Natural: MOVE 'Annuity Certificate' TO #CREF-CNTRCT-HEADER-2
        //* * #CIS-RQST-ID = 'MDO' AND #CIS-ANNTY-OPTION = 'SRA' OR = 'GSRA' /* 060706
        //*  060706
        if (condition((DbsUtil.maskMatches(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id(),"'MDO'") && ((((pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("SRA")             //Natural: IF #CIS-RQST-ID = MASK ( 'MDO' ) AND ( #CIS-ANNTY-OPTION = 'SRA' OR = 'GSRA' OR = 'IRA' OR = 'RA' OR = 'GRA' )
            || pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("GSRA")) || pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("IRA")) || pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("RA")) 
            || pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("GRA")))))
        {
            pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 7-97");                                                                                      //Natural: MOVE 'Ed. 7-97' TO #TIAA-EDITION-NUM-PG3-4
            pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                              //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6 #CREF-EDITION-NUM-PG3-4 #CREF-EDITION-NUM-PG5-6
            pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());
            pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());
        }                                                                                                                                                                 //Natural: END-IF
        //*  060706
        //*  CASH VALUE
        //*  SURV
        if (condition(DbsUtil.maskMatches(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id(),"'MDO'") && pdaCisa600.getCisa600_Pnd_Cis_Mdo_Contract_Cash_Status().equals("C")        //Natural: IF #CIS-RQST-ID = MASK ( 'MDO' ) AND #CIS-MDO-CONTRACT-CASH-STATUS = 'C' AND #CIS-MDO-CONTRACT-TYPE = 'S'
            && pdaCisa600.getCisa600_Pnd_Cis_Mdo_Contract_Type().equals("S")))
        {
            pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1027.2");                                                                                           //Natural: MOVE '1027.2' TO #TIAA-FORM-NUM-PG3-4 #TIAA-FORM-NUM-PG5-6
            pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1027.2");
            pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1027.2");                                                                                          //Natural: MOVE 'C1027.2' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
            pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1027.2");
            pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA MDO-SURV");                                                                                   //Natural: MOVE 'TIAA MDO-SURV' TO #TIAA-PRODUCT-CDE-3-4
            pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA MDO-CASH");                                                                                   //Natural: MOVE 'TIAA MDO-CASH' TO #TIAA-PRODUCT-CDE-5-6
            pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF MDO-SURV");                                                                                   //Natural: MOVE 'CREF MDO-SURV' TO #CREF-PRODUCT-CDE-3-4
            pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF MDO-CASH");                                                                                   //Natural: MOVE 'CREF MDO-CASH' TO #CREF-PRODUCT-CDE-5-6
            pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 7-97");                                                                                      //Natural: MOVE 'Ed. 7-97' TO #TIAA-EDITION-NUM-PG3-4
            pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                              //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6 #CREF-EDITION-NUM-PG3-4 #CREF-EDITION-NUM-PG5-6
            pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());
            pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  060706
            //*  NONCASH
            //*  SURV
            if (condition(DbsUtil.maskMatches(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id(),"'MDO'") && pdaCisa600.getCisa600_Pnd_Cis_Mdo_Contract_Cash_Status().equals("N")    //Natural: IF #CIS-RQST-ID = MASK ( 'MDO' ) AND #CIS-MDO-CONTRACT-CASH-STATUS = 'N' AND #CIS-MDO-CONTRACT-TYPE = 'S'
                && pdaCisa600.getCisa600_Pnd_Cis_Mdo_Contract_Type().equals("S")))
            {
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1028.2");                                                                                       //Natural: MOVE '1028.2' TO #TIAA-FORM-NUM-PG3-4 #TIAA-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1028.2");
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1028.2");                                                                                      //Natural: MOVE 'C1028.2' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1028.2");
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA MDO-SURV");                                                                               //Natural: MOVE 'TIAA MDO-SURV' TO #TIAA-PRODUCT-CDE-3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA MDO-NonCash");                                                                            //Natural: MOVE 'TIAA MDO-NonCash' TO #TIAA-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF MDO-SURV");                                                                               //Natural: MOVE 'CREF MDO-SURV' TO #CREF-PRODUCT-CDE-3-4
                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF MDO-NonCash");                                                                            //Natural: MOVE 'CREF MDO-NonCash' TO #CREF-PRODUCT-CDE-5-6
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 7-97");                                                                                  //Natural: MOVE 'Ed. 7-97' TO #TIAA-EDITION-NUM-PG3-4
                pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                          //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6 #CREF-EDITION-NUM-PG3-4 #CREF-EDITION-NUM-PG5-6
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());
                pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  060706
                //*  CASH VALUE
                if (condition(DbsUtil.maskMatches(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id(),"'MDO'") && pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("SRA")          //Natural: IF #CIS-RQST-ID = MASK ( 'MDO' ) AND #CIS-ANNTY-OPTION = 'SRA' AND #CIS-MDO-CONTRACT-CASH-STATUS = 'C'
                    && pdaCisa600.getCisa600_Pnd_Cis_Mdo_Contract_Cash_Status().equals("C")))
                {
                    pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1027.2");                                                                                   //Natural: MOVE '1027.2' TO #TIAA-FORM-NUM-PG3-4 #TIAA-FORM-NUM-PG5-6
                    pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1027.2");
                    pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1027.2");                                                                                  //Natural: MOVE 'C1027.2' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
                    pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1027.2");
                    pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA MDO-SRA");                                                                            //Natural: MOVE 'TIAA MDO-SRA' TO #TIAA-PRODUCT-CDE-3-4
                    pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA-MDO-CASH");                                                                           //Natural: MOVE 'TIAA-MDO-CASH' TO #TIAA-PRODUCT-CDE-5-6
                    pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF MDO-SRA");                                                                            //Natural: MOVE 'CREF MDO-SRA' TO #CREF-PRODUCT-CDE-3-4
                    pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF MDO-CASH");                                                                           //Natural: MOVE 'CREF MDO-CASH' TO #CREF-PRODUCT-CDE-5-6
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  060706
                    //*  CASH VALUE
                    if (condition(DbsUtil.maskMatches(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id(),"'MDO'") && pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("GSRA")     //Natural: IF #CIS-RQST-ID = MASK ( 'MDO' ) AND #CIS-ANNTY-OPTION = 'GSRA' AND #CIS-MDO-CONTRACT-CASH-STATUS = 'C'
                        && pdaCisa600.getCisa600_Pnd_Cis_Mdo_Contract_Cash_Status().equals("C")))
                    {
                        pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1027.2");                                                                               //Natural: MOVE '1027.2' TO #TIAA-FORM-NUM-PG3-4 #TIAA-FORM-NUM-PG5-6
                        pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1027.2");
                        pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1027.2");                                                                              //Natural: MOVE 'C1027.2' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
                        pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1027.2");
                        pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA MDO-GSRA");                                                                       //Natural: MOVE 'TIAA MDO-GSRA' TO #TIAA-PRODUCT-CDE-3-4
                        pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA MDO-CASH");                                                                       //Natural: MOVE 'TIAA MDO-CASH' TO #TIAA-PRODUCT-CDE-5-6
                        pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF MDO-GSRA");                                                                       //Natural: MOVE 'CREF MDO-GSRA' TO #CREF-PRODUCT-CDE-3-4
                        pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF MDO-CASH");                                                                       //Natural: MOVE 'CREF MDO-CASH' TO #CREF-PRODUCT-CDE-5-6
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  060706
                        //*  CASH VALUE
                        if (condition(DbsUtil.maskMatches(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id(),"'MDO'") && pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("IRA")  //Natural: IF #CIS-RQST-ID = MASK ( 'MDO' ) AND #CIS-ANNTY-OPTION = 'IRA' AND #CIS-MDO-CONTRACT-CASH-STATUS = 'C'
                            && pdaCisa600.getCisa600_Pnd_Cis_Mdo_Contract_Cash_Status().equals("C")))
                        {
                            pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1027.2");                                                                           //Natural: MOVE '1027.2' TO #TIAA-FORM-NUM-PG3-4 #TIAA-FORM-NUM-PG5-6
                            pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1027.2");
                            pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1027.2");                                                                          //Natural: MOVE 'C1027.2' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
                            pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1027.2");
                            pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA MDO-IRA");                                                                    //Natural: MOVE 'TIAA MDO-IRA' TO #TIAA-PRODUCT-CDE-3-4
                            pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA MDO-CASH");                                                                   //Natural: MOVE 'TIAA MDO-CASH' TO #TIAA-PRODUCT-CDE-5-6
                            pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF MDO-IRA");                                                                    //Natural: MOVE 'CREF MDO-IRA' TO #CREF-PRODUCT-CDE-3-4
                            pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF MDO-CASH");                                                                   //Natural: MOVE 'CREF MDO-CASH' TO #CREF-PRODUCT-CDE-5-6
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  060706
                            //*  NONCASH
                            if (condition(DbsUtil.maskMatches(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id(),"'MDO'") && pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("RA")  //Natural: IF #CIS-RQST-ID = MASK ( 'MDO' ) AND #CIS-ANNTY-OPTION = 'RA' AND #CIS-MDO-CONTRACT-CASH-STATUS = 'N'
                                && pdaCisa600.getCisa600_Pnd_Cis_Mdo_Contract_Cash_Status().equals("N")))
                            {
                                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1028.2");                                                                       //Natural: MOVE '1028.2' TO #TIAA-FORM-NUM-PG3-4 #TIAA-FORM-NUM-PG5-6
                                pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1028.2");
                                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1028.2");                                                                      //Natural: MOVE 'C1028.2' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
                                pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1028.2");
                                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA MDO-RA");                                                                 //Natural: MOVE 'TIAA MDO-RA' TO #TIAA-PRODUCT-CDE-3-4
                                pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA MDO-NonCash");                                                            //Natural: MOVE 'TIAA MDO-NonCash' TO #TIAA-PRODUCT-CDE-5-6
                                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF MDO-RA, GRA");                                                            //Natural: MOVE 'CREF MDO-RA, GRA' TO #CREF-PRODUCT-CDE-3-4
                                pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF MDO-NonCash");                                                            //Natural: MOVE 'CREF MDO-NonCash' TO #CREF-PRODUCT-CDE-5-6
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  060706
                                //*  NONCASH
                                if (condition(DbsUtil.maskMatches(pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id(),"'MDO'") && pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().equals("GRA")  //Natural: IF #CIS-RQST-ID = MASK ( 'MDO' ) AND #CIS-ANNTY-OPTION = 'GRA' AND #CIS-MDO-CONTRACT-CASH-STATUS = 'N'
                                    && pdaCisa600.getCisa600_Pnd_Cis_Mdo_Contract_Cash_Status().equals("N")))
                                {
                                    pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1028.2");                                                                   //Natural: MOVE '1028.2' TO #TIAA-FORM-NUM-PG3-4 #TIAA-FORM-NUM-PG5-6
                                    pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1028.2");
                                    pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4().setValue("C1028.2");                                                                  //Natural: MOVE 'C1028.2' TO #CREF-FORM-NUM-PG3-4 #CREF-FORM-NUM-PG5-6
                                    pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6().setValue("C1028.2");
                                    pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA MDO-GRA");                                                            //Natural: MOVE 'TIAA MDO-GRA' TO #TIAA-PRODUCT-CDE-3-4
                                    pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA MDO-NonCash");                                                        //Natural: MOVE 'TIAA MDO-NonCash' TO #TIAA-PRODUCT-CDE-5-6
                                    pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4().setValue("CREF MDO-RA, GRA");                                                        //Natural: MOVE 'CREF MDO-RA, GRA' TO #CREF-PRODUCT-CDE-3-4
                                    pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6().setValue("CREF MDO-NonCash");                                                        //Natural: MOVE 'CREF MDO-NonCash' TO #CREF-PRODUCT-CDE-5-6
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Tpa() throws Exception                                                                                                                          //Natural: MOVE-TPA
    {
        if (BLNatReinput.isReinput()) return;

        pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Your TIAA Transfer Payout Annuity");                                                                   //Natural: MOVE 'Your TIAA Transfer Payout Annuity' TO #TIAA-CNTRCT-HEADER-2
        pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1026");                                                                                                 //Natural: MOVE '1026' TO #TIAA-FORM-NUM-PG3-4
        pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1026");                                                                                                 //Natural: MOVE '1026' TO #TIAA-FORM-NUM-PG5-6
        pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("TIAA TPA");                                                                                            //Natural: MOVE 'TIAA TPA' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
        pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("TIAA TPA");
        pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 1-98", MoveOption.RightJustified);                                                               //Natural: MOVE RIGHT 'Ed. 1-98' TO #TIAA-EDITION-NUM-PG3-4
        pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                                  //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
    }
    private void sub_Move_Ipro() throws Exception                                                                                                                         //Natural: MOVE-IPRO
    {
        if (BLNatReinput.isReinput()) return;

        pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2().setValue("Interest Payment and Retirement Annuity Contract");                                                    //Natural: MOVE 'Interest Payment and Retirement Annuity Contract' TO #TIAA-CNTRCT-HEADER-2
        pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4().setValue("1025");                                                                                                 //Natural: MOVE '1025' TO #TIAA-FORM-NUM-PG3-4
        pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6().setValue("1025");                                                                                                 //Natural: MOVE '1025' TO #TIAA-FORM-NUM-PG5-6
        pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4().setValue("IPRO-DA");                                                                                             //Natural: MOVE 'IPRO-DA' TO #TIAA-PRODUCT-CDE-3-4 #TIAA-PRODUCT-CDE-5-6
        pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6().setValue("IPRO-DA");
        pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4().setValue("Ed. 1-01", MoveOption.RightJustified);                                                               //Natural: MOVE RIGHT 'Ed. 1-01' TO #TIAA-EDITION-NUM-PG3-4
        pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                                                  //Natural: MOVE #TIAA-EDITION-NUM-PG3-4 TO #TIAA-EDITION-NUM-PG5-6
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=62 LS=130");
        Global.format(2, "PS=62 LS=75 ZP=ON");
        Global.format(3, "PS=58 LS=130");
    }
}
