/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:09:34 PM
**        * FROM NATURAL SUBPROGRAM : Cisn601
************************************************************
**        * FILE NAME            : Cisn601.java
**        * CLASS NAME           : Cisn601
**        * INSTANCE NAME        : Cisn601
************************************************************
**-------------------------------------------------------------------*
**                                                                   *
** SYSTEM         : CIS SYSTEM                                       *
** AUTHOR         : CHARLES SINGLETON                                *
** DESCRIPTION    : THIS SUBPROGRAM CALLS THE BENE INTERFACE MODULE  *
**                : BENN970 FROM CISB600.                            *
**                                                                   *
**------------------------------------------------------------------ *
**                       M O D I F I C A T I O N S                   *
**------------------------------------------------------------------ *
**    DATE          MOD BY                 DESC OF CHANGE            *
** 06/08/17         MUKHR    PIN EXPANSION (C420007)     PINE        *
** 04/04/19  B.NEWSOM  BREAKING ACIS DEPENDENCIES OF THE BENE LEGACY *
**                     SYSTEM                             (BADOTBLS) *
**-------------------------------------------------------------------*

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisn601 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCisa601 pdaCisa601;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup bene_File;

    private DbsGroup bene_File_Bene_File_Data;
    private DbsField bene_File_Bene_Intrfcng_Systm;
    private DbsField bene_File_Bene_Rqstng_System;
    private DbsField bene_File_Bene_Intrfce_Bsnss_Dte;
    private DbsField bene_File_Bene_Intrfce_Mgrtn_Ind;
    private DbsField bene_File_Bene_Nmbr_Of_Benes;
    private DbsField bene_File_Bene_Tiaa_Nbr;
    private DbsField bene_File_Bene_Cref_Nbr;
    private DbsField bene_File_Bene_Pin_Nbr;
    private DbsField bene_File_Bene_Lob;
    private DbsField bene_File_Bene_Lob_Type;
    private DbsField bene_File_Bene_Part_Prfx;
    private DbsField bene_File_Bene_Part_Sffx;
    private DbsField bene_File_Bene_Part_First_Nme;
    private DbsField bene_File_Bene_Part_Middle_Nme;
    private DbsField bene_File_Bene_Part_Last_Nme;
    private DbsField bene_File_Bene_Part_Ssn;
    private DbsField bene_File_Bene_Part_Dob;
    private DbsField bene_File_Bene_Estate;
    private DbsField bene_File_Bene_Trust;
    private DbsField bene_File_Bene_Category;
    private DbsField bene_File_Bene_Effective_Dt;
    private DbsField bene_File_Bene_Mos_Ind;
    private DbsField bene_File_Bene_Mos_Irrvcble_Ind;
    private DbsField bene_File_Bene_Pymnt_Chld_Dcsd_Ind;
    private DbsField bene_File_Bene_Update_Dt;
    private DbsField bene_File_Bene_Update_Time;
    private DbsField bene_File_Bene_Update_By;
    private DbsField bene_File_Bene_Record_Status;
    private DbsField bene_File_Bene_Same_As_Ind;
    private DbsField bene_File_Bene_New_Issuefslash_Chng_Ind;
    private DbsField bene_File_Bene_Contract_Type;
    private DbsField bene_File_Bene_Tiaa_Cref_Ind;
    private DbsField bene_File_Bene_Stat;
    private DbsField bene_File_Bene_Dflt_To_Estate;
    private DbsField bene_File_Bene_More_Than_Five_Benes_Ind;
    private DbsField bene_File_Bene_Illgble_Ind;
    private DbsField bene_File_Bene_Exempt_Spouse_Rights;
    private DbsField bene_File_Bene_Spouse_Waived_Bnfts;
    private DbsField bene_File_Bene_Trust_Data_Fldr;
    private DbsField bene_File_Bene_Addr_Fldr;
    private DbsField bene_File_Bene_Co_Owner_Data_Fldr;
    private DbsField bene_File_Bene_Fldr_Log_Dte_Tme;
    private DbsField bene_File_Bene_Fldr_Min;
    private DbsField bene_File_Bene_Fldr_Srce_Id;
    private DbsField bene_File_Bene_Rqst_Timestamp;

    private DbsGroup bene_File__R_Field_1;
    private DbsField bene_File_Bene_Rqst_Date;
    private DbsField bene_File_Bene_Rqst_Time;
    private DbsField bene_File_Bene_Last_Vrfy_Dte;
    private DbsField bene_File_Bene_Last_Vrfy_Tme;
    private DbsField bene_File_Bene_Last_Vrfy_Userid;
    private DbsField bene_File_Bene_Last_Dsgntn_Srce;
    private DbsField bene_File_Bene_Last_Dsgntn_System;

    private DbsGroup bene_File_Bene_Data;
    private DbsField bene_File_Bene_Type;
    private DbsField bene_File_Bene_Name;
    private DbsField bene_File_Bene_Extended_Name;
    private DbsField bene_File_Bene_Ssn_Cd;
    private DbsField bene_File_Bene_Ssn_Nbr;
    private DbsField bene_File_Bene_Dob;
    private DbsField bene_File_Bene_Dte_Birth_Trust;
    private DbsField bene_File_Bene_Relationship_Free_Txt;
    private DbsField bene_File_Bene_Relationship_Cde;
    private DbsField bene_File_Bene_Prctge_Frctn_Ind;
    private DbsField bene_File_Bene_Irrvcbl_Ind;
    private DbsField bene_File_Bene_Alloc_Pct;
    private DbsField bene_File_Bene_Nmrtr_Nbr;
    private DbsField bene_File_Bene_Dnmntr_Nbr;
    private DbsField bene_File_Bene_Std_Txt_Ind;
    private DbsField bene_File_Bene_Sttlmnt_Rstrctn;

    private DbsGroup bene_File_Bene_Addtl_Info;
    private DbsField bene_File_Bene_Addr1;
    private DbsField bene_File_Bene_Addr2;
    private DbsField bene_File_Bene_Addr3_City;
    private DbsField bene_File_Bene_State;
    private DbsField bene_File_Bene_Zip;
    private DbsField bene_File_Bene_Country;
    private DbsField bene_File_Bene_Phone;
    private DbsField bene_File_Bene_Gender;
    private DbsField bene_File_Bene_Spcl_Txt;
    private DbsField bene_File_Bene_Mdo_Calc_Bene;
    private DbsField bene_File_Bene_Spcl_Dsgn_Txt;
    private DbsField bene_File_Bene_Hold_Cde;
    private DbsField bene_File_Pnd_Table_Rltn;

    private DbsGroup bene_File__R_Field_2;
    private DbsField bene_File_Pnd_Rltn_Cd;
    private DbsField bene_File_Pnd_Rltn_Text;
    private DbsField bene_File_Bene_Total_Contract_Written;
    private DbsField bene_File_Bene_Total_Mos_Written;
    private DbsField bene_File_Bene_Total_Dest_Written;
    private DbsField bene_File_Bene_Return_Code;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaCisa601 = new PdaCisa601(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        bene_File = localVariables.newGroupInRecord("bene_File", "BENE-FILE");

        bene_File_Bene_File_Data = bene_File.newGroupInGroup("bene_File_Bene_File_Data", "BENE-FILE-DATA");
        bene_File_Bene_Intrfcng_Systm = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfcng_Systm", "BENE-INTRFCNG-SYSTM", FieldType.STRING, 
            8);
        bene_File_Bene_Rqstng_System = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Rqstng_System", "BENE-RQSTNG-SYSTEM", FieldType.STRING, 
            10);
        bene_File_Bene_Intrfce_Bsnss_Dte = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfce_Bsnss_Dte", "BENE-INTRFCE-BSNSS-DTE", FieldType.STRING, 
            8);
        bene_File_Bene_Intrfce_Mgrtn_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfce_Mgrtn_Ind", "BENE-INTRFCE-MGRTN-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Nmbr_Of_Benes = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Nmbr_Of_Benes", "BENE-NMBR-OF-BENES", FieldType.NUMERIC, 
            3);
        bene_File_Bene_Tiaa_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Tiaa_Nbr", "BENE-TIAA-NBR", FieldType.STRING, 10);
        bene_File_Bene_Cref_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Cref_Nbr", "BENE-CREF-NBR", FieldType.STRING, 10);
        bene_File_Bene_Pin_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Pin_Nbr", "BENE-PIN-NBR", FieldType.NUMERIC, 12);
        bene_File_Bene_Lob = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Lob", "BENE-LOB", FieldType.STRING, 1);
        bene_File_Bene_Lob_Type = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Lob_Type", "BENE-LOB-TYPE", FieldType.STRING, 1);
        bene_File_Bene_Part_Prfx = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Prfx", "BENE-PART-PRFX", FieldType.STRING, 10);
        bene_File_Bene_Part_Sffx = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Sffx", "BENE-PART-SFFX", FieldType.STRING, 10);
        bene_File_Bene_Part_First_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_First_Nme", "BENE-PART-FIRST-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Middle_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Middle_Nme", "BENE-PART-MIDDLE-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Last_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Last_Nme", "BENE-PART-LAST-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Ssn = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Ssn", "BENE-PART-SSN", FieldType.NUMERIC, 9);
        bene_File_Bene_Part_Dob = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Dob", "BENE-PART-DOB", FieldType.NUMERIC, 8);
        bene_File_Bene_Estate = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Estate", "BENE-ESTATE", FieldType.STRING, 1);
        bene_File_Bene_Trust = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Trust", "BENE-TRUST", FieldType.STRING, 1);
        bene_File_Bene_Category = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Category", "BENE-CATEGORY", FieldType.STRING, 1);
        bene_File_Bene_Effective_Dt = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Effective_Dt", "BENE-EFFECTIVE-DT", FieldType.STRING, 8);
        bene_File_Bene_Mos_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Mos_Ind", "BENE-MOS-IND", FieldType.STRING, 1);
        bene_File_Bene_Mos_Irrvcble_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Mos_Irrvcble_Ind", "BENE-MOS-IRRVCBLE-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Pymnt_Chld_Dcsd_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Pymnt_Chld_Dcsd_Ind", "BENE-PYMNT-CHLD-DCSD-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Update_Dt = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_Dt", "BENE-UPDATE-DT", FieldType.STRING, 8);
        bene_File_Bene_Update_Time = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_Time", "BENE-UPDATE-TIME", FieldType.STRING, 7);
        bene_File_Bene_Update_By = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_By", "BENE-UPDATE-BY", FieldType.STRING, 8);
        bene_File_Bene_Record_Status = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Record_Status", "BENE-RECORD-STATUS", FieldType.STRING, 
            1);
        bene_File_Bene_Same_As_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Same_As_Ind", "BENE-SAME-AS-IND", FieldType.STRING, 1);
        bene_File_Bene_New_Issuefslash_Chng_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_New_Issuefslash_Chng_Ind", "BENE-NEW-ISSUE/CHNG-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Contract_Type = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Contract_Type", "BENE-CONTRACT-TYPE", FieldType.STRING, 
            1);
        bene_File_Bene_Tiaa_Cref_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Tiaa_Cref_Ind", "BENE-TIAA-CREF-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Stat = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Stat", "BENE-STAT", FieldType.STRING, 1);
        bene_File_Bene_Dflt_To_Estate = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Dflt_To_Estate", "BENE-DFLT-TO-ESTATE", FieldType.STRING, 
            1);
        bene_File_Bene_More_Than_Five_Benes_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_More_Than_Five_Benes_Ind", "BENE-MORE-THAN-FIVE-BENES-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Illgble_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Illgble_Ind", "BENE-ILLGBLE-IND", FieldType.STRING, 1);
        bene_File_Bene_Exempt_Spouse_Rights = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Exempt_Spouse_Rights", "BENE-EXEMPT-SPOUSE-RIGHTS", 
            FieldType.STRING, 1);
        bene_File_Bene_Spouse_Waived_Bnfts = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Spouse_Waived_Bnfts", "BENE-SPOUSE-WAIVED-BNFTS", 
            FieldType.STRING, 1);
        bene_File_Bene_Trust_Data_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Trust_Data_Fldr", "BENE-TRUST-DATA-FLDR", FieldType.STRING, 
            1);
        bene_File_Bene_Addr_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Addr_Fldr", "BENE-ADDR-FLDR", FieldType.STRING, 1);
        bene_File_Bene_Co_Owner_Data_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Co_Owner_Data_Fldr", "BENE-CO-OWNER-DATA-FLDR", FieldType.STRING, 
            1);
        bene_File_Bene_Fldr_Log_Dte_Tme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Log_Dte_Tme", "BENE-FLDR-LOG-DTE-TME", FieldType.STRING, 
            15);
        bene_File_Bene_Fldr_Min = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Min", "BENE-FLDR-MIN", FieldType.STRING, 11);
        bene_File_Bene_Fldr_Srce_Id = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Srce_Id", "BENE-FLDR-SRCE-ID", FieldType.STRING, 6);
        bene_File_Bene_Rqst_Timestamp = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Rqst_Timestamp", "BENE-RQST-TIMESTAMP", FieldType.STRING, 
            15);

        bene_File__R_Field_1 = bene_File_Bene_File_Data.newGroupInGroup("bene_File__R_Field_1", "REDEFINE", bene_File_Bene_Rqst_Timestamp);
        bene_File_Bene_Rqst_Date = bene_File__R_Field_1.newFieldInGroup("bene_File_Bene_Rqst_Date", "BENE-RQST-DATE", FieldType.STRING, 8);
        bene_File_Bene_Rqst_Time = bene_File__R_Field_1.newFieldInGroup("bene_File_Bene_Rqst_Time", "BENE-RQST-TIME", FieldType.STRING, 7);
        bene_File_Bene_Last_Vrfy_Dte = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Dte", "BENE-LAST-VRFY-DTE", FieldType.STRING, 
            8);
        bene_File_Bene_Last_Vrfy_Tme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Tme", "BENE-LAST-VRFY-TME", FieldType.STRING, 
            7);
        bene_File_Bene_Last_Vrfy_Userid = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Userid", "BENE-LAST-VRFY-USERID", FieldType.STRING, 
            8);
        bene_File_Bene_Last_Dsgntn_Srce = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Dsgntn_Srce", "BENE-LAST-DSGNTN-SRCE", FieldType.STRING, 
            1);
        bene_File_Bene_Last_Dsgntn_System = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Dsgntn_System", "BENE-LAST-DSGNTN-SYSTEM", FieldType.STRING, 
            1);

        bene_File_Bene_Data = bene_File_Bene_File_Data.newGroupInGroup("bene_File_Bene_Data", "BENE-DATA");
        bene_File_Bene_Type = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Type", "BENE-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Name = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Name", "BENE-NAME", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Extended_Name = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Extended_Name", "BENE-EXTENDED-NAME", FieldType.STRING, 
            35, new DbsArrayController(1, 40));
        bene_File_Bene_Ssn_Cd = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Ssn_Cd", "BENE-SSN-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Ssn_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Ssn_Nbr", "BENE-SSN-NBR", FieldType.STRING, 9, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dob = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dob", "BENE-DOB", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dte_Birth_Trust = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dte_Birth_Trust", "BENE-DTE-BIRTH-TRUST", FieldType.STRING, 
            8, new DbsArrayController(1, 40));
        bene_File_Bene_Relationship_Free_Txt = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Relationship_Free_Txt", "BENE-RELATIONSHIP-FREE-TXT", 
            FieldType.STRING, 15, new DbsArrayController(1, 40));
        bene_File_Bene_Relationship_Cde = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Relationship_Cde", "BENE-RELATIONSHIP-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 40));
        bene_File_Bene_Prctge_Frctn_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Prctge_Frctn_Ind", "BENE-PRCTGE-FRCTN-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        bene_File_Bene_Irrvcbl_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Irrvcbl_Ind", "BENE-IRRVCBL-IND", FieldType.STRING, 1, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Alloc_Pct = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Alloc_Pct", "BENE-ALLOC-PCT", FieldType.NUMERIC, 5, 2, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Nmrtr_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Nmrtr_Nbr", "BENE-NMRTR-NBR", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dnmntr_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dnmntr_Nbr", "BENE-DNMNTR-NBR", FieldType.NUMERIC, 3, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Std_Txt_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Std_Txt_Ind", "BENE-STD-TXT-IND", FieldType.STRING, 1, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Sttlmnt_Rstrctn = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Sttlmnt_Rstrctn", "BENE-STTLMNT-RSTRCTN", FieldType.STRING, 
            1, new DbsArrayController(1, 40));

        bene_File_Bene_Addtl_Info = bene_File_Bene_File_Data.newGroupInGroup("bene_File_Bene_Addtl_Info", "BENE-ADDTL-INFO");
        bene_File_Bene_Addr1 = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr1", "BENE-ADDR1", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Addr2 = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr2", "BENE-ADDR2", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Addr3_City = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr3_City", "BENE-ADDR3-CITY", FieldType.STRING, 35, 
            new DbsArrayController(1, 40));
        bene_File_Bene_State = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_State", "BENE-STATE", FieldType.STRING, 2, new DbsArrayController(1, 
            40));
        bene_File_Bene_Zip = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Zip", "BENE-ZIP", FieldType.STRING, 10, new DbsArrayController(1, 
            40));
        bene_File_Bene_Country = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Country", "BENE-COUNTRY", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Phone = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Phone", "BENE-PHONE", FieldType.STRING, 20, new DbsArrayController(1, 
            40));
        bene_File_Bene_Gender = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Gender", "BENE-GENDER", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Spcl_Txt = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Spcl_Txt", "BENE-SPCL-TXT", FieldType.STRING, 72, new 
            DbsArrayController(1, 40, 1, 3));
        bene_File_Bene_Mdo_Calc_Bene = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Mdo_Calc_Bene", "BENE-MDO-CALC-BENE", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        bene_File_Bene_Spcl_Dsgn_Txt = bene_File_Bene_File_Data.newFieldArrayInGroup("bene_File_Bene_Spcl_Dsgn_Txt", "BENE-SPCL-DSGN-TXT", FieldType.STRING, 
            72, new DbsArrayController(1, 60));
        bene_File_Bene_Hold_Cde = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Hold_Cde", "BENE-HOLD-CDE", FieldType.STRING, 8);
        bene_File_Pnd_Table_Rltn = bene_File.newFieldInGroup("bene_File_Pnd_Table_Rltn", "#TABLE-RLTN", FieldType.STRING, 1683);

        bene_File__R_Field_2 = bene_File.newGroupInGroup("bene_File__R_Field_2", "REDEFINE", bene_File_Pnd_Table_Rltn);
        bene_File_Pnd_Rltn_Cd = bene_File__R_Field_2.newFieldArrayInGroup("bene_File_Pnd_Rltn_Cd", "#RLTN-CD", FieldType.STRING, 2, new DbsArrayController(1, 
            99));
        bene_File_Pnd_Rltn_Text = bene_File__R_Field_2.newFieldArrayInGroup("bene_File_Pnd_Rltn_Text", "#RLTN-TEXT", FieldType.STRING, 15, new DbsArrayController(1, 
            99));
        bene_File_Bene_Total_Contract_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Contract_Written", "BENE-TOTAL-CONTRACT-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Total_Mos_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Mos_Written", "BENE-TOTAL-MOS-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Total_Dest_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Dest_Written", "BENE-TOTAL-DEST-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Return_Code = bene_File.newFieldInGroup("bene_File_Bene_Return_Code", "BENE-RETURN-CODE", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cisn601() throws Exception
    {
        super("Cisn601");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM LOAD-BENE-PDA
        sub_Load_Bene_Pda();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-BENE-INTERFACE
        sub_Call_Bene_Interface();
        if (condition(Global.isEscape())) {return;}
        //* *----------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-BENE-PDA
        //* *-------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-BENE-INTERFACE
    }
    private void sub_Load_Bene_Pda() throws Exception                                                                                                                     //Natural: LOAD-BENE-PDA
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        bene_File_Bene_File_Data.reset();                                                                                                                                 //Natural: RESET BENE-FILE-DATA
        bene_File_Bene_Intrfce_Bsnss_Dte.setValue(pdaCisa601.getCisa601_Pnd_Todays_Date());                                                                               //Natural: ASSIGN BENE-INTRFCE-BSNSS-DTE := CISA601.#TODAYS-DATE
        bene_File_Bene_Intrfcng_Systm.setValue("CIS");                                                                                                                    //Natural: ASSIGN BENE-INTRFCNG-SYSTM := 'CIS'
        bene_File_Bene_Rqstng_System.setValue("IA XFER");                                                                                                                 //Natural: ASSIGN BENE-RQSTNG-SYSTEM := 'IA XFER'
        bene_File_Bene_New_Issuefslash_Chng_Ind.setValue("N");                                                                                                            //Natural: ASSIGN BENE-NEW-ISSUE/CHNG-IND := 'N'
        bene_File_Bene_Nmbr_Of_Benes.setValue(1);                                                                                                                         //Natural: ASSIGN BENE-NMBR-OF-BENES := 1
        bene_File_Bene_Pin_Nbr.setValue(pdaCisa601.getCisa601_Pnd_Pin());                                                                                                 //Natural: ASSIGN BENE-PIN-NBR := CISA601.#PIN
        bene_File_Bene_Tiaa_Nbr.setValue(pdaCisa601.getCisa601_Pnd_Tiaa_Cntrct());                                                                                        //Natural: ASSIGN BENE-TIAA-NBR := CISA601.#TIAA-CNTRCT
        bene_File_Bene_Cref_Nbr.setValue(" ");                                                                                                                            //Natural: ASSIGN BENE-CREF-NBR := ' '
        bene_File_Bene_Contract_Type.setValue("I");                                                                                                                       //Natural: ASSIGN BENE-CONTRACT-TYPE := 'I'
        bene_File_Bene_Effective_Dt.setValue(pdaCisa601.getCisa601_Pnd_Todays_Date());                                                                                    //Natural: ASSIGN BENE-EFFECTIVE-DT := CISA601.#TODAYS-DATE
        bene_File_Bene_Tiaa_Cref_Ind.setValue(" ");                                                                                                                       //Natural: ASSIGN BENE-TIAA-CREF-IND := ' '
        bene_File_Bene_Last_Dsgntn_Srce.setValue("I");                                                                                                                    //Natural: ASSIGN BENE-LAST-DSGNTN-SRCE := 'I'
        bene_File_Bene_Last_Dsgntn_System.setValue("C");                                                                                                                  //Natural: ASSIGN BENE-LAST-DSGNTN-SYSTEM := 'C'
        bene_File_Bene_Same_As_Ind.setValue("Y");                                                                                                                         //Natural: ASSIGN BENE-SAME-AS-IND := 'Y'
        bene_File_Bene_Name.getValue(1).setValue(pdaCisa601.getCisa601_Pnd_Bene_Name());                                                                                  //Natural: ASSIGN BENE-NAME ( 1 ) := CISA601.#BENE-NAME
        bene_File_Bene_Type.getValue(1).setValue("P");                                                                                                                    //Natural: ASSIGN BENE-TYPE ( 1 ) := 'P'
        bene_File_Bene_Prctge_Frctn_Ind.getValue(1).setValue("P");                                                                                                        //Natural: ASSIGN BENE-PRCTGE-FRCTN-IND ( 1 ) := 'P'
        bene_File_Bene_Alloc_Pct.getValue(1).setValue(100);                                                                                                               //Natural: ASSIGN BENE-ALLOC-PCT ( 1 ) := 100.0
        bene_File_Bene_Return_Code.setValue(" ");                                                                                                                         //Natural: ASSIGN BENE-RETURN-CODE := ' '
    }
    private void sub_Call_Bene_Interface() throws Exception                                                                                                               //Natural: CALL-BENE-INTERFACE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        DbsUtil.callnat(Filtomdm.class , getCurrentProcessState(), bene_File);                                                                                            //Natural: CALLNAT 'FILTOMDM' BENE-FILE
        if (condition(Global.isEscape())) return;
        pdaCisa601.getCisa601_Pnd_Total_Bene_Contract().setValue(bene_File_Bene_Total_Contract_Written);                                                                  //Natural: ASSIGN #TOTAL-BENE-CONTRACT := BENE-TOTAL-CONTRACT-WRITTEN
        pdaCisa601.getCisa601_Pnd_Total_Bene_Mos().setValue(bene_File_Bene_Total_Mos_Written);                                                                            //Natural: ASSIGN #TOTAL-BENE-MOS := BENE-TOTAL-MOS-WRITTEN
        pdaCisa601.getCisa601_Pnd_Total_Bene_Dest().setValue(bene_File_Bene_Total_Dest_Written);                                                                          //Natural: ASSIGN #TOTAL-BENE-DEST := BENE-TOTAL-DEST-WRITTEN
        pdaCisa601.getCisa601_Pnd_Error_Code().setValue(bene_File_Bene_Return_Code);                                                                                      //Natural: ASSIGN CISA601.#ERROR-CODE := BENE-RETURN-CODE
    }

    //
}
