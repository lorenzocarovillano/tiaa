/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:09:03 PM
**        * FROM NATURAL SUBPROGRAM : Cisn2084
************************************************************
**        * FILE NAME            : Cisn2084.java
**        * CLASS NAME           : Cisn2084
**        * INSTANCE NAME        : Cisn2084
************************************************************
************************************************************************
* DATE         USER ID        CHANGE DESCRIPTION         TAG           *
* 05/09/2017 - GHOSABE      - PIN EXPANSION CHANGES-RESTOW(C420007)PINE*
************************************************************************

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisn2084 extends BLNatBase
{
    // Data Areas
    private LdaCisl2083 ldaCisl2083;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Cis_Tiaa_Nbr;
    private DbsField pnd_Mit_Log_Dte_Time;
    private DbsField pnd_Tiaa_Cref_Ind;
    private DbsField pnd_Found;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCisl2083 = new LdaCisl2083();
        registerRecord(ldaCisl2083);
        registerRecord(ldaCisl2083.getVw_cis_Prtcpnt_File());

        // parameters
        parameters = new DbsRecord();
        pnd_Cis_Tiaa_Nbr = parameters.newFieldInRecord("pnd_Cis_Tiaa_Nbr", "#CIS-TIAA-NBR", FieldType.STRING, 10);
        pnd_Cis_Tiaa_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Mit_Log_Dte_Time = parameters.newFieldInRecord("pnd_Mit_Log_Dte_Time", "#MIT-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_Mit_Log_Dte_Time.setParameterOption(ParameterOption.ByReference);
        pnd_Tiaa_Cref_Ind = parameters.newFieldInRecord("pnd_Tiaa_Cref_Ind", "#TIAA-CREF-IND", FieldType.STRING, 1);
        pnd_Tiaa_Cref_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_Found = parameters.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Found.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCisl2083.initializeValues();

        parameters.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cisn2084() throws Exception
    {
        super("Cisn2084");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM GET-MIT-LOG-DTE-TIME
        sub_Get_Mit_Log_Dte_Time();
        if (condition(Global.isEscape())) {return;}
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MIT-LOG-DTE-TIME
    }
    private void sub_Get_Mit_Log_Dte_Time() throws Exception                                                                                                              //Natural: GET-MIT-LOG-DTE-TIME
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tiaa_Cref_Ind.reset();                                                                                                                                        //Natural: RESET #TIAA-CREF-IND
        ldaCisl2083.getVw_cis_Prtcpnt_File().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) CIS-PRTCPNT-FILE BY CIS-TIAA-NBR EQ #CIS-TIAA-NBR
        (
        "READ01",
        new Wc[] { new Wc("CIS_TIAA_NBR", ">=", pnd_Cis_Tiaa_Nbr, WcType.BY) },
        new Oc[] { new Oc("CIS_TIAA_NBR", "ASC") },
        1
        );
        READ01:
        while (condition(ldaCisl2083.getVw_cis_Prtcpnt_File().readNextRow("READ01")))
        {
            if (condition(ldaCisl2083.getCis_Prtcpnt_File_Cis_Tiaa_Nbr().notEquals(pnd_Cis_Tiaa_Nbr)))                                                                    //Natural: IF CIS-TIAA-NBR NE #CIS-TIAA-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Found.setValue(true);                                                                                                                                     //Natural: ASSIGN #FOUND := TRUE
            pnd_Tiaa_Cref_Ind.setValue(ldaCisl2083.getCis_Prtcpnt_File_Cis_Cntrct_Type());                                                                                //Natural: ASSIGN #TIAA-CREF-IND := CIS-CNTRCT-TYPE
            pnd_Mit_Log_Dte_Time.setValue(ldaCisl2083.getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Log_Dte_Tme());                                                                //Natural: ASSIGN #MIT-LOG-DTE-TIME := CIS-RQST-SYS-MIT-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
