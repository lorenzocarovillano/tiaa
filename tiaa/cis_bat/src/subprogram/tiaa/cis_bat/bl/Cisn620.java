/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:09:36 PM
**        * FROM NATURAL SUBPROGRAM : Cisn620
************************************************************
**        * FILE NAME            : Cisn620.java
**        * CLASS NAME           : Cisn620
**        * INSTANCE NAME        : Cisn620
************************************************************
**----------------------------------------------------------------------
*  PROGRAM-ID  :  CISN620
*
*  DESCRIPTION :  FORMAT BENEFICIARY INFORMATION
*
*  CALLED BY   :  CISB610
*
*  NOTE        :  THIS PROGRAM CONTAINS LOWERCASE TEXT
*
*  UPDATED     :  NEW PROGRAM - COPY OF CISN5000.
*                 DEVELBISS 9/3/09
* 05/12/17  (BABRE)   PIN EXPANSION CHANGES. (C420007) STOW ONLY
**----------------------------------------------------------------------
*

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisn620 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCisa620 pdaCisa620;
    private LdaCisl5000 ldaCisl5000;
    private LdaCisl5001 ldaCisl5001;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Pr_Line;

    private DbsGroup pnd_Pr_Line__R_Field_1;
    private DbsField pnd_Pr_Line_Pnd_Pr_Lump_Auto_Ind;
    private DbsField pnd_Pr_Line_Pnd_Pr_Child_Prvsn;
    private DbsField pnd_Pr_Line_Pnd_Pr_Child_Prvsn_Fill;
    private DbsField pnd_Pr_Line_Pnd_Pr_Text;

    private DbsGroup pnd_Pr_Line__R_Field_2;
    private DbsField pnd_Pr_Line_Pnd_Pr_Name;
    private DbsField pnd_Pr_Line_Filler_1;
    private DbsField pnd_Pr_Line_Pnd_Pr_Soc;
    private DbsField pnd_Pr_Line_Filler_2;
    private DbsField pnd_Pr_Line_Pnd_Pr_Dob;
    private DbsField pnd_Pr_Line_Filler_3;
    private DbsField pnd_Pr_Line_Pnd_Pr_Relationship;
    private DbsField pnd_Pr_Line_Filler_4;
    private DbsField pnd_Pr_Line_Pnd_Pr_Share;
    private DbsField pnd_Pr_Text_1;

    private DbsGroup pnd_Pr_Text_1__R_Field_3;
    private DbsField pnd_Pr_Text_1_Pnd_Pr_Name_1;
    private DbsField pnd_Pr_Text_1_Filler_1;
    private DbsField pnd_Pr_Text_1_Pnd_Pr_Relationship_1;
    private DbsField pnd_Pr_Text_1_Filler_2;
    private DbsField pnd_Pr_Text_1_Pnd_Pr_Share_1;
    private DbsField pnd_Pr_Text_1_Filler_3;
    private DbsField pnd_Pr_Text_2;

    private DbsGroup pnd_Pr_Text_2__R_Field_4;
    private DbsField pnd_Pr_Text_2_Pnd_Pr_Name_2;
    private DbsField pnd_Pr_Text_2_Filler_1;
    private DbsField pnd_Pr_Text_2_Pnd_Pr_Relationship_2;
    private DbsField pnd_Pr_Text_2_Filler_2;

    private DbsGroup pnd_Sort_Key_Cnt;
    private DbsField pnd_Sort_Key_Cnt_Pnd_Sort_Key;
    private DbsField pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_1_Cnt;
    private DbsField pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_2_Cnt;
    private DbsField pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_3_Cnt;
    private DbsField pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_1_Cnt;
    private DbsField pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_2_Cnt;
    private DbsField pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_3_Cnt;

    private DbsGroup pnd_Ctrs;
    private DbsField pnd_Ctrs_Pnd_W_Prmry_Std_Cnt;
    private DbsField pnd_Ctrs_Pnd_W_Prmry_Std_With_Text_Cnt;
    private DbsField pnd_Ctrs_Pnd_W_Prmry_Special_Text_Cnt;
    private DbsField pnd_Ctrs_Pnd_W_Prmry_Lump_Sum_Cnt;
    private DbsField pnd_Ctrs_Pnd_W_Prmry_Auto_Cmnt_Val_Cnt;
    private DbsField pnd_Ctrs_Pnd_W_Prmry_Cnt;

    private DbsGroup pnd_Ctrs_Pnd_W_Prmry_Table;
    private DbsField pnd_Ctrs_Pnd_W_Prmry_Sort_Key;
    private DbsField pnd_Ctrs_Pnd_W_Cntgnt_Std_Cnt;
    private DbsField pnd_Ctrs_Pnd_W_Cntgnt_Std_With_Text_Cnt;
    private DbsField pnd_Ctrs_Pnd_W_Cntgnt_Special_Text_Cnt;
    private DbsField pnd_Ctrs_Pnd_W_Cntgnt_Lump_Sum_Cnt;
    private DbsField pnd_Ctrs_Pnd_W_Cntgnt_Auto_Cmnt_Val_Cnt;
    private DbsField pnd_Ctrs_Pnd_W_Cntgnt_Cnt;

    private DbsGroup pnd_Ctrs_Pnd_W_Cntgnt_Table;
    private DbsField pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key;
    private DbsField pnd_Temp_Sort_Key;
    private DbsField pnd_Temp_Print_Line;
    private DbsField pnd_C_Cis_Prmry_Bene_Spcl_Txt;
    private DbsField pnd_C_Cis_Cntgnt_Bene_Spcl_Txt;
    private DbsField pnd_Found;
    private DbsField pnd_Lump_Auto_Ind;
    private DbsField pnd_Child_Prvsn_Ind;
    private DbsField pnd_Child_Prvsn_Ind_Fill;
    private DbsField pnd_Child_Prvsn_Indc;
    private DbsField pnd_Calc_Bene;
    private DbsField pnd_Soc;

    private DbsGroup pnd_Soc__R_Field_5;
    private DbsField pnd_Soc_Pnd_Soc_A;
    private DbsField pnd_I;
    private DbsField pnd_I2;
    private DbsField pnd_J;
    private DbsField pnd_J2;
    private DbsField pnd_K;
    private DbsField pnd_Retry_Ctr;
    private DbsField pnd_Debug;
    private DbsField pnd_Lump_Sum_Break;

    private DbsGroup pnd_Lump_Sum_Break__R_Field_6;
    private DbsField pnd_Lump_Sum_Break_Pnd_Filler1;
    private DbsField pnd_Lump_Sum_Break_Pnd_Filler2;
    private DbsField pnd_Name;

    private DbsGroup pnd_Name__R_Field_7;
    private DbsField pnd_Name_Pnd_Name_Arr;
    private DbsField pnd_M;
    private DbsField pnd_L;
    private DbsField pnd_P;
    private DbsField pnd_N;
    private DbsField pnd_Nmrtr;
    private DbsField pnd_Dnmrtr;
    private DbsField pnd_Result;
    private DbsField pnd_Remainder;
    private DbsField pnd_Free_Form_Line;
    private DbsField pnd_Rqst_Id_Key;
    private DbsField pnd_Prmry_Extended_Name;
    private DbsField pnd_Cntgnt_Extended_Name;
    private DbsField pnd_Found_Extended;
    private DbsField pnd_Occurence;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCisl5000 = new LdaCisl5000();
        registerRecord(ldaCisl5000);
        registerRecord(ldaCisl5000.getVw_cis_Bene_File_01());
        ldaCisl5001 = new LdaCisl5001();
        registerRecord(ldaCisl5001);
        registerRecord(ldaCisl5001.getVw_cis_Bene_File_02());

        // parameters
        parameters = new DbsRecord();
        pdaCisa620 = new PdaCisa620(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Pr_Line = localVariables.newFieldInRecord("pnd_Pr_Line", "#PR-LINE", FieldType.STRING, 85);

        pnd_Pr_Line__R_Field_1 = localVariables.newGroupInRecord("pnd_Pr_Line__R_Field_1", "REDEFINE", pnd_Pr_Line);
        pnd_Pr_Line_Pnd_Pr_Lump_Auto_Ind = pnd_Pr_Line__R_Field_1.newFieldInGroup("pnd_Pr_Line_Pnd_Pr_Lump_Auto_Ind", "#PR-LUMP-AUTO-IND", FieldType.STRING, 
            2);
        pnd_Pr_Line_Pnd_Pr_Child_Prvsn = pnd_Pr_Line__R_Field_1.newFieldInGroup("pnd_Pr_Line_Pnd_Pr_Child_Prvsn", "#PR-CHILD-PRVSN", FieldType.STRING, 
            1);
        pnd_Pr_Line_Pnd_Pr_Child_Prvsn_Fill = pnd_Pr_Line__R_Field_1.newFieldInGroup("pnd_Pr_Line_Pnd_Pr_Child_Prvsn_Fill", "#PR-CHILD-PRVSN-FILL", FieldType.STRING, 
            1);
        pnd_Pr_Line_Pnd_Pr_Text = pnd_Pr_Line__R_Field_1.newFieldInGroup("pnd_Pr_Line_Pnd_Pr_Text", "#PR-TEXT", FieldType.STRING, 81);

        pnd_Pr_Line__R_Field_2 = pnd_Pr_Line__R_Field_1.newGroupInGroup("pnd_Pr_Line__R_Field_2", "REDEFINE", pnd_Pr_Line_Pnd_Pr_Text);
        pnd_Pr_Line_Pnd_Pr_Name = pnd_Pr_Line__R_Field_2.newFieldInGroup("pnd_Pr_Line_Pnd_Pr_Name", "#PR-NAME", FieldType.STRING, 35);
        pnd_Pr_Line_Filler_1 = pnd_Pr_Line__R_Field_2.newFieldInGroup("pnd_Pr_Line_Filler_1", "FILLER-1", FieldType.STRING, 1);
        pnd_Pr_Line_Pnd_Pr_Soc = pnd_Pr_Line__R_Field_2.newFieldInGroup("pnd_Pr_Line_Pnd_Pr_Soc", "#PR-SOC", FieldType.STRING, 11);
        pnd_Pr_Line_Filler_2 = pnd_Pr_Line__R_Field_2.newFieldInGroup("pnd_Pr_Line_Filler_2", "FILLER-2", FieldType.STRING, 3);
        pnd_Pr_Line_Pnd_Pr_Dob = pnd_Pr_Line__R_Field_2.newFieldInGroup("pnd_Pr_Line_Pnd_Pr_Dob", "#PR-DOB", FieldType.STRING, 10);
        pnd_Pr_Line_Filler_3 = pnd_Pr_Line__R_Field_2.newFieldInGroup("pnd_Pr_Line_Filler_3", "FILLER-3", FieldType.STRING, 2);
        pnd_Pr_Line_Pnd_Pr_Relationship = pnd_Pr_Line__R_Field_2.newFieldInGroup("pnd_Pr_Line_Pnd_Pr_Relationship", "#PR-RELATIONSHIP", FieldType.STRING, 
            10);
        pnd_Pr_Line_Filler_4 = pnd_Pr_Line__R_Field_2.newFieldInGroup("pnd_Pr_Line_Filler_4", "FILLER-4", FieldType.STRING, 2);
        pnd_Pr_Line_Pnd_Pr_Share = pnd_Pr_Line__R_Field_2.newFieldInGroup("pnd_Pr_Line_Pnd_Pr_Share", "#PR-SHARE", FieldType.STRING, 7);
        pnd_Pr_Text_1 = localVariables.newFieldInRecord("pnd_Pr_Text_1", "#PR-TEXT-1", FieldType.STRING, 81);

        pnd_Pr_Text_1__R_Field_3 = localVariables.newGroupInRecord("pnd_Pr_Text_1__R_Field_3", "REDEFINE", pnd_Pr_Text_1);
        pnd_Pr_Text_1_Pnd_Pr_Name_1 = pnd_Pr_Text_1__R_Field_3.newFieldInGroup("pnd_Pr_Text_1_Pnd_Pr_Name_1", "#PR-NAME-1", FieldType.STRING, 35);
        pnd_Pr_Text_1_Filler_1 = pnd_Pr_Text_1__R_Field_3.newFieldInGroup("pnd_Pr_Text_1_Filler_1", "FILLER-1", FieldType.STRING, 1);
        pnd_Pr_Text_1_Pnd_Pr_Relationship_1 = pnd_Pr_Text_1__R_Field_3.newFieldInGroup("pnd_Pr_Text_1_Pnd_Pr_Relationship_1", "#PR-RELATIONSHIP-1", FieldType.STRING, 
            10);
        pnd_Pr_Text_1_Filler_2 = pnd_Pr_Text_1__R_Field_3.newFieldInGroup("pnd_Pr_Text_1_Filler_2", "FILLER-2", FieldType.STRING, 5);
        pnd_Pr_Text_1_Pnd_Pr_Share_1 = pnd_Pr_Text_1__R_Field_3.newFieldInGroup("pnd_Pr_Text_1_Pnd_Pr_Share_1", "#PR-SHARE-1", FieldType.STRING, 7);
        pnd_Pr_Text_1_Filler_3 = pnd_Pr_Text_1__R_Field_3.newFieldInGroup("pnd_Pr_Text_1_Filler_3", "FILLER-3", FieldType.STRING, 23);
        pnd_Pr_Text_2 = localVariables.newFieldInRecord("pnd_Pr_Text_2", "#PR-TEXT-2", FieldType.STRING, 81);

        pnd_Pr_Text_2__R_Field_4 = localVariables.newGroupInRecord("pnd_Pr_Text_2__R_Field_4", "REDEFINE", pnd_Pr_Text_2);
        pnd_Pr_Text_2_Pnd_Pr_Name_2 = pnd_Pr_Text_2__R_Field_4.newFieldInGroup("pnd_Pr_Text_2_Pnd_Pr_Name_2", "#PR-NAME-2", FieldType.STRING, 35);
        pnd_Pr_Text_2_Filler_1 = pnd_Pr_Text_2__R_Field_4.newFieldInGroup("pnd_Pr_Text_2_Filler_1", "FILLER-1", FieldType.STRING, 27);
        pnd_Pr_Text_2_Pnd_Pr_Relationship_2 = pnd_Pr_Text_2__R_Field_4.newFieldInGroup("pnd_Pr_Text_2_Pnd_Pr_Relationship_2", "#PR-RELATIONSHIP-2", FieldType.STRING, 
            10);
        pnd_Pr_Text_2_Filler_2 = pnd_Pr_Text_2__R_Field_4.newFieldInGroup("pnd_Pr_Text_2_Filler_2", "FILLER-2", FieldType.STRING, 7);

        pnd_Sort_Key_Cnt = localVariables.newGroupInRecord("pnd_Sort_Key_Cnt", "#SORT-KEY-CNT");
        pnd_Sort_Key_Cnt_Pnd_Sort_Key = pnd_Sort_Key_Cnt.newFieldInGroup("pnd_Sort_Key_Cnt_Pnd_Sort_Key", "#SORT-KEY", FieldType.NUMERIC, 3);
        pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_1_Cnt = pnd_Sort_Key_Cnt.newFieldInGroup("pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_1_Cnt", "#PRMRY-SORT-KEY-1-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_2_Cnt = pnd_Sort_Key_Cnt.newFieldInGroup("pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_2_Cnt", "#PRMRY-SORT-KEY-2-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_3_Cnt = pnd_Sort_Key_Cnt.newFieldInGroup("pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_3_Cnt", "#PRMRY-SORT-KEY-3-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_1_Cnt = pnd_Sort_Key_Cnt.newFieldInGroup("pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_1_Cnt", "#CNTGNT-SORT-KEY-1-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_2_Cnt = pnd_Sort_Key_Cnt.newFieldInGroup("pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_2_Cnt", "#CNTGNT-SORT-KEY-2-CNT", 
            FieldType.NUMERIC, 3);
        pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_3_Cnt = pnd_Sort_Key_Cnt.newFieldInGroup("pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_3_Cnt", "#CNTGNT-SORT-KEY-3-CNT", 
            FieldType.NUMERIC, 3);

        pnd_Ctrs = localVariables.newGroupInRecord("pnd_Ctrs", "#CTRS");
        pnd_Ctrs_Pnd_W_Prmry_Std_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Prmry_Std_Cnt", "#W-PRMRY-STD-CNT", FieldType.NUMERIC, 2);
        pnd_Ctrs_Pnd_W_Prmry_Std_With_Text_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Prmry_Std_With_Text_Cnt", "#W-PRMRY-STD-WITH-TEXT-CNT", FieldType.NUMERIC, 
            2);
        pnd_Ctrs_Pnd_W_Prmry_Special_Text_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Prmry_Special_Text_Cnt", "#W-PRMRY-SPECIAL-TEXT-CNT", FieldType.NUMERIC, 
            2);
        pnd_Ctrs_Pnd_W_Prmry_Lump_Sum_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Prmry_Lump_Sum_Cnt", "#W-PRMRY-LUMP-SUM-CNT", FieldType.NUMERIC, 
            2);
        pnd_Ctrs_Pnd_W_Prmry_Auto_Cmnt_Val_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Prmry_Auto_Cmnt_Val_Cnt", "#W-PRMRY-AUTO-CMNT-VAL-CNT", FieldType.NUMERIC, 
            2);
        pnd_Ctrs_Pnd_W_Prmry_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Prmry_Cnt", "#W-PRMRY-CNT", FieldType.NUMERIC, 2);

        pnd_Ctrs_Pnd_W_Prmry_Table = pnd_Ctrs.newGroupArrayInGroup("pnd_Ctrs_Pnd_W_Prmry_Table", "#W-PRMRY-TABLE", new DbsArrayController(1, 80));
        pnd_Ctrs_Pnd_W_Prmry_Sort_Key = pnd_Ctrs_Pnd_W_Prmry_Table.newFieldInGroup("pnd_Ctrs_Pnd_W_Prmry_Sort_Key", "#W-PRMRY-SORT-KEY", FieldType.NUMERIC, 
            3);
        pnd_Ctrs_Pnd_W_Cntgnt_Std_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Cntgnt_Std_Cnt", "#W-CNTGNT-STD-CNT", FieldType.NUMERIC, 2);
        pnd_Ctrs_Pnd_W_Cntgnt_Std_With_Text_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Cntgnt_Std_With_Text_Cnt", "#W-CNTGNT-STD-WITH-TEXT-CNT", FieldType.NUMERIC, 
            2);
        pnd_Ctrs_Pnd_W_Cntgnt_Special_Text_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Cntgnt_Special_Text_Cnt", "#W-CNTGNT-SPECIAL-TEXT-CNT", FieldType.NUMERIC, 
            2);
        pnd_Ctrs_Pnd_W_Cntgnt_Lump_Sum_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Cntgnt_Lump_Sum_Cnt", "#W-CNTGNT-LUMP-SUM-CNT", FieldType.NUMERIC, 
            2);
        pnd_Ctrs_Pnd_W_Cntgnt_Auto_Cmnt_Val_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Cntgnt_Auto_Cmnt_Val_Cnt", "#W-CNTGNT-AUTO-CMNT-VAL-CNT", FieldType.NUMERIC, 
            2);
        pnd_Ctrs_Pnd_W_Cntgnt_Cnt = pnd_Ctrs.newFieldInGroup("pnd_Ctrs_Pnd_W_Cntgnt_Cnt", "#W-CNTGNT-CNT", FieldType.NUMERIC, 2);

        pnd_Ctrs_Pnd_W_Cntgnt_Table = pnd_Ctrs.newGroupArrayInGroup("pnd_Ctrs_Pnd_W_Cntgnt_Table", "#W-CNTGNT-TABLE", new DbsArrayController(1, 80));
        pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key = pnd_Ctrs_Pnd_W_Cntgnt_Table.newFieldInGroup("pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key", "#W-CNTGNT-SORT-KEY", FieldType.NUMERIC, 
            3);
        pnd_Temp_Sort_Key = localVariables.newFieldInRecord("pnd_Temp_Sort_Key", "#TEMP-SORT-KEY", FieldType.NUMERIC, 3);
        pnd_Temp_Print_Line = localVariables.newFieldInRecord("pnd_Temp_Print_Line", "#TEMP-PRINT-LINE", FieldType.STRING, 85);
        pnd_C_Cis_Prmry_Bene_Spcl_Txt = localVariables.newFieldInRecord("pnd_C_Cis_Prmry_Bene_Spcl_Txt", "#C-CIS-PRMRY-BENE-SPCL-TXT", FieldType.PACKED_DECIMAL, 
            1);
        pnd_C_Cis_Cntgnt_Bene_Spcl_Txt = localVariables.newFieldInRecord("pnd_C_Cis_Cntgnt_Bene_Spcl_Txt", "#C-CIS-CNTGNT-BENE-SPCL-TXT", FieldType.PACKED_DECIMAL, 
            1);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Lump_Auto_Ind = localVariables.newFieldInRecord("pnd_Lump_Auto_Ind", "#LUMP-AUTO-IND", FieldType.STRING, 2);
        pnd_Child_Prvsn_Ind = localVariables.newFieldInRecord("pnd_Child_Prvsn_Ind", "#CHILD-PRVSN-IND", FieldType.STRING, 1);
        pnd_Child_Prvsn_Ind_Fill = localVariables.newFieldInRecord("pnd_Child_Prvsn_Ind_Fill", "#CHILD-PRVSN-IND-FILL", FieldType.STRING, 1);
        pnd_Child_Prvsn_Indc = localVariables.newFieldInRecord("pnd_Child_Prvsn_Indc", "#CHILD-PRVSN-INDC", FieldType.BOOLEAN, 1);
        pnd_Calc_Bene = localVariables.newFieldInRecord("pnd_Calc_Bene", "#CALC-BENE", FieldType.BOOLEAN, 1);
        pnd_Soc = localVariables.newFieldInRecord("pnd_Soc", "#SOC", FieldType.NUMERIC, 9);

        pnd_Soc__R_Field_5 = localVariables.newGroupInRecord("pnd_Soc__R_Field_5", "REDEFINE", pnd_Soc);
        pnd_Soc_Pnd_Soc_A = pnd_Soc__R_Field_5.newFieldInGroup("pnd_Soc_Pnd_Soc_A", "#SOC-A", FieldType.STRING, 9);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 2);
        pnd_J2 = localVariables.newFieldInRecord("pnd_J2", "#J2", FieldType.PACKED_DECIMAL, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 2);
        pnd_Retry_Ctr = localVariables.newFieldInRecord("pnd_Retry_Ctr", "#RETRY-CTR", FieldType.PACKED_DECIMAL, 5);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Lump_Sum_Break = localVariables.newFieldInRecord("pnd_Lump_Sum_Break", "#LUMP-SUM-BREAK", FieldType.STRING, 2);

        pnd_Lump_Sum_Break__R_Field_6 = localVariables.newGroupInRecord("pnd_Lump_Sum_Break__R_Field_6", "REDEFINE", pnd_Lump_Sum_Break);
        pnd_Lump_Sum_Break_Pnd_Filler1 = pnd_Lump_Sum_Break__R_Field_6.newFieldInGroup("pnd_Lump_Sum_Break_Pnd_Filler1", "#FILLER1", FieldType.STRING, 
            1);
        pnd_Lump_Sum_Break_Pnd_Filler2 = pnd_Lump_Sum_Break__R_Field_6.newFieldInGroup("pnd_Lump_Sum_Break_Pnd_Filler2", "#FILLER2", FieldType.STRING, 
            1);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 35);

        pnd_Name__R_Field_7 = localVariables.newGroupInRecord("pnd_Name__R_Field_7", "REDEFINE", pnd_Name);
        pnd_Name_Pnd_Name_Arr = pnd_Name__R_Field_7.newFieldArrayInGroup("pnd_Name_Pnd_Name_Arr", "#NAME-ARR", FieldType.STRING, 1, new DbsArrayController(1, 
            35));
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 3);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.NUMERIC, 3);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.NUMERIC, 3);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.NUMERIC, 3);
        pnd_Nmrtr = localVariables.newFieldInRecord("pnd_Nmrtr", "#NMRTR", FieldType.NUMERIC, 5);
        pnd_Dnmrtr = localVariables.newFieldInRecord("pnd_Dnmrtr", "#DNMRTR", FieldType.NUMERIC, 5);
        pnd_Result = localVariables.newFieldInRecord("pnd_Result", "#RESULT", FieldType.NUMERIC, 5);
        pnd_Remainder = localVariables.newFieldInRecord("pnd_Remainder", "#REMAINDER", FieldType.NUMERIC, 5);
        pnd_Free_Form_Line = localVariables.newFieldInRecord("pnd_Free_Form_Line", "#FREE-FORM-LINE", FieldType.NUMERIC, 3);
        pnd_Rqst_Id_Key = localVariables.newFieldInRecord("pnd_Rqst_Id_Key", "#RQST-ID-KEY", FieldType.STRING, 30);
        pnd_Prmry_Extended_Name = localVariables.newFieldInRecord("pnd_Prmry_Extended_Name", "#PRMRY-EXTENDED-NAME", FieldType.STRING, 35);
        pnd_Cntgnt_Extended_Name = localVariables.newFieldInRecord("pnd_Cntgnt_Extended_Name", "#CNTGNT-EXTENDED-NAME", FieldType.STRING, 35);
        pnd_Found_Extended = localVariables.newFieldInRecord("pnd_Found_Extended", "#FOUND-EXTENDED", FieldType.BOOLEAN, 1);
        pnd_Occurence = localVariables.newFieldInRecord("pnd_Occurence", "#OCCURENCE", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCisl5000.initializeValues();
        ldaCisl5001.initializeValues();

        localVariables.reset();
        pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_1_Cnt.setInitialValue(100);
        pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_2_Cnt.setInitialValue(200);
        pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_3_Cnt.setInitialValue(300);
        pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_1_Cnt.setInitialValue(100);
        pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_2_Cnt.setInitialValue(200);
        pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_3_Cnt.setInitialValue(300);
        pnd_C_Cis_Prmry_Bene_Spcl_Txt.setInitialValue(3);
        pnd_C_Cis_Cntgnt_Bene_Spcl_Txt.setInitialValue(3);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cisn620() throws Exception
    {
        super("Cisn620");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CISN620", onError);
        setupReports();
        pdaCisa620.getCisa620_Pnd_Cisa_Return_Code().reset();                                                                                                             //Natural: FORMAT PS = 60 LS = 132;//Natural: RESET #CISA-RETURN-CODE #CISA-MSG
        pdaCisa620.getCisa620_Pnd_Cisa_Msg().reset();
        //*                                                                                                                                                               //Natural: ON ERROR
        //* *----------------------------------------------------------------------
        //*                             MAINLINE
        //* *----------------------------------------------------------------------
        //*  VCR
        pnd_Debug.setValue(true);                                                                                                                                         //Natural: MOVE TRUE TO #DEBUG
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, Global.getPROGRAM(),"(0810) START OF MAINLINE ...");                                                                                    //Natural: WRITE *PROGRAM '(0810) START OF MAINLINE ...'
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa620.getCisa620_Pnd_Cisa_Bene_Tiaa_Nbr());                                                                                    //Natural: WRITE '=' #CISA-BENE-TIAA-NBR
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa620.getCisa620_Pnd_Cisa_Bene_Cref_Nbr());                                                                                    //Natural: WRITE '=' #CISA-BENE-CREF-NBR
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa620.getCisa620_Pnd_Cisa_Bene_Rqst_Id());                                                                                     //Natural: WRITE '=' #CISA-BENE-RQST-ID
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa620.getCisa620_Pnd_Cisa_Bene_Unique_Id_Nbr());                                                                               //Natural: WRITE '=' #CISA-BENE-UNIQUE-ID-NBR
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa620.getCisa620_Pnd_Cisa_Bene_Rqst_Id_Key());                                                                                 //Natural: WRITE '=' #CISA-BENE-RQST-ID-KEY
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl5000.getPnd_Cis_Ben_Super_1().reset();                                                                                                                     //Natural: RESET #CIS-BEN-SUPER-1 #FOUND
        pnd_Found.reset();
        ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Rcrd_Type_Cde().setValue("1");                                                                                       //Natural: ASSIGN #S-CIS-RCRD-TYPE-CDE := '1'
        ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Rqst_Id().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Bene_Rqst_Id());                                              //Natural: ASSIGN #S-CIS-BENE-RQST-ID := #CISA-BENE-RQST-ID
        ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Unique_Id_Nbr().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Bene_Unique_Id_Nbr());                                  //Natural: ASSIGN #S-CIS-BENE-UNIQUE-ID-NBR := #CISA-BENE-UNIQUE-ID-NBR
        ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Tiaa_Nbr().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Bene_Tiaa_Nbr());                                            //Natural: ASSIGN #S-CIS-BENE-TIAA-NBR := #CISA-BENE-TIAA-NBR
        getReports().write(0, "KEY ",ldaCisl5000.getPnd_Cis_Ben_Super_1());                                                                                               //Natural: WRITE 'KEY ' #CIS-BEN-SUPER-1
        if (Global.isEscape()) return;
        ldaCisl5000.getVw_cis_Bene_File_01().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) CIS-BENE-FILE-01 WITH CIS-BEN-SUPER-1 = #CIS-BEN-SUPER-1
        (
        "READ01",
        new Wc[] { new Wc("CIS_BEN_SUPER_1", ">=", ldaCisl5000.getPnd_Cis_Ben_Super_1(), WcType.BY) },
        new Oc[] { new Oc("CIS_BEN_SUPER_1", "ASC") },
        1
        );
        READ01:
        while (condition(ldaCisl5000.getVw_cis_Bene_File_01().readNextRow("READ01")))
        {
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id().notEquals(ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Rqst_Id()) ||                    //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID NE #S-CIS-BENE-RQST-ID OR CIS-BENE-FILE-01.CIS-BENE-UNIQUE-ID-NBR NE #S-CIS-BENE-UNIQUE-ID-NBR OR CIS-BENE-FILE-01.CIS-BENE-TIAA-NBR NE #S-CIS-BENE-TIAA-NBR
                ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Unique_Id_Nbr().notEquals(ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Unique_Id_Nbr()) || 
                ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr().notEquals(ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Tiaa_Nbr())))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Found.setValue(true);                                                                                                                                     //Natural: ASSIGN #FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM RTN-1-MOVE-FIELDS-TO-PDA
            sub_Rtn_1_Move_Fields_To_Pda();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM RTN-2-FORMAT-BENE-INFO
            sub_Rtn_2_Format_Bene_Info();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  SORT IF STANDARD FORMAT
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Std_Free().equals("N")))                                                                               //Natural: IF CIS-BENE-STD-FREE = 'N'
            {
                //*  STD
                //*  STD W/ TXT
                //*  SPCL TXT
                pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_Ctrs_Pnd_W_Prmry_Std_Cnt.add(pnd_Ctrs_Pnd_W_Prmry_Std_With_Text_Cnt).add(pnd_Ctrs_Pnd_W_Prmry_Special_Text_Cnt)); //Natural: COMPUTE #I = #W-PRMRY-STD-CNT + #W-PRMRY-STD-WITH-TEXT-CNT + #W-PRMRY-SPECIAL-TEXT-CNT
                if (condition(pnd_I.greater(1)))                                                                                                                          //Natural: IF #I GT 1
                {
                                                                                                                                                                          //Natural: PERFORM RTN-6A-SORT-PRMRY-ARRAY
                    sub_Rtn_6a_Sort_Prmry_Array();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  STD
                //*  STD W/ TXT
                //*  SPCL TXT
                pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_Ctrs_Pnd_W_Cntgnt_Std_Cnt.add(pnd_Ctrs_Pnd_W_Cntgnt_Std_With_Text_Cnt).add(pnd_Ctrs_Pnd_W_Cntgnt_Special_Text_Cnt)); //Natural: COMPUTE #I = #W-CNTGNT-STD-CNT + #W-CNTGNT-STD-WITH-TEXT-CNT + #W-CNTGNT-SPECIAL-TEXT-CNT
                if (condition(pnd_I.greater(1)))                                                                                                                          //Natural: IF #I GT 1
                {
                                                                                                                                                                          //Natural: PERFORM RTN-6B-SORT-CNTGNT-ARRAY
                    sub_Rtn_6b_Sort_Cntgnt_Array();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM RTN-7-SET-PDA-INDICATORS
            sub_Rtn_7_Set_Pda_Indicators();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                                                                                                                                                                          //Natural: PERFORM RTN-99-DEBUG
                sub_Rtn_99_Debug();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(! (pnd_Found.getBoolean())))                                                                                                                        //Natural: IF NOT #FOUND
        {
            pdaCisa620.getCisa620_Pnd_Cisa_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "(1105)", ldaCisl5000.getPnd_Cis_Ben_Super_1(), "NOT FOUND"));            //Natural: COMPRESS *PROGRAM '(1105)' #CIS-BEN-SUPER-1 'NOT FOUND' INTO #CISA-MSG
            pdaCisa620.getCisa620_Pnd_Cisa_Return_Code().setValue(99);                                                                                                    //Natural: ASSIGN #CISA-RETURN-CODE := 99
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  END OF PROGRAM - SUBROUTINE SECTION FOLLOWS
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-1-MOVE-FIELDS-TO-PDA
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-2-FORMAT-BENE-INFO
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-3A-STANDARD
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-3B-SPECIAL-TEXT
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-4A-STANDARD
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-4B-SPECIAL-TEXT
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-5-FREE-FORM-TEXT
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-6A-SORT-PRMRY-ARRAY
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-6B-SORT-CNTGNT-ARRAY
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-7-SET-PDA-INDICATORS
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTN-99-DEBUG
        //* *-----------------------------*
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-NAME
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CIS-BENE-FILE-02
    }
    private void sub_Rtn_1_Move_Fields_To_Pda() throws Exception                                                                                                          //Natural: RTN-1-MOVE-FIELDS-TO-PDA
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_Name.reset();                                                                                                                                                 //Natural: RESET #NAME
        //*  VCR
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Dod().notEquals(getZero())))                                                                        //Natural: IF CIS-CLCLTN-BENE-DOD NE 0
        {
            pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Dod_A().setValueEdited(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Dod(),new ReportEditMask("YYYYMMDD"));      //Natural: MOVE EDITED CIS-CLCLTN-BENE-DOD ( EM = YYYYMMDD ) TO #CISA-CLCLTN-BENE-DOD-A
        }                                                                                                                                                                 //Natural: END-IF
        //*  VCR
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Dob().notEquals(getZero())))                                                                        //Natural: IF CIS-CLCLTN-BENE-DOB NE 0
        {
            pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Dob_A().setValueEdited(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Dob(),new ReportEditMask("YYYYMMDD"));      //Natural: MOVE EDITED CIS-CLCLTN-BENE-DOB ( EM = YYYYMMDD ) TO #CISA-CLCLTN-BENE-DOB-A
        }                                                                                                                                                                 //Natural: END-IF
        pdaCisa620.getCisa620_Pnd_Cisa_Bene_Std_Free().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Std_Free());                                                     //Natural: MOVE CIS-BENE-FILE-01.CIS-BENE-STD-FREE TO #CISA-BENE-STD-FREE
        pdaCisa620.getCisa620_Pnd_Cisa_Bene_Estate().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Estate());                                                         //Natural: MOVE CIS-BENE-FILE-01.CIS-BENE-ESTATE TO #CISA-BENE-ESTATE
        pdaCisa620.getCisa620_Pnd_Cisa_Bene_Trust().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Trust());                                                           //Natural: MOVE CIS-BENE-FILE-01.CIS-BENE-TRUST TO #CISA-BENE-TRUST
        pdaCisa620.getCisa620_Pnd_Cisa_Bene_Category().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Category());                                                     //Natural: MOVE CIS-BENE-FILE-01.CIS-BENE-CATEGORY TO #CISA-BENE-CATEGORY
        pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Bene_Child_Prvsn().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn());                                   //Natural: MOVE CIS-BENE-FILE-01.CIS-PRMRY-BENE-CHILD-PRVSN TO #CISA-PRMRY-BENE-CHILD-PRVSN
        pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Bene_Child_Prvsn().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn());                                 //Natural: MOVE CIS-BENE-FILE-01.CIS-CNTGNT-BENE-CHILD-PRVSN TO #CISA-CNTGNT-BENE-CHILD-PRVSN
        pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Crossover().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Crossover());                                     //Natural: MOVE CIS-BENE-FILE-01.CIS-CLCLTN-BENE-CROSSOVER TO #CISA-CLCLTN-BENE-CROSSOVER
        pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Name().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Name());                                               //Natural: MOVE CIS-BENE-FILE-01.CIS-CLCLTN-BENE-NAME TO #CISA-CLCLTN-BENE-NAME #NAME
        pnd_Name.setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Name());
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-NAME
        sub_Translate_Name();
        if (condition(Global.isEscape())) {return;}
        pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Name().setValue(pnd_Name);                                                                                             //Natural: MOVE #NAME TO #CISA-CLCLTN-BENE-NAME
        pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Rltnshp_Cde().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Rltnshp_Cde());                                 //Natural: MOVE CIS-BENE-FILE-01.CIS-CLCLTN-BENE-RLTNSHP-CDE TO #CISA-CLCLTN-BENE-RLTNSHP-CDE
        pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Ssn_Nbr().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Ssn_Nbr());                                         //Natural: MOVE CIS-BENE-FILE-01.CIS-CLCLTN-BENE-SSN-NBR TO #CISA-CLCLTN-BENE-SSN-NBR
        pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Sex_Cde().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Sex_Cde());                                         //Natural: MOVE CIS-BENE-FILE-01.CIS-CLCLTN-BENE-SEX-CDE TO #CISA-CLCLTN-BENE-SEX-CDE
        pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Calc_Method().setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Calc_Method());                                 //Natural: MOVE CIS-BENE-FILE-01.CIS-CLCLTN-BENE-CALC-METHOD TO #CISA-CLCLTN-BENE-CALC-METHOD
        short decideConditionsMet493 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CIS-BENE-FILE-01.CIS-CLCLTN-BENE-CALC-METHOD;//Natural: VALUE 'R'
        if (condition((ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Calc_Method().equals("R"))))
        {
            decideConditionsMet493++;
            pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Calc_Desc().setValue("Recalculation");                                                                             //Natural: ASSIGN #CISA-CLCLTN-BENE-CALC-DESC := 'Recalculation'
        }                                                                                                                                                                 //Natural: VALUE 'Y'
        else if (condition((ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Calc_Method().equals("Y"))))
        {
            decideConditionsMet493++;
            pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Calc_Desc().setValue("One-Year-Less");                                                                             //Natural: ASSIGN #CISA-CLCLTN-BENE-CALC-DESC := 'One-Year-Less'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Name().equals(" ")))                                                                                //Natural: IF CIS-BENE-FILE-01.CIS-CLCLTN-BENE-NAME = ' '
        {
            pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Name().setValue("Not Applicable");                                                                                 //Natural: ASSIGN #CISA-CLCLTN-BENE-NAME := 'Not Applicable'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Calc_Bene().setValue("- Calculation Beneficiary");                                                                 //Natural: ASSIGN #CISA-CLCLTN-BENE-CALC-BENE := '- Calculation Beneficiary'
        }                                                                                                                                                                 //Natural: END-IF
        pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Contain_Estate().setValue("N");                                                                                             //Natural: ASSIGN #CISA-CNTGNT-CONTAIN-ESTATE := 'N'
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde().getValue(pnd_I).equals("37")))                                                       //Natural: IF CIS-CNTGNT-BENE-RLTN-CDE ( #I ) EQ '37'
            {
                pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Contain_Estate().setValue("Y");                                                                                     //Natural: ASSIGN #CISA-CNTGNT-CONTAIN-ESTATE := 'Y'
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  RTN-1-MOVE-FIELDS-TO-PDA
    }
    private void sub_Rtn_2_Format_Bene_Info() throws Exception                                                                                                            //Natural: RTN-2-FORMAT-BENE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //*  STANDARD FORMAT
        short decideConditionsMet521 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CIS-BENE-STD-FREE;//Natural: VALUE 'N'
        if (condition((ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Std_Free().equals("N"))))
        {
            decideConditionsMet521++;
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 CIS-BENE-FILE-01.C*CIS-PRMRY-BNFCRY-DTA
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaCisl5000.getCis_Bene_File_01_Count_Castcis_Prmry_Bnfcry_Dta())); pnd_I.nadd(1))
            {
                pnd_Lump_Auto_Ind.reset();                                                                                                                                //Natural: RESET #LUMP-AUTO-IND #CHILD-PRVSN-IND #CHILD-PRVSN-IND-FILL #CHILD-PRVSN-INDC
                pnd_Child_Prvsn_Ind.reset();
                pnd_Child_Prvsn_Ind_Fill.reset();
                pnd_Child_Prvsn_Indc.reset();
                //*  060706
                if (condition(DbsUtil.maskMatches(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id(),"'MDO'")))                                                           //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ MASK ( 'MDO' )
                {
                    if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Name().getValue(pnd_I).equals(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Name())    //Natural: IF CIS-BENE-FILE-01.CIS-PRMRY-BENE-NAME ( #I ) EQ CIS-BENE-FILE-01.CIS-CLCLTN-BENE-NAME AND CIS-BENE-FILE-01.CIS-PRMRY-BENE-SSN-NBR ( #I ) EQ CIS-BENE-FILE-01.CIS-CLCLTN-BENE-SSN-NBR AND CIS-BENE-FILE-01.CIS-PRMRY-BENE-DOB ( #I ) EQ CIS-BENE-FILE-01.CIS-CLCLTN-BENE-DOB
                        && ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr().getValue(pnd_I).equals(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Ssn_Nbr()) 
                        && ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Dob().getValue(pnd_I).equals(ldaCisl5000.getCis_Bene_File_01_Cis_Clcltn_Bene_Dob())))
                    {
                        pnd_Lump_Auto_Ind.setValue("*");                                                                                                                  //Natural: MOVE '*' TO #LUMP-AUTO-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet534 = 0;                                                                                                                         //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CIS-PRMRY-BENE-LUMP-SUM ( #I ) = 'N'
                if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Lump_Sum().getValue(pnd_I).equals("N")))
                {
                    decideConditionsMet534++;
                    if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA")))                                                                       //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IA'
                    {
                        pnd_Ctrs_Pnd_W_Prmry_Lump_Sum_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #W-PRMRY-LUMP-SUM-CNT
                        pnd_Lump_Auto_Ind.setValue("b-");                                                                                                                 //Natural: MOVE 'b-' TO #LUMP-AUTO-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN CIS-PRMRY-BENE-AUTO-CMNT-VAL ( #I ) = 'Y'
                if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Auto_Cmnt_Val().getValue(pnd_I).equals("Y")))
                {
                    decideConditionsMet534++;
                    pnd_Ctrs_Pnd_W_Prmry_Auto_Cmnt_Val_Cnt.nadd(1);                                                                                                       //Natural: ADD 1 TO #W-PRMRY-AUTO-CMNT-VAL-CNT
                    pnd_Lump_Auto_Ind.setValue("a-");                                                                                                                     //Natural: MOVE 'a-' TO #LUMP-AUTO-IND
                }                                                                                                                                                         //Natural: WHEN CIS-PRMRY-BENE-CHILD-PRVSN = 'Y'
                if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn().equals("Y")))
                {
                    decideConditionsMet534++;
                    //*  060706
                    if (condition(DbsUtil.maskMatches(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id(),"'MDO'")))                                                       //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ MASK ( 'MDO' )
                    {
                        pnd_Child_Prvsn_Ind.setValue("#");                                                                                                                //Natural: MOVE '#' TO #CHILD-PRVSN-IND
                        pnd_Child_Prvsn_Ind_Fill.setValue("-");                                                                                                           //Natural: MOVE '-' TO #CHILD-PRVSN-IND-FILL
                        pnd_Child_Prvsn_Indc.setValue(true);                                                                                                              //Natural: MOVE TRUE TO #CHILD-PRVSN-INDC
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA")))                                                                       //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IA'
                    {
                        pnd_Child_Prvsn_Ind.setValue("c");                                                                                                                //Natural: MOVE 'c' TO #CHILD-PRVSN-IND
                        pnd_Child_Prvsn_Ind_Fill.setValue("-");                                                                                                           //Natural: MOVE '-' TO #CHILD-PRVSN-IND-FILL
                        pnd_Child_Prvsn_Indc.setValue(true);                                                                                                              //Natural: MOVE TRUE TO #CHILD-PRVSN-INDC
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet534 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                short decideConditionsMet560 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF CIS-PRMRY-BENE-STD-TXT ( #I );//Natural: VALUE 'S'
                if (condition((ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Std_Txt().getValue(pnd_I).equals("S"))))
                {
                    decideConditionsMet560++;
                    pnd_Ctrs_Pnd_W_Prmry_Std_Cnt.nadd(1);                                                                                                                 //Natural: ADD 1 TO #W-PRMRY-STD-CNT
                    pnd_Sort_Key_Cnt_Pnd_Sort_Key.setValue(pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_1_Cnt);                                                                    //Natural: MOVE #PRMRY-SORT-KEY-1-CNT TO #SORT-KEY
                                                                                                                                                                          //Natural: PERFORM RTN-3A-STANDARD
                    sub_Rtn_3a_Standard();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_1_Cnt.setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                                    //Natural: MOVE #SORT-KEY TO #PRMRY-SORT-KEY-1-CNT
                }                                                                                                                                                         //Natural: VALUE 'B'
                else if (condition((ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Std_Txt().getValue(pnd_I).equals("B"))))
                {
                    decideConditionsMet560++;
                    pnd_Ctrs_Pnd_W_Prmry_Std_With_Text_Cnt.nadd(1);                                                                                                       //Natural: ADD 1 TO #W-PRMRY-STD-WITH-TEXT-CNT
                    pnd_Sort_Key_Cnt_Pnd_Sort_Key.setValue(pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_2_Cnt);                                                                    //Natural: MOVE #PRMRY-SORT-KEY-2-CNT TO #SORT-KEY
                                                                                                                                                                          //Natural: PERFORM RTN-3A-STANDARD
                    sub_Rtn_3a_Standard();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Lump_Auto_Ind.reset();                                                                                                                            //Natural: RESET #LUMP-AUTO-IND
                                                                                                                                                                          //Natural: PERFORM RTN-3B-SPECIAL-TEXT
                    sub_Rtn_3b_Special_Text();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_2_Cnt.setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                                    //Natural: MOVE #SORT-KEY TO #PRMRY-SORT-KEY-2-CNT
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Std_Txt().getValue(pnd_I).equals("T"))))
                {
                    decideConditionsMet560++;
                    pnd_Ctrs_Pnd_W_Prmry_Special_Text_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #W-PRMRY-SPECIAL-TEXT-CNT
                    pnd_Sort_Key_Cnt_Pnd_Sort_Key.setValue(pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_3_Cnt);                                                                    //Natural: MOVE #PRMRY-SORT-KEY-3-CNT TO #SORT-KEY
                                                                                                                                                                          //Natural: PERFORM RTN-3B-SPECIAL-TEXT
                    sub_Rtn_3b_Special_Text();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Sort_Key_Cnt_Pnd_Prmry_Sort_Key_3_Cnt.setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                                    //Natural: MOVE #SORT-KEY TO #PRMRY-SORT-KEY-3-CNT
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Name().getValue(pnd_I).notEquals(" ")))                                                  //Natural: IF CIS-PRMRY-BENE-NAME ( #I ) NE ' '
                    {
                                                                                                                                                                          //Natural: PERFORM RTN-3A-STANDARD
                        sub_Rtn_3a_Standard();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            FOR03:                                                                                                                                                        //Natural: FOR #I = 1 C*CIS-CNTGNT-BNFCRY-DTA
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaCisl5000.getCis_Bene_File_01_Count_Castcis_Cntgnt_Bnfcry_Dta())); pnd_I.nadd(1))
            {
                pnd_Lump_Auto_Ind.reset();                                                                                                                                //Natural: RESET #LUMP-AUTO-IND #CHILD-PRVSN-IND #CHILD-PRVSN-IND-FILL #CHILD-PRVSN-INDC
                pnd_Child_Prvsn_Ind.reset();
                pnd_Child_Prvsn_Ind_Fill.reset();
                pnd_Child_Prvsn_Indc.reset();
                short decideConditionsMet592 = 0;                                                                                                                         //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CIS-CNTGNT-BENE-LUMP-SUM ( #I ) = 'N'
                if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Lump_Sum().getValue(pnd_I).equals("N")))
                {
                    decideConditionsMet592++;
                    if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA")))                                                                       //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IA'
                    {
                        pnd_Ctrs_Pnd_W_Cntgnt_Lump_Sum_Cnt.nadd(1);                                                                                                       //Natural: ADD 1 TO #W-CNTGNT-LUMP-SUM-CNT
                        pnd_Lump_Auto_Ind.setValue("b-");                                                                                                                 //Natural: MOVE 'b-' TO #LUMP-AUTO-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN CIS-CNTGNT-BENE-AUTO-CMNT-VAL ( #I ) = 'Y'
                if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Auto_Cmnt_Val().getValue(pnd_I).equals("Y")))
                {
                    decideConditionsMet592++;
                    pnd_Ctrs_Pnd_W_Cntgnt_Auto_Cmnt_Val_Cnt.nadd(1);                                                                                                      //Natural: ADD 1 TO #W-CNTGNT-AUTO-CMNT-VAL-CNT
                    pnd_Lump_Auto_Ind.setValue("a-");                                                                                                                     //Natural: MOVE 'a-' TO #LUMP-AUTO-IND
                }                                                                                                                                                         //Natural: WHEN CIS-BENE-FILE-01.CIS-CNTGNT-BENE-CHILD-PRVSN = 'Y'
                if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn().equals("Y")))
                {
                    decideConditionsMet592++;
                    //*  060706
                    if (condition(DbsUtil.maskMatches(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id(),"'MDO'")))                                                       //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ MASK ( 'MDO' )
                    {
                        pnd_Child_Prvsn_Ind.setValue("#");                                                                                                                //Natural: MOVE '#' TO #CHILD-PRVSN-IND
                        pnd_Child_Prvsn_Ind_Fill.setValue("-");                                                                                                           //Natural: MOVE '-' TO #CHILD-PRVSN-IND-FILL
                        pnd_Child_Prvsn_Indc.setValue(true);                                                                                                              //Natural: MOVE TRUE TO #CHILD-PRVSN-INDC
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA")))                                                                       //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IA'
                    {
                        pnd_Child_Prvsn_Ind.setValue("c");                                                                                                                //Natural: MOVE 'c' TO #CHILD-PRVSN-IND
                        pnd_Child_Prvsn_Ind_Fill.setValue("-");                                                                                                           //Natural: MOVE '-' TO #CHILD-PRVSN-IND-FILL
                        pnd_Child_Prvsn_Indc.setValue(true);                                                                                                              //Natural: MOVE TRUE TO #CHILD-PRVSN-INDC
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet592 == 0))
                {
                    pnd_Lump_Auto_Ind.reset();                                                                                                                            //Natural: RESET #LUMP-AUTO-IND
                }                                                                                                                                                         //Natural: END-DECIDE
                short decideConditionsMet617 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF CIS-CNTGNT-BENE-STD-TXT ( #I );//Natural: VALUE 'S'
                if (condition((ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Std_Txt().getValue(pnd_I).equals("S"))))
                {
                    decideConditionsMet617++;
                    pnd_Ctrs_Pnd_W_Cntgnt_Std_Cnt.nadd(1);                                                                                                                //Natural: ADD 1 TO #W-CNTGNT-STD-CNT
                    pnd_Sort_Key_Cnt_Pnd_Sort_Key.setValue(pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_1_Cnt);                                                                   //Natural: MOVE #CNTGNT-SORT-KEY-1-CNT TO #SORT-KEY
                                                                                                                                                                          //Natural: PERFORM RTN-4A-STANDARD
                    sub_Rtn_4a_Standard();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_1_Cnt.setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                                   //Natural: MOVE #SORT-KEY TO #CNTGNT-SORT-KEY-1-CNT
                }                                                                                                                                                         //Natural: VALUE 'B'
                else if (condition((ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Std_Txt().getValue(pnd_I).equals("B"))))
                {
                    decideConditionsMet617++;
                    pnd_Ctrs_Pnd_W_Cntgnt_Std_With_Text_Cnt.nadd(1);                                                                                                      //Natural: ADD 1 TO #W-CNTGNT-STD-WITH-TEXT-CNT
                    pnd_Sort_Key_Cnt_Pnd_Sort_Key.setValue(pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_2_Cnt);                                                                   //Natural: MOVE #CNTGNT-SORT-KEY-2-CNT TO #SORT-KEY
                                                                                                                                                                          //Natural: PERFORM RTN-4A-STANDARD
                    sub_Rtn_4a_Standard();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Lump_Auto_Ind.reset();                                                                                                                            //Natural: RESET #LUMP-AUTO-IND
                                                                                                                                                                          //Natural: PERFORM RTN-4B-SPECIAL-TEXT
                    sub_Rtn_4b_Special_Text();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_2_Cnt.setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                                   //Natural: MOVE #SORT-KEY TO #CNTGNT-SORT-KEY-2-CNT
                }                                                                                                                                                         //Natural: VALUE 'T'
                else if (condition((ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Std_Txt().getValue(pnd_I).equals("T"))))
                {
                    decideConditionsMet617++;
                    pnd_Ctrs_Pnd_W_Cntgnt_Special_Text_Cnt.nadd(1);                                                                                                       //Natural: ADD 1 TO #W-CNTGNT-SPECIAL-TEXT-CNT
                    pnd_Sort_Key_Cnt_Pnd_Sort_Key.setValue(pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_3_Cnt);                                                                   //Natural: MOVE #CNTGNT-SORT-KEY-3-CNT TO #SORT-KEY
                                                                                                                                                                          //Natural: PERFORM RTN-4B-SPECIAL-TEXT
                    sub_Rtn_4b_Special_Text();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Sort_Key_Cnt_Pnd_Cntgnt_Sort_Key_3_Cnt.setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                                   //Natural: MOVE #SORT-KEY TO #CNTGNT-SORT-KEY-3-CNT
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Name().getValue(pnd_I).notEquals(" ")))                                                 //Natural: IF CIS-CNTGNT-BENE-NAME ( #I ) NE ' '
                    {
                                                                                                                                                                          //Natural: PERFORM RTN-4A-STANDARD
                        sub_Rtn_4a_Standard();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  FREE-FORM TEXT
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 'Y'
        else if (condition((ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Std_Free().equals("Y"))))
        {
            decideConditionsMet521++;
                                                                                                                                                                          //Natural: PERFORM RTN-5-FREE-FORM-TEXT
            sub_Rtn_5_Free_Form_Text();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  RTN-2-FORMAT-BENE-INFO
    }
    private void sub_Rtn_3a_Standard() throws Exception                                                                                                                   //Natural: RTN-3A-STANDARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_Pr_Line.reset();                                                                                                                                              //Natural: RESET #PR-LINE #PR-TEXT-1 #PR-TEXT-2
        pnd_Pr_Text_1.reset();
        pnd_Pr_Text_2.reset();
        if (condition(pnd_Lump_Auto_Ind.notEquals(" ")))                                                                                                                  //Natural: IF #LUMP-AUTO-IND NE ' '
        {
            pnd_Lump_Sum_Break.setValue(pnd_Lump_Auto_Ind);                                                                                                               //Natural: ASSIGN #LUMP-SUM-BREAK := #LUMP-AUTO-IND
            if (condition(pnd_Child_Prvsn_Indc.getBoolean()))                                                                                                             //Natural: IF #CHILD-PRVSN-INDC
            {
                pnd_Lump_Sum_Break_Pnd_Filler2.setValue(",");                                                                                                             //Natural: ASSIGN #FILLER2 := ','
                pnd_Lump_Auto_Ind.setValue(pnd_Lump_Sum_Break);                                                                                                           //Natural: ASSIGN #LUMP-AUTO-IND := #LUMP-SUM-BREAK
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Child_Prvsn_Ind.setValue(pnd_Lump_Sum_Break_Pnd_Filler1);                                                                                             //Natural: ASSIGN #CHILD-PRVSN-IND := #FILLER1
                pnd_Child_Prvsn_Ind_Fill.setValue(pnd_Lump_Sum_Break_Pnd_Filler2);                                                                                        //Natural: ASSIGN #CHILD-PRVSN-IND-FILL := #FILLER2
                pnd_Lump_Auto_Ind.reset();                                                                                                                                //Natural: RESET #LUMP-AUTO-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Pr_Line_Pnd_Pr_Lump_Auto_Ind.setValue(pnd_Lump_Auto_Ind);                                                                                                     //Natural: MOVE #LUMP-AUTO-IND TO #PR-LUMP-AUTO-IND
        pnd_Pr_Line_Pnd_Pr_Child_Prvsn.setValue(pnd_Child_Prvsn_Ind);                                                                                                     //Natural: MOVE #CHILD-PRVSN-IND TO #PR-CHILD-PRVSN
        pnd_Pr_Line_Pnd_Pr_Child_Prvsn_Fill.setValue(pnd_Child_Prvsn_Ind_Fill);                                                                                           //Natural: MOVE #CHILD-PRVSN-IND-FILL TO #PR-CHILD-PRVSN-FILL
        pnd_Pr_Line_Pnd_Pr_Name.setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Name().getValue(pnd_I));                                                          //Natural: MOVE CIS-PRMRY-BENE-NAME ( #I ) TO #PR-NAME
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr().getValue(pnd_I).notEquals(getZero())))                                                     //Natural: IF CIS-PRMRY-BENE-SSN-NBR ( #I ) NE 0
        {
            //* *IF #CISA-RESID-ISSUE-STATE = 'CA' OR = '05'         /* JB122005
            //*  KG CA SSN
            pnd_Pr_Line_Pnd_Pr_Soc.setValue("ON FILE");                                                                                                                   //Natural: MOVE 'ON FILE' TO #PR-SOC
            //* *ELSE                                                /* JB122005
            //* *   MOVE CIS-PRMRY-BENE-SSN-NBR(#I)     TO  #SOC     /* JB122005
            //* *   MOVE EDITED #SOC-A (EM=XXX-XX-XXXX) TO  #PR-SOC  /* JB122005
            //* *END-IF /* (2305)                                    /* JB122005
            //*  (2300)
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Dob().getValue(pnd_I).notEquals(getZero())))                                                         //Natural: IF CIS-PRMRY-BENE-DOB ( #I ) NE 0
        {
            pnd_Pr_Line_Pnd_Pr_Dob.setValueEdited(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Dob().getValue(pnd_I),new ReportEditMask("MM' 'DD' 'YYYY"));             //Natural: MOVE EDITED CIS-PRMRY-BENE-DOB ( #I ) ( EM = MM' 'DD' 'YYYY ) TO #PR-DOB
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde().getValue(pnd_I).notEquals("37") && ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde().getValue(pnd_I).notEquals("38"))) //Natural: IF CIS-PRMRY-BENE-RLTN-CDE ( #I ) NE '37' AND CIS-PRMRY-BENE-RLTN-CDE ( #I ) NE '38'
        {
            pnd_Pr_Line_Pnd_Pr_Relationship.setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn().getValue(pnd_I));                                              //Natural: MOVE CIS-PRMRY-BENE-RLTN ( #I ) TO #PR-RELATIONSHIP
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde().getValue(pnd_I).equals("I")))                                                         //Natural: IF CIS-PRMRY-BENE-RLTN-CDE ( #I ) EQ 'I'
            {
                pnd_Pr_Line_Pnd_Pr_Relationship.setValue("Org.");                                                                                                         //Natural: ASSIGN #PR-RELATIONSHIP := 'Org.'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde().getValue(pnd_I).equals("K") || ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde().getValue(pnd_I).equals("L"))) //Natural: IF CIS-PRMRY-BENE-RLTN-CDE ( #I ) EQ 'K' OR CIS-PRMRY-BENE-RLTN-CDE ( #I ) EQ 'L'
            {
                pnd_Pr_Line_Pnd_Pr_Relationship.setValue("Trust");                                                                                                        //Natural: ASSIGN #PR-RELATIONSHIP := 'Trust'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(pnd_I).notEquals(getZero()) || ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_I).notEquals(getZero()))) //Natural: IF CIS-PRMRY-BENE-NMRTR-NBR ( #I ) NE 0 OR CIS-PRMRY-BENE-DNMNTR-NBR ( #I ) NE 0
        {
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_I).equals(100)))                                                       //Natural: IF CIS-PRMRY-BENE-DNMNTR-NBR ( #I ) EQ 100
            {
                pnd_Pr_Line_Pnd_Pr_Share.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(pnd_I),  //Natural: COMPRESS CIS-PRMRY-BENE-NMRTR-NBR ( #I ) '%' INTO #PR-SHARE LEAVING NO SPACE
                    "%"));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Nmrtr.reset();                                                                                                                                        //Natural: RESET #NMRTR #DNMRTR #RESULT #REMAINDER
                pnd_Dnmrtr.reset();
                pnd_Result.reset();
                pnd_Remainder.reset();
                pnd_Nmrtr.compute(new ComputeParameters(false, pnd_Nmrtr), ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(pnd_I).multiply(100));     //Natural: ASSIGN #NMRTR := CIS-PRMRY-BENE-NMRTR-NBR ( #I ) * 100
                pnd_Dnmrtr.setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_I));                                                         //Natural: ASSIGN #DNMRTR := CIS-PRMRY-BENE-DNMNTR-NBR ( #I )
                pnd_Remainder.compute(new ComputeParameters(false, pnd_Remainder), pnd_Nmrtr.mod(pnd_Dnmrtr));                                                            //Natural: DIVIDE #DNMRTR INTO #NMRTR GIVING #RESULT REMAINDER #REMAINDER
                pnd_Result.compute(new ComputeParameters(false, pnd_Result), pnd_Nmrtr.divide(pnd_Dnmrtr));
                if (condition(pnd_Remainder.equals(getZero())))                                                                                                           //Natural: IF #REMAINDER EQ 0
                {
                    pnd_Pr_Line_Pnd_Pr_Share.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Result, "%"));                                                  //Natural: COMPRESS #RESULT '%' INTO #PR-SHARE LEAVING NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Pr_Line_Pnd_Pr_Share.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(pnd_I),  //Natural: COMPRESS CIS-PRMRY-BENE-NMRTR-NBR ( #I ) '/' CIS-PRMRY-BENE-DNMNTR-NBR ( #I ) INTO #PR-SHARE LEAVING NO SPACE
                        "/", ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_I)));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sort_Key_Cnt_Pnd_Sort_Key.nadd(1);                                                                                                                            //Natural: ADD 1 TO #SORT-KEY
        pnd_Ctrs_Pnd_W_Prmry_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #W-PRMRY-CNT
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("TPA")))                                                                                  //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'TPA'
        {
            pnd_Pr_Text_1_Pnd_Pr_Name_1.setValue(pnd_Pr_Line_Pnd_Pr_Name);                                                                                                //Natural: MOVE #PR-NAME TO #PR-NAME-1
            pnd_Pr_Text_1_Pnd_Pr_Relationship_1.setValue(pnd_Pr_Line_Pnd_Pr_Relationship);                                                                                //Natural: MOVE #PR-RELATIONSHIP TO #PR-RELATIONSHIP-1
            pnd_Pr_Text_1_Pnd_Pr_Share_1.setValue(pnd_Pr_Line_Pnd_Pr_Share);                                                                                              //Natural: MOVE #PR-SHARE TO #PR-SHARE-1
            pnd_Pr_Line_Pnd_Pr_Text.setValue(pnd_Pr_Text_1);                                                                                                              //Natural: MOVE #PR-TEXT-1 TO #PR-TEXT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IPRO")))                                                                                 //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IPRO'
        {
            pnd_Pr_Text_2_Pnd_Pr_Name_2.setValue(pnd_Pr_Line_Pnd_Pr_Name);                                                                                                //Natural: MOVE #PR-NAME TO #PR-NAME-2
            pnd_Pr_Text_2_Pnd_Pr_Relationship_2.setValue(pnd_Pr_Line_Pnd_Pr_Relationship);                                                                                //Natural: MOVE #PR-RELATIONSHIP TO #PR-RELATIONSHIP-2
            pnd_Pr_Line_Pnd_Pr_Text.setValue(pnd_Pr_Text_2);                                                                                                              //Natural: MOVE #PR-TEXT-2 TO #PR-TEXT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ctrs_Pnd_W_Prmry_Sort_Key.getValue(pnd_Ctrs_Pnd_W_Prmry_Cnt).setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                         //Natural: MOVE #SORT-KEY TO #W-PRMRY-SORT-KEY ( #W-PRMRY-CNT )
        pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Print_Line().getValue(pnd_Ctrs_Pnd_W_Prmry_Cnt).setValue(pnd_Pr_Line);                                                       //Natural: MOVE #PR-LINE TO #CISA-PRMRY-PRINT-LINE ( #W-PRMRY-CNT )
        pnd_Prmry_Extended_Name.reset();                                                                                                                                  //Natural: RESET #PRMRY-EXTENDED-NAME #CNTGNT-EXTENDED-NAME #OCCURENCE #FOUND-EXTENDED
        pnd_Cntgnt_Extended_Name.reset();
        pnd_Occurence.reset();
        pnd_Found_Extended.reset();
        pnd_Occurence.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #OCCURENCE := #I
        DbsUtil.callnat(Cisn5001.class , getCurrentProcessState(), ldaCisl5000.getPnd_Cis_Ben_Super_1(), pnd_Rqst_Id_Key, pnd_Prmry_Extended_Name, pnd_Cntgnt_Extended_Name,  //Natural: CALLNAT 'CISN5001' #CIS-BEN-SUPER-1 #RQST-ID-KEY #PRMRY-EXTENDED-NAME #CNTGNT-EXTENDED-NAME #OCCURENCE #FOUND-EXTENDED
            pnd_Occurence, pnd_Found_Extended);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Found_Extended.getBoolean() && pnd_Prmry_Extended_Name.notEquals(" ")))                                                                         //Natural: IF #FOUND-EXTENDED AND #PRMRY-EXTENDED-NAME NE ' '
        {
            //* *  RESET #LUMP-AUTO-IND
            //* *    #CHILD-PRVSN-IND
            //* *    #CHILD-PRVSN-IND-FILL
            pnd_Pr_Line.reset();                                                                                                                                          //Natural: RESET #PR-LINE
            pnd_Sort_Key_Cnt_Pnd_Sort_Key.nadd(1);                                                                                                                        //Natural: ADD 1 TO #SORT-KEY
            pnd_Ctrs_Pnd_W_Prmry_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #W-PRMRY-CNT
            pnd_Pr_Line_Pnd_Pr_Text.setValue(pnd_Prmry_Extended_Name);                                                                                                    //Natural: MOVE #PRMRY-EXTENDED-NAME TO #PR-TEXT
            pnd_Ctrs_Pnd_W_Prmry_Sort_Key.getValue(pnd_Ctrs_Pnd_W_Prmry_Cnt).setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                     //Natural: MOVE #SORT-KEY TO #W-PRMRY-SORT-KEY ( #W-PRMRY-CNT )
            pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Print_Line().getValue(pnd_Ctrs_Pnd_W_Prmry_Cnt).setValue(pnd_Pr_Line);                                                   //Natural: MOVE #PR-LINE TO #CISA-PRMRY-PRINT-LINE ( #W-PRMRY-CNT )
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-3A-STANDARD
    }
    private void sub_Rtn_3b_Special_Text() throws Exception                                                                                                               //Natural: RTN-3B-SPECIAL-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_Pr_Line.reset();                                                                                                                                              //Natural: RESET #PR-LINE
        FOR04:                                                                                                                                                            //Natural: FOR #J = 1 #C-CIS-PRMRY-BENE-SPCL-TXT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_C_Cis_Prmry_Bene_Spcl_Txt)); pnd_J.nadd(1))
        {
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_I,pnd_J).equals(" ")))                                                   //Natural: IF CIS-PRMRY-BENE-SPCL-TXT ( #I,#J ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sort_Key_Cnt_Pnd_Sort_Key.nadd(1);                                                                                                                        //Natural: ADD 1 TO #SORT-KEY
            pnd_Ctrs_Pnd_W_Prmry_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #W-PRMRY-CNT
            pnd_Pr_Line_Pnd_Pr_Text.setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_I,pnd_J));                                            //Natural: MOVE CIS-PRMRY-BENE-SPCL-TXT ( #I,#J ) TO #PR-TEXT
            pnd_Ctrs_Pnd_W_Prmry_Sort_Key.getValue(pnd_Ctrs_Pnd_W_Prmry_Cnt).setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                     //Natural: MOVE #SORT-KEY TO #W-PRMRY-SORT-KEY ( #W-PRMRY-CNT )
            pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Print_Line().getValue(pnd_Ctrs_Pnd_W_Prmry_Cnt).setValue(pnd_Pr_Line);                                                   //Natural: MOVE #PR-LINE TO #CISA-PRMRY-PRINT-LINE ( #W-PRMRY-CNT )
            pnd_Pr_Line.reset();                                                                                                                                          //Natural: RESET #PR-LINE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  RTN-3B-SPECIAL-TEXT
    }
    private void sub_Rtn_4a_Standard() throws Exception                                                                                                                   //Natural: RTN-4A-STANDARD
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_Pr_Line.reset();                                                                                                                                              //Natural: RESET #PR-LINE #PR-TEXT-1 #PR-TEXT-2
        pnd_Pr_Text_1.reset();
        pnd_Pr_Text_2.reset();
        if (condition(pnd_Lump_Auto_Ind.notEquals(" ")))                                                                                                                  //Natural: IF #LUMP-AUTO-IND NE ' '
        {
            pnd_Lump_Sum_Break.setValue(pnd_Lump_Auto_Ind);                                                                                                               //Natural: ASSIGN #LUMP-SUM-BREAK := #LUMP-AUTO-IND
            if (condition(pnd_Child_Prvsn_Indc.getBoolean()))                                                                                                             //Natural: IF #CHILD-PRVSN-INDC
            {
                pnd_Lump_Sum_Break_Pnd_Filler2.setValue(",");                                                                                                             //Natural: ASSIGN #FILLER2 := ','
                pnd_Lump_Auto_Ind.setValue(pnd_Lump_Sum_Break);                                                                                                           //Natural: ASSIGN #LUMP-AUTO-IND := #LUMP-SUM-BREAK
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Child_Prvsn_Ind.setValue(pnd_Lump_Sum_Break_Pnd_Filler1);                                                                                             //Natural: ASSIGN #CHILD-PRVSN-IND := #FILLER1
                pnd_Child_Prvsn_Ind_Fill.setValue(pnd_Lump_Sum_Break_Pnd_Filler2);                                                                                        //Natural: ASSIGN #CHILD-PRVSN-IND-FILL := #FILLER2
                pnd_Lump_Auto_Ind.reset();                                                                                                                                //Natural: RESET #LUMP-AUTO-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Pr_Line_Pnd_Pr_Lump_Auto_Ind.setValue(pnd_Lump_Auto_Ind);                                                                                                     //Natural: MOVE #LUMP-AUTO-IND TO #PR-LUMP-AUTO-IND
        pnd_Pr_Line_Pnd_Pr_Child_Prvsn.setValue(pnd_Child_Prvsn_Ind);                                                                                                     //Natural: MOVE #CHILD-PRVSN-IND TO #PR-CHILD-PRVSN
        pnd_Pr_Line_Pnd_Pr_Child_Prvsn_Fill.setValue(pnd_Child_Prvsn_Ind_Fill);                                                                                           //Natural: MOVE #CHILD-PRVSN-IND-FILL TO #PR-CHILD-PRVSN-FILL
        pnd_Pr_Line_Pnd_Pr_Name.setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Name().getValue(pnd_I));                                                         //Natural: MOVE CIS-CNTGNT-BENE-NAME ( #I ) TO #PR-NAME
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr().getValue(pnd_I).notEquals(getZero())))                                                    //Natural: IF CIS-CNTGNT-BENE-SSN-NBR ( #I ) NE 0
        {
            //* *IF  #CISA-RESID-ISSUE-STATE EQ 'CA' OR EQ '05'       /* JB122005N
            //*  KG CA SSN
            pnd_Pr_Line_Pnd_Pr_Soc.setValue("ON FILE");                                                                                                                   //Natural: MOVE 'ON FILE' TO #PR-SOC
            //* *ELSE                                                 /* JB122005N
            //* *    MOVE CIS-CNTGNT-BENE-SSN-NBR(#I)     TO  #SOC    /* JB122005
            //* *    MOVE EDITED #SOC-A (EM=XXX-XX-XXXX)  TO  #PR-SOC /* JB122005
            //* *END-IF                                               /* JB122005
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dob().getValue(pnd_I).notEquals(getZero())))                                                        //Natural: IF CIS-CNTGNT-BENE-DOB ( #I ) NE 0
        {
            pnd_Pr_Line_Pnd_Pr_Dob.setValueEdited(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dob().getValue(pnd_I),new ReportEditMask("MM' 'DD' 'YYYY"));            //Natural: MOVE EDITED CIS-CNTGNT-BENE-DOB ( #I ) ( EM = MM' 'DD' 'YYYY ) TO #PR-DOB
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde().getValue(pnd_I).notEquals("37") && ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde().getValue(pnd_I).notEquals("38"))) //Natural: IF CIS-CNTGNT-BENE-RLTN-CDE ( #I ) NE '37' AND CIS-CNTGNT-BENE-RLTN-CDE ( #I ) NE '38'
        {
            pnd_Pr_Line_Pnd_Pr_Relationship.setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn().getValue(pnd_I));                                             //Natural: MOVE CIS-CNTGNT-BENE-RLTN ( #I ) TO #PR-RELATIONSHIP
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde().getValue(pnd_I).equals("I")))                                                        //Natural: IF CIS-CNTGNT-BENE-RLTN-CDE ( #I ) EQ 'I'
            {
                pnd_Pr_Line_Pnd_Pr_Relationship.setValue("Org.");                                                                                                         //Natural: ASSIGN #PR-RELATIONSHIP := 'Org.'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde().getValue(pnd_I).equals("K") || ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde().getValue(pnd_I).equals("L"))) //Natural: IF CIS-CNTGNT-BENE-RLTN-CDE ( #I ) EQ 'K' OR CIS-CNTGNT-BENE-RLTN-CDE ( #I ) EQ 'L'
            {
                pnd_Pr_Line_Pnd_Pr_Relationship.setValue("Trust");                                                                                                        //Natural: ASSIGN #PR-RELATIONSHIP := 'Trust'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(pnd_I).notEquals(getZero()) || ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_I).notEquals(getZero()))) //Natural: IF CIS-CNTGNT-BENE-NMRTR-NBR ( #I ) NE 0 OR CIS-CNTGNT-BENE-DNMNTR-NBR ( #I ) NE 0
        {
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_I).equals(100)))                                                      //Natural: IF CIS-CNTGNT-BENE-DNMNTR-NBR ( #I ) EQ 100
            {
                pnd_Pr_Line_Pnd_Pr_Share.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(pnd_I),  //Natural: COMPRESS CIS-CNTGNT-BENE-NMRTR-NBR ( #I ) '%' INTO #PR-SHARE LEAVING NO SPACE
                    "%"));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Nmrtr.reset();                                                                                                                                        //Natural: RESET #NMRTR #DNMRTR #RESULT #REMAINDER
                pnd_Dnmrtr.reset();
                pnd_Result.reset();
                pnd_Remainder.reset();
                pnd_Nmrtr.compute(new ComputeParameters(false, pnd_Nmrtr), ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(pnd_I).multiply(100));    //Natural: ASSIGN #NMRTR := CIS-CNTGNT-BENE-NMRTR-NBR ( #I ) * 100
                pnd_Dnmrtr.setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_I));                                                        //Natural: ASSIGN #DNMRTR := CIS-CNTGNT-BENE-DNMNTR-NBR ( #I )
                pnd_Remainder.compute(new ComputeParameters(false, pnd_Remainder), pnd_Nmrtr.mod(pnd_Dnmrtr));                                                            //Natural: DIVIDE #DNMRTR INTO #NMRTR GIVING #RESULT REMAINDER #REMAINDER
                pnd_Result.compute(new ComputeParameters(false, pnd_Result), pnd_Nmrtr.divide(pnd_Dnmrtr));
                if (condition(pnd_Remainder.equals(getZero())))                                                                                                           //Natural: IF #REMAINDER EQ 0
                {
                    pnd_Pr_Line_Pnd_Pr_Share.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Result, "%"));                                                  //Natural: COMPRESS #RESULT '%' INTO #PR-SHARE LEAVING NO SPACE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Pr_Line_Pnd_Pr_Share.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(pnd_I),  //Natural: COMPRESS CIS-CNTGNT-BENE-NMRTR-NBR ( #I ) '/' CIS-CNTGNT-BENE-DNMNTR-NBR ( #I ) INTO #PR-SHARE LEAVING NO SPACE
                        "/", ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_I)));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sort_Key_Cnt_Pnd_Sort_Key.nadd(1);                                                                                                                            //Natural: ADD 1 TO #SORT-KEY
        pnd_Ctrs_Pnd_W_Cntgnt_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #W-CNTGNT-CNT
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("TPA")))                                                                                  //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'TPA'
        {
            pnd_Pr_Text_1_Pnd_Pr_Name_1.setValue(pnd_Pr_Line_Pnd_Pr_Name);                                                                                                //Natural: MOVE #PR-NAME TO #PR-NAME-1
            pnd_Pr_Text_1_Pnd_Pr_Relationship_1.setValue(pnd_Pr_Line_Pnd_Pr_Relationship);                                                                                //Natural: MOVE #PR-RELATIONSHIP TO #PR-RELATIONSHIP-1
            pnd_Pr_Text_1_Pnd_Pr_Share_1.setValue(pnd_Pr_Line_Pnd_Pr_Share);                                                                                              //Natural: MOVE #PR-SHARE TO #PR-SHARE-1
            pnd_Pr_Line_Pnd_Pr_Text.setValue(pnd_Pr_Text_1);                                                                                                              //Natural: MOVE #PR-TEXT-1 TO #PR-TEXT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IPRO")))                                                                                 //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IPRO'
        {
            pnd_Pr_Text_2_Pnd_Pr_Name_2.setValue(pnd_Pr_Line_Pnd_Pr_Name);                                                                                                //Natural: MOVE #PR-NAME TO #PR-NAME-2
            pnd_Pr_Text_2_Pnd_Pr_Relationship_2.setValue(pnd_Pr_Line_Pnd_Pr_Relationship);                                                                                //Natural: MOVE #PR-RELATIONSHIP TO #PR-RELATIONSHIP-2
            pnd_Pr_Line_Pnd_Pr_Text.setValue(pnd_Pr_Text_2);                                                                                                              //Natural: MOVE #PR-TEXT-2 TO #PR-TEXT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key.getValue(pnd_Ctrs_Pnd_W_Cntgnt_Cnt).setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                       //Natural: MOVE #SORT-KEY TO #W-CNTGNT-SORT-KEY ( #W-CNTGNT-CNT )
        pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Print_Line().getValue(pnd_Ctrs_Pnd_W_Cntgnt_Cnt).setValue(pnd_Pr_Line);                                                     //Natural: MOVE #PR-LINE TO #CISA-CNTGNT-PRINT-LINE ( #W-CNTGNT-CNT )
        pnd_Prmry_Extended_Name.reset();                                                                                                                                  //Natural: RESET #PRMRY-EXTENDED-NAME #CNTGNT-EXTENDED-NAME #OCCURENCE #FOUND-EXTENDED
        pnd_Cntgnt_Extended_Name.reset();
        pnd_Occurence.reset();
        pnd_Found_Extended.reset();
        pnd_Occurence.setValue(pnd_I);                                                                                                                                    //Natural: ASSIGN #OCCURENCE := #I
        DbsUtil.callnat(Cisn5001.class , getCurrentProcessState(), ldaCisl5000.getPnd_Cis_Ben_Super_1(), pnd_Rqst_Id_Key, pnd_Prmry_Extended_Name, pnd_Cntgnt_Extended_Name,  //Natural: CALLNAT 'CISN5001' #CIS-BEN-SUPER-1 #RQST-ID-KEY #PRMRY-EXTENDED-NAME #CNTGNT-EXTENDED-NAME #OCCURENCE #FOUND-EXTENDED
            pnd_Occurence, pnd_Found_Extended);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Found_Extended.getBoolean() && pnd_Cntgnt_Extended_Name.notEquals(" ")))                                                                        //Natural: IF #FOUND-EXTENDED AND #CNTGNT-EXTENDED-NAME NE ' '
        {
            pnd_Pr_Line.reset();                                                                                                                                          //Natural: RESET #PR-LINE
            //*     #CHILD-PRVSN-IND
            //*     #CHILD-PRVSN-IND-FILL
            pnd_Sort_Key_Cnt_Pnd_Sort_Key.nadd(1);                                                                                                                        //Natural: ADD 1 TO #SORT-KEY
            pnd_Ctrs_Pnd_W_Cntgnt_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #W-CNTGNT-CNT
            pnd_Pr_Line_Pnd_Pr_Text.setValue(pnd_Cntgnt_Extended_Name);                                                                                                   //Natural: MOVE #CNTGNT-EXTENDED-NAME TO #PR-TEXT
            pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key.getValue(pnd_Ctrs_Pnd_W_Cntgnt_Cnt).setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                   //Natural: MOVE #SORT-KEY TO #W-CNTGNT-SORT-KEY ( #W-CNTGNT-CNT )
            pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Print_Line().getValue(pnd_Ctrs_Pnd_W_Cntgnt_Cnt).setValue(pnd_Pr_Line);                                                 //Natural: MOVE #PR-LINE TO #CISA-CNTGNT-PRINT-LINE ( #W-CNTGNT-CNT )
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-4A-STANDARD
    }
    private void sub_Rtn_4b_Special_Text() throws Exception                                                                                                               //Natural: RTN-4B-SPECIAL-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_Pr_Line.reset();                                                                                                                                              //Natural: RESET #PR-LINE
        FOR05:                                                                                                                                                            //Natural: FOR #J = 1 #C-CIS-CNTGNT-BENE-SPCL-TXT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_C_Cis_Cntgnt_Bene_Spcl_Txt)); pnd_J.nadd(1))
        {
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt().getValue(pnd_I,pnd_J).equals(" ")))                                                  //Natural: IF CIS-CNTGNT-BENE-SPCL-TXT ( #I,#J ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sort_Key_Cnt_Pnd_Sort_Key.nadd(1);                                                                                                                        //Natural: ADD 1 TO #SORT-KEY
            pnd_Ctrs_Pnd_W_Cntgnt_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #W-CNTGNT-CNT
            pnd_Pr_Line_Pnd_Pr_Text.setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt().getValue(pnd_I,pnd_J));                                           //Natural: MOVE CIS-CNTGNT-BENE-SPCL-TXT ( #I,#J ) TO #PR-TEXT
            pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key.getValue(pnd_Ctrs_Pnd_W_Cntgnt_Cnt).setValue(pnd_Sort_Key_Cnt_Pnd_Sort_Key);                                                   //Natural: MOVE #SORT-KEY TO #W-CNTGNT-SORT-KEY ( #W-CNTGNT-CNT )
            pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Print_Line().getValue(pnd_Ctrs_Pnd_W_Cntgnt_Cnt).setValue(pnd_Pr_Line);                                                 //Natural: MOVE #PR-LINE TO #CISA-CNTGNT-PRINT-LINE ( #W-CNTGNT-CNT )
            pnd_Pr_Line.reset();                                                                                                                                          //Natural: RESET #PR-LINE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  RTN-4B-SPECIAL-TEXT
    }
    private void sub_Rtn_5_Free_Form_Text() throws Exception                                                                                                              //Natural: RTN-5-FREE-FORM-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_Pr_Line.reset();                                                                                                                                              //Natural: RESET #PR-LINE #W-PRMRY-CNT
        pnd_Ctrs_Pnd_W_Prmry_Cnt.reset();
        if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA")))                                                                                   //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IA'
        {
            FOR1:                                                                                                                                                         //Natural: FOR #I = 20 TO 1 STEP -1
            for (pnd_I.setValue(20); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
            {
                FOR2:                                                                                                                                                     //Natural: FOR #J = 3 TO 1 STEP -1
                for (pnd_J.setValue(3); condition(pnd_J.greaterOrEqual(1)); pnd_J.nsubtract(1))
                {
                    if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_I,pnd_J).notEquals(" ")))                                        //Natural: IF CIS-PRMRY-BENE-SPCL-TXT ( #I,#J ) NE ' '
                    {
                        pnd_Free_Form_Line.setValue(pnd_I);                                                                                                               //Natural: ASSIGN #FREE-FORM-LINE := #I
                        if (true) break FOR1;                                                                                                                             //Natural: ESCAPE BOTTOM ( FOR1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FOR1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FOR1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            FOR06:                                                                                                                                                        //Natural: FOR #I = 1 TO #FREE-FORM-LINE
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Free_Form_Line)); pnd_I.nadd(1))
            {
                FOR07:                                                                                                                                                    //Natural: FOR #J = 1 #C-CIS-PRMRY-BENE-SPCL-TXT
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_C_Cis_Prmry_Bene_Spcl_Txt)); pnd_J.nadd(1))
                {
                    pnd_Ctrs_Pnd_W_Prmry_Cnt.nadd(1);                                                                                                                     //Natural: ADD 1 TO #W-PRMRY-CNT
                    pnd_Pr_Line_Pnd_Pr_Text.setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_I,pnd_J));                                    //Natural: MOVE CIS-PRMRY-BENE-SPCL-TXT ( #I,#J ) TO #PR-TEXT
                    pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Print_Line().getValue(pnd_Ctrs_Pnd_W_Prmry_Cnt).setValue(pnd_Pr_Line);                                           //Natural: MOVE #PR-LINE TO #CISA-PRMRY-PRINT-LINE ( #W-PRMRY-CNT )
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  061307 START
            if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt().getValue("*","*").notEquals(" ")))                                                   //Natural: IF CIS-CNTGNT-BENE-SPCL-TXT ( *,* ) NE ' '
            {
                pnd_I.setValue(1);                                                                                                                                        //Natural: ASSIGN #I := 1
                                                                                                                                                                          //Natural: PERFORM RTN-4B-SPECIAL-TEXT
                sub_Rtn_4b_Special_Text();
                if (condition(Global.isEscape())) {return;}
                //*  061307 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  060706
        if (condition(DbsUtil.maskMatches(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id(),"'MDO'")))                                                                   //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ MASK ( 'MDO' )
        {
                                                                                                                                                                          //Natural: PERFORM READ-CIS-BENE-FILE-02
            sub_Read_Cis_Bene_File_02();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  RTN-5-FREE-FORM-TEXT
    }
    private void sub_Rtn_6a_Sort_Prmry_Array() throws Exception                                                                                                           //Natural: RTN-6A-SORT-PRMRY-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_I2.compute(new ComputeParameters(false, pnd_I2), pnd_Ctrs_Pnd_W_Prmry_Cnt.subtract(1));                                                                       //Natural: COMPUTE #I2 = #W-PRMRY-CNT - 1
        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 #I2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I2)); pnd_I.nadd(1))
        {
            pnd_J2.compute(new ComputeParameters(false, pnd_J2), pnd_Ctrs_Pnd_W_Prmry_Cnt.subtract(pnd_I));                                                               //Natural: COMPUTE #J2 = #W-PRMRY-CNT - #I
            FOR09:                                                                                                                                                        //Natural: FOR #J = 1 #J2
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_J2)); pnd_J.nadd(1))
            {
                if (condition(pnd_Ctrs_Pnd_W_Prmry_Sort_Key.getValue(pnd_J).greater(pnd_Ctrs_Pnd_W_Prmry_Sort_Key.getValue(pnd_J.getDec().add(1)))))                      //Natural: IF #W-PRMRY-SORT-KEY ( #J ) GT #W-PRMRY-SORT-KEY ( #J+1 )
                {
                    pnd_Temp_Sort_Key.setValue(pnd_Ctrs_Pnd_W_Prmry_Sort_Key.getValue(pnd_J.getDec().add(1)));                                                            //Natural: MOVE #W-PRMRY-SORT-KEY ( #J+1 ) TO #TEMP-SORT-KEY
                    pnd_Temp_Print_Line.setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Print_Line().getValue(pnd_J.getDec().add(1)));                                      //Natural: MOVE #CISA-PRMRY-PRINT-LINE ( #J+1 ) TO #TEMP-PRINT-LINE
                    pnd_Ctrs_Pnd_W_Prmry_Sort_Key.getValue(pnd_J.getDec().add(1)).setValue(pnd_Ctrs_Pnd_W_Prmry_Sort_Key.getValue(pnd_J));                                //Natural: MOVE #W-PRMRY-SORT-KEY ( #J ) TO #W-PRMRY-SORT-KEY ( #J+1 )
                    pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Print_Line().getValue(pnd_J.getDec().add(1)).setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Print_Line().getValue(pnd_J)); //Natural: MOVE #CISA-PRMRY-PRINT-LINE ( #J ) TO #CISA-PRMRY-PRINT-LINE ( #J+1 )
                    pnd_Ctrs_Pnd_W_Prmry_Sort_Key.getValue(pnd_J).setValue(pnd_Temp_Sort_Key);                                                                            //Natural: MOVE #TEMP-SORT-KEY TO #W-PRMRY-SORT-KEY ( #J )
                    pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Print_Line().getValue(pnd_J).setValue(pnd_Temp_Print_Line);                                                      //Natural: MOVE #TEMP-PRINT-LINE TO #CISA-PRMRY-PRINT-LINE ( #J )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  RTN-6A-SORT-PRMRY-ARRAY
    }
    private void sub_Rtn_6b_Sort_Cntgnt_Array() throws Exception                                                                                                          //Natural: RTN-6B-SORT-CNTGNT-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_I2.compute(new ComputeParameters(false, pnd_I2), pnd_Ctrs_Pnd_W_Cntgnt_Cnt.subtract(1));                                                                      //Natural: COMPUTE #I2 = #W-CNTGNT-CNT - 1
        FOR10:                                                                                                                                                            //Natural: FOR #I = 1 #I2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I2)); pnd_I.nadd(1))
        {
            pnd_J2.compute(new ComputeParameters(false, pnd_J2), pnd_Ctrs_Pnd_W_Cntgnt_Cnt.subtract(pnd_I));                                                              //Natural: COMPUTE #J2 = #W-CNTGNT-CNT - #I
            FOR11:                                                                                                                                                        //Natural: FOR #J = 1 #J2
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_J2)); pnd_J.nadd(1))
            {
                if (condition(pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key.getValue(pnd_J).greater(pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key.getValue(pnd_J.getDec().add(1)))))                    //Natural: IF #W-CNTGNT-SORT-KEY ( #J ) GT #W-CNTGNT-SORT-KEY ( #J+1 )
                {
                    pnd_Temp_Sort_Key.setValue(pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key.getValue(pnd_J.getDec().add(1)));                                                           //Natural: MOVE #W-CNTGNT-SORT-KEY ( #J+1 ) TO #TEMP-SORT-KEY
                    pnd_Temp_Print_Line.setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Print_Line().getValue(pnd_J.getDec().add(1)));                                     //Natural: MOVE #CISA-CNTGNT-PRINT-LINE ( #J+1 ) TO #TEMP-PRINT-LINE
                    pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key.getValue(pnd_J.getDec().add(1)).setValue(pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key.getValue(pnd_J));                              //Natural: MOVE #W-CNTGNT-SORT-KEY ( #J ) TO #W-CNTGNT-SORT-KEY ( #J+1 )
                    pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Print_Line().getValue(pnd_J.getDec().add(1)).setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Print_Line().getValue(pnd_J)); //Natural: MOVE #CISA-CNTGNT-PRINT-LINE ( #J ) TO #CISA-CNTGNT-PRINT-LINE ( #J+1 )
                    pnd_Ctrs_Pnd_W_Cntgnt_Sort_Key.getValue(pnd_J).setValue(pnd_Temp_Sort_Key);                                                                           //Natural: MOVE #TEMP-SORT-KEY TO #W-CNTGNT-SORT-KEY ( #J )
                    pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Print_Line().getValue(pnd_J).setValue(pnd_Temp_Print_Line);                                                     //Natural: MOVE #TEMP-PRINT-LINE TO #CISA-CNTGNT-PRINT-LINE ( #J )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  RTN-6B-SORT-CNTGNT-ARRAY
    }
    private void sub_Rtn_7_Set_Pda_Indicators() throws Exception                                                                                                          //Natural: RTN-7-SET-PDA-INDICATORS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //*    SET PRMRY VARIABLES
        //*    -------------------
        short decideConditionsMet988 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #W-PRMRY-STD-CNT GT 0 AND #W-PRMRY-SPECIAL-TEXT-CNT GT 0
        if (condition(pnd_Ctrs_Pnd_W_Prmry_Std_Cnt.greater(getZero()) && pnd_Ctrs_Pnd_W_Prmry_Special_Text_Cnt.greater(getZero())))
        {
            decideConditionsMet988++;
            pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Std_Or_Text_Ind().setValue("B");                                                                                         //Natural: ASSIGN #CISA-PRMRY-STD-OR-TEXT-IND := 'B'
        }                                                                                                                                                                 //Natural: WHEN #W-PRMRY-STD-WITH-TEXT-CNT GT 0
        else if (condition(pnd_Ctrs_Pnd_W_Prmry_Std_With_Text_Cnt.greater(getZero())))
        {
            decideConditionsMet988++;
            pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Std_Or_Text_Ind().setValue("B");                                                                                         //Natural: ASSIGN #CISA-PRMRY-STD-OR-TEXT-IND := 'B'
        }                                                                                                                                                                 //Natural: WHEN #W-PRMRY-STD-CNT GT 0
        else if (condition(pnd_Ctrs_Pnd_W_Prmry_Std_Cnt.greater(getZero())))
        {
            decideConditionsMet988++;
            pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Std_Or_Text_Ind().setValue("S");                                                                                         //Natural: ASSIGN #CISA-PRMRY-STD-OR-TEXT-IND := 'S'
        }                                                                                                                                                                 //Natural: WHEN #W-PRMRY-SPECIAL-TEXT-CNT GT 0
        else if (condition(pnd_Ctrs_Pnd_W_Prmry_Special_Text_Cnt.greater(getZero())))
        {
            decideConditionsMet988++;
            pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Std_Or_Text_Ind().setValue("T");                                                                                         //Natural: ASSIGN #CISA-PRMRY-STD-OR-TEXT-IND := 'T'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  STANDARD FORMAT
        if (condition(pdaCisa620.getCisa620_Pnd_Cisa_Bene_Std_Free().equals("N")))                                                                                        //Natural: IF #CISA-BENE-STD-FREE = 'N'
        {
            if (condition(pnd_Ctrs_Pnd_W_Prmry_Lump_Sum_Cnt.greater(getZero())))                                                                                          //Natural: IF #W-PRMRY-LUMP-SUM-CNT GT 0
            {
                pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Lump_Sum_Ind().setValue("N");                                                                                        //Natural: ASSIGN #CISA-PRMRY-LUMP-SUM-IND := 'N'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Lump_Sum_Ind().setValue("Y");                                                                                        //Natural: ASSIGN #CISA-PRMRY-LUMP-SUM-IND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ctrs_Pnd_W_Prmry_Auto_Cmnt_Val_Cnt.greater(getZero())))                                                                                     //Natural: IF #W-PRMRY-AUTO-CMNT-VAL-CNT GT 0
            {
                pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Auto_Cmnt_Val_Ind().setValue("Y");                                                                                   //Natural: ASSIGN #CISA-PRMRY-AUTO-CMNT-VAL-IND := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Auto_Cmnt_Val_Ind().setValue("N");                                                                                   //Natural: ASSIGN #CISA-PRMRY-AUTO-CMNT-VAL-IND := 'N'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Cnt().setValue(pnd_Ctrs_Pnd_W_Prmry_Cnt);                                                                                    //Natural: ASSIGN #CISA-PRMRY-CNT := #W-PRMRY-CNT
        //*    SET CNTGNT VARIABLES
        //*    --------------------
        short decideConditionsMet1020 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #W-CNTGNT-STD-CNT GT 0 AND #W-CNTGNT-SPECIAL-TEXT-CNT GT 0
        if (condition(pnd_Ctrs_Pnd_W_Cntgnt_Std_Cnt.greater(getZero()) && pnd_Ctrs_Pnd_W_Cntgnt_Special_Text_Cnt.greater(getZero())))
        {
            decideConditionsMet1020++;
            pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Std_Or_Text_Ind().setValue("B");                                                                                        //Natural: ASSIGN #CISA-CNTGNT-STD-OR-TEXT-IND := 'B'
        }                                                                                                                                                                 //Natural: WHEN #W-CNTGNT-STD-WITH-TEXT-CNT GT 0
        else if (condition(pnd_Ctrs_Pnd_W_Cntgnt_Std_With_Text_Cnt.greater(getZero())))
        {
            decideConditionsMet1020++;
            pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Std_Or_Text_Ind().setValue("B");                                                                                        //Natural: ASSIGN #CISA-CNTGNT-STD-OR-TEXT-IND := 'B'
        }                                                                                                                                                                 //Natural: WHEN #W-CNTGNT-STD-CNT GT 0
        else if (condition(pnd_Ctrs_Pnd_W_Cntgnt_Std_Cnt.greater(getZero())))
        {
            decideConditionsMet1020++;
            pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Std_Or_Text_Ind().setValue("S");                                                                                        //Natural: ASSIGN #CISA-CNTGNT-STD-OR-TEXT-IND := 'S'
        }                                                                                                                                                                 //Natural: WHEN #W-CNTGNT-SPECIAL-TEXT-CNT GT 0
        else if (condition(pnd_Ctrs_Pnd_W_Cntgnt_Special_Text_Cnt.greater(getZero())))
        {
            decideConditionsMet1020++;
            pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Std_Or_Text_Ind().setValue("T");                                                                                        //Natural: ASSIGN #CISA-CNTGNT-STD-OR-TEXT-IND := 'T'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  STANDARD FORMAT
        if (condition(pdaCisa620.getCisa620_Pnd_Cisa_Bene_Std_Free().equals("N")))                                                                                        //Natural: IF #CISA-BENE-STD-FREE = 'N'
        {
            if (condition(pnd_Ctrs_Pnd_W_Cntgnt_Lump_Sum_Cnt.greater(getZero())))                                                                                         //Natural: IF #W-CNTGNT-LUMP-SUM-CNT GT 0
            {
                pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Lump_Sum_Ind().setValue("N");                                                                                       //Natural: ASSIGN #CISA-CNTGNT-LUMP-SUM-IND := 'N'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Lump_Sum_Ind().setValue("Y");                                                                                       //Natural: ASSIGN #CISA-CNTGNT-LUMP-SUM-IND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ctrs_Pnd_W_Cntgnt_Auto_Cmnt_Val_Cnt.greater(getZero())))                                                                                    //Natural: IF #W-CNTGNT-AUTO-CMNT-VAL-CNT GT 0
            {
                pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Auto_Cmnt_Val_Ind().setValue("Y");                                                                                  //Natural: ASSIGN #CISA-CNTGNT-AUTO-CMNT-VAL-IND := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Auto_Cmnt_Val_Ind().setValue("N");                                                                                  //Natural: ASSIGN #CISA-CNTGNT-AUTO-CMNT-VAL-IND := 'N'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Cnt().setValue(pnd_Ctrs_Pnd_W_Cntgnt_Cnt);                                                                                  //Natural: ASSIGN #CISA-CNTGNT-CNT := #W-CNTGNT-CNT
        //*  RTN-7-SET-PDA-INDICATORS
    }
    private void sub_Rtn_99_Debug() throws Exception                                                                                                                      //Natural: RTN-99-DEBUG
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        getReports().write(0, "-",new RepeatItem(20),Global.getPROGRAM(),"(##L5300.)","-",new RepeatItem(20));                                                            //Natural: WRITE '-' ( 20 ) *PROGRAM '(##L5300.)' '-' ( 20 )
        if (Global.isEscape()) return;
        //* *WRITE '=' #CISA-BENE-TIAA-NBR
        //* *WRITE '=' #CISA-BENE-CREF-NBR
        //* *WRITE '=' #CISA-BENE-RQST-ID
        //* *WRITE '=' #CISA-BENE-UNIQUE-ID-NBR
        //* *WRITE '=' #CISA-BENE-RQST-ID-KEY
        //* *WRITE '=' #CISA-BENE-STD-FREE
        //* *WRITE '=' #CISA-BENE-ESTATE
        //* *WRITE '=' #CISA-BENE-TRUST
        //* *WRITE '=' #CISA-BENE-CATEGORY
        //* *WRITE '=' #CISA-PRMRY-BENE-CHILD-PRVSN
        //* *WRITE '=' #CISA-CNTGNT-BENE-CHILD-PRVSN
        //* *WRITE '=' #CISA-CLCLTN-BENE-DOD-A
        //* *WRITE '=' #CISA-CLCLTN-BENE-CROSSOVER
        //* *WRITE '=' #CISA-CLCLTN-BENE-NAME
        //* *WRITE '=' #CISA-CLCLTN-BENE-RLTNSHP-CDE
        //* *WRITE '=' #CISA-CLCLTN-BENE-SSN-NBR
        //* *WRITE '=' #CISA-CLCLTN-BENE-DOB-A
        //* *WRITE '=' #CISA-CLCLTN-BENE-SEX-CDE
        //* *WRITE '=' #CISA-CLCLTN-BENE-CALC-METHOD
        //* *WRITE '=' #CISA-CLCLTN-BENE-CALC-METHOD-DESC
        //* *WRITE '=' #CISA-CLCLTN-BENE-CALC-BENEFICIARY
        //* *WRITE '=' #CISA-PRMRY-STD-OR-TEXT-IND
        //* *WRITE '=' #CISA-PRMRY-LUMP-SUM-IND
        //* *WRITE '=' #CISA-PRMRY-AUTO-CMNT-VAL-IND
        //* *WRITE '=' #CISA-PRMRY-CNT
        //* *WRITE '=' #FOUND-EXTENDED
        //* *WRITE '=' #CIS-BEN-SUPER-1
        //* *WRITE '=' #PRMRY-EXTENDED-NAME
        //* *WRITE '=' #OCCURENCE
        //* *WRITE '-'(10) 'PRMRY BENE INFO' '-' (10)
        //* *FOR #I = 1 #CISA-PRMRY-CNT
        //*    WRITE #W-PRMRY-SORT-KEY      (#I) (EM=999)
        //*          #CISA-PRMRY-PRINT-LINE (#I) (AL=75)
        //* *  WRITE #CISA-PRMRY-PRINT-LINE (#I) (AL=78)
        //* *END-FOR
        //* *WRITE '=' #CISA-CNTGNT-STD-OR-TEXT-IND
        //* *WRITE '=' #CISA-CNTGNT-LUMP-SUM-IND
        //* *WRITE '=' #CISA-CNTGNT-AUTO-CMNT-VAL-IND
        //* *WRITE '=' #CISA-CNTGNT-CNT
        //* *WRITE '-'(10) 'CNTGNT BENE INFO' '-'(10)
        //* *FOR #I = 1 #CISA-CNTGNT-CNT
        //*  WRITE #W-CNTGNT-SORT-KEY      (#I) (EM=999)
        //*        #CISA-CNTGNT-PRINT-LINE (#I) (AL=75)
        //* *  WRITE #CISA-CNTGNT-PRINT-LINE (#I) (AL=78)
        //* *  WRITE '=' #PR-SHARE
        //* *END-FOR
        getReports().write(0, "=",pdaCisa620.getCisa620_Pnd_Cisa_Return_Code());                                                                                          //Natural: WRITE '=' #CISA-RETURN-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCisa620.getCisa620_Pnd_Cisa_Msg(), new AlphanumericLength (78));                                                                     //Natural: WRITE '=' #CISA-MSG ( AL = 78 )
        if (Global.isEscape()) return;
        //*  RTN-99-DEBUG
    }
    private void sub_Translate_Name() throws Exception                                                                                                                    //Natural: TRANSLATE-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------*
        if (condition(pnd_Name_Pnd_Name_Arr.getValue(1).notEquals(" ") || pnd_Name_Pnd_Name_Arr.getValue(1).equals(".") || pnd_Name_Pnd_Name_Arr.getValue(1).equals(",")  //Natural: IF #NAME-ARR ( 1 ) NE ' ' OR #NAME-ARR ( 1 ) EQ '.' OR #NAME-ARR ( 1 ) EQ ',' OR #NAME-ARR ( 1 ) EQ "'" OR #NAME-ARR ( 1 ) EQ ';' OR #NAME-ARR ( 1 ) EQ ':' OR #NAME-ARR ( 1 ) EQ '/'
            || pnd_Name_Pnd_Name_Arr.getValue(1).equals("'") || pnd_Name_Pnd_Name_Arr.getValue(1).equals(";") || pnd_Name_Pnd_Name_Arr.getValue(1).equals(":") 
            || pnd_Name_Pnd_Name_Arr.getValue(1).equals("/")))
        {
            DbsUtil.examine(new ExamineSource(pnd_Name_Pnd_Name_Arr.getValue(1)), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE #NAME-ARR ( 1 ) TRANSLATE INTO UPPER CASE
            DbsUtil.examine(new ExamineSource(pnd_Name_Pnd_Name_Arr.getValue("*"),true), new ExamineSearch(" "), new ExamineGivingIndex(pnd_L));                          //Natural: EXAMINE FULL #NAME-ARR ( * ) FOR ' ' GIVING INDEX #L
            if (condition(pnd_L.less(35)))                                                                                                                                //Natural: IF #L LT 35
            {
                DbsUtil.examine(new ExamineSource(pnd_Name_Pnd_Name_Arr.getValue(2,":",pnd_L)), new ExamineTranslate(TranslateOption.Lower));                             //Natural: EXAMINE #NAME-ARR ( 2:#L ) TRANSLATE INTO LOWER CASE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR12:                                                                                                                                                            //Natural: FOR #P = 1 TO 35
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(35)); pnd_P.nadd(1))
        {
            if (condition(pnd_Name_Pnd_Name_Arr.getValue(pnd_P).equals(" ") || pnd_Name_Pnd_Name_Arr.getValue(pnd_P).equals(".") || pnd_Name_Pnd_Name_Arr.getValue(pnd_P).equals(",")  //Natural: IF #NAME-ARR ( #P ) EQ ' ' OR #NAME-ARR ( #P ) EQ '.' OR #NAME-ARR ( #P ) EQ ',' OR #NAME-ARR ( #P ) EQ "'" OR #NAME-ARR ( #P ) EQ ';' OR #NAME-ARR ( #P ) EQ ':' OR #NAME-ARR ( #P ) EQ '/'
                || pnd_Name_Pnd_Name_Arr.getValue(pnd_P).equals("'") || pnd_Name_Pnd_Name_Arr.getValue(pnd_P).equals(";") || pnd_Name_Pnd_Name_Arr.getValue(pnd_P).equals(":") 
                || pnd_Name_Pnd_Name_Arr.getValue(pnd_P).equals("/")))
            {
                if (condition(pnd_P.less(35)))                                                                                                                            //Natural: IF #P LT 35
                {
                    DbsUtil.examine(new ExamineSource(pnd_Name_Pnd_Name_Arr.getValue(pnd_P.getDec().add(1))), new ExamineTranslate(TranslateOption.Upper));               //Natural: EXAMINE #NAME-ARR ( #P+1 ) TRANSLATE INTO UPPER CASE
                    pnd_N.compute(new ComputeParameters(false, pnd_N), pnd_P.add(2));                                                                                     //Natural: ASSIGN #N := #P + 2
                    FOR13:                                                                                                                                                //Natural: FOR #M = #N TO 35
                    for (pnd_M.setValue(pnd_N); condition(pnd_M.lessOrEqual(35)); pnd_M.nadd(1))
                    {
                        if (condition(pnd_N.lessOrEqual(35) && pnd_M.lessOrEqual(35)))                                                                                    //Natural: IF #N LE 35 AND #M LE 35
                        {
                            DbsUtil.examine(new ExamineSource(pnd_Name_Pnd_Name_Arr.getValue(pnd_N,":",pnd_M)), new ExamineTranslate(TranslateOption.Lower));             //Natural: EXAMINE #NAME-ARR ( #N:#M ) TRANSLATE INTO LOWER CASE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  TRANSLATE NAME
    }
    private void sub_Read_Cis_Bene_File_02() throws Exception                                                                                                             //Natural: READ-CIS-BENE-FILE-02
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Found.reset();                                                                                                                                                //Natural: RESET #FOUND
        ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Rcrd_Type_Cde().setValue("2");                                                                                       //Natural: ASSIGN #S-CIS-RCRD-TYPE-CDE := '2'
        ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Rqst_Id().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Bene_Rqst_Id());                                              //Natural: ASSIGN #S-CIS-BENE-RQST-ID := #CISA-BENE-RQST-ID
        ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Unique_Id_Nbr().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Bene_Unique_Id_Nbr());                                  //Natural: ASSIGN #S-CIS-BENE-UNIQUE-ID-NBR := #CISA-BENE-UNIQUE-ID-NBR
        ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Tiaa_Nbr().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Bene_Tiaa_Nbr());                                            //Natural: ASSIGN #S-CIS-BENE-TIAA-NBR := #CISA-BENE-TIAA-NBR
        ldaCisl5001.getVw_cis_Bene_File_02().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) CIS-BENE-FILE-02 WITH CIS-BEN-SUPER-1 = #CIS-BEN-SUPER-1
        (
        "READ02",
        new Wc[] { new Wc("CIS_BEN_SUPER_1", ">=", ldaCisl5000.getPnd_Cis_Ben_Super_1(), WcType.BY) },
        new Oc[] { new Oc("CIS_BEN_SUPER_1", "ASC") },
        1
        );
        READ02:
        while (condition(ldaCisl5001.getVw_cis_Bene_File_02().readNextRow("READ02")))
        {
            if (condition(ldaCisl5001.getCis_Bene_File_02_Cis_Bene_Rqst_Id().notEquals(ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Rqst_Id()) ||                    //Natural: IF CIS-BENE-FILE-02.CIS-BENE-RQST-ID NE #S-CIS-BENE-RQST-ID OR CIS-BENE-FILE-02.CIS-BENE-UNIQUE-ID-NBR NE #S-CIS-BENE-UNIQUE-ID-NBR OR CIS-BENE-FILE-02.CIS-BENE-TIAA-NBR NE #S-CIS-BENE-TIAA-NBR
                ldaCisl5001.getCis_Bene_File_02_Cis_Bene_Unique_Id_Nbr().notEquals(ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Unique_Id_Nbr()) || 
                ldaCisl5001.getCis_Bene_File_02_Cis_Bene_Tiaa_Nbr().notEquals(ldaCisl5000.getPnd_Cis_Ben_Super_1_Pnd_S_Cis_Bene_Tiaa_Nbr())))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Found.setValue(true);                                                                                                                                     //Natural: ASSIGN #FOUND := TRUE
            FOR14:                                                                                                                                                        //Natural: FOR #I = 60 TO 1 STEP -1
            for (pnd_I.setValue(60); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
            {
                if (condition(ldaCisl5001.getCis_Bene_File_02_Cis_Bene_Dsgntn_Txt().getValue(pnd_I).notEquals(" ")))                                                      //Natural: IF CIS-BENE-FILE-02.CIS-BENE-DSGNTN-TXT ( #I ) NE ' '
                {
                    pnd_Free_Form_Line.setValue(pnd_I);                                                                                                                   //Natural: ASSIGN #FREE-FORM-LINE := #I
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR15:                                                                                                                                                        //Natural: FOR #I = 1 TO #FREE-FORM-LINE
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Free_Form_Line)); pnd_I.nadd(1))
            {
                pnd_Ctrs_Pnd_W_Prmry_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #W-PRMRY-CNT
                pnd_Pr_Line_Pnd_Pr_Text.setValue(ldaCisl5001.getCis_Bene_File_02_Cis_Bene_Dsgntn_Txt().getValue(pnd_I));                                                  //Natural: MOVE CIS-BENE-DSGNTN-TXT ( #I ) TO #PR-TEXT
                pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Print_Line().getValue(pnd_Ctrs_Pnd_W_Prmry_Cnt).setValue(pnd_Pr_Line);                                               //Natural: MOVE #PR-LINE TO #CISA-PRMRY-PRINT-LINE ( #W-PRMRY-CNT )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(! (pnd_Found.getBoolean())))                                                                                                                        //Natural: IF NOT #FOUND
        {
            pdaCisa620.getCisa620_Pnd_Cisa_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "(##L1105.)", ldaCisl5000.getPnd_Cis_Ben_Super_1(), "NOT FOUND"));        //Natural: COMPRESS *PROGRAM '(##L1105.)' #CIS-BEN-SUPER-1 'NOT FOUND' INTO #CISA-MSG
            pdaCisa620.getCisa620_Pnd_Cisa_Return_Code().setValue(99);                                                                                                    //Natural: ASSIGN #CISA-RETURN-CODE := 99
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        if (condition(Global.getERROR_NR().equals(3145)))                                                                                                                 //Natural: IF *ERROR-NR = 3145
        {
            if (condition(pnd_Retry_Ctr.less(300)))                                                                                                                       //Natural: IF #RETRY-CTR LT 300
            {
                pnd_Retry_Ctr.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CTR
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaCisa620.getCisa620_Pnd_Cisa_Msg().setValue(DbsUtil.compress(Global.getPROGRAM(), "(0740)", "ADABAS ERROR", "NBR  :", Global.getERROR_NR(),                     //Natural: COMPRESS *PROGRAM '(0740)' 'ADABAS ERROR' 'NBR  :' *ERROR-NR 'LINE :' *ERROR-LINE INTO #CISA-MSG
            "LINE :", Global.getERROR_LINE()));
        pdaCisa620.getCisa620_Pnd_Cisa_Return_Code().setValue(36);                                                                                                        //Natural: ASSIGN #CISA-RETURN-CODE := 36
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
    }
}
