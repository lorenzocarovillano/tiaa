/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:08:59 PM
**        * FROM NATURAL SUBPROGRAM : Cisn2081
************************************************************
**        * FILE NAME            : Cisn2081.java
**        * CLASS NAME           : Cisn2081
**        * INSTANCE NAME        : Cisn2081
************************************************************
************************************************************************
*  PROGRAM:   CISN2000                                                 *
*   SYSTEM:   CIS                                                      *
*     DATE:   JANUARY 28, 1997                                         *
*                                                                      *
* DESCRIPTION                                                          *
* -------------------------------------------------------------------- *
* THE PURPOSE OF THE SUBPROGRAM IS TO READ THE FILE CIS-BENE-FILE      *
* AND LOAD TO PDA CISA2081.                                            *
************************************************************************
*    DATE     PROGRAMMER        CHANGE DESCRIPTION                     *
* ----------  ----------------  -------------------------------------- *
* 06/05/2008  JANET BERGHEISER  RESTOW FOR CISL2082 FOR NATURAL UPGRADE*
* 05/10/2017 - GHOSABE      - PIN EXPANSION CHANGES.  (C420007)    PINE*
************************************************************************
*

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisn2081 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCisa2081 pdaCisa2081;
    private LdaCisl2081 ldaCisl2081;
    private LdaCisl2082 ldaCisl2082;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_R;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;
    private DbsField pnd_Key_Pnd_Rqst_Id;
    private DbsField pnd_Key_Pnd_Unq_Id;
    private DbsField pnd_Key_Pnd_Tiaa_Nbr;
    private DbsField pnd_Cis_Ben_Super_2;

    private DbsGroup pnd_Cis_Ben_Super_2__R_Field_2;
    private DbsField pnd_Cis_Ben_Super_2_Pnd_Cis_Rcrd_Type_Cde;
    private DbsField pnd_Cis_Ben_Super_2_Pnd_Cis_Bene_Tiaa_Nbr;
    private DbsField pnd_Cis_Ben_Super_3;

    private DbsGroup pnd_Cis_Ben_Super_3__R_Field_3;
    private DbsField pnd_Cis_Ben_Super_3_Pnd_Cis_Rcrd_Type_Cde_1;
    private DbsField pnd_Cis_Ben_Super_3_Pnd_Cis_Bene_Rqst_Id_Key;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCisl2081 = new LdaCisl2081();
        registerRecord(ldaCisl2081);
        registerRecord(ldaCisl2081.getVw_cis_Bene_File_01());
        ldaCisl2082 = new LdaCisl2082();
        registerRecord(ldaCisl2082);
        registerRecord(ldaCisl2082.getVw_cis_Bene_File_02());

        // parameters
        parameters = new DbsRecord();
        pdaCisa2081 = new PdaCisa2081(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.NUMERIC, 2);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 25);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);
        pnd_Key_Pnd_Rqst_Id = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 8);
        pnd_Key_Pnd_Unq_Id = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Unq_Id", "#UNQ-ID", FieldType.NUMERIC, 7);
        pnd_Key_Pnd_Tiaa_Nbr = pnd_Key__R_Field_1.newFieldInGroup("pnd_Key_Pnd_Tiaa_Nbr", "#TIAA-NBR", FieldType.STRING, 10);
        pnd_Cis_Ben_Super_2 = localVariables.newFieldInRecord("pnd_Cis_Ben_Super_2", "#CIS-BEN-SUPER-2", FieldType.STRING, 11);

        pnd_Cis_Ben_Super_2__R_Field_2 = localVariables.newGroupInRecord("pnd_Cis_Ben_Super_2__R_Field_2", "REDEFINE", pnd_Cis_Ben_Super_2);
        pnd_Cis_Ben_Super_2_Pnd_Cis_Rcrd_Type_Cde = pnd_Cis_Ben_Super_2__R_Field_2.newFieldInGroup("pnd_Cis_Ben_Super_2_Pnd_Cis_Rcrd_Type_Cde", "#CIS-RCRD-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Cis_Ben_Super_2_Pnd_Cis_Bene_Tiaa_Nbr = pnd_Cis_Ben_Super_2__R_Field_2.newFieldInGroup("pnd_Cis_Ben_Super_2_Pnd_Cis_Bene_Tiaa_Nbr", "#CIS-BENE-TIAA-NBR", 
            FieldType.STRING, 10);
        pnd_Cis_Ben_Super_3 = localVariables.newFieldInRecord("pnd_Cis_Ben_Super_3", "#CIS-BEN-SUPER-3", FieldType.STRING, 36);

        pnd_Cis_Ben_Super_3__R_Field_3 = localVariables.newGroupInRecord("pnd_Cis_Ben_Super_3__R_Field_3", "REDEFINE", pnd_Cis_Ben_Super_3);
        pnd_Cis_Ben_Super_3_Pnd_Cis_Rcrd_Type_Cde_1 = pnd_Cis_Ben_Super_3__R_Field_3.newFieldInGroup("pnd_Cis_Ben_Super_3_Pnd_Cis_Rcrd_Type_Cde_1", "#CIS-RCRD-TYPE-CDE-1", 
            FieldType.STRING, 1);
        pnd_Cis_Ben_Super_3_Pnd_Cis_Bene_Rqst_Id_Key = pnd_Cis_Ben_Super_3__R_Field_3.newFieldInGroup("pnd_Cis_Ben_Super_3_Pnd_Cis_Bene_Rqst_Id_Key", 
            "#CIS-BENE-RQST-ID-KEY", FieldType.STRING, 35);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCisl2081.initializeValues();
        ldaCisl2082.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cisn2081() throws Exception
    {
        super("Cisn2081");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Cis_Ben_Super_2_Pnd_Cis_Rcrd_Type_Cde.setValue("1");                                                                                                          //Natural: ASSIGN #CIS-RCRD-TYPE-CDE := '1'
        pnd_Cis_Ben_Super_2_Pnd_Cis_Bene_Tiaa_Nbr.setValue(pdaCisa2081.getCisa2081_Cis_Bene_Tiaa_Nbr());                                                                  //Natural: ASSIGN #CIS-BENE-TIAA-NBR := CISA2081.CIS-BENE-TIAA-NBR
        ldaCisl2081.getVw_cis_Bene_File_01().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) CIS-BENE-FILE-01 BY CIS-BEN-SUPER-2 EQ #CIS-BEN-SUPER-2
        (
        "R1",
        new Wc[] { new Wc("CIS_BEN_SUPER_2", ">=", pnd_Cis_Ben_Super_2, WcType.BY) },
        new Oc[] { new Oc("CIS_BEN_SUPER_2", "ASC") },
        1
        );
        R1:
        while (condition(ldaCisl2081.getVw_cis_Bene_File_01().readNextRow("R1")))
        {
            if (condition(pnd_Cis_Ben_Super_2_Pnd_Cis_Bene_Tiaa_Nbr.notEquals(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr())))                                      //Natural: IF #CIS-BENE-TIAA-NBR NE CIS-BENE-FILE-01.CIS-BENE-TIAA-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pdaCisa2081.getCisa2081_Cis_Isn_01().setValue(ldaCisl2081.getVw_cis_Bene_File_01().getAstISN("R1"));                                                          //Natural: ASSIGN CISA2081.CIS-ISN-01 := *ISN ( R1. )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *
        if (condition(pdaCisa2081.getCisa2081_Cis_Isn_01().equals(getZero())))                                                                                            //Natural: IF CISA2081.CIS-ISN-01 EQ 0
        {
                                                                                                                                                                          //Natural: PERFORM GET-BYRQST-ID-KEY
            sub_Get_Byrqst_Id_Key();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa2081.getCisa2081_Cis_Isn_01().notEquals(getZero())))                                                                                         //Natural: IF CISA2081.CIS-ISN-01 NE 0
        {
            GET1:                                                                                                                                                         //Natural: GET CIS-BENE-FILE-01 CISA2081.CIS-ISN-01
            ldaCisl2081.getVw_cis_Bene_File_01().readByID(pdaCisa2081.getCisa2081_Cis_Isn_01().getLong(), "GET1");
            ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Sync_Ind().setValue(pdaCisa2081.getCisa2081_Cis_Bene_Sync_Ind());                                                    //Natural: ASSIGN CIS-BENE-FILE-01.CIS-BENE-SYNC-IND := CISA2081.CIS-BENE-SYNC-IND
            ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Sync_Upd_Date().setValue(pdaCisa2081.getCisa2081_Cis_Bene_Sync_Upd_Date());                                          //Natural: ASSIGN CIS-BENE-FILE-01.CIS-BENE-SYNC-UPD-DATE := CISA2081.CIS-BENE-SYNC-UPD-DATE
            getReports().write(0, "=",pdaCisa2081.getCisa2081_Cis_Bene_Tiaa_Nbr(),"UPDATED");                                                                             //Natural: WRITE '=' CISA2081.CIS-BENE-TIAA-NBR 'UPDATED'
            if (Global.isEscape()) return;
            ldaCisl2081.getVw_cis_Bene_File_01().updateDBRow("GET1");                                                                                                     //Natural: UPDATE ( GET1. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BYRQST-ID-KEY
    }
    private void sub_Get_Byrqst_Id_Key() throws Exception                                                                                                                 //Natural: GET-BYRQST-ID-KEY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cis_Ben_Super_3_Pnd_Cis_Rcrd_Type_Cde_1.setValue("1");                                                                                                        //Natural: ASSIGN #CIS-RCRD-TYPE-CDE-1 := '1'
        pnd_Cis_Ben_Super_3_Pnd_Cis_Bene_Rqst_Id_Key.setValue(pdaCisa2081.getCisa2081_Cis_Bene_Rqst_Id_Key());                                                            //Natural: ASSIGN #CIS-BENE-RQST-ID-KEY := CISA2081.CIS-BENE-RQST-ID-KEY
        ldaCisl2081.getVw_cis_Bene_File_01().startDatabaseRead                                                                                                            //Natural: READ ( 1 ) CIS-BENE-FILE-01 BY CIS-BEN-SUPER-2 EQ #CIS-BEN-SUPER-2
        (
        "R2",
        new Wc[] { new Wc("CIS_BEN_SUPER_2", ">=", pnd_Cis_Ben_Super_2, WcType.BY) },
        new Oc[] { new Oc("CIS_BEN_SUPER_2", "ASC") },
        1
        );
        R2:
        while (condition(ldaCisl2081.getVw_cis_Bene_File_01().readNextRow("R2")))
        {
            if (condition(pnd_Cis_Ben_Super_2_Pnd_Cis_Bene_Tiaa_Nbr.notEquals(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr())))                                      //Natural: IF #CIS-BENE-TIAA-NBR NE CIS-BENE-FILE-01.CIS-BENE-TIAA-NBR
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pdaCisa2081.getCisa2081_Cis_Isn_01().setValue(ldaCisl2081.getVw_cis_Bene_File_01().getAstISN("R2"));                                                          //Natural: ASSIGN CISA2081.CIS-ISN-01 := *ISN ( R2. )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
