/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:21:50 PM
**        * FROM NATURAL PROGRAM : Cisb9900
************************************************************
**        * FILE NAME            : Cisb9900.java
**        * CLASS NAME           : Cisb9900
**        * INSTANCE NAME        : Cisb9900
************************************************************
**======================================================================
** PROGRAM NAME   : CISB9900
** DESCRIPTION    : EXTRACT DATA THAT WILL BE SENT TO CPI
** CREATED        : 03/30/2018
** CREATED BY     : BUDDY NEWSOM
**
** HISTORY:
** DATE       CHANGED BY   DESCRIPTION OF CHANGE
**------------------------------------------------------------------
** 11/28/2018BUDDY NEWSOM  THIS IS A DATA ISSUE FROM THE KAFKA. THERE
**                         ARE 2 DUPLICATE RECORDS, ALL THE DETAILS
**                         INCLUDING THE EVENT_IDENTIFIER ARE SAME. ALSO
**                         THE BUSINESS DAY TOPIC FROM KAFKA IS COUNTING
**                         THESE 2 RECORDS AS DIFFERENT EVENTS.
**                         INC4678056
**======================================================================

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb9900 extends BLNatBase
{
    // Data Areas
    private GdaCisg0051 gdaCisg0051;
    private PdaCisa0050 pdaCisa0050;
    private PdaCisa1000 pdaCisa1000;
    private PdaNeca4000 pdaNeca4000;
    private LdaAppl170 ldaAppl170;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Recover_File;
    private DbsField pnd_Recover_File_Pnd_Recover_Tiaa;

    private DbsGroup pnd_Recover_File__R_Field_1;
    private DbsField pnd_Recover_File_Pnd_Recover_Print_Ind;
    private DbsField pnd_Recover_File_Pnd_Recover_Filler;

    private DataAccessProgramView vw_cis_Prtcpnt;
    private DbsField cis_Prtcpnt_Cis_Pin_Nbr;
    private DbsField cis_Prtcpnt_Cis_Rqst_Id_Key;
    private DbsField cis_Prtcpnt_Cis_Tiaa_Nbr;
    private DbsField cis_Prtcpnt_Cis_Cert_Nbr;
    private DbsField cis_Prtcpnt_Cis_Tiaa_Doi;
    private DbsField cis_Prtcpnt_Cis_Cref_Doi;
    private DbsField cis_Prtcpnt_Cis_Frst_Annt_Ssn;
    private DbsField cis_Prtcpnt_Cis_Frst_Annt_Prfx;
    private DbsField cis_Prtcpnt_Cis_Frst_Annt_Frst_Nme;
    private DbsField cis_Prtcpnt_Cis_Frst_Annt_Mid_Nme;
    private DbsField cis_Prtcpnt_Cis_Frst_Annt_Lst_Nme;
    private DbsField cis_Prtcpnt_Cis_Frst_Annt_Sffx;
    private DbsField cis_Prtcpnt_Cis_Frst_Annt_Ctznshp_Cd;
    private DbsField cis_Prtcpnt_Cis_Frst_Annt_Rsdnc_Cde;
    private DbsField cis_Prtcpnt_Cis_Frst_Annt_Dob;
    private DbsField cis_Prtcpnt_Cis_Frst_Annt_Sex_Cde;
    private DbsField cis_Prtcpnt_Cis_Frst_Annt_Calc_Method;
    private DbsField cis_Prtcpnt_Cis_Four_Fifty_Seven_Ind;
    private DbsField cis_Prtcpnt_Cis_Institution_Name;
    private DbsField cis_Prtcpnt_Cis_Opn_Clsd_Ind;
    private DbsField cis_Prtcpnt_Cis_Status_Cd;
    private DbsField cis_Prtcpnt_Cis_Cntrct_Type;
    private DbsField cis_Prtcpnt_Cis_Cntrct_Apprvl_Ind;
    private DbsField cis_Prtcpnt_Cis_Rqst_Id;
    private DbsField cis_Prtcpnt_Cis_Hold_Cde;
    private DbsField cis_Prtcpnt_Cis_Cntrct_Print_Dte;
    private DbsField cis_Prtcpnt_Cis_Extract_Date;

    private DbsGroup cis_Prtcpnt_Cis_Mit_Request;
    private DbsField cis_Prtcpnt_Cis_Mit_Rqst_Log_Dte_Tme;
    private DbsField cis_Prtcpnt_Cis_Mit_Rqst_Log_Oprtr_Cde;
    private DbsField cis_Prtcpnt_Cis_Mit_Original_Unit_Cde;
    private DbsField cis_Prtcpnt_Cis_Mit_Work_Process_Id;
    private DbsField cis_Prtcpnt_Cis_Mit_Step_Id;
    private DbsField cis_Prtcpnt_Cis_Mit_Status_Cde;
    private DbsField cis_Prtcpnt_Cis_Appl_Rcvd_Dte;
    private DbsField cis_Prtcpnt_Cis_Appl_Rcvd_User_Id;
    private DbsField cis_Prtcpnt_Cis_Appl_Entry_User_Id;
    private DbsField cis_Prtcpnt_Cis_Annty_Option;
    private DbsField cis_Prtcpnt_Cis_Pymnt_Mode;
    private DbsField cis_Prtcpnt_Cis_Annty_Start_Dte;
    private DbsField cis_Prtcpnt_Cis_Annty_End_Dte;
    private DbsField cis_Prtcpnt_Cis_Grnted_Period_Yrs;
    private DbsField cis_Prtcpnt_Cis_Grnted_Period_Dys;
    private DbsField cis_Prtcpnt_Cis_Scnd_Annt_Prfx;
    private DbsField cis_Prtcpnt_Cis_Scnd_Annt_Frst_Nme;
    private DbsField cis_Prtcpnt_Cis_Scnd_Annt_Mid_Nme;
    private DbsField cis_Prtcpnt_Cis_Scnd_Annt_Lst_Nme;
    private DbsField cis_Prtcpnt_Cis_Scnd_Annt_Sffx;
    private DbsField cis_Prtcpnt_Cis_Scnd_Annt_Ssn;
    private DbsField cis_Prtcpnt_Cis_Scnd_Annt_Dob;
    private DbsField cis_Prtcpnt_Cis_Scnd_Annt_Sex_Cde;
    private DbsField cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Pin_Nbr;
    private DbsField cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Prfx;
    private DbsField cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Frst_Nme;
    private DbsField cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Mid_Nme;
    private DbsField cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Lst_Nme;
    private DbsField cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Sffx;
    private DbsField cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Birth_Dte;
    private DbsField cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Death_Dte;
    private DbsField cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Ssn;
    private DbsField cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Rltn_Cde;
    private DbsField cis_Prtcpnt_Cis_Grnted_Grd_Amt;
    private DbsField cis_Prtcpnt_Cis_Grnted_Std_Amt;
    private DbsField cis_Prtcpnt_Count_Castcis_Tiaa_Commuted_Info;

    private DbsGroup cis_Prtcpnt_Cis_Tiaa_Commuted_Info;
    private DbsField cis_Prtcpnt_Cis_Comut_Grnted_Amt;
    private DbsField cis_Prtcpnt_Cis_Comut_Int_Rate;
    private DbsField cis_Prtcpnt_Cis_Comut_Pymt_Method;
    private DbsField cis_Prtcpnt_Cis_Grnted_Int_Rate;
    private DbsField cis_Prtcpnt_Cis_Surv_Redct_Amt;
    private DbsField cis_Prtcpnt_Count_Castcis_Da_Tiaa_Cntrcts_Rqst;

    private DbsGroup cis_Prtcpnt_Cis_Da_Tiaa_Cntrcts_Rqst;
    private DbsField cis_Prtcpnt_Cis_Da_Tiaa_Nbr;
    private DbsField cis_Prtcpnt_Cis_Da_Tiaa_Proceeds_Amt;
    private DbsField cis_Prtcpnt_Cis_Da_Rea_Proceeds_Amt;

    private DbsGroup cis_Prtcpnt_Cis_Post_Ret_Trnsfer;
    private DbsField cis_Prtcpnt_Cis_Trnsf_Flag;
    private DbsField cis_Prtcpnt_Cis_Trnsf_Acct_Cde;
    private DbsField cis_Prtcpnt_Cis_Trnsf_Pymnt_Mode;
    private DbsField cis_Prtcpnt_Cis_Trnsf_Units;
    private DbsField cis_Prtcpnt_Cis_Trnsf_Cert_Nbr;
    private DbsField cis_Prtcpnt_Cis_Trnsf_From_Company;
    private DbsField cis_Prtcpnt_Cis_Trnsf_To_Company;
    private DbsField cis_Prtcpnt_Cis_Trnsf_Tiaa_Rea_Nbr;
    private DbsField cis_Prtcpnt_Cis_Trnsf_Rea_Units;
    private DbsField cis_Prtcpnt_Count_Castcis_Da_Cref_Cntrcts_Rqst;

    private DbsGroup cis_Prtcpnt_Cis_Da_Cref_Cntrcts_Rqst;
    private DbsField cis_Prtcpnt_Cis_Da_Cert_Nbr;
    private DbsField cis_Prtcpnt_Cis_Da_Cref_Proceeds_Amt;
    private DbsField cis_Prtcpnt_Count_Castcis_Cref_Annty_Pymnt;

    private DbsGroup cis_Prtcpnt_Cis_Cref_Annty_Pymnt;
    private DbsField cis_Prtcpnt_Cis_Cref_Acct_Cde;
    private DbsField cis_Prtcpnt_Cis_Cref_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_Cis_Cref_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_Cis_Cref_Annty_Amt;
    private DbsField cis_Prtcpnt_Cis_Rea_Annty_Amt;
    private DbsField cis_Prtcpnt_Cis_Rea_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_Cis_Rea_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_Cis_Rea_Surv_Nbr_Units;
    private DbsField cis_Prtcpnt_Cis_Surrender_Chg;
    private DbsField cis_Prtcpnt_Cis_Contingencies_Chg;
    private DbsField cis_Prtcpnt_Cis_Mdo_Contract_Cash_Status;
    private DbsField cis_Prtcpnt_Cis_Mdo_Contract_Type;
    private DbsField cis_Prtcpnt_Cis_Mdo_Traditional_Amt;
    private DbsField cis_Prtcpnt_Cis_Mdo_Tiaa_Int_Pymnt_Amt;
    private DbsField cis_Prtcpnt_Cis_Mdo_Cref_Int_Pymnt_Amt;
    private DbsField cis_Prtcpnt_Cis_Mdo_Tiaa_Excluded_Amt;
    private DbsField cis_Prtcpnt_Cis_Mdo_Cref_Excluded_Amt;
    private DbsField cis_Prtcpnt_Cis_Tiaa_Personal_Annuity;
    private DbsField cis_Prtcpnt_Cis_Issue_State_Name;
    private DbsField cis_Prtcpnt_Cis_Issue_State_Cd;
    private DbsField cis_Prtcpnt_Cis_Orig_Issue_State;
    private DbsField cis_Prtcpnt_Cis_Ppg_Code;
    private DbsField cis_Prtcpnt_Cis_Region_Code;
    private DbsField cis_Prtcpnt_Cis_Ownership_Cd;
    private DbsField cis_Prtcpnt_Cis_Tax_Witholding;
    private DbsField cis_Prtcpnt_Cis_Bill_Code;
    private DbsField cis_Prtcpnt_Cis_Lob;
    private DbsField cis_Prtcpnt_Cis_Lob_Type;
    private DbsField cis_Prtcpnt_Cis_Mail_Instructions;
    private DbsField cis_Prtcpnt_Cis_Premium_Paying;
    private DbsField cis_Prtcpnt_Cis_Cashability_Ind;
    private DbsField cis_Prtcpnt_Cis_Transferability_Ind;
    private DbsField cis_Prtcpnt_Cis_Addr_Syn_Ind;
    private DbsField cis_Prtcpnt_Cis_Addr_Process_Env;

    private DbsGroup cis_Prtcpnt_Cis_Address_Info;
    private DbsField cis_Prtcpnt_Cis_Address_Chg_Ind;
    private DbsField cis_Prtcpnt_Cis_Address_Dest_Name;
    private DbsGroup cis_Prtcpnt_Cis_Address_TxtMuGroup;
    private DbsField cis_Prtcpnt_Cis_Address_Txt;
    private DbsField cis_Prtcpnt_Cis_Zip_Code;
    private DbsField cis_Prtcpnt_Cis_Bank_Pymnt_Acct_Nmbr;
    private DbsField cis_Prtcpnt_Cis_Bank_Aba_Acct_Nmbr;
    private DbsField cis_Prtcpnt_Cis_Stndrd_Trn_Cd;
    private DbsField cis_Prtcpnt_Cis_Finalist_Reason_Cd;
    private DbsField cis_Prtcpnt_Cis_Addr_Usage_Code;
    private DbsField cis_Prtcpnt_Cis_Checking_Saving_Cd;
    private DbsField cis_Prtcpnt_Cis_Addr_Stndrd_Code;
    private DbsField cis_Prtcpnt_Cis_Stndrd_Overide;
    private DbsField cis_Prtcpnt_Cis_Postal_Data_Fields;
    private DbsField cis_Prtcpnt_Cis_Geographic_Cd;
    private DbsField cis_Prtcpnt_Cis_Rtb_Pct;
    private DbsField cis_Prtcpnt_Cis_Rtb_Amt;

    private DbsGroup cis_Prtcpnt_Cis_Rtb_Dest_Data;
    private DbsField cis_Prtcpnt_Cis_Rtb_Dest_Pct;
    private DbsField cis_Prtcpnt_Cis_Rtb_Dest_Amt;
    private DbsField cis_Prtcpnt_Cis_Rtb_Dest_Nme;
    private DbsGroup cis_Prtcpnt_Cis_Rtb_Dest_AddrMuGroup;
    private DbsField cis_Prtcpnt_Cis_Rtb_Dest_Addr;
    private DbsField cis_Prtcpnt_Cis_Rtb_Bank_Accnt_Nbr;
    private DbsField cis_Prtcpnt_Cis_Rtb_Trnst_Nbr;
    private DbsField cis_Prtcpnt_Cis_Rtb_Dest_Zip;
    private DbsField cis_Prtcpnt_Cis_Rtb_Dest_Type_Cd;
    private DbsField cis_Prtcpnt_Cis_Rtb_Fed_Tax_Wh_Pct;
    private DbsField cis_Prtcpnt_Cis_Rtb_Fed_Tax_Wh_Desc;
    private DbsField cis_Prtcpnt_Cis_Rtb_St_Tax_Wh_Pct;
    private DbsField cis_Prtcpnt_Cis_Rtb_St_Tax_Wh_Desc;
    private DbsField cis_Prtcpnt_Cis_Rtb_Non_Res_Tax_Wh_Pct;
    private DbsField cis_Prtcpnt_Cis_Rtb_Non_Res_Tax_Wh_Desc;
    private DbsField cis_Prtcpnt_Cis_Pe_Pymt_Fed_Tax_Wh_Pct;
    private DbsField cis_Prtcpnt_Cis_Pe_Pymt_Fed_Tax_Wh_Desc;
    private DbsField cis_Prtcpnt_Cis_Pe_Pymt_St_Tax_Wh_Pct;
    private DbsField cis_Prtcpnt_Cis_Pe_Pymt_St_Tax_Wh_Desc;
    private DbsField cis_Prtcpnt_Cis_Corp_Sync_Ind;

    private DbsGroup cis_Prtcpnt_Cis_Rqst_System_Mit_Dta;
    private DbsField cis_Prtcpnt_Cis_Rqst_Sys_Mit_Sync_Ind;
    private DbsField cis_Prtcpnt_Cis_Rqst_Sys_Mit_Log_Dte_Tme;
    private DbsField cis_Prtcpnt_Cis_Rqst_Sys_Mit_Function;
    private DbsField cis_Prtcpnt_Cis_Rqst_Sys_Mit_Process_Env;
    private DbsField cis_Prtcpnt_Cis_Mit_Error_Cde;
    private DbsField cis_Prtcpnt_Cis_Mit_Error_Msg;
    private DbsField cis_Prtcpnt_Cis_Cor_Sync_Ind;
    private DbsField cis_Prtcpnt_Cis_Cor_Process_Env;
    private DbsField cis_Prtcpnt_Cis_Cor_Ph_Rcd_Typ_Cde;
    private DbsField cis_Prtcpnt_Cis_Cor_Ph_Neg_Election_Cde;
    private DbsField cis_Prtcpnt_Cis_Cor_Ph_Occupnt_Nme;
    private DbsField cis_Prtcpnt_Cis_Cor_Ph_Xref_Pin;
    private DbsField cis_Prtcpnt_Cis_Cor_Ph_Active_Vip_Cnt;
    private DbsField cis_Prtcpnt_Count_Castcis_Cor_Ph_Actve_Vip_Grp;

    private DbsGroup cis_Prtcpnt_Cis_Cor_Ph_Actve_Vip_Grp;
    private DbsField cis_Prtcpnt_Cis_Cor_Ph_Actve_Vip_Cde;
    private DbsField cis_Prtcpnt_Cis_Cor_Ph_Actve_Vip_Area_Cde;
    private DbsField cis_Prtcpnt_Cis_Cor_Mail_Table_Cnt;
    private DbsField cis_Prtcpnt_Count_Castcis_Cor_Ph_Mail_Grp;

    private DbsGroup cis_Prtcpnt_Cis_Cor_Ph_Mail_Grp;
    private DbsField cis_Prtcpnt_Cis_Cor_Phmail_Cde;
    private DbsField cis_Prtcpnt_Cis_Cor_Phmail_Area_Of_Origin;
    private DbsField cis_Prtcpnt_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte;
    private DbsField cis_Prtcpnt_Cis_Cor_Cntrct_Table_Cnt;
    private DbsField cis_Prtcpnt_Cis_Cor_Cntrct_Status_Cde;
    private DbsField cis_Prtcpnt_Cis_Cor_Cntrct_Status_Yr_Dte;
    private DbsField cis_Prtcpnt_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField cis_Prtcpnt_Cis_Cor_Error_Cde;
    private DbsField cis_Prtcpnt_Cis_Cor_Error_Msg;
    private DbsField cis_Prtcpnt_Cis_Pull_Code;
    private DbsField cis_Prtcpnt_Cis_Annual_Required_Dist_Amt;
    private DbsField cis_Prtcpnt_Cis_Required_Begin_Date;
    private DbsField cis_Prtcpnt_Cis_Rea_Nbr;
    private DbsField cis_Prtcpnt_Cis_Rea_Doi;
    private DbsField cis_Prtcpnt_Cis_Cref_Issue_State;
    private DbsField cis_Prtcpnt_Cis_Tiaa_Cntrct_Type;
    private DbsField cis_Prtcpnt_Cis_Rea_Cntrct_Type;
    private DbsField cis_Prtcpnt_Cis_Cref_Cntrct_Type;
    private DbsField cis_Prtcpnt_Count_Castcis_Tiaa_Commuted_Info_2;

    private DbsGroup cis_Prtcpnt_Cis_Tiaa_Commuted_Info_2;
    private DbsField cis_Prtcpnt_Cis_Comut_Grnted_Amt_2;
    private DbsField cis_Prtcpnt_Cis_Comut_Int_Rate_2;
    private DbsField cis_Prtcpnt_Cis_Comut_Pymt_Method_2;
    private DbsField cis_Prtcpnt_Cis_Comut_Mortality_Basis;
    private DbsField cis_Prtcpnt_Cis_Grnted_Period_Mnts;

    private DbsGroup cis_Prtcpnt_Cis_Sg_Fund_Identifier;
    private DbsField cis_Prtcpnt_Cis_Sg_Fund_Ticker;
    private DbsField cis_Prtcpnt_Cis_Sg_Fund_Allocation;
    private DbsField cis_Prtcpnt_Cis_Sg_Fund_Amt;
    private DbsField cis_Prtcpnt_Cis_Sg_Text_Udf_1;
    private DbsField cis_Prtcpnt_Cis_Sg_Text_Udf_2;
    private DbsField cis_Prtcpnt_Cis_Sg_Text_Udf_3;
    private DbsField cis_Prtcpnt_Cis_Tacc_Annty_Amt;

    private DbsGroup cis_Prtcpnt_Cis_Tacc_Fund_Info;
    private DbsField cis_Prtcpnt_Cis_Tacc_Ind;
    private DbsField cis_Prtcpnt_Cis_Tacc_Account;
    private DbsField cis_Prtcpnt_Cis_Tacc_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_Cis_Tacc_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_Cis_Decedent_Contract;

    private DataAccessProgramView vw_cis_Prt_View;

    private DbsGroup cis_Prt_View_R_Reprint;
    private DbsField cis_Prt_View_R_Pin_Num;
    private DbsField cis_Prt_View_R_Sys_Id_Cde;
    private DbsField cis_Prt_View_R_Package_Cde;
    private DbsField cis_Prt_View_R_Rqst_Unit_Cde;
    private DbsField cis_Prt_View_R_Payee_Cde;
    private DbsField cis_Prt_View_R_Req_Id_Key;
    private DbsField cis_Prt_View_R_Cref_Cntrct;
    private DbsField cis_Prt_View_R_Name;
    private DbsField cis_Prt_View_R_Mit_Log_Date_Time;
    private DbsField cis_Prt_View_R_Mit_Wpid;
    private DbsField cis_Prt_View_R_Mit_Status_Code;
    private DbsField cis_Prt_View_R_Element_Max;
    private DbsGroup cis_Prt_View_Count_Castr_Edl_ElementsMuGroup;
    private DbsField cis_Prt_View_Count_Castr_Edl_Elements;
    private DbsGroup cis_Prt_View_R_Edl_ElementsMuGroup;
    private DbsField cis_Prt_View_R_Edl_Elements;
    private DbsGroup cis_Prt_View_Count_Castr_Edl_Elements_2MuGroup;
    private DbsField cis_Prt_View_Count_Castr_Edl_Elements_2;
    private DbsGroup cis_Prt_View_R_Edl_Elements_2MuGroup;
    private DbsField cis_Prt_View_R_Edl_Elements_2;
    private DbsField cis_Prt_View_R_Date;
    private DbsField cis_Prt_View_R_Status_Ind;
    private DbsField cis_Prt_View_R_Tiaa_Cntrct;

    private DataAccessProgramView vw_cis_Bene_File;
    private DbsField cis_Bene_File_Cis_Rcrd_Type_Cde;
    private DbsField cis_Bene_File_Cis_Bene_Tiaa_Nbr;
    private DbsField cis_Bene_File_Cis_Bene_Cref_Nbr;
    private DbsField cis_Bene_File_Cis_Bene_Rqst_Id;
    private DbsField cis_Bene_File_Cis_Bene_Unique_Id_Nbr;
    private DbsField cis_Bene_File_Cis_Bene_Annt_Ssn;
    private DbsField cis_Bene_File_Cis_Bene_Annt_Prfx;
    private DbsField cis_Bene_File_Cis_Bene_Annt_Frst_Nme;
    private DbsField cis_Bene_File_Cis_Bene_Annt_Mid_Nme;
    private DbsField cis_Bene_File_Cis_Bene_Annt_Lst_Nme;
    private DbsField cis_Bene_File_Cis_Bene_Annt_Sffx;
    private DbsField cis_Bene_File_Cis_Bene_Rqst_Id_Key;
    private DbsField cis_Bene_File_Cis_Bene_Std_Free;
    private DbsField cis_Bene_File_Cis_Bene_Estate;
    private DbsField cis_Bene_File_Cis_Bene_Trust;
    private DbsField cis_Bene_File_Cis_Bene_Category;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Child_Prvsn;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Child_Prvsn;

    private DbsGroup cis_Bene_File_Cis_Clcltn_Bnfcry_Dta;
    private DbsField cis_Bene_File_Cis_Clcltn_Bene_Dod;
    private DbsField cis_Bene_File_Cis_Clcltn_Bene_Crossover;
    private DbsField cis_Bene_File_Cis_Clcltn_Bene_Name;
    private DbsField cis_Bene_File_Cis_Clcltn_Bene_Rltnshp_Cde;
    private DbsField cis_Bene_File_Cis_Clcltn_Bene_Ssn_Nbr;
    private DbsField cis_Bene_File_Cis_Clcltn_Bene_Dob;
    private DbsField cis_Bene_File_Cis_Clcltn_Bene_Sex_Cde;
    private DbsField cis_Bene_File_Cis_Clcltn_Bene_Calc_Method;
    private DbsField cis_Bene_File_Count_Castcis_Prmry_Bnfcry_Dta;

    private DbsGroup cis_Bene_File_Cis_Prmry_Bnfcry_Dta;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Std_Txt;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Lump_Sum;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Auto_Cmnt_Val;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Name;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Rltn;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Rltn_Cde;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Ssn_Nbr;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Dob;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Allctn_Pct;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Nmrtr_Nbr;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Dnmntr_Nbr;
    private DbsGroup cis_Bene_File_Cis_Prmry_Bene_Spcl_TxtMuGroup;
    private DbsField cis_Bene_File_Cis_Prmry_Bene_Spcl_Txt;
    private DbsField cis_Bene_File_Count_Castcis_Cntgnt_Bnfcry_Dta;

    private DbsGroup cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Std_Txt;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Lump_Sum;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Auto_Cmnt_Val;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Name;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Rltn;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Rltn_Cde;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Ssn_Nbr;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Dob;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Allctn_Pct;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Nmrtr_Nbr;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Dnmntr_Nbr;
    private DbsGroup cis_Bene_File_Cis_Cntgnt_Bene_Spcl_TxtMuGroup;
    private DbsField cis_Bene_File_Cis_Cntgnt_Bene_Spcl_Txt;

    private DataAccessProgramView vw_nec_Fnd_View1;
    private DbsField nec_Fnd_View1_Nec_Table_Cde;
    private DbsField nec_Fnd_View1_Nec_Ticker_Symbol;
    private DbsField nec_Fnd_View1_Nec_Fund_Family_Cde;
    private DbsField nec_Fnd_View1_Nec_Fund_Nme;
    private DbsField nec_Fnd_View1_Nec_Fund_Inception_Dte;
    private DbsField nec_Fnd_View1_Nec_Fund_Cusip;
    private DbsField nec_Fnd_View1_Nec_Category_Class_Cde;
    private DbsField nec_Fnd_View1_Nec_Category_Rptng_Seq;
    private DbsField nec_Fnd_View1_Nec_Category_Fund_Seq;
    private DbsField nec_Fnd_View1_Nec_Alpha_Fund_Cde;
    private DbsField nec_Fnd_View1_Nec_Num_Fund_Cde;
    private DbsField nec_Fnd_View1_Nec_Reporting_Seq;
    private DbsField nec_Fnd_View1_Nec_Alpha_Fund_Seq;
    private DbsField nec_Fnd_View1_Nec_Abbr_Factor_Key;
    private DbsField nec_Fnd_View1_Nec_Act_Unit_Acct_Nm;
    private DbsField nec_Fnd_View1_Nec_Functional_Cmpny;
    private DbsField nec_Fnd_View1_Nec_Fnd_Super1;
    private DbsField nec_Fnd_View1_Nec_Act_Class_Cde;
    private DbsField nec_Fnd_View1_Nec_Act_Invesment_Typ;
    private DbsField nec_Fnd_View1_Nec_Investment_Grouping_Id;
    private DbsField pnd_Output_File;

    private DbsGroup pnd_Output_File__R_Field_2;

    private DbsGroup pnd_Output_File_Pnd_Output_File_R;
    private DbsField pnd_Output_File_Cis_Rqst_Id;
    private DbsField pnd_Output_File_Pnd_Cis_Print_Ind;
    private DbsField pnd_Output_File_Cis_Tiaa_Nbr;
    private DbsField pnd_Output_File_Cis_Cert_Nbr;
    private DbsField pnd_Output_File_Cis_Orig_Issue_State;
    private DbsField pnd_Output_File_Cis_Cntrct_Apprvl_Ind;
    private DbsField pnd_Output_File_Cis_Pull_Code;
    private DbsField pnd_Output_File_Cis_Four_Fifty_Seven_Ind;
    private DbsField pnd_Output_File_Cis_Institution_Name;
    private DbsField pnd_Output_File_Pnd_Cis_Tiaa_Doi;
    private DbsField pnd_Output_File_Pnd_Cis_Cref_Doi;

    private DbsGroup pnd_Output_File__R_Field_3;
    private DbsField pnd_Output_File_Pnd_Cis_Cref_Doi_N;
    private DbsField pnd_Output_File_Cis_Cntrct_Type;
    private DbsField pnd_Output_File_Cis_Tiaa_Cntrct_Type;
    private DbsField pnd_Output_File_Cis_Cref_Cntrct_Type;
    private DbsField pnd_Output_File_Cis_Rea_Cntrct_Type;
    private DbsField pnd_Output_File_Cis_Lob;
    private DbsField pnd_Output_File_Cis_Lob_Type;
    private DbsField pnd_Output_File_Cis_Ownership_Cd;
    private DbsField pnd_Output_File_Pnd_Cis_Access_Ind;
    private DbsField pnd_Output_File_Cis_Frst_Annt_Ctznshp_Cd;
    private DbsField pnd_Output_File_Pnd_Cis_Atra_Ind;
    private DbsField pnd_Output_File_Pnd_Cis_Atra_Issue_Dte;
    private DbsField pnd_Output_File_Pnd_Cis_Prmry_Bene_Child_Prvsn;
    private DbsField pnd_Output_File_Pnd_Cis_Cntgnt_Bene_Child_Prvsn;
    private DbsField pnd_Output_File_Cis_Pin_Nbr;
    private DbsField pnd_Output_File_Cis_Frst_Annt_Ssn;
    private DbsField pnd_Output_File_Pnd_Cis_Frst_Annt_Dob;
    private DbsField pnd_Output_File_Cis_Issue_State_Cd;
    private DbsField pnd_Output_File_Cis_Annty_Option;
    private DbsField pnd_Output_File_Pnd_Cis_Annty_Start_Dte;
    private DbsField pnd_Output_File_Pnd_Cis_Annty_End_Dte;
    private DbsField pnd_Output_File_Cis_Grnted_Period_Yrs;
    private DbsField pnd_Output_File_Cis_Grnted_Period_Dys;
    private DbsField pnd_Output_File_Cis_Grnted_Grd_Amt;
    private DbsField pnd_Output_File_Cis_Grnted_Std_Amt;
    private DbsField pnd_Output_File_Cis_Pymnt_Mode;

    private DbsGroup pnd_Output_File_Pnd_Cis_Da_Information;
    private DbsField pnd_Output_File_Pnd_Cis_Da_Tiaa_Nbr;
    private DbsField pnd_Output_File_Pnd_Cis_Da_Cert_Nbr;
    private DbsField pnd_Output_File_Pnd_Cis_Da_Rea_Proceeds_Amt;
    private DbsField pnd_Output_File_Pnd_Cis_Da_Tiaa_Proceeds_Amt;
    private DbsField pnd_Output_File_Pnd_Cis_Da_Cref_Proceeds_Amt;

    private DbsGroup pnd_Output_File_Pnd_Cis_Cref_Information;
    private DbsField pnd_Output_File_Pnd_Cis_Sg_Fund_Ticker;
    private DbsField pnd_Output_File_Pnd_Cis_Nec_Fund_Nme;
    private DbsField pnd_Output_File_Pnd_Cis_Cref_Acct_Cde;
    private DbsField pnd_Output_File_Pnd_Cis_Cref_Mnthly_Nbr_Units;
    private DbsField pnd_Output_File_Pnd_Cis_Cref_Annual_Nbr_Units;
    private DbsField pnd_Output_File_Pnd_Cis_Cref_Annty_Amt;
    private DbsField pnd_Output_File_Pnd_Cis_Cref_Mnthly_Amt;
    private DbsField pnd_Output_File_Cis_Rea_Mnthly_Nbr_Units;
    private DbsField pnd_Output_File_Cis_Rea_Annual_Nbr_Units;
    private DbsField pnd_Output_File_Pnd_Cis_Rtb_Amt;
    private DbsField pnd_Output_File_Cis_Rea_Annty_Amt;

    private DbsGroup pnd_Output_File_Pnd_Cis_Tacc_Information;
    private DbsField pnd_Output_File_Pnd_Cis_Tacc_Account;
    private DbsField pnd_Output_File_Pnd_Cis_Tacc_Account_Name;
    private DbsField pnd_Output_File_Pnd_Cis_Tacc_Mnthly_Nbr_Units;
    private DbsField pnd_Output_File_Pnd_Cis_Tacc_Annual_Nbr_Units;
    private DbsField pnd_Output_File_Pnd_Cis_Tacc_Mnthly_Amt;
    private DbsField pnd_Output_File_Pnd_Cis_Tacc_Annty_Amt;

    private DbsGroup pnd_Output_File_Cis_Comut_Information;
    private DbsField pnd_Output_File_Pnd_Cis_Comut_Grnted_Amt;
    private DbsField pnd_Output_File_Pnd_Cis_Comut_Int_Rate;
    private DbsField pnd_Output_File_Pnd_Cis_Comut_Pymt_Method;

    private DbsGroup pnd_Output_File_Cis_Comut_2_Information;
    private DbsField pnd_Output_File_Pnd_Cis_Comut_Grnted_Amt_2;
    private DbsField pnd_Output_File_Pnd_Cis_Comut_Int_Rate_2;
    private DbsField pnd_Output_File_Pnd_Cis_Comut_Pymt_Method_2;
    private DbsField pnd_Output_File_Pnd_Cis_Comut_Mortality_Basis;

    private DbsGroup pnd_Output_File_Pnd_Cis_Bene_Information;
    private DbsField pnd_Output_File_Pnd_Cis_Bene_Type;
    private DbsField pnd_Output_File_Pnd_Cis_Bene_Name;
    private DbsField pnd_Output_File_Pnd_Cis_Bene_Rltn;
    private DbsField pnd_Output_File_Pnd_Cis_Bene_Rltn_Cde;
    private DbsField pnd_Output_File_Pnd_Cis_Bene_Ssn_Nbr;
    private DbsField pnd_Output_File_Pnd_Cis_Bene_Dob;
    private DbsField pnd_Output_File_Pnd_Cis_Bene_Nmrtr_Nbr;
    private DbsField pnd_Output_File_Pnd_Cis_Bene_Dnmntr_Nbr;
    private DbsField pnd_Output_File_Pnd_Cis_Bene_Spcl_Txt;
    private DbsField pnd_Output_File_Pnd_Cis_Scnd_Annt_Full_Name;
    private DbsField pnd_Output_File_Cis_Scnd_Annt_Ssn;
    private DbsField pnd_Output_File_Pnd_Cis_Scnd_Annt_Dob;
    private DbsField pnd_Output_File_Cis_Trnsf_Units;
    private DbsField pnd_Output_File_Cis_Trnsf_Cert_Nbr;
    private DbsField pnd_Output_File_Cis_Trnsf_Tiaa_Rea_Nbr;
    private DbsField pnd_Output_File_Cis_Trnsf_Acct_Cde;
    private DbsField pnd_Output_File_Cis_Trnsf_Flag;
    private DbsField pnd_Output_File_Cis_Trnsf_From_Company;
    private DbsField pnd_Output_File_Cis_Trnsf_To_Company;
    private DbsField pnd_Cis_Ben_Super_2;

    private DbsGroup pnd_Cis_Ben_Super_2__R_Field_4;
    private DbsField pnd_Cis_Ben_Super_2_Pnd_Cis_Rcrd_Type_Cde;
    private DbsField pnd_Cis_Ben_Super_2_Pnd_Cis_Bene_Tiaa_Nbr;
    private DbsField pnd_Output_File2;
    private DbsField pnd_Output_File3;
    private DbsField pnd_Print_Ind;
    private DbsField pnd_Recover_Ind;
    private DbsField pnd_Todays_Business_Dt;

    private DbsGroup pnd_Todays_Business_Dt__R_Field_5;
    private DbsField pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A;
    private DbsField pnd_Todays_Business_Time;
    private DbsField pnd_Print_Date;

    private DbsGroup pnd_Print_Date__R_Field_6;
    private DbsField pnd_Print_Date_Pnd_Print_Date_N;
    private DbsField pnd_Cis_Pipe;
    private DbsField pnd_Cis_Tilde;
    private DbsField pnd_Cis_Caret;
    private DbsField pnd_Nec_Fnd_Super1;

    private DbsGroup pnd_Nec_Fnd_Super1__R_Field_7;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Table_Cde_Super1;
    private DbsField pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Ticker_Symbol_Super1;
    private DbsField pnd_Cis_Da_Occurrence;
    private DbsField pnd_Cis_Cref_Occurrence;
    private DbsField pnd_Cis_Tacc_Occurrence;
    private DbsField pnd_Cis_Comut_Occurrence;
    private DbsField pnd_Cis_Comut_2_Occurrence;
    private DbsField pnd_Cis_Bene_Occurrence;
    private DbsField pnd_Recover;
    private DbsField pnd_Recover_Only;
    private DbsField pnd_Recover_Part;
    private DbsField pnd_Part;
    private DbsField pnd_Reprint;
    private DbsField pnd_Total_Detail;
    private DbsField pnd_Summary;
    private DbsField pnd_Mdm;
    private DbsField pnd_Non_Mdm;
    private DbsField pnd_Mq;
    private DbsField pnd_Mq_Detail;
    private DbsField pnd_Mq_Error;
    private DbsField pnd_Output;
    private DbsField pnd_I2;
    private DbsField pnd_I3;
    private DbsField pnd_Rc;
    private DbsField pnd_First_Name_S;
    private DbsField pnd_Filler_S1;
    private DbsField pnd_Mid_Name_S;
    private DbsField pnd_Filler_S2;
    private DbsField pnd_Last_Name_S;

    private DbsGroup pnd_Cis_Tiaa_Cntrct_Nbr_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data;

    private DbsGroup pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_8;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_State_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_Approval_Ind_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input;
    private DbsField pnd_Tiaa_Date_N;

    private DbsGroup pnd_Tiaa_Date_N__R_Field_9;
    private DbsField pnd_Tiaa_Date_N_Pnd_Tiaa_Date_A;
    private DbsField pnd_Date;
    private DbsField pnd_Save_Cis_Tiaa_Nbr;
    private DbsField pnd_Seconds;
    private DbsField pnd_Ix1;
    private DbsField pnd_Ix2;
    private int cmrollReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCisg0051 = GdaCisg0051.getInstance(getCallnatLevel());
        registerRecord(gdaCisg0051);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaCisa0050 = new PdaCisa0050(localVariables);
        pdaCisa1000 = new PdaCisa1000(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);
        ldaAppl170 = new LdaAppl170();
        registerRecord(ldaAppl170);

        // Local Variables

        pnd_Recover_File = localVariables.newGroupInRecord("pnd_Recover_File", "#RECOVER-FILE");
        pnd_Recover_File_Pnd_Recover_Tiaa = pnd_Recover_File.newFieldInGroup("pnd_Recover_File_Pnd_Recover_Tiaa", "#RECOVER-TIAA", FieldType.STRING, 10);

        pnd_Recover_File__R_Field_1 = pnd_Recover_File.newGroupInGroup("pnd_Recover_File__R_Field_1", "REDEFINE", pnd_Recover_File_Pnd_Recover_Tiaa);
        pnd_Recover_File_Pnd_Recover_Print_Ind = pnd_Recover_File__R_Field_1.newFieldInGroup("pnd_Recover_File_Pnd_Recover_Print_Ind", "#RECOVER-PRINT-IND", 
            FieldType.STRING, 1);
        pnd_Recover_File_Pnd_Recover_Filler = pnd_Recover_File__R_Field_1.newFieldInGroup("pnd_Recover_File_Pnd_Recover_Filler", "#RECOVER-FILLER", FieldType.STRING, 
            9);

        vw_cis_Prtcpnt = new DataAccessProgramView(new NameInfo("vw_cis_Prtcpnt", "CIS-PRTCPNT"), "CIS_PRTCPNT_FILE_12", "CIS_PRTCPNT_FILE", DdmPeriodicGroups.getInstance().getGroups("CIS_PRTCPNT_FILE_12"));
        cis_Prtcpnt_Cis_Pin_Nbr = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CIS_PIN_NBR");
        cis_Prtcpnt_Cis_Rqst_Id_Key = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "CIS_RQST_ID_KEY");
        cis_Prtcpnt_Cis_Tiaa_Nbr = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CIS_TIAA_NBR");
        cis_Prtcpnt_Cis_Cert_Nbr = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CIS_CERT_NBR");
        cis_Prtcpnt_Cis_Tiaa_Doi = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CIS_TIAA_DOI");
        cis_Prtcpnt_Cis_Cref_Doi = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CIS_CREF_DOI");
        cis_Prtcpnt_Cis_Frst_Annt_Ssn = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Frst_Annt_Ssn", "CIS-FRST-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_SSN");
        cis_Prtcpnt_Cis_Frst_Annt_Prfx = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Frst_Annt_Prfx", "CIS-FRST-ANNT-PRFX", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_PRFX");
        cis_Prtcpnt_Cis_Frst_Annt_Frst_Nme = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Frst_Annt_Frst_Nme", "CIS-FRST-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_FRST_NME");
        cis_Prtcpnt_Cis_Frst_Annt_Mid_Nme = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Frst_Annt_Mid_Nme", "CIS-FRST-ANNT-MID-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_MID_NME");
        cis_Prtcpnt_Cis_Frst_Annt_Lst_Nme = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Frst_Annt_Lst_Nme", "CIS-FRST-ANNT-LST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_LST_NME");
        cis_Prtcpnt_Cis_Frst_Annt_Sffx = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Frst_Annt_Sffx", "CIS-FRST-ANNT-SFFX", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_SFFX");
        cis_Prtcpnt_Cis_Frst_Annt_Ctznshp_Cd = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Frst_Annt_Ctznshp_Cd", "CIS-FRST-ANNT-CTZNSHP-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_CTZNSHP_CD");
        cis_Prtcpnt_Cis_Frst_Annt_Rsdnc_Cde = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Frst_Annt_Rsdnc_Cde", "CIS-FRST-ANNT-RSDNC-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_RSDNC_CDE");
        cis_Prtcpnt_Cis_Frst_Annt_Dob = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Frst_Annt_Dob", "CIS-FRST-ANNT-DOB", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_FRST_ANNT_DOB");
        cis_Prtcpnt_Cis_Frst_Annt_Sex_Cde = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Frst_Annt_Sex_Cde", "CIS-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_SEX_CDE");
        cis_Prtcpnt_Cis_Frst_Annt_Calc_Method = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Frst_Annt_Calc_Method", "CIS-FRST-ANNT-CALC-METHOD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_FRST_ANNT_CALC_METHOD");
        cis_Prtcpnt_Cis_Four_Fifty_Seven_Ind = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Four_Fifty_Seven_Ind", "CIS-FOUR-FIFTY-SEVEN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_FOUR_FIFTY_SEVEN_IND");
        cis_Prtcpnt_Cis_Institution_Name = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Institution_Name", "CIS-INSTITUTION-NAME", FieldType.STRING, 
            72, RepeatingFieldStrategy.None, "CIS_INSTITUTION_NAME");
        cis_Prtcpnt_Cis_Opn_Clsd_Ind = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Opn_Clsd_Ind", "CIS-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_OPN_CLSD_IND");
        cis_Prtcpnt_Cis_Status_Cd = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Status_Cd", "CIS-STATUS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_STATUS_CD");
        cis_Prtcpnt_Cis_Cntrct_Type = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CIS_CNTRCT_TYPE");
        cis_Prtcpnt_Cis_Cntrct_Apprvl_Ind = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cntrct_Apprvl_Ind", "CIS-CNTRCT-APPRVL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_CNTRCT_APPRVL_IND");
        cis_Prtcpnt_Cis_Rqst_Id = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CIS_RQST_ID");
        cis_Prtcpnt_Cis_Hold_Cde = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Hold_Cde", "CIS-HOLD-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CIS_HOLD_CDE");
        cis_Prtcpnt_Cis_Cntrct_Print_Dte = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cntrct_Print_Dte", "CIS-CNTRCT-PRINT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_CNTRCT_PRINT_DTE");
        cis_Prtcpnt_Cis_Extract_Date = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Extract_Date", "CIS-EXTRACT-DATE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_EXTRACT_DATE");

        cis_Prtcpnt_Cis_Mit_Request = vw_cis_Prtcpnt.getRecord().newGroupInGroup("CIS_PRTCPNT_CIS_MIT_REQUEST", "CIS-MIT-REQUEST");
        cis_Prtcpnt_Cis_Mit_Rqst_Log_Dte_Tme = cis_Prtcpnt_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_Cis_Mit_Rqst_Log_Dte_Tme", "CIS-MIT-RQST-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CIS_MIT_RQST_LOG_DTE_TME");
        cis_Prtcpnt_Cis_Mit_Rqst_Log_Oprtr_Cde = cis_Prtcpnt_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_Cis_Mit_Rqst_Log_Oprtr_Cde", "CIS-MIT-RQST-LOG-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_MIT_RQST_LOG_OPRTR_CDE");
        cis_Prtcpnt_Cis_Mit_Original_Unit_Cde = cis_Prtcpnt_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_Cis_Mit_Original_Unit_Cde", "CIS-MIT-ORIGINAL-UNIT-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_MIT_ORIGINAL_UNIT_CDE");
        cis_Prtcpnt_Cis_Mit_Work_Process_Id = cis_Prtcpnt_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_Cis_Mit_Work_Process_Id", "CIS-MIT-WORK-PROCESS-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "CIS_MIT_WORK_PROCESS_ID");
        cis_Prtcpnt_Cis_Mit_Step_Id = cis_Prtcpnt_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_Cis_Mit_Step_Id", "CIS-MIT-STEP-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "CIS_MIT_STEP_ID");
        cis_Prtcpnt_Cis_Mit_Status_Cde = cis_Prtcpnt_Cis_Mit_Request.newFieldInGroup("cis_Prtcpnt_Cis_Mit_Status_Cde", "CIS-MIT-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CIS_MIT_STATUS_CDE");
        cis_Prtcpnt_Cis_Appl_Rcvd_Dte = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Appl_Rcvd_Dte", "CIS-APPL-RCVD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_APPL_RCVD_DTE");
        cis_Prtcpnt_Cis_Appl_Rcvd_User_Id = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Appl_Rcvd_User_Id", "CIS-APPL-RCVD-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_APPL_RCVD_USER_ID");
        cis_Prtcpnt_Cis_Appl_Entry_User_Id = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Appl_Entry_User_Id", "CIS-APPL-ENTRY-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_APPL_ENTRY_USER_ID");
        cis_Prtcpnt_Cis_Annty_Option = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_ANNTY_OPTION");
        cis_Prtcpnt_Cis_Pymnt_Mode = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Pymnt_Mode", "CIS-PYMNT-MODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_PYMNT_MODE");
        cis_Prtcpnt_Cis_Annty_Start_Dte = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Annty_Start_Dte", "CIS-ANNTY-START-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_ANNTY_START_DTE");
        cis_Prtcpnt_Cis_Annty_End_Dte = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Annty_End_Dte", "CIS-ANNTY-END-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_ANNTY_END_DTE");
        cis_Prtcpnt_Cis_Grnted_Period_Yrs = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Grnted_Period_Yrs", "CIS-GRNTED-PERIOD-YRS", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CIS_GRNTED_PERIOD_YRS");
        cis_Prtcpnt_Cis_Grnted_Period_Dys = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Grnted_Period_Dys", "CIS-GRNTED-PERIOD-DYS", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CIS_GRNTED_PERIOD_DYS");
        cis_Prtcpnt_Cis_Scnd_Annt_Prfx = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Scnd_Annt_Prfx", "CIS-SCND-ANNT-PRFX", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_PRFX");
        cis_Prtcpnt_Cis_Scnd_Annt_Frst_Nme = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Scnd_Annt_Frst_Nme", "CIS-SCND-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_FRST_NME");
        cis_Prtcpnt_Cis_Scnd_Annt_Mid_Nme = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Scnd_Annt_Mid_Nme", "CIS-SCND-ANNT-MID-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_MID_NME");
        cis_Prtcpnt_Cis_Scnd_Annt_Lst_Nme = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Scnd_Annt_Lst_Nme", "CIS-SCND-ANNT-LST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_LST_NME");
        cis_Prtcpnt_Cis_Scnd_Annt_Sffx = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Scnd_Annt_Sffx", "CIS-SCND-ANNT-SFFX", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SFFX");
        cis_Prtcpnt_Cis_Scnd_Annt_Ssn = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SSN");
        cis_Prtcpnt_Cis_Scnd_Annt_Dob = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Scnd_Annt_Dob", "CIS-SCND-ANNT-DOB", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_SCND_ANNT_DOB");
        cis_Prtcpnt_Cis_Scnd_Annt_Sex_Cde = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Scnd_Annt_Sex_Cde", "CIS-SCND-ANNT-SEX-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SEX_CDE");
        cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Pin_Nbr = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Pin_Nbr", "CIS-ORGNL-PRTCPNT-PIN-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_PIN_NBR");
        cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Prfx = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Prfx", "CIS-ORGNL-PRTCPNT-PRFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_PRFX");
        cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Frst_Nme = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Frst_Nme", "CIS-ORGNL-PRTCPNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_FRST_NME");
        cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Mid_Nme = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Mid_Nme", "CIS-ORGNL-PRTCPNT-MID-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_MID_NME");
        cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Lst_Nme = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Lst_Nme", "CIS-ORGNL-PRTCPNT-LST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_LST_NME");
        cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Sffx = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Sffx", "CIS-ORGNL-PRTCPNT-SFFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_SFFX");
        cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Birth_Dte = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Birth_Dte", "CIS-ORGNL-PRTCPNT-BIRTH-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_BIRTH_DTE");
        cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Death_Dte = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Death_Dte", "CIS-ORGNL-PRTCPNT-DEATH-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_DEATH_DTE");
        cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Ssn = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Ssn", "CIS-ORGNL-PRTCPNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_SSN");
        cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Rltn_Cde = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Orgnl_Prtcpnt_Rltn_Cde", "CIS-ORGNL-PRTCPNT-RLTN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_ORGNL_PRTCPNT_RLTN_CDE");
        cis_Prtcpnt_Cis_Grnted_Grd_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Grnted_Grd_Amt", "CIS-GRNTED-GRD-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_GRD_AMT");
        cis_Prtcpnt_Cis_Grnted_Std_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Grnted_Std_Amt", "CIS-GRNTED-STD-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_STD_AMT");
        cis_Prtcpnt_Count_Castcis_Tiaa_Commuted_Info = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Count_Castcis_Tiaa_Commuted_Info", "C*CIS-TIAA-COMMUTED-INFO", 
            RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");

        cis_Prtcpnt_Cis_Tiaa_Commuted_Info = vw_cis_Prtcpnt.getRecord().newGroupInGroup("cis_Prtcpnt_Cis_Tiaa_Commuted_Info", "CIS-TIAA-COMMUTED-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_Cis_Comut_Grnted_Amt = cis_Prtcpnt_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Comut_Grnted_Amt", "CIS-COMUT-GRNTED-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_GRNTED_AMT", "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_Cis_Comut_Int_Rate = cis_Prtcpnt_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Comut_Int_Rate", "CIS-COMUT-INT-RATE", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_INT_RATE", "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_Cis_Comut_Pymt_Method = cis_Prtcpnt_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Comut_Pymt_Method", "CIS-COMUT-PYMT-METHOD", 
            FieldType.STRING, 8, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_PYMT_METHOD", "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_Cis_Grnted_Int_Rate = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Grnted_Int_Rate", "CIS-GRNTED-INT-RATE", FieldType.NUMERIC, 
            5, 3, RepeatingFieldStrategy.None, "CIS_GRNTED_INT_RATE");
        cis_Prtcpnt_Cis_Surv_Redct_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Surv_Redct_Amt", "CIS-SURV-REDCT-AMT", FieldType.NUMERIC, 
            5, 2, RepeatingFieldStrategy.None, "CIS_SURV_REDCT_AMT");
        cis_Prtcpnt_Count_Castcis_Da_Tiaa_Cntrcts_Rqst = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Count_Castcis_Da_Tiaa_Cntrcts_Rqst", 
            "C*CIS-DA-TIAA-CNTRCTS-RQST", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");

        cis_Prtcpnt_Cis_Da_Tiaa_Cntrcts_Rqst = vw_cis_Prtcpnt.getRecord().newGroupInGroup("cis_Prtcpnt_Cis_Da_Tiaa_Cntrcts_Rqst", "CIS-DA-TIAA-CNTRCTS-RQST", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_Cis_Da_Tiaa_Nbr = cis_Prtcpnt_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_Cis_Da_Tiaa_Nbr", "CIS-DA-TIAA-NBR", FieldType.STRING, 
            10, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_TIAA_NBR", "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_Cis_Da_Tiaa_Proceeds_Amt = cis_Prtcpnt_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_Cis_Da_Tiaa_Proceeds_Amt", "CIS-DA-TIAA-PROCEEDS-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_TIAA_PROCEEDS_AMT", "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_Cis_Da_Rea_Proceeds_Amt = cis_Prtcpnt_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_Cis_Da_Rea_Proceeds_Amt", "CIS-DA-REA-PROCEEDS-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_REA_PROCEEDS_AMT", "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");

        cis_Prtcpnt_Cis_Post_Ret_Trnsfer = vw_cis_Prtcpnt.getRecord().newGroupInGroup("CIS_PRTCPNT_CIS_POST_RET_TRNSFER", "CIS-POST-RET-TRNSFER");
        cis_Prtcpnt_Cis_Trnsf_Flag = cis_Prtcpnt_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_Cis_Trnsf_Flag", "CIS-TRNSF-FLAG", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_TRNSF_FLAG");
        cis_Prtcpnt_Cis_Trnsf_Acct_Cde = cis_Prtcpnt_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_Cis_Trnsf_Acct_Cde", "CIS-TRNSF-ACCT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_TRNSF_ACCT_CDE");
        cis_Prtcpnt_Cis_Trnsf_Pymnt_Mode = cis_Prtcpnt_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_Cis_Trnsf_Pymnt_Mode", "CIS-TRNSF-PYMNT-MODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_TRNSF_PYMNT_MODE");
        cis_Prtcpnt_Cis_Trnsf_Units = cis_Prtcpnt_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_Cis_Trnsf_Units", "CIS-TRNSF-UNITS", FieldType.NUMERIC, 
            8, 3, RepeatingFieldStrategy.None, "CIS_TRNSF_UNITS");
        cis_Prtcpnt_Cis_Trnsf_Cert_Nbr = cis_Prtcpnt_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_Cis_Trnsf_Cert_Nbr", "CIS-TRNSF-CERT-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_TRNSF_CERT_NBR");
        cis_Prtcpnt_Cis_Trnsf_From_Company = cis_Prtcpnt_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_Cis_Trnsf_From_Company", "CIS-TRNSF-FROM-COMPANY", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_TRNSF_FROM_COMPANY");
        cis_Prtcpnt_Cis_Trnsf_To_Company = cis_Prtcpnt_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_Cis_Trnsf_To_Company", "CIS-TRNSF-TO-COMPANY", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_TRNSF_TO_COMPANY");
        cis_Prtcpnt_Cis_Trnsf_Tiaa_Rea_Nbr = cis_Prtcpnt_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_Cis_Trnsf_Tiaa_Rea_Nbr", "CIS-TRNSF-TIAA-REA-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_TRNSF_TIAA_REA_NBR");
        cis_Prtcpnt_Cis_Trnsf_Rea_Units = cis_Prtcpnt_Cis_Post_Ret_Trnsfer.newFieldInGroup("cis_Prtcpnt_Cis_Trnsf_Rea_Units", "CIS-TRNSF-REA-UNITS", FieldType.NUMERIC, 
            8, 3, RepeatingFieldStrategy.None, "CIS_TRNSF_REA_UNITS");
        cis_Prtcpnt_Count_Castcis_Da_Cref_Cntrcts_Rqst = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Count_Castcis_Da_Cref_Cntrcts_Rqst", 
            "C*CIS-DA-CREF-CNTRCTS-RQST", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");

        cis_Prtcpnt_Cis_Da_Cref_Cntrcts_Rqst = vw_cis_Prtcpnt.getRecord().newGroupInGroup("cis_Prtcpnt_Cis_Da_Cref_Cntrcts_Rqst", "CIS-DA-CREF-CNTRCTS-RQST", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_Cis_Da_Cert_Nbr = cis_Prtcpnt_Cis_Da_Cref_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_Cis_Da_Cert_Nbr", "CIS-DA-CERT-NBR", FieldType.STRING, 
            10, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_CERT_NBR", "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_Cis_Da_Cref_Proceeds_Amt = cis_Prtcpnt_Cis_Da_Cref_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_Cis_Da_Cref_Proceeds_Amt", "CIS-DA-CREF-PROCEEDS-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_CREF_PROCEEDS_AMT", "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_Count_Castcis_Cref_Annty_Pymnt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Count_Castcis_Cref_Annty_Pymnt", "C*CIS-CREF-ANNTY-PYMNT", 
            RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");

        cis_Prtcpnt_Cis_Cref_Annty_Pymnt = vw_cis_Prtcpnt.getRecord().newGroupInGroup("cis_Prtcpnt_Cis_Cref_Annty_Pymnt", "CIS-CREF-ANNTY-PYMNT", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_Cis_Cref_Acct_Cde = cis_Prtcpnt_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_Cis_Cref_Acct_Cde", "CIS-CREF-ACCT-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_ACCT_CDE", "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_Cis_Cref_Mnthly_Nbr_Units = cis_Prtcpnt_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_Cis_Cref_Mnthly_Nbr_Units", "CIS-CREF-MNTHLY-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_MNTHLY_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_Cis_Cref_Annual_Nbr_Units = cis_Prtcpnt_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_Cis_Cref_Annual_Nbr_Units", "CIS-CREF-ANNUAL-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_ANNUAL_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_Cis_Cref_Annty_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cref_Annty_Amt", "CIS-CREF-ANNTY-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_CREF_ANNTY_AMT");
        cis_Prtcpnt_Cis_Rea_Annty_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rea_Annty_Amt", "CIS-REA-ANNTY-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_REA_ANNTY_AMT");
        cis_Prtcpnt_Cis_Rea_Mnthly_Nbr_Units = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rea_Mnthly_Nbr_Units", "CIS-REA-MNTHLY-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_MNTHLY_NBR_UNITS");
        cis_Prtcpnt_Cis_Rea_Annual_Nbr_Units = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rea_Annual_Nbr_Units", "CIS-REA-ANNUAL-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_ANNUAL_NBR_UNITS");
        cis_Prtcpnt_Cis_Rea_Surv_Nbr_Units = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rea_Surv_Nbr_Units", "CIS-REA-SURV-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_SURV_NBR_UNITS");
        cis_Prtcpnt_Cis_Surrender_Chg = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Surrender_Chg", "CIS-SURRENDER-CHG", FieldType.NUMERIC, 
            4, 2, RepeatingFieldStrategy.None, "CIS_SURRENDER_CHG");
        cis_Prtcpnt_Cis_Contingencies_Chg = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Contingencies_Chg", "CIS-CONTINGENCIES-CHG", FieldType.NUMERIC, 
            4, 2, RepeatingFieldStrategy.None, "CIS_CONTINGENCIES_CHG");
        cis_Prtcpnt_Cis_Mdo_Contract_Cash_Status = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Mdo_Contract_Cash_Status", "CIS-MDO-CONTRACT-CASH-STATUS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_MDO_CONTRACT_CASH_STATUS");
        cis_Prtcpnt_Cis_Mdo_Contract_Type = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Mdo_Contract_Type", "CIS-MDO-CONTRACT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_MDO_CONTRACT_TYPE");
        cis_Prtcpnt_Cis_Mdo_Traditional_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Mdo_Traditional_Amt", "CIS-MDO-TRADITIONAL-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TRADITIONAL_AMT");
        cis_Prtcpnt_Cis_Mdo_Tiaa_Int_Pymnt_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Mdo_Tiaa_Int_Pymnt_Amt", "CIS-MDO-TIAA-INT-PYMNT-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TIAA_INT_PYMNT_AMT");
        cis_Prtcpnt_Cis_Mdo_Cref_Int_Pymnt_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Mdo_Cref_Int_Pymnt_Amt", "CIS-MDO-CREF-INT-PYMNT-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_CREF_INT_PYMNT_AMT");
        cis_Prtcpnt_Cis_Mdo_Tiaa_Excluded_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Mdo_Tiaa_Excluded_Amt", "CIS-MDO-TIAA-EXCLUDED-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TIAA_EXCLUDED_AMT");
        cis_Prtcpnt_Cis_Mdo_Cref_Excluded_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Mdo_Cref_Excluded_Amt", "CIS-MDO-CREF-EXCLUDED-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_CREF_EXCLUDED_AMT");
        cis_Prtcpnt_Cis_Tiaa_Personal_Annuity = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Tiaa_Personal_Annuity", "CIS-TIAA-PERSONAL-ANNUITY", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_TIAA_PERSONAL_ANNUITY");
        cis_Prtcpnt_Cis_Issue_State_Name = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Issue_State_Name", "CIS-ISSUE-STATE-NAME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "CIS_ISSUE_STATE_NAME");
        cis_Prtcpnt_Cis_Issue_State_Cd = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Issue_State_Cd", "CIS-ISSUE-STATE-CD", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CIS_ISSUE_STATE_CD");
        cis_Prtcpnt_Cis_Orig_Issue_State = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Orig_Issue_State", "CIS-ORIG-ISSUE-STATE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CIS_ORIG_ISSUE_STATE");
        cis_Prtcpnt_Cis_Ppg_Code = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Ppg_Code", "CIS-PPG-CODE", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "CIS_PPG_CODE");
        cis_Prtcpnt_Cis_Region_Code = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Region_Code", "CIS-REGION-CODE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "CIS_REGION_CODE");
        cis_Prtcpnt_Cis_Ownership_Cd = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Ownership_Cd", "CIS-OWNERSHIP-CD", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CIS_OWNERSHIP_CD");
        cis_Prtcpnt_Cis_Tax_Witholding = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Tax_Witholding", "CIS-TAX-WITHOLDING", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CIS_TAX_WITHOLDING");
        cis_Prtcpnt_Cis_Bill_Code = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Bill_Code", "CIS-BILL-CODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_BILL_CODE");
        cis_Prtcpnt_Cis_Lob = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Lob", "CIS-LOB", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_LOB");
        cis_Prtcpnt_Cis_Lob_Type = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Lob_Type", "CIS-LOB-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_LOB_TYPE");
        cis_Prtcpnt_Cis_Mail_Instructions = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Mail_Instructions", "CIS-MAIL-INSTRUCTIONS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_MAIL_INSTRUCTIONS");
        cis_Prtcpnt_Cis_Premium_Paying = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Premium_Paying", "CIS-PREMIUM-PAYING", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_PREMIUM_PAYING");
        cis_Prtcpnt_Cis_Cashability_Ind = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cashability_Ind", "CIS-CASHABILITY-IND", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CIS_CASHABILITY_IND");
        cis_Prtcpnt_Cis_Transferability_Ind = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Transferability_Ind", "CIS-TRANSFERABILITY-IND", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_TRANSFERABILITY_IND");
        cis_Prtcpnt_Cis_Addr_Syn_Ind = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Addr_Syn_Ind", "CIS-ADDR-SYN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_ADDR_SYN_IND");
        cis_Prtcpnt_Cis_Addr_Process_Env = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Addr_Process_Env", "CIS-ADDR-PROCESS-ENV", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_ADDR_PROCESS_ENV");

        cis_Prtcpnt_Cis_Address_Info = vw_cis_Prtcpnt.getRecord().newGroupInGroup("cis_Prtcpnt_Cis_Address_Info", "CIS-ADDRESS-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Address_Chg_Ind = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Address_Chg_Ind", "CIS-ADDRESS-CHG-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDRESS_CHG_IND", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Address_Dest_Name = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Address_Dest_Name", "CIS-ADDRESS-DEST-NAME", 
            FieldType.STRING, 35, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDRESS_DEST_NAME", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Address_TxtMuGroup = cis_Prtcpnt_Cis_Address_Info.newGroupInGroup("CIS_PRTCPNT_CIS_ADDRESS_TXTMuGroup", "CIS_ADDRESS_TXTMuGroup", 
            RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_PRTCPNT_FILE_CIS_ADDRESS_TXT", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Address_Txt = cis_Prtcpnt_Cis_Address_TxtMuGroup.newFieldArrayInGroup("cis_Prtcpnt_Cis_Address_Txt", "CIS-ADDRESS-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 3, 1, 5) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_ADDRESS_TXT", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Zip_Code = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Zip_Code", "CIS-ZIP-CODE", FieldType.STRING, 9, 
            new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ZIP_CODE", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Bank_Pymnt_Acct_Nmbr = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Bank_Pymnt_Acct_Nmbr", "CIS-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BANK_PYMNT_ACCT_NMBR", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Bank_Aba_Acct_Nmbr = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Bank_Aba_Acct_Nmbr", "CIS-BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BANK_ABA_ACCT_NMBR", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Stndrd_Trn_Cd = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Stndrd_Trn_Cd", "CIS-STNDRD-TRN-CD", FieldType.STRING, 
            2, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_STNDRD_TRN_CD", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Finalist_Reason_Cd = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Finalist_Reason_Cd", "CIS-FINALIST-REASON-CD", 
            FieldType.STRING, 10, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_FINALIST_REASON_CD", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Addr_Usage_Code = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Addr_Usage_Code", "CIS-ADDR-USAGE-CODE", 
            FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDR_USAGE_CODE", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Checking_Saving_Cd = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Checking_Saving_Cd", "CIS-CHECKING-SAVING-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CHECKING_SAVING_CD", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Addr_Stndrd_Code = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Addr_Stndrd_Code", "CIS-ADDR-STNDRD-CODE", 
            FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDR_STNDRD_CODE", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Stndrd_Overide = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Stndrd_Overide", "CIS-STNDRD-OVERIDE", FieldType.STRING, 
            1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_STNDRD_OVERIDE", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Postal_Data_Fields = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Postal_Data_Fields", "CIS-POSTAL-DATA-FIELDS", 
            FieldType.STRING, 44, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_POSTAL_DATA_FIELDS", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Geographic_Cd = cis_Prtcpnt_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Geographic_Cd", "CIS-GEOGRAPHIC-CD", FieldType.STRING, 
            2, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_GEOGRAPHIC_CD", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_Cis_Rtb_Pct = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rtb_Pct", "CIS-RTB-PCT", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CIS_RTB_PCT");
        cis_Prtcpnt_Cis_Rtb_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rtb_Amt", "CIS-RTB-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "CIS_RTB_AMT");

        cis_Prtcpnt_Cis_Rtb_Dest_Data = vw_cis_Prtcpnt.getRecord().newGroupInGroup("cis_Prtcpnt_Cis_Rtb_Dest_Data", "CIS-RTB-DEST-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_Cis_Rtb_Dest_Pct = cis_Prtcpnt_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_Cis_Rtb_Dest_Pct", "CIS-RTB-DEST-PCT", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_PCT", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_Cis_Rtb_Dest_Amt = cis_Prtcpnt_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_Cis_Rtb_Dest_Amt", "CIS-RTB-DEST-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_AMT", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_Cis_Rtb_Dest_Nme = cis_Prtcpnt_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_Cis_Rtb_Dest_Nme", "CIS-RTB-DEST-NME", FieldType.STRING, 
            35, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_NME", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_Cis_Rtb_Dest_AddrMuGroup = cis_Prtcpnt_Cis_Rtb_Dest_Data.newGroupInGroup("CIS_PRTCPNT_CIS_RTB_DEST_ADDRMuGroup", "CIS_RTB_DEST_ADDRMuGroup", 
            RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_PRTCPNT_FILE_CIS_RTB_DEST_ADDR", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_Cis_Rtb_Dest_Addr = cis_Prtcpnt_Cis_Rtb_Dest_AddrMuGroup.newFieldArrayInGroup("cis_Prtcpnt_Cis_Rtb_Dest_Addr", "CIS-RTB-DEST-ADDR", 
            FieldType.STRING, 35, new DbsArrayController(1, 3, 1, 5) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_RTB_DEST_ADDR", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_Cis_Rtb_Bank_Accnt_Nbr = cis_Prtcpnt_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_Cis_Rtb_Bank_Accnt_Nbr", "CIS-RTB-BANK-ACCNT-NBR", 
            FieldType.STRING, 21, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_BANK_ACCNT_NBR", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_Cis_Rtb_Trnst_Nbr = cis_Prtcpnt_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_Cis_Rtb_Trnst_Nbr", "CIS-RTB-TRNST-NBR", FieldType.NUMERIC, 
            9, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_TRNST_NBR", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_Cis_Rtb_Dest_Zip = cis_Prtcpnt_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_Cis_Rtb_Dest_Zip", "CIS-RTB-DEST-ZIP", FieldType.STRING, 
            9, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_ZIP", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_Cis_Rtb_Dest_Type_Cd = cis_Prtcpnt_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_Cis_Rtb_Dest_Type_Cd", "CIS-RTB-DEST-TYPE-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_TYPE_CD", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_Cis_Rtb_Fed_Tax_Wh_Pct = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rtb_Fed_Tax_Wh_Pct", "CIS-RTB-FED-TAX-WH-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_RTB_FED_TAX_WH_PCT");
        cis_Prtcpnt_Cis_Rtb_Fed_Tax_Wh_Desc = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rtb_Fed_Tax_Wh_Desc", "CIS-RTB-FED-TAX-WH-DESC", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "CIS_RTB_FED_TAX_WH_DESC");
        cis_Prtcpnt_Cis_Rtb_St_Tax_Wh_Pct = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rtb_St_Tax_Wh_Pct", "CIS-RTB-ST-TAX-WH-PCT", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CIS_RTB_ST_TAX_WH_PCT");
        cis_Prtcpnt_Cis_Rtb_St_Tax_Wh_Desc = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rtb_St_Tax_Wh_Desc", "CIS-RTB-ST-TAX-WH-DESC", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "CIS_RTB_ST_TAX_WH_DESC");
        cis_Prtcpnt_Cis_Rtb_Non_Res_Tax_Wh_Pct = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rtb_Non_Res_Tax_Wh_Pct", "CIS-RTB-NON-RES-TAX-WH-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_RTB_NON_RES_TAX_WH_PCT");
        cis_Prtcpnt_Cis_Rtb_Non_Res_Tax_Wh_Desc = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rtb_Non_Res_Tax_Wh_Desc", "CIS-RTB-NON-RES-TAX-WH-DESC", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "CIS_RTB_NON_RES_TAX_WH_DESC");
        cis_Prtcpnt_Cis_Pe_Pymt_Fed_Tax_Wh_Pct = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Pe_Pymt_Fed_Tax_Wh_Pct", "CIS-PE-PYMT-FED-TAX-WH-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_PE_PYMT_FED_TAX_WH_PCT");
        cis_Prtcpnt_Cis_Pe_Pymt_Fed_Tax_Wh_Desc = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Pe_Pymt_Fed_Tax_Wh_Desc", "CIS-PE-PYMT-FED-TAX-WH-DESC", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "CIS_PE_PYMT_FED_TAX_WH_DESC");
        cis_Prtcpnt_Cis_Pe_Pymt_St_Tax_Wh_Pct = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Pe_Pymt_St_Tax_Wh_Pct", "CIS-PE-PYMT-ST-TAX-WH-PCT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_PE_PYMT_ST_TAX_WH_PCT");
        cis_Prtcpnt_Cis_Pe_Pymt_St_Tax_Wh_Desc = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Pe_Pymt_St_Tax_Wh_Desc", "CIS-PE-PYMT-ST-TAX-WH-DESC", 
            FieldType.STRING, 50, RepeatingFieldStrategy.None, "CIS_PE_PYMT_ST_TAX_WH_DESC");
        cis_Prtcpnt_Cis_Corp_Sync_Ind = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Corp_Sync_Ind", "CIS-CORP-SYNC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_CORP_SYNC_IND");

        cis_Prtcpnt_Cis_Rqst_System_Mit_Dta = vw_cis_Prtcpnt.getRecord().newGroupInGroup("CIS_PRTCPNT_CIS_RQST_SYSTEM_MIT_DTA", "CIS-RQST-SYSTEM-MIT-DTA");
        cis_Prtcpnt_Cis_Rqst_Sys_Mit_Sync_Ind = cis_Prtcpnt_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_Cis_Rqst_Sys_Mit_Sync_Ind", "CIS-RQST-SYS-MIT-SYNC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_RQST_SYS_MIT_SYNC_IND");
        cis_Prtcpnt_Cis_Rqst_Sys_Mit_Log_Dte_Tme = cis_Prtcpnt_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_Cis_Rqst_Sys_Mit_Log_Dte_Tme", "CIS-RQST-SYS-MIT-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "CIS_RQST_SYS_MIT_LOG_DTE_TME");
        cis_Prtcpnt_Cis_Rqst_Sys_Mit_Function = cis_Prtcpnt_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_Cis_Rqst_Sys_Mit_Function", "CIS-RQST-SYS-MIT-FUNCTION", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_RQST_SYS_MIT_FUNCTION");
        cis_Prtcpnt_Cis_Rqst_Sys_Mit_Process_Env = cis_Prtcpnt_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_Cis_Rqst_Sys_Mit_Process_Env", "CIS-RQST-SYS-MIT-PROCESS-ENV", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_RQST_SYS_MIT_PROCESS_ENV");
        cis_Prtcpnt_Cis_Mit_Error_Cde = cis_Prtcpnt_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_Cis_Mit_Error_Cde", "CIS-MIT-ERROR-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CIS_MIT_ERROR_CDE");
        cis_Prtcpnt_Cis_Mit_Error_Msg = cis_Prtcpnt_Cis_Rqst_System_Mit_Dta.newFieldInGroup("cis_Prtcpnt_Cis_Mit_Error_Msg", "CIS-MIT-ERROR-MSG", FieldType.STRING, 
            72, RepeatingFieldStrategy.None, "CIS_MIT_ERROR_MSG");
        cis_Prtcpnt_Cis_Cor_Sync_Ind = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Sync_Ind", "CIS-COR-SYNC-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_COR_SYNC_IND");
        cis_Prtcpnt_Cis_Cor_Process_Env = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Process_Env", "CIS-COR-PROCESS-ENV", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_COR_PROCESS_ENV");
        cis_Prtcpnt_Cis_Cor_Ph_Rcd_Typ_Cde = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Ph_Rcd_Typ_Cde", "CIS-COR-PH-RCD-TYP-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_COR_PH_RCD_TYP_CDE");
        cis_Prtcpnt_Cis_Cor_Ph_Neg_Election_Cde = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Ph_Neg_Election_Cde", "CIS-COR-PH-NEG-ELECTION-CDE", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CIS_COR_PH_NEG_ELECTION_CDE");
        cis_Prtcpnt_Cis_Cor_Ph_Occupnt_Nme = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Ph_Occupnt_Nme", "CIS-COR-PH-OCCUPNT-NME", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_COR_PH_OCCUPNT_NME");
        cis_Prtcpnt_Cis_Cor_Ph_Xref_Pin = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Ph_Xref_Pin", "CIS-COR-PH-XREF-PIN", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CIS_COR_PH_XREF_PIN");
        cis_Prtcpnt_Cis_Cor_Ph_Active_Vip_Cnt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Ph_Active_Vip_Cnt", "CIS-COR-PH-ACTIVE-VIP-CNT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_COR_PH_ACTIVE_VIP_CNT");
        cis_Prtcpnt_Count_Castcis_Cor_Ph_Actve_Vip_Grp = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Count_Castcis_Cor_Ph_Actve_Vip_Grp", 
            "C*CIS-COR-PH-ACTVE-VIP-GRP", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_COR_PH_ACTVE_VIP_GRP");

        cis_Prtcpnt_Cis_Cor_Ph_Actve_Vip_Grp = vw_cis_Prtcpnt.getRecord().newGroupInGroup("cis_Prtcpnt_Cis_Cor_Ph_Actve_Vip_Grp", "CIS-COR-PH-ACTVE-VIP-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_COR_PH_ACTVE_VIP_GRP");
        cis_Prtcpnt_Cis_Cor_Ph_Actve_Vip_Cde = cis_Prtcpnt_Cis_Cor_Ph_Actve_Vip_Grp.newFieldArrayInGroup("cis_Prtcpnt_Cis_Cor_Ph_Actve_Vip_Cde", "CIS-COR-PH-ACTVE-VIP-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 8) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COR_PH_ACTVE_VIP_CDE", "CIS_PRTCPNT_FILE_CIS_COR_PH_ACTVE_VIP_GRP");
        cis_Prtcpnt_Cis_Cor_Ph_Actve_Vip_Area_Cde = cis_Prtcpnt_Cis_Cor_Ph_Actve_Vip_Grp.newFieldArrayInGroup("cis_Prtcpnt_Cis_Cor_Ph_Actve_Vip_Area_Cde", 
            "CIS-COR-PH-ACTVE-VIP-AREA-CDE", FieldType.STRING, 3, new DbsArrayController(1, 8) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COR_PH_ACTVE_VIP_AREA_CDE", 
            "CIS_PRTCPNT_FILE_CIS_COR_PH_ACTVE_VIP_GRP");
        cis_Prtcpnt_Cis_Cor_Mail_Table_Cnt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Mail_Table_Cnt", "CIS-COR-MAIL-TABLE-CNT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_COR_MAIL_TABLE_CNT");
        cis_Prtcpnt_Count_Castcis_Cor_Ph_Mail_Grp = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Count_Castcis_Cor_Ph_Mail_Grp", "C*CIS-COR-PH-MAIL-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_COR_PH_MAIL_GRP");

        cis_Prtcpnt_Cis_Cor_Ph_Mail_Grp = vw_cis_Prtcpnt.getRecord().newGroupInGroup("cis_Prtcpnt_Cis_Cor_Ph_Mail_Grp", "CIS-COR-PH-MAIL-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_PRTCPNT_FILE_CIS_COR_PH_MAIL_GRP");
        cis_Prtcpnt_Cis_Cor_Phmail_Cde = cis_Prtcpnt_Cis_Cor_Ph_Mail_Grp.newFieldArrayInGroup("cis_Prtcpnt_Cis_Cor_Phmail_Cde", "CIS-COR-PHMAIL-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 25) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COR_PHMAIL_CDE", "CIS_PRTCPNT_FILE_CIS_COR_PH_MAIL_GRP");
        cis_Prtcpnt_Cis_Cor_Phmail_Area_Of_Origin = cis_Prtcpnt_Cis_Cor_Ph_Mail_Grp.newFieldArrayInGroup("cis_Prtcpnt_Cis_Cor_Phmail_Area_Of_Origin", 
            "CIS-COR-PHMAIL-AREA-OF-ORIGIN", FieldType.STRING, 3, new DbsArrayController(1, 25) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COR_PHMAIL_AREA_OF_ORIGIN", 
            "CIS_PRTCPNT_FILE_CIS_COR_PH_MAIL_GRP");
        cis_Prtcpnt_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte", "CIS-COR-INST-PH-RMTTNG-INSTN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_COR_INST_PH_RMTTNG_INSTN_DTE");
        cis_Prtcpnt_Cis_Cor_Cntrct_Table_Cnt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Cntrct_Table_Cnt", "CIS-COR-CNTRCT-TABLE-CNT", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_COR_CNTRCT_TABLE_CNT");
        cis_Prtcpnt_Cis_Cor_Cntrct_Status_Cde = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Cntrct_Status_Cde", "CIS-COR-CNTRCT-STATUS-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_COR_CNTRCT_STATUS_CDE");
        cis_Prtcpnt_Cis_Cor_Cntrct_Status_Yr_Dte = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Cntrct_Status_Yr_Dte", "CIS-COR-CNTRCT-STATUS-YR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CIS_COR_CNTRCT_STATUS_YR_DTE");
        cis_Prtcpnt_Cis_Cor_Cntrct_Payee_Cde = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_COR_CNTRCT_PAYEE_CDE");
        cis_Prtcpnt_Cis_Cor_Error_Cde = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Error_Cde", "CIS-COR-ERROR-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CIS_COR_ERROR_CDE");
        cis_Prtcpnt_Cis_Cor_Error_Msg = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cor_Error_Msg", "CIS-COR-ERROR-MSG", FieldType.STRING, 
            72, RepeatingFieldStrategy.None, "CIS_COR_ERROR_MSG");
        cis_Prtcpnt_Cis_Pull_Code = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Pull_Code", "CIS-PULL-CODE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CIS_PULL_CODE");
        cis_Prtcpnt_Cis_Annual_Required_Dist_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Annual_Required_Dist_Amt", "CIS-ANNUAL-REQUIRED-DIST-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_ANNUAL_REQUIRED_DIST_AMT");
        cis_Prtcpnt_Cis_Required_Begin_Date = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Required_Begin_Date", "CIS-REQUIRED-BEGIN-DATE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_REQUIRED_BEGIN_DATE");
        cis_Prtcpnt_Cis_Rea_Nbr = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rea_Nbr", "CIS-REA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CIS_REA_NBR");
        cis_Prtcpnt_Cis_Rea_Doi = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rea_Doi", "CIS-REA-DOI", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CIS_REA_DOI");
        cis_Prtcpnt_Cis_Cref_Issue_State = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cref_Issue_State", "CIS-CREF-ISSUE-STATE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CIS_CREF_ISSUE_STATE");
        cis_Prtcpnt_Cis_Tiaa_Cntrct_Type = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Tiaa_Cntrct_Type", "CIS-TIAA-CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_TIAA_CNTRCT_TYPE");
        cis_Prtcpnt_Cis_Rea_Cntrct_Type = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Rea_Cntrct_Type", "CIS-REA-CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_REA_CNTRCT_TYPE");
        cis_Prtcpnt_Cis_Cref_Cntrct_Type = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Cref_Cntrct_Type", "CIS-CREF-CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_CREF_CNTRCT_TYPE");
        cis_Prtcpnt_Count_Castcis_Tiaa_Commuted_Info_2 = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Count_Castcis_Tiaa_Commuted_Info_2", 
            "C*CIS-TIAA-COMMUTED-INFO-2", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");

        cis_Prtcpnt_Cis_Tiaa_Commuted_Info_2 = vw_cis_Prtcpnt.getRecord().newGroupInGroup("cis_Prtcpnt_Cis_Tiaa_Commuted_Info_2", "CIS-TIAA-COMMUTED-INFO-2", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_Cis_Comut_Grnted_Amt_2 = cis_Prtcpnt_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_Cis_Comut_Grnted_Amt_2", "CIS-COMUT-GRNTED-AMT-2", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_GRNTED_AMT_2", "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_Cis_Comut_Int_Rate_2 = cis_Prtcpnt_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_Cis_Comut_Int_Rate_2", "CIS-COMUT-INT-RATE-2", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_INT_RATE_2", "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_Cis_Comut_Pymt_Method_2 = cis_Prtcpnt_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_Cis_Comut_Pymt_Method_2", "CIS-COMUT-PYMT-METHOD-2", 
            FieldType.STRING, 8, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_PYMT_METHOD_2", "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_Cis_Comut_Mortality_Basis = cis_Prtcpnt_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_Cis_Comut_Mortality_Basis", "CIS-COMUT-MORTALITY-BASIS", 
            FieldType.STRING, 30, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_MORTALITY_BASIS", "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_Cis_Grnted_Period_Mnts = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Grnted_Period_Mnts", "CIS-GRNTED-PERIOD-MNTS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_PERIOD_MNTS");

        cis_Prtcpnt_Cis_Sg_Fund_Identifier = vw_cis_Prtcpnt.getRecord().newGroupInGroup("cis_Prtcpnt_Cis_Sg_Fund_Identifier", "CIS-SG-FUND-IDENTIFIER", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Prtcpnt_Cis_Sg_Fund_Ticker = cis_Prtcpnt_Cis_Sg_Fund_Identifier.newFieldArrayInGroup("cis_Prtcpnt_Cis_Sg_Fund_Ticker", "CIS-SG-FUND-TICKER", 
            FieldType.STRING, 10, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_SG_FUND_TICKER", "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Prtcpnt_Cis_Sg_Fund_Allocation = cis_Prtcpnt_Cis_Sg_Fund_Identifier.newFieldArrayInGroup("cis_Prtcpnt_Cis_Sg_Fund_Allocation", "CIS-SG-FUND-ALLOCATION", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_SG_FUND_ALLOCATION", "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Prtcpnt_Cis_Sg_Fund_Amt = cis_Prtcpnt_Cis_Sg_Fund_Identifier.newFieldArrayInGroup("cis_Prtcpnt_Cis_Sg_Fund_Amt", "CIS-SG-FUND-AMT", FieldType.NUMERIC, 
            13, 4, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_SG_FUND_AMT", "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Prtcpnt_Cis_Sg_Text_Udf_1 = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Sg_Text_Udf_1", "CIS-SG-TEXT-UDF-1", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_SG_TEXT_UDF_1");
        cis_Prtcpnt_Cis_Sg_Text_Udf_2 = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Sg_Text_Udf_2", "CIS-SG-TEXT-UDF-2", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_SG_TEXT_UDF_2");
        cis_Prtcpnt_Cis_Sg_Text_Udf_3 = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_SG_TEXT_UDF_3");
        cis_Prtcpnt_Cis_Tacc_Annty_Amt = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Tacc_Annty_Amt", "CIS-TACC-ANNTY-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_TACC_ANNTY_AMT");

        cis_Prtcpnt_Cis_Tacc_Fund_Info = vw_cis_Prtcpnt.getRecord().newGroupInGroup("cis_Prtcpnt_Cis_Tacc_Fund_Info", "CIS-TACC-FUND-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_Cis_Tacc_Ind = cis_Prtcpnt_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Tacc_Ind", "CIS-TACC-IND", FieldType.STRING, 4, 
            new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_IND", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_Cis_Tacc_Account = cis_Prtcpnt_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Tacc_Account", "CIS-TACC-ACCOUNT", FieldType.STRING, 
            10, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_ACCOUNT", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_Cis_Tacc_Mnthly_Nbr_Units = cis_Prtcpnt_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Tacc_Mnthly_Nbr_Units", "CIS-TACC-MNTHLY-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_MNTHLY_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_Cis_Tacc_Annual_Nbr_Units = cis_Prtcpnt_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_Cis_Tacc_Annual_Nbr_Units", "CIS-TACC-ANNUAL-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_ANNUAL_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_Cis_Decedent_Contract = vw_cis_Prtcpnt.getRecord().newFieldInGroup("cis_Prtcpnt_Cis_Decedent_Contract", "CIS-DECEDENT-CONTRACT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_DECEDENT_CONTRACT");
        registerRecord(vw_cis_Prtcpnt);

        vw_cis_Prt_View = new DataAccessProgramView(new NameInfo("vw_cis_Prt_View", "CIS-PRT-VIEW"), "CIS_REPRT_FILE_12", "CIS_REPRT_FILE");

        cis_Prt_View_R_Reprint = vw_cis_Prt_View.getRecord().newGroupInGroup("CIS_PRT_VIEW_R_REPRINT", "R-REPRINT");
        cis_Prt_View_R_Pin_Num = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Pin_Num", "R-PIN-NUM", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "R_PIN_NUM");
        cis_Prt_View_R_Sys_Id_Cde = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Sys_Id_Cde", "R-SYS-ID-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "R_SYS_ID_CDE");
        cis_Prt_View_R_Package_Cde = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Package_Cde", "R-PACKAGE-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "R_PACKAGE_CDE");
        cis_Prt_View_R_Rqst_Unit_Cde = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Rqst_Unit_Cde", "R-RQST-UNIT-CDE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "R_RQST_UNIT_CDE");
        cis_Prt_View_R_Payee_Cde = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Payee_Cde", "R-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "R_PAYEE_CDE");
        cis_Prt_View_R_Req_Id_Key = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Req_Id_Key", "R-REQ-ID-KEY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "R_REQ_ID_KEY");
        cis_Prt_View_R_Cref_Cntrct = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Cref_Cntrct", "R-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "R_CREF_CNTRCT");
        cis_Prt_View_R_Name = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Name", "R-NAME", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "R_NAME");
        cis_Prt_View_R_Mit_Log_Date_Time = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Mit_Log_Date_Time", "R-MIT-LOG-DATE-TIME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "R_MIT_LOG_DATE_TIME");
        cis_Prt_View_R_Mit_Wpid = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Mit_Wpid", "R-MIT-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "R_MIT_WPID");
        cis_Prt_View_R_Mit_Status_Code = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Mit_Status_Code", "R-MIT-STATUS-CODE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "R_MIT_STATUS_CODE");
        cis_Prt_View_R_Element_Max = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_R_Element_Max", "R-ELEMENT-MAX", FieldType.PACKED_DECIMAL, 3, 
            RepeatingFieldStrategy.None, "R_ELEMENT_MAX");
        cis_Prt_View_Count_Castr_Edl_Elements = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_Count_Castr_Edl_Elements", "C*R-EDL-ELEMENTS", RepeatingFieldStrategy.CAsteriskVariable, 
            "CIS_REPRT_FILE_R_EDL_ELEMENTS");
        cis_Prt_View_R_Edl_ElementsMuGroup = cis_Prt_View_R_Reprint.newGroupInGroup("CIS_PRT_VIEW_R_EDL_ELEMENTSMuGroup", "R_EDL_ELEMENTSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "CIS_REPRT_FILE_R_EDL_ELEMENTS");
        cis_Prt_View_R_Edl_Elements = cis_Prt_View_R_Edl_ElementsMuGroup.newFieldArrayInGroup("cis_Prt_View_R_Edl_Elements", "R-EDL-ELEMENTS", FieldType.STRING, 
            6, new DbsArrayController(1, 191), RepeatingFieldStrategy.SubTableFieldArray, "R_EDL_ELEMENTS");
        cis_Prt_View_Count_Castr_Edl_Elements_2 = cis_Prt_View_R_Reprint.newFieldInGroup("cis_Prt_View_Count_Castr_Edl_Elements_2", "C*R-EDL-ELEMENTS-2", 
            RepeatingFieldStrategy.CAsteriskVariable, "CIS_REPRT_FILE_R_EDL_ELEMENTS_2");
        cis_Prt_View_R_Edl_Elements_2MuGroup = cis_Prt_View_R_Reprint.newGroupInGroup("CIS_PRT_VIEW_R_EDL_ELEMENTS_2MuGroup", "R_EDL_ELEMENTS_2MuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CIS_REPRT_FILE_R_EDL_ELEMENTS_2");
        cis_Prt_View_R_Edl_Elements_2 = cis_Prt_View_R_Edl_Elements_2MuGroup.newFieldArrayInGroup("cis_Prt_View_R_Edl_Elements_2", "R-EDL-ELEMENTS-2", 
            FieldType.STRING, 6, new DbsArrayController(1, 34), RepeatingFieldStrategy.SubTableFieldArray, "R_EDL_ELEMENTS_2");
        cis_Prt_View_R_Date = vw_cis_Prt_View.getRecord().newFieldInGroup("cis_Prt_View_R_Date", "R-DATE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "R_DATE");
        cis_Prt_View_R_Status_Ind = vw_cis_Prt_View.getRecord().newFieldInGroup("cis_Prt_View_R_Status_Ind", "R-STATUS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "R_STATUS_IND");
        cis_Prt_View_R_Tiaa_Cntrct = vw_cis_Prt_View.getRecord().newFieldInGroup("cis_Prt_View_R_Tiaa_Cntrct", "R-TIAA-CNTRCT", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "R_TIAA_CNTRCT");
        registerRecord(vw_cis_Prt_View);

        vw_cis_Bene_File = new DataAccessProgramView(new NameInfo("vw_cis_Bene_File", "CIS-BENE-FILE"), "CIS_BENE_FILE_01_12", "CIS_BENE_FILE", DdmPeriodicGroups.getInstance().getGroups("CIS_BENE_FILE_01_12"));
        cis_Bene_File_Cis_Rcrd_Type_Cde = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Rcrd_Type_Cde", "CIS-RCRD-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_RCRD_TYPE_CDE");
        cis_Bene_File_Cis_Bene_Tiaa_Nbr = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Tiaa_Nbr", "CIS-BENE-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_BENE_TIAA_NBR");
        cis_Bene_File_Cis_Bene_Cref_Nbr = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Cref_Nbr", "CIS-BENE-CREF-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_BENE_CREF_NBR");
        cis_Bene_File_Cis_Bene_Rqst_Id = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Rqst_Id", "CIS-BENE-RQST-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_BENE_RQST_ID");
        cis_Bene_File_Cis_Bene_Unique_Id_Nbr = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Unique_Id_Nbr", "CIS-BENE-UNIQUE-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CIS_BENE_UNIQUE_ID_NBR");
        cis_Bene_File_Cis_Bene_Annt_Ssn = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Annt_Ssn", "CIS-BENE-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_SSN");
        cis_Bene_File_Cis_Bene_Annt_Prfx = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Annt_Prfx", "CIS-BENE-ANNT-PRFX", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_PRFX");
        cis_Bene_File_Cis_Bene_Annt_Frst_Nme = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Annt_Frst_Nme", "CIS-BENE-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_FRST_NME");
        cis_Bene_File_Cis_Bene_Annt_Mid_Nme = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Annt_Mid_Nme", "CIS-BENE-ANNT-MID-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_MID_NME");
        cis_Bene_File_Cis_Bene_Annt_Lst_Nme = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Annt_Lst_Nme", "CIS-BENE-ANNT-LST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_LST_NME");
        cis_Bene_File_Cis_Bene_Annt_Sffx = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Annt_Sffx", "CIS-BENE-ANNT-SFFX", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_BENE_ANNT_SFFX");
        cis_Bene_File_Cis_Bene_Rqst_Id_Key = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Rqst_Id_Key", "CIS-BENE-RQST-ID-KEY", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "CIS_BENE_RQST_ID_KEY");
        cis_Bene_File_Cis_Bene_Std_Free = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Std_Free", "CIS-BENE-STD-FREE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_BENE_STD_FREE");
        cis_Bene_File_Cis_Bene_Estate = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Estate", "CIS-BENE-ESTATE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_BENE_ESTATE");
        cis_Bene_File_Cis_Bene_Trust = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Trust", "CIS-BENE-TRUST", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_BENE_TRUST");
        cis_Bene_File_Cis_Bene_Category = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Bene_Category", "CIS-BENE-CATEGORY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_BENE_CATEGORY");
        cis_Bene_File_Cis_Prmry_Bene_Child_Prvsn = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Prmry_Bene_Child_Prvsn", "CIS-PRMRY-BENE-CHILD-PRVSN", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_PRMRY_BENE_CHILD_PRVSN");
        cis_Bene_File_Cis_Cntgnt_Bene_Child_Prvsn = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Child_Prvsn", "CIS-CNTGNT-BENE-CHILD-PRVSN", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CNTGNT_BENE_CHILD_PRVSN");

        cis_Bene_File_Cis_Clcltn_Bnfcry_Dta = vw_cis_Bene_File.getRecord().newGroupInGroup("CIS_BENE_FILE_CIS_CLCLTN_BNFCRY_DTA", "CIS-CLCLTN-BNFCRY-DTA");
        cis_Bene_File_Cis_Clcltn_Bene_Dod = cis_Bene_File_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_Cis_Clcltn_Bene_Dod", "CIS-CLCLTN-BENE-DOD", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_DOD");
        cis_Bene_File_Cis_Clcltn_Bene_Crossover = cis_Bene_File_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_Cis_Clcltn_Bene_Crossover", "CIS-CLCLTN-BENE-CROSSOVER", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_CROSSOVER");
        cis_Bene_File_Cis_Clcltn_Bene_Name = cis_Bene_File_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_Cis_Clcltn_Bene_Name", "CIS-CLCLTN-BENE-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_NAME");
        cis_Bene_File_Cis_Clcltn_Bene_Rltnshp_Cde = cis_Bene_File_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_Cis_Clcltn_Bene_Rltnshp_Cde", "CIS-CLCLTN-BENE-RLTNSHP-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_RLTNSHP_CDE");
        cis_Bene_File_Cis_Clcltn_Bene_Ssn_Nbr = cis_Bene_File_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_Cis_Clcltn_Bene_Ssn_Nbr", "CIS-CLCLTN-BENE-SSN-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_SSN_NBR");
        cis_Bene_File_Cis_Clcltn_Bene_Dob = cis_Bene_File_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_Cis_Clcltn_Bene_Dob", "CIS-CLCLTN-BENE-DOB", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_DOB");
        cis_Bene_File_Cis_Clcltn_Bene_Sex_Cde = cis_Bene_File_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_Cis_Clcltn_Bene_Sex_Cde", "CIS-CLCLTN-BENE-SEX-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_SEX_CDE");
        cis_Bene_File_Cis_Clcltn_Bene_Calc_Method = cis_Bene_File_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Bene_File_Cis_Clcltn_Bene_Calc_Method", "CIS-CLCLTN-BENE-CALC-METHOD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_CALC_METHOD");
        cis_Bene_File_Count_Castcis_Prmry_Bnfcry_Dta = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Count_Castcis_Prmry_Bnfcry_Dta", "C*CIS-PRMRY-BNFCRY-DTA", 
            RepeatingFieldStrategy.CAsteriskVariable, "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");

        cis_Bene_File_Cis_Prmry_Bnfcry_Dta = vw_cis_Bene_File.getRecord().newGroupInGroup("cis_Bene_File_Cis_Prmry_Bnfcry_Dta", "CIS-PRMRY-BNFCRY-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Std_Txt = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Std_Txt", "CIS-PRMRY-BENE-STD-TXT", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_STD_TXT", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Lump_Sum = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Lump_Sum", "CIS-PRMRY-BENE-LUMP-SUM", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_LUMP_SUM", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Auto_Cmnt_Val = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Auto_Cmnt_Val", 
            "CIS-PRMRY-BENE-AUTO-CMNT-VAL", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_AUTO_CMNT_VAL", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Name = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Name", "CIS-PRMRY-BENE-NAME", 
            FieldType.STRING, 35, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_NAME", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Rltn = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Rltn", "CIS-PRMRY-BENE-RLTN", 
            FieldType.STRING, 10, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_RLTN", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Rltn_Cde = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Rltn_Cde", "CIS-PRMRY-BENE-RLTN-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_RLTN_CDE", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Ssn_Nbr = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Ssn_Nbr", "CIS-PRMRY-BENE-SSN-NBR", 
            FieldType.NUMERIC, 9, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_SSN_NBR", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Dob = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Dob", "CIS-PRMRY-BENE-DOB", 
            FieldType.DATE, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_DOB", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Allctn_Pct = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Allctn_Pct", "CIS-PRMRY-BENE-ALLCTN-PCT", 
            FieldType.PACKED_DECIMAL, 5, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_ALLCTN_PCT", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Nmrtr_Nbr = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Nmrtr_Nbr", "CIS-PRMRY-BENE-NMRTR-NBR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_NMRTR_NBR", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Dnmntr_Nbr = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Dnmntr_Nbr", "CIS-PRMRY-BENE-DNMNTR-NBR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_DNMNTR_NBR", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Spcl_TxtMuGroup = cis_Bene_File_Cis_Prmry_Bnfcry_Dta.newGroupInGroup("CIS_BENE_FILE_CIS_PRMRY_BENE_SPCL_TXTMuGroup", 
            "CIS_PRMRY_BENE_SPCL_TXTMuGroup", RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_BENE_FILE_CIS_PRMRY_BENE_SPCL_TXT", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Cis_Prmry_Bene_Spcl_Txt = cis_Bene_File_Cis_Prmry_Bene_Spcl_TxtMuGroup.newFieldArrayInGroup("cis_Bene_File_Cis_Prmry_Bene_Spcl_Txt", 
            "CIS-PRMRY-BENE-SPCL-TXT", FieldType.STRING, 72, new DbsArrayController(1, 20, 1, 3) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_PRMRY_BENE_SPCL_TXT", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Bene_File_Count_Castcis_Cntgnt_Bnfcry_Dta = vw_cis_Bene_File.getRecord().newFieldInGroup("cis_Bene_File_Count_Castcis_Cntgnt_Bnfcry_Dta", 
            "C*CIS-CNTGNT-BNFCRY-DTA", RepeatingFieldStrategy.CAsteriskVariable, "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");

        cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta = vw_cis_Bene_File.getRecord().newGroupInGroup("cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta", "CIS-CNTGNT-BNFCRY-DTA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Std_Txt = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Std_Txt", "CIS-CNTGNT-BENE-STD-TXT", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_STD_TXT", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Lump_Sum = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Lump_Sum", "CIS-CNTGNT-BENE-LUMP-SUM", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_LUMP_SUM", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Auto_Cmnt_Val = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Auto_Cmnt_Val", 
            "CIS-CNTGNT-BENE-AUTO-CMNT-VAL", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_AUTO_CMNT_VAL", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Name = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Name", "CIS-CNTGNT-BENE-NAME", 
            FieldType.STRING, 35, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_NAME", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Rltn = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Rltn", "CIS-CNTGNT-BENE-RLTN", 
            FieldType.STRING, 10, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_RLTN", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Rltn_Cde = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Rltn_Cde", "CIS-CNTGNT-BENE-RLTN-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_RLTN_CDE", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Ssn_Nbr = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Ssn_Nbr", "CIS-CNTGNT-BENE-SSN-NBR", 
            FieldType.NUMERIC, 9, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_SSN_NBR", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Dob = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Dob", "CIS-CNTGNT-BENE-DOB", 
            FieldType.DATE, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_DOB", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Allctn_Pct = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Allctn_Pct", 
            "CIS-CNTGNT-BENE-ALLCTN-PCT", FieldType.PACKED_DECIMAL, 5, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_CNTGNT_BENE_ALLCTN_PCT", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Nmrtr_Nbr = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Nmrtr_Nbr", 
            "CIS-CNTGNT-BENE-NMRTR-NBR", FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_NMRTR_NBR", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Dnmntr_Nbr = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Dnmntr_Nbr", 
            "CIS-CNTGNT-BENE-DNMNTR-NBR", FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_DNMNTR_NBR", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Spcl_TxtMuGroup = cis_Bene_File_Cis_Cntgnt_Bnfcry_Dta.newGroupInGroup("CIS_BENE_FILE_CIS_CNTGNT_BENE_SPCL_TXTMuGroup", 
            "CIS_CNTGNT_BENE_SPCL_TXTMuGroup", RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_BENE_FILE_CIS_CNTGNT_BENE_SPCL_TXT", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Bene_File_Cis_Cntgnt_Bene_Spcl_Txt = cis_Bene_File_Cis_Cntgnt_Bene_Spcl_TxtMuGroup.newFieldArrayInGroup("cis_Bene_File_Cis_Cntgnt_Bene_Spcl_Txt", 
            "CIS-CNTGNT-BENE-SPCL-TXT", FieldType.STRING, 72, new DbsArrayController(1, 20, 1, 3) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_CNTGNT_BENE_SPCL_TXT", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        registerRecord(vw_cis_Bene_File);

        vw_nec_Fnd_View1 = new DataAccessProgramView(new NameInfo("vw_nec_Fnd_View1", "NEC-FND-VIEW1"), "NEW_EXT_CNTRL_FND", "NEW_EXT_CNTRL");
        nec_Fnd_View1_Nec_Table_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Table_Cde", "NEC-TABLE-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        nec_Fnd_View1_Nec_Ticker_Symbol = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        nec_Fnd_View1_Nec_Fund_Family_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Fund_Family_Cde", "NEC-FUND-FAMILY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "NEC_FUND_FAMILY_CDE");
        nec_Fnd_View1_Nec_Fund_Nme = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Fund_Nme", "NEC-FUND-NME", FieldType.STRING, 60, 
            RepeatingFieldStrategy.None, "NEC_FUND_NME");
        nec_Fnd_View1_Nec_Fund_Inception_Dte = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Fund_Inception_Dte", "NEC-FUND-INCEPTION-DTE", 
            FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "NEC_FUND_INCEPTION_DTE");
        nec_Fnd_View1_Nec_Fund_Cusip = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Fund_Cusip", "NEC-FUND-CUSIP", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "NEC_FUND_CUSIP");
        nec_Fnd_View1_Nec_Category_Class_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Category_Class_Cde", "NEC-CATEGORY-CLASS-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_CATEGORY_CLASS_CDE");
        nec_Fnd_View1_Nec_Category_Rptng_Seq = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Category_Rptng_Seq", "NEC-CATEGORY-RPTNG-SEQ", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_CATEGORY_RPTNG_SEQ");
        nec_Fnd_View1_Nec_Category_Fund_Seq = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Category_Fund_Seq", "NEC-CATEGORY-FUND-SEQ", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_CATEGORY_FUND_SEQ");
        nec_Fnd_View1_Nec_Alpha_Fund_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Alpha_Fund_Cde", "NEC-ALPHA-FUND-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "NEC_ALPHA_FUND_CDE");
        nec_Fnd_View1_Nec_Num_Fund_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Num_Fund_Cde", "NEC-NUM-FUND-CDE", FieldType.NUMERIC, 
            5, RepeatingFieldStrategy.None, "NEC_NUM_FUND_CDE");
        nec_Fnd_View1_Nec_Reporting_Seq = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Reporting_Seq", "NEC-REPORTING-SEQ", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "NEC_REPORTING_SEQ");
        nec_Fnd_View1_Nec_Alpha_Fund_Seq = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Alpha_Fund_Seq", "NEC-ALPHA-FUND-SEQ", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "NEC_ALPHA_FUND_SEQ");
        nec_Fnd_View1_Nec_Abbr_Factor_Key = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Abbr_Factor_Key", "NEC-ABBR-FACTOR-KEY", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "NEC_ABBR_FACTOR_KEY");
        nec_Fnd_View1_Nec_Act_Unit_Acct_Nm = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Act_Unit_Acct_Nm", "NEC-ACT-UNIT-ACCT-NM", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "NEC_ACT_UNIT_ACCT_NM");
        nec_Fnd_View1_Nec_Functional_Cmpny = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Functional_Cmpny", "NEC-FUNCTIONAL-CMPNY", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_FUNCTIONAL_CMPNY");
        nec_Fnd_View1_Nec_Fnd_Super1 = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Fnd_Super1", "NEC-FND-SUPER1", FieldType.STRING, 
            13, RepeatingFieldStrategy.None, "NEC_FND_SUPER1");
        nec_Fnd_View1_Nec_Fnd_Super1.setSuperDescriptor(true);
        nec_Fnd_View1_Nec_Act_Class_Cde = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Act_Class_Cde", "NEC-ACT-CLASS-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "NEC_ACT_CLASS_CDE");
        nec_Fnd_View1_Nec_Act_Invesment_Typ = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Act_Invesment_Typ", "NEC-ACT-INVESMENT-TYP", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_ACT_INVESMENT_TYP");
        nec_Fnd_View1_Nec_Investment_Grouping_Id = vw_nec_Fnd_View1.getRecord().newFieldInGroup("nec_Fnd_View1_Nec_Investment_Grouping_Id", "NEC-INVESTMENT-GROUPING-ID", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "NEC_INVESTMENT_GROUPING_ID");
        registerRecord(vw_nec_Fnd_View1);

        pnd_Output_File = localVariables.newFieldInRecord("pnd_Output_File", "#OUTPUT-FILE", FieldType.STRING, 19583);

        pnd_Output_File__R_Field_2 = localVariables.newGroupInRecord("pnd_Output_File__R_Field_2", "REDEFINE", pnd_Output_File);

        pnd_Output_File_Pnd_Output_File_R = pnd_Output_File__R_Field_2.newGroupInGroup("pnd_Output_File_Pnd_Output_File_R", "#OUTPUT-FILE-R");
        pnd_Output_File_Cis_Rqst_Id = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 
            8);
        pnd_Output_File_Pnd_Cis_Print_Ind = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Print_Ind", "#CIS-PRINT-IND", FieldType.STRING, 
            1);
        pnd_Output_File_Cis_Tiaa_Nbr = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 
            10);
        pnd_Output_File_Cis_Cert_Nbr = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 
            10);
        pnd_Output_File_Cis_Orig_Issue_State = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Orig_Issue_State", "CIS-ORIG-ISSUE-STATE", 
            FieldType.STRING, 2);
        pnd_Output_File_Cis_Cntrct_Apprvl_Ind = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Cntrct_Apprvl_Ind", "CIS-CNTRCT-APPRVL-IND", 
            FieldType.STRING, 1);
        pnd_Output_File_Cis_Pull_Code = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Pull_Code", "CIS-PULL-CODE", FieldType.STRING, 
            4);
        pnd_Output_File_Cis_Four_Fifty_Seven_Ind = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Four_Fifty_Seven_Ind", "CIS-FOUR-FIFTY-SEVEN-IND", 
            FieldType.STRING, 1);
        pnd_Output_File_Cis_Institution_Name = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Institution_Name", "CIS-INSTITUTION-NAME", 
            FieldType.STRING, 32);
        pnd_Output_File_Pnd_Cis_Tiaa_Doi = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Tiaa_Doi", "#CIS-TIAA-DOI", FieldType.STRING, 
            8);
        pnd_Output_File_Pnd_Cis_Cref_Doi = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Cref_Doi", "#CIS-CREF-DOI", FieldType.STRING, 
            8);

        pnd_Output_File__R_Field_3 = pnd_Output_File_Pnd_Output_File_R.newGroupInGroup("pnd_Output_File__R_Field_3", "REDEFINE", pnd_Output_File_Pnd_Cis_Cref_Doi);
        pnd_Output_File_Pnd_Cis_Cref_Doi_N = pnd_Output_File__R_Field_3.newFieldInGroup("pnd_Output_File_Pnd_Cis_Cref_Doi_N", "#CIS-CREF-DOI-N", FieldType.NUMERIC, 
            8);
        pnd_Output_File_Cis_Cntrct_Type = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Output_File_Cis_Tiaa_Cntrct_Type = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Tiaa_Cntrct_Type", "CIS-TIAA-CNTRCT-TYPE", 
            FieldType.STRING, 1);
        pnd_Output_File_Cis_Cref_Cntrct_Type = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Cref_Cntrct_Type", "CIS-CREF-CNTRCT-TYPE", 
            FieldType.STRING, 1);
        pnd_Output_File_Cis_Rea_Cntrct_Type = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Rea_Cntrct_Type", "CIS-REA-CNTRCT-TYPE", 
            FieldType.STRING, 1);
        pnd_Output_File_Cis_Lob = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Lob", "CIS-LOB", FieldType.STRING, 1);
        pnd_Output_File_Cis_Lob_Type = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Lob_Type", "CIS-LOB-TYPE", FieldType.STRING, 
            1);
        pnd_Output_File_Cis_Ownership_Cd = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Ownership_Cd", "CIS-OWNERSHIP-CD", FieldType.STRING, 
            1);
        pnd_Output_File_Pnd_Cis_Access_Ind = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Access_Ind", "#CIS-ACCESS-IND", 
            FieldType.STRING, 1);
        pnd_Output_File_Cis_Frst_Annt_Ctznshp_Cd = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Frst_Annt_Ctznshp_Cd", "CIS-FRST-ANNT-CTZNSHP-CD", 
            FieldType.STRING, 2);
        pnd_Output_File_Pnd_Cis_Atra_Ind = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Atra_Ind", "#CIS-ATRA-IND", FieldType.STRING, 
            1);
        pnd_Output_File_Pnd_Cis_Atra_Issue_Dte = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Atra_Issue_Dte", "#CIS-ATRA-ISSUE-DTE", 
            FieldType.STRING, 8);
        pnd_Output_File_Pnd_Cis_Prmry_Bene_Child_Prvsn = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Prmry_Bene_Child_Prvsn", 
            "#CIS-PRMRY-BENE-CHILD-PRVSN", FieldType.STRING, 1);
        pnd_Output_File_Pnd_Cis_Cntgnt_Bene_Child_Prvsn = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Cntgnt_Bene_Child_Prvsn", 
            "#CIS-CNTGNT-BENE-CHILD-PRVSN", FieldType.STRING, 1);
        pnd_Output_File_Cis_Pin_Nbr = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Output_File_Cis_Frst_Annt_Ssn = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Frst_Annt_Ssn", "CIS-FRST-ANNT-SSN", 
            FieldType.NUMERIC, 9);
        pnd_Output_File_Pnd_Cis_Frst_Annt_Dob = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Frst_Annt_Dob", "#CIS-FRST-ANNT-DOB", 
            FieldType.STRING, 8);
        pnd_Output_File_Cis_Issue_State_Cd = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Issue_State_Cd", "CIS-ISSUE-STATE-CD", 
            FieldType.STRING, 2);
        pnd_Output_File_Cis_Annty_Option = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 
            10);
        pnd_Output_File_Pnd_Cis_Annty_Start_Dte = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Annty_Start_Dte", "#CIS-ANNTY-START-DTE", 
            FieldType.STRING, 8);
        pnd_Output_File_Pnd_Cis_Annty_End_Dte = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Annty_End_Dte", "#CIS-ANNTY-END-DTE", 
            FieldType.STRING, 8);
        pnd_Output_File_Cis_Grnted_Period_Yrs = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Grnted_Period_Yrs", "CIS-GRNTED-PERIOD-YRS", 
            FieldType.NUMERIC, 2);
        pnd_Output_File_Cis_Grnted_Period_Dys = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Grnted_Period_Dys", "CIS-GRNTED-PERIOD-DYS", 
            FieldType.NUMERIC, 3);
        pnd_Output_File_Cis_Grnted_Grd_Amt = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Grnted_Grd_Amt", "CIS-GRNTED-GRD-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Output_File_Cis_Grnted_Std_Amt = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Grnted_Std_Amt", "CIS-GRNTED-STD-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Output_File_Cis_Pymnt_Mode = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Pymnt_Mode", "CIS-PYMNT-MODE", FieldType.STRING, 
            1);

        pnd_Output_File_Pnd_Cis_Da_Information = pnd_Output_File_Pnd_Output_File_R.newGroupArrayInGroup("pnd_Output_File_Pnd_Cis_Da_Information", "#CIS-DA-INFORMATION", 
            new DbsArrayController(1, 20));
        pnd_Output_File_Pnd_Cis_Da_Tiaa_Nbr = pnd_Output_File_Pnd_Cis_Da_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Da_Tiaa_Nbr", "#CIS-DA-TIAA-NBR", 
            FieldType.STRING, 10);
        pnd_Output_File_Pnd_Cis_Da_Cert_Nbr = pnd_Output_File_Pnd_Cis_Da_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Da_Cert_Nbr", "#CIS-DA-CERT-NBR", 
            FieldType.STRING, 10);
        pnd_Output_File_Pnd_Cis_Da_Rea_Proceeds_Amt = pnd_Output_File_Pnd_Cis_Da_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Da_Rea_Proceeds_Amt", 
            "#CIS-DA-REA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Output_File_Pnd_Cis_Da_Tiaa_Proceeds_Amt = pnd_Output_File_Pnd_Cis_Da_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Da_Tiaa_Proceeds_Amt", 
            "#CIS-DA-TIAA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Output_File_Pnd_Cis_Da_Cref_Proceeds_Amt = pnd_Output_File_Pnd_Cis_Da_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Da_Cref_Proceeds_Amt", 
            "#CIS-DA-CREF-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2);

        pnd_Output_File_Pnd_Cis_Cref_Information = pnd_Output_File_Pnd_Output_File_R.newGroupArrayInGroup("pnd_Output_File_Pnd_Cis_Cref_Information", 
            "#CIS-CREF-INFORMATION", new DbsArrayController(1, 20));
        pnd_Output_File_Pnd_Cis_Sg_Fund_Ticker = pnd_Output_File_Pnd_Cis_Cref_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Sg_Fund_Ticker", "#CIS-SG-FUND-TICKER", 
            FieldType.STRING, 10);
        pnd_Output_File_Pnd_Cis_Nec_Fund_Nme = pnd_Output_File_Pnd_Cis_Cref_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Nec_Fund_Nme", "#CIS-NEC-FUND-NME", 
            FieldType.STRING, 60);
        pnd_Output_File_Pnd_Cis_Cref_Acct_Cde = pnd_Output_File_Pnd_Cis_Cref_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Cref_Acct_Cde", "#CIS-CREF-ACCT-CDE", 
            FieldType.STRING, 1);
        pnd_Output_File_Pnd_Cis_Cref_Mnthly_Nbr_Units = pnd_Output_File_Pnd_Cis_Cref_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Cref_Mnthly_Nbr_Units", 
            "#CIS-CREF-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 11, 3);
        pnd_Output_File_Pnd_Cis_Cref_Annual_Nbr_Units = pnd_Output_File_Pnd_Cis_Cref_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Cref_Annual_Nbr_Units", 
            "#CIS-CREF-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 11, 3);
        pnd_Output_File_Pnd_Cis_Cref_Annty_Amt = pnd_Output_File_Pnd_Cis_Cref_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Cref_Annty_Amt", "#CIS-CREF-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Output_File_Pnd_Cis_Cref_Mnthly_Amt = pnd_Output_File_Pnd_Cis_Cref_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Cref_Mnthly_Amt", 
            "#CIS-CREF-MNTHLY-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Output_File_Cis_Rea_Mnthly_Nbr_Units = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Rea_Mnthly_Nbr_Units", "CIS-REA-MNTHLY-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3);
        pnd_Output_File_Cis_Rea_Annual_Nbr_Units = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Rea_Annual_Nbr_Units", "CIS-REA-ANNUAL-NBR-UNITS", 
            FieldType.NUMERIC, 11, 3);
        pnd_Output_File_Pnd_Cis_Rtb_Amt = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Rtb_Amt", "#CIS-RTB-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Output_File_Cis_Rea_Annty_Amt = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Rea_Annty_Amt", "CIS-REA-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2);

        pnd_Output_File_Pnd_Cis_Tacc_Information = pnd_Output_File_Pnd_Output_File_R.newGroupArrayInGroup("pnd_Output_File_Pnd_Cis_Tacc_Information", 
            "#CIS-TACC-INFORMATION", new DbsArrayController(1, 20));
        pnd_Output_File_Pnd_Cis_Tacc_Account = pnd_Output_File_Pnd_Cis_Tacc_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Tacc_Account", "#CIS-TACC-ACCOUNT", 
            FieldType.STRING, 10);
        pnd_Output_File_Pnd_Cis_Tacc_Account_Name = pnd_Output_File_Pnd_Cis_Tacc_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Tacc_Account_Name", 
            "#CIS-TACC-ACCOUNT-NAME", FieldType.STRING, 60);
        pnd_Output_File_Pnd_Cis_Tacc_Mnthly_Nbr_Units = pnd_Output_File_Pnd_Cis_Tacc_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Tacc_Mnthly_Nbr_Units", 
            "#CIS-TACC-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 11, 3);
        pnd_Output_File_Pnd_Cis_Tacc_Annual_Nbr_Units = pnd_Output_File_Pnd_Cis_Tacc_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Tacc_Annual_Nbr_Units", 
            "#CIS-TACC-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 11, 3);
        pnd_Output_File_Pnd_Cis_Tacc_Mnthly_Amt = pnd_Output_File_Pnd_Cis_Tacc_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Tacc_Mnthly_Amt", 
            "#CIS-TACC-MNTHLY-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Output_File_Pnd_Cis_Tacc_Annty_Amt = pnd_Output_File_Pnd_Cis_Tacc_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Tacc_Annty_Amt", "#CIS-TACC-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2);

        pnd_Output_File_Cis_Comut_Information = pnd_Output_File_Pnd_Output_File_R.newGroupArrayInGroup("pnd_Output_File_Cis_Comut_Information", "CIS-COMUT-INFORMATION", 
            new DbsArrayController(1, 5));
        pnd_Output_File_Pnd_Cis_Comut_Grnted_Amt = pnd_Output_File_Cis_Comut_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Comut_Grnted_Amt", "#CIS-COMUT-GRNTED-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Output_File_Pnd_Cis_Comut_Int_Rate = pnd_Output_File_Cis_Comut_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Comut_Int_Rate", "#CIS-COMUT-INT-RATE", 
            FieldType.NUMERIC, 5, 3);
        pnd_Output_File_Pnd_Cis_Comut_Pymt_Method = pnd_Output_File_Cis_Comut_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Comut_Pymt_Method", 
            "#CIS-COMUT-PYMT-METHOD", FieldType.STRING, 8);

        pnd_Output_File_Cis_Comut_2_Information = pnd_Output_File_Pnd_Output_File_R.newGroupArrayInGroup("pnd_Output_File_Cis_Comut_2_Information", "CIS-COMUT-2-INFORMATION", 
            new DbsArrayController(1, 20));
        pnd_Output_File_Pnd_Cis_Comut_Grnted_Amt_2 = pnd_Output_File_Cis_Comut_2_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Comut_Grnted_Amt_2", 
            "#CIS-COMUT-GRNTED-AMT-2", FieldType.NUMERIC, 11, 2);
        pnd_Output_File_Pnd_Cis_Comut_Int_Rate_2 = pnd_Output_File_Cis_Comut_2_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Comut_Int_Rate_2", 
            "#CIS-COMUT-INT-RATE-2", FieldType.NUMERIC, 5, 3);
        pnd_Output_File_Pnd_Cis_Comut_Pymt_Method_2 = pnd_Output_File_Cis_Comut_2_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Comut_Pymt_Method_2", 
            "#CIS-COMUT-PYMT-METHOD-2", FieldType.STRING, 8);
        pnd_Output_File_Pnd_Cis_Comut_Mortality_Basis = pnd_Output_File_Cis_Comut_2_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Comut_Mortality_Basis", 
            "#CIS-COMUT-MORTALITY-BASIS", FieldType.STRING, 30);

        pnd_Output_File_Pnd_Cis_Bene_Information = pnd_Output_File_Pnd_Output_File_R.newGroupArrayInGroup("pnd_Output_File_Pnd_Cis_Bene_Information", 
            "#CIS-BENE-INFORMATION", new DbsArrayController(1, 40));
        pnd_Output_File_Pnd_Cis_Bene_Type = pnd_Output_File_Pnd_Cis_Bene_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Bene_Type", "#CIS-BENE-TYPE", 
            FieldType.STRING, 1);
        pnd_Output_File_Pnd_Cis_Bene_Name = pnd_Output_File_Pnd_Cis_Bene_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Bene_Name", "#CIS-BENE-NAME", 
            FieldType.STRING, 35);
        pnd_Output_File_Pnd_Cis_Bene_Rltn = pnd_Output_File_Pnd_Cis_Bene_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Bene_Rltn", "#CIS-BENE-RLTN", 
            FieldType.STRING, 10);
        pnd_Output_File_Pnd_Cis_Bene_Rltn_Cde = pnd_Output_File_Pnd_Cis_Bene_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Bene_Rltn_Cde", "#CIS-BENE-RLTN-CDE", 
            FieldType.STRING, 2);
        pnd_Output_File_Pnd_Cis_Bene_Ssn_Nbr = pnd_Output_File_Pnd_Cis_Bene_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Bene_Ssn_Nbr", "#CIS-BENE-SSN-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Output_File_Pnd_Cis_Bene_Dob = pnd_Output_File_Pnd_Cis_Bene_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Bene_Dob", "#CIS-BENE-DOB", 
            FieldType.STRING, 10);
        pnd_Output_File_Pnd_Cis_Bene_Nmrtr_Nbr = pnd_Output_File_Pnd_Cis_Bene_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Bene_Nmrtr_Nbr", "#CIS-BENE-NMRTR-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Output_File_Pnd_Cis_Bene_Dnmntr_Nbr = pnd_Output_File_Pnd_Cis_Bene_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Bene_Dnmntr_Nbr", 
            "#CIS-BENE-DNMNTR-NBR", FieldType.NUMERIC, 3);
        pnd_Output_File_Pnd_Cis_Bene_Spcl_Txt = pnd_Output_File_Pnd_Cis_Bene_Information.newFieldInGroup("pnd_Output_File_Pnd_Cis_Bene_Spcl_Txt", "#CIS-BENE-SPCL-TXT", 
            FieldType.STRING, 216);
        pnd_Output_File_Pnd_Cis_Scnd_Annt_Full_Name = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Scnd_Annt_Full_Name", 
            "#CIS-SCND-ANNT-FULL-NAME", FieldType.STRING, 100);
        pnd_Output_File_Cis_Scnd_Annt_Ssn = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", 
            FieldType.STRING, 9);
        pnd_Output_File_Pnd_Cis_Scnd_Annt_Dob = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Pnd_Cis_Scnd_Annt_Dob", "#CIS-SCND-ANNT-DOB", 
            FieldType.STRING, 8);
        pnd_Output_File_Cis_Trnsf_Units = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Trnsf_Units", "CIS-TRNSF-UNITS", FieldType.NUMERIC, 
            8, 3);
        pnd_Output_File_Cis_Trnsf_Cert_Nbr = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Trnsf_Cert_Nbr", "CIS-TRNSF-CERT-NBR", 
            FieldType.STRING, 10);
        pnd_Output_File_Cis_Trnsf_Tiaa_Rea_Nbr = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Trnsf_Tiaa_Rea_Nbr", "CIS-TRNSF-TIAA-REA-NBR", 
            FieldType.STRING, 10);
        pnd_Output_File_Cis_Trnsf_Acct_Cde = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Trnsf_Acct_Cde", "CIS-TRNSF-ACCT-CDE", 
            FieldType.STRING, 1);
        pnd_Output_File_Cis_Trnsf_Flag = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Trnsf_Flag", "CIS-TRNSF-FLAG", FieldType.STRING, 
            1);
        pnd_Output_File_Cis_Trnsf_From_Company = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Trnsf_From_Company", "CIS-TRNSF-FROM-COMPANY", 
            FieldType.STRING, 4);
        pnd_Output_File_Cis_Trnsf_To_Company = pnd_Output_File_Pnd_Output_File_R.newFieldInGroup("pnd_Output_File_Cis_Trnsf_To_Company", "CIS-TRNSF-TO-COMPANY", 
            FieldType.STRING, 4);
        pnd_Cis_Ben_Super_2 = localVariables.newFieldInRecord("pnd_Cis_Ben_Super_2", "#CIS-BEN-SUPER-2", FieldType.STRING, 11);

        pnd_Cis_Ben_Super_2__R_Field_4 = localVariables.newGroupInRecord("pnd_Cis_Ben_Super_2__R_Field_4", "REDEFINE", pnd_Cis_Ben_Super_2);
        pnd_Cis_Ben_Super_2_Pnd_Cis_Rcrd_Type_Cde = pnd_Cis_Ben_Super_2__R_Field_4.newFieldInGroup("pnd_Cis_Ben_Super_2_Pnd_Cis_Rcrd_Type_Cde", "#CIS-RCRD-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Cis_Ben_Super_2_Pnd_Cis_Bene_Tiaa_Nbr = pnd_Cis_Ben_Super_2__R_Field_4.newFieldInGroup("pnd_Cis_Ben_Super_2_Pnd_Cis_Bene_Tiaa_Nbr", "#CIS-BENE-TIAA-NBR", 
            FieldType.STRING, 10);
        pnd_Output_File2 = localVariables.newFieldInRecord("pnd_Output_File2", "#OUTPUT-FILE2", FieldType.STRING, 19583);
        pnd_Output_File3 = localVariables.newFieldArrayInRecord("pnd_Output_File3", "#OUTPUT-FILE3", FieldType.STRING, 3000, new DbsArrayController(1, 
            20));
        pnd_Print_Ind = localVariables.newFieldInRecord("pnd_Print_Ind", "#PRINT-IND", FieldType.STRING, 1);
        pnd_Recover_Ind = localVariables.newFieldInRecord("pnd_Recover_Ind", "#RECOVER-IND", FieldType.STRING, 1);
        pnd_Todays_Business_Dt = localVariables.newFieldInRecord("pnd_Todays_Business_Dt", "#TODAYS-BUSINESS-DT", FieldType.NUMERIC, 8);

        pnd_Todays_Business_Dt__R_Field_5 = localVariables.newGroupInRecord("pnd_Todays_Business_Dt__R_Field_5", "REDEFINE", pnd_Todays_Business_Dt);
        pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A = pnd_Todays_Business_Dt__R_Field_5.newFieldInGroup("pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A", 
            "#TODAYS-BUSINESS-DT-A", FieldType.STRING, 8);
        pnd_Todays_Business_Time = localVariables.newFieldInRecord("pnd_Todays_Business_Time", "#TODAYS-BUSINESS-TIME", FieldType.NUMERIC, 7);
        pnd_Print_Date = localVariables.newFieldInRecord("pnd_Print_Date", "#PRINT-DATE", FieldType.STRING, 8);

        pnd_Print_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Print_Date__R_Field_6", "REDEFINE", pnd_Print_Date);
        pnd_Print_Date_Pnd_Print_Date_N = pnd_Print_Date__R_Field_6.newFieldInGroup("pnd_Print_Date_Pnd_Print_Date_N", "#PRINT-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Cis_Pipe = localVariables.newFieldInRecord("pnd_Cis_Pipe", "#CIS-PIPE", FieldType.STRING, 1);
        pnd_Cis_Tilde = localVariables.newFieldInRecord("pnd_Cis_Tilde", "#CIS-TILDE", FieldType.STRING, 1);
        pnd_Cis_Caret = localVariables.newFieldInRecord("pnd_Cis_Caret", "#CIS-CARET", FieldType.STRING, 1);
        pnd_Nec_Fnd_Super1 = localVariables.newFieldInRecord("pnd_Nec_Fnd_Super1", "#NEC-FND-SUPER1", FieldType.STRING, 13);

        pnd_Nec_Fnd_Super1__R_Field_7 = localVariables.newGroupInRecord("pnd_Nec_Fnd_Super1__R_Field_7", "REDEFINE", pnd_Nec_Fnd_Super1);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Table_Cde_Super1 = pnd_Nec_Fnd_Super1__R_Field_7.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Table_Cde_Super1", 
            "#NEC-FND-TABLE-CDE-SUPER1", FieldType.STRING, 3);
        pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Ticker_Symbol_Super1 = pnd_Nec_Fnd_Super1__R_Field_7.newFieldInGroup("pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Ticker_Symbol_Super1", 
            "#NEC-FND-TICKER-SYMBOL-SUPER1", FieldType.STRING, 10);
        pnd_Cis_Da_Occurrence = localVariables.newFieldInRecord("pnd_Cis_Da_Occurrence", "#CIS-DA-OCCURRENCE", FieldType.NUMERIC, 3);
        pnd_Cis_Cref_Occurrence = localVariables.newFieldInRecord("pnd_Cis_Cref_Occurrence", "#CIS-CREF-OCCURRENCE", FieldType.NUMERIC, 3);
        pnd_Cis_Tacc_Occurrence = localVariables.newFieldInRecord("pnd_Cis_Tacc_Occurrence", "#CIS-TACC-OCCURRENCE", FieldType.NUMERIC, 3);
        pnd_Cis_Comut_Occurrence = localVariables.newFieldInRecord("pnd_Cis_Comut_Occurrence", "#CIS-COMUT-OCCURRENCE", FieldType.NUMERIC, 3);
        pnd_Cis_Comut_2_Occurrence = localVariables.newFieldInRecord("pnd_Cis_Comut_2_Occurrence", "#CIS-COMUT-2-OCCURRENCE", FieldType.NUMERIC, 3);
        pnd_Cis_Bene_Occurrence = localVariables.newFieldInRecord("pnd_Cis_Bene_Occurrence", "#CIS-BENE-OCCURRENCE", FieldType.NUMERIC, 3);
        pnd_Recover = localVariables.newFieldInRecord("pnd_Recover", "#RECOVER", FieldType.NUMERIC, 9);
        pnd_Recover_Only = localVariables.newFieldInRecord("pnd_Recover_Only", "#RECOVER-ONLY", FieldType.NUMERIC, 9);
        pnd_Recover_Part = localVariables.newFieldInRecord("pnd_Recover_Part", "#RECOVER-PART", FieldType.NUMERIC, 9);
        pnd_Part = localVariables.newFieldInRecord("pnd_Part", "#PART", FieldType.NUMERIC, 9);
        pnd_Reprint = localVariables.newFieldInRecord("pnd_Reprint", "#REPRINT", FieldType.NUMERIC, 9);
        pnd_Total_Detail = localVariables.newFieldInRecord("pnd_Total_Detail", "#TOTAL-DETAIL", FieldType.NUMERIC, 9);
        pnd_Summary = localVariables.newFieldInRecord("pnd_Summary", "#SUMMARY", FieldType.NUMERIC, 9);
        pnd_Mdm = localVariables.newFieldInRecord("pnd_Mdm", "#MDM", FieldType.NUMERIC, 9);
        pnd_Non_Mdm = localVariables.newFieldInRecord("pnd_Non_Mdm", "#NON-MDM", FieldType.NUMERIC, 9);
        pnd_Mq = localVariables.newFieldInRecord("pnd_Mq", "#MQ", FieldType.NUMERIC, 9);
        pnd_Mq_Detail = localVariables.newFieldInRecord("pnd_Mq_Detail", "#MQ-DETAIL", FieldType.NUMERIC, 9);
        pnd_Mq_Error = localVariables.newFieldInRecord("pnd_Mq_Error", "#MQ-ERROR", FieldType.NUMERIC, 9);
        pnd_Output = localVariables.newFieldInRecord("pnd_Output", "#OUTPUT", FieldType.NUMERIC, 9);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.NUMERIC, 3);
        pnd_I3 = localVariables.newFieldInRecord("pnd_I3", "#I3", FieldType.NUMERIC, 3);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_First_Name_S = localVariables.newFieldInRecord("pnd_First_Name_S", "#FIRST-NAME-S", FieldType.STRING, 30);
        pnd_Filler_S1 = localVariables.newFieldInRecord("pnd_Filler_S1", "#FILLER-S1", FieldType.STRING, 1);
        pnd_Mid_Name_S = localVariables.newFieldInRecord("pnd_Mid_Name_S", "#MID-NAME-S", FieldType.STRING, 1);
        pnd_Filler_S2 = localVariables.newFieldInRecord("pnd_Filler_S2", "#FILLER-S2", FieldType.STRING, 1);
        pnd_Last_Name_S = localVariables.newFieldInRecord("pnd_Last_Name_S", "#LAST-NAME-S", FieldType.STRING, 30);

        pnd_Cis_Tiaa_Cntrct_Nbr_Input = localVariables.newGroupArrayInRecord("pnd_Cis_Tiaa_Cntrct_Nbr_Input", "#CIS-TIAA-CNTRCT-NBR-INPUT", new DbsArrayController(1, 
            20));
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data = pnd_Cis_Tiaa_Cntrct_Nbr_Input.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data", 
            "#CIS-TIAA-CNTRCT-NBR-INPUT-DATA", FieldType.STRING, 10);

        pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_8 = pnd_Cis_Tiaa_Cntrct_Nbr_Input.newGroupInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_8", "REDEFINE", 
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_8.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input", 
            "#FROM-STATE-INPUT", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_8.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input", 
            "#FROM-APPROVAL-IND-INPUT", FieldType.STRING, 1);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_State_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_8.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_State_Input", 
            "#TO-STATE-INPUT", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_Approval_Ind_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_8.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_Approval_Ind_Input", 
            "#TO-APPROVAL-IND-INPUT", FieldType.STRING, 1);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_8.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input", 
            "#RES-STATE-INPUT", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_8.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input", 
            "#RES-APPROVAL-IND-INPUT", FieldType.STRING, 1);
        pnd_Tiaa_Date_N = localVariables.newFieldInRecord("pnd_Tiaa_Date_N", "#TIAA-DATE-N", FieldType.NUMERIC, 8);

        pnd_Tiaa_Date_N__R_Field_9 = localVariables.newGroupInRecord("pnd_Tiaa_Date_N__R_Field_9", "REDEFINE", pnd_Tiaa_Date_N);
        pnd_Tiaa_Date_N_Pnd_Tiaa_Date_A = pnd_Tiaa_Date_N__R_Field_9.newFieldInGroup("pnd_Tiaa_Date_N_Pnd_Tiaa_Date_A", "#TIAA-DATE-A", FieldType.STRING, 
            8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Save_Cis_Tiaa_Nbr = localVariables.newFieldArrayInRecord("pnd_Save_Cis_Tiaa_Nbr", "#SAVE-CIS-TIAA-NBR", FieldType.STRING, 10, new DbsArrayController(1, 
            999999));
        pnd_Seconds = localVariables.newFieldInRecord("pnd_Seconds", "#SECONDS", FieldType.INTEGER, 2);
        pnd_Ix1 = localVariables.newFieldInRecord("pnd_Ix1", "#IX1", FieldType.NUMERIC, 6);
        pnd_Ix2 = localVariables.newFieldInRecord("pnd_Ix2", "#IX2", FieldType.NUMERIC, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cis_Prtcpnt.reset();
        vw_cis_Prt_View.reset();
        vw_cis_Bene_File.reset();
        vw_nec_Fnd_View1.reset();

        ldaAppl170.initializeValues();

        localVariables.reset();
        pnd_Cis_Pipe.setInitialValue("|");
        pnd_Cis_Tilde.setInitialValue("~");
        pnd_Cis_Caret.setInitialValue("^");
        pnd_Seconds.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisb9900() throws Exception
    {
        super("Cisb9900");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132
        pnd_Todays_Business_Time.setValue(Global.getTIMN());                                                                                                              //Natural: ASSIGN #TODAYS-BUSINESS-TIME := *TIMN
        getReports().write(0, "=",pnd_Todays_Business_Time);                                                                                                              //Natural: WRITE '=' #TODAYS-BUSINESS-TIME
        if (Global.isEscape()) return;
        if (condition(pnd_Todays_Business_Time.less(500000)))                                                                                                             //Natural: IF #TODAYS-BUSINESS-TIME < 0500000
        {
            pnd_Date.compute(new ComputeParameters(false, pnd_Date), Global.getDATX().subtract(1));                                                                       //Natural: ASSIGN #DATE := *DATX - 1
            pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A.setValueEdited(pnd_Date,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #DATE ( EM = YYYYMMDD ) TO #TODAYS-BUSINESS-DT-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Todays_Business_Dt.setValue(Global.getDATN());                                                                                                            //Natural: ASSIGN #TODAYS-BUSINESS-DT := *DATN
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "=",pnd_Todays_Business_Dt);                                                                                                                //Natural: WRITE '=' #TODAYS-BUSINESS-DT
        if (Global.isEscape()) return;
        DbsUtil.callnat(Scin8888.class , getCurrentProcessState(), pnd_Todays_Business_Dt);                                                                               //Natural: CALLNAT 'SCIN8888' #TODAYS-BUSINESS-DT
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Todays_Business_Dt.equals(getZero())))                                                                                                          //Natural: IF #TODAYS-BUSINESS-DT = 0
        {
            getReports().write(0, "*********************");                                                                                                               //Natural: WRITE '*********************'
            if (Global.isEscape()) return;
            getReports().write(0, "******* THERE IS NO BUSINESS DATE ON THE FILE *******");                                                                               //Natural: WRITE '******* THERE IS NO BUSINESS DATE ON THE FILE *******'
            if (Global.isEscape()) return;
            getReports().write(0, "*********************");                                                                                                               //Natural: WRITE '*********************'
            if (Global.isEscape()) return;
            DbsUtil.terminate(12);  if (true) return;                                                                                                                     //Natural: TERMINATE 12
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "INVOKING OPEN :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                     //Natural: WRITE 'INVOKING OPEN :' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        DbsUtil.invokeMain(DbsUtil.getBlType("CISP0051"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'CISP0051'
        if (condition(Global.isEscape())) return;
        getReports().write(0, "FINISHED OPEN:",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                      //Natural: WRITE 'FINISHED OPEN:' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        if (condition(gdaCisg0051.getPnd_Pnd_Cisg0051_Pnd_Pnd_Data_Response().getValue(1).notEquals("0")))                                                                //Natural: IF ##DATA-RESPONSE ( 1 ) NE '0'
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO 60
            for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(60)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
            {
                pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaCisg0051.getPnd_Pnd_Cisg0051_Pnd_Pnd_Data_Response().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()))); //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I ) INTO #RC LEAVING NO SPACE
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Rc);                                                                                                                            //Natural: WRITE '=' #RC
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
                NEWLINE);
            if (Global.isEscape()) return;
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Recover_Ind.reset();                                                                                                                                          //Natural: RESET #RECOVER-IND
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 #RECOVER-FILE
        while (condition(getWorkFiles().read(2, pnd_Recover_File)))
        {
            if (condition(pnd_Recover_File_Pnd_Recover_Tiaa.equals(" ")))                                                                                                 //Natural: IF #RECOVER-TIAA = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "PROCESSING RECOVERY FILE");                                                                                                            //Natural: WRITE 'PROCESSING RECOVERY FILE'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Recover.nadd(1);                                                                                                                                          //Natural: ASSIGN #RECOVER := #RECOVER + 1
            if (condition(pnd_Recover.equals(1)))                                                                                                                         //Natural: IF #RECOVER = 1
            {
                if (condition(pnd_Recover_File_Pnd_Recover_Filler.equals(" ") && pnd_Recover_File_Pnd_Recover_Print_Ind.equals("R")))                                     //Natural: IF #RECOVER-FILLER = ' ' AND #RECOVER-PRINT-IND = 'R'
                {
                    pnd_Recover_Only.nadd(1);                                                                                                                             //Natural: ASSIGN #RECOVER-ONLY := #RECOVER-ONLY + 1
                    pnd_Recover_Ind.setValue(pnd_Recover_File_Pnd_Recover_Print_Ind);                                                                                     //Natural: ASSIGN #RECOVER-IND := #RECOVER-PRINT-IND
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            vw_cis_Prtcpnt.startDatabaseFind                                                                                                                              //Natural: FIND CIS-PRTCPNT WITH CIS-TIAA-NBR = #RECOVER-TIAA
            (
            "FIND01",
            new Wc[] { new Wc("CIS_TIAA_NBR", "=", pnd_Recover_File_Pnd_Recover_Tiaa, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_cis_Prtcpnt.readNextRow("FIND01", true)))
            {
                vw_cis_Prtcpnt.setIfNotFoundControlFlag(false);
                if (condition(vw_cis_Prtcpnt.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO RECS FOUND ON CIS PARTICIPANT FILE FOR CONTRACT: ",pnd_Recover_File_Pnd_Recover_Tiaa);                                      //Natural: WRITE 'NO RECS FOUND ON CIS PARTICIPANT FILE FOR CONTRACT: ' #RECOVER-TIAA
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Recover_Part.nadd(1);                                                                                                                                 //Natural: ASSIGN #RECOVER-PART := #RECOVER-PART + 1
                                                                                                                                                                          //Natural: PERFORM IA-PROCESS
                sub_Ia_Process();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Recover_Ind.equals(" ")))                                                                                                                       //Natural: IF #RECOVER-IND = ' '
        {
            pnd_Print_Ind.reset();                                                                                                                                        //Natural: RESET #PRINT-IND #SAVE-CIS-TIAA-NBR ( * ) #IX2
            pnd_Save_Cis_Tiaa_Nbr.getValue("*").reset();
            pnd_Ix2.reset();
            vw_cis_Prtcpnt.startDatabaseRead                                                                                                                              //Natural: READ CIS-PRTCPNT BY CIS-OPEN-STATUS STARTING FROM 'OI'
            (
            "READ02",
            new Wc[] { new Wc("CIS_OPEN_STATUS", ">=", "OI", WcType.BY) },
            new Oc[] { new Oc("CIS_OPEN_STATUS", "ASC") }
            );
            READ02:
            while (condition(vw_cis_Prtcpnt.readNextRow("READ02")))
            {
                if (condition(! (cis_Prtcpnt_Cis_Opn_Clsd_Ind.equals("O") && cis_Prtcpnt_Cis_Status_Cd.equals("I"))))                                                     //Natural: IF NOT ( CIS-OPN-CLSD-IND = 'O' AND CIS-STATUS-CD = 'I' )
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cis_Prtcpnt_Cis_Decedent_Contract.greater(" ")))                                                                                            //Natural: IF CIS-DECEDENT-CONTRACT GT ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(!(cis_Prtcpnt_Cis_Rqst_Id.equals("IA") && cis_Prtcpnt_Cis_Pin_Nbr.greater(getZero()))))                                                     //Natural: ACCEPT IF CIS-PRTCPNT.CIS-RQST-ID = 'IA' AND CIS-PRTCPNT.CIS-PIN-NBR > 0
                {
                    continue;
                }
                pnd_Print_Date.setValueEdited(cis_Prtcpnt_Cis_Cntrct_Print_Dte,new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED CIS-CNTRCT-PRINT-DTE ( EM = YYYYMMDD ) TO #PRINT-DATE
                if (condition(pnd_Print_Date_Pnd_Print_Date_N.equals(getZero()) || pnd_Print_Date_Pnd_Print_Date_N.greater(pnd_Todays_Business_Dt)))                      //Natural: IF #PRINT-DATE-N = 0 OR #PRINT-DATE-N > #TODAYS-BUSINESS-DT
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Part.nadd(1);                                                                                                                                         //Natural: ASSIGN #PART := #PART + 1
                                                                                                                                                                          //Natural: PERFORM IA-PROCESS
                sub_Ia_Process();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            pnd_Reprint.reset();                                                                                                                                          //Natural: RESET #REPRINT #SAVE-CIS-TIAA-NBR ( * ) #IX2
            pnd_Save_Cis_Tiaa_Nbr.getValue("*").reset();
            pnd_Ix2.reset();
            vw_cis_Prt_View.startDatabaseRead                                                                                                                             //Natural: READ CIS-PRT-VIEW BY R-TIAA-CNTRCT
            (
            "READ03",
            new Oc[] { new Oc("R_TIAA_CNTRCT", "ASC") }
            );
            READ03:
            while (condition(vw_cis_Prt_View.readNextRow("READ03")))
            {
                if (condition(!(cis_Prt_View_R_Pin_Num.greater(getZero()))))                                                                                              //Natural: ACCEPT IF R-PIN-NUM > 0
                {
                    continue;
                }
                vw_cis_Prtcpnt.startDatabaseRead                                                                                                                          //Natural: READ CIS-PRTCPNT BY CIS-RQST-ID-KEY = CIS-PRT-VIEW.R-REQ-ID-KEY
                (
                "READ04",
                new Wc[] { new Wc("CIS_RQST_ID_KEY", ">=", cis_Prt_View_R_Req_Id_Key, WcType.BY) },
                new Oc[] { new Oc("CIS_RQST_ID_KEY", "ASC") }
                );
                READ04:
                while (condition(vw_cis_Prtcpnt.readNextRow("READ04")))
                {
                    short decideConditionsMet1945 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CIS-RQST-ID-KEY NE CIS-PRT-VIEW.R-REQ-ID-KEY
                    if (condition(cis_Prtcpnt_Cis_Rqst_Id_Key.notEquals(cis_Prt_View_R_Req_Id_Key)))
                    {
                        decideConditionsMet1945++;
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: WHEN CIS-PRT-VIEW.R-TIAA-CNTRCT NE ' '
                    else if (condition(cis_Prt_View_R_Tiaa_Cntrct.notEquals(" ")))
                    {
                        decideConditionsMet1945++;
                        if (condition(cis_Prt_View_R_Tiaa_Cntrct.notEquals(cis_Prtcpnt_Cis_Tiaa_Nbr)))                                                                    //Natural: IF CIS-PRT-VIEW.R-TIAA-CNTRCT NE CIS-PRTCPNT.CIS-TIAA-NBR
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN CIS-PRT-VIEW.R-CREF-CNTRCT NE ' '
                    else if (condition(cis_Prt_View_R_Cref_Cntrct.notEquals(" ")))
                    {
                        decideConditionsMet1945++;
                        if (condition(cis_Prt_View_R_Cref_Cntrct.notEquals(cis_Prtcpnt_Cis_Cert_Nbr)))                                                                    //Natural: IF CIS-PRT-VIEW.R-CREF-CNTRCT NE CIS-PRTCPNT.CIS-CERT-NBR
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(cis_Prtcpnt_Cis_Rqst_Id.equals("IA") && cis_Prtcpnt_Cis_Pin_Nbr.greater(getZero())))                                                    //Natural: IF CIS-PRTCPNT.CIS-RQST-ID = 'IA' AND CIS-PRTCPNT.CIS-PIN-NBR > 0
                    {
                        pnd_Print_Ind.setValue("R");                                                                                                                      //Natural: ASSIGN #PRINT-IND := 'R'
                        pnd_Reprint.nadd(1);                                                                                                                              //Natural: ASSIGN #REPRINT := #REPRINT + 1
                                                                                                                                                                          //Natural: PERFORM IA-PROCESS
                        sub_Ia_Process();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Output_File2.reset();                                                                                                                                         //Natural: RESET #OUTPUT-FILE2 #I-PDA-DATA-A
        pdaCisa0050.getPnd_Cisa0050_Pnd_I_Pda_Data_A().reset();
        pnd_Output.nadd(1);                                                                                                                                               //Natural: ASSIGN #OUTPUT := #OUTPUT + 1
        pnd_Part.nadd(pnd_Recover_Part);                                                                                                                                  //Natural: ASSIGN #PART := #PART + #RECOVER-PART
        pnd_Total_Detail.compute(new ComputeParameters(false, pnd_Total_Detail), pnd_Part.add(pnd_Reprint));                                                              //Natural: ASSIGN #TOTAL-DETAIL := #PART + #REPRINT
        pnd_Summary.nadd(1);                                                                                                                                              //Natural: ASSIGN #SUMMARY := #SUMMARY + 1
        pnd_Output_File2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "SUMMARY RECORD", pnd_Cis_Pipe, pnd_Todays_Business_Dt, pnd_Cis_Pipe,                   //Natural: COMPRESS 'SUMMARY RECORD' #CIS-PIPE #TODAYS-BUSINESS-DT #CIS-PIPE #PART #CIS-PIPE #REPRINT #CIS-PIPE '0' #CIS-PIPE #MQ-DETAIL INTO #OUTPUT-FILE2 LEAVING NO SPACE
            pnd_Part, pnd_Cis_Pipe, pnd_Reprint, pnd_Cis_Pipe, "0", pnd_Cis_Pipe, pnd_Mq_Detail));
        DbsUtil.examine(new ExamineSource(pnd_Output_File2), new ExamineSearch(" "), new ExamineGivingLength(pdaCisa0050.getPnd_Cisa0050_Pnd_I_Data_Length()));           //Natural: EXAMINE #OUTPUT-FILE2 FOR ' ' GIVING LENGTH #I-DATA-LENGTH
        setValueToSubstring(pnd_Output_File2.getSubstring(1,pdaCisa0050.getPnd_Cisa0050_Pnd_I_Data_Length().getInt()),pdaCisa0050.getPnd_Cisa0050_Pnd_I_Pda_Data_A(),     //Natural: MOVE SUBSTR ( #OUTPUT-FILE2,1,#I-DATA-LENGTH ) TO SUBSTR ( #I-PDA-DATA-A,1,#I-DATA-LENGTH )
            1,pdaCisa0050.getPnd_Cisa0050_Pnd_I_Data_Length().getInt());
        pdaCisa0050.getPnd_Cisa0050_Pnd_I_Tiaa().reset();                                                                                                                 //Natural: RESET #I-TIAA #CIS-PRINT-IND
        pnd_Output_File_Pnd_Cis_Print_Ind.reset();
        getWorkFiles().write(1, false, pdaCisa0050.getPnd_Cisa0050_Pnd_I_Tiaa(), " ", pnd_Output_File_Pnd_Cis_Print_Ind, " ", pdaCisa0050.getPnd_Cisa0050_Pnd_I_Data_Length(),  //Natural: WRITE WORK FILE 1 #I-TIAA ' ' #CIS-PRINT-IND ' ' #I-DATA-LENGTH ' ' #I-PDA-DATA-A
            " ", pdaCisa0050.getPnd_Cisa0050_Pnd_I_Pda_Data_A());
        pdaCisa0050.getPnd_Cisa0050_Pnd_I_Mq_Count().setValue(pnd_Output);                                                                                                //Natural: ASSIGN #I-MQ-COUNT := #OUTPUT
        DbsUtil.callnat(Cisn0050.class , getCurrentProcessState(), pdaCisa0050.getPnd_Cisa0050());                                                                        //Natural: CALLNAT 'CISN0050' #CISA0050
        if (condition(Global.isEscape())) return;
        if (condition(pdaCisa0050.getPnd_Cisa0050_Pnd_O_Return_Code().equals("0000")))                                                                                    //Natural: IF #O-RETURN-CODE EQ '0000'
        {
            pnd_Mq.nadd(1);                                                                                                                                               //Natural: ASSIGN #MQ := #MQ + 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Mq_Error.nadd(1);                                                                                                                                         //Natural: ASSIGN #MQ-ERROR := #MQ-ERROR + 1
            getReports().write(0, pdaCisa0050.getPnd_Cisa0050_Pnd_O_Return_Code()," ",pdaCisa0050.getPnd_Cisa0050_Pnd_O_Return_Text());                                   //Natural: WRITE #O-RETURN-CODE ' ' #O-RETURN-TEXT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.invokeMain(DbsUtil.getBlType("CISP0052"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'CISP0052'
        if (condition(Global.isEscape())) return;
        getReports().write(0, "LEAVING CISB9900 :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                  //Natural: WRITE 'LEAVING CISB9900 :' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL INPUT RECOVERY RECS READ:                   ",pnd_Recover);                                                                          //Natural: WRITE 'TOTAL INPUT RECOVERY RECS READ:                   ' #RECOVER
        if (Global.isEscape()) return;
        getReports().write(0, "RECOVERY PROCESS ONLY                             ",pnd_Recover_Only);                                                                     //Natural: WRITE 'RECOVERY PROCESS ONLY                             ' #RECOVER-ONLY
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL PARTICIPANT RECS RETRIEVED FROM RECOVERY:   ",pnd_Recover_Part);                                                                     //Natural: WRITE 'TOTAL PARTICIPANT RECS RETRIEVED FROM RECOVERY:   ' #RECOVER-PART
        if (Global.isEscape()) return;
        getReports().write(0, "--------------------------------------------------------------");                                                                          //Natural: WRITE '--------------------------------------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL ENROLLMENTS RECORDS PROCESSED:              ",pnd_Part);                                                                             //Natural: WRITE 'TOTAL ENROLLMENTS RECORDS PROCESSED:              ' #PART
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL REPRINT RECORDS PROCESSED:                  ",pnd_Reprint);                                                                          //Natural: WRITE 'TOTAL REPRINT RECORDS PROCESSED:                  ' #REPRINT
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL IA ENROLLMENTS & REPRINTS PROCESSED:        ",pnd_Total_Detail);                                                                     //Natural: WRITE 'TOTAL IA ENROLLMENTS & REPRINTS PROCESSED:        ' #TOTAL-DETAIL
        if (Global.isEscape()) return;
        getReports().write(0, "--------------------------------------------------------------");                                                                          //Natural: WRITE '--------------------------------------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL IA RECORDS FROM ENROLLMENTS/REPRINT(NEW):   ",pnd_Mdm);                                                                              //Natural: WRITE 'TOTAL IA RECORDS FROM ENROLLMENTS/REPRINT(NEW):   ' #MDM
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL IA RECORDS FROM ENROLLMENTS/REPRINT(OLD):   ",pnd_Non_Mdm);                                                                          //Natural: WRITE 'TOTAL IA RECORDS FROM ENROLLMENTS/REPRINT(OLD):   ' #NON-MDM
        if (Global.isEscape()) return;
        getReports().write(0, "--------------------------------------------------------------");                                                                          //Natural: WRITE '--------------------------------------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL DETAIL RECORDS SENT TO CPI:                 ",pnd_Mq_Detail);                                                                        //Natural: WRITE 'TOTAL DETAIL RECORDS SENT TO CPI:                 ' #MQ-DETAIL
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL RECORDS (DETAIL + SUMMARY) SENT TO CPI:     ",pnd_Mq);                                                                               //Natural: WRITE 'TOTAL RECORDS (DETAIL + SUMMARY) SENT TO CPI:     ' #MQ
        if (Global.isEscape()) return;
        getReports().write(0, "--------------------------------------------------------------");                                                                          //Natural: WRITE '--------------------------------------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "SUMMARY RECORD CREATED:                           ",pnd_Summary);                                                                          //Natural: WRITE 'SUMMARY RECORD CREATED:                           ' #SUMMARY
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL OF RECORDS WRITTEN TO OUTPUT FILE:          ",pnd_Output);                                                                           //Natural: WRITE 'TOTAL OF RECORDS WRITTEN TO OUTPUT FILE:          ' #OUTPUT
        if (Global.isEscape()) return;
        getReports().write(0, "--------------------------------------------------------------");                                                                          //Natural: WRITE '--------------------------------------------------------------'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL OF ALL MQ ERRORS - RECORDS NOT SENT TO CPI: ",pnd_Mq_Error);                                                                         //Natural: WRITE 'TOTAL OF ALL MQ ERRORS - RECORDS NOT SENT TO CPI: ' #MQ-ERROR
        if (Global.isEscape()) return;
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-PROCESS
    }
    private void sub_Ia_Process() throws Exception                                                                                                                        //Natural: IA-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        if (condition(cis_Prtcpnt_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                                       //Natural: IF CIS-SG-TEXT-UDF-3 = 'MDM'
        {
            pnd_Mdm.nadd(1);                                                                                                                                              //Natural: ASSIGN #MDM := #MDM + 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Non_Mdm.nadd(1);                                                                                                                                          //Natural: ASSIGN #NON-MDM := #NON-MDM + 1
            //*  CONVERT ORIG-STATE-CODE TO ALPHA & GET STATE NAME
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 62
        for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(62)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
        {
            if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(cis_Prtcpnt_Cis_Orig_Issue_State))) //Natural: IF #STATE-CODE-N ( #I ) = CIS-PRTCPNT.CIS-ORIG-ISSUE-STATE
            {
                cis_Prtcpnt_Cis_Orig_Issue_State.setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));         //Natural: MOVE #STATE-CODE-A ( #I ) TO CIS-PRTCPNT.CIS-ORIG-ISSUE-STATE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CONVERT STATE-CODE TO ALPHA
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 62
        for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(62)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
        {
            if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(cis_Prtcpnt_Cis_Issue_State_Cd)))  //Natural: IF #STATE-CODE-N ( #I ) = CIS-PRTCPNT.CIS-ISSUE-STATE-CD
            {
                cis_Prtcpnt_Cis_Issue_State_Cd.setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));           //Natural: MOVE #STATE-CODE-A ( #I ) TO CIS-PRTCPNT.CIS-ISSUE-STATE-CD
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I
        pnd_Output_File.reset();                                                                                                                                          //Natural: RESET #OUTPUT-FILE #OUTPUT-FILE2 #I-PDA-DATA-A #CISA0050 #CIS-DA-OCCURRENCE #CIS-CREF-OCCURRENCE #CIS-TACC-OCCURRENCE #CIS-COMUT-OCCURRENCE #CIS-COMUT-2-OCCURRENCE #CIS-BENE-OCCURRENCE #CIS-TIAA-CNTRCT-NBR-INPUT ( * )
        pnd_Output_File2.reset();
        pdaCisa0050.getPnd_Cisa0050_Pnd_I_Pda_Data_A().reset();
        pdaCisa0050.getPnd_Cisa0050().reset();
        pnd_Cis_Da_Occurrence.reset();
        pnd_Cis_Cref_Occurrence.reset();
        pnd_Cis_Tacc_Occurrence.reset();
        pnd_Cis_Comut_Occurrence.reset();
        pnd_Cis_Comut_2_Occurrence.reset();
        pnd_Cis_Bene_Occurrence.reset();
        pnd_Cis_Tiaa_Cntrct_Nbr_Input.getValue("*").reset();
        pdaCisa0050.getPnd_Cisa0050_Pnd_Business_Process_Date().setValue(pnd_Todays_Business_Dt);                                                                         //Natural: ASSIGN #BUSINESS-PROCESS-DATE := #TODAYS-BUSINESS-DT
        if (condition(cis_Prtcpnt_Cis_Rqst_Id.equals("IA") && cis_Prtcpnt_Cis_Trnsf_Flag.equals("N")))                                                                    //Natural: IF CIS-PRTCPNT.CIS-RQST-ID = 'IA' AND CIS-PRTCPNT.CIS-TRNSF-FLAG EQ 'N'
        {
            if (condition(cis_Prtcpnt_Cis_Cntrct_Apprvl_Ind.equals("Y") || cis_Prtcpnt_Cis_Cntrct_Apprvl_Ind.equals("N")))                                                //Natural: IF CIS-PRTCPNT.CIS-CNTRCT-APPRVL-IND = 'Y' OR = 'N'
            {
                short decideConditionsMet2093 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF CIS-PRTCPNT.CIS-CNTRCT-APPRVL-IND;//Natural: VALUE 'Y'
                if (condition((cis_Prtcpnt_Cis_Cntrct_Apprvl_Ind.equals("Y"))))
                {
                    decideConditionsMet2093++;
                    cis_Prtcpnt_Cis_Tiaa_Cntrct_Type.setValue("A");                                                                                                       //Natural: MOVE 'A' TO CIS-PRTCPNT.CIS-TIAA-CNTRCT-TYPE
                    cis_Prtcpnt_Cis_Cref_Cntrct_Type.setValue("A");                                                                                                       //Natural: MOVE 'A' TO CIS-PRTCPNT.CIS-CREF-CNTRCT-TYPE
                    cis_Prtcpnt_Cis_Rea_Cntrct_Type.setValue("A");                                                                                                        //Natural: MOVE 'A' TO CIS-PRTCPNT.CIS-REA-CNTRCT-TYPE
                }                                                                                                                                                         //Natural: VALUE 'N'
                else if (condition((cis_Prtcpnt_Cis_Cntrct_Apprvl_Ind.equals("N"))))
                {
                    decideConditionsMet2093++;
                    cis_Prtcpnt_Cis_Tiaa_Cntrct_Type.setValue("S");                                                                                                       //Natural: MOVE 'S' TO CIS-PRTCPNT.CIS-TIAA-CNTRCT-TYPE
                    cis_Prtcpnt_Cis_Cref_Cntrct_Type.setValue("S");                                                                                                       //Natural: MOVE 'S' TO CIS-PRTCPNT.CIS-CREF-CNTRCT-TYPE
                    cis_Prtcpnt_Cis_Rea_Cntrct_Type.setValue("A");                                                                                                        //Natural: MOVE 'A' TO CIS-PRTCPNT.CIS-REA-CNTRCT-TYPE
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input.getValue(1).setValue(cis_Prtcpnt_Cis_Issue_State_Cd);                                                   //Natural: MOVE CIS-PRTCPNT.CIS-ISSUE-STATE-CD TO #RES-STATE-INPUT ( 1 )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(cis_Prtcpnt_Cis_Orig_Issue_State);                                                //Natural: MOVE CIS-PRTCPNT.CIS-ORIG-ISSUE-STATE TO #FROM-STATE-INPUT ( 1 )
                pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*")); //Natural: MOVE #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * ) TO CISA1000.#CIS-TIAA-CNTRCT-NBR ( * )
                pdaCisa1000.getCisa1000_Pnd_Cis_Requestor().setValue("IA");                                                                                               //Natural: ASSIGN CISA1000.#CIS-REQUESTOR := 'IA'
                pdaCisa1000.getCisa1000_Pnd_Cis_Assign_Issue_Cntrct_Ind().setValue("T");                                                                                  //Natural: ASSIGN CISA1000.#CIS-ASSIGN-ISSUE-CNTRCT-IND := 'T'
                pnd_Tiaa_Date_N_Pnd_Tiaa_Date_A.setValueEdited(cis_Prtcpnt_Cis_Tiaa_Doi,new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED CIS-TIAA-DOI ( EM = YYYYMMDD ) TO #TIAA-DATE-A
                pdaCisa1000.getCisa1000_Pnd_Cis_Issue_Dt().setValue(pnd_Tiaa_Date_N);                                                                                     //Natural: ASSIGN CISA1000.#CIS-ISSUE-DT := #TIAA-DATE-N
                pdaCisa1000.getCisa1000_Pnd_Cis_Functions().getValue(10).setValue("YO");                                                                                  //Natural: ASSIGN CISA1000.#CIS-FUNCTIONS ( 10 ) := 'YO'
                DbsUtil.callnat(Cisn1000.class , getCurrentProcessState(), pdaCisa1000.getCisa1000(), pdaCisa1000.getPnd_Cis_Misc());                                     //Natural: CALLNAT 'CISN1000' CISA1000 #CIS-MISC
                if (condition(Global.isEscape())) return;
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*").setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*")); //Natural: MOVE CISA1000.#CIS-TIAA-CNTRCT-NBR ( * ) TO #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                cis_Prtcpnt_Cis_Tiaa_Cntrct_Type.setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1));                                         //Natural: MOVE #FROM-APPROVAL-IND-INPUT ( 1 ) TO CIS-PRTCPNT.CIS-TIAA-CNTRCT-TYPE
                cis_Prtcpnt_Cis_Cref_Cntrct_Type.setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2));                                         //Natural: MOVE #FROM-APPROVAL-IND-INPUT ( 2 ) TO CIS-PRTCPNT.CIS-CREF-CNTRCT-TYPE
                cis_Prtcpnt_Cis_Rea_Cntrct_Type.setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input.getValue(3));                                           //Natural: MOVE #RES-APPROVAL-IND-INPUT ( 3 ) TO CIS-PRTCPNT.CIS-REA-CNTRCT-TYPE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Prtcpnt_Cis_Rqst_Id.equals("IA") && cis_Prtcpnt_Cis_Trnsf_Flag.equals("Y")))                                                                    //Natural: IF CIS-PRTCPNT.CIS-RQST-ID = 'IA' AND CIS-PRTCPNT.CIS-TRNSF-FLAG EQ 'Y'
        {
            if (condition(cis_Prtcpnt_Cis_Tiaa_Cntrct_Type.equals("C")))                                                                                                  //Natural: IF CIS-PRTCPNT.CIS-TIAA-CNTRCT-TYPE = 'C'
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input.getValue(1).setValue(cis_Prtcpnt_Cis_Issue_State_Cd);                                                   //Natural: MOVE CIS-PRTCPNT.CIS-ISSUE-STATE-CD TO #RES-STATE-INPUT ( 1 )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(cis_Prtcpnt_Cis_Orig_Issue_State);                                                //Natural: MOVE CIS-PRTCPNT.CIS-ORIG-ISSUE-STATE TO #FROM-STATE-INPUT ( 1 )
                pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*")); //Natural: MOVE #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * ) TO CISA1000.#CIS-TIAA-CNTRCT-NBR ( * )
                pdaCisa1000.getCisa1000_Pnd_Cis_Requestor().setValue("IA");                                                                                               //Natural: ASSIGN CISA1000.#CIS-REQUESTOR := 'IA'
                pdaCisa1000.getCisa1000_Pnd_Cis_Assign_Issue_Cntrct_Ind().setValue("T");                                                                                  //Natural: ASSIGN CISA1000.#CIS-ASSIGN-ISSUE-CNTRCT-IND := 'T'
                pnd_Tiaa_Date_N_Pnd_Tiaa_Date_A.setValueEdited(cis_Prtcpnt_Cis_Tiaa_Doi,new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED CIS-TIAA-DOI ( EM = YYYYMMDD ) TO #TIAA-DATE-A
                pdaCisa1000.getCisa1000_Pnd_Cis_Issue_Dt().setValue(pnd_Tiaa_Date_N);                                                                                     //Natural: ASSIGN CISA1000.#CIS-ISSUE-DT := #TIAA-DATE-N
                pdaCisa1000.getCisa1000_Pnd_Cis_Functions().getValue(10).setValue("YO");                                                                                  //Natural: ASSIGN CISA1000.#CIS-FUNCTIONS ( 10 ) := 'YO'
                DbsUtil.callnat(Cisn1000.class , getCurrentProcessState(), pdaCisa1000.getCisa1000(), pdaCisa1000.getPnd_Cis_Misc());                                     //Natural: CALLNAT 'CISN1000' CISA1000 #CIS-MISC
                if (condition(Global.isEscape())) return;
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*").setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*")); //Natural: MOVE CISA1000.#CIS-TIAA-CNTRCT-NBR ( * ) TO #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                cis_Prtcpnt_Cis_Tiaa_Cntrct_Type.setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1));                                         //Natural: MOVE #FROM-APPROVAL-IND-INPUT ( 1 ) TO CIS-PRTCPNT.CIS-TIAA-CNTRCT-TYPE
                cis_Prtcpnt_Cis_Rea_Cntrct_Type.setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input.getValue(3));                                           //Natural: MOVE #RES-APPROVAL-IND-INPUT ( 3 ) TO CIS-PRTCPNT.CIS-REA-CNTRCT-TYPE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Prtcpnt_Cis_Zip_Code.getValue("*").equals("FORGN") || cis_Prtcpnt_Cis_Zip_Code.getValue("*").equals("CANAD")))                                  //Natural: IF CIS-PRTCPNT.CIS-ZIP-CODE ( * ) = 'FORGN' OR = 'CANAD'
        {
            cis_Prtcpnt_Cis_Pull_Code.setValue("SPEC");                                                                                                                   //Natural: ASSIGN CIS-PRTCPNT.CIS-PULL-CODE := 'SPEC'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cis_Prtcpnt_Cis_Tiaa_Cntrct_Type.equals("S") || cis_Prtcpnt_Cis_Cref_Cntrct_Type.equals("S")))                                                  //Natural: IF CIS-PRTCPNT.CIS-TIAA-CNTRCT-TYPE = 'S' OR CIS-PRTCPNT.CIS-CREF-CNTRCT-TYPE = 'S'
            {
                cis_Prtcpnt_Cis_Pull_Code.setValue("SPEC");                                                                                                               //Natural: ASSIGN CIS-PRTCPNT.CIS-PULL-CODE := 'SPEC'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  #CIS-REA-ANNTY-MNTHLY-AMT
        pnd_Output_File_Pnd_Output_File_R.setValuesByName(vw_cis_Prtcpnt);                                                                                                //Natural: MOVE BY NAME CIS-PRTCPNT TO #OUTPUT-FILE-R
        pdaCisa0050.getPnd_Cisa0050_Pnd_Cis_Rqst_Id_Key().setValue(cis_Prtcpnt_Cis_Rqst_Id_Key);                                                                          //Natural: ASSIGN #CIS-RQST-ID-KEY := CIS-RQST-ID-KEY
        pnd_Output_File_Pnd_Cis_Access_Ind.setValue(cis_Prtcpnt_Cis_Tacc_Ind.getValue(1));                                                                                //Natural: ASSIGN #CIS-ACCESS-IND := CIS-TACC-IND ( 1 )
        pnd_Output_File_Pnd_Cis_Atra_Ind.setValue(cis_Prtcpnt_Cis_Sg_Text_Udf_1.getSubstring(1,1));                                                                       //Natural: ASSIGN #CIS-ATRA-IND := SUBSTR ( CIS-SG-TEXT-UDF-1,1,1 )
        pnd_Output_File_Pnd_Cis_Atra_Issue_Dte.setValue(cis_Prtcpnt_Cis_Sg_Text_Udf_1.getSubstring(2,9));                                                                 //Natural: ASSIGN #CIS-ATRA-ISSUE-DTE := SUBSTR ( CIS-SG-TEXT-UDF-1,2,9 )
        pnd_Output_File_Pnd_Cis_Print_Ind.setValue(pnd_Print_Ind);                                                                                                        //Natural: ASSIGN #CIS-PRINT-IND := #PRINT-IND
        pnd_Output_File_Pnd_Cis_Rtb_Amt.setValue(cis_Prtcpnt_Cis_Rtb_Amt);                                                                                                //Natural: ASSIGN #CIS-RTB-AMT := CIS-RTB-AMT
        if (condition(cis_Prtcpnt_Cis_Tiaa_Doi.greater(getZero())))                                                                                                       //Natural: IF CIS-TIAA-DOI > 0
        {
            pnd_Output_File_Pnd_Cis_Tiaa_Doi.setValueEdited(cis_Prtcpnt_Cis_Tiaa_Doi,new ReportEditMask("YYYYMMDD"));                                                     //Natural: MOVE EDITED CIS-TIAA-DOI ( EM = YYYYMMDD ) TO #CIS-TIAA-DOI
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Prtcpnt_Cis_Cref_Doi.greater(getZero())))                                                                                                       //Natural: IF CIS-CREF-DOI > 0
        {
            pnd_Output_File_Pnd_Cis_Cref_Doi.setValueEdited(cis_Prtcpnt_Cis_Cref_Doi,new ReportEditMask("YYYYMMDD"));                                                     //Natural: MOVE EDITED CIS-CREF-DOI ( EM = YYYYMMDD ) TO #CIS-CREF-DOI
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Prtcpnt_Cis_Frst_Annt_Dob.greater(getZero())))                                                                                                  //Natural: IF CIS-FRST-ANNT-DOB > 0
        {
            pnd_Output_File_Pnd_Cis_Frst_Annt_Dob.setValueEdited(cis_Prtcpnt_Cis_Frst_Annt_Dob,new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED CIS-FRST-ANNT-DOB ( EM = YYYYMMDD ) TO #CIS-FRST-ANNT-DOB
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Prtcpnt_Cis_Annty_Start_Dte.greater(getZero())))                                                                                                //Natural: IF CIS-ANNTY-START-DTE > 0
        {
            pnd_Output_File_Pnd_Cis_Annty_Start_Dte.setValueEdited(cis_Prtcpnt_Cis_Annty_Start_Dte,new ReportEditMask("YYYYMMDD"));                                       //Natural: MOVE EDITED CIS-ANNTY-START-DTE ( EM = YYYYMMDD ) TO #CIS-ANNTY-START-DTE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Prtcpnt_Cis_Annty_End_Dte.greater(getZero())))                                                                                                  //Natural: IF CIS-ANNTY-END-DTE > 0
        {
            pnd_Output_File_Pnd_Cis_Annty_End_Dte.setValueEdited(cis_Prtcpnt_Cis_Annty_End_Dte,new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED CIS-ANNTY-END-DTE ( EM = YYYYMMDD ) TO #CIS-ANNTY-END-DTE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Prtcpnt_Cis_Scnd_Annt_Dob.greater(getZero())))                                                                                                  //Natural: IF CIS-SCND-ANNT-DOB > 0
        {
            pnd_Output_File_Pnd_Cis_Scnd_Annt_Dob.setValueEdited(cis_Prtcpnt_Cis_Scnd_Annt_Dob,new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED CIS-SCND-ANNT-DOB ( EM = YYYYMMDD ) TO #CIS-SCND-ANNT-DOB
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Prtcpnt_Cis_Scnd_Annt_Frst_Nme.greater(" ") || cis_Prtcpnt_Cis_Scnd_Annt_Lst_Nme.greater(" ")))                                                 //Natural: IF CIS-SCND-ANNT-FRST-NME GT ' ' OR CIS-SCND-ANNT-LST-NME GT ' '
        {
            DbsUtil.examine(new ExamineSource(cis_Prtcpnt_Cis_Scnd_Annt_Frst_Nme,2,29), new ExamineTranslate(TranslateOption.Lower));                                     //Natural: EXAMINE SUBSTRING ( CIS-SCND-ANNT-FRST-NME,2,29 ) TRANSLATE INTO LOWER
            pnd_First_Name_S.setValue(cis_Prtcpnt_Cis_Scnd_Annt_Frst_Nme);                                                                                                //Natural: MOVE CIS-SCND-ANNT-FRST-NME TO #FIRST-NAME-S
            pnd_Filler_S1.setValue("�");                                                                                                                                  //Natural: MOVE '�' TO #FILLER-S1
            DbsUtil.examine(new ExamineSource(cis_Prtcpnt_Cis_Scnd_Annt_Mid_Nme,2,29), new ExamineTranslate(TranslateOption.Lower));                                      //Natural: EXAMINE SUBSTRING ( CIS-SCND-ANNT-MID-NME,2,29 ) TRANSLATE INTO LOWER
            pnd_Mid_Name_S.setValue(cis_Prtcpnt_Cis_Scnd_Annt_Mid_Nme);                                                                                                   //Natural: MOVE CIS-SCND-ANNT-MID-NME TO #MID-NAME-S
            pnd_Filler_S2.setValue("�");                                                                                                                                  //Natural: MOVE '�' TO #FILLER-S2
            DbsUtil.examine(new ExamineSource(cis_Prtcpnt_Cis_Scnd_Annt_Lst_Nme,2,29), new ExamineTranslate(TranslateOption.Lower));                                      //Natural: EXAMINE SUBSTRING ( CIS-SCND-ANNT-LST-NME,2,29 ) TRANSLATE INTO LOWER
            pnd_Last_Name_S.setValue(cis_Prtcpnt_Cis_Scnd_Annt_Lst_Nme);                                                                                                  //Natural: MOVE CIS-SCND-ANNT-LST-NME TO #LAST-NAME-S
            pnd_Output_File_Pnd_Cis_Scnd_Annt_Full_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_First_Name_S, pnd_Filler_S1, pnd_Mid_Name_S,         //Natural: COMPRESS #FIRST-NAME-S #FILLER-S1 #MID-NAME-S #FILLER-S2 #LAST-NAME-S TO #CIS-SCND-ANNT-FULL-NAME LEAVING NO SPACE
                pnd_Filler_S2, pnd_Last_Name_S));
            if (condition(cis_Prtcpnt_Cis_Rqst_Id.equals("IA") && cis_Prtcpnt_Cis_Trnsf_Flag.equals("Y") && cis_Prtcpnt_Cis_Ownership_Cd.equals(1)))                      //Natural: IF CIS-PRTCPNT.CIS-RQST-ID = 'IA' AND CIS-PRTCPNT.CIS-TRNSF-FLAG = 'Y' AND CIS-PRTCPNT.CIS-OWNERSHIP-CD = 1
            {
                pnd_Output_File_Pnd_Cis_Scnd_Annt_Full_Name.setValue(DbsUtil.compress(pnd_Output_File_Pnd_Cis_Scnd_Annt_Full_Name, "- Deceased"));                        //Natural: COMPRESS #CIS-SCND-ANNT-FULL-NAME '- Deceased' INTO #CIS-SCND-ANNT-FULL-NAME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(20)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
        {
            if (condition(cis_Prtcpnt_Cis_Da_Tiaa_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(" ") && cis_Prtcpnt_Cis_Da_Cert_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(" "))) //Natural: IF CIS-DA-TIAA-NBR ( #I ) = ' ' AND CIS-DA-CERT-NBR ( #I ) = ' '
            {
                if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(1)))                                                                                           //Natural: IF #I = 1
                {
                    pnd_Cis_Da_Occurrence.setValue(0);                                                                                                                    //Natural: ASSIGN #CIS-DA-OCCURRENCE := 0
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_File_Pnd_Cis_Da_Tiaa_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Da_Tiaa_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-DA-TIAA-NBR ( #I ) := CIS-DA-TIAA-NBR ( #I )
            pnd_Output_File_Pnd_Cis_Da_Cert_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Da_Cert_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-DA-CERT-NBR ( #I ) := CIS-DA-CERT-NBR ( #I )
            pnd_Output_File_Pnd_Cis_Da_Rea_Proceeds_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Da_Rea_Proceeds_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-DA-REA-PROCEEDS-AMT ( #I ) := CIS-DA-REA-PROCEEDS-AMT ( #I )
            pnd_Output_File_Pnd_Cis_Da_Tiaa_Proceeds_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Da_Tiaa_Proceeds_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-DA-TIAA-PROCEEDS-AMT ( #I ) := CIS-DA-TIAA-PROCEEDS-AMT ( #I )
            pnd_Output_File_Pnd_Cis_Da_Cref_Proceeds_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Da_Cref_Proceeds_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-DA-CREF-PROCEEDS-AMT ( #I ) := CIS-DA-CREF-PROCEEDS-AMT ( #I )
            pnd_Cis_Da_Occurrence.setValue(ldaAppl170.getPnd_Work_Fields_Pnd_I());                                                                                        //Natural: ASSIGN #CIS-DA-OCCURRENCE := #I
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(20)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
        {
            if (condition(cis_Prtcpnt_Cis_Cref_Acct_Cde.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(" ") && cis_Prtcpnt_Cis_Sg_Fund_Ticker.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(" "))) //Natural: IF CIS-CREF-ACCT-CDE ( #I ) = ' ' AND CIS-SG-FUND-TICKER ( #I ) = ' '
            {
                if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(1)))                                                                                           //Natural: IF #I = 1
                {
                    pnd_Cis_Cref_Occurrence.setValue(0);                                                                                                                  //Natural: ASSIGN #CIS-CREF-OCCURRENCE := 0
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_File_Pnd_Cis_Sg_Fund_Ticker.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Sg_Fund_Ticker.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-SG-FUND-TICKER ( #I ) := CIS-SG-FUND-TICKER ( #I )
            pnd_Output_File_Pnd_Cis_Cref_Acct_Cde.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Cref_Acct_Cde.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-CREF-ACCT-CDE ( #I ) := CIS-CREF-ACCT-CDE ( #I )
            pnd_Output_File_Pnd_Cis_Cref_Mnthly_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Cref_Mnthly_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-CREF-MNTHLY-NBR-UNITS ( #I ) := CIS-CREF-MNTHLY-NBR-UNITS ( #I )
            pnd_Output_File_Pnd_Cis_Cref_Annual_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Cref_Annual_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-CREF-ANNUAL-NBR-UNITS ( #I ) := CIS-CREF-ANNUAL-NBR-UNITS ( #I )
            pnd_Output_File_Pnd_Cis_Cref_Annty_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Cref_Annty_Amt);                              //Natural: ASSIGN #CIS-CREF-ANNTY-AMT ( #I ) := CIS-CREF-ANNTY-AMT
            pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Table_Cde_Super1.setValue("FND");                                                                                              //Natural: ASSIGN #NEC-FND-TABLE-CDE-SUPER1 := 'FND'
            pnd_Nec_Fnd_Super1_Pnd_Nec_Fnd_Ticker_Symbol_Super1.setValue(pnd_Output_File_Pnd_Cis_Sg_Fund_Ticker.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));         //Natural: ASSIGN #NEC-FND-TICKER-SYMBOL-SUPER1 := #CIS-SG-FUND-TICKER ( #I )
            vw_nec_Fnd_View1.startDatabaseFind                                                                                                                            //Natural: FIND ( 1 ) NEC-FND-VIEW1 WITH NEC-FND-VIEW1.NEC-FND-SUPER1 = #NEC-FND-SUPER1
            (
            "FIND02",
            new Wc[] { new Wc("NEC_FND_SUPER1", "=", pnd_Nec_Fnd_Super1, WcType.WITH) },
            1
            );
            FIND02:
            while (condition(vw_nec_Fnd_View1.readNextRow("FIND02", true)))
            {
                vw_nec_Fnd_View1.setIfNotFoundControlFlag(false);
                if (condition(vw_nec_Fnd_View1.getAstCOUNTER().equals(0)))                                                                                                //Natural: IF NO RECORD FOUND
                {
                    pnd_Output_File_Pnd_Cis_Nec_Fund_Nme.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).reset();                                                         //Natural: RESET #CIS-NEC-FUND-NME ( #I )
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Output_File_Pnd_Cis_Nec_Fund_Nme.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(nec_Fnd_View1_Nec_Fund_Nme);                                //Natural: ASSIGN #CIS-NEC-FUND-NME ( #I ) := NEC-FND-VIEW1.NEC-FUND-NME
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            DbsUtil.examine(new ExamineSource(pnd_Output_File_Pnd_Cis_Nec_Fund_Nme.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())), new ExamineSearch(" "),              //Natural: EXAMINE #CIS-NEC-FUND-NME ( #I ) FOR ' ' REPLACE '�'
                new ExamineReplace("�"));
            pnd_Output_File_Pnd_Cis_Cref_Mnthly_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).reset();                                                              //Natural: RESET #CIS-CREF-MNTHLY-AMT ( #I )
            pnd_Cis_Cref_Occurrence.setValue(ldaAppl170.getPnd_Work_Fields_Pnd_I());                                                                                      //Natural: ASSIGN #CIS-CREF-OCCURRENCE := #I
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(20)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
        {
            if (condition(cis_Prtcpnt_Cis_Tacc_Account.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(" ")))                                                      //Natural: IF CIS-TACC-ACCOUNT ( #I ) = ' '
            {
                if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(1)))                                                                                           //Natural: IF #I = 1
                {
                    pnd_Cis_Tacc_Occurrence.setValue(0);                                                                                                                  //Natural: ASSIGN #CIS-TACC-OCCURRENCE := 0
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pdaNeca4000.getNeca4000().reset();                                                                                                                            //Natural: RESET NECA4000 REQUEST-IND
            pdaNeca4000.getNeca4000_Request_Ind().reset();
            pdaNeca4000.getNeca4000_Function_Cde().setValue("FDS");                                                                                                       //Natural: ASSIGN FUNCTION-CDE := 'FDS'
            pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                 //Natural: ASSIGN INPT-KEY-OPTION-CDE := '01'
            pdaNeca4000.getNeca4000_Fds_Key_Ticker_Symbol().setValue(cis_Prtcpnt_Cis_Tacc_Account.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));                       //Natural: ASSIGN FDS-KEY-TICKER-SYMBOL := CIS-TACC-ACCOUNT ( #I )
            pdaNeca4000.getNeca4000_Fds_Key_Fund_Name_Ref_Cde().setValue("18");                                                                                           //Natural: ASSIGN FDS-KEY-FUND-NAME-REF-CDE := '18'
            DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                        //Natural: CALLNAT 'NECN4000' NECA4000
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Output_File_Pnd_Cis_Cref_Doi_N.less(20150424)))                                                                                             //Natural: IF #CIS-CREF-DOI-N < 20150424
            {
                DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1)), new ExamineSearch(" R1 "), new ExamineReplace("   "));             //Natural: EXAMINE NECA4000.FDS-FUND-NME ( 1 ) FOR ' R1 ' REPLACE WITH '   '
                DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1)), new ExamineSearch(" R2 "), new ExamineReplace("   "));             //Natural: EXAMINE NECA4000.FDS-FUND-NME ( 1 ) FOR ' R2 ' REPLACE WITH '   '
                DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1)), new ExamineSearch(" R3 "), new ExamineReplace("   "));             //Natural: EXAMINE NECA4000.FDS-FUND-NME ( 1 ) FOR ' R3 ' REPLACE WITH '   '
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_File_Pnd_Cis_Tacc_Account_Name.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1));       //Natural: ASSIGN #CIS-TACC-ACCOUNT-NAME ( #I ) := NECA4000.FDS-FUND-NME ( 1 )
            pnd_Output_File_Pnd_Cis_Tacc_Account.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Tacc_Account.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-TACC-ACCOUNT ( #I ) := CIS-TACC-ACCOUNT ( #I )
            pnd_Output_File_Pnd_Cis_Tacc_Mnthly_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Tacc_Mnthly_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-TACC-MNTHLY-NBR-UNITS ( #I ) := CIS-TACC-MNTHLY-NBR-UNITS ( #I )
            pnd_Output_File_Pnd_Cis_Tacc_Annual_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Tacc_Annual_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-TACC-ANNUAL-NBR-UNITS ( #I ) := CIS-TACC-ANNUAL-NBR-UNITS ( #I )
            pnd_Output_File_Pnd_Cis_Tacc_Annty_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Tacc_Annty_Amt);                              //Natural: ASSIGN #CIS-TACC-ANNTY-AMT ( #I ) := CIS-TACC-ANNTY-AMT
            pnd_Cis_Tacc_Occurrence.setValue(ldaAppl170.getPnd_Work_Fields_Pnd_I());                                                                                      //Natural: ASSIGN #CIS-TACC-OCCURRENCE := #I
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I
        FOR07:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(5)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
        {
            if (condition(cis_Prtcpnt_Cis_Comut_Grnted_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(getZero())))                                            //Natural: IF CIS-COMUT-GRNTED-AMT ( #I ) = 0
            {
                if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(1)))                                                                                           //Natural: IF #I = 1
                {
                    pnd_Cis_Comut_Occurrence.setValue(0);                                                                                                                 //Natural: ASSIGN #CIS-COMUT-OCCURRENCE := 0
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_File_Pnd_Cis_Comut_Grnted_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Comut_Grnted_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-COMUT-GRNTED-AMT ( #I ) := CIS-COMUT-GRNTED-AMT ( #I )
            pnd_Output_File_Pnd_Cis_Comut_Int_Rate.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Comut_Int_Rate.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-COMUT-INT-RATE ( #I ) := CIS-COMUT-INT-RATE ( #I )
            pnd_Output_File_Pnd_Cis_Comut_Pymt_Method.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Prtcpnt_Cis_Comut_Pymt_Method.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-COMUT-PYMT-METHOD ( #I ) := CIS-COMUT-PYMT-METHOD ( #I )
            pnd_Cis_Comut_Occurrence.setValue(ldaAppl170.getPnd_Work_Fields_Pnd_I());                                                                                     //Natural: ASSIGN #CIS-COMUT-OCCURRENCE := #I
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I #I3 #CIS-COMUT-2-OCCURRENCE
        pnd_I3.reset();
        pnd_Cis_Comut_2_Occurrence.reset();
        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(20)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
        {
            if (condition(cis_Prtcpnt_Cis_Comut_Grnted_Amt_2.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(getZero())))                                          //Natural: IF CIS-COMUT-GRNTED-AMT-2 ( #I ) = 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_I3.nadd(1);                                                                                                                                               //Natural: ASSIGN #I3 := #I3 + 1
            pnd_Output_File_Pnd_Cis_Comut_Grnted_Amt_2.getValue(pnd_I3).setValue(cis_Prtcpnt_Cis_Comut_Grnted_Amt_2.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));     //Natural: ASSIGN #CIS-COMUT-GRNTED-AMT-2 ( #I3 ) := CIS-COMUT-GRNTED-AMT-2 ( #I )
            pnd_Output_File_Pnd_Cis_Comut_Int_Rate_2.getValue(pnd_I3).setValue(cis_Prtcpnt_Cis_Comut_Int_Rate_2.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));         //Natural: ASSIGN #CIS-COMUT-INT-RATE-2 ( #I3 ) := CIS-COMUT-INT-RATE-2 ( #I )
            pnd_Output_File_Pnd_Cis_Comut_Pymt_Method_2.getValue(pnd_I3).setValue(cis_Prtcpnt_Cis_Comut_Pymt_Method_2.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));   //Natural: ASSIGN #CIS-COMUT-PYMT-METHOD-2 ( #I3 ) := CIS-COMUT-PYMT-METHOD-2 ( #I )
            pnd_Output_File_Pnd_Cis_Comut_Mortality_Basis.getValue(pnd_I3).setValue(cis_Prtcpnt_Cis_Comut_Mortality_Basis.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-COMUT-MORTALITY-BASIS ( #I3 ) := CIS-COMUT-MORTALITY-BASIS ( #I )
            pnd_Cis_Comut_2_Occurrence.setValue(pnd_I3);                                                                                                                  //Natural: ASSIGN #CIS-COMUT-2-OCCURRENCE := #I3
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Cis_Ben_Super_2_Pnd_Cis_Bene_Tiaa_Nbr.reset();                                                                                                                //Natural: RESET #CIS-BENE-TIAA-NBR
        pnd_Cis_Ben_Super_2_Pnd_Cis_Bene_Tiaa_Nbr.setValue(cis_Prtcpnt_Cis_Tiaa_Nbr);                                                                                     //Natural: ASSIGN #CIS-BENE-TIAA-NBR := CIS-PRTCPNT.CIS-TIAA-NBR
        pnd_Cis_Ben_Super_2_Pnd_Cis_Rcrd_Type_Cde.setValue(1);                                                                                                            //Natural: ASSIGN #CIS-RCRD-TYPE-CDE := 1
        vw_cis_Bene_File.startDatabaseFind                                                                                                                                //Natural: FIND CIS-BENE-FILE WITH CIS-BEN-SUPER-2 = #CIS-BEN-SUPER-2
        (
        "FIND03",
        new Wc[] { new Wc("CIS_BEN_SUPER_2", "=", pnd_Cis_Ben_Super_2, WcType.WITH) }
        );
        FIND03:
        while (condition(vw_cis_Bene_File.readNextRow("FIND03", true)))
        {
            vw_cis_Bene_File.setIfNotFoundControlFlag(false);
            if (condition(vw_cis_Bene_File.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "KEY NOT FOUND ON CIS-BENE-FILE FOR:",pnd_Cis_Ben_Super_2);                                                                         //Natural: WRITE 'KEY NOT FOUND ON CIS-BENE-FILE FOR:' #CIS-BEN-SUPER-2
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Output_File_Pnd_Cis_Prmry_Bene_Child_Prvsn.setValue(cis_Bene_File_Cis_Prmry_Bene_Child_Prvsn);                                                            //Natural: ASSIGN #CIS-PRMRY-BENE-CHILD-PRVSN := CIS-PRMRY-BENE-CHILD-PRVSN
            ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                //Natural: RESET #I
            FOR09:                                                                                                                                                        //Natural: FOR #I = 1 TO 20
            for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(20)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
            {
                if (condition(cis_Bene_File_Cis_Prmry_Bene_Name.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(" ")))                                             //Natural: IF CIS-PRMRY-BENE-NAME ( #I ) = ' '
                {
                    if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(1)))                                                                                       //Natural: IF #I = 1
                    {
                        pnd_Cis_Bene_Occurrence.setValue(0);                                                                                                              //Natural: ASSIGN #CIS-BENE-OCCURRENCE := 0
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Output_File_Pnd_Cis_Bene_Type.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue("P");                                                          //Natural: ASSIGN #CIS-BENE-TYPE ( #I ) := 'P'
                pnd_Output_File_Pnd_Cis_Bene_Name.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Bene_File_Cis_Prmry_Bene_Name.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-BENE-NAME ( #I ) := CIS-PRMRY-BENE-NAME ( #I )
                pnd_Output_File_Pnd_Cis_Bene_Rltn.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Bene_File_Cis_Prmry_Bene_Rltn.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-BENE-RLTN ( #I ) := CIS-PRMRY-BENE-RLTN ( #I )
                pnd_Output_File_Pnd_Cis_Bene_Rltn_Cde.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Bene_File_Cis_Prmry_Bene_Rltn_Cde.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-BENE-RLTN-CDE ( #I ) := CIS-PRMRY-BENE-RLTN-CDE ( #I )
                pnd_Output_File_Pnd_Cis_Bene_Ssn_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Bene_File_Cis_Prmry_Bene_Ssn_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-BENE-SSN-NBR ( #I ) := CIS-PRMRY-BENE-SSN-NBR ( #I )
                pnd_Output_File_Pnd_Cis_Bene_Nmrtr_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Bene_File_Cis_Prmry_Bene_Nmrtr_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-BENE-NMRTR-NBR ( #I ) := CIS-PRMRY-BENE-NMRTR-NBR ( #I )
                pnd_Output_File_Pnd_Cis_Bene_Dnmntr_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(cis_Bene_File_Cis_Prmry_Bene_Dnmntr_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-BENE-DNMNTR-NBR ( #I ) := CIS-PRMRY-BENE-DNMNTR-NBR ( #I )
                if (condition(cis_Bene_File_Cis_Prmry_Bene_Dob.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).greater(getZero())))                                       //Natural: IF CIS-PRMRY-BENE-DOB ( #I ) > 0
                {
                    pnd_Output_File_Pnd_Cis_Bene_Dob.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValueEdited(cis_Bene_File_Cis_Prmry_Bene_Dob.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),new  //Natural: MOVE EDITED CIS-PRMRY-BENE-DOB ( #I ) ( EM = MM' 'DD' 'YYYY ) TO #CIS-BENE-DOB ( #I )
                        ReportEditMask("MM' 'DD' 'YYYY"));
                }                                                                                                                                                         //Natural: END-IF
                pnd_Output_File_Pnd_Cis_Bene_Spcl_Txt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(cis_Bene_File_Cis_Prmry_Bene_Spcl_Txt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I(), //Natural: COMPRESS CIS-PRMRY-BENE-SPCL-TXT ( #I,* ) INTO #CIS-BENE-SPCL-TXT ( #I )
                    "*")));
                DbsUtil.examine(new ExamineSource(pnd_Output_File_Pnd_Cis_Bene_Spcl_Txt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())), new ExamineSearch(" "),         //Natural: EXAMINE #CIS-BENE-SPCL-TXT ( #I ) FOR ' ' REPLACE '�'
                    new ExamineReplace("�"));
                pnd_Cis_Bene_Occurrence.setValue(ldaAppl170.getPnd_Work_Fields_Pnd_I());                                                                                  //Natural: ASSIGN #CIS-BENE-OCCURRENCE := #I
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_I2.compute(new ComputeParameters(false, pnd_I2), pnd_Cis_Bene_Occurrence.add(1));                                                                         //Natural: ASSIGN #I2 := #CIS-BENE-OCCURRENCE + 1
            ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                //Natural: RESET #I
            pnd_Output_File_Pnd_Cis_Cntgnt_Bene_Child_Prvsn.setValue(cis_Bene_File_Cis_Cntgnt_Bene_Child_Prvsn);                                                          //Natural: ASSIGN #CIS-CNTGNT-BENE-CHILD-PRVSN := CIS-CNTGNT-BENE-CHILD-PRVSN
            FOR10:                                                                                                                                                        //Natural: FOR #I = 1 TO 20
            for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(20)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
            {
                if (condition(cis_Bene_File_Cis_Cntgnt_Bene_Name.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals(" ")))                                            //Natural: IF CIS-CNTGNT-BENE-NAME ( #I ) = ' '
                {
                    if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(1) && pnd_Cis_Bene_Occurrence.equals(getZero())))                                          //Natural: IF #I = 1 AND #CIS-BENE-OCCURRENCE = 0
                    {
                        pnd_Cis_Bene_Occurrence.setValue(0);                                                                                                              //Natural: ASSIGN #CIS-BENE-OCCURRENCE := 0
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Output_File_Pnd_Cis_Bene_Type.getValue(pnd_I2).setValue("C");                                                                                         //Natural: ASSIGN #CIS-BENE-TYPE ( #I2 ) := 'C'
                pnd_Output_File_Pnd_Cis_Bene_Name.getValue(pnd_I2).setValue(cis_Bene_File_Cis_Cntgnt_Bene_Name.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));          //Natural: ASSIGN #CIS-BENE-NAME ( #I2 ) := CIS-CNTGNT-BENE-NAME ( #I )
                pnd_Output_File_Pnd_Cis_Bene_Rltn.getValue(pnd_I2).setValue(cis_Bene_File_Cis_Cntgnt_Bene_Rltn.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));          //Natural: ASSIGN #CIS-BENE-RLTN ( #I2 ) := CIS-CNTGNT-BENE-RLTN ( #I )
                pnd_Output_File_Pnd_Cis_Bene_Rltn_Cde.getValue(pnd_I2).setValue(cis_Bene_File_Cis_Cntgnt_Bene_Rltn_Cde.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));  //Natural: ASSIGN #CIS-BENE-RLTN-CDE ( #I2 ) := CIS-CNTGNT-BENE-RLTN-CDE ( #I )
                pnd_Output_File_Pnd_Cis_Bene_Ssn_Nbr.getValue(pnd_I2).setValue(cis_Bene_File_Cis_Cntgnt_Bene_Ssn_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));    //Natural: ASSIGN #CIS-BENE-SSN-NBR ( #I2 ) := CIS-CNTGNT-BENE-SSN-NBR ( #I )
                pnd_Output_File_Pnd_Cis_Bene_Nmrtr_Nbr.getValue(pnd_I2).setValue(cis_Bene_File_Cis_Cntgnt_Bene_Nmrtr_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-BENE-NMRTR-NBR ( #I2 ) := CIS-CNTGNT-BENE-NMRTR-NBR ( #I )
                pnd_Output_File_Pnd_Cis_Bene_Dnmntr_Nbr.getValue(pnd_I2).setValue(cis_Bene_File_Cis_Cntgnt_Bene_Dnmntr_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-BENE-DNMNTR-NBR ( #I2 ) := CIS-CNTGNT-BENE-DNMNTR-NBR ( #I )
                if (condition(cis_Bene_File_Cis_Cntgnt_Bene_Dob.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).greater(getZero())))                                      //Natural: IF CIS-CNTGNT-BENE-DOB ( #I ) > 0
                {
                    pnd_Output_File_Pnd_Cis_Bene_Dob.getValue(pnd_I2).setValueEdited(cis_Bene_File_Cis_Cntgnt_Bene_Dob.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),new  //Natural: MOVE EDITED CIS-CNTGNT-BENE-DOB ( #I ) ( EM = MM' 'DD' 'YYYY ) TO #CIS-BENE-DOB ( #I2 )
                        ReportEditMask("MM' 'DD' 'YYYY"));
                }                                                                                                                                                         //Natural: END-IF
                pnd_Output_File_Pnd_Cis_Bene_Spcl_Txt.getValue(pnd_I2).setValue(DbsUtil.compress(cis_Bene_File_Cis_Cntgnt_Bene_Spcl_Txt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I(), //Natural: COMPRESS CIS-CNTGNT-BENE-SPCL-TXT ( #I,* ) INTO #CIS-BENE-SPCL-TXT ( #I2 )
                    "*")));
                DbsUtil.examine(new ExamineSource(pnd_Output_File_Pnd_Cis_Bene_Spcl_Txt.getValue(pnd_I2)), new ExamineSearch(" "), new ExamineReplace("�"));              //Natural: EXAMINE #CIS-BENE-SPCL-TXT ( #I2 ) FOR ' ' REPLACE '�'
                pnd_I2.nadd(1);                                                                                                                                           //Natural: ASSIGN #I2 := #I2 + 1
                pnd_Cis_Bene_Occurrence.setValue(pnd_I2);                                                                                                                 //Natural: ASSIGN #CIS-BENE-OCCURRENCE := #I2
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Cis_Bene_Occurrence.greater(getZero())))                                                                                                        //Natural: IF #CIS-BENE-OCCURRENCE > 0
        {
            if (condition(pnd_Output_File_Pnd_Cis_Bene_Type.getValue(pnd_Cis_Bene_Occurrence).equals(" ")))                                                               //Natural: IF #CIS-BENE-TYPE ( #CIS-BENE-OCCURRENCE ) = ' '
            {
                pnd_Cis_Bene_Occurrence.nsubtract(1);                                                                                                                     //Natural: ASSIGN #CIS-BENE-OCCURRENCE := #CIS-BENE-OCCURRENCE - 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Output_File2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File_Cis_Rqst_Id, pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Print_Ind,           //Natural: COMPRESS #OUTPUT-FILE.CIS-RQST-ID #CIS-PIPE #OUTPUT-FILE.#CIS-PRINT-IND #CIS-PIPE #OUTPUT-FILE.CIS-TIAA-NBR #CIS-PIPE #OUTPUT-FILE.CIS-CERT-NBR #CIS-PIPE #OUTPUT-FILE.CIS-ORIG-ISSUE-STATE #CIS-PIPE #OUTPUT-FILE.CIS-CNTRCT-APPRVL-IND #CIS-PIPE #OUTPUT-FILE.CIS-PULL-CODE #CIS-PIPE #OUTPUT-FILE.CIS-FOUR-FIFTY-SEVEN-IND #CIS-PIPE #OUTPUT-FILE.CIS-INSTITUTION-NAME #CIS-PIPE #OUTPUT-FILE.#CIS-TIAA-DOI #CIS-PIPE #OUTPUT-FILE.#CIS-CREF-DOI #CIS-PIPE #OUTPUT-FILE.CIS-CNTRCT-TYPE #CIS-PIPE #OUTPUT-FILE.CIS-TIAA-CNTRCT-TYPE #CIS-PIPE #OUTPUT-FILE.CIS-CREF-CNTRCT-TYPE #CIS-PIPE #OUTPUT-FILE.CIS-REA-CNTRCT-TYPE #CIS-PIPE #OUTPUT-FILE.CIS-LOB #CIS-PIPE #OUTPUT-FILE.CIS-LOB-TYPE #CIS-PIPE #OUTPUT-FILE.CIS-OWNERSHIP-CD #CIS-PIPE #OUTPUT-FILE.#CIS-ACCESS-IND #CIS-PIPE #OUTPUT-FILE.CIS-FRST-ANNT-CTZNSHP-CD #CIS-PIPE #OUTPUT-FILE.#CIS-ATRA-IND #CIS-PIPE #OUTPUT-FILE.#CIS-ATRA-ISSUE-DTE #CIS-PIPE #OUTPUT-FILE.#CIS-PRMRY-BENE-CHILD-PRVSN #CIS-PIPE #OUTPUT-FILE.#CIS-CNTGNT-BENE-CHILD-PRVSN #CIS-PIPE #OUTPUT-FILE.CIS-PIN-NBR #CIS-PIPE #OUTPUT-FILE.CIS-FRST-ANNT-SSN #CIS-PIPE #OUTPUT-FILE.#CIS-FRST-ANNT-DOB #CIS-PIPE #OUTPUT-FILE.CIS-ISSUE-STATE-CD #CIS-PIPE #OUTPUT-FILE.CIS-ANNTY-OPTION #CIS-PIPE #OUTPUT-FILE.#CIS-ANNTY-START-DTE #CIS-PIPE #OUTPUT-FILE.#CIS-ANNTY-END-DTE #CIS-PIPE #OUTPUT-FILE.CIS-GRNTED-PERIOD-YRS #CIS-PIPE #OUTPUT-FILE.CIS-GRNTED-PERIOD-DYS #CIS-PIPE #OUTPUT-FILE.CIS-GRNTED-GRD-AMT #CIS-PIPE #OUTPUT-FILE.CIS-GRNTED-STD-AMT #CIS-PIPE #OUTPUT-FILE.CIS-PYMNT-MODE #CIS-PIPE INTO #OUTPUT-FILE2 LEAVING NO SPACE
            pnd_Cis_Pipe, pnd_Output_File_Cis_Tiaa_Nbr, pnd_Cis_Pipe, pnd_Output_File_Cis_Cert_Nbr, pnd_Cis_Pipe, pnd_Output_File_Cis_Orig_Issue_State, 
            pnd_Cis_Pipe, pnd_Output_File_Cis_Cntrct_Apprvl_Ind, pnd_Cis_Pipe, pnd_Output_File_Cis_Pull_Code, pnd_Cis_Pipe, pnd_Output_File_Cis_Four_Fifty_Seven_Ind, 
            pnd_Cis_Pipe, pnd_Output_File_Cis_Institution_Name, pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Tiaa_Doi, pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Cref_Doi, 
            pnd_Cis_Pipe, pnd_Output_File_Cis_Cntrct_Type, pnd_Cis_Pipe, pnd_Output_File_Cis_Tiaa_Cntrct_Type, pnd_Cis_Pipe, pnd_Output_File_Cis_Cref_Cntrct_Type, 
            pnd_Cis_Pipe, pnd_Output_File_Cis_Rea_Cntrct_Type, pnd_Cis_Pipe, pnd_Output_File_Cis_Lob, pnd_Cis_Pipe, pnd_Output_File_Cis_Lob_Type, pnd_Cis_Pipe, 
            pnd_Output_File_Cis_Ownership_Cd, pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Access_Ind, pnd_Cis_Pipe, pnd_Output_File_Cis_Frst_Annt_Ctznshp_Cd, 
            pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Atra_Ind, pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Atra_Issue_Dte, pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Prmry_Bene_Child_Prvsn, 
            pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Cntgnt_Bene_Child_Prvsn, pnd_Cis_Pipe, pnd_Output_File_Cis_Pin_Nbr, pnd_Cis_Pipe, pnd_Output_File_Cis_Frst_Annt_Ssn, 
            pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Frst_Annt_Dob, pnd_Cis_Pipe, pnd_Output_File_Cis_Issue_State_Cd, pnd_Cis_Pipe, pnd_Output_File_Cis_Annty_Option, 
            pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Annty_Start_Dte, pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Annty_End_Dte, pnd_Cis_Pipe, pnd_Output_File_Cis_Grnted_Period_Yrs, 
            pnd_Cis_Pipe, pnd_Output_File_Cis_Grnted_Period_Dys, pnd_Cis_Pipe, pnd_Output_File_Cis_Grnted_Grd_Amt, pnd_Cis_Pipe, pnd_Output_File_Cis_Grnted_Std_Amt, 
            pnd_Cis_Pipe, pnd_Output_File_Cis_Pymnt_Mode, pnd_Cis_Pipe));
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I #OUTPUT-FILE3 ( * )
        pnd_Output_File3.getValue("*").reset();
        if (condition(pnd_Cis_Da_Occurrence.greater(getZero())))                                                                                                          //Natural: IF #CIS-DA-OCCURRENCE > 0
        {
            FOR11:                                                                                                                                                        //Natural: FOR #I = 1 TO #CIS-DA-OCCURRENCE
            for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(pnd_Cis_Da_Occurrence)); 
                ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
            {
                pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File_Pnd_Cis_Da_Tiaa_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #CIS-DA-TIAA-NBR ( #I ) #CIS-TILDE #CIS-DA-CERT-NBR ( #I ) #CIS-TILDE #CIS-DA-REA-PROCEEDS-AMT ( #I ) #CIS-TILDE #CIS-DA-TIAA-PROCEEDS-AMT ( #I ) #CIS-TILDE #CIS-DA-CREF-PROCEEDS-AMT ( #I ) INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Da_Cert_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Da_Rea_Proceeds_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), 
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Da_Tiaa_Proceeds_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Da_Cref_Proceeds_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())));
                if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(pnd_Cis_Da_Occurrence)))                                                                       //Natural: IF #I = #CIS-DA-OCCURRENCE
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-PIPE INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Pipe));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-CARET INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Caret));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_File3.getValue(1).setValue(pnd_Cis_Pipe);                                                                                                          //Natural: ASSIGN #OUTPUT-FILE3 ( 1 ) := #CIS-PIPE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Output_File2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File2, pnd_Output_File3.getValue("*")));                                     //Natural: COMPRESS #OUTPUT-FILE2 #OUTPUT-FILE3 ( * ) INTO #OUTPUT-FILE2 LEAVING NO SPACE
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I #OUTPUT-FILE3 ( * )
        pnd_Output_File3.getValue("*").reset();
        if (condition(pnd_Cis_Cref_Occurrence.greater(getZero())))                                                                                                        //Natural: IF #CIS-CREF-OCCURRENCE > 0
        {
            FOR12:                                                                                                                                                        //Natural: FOR #I = 1 TO #CIS-CREF-OCCURRENCE
            for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(pnd_Cis_Cref_Occurrence)); 
                ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
            {
                pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File_Pnd_Cis_Nec_Fund_Nme.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #CIS-NEC-FUND-NME ( #I ) #CIS-TILDE #CIS-CREF-ACCT-CDE ( #I ) #CIS-TILDE #CIS-CREF-MNTHLY-NBR-UNITS ( #I ) #CIS-TILDE #CIS-CREF-ANNUAL-NBR-UNITS ( #I ) #CIS-TILDE #CIS-CREF-MNTHLY-AMT ( #I ) #CIS-TILDE #CIS-CREF-ANNTY-AMT ( #I ) INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Cref_Acct_Cde.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Cref_Mnthly_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), 
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Cref_Annual_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Cref_Mnthly_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), 
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Cref_Annty_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())));
                if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(pnd_Cis_Cref_Occurrence)))                                                                     //Natural: IF #I = #CIS-CREF-OCCURRENCE
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-PIPE INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Pipe));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-CARET INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Caret));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_File3.getValue(1).setValue(pnd_Cis_Pipe);                                                                                                          //Natural: ASSIGN #OUTPUT-FILE3 ( 1 ) := #CIS-PIPE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Output_File2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File2, pnd_Output_File3.getValue("*")));                                     //Natural: COMPRESS #OUTPUT-FILE2 #OUTPUT-FILE3 ( * ) INTO #OUTPUT-FILE2 LEAVING NO SPACE
        //*  #CIS-REA-ANNTY-MNTHLY-AMT
        pnd_Output_File2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File2, pnd_Output_File_Cis_Rea_Mnthly_Nbr_Units, pnd_Cis_Pipe,               //Natural: COMPRESS #OUTPUT-FILE2 #OUTPUT-FILE.CIS-REA-MNTHLY-NBR-UNITS #CIS-PIPE #OUTPUT-FILE.CIS-REA-ANNUAL-NBR-UNITS #CIS-PIPE #OUTPUT-FILE.#CIS-RTB-AMT #CIS-PIPE #OUTPUT-FILE.CIS-REA-ANNTY-AMT #CIS-PIPE INTO #OUTPUT-FILE2 LEAVING NO SPACE
            pnd_Output_File_Cis_Rea_Annual_Nbr_Units, pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Rtb_Amt, pnd_Cis_Pipe, pnd_Output_File_Cis_Rea_Annty_Amt, pnd_Cis_Pipe));
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I #OUTPUT-FILE3 ( * )
        pnd_Output_File3.getValue("*").reset();
        if (condition(pnd_Cis_Tacc_Occurrence.greater(getZero())))                                                                                                        //Natural: IF #CIS-TACC-OCCURRENCE > 0
        {
            FOR13:                                                                                                                                                        //Natural: FOR #I = 1 TO #CIS-TACC-OCCURRENCE
            for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(pnd_Cis_Tacc_Occurrence)); 
                ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
            {
                pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File_Pnd_Cis_Tacc_Account_Name.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #CIS-TACC-ACCOUNT-NAME ( #I ) #CIS-TILDE #CIS-TACC-MNTHLY-NBR-UNITS ( #I ) #CIS-TILDE #CIS-TACC-ANNUAL-NBR-UNITS ( #I ) #CIS-TILDE #CIS-TACC-MNTHLY-AMT ( #I ) #CIS-TILDE #CIS-TACC-ANNTY-AMT ( #I ) INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Tacc_Mnthly_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Tacc_Annual_Nbr_Units.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), 
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Tacc_Mnthly_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Tacc_Annty_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())));
                if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(pnd_Cis_Tacc_Occurrence)))                                                                     //Natural: IF #I = #CIS-TACC-OCCURRENCE
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-PIPE INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Pipe));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-CARET INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Caret));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_File3.getValue(1).setValue(pnd_Cis_Pipe);                                                                                                          //Natural: ASSIGN #OUTPUT-FILE3 ( 1 ) := #CIS-PIPE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Output_File2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File2, pnd_Output_File3.getValue("*")));                                     //Natural: COMPRESS #OUTPUT-FILE2 #OUTPUT-FILE3 ( * ) INTO #OUTPUT-FILE2 LEAVING NO SPACE
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I #OUTPUT-FILE3 ( * )
        pnd_Output_File3.getValue("*").reset();
        if (condition(pnd_Cis_Comut_Occurrence.greater(getZero())))                                                                                                       //Natural: IF #CIS-COMUT-OCCURRENCE > 0
        {
            FOR14:                                                                                                                                                        //Natural: FOR #I = 1 TO #CIS-COMUT-OCCURRENCE
            for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(pnd_Cis_Comut_Occurrence)); 
                ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
            {
                pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File_Pnd_Cis_Comut_Grnted_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #CIS-COMUT-GRNTED-AMT ( #I ) #CIS-TILDE #CIS-COMUT-INT-RATE ( #I ) #CIS-TILDE #CIS-COMUT-PYMT-METHOD ( #I ) INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Comut_Int_Rate.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Comut_Pymt_Method.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())));
                if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(pnd_Cis_Comut_Occurrence)))                                                                    //Natural: IF #I = #CIS-COMUT-OCCURRENCE
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-PIPE INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Pipe));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-CARET INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Caret));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_File3.getValue(1).setValue(pnd_Cis_Pipe);                                                                                                          //Natural: ASSIGN #OUTPUT-FILE3 ( 1 ) := #CIS-PIPE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Output_File2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File2, pnd_Output_File3.getValue("*")));                                     //Natural: COMPRESS #OUTPUT-FILE2 #OUTPUT-FILE3 ( * ) INTO #OUTPUT-FILE2 LEAVING NO SPACE
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I #OUTPUT-FILE3 ( * )
        pnd_Output_File3.getValue("*").reset();
        if (condition(pnd_Cis_Comut_2_Occurrence.greater(getZero())))                                                                                                     //Natural: IF #CIS-COMUT-2-OCCURRENCE > 0
        {
            FOR15:                                                                                                                                                        //Natural: FOR #I = 1 TO #CIS-COMUT-2-OCCURRENCE
            for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(pnd_Cis_Comut_2_Occurrence)); 
                ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
            {
                pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File_Pnd_Cis_Comut_Grnted_Amt_2.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #CIS-COMUT-GRNTED-AMT-2 ( #I ) #CIS-TILDE #CIS-COMUT-INT-RATE-2 ( #I ) #CIS-TILDE #CIS-COMUT-PYMT-METHOD-2 ( #I ) #CIS-TILDE #CIS-COMUT-MORTALITY-BASIS ( #I ) INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Comut_Int_Rate_2.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Comut_Pymt_Method_2.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), 
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Comut_Mortality_Basis.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())));
                if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(pnd_Cis_Comut_2_Occurrence)))                                                                  //Natural: IF #I = #CIS-COMUT-2-OCCURRENCE
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-PIPE INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Pipe));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-CARET INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Caret));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_File3.getValue(1).setValue(pnd_Cis_Pipe);                                                                                                          //Natural: ASSIGN #OUTPUT-FILE3 ( 1 ) := #CIS-PIPE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Output_File2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File2, pnd_Output_File3.getValue("*")));                                     //Natural: COMPRESS #OUTPUT-FILE2 #OUTPUT-FILE3 ( * ) INTO #OUTPUT-FILE2 LEAVING NO SPACE
        ldaAppl170.getPnd_Work_Fields_Pnd_I().reset();                                                                                                                    //Natural: RESET #I #OUTPUT-FILE3 ( * )
        pnd_Output_File3.getValue("*").reset();
        if (condition(pnd_Cis_Bene_Occurrence.greater(getZero())))                                                                                                        //Natural: IF #CIS-BENE-OCCURRENCE > 0
        {
            FOR16:                                                                                                                                                        //Natural: FOR #I = 1 TO #CIS-BENE-OCCURRENCE
            for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(pnd_Cis_Bene_Occurrence)); 
                ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
            {
                pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File_Pnd_Cis_Bene_Type.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #CIS-BENE-TYPE ( #I ) #CIS-TILDE #CIS-BENE-NAME ( #I ) #CIS-TILDE #CIS-BENE-RLTN ( #I ) #CIS-TILDE #CIS-BENE-RLTN-CDE ( #I ) #CIS-TILDE #CIS-BENE-SSN-NBR ( #I ) #CIS-TILDE #CIS-BENE-DOB ( #I ) #CIS-TILDE #CIS-BENE-NMRTR-NBR ( #I ) #CIS-TILDE #CIS-BENE-DNMNTR-NBR ( #I ) #CIS-TILDE #CIS-BENE-SPCL-TXT ( #I ) INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Bene_Name.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Bene_Rltn.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), 
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Bene_Rltn_Cde.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Bene_Ssn_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), 
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Bene_Dob.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Bene_Nmrtr_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), 
                    pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Bene_Dnmntr_Nbr.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()), pnd_Cis_Tilde, pnd_Output_File_Pnd_Cis_Bene_Spcl_Txt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())));
                if (condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().equals(pnd_Cis_Bene_Occurrence)))                                                                     //Natural: IF #I = #CIS-BENE-OCCURRENCE
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-PIPE INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Pipe));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File3.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),  //Natural: COMPRESS #OUTPUT-FILE3 ( #I ) #CIS-CARET INTO #OUTPUT-FILE3 ( #I ) LEAVING NO SPACE
                        pnd_Cis_Caret));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Output_File3.getValue(1).setValue(pnd_Cis_Pipe);                                                                                                          //Natural: ASSIGN #OUTPUT-FILE3 ( 1 ) := #CIS-PIPE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Output_File2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File2, pnd_Output_File3.getValue("*")));                                     //Natural: COMPRESS #OUTPUT-FILE2 #OUTPUT-FILE3 ( * ) INTO #OUTPUT-FILE2 LEAVING NO SPACE
        pnd_Output_File2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Output_File2, pnd_Output_File_Pnd_Cis_Scnd_Annt_Full_Name, pnd_Cis_Pipe,            //Natural: COMPRESS #OUTPUT-FILE2 #OUTPUT-FILE.#CIS-SCND-ANNT-FULL-NAME #CIS-PIPE #OUTPUT-FILE.CIS-SCND-ANNT-SSN #CIS-PIPE #OUTPUT-FILE.#CIS-SCND-ANNT-DOB #CIS-PIPE #OUTPUT-FILE.CIS-TRNSF-UNITS #CIS-PIPE #OUTPUT-FILE.CIS-TRNSF-CERT-NBR #CIS-PIPE #OUTPUT-FILE.CIS-TRNSF-TIAA-REA-NBR #CIS-PIPE #OUTPUT-FILE.CIS-TRNSF-ACCT-CDE #CIS-PIPE #OUTPUT-FILE.CIS-TRNSF-FLAG #CIS-PIPE #OUTPUT-FILE.CIS-TRNSF-FROM-COMPANY #CIS-PIPE #OUTPUT-FILE.CIS-TRNSF-TO-COMPANY INTO #OUTPUT-FILE2 LEAVING NO SPACE
            pnd_Output_File_Cis_Scnd_Annt_Ssn, pnd_Cis_Pipe, pnd_Output_File_Pnd_Cis_Scnd_Annt_Dob, pnd_Cis_Pipe, pnd_Output_File_Cis_Trnsf_Units, pnd_Cis_Pipe, 
            pnd_Output_File_Cis_Trnsf_Cert_Nbr, pnd_Cis_Pipe, pnd_Output_File_Cis_Trnsf_Tiaa_Rea_Nbr, pnd_Cis_Pipe, pnd_Output_File_Cis_Trnsf_Acct_Cde, 
            pnd_Cis_Pipe, pnd_Output_File_Cis_Trnsf_Flag, pnd_Cis_Pipe, pnd_Output_File_Cis_Trnsf_From_Company, pnd_Cis_Pipe, pnd_Output_File_Cis_Trnsf_To_Company));
        pdaCisa0050.getPnd_Cisa0050_Pnd_I_Pin().setValue(cis_Prtcpnt_Cis_Pin_Nbr);                                                                                        //Natural: MOVE CIS-PRTCPNT.CIS-PIN-NBR TO #I-PIN
        pdaCisa0050.getPnd_Cisa0050_Pnd_I_Tiaa().setValue(cis_Prtcpnt_Cis_Tiaa_Nbr);                                                                                      //Natural: MOVE CIS-PRTCPNT.CIS-TIAA-NBR TO #I-TIAA
        DbsUtil.examine(new ExamineSource(pnd_Output_File2), new ExamineSearch(" "), new ExamineGivingLength(pdaCisa0050.getPnd_Cisa0050_Pnd_I_Data_Length()));           //Natural: EXAMINE #OUTPUT-FILE2 FOR ' ' GIVING LENGTH #I-DATA-LENGTH
        DbsUtil.examine(new ExamineSource(pnd_Output_File2), new ExamineSearch("�"), new ExamineReplace(" "));                                                            //Natural: EXAMINE #OUTPUT-FILE2 FOR '�' REPLACE ' '
        setValueToSubstring(pnd_Output_File2.getSubstring(1,pdaCisa0050.getPnd_Cisa0050_Pnd_I_Data_Length().getInt()),pdaCisa0050.getPnd_Cisa0050_Pnd_I_Pda_Data_A(),     //Natural: MOVE SUBSTR ( #OUTPUT-FILE2,1,#I-DATA-LENGTH ) TO SUBSTR ( #I-PDA-DATA-A,1,#I-DATA-LENGTH )
            1,pdaCisa0050.getPnd_Cisa0050_Pnd_I_Data_Length().getInt());
        pnd_Output.nadd(1);                                                                                                                                               //Natural: ASSIGN #OUTPUT := #OUTPUT + 1
        getWorkFiles().write(1, false, pdaCisa0050.getPnd_Cisa0050_Pnd_I_Tiaa(), " ", pnd_Output_File_Pnd_Cis_Print_Ind, " ", pdaCisa0050.getPnd_Cisa0050_Pnd_I_Data_Length(),  //Natural: WRITE WORK FILE 1 #I-TIAA ' ' #CIS-PRINT-IND ' ' #I-DATA-LENGTH ' ' #I-PDA-DATA-A
            " ", pdaCisa0050.getPnd_Cisa0050_Pnd_I_Pda_Data_A());
        pdaCisa0050.getPnd_Cisa0050_Pnd_I_Mq_Count().setValue(pnd_Output);                                                                                                //Natural: ASSIGN #I-MQ-COUNT := #OUTPUT
        pnd_Ix1.reset();                                                                                                                                                  //Natural: RESET #IX1
        //*  INC4678056
        //*  INC4678056
        DbsUtil.examine(new ExamineSource(pnd_Save_Cis_Tiaa_Nbr.getValue("*")), new ExamineSearch(pnd_Output_File_Cis_Tiaa_Nbr), new ExamineGivingIndex(pnd_Ix1));        //Natural: EXAMINE #SAVE-CIS-TIAA-NBR ( * ) FOR #OUTPUT-FILE.CIS-TIAA-NBR GIVING INDEX #IX1
        //*                                                           /* INC4678056
        //*  INC4678056
        if (condition(pnd_Ix1.greater(getZero())))                                                                                                                        //Natural: IF #IX1 > 0
        {
            //*  INC4678056
            cmrollReturnCode = DbsUtil.callExternalProgram("CMROLL",pnd_Seconds);                                                                                         //Natural: CALL 'CMROLL' #SECONDS
            //*  INC4678056
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  INC4678056
            if (condition(pnd_Print_Ind.notEquals("R")))                                                                                                                  //Natural: IF #PRINT-IND <> 'R'
            {
                //*  INC4678056
                //*  INC4678056
                //*  INC4678056
                if (condition(pnd_Ix2.less(999999)))                                                                                                                      //Natural: IF #IX2 < 999999
                {
                    pnd_Ix2.nadd(1);                                                                                                                                      //Natural: ASSIGN #IX2 := #IX2 + 1
                    pnd_Save_Cis_Tiaa_Nbr.getValue(pnd_Ix2).setValue(pnd_Output_File_Cis_Tiaa_Nbr);                                                                       //Natural: ASSIGN #SAVE-CIS-TIAA-NBR ( #IX2 ) := #OUTPUT-FILE.CIS-TIAA-NBR
                    //*  INC4678056
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  INC4678056
                    getReports().write(0, "CONTRACT TABLE IN CISB9900 IS FULL");                                                                                          //Natural: WRITE 'CONTRACT TABLE IN CISB9900 IS FULL'
                    if (Global.isEscape()) return;
                    //*  INC4678056
                }                                                                                                                                                         //Natural: END-IF
                //*  INC4678056
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  INC4678056
                //*  INC4678056
                //*  INC4678056
                pnd_Save_Cis_Tiaa_Nbr.getValue("*").reset();                                                                                                              //Natural: RESET #SAVE-CIS-TIAA-NBR ( * )
                pnd_Ix2.setValue(1);                                                                                                                                      //Natural: ASSIGN #IX2 := 1
                pnd_Save_Cis_Tiaa_Nbr.getValue(pnd_Ix2).setValue(pnd_Output_File_Cis_Tiaa_Nbr);                                                                           //Natural: ASSIGN #SAVE-CIS-TIAA-NBR ( #IX2 ) := #OUTPUT-FILE.CIS-TIAA-NBR
                //*  INC4678056
            }                                                                                                                                                             //Natural: END-IF
            //*  INC4678056
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Cisn0050.class , getCurrentProcessState(), pdaCisa0050.getPnd_Cisa0050());                                                                        //Natural: CALLNAT 'CISN0050' #CISA0050
        if (condition(Global.isEscape())) return;
        if (condition(pdaCisa0050.getPnd_Cisa0050_Pnd_O_Return_Code().equals("0000")))                                                                                    //Natural: IF #O-RETURN-CODE EQ '0000'
        {
            pnd_Mq.nadd(1);                                                                                                                                               //Natural: ASSIGN #MQ := #MQ + 1
            pnd_Mq_Detail.nadd(1);                                                                                                                                        //Natural: ASSIGN #MQ-DETAIL := #MQ-DETAIL + 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Mq_Error.nadd(1);                                                                                                                                         //Natural: ASSIGN #MQ-ERROR := #MQ-ERROR + 1
            getReports().write(0, pdaCisa0050.getPnd_Cisa0050_Pnd_O_Return_Code()," ",pdaCisa0050.getPnd_Cisa0050_Pnd_O_Return_Text());                                   //Natural: WRITE #O-RETURN-CODE ' ' #O-RETURN-TEXT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132");
    }
}
