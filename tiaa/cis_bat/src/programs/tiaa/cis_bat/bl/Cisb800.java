/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:21:46 PM
**        * FROM NATURAL PROGRAM : Cisb800
************************************************************
**        * FILE NAME            : Cisb800.java
**        * CLASS NAME           : Cisb800
**        * INSTANCE NAME        : Cisb800
************************************************************
************************************************************************
* 06/07/06 O SOTTO CHANGED AS PART OF MDO LEGAL SPLIT. SC 060706. ******
* 09/28/09 DEVELBISS - CHANGE TO HANDLE MULTIPLE PRODUCTS - SEE DIAG   *
* 05/09/2017 - GHOSABE      - PIN EXPANSION CHANGES.  (C420007)    PINE.
************************************************************************

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb800 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Part_Wfile;
    private DbsField pnd_Part_Wfile_Pnd_Job_Name;
    private DbsField pnd_Part_Wfile_Pnd_Job_Reason;
    private DbsField pnd_Part_Wfile_Pnd_Cntrct_Type;
    private DbsField pnd_Part_Wfile_Pnd_Cntrct_Approval;
    private DbsField pnd_Part_Wfile_Pnd_Cntrct_Sys_Id;
    private DbsField pnd_Part_Wfile_Pnd_Cntrct_Option;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Cntrct_Cash_Status;
    private DbsField pnd_Part_Wfile_Pnd_Cntrct_Retr_Surv_Status;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Header_1;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Header_2;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Cntrct_Header_1;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Cntrct_Header_2;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Cntrct_Header_1;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Cntrct_Header_2;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Income_Option;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Income_Option;
    private DbsField pnd_Part_Wfile_Pnd_Cntrct_Mergeset_Id;

    private DbsGroup pnd_Part_Wfile__R_Field_1;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Cntrct_A;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Iss_Dte;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Number;

    private DbsGroup pnd_Part_Wfile__R_Field_2;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_F;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Dash_1;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_6;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Dash_2;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_L;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_R;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Real_Estate_Num;

    private DbsGroup pnd_Part_Wfile__R_Field_3;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_R_F;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Dash_R_1;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_R_6;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Dash_R_2;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_R_L;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Dash_R_3;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_R_Rea;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_R_R;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Cntrct_Number;

    private DbsGroup pnd_Part_Wfile__R_Field_4;
    private DbsField pnd_Part_Wfile_Pnd_Cref_F;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Dash_1;
    private DbsField pnd_Part_Wfile_Pnd_Cref_6;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Dash_2;
    private DbsField pnd_Part_Wfile_Pnd_Cref_L;
    private DbsField pnd_Part_Wfile_Pnd_Reef_R;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Issue_Date;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Issue_Date;
    private DbsField pnd_Part_Wfile_Pnd_Freq_Of_Payment;
    private DbsField pnd_Part_Wfile_Pnd_First_Payment_Date;
    private DbsField pnd_Part_Wfile_Pnd_Last_Payment_Date;
    private DbsField pnd_Part_Wfile_Pnd_Guaranteed_Period;

    private DbsGroup pnd_Part_Wfile__R_Field_5;
    private DbsField pnd_Part_Wfile_Pnd_G_Yy;
    private DbsField pnd_Part_Wfile_Pnd_G_Filler1;
    private DbsField pnd_Part_Wfile_Pnd_G_Years;
    private DbsField pnd_Part_Wfile_Pnd_G_Filler2;
    private DbsField pnd_Part_Wfile_Pnd_G_Dd;
    private DbsField pnd_Part_Wfile_Pnd_G_Filler3;
    private DbsField pnd_Part_Wfile_Pnd_G_Days;
    private DbsField pnd_Part_Wfile_Pnd_G_Filler_4;
    private DbsField pnd_Part_Wfile_Pnd_First_Guaranteed_P_Date;

    private DbsGroup pnd_Part_Wfile__R_Field_6;
    private DbsField pnd_Part_Wfile_Pnd_G_P_Mm;
    private DbsField pnd_Part_Wfile_Pnd_G_P_B1;
    private DbsField pnd_Part_Wfile_Pnd_G_P_Dd;
    private DbsField pnd_Part_Wfile_Pnd_G_P_B2;
    private DbsField pnd_Part_Wfile_Pnd_G_P_Ccyy;
    private DbsField pnd_Part_Wfile_Pnd_End_Guaranteed_P_Date;

    private DbsGroup pnd_Part_Wfile__R_Field_7;
    private DbsField pnd_Part_Wfile_Pnd_E_G_Mm;
    private DbsField pnd_Part_Wfile_Pnd_E_G_B1;
    private DbsField pnd_Part_Wfile_Pnd_E_G_Dd;
    private DbsField pnd_Part_Wfile_Pnd_E_G_B2;
    private DbsField pnd_Part_Wfile_Pnd_E_G_Ccyy;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Form_Num_Pg3_4;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Form_Num_Pg5_6;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Form_Num_Pg3_4;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Form_Num_Pg5_6;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Form_Num_Pg3_4;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Form_Num_Pg5_6;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Product_Cde_3_4;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Product_Cde_5_6;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Product_Cde_3_4;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Product_Cde_5_6;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Product_Cde_3_4;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Product_Cde_5_6;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Edition_Num_Pg3_4;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Edition_Num_Pg5_6;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Edition_Num_Pg3_4;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Edition_Num_Pg5_6;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Edition_Num_Pg3_4;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Edition_Num_Pg5_6;
    private DbsField pnd_Part_Wfile_Pnd_Pin_Number;
    private DbsField pnd_Part_Wfile_Pnd_First_Annu_Cntrct_Name;

    private DbsGroup pnd_Part_Wfile__R_Field_8;
    private DbsField pnd_Part_Wfile_Pnd_First_Annu_Name;
    private DbsField pnd_Part_Wfile_Pnd_Filler_1;
    private DbsField pnd_Part_Wfile_Pnd_Middle_Int;
    private DbsField pnd_Part_Wfile_Pnd_Filler_2;
    private DbsField pnd_Part_Wfile_Pnd_Last_Annu_Name;
    private DbsField pnd_Part_Wfile_Pnd_Filler_3;
    private DbsField pnd_Part_Wfile_Pnd_Suffix;
    private DbsField pnd_Part_Wfile_Pnd_First_Annu_Last_Name;
    private DbsField pnd_Part_Wfile_Pnd_First_Annu_First_Name;
    private DbsField pnd_Part_Wfile_Pnd_First_Annu_Mid_Name;
    private DbsField pnd_Part_Wfile_Pnd_First_Annu_Title;
    private DbsField pnd_Part_Wfile_Pnd_First_Annu_Suffix;
    private DbsField pnd_Part_Wfile_Pnd_First_Annu_Citizenship;
    private DbsField pnd_Part_Wfile_Pnd_First_Annu_Ssn;
    private DbsField pnd_Part_Wfile_Pnd_First_Annu_Dob;
    private DbsField pnd_Part_Wfile_Pnd_First_Annu_Res_State;
    private DbsField pnd_Part_Wfile_Pnd_Annu_Address_Name;

    private DbsGroup pnd_Part_Wfile__R_Field_9;
    private DbsField pnd_Part_Wfile_Pnd_Prefix_A;
    private DbsField pnd_Part_Wfile_Pnd_Filler_A1;
    private DbsField pnd_Part_Wfile_Pnd_First_Name_A;
    private DbsField pnd_Part_Wfile_Pnd_Filler_A2;
    private DbsField pnd_Part_Wfile_Pnd_Middle_Init_A;
    private DbsField pnd_Part_Wfile_Pnd_Filler_A3;
    private DbsField pnd_Part_Wfile_Pnd_Last_Name_A;
    private DbsField pnd_Part_Wfile_Pnd_Filler_A4;
    private DbsField pnd_Part_Wfile_Pnd_Suffix_A;
    private DbsField pnd_Part_Wfile_Pnd_Annu_Welcome_Name;

    private DbsGroup pnd_Part_Wfile__R_Field_10;
    private DbsField pnd_Part_Wfile_Pnd_Prefix_W;
    private DbsField pnd_Part_Wfile_Pnd_Filler_W;
    private DbsField pnd_Part_Wfile_Pnd_Last_Name_W;
    private DbsField pnd_Part_Wfile_Pnd_Annu_Dir_Name;

    private DbsGroup pnd_Part_Wfile__R_Field_11;
    private DbsField pnd_Part_Wfile_Pnd_First_Name_D;
    private DbsField pnd_Part_Wfile_Pnd_Filler_D1;
    private DbsField pnd_Part_Wfile_Pnd_Mid_Name_D;
    private DbsField pnd_Part_Wfile_Pnd_Filler_D2;
    private DbsField pnd_Part_Wfile_Pnd_Last_Name_D;
    private DbsField pnd_Part_Wfile_Pnd_First_A_C_M;
    private DbsField pnd_Part_Wfile_Pnd_First_A_C_M_Literal;
    private DbsField pnd_Part_Wfile_Pnd_Calc_Participant;
    private DbsField pnd_Part_Wfile_Pnd_First_Annuit_Mail_Addr1;
    private DbsField pnd_Part_Wfile_Pnd_First_Annuit_Zipcde;
    private DbsField pnd_Part_Wfile_Pnd_Second_Annuit_Cntrct_Name;

    private DbsGroup pnd_Part_Wfile__R_Field_12;
    private DbsField pnd_Part_Wfile_Pnd_First_Name_S;
    private DbsField pnd_Part_Wfile_Pnd_Filler_S1;
    private DbsField pnd_Part_Wfile_Pnd_Mid_Name_S;
    private DbsField pnd_Part_Wfile_Pnd_Filler_S2;
    private DbsField pnd_Part_Wfile_Pnd_Last_Name_S;
    private DbsField pnd_Part_Wfile_Pnd_Second_Annuit_Ssn;
    private DbsField pnd_Part_Wfile_Pnd_Second_Annuit_Dob;
    private DbsField pnd_Part_Wfile_Pnd_Dollar_G;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Graded_Amt;

    private DbsGroup pnd_Part_Wfile__R_Field_13;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Graded_Amt_N;
    private DbsField pnd_Part_Wfile_Pnd_Dollar_Tot;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Standard_Amt;

    private DbsGroup pnd_Part_Wfile__R_Field_14;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Standard_Amt_N;
    private DbsField pnd_Part_Wfile_Pnd_Dollar_S;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Tot_Grd_Stnd_Amt;

    private DbsGroup pnd_Part_Wfile__R_Field_15;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Tot_Grd_Stnd_Amt_N;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Guarant_Period_Flag;
    private DbsField pnd_Part_Wfile_Pnd_Multiple_Com_Int_Rate;
    private DbsField pnd_Part_Wfile_Pnd_Death_Ben_After_Iss_Flag;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Com_Info;

    private DbsGroup pnd_Part_Wfile__R_Field_16;

    private DbsGroup pnd_Part_Wfile_Pnd_Redefine_Tiaa_Info;
    private DbsField pnd_Part_Wfile_Pnd_Dollar_Ci;
    private DbsField pnd_Part_Wfile_Pnd_Comm_Amt_Ci;
    private DbsField pnd_Part_Wfile_Pnd_Comm_Int_Rate_Ci;
    private DbsField pnd_Part_Wfile_Pnd_Comm_Method_Ci;
    private DbsField pnd_Part_Wfile_Pnd_Surivor_Reduction_Literal;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Contract_Settled_Flag;

    private DbsGroup pnd_Part_Wfile__R_Field_17;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Contract_Settled_Flag_N;
    private DbsField pnd_Part_Wfile_Pnd_Dollar_Pros_Sign;
    private DbsField pnd_Part_Wfile_Pnd_Tot_Tiaa_Paying_Pros_Amt;

    private DbsGroup pnd_Part_Wfile__R_Field_18;
    private DbsField pnd_Part_Wfile_Pnd_Tot_Tiaa_Paying_Pros_Amt_N;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Payin_Cntrct_Info;

    private DbsGroup pnd_Part_Wfile__R_Field_19;

    private DbsGroup pnd_Part_Wfile_Pnd_Redefine_Tiaa_C_I;
    private DbsField pnd_Part_Wfile_Pnd_Da_Payin_Cntrct;

    private DbsGroup pnd_Part_Wfile__R_Field_20;
    private DbsField pnd_Part_Wfile_Pnd_Da_Payin_Cntrct_N;
    private DbsField pnd_Part_Wfile_Pnd_Process_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Process_Amt;

    private DbsGroup pnd_Part_Wfile__R_Field_21;
    private DbsField pnd_Part_Wfile_Pnd_Process_Amt_N;
    private DbsField pnd_Part_Wfile_Pnd_Post_Ret_Trans_Flag;
    private DbsField pnd_Part_Wfile_Pnd_Transfer_Units;
    private DbsField pnd_Part_Wfile_Pnd_Transfer_Payout_Cntrct;
    private DbsField pnd_Part_Wfile_Pnd_Transfer_Fund_Name;
    private DbsField pnd_Part_Wfile_Pnd_Transfer_Cmp_Name;
    private DbsField pnd_Part_Wfile_Pnd_Transfer_Mode;
    private DbsField pnd_Part_Wfile_Pnd_Num_Cref_Payout_Account;

    private DbsGroup pnd_Part_Wfile__R_Field_22;
    private DbsField pnd_Part_Wfile_Pnd_Num_Cref_Payout_Account_N;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Payout_Info;

    private DbsGroup pnd_Part_Wfile__R_Field_23;

    private DbsGroup pnd_Part_Wfile_Pnd_Redefine_Cref_P_Info;
    private DbsField pnd_Part_Wfile_Pnd_Account;
    private DbsField pnd_Part_Wfile_Pnd_Monthly_Units;
    private DbsField pnd_Part_Wfile_Pnd_Annual_Units;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Cntrct_Settled;

    private DbsGroup pnd_Part_Wfile__R_Field_24;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Cntrct_Settled_N;
    private DbsField pnd_Part_Wfile_Pnd_Dollar_Tot_Cref_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Tot_Cref_Payin_Amt;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Payin_Cntrct_Info;

    private DbsGroup pnd_Part_Wfile__R_Field_25;

    private DbsGroup pnd_Part_Wfile_Pnd_Redefine_Cref_P_C_Info;
    private DbsField pnd_Part_Wfile_Pnd_Da_Payin_Contract;
    private DbsField pnd_Part_Wfile_Pnd_Da_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Process_Amount;

    private DbsGroup pnd_Part_Wfile__R_Field_26;
    private DbsField pnd_Part_Wfile_Pnd_Process_Amount_N;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Cnt_Settled;

    private DbsGroup pnd_Part_Wfile__R_Field_27;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Cnt_Settled_N;
    private DbsField pnd_Part_Wfile_Pnd_Tot_Tiaa_Rea_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt;

    private DbsGroup pnd_Part_Wfile__R_Field_28;
    private DbsField pnd_Part_Wfile_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt_N;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Payin_Cntrct_Info;

    private DbsGroup pnd_Part_Wfile__R_Field_29;

    private DbsGroup pnd_Part_Wfile_Pnd_Redefine_Tiaa_R_P_C_Info;
    private DbsField pnd_Part_Wfile_Pnd_Da_Payin_Cntrct_Info;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Rea_Payin_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Proceeds_Amt_Info;

    private DbsGroup pnd_Part_Wfile__R_Field_30;
    private DbsField pnd_Part_Wfile_Pnd_Proceeds_Amt_Info_N;
    private DbsField pnd_Part_Wfile_Pnd_Rea_First_Annu_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Rea_First_Annuity_Payment;

    private DbsGroup pnd_Part_Wfile__R_Field_31;
    private DbsField pnd_Part_Wfile_Pnd_Rea_First_Annuity_Payment_N;
    private DbsField pnd_Part_Wfile_Pnd_Rea_Mnth_Units_Payable;
    private DbsField pnd_Part_Wfile_Pnd_Rea_Annu_Units_Payable;
    private DbsField pnd_Part_Wfile_Pnd_Rea_Survivor_Units_Payable;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Trad_Annu_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Traditional_Annu_Amt;

    private DbsGroup pnd_Part_Wfile__R_Field_32;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Traditional_Annu_Amt_N;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Tiaa_Initial_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Initial_Paymeny_Amt;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Cref_Initial_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Cref_Init_Paymeny_Amt;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Tiaa_Exclude_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Tiaa_Exclude_Amt;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Cref_Exclude_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Mdo_Cref_Exclude_Amt;
    private DbsField pnd_Part_Wfile_Pnd_Traditional_G_I_R;
    private DbsField pnd_Part_Wfile_Pnd_Surrender_Charge;
    private DbsField pnd_Part_Wfile_Pnd_Contigent_Charge;
    private DbsField pnd_Part_Wfile_Pnd_Separate_Charge;
    private DbsField pnd_Part_Wfile_Pnd_Orig_Part_Name;

    private DbsGroup pnd_Part_Wfile__R_Field_33;
    private DbsField pnd_Part_Wfile_Pnd_First_Name_O;
    private DbsField pnd_Part_Wfile_Pnd_Filler_O1;
    private DbsField pnd_Part_Wfile_Pnd_Middle_Init_O;
    private DbsField pnd_Part_Wfile_Pnd_Filler_O2;
    private DbsField pnd_Part_Wfile_Pnd_Last_Name_O;
    private DbsField pnd_Part_Wfile_Pnd_Filler_O3;
    private DbsField pnd_Part_Wfile_Pnd_Suffix_O;
    private DbsField pnd_Part_Wfile_Pnd_Orig_Part_Ssn;
    private DbsField pnd_Part_Wfile_Pnd_Orig_Part_Dob;
    private DbsField pnd_Part_Wfile_Pnd_Orig_Part_Dod;
    private DbsField pnd_Part_Wfile_Pnd_Rbt_Requester;
    private DbsField pnd_Part_Wfile_Pnd_Retir_Trans_Bene_Per;
    private DbsField pnd_Part_Wfile_Pnd_Retir_Trans_Bene_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Retir_Trans_Bene_Amt;
    private DbsField pnd_Part_Wfile_Pnd_Gsra_Ira_Surr_Chg;
    private DbsField pnd_Part_Wfile_Pnd_Da_Death_Surr_Right;
    private DbsField pnd_Part_Wfile_Pnd_Gra_Surr_Right;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Num;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Cntrct_Num;
    private DbsField pnd_Part_Wfile_Pnd_Rqst_Id_Key;
    private DbsField pnd_Part_Wfile_Pnd_Mit_Log_Dte_Tme;
    private DbsField pnd_Part_Wfile_Pnd_Mit_Log_Opr_Cde;
    private DbsField pnd_Part_Wfile_Pnd_Mit_Org_Unit_Cde;
    private DbsField pnd_Part_Wfile_Pnd_Mit_Wpid;
    private DbsField pnd_Part_Wfile_Pnd_Mit_Step_Id;
    private DbsField pnd_Part_Wfile_Pnd_Mit_Status_Cde;
    private DbsField pnd_Part_Wfile_Pnd_Extract_Dte;
    private DbsField pnd_Part_Wfile_Pnd_Inst_Name;
    private DbsField pnd_Part_Wfile_Pnd_Mailing_Inst;
    private DbsField pnd_Part_Wfile_Pnd_Pullout_Code;
    private DbsField pnd_Part_Wfile_Pnd_Stat_Of_Issue_Name;
    private DbsField pnd_Part_Wfile_Pnd_Stat_Of_Issue_Code;
    private DbsField pnd_Part_Wfile_Pnd_Original_Issue_Stat_Cd;
    private DbsField pnd_Part_Wfile_Pnd_Ppg_Code;
    private DbsField pnd_Part_Wfile_Pnd_Lob_Code;
    private DbsField pnd_Part_Wfile_Pnd_Lob_Type;
    private DbsField pnd_Part_Wfile_Pnd_Region_Code;
    private DbsField pnd_Part_Wfile_Pnd_Personal_Annuity;
    private DbsField pnd_Part_Wfile_Pnd_Grade_Amt_Ind;
    private DbsField pnd_Part_Wfile_Pnd_Reval_Cde;
    private DbsField pnd_Part_Wfile_Pnd_First_Payment_Aft_Issue;
    private DbsField pnd_Part_Wfile_Pnd_Annual_Req_Dis_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Annual_Req_Dist;

    private DbsGroup pnd_Part_Wfile__R_Field_34;
    private DbsField pnd_Part_Wfile_Pnd_Annual_Req_Dist_N;
    private DbsField pnd_Part_Wfile_Pnd_Req_Begin_Dte;
    private DbsField pnd_Part_Wfile_Pnd_First_Req_Pymt_Amt_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_First_Req_Pymt_Amt;

    private DbsGroup pnd_Part_Wfile__R_Field_35;
    private DbsField pnd_Part_Wfile_Pnd_First_Req_Pymt_Amt_N;
    private DbsField pnd_Part_Wfile_Pnd_Ownership_Cde;
    private DbsField pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Type;
    private DbsField pnd_Part_Wfile_Pnd_Rea_Cntrct_Type;
    private DbsField pnd_Part_Wfile_Pnd_Cref_Cntrct_Type;
    private DbsField pnd_Part_Wfile_Pnd_Check_Mailing_Addr;
    private DbsField pnd_Part_Wfile_Pnd_Bank_Accnt_No;
    private DbsField pnd_Part_Wfile_Pnd_Bank_Transit_Code;
    private DbsField pnd_Part_Wfile_Pnd_Commuted_Intr_Num;
    private DbsField pnd_Part_Wfile_Pnd_Commuted_Info_Table;

    private DbsGroup pnd_Part_Wfile__R_Field_36;

    private DbsGroup pnd_Part_Wfile_Pnd_Commuted_Inf;
    private DbsField pnd_Part_Wfile_Pnd_Comut_Grnted_Amt_Dollar;
    private DbsField pnd_Part_Wfile_Pnd_Comut_Grnted_Amt;
    private DbsField pnd_Part_Wfile_Pnd_Comut_Intr_Rate;
    private DbsField pnd_Part_Wfile_Pnd_Comut_Pymt_Method;
    private DbsField pnd_Part_Wfile_Pnd_Comut_Mortality_Basis;
    private DbsField pnd_Part_Wfile_Pnd_Cis_Filler;
    private DbsField pnd_Txt1;
    private DbsField pnd_Txt2;
    private DbsField pnd_J;
    private DbsField pnd_I;
    private DbsField pnd_Full_Date;
    private DbsField pnd_Tiaa_Date;

    private DbsGroup pnd_Tiaa_Date__R_Field_37;
    private DbsField pnd_Tiaa_Date_Pnd_Yyyy;
    private DbsField pnd_Tiaa_Date_Pnd_Mm;
    private DbsField pnd_Tiaa_Date_Pnd_Dd;
    private DbsField pnd_Job;
    private DbsField pnd_Job_Title;
    private DbsField pnd_Old_Id;
    private DbsField pnd_Id;
    private DbsField pnd_Full_Name;
    private DbsField pnd_N;
    private DbsField pnd_Fr_Nx;
    private DbsField pnd_Lst_Len;
    private DbsField pnd_Frst_Len;
    private DbsField pnd_Mid_Len;
    private DbsField pnd_Prfx_Len;
    private DbsField pnd_Begin;
    private DbsField pnd_Begin1;
    private DbsField pnd_Begin2;
    private DbsField pnd_Last_Name;
    private DbsField pnd_Frst_Name;
    private DbsField pnd_Mid_Name;
    private DbsField pnd_Prfx;
    private DbsField pnd_Count_Job;
    private DbsField pnd_Grd_Tot;
    private DbsField pnd_Type;
    private DbsField pnd_Status;
    private DbsField pnd_Fld1;
    private DbsField pnd_Id_Cont;
    private DbsField pnd_Id_Name1;
    private DbsField pnd_Id_Name2;
    private DbsField pnd_Line1;
    private DbsField pnd_Uln1;
    private DbsField pnd_C_Ty;
    private DbsField pnd_Tot;
    private DbsField pnd_Tot_Record;
    private DbsField pnd_Job_Rsn;
    private DbsField pnd_Job_Desc_1;
    private DbsField pnd_Job_Desc_2;
    private DbsField pnd_Contract_Num;
    private DbsField pnd_Input_Parm;
    private DbsField pnd_Reprint;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Part_Wfile = localVariables.newGroupInRecord("pnd_Part_Wfile", "#PART-WFILE");
        pnd_Part_Wfile_Pnd_Job_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Job_Name", "#JOB-NAME", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_Job_Reason = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Job_Reason", "#JOB-REASON", FieldType.STRING, 32);
        pnd_Part_Wfile_Pnd_Cntrct_Type = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cntrct_Type", "#CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Cntrct_Approval = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cntrct_Approval", "#CNTRCT-APPROVAL", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Cntrct_Sys_Id = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cntrct_Sys_Id", "#CNTRCT-SYS-ID", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_Cntrct_Option = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cntrct_Option", "#CNTRCT-OPTION", FieldType.STRING, 10);
        pnd_Part_Wfile_Pnd_Mdo_Cntrct_Cash_Status = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Cntrct_Cash_Status", "#MDO-CNTRCT-CASH-STATUS", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Cntrct_Retr_Surv_Status = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cntrct_Retr_Surv_Status", "#CNTRCT-RETR-SURV-STATUS", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Header_1 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Header_1", "#TIAA-CNTRCT-HEADER-1", FieldType.STRING, 
            85);
        pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Header_2 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Header_2", "#TIAA-CNTRCT-HEADER-2", FieldType.STRING, 
            85);
        pnd_Part_Wfile_Pnd_Cref_Cntrct_Header_1 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Cntrct_Header_1", "#CREF-CNTRCT-HEADER-1", FieldType.STRING, 
            85);
        pnd_Part_Wfile_Pnd_Cref_Cntrct_Header_2 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Cntrct_Header_2", "#CREF-CNTRCT-HEADER-2", FieldType.STRING, 
            85);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Cntrct_Header_1 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Cntrct_Header_1", "#TIAA-REA-CNTRCT-HEADER-1", 
            FieldType.STRING, 85);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Cntrct_Header_2 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Cntrct_Header_2", "#TIAA-REA-CNTRCT-HEADER-2", 
            FieldType.STRING, 85);
        pnd_Part_Wfile_Pnd_Tiaa_Income_Option = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Income_Option", "#TIAA-INCOME-OPTION", FieldType.STRING, 
            85);
        pnd_Part_Wfile_Pnd_Cref_Income_Option = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Income_Option", "#CREF-INCOME-OPTION", FieldType.STRING, 
            85);
        pnd_Part_Wfile_Pnd_Cntrct_Mergeset_Id = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cntrct_Mergeset_Id", "#CNTRCT-MERGESET-ID", FieldType.STRING, 
            18);

        pnd_Part_Wfile__R_Field_1 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_1", "REDEFINE", pnd_Part_Wfile_Pnd_Cntrct_Mergeset_Id);
        pnd_Part_Wfile_Pnd_Tiaa_Cntrct_A = pnd_Part_Wfile__R_Field_1.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Cntrct_A", "#TIAA-CNTRCT-A", FieldType.STRING, 
            10);
        pnd_Part_Wfile_Pnd_Tiaa_Iss_Dte = pnd_Part_Wfile__R_Field_1.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Iss_Dte", "#TIAA-ISS-DTE", FieldType.STRING, 
            8);
        pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Number = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Number", "#TIAA-CNTRCT-NUMBER", FieldType.STRING, 
            12);

        pnd_Part_Wfile__R_Field_2 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_2", "REDEFINE", pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Number);
        pnd_Part_Wfile_Pnd_Tiaa_F = pnd_Part_Wfile__R_Field_2.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_F", "#TIAA-F", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Tiaa_Dash_1 = pnd_Part_Wfile__R_Field_2.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Dash_1", "#TIAA-DASH-1", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Tiaa_6 = pnd_Part_Wfile__R_Field_2.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_6", "#TIAA-6", FieldType.STRING, 6);
        pnd_Part_Wfile_Pnd_Tiaa_Dash_2 = pnd_Part_Wfile__R_Field_2.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Dash_2", "#TIAA-DASH-2", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Tiaa_L = pnd_Part_Wfile__R_Field_2.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_L", "#TIAA-L", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Tiaa_R = pnd_Part_Wfile__R_Field_2.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_R", "#TIAA-R", FieldType.STRING, 2);
        pnd_Part_Wfile_Pnd_Tiaa_Real_Estate_Num = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Real_Estate_Num", "#TIAA-REAL-ESTATE-NUM", FieldType.STRING, 
            16);

        pnd_Part_Wfile__R_Field_3 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_3", "REDEFINE", pnd_Part_Wfile_Pnd_Tiaa_Real_Estate_Num);
        pnd_Part_Wfile_Pnd_Tiaa_R_F = pnd_Part_Wfile__R_Field_3.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_R_F", "#TIAA-R-F", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Tiaa_Dash_R_1 = pnd_Part_Wfile__R_Field_3.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Dash_R_1", "#TIAA-DASH-R-1", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Tiaa_R_6 = pnd_Part_Wfile__R_Field_3.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_R_6", "#TIAA-R-6", FieldType.STRING, 6);
        pnd_Part_Wfile_Pnd_Tiaa_Dash_R_2 = pnd_Part_Wfile__R_Field_3.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Dash_R_2", "#TIAA-DASH-R-2", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Tiaa_R_L = pnd_Part_Wfile__R_Field_3.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_R_L", "#TIAA-R-L", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Tiaa_Dash_R_3 = pnd_Part_Wfile__R_Field_3.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Dash_R_3", "#TIAA-DASH-R-3", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Tiaa_R_Rea = pnd_Part_Wfile__R_Field_3.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_R_Rea", "#TIAA-R-REA", FieldType.STRING, 3);
        pnd_Part_Wfile_Pnd_Tiaa_R_R = pnd_Part_Wfile__R_Field_3.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_R_R", "#TIAA-R-R", FieldType.STRING, 2);
        pnd_Part_Wfile_Pnd_Cref_Cntrct_Number = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Cntrct_Number", "#CREF-CNTRCT-NUMBER", FieldType.STRING, 
            12);

        pnd_Part_Wfile__R_Field_4 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_4", "REDEFINE", pnd_Part_Wfile_Pnd_Cref_Cntrct_Number);
        pnd_Part_Wfile_Pnd_Cref_F = pnd_Part_Wfile__R_Field_4.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_F", "#CREF-F", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Cref_Dash_1 = pnd_Part_Wfile__R_Field_4.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Dash_1", "#CREF-DASH-1", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Cref_6 = pnd_Part_Wfile__R_Field_4.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_6", "#CREF-6", FieldType.STRING, 6);
        pnd_Part_Wfile_Pnd_Cref_Dash_2 = pnd_Part_Wfile__R_Field_4.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Dash_2", "#CREF-DASH-2", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Cref_L = pnd_Part_Wfile__R_Field_4.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_L", "#CREF-L", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Reef_R = pnd_Part_Wfile__R_Field_4.newFieldInGroup("pnd_Part_Wfile_Pnd_Reef_R", "#REEF-R", FieldType.STRING, 2);
        pnd_Part_Wfile_Pnd_Tiaa_Issue_Date = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Issue_Date", "#TIAA-ISSUE-DATE", FieldType.STRING, 
            8);
        pnd_Part_Wfile_Pnd_Cref_Issue_Date = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Issue_Date", "#CREF-ISSUE-DATE", FieldType.STRING, 
            8);
        pnd_Part_Wfile_Pnd_Freq_Of_Payment = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Freq_Of_Payment", "#FREQ-OF-PAYMENT", FieldType.STRING, 
            13);
        pnd_Part_Wfile_Pnd_First_Payment_Date = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Payment_Date", "#FIRST-PAYMENT-DATE", FieldType.STRING, 
            8);
        pnd_Part_Wfile_Pnd_Last_Payment_Date = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Last_Payment_Date", "#LAST-PAYMENT-DATE", FieldType.STRING, 
            8);
        pnd_Part_Wfile_Pnd_Guaranteed_Period = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Guaranteed_Period", "#GUARANTEED-PERIOD", FieldType.STRING, 
            18);

        pnd_Part_Wfile__R_Field_5 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_5", "REDEFINE", pnd_Part_Wfile_Pnd_Guaranteed_Period);
        pnd_Part_Wfile_Pnd_G_Yy = pnd_Part_Wfile__R_Field_5.newFieldInGroup("pnd_Part_Wfile_Pnd_G_Yy", "#G-YY", FieldType.STRING, 2);
        pnd_Part_Wfile_Pnd_G_Filler1 = pnd_Part_Wfile__R_Field_5.newFieldInGroup("pnd_Part_Wfile_Pnd_G_Filler1", "#G-FILLER1", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_G_Years = pnd_Part_Wfile__R_Field_5.newFieldInGroup("pnd_Part_Wfile_Pnd_G_Years", "#G-YEARS", FieldType.STRING, 5);
        pnd_Part_Wfile_Pnd_G_Filler2 = pnd_Part_Wfile__R_Field_5.newFieldInGroup("pnd_Part_Wfile_Pnd_G_Filler2", "#G-FILLER2", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_G_Dd = pnd_Part_Wfile__R_Field_5.newFieldInGroup("pnd_Part_Wfile_Pnd_G_Dd", "#G-DD", FieldType.STRING, 3);
        pnd_Part_Wfile_Pnd_G_Filler3 = pnd_Part_Wfile__R_Field_5.newFieldInGroup("pnd_Part_Wfile_Pnd_G_Filler3", "#G-FILLER3", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_G_Days = pnd_Part_Wfile__R_Field_5.newFieldInGroup("pnd_Part_Wfile_Pnd_G_Days", "#G-DAYS", FieldType.STRING, 4);
        pnd_Part_Wfile_Pnd_G_Filler_4 = pnd_Part_Wfile__R_Field_5.newFieldInGroup("pnd_Part_Wfile_Pnd_G_Filler_4", "#G-FILLER-4", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_First_Guaranteed_P_Date = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Guaranteed_P_Date", "#FIRST-GUARANTEED-P-DATE", 
            FieldType.STRING, 14);

        pnd_Part_Wfile__R_Field_6 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_6", "REDEFINE", pnd_Part_Wfile_Pnd_First_Guaranteed_P_Date);
        pnd_Part_Wfile_Pnd_G_P_Mm = pnd_Part_Wfile__R_Field_6.newFieldInGroup("pnd_Part_Wfile_Pnd_G_P_Mm", "#G-P-MM", FieldType.STRING, 2);
        pnd_Part_Wfile_Pnd_G_P_B1 = pnd_Part_Wfile__R_Field_6.newFieldInGroup("pnd_Part_Wfile_Pnd_G_P_B1", "#G-P-B1", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_G_P_Dd = pnd_Part_Wfile__R_Field_6.newFieldInGroup("pnd_Part_Wfile_Pnd_G_P_Dd", "#G-P-DD", FieldType.STRING, 2);
        pnd_Part_Wfile_Pnd_G_P_B2 = pnd_Part_Wfile__R_Field_6.newFieldInGroup("pnd_Part_Wfile_Pnd_G_P_B2", "#G-P-B2", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_G_P_Ccyy = pnd_Part_Wfile__R_Field_6.newFieldInGroup("pnd_Part_Wfile_Pnd_G_P_Ccyy", "#G-P-CCYY", FieldType.STRING, 4);
        pnd_Part_Wfile_Pnd_End_Guaranteed_P_Date = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_End_Guaranteed_P_Date", "#END-GUARANTEED-P-DATE", 
            FieldType.STRING, 14);

        pnd_Part_Wfile__R_Field_7 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_7", "REDEFINE", pnd_Part_Wfile_Pnd_End_Guaranteed_P_Date);
        pnd_Part_Wfile_Pnd_E_G_Mm = pnd_Part_Wfile__R_Field_7.newFieldInGroup("pnd_Part_Wfile_Pnd_E_G_Mm", "#E-G-MM", FieldType.STRING, 2);
        pnd_Part_Wfile_Pnd_E_G_B1 = pnd_Part_Wfile__R_Field_7.newFieldInGroup("pnd_Part_Wfile_Pnd_E_G_B1", "#E-G-B1", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_E_G_Dd = pnd_Part_Wfile__R_Field_7.newFieldInGroup("pnd_Part_Wfile_Pnd_E_G_Dd", "#E-G-DD", FieldType.STRING, 2);
        pnd_Part_Wfile_Pnd_E_G_B2 = pnd_Part_Wfile__R_Field_7.newFieldInGroup("pnd_Part_Wfile_Pnd_E_G_B2", "#E-G-B2", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_E_G_Ccyy = pnd_Part_Wfile__R_Field_7.newFieldInGroup("pnd_Part_Wfile_Pnd_E_G_Ccyy", "#E-G-CCYY", FieldType.STRING, 4);
        pnd_Part_Wfile_Pnd_Tiaa_Form_Num_Pg3_4 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Form_Num_Pg3_4", "#TIAA-FORM-NUM-PG3-4", FieldType.STRING, 
            20);
        pnd_Part_Wfile_Pnd_Tiaa_Form_Num_Pg5_6 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Form_Num_Pg5_6", "#TIAA-FORM-NUM-PG5-6", FieldType.STRING, 
            20);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Form_Num_Pg3_4 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Form_Num_Pg3_4", "#TIAA-REA-FORM-NUM-PG3-4", 
            FieldType.STRING, 20);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Form_Num_Pg5_6 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Form_Num_Pg5_6", "#TIAA-REA-FORM-NUM-PG5-6", 
            FieldType.STRING, 20);
        pnd_Part_Wfile_Pnd_Cref_Form_Num_Pg3_4 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Form_Num_Pg3_4", "#CREF-FORM-NUM-PG3-4", FieldType.STRING, 
            20);
        pnd_Part_Wfile_Pnd_Cref_Form_Num_Pg5_6 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Form_Num_Pg5_6", "#CREF-FORM-NUM-PG5-6", FieldType.STRING, 
            20);
        pnd_Part_Wfile_Pnd_Tiaa_Product_Cde_3_4 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Product_Cde_3_4", "#TIAA-PRODUCT-CDE-3-4", FieldType.STRING, 
            35);
        pnd_Part_Wfile_Pnd_Tiaa_Product_Cde_5_6 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Product_Cde_5_6", "#TIAA-PRODUCT-CDE-5-6", FieldType.STRING, 
            35);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Product_Cde_3_4 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Product_Cde_3_4", "#TIAA-REA-PRODUCT-CDE-3-4", 
            FieldType.STRING, 35);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Product_Cde_5_6 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Product_Cde_5_6", "#TIAA-REA-PRODUCT-CDE-5-6", 
            FieldType.STRING, 35);
        pnd_Part_Wfile_Pnd_Cref_Product_Cde_3_4 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Product_Cde_3_4", "#CREF-PRODUCT-CDE-3-4", FieldType.STRING, 
            35);
        pnd_Part_Wfile_Pnd_Cref_Product_Cde_5_6 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Product_Cde_5_6", "#CREF-PRODUCT-CDE-5-6", FieldType.STRING, 
            35);
        pnd_Part_Wfile_Pnd_Tiaa_Edition_Num_Pg3_4 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Edition_Num_Pg3_4", "#TIAA-EDITION-NUM-PG3-4", 
            FieldType.STRING, 15);
        pnd_Part_Wfile_Pnd_Tiaa_Edition_Num_Pg5_6 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Edition_Num_Pg5_6", "#TIAA-EDITION-NUM-PG5-6", 
            FieldType.STRING, 15);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Edition_Num_Pg3_4 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Edition_Num_Pg3_4", "#TIAA-REA-EDITION-NUM-PG3-4", 
            FieldType.STRING, 15);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Edition_Num_Pg5_6 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Edition_Num_Pg5_6", "#TIAA-REA-EDITION-NUM-PG5-6", 
            FieldType.STRING, 15);
        pnd_Part_Wfile_Pnd_Cref_Edition_Num_Pg3_4 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Edition_Num_Pg3_4", "#CREF-EDITION-NUM-PG3-4", 
            FieldType.STRING, 15);
        pnd_Part_Wfile_Pnd_Cref_Edition_Num_Pg5_6 = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Edition_Num_Pg5_6", "#CREF-EDITION-NUM-PG5-6", 
            FieldType.STRING, 15);
        pnd_Part_Wfile_Pnd_Pin_Number = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.NUMERIC, 12);
        pnd_Part_Wfile_Pnd_First_Annu_Cntrct_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annu_Cntrct_Name", "#FIRST-ANNU-CNTRCT-NAME", 
            FieldType.STRING, 72);

        pnd_Part_Wfile__R_Field_8 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_8", "REDEFINE", pnd_Part_Wfile_Pnd_First_Annu_Cntrct_Name);
        pnd_Part_Wfile_Pnd_First_Annu_Name = pnd_Part_Wfile__R_Field_8.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annu_Name", "#FIRST-ANNU-NAME", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Filler_1 = pnd_Part_Wfile__R_Field_8.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_1", "#FILLER-1", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Middle_Int = pnd_Part_Wfile__R_Field_8.newFieldInGroup("pnd_Part_Wfile_Pnd_Middle_Int", "#MIDDLE-INT", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Filler_2 = pnd_Part_Wfile__R_Field_8.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Last_Annu_Name = pnd_Part_Wfile__R_Field_8.newFieldInGroup("pnd_Part_Wfile_Pnd_Last_Annu_Name", "#LAST-ANNU-NAME", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Filler_3 = pnd_Part_Wfile__R_Field_8.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_3", "#FILLER-3", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Suffix = pnd_Part_Wfile__R_Field_8.newFieldInGroup("pnd_Part_Wfile_Pnd_Suffix", "#SUFFIX", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_First_Annu_Last_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annu_Last_Name", "#FIRST-ANNU-LAST-NAME", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_First_Annu_First_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annu_First_Name", "#FIRST-ANNU-FIRST-NAME", 
            FieldType.STRING, 30);
        pnd_Part_Wfile_Pnd_First_Annu_Mid_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annu_Mid_Name", "#FIRST-ANNU-MID-NAME", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_First_Annu_Title = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annu_Title", "#FIRST-ANNU-TITLE", FieldType.STRING, 
            8);
        pnd_Part_Wfile_Pnd_First_Annu_Suffix = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annu_Suffix", "#FIRST-ANNU-SUFFIX", FieldType.STRING, 
            8);
        pnd_Part_Wfile_Pnd_First_Annu_Citizenship = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annu_Citizenship", "#FIRST-ANNU-CITIZENSHIP", 
            FieldType.STRING, 25);
        pnd_Part_Wfile_Pnd_First_Annu_Ssn = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annu_Ssn", "#FIRST-ANNU-SSN", FieldType.STRING, 11);
        pnd_Part_Wfile_Pnd_First_Annu_Dob = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annu_Dob", "#FIRST-ANNU-DOB", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_First_Annu_Res_State = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annu_Res_State", "#FIRST-ANNU-RES-STATE", FieldType.STRING, 
            2);
        pnd_Part_Wfile_Pnd_Annu_Address_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Annu_Address_Name", "#ANNU-ADDRESS-NAME", FieldType.STRING, 
            81);

        pnd_Part_Wfile__R_Field_9 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_9", "REDEFINE", pnd_Part_Wfile_Pnd_Annu_Address_Name);
        pnd_Part_Wfile_Pnd_Prefix_A = pnd_Part_Wfile__R_Field_9.newFieldInGroup("pnd_Part_Wfile_Pnd_Prefix_A", "#PREFIX-A", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_Filler_A1 = pnd_Part_Wfile__R_Field_9.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_A1", "#FILLER-A1", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_First_Name_A = pnd_Part_Wfile__R_Field_9.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Name_A", "#FIRST-NAME-A", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Filler_A2 = pnd_Part_Wfile__R_Field_9.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_A2", "#FILLER-A2", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Middle_Init_A = pnd_Part_Wfile__R_Field_9.newFieldInGroup("pnd_Part_Wfile_Pnd_Middle_Init_A", "#MIDDLE-INIT-A", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Filler_A3 = pnd_Part_Wfile__R_Field_9.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_A3", "#FILLER-A3", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Last_Name_A = pnd_Part_Wfile__R_Field_9.newFieldInGroup("pnd_Part_Wfile_Pnd_Last_Name_A", "#LAST-NAME-A", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Filler_A4 = pnd_Part_Wfile__R_Field_9.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_A4", "#FILLER-A4", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Suffix_A = pnd_Part_Wfile__R_Field_9.newFieldInGroup("pnd_Part_Wfile_Pnd_Suffix_A", "#SUFFIX-A", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_Annu_Welcome_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Annu_Welcome_Name", "#ANNU-WELCOME-NAME", FieldType.STRING, 
            39);

        pnd_Part_Wfile__R_Field_10 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_10", "REDEFINE", pnd_Part_Wfile_Pnd_Annu_Welcome_Name);
        pnd_Part_Wfile_Pnd_Prefix_W = pnd_Part_Wfile__R_Field_10.newFieldInGroup("pnd_Part_Wfile_Pnd_Prefix_W", "#PREFIX-W", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_Filler_W = pnd_Part_Wfile__R_Field_10.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_W", "#FILLER-W", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Last_Name_W = pnd_Part_Wfile__R_Field_10.newFieldInGroup("pnd_Part_Wfile_Pnd_Last_Name_W", "#LAST-NAME-W", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Annu_Dir_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Annu_Dir_Name", "#ANNU-DIR-NAME", FieldType.STRING, 63);

        pnd_Part_Wfile__R_Field_11 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_11", "REDEFINE", pnd_Part_Wfile_Pnd_Annu_Dir_Name);
        pnd_Part_Wfile_Pnd_First_Name_D = pnd_Part_Wfile__R_Field_11.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Name_D", "#FIRST-NAME-D", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Filler_D1 = pnd_Part_Wfile__R_Field_11.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_D1", "#FILLER-D1", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Mid_Name_D = pnd_Part_Wfile__R_Field_11.newFieldInGroup("pnd_Part_Wfile_Pnd_Mid_Name_D", "#MID-NAME-D", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Filler_D2 = pnd_Part_Wfile__R_Field_11.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_D2", "#FILLER-D2", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Last_Name_D = pnd_Part_Wfile__R_Field_11.newFieldInGroup("pnd_Part_Wfile_Pnd_Last_Name_D", "#LAST-NAME-D", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_First_A_C_M = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_A_C_M", "#FIRST-A-C-M", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_First_A_C_M_Literal = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_A_C_M_Literal", "#FIRST-A-C-M-LITERAL", FieldType.STRING, 
            15);
        pnd_Part_Wfile_Pnd_Calc_Participant = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Calc_Participant", "#CALC-PARTICIPANT", FieldType.STRING, 
            13);
        pnd_Part_Wfile_Pnd_First_Annuit_Mail_Addr1 = pnd_Part_Wfile.newFieldArrayInGroup("pnd_Part_Wfile_Pnd_First_Annuit_Mail_Addr1", "#FIRST-ANNUIT-MAIL-ADDR1", 
            FieldType.STRING, 35, new DbsArrayController(1, 5));
        pnd_Part_Wfile_Pnd_First_Annuit_Zipcde = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Annuit_Zipcde", "#FIRST-ANNUIT-ZIPCDE", FieldType.STRING, 
            9);
        pnd_Part_Wfile_Pnd_Second_Annuit_Cntrct_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Second_Annuit_Cntrct_Name", "#SECOND-ANNUIT-CNTRCT-NAME", 
            FieldType.STRING, 63);

        pnd_Part_Wfile__R_Field_12 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_12", "REDEFINE", pnd_Part_Wfile_Pnd_Second_Annuit_Cntrct_Name);
        pnd_Part_Wfile_Pnd_First_Name_S = pnd_Part_Wfile__R_Field_12.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Name_S", "#FIRST-NAME-S", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Filler_S1 = pnd_Part_Wfile__R_Field_12.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_S1", "#FILLER-S1", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Mid_Name_S = pnd_Part_Wfile__R_Field_12.newFieldInGroup("pnd_Part_Wfile_Pnd_Mid_Name_S", "#MID-NAME-S", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Filler_S2 = pnd_Part_Wfile__R_Field_12.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_S2", "#FILLER-S2", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Last_Name_S = pnd_Part_Wfile__R_Field_12.newFieldInGroup("pnd_Part_Wfile_Pnd_Last_Name_S", "#LAST-NAME-S", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Second_Annuit_Ssn = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Second_Annuit_Ssn", "#SECOND-ANNUIT-SSN", FieldType.STRING, 
            11);
        pnd_Part_Wfile_Pnd_Second_Annuit_Dob = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Second_Annuit_Dob", "#SECOND-ANNUIT-DOB", FieldType.STRING, 
            8);
        pnd_Part_Wfile_Pnd_Dollar_G = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Dollar_G", "#DOLLAR-G", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Tiaa_Graded_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Graded_Amt", "#TIAA-GRADED-AMT", FieldType.STRING, 
            14);

        pnd_Part_Wfile__R_Field_13 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_13", "REDEFINE", pnd_Part_Wfile_Pnd_Tiaa_Graded_Amt);
        pnd_Part_Wfile_Pnd_Tiaa_Graded_Amt_N = pnd_Part_Wfile__R_Field_13.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Graded_Amt_N", "#TIAA-GRADED-AMT-N", 
            FieldType.NUMERIC, 14, 2);
        pnd_Part_Wfile_Pnd_Dollar_Tot = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Dollar_Tot", "#DOLLAR-TOT", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Tiaa_Standard_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Standard_Amt", "#TIAA-STANDARD-AMT", FieldType.STRING, 
            14);

        pnd_Part_Wfile__R_Field_14 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_14", "REDEFINE", pnd_Part_Wfile_Pnd_Tiaa_Standard_Amt);
        pnd_Part_Wfile_Pnd_Tiaa_Standard_Amt_N = pnd_Part_Wfile__R_Field_14.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Standard_Amt_N", "#TIAA-STANDARD-AMT-N", 
            FieldType.NUMERIC, 14, 2);
        pnd_Part_Wfile_Pnd_Dollar_S = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Dollar_S", "#DOLLAR-S", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Tiaa_Tot_Grd_Stnd_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Tot_Grd_Stnd_Amt", "#TIAA-TOT-GRD-STND-AMT", 
            FieldType.STRING, 14);

        pnd_Part_Wfile__R_Field_15 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_15", "REDEFINE", pnd_Part_Wfile_Pnd_Tiaa_Tot_Grd_Stnd_Amt);
        pnd_Part_Wfile_Pnd_Tiaa_Tot_Grd_Stnd_Amt_N = pnd_Part_Wfile__R_Field_15.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Tot_Grd_Stnd_Amt_N", "#TIAA-TOT-GRD-STND-AMT-N", 
            FieldType.NUMERIC, 14, 2);
        pnd_Part_Wfile_Pnd_Tiaa_Guarant_Period_Flag = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Guarant_Period_Flag", "#TIAA-GUARANT-PERIOD-FLAG", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Multiple_Com_Int_Rate = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Multiple_Com_Int_Rate", "#MULTIPLE-COM-INT-RATE", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Death_Ben_After_Iss_Flag = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Death_Ben_After_Iss_Flag", "#DEATH-BEN-AFTER-ISS-FLAG", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Tiaa_Com_Info = pnd_Part_Wfile.newFieldArrayInGroup("pnd_Part_Wfile_Pnd_Tiaa_Com_Info", "#TIAA-COM-INFO", FieldType.STRING, 
            28, new DbsArrayController(1, 5));

        pnd_Part_Wfile__R_Field_16 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_16", "REDEFINE", pnd_Part_Wfile_Pnd_Tiaa_Com_Info);

        pnd_Part_Wfile_Pnd_Redefine_Tiaa_Info = pnd_Part_Wfile__R_Field_16.newGroupArrayInGroup("pnd_Part_Wfile_Pnd_Redefine_Tiaa_Info", "#REDEFINE-TIAA-INFO", 
            new DbsArrayController(1, 5));
        pnd_Part_Wfile_Pnd_Dollar_Ci = pnd_Part_Wfile_Pnd_Redefine_Tiaa_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Dollar_Ci", "#DOLLAR-CI", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Comm_Amt_Ci = pnd_Part_Wfile_Pnd_Redefine_Tiaa_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Comm_Amt_Ci", "#COMM-AMT-CI", FieldType.STRING, 
            14);
        pnd_Part_Wfile_Pnd_Comm_Int_Rate_Ci = pnd_Part_Wfile_Pnd_Redefine_Tiaa_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Comm_Int_Rate_Ci", "#COMM-INT-RATE-CI", 
            FieldType.STRING, 5);
        pnd_Part_Wfile_Pnd_Comm_Method_Ci = pnd_Part_Wfile_Pnd_Redefine_Tiaa_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Comm_Method_Ci", "#COMM-METHOD-CI", 
            FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_Surivor_Reduction_Literal = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Surivor_Reduction_Literal", "#SURIVOR-REDUCTION-LITERAL", 
            FieldType.STRING, 10);
        pnd_Part_Wfile_Pnd_Tiaa_Contract_Settled_Flag = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Contract_Settled_Flag", "#TIAA-CONTRACT-SETTLED-FLAG", 
            FieldType.STRING, 2);

        pnd_Part_Wfile__R_Field_17 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_17", "REDEFINE", pnd_Part_Wfile_Pnd_Tiaa_Contract_Settled_Flag);
        pnd_Part_Wfile_Pnd_Tiaa_Contract_Settled_Flag_N = pnd_Part_Wfile__R_Field_17.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Contract_Settled_Flag_N", 
            "#TIAA-CONTRACT-SETTLED-FLAG-N", FieldType.NUMERIC, 2);
        pnd_Part_Wfile_Pnd_Dollar_Pros_Sign = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Dollar_Pros_Sign", "#DOLLAR-PROS-SIGN", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Tot_Tiaa_Paying_Pros_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tot_Tiaa_Paying_Pros_Amt", "#TOT-TIAA-PAYING-PROS-AMT", 
            FieldType.STRING, 14);

        pnd_Part_Wfile__R_Field_18 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_18", "REDEFINE", pnd_Part_Wfile_Pnd_Tot_Tiaa_Paying_Pros_Amt);
        pnd_Part_Wfile_Pnd_Tot_Tiaa_Paying_Pros_Amt_N = pnd_Part_Wfile__R_Field_18.newFieldInGroup("pnd_Part_Wfile_Pnd_Tot_Tiaa_Paying_Pros_Amt_N", "#TOT-TIAA-PAYING-PROS-AMT-N", 
            FieldType.NUMERIC, 14, 2);
        pnd_Part_Wfile_Pnd_Tiaa_Payin_Cntrct_Info = pnd_Part_Wfile.newFieldArrayInGroup("pnd_Part_Wfile_Pnd_Tiaa_Payin_Cntrct_Info", "#TIAA-PAYIN-CNTRCT-INFO", 
            FieldType.STRING, 27, new DbsArrayController(1, 20));

        pnd_Part_Wfile__R_Field_19 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_19", "REDEFINE", pnd_Part_Wfile_Pnd_Tiaa_Payin_Cntrct_Info);

        pnd_Part_Wfile_Pnd_Redefine_Tiaa_C_I = pnd_Part_Wfile__R_Field_19.newGroupArrayInGroup("pnd_Part_Wfile_Pnd_Redefine_Tiaa_C_I", "#REDEFINE-TIAA-C-I", 
            new DbsArrayController(1, 20));
        pnd_Part_Wfile_Pnd_Da_Payin_Cntrct = pnd_Part_Wfile_Pnd_Redefine_Tiaa_C_I.newFieldInGroup("pnd_Part_Wfile_Pnd_Da_Payin_Cntrct", "#DA-PAYIN-CNTRCT", 
            FieldType.STRING, 12);

        pnd_Part_Wfile__R_Field_20 = pnd_Part_Wfile_Pnd_Redefine_Tiaa_C_I.newGroupInGroup("pnd_Part_Wfile__R_Field_20", "REDEFINE", pnd_Part_Wfile_Pnd_Da_Payin_Cntrct);
        pnd_Part_Wfile_Pnd_Da_Payin_Cntrct_N = pnd_Part_Wfile__R_Field_20.newFieldInGroup("pnd_Part_Wfile_Pnd_Da_Payin_Cntrct_N", "#DA-PAYIN-CNTRCT-N", 
            FieldType.NUMERIC, 12);
        pnd_Part_Wfile_Pnd_Process_Dollar = pnd_Part_Wfile_Pnd_Redefine_Tiaa_C_I.newFieldInGroup("pnd_Part_Wfile_Pnd_Process_Dollar", "#PROCESS-$", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Process_Amt = pnd_Part_Wfile_Pnd_Redefine_Tiaa_C_I.newFieldInGroup("pnd_Part_Wfile_Pnd_Process_Amt", "#PROCESS-AMT", FieldType.STRING, 
            14);

        pnd_Part_Wfile__R_Field_21 = pnd_Part_Wfile_Pnd_Redefine_Tiaa_C_I.newGroupInGroup("pnd_Part_Wfile__R_Field_21", "REDEFINE", pnd_Part_Wfile_Pnd_Process_Amt);
        pnd_Part_Wfile_Pnd_Process_Amt_N = pnd_Part_Wfile__R_Field_21.newFieldInGroup("pnd_Part_Wfile_Pnd_Process_Amt_N", "#PROCESS-AMT-N", FieldType.NUMERIC, 
            14, 2);
        pnd_Part_Wfile_Pnd_Post_Ret_Trans_Flag = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Post_Ret_Trans_Flag", "#POST-RET-TRANS-FLAG", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Transfer_Units = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Transfer_Units", "#TRANSFER-UNITS", FieldType.STRING, 9);
        pnd_Part_Wfile_Pnd_Transfer_Payout_Cntrct = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Transfer_Payout_Cntrct", "#TRANSFER-PAYOUT-CNTRCT", 
            FieldType.STRING, 12);
        pnd_Part_Wfile_Pnd_Transfer_Fund_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Transfer_Fund_Name", "#TRANSFER-FUND-NAME", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Transfer_Cmp_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Transfer_Cmp_Name", "#TRANSFER-CMP-NAME", FieldType.STRING, 
            4);
        pnd_Part_Wfile_Pnd_Transfer_Mode = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Transfer_Mode", "#TRANSFER-MODE", FieldType.STRING, 13);
        pnd_Part_Wfile_Pnd_Num_Cref_Payout_Account = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Num_Cref_Payout_Account", "#NUM-CREF-PAYOUT-ACCOUNT", 
            FieldType.STRING, 2);

        pnd_Part_Wfile__R_Field_22 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_22", "REDEFINE", pnd_Part_Wfile_Pnd_Num_Cref_Payout_Account);
        pnd_Part_Wfile_Pnd_Num_Cref_Payout_Account_N = pnd_Part_Wfile__R_Field_22.newFieldInGroup("pnd_Part_Wfile_Pnd_Num_Cref_Payout_Account_N", "#NUM-CREF-PAYOUT-ACCOUNT-N", 
            FieldType.NUMERIC, 2);
        pnd_Part_Wfile_Pnd_Cref_Payout_Info = pnd_Part_Wfile.newFieldArrayInGroup("pnd_Part_Wfile_Pnd_Cref_Payout_Info", "#CREF-PAYOUT-INFO", FieldType.STRING, 
            54, new DbsArrayController(1, 20));

        pnd_Part_Wfile__R_Field_23 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_23", "REDEFINE", pnd_Part_Wfile_Pnd_Cref_Payout_Info);

        pnd_Part_Wfile_Pnd_Redefine_Cref_P_Info = pnd_Part_Wfile__R_Field_23.newGroupArrayInGroup("pnd_Part_Wfile_Pnd_Redefine_Cref_P_Info", "#REDEFINE-CREF-P-INFO", 
            new DbsArrayController(1, 20));
        pnd_Part_Wfile_Pnd_Account = pnd_Part_Wfile_Pnd_Redefine_Cref_P_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Account", "#ACCOUNT", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Monthly_Units = pnd_Part_Wfile_Pnd_Redefine_Cref_P_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Monthly_Units", "#MONTHLY-UNITS", 
            FieldType.STRING, 12);
        pnd_Part_Wfile_Pnd_Annual_Units = pnd_Part_Wfile_Pnd_Redefine_Cref_P_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Annual_Units", "#ANNUAL-UNITS", 
            FieldType.STRING, 12);
        pnd_Part_Wfile_Pnd_Cref_Cntrct_Settled = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Cntrct_Settled", "#CREF-CNTRCT-SETTLED", FieldType.STRING, 
            2);

        pnd_Part_Wfile__R_Field_24 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_24", "REDEFINE", pnd_Part_Wfile_Pnd_Cref_Cntrct_Settled);
        pnd_Part_Wfile_Pnd_Cref_Cntrct_Settled_N = pnd_Part_Wfile__R_Field_24.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Cntrct_Settled_N", "#CREF-CNTRCT-SETTLED-N", 
            FieldType.NUMERIC, 2);
        pnd_Part_Wfile_Pnd_Dollar_Tot_Cref_Dollar = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Dollar_Tot_Cref_Dollar", "#DOLLAR-TOT-CREF-$", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Tot_Cref_Payin_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tot_Cref_Payin_Amt", "#TOT-CREF-PAYIN-AMT", FieldType.STRING, 
            14);
        pnd_Part_Wfile_Pnd_Cref_Payin_Cntrct_Info = pnd_Part_Wfile.newFieldArrayInGroup("pnd_Part_Wfile_Pnd_Cref_Payin_Cntrct_Info", "#CREF-PAYIN-CNTRCT-INFO", 
            FieldType.STRING, 27, new DbsArrayController(1, 20));

        pnd_Part_Wfile__R_Field_25 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_25", "REDEFINE", pnd_Part_Wfile_Pnd_Cref_Payin_Cntrct_Info);

        pnd_Part_Wfile_Pnd_Redefine_Cref_P_C_Info = pnd_Part_Wfile__R_Field_25.newGroupArrayInGroup("pnd_Part_Wfile_Pnd_Redefine_Cref_P_C_Info", "#REDEFINE-CREF-P-C-INFO", 
            new DbsArrayController(1, 20));
        pnd_Part_Wfile_Pnd_Da_Payin_Contract = pnd_Part_Wfile_Pnd_Redefine_Cref_P_C_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Da_Payin_Contract", "#DA-PAYIN-CONTRACT", 
            FieldType.STRING, 12);
        pnd_Part_Wfile_Pnd_Da_Dollar = pnd_Part_Wfile_Pnd_Redefine_Cref_P_C_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Da_Dollar", "#DA-DOLLAR", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Process_Amount = pnd_Part_Wfile_Pnd_Redefine_Cref_P_C_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Process_Amount", "#PROCESS-AMOUNT", 
            FieldType.STRING, 14);

        pnd_Part_Wfile__R_Field_26 = pnd_Part_Wfile_Pnd_Redefine_Cref_P_C_Info.newGroupInGroup("pnd_Part_Wfile__R_Field_26", "REDEFINE", pnd_Part_Wfile_Pnd_Process_Amount);
        pnd_Part_Wfile_Pnd_Process_Amount_N = pnd_Part_Wfile__R_Field_26.newFieldInGroup("pnd_Part_Wfile_Pnd_Process_Amount_N", "#PROCESS-AMOUNT-N", FieldType.NUMERIC, 
            14, 2);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Cnt_Settled = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Cnt_Settled", "#TIAA-REA-CNT-SETTLED", FieldType.STRING, 
            2);

        pnd_Part_Wfile__R_Field_27 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_27", "REDEFINE", pnd_Part_Wfile_Pnd_Tiaa_Rea_Cnt_Settled);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Cnt_Settled_N = pnd_Part_Wfile__R_Field_27.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Cnt_Settled_N", "#TIAA-REA-CNT-SETTLED-N", 
            FieldType.NUMERIC, 2);
        pnd_Part_Wfile_Pnd_Tot_Tiaa_Rea_Dollar = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tot_Tiaa_Rea_Dollar", "#TOT-TIAA-REA-$", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt", "#TOT-TIAA-REA-PAYIN-PROC-AMT", 
            FieldType.STRING, 14);

        pnd_Part_Wfile__R_Field_28 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_28", "REDEFINE", pnd_Part_Wfile_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt);
        pnd_Part_Wfile_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt_N = pnd_Part_Wfile__R_Field_28.newFieldInGroup("pnd_Part_Wfile_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt_N", 
            "#TOT-TIAA-REA-PAYIN-PROC-AMT-N", FieldType.NUMERIC, 14, 2);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Payin_Cntrct_Info = pnd_Part_Wfile.newFieldArrayInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Payin_Cntrct_Info", "#TIAA-REA-PAYIN-CNTRCT-INFO", 
            FieldType.STRING, 27, new DbsArrayController(1, 20));

        pnd_Part_Wfile__R_Field_29 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_29", "REDEFINE", pnd_Part_Wfile_Pnd_Tiaa_Rea_Payin_Cntrct_Info);

        pnd_Part_Wfile_Pnd_Redefine_Tiaa_R_P_C_Info = pnd_Part_Wfile__R_Field_29.newGroupArrayInGroup("pnd_Part_Wfile_Pnd_Redefine_Tiaa_R_P_C_Info", "#REDEFINE-TIAA-R-P-C-INFO", 
            new DbsArrayController(1, 20));
        pnd_Part_Wfile_Pnd_Da_Payin_Cntrct_Info = pnd_Part_Wfile_Pnd_Redefine_Tiaa_R_P_C_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Da_Payin_Cntrct_Info", 
            "#DA-PAYIN-CNTRCT-INFO", FieldType.STRING, 12);
        pnd_Part_Wfile_Pnd_Tiaa_Rea_Payin_Dollar = pnd_Part_Wfile_Pnd_Redefine_Tiaa_R_P_C_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Rea_Payin_Dollar", 
            "#TIAA-REA-PAYIN-$", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Proceeds_Amt_Info = pnd_Part_Wfile_Pnd_Redefine_Tiaa_R_P_C_Info.newFieldInGroup("pnd_Part_Wfile_Pnd_Proceeds_Amt_Info", "#PROCEEDS-AMT-INFO", 
            FieldType.STRING, 14);

        pnd_Part_Wfile__R_Field_30 = pnd_Part_Wfile_Pnd_Redefine_Tiaa_R_P_C_Info.newGroupInGroup("pnd_Part_Wfile__R_Field_30", "REDEFINE", pnd_Part_Wfile_Pnd_Proceeds_Amt_Info);
        pnd_Part_Wfile_Pnd_Proceeds_Amt_Info_N = pnd_Part_Wfile__R_Field_30.newFieldInGroup("pnd_Part_Wfile_Pnd_Proceeds_Amt_Info_N", "#PROCEEDS-AMT-INFO-N", 
            FieldType.NUMERIC, 14, 2);
        pnd_Part_Wfile_Pnd_Rea_First_Annu_Dollar = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Rea_First_Annu_Dollar", "#REA-FIRST-ANNU-$", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Rea_First_Annuity_Payment = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Rea_First_Annuity_Payment", "#REA-FIRST-ANNUITY-PAYMENT", 
            FieldType.STRING, 14);

        pnd_Part_Wfile__R_Field_31 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_31", "REDEFINE", pnd_Part_Wfile_Pnd_Rea_First_Annuity_Payment);
        pnd_Part_Wfile_Pnd_Rea_First_Annuity_Payment_N = pnd_Part_Wfile__R_Field_31.newFieldInGroup("pnd_Part_Wfile_Pnd_Rea_First_Annuity_Payment_N", 
            "#REA-FIRST-ANNUITY-PAYMENT-N", FieldType.NUMERIC, 14, 2);
        pnd_Part_Wfile_Pnd_Rea_Mnth_Units_Payable = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Rea_Mnth_Units_Payable", "#REA-MNTH-UNITS-PAYABLE", 
            FieldType.STRING, 12);
        pnd_Part_Wfile_Pnd_Rea_Annu_Units_Payable = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Rea_Annu_Units_Payable", "#REA-ANNU-UNITS-PAYABLE", 
            FieldType.STRING, 12);
        pnd_Part_Wfile_Pnd_Rea_Survivor_Units_Payable = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Rea_Survivor_Units_Payable", "#REA-SURVIVOR-UNITS-PAYABLE", 
            FieldType.STRING, 12);
        pnd_Part_Wfile_Pnd_Mdo_Trad_Annu_Dollar = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Trad_Annu_Dollar", "#MDO-TRAD-ANNU-$", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Mdo_Traditional_Annu_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Traditional_Annu_Amt", "#MDO-TRADITIONAL-ANNU-AMT", 
            FieldType.STRING, 14);

        pnd_Part_Wfile__R_Field_32 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_32", "REDEFINE", pnd_Part_Wfile_Pnd_Mdo_Traditional_Annu_Amt);
        pnd_Part_Wfile_Pnd_Mdo_Traditional_Annu_Amt_N = pnd_Part_Wfile__R_Field_32.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Traditional_Annu_Amt_N", "#MDO-TRADITIONAL-ANNU-AMT-N", 
            FieldType.NUMERIC, 14, 2);
        pnd_Part_Wfile_Pnd_Mdo_Tiaa_Initial_Dollar = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Tiaa_Initial_Dollar", "#MDO-TIAA-INITIAL-$", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Mdo_Initial_Paymeny_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Initial_Paymeny_Amt", "#MDO-INITIAL-PAYMENY-AMT", 
            FieldType.STRING, 14);
        pnd_Part_Wfile_Pnd_Mdo_Cref_Initial_Dollar = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Cref_Initial_Dollar", "#MDO-CREF-INITIAL-$", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Mdo_Cref_Init_Paymeny_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Cref_Init_Paymeny_Amt", "#MDO-CREF-INIT-PAYMENY-AMT", 
            FieldType.STRING, 14);
        pnd_Part_Wfile_Pnd_Mdo_Tiaa_Exclude_Dollar = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Tiaa_Exclude_Dollar", "#MDO-TIAA-EXCLUDE-$", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Mdo_Tiaa_Exclude_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Tiaa_Exclude_Amt", "#MDO-TIAA-EXCLUDE-AMT", FieldType.STRING, 
            14);
        pnd_Part_Wfile_Pnd_Mdo_Cref_Exclude_Dollar = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Cref_Exclude_Dollar", "#MDO-CREF-EXCLUDE-$", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Mdo_Cref_Exclude_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mdo_Cref_Exclude_Amt", "#MDO-CREF-EXCLUDE-AMT", FieldType.STRING, 
            14);
        pnd_Part_Wfile_Pnd_Traditional_G_I_R = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Traditional_G_I_R", "#TRADITIONAL-G-I-R", FieldType.STRING, 
            6);
        pnd_Part_Wfile_Pnd_Surrender_Charge = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Surrender_Charge", "#SURRENDER-CHARGE", FieldType.STRING, 
            6);
        pnd_Part_Wfile_Pnd_Contigent_Charge = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Contigent_Charge", "#CONTIGENT-CHARGE", FieldType.STRING, 
            6);
        pnd_Part_Wfile_Pnd_Separate_Charge = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Separate_Charge", "#SEPARATE-CHARGE", FieldType.STRING, 
            5);
        pnd_Part_Wfile_Pnd_Orig_Part_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Orig_Part_Name", "#ORIG-PART-NAME", FieldType.STRING, 72);

        pnd_Part_Wfile__R_Field_33 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_33", "REDEFINE", pnd_Part_Wfile_Pnd_Orig_Part_Name);
        pnd_Part_Wfile_Pnd_First_Name_O = pnd_Part_Wfile__R_Field_33.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Name_O", "#FIRST-NAME-O", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Filler_O1 = pnd_Part_Wfile__R_Field_33.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_O1", "#FILLER-O1", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Middle_Init_O = pnd_Part_Wfile__R_Field_33.newFieldInGroup("pnd_Part_Wfile_Pnd_Middle_Init_O", "#MIDDLE-INIT-O", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Filler_O2 = pnd_Part_Wfile__R_Field_33.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_O2", "#FILLER-O2", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Last_Name_O = pnd_Part_Wfile__R_Field_33.newFieldInGroup("pnd_Part_Wfile_Pnd_Last_Name_O", "#LAST-NAME-O", FieldType.STRING, 
            30);
        pnd_Part_Wfile_Pnd_Filler_O3 = pnd_Part_Wfile__R_Field_33.newFieldInGroup("pnd_Part_Wfile_Pnd_Filler_O3", "#FILLER-O3", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Suffix_O = pnd_Part_Wfile__R_Field_33.newFieldInGroup("pnd_Part_Wfile_Pnd_Suffix_O", "#SUFFIX-O", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_Orig_Part_Ssn = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Orig_Part_Ssn", "#ORIG-PART-SSN", FieldType.STRING, 11);
        pnd_Part_Wfile_Pnd_Orig_Part_Dob = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Orig_Part_Dob", "#ORIG-PART-DOB", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_Orig_Part_Dod = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Orig_Part_Dod", "#ORIG-PART-DOD", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_Rbt_Requester = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Rbt_Requester", "#RBT-REQUESTER", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Retir_Trans_Bene_Per = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Retir_Trans_Bene_Per", "#RETIR-TRANS-BENE-PER", FieldType.STRING, 
            3);
        pnd_Part_Wfile_Pnd_Retir_Trans_Bene_Dollar = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Retir_Trans_Bene_Dollar", "#RETIR-TRANS-BENE-$", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Retir_Trans_Bene_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Retir_Trans_Bene_Amt", "#RETIR-TRANS-BENE-AMT", FieldType.STRING, 
            14);
        pnd_Part_Wfile_Pnd_Gsra_Ira_Surr_Chg = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Gsra_Ira_Surr_Chg", "#GSRA-IRA-SURR-CHG", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Da_Death_Surr_Right = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Da_Death_Surr_Right", "#DA-DEATH-SURR-RIGHT", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Gra_Surr_Right = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Gra_Surr_Right", "#GRA-SURR-RIGHT", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Num = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Num", "#TIAA-CNTRCT-NUM", FieldType.STRING, 
            10);
        pnd_Part_Wfile_Pnd_Cref_Cntrct_Num = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Cntrct_Num", "#CREF-CNTRCT-NUM", FieldType.STRING, 
            10);
        pnd_Part_Wfile_Pnd_Rqst_Id_Key = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Rqst_Id_Key", "#RQST-ID-KEY", FieldType.STRING, 35);
        pnd_Part_Wfile_Pnd_Mit_Log_Dte_Tme = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mit_Log_Dte_Tme", "#MIT-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_Part_Wfile_Pnd_Mit_Log_Opr_Cde = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mit_Log_Opr_Cde", "#MIT-LOG-OPR-CDE", FieldType.STRING, 
            8);
        pnd_Part_Wfile_Pnd_Mit_Org_Unit_Cde = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mit_Org_Unit_Cde", "#MIT-ORG-UNIT-CDE", FieldType.STRING, 
            8);
        pnd_Part_Wfile_Pnd_Mit_Wpid = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mit_Wpid", "#MIT-WPID", FieldType.STRING, 6);
        pnd_Part_Wfile_Pnd_Mit_Step_Id = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mit_Step_Id", "#MIT-STEP-ID", FieldType.STRING, 6);
        pnd_Part_Wfile_Pnd_Mit_Status_Cde = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mit_Status_Cde", "#MIT-STATUS-CDE", FieldType.STRING, 4);
        pnd_Part_Wfile_Pnd_Extract_Dte = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Extract_Dte", "#EXTRACT-DTE", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_Inst_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Inst_Name", "#INST-NAME", FieldType.STRING, 76);
        pnd_Part_Wfile_Pnd_Mailing_Inst = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Mailing_Inst", "#MAILING-INST", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Pullout_Code = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Pullout_Code", "#PULLOUT-CODE", FieldType.STRING, 4);
        pnd_Part_Wfile_Pnd_Stat_Of_Issue_Name = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Stat_Of_Issue_Name", "#STAT-OF-ISSUE-NAME", FieldType.STRING, 
            15);
        pnd_Part_Wfile_Pnd_Stat_Of_Issue_Code = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Stat_Of_Issue_Code", "#STAT-OF-ISSUE-CODE", FieldType.STRING, 
            2);
        pnd_Part_Wfile_Pnd_Original_Issue_Stat_Cd = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Original_Issue_Stat_Cd", "#ORIGINAL-ISSUE-STAT-CD", 
            FieldType.STRING, 2);
        pnd_Part_Wfile_Pnd_Ppg_Code = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Ppg_Code", "#PPG-CODE", FieldType.STRING, 6);
        pnd_Part_Wfile_Pnd_Lob_Code = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Lob_Code", "#LOB-CODE", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Lob_Type = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Lob_Type", "#LOB-TYPE", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Region_Code = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Region_Code", "#REGION-CODE", FieldType.STRING, 3);
        pnd_Part_Wfile_Pnd_Personal_Annuity = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Personal_Annuity", "#PERSONAL-ANNUITY", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Grade_Amt_Ind = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Grade_Amt_Ind", "#GRADE-AMT-IND", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Reval_Cde = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Reval_Cde", "#REVAL-CDE", FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_First_Payment_Aft_Issue = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Payment_Aft_Issue", "#FIRST-PAYMENT-AFT-ISSUE", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Annual_Req_Dis_Dollar = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Annual_Req_Dis_Dollar", "#ANNUAL-REQ-DIS-$", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Annual_Req_Dist = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Annual_Req_Dist", "#ANNUAL-REQ-DIST", FieldType.STRING, 
            14);

        pnd_Part_Wfile__R_Field_34 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_34", "REDEFINE", pnd_Part_Wfile_Pnd_Annual_Req_Dist);
        pnd_Part_Wfile_Pnd_Annual_Req_Dist_N = pnd_Part_Wfile__R_Field_34.newFieldInGroup("pnd_Part_Wfile_Pnd_Annual_Req_Dist_N", "#ANNUAL-REQ-DIST-N", 
            FieldType.NUMERIC, 14, 2);
        pnd_Part_Wfile_Pnd_Req_Begin_Dte = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Req_Begin_Dte", "#REQ-BEGIN-DTE", FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_First_Req_Pymt_Amt_Dollar = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Req_Pymt_Amt_Dollar", "#FIRST-REQ-PYMT-AMT-$", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_First_Req_Pymt_Amt = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Req_Pymt_Amt", "#FIRST-REQ-PYMT-AMT", FieldType.STRING, 
            14);

        pnd_Part_Wfile__R_Field_35 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_35", "REDEFINE", pnd_Part_Wfile_Pnd_First_Req_Pymt_Amt);
        pnd_Part_Wfile_Pnd_First_Req_Pymt_Amt_N = pnd_Part_Wfile__R_Field_35.newFieldInGroup("pnd_Part_Wfile_Pnd_First_Req_Pymt_Amt_N", "#FIRST-REQ-PYMT-AMT-N", 
            FieldType.NUMERIC, 14, 2);
        pnd_Part_Wfile_Pnd_Ownership_Cde = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Ownership_Cde", "#OWNERSHIP-CDE", FieldType.NUMERIC, 1);
        pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Type = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Type", "#TIAA-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Rea_Cntrct_Type = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Rea_Cntrct_Type", "#REA-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Cref_Cntrct_Type = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cref_Cntrct_Type", "#CREF-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Part_Wfile_Pnd_Check_Mailing_Addr = pnd_Part_Wfile.newFieldArrayInGroup("pnd_Part_Wfile_Pnd_Check_Mailing_Addr", "#CHECK-MAILING-ADDR", FieldType.STRING, 
            35, new DbsArrayController(1, 5));
        pnd_Part_Wfile_Pnd_Bank_Accnt_No = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Bank_Accnt_No", "#BANK-ACCNT-NO", FieldType.STRING, 21);
        pnd_Part_Wfile_Pnd_Bank_Transit_Code = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Bank_Transit_Code", "#BANK-TRANSIT-CODE", FieldType.STRING, 
            9);
        pnd_Part_Wfile_Pnd_Commuted_Intr_Num = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Commuted_Intr_Num", "#COMMUTED-INTR-NUM", FieldType.NUMERIC, 
            2);
        pnd_Part_Wfile_Pnd_Commuted_Info_Table = pnd_Part_Wfile.newFieldArrayInGroup("pnd_Part_Wfile_Pnd_Commuted_Info_Table", "#COMMUTED-INFO-TABLE", 
            FieldType.STRING, 58, new DbsArrayController(1, 20));

        pnd_Part_Wfile__R_Field_36 = pnd_Part_Wfile.newGroupInGroup("pnd_Part_Wfile__R_Field_36", "REDEFINE", pnd_Part_Wfile_Pnd_Commuted_Info_Table);

        pnd_Part_Wfile_Pnd_Commuted_Inf = pnd_Part_Wfile__R_Field_36.newGroupArrayInGroup("pnd_Part_Wfile_Pnd_Commuted_Inf", "#COMMUTED-INF", new DbsArrayController(1, 
            20));
        pnd_Part_Wfile_Pnd_Comut_Grnted_Amt_Dollar = pnd_Part_Wfile_Pnd_Commuted_Inf.newFieldInGroup("pnd_Part_Wfile_Pnd_Comut_Grnted_Amt_Dollar", "#COMUT-GRNTED-AMT-DOLLAR", 
            FieldType.STRING, 1);
        pnd_Part_Wfile_Pnd_Comut_Grnted_Amt = pnd_Part_Wfile_Pnd_Commuted_Inf.newFieldInGroup("pnd_Part_Wfile_Pnd_Comut_Grnted_Amt", "#COMUT-GRNTED-AMT", 
            FieldType.STRING, 14);
        pnd_Part_Wfile_Pnd_Comut_Intr_Rate = pnd_Part_Wfile_Pnd_Commuted_Inf.newFieldInGroup("pnd_Part_Wfile_Pnd_Comut_Intr_Rate", "#COMUT-INTR-RATE", 
            FieldType.STRING, 5);
        pnd_Part_Wfile_Pnd_Comut_Pymt_Method = pnd_Part_Wfile_Pnd_Commuted_Inf.newFieldInGroup("pnd_Part_Wfile_Pnd_Comut_Pymt_Method", "#COMUT-PYMT-METHOD", 
            FieldType.STRING, 8);
        pnd_Part_Wfile_Pnd_Comut_Mortality_Basis = pnd_Part_Wfile_Pnd_Commuted_Inf.newFieldInGroup("pnd_Part_Wfile_Pnd_Comut_Mortality_Basis", "#COMUT-MORTALITY-BASIS", 
            FieldType.STRING, 30);
        pnd_Part_Wfile_Pnd_Cis_Filler = pnd_Part_Wfile.newFieldInGroup("pnd_Part_Wfile_Pnd_Cis_Filler", "#CIS-FILLER", FieldType.STRING, 101);
        pnd_Txt1 = localVariables.newFieldInRecord("pnd_Txt1", "#TXT1", FieldType.STRING, 14);
        pnd_Txt2 = localVariables.newFieldInRecord("pnd_Txt2", "#TXT2", FieldType.STRING, 14);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Full_Date = localVariables.newFieldInRecord("pnd_Full_Date", "#FULL-DATE", FieldType.STRING, 10);
        pnd_Tiaa_Date = localVariables.newFieldInRecord("pnd_Tiaa_Date", "#TIAA-DATE", FieldType.STRING, 8);

        pnd_Tiaa_Date__R_Field_37 = localVariables.newGroupInRecord("pnd_Tiaa_Date__R_Field_37", "REDEFINE", pnd_Tiaa_Date);
        pnd_Tiaa_Date_Pnd_Yyyy = pnd_Tiaa_Date__R_Field_37.newFieldInGroup("pnd_Tiaa_Date_Pnd_Yyyy", "#YYYY", FieldType.STRING, 4);
        pnd_Tiaa_Date_Pnd_Mm = pnd_Tiaa_Date__R_Field_37.newFieldInGroup("pnd_Tiaa_Date_Pnd_Mm", "#MM", FieldType.STRING, 2);
        pnd_Tiaa_Date_Pnd_Dd = pnd_Tiaa_Date__R_Field_37.newFieldInGroup("pnd_Tiaa_Date_Pnd_Dd", "#DD", FieldType.STRING, 2);
        pnd_Job = localVariables.newFieldInRecord("pnd_Job", "#JOB", FieldType.STRING, 8);
        pnd_Job_Title = localVariables.newFieldInRecord("pnd_Job_Title", "#JOB-TITLE", FieldType.STRING, 33);
        pnd_Old_Id = localVariables.newFieldInRecord("pnd_Old_Id", "#OLD-ID", FieldType.STRING, 3);
        pnd_Id = localVariables.newFieldInRecord("pnd_Id", "#ID", FieldType.STRING, 4);
        pnd_Full_Name = localVariables.newFieldInRecord("pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.INTEGER, 2);
        pnd_Fr_Nx = localVariables.newFieldInRecord("pnd_Fr_Nx", "#FR-NX", FieldType.INTEGER, 2);
        pnd_Lst_Len = localVariables.newFieldInRecord("pnd_Lst_Len", "#LST-LEN", FieldType.INTEGER, 2);
        pnd_Frst_Len = localVariables.newFieldInRecord("pnd_Frst_Len", "#FRST-LEN", FieldType.INTEGER, 2);
        pnd_Mid_Len = localVariables.newFieldInRecord("pnd_Mid_Len", "#MID-LEN", FieldType.INTEGER, 2);
        pnd_Prfx_Len = localVariables.newFieldInRecord("pnd_Prfx_Len", "#PRFX-LEN", FieldType.INTEGER, 2);
        pnd_Begin = localVariables.newFieldInRecord("pnd_Begin", "#BEGIN", FieldType.INTEGER, 2);
        pnd_Begin1 = localVariables.newFieldInRecord("pnd_Begin1", "#BEGIN1", FieldType.INTEGER, 2);
        pnd_Begin2 = localVariables.newFieldInRecord("pnd_Begin2", "#BEGIN2", FieldType.INTEGER, 2);
        pnd_Last_Name = localVariables.newFieldInRecord("pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 30);
        pnd_Frst_Name = localVariables.newFieldInRecord("pnd_Frst_Name", "#FRST-NAME", FieldType.STRING, 30);
        pnd_Mid_Name = localVariables.newFieldInRecord("pnd_Mid_Name", "#MID-NAME", FieldType.STRING, 30);
        pnd_Prfx = localVariables.newFieldInRecord("pnd_Prfx", "#PRFX", FieldType.STRING, 3);
        pnd_Count_Job = localVariables.newFieldArrayInRecord("pnd_Count_Job", "#COUNT-JOB", FieldType.NUMERIC, 5, new DbsArrayController(1, 5));
        pnd_Grd_Tot = localVariables.newFieldInRecord("pnd_Grd_Tot", "#GRD-TOT", FieldType.NUMERIC, 7);
        pnd_Type = localVariables.newFieldInRecord("pnd_Type", "#TYPE", FieldType.STRING, 4);
        pnd_Status = localVariables.newFieldInRecord("pnd_Status", "#STATUS", FieldType.STRING, 8);
        pnd_Fld1 = localVariables.newFieldArrayInRecord("pnd_Fld1", "#FLD1", FieldType.STRING, 2, new DbsArrayController(1, 3));
        pnd_Id_Cont = localVariables.newFieldInRecord("pnd_Id_Cont", "#ID-CONT", FieldType.STRING, 15);
        pnd_Id_Name1 = localVariables.newFieldInRecord("pnd_Id_Name1", "#ID-NAME1", FieldType.STRING, 42);
        pnd_Id_Name2 = localVariables.newFieldInRecord("pnd_Id_Name2", "#ID-NAME2", FieldType.STRING, 42);
        pnd_Line1 = localVariables.newFieldInRecord("pnd_Line1", "#LINE1", FieldType.STRING, 30);
        pnd_Uln1 = localVariables.newFieldInRecord("pnd_Uln1", "#ULN1", FieldType.STRING, 30);
        pnd_C_Ty = localVariables.newFieldInRecord("pnd_C_Ty", "#C-TY", FieldType.STRING, 14);
        pnd_Tot = localVariables.newFieldInRecord("pnd_Tot", "#TOT", FieldType.NUMERIC, 4);
        pnd_Tot_Record = localVariables.newFieldInRecord("pnd_Tot_Record", "#TOT-RECORD", FieldType.NUMERIC, 6);
        pnd_Job_Rsn = localVariables.newFieldInRecord("pnd_Job_Rsn", "#JOB-RSN", FieldType.STRING, 30);
        pnd_Job_Desc_1 = localVariables.newFieldInRecord("pnd_Job_Desc_1", "#JOB-DESC-1", FieldType.STRING, 43);
        pnd_Job_Desc_2 = localVariables.newFieldInRecord("pnd_Job_Desc_2", "#JOB-DESC-2", FieldType.STRING, 30);
        pnd_Contract_Num = localVariables.newFieldInRecord("pnd_Contract_Num", "#CONTRACT-NUM", FieldType.STRING, 10);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 8);
        pnd_Reprint = localVariables.newFieldInRecord("pnd_Reprint", "#REPRINT", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Part_Wfile_Pnd_Guaranteed_Period.setInitialValue("NONE");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisb800() throws Exception
    {
        super("Cisb800");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cisb800|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                //*  FORMAT(1) PS=60 LS=132                                      /* PINE
                //*  PINE
                //*  PINE                                                                                                                                                 //Natural: FORMAT ( 1 ) PS = 60 LS = 133;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
                //*                                                                                                                                                       //Natural: FORMAT PS = 60 LS = 80
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                if (condition(pnd_Input_Parm.equals("REPRINT")))                                                                                                          //Natural: IF #INPUT-PARM = 'REPRINT'
                {
                    pnd_Reprint.setValue(true);                                                                                                                           //Natural: ASSIGN #REPRINT := TRUE
                }                                                                                                                                                         //Natural: END-IF
                boolean endOfDataRd = true;                                                                                                                               //Natural: READ WORK 1 #PART-WFILE
                boolean firstRd = true;
                RD:
                while (condition(getWorkFiles().read(1, pnd_Part_Wfile)))
                {
                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventRd();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataRd = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    pnd_Id.setValue(pnd_Part_Wfile_Pnd_Cntrct_Sys_Id);                                                                                                    //Natural: ASSIGN #ID := #PART-WFILE.#CNTRCT-SYS-ID
                    if (condition(DbsUtil.maskMatches(pnd_Id,"'MDO'")))                                                                                                   //Natural: IF #ID = MASK ( 'MDO' )
                    {
                        pnd_Id.setValue("MDO");                                                                                                                           //Natural: ASSIGN #ID := 'MDO'
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Id_Cont.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Id, " CONTRACT #"));                                                         //Natural: COMPRESS #ID ' CONTRACT #' INTO #ID-CONT LEAVING NO
                    pnd_Id_Name1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Id, " CONTRACT PACKAGE DETAIL REPORT"));                                    //Natural: COMPRESS #ID ' CONTRACT PACKAGE DETAIL REPORT' INTO #ID-NAME1 LEAVING NO
                    pnd_Id_Name2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Id, " CONTRACT PACKAGE DETAIL SUMMARY REPORT"));                            //Natural: COMPRESS #ID ' CONTRACT PACKAGE DETAIL SUMMARY REPORT' INTO #ID-NAME2 LEAVING NO
                    if (condition(pnd_Reprint.getBoolean()))                                                                                                              //Natural: IF #REPRINT
                    {
                        short decideConditionsMet420 = 0;                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #PART-WFILE.#JOB-NAME;//Natural: VALUE 'P1235CID'
                        if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1235CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1435CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1435CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1240CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1240CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1440CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1440CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1245CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1245CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1445CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1445CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1255CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1255CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1455CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1455CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1260CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1260CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1460CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1460CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1335CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1335CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1535CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1535CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1340CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1340CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1540CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1540CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1345CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1345CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1545CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1545CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1355CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1355CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1555CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1555CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1360CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1360CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1560CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1560CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1635CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1635CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1735CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1735CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1655CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1655CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1755CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1755CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1835CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1835CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1935CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1935CID'
                        }                                                                                                                                                 //Natural: VALUE 'P1855CID'
                        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1855CID"))))
                        {
                            decideConditionsMet420++;
                            pnd_Part_Wfile_Pnd_Job_Name.setValue("P1955CID");                                                                                             //Natural: ASSIGN #JOB-NAME := 'P1955CID'
                        }                                                                                                                                                 //Natural: NONE VALUE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM JOB-TABLE
                    sub_Job_Table();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-PER-JOB
                    sub_Write_Per_Job();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*                                                                                                                                                   //Natural: AT BREAK OF #PART-WFILE.#JOB-NAME
                    //*   AT END OF DATA
                    //*     NEWPAGE(2)
                    //*      PERFORM WRITE-SUMMARY
                    //*   END-ENDDATA
                    //* * DIAG START
                    //* * DIAG END                                                                                                                                        //Natural: AT BREAK OF #PART-WFILE.#CNTRCT-SYS-ID
                }                                                                                                                                                         //Natural: END-WORK
                RD_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventRd(endOfDataRd);
                }
                if (Global.isEscape()) return;
                if (condition(pnd_Tot_Record.equals(getZero())))                                                                                                          //Natural: IF #TOT-RECORD = 0
                {
                    //* * DIAG START
                    pnd_Id_Name1.setValue("CONTRACT PACKAGE DETAIL REPORT");                                                                                              //Natural: ASSIGN #ID-NAME1 := 'CONTRACT PACKAGE DETAIL REPORT'
                    pnd_Id_Name2.setValue("CONTRACT PACKAGE DETAIL SUMMARY REPORT");                                                                                      //Natural: ASSIGN #ID-NAME2 := 'CONTRACT PACKAGE DETAIL SUMMARY REPORT'
                    //*   DECIDE ON FIRST VALUE OF *INIT-USER
                    //*    VALUE 'P1230CID'
                    //*     #ID-NAME1 := 'MDO CONTRACT PACKAGE DETAIL REPORT'
                    //*     #ID-NAME2 := 'MDO CONTRACT PACKAGE DETAIL SUMMARY REPORT'
                    //*    VALUE 'P1330CID'
                    //*     #ID-NAME1 := 'IA CONTRACT PACKAGE DETAIL REPORT'
                    //*     #ID-NAME2 := 'IA CONTRACT PACKAGE DETAIL SUMMARY REPORT'
                    //*    VALUE 'P1630CID'
                    //*     #ID-NAME1 := 'TPA CONTRACT PACKAGE DETAIL REPORT'
                    //*     #ID-NAME2 := 'TPA CONTRACT PACKAGE DETAIL SUMMARY REPORT'
                    //*    VALUE 'P1830CID'
                    //*     #ID-NAME1 := 'IPRO CONTRACT PACKAGE DETAIL REPORT'
                    //*     #ID-NAME2 := 'IPRO CONTRACT PACKAGE DETAIL SUMMARY REPORT'
                    //*    NONE
                    //*    IGNORE
                    //*   END-DECIDE
                    //* * DIAG END
                    getReports().write(1, NEWLINE,NEWLINE,new ColumnSpacing(10),"*****  THERE IS NO INPUT DATA TO PROCESS *****");                                        //Natural: WRITE ( 1 ) // 10X '*****  THERE IS NO INPUT DATA TO PROCESS *****'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  DIAG START                                                                                                                                           //Natural: AT TOP OF PAGE ( 2 )
                //*  IF #ID NE 'TPA'
                //*    AND #ID NE 'IPRO'
                //*    PERFORM WRITE-SUMMARY
                //*  ELSE
                //*    PERFORM TPA-WRITE-SUMMARY
                //*  END-IF
                //*  DIAG END
                //* *
                //* ***********************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PER-JOB
                //*      #PART-WFILE.#PIN-NUMBER 4X
                //*       #PART-WFILE.#JOB-REASON
                //*  DIAG START
                //* ***********************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-SUMMARY
                //* ***********************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY
                //* ***********************************************************
                //*  WRITE (2) 42X #ID-NAME2
                //*  /// 5X '  USER   ' 5X ' SYSTEM   '
                //*   / 5X 'REQUESTED' 5X 'DETERMINED' 5X 'UNAPPROVED'
                //*   / 5X 'PULLOUT  ' 5X 'PULLOUTS  ' 5X '  STATES  '
                //*     5X 'AUTOMATED' 5X 'OVERNIGHT'
                //*   / 5X #JOB-DESC-1 #JOB-DESC-2
                //*   / 5X '----------' 4X '----------' 5X '----------'
                //*     5X '----------' 4X '---------'
                //* ***********************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TPA-WRITE-SUMMARY
                //* ***********************************************************
                //*  WRITE (2) 42X #ID-NAME2
                //*  /// 5X '  USER   '
                //*    / 5X 'REQUESTED'
                //*    / 5X 'PULLOUT  '
                //*      5X 'AUTOMATED'
                //*    / 5X #JOB-DESC-1
                //*    / 5X '----------'
                //*      4X '----------'
                //* *********************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: JOB-TABLE
                //* *********************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONT-TYPE
                //* *********************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUBSTRING-NAME
                //* ************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-MID-NAME
                //* ************************************************************
                //*      #JOB-TITLE := 'SYSTEM DETERMINED PULLOUT        '
                //*      #JOB-TITLE := 'AUTOMATED (MAILED TO PARTICIPANT)'
                //*      #JOB-TITLE := 'MAILED OVERNIGHT                 '
                //*      #JOB-TITLE := 'UNAPPROVED STATES                '
                //*  COMPRESS #PART-WFILE.#CNTRCT-RETR-SURV-STATUS
                //*           #PART-WFILE.#MDO-CNTRCT-CASH-STATUS
                //*             INTO #FLD1(#I) LEAVING NO
                //*  * * * IA ONLY
                //*   #FLD1(#I) :=  #PART-WFILE.#CNTRCT-OPTION
                //*    VALUE 'OL'                 /*  LIFE ANNUITY
                //*    VALUE 'IR'                 /*  INSTALLMENT REFUND
                //*    VALUE 'AC'                 /*  ANNUITY CERTAIN
                //*    VALUE 'LSF'                /*  FULL BENEFIT
                //*    VALUE 'LS'                 /*  HALF BENEFIT
                //*    VALUE 'JS'                 /*  2/3 BENEFIT
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Write_Per_Job() throws Exception                                                                                                                     //Natural: WRITE-PER-JOB
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        pnd_Tot.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO #TOT
        pnd_Tot_Record.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #TOT-RECORD
                                                                                                                                                                          //Natural: PERFORM SUBSTRING-NAME
        sub_Substring_Name();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(pnd_Part_Wfile_Pnd_Tiaa_Issue_Date.equals("0") || pnd_Part_Wfile_Pnd_Tiaa_Issue_Date.equals(" ")))                                                  //Natural: IF #PART-WFILE.#TIAA-ISSUE-DATE = '0' OR = ' '
        {
            pnd_Tiaa_Date.setValue(pnd_Part_Wfile_Pnd_Cref_Issue_Date);                                                                                                   //Natural: ASSIGN #TIAA-DATE := #PART-WFILE.#CREF-ISSUE-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tiaa_Date.setValue(pnd_Part_Wfile_Pnd_Tiaa_Issue_Date);                                                                                                   //Natural: ASSIGN #TIAA-DATE := #PART-WFILE.#TIAA-ISSUE-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Full_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Tiaa_Date_Pnd_Mm, "/", pnd_Tiaa_Date_Pnd_Dd, "/", pnd_Tiaa_Date_Pnd_Yyyy));            //Natural: COMPRESS #MM '/' #DD '/' #YYYY INTO #FULL-DATE LEAVING NO
        if (condition(pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Num.notEquals(" ")))                                                                                                 //Natural: IF #PART-WFILE.#TIAA-CNTRCT-NUM NE ' '
        {
            pnd_Contract_Num.setValue(pnd_Part_Wfile_Pnd_Tiaa_Cntrct_Num);                                                                                                //Natural: ASSIGN #CONTRACT-NUM := #PART-WFILE.#TIAA-CNTRCT-NUM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Contract_Num.setValue(pnd_Part_Wfile_Pnd_Cref_Cntrct_Num);                                                                                                //Natural: ASSIGN #CONTRACT-NUM := #PART-WFILE.#CREF-CNTRCT-NUM
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Part_Wfile_Pnd_Post_Ret_Trans_Flag.equals("Y")))                                                                                                //Natural: IF #POST-RET-TRANS-FLAG = 'Y'
        {
            pnd_Contract_Num.setValue(pnd_Part_Wfile_Pnd_Cref_Cntrct_Num);                                                                                                //Natural: ASSIGN #CONTRACT-NUM := #PART-WFILE.#CREF-CNTRCT-NUM
        }                                                                                                                                                                 //Natural: END-IF
        //*  PINE
        if (condition(pnd_Part_Wfile_Pnd_Job_Name.notEquals("P1240CID") && pnd_Part_Wfile_Pnd_Job_Name.notEquals("P1340CID")))                                            //Natural: IF #JOB-NAME NE 'P1240CID' AND #JOB-NAME NE 'P1340CID'
        {
            getReports().write(1, pnd_Part_Wfile_Pnd_Pin_Number,new ColumnSpacing(2),pnd_Full_Name,pnd_Contract_Num,new ColumnSpacing(3),pnd_Full_Date,new                //Natural: WRITE ( 1 ) #PART-WFILE.#PIN-NUMBER 2X #FULL-NAME #CONTRACT-NUM 3X #FULL-DATE 2X #PART-WFILE.#FIRST-ANNU-SSN 2X #PART-WFILE.#ORIGINAL-ISSUE-STAT-CD 11X #TXT1 #TXT2
                ColumnSpacing(2),pnd_Part_Wfile_Pnd_First_Annu_Ssn,new ColumnSpacing(2),pnd_Part_Wfile_Pnd_Original_Issue_Stat_Cd,new ColumnSpacing(11),
                pnd_Txt1,pnd_Txt2);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  P1240CID AND P1340CID ONLY **
            pnd_Job_Rsn.setValue(pnd_Part_Wfile_Pnd_Job_Reason.getSubstring(3,29));                                                                                       //Natural: MOVE SUBSTRING ( #PART-WFILE.#JOB-REASON,3,29 ) TO #JOB-RSN
            getReports().write(1, pnd_Part_Wfile_Pnd_Pin_Number,new ColumnSpacing(4),pnd_Full_Name,pnd_Contract_Num,new ColumnSpacing(3),pnd_Full_Date,new                //Natural: WRITE ( 1 ) #PART-WFILE.#PIN-NUMBER 4X #FULL-NAME #CONTRACT-NUM 3X #FULL-DATE 2X #PART-WFILE.#FIRST-ANNU-SSN 2X #PART-WFILE.#ORIGINAL-ISSUE-STAT-CD 11X #JOB-RSN
                ColumnSpacing(2),pnd_Part_Wfile_Pnd_First_Annu_Ssn,new ColumnSpacing(2),pnd_Part_Wfile_Pnd_Original_Issue_Stat_Cd,new ColumnSpacing(11),
                pnd_Job_Rsn);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Txt1.reset();                                                                                                                                                 //Natural: RESET #TXT1 #TXT2 #LINE1 #ULN1
        pnd_Txt2.reset();
        pnd_Line1.reset();
        pnd_Uln1.reset();
    }
    private void sub_Determine_Summary() throws Exception                                                                                                                 //Natural: DETERMINE-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        if (condition(pnd_Id.notEquals("TPA") && pnd_Id.notEquals("IPRO")))                                                                                               //Natural: IF #ID NE 'TPA' AND #ID NE 'IPRO'
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY
            sub_Write_Summary();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM TPA-WRITE-SUMMARY
            sub_Tpa_Write_Summary();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Summary() throws Exception                                                                                                                     //Natural: WRITE-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_Grd_Tot.compute(new ComputeParameters(false, pnd_Grd_Tot), pnd_Count_Job.getValue(1).add(pnd_Count_Job.getValue(2)).add(pnd_Count_Job.getValue(3)).add(pnd_Count_Job.getValue(4)).add(pnd_Count_Job.getValue(5))); //Natural: ASSIGN #GRD-TOT := #COUNT-JOB ( 1 ) + #COUNT-JOB ( 2 ) + #COUNT-JOB ( 3 ) + #COUNT-JOB ( 4 ) + #COUNT-JOB ( 5 )
        getReports().write(2, new ColumnSpacing(6),pnd_Count_Job.getValue(1), new ReportEditMask ("ZZ,ZZ9"),new ColumnSpacing(8),pnd_Count_Job.getValue(2),               //Natural: WRITE ( 2 ) 6X #COUNT-JOB ( 1 ) 8X #COUNT-JOB ( 2 ) 9X #COUNT-JOB ( 3 ) 8X #COUNT-JOB ( 4 ) 8X #COUNT-JOB ( 5 ) ///
            new ReportEditMask ("ZZ,ZZ9"),new ColumnSpacing(9),pnd_Count_Job.getValue(3), new ReportEditMask ("ZZ,ZZ9"),new ColumnSpacing(8),pnd_Count_Job.getValue(4), 
            new ReportEditMask ("ZZ,ZZ9"),new ColumnSpacing(8),pnd_Count_Job.getValue(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, "GRAND TOTAL ===> ",pnd_Grd_Tot, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                                         //Natural: WRITE ( 2 ) 'GRAND TOTAL ===> ' #GRD-TOT
        if (Global.isEscape()) return;
        pnd_Count_Job.getValue("*").reset();                                                                                                                              //Natural: RESET #COUNT-JOB ( * ) #GRD-TOT
        pnd_Grd_Tot.reset();
    }
    private void sub_Tpa_Write_Summary() throws Exception                                                                                                                 //Natural: TPA-WRITE-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        pnd_Grd_Tot.compute(new ComputeParameters(false, pnd_Grd_Tot), pnd_Count_Job.getValue(1).add(pnd_Count_Job.getValue(4)));                                         //Natural: ASSIGN #GRD-TOT := #COUNT-JOB ( 1 ) + #COUNT-JOB ( 4 )
        getReports().write(2, new ColumnSpacing(6),pnd_Count_Job.getValue(1), new ReportEditMask ("ZZ,ZZ9"),new ColumnSpacing(8),pnd_Count_Job.getValue(4),               //Natural: WRITE ( 2 ) 6X #COUNT-JOB ( 1 ) 8X #COUNT-JOB ( 4 ) ///
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, "GRAND TOTAL ===> ",pnd_Grd_Tot, new ReportEditMask ("Z,ZZZ,ZZ9"));                                                                         //Natural: WRITE ( 2 ) 'GRAND TOTAL ===> ' #GRD-TOT
        if (Global.isEscape()) return;
        pnd_Count_Job.getValue("*").reset();                                                                                                                              //Natural: RESET #COUNT-JOB ( * ) #GRD-TOT
        pnd_Grd_Tot.reset();
    }
    private void sub_Job_Table() throws Exception                                                                                                                         //Natural: JOB-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************
        short decideConditionsMet654 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #PART-WFILE.#JOB-NAME;//Natural: VALUE 'P1235CID','P1335CID','P1635CID','P1835CID'
        if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1235CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1335CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1635CID") 
            || pnd_Part_Wfile_Pnd_Job_Name.equals("P1835CID"))))
        {
            decideConditionsMet654++;
            pnd_Job_Title.setValue("USER REQUESTED PULLOUTS");                                                                                                            //Natural: ASSIGN #JOB-TITLE := 'USER REQUESTED PULLOUTS'
            if (condition(pnd_Id.equals("TPA")))                                                                                                                          //Natural: IF #ID = 'TPA'
            {
                pnd_Line1.setValue("OPTION");                                                                                                                             //Natural: ASSIGN #LINE1 := 'OPTION'
                pnd_Uln1.setValue("------");                                                                                                                              //Natural: ASSIGN #ULN1 := '------'
                pnd_Txt1.setValue(pnd_Part_Wfile_Pnd_Cntrct_Option);                                                                                                      //Natural: ASSIGN #TXT1 := #PART-WFILE.#CNTRCT-OPTION
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Id.equals("IPRO")))                                                                                                                         //Natural: IF #ID = 'IPRO'
            {
                pnd_Line1.setValue("OPTION");                                                                                                                             //Natural: ASSIGN #LINE1 := 'OPTION'
                pnd_Uln1.setValue("------");                                                                                                                              //Natural: ASSIGN #ULN1 := '------'
                pnd_Txt1.setValue(pnd_Part_Wfile_Pnd_Cntrct_Option);                                                                                                      //Natural: ASSIGN #TXT1 := #PART-WFILE.#CNTRCT-OPTION
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Job.getValue(1).nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT-JOB ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'P1435CID','P1535CID','P1735CID','P1935CID'
        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1435CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1535CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1735CID") 
            || pnd_Part_Wfile_Pnd_Job_Name.equals("P1935CID"))))
        {
            decideConditionsMet654++;
            pnd_Job_Title.setValue("REPRINT - RETURN TO UNIT");                                                                                                           //Natural: ASSIGN #JOB-TITLE := 'REPRINT - RETURN TO UNIT'
            if (condition(pnd_Id.equals("TPA")))                                                                                                                          //Natural: IF #ID = 'TPA'
            {
                pnd_Line1.setValue("OPTION");                                                                                                                             //Natural: ASSIGN #LINE1 := 'OPTION'
                pnd_Uln1.setValue("------");                                                                                                                              //Natural: ASSIGN #ULN1 := '------'
                pnd_Txt1.setValue(pnd_Part_Wfile_Pnd_Cntrct_Option);                                                                                                      //Natural: ASSIGN #TXT1 := #PART-WFILE.#CNTRCT-OPTION
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Id.equals("IPRO")))                                                                                                                         //Natural: IF #ID = 'IPRO'
            {
                pnd_Line1.setValue("OPTION");                                                                                                                             //Natural: ASSIGN #LINE1 := 'OPTION'
                pnd_Uln1.setValue("------");                                                                                                                              //Natural: ASSIGN #ULN1 := '------'
                pnd_Txt1.setValue(pnd_Part_Wfile_Pnd_Cntrct_Option);                                                                                                      //Natural: ASSIGN #TXT1 := #PART-WFILE.#CNTRCT-OPTION
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Job.getValue(1).nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT-JOB ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'P1240CID','P1340CID'
        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1240CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1340CID"))))
        {
            decideConditionsMet654++;
            pnd_Job_Title.setValue("SYSTEM DETERMINED PULLOUT");                                                                                                          //Natural: ASSIGN #JOB-TITLE := 'SYSTEM DETERMINED PULLOUT'
            pnd_Count_Job.getValue(2).nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT-JOB ( 2 )
            pnd_Line1.setValue("         REASON                 ");                                                                                                       //Natural: ASSIGN #LINE1 := '         REASON                 '
            pnd_Uln1.setValue("--------------------------------");                                                                                                        //Natural: ASSIGN #ULN1 := '--------------------------------'
        }                                                                                                                                                                 //Natural: VALUE 'P1440CID','P1540CID'
        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1440CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1540CID"))))
        {
            decideConditionsMet654++;
            pnd_Job_Title.setValue("REPRINT - RETURN TO UNIT");                                                                                                           //Natural: ASSIGN #JOB-TITLE := 'REPRINT - RETURN TO UNIT'
            pnd_Count_Job.getValue(2).nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT-JOB ( 2 )
            pnd_Line1.setValue("         REASON                 ");                                                                                                       //Natural: ASSIGN #LINE1 := '         REASON                 '
            pnd_Uln1.setValue("--------------------------------");                                                                                                        //Natural: ASSIGN #ULN1 := '--------------------------------'
        }                                                                                                                                                                 //Natural: VALUE 'P1245CID','P1345CID'
        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1245CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1345CID"))))
        {
            decideConditionsMet654++;
            pnd_Job_Title.setValue("UNAPPROVED STATES");                                                                                                                  //Natural: ASSIGN #JOB-TITLE := 'UNAPPROVED STATES'
            if (condition(pnd_Id.equals("IA")))                                                                                                                           //Natural: IF #ID = 'IA'
            {
                pnd_Line1.setValue("STTLMT OPTION PRODUCT STTLD");                                                                                                        //Natural: ASSIGN #LINE1 := 'STTLMT OPTION PRODUCT STTLD'
                pnd_Uln1.setValue("------------- -------------");                                                                                                         //Natural: ASSIGN #ULN1 := '------------- -------------'
                pnd_Txt1.setValue(pnd_Part_Wfile_Pnd_Cntrct_Option);                                                                                                      //Natural: ASSIGN #TXT1 := #PART-WFILE.#CNTRCT-OPTION
                pnd_Txt2.setValue(pnd_Part_Wfile_Pnd_Cntrct_Type);                                                                                                        //Natural: ASSIGN #TXT2 := #PART-WFILE.#CNTRCT-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Line1.setValue("CNTRCT TYPE");                                                                                                                        //Natural: ASSIGN #LINE1 := 'CNTRCT TYPE'
                pnd_Uln1.setValue("-------------");                                                                                                                       //Natural: ASSIGN #ULN1 := '-------------'
                pnd_Txt1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Part_Wfile_Pnd_Cntrct_Retr_Surv_Status, "/", pnd_Part_Wfile_Pnd_Mdo_Cntrct_Cash_Status)); //Natural: COMPRESS #PART-WFILE.#CNTRCT-RETR-SURV-STATUS '/' #PART-WFILE.#MDO-CNTRCT-CASH-STATUS INTO #TXT1 LEAVING NO
                                                                                                                                                                          //Natural: PERFORM CONT-TYPE
                sub_Cont_Type();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Txt1.setValue(pnd_C_Ty);                                                                                                                              //Natural: ASSIGN #TXT1 := #C-TY
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Job.getValue(3).nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT-JOB ( 3 )
        }                                                                                                                                                                 //Natural: VALUE 'P1445CID','P1545CID'
        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1445CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1545CID"))))
        {
            decideConditionsMet654++;
            pnd_Job_Title.setValue("REPRINT - RETURN TO UNIT");                                                                                                           //Natural: ASSIGN #JOB-TITLE := 'REPRINT - RETURN TO UNIT'
            if (condition(pnd_Id.equals("IA")))                                                                                                                           //Natural: IF #ID = 'IA'
            {
                pnd_Line1.setValue("STTLMT OPTION PRODUCT STTLD");                                                                                                        //Natural: ASSIGN #LINE1 := 'STTLMT OPTION PRODUCT STTLD'
                pnd_Uln1.setValue("------------- -------------");                                                                                                         //Natural: ASSIGN #ULN1 := '------------- -------------'
                pnd_Txt1.setValue(pnd_Part_Wfile_Pnd_Cntrct_Option);                                                                                                      //Natural: ASSIGN #TXT1 := #PART-WFILE.#CNTRCT-OPTION
                pnd_Txt2.setValue(pnd_Part_Wfile_Pnd_Cntrct_Type);                                                                                                        //Natural: ASSIGN #TXT2 := #PART-WFILE.#CNTRCT-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Line1.setValue("CNTRCT TYPE");                                                                                                                        //Natural: ASSIGN #LINE1 := 'CNTRCT TYPE'
                pnd_Uln1.setValue("-------------");                                                                                                                       //Natural: ASSIGN #ULN1 := '-------------'
                pnd_Txt1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Part_Wfile_Pnd_Cntrct_Retr_Surv_Status, "/", pnd_Part_Wfile_Pnd_Mdo_Cntrct_Cash_Status)); //Natural: COMPRESS #PART-WFILE.#CNTRCT-RETR-SURV-STATUS '/' #PART-WFILE.#MDO-CNTRCT-CASH-STATUS INTO #TXT1 LEAVING NO
                                                                                                                                                                          //Natural: PERFORM CONT-TYPE
                sub_Cont_Type();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Txt1.setValue(pnd_C_Ty);                                                                                                                              //Natural: ASSIGN #TXT1 := #C-TY
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Job.getValue(3).nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT-JOB ( 3 )
        }                                                                                                                                                                 //Natural: VALUE 'P1255CID','P1355CID','P1655CID','P1855CID'
        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1255CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1355CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1655CID") 
            || pnd_Part_Wfile_Pnd_Job_Name.equals("P1855CID"))))
        {
            decideConditionsMet654++;
            pnd_Job_Title.setValue("AUTOMATED (MAILED TO PARTICIPANT)");                                                                                                  //Natural: ASSIGN #JOB-TITLE := 'AUTOMATED (MAILED TO PARTICIPANT)'
            if (condition(pnd_Id.equals("TPA")))                                                                                                                          //Natural: IF #ID = 'TPA'
            {
                pnd_Line1.setValue("OPTION");                                                                                                                             //Natural: ASSIGN #LINE1 := 'OPTION'
                pnd_Uln1.setValue("------");                                                                                                                              //Natural: ASSIGN #ULN1 := '------'
                pnd_Txt1.setValue(pnd_Part_Wfile_Pnd_Cntrct_Option);                                                                                                      //Natural: ASSIGN #TXT1 := #PART-WFILE.#CNTRCT-OPTION
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Id.equals("IPRO")))                                                                                                                         //Natural: IF #ID = 'IPRO'
            {
                pnd_Line1.setValue("OPTION");                                                                                                                             //Natural: ASSIGN #LINE1 := 'OPTION'
                pnd_Uln1.setValue("------");                                                                                                                              //Natural: ASSIGN #ULN1 := '------'
                pnd_Txt1.setValue(pnd_Part_Wfile_Pnd_Cntrct_Option);                                                                                                      //Natural: ASSIGN #TXT1 := #PART-WFILE.#CNTRCT-OPTION
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Job.getValue(4).nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT-JOB ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 'P1455CID','P1555CID','P1755CID','P1955CID'
        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1455CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1555CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1755CID") 
            || pnd_Part_Wfile_Pnd_Job_Name.equals("P1955CID"))))
        {
            decideConditionsMet654++;
            pnd_Job_Title.setValue("REPRINT - RETURN TO UNIT");                                                                                                           //Natural: ASSIGN #JOB-TITLE := 'REPRINT - RETURN TO UNIT'
            if (condition(pnd_Id.equals("TPA")))                                                                                                                          //Natural: IF #ID = 'TPA'
            {
                pnd_Line1.setValue("OPTION");                                                                                                                             //Natural: ASSIGN #LINE1 := 'OPTION'
                pnd_Uln1.setValue("------");                                                                                                                              //Natural: ASSIGN #ULN1 := '------'
                pnd_Txt1.setValue(pnd_Part_Wfile_Pnd_Cntrct_Option);                                                                                                      //Natural: ASSIGN #TXT1 := #PART-WFILE.#CNTRCT-OPTION
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Id.equals("IPRO")))                                                                                                                         //Natural: IF #ID = 'IPRO'
            {
                pnd_Line1.setValue("OPTION");                                                                                                                             //Natural: ASSIGN #LINE1 := 'OPTION'
                pnd_Uln1.setValue("------");                                                                                                                              //Natural: ASSIGN #ULN1 := '------'
                pnd_Txt1.setValue(pnd_Part_Wfile_Pnd_Cntrct_Option);                                                                                                      //Natural: ASSIGN #TXT1 := #PART-WFILE.#CNTRCT-OPTION
            }                                                                                                                                                             //Natural: END-IF
            pnd_Count_Job.getValue(4).nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT-JOB ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 'P1260CID','P1360CID'
        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1260CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1360CID"))))
        {
            decideConditionsMet654++;
            pnd_Job_Title.setValue("MAILED OVERNIGHT");                                                                                                                   //Natural: ASSIGN #JOB-TITLE := 'MAILED OVERNIGHT'
            pnd_Count_Job.getValue(5).nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT-JOB ( 5 )
        }                                                                                                                                                                 //Natural: VALUE 'P1460CID','P1560CID'
        else if (condition((pnd_Part_Wfile_Pnd_Job_Name.equals("P1460CID") || pnd_Part_Wfile_Pnd_Job_Name.equals("P1560CID"))))
        {
            decideConditionsMet654++;
            pnd_Job_Title.setValue("REPRINT - RETURN TO UNIT");                                                                                                           //Natural: ASSIGN #JOB-TITLE := 'REPRINT - RETURN TO UNIT'
            pnd_Count_Job.getValue(5).nadd(1);                                                                                                                            //Natural: ADD 1 TO #COUNT-JOB ( 5 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* * DIAG START
        //*  IF #ID = 'IA'
        //*    #JOB-DESC-1 := '(P1335CID)    (P1340CID)     (P1345CID)  '
        //*    #JOB-DESC-2 := '(P1355CID)    (P1360CID)'
        //*  END-IF
        //*  IF #ID = MASK('MDO')                                       /* 060706
        //*    #JOB-DESC-1 := '(P1235CID)    (P1240CID)     (P1245CID)  '
        //*    #JOB-DESC-2 := '(P1255CID)    (P1260CID)'
        //*  END-IF
        //*  IF #ID = 'TPA'
        //*    #JOB-DESC-1 := '(P1635CID)    (P1655CID)'
        //*  END-IF
        //*  IF #ID = 'TPA'                                       /* DIAG
        //*    #JOB-DESC-1 := '(P1835CID)    (P1855CID)'
        //*  END-IF
        short decideConditionsMet772 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #ID;//Natural: VALUE 'IA'
        if (condition((pnd_Id.equals("IA"))))
        {
            decideConditionsMet772++;
            if (condition(pnd_Reprint.getBoolean()))                                                                                                                      //Natural: IF #REPRINT
            {
                pnd_Job_Desc_1.setValue("(P1535CID)    (P1540CID)     (P1545CID)  ");                                                                                     //Natural: ASSIGN #JOB-DESC-1 := '(P1535CID)    (P1540CID)     (P1545CID)  '
                pnd_Job_Desc_2.setValue("(P1555CID)    (P1560CID)");                                                                                                      //Natural: ASSIGN #JOB-DESC-2 := '(P1555CID)    (P1560CID)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Job_Desc_1.setValue("(P1335CID)    (P1340CID)     (P1345CID)  ");                                                                                     //Natural: ASSIGN #JOB-DESC-1 := '(P1335CID)    (P1340CID)     (P1345CID)  '
                pnd_Job_Desc_2.setValue("(P1355CID)    (P1360CID)");                                                                                                      //Natural: ASSIGN #JOB-DESC-2 := '(P1355CID)    (P1360CID)'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'MDO'
        else if (condition((pnd_Id.equals("MDO"))))
        {
            decideConditionsMet772++;
            if (condition(pnd_Reprint.getBoolean()))                                                                                                                      //Natural: IF #REPRINT
            {
                pnd_Job_Desc_1.setValue("(P1435CID)    (P1440CID)     (P1445CID)  ");                                                                                     //Natural: ASSIGN #JOB-DESC-1 := '(P1435CID)    (P1440CID)     (P1445CID)  '
                pnd_Job_Desc_2.setValue("(P1455CID)    (P1460CID)");                                                                                                      //Natural: ASSIGN #JOB-DESC-2 := '(P1455CID)    (P1460CID)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Job_Desc_1.setValue("(P1235CID)    (P1240CID)     (P1245CID)  ");                                                                                     //Natural: ASSIGN #JOB-DESC-1 := '(P1235CID)    (P1240CID)     (P1245CID)  '
                pnd_Job_Desc_2.setValue("(P1255CID)    (P1260CID)");                                                                                                      //Natural: ASSIGN #JOB-DESC-2 := '(P1255CID)    (P1260CID)'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'TPA'
        else if (condition((pnd_Id.equals("TPA"))))
        {
            decideConditionsMet772++;
            if (condition(pnd_Reprint.getBoolean()))                                                                                                                      //Natural: IF #REPRINT
            {
                pnd_Job_Desc_1.setValue("(P1735CID)    (P1755CID)");                                                                                                      //Natural: ASSIGN #JOB-DESC-1 := '(P1735CID)    (P1755CID)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Job_Desc_1.setValue("(P1635CID)    (P1655CID)");                                                                                                      //Natural: ASSIGN #JOB-DESC-1 := '(P1635CID)    (P1655CID)'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'IPRO'
        else if (condition((pnd_Id.equals("IPRO"))))
        {
            decideConditionsMet772++;
            if (condition(pnd_Reprint.getBoolean()))                                                                                                                      //Natural: IF #REPRINT
            {
                pnd_Job_Desc_1.setValue("(P1935CID)    (P1955CID)");                                                                                                      //Natural: ASSIGN #JOB-DESC-1 := '(P1935CID)    (P1955CID)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Job_Desc_1.setValue("(P1835CID)    (P1855CID)");                                                                                                      //Natural: ASSIGN #JOB-DESC-1 := '(P1835CID)    (P1855CID)'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* * DIAG END
    }
    private void sub_Cont_Type() throws Exception                                                                                                                         //Natural: CONT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************
        short decideConditionsMet810 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #TXT1;//Natural: VALUE 'R/C'
        if (condition((pnd_Txt1.equals("R/C"))))
        {
            decideConditionsMet810++;
            pnd_C_Ty.setValue("RET. CASH");                                                                                                                               //Natural: ASSIGN #C-TY := 'RET. CASH'
        }                                                                                                                                                                 //Natural: VALUE 'R/N'
        else if (condition((pnd_Txt1.equals("R/N"))))
        {
            decideConditionsMet810++;
            pnd_C_Ty.setValue("RET. NON-CASH");                                                                                                                           //Natural: ASSIGN #C-TY := 'RET. NON-CASH'
        }                                                                                                                                                                 //Natural: VALUE 'S/C'
        else if (condition((pnd_Txt1.equals("S/C"))))
        {
            decideConditionsMet810++;
            pnd_C_Ty.setValue("SURV. CASH");                                                                                                                              //Natural: ASSIGN #C-TY := 'SURV. CASH'
        }                                                                                                                                                                 //Natural: VALUE 'S/N'
        else if (condition((pnd_Txt1.equals("S/N"))))
        {
            decideConditionsMet810++;
            pnd_C_Ty.setValue("SURV. NON-CASH");                                                                                                                          //Natural: ASSIGN #C-TY := 'SURV. NON-CASH'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Substring_Name() throws Exception                                                                                                                    //Natural: SUBSTRING-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************
        DbsUtil.examine(new ExamineSource(pnd_Part_Wfile_Pnd_First_Annu_First_Name,1,1), new ExamineSearch(pnd_Part_Wfile_Pnd_First_Annu_First_Name),                     //Natural: EXAMINE SUBSTRING ( #PART-WFILE.#FIRST-ANNU-FIRST-NAME,1 ) FOR #PART-WFILE.#FIRST-ANNU-FIRST-NAME GIVING LENGTH #FRST-LEN
            new ExamineGivingLength(pnd_Frst_Len));
        DbsUtil.examine(new ExamineSource(pnd_Part_Wfile_Pnd_First_Annu_Mid_Name,1,1), new ExamineSearch(pnd_Part_Wfile_Pnd_First_Annu_Mid_Name), new                     //Natural: EXAMINE SUBSTRING ( #PART-WFILE.#FIRST-ANNU-MID-NAME,1 ) FOR #PART-WFILE.#FIRST-ANNU-MID-NAME GIVING LENGTH #MID-LEN
            ExamineGivingLength(pnd_Mid_Len));
        DbsUtil.examine(new ExamineSource(pnd_Part_Wfile_Pnd_First_Annu_Last_Name,1,1), new ExamineSearch(pnd_Part_Wfile_Pnd_First_Annu_Last_Name), new                   //Natural: EXAMINE SUBSTRING ( #PART-WFILE.#FIRST-ANNU-LAST-NAME,1 ) FOR #PART-WFILE.#FIRST-ANNU-LAST-NAME GIVING LENGTH #LST-LEN
            ExamineGivingLength(pnd_Lst_Len));
        if (condition(pnd_Lst_Len.greater(getZero())))                                                                                                                    //Natural: IF #LST-LEN GT 0
        {
            pnd_Last_Name.setValue(pnd_Part_Wfile_Pnd_First_Annu_Last_Name.getSubstring(1,pnd_Lst_Len.getInt()));                                                         //Natural: MOVE SUBSTRING ( #PART-WFILE.#FIRST-ANNU-LAST-NAME,1,#LST-LEN ) TO #LAST-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Last_Name.setValue(pnd_Part_Wfile_Pnd_First_Annu_Last_Name);                                                                                              //Natural: ASSIGN #LAST-NAME := #PART-WFILE.#FIRST-ANNU-LAST-NAME
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Begin.compute(new ComputeParameters(false, pnd_Begin), pnd_Frst_Len.add(pnd_Lst_Len).add(1));                                                                 //Natural: ASSIGN #BEGIN := #FRST-LEN + #LST-LEN + 1
        if (condition(pnd_Begin.lessOrEqual(35) && pnd_Frst_Len.greater(getZero())))                                                                                      //Natural: IF #BEGIN LE 35 AND #FRST-LEN GT 0
        {
            pnd_Frst_Name.setValue(pnd_Part_Wfile_Pnd_First_Annu_First_Name.getSubstring(1,pnd_Frst_Len.getInt()));                                                       //Natural: MOVE SUBSTRING ( #PART-WFILE.#FIRST-ANNU-FIRST-NAME,1,#FRST-LEN ) TO #FRST-NAME
            pnd_Fr_Nx.setValue(pnd_Frst_Len);                                                                                                                             //Natural: ASSIGN #FR-NX := #FRST-LEN
                                                                                                                                                                          //Natural: PERFORM LOAD-MID-NAME
            sub_Load_Mid_Name();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR:                                                                                                                                                          //Natural: FOR #N 1 30
            for (pnd_N.setValue(1); condition(pnd_N.lessOrEqual(30)); pnd_N.nadd(1))
            {
                if (condition(((pnd_N.add(pnd_Lst_Len).add(1)).greater(35)) || pnd_Part_Wfile_Pnd_First_Annu_First_Name.getSubstring(pnd_N.getInt(),1).equals(" ")))      //Natural: IF ( ( #N + #LST-LEN + 1 ) GT 35 ) OR SUBSTRING ( #PART-WFILE.#FIRST-ANNU-FIRST-NAME,#N,1 ) = ' '
                {
                    if (true) break FOR;                                                                                                                                  //Natural: ESCAPE BOTTOM ( FOR. )
                }                                                                                                                                                         //Natural: END-IF
                //*     #FR-NX := #N
                setValueToSubstring(pnd_Part_Wfile_Pnd_First_Annu_First_Name.getSubstring(pnd_N.getInt(),1),pnd_Frst_Name,pnd_N.getInt());                                //Natural: MOVE SUBSTRING ( #PART-WFILE.#FIRST-ANNU-FIRST-NAME,#N,1 ) TO SUBSTRING ( #FRST-NAME,#N )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Full_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Last_Name));                                                                                  //Natural: COMPRESS #FRST-NAME ' ' #LAST-NAME INTO #FULL-NAME LEAVING
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Load_Mid_Name() throws Exception                                                                                                                     //Natural: LOAD-MID-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************************************
        if (condition(pnd_Part_Wfile_Pnd_First_Annu_Mid_Name.equals(" ")))                                                                                                //Natural: IF #PART-WFILE.#FIRST-ANNU-MID-NAME = ' '
        {
            pnd_Full_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Last_Name));                                                                                  //Natural: COMPRESS #FRST-NAME ' ' #LAST-NAME INTO #FULL-NAME LEAVING
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Begin1.compute(new ComputeParameters(false, pnd_Begin1), pnd_Fr_Nx.add(pnd_Lst_Len).add(pnd_Mid_Len).add(2));                                                 //Natural: ASSIGN #BEGIN1 := #FR-NX + #LST-LEN + #MID-LEN + 2
        if (condition(pnd_Begin1.lessOrEqual(35) && pnd_Mid_Len.greater(getZero())))                                                                                      //Natural: IF #BEGIN1 LE 35 AND #MID-LEN GT 0
        {
            pnd_Mid_Name.setValue(pnd_Part_Wfile_Pnd_First_Annu_Mid_Name.getSubstring(1,pnd_Mid_Len.getInt()));                                                           //Natural: MOVE SUBSTRING ( #PART-WFILE.#FIRST-ANNU-MID-NAME,1,#MID-LEN ) TO #MID-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Mid_Name.setValue(pnd_Part_Wfile_Pnd_First_Annu_Mid_Name.getSubstring(1,1));                                                                              //Natural: MOVE SUBSTRING ( #PART-WFILE.#FIRST-ANNU-MID-NAME,1,1 ) TO #MID-NAME
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Full_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Mid_Name, " ", pnd_Last_Name));                                                                   //Natural: COMPRESS #FRST-NAME ' ' #MID-NAME ' ' #LAST-NAME INTO #FULL-NAME LEAVING
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM());                                                                                                           //Natural: WRITE ( 1 ) *PROGRAM
                    getReports().write(1, "DATE:",Global.getDATX(),new ColumnSpacing(30),"CONSOLIDATED ISSUE SYSTEM",new ColumnSpacing(20),"PAGE:",getReports().getPageNumberDbs(1),NEWLINE,"TIME:",Global.getTIMX(),new  //Natural: WRITE ( 1 ) 'DATE:' *DATX 30X 'CONSOLIDATED ISSUE SYSTEM' 20X 'PAGE:' *PAGE-NUMBER ( 1 ) / 'TIME:' *TIMX 25X #ID-NAME1 // 40X 'FOR:' #JOB-TITLE / 40X 'JOB #:' #JOB-NAME // 'PIN #        ' 1X 'PARTICIPANT NAME' 17X #ID-CONT 2X 'ISSUE DATE' 2X 'SSN' 10X 'ISSUE STATE' 2X #LINE1 / '-------------' 1X '----------------' 17X '---------------' 2X '----------' 2X '----------' 3X '-----------' 2X #ULN1
                        ColumnSpacing(25),pnd_Id_Name1,NEWLINE,NEWLINE,new ColumnSpacing(40),"FOR:",pnd_Job_Title,NEWLINE,new ColumnSpacing(40),"JOB #:",pnd_Part_Wfile_Pnd_Job_Name,NEWLINE,NEWLINE,"PIN #        ",new 
                        ColumnSpacing(1),"PARTICIPANT NAME",new ColumnSpacing(17),pnd_Id_Cont,new ColumnSpacing(2),"ISSUE DATE",new ColumnSpacing(2),"SSN",new 
                        ColumnSpacing(10),"ISSUE STATE",new ColumnSpacing(2),pnd_Line1,NEWLINE,"-------------",new ColumnSpacing(1),"----------------",new 
                        ColumnSpacing(17),"---------------",new ColumnSpacing(2),"----------",new ColumnSpacing(2),"----------",new ColumnSpacing(3),"-----------",new 
                        ColumnSpacing(2),pnd_Uln1);
                    //*    / 'TIME:' *TIMX 25X #ID 'CONTRACT PACKAGE DETAIL REPORT'
                    //*  // 'PIN #      ' 1X 'PARTICIPANT NAME' 17X #ID-CONT
                    //*   / '-----------' 1X '----------------' 17X '---------------'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*   NEWPAGE(1)
                    if (condition(pnd_Id.notEquals("TPA") && pnd_Id.notEquals("IPRO")))                                                                                   //Natural: IF #ID NE 'TPA' AND #ID NE 'IPRO'
                    {
                        getReports().write(2, Global.getPROGRAM());                                                                                                       //Natural: WRITE ( 2 ) *PROGRAM
                        getReports().write(2, "DATE:",Global.getDATX(),new ColumnSpacing(30),"CONSOLIDATED ISSUE SYSTEM",new ColumnSpacing(20),"PAGE:",getReports().getPageNumberDbs(2),NEWLINE,"TIME:",Global.getTIMX(),new  //Natural: WRITE ( 2 ) 'DATE:' *DATX 30X 'CONSOLIDATED ISSUE SYSTEM' 20X 'PAGE:' *PAGE-NUMBER ( 2 ) / 'TIME:' *TIMX 25X #ID-NAME2 /// 5X '  USER   ' 5X ' SYSTEM   ' / 5X 'REQUESTED' 5X 'DETERMINED' 5X 'UNAPPROVED' / 5X 'PULLOUT  ' 5X 'PULLOUTS  ' 5X '  STATES  ' 5X 'AUTOMATED' 5X 'OVERNIGHT' / 5X #JOB-DESC-1 #JOB-DESC-2 / 5X '----------' 4X '----------' 5X '----------' 5X '----------' 4X '---------'
                            ColumnSpacing(25),pnd_Id_Name2,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(5),"  USER   ",new ColumnSpacing(5)," SYSTEM   ",NEWLINE,new 
                            ColumnSpacing(5),"REQUESTED",new ColumnSpacing(5),"DETERMINED",new ColumnSpacing(5),"UNAPPROVED",NEWLINE,new ColumnSpacing(5),"PULLOUT  ",new 
                            ColumnSpacing(5),"PULLOUTS  ",new ColumnSpacing(5),"  STATES  ",new ColumnSpacing(5),"AUTOMATED",new ColumnSpacing(5),"OVERNIGHT",NEWLINE,new 
                            ColumnSpacing(5),pnd_Job_Desc_1,pnd_Job_Desc_2,NEWLINE,new ColumnSpacing(5),"----------",new ColumnSpacing(4),"----------",new 
                            ColumnSpacing(5),"----------",new ColumnSpacing(5),"----------",new ColumnSpacing(4),"---------");
                        //*    / 'TIME:' *TIMX 25X #ID 'CONTRACT PACKAGE DETAIL SUMMARY REPORT'
                        //*  TPA AND IPRO ONLY
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, Global.getPROGRAM());                                                                                                       //Natural: WRITE ( 2 ) *PROGRAM
                        getReports().write(2, "DATE:",Global.getDATX(),new ColumnSpacing(30),"CONSOLIDATED ISSUE SYSTEM",new ColumnSpacing(20),"PAGE:",getReports().getPageNumberDbs(2),NEWLINE,"TIME:",Global.getTIMX(),new  //Natural: WRITE ( 2 ) 'DATE:' *DATX 30X 'CONSOLIDATED ISSUE SYSTEM' 20X 'PAGE:' *PAGE-NUMBER ( 2 ) / 'TIME:' *TIMX 25X #ID-NAME2 /// 5X '  USER   ' / 5X 'REQUESTED' / 5X 'PULLOUT  ' 5X 'AUTOMATED' / 5X #JOB-DESC-1 / 5X '----------' 4X '----------'
                            ColumnSpacing(25),pnd_Id_Name2,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(5),"  USER   ",NEWLINE,new ColumnSpacing(5),"REQUESTED",NEWLINE,new 
                            ColumnSpacing(5),"PULLOUT  ",new ColumnSpacing(5),"AUTOMATED",NEWLINE,new ColumnSpacing(5),pnd_Job_Desc_1,NEWLINE,new ColumnSpacing(5),"----------",new 
                            ColumnSpacing(4),"----------");
                        //*    / 'TIME:' *TIMX 25X #ID 'CONTRACT PACKAGE DETAIL SUMMARY REPORT'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventRd() throws Exception {atBreakEventRd(false);}
    private void atBreakEventRd(boolean endOfData) throws Exception
    {
        boolean pnd_Part_Wfile_Pnd_Job_NameIsBreak = pnd_Part_Wfile_Pnd_Job_Name.isBreak(endOfData);
        boolean pnd_Part_Wfile_Pnd_Cntrct_Sys_IdIsBreak = pnd_Part_Wfile_Pnd_Cntrct_Sys_Id.isBreak(endOfData);
        if (condition(pnd_Part_Wfile_Pnd_Job_NameIsBreak || pnd_Part_Wfile_Pnd_Cntrct_Sys_IdIsBreak))
        {
            getReports().write(1, NEWLINE,NEWLINE,"  ** NUMBER OF RECORDS TOTAL:",pnd_Tot, new ReportEditMask ("Z,ZZ9"));                                                 //Natural: WRITE ( 1 ) // '  ** NUMBER OF RECORDS TOTAL:' #TOT
            if (condition(Global.isEscape())) return;
            pnd_Tot.reset();                                                                                                                                              //Natural: RESET #TOT
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Part_Wfile_Pnd_Cntrct_Sys_IdIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-SUMMARY
            sub_Determine_Summary();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=132");
        Global.format(0, "PS=60 LS=80");
    }
}
