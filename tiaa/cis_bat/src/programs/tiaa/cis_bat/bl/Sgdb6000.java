/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:23:41 PM
**        * FROM NATURAL PROGRAM : Sgdb6000
************************************************************
**        * FILE NAME            : Sgdb6000.java
**        * CLASS NAME           : Sgdb6000
**        * INSTANCE NAME        : Sgdb6000
************************************************************
************************************************************************
* PROGRAM : SGDB6000
* SYSTEM  : TOPS RELEASE XX
* TITLE   : UPDATE CIS FOR NEW TPA/IPRO ISSUED BY OMNI STATION
* FUNCTION: UPDATE CIS
* CREATED : SEPT, 2007 BY K.GATES
*         :
* DETAILS :
*         :
* MAINT HISTORY
*
* 4 FEB 08 D.GEARY  ADD CIS FLAG FOR IPRO ATRA SUBPLANS.
* 10/2009  JJG    INC820022-PRB43074-ACTION11604 MOVE #CIS-ORG-ISSUE-ST
*                 TO CIS-ORIG-ISSUE-STATE INSTEAD OF #RESID-STAE
*
* 09/2011 B.HOLLOWAY - CORRECT THE LOGIC THAT POPULATES THE ADDRESS
*                      FIELDS ON THE CIS-PARTCPNT-FILE
*                    - ADDED #DEBUG(L) FIELD TO FACILITATE TESTING -
*                      THE INIT VALUE OF #DEBUG MUST BE FALSE BEFORE
*                      MOVING THIS MODULE TO PRODUCTION
* 06/2014 B. NEWSOM  - INTRODUCED A NEW PARM FILE (WORKFILE 2)    (CREA)
*                      CONTAINING THE FUND/TICKER INFORMATION     (CREA)
*                      NEEDED TO PROCESS THE RECORDS. THIS ELIMI- (CREA)
*                      NATES THE HARDCODING OF THE TICKER SYMBOLS.(CREA)
*                    - ADDED AN ON ERROR ROUTINE TO TRAP SYSTEM   (CREA)
*                      ERRORS AND DISPLAY A MESSAGE BEFORE THE    (CREA)
*                      PROGRAM ABENDS.                            (CREA)
*                     -REMOVED DEAD CODE - REPORT-INTERNAL-ROLLOVER
* 09/2015 B. ELLO    - RESTOW FOR CISL200 - ADDED NEW FIELDS IN   (BIP)
*                    - THE CIS-PRTCPNT-FILE FOR BENE IN THE PLAN. (BIP)
* 10/2015 RJ FRANKLIN- RESET THE WORKFILE STORAGE AREA IN THE EVENT OF
*                      BYPASSED RECORDS.                     (CHG365909)
*  08/31/2016 - NEWSOM  - PIN EXPANSION PROJECT                 (PINEXP)
* 03/12/2019  - ELLO   REMOVED PDA ALCA911 SINCE IT IS NOT USED.
* 05/31/2019  - ELLO   ADDED UPDATE TO ACIS REPRINT FILE FOR THE IPRO
*                      CONTRACT                             (CNTRSTRTGY)
************************************************************************

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Sgdb6000 extends BLNatBase
{
    // Data Areas
    private LdaCisl200 ldaCisl200;
    private LdaCisl2020 ldaCisl2020;
    private LdaAppl170 ldaAppl170;
    private PdaNeca4000 pdaNeca4000;
    private PdaCisa4000 pdaCisa4000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_acis_Reprint_Fl;
    private DbsField acis_Reprint_Fl_Rp_Tiaa_Contr;
    private DbsField acis_Reprint_Fl_Count_Castrp_Related_Contract_Info;

    private DbsGroup acis_Reprint_Fl_Rp_Related_Contract_Info;
    private DbsField acis_Reprint_Fl_Rp_Related_Contract_Type;
    private DbsField acis_Reprint_Fl_Rp_Related_Tiaa_No;
    private DbsField acis_Reprint_Fl_Rp_Related_Tiaa_Issue_Date;
    private DbsField acis_Reprint_Fl_Rp_Related_First_Payment_Date;
    private DbsField acis_Reprint_Fl_Rp_Related_Tiaa_Total_Amt;
    private DbsField acis_Reprint_Fl_Rp_Related_Last_Payment_Date;
    private DbsField acis_Reprint_Fl_Rp_Related_Payment_Frequency;
    private DbsField acis_Reprint_Fl_Rp_Related_Tiaa_Issue_State;

    private DbsGroup pnd_In_Translte_File;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Ticker;
    private DbsField pnd_In_Translte_File_Pnd_F1;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name;
    private DbsField pnd_In_Translte_File_Pnd_F2;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq;
    private DbsField pnd_In_Translte_File_Pnd_F3;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Pull_Code;
    private DbsField pnd_In_Translte_File_Pnd_F4;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Fund_Type;
    private DbsField pnd_In_Translte_File_Pnd_F5;

    private DbsGroup pnd_Translte_File;
    private DbsField pnd_Translte_File_Pnd_Translte_Ticker;
    private DbsField pnd_Translte_File_Pnd_F1;
    private DbsField pnd_Translte_File_Pnd_Translte_Ticker_Name;
    private DbsField pnd_Translte_File_Pnd_F2;
    private DbsField pnd_Translte_File_Pnd_Translte_Fund_Seq;
    private DbsField pnd_Translte_File_Pnd_F3;
    private DbsField pnd_Translte_File_Pnd_Translte_Pull_Code;
    private DbsField pnd_Translte_File_Pnd_F4;
    private DbsField pnd_Translte_File_Pnd_Translte_Fund_Type;
    private DbsField pnd_Translte_File_Pnd_F5;
    private DbsField pnd_Translte_File_Pnd_Translte_Error_Message;

    private DbsGroup pnd_Rec;
    private DbsField pnd_Rec_Pnd_Rec_Data;

    private DbsGroup pnd_Rec__R_Field_1;
    private DbsField pnd_Rec_Pnd_Rec_Data_P;
    private DbsField pnd_Rec_Pnd_Rec_Data_Fil;
    private DbsField pnd_Rec_Pnd_Rec_Data_1;
    private DbsField pnd_Rec_Pnd_Rec_Data_2;
    private DbsField pnd_Input_Part_Aa;

    private DbsGroup pnd_Input_Part;
    private DbsField pnd_Input_Part_Pnd_Rec_Iden;
    private DbsField pnd_Input_Part_Pnd_Rec_Type;
    private DbsField pnd_Input_Part_Pnd_Plan_Name;
    private DbsField pnd_Input_Part_Pnd_Plan_Id;
    private DbsField pnd_Input_Part_Pnd_Erisa_Ind;
    private DbsField pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct;
    private DbsField pnd_Input_Part_Pnd_Cis_Tpa_Cref_Cntrct;
    private DbsField pnd_Input_Part_Pnd_Cis_Pin_Nbr;

    private DbsGroup pnd_Input_Part__R_Field_2;
    private DbsField pnd_Input_Part_Pnd_Cis_Pin_Nbr_N;

    private DbsGroup pnd_Input_Part__R_Field_3;
    private DbsField pnd_Input_Part_Pnd_Cis_Pin_Nbr_N7;
    private DbsField pnd_Input_Part_Pnd_Cis_Pin_Filler;
    private DbsField pnd_Input_Part_Pnd_Cis_Rqst_Id_Key;
    private DbsField pnd_Input_Part_Pnd_Cis_Tpa_Cntrct;
    private DbsField pnd_Input_Part_Pnd_Cis_Requestor;
    private DbsField pnd_Input_Part_Pnd_Cis_Cntrct_Type;
    private DbsField pnd_Input_Part_Pnd_Cis_Opn_Clsd_Ind;
    private DbsField pnd_Input_Part_Pnd_Cis_Status_Cd;
    private DbsField pnd_Input_Part_Pnd_Cis_Annty_Option;
    private DbsField pnd_Input_Part_Pnd_Cis_Cntrct_Print_Dte;
    private DbsField pnd_Input_Part_Pnd_Cis_Frst_Annt_Frst_Nme;
    private DbsField pnd_Input_Part_Pnd_Cis_Frst_Annt_Mid_Nme;
    private DbsField pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme;
    private DbsField pnd_Input_Part_Pnd_Cis_Frst_Annt_Prf_Nme;
    private DbsField pnd_Input_Part_Pnd_Cis_Frst_Annt_Sex_Cde;
    private DbsField pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn;

    private DbsGroup pnd_Input_Part__R_Field_4;
    private DbsField pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn_N;
    private DbsField pnd_Input_Part_Pnd_Cis_Frst_Annt_Dob;
    private DbsField pnd_Input_Part_Pnd_Addrss_Line_1;
    private DbsField pnd_Input_Part_Pnd_Addrss_Line_2;
    private DbsField pnd_Input_Part_Pnd_Addrss_Line_2_3;
    private DbsField pnd_Input_Part_Pnd_Addrss_Line_3;
    private DbsField pnd_Input_Part_Pnd_City;
    private DbsField pnd_Input_Part_Pnd_Resid_Stae;
    private DbsField pnd_Input_Part_Pnd_Zip;
    private DbsField pnd_Input_Part_Pnd_Term_Date;
    private DbsField pnd_Input_Part_Pnd_Cis_Frst_Annt_Ctznshp_Cd;
    private DbsField pnd_Input_Part_Pnd_Cis_Tiaa_Doi;
    private DbsField pnd_Input_Part_Pnd_Cis_Annty_Start_Dte;
    private DbsField pnd_Input_Part_Pnd_Cis_Annty_End_Dte;
    private DbsField pnd_Input_Part_Pnd_Cis_Pymnt_Mode;
    private DbsField pnd_Input_Part_Pnd_Cis_Grnted_Periods_Yrs;

    private DbsGroup pnd_Input_Part__R_Field_5;
    private DbsField pnd_Input_Part_Pnd_Cis_Grnted_Periods_Yrs_N;
    private DbsField pnd_Input_Part_Pnd_Cis_Grnted_Grd_Amt;

    private DbsGroup pnd_Input_Part__R_Field_6;
    private DbsField pnd_Input_Part_Pnd_Cis_Grnted_Grd_Amt_N;
    private DbsField pnd_Input_Part_Pnd_Cis_Lob;
    private DbsField pnd_Input_Part_Pnd_Cis_Lob_Type;
    private DbsField pnd_Input_Part_Pnd_Cis_Da_Tiaa_Nbr_Sttl;
    private DbsField pnd_Input_Part_Pnd_Cis_Da_Proceeds_Amt;

    private DbsGroup pnd_Input_Part__R_Field_7;
    private DbsField pnd_Input_Part_Pnd_Cis_Da_Proceeds_Amt_N;
    private DbsField pnd_Input_Part_Pnd_Cis_Rtb_Amt;

    private DbsGroup pnd_Input_Part__R_Field_8;
    private DbsField pnd_Input_Part_Pnd_Cis_Rtb_Amt_N;
    private DbsField pnd_Input_Part_Pnd_Cis_Rtb_Pct;

    private DbsGroup pnd_Input_Part__R_Field_9;
    private DbsField pnd_Input_Part_Pnd_Cis_Rtb_Pct_N;
    private DbsField pnd_Input_Part_Pnd_Cis_Trnsf_Tiaa_Rea_Nbr;
    private DbsField pnd_Input_Part_Pnd_Cis_Trnsf_Tiaa_Cer_Nbr;
    private DbsField pnd_Input_Part_Pnd_Cis_Handle_Code;
    private DbsField pnd_Input_Part_Pnd_Cis_Org_Issue_St;
    private DbsField pnd_Input_Part_Pnd_Fill;
    private DbsField pnd_Input_Part_Pnd_Cis_Comut_Grnted_Amt;

    private DbsGroup pnd_Input_Part__R_Field_10;
    private DbsField pnd_Input_Part_Pnd_Cis_Comut_Grnted_Amt_N;
    private DbsField pnd_Input_Part_Pnd_Cis_Comut_Grnted_Pct;

    private DbsGroup pnd_Input_Part__R_Field_11;
    private DbsField pnd_Input_Part_Pnd_Cis_Comut_Grnted_Pct_N;
    private DbsField pnd_Input_Part_Pnd_Dest_Name;
    private DbsField pnd_Input_Part_Pnd_Dest_Addr_Line;
    private DbsField pnd_Input_Part_Pnd_Dest_Addr_Line_2;
    private DbsField pnd_Input_Part_Pnd_Dest_Addr_Line_3;
    private DbsField pnd_Input_Part_Pnd_Dest_Addr_City;
    private DbsField pnd_Input_Part_Pnd_Dest_Addr_St;
    private DbsField pnd_Input_Part_Pnd_Dest_Addr_Zip;
    private DbsField pnd_Input_Part_Pnd_Dest_Bank_Type;
    private DbsField pnd_Input_Part_Pnd_Dest_Bank_Acct_Nbr;
    private DbsField pnd_Input_Part_Pnd_Dest_Bank_Trst_Nbr;
    private DbsField pnd_Input_Part_Pnd_Rollover_Dest_Name;
    private DbsField pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line;
    private DbsField pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_2;
    private DbsField pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_3;
    private DbsField pnd_Input_Part_Pnd_Rollover_Dest_Addr_City;
    private DbsField pnd_Input_Part_Pnd_Rollover_Dest_Addr_St;
    private DbsField pnd_Input_Part_Pnd_Rollover_Dest_Addr_Zip;
    private DbsField pnd_Input_Part_Pnd_Rollover_Dest_Bank_Acct_Nbr;
    private DbsField pnd_Input_Part_Pnd_Power_Image_Id;
    private DbsField pnd_Input_Part_Pnd_Fed_Ovr_Or_Add;
    private DbsField pnd_Input_Part_Pnd_Fed_Dol_Or_Pct;
    private DbsField pnd_Input_Part_Pnd_Fed_Pct_Amt_Or_Dol;

    private DbsGroup pnd_Input_Part__R_Field_12;
    private DbsField pnd_Input_Part_Pnd_Fed_Pct_Amt_Or_Dol_N;
    private DbsField pnd_Input_Part_Pnd_Fed_No_Of_Exempt;

    private DbsGroup pnd_Input_Part__R_Field_13;
    private DbsField pnd_Input_Part_Pnd_Fed_No_Of_Exempt_N;
    private DbsField pnd_Input_Part_Pnd_St_Ovr_Or_Add;
    private DbsField pnd_Input_Part_Pnd_St_Dol_Or_Pct;
    private DbsField pnd_Input_Part_Pnd_St_Pct_Amt_Or_Dol;

    private DbsGroup pnd_Input_Part__R_Field_14;
    private DbsField pnd_Input_Part_Pnd_St_Pct_Amt_Or_Dol_N;
    private DbsField pnd_Input_Part_Pnd_St_No_Of_Exempt;

    private DbsGroup pnd_Input_Part__R_Field_15;
    private DbsField pnd_Input_Part_Pnd_St_No_Of_Exempt_N;
    private DbsField pnd_Input_Part_Pnd_Fed_Opt;
    private DbsField pnd_Input_Part_Pnd_St_Opt;
    private DbsField pnd_Input_Part_Pnd_Fed_Marital_Sts;
    private DbsField pnd_Input_Part_Pnd_St_Marital_Sts;
    private DbsField pnd_Input_Part_Pnd_Dollar_Or_Pcnt;
    private DbsField pnd_Input_Part_Pnd_Dollar_Or_Pcnt_Amt;
    private DbsField pnd_Input_Part_Pnd_Sub_Plan;
    private DbsField pnd_Input_Part_Pnd_Foreign_Or_Domestic;
    private DbsField pnd_Input_Part_Pnd_Country_For_Dometic;
    private DbsField pnd_Input_Part_Pnd_Fund_Allocation;

    private DbsGroup pnd_Input_Part__R_Field_16;
    private DbsField pnd_Input_Part_Pnd_Fund_Ticker;
    private DbsField pnd_Input_Part_Pnd_Fund_Alloc;

    private DbsGroup pnd_Input_Part__R_Field_17;
    private DbsField pnd_Input_Part_Pnd_Input_Part_A;

    private DbsGroup pnd_Input_Part__R_Field_18;
    private DbsField pnd_Input_Part_Pnd_Rec_Ident;
    private DbsField pnd_Input_Part_Pnd_Rec_Type_B;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Tpa_Tiaa;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Tpa_Cref;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Pin;

    private DbsGroup pnd_Input_Part__R_Field_19;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Pin_N;

    private DbsGroup pnd_Input_Part__R_Field_20;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Pin_N7;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Pin_Filler;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Frst_Annt_Fname;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Last_Annt_Nme;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Frst_Gender;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Frst_Ssn;

    private DbsGroup pnd_Input_Part__R_Field_21;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Frst_Ssn_N;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Frst_Dob;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Ssn;

    private DbsGroup pnd_Input_Part__R_Field_22;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Ssn_N;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Name;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Dob;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Reltn_Cde;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Reltn;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Category;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Alloc;

    private DbsGroup pnd_Input_Part__R_Field_23;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Alloc_1_N;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Alloc_1_A;
    private DbsField pnd_Input_Part_Pnd_Cis_Bene_Alloc_4_N;

    private DbsGroup pnd_Input_Part__R_Field_24;
    private DbsField pnd_Input_Part_Pnd_Rec_Iden_Uppm;
    private DbsField pnd_Input_Part_Pnd_Rec_Type_Uppm;
    private DbsField pnd_Input_Part_Pnd_Intrest_Pymt_In_Rate;

    private DbsGroup pnd_Input_Part__R_Field_25;
    private DbsField pnd_Input_Part_Pnd_Intrest_Pymt_In_Rate_N;
    private DbsField pnd_Input_Part_Pnd_Intrest_Pymt_Ia;

    private DbsGroup pnd_Input_Part__R_Field_26;
    private DbsField pnd_Input_Part_Pnd_Intrest_Pymt_Ia_N;
    private DbsField pnd_Input_Part_Pnd_Payment;

    private DbsGroup pnd_Input_Part__R_Field_27;
    private DbsField pnd_Input_Part_Pnd_Payment_N;
    private DbsField pnd_Input_Part_Pnd_Table_Discription;
    private DbsField pnd_Alloc;

    private DbsGroup pnd_Alloc__R_Field_28;
    private DbsField pnd_Alloc_Pnd_Alloc_N;
    private DbsField pnd_Alloc_Pnd_Alloc_N_2;
    private DbsField pnd_Bene_Alloc;
    private DbsField pnd_Bene_Cnt_Prmry;
    private DbsField pnd_Bene_Cnt_Cntgnt;
    private DbsField pnd_X;
    private DbsField pnd_Rsdnce_St;

    private DbsGroup pnd_Rsdnce_St__R_Field_29;
    private DbsField pnd_Rsdnce_St_Pnd_Rsdnce_St_A;
    private DbsField pnd_Escape_Top;
    private DbsField pnd_Fnd_Al_Tic;

    private DbsGroup pnd_Fnd_Al_Tic__R_Field_30;
    private DbsField pnd_Fnd_Al_Tic_Pnd_Fnd_Tic;
    private DbsField pnd_Fnd_Al_Tic_Pnd_Fnd_Al;
    private DbsField pnd_City_Done;
    private DbsField pnd_Uppm;
    private DbsField pnd_Int_Rate;

    private DbsGroup pnd_Int_Rate__R_Field_31;
    private DbsField pnd_Int_Rate_Pnd_Int_Rate_A2;
    private DbsField pnd_Int_Rate_Pnd_Int_Rate_B2;
    private DbsField pnd_Uppm_Collected;
    private DbsField pnd_Mit_Effective_Dt_N;

    private DbsGroup pnd_Mit_Effective_Dt_N__R_Field_32;
    private DbsField pnd_Mit_Effective_Dt_N_Pnd_Mit_Effective_Dt;
    private DbsField pnd_Total_Add;
    private DbsField pnd_Total_Dup;
    private DbsField pnd_Total_Err;
    private DbsField pnd_Err_Message;
    private DbsField pnd_Tiaa_Cntrct_Found;

    private DbsGroup pnd_Hold_Address;
    private DbsField pnd_Hold_Address_Pnd_Hdest_Addr1;
    private DbsField pnd_Hold_Address_Pnd_Hdest_Addr2;
    private DbsField pnd_Hold_Address_Pnd_Hdest_Addr3;
    private DbsField pnd_Hold_Address_Pnd_Hdest_City;
    private DbsField pnd_Hold_Address_Pnd_Hdest_State;
    private DbsField pnd_Hold_Address_Pnd_Hzip;
    private DbsField pnd_Foreign_Or_Canada;
    private DbsField pnd_Addr_Indx;
    private DbsField pnd_Fund_Idx;
    private DbsField pnd_Fund_Count;
    private DbsField pnd_Max_Count;
    private DbsField pnd_Debug;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCisl200 = new LdaCisl200();
        registerRecord(ldaCisl200);
        registerRecord(ldaCisl200.getVw_cis_Prtcpnt_File());
        ldaCisl2020 = new LdaCisl2020();
        registerRecord(ldaCisl2020);
        registerRecord(ldaCisl2020.getVw_cis_Bene_File_01());
        ldaAppl170 = new LdaAppl170();
        registerRecord(ldaAppl170);
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaCisa4000 = new PdaCisa4000(localVariables);

        // Local Variables

        vw_acis_Reprint_Fl = new DataAccessProgramView(new NameInfo("vw_acis_Reprint_Fl", "ACIS-REPRINT-FL"), "ACIS_REPRINT_FL_12", "ACIS_RPRNT_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("ACIS_REPRINT_FL_12"));
        acis_Reprint_Fl_Rp_Tiaa_Contr = vw_acis_Reprint_Fl.getRecord().newFieldInGroup("acis_Reprint_Fl_Rp_Tiaa_Contr", "RP-TIAA-CONTR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "RP_TIAA_CONTR");
        acis_Reprint_Fl_Rp_Tiaa_Contr.setDdmHeader("TIAA/CONTR");
        acis_Reprint_Fl_Count_Castrp_Related_Contract_Info = vw_acis_Reprint_Fl.getRecord().newFieldInGroup("acis_Reprint_Fl_Count_Castrp_Related_Contract_Info", 
            "C*RP-RELATED-CONTRACT-INFO", RepeatingFieldStrategy.CAsteriskVariable, "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");

        acis_Reprint_Fl_Rp_Related_Contract_Info = vw_acis_Reprint_Fl.getRecord().newGroupArrayInGroup("acis_Reprint_Fl_Rp_Related_Contract_Info", "RP-RELATED-CONTRACT-INFO", 
            new DbsArrayController(1, 30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Contract_Type = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Contract_Type", 
            "RP-RELATED-CONTRACT-TYPE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_CONTRACT_TYPE", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Tiaa_No = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Tiaa_No", "RP-RELATED-TIAA-NO", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_NO", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Tiaa_Issue_Date = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Tiaa_Issue_Date", 
            "RP-RELATED-TIAA-ISSUE-DATE", FieldType.NUMERIC, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_ISSUE_DATE", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_First_Payment_Date = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_First_Payment_Date", 
            "RP-RELATED-FIRST-PAYMENT-DATE", FieldType.NUMERIC, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_FIRST_PAYMENT_DATE", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Tiaa_Total_Amt = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Tiaa_Total_Amt", 
            "RP-RELATED-TIAA-TOTAL-AMT", FieldType.NUMERIC, 10, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_TOTAL_AMT", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Last_Payment_Date = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Last_Payment_Date", 
            "RP-RELATED-LAST-PAYMENT-DATE", FieldType.NUMERIC, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_LAST_PAYMENT_DATE", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Payment_Frequency = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Payment_Frequency", 
            "RP-RELATED-PAYMENT-FREQUENCY", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_PAYMENT_FREQUENCY", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Tiaa_Issue_State = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Tiaa_Issue_State", 
            "RP-RELATED-TIAA-ISSUE-STATE", FieldType.STRING, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_ISSUE_STATE", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        registerRecord(vw_acis_Reprint_Fl);

        pnd_In_Translte_File = localVariables.newGroupInRecord("pnd_In_Translte_File", "#IN-TRANSLTE-FILE");
        pnd_In_Translte_File_Pnd_In_Translte_Ticker = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Ticker", "#IN-TRANSLTE-TICKER", 
            FieldType.STRING, 10);
        pnd_In_Translte_File_Pnd_F1 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name", "#IN-TRANSLTE-TICKER-NAME", 
            FieldType.STRING, 26);
        pnd_In_Translte_File_Pnd_F2 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq", "#IN-TRANSLTE-FUND-SEQ", 
            FieldType.NUMERIC, 2);
        pnd_In_Translte_File_Pnd_F3 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Pull_Code = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Pull_Code", "#IN-TRANSLTE-PULL-CODE", 
            FieldType.STRING, 4);
        pnd_In_Translte_File_Pnd_F4 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Fund_Type = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Fund_Type", "#IN-TRANSLTE-FUND-TYPE", 
            FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_F5 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F5", "#F5", FieldType.STRING, 153);

        pnd_Translte_File = localVariables.newGroupArrayInRecord("pnd_Translte_File", "#TRANSLTE-FILE", new DbsArrayController(1, 20));
        pnd_Translte_File_Pnd_Translte_Ticker = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Ticker", "#TRANSLTE-TICKER", FieldType.STRING, 
            10);
        pnd_Translte_File_Pnd_F1 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Ticker_Name = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Ticker_Name", "#TRANSLTE-TICKER-NAME", 
            FieldType.STRING, 26);
        pnd_Translte_File_Pnd_F2 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Fund_Seq = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Fund_Seq", "#TRANSLTE-FUND-SEQ", FieldType.NUMERIC, 
            2);
        pnd_Translte_File_Pnd_F3 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Pull_Code = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Pull_Code", "#TRANSLTE-PULL-CODE", 
            FieldType.STRING, 4);
        pnd_Translte_File_Pnd_F4 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Fund_Type = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Fund_Type", "#TRANSLTE-FUND-TYPE", 
            FieldType.STRING, 1);
        pnd_Translte_File_Pnd_F5 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F5", "#F5", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Error_Message = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Error_Message", "#TRANSLTE-ERROR-MESSAGE", 
            FieldType.STRING, 40);

        pnd_Rec = localVariables.newGroupInRecord("pnd_Rec", "#REC");
        pnd_Rec_Pnd_Rec_Data = pnd_Rec.newFieldInGroup("pnd_Rec_Pnd_Rec_Data", "#REC-DATA", FieldType.STRING, 250);

        pnd_Rec__R_Field_1 = pnd_Rec.newGroupInGroup("pnd_Rec__R_Field_1", "REDEFINE", pnd_Rec_Pnd_Rec_Data);
        pnd_Rec_Pnd_Rec_Data_P = pnd_Rec__R_Field_1.newFieldInGroup("pnd_Rec_Pnd_Rec_Data_P", "#REC-DATA-P", FieldType.STRING, 4);
        pnd_Rec_Pnd_Rec_Data_Fil = pnd_Rec__R_Field_1.newFieldInGroup("pnd_Rec_Pnd_Rec_Data_Fil", "#REC-DATA-FIL", FieldType.STRING, 246);
        pnd_Rec_Pnd_Rec_Data_1 = pnd_Rec.newFieldInGroup("pnd_Rec_Pnd_Rec_Data_1", "#REC-DATA-1", FieldType.STRING, 250);
        pnd_Rec_Pnd_Rec_Data_2 = pnd_Rec.newFieldInGroup("pnd_Rec_Pnd_Rec_Data_2", "#REC-DATA-2", FieldType.STRING, 250);
        pnd_Input_Part_Aa = localVariables.newFieldArrayInRecord("pnd_Input_Part_Aa", "#INPUT-PART-AA", FieldType.STRING, 1, new DbsArrayController(1, 
            1249));

        pnd_Input_Part = localVariables.newGroupInRecord("pnd_Input_Part", "#INPUT-PART");
        pnd_Input_Part_Pnd_Rec_Iden = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Rec_Iden", "#REC-IDEN", FieldType.STRING, 4);
        pnd_Input_Part_Pnd_Rec_Type = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Rec_Type", "#REC-TYPE", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Plan_Name = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Plan_Name", "#PLAN-NAME", FieldType.STRING, 32);
        pnd_Input_Part_Pnd_Plan_Id = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Plan_Id", "#PLAN-ID", FieldType.STRING, 6);
        pnd_Input_Part_Pnd_Erisa_Ind = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Erisa_Ind", "#ERISA-IND", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct", "#CIS-TPA-TIAA-CNTRCT", FieldType.STRING, 
            10);
        pnd_Input_Part_Pnd_Cis_Tpa_Cref_Cntrct = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Tpa_Cref_Cntrct", "#CIS-TPA-CREF-CNTRCT", FieldType.STRING, 
            10);
        pnd_Input_Part_Pnd_Cis_Pin_Nbr = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Pin_Nbr", "#CIS-PIN-NBR", FieldType.STRING, 12);

        pnd_Input_Part__R_Field_2 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_2", "REDEFINE", pnd_Input_Part_Pnd_Cis_Pin_Nbr);
        pnd_Input_Part_Pnd_Cis_Pin_Nbr_N = pnd_Input_Part__R_Field_2.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Pin_Nbr_N", "#CIS-PIN-NBR-N", FieldType.NUMERIC, 
            12);

        pnd_Input_Part__R_Field_3 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_3", "REDEFINE", pnd_Input_Part_Pnd_Cis_Pin_Nbr);
        pnd_Input_Part_Pnd_Cis_Pin_Nbr_N7 = pnd_Input_Part__R_Field_3.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Pin_Nbr_N7", "#CIS-PIN-NBR-N7", FieldType.NUMERIC, 
            7);
        pnd_Input_Part_Pnd_Cis_Pin_Filler = pnd_Input_Part__R_Field_3.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Pin_Filler", "#CIS-PIN-FILLER", FieldType.STRING, 
            5);
        pnd_Input_Part_Pnd_Cis_Rqst_Id_Key = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Rqst_Id_Key", "#CIS-RQST-ID-KEY", FieldType.STRING, 
            30);
        pnd_Input_Part_Pnd_Cis_Tpa_Cntrct = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Tpa_Cntrct", "#CIS-TPA-CNTRCT", FieldType.STRING, 10);
        pnd_Input_Part_Pnd_Cis_Requestor = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Requestor", "#CIS-REQUESTOR", FieldType.STRING, 3);
        pnd_Input_Part_Pnd_Cis_Cntrct_Type = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Cntrct_Type", "#CIS-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Input_Part_Pnd_Cis_Opn_Clsd_Ind = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Opn_Clsd_Ind", "#CIS-OPN-CLSD-IND", FieldType.STRING, 
            1);
        pnd_Input_Part_Pnd_Cis_Status_Cd = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Status_Cd", "#CIS-STATUS-CD", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Cis_Annty_Option = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Annty_Option", "#CIS-ANNTY-OPTION", FieldType.STRING, 
            4);
        pnd_Input_Part_Pnd_Cis_Cntrct_Print_Dte = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Cntrct_Print_Dte", "#CIS-CNTRCT-PRINT-DTE", FieldType.STRING, 
            8);
        pnd_Input_Part_Pnd_Cis_Frst_Annt_Frst_Nme = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Frst_Annt_Frst_Nme", "#CIS-FRST-ANNT-FRST-NME", 
            FieldType.STRING, 20);
        pnd_Input_Part_Pnd_Cis_Frst_Annt_Mid_Nme = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Frst_Annt_Mid_Nme", "#CIS-FRST-ANNT-MID-NME", 
            FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme", "#CIS-FRST-ANNT-LST-NME", 
            FieldType.STRING, 20);
        pnd_Input_Part_Pnd_Cis_Frst_Annt_Prf_Nme = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Frst_Annt_Prf_Nme", "#CIS-FRST-ANNT-PRF-NME", 
            FieldType.STRING, 3);
        pnd_Input_Part_Pnd_Cis_Frst_Annt_Sex_Cde = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Frst_Annt_Sex_Cde", "#CIS-FRST-ANNT-SEX-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn", "#CIS-FRST-ANNT-SSN", FieldType.STRING, 
            9);

        pnd_Input_Part__R_Field_4 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_4", "REDEFINE", pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn);
        pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn_N = pnd_Input_Part__R_Field_4.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn_N", "#CIS-FRST-ANNT-SSN-N", 
            FieldType.NUMERIC, 9);
        pnd_Input_Part_Pnd_Cis_Frst_Annt_Dob = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Frst_Annt_Dob", "#CIS-FRST-ANNT-DOB", FieldType.STRING, 
            8);
        pnd_Input_Part_Pnd_Addrss_Line_1 = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Addrss_Line_1", "#ADDRSS-LINE-1", FieldType.STRING, 40);
        pnd_Input_Part_Pnd_Addrss_Line_2 = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Addrss_Line_2", "#ADDRSS-LINE-2", FieldType.STRING, 33);
        pnd_Input_Part_Pnd_Addrss_Line_2_3 = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Addrss_Line_2_3", "#ADDRSS-LINE-2-3", FieldType.STRING, 
            7);
        pnd_Input_Part_Pnd_Addrss_Line_3 = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Addrss_Line_3", "#ADDRSS-LINE-3", FieldType.STRING, 40);
        pnd_Input_Part_Pnd_City = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_City", "#CITY", FieldType.STRING, 28);
        pnd_Input_Part_Pnd_Resid_Stae = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Resid_Stae", "#RESID-STAE", FieldType.STRING, 2);
        pnd_Input_Part_Pnd_Zip = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Zip", "#ZIP", FieldType.STRING, 9);
        pnd_Input_Part_Pnd_Term_Date = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Term_Date", "#TERM-DATE", FieldType.STRING, 8);
        pnd_Input_Part_Pnd_Cis_Frst_Annt_Ctznshp_Cd = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Frst_Annt_Ctznshp_Cd", "#CIS-FRST-ANNT-CTZNSHP-CD", 
            FieldType.STRING, 2);
        pnd_Input_Part_Pnd_Cis_Tiaa_Doi = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Tiaa_Doi", "#CIS-TIAA-DOI", FieldType.STRING, 8);
        pnd_Input_Part_Pnd_Cis_Annty_Start_Dte = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Annty_Start_Dte", "#CIS-ANNTY-START-DTE", FieldType.STRING, 
            8);
        pnd_Input_Part_Pnd_Cis_Annty_End_Dte = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Annty_End_Dte", "#CIS-ANNTY-END-DTE", FieldType.STRING, 
            8);
        pnd_Input_Part_Pnd_Cis_Pymnt_Mode = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Pymnt_Mode", "#CIS-PYMNT-MODE", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Cis_Grnted_Periods_Yrs = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Grnted_Periods_Yrs", "#CIS-GRNTED-PERIODS-YRS", 
            FieldType.STRING, 2);

        pnd_Input_Part__R_Field_5 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_5", "REDEFINE", pnd_Input_Part_Pnd_Cis_Grnted_Periods_Yrs);
        pnd_Input_Part_Pnd_Cis_Grnted_Periods_Yrs_N = pnd_Input_Part__R_Field_5.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Grnted_Periods_Yrs_N", "#CIS-GRNTED-PERIODS-YRS-N", 
            FieldType.NUMERIC, 2);
        pnd_Input_Part_Pnd_Cis_Grnted_Grd_Amt = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Grnted_Grd_Amt", "#CIS-GRNTED-GRD-AMT", FieldType.STRING, 
            11);

        pnd_Input_Part__R_Field_6 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_6", "REDEFINE", pnd_Input_Part_Pnd_Cis_Grnted_Grd_Amt);
        pnd_Input_Part_Pnd_Cis_Grnted_Grd_Amt_N = pnd_Input_Part__R_Field_6.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Grnted_Grd_Amt_N", "#CIS-GRNTED-GRD-AMT-N", 
            FieldType.NUMERIC, 11, 2);
        pnd_Input_Part_Pnd_Cis_Lob = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Lob", "#CIS-LOB", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Cis_Lob_Type = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Lob_Type", "#CIS-LOB-TYPE", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Cis_Da_Tiaa_Nbr_Sttl = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Da_Tiaa_Nbr_Sttl", "#CIS-DA-TIAA-NBR-STTL", FieldType.STRING, 
            10);
        pnd_Input_Part_Pnd_Cis_Da_Proceeds_Amt = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Da_Proceeds_Amt", "#CIS-DA-PROCEEDS-AMT", FieldType.STRING, 
            11);

        pnd_Input_Part__R_Field_7 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_7", "REDEFINE", pnd_Input_Part_Pnd_Cis_Da_Proceeds_Amt);
        pnd_Input_Part_Pnd_Cis_Da_Proceeds_Amt_N = pnd_Input_Part__R_Field_7.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Da_Proceeds_Amt_N", "#CIS-DA-PROCEEDS-AMT-N", 
            FieldType.NUMERIC, 11, 2);
        pnd_Input_Part_Pnd_Cis_Rtb_Amt = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Rtb_Amt", "#CIS-RTB-AMT", FieldType.STRING, 11);

        pnd_Input_Part__R_Field_8 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_8", "REDEFINE", pnd_Input_Part_Pnd_Cis_Rtb_Amt);
        pnd_Input_Part_Pnd_Cis_Rtb_Amt_N = pnd_Input_Part__R_Field_8.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Rtb_Amt_N", "#CIS-RTB-AMT-N", FieldType.NUMERIC, 
            11, 2);
        pnd_Input_Part_Pnd_Cis_Rtb_Pct = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Rtb_Pct", "#CIS-RTB-PCT", FieldType.STRING, 4);

        pnd_Input_Part__R_Field_9 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_9", "REDEFINE", pnd_Input_Part_Pnd_Cis_Rtb_Pct);
        pnd_Input_Part_Pnd_Cis_Rtb_Pct_N = pnd_Input_Part__R_Field_9.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Rtb_Pct_N", "#CIS-RTB-PCT-N", FieldType.NUMERIC, 
            4, 2);
        pnd_Input_Part_Pnd_Cis_Trnsf_Tiaa_Rea_Nbr = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Trnsf_Tiaa_Rea_Nbr", "#CIS-TRNSF-TIAA-REA-NBR", 
            FieldType.STRING, 10);
        pnd_Input_Part_Pnd_Cis_Trnsf_Tiaa_Cer_Nbr = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Trnsf_Tiaa_Cer_Nbr", "#CIS-TRNSF-TIAA-CER-NBR", 
            FieldType.STRING, 10);
        pnd_Input_Part_Pnd_Cis_Handle_Code = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Handle_Code", "#CIS-HANDLE-CODE", FieldType.STRING, 
            1);
        pnd_Input_Part_Pnd_Cis_Org_Issue_St = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Org_Issue_St", "#CIS-ORG-ISSUE-ST", FieldType.STRING, 
            2);
        pnd_Input_Part_Pnd_Fill = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Fill", "#FILL", FieldType.STRING, 5);
        pnd_Input_Part_Pnd_Cis_Comut_Grnted_Amt = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Comut_Grnted_Amt", "#CIS-COMUT-GRNTED-AMT", FieldType.STRING, 
            11);

        pnd_Input_Part__R_Field_10 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_10", "REDEFINE", pnd_Input_Part_Pnd_Cis_Comut_Grnted_Amt);
        pnd_Input_Part_Pnd_Cis_Comut_Grnted_Amt_N = pnd_Input_Part__R_Field_10.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Comut_Grnted_Amt_N", "#CIS-COMUT-GRNTED-AMT-N", 
            FieldType.NUMERIC, 11, 2);
        pnd_Input_Part_Pnd_Cis_Comut_Grnted_Pct = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Comut_Grnted_Pct", "#CIS-COMUT-GRNTED-PCT", FieldType.STRING, 
            4);

        pnd_Input_Part__R_Field_11 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_11", "REDEFINE", pnd_Input_Part_Pnd_Cis_Comut_Grnted_Pct);
        pnd_Input_Part_Pnd_Cis_Comut_Grnted_Pct_N = pnd_Input_Part__R_Field_11.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Comut_Grnted_Pct_N", "#CIS-COMUT-GRNTED-PCT-N", 
            FieldType.NUMERIC, 4, 2);
        pnd_Input_Part_Pnd_Dest_Name = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dest_Name", "#DEST-NAME", FieldType.STRING, 32);
        pnd_Input_Part_Pnd_Dest_Addr_Line = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dest_Addr_Line", "#DEST-ADDR-LINE", FieldType.STRING, 40);
        pnd_Input_Part_Pnd_Dest_Addr_Line_2 = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dest_Addr_Line_2", "#DEST-ADDR-LINE-2", FieldType.STRING, 
            40);
        pnd_Input_Part_Pnd_Dest_Addr_Line_3 = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dest_Addr_Line_3", "#DEST-ADDR-LINE-3", FieldType.STRING, 
            40);
        pnd_Input_Part_Pnd_Dest_Addr_City = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dest_Addr_City", "#DEST-ADDR-CITY", FieldType.STRING, 28);
        pnd_Input_Part_Pnd_Dest_Addr_St = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dest_Addr_St", "#DEST-ADDR-ST", FieldType.STRING, 2);
        pnd_Input_Part_Pnd_Dest_Addr_Zip = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dest_Addr_Zip", "#DEST-ADDR-ZIP", FieldType.STRING, 9);
        pnd_Input_Part_Pnd_Dest_Bank_Type = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dest_Bank_Type", "#DEST-BANK-TYPE", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Dest_Bank_Acct_Nbr = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dest_Bank_Acct_Nbr", "#DEST-BANK-ACCT-NBR", FieldType.STRING, 
            17);
        pnd_Input_Part_Pnd_Dest_Bank_Trst_Nbr = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dest_Bank_Trst_Nbr", "#DEST-BANK-TRST-NBR", FieldType.STRING, 
            10);
        pnd_Input_Part_Pnd_Rollover_Dest_Name = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Rollover_Dest_Name", "#ROLLOVER-DEST-NAME", FieldType.STRING, 
            30);
        pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line", "#ROLLOVER-DEST-ADDR-LINE", 
            FieldType.STRING, 40);
        pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_2 = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_2", "#ROLLOVER-DEST-ADDR-LINE-2", 
            FieldType.STRING, 40);
        pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_3 = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_3", "#ROLLOVER-DEST-ADDR-LINE-3", 
            FieldType.STRING, 40);
        pnd_Input_Part_Pnd_Rollover_Dest_Addr_City = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Rollover_Dest_Addr_City", "#ROLLOVER-DEST-ADDR-CITY", 
            FieldType.STRING, 28);
        pnd_Input_Part_Pnd_Rollover_Dest_Addr_St = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Rollover_Dest_Addr_St", "#ROLLOVER-DEST-ADDR-ST", 
            FieldType.STRING, 2);
        pnd_Input_Part_Pnd_Rollover_Dest_Addr_Zip = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Rollover_Dest_Addr_Zip", "#ROLLOVER-DEST-ADDR-ZIP", 
            FieldType.STRING, 9);
        pnd_Input_Part_Pnd_Rollover_Dest_Bank_Acct_Nbr = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Rollover_Dest_Bank_Acct_Nbr", "#ROLLOVER-DEST-BANK-ACCT-NBR", 
            FieldType.STRING, 17);
        pnd_Input_Part_Pnd_Power_Image_Id = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Power_Image_Id", "#POWER-IMAGE-ID", FieldType.STRING, 15);
        pnd_Input_Part_Pnd_Fed_Ovr_Or_Add = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Fed_Ovr_Or_Add", "#FED-OVR-OR-ADD", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Fed_Dol_Or_Pct = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Fed_Dol_Or_Pct", "#FED-DOL-OR-PCT", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Fed_Pct_Amt_Or_Dol = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Fed_Pct_Amt_Or_Dol", "#FED-PCT-AMT-OR-DOL", FieldType.STRING, 
            11);

        pnd_Input_Part__R_Field_12 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_12", "REDEFINE", pnd_Input_Part_Pnd_Fed_Pct_Amt_Or_Dol);
        pnd_Input_Part_Pnd_Fed_Pct_Amt_Or_Dol_N = pnd_Input_Part__R_Field_12.newFieldInGroup("pnd_Input_Part_Pnd_Fed_Pct_Amt_Or_Dol_N", "#FED-PCT-AMT-OR-DOL-N", 
            FieldType.NUMERIC, 11, 2);
        pnd_Input_Part_Pnd_Fed_No_Of_Exempt = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Fed_No_Of_Exempt", "#FED-NO-OF-EXEMPT", FieldType.STRING, 
            1);

        pnd_Input_Part__R_Field_13 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_13", "REDEFINE", pnd_Input_Part_Pnd_Fed_No_Of_Exempt);
        pnd_Input_Part_Pnd_Fed_No_Of_Exempt_N = pnd_Input_Part__R_Field_13.newFieldInGroup("pnd_Input_Part_Pnd_Fed_No_Of_Exempt_N", "#FED-NO-OF-EXEMPT-N", 
            FieldType.NUMERIC, 1);
        pnd_Input_Part_Pnd_St_Ovr_Or_Add = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_St_Ovr_Or_Add", "#ST-OVR-OR-ADD", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_St_Dol_Or_Pct = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_St_Dol_Or_Pct", "#ST-DOL-OR-PCT", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_St_Pct_Amt_Or_Dol = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_St_Pct_Amt_Or_Dol", "#ST-PCT-AMT-OR-DOL", FieldType.STRING, 
            11);

        pnd_Input_Part__R_Field_14 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_14", "REDEFINE", pnd_Input_Part_Pnd_St_Pct_Amt_Or_Dol);
        pnd_Input_Part_Pnd_St_Pct_Amt_Or_Dol_N = pnd_Input_Part__R_Field_14.newFieldInGroup("pnd_Input_Part_Pnd_St_Pct_Amt_Or_Dol_N", "#ST-PCT-AMT-OR-DOL-N", 
            FieldType.NUMERIC, 11, 2);
        pnd_Input_Part_Pnd_St_No_Of_Exempt = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_St_No_Of_Exempt", "#ST-NO-OF-EXEMPT", FieldType.STRING, 
            1);

        pnd_Input_Part__R_Field_15 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_15", "REDEFINE", pnd_Input_Part_Pnd_St_No_Of_Exempt);
        pnd_Input_Part_Pnd_St_No_Of_Exempt_N = pnd_Input_Part__R_Field_15.newFieldInGroup("pnd_Input_Part_Pnd_St_No_Of_Exempt_N", "#ST-NO-OF-EXEMPT-N", 
            FieldType.NUMERIC, 1);
        pnd_Input_Part_Pnd_Fed_Opt = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Fed_Opt", "#FED-OPT", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_St_Opt = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_St_Opt", "#ST-OPT", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Fed_Marital_Sts = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Fed_Marital_Sts", "#FED-MARITAL-STS", FieldType.STRING, 
            1);
        pnd_Input_Part_Pnd_St_Marital_Sts = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_St_Marital_Sts", "#ST-MARITAL-STS", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Dollar_Or_Pcnt = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dollar_Or_Pcnt", "#DOLLAR-OR-PCNT", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Dollar_Or_Pcnt_Amt = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Dollar_Or_Pcnt_Amt", "#DOLLAR-OR-PCNT-AMT", FieldType.STRING, 
            5);
        pnd_Input_Part_Pnd_Sub_Plan = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Sub_Plan", "#SUB-PLAN", FieldType.STRING, 6);
        pnd_Input_Part_Pnd_Foreign_Or_Domestic = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Foreign_Or_Domestic", "#FOREIGN-OR-DOMESTIC", FieldType.STRING, 
            1);
        pnd_Input_Part_Pnd_Country_For_Dometic = pnd_Input_Part.newFieldInGroup("pnd_Input_Part_Pnd_Country_For_Dometic", "#COUNTRY-FOR-DOMETIC", FieldType.STRING, 
            2);
        pnd_Input_Part_Pnd_Fund_Allocation = pnd_Input_Part.newFieldArrayInGroup("pnd_Input_Part_Pnd_Fund_Allocation", "#FUND-ALLOCATION", FieldType.STRING, 
            14, new DbsArrayController(1, 20));

        pnd_Input_Part__R_Field_16 = pnd_Input_Part.newGroupInGroup("pnd_Input_Part__R_Field_16", "REDEFINE", pnd_Input_Part_Pnd_Fund_Allocation);
        pnd_Input_Part_Pnd_Fund_Ticker = pnd_Input_Part__R_Field_16.newFieldArrayInGroup("pnd_Input_Part_Pnd_Fund_Ticker", "#FUND-TICKER", FieldType.STRING, 
            10, new DbsArrayController(1, 20));
        pnd_Input_Part_Pnd_Fund_Alloc = pnd_Input_Part__R_Field_16.newFieldArrayInGroup("pnd_Input_Part_Pnd_Fund_Alloc", "#FUND-ALLOC", FieldType.NUMERIC, 
            4, 1, new DbsArrayController(1, 20));

        pnd_Input_Part__R_Field_17 = localVariables.newGroupInRecord("pnd_Input_Part__R_Field_17", "REDEFINE", pnd_Input_Part);
        pnd_Input_Part_Pnd_Input_Part_A = pnd_Input_Part__R_Field_17.newFieldArrayInGroup("pnd_Input_Part_Pnd_Input_Part_A", "#INPUT-PART-A", FieldType.STRING, 
            1, new DbsArrayController(1, 1249));

        pnd_Input_Part__R_Field_18 = localVariables.newGroupInRecord("pnd_Input_Part__R_Field_18", "REDEFINE", pnd_Input_Part);
        pnd_Input_Part_Pnd_Rec_Ident = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Rec_Ident", "#REC-IDENT", FieldType.STRING, 4);
        pnd_Input_Part_Pnd_Rec_Type_B = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Rec_Type_B", "#REC-TYPE-B", FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Cis_Bene_Tpa_Tiaa = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Tpa_Tiaa", "#CIS-BENE-TPA-TIAA", 
            FieldType.STRING, 10);
        pnd_Input_Part_Pnd_Cis_Bene_Tpa_Cref = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Tpa_Cref", "#CIS-BENE-TPA-CREF", 
            FieldType.STRING, 10);
        pnd_Input_Part_Pnd_Cis_Bene_Pin = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Pin", "#CIS-BENE-PIN", FieldType.STRING, 
            12);

        pnd_Input_Part__R_Field_19 = pnd_Input_Part__R_Field_18.newGroupInGroup("pnd_Input_Part__R_Field_19", "REDEFINE", pnd_Input_Part_Pnd_Cis_Bene_Pin);
        pnd_Input_Part_Pnd_Cis_Bene_Pin_N = pnd_Input_Part__R_Field_19.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Pin_N", "#CIS-BENE-PIN-N", FieldType.NUMERIC, 
            12);

        pnd_Input_Part__R_Field_20 = pnd_Input_Part__R_Field_18.newGroupInGroup("pnd_Input_Part__R_Field_20", "REDEFINE", pnd_Input_Part_Pnd_Cis_Bene_Pin);
        pnd_Input_Part_Pnd_Cis_Bene_Pin_N7 = pnd_Input_Part__R_Field_20.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Pin_N7", "#CIS-BENE-PIN-N7", FieldType.NUMERIC, 
            7);
        pnd_Input_Part_Pnd_Cis_Bene_Pin_Filler = pnd_Input_Part__R_Field_20.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Pin_Filler", "#CIS-BENE-PIN-FILLER", 
            FieldType.STRING, 5);
        pnd_Input_Part_Pnd_Cis_Bene_Frst_Annt_Fname = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Frst_Annt_Fname", "#CIS-BENE-FRST-ANNT-FNAME", 
            FieldType.STRING, 20);
        pnd_Input_Part_Pnd_Cis_Bene_Last_Annt_Nme = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Last_Annt_Nme", "#CIS-BENE-LAST-ANNT-NME", 
            FieldType.STRING, 20);
        pnd_Input_Part_Pnd_Cis_Bene_Frst_Gender = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Frst_Gender", "#CIS-BENE-FRST-GENDER", 
            FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Cis_Bene_Frst_Ssn = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Frst_Ssn", "#CIS-BENE-FRST-SSN", 
            FieldType.STRING, 9);

        pnd_Input_Part__R_Field_21 = pnd_Input_Part__R_Field_18.newGroupInGroup("pnd_Input_Part__R_Field_21", "REDEFINE", pnd_Input_Part_Pnd_Cis_Bene_Frst_Ssn);
        pnd_Input_Part_Pnd_Cis_Bene_Frst_Ssn_N = pnd_Input_Part__R_Field_21.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Frst_Ssn_N", "#CIS-BENE-FRST-SSN-N", 
            FieldType.NUMERIC, 9);
        pnd_Input_Part_Pnd_Cis_Bene_Frst_Dob = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Frst_Dob", "#CIS-BENE-FRST-DOB", 
            FieldType.STRING, 8);
        pnd_Input_Part_Pnd_Cis_Bene_Ssn = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Ssn", "#CIS-BENE-SSN", FieldType.STRING, 
            9);

        pnd_Input_Part__R_Field_22 = pnd_Input_Part__R_Field_18.newGroupInGroup("pnd_Input_Part__R_Field_22", "REDEFINE", pnd_Input_Part_Pnd_Cis_Bene_Ssn);
        pnd_Input_Part_Pnd_Cis_Bene_Ssn_N = pnd_Input_Part__R_Field_22.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Ssn_N", "#CIS-BENE-SSN-N", FieldType.NUMERIC, 
            9);
        pnd_Input_Part_Pnd_Cis_Bene_Name = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Name", "#CIS-BENE-NAME", FieldType.STRING, 
            30);
        pnd_Input_Part_Pnd_Cis_Bene_Dob = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Dob", "#CIS-BENE-DOB", FieldType.STRING, 
            11);
        pnd_Input_Part_Pnd_Cis_Bene_Reltn_Cde = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Reltn_Cde", "#CIS-BENE-RELTN-CDE", 
            FieldType.STRING, 2);
        pnd_Input_Part_Pnd_Cis_Bene_Reltn = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Reltn", "#CIS-BENE-RELTN", FieldType.STRING, 
            10);
        pnd_Input_Part_Pnd_Cis_Bene_Category = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Category", "#CIS-BENE-CATEGORY", 
            FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Cis_Bene_Alloc = pnd_Input_Part__R_Field_18.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Alloc", "#CIS-BENE-ALLOC", FieldType.STRING, 
            6);

        pnd_Input_Part__R_Field_23 = pnd_Input_Part__R_Field_18.newGroupInGroup("pnd_Input_Part__R_Field_23", "REDEFINE", pnd_Input_Part_Pnd_Cis_Bene_Alloc);
        pnd_Input_Part_Pnd_Cis_Bene_Alloc_1_N = pnd_Input_Part__R_Field_23.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Alloc_1_N", "#CIS-BENE-ALLOC-1-N", 
            FieldType.NUMERIC, 1);
        pnd_Input_Part_Pnd_Cis_Bene_Alloc_1_A = pnd_Input_Part__R_Field_23.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Alloc_1_A", "#CIS-BENE-ALLOC-1-A", 
            FieldType.STRING, 1);
        pnd_Input_Part_Pnd_Cis_Bene_Alloc_4_N = pnd_Input_Part__R_Field_23.newFieldInGroup("pnd_Input_Part_Pnd_Cis_Bene_Alloc_4_N", "#CIS-BENE-ALLOC-4-N", 
            FieldType.NUMERIC, 4);

        pnd_Input_Part__R_Field_24 = localVariables.newGroupInRecord("pnd_Input_Part__R_Field_24", "REDEFINE", pnd_Input_Part);
        pnd_Input_Part_Pnd_Rec_Iden_Uppm = pnd_Input_Part__R_Field_24.newFieldInGroup("pnd_Input_Part_Pnd_Rec_Iden_Uppm", "#REC-IDEN-UPPM", FieldType.STRING, 
            4);
        pnd_Input_Part_Pnd_Rec_Type_Uppm = pnd_Input_Part__R_Field_24.newFieldInGroup("pnd_Input_Part_Pnd_Rec_Type_Uppm", "#REC-TYPE-UPPM", FieldType.STRING, 
            1);
        pnd_Input_Part_Pnd_Intrest_Pymt_In_Rate = pnd_Input_Part__R_Field_24.newFieldInGroup("pnd_Input_Part_Pnd_Intrest_Pymt_In_Rate", "#INTREST-PYMT-IN-RATE", 
            FieldType.STRING, 4);

        pnd_Input_Part__R_Field_25 = pnd_Input_Part__R_Field_24.newGroupInGroup("pnd_Input_Part__R_Field_25", "REDEFINE", pnd_Input_Part_Pnd_Intrest_Pymt_In_Rate);
        pnd_Input_Part_Pnd_Intrest_Pymt_In_Rate_N = pnd_Input_Part__R_Field_25.newFieldInGroup("pnd_Input_Part_Pnd_Intrest_Pymt_In_Rate_N", "#INTREST-PYMT-IN-RATE-N", 
            FieldType.NUMERIC, 4, 2);
        pnd_Input_Part_Pnd_Intrest_Pymt_Ia = pnd_Input_Part__R_Field_24.newFieldInGroup("pnd_Input_Part_Pnd_Intrest_Pymt_Ia", "#INTREST-PYMT-IA", FieldType.STRING, 
            4);

        pnd_Input_Part__R_Field_26 = pnd_Input_Part__R_Field_24.newGroupInGroup("pnd_Input_Part__R_Field_26", "REDEFINE", pnd_Input_Part_Pnd_Intrest_Pymt_Ia);
        pnd_Input_Part_Pnd_Intrest_Pymt_Ia_N = pnd_Input_Part__R_Field_26.newFieldInGroup("pnd_Input_Part_Pnd_Intrest_Pymt_Ia_N", "#INTREST-PYMT-IA-N", 
            FieldType.NUMERIC, 4, 2);
        pnd_Input_Part_Pnd_Payment = pnd_Input_Part__R_Field_24.newFieldInGroup("pnd_Input_Part_Pnd_Payment", "#PAYMENT", FieldType.STRING, 11);

        pnd_Input_Part__R_Field_27 = pnd_Input_Part__R_Field_24.newGroupInGroup("pnd_Input_Part__R_Field_27", "REDEFINE", pnd_Input_Part_Pnd_Payment);
        pnd_Input_Part_Pnd_Payment_N = pnd_Input_Part__R_Field_27.newFieldInGroup("pnd_Input_Part_Pnd_Payment_N", "#PAYMENT-N", FieldType.NUMERIC, 11, 
            2);
        pnd_Input_Part_Pnd_Table_Discription = pnd_Input_Part__R_Field_24.newFieldInGroup("pnd_Input_Part_Pnd_Table_Discription", "#TABLE-DISCRIPTION", 
            FieldType.STRING, 80);
        pnd_Alloc = localVariables.newFieldInRecord("pnd_Alloc", "#ALLOC", FieldType.STRING, 5);

        pnd_Alloc__R_Field_28 = localVariables.newGroupInRecord("pnd_Alloc__R_Field_28", "REDEFINE", pnd_Alloc);
        pnd_Alloc_Pnd_Alloc_N = pnd_Alloc__R_Field_28.newFieldInGroup("pnd_Alloc_Pnd_Alloc_N", "#ALLOC-N", FieldType.NUMERIC, 3, 2);
        pnd_Alloc_Pnd_Alloc_N_2 = pnd_Alloc__R_Field_28.newFieldInGroup("pnd_Alloc_Pnd_Alloc_N_2", "#ALLOC-N-2", FieldType.NUMERIC, 2);
        pnd_Bene_Alloc = localVariables.newFieldInRecord("pnd_Bene_Alloc", "#BENE-ALLOC", FieldType.NUMERIC, 5, 2);
        pnd_Bene_Cnt_Prmry = localVariables.newFieldInRecord("pnd_Bene_Cnt_Prmry", "#BENE-CNT-PRMRY", FieldType.NUMERIC, 9);
        pnd_Bene_Cnt_Cntgnt = localVariables.newFieldInRecord("pnd_Bene_Cnt_Cntgnt", "#BENE-CNT-CNTGNT", FieldType.NUMERIC, 9);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 9);
        pnd_Rsdnce_St = localVariables.newFieldInRecord("pnd_Rsdnce_St", "#RSDNCE-ST", FieldType.NUMERIC, 2);

        pnd_Rsdnce_St__R_Field_29 = localVariables.newGroupInRecord("pnd_Rsdnce_St__R_Field_29", "REDEFINE", pnd_Rsdnce_St);
        pnd_Rsdnce_St_Pnd_Rsdnce_St_A = pnd_Rsdnce_St__R_Field_29.newFieldInGroup("pnd_Rsdnce_St_Pnd_Rsdnce_St_A", "#RSDNCE-ST-A", FieldType.STRING, 2);
        pnd_Escape_Top = localVariables.newFieldInRecord("pnd_Escape_Top", "#ESCAPE-TOP", FieldType.BOOLEAN, 1);
        pnd_Fnd_Al_Tic = localVariables.newFieldInRecord("pnd_Fnd_Al_Tic", "#FND-AL-TIC", FieldType.STRING, 14);

        pnd_Fnd_Al_Tic__R_Field_30 = localVariables.newGroupInRecord("pnd_Fnd_Al_Tic__R_Field_30", "REDEFINE", pnd_Fnd_Al_Tic);
        pnd_Fnd_Al_Tic_Pnd_Fnd_Tic = pnd_Fnd_Al_Tic__R_Field_30.newFieldInGroup("pnd_Fnd_Al_Tic_Pnd_Fnd_Tic", "#FND-TIC", FieldType.STRING, 10);
        pnd_Fnd_Al_Tic_Pnd_Fnd_Al = pnd_Fnd_Al_Tic__R_Field_30.newFieldInGroup("pnd_Fnd_Al_Tic_Pnd_Fnd_Al", "#FND-AL", FieldType.NUMERIC, 4, 1);
        pnd_City_Done = localVariables.newFieldInRecord("pnd_City_Done", "#CITY-DONE", FieldType.BOOLEAN, 1);
        pnd_Uppm = localVariables.newFieldInRecord("pnd_Uppm", "#UPPM", FieldType.NUMERIC, 2);
        pnd_Int_Rate = localVariables.newFieldInRecord("pnd_Int_Rate", "#INT-RATE", FieldType.STRING, 4);

        pnd_Int_Rate__R_Field_31 = localVariables.newGroupInRecord("pnd_Int_Rate__R_Field_31", "REDEFINE", pnd_Int_Rate);
        pnd_Int_Rate_Pnd_Int_Rate_A2 = pnd_Int_Rate__R_Field_31.newFieldInGroup("pnd_Int_Rate_Pnd_Int_Rate_A2", "#INT-RATE-A2", FieldType.STRING, 2);
        pnd_Int_Rate_Pnd_Int_Rate_B2 = pnd_Int_Rate__R_Field_31.newFieldInGroup("pnd_Int_Rate_Pnd_Int_Rate_B2", "#INT-RATE-B2", FieldType.STRING, 2);
        pnd_Uppm_Collected = localVariables.newFieldInRecord("pnd_Uppm_Collected", "#UPPM-COLLECTED", FieldType.BOOLEAN, 1);
        pnd_Mit_Effective_Dt_N = localVariables.newFieldInRecord("pnd_Mit_Effective_Dt_N", "#MIT-EFFECTIVE-DT-N", FieldType.NUMERIC, 8);

        pnd_Mit_Effective_Dt_N__R_Field_32 = localVariables.newGroupInRecord("pnd_Mit_Effective_Dt_N__R_Field_32", "REDEFINE", pnd_Mit_Effective_Dt_N);
        pnd_Mit_Effective_Dt_N_Pnd_Mit_Effective_Dt = pnd_Mit_Effective_Dt_N__R_Field_32.newFieldInGroup("pnd_Mit_Effective_Dt_N_Pnd_Mit_Effective_Dt", 
            "#MIT-EFFECTIVE-DT", FieldType.STRING, 8);
        pnd_Total_Add = localVariables.newFieldInRecord("pnd_Total_Add", "#TOTAL-ADD", FieldType.NUMERIC, 9);
        pnd_Total_Dup = localVariables.newFieldInRecord("pnd_Total_Dup", "#TOTAL-DUP", FieldType.NUMERIC, 9);
        pnd_Total_Err = localVariables.newFieldInRecord("pnd_Total_Err", "#TOTAL-ERR", FieldType.NUMERIC, 9);
        pnd_Err_Message = localVariables.newFieldInRecord("pnd_Err_Message", "#ERR-MESSAGE", FieldType.STRING, 72);
        pnd_Tiaa_Cntrct_Found = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct_Found", "#TIAA-CNTRCT-FOUND", FieldType.BOOLEAN, 1);

        pnd_Hold_Address = localVariables.newGroupInRecord("pnd_Hold_Address", "#HOLD-ADDRESS");
        pnd_Hold_Address_Pnd_Hdest_Addr1 = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hdest_Addr1", "#HDEST-ADDR1", FieldType.STRING, 40);
        pnd_Hold_Address_Pnd_Hdest_Addr2 = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hdest_Addr2", "#HDEST-ADDR2", FieldType.STRING, 40);
        pnd_Hold_Address_Pnd_Hdest_Addr3 = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hdest_Addr3", "#HDEST-ADDR3", FieldType.STRING, 40);
        pnd_Hold_Address_Pnd_Hdest_City = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hdest_City", "#HDEST-CITY", FieldType.STRING, 28);
        pnd_Hold_Address_Pnd_Hdest_State = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hdest_State", "#HDEST-STATE", FieldType.STRING, 3);
        pnd_Hold_Address_Pnd_Hzip = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hzip", "#HZIP", FieldType.STRING, 9);
        pnd_Foreign_Or_Canada = localVariables.newFieldArrayInRecord("pnd_Foreign_Or_Canada", "#FOREIGN-OR-CANADA", FieldType.STRING, 5, new DbsArrayController(1, 
            2));
        pnd_Addr_Indx = localVariables.newFieldInRecord("pnd_Addr_Indx", "#ADDR-INDX", FieldType.NUMERIC, 1);
        pnd_Fund_Idx = localVariables.newFieldInRecord("pnd_Fund_Idx", "#FUND-IDX", FieldType.NUMERIC, 3);
        pnd_Fund_Count = localVariables.newFieldInRecord("pnd_Fund_Count", "#FUND-COUNT", FieldType.NUMERIC, 3);
        pnd_Max_Count = localVariables.newFieldInRecord("pnd_Max_Count", "#MAX-COUNT", FieldType.NUMERIC, 2);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_acis_Reprint_Fl.reset();

        ldaCisl200.initializeValues();
        ldaCisl2020.initializeValues();
        ldaAppl170.initializeValues();

        localVariables.reset();
        pnd_Foreign_Or_Canada.getValue(1).setInitialValue("FORGN");
        pnd_Foreign_Or_Canada.getValue(2).setInitialValue("CANAD");
        pnd_Debug.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Sgdb6000() throws Exception
    {
        super("Sgdb6000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SGDB6000", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        setupReports();
        pnd_Total_Add.reset();                                                                                                                                            //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132;//Natural: FORMAT ( 3 ) PS = 60 LS = 132;//Natural: FORMAT ( 4 ) PS = 60 LS = 132;//Natural: RESET #TOTAL-ADD #TOTAL-ERR #TOTAL-DUP
        pnd_Total_Err.reset();
        pnd_Total_Dup.reset();
        //* CREA
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 #IN-TRANSLTE-FILE
        while (condition(getWorkFiles().read(2, pnd_In_Translte_File)))
        {
            //* CREA
            //* CREA
            //* CREA
            //* CREA
            //* CREA
            //* CREA
            //* CREA
            if (condition(!(pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq.greater(0))))                                                                                   //Natural: ACCEPT IF #IN-TRANSLTE-FUND-SEQ > 00
            {
                continue;
            }
            pnd_Fund_Count.nadd(1);                                                                                                                                       //Natural: ASSIGN #FUND-COUNT := #FUND-COUNT + 1
            pnd_Translte_File_Pnd_Translte_Ticker.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Ticker);                                         //Natural: ASSIGN #TRANSLTE-TICKER ( #FUND-COUNT ) := #IN-TRANSLTE-TICKER
            pnd_Translte_File_Pnd_Translte_Ticker_Name.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name);                               //Natural: ASSIGN #TRANSLTE-TICKER-NAME ( #FUND-COUNT ) := #IN-TRANSLTE-TICKER-NAME
            pnd_Translte_File_Pnd_Translte_Fund_Seq.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq);                                     //Natural: ASSIGN #TRANSLTE-FUND-SEQ ( #FUND-COUNT ) := #IN-TRANSLTE-FUND-SEQ
            pnd_Translte_File_Pnd_Translte_Pull_Code.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Pull_Code);                                   //Natural: ASSIGN #TRANSLTE-PULL-CODE ( #FUND-COUNT ) := #IN-TRANSLTE-PULL-CODE
            pnd_Translte_File_Pnd_Translte_Fund_Type.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Fund_Type);                                   //Natural: ASSIGN #TRANSLTE-FUND-TYPE ( #FUND-COUNT ) := #IN-TRANSLTE-FUND-TYPE
            //* CREA
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* CREA
        getReports().write(0, "=",pnd_Fund_Count);                                                                                                                        //Natural: WRITE '=' #FUND-COUNT
        if (Global.isEscape()) return;
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-PART-AA ( * )
        while (condition(getWorkFiles().read(1, pnd_Input_Part_Aa.getValue("*"))))
        {
            pnd_Input_Part_Pnd_Input_Part_A.getValue("*").reset();                                                                                                        //Natural: RESET #INPUT-PART-A ( * )
            pnd_Input_Part_Pnd_Input_Part_A.getValue("*").setValue(pnd_Input_Part_Aa.getValue("*"));                                                                      //Natural: ASSIGN #INPUT-PART-A ( * ) := #INPUT-PART-AA ( * )
            if (condition(pnd_Input_Part_Pnd_Rec_Iden.equals("IPRP") || pnd_Input_Part_Pnd_Rec_Iden.equals("IPIP")))                                                      //Natural: IF #INPUT-PART.#REC-IDEN EQ 'IPRP' OR #INPUT-PART.#REC-IDEN EQ 'IPIP'
            {
                pnd_City_Done.reset();                                                                                                                                    //Natural: RESET #CITY-DONE #UPPM
                pnd_Uppm.reset();
                //*  A=ADD OR C=CHANGE
                if (condition(pnd_Input_Part_Pnd_Rec_Type.equals("C")))                                                                                                   //Natural: IF #REC-TYPE EQ 'C'
                {
                                                                                                                                                                          //Natural: PERFORM STORE-BENE
                    sub_Store_Bene();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Escape_Top.setValue(true);                                                                                                                        //Natural: ASSIGN #ESCAPE-TOP := TRUE
                    //*  CHG365909
                    pnd_Input_Part_Aa.getValue("*").reset();                                                                                                              //Natural: RESET #INPUT-PART-AA ( * )
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Err_Message.reset();                                                                                                                                  //Natural: RESET #ERR-MESSAGE
                getReports().write(0, "*****************************");                                                                                                   //Natural: WRITE '*****************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  PINEXP
                getReports().write(0, "=",pnd_Input_Part_Pnd_Cis_Pin_Nbr,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Tiaa_Doi,  //Natural: WRITE '=' #CIS-PIN-NBR / '=' #CIS-TPA-TIAA-CNTRCT / '=' #CIS-TIAA-DOI / '=' #CIS-TIAA-DOI / '=' #CIS-FRST-ANNT-SSN-N / '=' #CIS-FRST-ANNT-FRST-NME / '=' #CIS-FRST-ANNT-LST-NME
                    NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Tiaa_Doi,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn_N,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Frst_Nme,
                    NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*****************************");                                                                                                   //Natural: WRITE '*****************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  PINEXP
                if (condition(pnd_Input_Part_Pnd_Cis_Pin_Nbr_N7.equals(getZero()) || pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct.equals(" ") || pnd_Input_Part_Pnd_Cis_Tiaa_Doi.equals(" ")  //Natural: IF #CIS-PIN-NBR-N7 EQ 0 OR #CIS-TPA-TIAA-CNTRCT EQ ' ' OR #CIS-TIAA-DOI EQ ' ' OR #CIS-TIAA-DOI EQ ' ' OR #CIS-FRST-ANNT-SSN-N EQ 0 OR #CIS-FRST-ANNT-FRST-NME EQ ' ' OR #CIS-FRST-ANNT-LST-NME EQ ' '
                    || pnd_Input_Part_Pnd_Cis_Tiaa_Doi.equals(" ") || pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn_N.equals(getZero()) || pnd_Input_Part_Pnd_Cis_Frst_Annt_Frst_Nme.equals(" ") 
                    || pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme.equals(" ")))
                {
                    pnd_Err_Message.setValue("OMNI KEY INFO MISSING");                                                                                                    //Natural: ASSIGN #ERR-MESSAGE := "OMNI KEY INFO MISSING"
                                                                                                                                                                          //Natural: PERFORM GENERATE-ERROR-REPORT
                    sub_Generate_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Escape_Top.setValue(true);                                                                                                                        //Natural: ASSIGN #ESCAPE-TOP := TRUE
                                                                                                                                                                          //Natural: PERFORM STORE-BENE
                    sub_Store_Bene();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Total_Err.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-ERR
                    //*  CHG365909
                    pnd_Input_Part_Aa.getValue("*").reset();                                                                                                              //Natural: RESET #INPUT-PART-AA ( * )
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-CIS-PRTCPNT
                sub_Get_Cis_Prtcpnt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  09/2011 B.HOLLOWAY START - RESET TO ALLOW DUPS DURING TESTING
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    pnd_Tiaa_Cntrct_Found.reset();                                                                                                                        //Natural: RESET #TIAA-CNTRCT-FOUND
                }                                                                                                                                                         //Natural: END-IF
                //*  09/2011 B.HOLLOWAY END
                if (condition(pnd_Tiaa_Cntrct_Found.getBoolean()))                                                                                                        //Natural: IF #TIAA-CNTRCT-FOUND
                {
                    pnd_Escape_Top.setValue(true);                                                                                                                        //Natural: ASSIGN #ESCAPE-TOP := TRUE
                                                                                                                                                                          //Natural: PERFORM STORE-BENE
                    sub_Store_Bene();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM GENERATE-CIS-REPORT-DUPLICATE
                    sub_Generate_Cis_Report_Duplicate();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Total_Dup.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-DUP
                    //*  CHG365909
                    pnd_Input_Part_Aa.getValue("*").reset();                                                                                                              //Natural: RESET #INPUT-PART-AA ( * )
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Part_Pnd_Rec_Iden.equals("IPRP") || pnd_Input_Part_Pnd_Rec_Iden.equals("IPIP")))                                                      //Natural: IF #INPUT-PART.#REC-IDEN EQ 'IPRP' OR #INPUT-PART.#REC-IDEN EQ 'IPIP'
            {
                pnd_Escape_Top.setValue(false);                                                                                                                           //Natural: ASSIGN #ESCAPE-TOP := FALSE
                                                                                                                                                                          //Natural: PERFORM STORE-BENE
                sub_Store_Bene();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Bene_Cnt_Prmry.reset();                                                                                                                               //Natural: RESET #BENE-CNT-PRMRY #BENE-CNT-CNTGNT
                pnd_Bene_Cnt_Cntgnt.reset();
                                                                                                                                                                          //Natural: PERFORM LOAD-PARTICIPANT-DATA
                sub_Load_Participant_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CNTRSTRTGY
                                                                                                                                                                          //Natural: PERFORM UPDATE-ACIS-REPRINT-CONTRACT
                sub_Update_Acis_Reprint_Contract();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Escape_Top.getBoolean()))                                                                                                                   //Natural: IF #ESCAPE-TOP
            {
                                                                                                                                                                          //Natural: PERFORM STORE-BENE
                sub_Store_Bene();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Escape_Top.setValue(true);                                                                                                                            //Natural: ASSIGN #ESCAPE-TOP := TRUE
                //*  CHG365909
                pnd_Input_Part_Aa.getValue("*").reset();                                                                                                                  //Natural: RESET #INPUT-PART-AA ( * )
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Part_Pnd_Rec_Iden.equals("IPRB")))                                                                                                    //Natural: IF #INPUT-PART.#REC-IDEN EQ 'IPRB'
            {
                //*  A=ADD OR C=CHANGE
                if (condition(pnd_Input_Part_Pnd_Rec_Type_B.equals("C")))                                                                                                 //Natural: IF #REC-TYPE-B EQ 'C'
                {
                    pnd_Escape_Top.setValue(true);                                                                                                                        //Natural: ASSIGN #ESCAPE-TOP := TRUE
                    //*  CHG365909
                    pnd_Input_Part_Aa.getValue("*").reset();                                                                                                              //Natural: RESET #INPUT-PART-AA ( * )
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOAD-BENE-DATA
                sub_Load_Bene_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Part_Pnd_Rec_Iden.equals("IPRM")))                                                                                                    //Natural: IF #INPUT-PART.#REC-IDEN EQ 'IPRM'
            {
                //*  A=ADD OR C=CHANGE
                if (condition(pnd_Input_Part_Pnd_Rec_Type_B.equals("C")))                                                                                                 //Natural: IF #REC-TYPE-B EQ 'C'
                {
                    pnd_Escape_Top.setValue(true);                                                                                                                        //Natural: ASSIGN #ESCAPE-TOP := TRUE
                    //*  CHG365909
                    pnd_Input_Part_Aa.getValue("*").reset();                                                                                                              //Natural: RESET #INPUT-PART-AA ( * )
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM LOAD-UPPM-TABLE
                sub_Load_Uppm_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Input_Part_Aa.getValue("*").reset();                                                                                                                      //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 );//Natural: AT TOP OF PAGE ( 3 );//Natural: AT TOP OF PAGE ( 4 );//Natural: AT END OF DATA;//Natural: RESET #INPUT-PART-AA ( * )
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            if (condition(! (pnd_Escape_Top.getBoolean())))                                                                                                               //Natural: IF NOT #ESCAPE-TOP
            {
                ldaCisl2020.getVw_cis_Bene_File_01().insertDBRow();                                                                                                       //Natural: STORE CIS-BENE-FILE-01
                //*  09/2011 B.HOLLOWAY START - BACKOUT IF TESTING
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
                //*  09/2011 B.HOLLOWAY END
                ldaCisl2020.getVw_cis_Bene_File_01().reset();                                                                                                             //Natural: RESET CIS-BENE-FILE-01
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOTITLE,"TOTAL IPRO PARTICIPANT DUPLICATE ON CIS ==>",pnd_Total_Dup);                                                      //Natural: WRITE ( 2 ) 'TOTAL IPRO PARTICIPANT DUPLICATE ON CIS ==>' #TOTAL-DUP
            if (condition(Global.isEscape())) return;
            getReports().write(3, ReportOption.NOTITLE,"TOTAL IPRO PARTICIPANT ADDED TO CIS ==>",pnd_Total_Add);                                                          //Natural: WRITE ( 3 ) 'TOTAL IPRO PARTICIPANT ADDED TO CIS ==>' #TOTAL-ADD
            if (condition(Global.isEscape())) return;
            getReports().write(4, ReportOption.NOTITLE,"TOTAL IPRO PARTICIPANT ERRORED  OUT ==>",pnd_Total_Err);                                                          //Natural: WRITE ( 4 ) 'TOTAL IPRO PARTICIPANT ERRORED  OUT ==>' #TOTAL-ERR
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* *-------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-PARTICIPANT-DATA
        //* *
        //* *-------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-BENE-DATA
        //*  CIS-BENE-ANNT-PRFX
        //*  CIS-BENE-ANNT-MID-NME
        //*  CIS-BENE-ANNT-SFFX
        //* *-------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CIS-PRTCPNT
        //* *-------------------------------
        //* *---------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-CIS-REPORT-DUPLICATE
        //* *---------------------------------------------
        //* *---------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-CIS-UPDATE-REPORT
        //* *---------------------------------------------
        //* *---------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-ERROR-REPORT
        //* *---------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-BENE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-UPPM-TABLE
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-MIT
        //* *CISA4000.MIT-LOG-DTE-TME(1)   := CIS-RQST-LOG-DTE-TME
        //*  09/2011 B.HOLLOWAY START NEW ADDRESS REFORMAT ROUTINE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-ADDRESS
        //* **********************************************************************
        //*  09/2011 B.HOLLOWAY END
        //* ************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-ACIS-REPRINT-CONTRACT
        //* ***********************************************************************
        //*  CREA
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Load_Participant_Data() throws Exception                                                                                                             //Natural: LOAD-PARTICIPANT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        //*  #CIS-PIN-NBR-N := 2664185
        getReports().write(0, "PARTICIPANT INFORMATION");                                                                                                                 //Natural: WRITE 'PARTICIPANT INFORMATION'
        if (Global.isEscape()) return;
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        //*  PINEXP
        getReports().write(0, NEWLINE,"=",pnd_Input_Part_Pnd_Rec_Iden,NEWLINE,"=",pnd_Input_Part_Pnd_Rec_Type,NEWLINE,"=",pnd_Input_Part_Pnd_Plan_Name,                   //Natural: WRITE / '=' #REC-IDEN / '=' #REC-TYPE / '=' #PLAN-NAME / '=' #PLAN-ID / '=' #ERISA-IND / '=' #CIS-TPA-TIAA-CNTRCT / '=' #CIS-TPA-CREF-CNTRCT / '=' #CIS-PIN-NBR / '=' #CIS-RQST-ID-KEY / '=' #CIS-TPA-CNTRCT / '=' #CIS-REQUESTOR / '=' #CIS-CNTRCT-TYPE / '=' #CIS-OPN-CLSD-IND / '=' #CIS-STATUS-CD / '=' #CIS-ANNTY-OPTION / '=' #CIS-CNTRCT-PRINT-DTE / '=' #CIS-FRST-ANNT-FRST-NME / '=' #CIS-FRST-ANNT-MID-NME / '=' #CIS-FRST-ANNT-LST-NME / '=' #CIS-FRST-ANNT-PRF-NME / '=' #CIS-FRST-ANNT-SEX-CDE / '=' #CIS-FRST-ANNT-SSN
            NEWLINE,"=",pnd_Input_Part_Pnd_Plan_Id,NEWLINE,"=",pnd_Input_Part_Pnd_Erisa_Ind,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct,NEWLINE,"=",
            pnd_Input_Part_Pnd_Cis_Tpa_Cref_Cntrct,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Pin_Nbr,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Rqst_Id_Key,NEWLINE,"=",
            pnd_Input_Part_Pnd_Cis_Tpa_Cntrct,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Requestor,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Cntrct_Type,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Opn_Clsd_Ind,
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Status_Cd,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Annty_Option,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Cntrct_Print_Dte,
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Frst_Nme,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Mid_Nme,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme,
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Prf_Nme,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Sex_Cde,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn);
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn_N,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Dob,NEWLINE,"=",pnd_Input_Part_Pnd_Addrss_Line_1, //Natural: WRITE / '=' #CIS-FRST-ANNT-SSN-N / '=' #CIS-FRST-ANNT-DOB / '=' #ADDRSS-LINE-1 / '=' #ADDRSS-LINE-2 / '=' #ADDRSS-LINE-2-3 / '=' #ADDRSS-LINE-3 / '=' #CITY / '=' #RESID-STAE / '=' #ZIP / '=' #TERM-DATE / '=' #CIS-FRST-ANNT-CTZNSHP-CD / '=' #CIS-TIAA-DOI / '=' #CIS-ANNTY-START-DTE / '=' #CIS-ANNTY-END-DTE / '=' #CIS-PYMNT-MODE / '=' #CIS-GRNTED-PERIODS-YRS / '=' #CIS-GRNTED-PERIODS-YRS-N / '=' #CIS-GRNTED-GRD-AMT / '=' #CIS-GRNTED-GRD-AMT-N / '=' #CIS-LOB / '=' #CIS-LOB-TYPE
            NEWLINE,"=",pnd_Input_Part_Pnd_Addrss_Line_2,NEWLINE,"=",pnd_Input_Part_Pnd_Addrss_Line_2_3,NEWLINE,"=",pnd_Input_Part_Pnd_Addrss_Line_3,NEWLINE,
            "=",pnd_Input_Part_Pnd_City,NEWLINE,"=",pnd_Input_Part_Pnd_Resid_Stae,NEWLINE,"=",pnd_Input_Part_Pnd_Zip,NEWLINE,"=",pnd_Input_Part_Pnd_Term_Date,
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Frst_Annt_Ctznshp_Cd,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Tiaa_Doi,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Annty_Start_Dte,
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Annty_End_Dte,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Pymnt_Mode,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Grnted_Periods_Yrs,
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Grnted_Periods_Yrs_N,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Grnted_Grd_Amt,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Grnted_Grd_Amt_N,
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Lob,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Lob_Type);
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Da_Tiaa_Nbr_Sttl,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Da_Proceeds_Amt,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Da_Proceeds_Amt_N, //Natural: WRITE / '=' #CIS-DA-TIAA-NBR-STTL / '=' #CIS-DA-PROCEEDS-AMT / '=' #CIS-DA-PROCEEDS-AMT-N / '=' #CIS-RTB-AMT / '=' #CIS-RTB-AMT-N / '=' #CIS-RTB-PCT / '=' #CIS-RTB-PCT-N / '=' #CIS-TRNSF-TIAA-REA-NBR / '=' #CIS-TRNSF-TIAA-CER-NBR / '=' #CIS-HANDLE-CODE / '=' #CIS-ORG-ISSUE-ST / '=' #FILL / '=' #CIS-COMUT-GRNTED-AMT / '='#CIS-COMUT-GRNTED-PCT-N / '=' #DEST-NAME / '=' #DEST-ADDR-LINE / '=' #DEST-ADDR-LINE-2 / '=' #DEST-ADDR-LINE-3 / '=' #DEST-ADDR-CITY / '=' #DEST-ADDR-ST
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Rtb_Amt,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Rtb_Amt_N,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Rtb_Pct,NEWLINE,"=",
            pnd_Input_Part_Pnd_Cis_Rtb_Pct_N,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Trnsf_Tiaa_Rea_Nbr,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Trnsf_Tiaa_Cer_Nbr,
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Handle_Code,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Org_Issue_St,NEWLINE,"=",pnd_Input_Part_Pnd_Fill,NEWLINE,"=",
            pnd_Input_Part_Pnd_Cis_Comut_Grnted_Amt,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Comut_Grnted_Pct_N,NEWLINE,"=",pnd_Input_Part_Pnd_Dest_Name,NEWLINE,
            "=",pnd_Input_Part_Pnd_Dest_Addr_Line,NEWLINE,"=",pnd_Input_Part_Pnd_Dest_Addr_Line_2,NEWLINE,"=",pnd_Input_Part_Pnd_Dest_Addr_Line_3,NEWLINE,
            "=",pnd_Input_Part_Pnd_Dest_Addr_City,NEWLINE,"=",pnd_Input_Part_Pnd_Dest_Addr_St);
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",pnd_Input_Part_Pnd_Dest_Addr_Zip,NEWLINE,"=",pnd_Input_Part_Pnd_Dest_Bank_Type,NEWLINE,"=",pnd_Input_Part_Pnd_Dest_Bank_Acct_Nbr, //Natural: WRITE / '=' #DEST-ADDR-ZIP / '=' #DEST-BANK-TYPE / '=' #DEST-BANK-ACCT-NBR / '=' #DEST-BANK-TRST-NBR /'=' #ROLLOVER-DEST-NAME /'=' #ROLLOVER-DEST-ADDR-LINE /'=' #ROLLOVER-DEST-ADDR-LINE-2 /'=' #ROLLOVER-DEST-ADDR-LINE-3 /'=' #ROLLOVER-DEST-ADDR-CITY /'=' #ROLLOVER-DEST-ADDR-ST /'=' #ROLLOVER-DEST-ADDR-ZIP /'=' #ROLLOVER-DEST-BANK-ACCT-NBR
            NEWLINE,"=",pnd_Input_Part_Pnd_Dest_Bank_Trst_Nbr,NEWLINE,"=",pnd_Input_Part_Pnd_Rollover_Dest_Name,NEWLINE,"=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line,
            NEWLINE,"=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_2,NEWLINE,"=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_3,NEWLINE,"=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_City,
            NEWLINE,"=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_St,NEWLINE,"=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_Zip,NEWLINE,"=",pnd_Input_Part_Pnd_Rollover_Dest_Bank_Acct_Nbr);
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",pnd_Input_Part_Pnd_Fund_Allocation.getValue(1,":",20),NEWLINE,"=",ldaCisl200.getCis_Prtcpnt_File_Cis_Cref_Acct_Cde().getValue(1, //Natural: WRITE /'=' #FUND-ALLOCATION ( 1:20 ) / '=' CIS-CREF-ACCT-CDE ( 1:20 ) / '=' #POWER-IMAGE-ID /'=' CIS-CREF-ANNUAL-NBR-UNITS ( 1:20 ) / '**********************************************************'
            ":",20),NEWLINE,"=",pnd_Input_Part_Pnd_Power_Image_Id,NEWLINE,"=",ldaCisl200.getCis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units().getValue(1,":",
            20),NEWLINE,"**********************************************************");
        if (Global.isEscape()) return;
        ldaCisl200.getVw_cis_Prtcpnt_File().reset();                                                                                                                      //Natural: RESET CIS-PRTCPNT-FILE
        //*  PINEXP
        //*  PINEXP
        if (condition(pnd_Input_Part_Pnd_Cis_Pin_Filler.equals(" ")))                                                                                                     //Natural: IF #CIS-PIN-FILLER = ' '
        {
            ldaCisl200.getCis_Prtcpnt_File_Cis_Pin_Nbr().setValue(pnd_Input_Part_Pnd_Cis_Pin_Nbr_N7);                                                                     //Natural: ASSIGN CIS-PIN-NBR := #CIS-PIN-NBR-N7
            //*  PINEXP
            //*  PINEXP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCisl200.getCis_Prtcpnt_File_Cis_Pin_Nbr().setValue(pnd_Input_Part_Pnd_Cis_Pin_Nbr_N);                                                                      //Natural: ASSIGN CIS-PIN-NBR := #CIS-PIN-NBR-N
            //*  PINEXP
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id_Key().setValue(pnd_Input_Part_Pnd_Cis_Rqst_Id_Key);                                                                    //Natural: ASSIGN CIS-RQST-ID-KEY := #CIS-RQST-ID-KEY
        ldaCisl200.getCis_Prtcpnt_File_Cis_Tiaa_Nbr().setValue(pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct);                                                                   //Natural: ASSIGN CIS-TIAA-NBR := #CIS-TPA-TIAA-CNTRCT
        ldaCisl200.getCis_Prtcpnt_File_Cis_Cert_Nbr().setValue(pnd_Input_Part_Pnd_Cis_Tpa_Cref_Cntrct);                                                                   //Natural: ASSIGN CIS-CERT-NBR := #CIS-TPA-CREF-CNTRCT
        ldaCisl200.getCis_Prtcpnt_File_Cis_Tiaa_Doi().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Part_Pnd_Cis_Tiaa_Doi);                                     //Natural: MOVE EDITED #CIS-TIAA-DOI TO CIS-TIAA-DOI ( EM = YYYYMMDD )
        //*  CONVERT STATE-CODE TO ALPHA
        ldaCisl200.getCis_Prtcpnt_File_Cis_Cref_Doi().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Part_Pnd_Cis_Tiaa_Doi);                                     //Natural: MOVE EDITED #CIS-TIAA-DOI TO CIS-CREF-DOI ( EM = YYYYMMDD )
        ldaCisl200.getCis_Prtcpnt_File_Cis_Frst_Annt_Ssn().setValue(pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn_N);                                                              //Natural: ASSIGN CIS-FRST-ANNT-SSN := #CIS-FRST-ANNT-SSN-N
        ldaCisl200.getCis_Prtcpnt_File_Cis_Frst_Annt_Frst_Nme().setValue(pnd_Input_Part_Pnd_Cis_Frst_Annt_Frst_Nme);                                                      //Natural: ASSIGN CIS-FRST-ANNT-FRST-NME := #CIS-FRST-ANNT-FRST-NME
        ldaCisl200.getCis_Prtcpnt_File_Cis_Frst_Annt_Lst_Nme().setValue(pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme);                                                        //Natural: ASSIGN CIS-FRST-ANNT-LST-NME := #CIS-FRST-ANNT-LST-NME
        ldaCisl200.getCis_Prtcpnt_File_Cis_Frst_Annt_Ctznshp_Cd().setValue(pnd_Input_Part_Pnd_Cis_Frst_Annt_Ctznshp_Cd);                                                  //Natural: ASSIGN CIS-FRST-ANNT-CTZNSHP-CD := #CIS-FRST-ANNT-CTZNSHP-CD
        ldaCisl200.getCis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde().setValue(pnd_Input_Part_Pnd_Resid_Stae);                                                                 //Natural: ASSIGN CIS-FRST-ANNT-RSDNC-CDE := #RESID-STAE
        ldaCisl200.getCis_Prtcpnt_File_Cis_Pymnt_Mode().setValue(pnd_Input_Part_Pnd_Cis_Pymnt_Mode);                                                                      //Natural: ASSIGN CIS-PYMNT-MODE := #CIS-PYMNT-MODE
        FOR01:                                                                                                                                                            //Natural: FOR #X 1 62
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(62)); pnd_X.nadd(1))
        {
            if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_X).equals(pnd_Input_Part_Pnd_Resid_Stae)))                                   //Natural: IF #STATE-CODE-A ( #X ) = #RESID-STAE
            {
                pnd_Rsdnce_St_Pnd_Rsdnce_St_A.setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(pnd_X));                                            //Natural: MOVE #STATE-CODE-N ( #X ) TO #RSDNCE-ST-A
                pnd_Rsdnce_St_Pnd_Rsdnce_St_A.setValue(pnd_Rsdnce_St_Pnd_Rsdnce_St_A, MoveOption.RightJustified);                                                         //Natural: MOVE RIGHT JUSTIFIED #RSDNCE-ST-A TO #RSDNCE-ST-A
                DbsUtil.examine(new ExamineSource(pnd_Rsdnce_St_Pnd_Rsdnce_St_A), new ExamineSearch(" "), new ExamineReplace("0"));                                       //Natural: EXAMINE #RSDNCE-ST-A FOR ' ' REPLACE WITH '0'
                ldaCisl200.getCis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde().setValue(pnd_Rsdnce_St);                                                                         //Natural: MOVE #RSDNCE-ST TO CIS-FRST-ANNT-RSDNC-CDE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Input_Part_Pnd_Resid_Stae.equals("NY")))                                                                                                        //Natural: IF #RESID-STAE EQ 'NY'
        {
            pnd_Rsdnce_St.setValue(35);                                                                                                                                   //Natural: ASSIGN #RSDNCE-ST := 35
            ldaCisl200.getCis_Prtcpnt_File_Cis_Frst_Annt_Rsdnc_Cde().setValue(pnd_Rsdnce_St);                                                                             //Natural: MOVE #RSDNCE-ST TO CIS-FRST-ANNT-RSDNC-CDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl200.getCis_Prtcpnt_File_Cis_Frst_Annt_Dob().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Part_Pnd_Cis_Frst_Annt_Dob);                           //Natural: MOVE EDITED #CIS-FRST-ANNT-DOB TO CIS-FRST-ANNT-DOB ( EM = YYYYMMDD )
        ldaCisl200.getCis_Prtcpnt_File_Cis_Opn_Clsd_Ind().setValue(pnd_Input_Part_Pnd_Cis_Opn_Clsd_Ind);                                                                  //Natural: ASSIGN CIS-OPN-CLSD-IND := #CIS-OPN-CLSD-IND
        ldaCisl200.getCis_Prtcpnt_File_Cis_Status_Cd().setValue(pnd_Input_Part_Pnd_Cis_Status_Cd);                                                                        //Natural: ASSIGN CIS-STATUS-CD := #CIS-STATUS-CD
        ldaCisl200.getCis_Prtcpnt_File_Cis_Cntrct_Type().setValue(pnd_Input_Part_Pnd_Cis_Cntrct_Type);                                                                    //Natural: ASSIGN CIS-CNTRCT-TYPE := #CIS-CNTRCT-TYPE
        ldaCisl200.getCis_Prtcpnt_File_Cis_Cntrct_Apprvl_Ind().setValue("Y");                                                                                             //Natural: ASSIGN CIS-CNTRCT-APPRVL-IND := 'Y'
        if (condition(pnd_Input_Part_Pnd_Cis_Requestor.equals("IPR")))                                                                                                    //Natural: IF #CIS-REQUESTOR EQ 'IPR'
        {
            ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id().setValue("IPRO");                                                                                                //Natural: ASSIGN CIS-RQST-ID := 'IPRO'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id().setValue(pnd_Input_Part_Pnd_Cis_Requestor);                                                                      //Natural: ASSIGN CIS-RQST-ID := #CIS-REQUESTOR
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl200.getCis_Prtcpnt_File_Cis_Cntrct_Print_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Part_Pnd_Cis_Tiaa_Doi);                             //Natural: MOVE EDITED #CIS-TIAA-DOI TO CIS-CNTRCT-PRINT-DTE ( EM = YYYYMMDD )
        ldaCisl200.getCis_Prtcpnt_File_Cis_Extract_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Part_Pnd_Cis_Tiaa_Doi);                                 //Natural: MOVE EDITED #CIS-TIAA-DOI TO CIS-EXTRACT-DATE ( EM = YYYYMMDD )
        ldaCisl200.getCis_Prtcpnt_File_Cis_Appl_Rcvd_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Part_Pnd_Cis_Tiaa_Doi);                                //Natural: MOVE EDITED #CIS-TIAA-DOI TO CIS-APPL-RCVD-DTE ( EM = YYYYMMDD )
        ldaCisl200.getCis_Prtcpnt_File_Cis_Annty_Option().setValue(pnd_Input_Part_Pnd_Cis_Annty_Option);                                                                  //Natural: ASSIGN CIS-ANNTY-OPTION := #CIS-ANNTY-OPTION
        //*  ACTION11604
        ldaCisl200.getCis_Prtcpnt_File_Cis_Annty_Start_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Part_Pnd_Cis_Annty_Start_Dte);                       //Natural: MOVE EDITED #CIS-ANNTY-START-DTE TO CIS-ANNTY-START-DTE ( EM = YYYYMMDD )
        ldaCisl200.getCis_Prtcpnt_File_Cis_Orig_Issue_State().setValue(pnd_Input_Part_Pnd_Cis_Org_Issue_St);                                                              //Natural: ASSIGN CIS-ORIG-ISSUE-STATE := #CIS-ORG-ISSUE-ST
        ldaCisl200.getCis_Prtcpnt_File_Cis_Issue_State_Cd().setValue(pnd_Input_Part_Pnd_Resid_Stae);                                                                      //Natural: ASSIGN CIS-ISSUE-STATE-CD := #RESID-STAE
        ldaCisl200.getCis_Prtcpnt_File_Cis_Lob().setValue("D");                                                                                                           //Natural: ASSIGN CIS-LOB := 'D'
        ldaCisl200.getCis_Prtcpnt_File_Cis_Lob_Type().setValue(pnd_Input_Part_Pnd_Cis_Lob_Type);                                                                          //Natural: ASSIGN CIS-LOB-TYPE := #CIS-LOB-TYPE
        ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().setValue("S");                                                                                                  //Natural: ASSIGN CIS-ADDR-SYN-IND := 'S'
        ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Process_Env().setValue("YB");                                                                                             //Natural: ASSIGN CIS-ADDR-PROCESS-ENV := 'YB'
        ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Chg_Ind().getValue(1).setValue("Y");                                                                                   //Natural: ASSIGN CIS-ADDRESS-CHG-IND ( 1 ) := 'Y'
        pnd_Hold_Address_Pnd_Hdest_Addr1.setValue(pnd_Input_Part_Pnd_Addrss_Line_1);                                                                                      //Natural: ASSIGN #HDEST-ADDR1 := #ADDRSS-LINE-1
        pnd_Hold_Address_Pnd_Hdest_Addr2.setValue(pnd_Input_Part_Pnd_Addrss_Line_2);                                                                                      //Natural: ASSIGN #HDEST-ADDR2 := #ADDRSS-LINE-2
        pnd_Hold_Address_Pnd_Hdest_Addr3.setValue(pnd_Input_Part_Pnd_Addrss_Line_3);                                                                                      //Natural: ASSIGN #HDEST-ADDR3 := #ADDRSS-LINE-3
        pnd_Hold_Address_Pnd_Hdest_City.setValue(pnd_Input_Part_Pnd_City);                                                                                                //Natural: ASSIGN #HDEST-CITY := #CITY
        pnd_Hold_Address_Pnd_Hdest_State.setValue(pnd_Input_Part_Pnd_Resid_Stae);                                                                                         //Natural: ASSIGN #HDEST-STATE := #RESID-STAE
        pnd_Hold_Address_Pnd_Hzip.setValue(pnd_Input_Part_Pnd_Zip);                                                                                                       //Natural: ASSIGN #HZIP := #ZIP
        pnd_Addr_Indx.setValue(1);                                                                                                                                        //Natural: ASSIGN #ADDR-INDX := 1
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
        sub_Reformat_Address();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getReports().write(0, "REFORMAT MAIN ADDRESS");                                                                                                               //Natural: WRITE 'REFORMAT MAIN ADDRESS'
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Input_Part_Pnd_Addrss_Line_1);                                                                                                  //Natural: WRITE '=' #ADDRSS-LINE-1
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Input_Part_Pnd_Addrss_Line_2);                                                                                                  //Natural: WRITE '=' #ADDRSS-LINE-2
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Input_Part_Pnd_Addrss_Line_3);                                                                                                  //Natural: WRITE '=' #ADDRSS-LINE-3
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Input_Part_Pnd_City);                                                                                                           //Natural: WRITE '=' #CITY
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Input_Part_Pnd_Resid_Stae);                                                                                                     //Natural: WRITE '=' #RESID-STAE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Input_Part_Pnd_Zip);                                                                                                            //Natural: WRITE '=' #ZIP
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,1));                                                        //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,2));                                                        //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,3));                                                        //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,3 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,4));                                                        //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,4 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Zip_Code().getValue(pnd_Addr_Indx));                                                             //Natural: WRITE '=' CIS-ZIP-CODE ( #ADDR-INDX )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Usage_Code().getValue(pnd_Addr_Indx).setValue("1");                                                                       //Natural: ASSIGN CIS-ADDR-USAGE-CODE ( #ADDR-INDX ) := '1'
        if (condition(pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line.notEquals(" ")))                                                                                         //Natural: IF #ROLLOVER-DEST-ADDR-LINE NE ' '
        {
            pnd_Hold_Address_Pnd_Hdest_Addr1.setValue(pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line);                                                                        //Natural: ASSIGN #HDEST-ADDR1 := #ROLLOVER-DEST-ADDR-LINE
            pnd_Hold_Address_Pnd_Hdest_Addr2.setValue(pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_2);                                                                      //Natural: ASSIGN #HDEST-ADDR2 := #ROLLOVER-DEST-ADDR-LINE-2
            pnd_Hold_Address_Pnd_Hdest_Addr3.setValue(pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_3);                                                                      //Natural: ASSIGN #HDEST-ADDR3 := #ROLLOVER-DEST-ADDR-LINE-3
            pnd_Hold_Address_Pnd_Hdest_City.setValue(pnd_Input_Part_Pnd_Rollover_Dest_Addr_City);                                                                         //Natural: ASSIGN #HDEST-CITY := #ROLLOVER-DEST-ADDR-CITY
            pnd_Hold_Address_Pnd_Hdest_State.setValue(pnd_Input_Part_Pnd_Rollover_Dest_Addr_St);                                                                          //Natural: ASSIGN #HDEST-STATE := #ROLLOVER-DEST-ADDR-ST
            pnd_Hold_Address_Pnd_Hzip.setValue(pnd_Input_Part_Pnd_Rollover_Dest_Addr_Zip);                                                                                //Natural: ASSIGN #HZIP := #ROLLOVER-DEST-ADDR-ZIP
            pnd_Addr_Indx.setValue(2);                                                                                                                                    //Natural: ASSIGN #ADDR-INDX := 2
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
            sub_Reformat_Address();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, "REFORMAT ROLLOVER ADDRESS");                                                                                                       //Natural: WRITE 'REFORMAT ROLLOVER ADDRESS'
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line);                                                                                    //Natural: WRITE '=' #ROLLOVER-DEST-ADDR-LINE
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_2);                                                                                  //Natural: WRITE '=' #ROLLOVER-DEST-ADDR-LINE-2
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_Line_3);                                                                                  //Natural: WRITE '=' #ROLLOVER-DEST-ADDR-LINE-3
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_City);                                                                                    //Natural: WRITE '=' #ROLLOVER-DEST-ADDR-CITY
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_St);                                                                                      //Natural: WRITE '=' #ROLLOVER-DEST-ADDR-ST
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Input_Part_Pnd_Rollover_Dest_Addr_Zip);                                                                                     //Natural: WRITE '=' #ROLLOVER-DEST-ADDR-ZIP
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,1));                                                    //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,1 )
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,2));                                                    //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,2 )
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,3));                                                    //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,3 )
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,4));                                                    //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,4 )
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Zip_Code().getValue(pnd_Addr_Indx));                                                         //Natural: WRITE '=' CIS-ZIP-CODE ( #ADDR-INDX )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Usage_Code().getValue(pnd_Addr_Indx).setValue("2");                                                                   //Natural: ASSIGN CIS-ADDR-USAGE-CODE ( #ADDR-INDX ) := '2'
            ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Dest_Name().getValue(pnd_Addr_Indx).setValue(pnd_Input_Part_Pnd_Rollover_Dest_Name);                               //Natural: ASSIGN CIS-ADDRESS-DEST-NAME ( #ADDR-INDX ) := #ROLLOVER-DEST-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Input_Part_Pnd_Dest_Addr_Line.notEquals(" ")))                                                                                              //Natural: IF #DEST-ADDR-LINE NE ' '
            {
                pnd_Hold_Address_Pnd_Hdest_Addr1.setValue(pnd_Input_Part_Pnd_Dest_Addr_Line);                                                                             //Natural: ASSIGN #HDEST-ADDR1 := #DEST-ADDR-LINE
                pnd_Hold_Address_Pnd_Hdest_Addr2.setValue(pnd_Input_Part_Pnd_Dest_Addr_Line_2);                                                                           //Natural: ASSIGN #HDEST-ADDR2 := #DEST-ADDR-LINE-2
                pnd_Hold_Address_Pnd_Hdest_Addr3.setValue(pnd_Input_Part_Pnd_Dest_Addr_Line_3);                                                                           //Natural: ASSIGN #HDEST-ADDR3 := #DEST-ADDR-LINE-3
                pnd_Hold_Address_Pnd_Hdest_City.setValue(pnd_Input_Part_Pnd_Dest_Addr_City);                                                                              //Natural: ASSIGN #HDEST-CITY := #DEST-ADDR-CITY
                pnd_Hold_Address_Pnd_Hdest_State.setValue(pnd_Input_Part_Pnd_Dest_Addr_St);                                                                               //Natural: ASSIGN #HDEST-STATE := #DEST-ADDR-ST
                pnd_Hold_Address_Pnd_Hzip.setValue(pnd_Input_Part_Pnd_Dest_Addr_Zip);                                                                                     //Natural: ASSIGN #HZIP := #DEST-ADDR-ZIP
                pnd_Addr_Indx.setValue(2);                                                                                                                                //Natural: ASSIGN #ADDR-INDX := 2
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
                sub_Reformat_Address();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getReports().write(0, "REFORMAT DEST ADDRESS");                                                                                                       //Natural: WRITE 'REFORMAT DEST ADDRESS'
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",pnd_Input_Part_Pnd_Dest_Addr_Line);                                                                                         //Natural: WRITE '=' #DEST-ADDR-LINE
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",pnd_Input_Part_Pnd_Dest_Addr_Line_2);                                                                                       //Natural: WRITE '=' #DEST-ADDR-LINE-2
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",pnd_Input_Part_Pnd_Dest_Addr_Line_3);                                                                                       //Natural: WRITE '=' #DEST-ADDR-LINE-3
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",pnd_Input_Part_Pnd_Dest_Addr_City);                                                                                         //Natural: WRITE '=' #DEST-ADDR-CITY
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",pnd_Input_Part_Pnd_Dest_Addr_St);                                                                                           //Natural: WRITE '=' #DEST-ADDR-ST
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",pnd_Input_Part_Pnd_Dest_Addr_Zip);                                                                                          //Natural: WRITE '=' #DEST-ADDR-ZIP
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,1));                                                //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,1 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,2));                                                //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,2 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,3));                                                //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,3 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,4));                                                //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,4 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Zip_Code().getValue(pnd_Addr_Indx));                                                     //Natural: WRITE '=' CIS-ZIP-CODE ( #ADDR-INDX )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Usage_Code().getValue(pnd_Addr_Indx).setValue("2");                                                               //Natural: ASSIGN CIS-ADDR-USAGE-CODE ( #ADDR-INDX ) := '2'
                ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Dest_Name().getValue(pnd_Addr_Indx).setValue(pnd_Input_Part_Pnd_Dest_Name);                                    //Natural: ASSIGN CIS-ADDRESS-DEST-NAME ( #ADDR-INDX ) := #DEST-NAME
                ldaCisl200.getCis_Prtcpnt_File_Cis_Bank_Pymnt_Acct_Nmbr().getValue(pnd_Addr_Indx).setValue(pnd_Input_Part_Pnd_Dest_Bank_Acct_Nbr);                        //Natural: ASSIGN CIS-BANK-PYMNT-ACCT-NMBR ( #ADDR-INDX ) := #DEST-BANK-ACCT-NBR
                ldaCisl200.getCis_Prtcpnt_File_Cis_Bank_Aba_Acct_Nmbr().getValue(pnd_Addr_Indx).setValue(pnd_Input_Part_Pnd_Dest_Bank_Trst_Nbr);                          //Natural: ASSIGN CIS-BANK-ABA-ACCT-NMBR ( #ADDR-INDX ) := #DEST-BANK-TRST-NBR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl200.getCis_Prtcpnt_File_Cis_Corp_Sync_Ind().setValue("Y");                                                                                                 //Natural: ASSIGN CIS-CORP-SYNC-IND := 'Y'
        ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().setValue("S");                                                                                                  //Natural: ASSIGN CIS-COR-SYNC-IND := 'S'
        ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Process_Env().setValue("YB");                                                                                              //Natural: ASSIGN CIS-COR-PROCESS-ENV := 'YB'
        ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Ph_Rcd_Typ_Cde().setValue(1);                                                                                              //Natural: ASSIGN CIS-COR-PH-RCD-TYP-CDE := 01
        ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Cntrct_Payee_Cde().setValue("01");                                                                                         //Natural: ASSIGN CIS-COR-CNTRCT-PAYEE-CDE := '01'
        //* CREA
        //* CREA
        if (condition(pnd_Input_Part_Pnd_Cis_Handle_Code.equals("O")))                                                                                                    //Natural: IF #CIS-HANDLE-CODE EQ 'O'
        {
            ldaCisl200.getCis_Prtcpnt_File_Cis_Pull_Code().setValue("OVER");                                                                                              //Natural: ASSIGN CIS-PULL-CODE := 'OVER'
            //* CREA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* CREA
            //* CREA
            if (condition(pnd_Input_Part_Pnd_Cis_Handle_Code.equals("P")))                                                                                                //Natural: IF #CIS-HANDLE-CODE EQ 'P'
            {
                ldaCisl200.getCis_Prtcpnt_File_Cis_Pull_Code().setValue("SPEC");                                                                                          //Natural: ASSIGN CIS-PULL-CODE := 'SPEC'
                //* CREA
            }                                                                                                                                                             //Natural: END-IF
            //* CREA
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Part_Pnd_Cis_Requestor.notEquals("IPI")))                                                                                                 //Natural: IF #CIS-REQUESTOR NE 'IPI'
        {
            ldaCisl200.getCis_Prtcpnt_File_Cis_Grnted_Grd_Amt().setValue(pnd_Input_Part_Pnd_Cis_Grnted_Grd_Amt_N);                                                        //Natural: ASSIGN CIS-GRNTED-GRD-AMT := #CIS-GRNTED-GRD-AMT-N
            ldaCisl200.getCis_Prtcpnt_File_Cis_Grnted_Std_Amt().setValue(pnd_Input_Part_Pnd_Cis_Grnted_Grd_Amt_N);                                                        //Natural: ASSIGN CIS-GRNTED-STD-AMT := #CIS-GRNTED-GRD-AMT-N
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl200.getCis_Prtcpnt_File_Cis_Da_Tiaa_Nbr().getValue(1).setValue(pnd_Input_Part_Pnd_Cis_Da_Tiaa_Nbr_Sttl);                                                   //Natural: ASSIGN CIS-DA-TIAA-NBR ( 1 ) := #CIS-DA-TIAA-NBR-STTL
        ldaCisl200.getCis_Prtcpnt_File_Cis_Da_Tiaa_Proceeds_Amt().getValue(1).setValue(pnd_Input_Part_Pnd_Cis_Da_Proceeds_Amt_N);                                         //Natural: ASSIGN CIS-DA-TIAA-PROCEEDS-AMT ( 1 ) := #CIS-DA-PROCEEDS-AMT-N
        ldaCisl200.getCis_Prtcpnt_File_Cis_Trnsf_Tiaa_Rea_Nbr().setValue(pnd_Input_Part_Pnd_Cis_Trnsf_Tiaa_Rea_Nbr);                                                      //Natural: ASSIGN CIS-TRNSF-TIAA-REA-NBR := #CIS-TRNSF-TIAA-REA-NBR
        ldaCisl200.getCis_Prtcpnt_File_Cis_Trnsf_Cert_Nbr().setValue(pnd_Input_Part_Pnd_Cis_Trnsf_Tiaa_Cer_Nbr);                                                          //Natural: ASSIGN CIS-TRNSF-CERT-NBR := #CIS-TRNSF-TIAA-CER-NBR
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(20)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
        {
            if (condition(pnd_Input_Part_Pnd_Fund_Allocation.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).notEquals(" ")))                                             //Natural: IF #FUND-ALLOCATION ( #I ) NE ' '
            {
                //* CREA
                //* CREA
                DbsUtil.examine(new ExamineSource(pnd_Translte_File_Pnd_Translte_Ticker.getValue("*")), new ExamineSearch(pnd_Input_Part_Pnd_Fund_Ticker.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())),  //Natural: EXAMINE #TRANSLTE-TICKER ( * ) #FUND-TICKER ( #I ) GIVING INDEX #FUND-IDX
                    new ExamineGivingIndex(pnd_Fund_Idx));
                //* CREA
                if (condition(pnd_Fund_Idx.greater(getZero())))                                                                                                           //Natural: IF #FUND-IDX > 0
                {
                    //* CREA
                    //* CREA
                    if (condition(pnd_Translte_File_Pnd_Translte_Pull_Code.getValue(pnd_Fund_Idx).notEquals(" ")))                                                        //Natural: IF #TRANSLTE-PULL-CODE ( #FUND-IDX ) NE ' '
                    {
                        ldaCisl200.getCis_Prtcpnt_File_Cis_Pull_Code().setValue(pnd_Translte_File_Pnd_Translte_Pull_Code.getValue(pnd_Fund_Idx));                         //Natural: ASSIGN CIS-PULL-CODE := #TRANSLTE-PULL-CODE ( #FUND-IDX )
                        //* CREA
                    }                                                                                                                                                     //Natural: END-IF
                    //* CREA
                    //* CREA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCisl200.getCis_Prtcpnt_File_Cis_Pull_Code().setValue("SPEC");                                                                                      //Natural: ASSIGN CIS-PULL-CODE := 'SPEC'
                    //* CREA
                }                                                                                                                                                         //Natural: END-IF
                pdaNeca4000.getNeca4000().reset();                                                                                                                        //Natural: RESET NECA4000 #FND-AL-TIC
                pnd_Fnd_Al_Tic.reset();
                pnd_Fnd_Al_Tic.setValue(pnd_Input_Part_Pnd_Fund_Allocation.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()));                                              //Natural: ASSIGN #FND-AL-TIC := #FUND-ALLOCATION ( #I )
                pdaNeca4000.getNeca4000_Function_Cde().setValue("FND");                                                                                                   //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'FND'
                pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                             //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '01'
                pdaNeca4000.getNeca4000_Fnd_Key1_Table_Cde().setValue("FND");                                                                                             //Natural: ASSIGN NECA4000.FND-KEY1-TABLE-CDE := 'FND'
                pdaNeca4000.getNeca4000_Fnd_Key1_Ticker_Symbol().setValue(pnd_Fnd_Al_Tic_Pnd_Fnd_Tic);                                                                    //Natural: ASSIGN NECA4000.FND-KEY1-TICKER-SYMBOL := #FND-TIC
                DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                    //Natural: CALLNAT 'NECN4000' NECA4000
                if (condition(Global.isEscape())) return;
                ldaCisl200.getCis_Prtcpnt_File_Cis_Cref_Acct_Cde().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(pdaNeca4000.getNeca4000_Fnd_Alpha_Fund_Cde().getValue(1)); //Natural: ASSIGN CIS-CREF-ACCT-CDE ( #I ) := FND-ALPHA-FUND-CDE ( 1 )
                ldaCisl200.getCis_Prtcpnt_File_Cis_Cref_Annual_Nbr_Units().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(pnd_Fnd_Al_Tic_Pnd_Fnd_Al);           //Natural: ASSIGN CIS-CREF-ANNUAL-NBR-UNITS ( #I ) := #FND-AL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *
        //* * ADD ATRA FLAG FOR CIS EXTRACT   /* DG START (4 FEB 08)
        if (condition(pnd_Input_Part_Pnd_Sub_Plan.getSubstring(1,3).equals("IPA") || pnd_Input_Part_Pnd_Sub_Plan.getSubstring(1,3).equals("IPB")))                        //Natural: IF SUBSTR ( #SUB-PLAN,1,3 ) = 'IPA' OR = 'IPB'
        {
            ldaCisl200.getCis_Prtcpnt_File_Cis_Sg_Text_Udf_1().setValue("A");                                                                                             //Natural: ASSIGN CIS-SG-TEXT-UDF-1 := 'A'
            //*  DG END
        }                                                                                                                                                                 //Natural: END-IF
        PND_PND_L3680:                                                                                                                                                    //Natural: STORE CIS-PRTCPNT-FILE
        ldaCisl200.getVw_cis_Prtcpnt_File().insertDBRow("PND_PND_L3680");
        //*  09/2011 B.HOLLOWAY START - BACKOUT IF TESTING
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  09/2011 B.HOLLOWAY END
                                                                                                                                                                          //Natural: PERFORM UPDATE-MIT
        sub_Update_Mit();
        if (condition(Global.isEscape())) {return;}
        //*  09/2011 B.HOLLOWAY START - BACKOUT IF TESTING
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  09/2011 B.HOLLOWAY END
                                                                                                                                                                          //Natural: PERFORM GENERATE-CIS-UPDATE-REPORT
        sub_Generate_Cis_Update_Report();
        if (condition(Global.isEscape())) {return;}
        pnd_Total_Add.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #TOTAL-ADD
        pnd_Input_Part_Aa.getValue("*").reset();                                                                                                                          //Natural: RESET #INPUT-PART-AA ( * )
    }
    private void sub_Load_Bene_Data() throws Exception                                                                                                                    //Natural: LOAD-BENE-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        getReports().write(0, "=",ldaCisl200.getCis_Prtcpnt_File_Cis_Pin_Nbr(),"YYYYYYYYYYYY");                                                                           //Natural: WRITE '=' CIS-PIN-NBR 'YYYYYYYYYYYY'
        if (Global.isEscape()) return;
        ldaCisl2020.getCis_Bene_File_01_Cis_Rcrd_Type_Cde().setValue("1");                                                                                                //Natural: ASSIGN CIS-RCRD-TYPE-CDE := '1'
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr().setValue(pnd_Input_Part_Pnd_Cis_Bene_Tpa_Tiaa);                                                               //Natural: ASSIGN CIS-BENE-TIAA-NBR := #CIS-BENE-TPA-TIAA
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Cref_Nbr().setValue(pnd_Input_Part_Pnd_Cis_Bene_Tpa_Cref);                                                               //Natural: ASSIGN CIS-BENE-CREF-NBR := #CIS-BENE-TPA-CREF
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Rqst_Id().setValue("IPRO");                                                                                              //Natural: ASSIGN CIS-BENE-RQST-ID := 'IPRO'
        //*  PINEXP
        //*  PINEXP
        if (condition(pnd_Input_Part_Pnd_Cis_Bene_Pin_Filler.equals(" ")))                                                                                                //Natural: IF #CIS-BENE-PIN-FILLER = ' '
        {
            ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Unique_Id_Nbr().setValue(pnd_Input_Part_Pnd_Cis_Bene_Pin_N7);                                                        //Natural: ASSIGN CIS-BENE-UNIQUE-ID-NBR := #CIS-BENE-PIN-N7
            //*  PINEXP
            //*  PINEXP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Unique_Id_Nbr().setValue(pnd_Input_Part_Pnd_Cis_Bene_Pin_N);                                                         //Natural: ASSIGN CIS-BENE-UNIQUE-ID-NBR := #CIS-BENE-PIN-N
            //*  PINEXP
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Ssn().setValue(pnd_Input_Part_Pnd_Cis_Bene_Frst_Ssn_N);                                                             //Natural: ASSIGN CIS-BENE-ANNT-SSN := #CIS-BENE-FRST-SSN-N
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Frst_Nme().setValue(pnd_Input_Part_Pnd_Cis_Bene_Frst_Annt_Fname);                                                   //Natural: ASSIGN CIS-BENE-ANNT-FRST-NME := #CIS-BENE-FRST-ANNT-FNAME
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Lst_Nme().setValue(pnd_Input_Part_Pnd_Cis_Bene_Last_Annt_Nme);                                                      //Natural: ASSIGN CIS-BENE-ANNT-LST-NME := #CIS-BENE-LAST-ANNT-NME
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Rqst_Id_Key().setValue(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id_Key());                                                //Natural: ASSIGN CIS-BENE-RQST-ID-KEY := CIS-RQST-ID-KEY
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Std_Free().setValue("N");                                                                                                //Natural: ASSIGN CIS-BENE-STD-FREE := 'N'
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Estate().setValue("N");                                                                                                  //Natural: ASSIGN CIS-BENE-ESTATE := 'N'
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Trust().setValue("N");                                                                                                   //Natural: ASSIGN CIS-BENE-TRUST := 'N'
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Category().setValue(pnd_Input_Part_Pnd_Cis_Bene_Category);                                                               //Natural: ASSIGN CIS-BENE-CATEGORY := #CIS-BENE-CATEGORY
        ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn().setValue("N");                                                                                       //Natural: ASSIGN CIS-PRMRY-BENE-CHILD-PRVSN := 'N'
        ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn().setValue("N");                                                                                      //Natural: ASSIGN CIS-CNTGNT-BENE-CHILD-PRVSN := 'N'
        if (condition(pnd_Input_Part_Pnd_Cis_Bene_Category.equals("P")))                                                                                                  //Natural: IF #CIS-BENE-CATEGORY EQ 'P'
        {
            pnd_Bene_Cnt_Prmry.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #BENE-CNT-PRMRY
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Std_Txt().getValue(pnd_Bene_Cnt_Prmry).setValue("N");                                                          //Natural: ASSIGN CIS-PRMRY-BENE-STD-TXT ( #BENE-CNT-PRMRY ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Lump_Sum().getValue(pnd_Bene_Cnt_Prmry).setValue("N");                                                         //Natural: ASSIGN CIS-PRMRY-BENE-LUMP-SUM ( #BENE-CNT-PRMRY ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Auto_Cmnt_Val().getValue(pnd_Bene_Cnt_Prmry).setValue("N");                                                    //Natural: ASSIGN CIS-PRMRY-BENE-AUTO-CMNT-VAL ( #BENE-CNT-PRMRY ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Name().getValue(pnd_Bene_Cnt_Prmry).setValue(pnd_Input_Part_Pnd_Cis_Bene_Name);                                //Natural: ASSIGN CIS-PRMRY-BENE-NAME ( #BENE-CNT-PRMRY ) := #CIS-BENE-NAME
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn().getValue(pnd_Bene_Cnt_Prmry).setValue(pnd_Input_Part_Pnd_Cis_Bene_Reltn);                               //Natural: ASSIGN CIS-PRMRY-BENE-RLTN ( #BENE-CNT-PRMRY ) := #CIS-BENE-RELTN
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde().getValue(pnd_Bene_Cnt_Prmry).setValue(pnd_Input_Part_Pnd_Cis_Bene_Reltn_Cde);                       //Natural: ASSIGN CIS-PRMRY-BENE-RLTN-CDE ( #BENE-CNT-PRMRY ) := #CIS-BENE-RELTN-CDE
            if (condition(pnd_Input_Part_Pnd_Cis_Bene_Ssn_N.greater(getZero())))                                                                                          //Natural: IF #CIS-BENE-SSN-N GT 0
            {
                ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr().getValue(pnd_Bene_Cnt_Prmry).setValue(pnd_Input_Part_Pnd_Cis_Bene_Ssn_N);                        //Natural: ASSIGN CIS-PRMRY-BENE-SSN-NBR ( #BENE-CNT-PRMRY ) := #CIS-BENE-SSN-N
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Part_Pnd_Cis_Bene_Dob.notEquals(" ")))                                                                                                //Natural: IF #CIS-BENE-DOB NE ' '
            {
                ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Dob().getValue(pnd_Bene_Cnt_Prmry).setValueEdited(new ReportEditMask("YYYY/MM/DD"),pnd_Input_Part_Pnd_Cis_Bene_Dob); //Natural: MOVE EDITED #CIS-BENE-DOB TO CIS-PRMRY-BENE-DOB ( #BENE-CNT-PRMRY ) ( EM = YYYY/MM/DD )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Alloc.reset();                                                                                                                                            //Natural: RESET #ALLOC #BENE-ALLOC
            pnd_Bene_Alloc.reset();
            pnd_Alloc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Part_Pnd_Cis_Bene_Alloc_1_N, pnd_Input_Part_Pnd_Cis_Bene_Alloc_4_N));            //Natural: COMPRESS #CIS-BENE-ALLOC-1-N #CIS-BENE-ALLOC-4-N INTO #ALLOC LEAVING NO SPACE
            pnd_Alloc.setValue(pnd_Alloc, MoveOption.LeftJustified);                                                                                                      //Natural: MOVE LEFT JUSTIFIED #ALLOC TO #ALLOC
            DbsUtil.examine(new ExamineSource(pnd_Alloc), new ExamineSearch(" "), new ExamineReplace("0"));                                                               //Natural: EXAMINE #ALLOC FOR ' ' REPLACE WITH '0'
            pnd_Bene_Alloc.compute(new ComputeParameters(false, pnd_Bene_Alloc), pnd_Alloc_Pnd_Alloc_N.multiply(100));                                                    //Natural: ASSIGN #BENE-ALLOC := #ALLOC-N *100
            if (condition(pnd_Bene_Alloc.greater(getZero())))                                                                                                             //Natural: IF #BENE-ALLOC GT 0
            {
                ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Allctn_Pct().getValue(pnd_Bene_Cnt_Prmry).setValue(pnd_Bene_Alloc);                                        //Natural: ASSIGN CIS-PRMRY-BENE-ALLCTN-PCT ( #BENE-CNT-PRMRY ) := #BENE-ALLOC
                ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(pnd_Bene_Cnt_Prmry).setValue(pnd_Bene_Alloc);                                         //Natural: ASSIGN CIS-PRMRY-BENE-NMRTR-NBR ( #BENE-CNT-PRMRY ) := #BENE-ALLOC
                ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_Bene_Cnt_Prmry).setValue(100);                                                   //Natural: ASSIGN CIS-PRMRY-BENE-DNMNTR-NBR ( #BENE-CNT-PRMRY ) := 100
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Part_Pnd_Cis_Bene_Category.equals("C")))                                                                                                  //Natural: IF #CIS-BENE-CATEGORY EQ 'C'
        {
            pnd_Bene_Cnt_Cntgnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #BENE-CNT-CNTGNT
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Std_Txt().getValue(pnd_Bene_Cnt_Cntgnt).setValue("N");                                                        //Natural: ASSIGN CIS-CNTGNT-BENE-STD-TXT ( #BENE-CNT-CNTGNT ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Lump_Sum().getValue(pnd_Bene_Cnt_Cntgnt).setValue("N");                                                       //Natural: ASSIGN CIS-CNTGNT-BENE-LUMP-SUM ( #BENE-CNT-CNTGNT ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Auto_Cmnt_Val().getValue(pnd_Bene_Cnt_Cntgnt).setValue("N");                                                  //Natural: ASSIGN CIS-CNTGNT-BENE-AUTO-CMNT-VAL ( #BENE-CNT-CNTGNT ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Name().getValue(pnd_Bene_Cnt_Cntgnt).setValue(pnd_Input_Part_Pnd_Cis_Bene_Name);                              //Natural: ASSIGN CIS-CNTGNT-BENE-NAME ( #BENE-CNT-CNTGNT ) := #CIS-BENE-NAME
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn().getValue(pnd_Bene_Cnt_Cntgnt).setValue(pnd_Input_Part_Pnd_Cis_Bene_Reltn);                             //Natural: ASSIGN CIS-CNTGNT-BENE-RLTN ( #BENE-CNT-CNTGNT ) := #CIS-BENE-RELTN
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde().getValue(pnd_Bene_Cnt_Cntgnt).setValue(pnd_Input_Part_Pnd_Cis_Bene_Reltn_Cde);                     //Natural: ASSIGN CIS-CNTGNT-BENE-RLTN-CDE ( #BENE-CNT-CNTGNT ) := #CIS-BENE-RELTN-CDE
            if (condition(pnd_Input_Part_Pnd_Cis_Bene_Ssn_N.greater(getZero())))                                                                                          //Natural: IF #CIS-BENE-SSN-N GT 0
            {
                ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr().getValue(pnd_Bene_Cnt_Cntgnt).setValue(pnd_Input_Part_Pnd_Cis_Bene_Ssn_N);                      //Natural: ASSIGN CIS-CNTGNT-BENE-SSN-NBR ( #BENE-CNT-CNTGNT ) := #CIS-BENE-SSN-N
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Part_Pnd_Cis_Bene_Dob.notEquals(" ")))                                                                                                //Natural: IF #CIS-BENE-DOB NE ' '
            {
                ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dob().getValue(pnd_Bene_Cnt_Cntgnt).setValueEdited(new ReportEditMask("YYYY/MM/DD"),pnd_Input_Part_Pnd_Cis_Bene_Dob); //Natural: MOVE EDITED #CIS-BENE-DOB TO CIS-CNTGNT-BENE-DOB ( #BENE-CNT-CNTGNT ) ( EM = YYYY/MM/DD )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Alloc.reset();                                                                                                                                            //Natural: RESET #ALLOC #BENE-ALLOC
            pnd_Bene_Alloc.reset();
            pnd_Alloc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Part_Pnd_Cis_Bene_Alloc_1_N, pnd_Input_Part_Pnd_Cis_Bene_Alloc_4_N));            //Natural: COMPRESS #CIS-BENE-ALLOC-1-N #CIS-BENE-ALLOC-4-N INTO #ALLOC LEAVING NO SPACE
            pnd_Alloc.setValue(pnd_Alloc, MoveOption.LeftJustified);                                                                                                      //Natural: MOVE LEFT JUSTIFIED #ALLOC TO #ALLOC
            DbsUtil.examine(new ExamineSource(pnd_Alloc), new ExamineSearch(" "), new ExamineReplace("0"));                                                               //Natural: EXAMINE #ALLOC FOR ' ' REPLACE WITH '0'
            pnd_Bene_Alloc.compute(new ComputeParameters(false, pnd_Bene_Alloc), pnd_Alloc_Pnd_Alloc_N.multiply(100));                                                    //Natural: ASSIGN #BENE-ALLOC := #ALLOC-N *100
            if (condition(pnd_Bene_Alloc.greater(getZero())))                                                                                                             //Natural: IF #BENE-ALLOC GT 0
            {
                ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Allctn_Pct().getValue(pnd_Bene_Cnt_Cntgnt).setValue(pnd_Bene_Alloc);                                      //Natural: ASSIGN CIS-CNTGNT-BENE-ALLCTN-PCT ( #BENE-CNT-CNTGNT ) := #BENE-ALLOC
                ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(pnd_Bene_Cnt_Cntgnt).setValue(pnd_Bene_Alloc);                                       //Natural: ASSIGN CIS-CNTGNT-BENE-NMRTR-NBR ( #BENE-CNT-CNTGNT ) := #BENE-ALLOC
                ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_Bene_Cnt_Cntgnt).setValue(100);                                                 //Natural: ASSIGN CIS-CNTGNT-BENE-DNMNTR-NBR ( #BENE-CNT-CNTGNT ) := 100
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().eject(0, true);                                                                                                                                      //Natural: EJECT
        getReports().write(0, "BENE INFORMATION");                                                                                                                        //Natural: WRITE 'BENE INFORMATION'
        if (Global.isEscape()) return;
        getReports().write(0, " ");                                                                                                                                       //Natural: WRITE ' '
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Tpa_Tiaa,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Tpa_Cref,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Pin, //Natural: WRITE / '=' #CIS-BENE-TPA-TIAA / '=' #CIS-BENE-TPA-CREF / '=' #CIS-BENE-PIN / '=' #CIS-BENE-FRST-ANNT-FNAME / '=' #CIS-BENE-LAST-ANNT-NME / '=' #CIS-BENE-FRST-GENDER / '=' #CIS-BENE-FRST-SSN / '=' #CIS-BENE-FRST-DOB / '=' #CIS-BENE-SSN / '=' #CIS-BENE-NAME / '=' #CIS-BENE-DOB / '=' #CIS-BENE-RELTN / '=' #CIS-BENE-CATEGORY / '=' #BENE-ALLOC / '=' #ALLOC
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Frst_Annt_Fname,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Last_Annt_Nme,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Frst_Gender,
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Frst_Ssn,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Frst_Dob,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Ssn,
            NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Name,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Dob,NEWLINE,"=",pnd_Input_Part_Pnd_Cis_Bene_Reltn,NEWLINE,
            "=",pnd_Input_Part_Pnd_Cis_Bene_Category,NEWLINE,"=",pnd_Bene_Alloc,NEWLINE,"=",pnd_Alloc);
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cis_Prtcpnt() throws Exception                                                                                                                   //Natural: GET-CIS-PRTCPNT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tiaa_Cntrct_Found.setValue(false);                                                                                                                            //Natural: ASSIGN #TIAA-CNTRCT-FOUND := FALSE
        ldaCisl200.getVw_cis_Prtcpnt_File().startDatabaseRead                                                                                                             //Natural: READ CIS-PRTCPNT-FILE BY CIS-TIAA-NBR EQ #CIS-TPA-TIAA-CNTRCT
        (
        "READ1",
        new Wc[] { new Wc("CIS_TIAA_NBR", ">=", pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct, WcType.BY) },
        new Oc[] { new Oc("CIS_TIAA_NBR", "ASC") }
        );
        READ1:
        while (condition(ldaCisl200.getVw_cis_Prtcpnt_File().readNextRow("READ1")))
        {
            if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Tiaa_Nbr().notEquals(pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct)))                                               //Natural: IF CIS-TIAA-NBR NE #CIS-TPA-TIAA-CNTRCT
            {
                pnd_Tiaa_Cntrct_Found.setValue(false);                                                                                                                    //Natural: ASSIGN #TIAA-CNTRCT-FOUND := FALSE
                if (true) break READ1;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ1. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Tiaa_Nbr().equals(pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct)))                                                  //Natural: IF CIS-TIAA-NBR EQ #CIS-TPA-TIAA-CNTRCT
            {
                pnd_Tiaa_Cntrct_Found.setValue(true);                                                                                                                     //Natural: ASSIGN #TIAA-CNTRCT-FOUND := TRUE
                if (true) break READ1;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Generate_Cis_Report_Duplicate() throws Exception                                                                                                     //Natural: GENERATE-CIS-REPORT-DUPLICATE
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(2, "Plan Id",                                                                                                                                //Natural: DISPLAY ( 2 ) 'Plan Id' #PLAN-ID 'Sub Plan' #SUB-PLAN 'IPRO Cntrct' #CIS-TPA-TIAA-CNTRCT 'PIN Nbr' #CIS-PIN-NBR 'SSN' #CIS-FRST-ANNT-SSN 'Last Name' #CIS-FRST-ANNT-LST-NME 'TDOI' #CIS-TIAA-DOI 'Annty OPT' #CIS-ANNTY-OPTION
        		pnd_Input_Part_Pnd_Plan_Id,"Sub Plan",
        		pnd_Input_Part_Pnd_Sub_Plan,"IPRO Cntrct",
        		pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct,"PIN Nbr",
        		pnd_Input_Part_Pnd_Cis_Pin_Nbr,"SSN",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn,"Last Name",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme,"TDOI",
        		pnd_Input_Part_Pnd_Cis_Tiaa_Doi,"Annty OPT",
        		pnd_Input_Part_Pnd_Cis_Annty_Option);
        if (Global.isEscape()) return;
    }
    private void sub_Generate_Cis_Update_Report() throws Exception                                                                                                        //Natural: GENERATE-CIS-UPDATE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(3, "Plan Id",                                                                                                                                //Natural: DISPLAY ( 3 ) 'Plan Id' #PLAN-ID 'Sub Plan' #SUB-PLAN 'PIN Nbr' #CIS-PIN-NBR 'SSN' #CIS-FRST-ANNT-SSN 'Last Name' #CIS-FRST-ANNT-LST-NME ( AL = 20 ) 'TDOI' #CIS-TIAA-DOI 'IPRO Cntrct' #CIS-TPA-TIAA-CNTRCT 'Annty OPT' #CIS-ANNTY-OPTION
        		pnd_Input_Part_Pnd_Plan_Id,"Sub Plan",
        		pnd_Input_Part_Pnd_Sub_Plan,"PIN Nbr",
        		pnd_Input_Part_Pnd_Cis_Pin_Nbr,"SSN",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn,"Last Name",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme, new AlphanumericLength (20),"TDOI",
        		pnd_Input_Part_Pnd_Cis_Tiaa_Doi,"IPRO Cntrct",
        		pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct,"Annty OPT",
        		pnd_Input_Part_Pnd_Cis_Annty_Option);
        if (Global.isEscape()) return;
    }
    private void sub_Generate_Error_Report() throws Exception                                                                                                             //Natural: GENERATE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(4, "Plan Id",                                                                                                                                //Natural: DISPLAY ( 4 ) 'Plan Id' #PLAN-ID 'Sub Plan' #SUB-PLAN 'PIN Nbr' #CIS-PIN-NBR 'SSN' #CIS-FRST-ANNT-SSN 'IPRO Cntrct' #CIS-TPA-TIAA-CNTRCT 'Last Name' #CIS-FRST-ANNT-LST-NME ( AL = 10 ) 'TDOI' #CIS-TIAA-DOI 'ERROR' #ERR-MESSAGE ( AL = 20 )
        		pnd_Input_Part_Pnd_Plan_Id,"Sub Plan",
        		pnd_Input_Part_Pnd_Sub_Plan,"PIN Nbr",
        		pnd_Input_Part_Pnd_Cis_Pin_Nbr,"SSN",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn,"IPRO Cntrct",
        		pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct,"Last Name",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme, new AlphanumericLength (10),"TDOI",
        		pnd_Input_Part_Pnd_Cis_Tiaa_Doi,"ERROR",
        		pnd_Err_Message, new AlphanumericLength (20));
        if (Global.isEscape()) return;
    }
    private void sub_Store_Bene() throws Exception                                                                                                                        //Natural: STORE-BENE
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, "=",pnd_Bene_Cnt_Prmry,"=",pnd_Bene_Cnt_Cntgnt,"FROM STORE");                                                                               //Natural: WRITE '=' #BENE-CNT-PRMRY '=' #BENE-CNT-CNTGNT 'FROM STORE'
        if (Global.isEscape()) return;
        if (condition(pnd_Bene_Cnt_Prmry.greater(getZero()) || pnd_Bene_Cnt_Cntgnt.greater(getZero())))                                                                   //Natural: IF #BENE-CNT-PRMRY GT 0 OR #BENE-CNT-CNTGNT GT 0
        {
            getReports().write(0, "=",NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Rcrd_Type_Cde(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr(),    //Natural: WRITE '=' /'=' CIS-RCRD-TYPE-CDE /'=' CIS-BENE-TIAA-NBR /'=' CIS-BENE-CREF-NBR /'=' CIS-BENE-RQST-ID /'=' CIS-BENE-UNIQUE-ID-NBR /'=' CIS-BENE-ANNT-SSN /'=' CIS-BENE-ANNT-PRFX /'=' CIS-BENE-ANNT-FRST-NME /'=' CIS-BENE-ANNT-MID-NME /'=' CIS-BENE-ANNT-LST-NME /'=' CIS-BENE-ANNT-SFFX /'=' CIS-BENE-RQST-ID-KEY /'=' CIS-BENE-STD-FREE /'=' CIS-BENE-ESTATE /'=' CIS-BENE-TRUST /'=' CIS-BENE-CATEGORY /'=' CIS-PRMRY-BENE-CHILD-PRVSN /'=' CIS-CNTGNT-BENE-CHILD-PRVSN / '=' CIS-PRMRY-BENE-NAME ( 1:2 )
                NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Cref_Nbr(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Rqst_Id(),NEWLINE,"=",
                ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Unique_Id_Nbr(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Ssn(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Prfx(),
                NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Frst_Nme(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Mid_Nme(),
                NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Lst_Nme(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Sffx(),NEWLINE,
                "=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Rqst_Id_Key(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Std_Free(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Estate(),
                NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Trust(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Category(),NEWLINE,"=",
                ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn(),NEWLINE,
                "=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Name().getValue(1,":",2));
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Extndd_Nme().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn().getValue(1, //Natural: WRITE / '=' CIS-PRMRY-BENE-EXTNDD-NME ( 1:2 ) / '=' CIS-PRMRY-BENE-RLTN ( 1:2 ) / '=' CIS-PRMRY-BENE-RLTN-CDE ( 1:2 ) / '=' CIS-PRMRY-BENE-SSN-NBR ( 1:2 ) / '=' CIS-PRMRY-BENE-DOB ( 1:2 ) / '=' CIS-PRMRY-BENE-ALLCTN-PCT ( 1:2 ) / '=' CIS-PRMRY-BENE-NMRTR-NBR ( 1:2 ) / '=' CIS-PRMRY-BENE-DNMNTR-NBR ( 1:2 ) / '=' CIS-CNTGNT-BENE-NAME ( 1:2 ) / '=' CIS-CNTGNT-BENE-EXTNDD-NME ( 1:2 ) / '=' CIS-CNTGNT-BENE-RLTN ( 1:2 ) / '=' CIS-CNTGNT-BENE-RLTN-CDE ( 1:2 ) / '=' CIS-CNTGNT-BENE-SSN-NBR ( 1:2 ) / '=' CIS-CNTGNT-BENE-DOB ( 1:2 ) / '=' CIS-CNTGNT-BENE-ALLCTN-PCT ( 1:2 ) / '=' CIS-CNTGNT-BENE-NMRTR-NBR ( 1:2 ) / '=' CIS-CNTGNT-BENE-DNMNTR-NBR ( 1:2 )
                ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr().getValue(1,
                ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Dob().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Allctn_Pct().getValue(1,
                ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(1,
                ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Name().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Extndd_Nme().getValue(1,
                ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde().getValue(1,
                ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dob().getValue(1,
                ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Allctn_Pct().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(1,
                ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(1,":",2));
            if (Global.isEscape()) return;
            ldaCisl2020.getVw_cis_Bene_File_01().insertDBRow();                                                                                                           //Natural: STORE CIS-BENE-FILE-01
            //*  09/2011 B.HOLLOWAY START - BACKOUT IF TESTING
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            //*  09/2011 B.HOLLOWAY END
            ldaCisl2020.getVw_cis_Bene_File_01().reset();                                                                                                                 //Natural: RESET CIS-BENE-FILE-01
            pnd_Bene_Cnt_Prmry.reset();                                                                                                                                   //Natural: RESET #BENE-CNT-PRMRY #BENE-CNT-CNTGNT
            pnd_Bene_Cnt_Cntgnt.reset();
            getReports().write(0, "STORE DONE ***************");                                                                                                          //Natural: WRITE 'STORE DONE ***************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Load_Uppm_Table() throws Exception                                                                                                                   //Natural: LOAD-UPPM-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        GET1:                                                                                                                                                             //Natural: GET CIS-PRTCPNT-FILE *ISN ( ##L3680. )
        ldaCisl200.getVw_cis_Prtcpnt_File().readByID(ldaCisl200.getVw_cis_Prtcpnt_File().getAstISN("PND_PND_L3680"), "GET1");
        pnd_Int_Rate.reset();                                                                                                                                             //Natural: RESET #INT-RATE
        pnd_Uppm.nadd(1);                                                                                                                                                 //Natural: ADD 1 TO #UPPM
        pnd_Int_Rate.setValue(pnd_Input_Part_Pnd_Intrest_Pymt_Ia);                                                                                                        //Natural: ASSIGN #INT-RATE := #INTREST-PYMT-IA
        ldaCisl200.getCis_Prtcpnt_File_Cis_Comut_Pymt_Method_2().getValue(pnd_Uppm).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Int_Rate_Pnd_Int_Rate_A2,  //Natural: COMPRESS #INT-RATE-A2 '.' #INT-RATE-B2 INTO CIS-COMUT-PYMT-METHOD-2 ( #UPPM ) LEAVING NO SPACE
            ".", pnd_Int_Rate_Pnd_Int_Rate_B2));
        ldaCisl200.getCis_Prtcpnt_File_Cis_Comut_Grnted_Amt_2().getValue(pnd_Uppm).setValue(pnd_Input_Part_Pnd_Payment_N);                                                //Natural: ASSIGN CIS-COMUT-GRNTED-AMT-2 ( #UPPM ) := #PAYMENT-N
        ldaCisl200.getCis_Prtcpnt_File_Cis_Comut_Int_Rate_2().getValue(pnd_Uppm).setValue(pnd_Input_Part_Pnd_Intrest_Pymt_In_Rate_N);                                     //Natural: ASSIGN CIS-COMUT-INT-RATE-2 ( #UPPM ) := #INTREST-PYMT-IN-RATE-N
        ldaCisl200.getCis_Prtcpnt_File_Cis_Comut_Mortality_Basis().getValue(pnd_Uppm).setValue(pnd_Input_Part_Pnd_Table_Discription);                                     //Natural: ASSIGN CIS-COMUT-MORTALITY-BASIS ( #UPPM ) := #TABLE-DISCRIPTION
        ldaCisl200.getVw_cis_Prtcpnt_File().updateDBRow("GET1");                                                                                                          //Natural: UPDATE ( GET1. )
        //*  09/2011 B.HOLLOWAY START - BACKOUT IF TESTING
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  09/2011 B.HOLLOWAY END
        getReports().write(0, "=",NEWLINE,"=",ldaCisl200.getCis_Prtcpnt_File_Cis_Comut_Pymt_Method_2().getValue(1,":",20),NEWLINE,"=",ldaCisl200.getCis_Prtcpnt_File_Cis_Comut_Grnted_Amt_2().getValue(1, //Natural: WRITE '=' / '=' CIS-COMUT-PYMT-METHOD-2 ( 1:20 ) / '=' CIS-COMUT-GRNTED-AMT-2 ( 1:20 ) / '=' CIS-COMUT-INT-RATE-2 ( 1:20 ) / '=' CIS-COMUT-MORTALITY-BASIS ( 1:20 )
            ":",20),NEWLINE,"=",ldaCisl200.getCis_Prtcpnt_File_Cis_Comut_Int_Rate_2().getValue(1,":",20),NEWLINE,"=",ldaCisl200.getCis_Prtcpnt_File_Cis_Comut_Mortality_Basis().getValue(1,
            ":",20));
        if (Global.isEscape()) return;
    }
    private void sub_Update_Mit() throws Exception                                                                                                                        //Natural: UPDATE-MIT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //*  HK
        pdaCisa4000.getCisa4000().reset();                                                                                                                                //Natural: RESET CISA4000
        pdaCisa4000.getCisa4000_Mit_Tpsys().setValue("BATCHCIS");                                                                                                         //Natural: ASSIGN CISA4000.MIT-TPSYS := 'BATCHCIS'
        pdaCisa4000.getCisa4000_Mit_Tpsys().setValue(Global.getTPSYS());                                                                                                  //Natural: ASSIGN CISA4000.MIT-TPSYS := *TPSYS
        pdaCisa4000.getCisa4000_Cis_Mit_Requestor().setValue("IPRO");                                                                                                     //Natural: ASSIGN CISA4000.CIS-MIT-REQUESTOR := 'IPRO'
        pdaCisa4000.getCisa4000_Mit_Requestor().setValue("CIS");                                                                                                          //Natural: ASSIGN CISA4000.MIT-REQUESTOR := 'CIS'
        pdaCisa4000.getCisa4000_Mit_Init_Id().setValue("CIS");                                                                                                            //Natural: ASSIGN CISA4000.MIT-INIT-ID := 'CIS'
        pdaCisa4000.getCisa4000_Mit_Init_User().setValue("CIS");                                                                                                          //Natural: ASSIGN CISA4000.MIT-INIT-USER := 'CIS'
        pdaCisa4000.getCisa4000_Mit_Datn().setValue(Global.getDATN());                                                                                                    //Natural: ASSIGN CISA4000.MIT-DATN := *DATN
        pdaCisa4000.getCisa4000_Mit_Pin().setValue(ldaCisl200.getCis_Prtcpnt_File_Cis_Pin_Nbr());                                                                         //Natural: ASSIGN CISA4000.MIT-PIN := CIS-PIN-NBR
        pdaCisa4000.getCisa4000_Mit_Entry_User_Id().setValue("CIS");                                                                                                      //Natural: ASSIGN CISA4000.MIT-ENTRY-USER-ID := 'CIS'
        pdaCisa4000.getCisa4000_Mit_Contract_Type().setValue(ldaCisl200.getCis_Prtcpnt_File_Cis_Cntrct_Type());                                                           //Natural: ASSIGN CISA4000.MIT-CONTRACT-TYPE := CIS-CNTRCT-TYPE
        pdaCisa4000.getCisa4000_Mit_Existing_Rqst_Log_Unit().setValue("BCUPR");                                                                                           //Natural: ASSIGN CISA4000.MIT-EXISTING-RQST-LOG-UNIT := 'BCUPR'
        pnd_Mit_Effective_Dt_N_Pnd_Mit_Effective_Dt.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Annty_Start_Dte(),new ReportEditMask("YYYYMMDD"));                  //Natural: MOVE EDITED CIS-ANNTY-START-DTE ( EM = YYYYMMDD ) TO #MIT-EFFECTIVE-DT
        pdaCisa4000.getCisa4000_Mit_Cntct_Mode().getValue(1).setValue("C");                                                                                               //Natural: ASSIGN MIT-CNTCT-MODE ( 1 ) := 'C'
        pdaCisa4000.getCisa4000_Mit_Effctv_Dte().getValue(1).setValue(pnd_Mit_Effective_Dt_N);                                                                            //Natural: ASSIGN CISA4000.MIT-EFFCTV-DTE ( 1 ) := #MIT-EFFECTIVE-DT-N
        pdaCisa4000.getCisa4000_Mit_Existing_Rqst_Log_Unit().setValue("BCUPR");                                                                                           //Natural: ASSIGN CISA4000.MIT-EXISTING-RQST-LOG-UNIT := 'BCUPR'
        //*  ADD A REQUEST
        pnd_Mit_Effective_Dt_N_Pnd_Mit_Effective_Dt.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Annty_Start_Dte(),new ReportEditMask("YYYYMMDD"));                  //Natural: MOVE EDITED CIS-ANNTY-START-DTE ( EM = YYYYMMDD ) TO #MIT-EFFECTIVE-DT
        pdaCisa4000.getCisa4000_Mit_Cntct_Mode().getValue(1).setValue("C");                                                                                               //Natural: ASSIGN MIT-CNTCT-MODE ( 1 ) := 'C'
        pdaCisa4000.getCisa4000_Mit_Effctv_Dte().getValue(1).setValue(pnd_Mit_Effective_Dt_N);                                                                            //Natural: ASSIGN CISA4000.MIT-EFFCTV-DTE ( 1 ) := #MIT-EFFECTIVE-DT-N
        pdaCisa4000.getCisa4000_Mit_Rcvd_Dte().getValue(1).setValue(pnd_Mit_Effective_Dt_N);                                                                              //Natural: ASSIGN CISA4000.MIT-RCVD-DTE ( 1 ) := #MIT-EFFECTIVE-DT-N
        pdaCisa4000.getCisa4000_Mit_Rcvd_Dte().getValue(1).setValue(Global.getDATN());                                                                                    //Natural: ASSIGN CISA4000.MIT-RCVD-DTE ( 1 ) := *DATN
        pdaCisa4000.getCisa4000_Mit_Tiaa_Nbr().setValue(ldaCisl200.getCis_Prtcpnt_File_Cis_Tiaa_Nbr());                                                                   //Natural: ASSIGN CISA4000.MIT-TIAA-NBR := CIS-TIAA-NBR
        pdaCisa4000.getCisa4000_Mit_Function().getValue(1).setValue("02");                                                                                                //Natural: ASSIGN CISA4000.MIT-FUNCTION ( 1 ) := '02'
        pdaCisa4000.getCisa4000_Mit_Action().getValue(1).setValue("AR");                                                                                                  //Natural: ASSIGN CISA4000.MIT-ACTION ( 1 ) := 'AR'
        //*  HK
        if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Pull_Code().equals("SPEC")))                                                                                     //Natural: IF CIS-PULL-CODE = 'SPEC'
        {
            pdaCisa4000.getCisa4000_Mit_Stts_Cde().getValue(1).setValue("I3");                                                                                            //Natural: ASSIGN CISA4000.MIT-STTS-CDE ( 1 ) := 'I3'
            //*  HK
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCisa4000.getCisa4000_Mit_Stts_Cde().getValue(1).setValue("I6");                                                                                            //Natural: ASSIGN CISA4000.MIT-STTS-CDE ( 1 ) := 'I6'
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Cisn4000.class , getCurrentProcessState(), pdaCisa4000.getCisa4000());                                                                            //Natural: CALLNAT 'CISN4000' CISA4000
        if (condition(Global.isEscape())) return;
        getReports().write(0, "=",pdaCisa4000.getCisa4000_Mit_Msg(),NEWLINE,"=",pdaCisa4000.getCisa4000_Mit_Msg_Nbr());                                                   //Natural: WRITE '=' MIT-MSG /'=' MIT-MSG-NBR
        if (Global.isEscape()) return;
    }
    private void sub_Reformat_Address() throws Exception                                                                                                                  //Natural: REFORMAT-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,1).setValue(pnd_Hold_Address_Pnd_Hdest_Addr1);                                            //Natural: ASSIGN CIS-ADDRESS-TXT ( #ADDR-INDX,1 ) := #HDEST-ADDR1
        ldaCisl200.getCis_Prtcpnt_File_Cis_Zip_Code().getValue(pnd_Addr_Indx).setValue(pnd_Hold_Address_Pnd_Hzip);                                                        //Natural: ASSIGN CIS-ZIP-CODE ( #ADDR-INDX ) := #HZIP
        if (condition(pnd_Hold_Address_Pnd_Hzip.equals(pnd_Foreign_Or_Canada.getValue("*"))))                                                                             //Natural: IF #HZIP = #FOREIGN-OR-CANADA ( * )
        {
            ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,2).setValue(pnd_Hold_Address_Pnd_Hdest_Addr2);                                        //Natural: ASSIGN CIS-ADDRESS-TXT ( #ADDR-INDX,2 ) := #HDEST-ADDR2
            ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,3).setValue(pnd_Hold_Address_Pnd_Hdest_Addr3);                                        //Natural: ASSIGN CIS-ADDRESS-TXT ( #ADDR-INDX,3 ) := #HDEST-ADDR3
            ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,4).setValue(DbsUtil.compress(pnd_Hold_Address_Pnd_Hdest_City, " ",                    //Natural: COMPRESS #HDEST-CITY ' ' #HDEST-STATE INTO CIS-ADDRESS-TXT ( #ADDR-INDX,4 )
                pnd_Hold_Address_Pnd_Hdest_State));
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Hold_Address_Pnd_Hdest_Addr2.equals(" ")))                                                                                                      //Natural: IF #HDEST-ADDR2 = ' '
        {
            ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,2).setValue(DbsUtil.compress(pnd_Hold_Address_Pnd_Hdest_City, " ",                    //Natural: COMPRESS #HDEST-CITY ' ' #HDEST-STATE INTO CIS-ADDRESS-TXT ( #ADDR-INDX,2 )
                pnd_Hold_Address_Pnd_Hdest_State));
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,2).setValue(pnd_Hold_Address_Pnd_Hdest_Addr2);                                            //Natural: ASSIGN CIS-ADDRESS-TXT ( #ADDR-INDX,2 ) := #HDEST-ADDR2
        if (condition(pnd_Hold_Address_Pnd_Hdest_Addr3.equals(" ")))                                                                                                      //Natural: IF #HDEST-ADDR3 = ' '
        {
            ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,3).setValue(DbsUtil.compress(pnd_Hold_Address_Pnd_Hdest_City, " ",                    //Natural: COMPRESS #HDEST-CITY ' ' #HDEST-STATE INTO CIS-ADDRESS-TXT ( #ADDR-INDX,3 )
                pnd_Hold_Address_Pnd_Hdest_State));
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,3).setValue(pnd_Hold_Address_Pnd_Hdest_Addr3);                                            //Natural: ASSIGN CIS-ADDRESS-TXT ( #ADDR-INDX,3 ) := #HDEST-ADDR3
        ldaCisl200.getCis_Prtcpnt_File_Cis_Address_Txt().getValue(pnd_Addr_Indx,4).setValue(DbsUtil.compress(pnd_Hold_Address_Pnd_Hdest_City, " ", pnd_Hold_Address_Pnd_Hdest_State)); //Natural: COMPRESS #HDEST-CITY ' ' #HDEST-STATE INTO CIS-ADDRESS-TXT ( #ADDR-INDX,4 )
    }
    //*  CNTRSTRTGY
    private void sub_Update_Acis_Reprint_Contract() throws Exception                                                                                                      //Natural: UPDATE-ACIS-REPRINT-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************************
        vw_acis_Reprint_Fl.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) ACIS-REPRINT-FL WITH RP-TIAA-CONTR = CIS-DA-TIAA-NBR ( 1 )
        (
        "L_FIND_REP",
        new Wc[] { new Wc("RP_TIAA_CONTR", "=", ldaCisl200.getCis_Prtcpnt_File_Cis_Da_Tiaa_Nbr().getValue(1), WcType.WITH) },
        1
        );
        L_FIND_REP:
        while (condition(vw_acis_Reprint_Fl.readNextRow("L_FIND_REP", true)))
        {
            vw_acis_Reprint_Fl.setIfNotFoundControlFlag(false);
            if (condition(vw_acis_Reprint_Fl.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "ACIS REPRINT RECORD NOT FOUND FOR CONTRACT:",ldaCisl200.getCis_Prtcpnt_File_Cis_Da_Tiaa_Nbr().getValue(1));                        //Natural: WRITE 'ACIS REPRINT RECORD NOT FOUND FOR CONTRACT:' CIS-DA-TIAA-NBR ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("L_FIND_REP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("L_FIND_REP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Max_Count.setValue(acis_Reprint_Fl_Count_Castrp_Related_Contract_Info);                                                                                   //Natural: ASSIGN #MAX-COUNT := C*RP-RELATED-CONTRACT-INFO
            //*  IPRO
            pnd_Max_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #MAX-COUNT
            acis_Reprint_Fl_Rp_Related_Contract_Type.getValue(pnd_Max_Count).setValue("I");                                                                               //Natural: ASSIGN RP-RELATED-CONTRACT-TYPE ( #MAX-COUNT ) := 'I'
            acis_Reprint_Fl_Rp_Related_Tiaa_No.getValue(pnd_Max_Count).setValue(ldaCisl200.getCis_Prtcpnt_File_Cis_Tiaa_Nbr());                                           //Natural: ASSIGN RP-RELATED-TIAA-NO ( #MAX-COUNT ) := CIS-TIAA-NBR
            acis_Reprint_Fl_Rp_Related_Tiaa_Issue_Date.getValue(pnd_Max_Count).compute(new ComputeParameters(false, acis_Reprint_Fl_Rp_Related_Tiaa_Issue_Date.getValue(pnd_Max_Count)),  //Natural: ASSIGN RP-RELATED-TIAA-ISSUE-DATE ( #MAX-COUNT ) := VAL ( #CIS-TIAA-DOI )
                pnd_Input_Part_Pnd_Cis_Tiaa_Doi.val());
            acis_Reprint_Fl_Rp_Related_First_Payment_Date.getValue(pnd_Max_Count).compute(new ComputeParameters(false, acis_Reprint_Fl_Rp_Related_First_Payment_Date.getValue(pnd_Max_Count)),  //Natural: ASSIGN RP-RELATED-FIRST-PAYMENT-DATE ( #MAX-COUNT ) := VAL ( #CIS-ANNTY-START-DTE )
                pnd_Input_Part_Pnd_Cis_Annty_Start_Dte.val());
            acis_Reprint_Fl_Rp_Related_Tiaa_Total_Amt.getValue(pnd_Max_Count).setValue(ldaCisl200.getCis_Prtcpnt_File_Cis_Da_Tiaa_Proceeds_Amt().getValue(1));            //Natural: ASSIGN RP-RELATED-TIAA-TOTAL-AMT ( #MAX-COUNT ) := CIS-DA-TIAA-PROCEEDS-AMT ( 1 )
            acis_Reprint_Fl_Rp_Related_Last_Payment_Date.getValue(pnd_Max_Count).compute(new ComputeParameters(false, acis_Reprint_Fl_Rp_Related_Last_Payment_Date.getValue(pnd_Max_Count)),  //Natural: ASSIGN RP-RELATED-LAST-PAYMENT-DATE ( #MAX-COUNT ) := VAL ( #CIS-ANNTY-END-DTE )
                pnd_Input_Part_Pnd_Cis_Annty_End_Dte.val());
            acis_Reprint_Fl_Rp_Related_Payment_Frequency.getValue(pnd_Max_Count).setValue(ldaCisl200.getCis_Prtcpnt_File_Cis_Pymnt_Mode());                               //Natural: ASSIGN RP-RELATED-PAYMENT-FREQUENCY ( #MAX-COUNT ) := CIS-PYMNT-MODE
            acis_Reprint_Fl_Rp_Related_Tiaa_Issue_State.getValue(pnd_Max_Count).setValue(ldaCisl200.getCis_Prtcpnt_File_Cis_Orig_Issue_State());                          //Natural: ASSIGN RP-RELATED-TIAA-ISSUE-STATE ( #MAX-COUNT ) := CIS-ORIG-ISSUE-STATE
            vw_acis_Reprint_Fl.updateDBRow("L_FIND_REP");                                                                                                                 //Natural: UPDATE ( L-FIND-REP. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(100),"Page No :",new TabSetting(110),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) 001T *PROGRAM 100T 'Page No :' 110T *PAGE-NUMBER ( 1 ) ( AD = OD CD = NE ZP = ON ) / 001T *DATU '-' *TIMX ( EM = HH:IIAP )
                        new FieldAttributes ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,new TabSetting(1),Global.getDATU(),"-",Global.getTIMX(), 
                        new ReportEditMask ("HH:IIAP"));
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(42),"INTERNAL ROLLOVER IPRO");                                                              //Natural: WRITE ( 1 ) NOTITLE 42T 'INTERNAL ROLLOVER IPRO'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(100),"Page No :",new TabSetting(110),getReports().getPageNumberDbs(2),  //Natural: WRITE ( 2 ) 001T *PROGRAM 100T 'Page No :' 110T *PAGE-NUMBER ( 2 ) ( AD = OD CD = NE ZP = ON ) / 001T *DATU '-' *TIMX ( EM = HH:IIAP )
                        new FieldAttributes ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,new TabSetting(1),Global.getDATU(),"-",Global.getTIMX(), 
                        new ReportEditMask ("HH:IIAP"));
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(42),"DUPLICATE CONTRACTS ON CIS");                                                          //Natural: WRITE ( 2 ) NOTITLE 42T 'DUPLICATE CONTRACTS ON CIS'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(100),"Page No :",new TabSetting(110),getReports().getPageNumberDbs(3),  //Natural: WRITE ( 3 ) 001T *PROGRAM 100T 'Page No :' 110T *PAGE-NUMBER ( 3 ) ( AD = OD CD = NE ZP = ON ) / 001T *DATU '-' *TIMX ( EM = HH:IIAP )
                        new FieldAttributes ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,new TabSetting(1),Global.getDATU(),"-",Global.getTIMX(), 
                        new ReportEditMask ("HH:IIAP"));
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(42),"PARTICIPANT ADD ON CIS");                                                              //Natural: WRITE ( 3 ) NOTITLE 42T 'PARTICIPANT ADD ON CIS'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(100),"Page No :",new TabSetting(110),getReports().getPageNumberDbs(4),  //Natural: WRITE ( 4 ) 001T *PROGRAM 100T 'Page No :' 110T *PAGE-NUMBER ( 4 ) ( AD = OD CD = NE ZP = ON ) / 001T *DATU '-' *TIMX ( EM = HH:IIAP )
                        new FieldAttributes ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,new TabSetting(1),Global.getDATU(),"-",Global.getTIMX(), 
                        new ReportEditMask ("HH:IIAP"));
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(42),"ERROR KEY OMNI INFOR MISSING");                                                        //Natural: WRITE ( 4 ) NOTITLE 42T 'ERROR KEY OMNI INFOR MISSING'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //*  CREA
        //*  CREA
        //*  CREA
        //*  CREA
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"ERROR:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"LINE:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'ERROR:' 25T *ERROR-NR / 'LINE:' 25T *ERROR-LINE // 'RQST-ID:' 25T #CIS-RQST-ID-KEY
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"RQST-ID:",new TabSetting(25),pnd_Input_Part_Pnd_Cis_Rqst_Id_Key);
        //*  CREA
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
        Global.format(3, "PS=60 LS=132");
        Global.format(4, "PS=60 LS=132");

        getReports().setDisplayColumns(2, "Plan Id",
        		pnd_Input_Part_Pnd_Plan_Id,"Sub Plan",
        		pnd_Input_Part_Pnd_Sub_Plan,"IPRO Cntrct",
        		pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct,"PIN Nbr",
        		pnd_Input_Part_Pnd_Cis_Pin_Nbr,"SSN",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn,"Last Name",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme,"TDOI",
        		pnd_Input_Part_Pnd_Cis_Tiaa_Doi,"Annty OPT",
        		pnd_Input_Part_Pnd_Cis_Annty_Option);
        getReports().setDisplayColumns(3, "Plan Id",
        		pnd_Input_Part_Pnd_Plan_Id,"Sub Plan",
        		pnd_Input_Part_Pnd_Sub_Plan,"PIN Nbr",
        		pnd_Input_Part_Pnd_Cis_Pin_Nbr,"SSN",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn,"Last Name",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme, new AlphanumericLength (20),"TDOI",
        		pnd_Input_Part_Pnd_Cis_Tiaa_Doi,"IPRO Cntrct",
        		pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct,"Annty OPT",
        		pnd_Input_Part_Pnd_Cis_Annty_Option);
        getReports().setDisplayColumns(4, "Plan Id",
        		pnd_Input_Part_Pnd_Plan_Id,"Sub Plan",
        		pnd_Input_Part_Pnd_Sub_Plan,"PIN Nbr",
        		pnd_Input_Part_Pnd_Cis_Pin_Nbr,"SSN",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Ssn,"IPRO Cntrct",
        		pnd_Input_Part_Pnd_Cis_Tpa_Tiaa_Cntrct,"Last Name",
        		pnd_Input_Part_Pnd_Cis_Frst_Annt_Lst_Nme, new AlphanumericLength (10),"TDOI",
        		pnd_Input_Part_Pnd_Cis_Tiaa_Doi,"ERROR",
        		pnd_Err_Message, new AlphanumericLength (20));
    }
}
