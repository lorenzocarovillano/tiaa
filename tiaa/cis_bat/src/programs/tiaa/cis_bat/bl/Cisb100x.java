/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:21:03 PM
**        * FROM NATURAL PROGRAM : Cisb100x
************************************************************
**        * FILE NAME            : Cisb100x.java
**        * CLASS NAME           : Cisb100x
**        * INSTANCE NAME        : Cisb100x
************************************************************
************************************************************************
* PROGRAM  : CISB100X
* SYSTEM   : CIS
*
* MOD DATE   MOD BY   DESCRIPTION OF CHANGES
* ---------  -------- --------------------------------------------------
*
* OCT 04 06  TINIO    CLONED CISB100 TO SEPARATE LEGACY ISSUES FROM OMNI
*                     FOR REPORTS 1, 3, AND 8.
* 03/23/09 K GATES    CHANGED AS PART OF IA TIAA ACCESS.  SEE TACC KG
* 08/26/2015 - BUDDY NEWSOM - INSTITUTIONAL PREMIUM FILE SUNSET (IPFS)
* REMOVE VIEW INSIDE THE PROGRAM AS IT IS NOT BEING USED OR REFERENCED.
* REPLACE IT WITH A CALL TO NECN4000 TO RETREVE THE BUSINESS DATE.
*
* 05/09/2017 -(GHOSABE)     - PIN EXPANSION CHANGES.(C420007)  PINE.
*
************************************************************************
*

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb100x extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cis;
    private DbsField cis_Cis_Pin_Nbr;
    private DbsField cis_Cis_Rqst_Id;
    private DbsField cis_Cis_Opn_Clsd_Ind;
    private DbsField cis_Cis_Status_Cd;
    private DbsField cis_Cis_Rqst_Id_Key;
    private DbsField cis_Cis_Cntrct_Apprvl_Ind;
    private DbsField cis_Cis_Tiaa_Nbr;
    private DbsField cis_Cis_Cert_Nbr;
    private DbsField cis_Cis_Tiaa_Doi;
    private DbsField cis_Cis_Cref_Doi;
    private DbsField cis_Cis_Annty_Option;
    private DbsField cis_Cis_Cntrct_Print_Dte;
    private DbsField cis_Cis_Cntrct_Type;
    private DbsField cis_Cis_Pymnt_Mode;
    private DbsField cis_Cis_Grnted_Grd_Amt;
    private DbsField cis_Cis_Grnted_Std_Amt;

    private DbsGroup cis_Cis_Da_Tiaa_Cntrcts_Rqst;
    private DbsField cis_Cis_Da_Tiaa_Nbr;
    private DbsField cis_Cis_Da_Tiaa_Proceeds_Amt;
    private DbsField cis_Cis_Da_Rea_Proceeds_Amt;

    private DbsGroup cis_Cis_Da_Cref_Cntrcts_Rqst;
    private DbsField cis_Cis_Da_Cref_Proceeds_Amt;

    private DbsGroup cis_Cis_Cref_Annty_Pymnt;
    private DbsField cis_Cis_Cref_Acct_Cde;
    private DbsField cis_Cis_Cref_Mnthly_Nbr_Units;
    private DbsField cis_Cis_Cref_Annual_Nbr_Units;
    private DbsField cis_Cis_Rea_Mnthly_Nbr_Units;
    private DbsField cis_Cis_Rea_Annual_Nbr_Units;
    private DbsField cis_Cis_Rea_Annty_Amt;
    private DbsField cis_Cis_Mdo_Contract_Cash_Status;
    private DbsField cis_Cis_Mdo_Contract_Type;
    private DbsField cis_Cis_Mdo_Traditional_Amt;
    private DbsField cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt;
    private DbsField cis_Cis_Mdo_Cref_Int_Pymnt_Amt;
    private DbsField cis_Cis_Mdo_Tiaa_Excluded_Amt;
    private DbsField cis_Cis_Mdo_Cref_Excluded_Amt;
    private DbsField cis_Cis_Rtb_Amt;
    private DbsField cis_Cis_Trnsf_Flag;
    private DbsField cis_Cis_Corp_Sync_Ind;
    private DbsField cis_Cis_Tacc_Annty_Amt;

    private DbsGroup cis_Cis_Tacc_Fund_Info;
    private DbsField cis_Cis_Tacc_Ind;
    private DbsField cis_Cis_Tacc_Account;
    private DbsField cis_Cis_Tacc_Mnthly_Nbr_Units;
    private DbsField cis_Cis_Tacc_Annual_Nbr_Units;

    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField pnd_Trans_Cntrct_Key;

    private DbsGroup pnd_Trans_Cntrct_Key__R_Field_1;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte;
    private DbsField pnd_Trans_Cntrct_Key_Pnd_Trans_Cde;
    private DbsField pnd_Omni;
    private DbsField pnd_P;
    private DbsField pnd_R;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_I;
    private DbsField pnd_Grnted_Grd_Amt;
    private DbsField pnd_Grnted_Grd_Amt_Tpa;
    private DbsField pnd_Grnted_Grd_Amt_Ipro;
    private DbsField pnd_Grnted_Std_Amt;
    private DbsField pnd_Grnted_Std_Amt_Tpa;
    private DbsField pnd_Grnted_Std_Amt_Ipro;
    private DbsField pnd_Grnted_Grd_Amt_Tot;
    private DbsField pnd_Grnted_Std_Amt_Tot;
    private DbsField pnd_M_Tiaa_Rea;
    private DbsField pnd_M_Mma;
    private DbsField pnd_M_Stock;
    private DbsField pnd_M_Social;
    private DbsField pnd_M_Global;
    private DbsField pnd_M_Bond;
    private DbsField pnd_M_Grth;
    private DbsField pnd_M_Equ;
    private DbsField pnd_M_Ilb;
    private DbsField pnd_M_Taccess;
    private DbsField pnd_A_Tiaa_Rea;
    private DbsField pnd_A_Mma;
    private DbsField pnd_A_Stock;
    private DbsField pnd_A_Social;
    private DbsField pnd_A_Global;
    private DbsField pnd_A_Bond;
    private DbsField pnd_A_Grth;
    private DbsField pnd_A_Equ;
    private DbsField pnd_A_Ilb;
    private DbsField pnd_A_Taccess;
    private DbsField pnd_Trad_Annuity;
    private DbsField pnd_Trad_Annuity_Tpa;
    private DbsField pnd_Trad_Annuity_Ipro;
    private DbsField pnd_Real_Annuity;
    private DbsField pnd_Cref_Annuity;
    private DbsField pnd_Tacc_Annuity;
    private DbsField pnd_Da_Tiaa_Proceeds_Amt;
    private DbsField pnd_Da_Tiaa_Proceeds_Amt_Tpa;
    private DbsField pnd_Da_Tiaa_Proceeds_Amt_Ipro;
    private DbsField pnd_Da_Rea_Proceeds_Amt;
    private DbsField pnd_Da_Cref_Proceeds_Amt;
    private DbsField pnd_Contract_Issued;
    private DbsField pnd_Contract_Issued_Tpa;
    private DbsField pnd_Contract_Issued_Ipro;
    private DbsField pnd_Certif_Issued;
    private DbsField pnd_Certif_Issued2;
    private DbsField pnd_No_Rec;
    private DbsField pnd_No_Rec_Ipro;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Print_Date;
    private DbsField pnd_A;
    private DbsField pnd_Apprvl;
    private DbsField pnd_N_Apprvl;

    private DbsGroup pnd_Mdo_Fields;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Cref_Int_Pymnt;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Cref_Excl;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Mma;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Stock;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Social;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Global;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Bond;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Grth;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Equ;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Ilb;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Units;
    private DbsField pnd_O_Grnted_Grd_Amt;
    private DbsField pnd_O_Grnted_Grd_Amt_Tpa;
    private DbsField pnd_O_Grnted_Grd_Amt_Ipro;
    private DbsField pnd_O_Grnted_Std_Amt;
    private DbsField pnd_O_Grnted_Std_Amt_Tpa;
    private DbsField pnd_O_Grnted_Std_Amt_Ipro;
    private DbsField pnd_O_Grnted_Grd_Amt_Tot;
    private DbsField pnd_O_Grnted_Std_Amt_Tot;
    private DbsField pnd_O_M_Tiaa_Rea;
    private DbsField pnd_O_M_Mma;
    private DbsField pnd_O_M_Stock;
    private DbsField pnd_O_M_Social;
    private DbsField pnd_O_M_Global;
    private DbsField pnd_O_M_Bond;
    private DbsField pnd_O_M_Grth;
    private DbsField pnd_O_M_Equ;
    private DbsField pnd_O_M_Ilb;
    private DbsField pnd_O_M_Taccess;
    private DbsField pnd_O_A_Tiaa_Rea;
    private DbsField pnd_O_A_Mma;
    private DbsField pnd_O_A_Stock;
    private DbsField pnd_O_A_Social;
    private DbsField pnd_O_A_Global;
    private DbsField pnd_O_A_Bond;
    private DbsField pnd_O_A_Grth;
    private DbsField pnd_O_A_Equ;
    private DbsField pnd_O_A_Ilb;
    private DbsField pnd_O_A_Taccess;
    private DbsField pnd_O_Trad_Annuity;
    private DbsField pnd_O_Trad_Annuity_Tpa;
    private DbsField pnd_O_Trad_Annuity_Ipro;
    private DbsField pnd_O_Real_Annuity;
    private DbsField pnd_O_Cref_Annuity;
    private DbsField pnd_O_Tacc_Annuity;
    private DbsField pnd_O_Da_Tiaa_Proceeds_Amt;
    private DbsField pnd_O_Da_Tiaa_Proceeds_Amt_Tpa;
    private DbsField pnd_O_Da_Tiaa_Proceeds_Amt_Ipro;
    private DbsField pnd_O_Da_Rea_Proceeds_Amt;
    private DbsField pnd_O_Da_Cref_Proceeds_Amt;
    private DbsField pnd_O_Contract_Issued;
    private DbsField pnd_O_Contract_Issued_Tpa;
    private DbsField pnd_O_Contract_Issued_Ipro;
    private DbsField pnd_O_Certif_Issued;
    private DbsField pnd_O_Certif_Issued2;
    private DbsField pnd_O_No_Rec;
    private DbsField pnd_O_Apprvl;
    private DbsField pnd_O_N_Apprvl;

    private DbsGroup pnd_O_Mdo_Fields;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Units;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Amt;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Traditional_Amt;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Int_Pymnt;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Cref_Int_Pymnt;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Excl;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Cref_Excl;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Mma;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Stock;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Social;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Global;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Bond;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Grth;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Equ;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Ilb;
    private DbsField pnd_O_Mdo_Fields_Pnd_O_Mdo_Units;
    private DbsField pnd_Mdo_Cref;
    private DbsField pnd_O_Mdo_Cref;
    private DbsField pnd_Todays_Business_Dt;

    private DbsGroup pnd_Todays_Business_Dt__R_Field_2;
    private DbsField pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A;

    private DbsGroup pnd_Todays_Business_Dt__R_Field_3;
    private DbsField pnd_Todays_Business_Dt_Pnd_Todays_Bus_Cc;
    private DbsField pnd_Todays_Business_Dt_Pnd_Todays_Bus_Yy;
    private DbsField pnd_Todays_Business_Dt_Pnd_Todays_Bus_Mm;
    private DbsField pnd_Todays_Business_Dt_Pnd_Todays_Bus_Dd;
    private DbsField pnd_Cis_Open_Status;
    private DbsField pnd_Aprl_Y_N;
    private DbsField pnd_O_Aprl_Y_N;
    private DbsField pnd_Cis_Rqst_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cis = new DataAccessProgramView(new NameInfo("vw_cis", "CIS"), "CIS_PRTCPNT_FILE_12", "CIS_PRTCPNT_FILE", DdmPeriodicGroups.getInstance().getGroups("CIS_PRTCPNT_FILE_12"));
        cis_Cis_Pin_Nbr = vw_cis.getRecord().newFieldInGroup("cis_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CIS_PIN_NBR");
        cis_Cis_Rqst_Id = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_RQST_ID");
        cis_Cis_Opn_Clsd_Ind = vw_cis.getRecord().newFieldInGroup("cis_Cis_Opn_Clsd_Ind", "CIS-OPN-CLSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_OPN_CLSD_IND");
        cis_Cis_Status_Cd = vw_cis.getRecord().newFieldInGroup("cis_Cis_Status_Cd", "CIS-STATUS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_STATUS_CD");
        cis_Cis_Rqst_Id_Key = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "CIS_RQST_ID_KEY");
        cis_Cis_Cntrct_Apprvl_Ind = vw_cis.getRecord().newFieldInGroup("cis_Cis_Cntrct_Apprvl_Ind", "CIS-CNTRCT-APPRVL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_CNTRCT_APPRVL_IND");
        cis_Cis_Tiaa_Nbr = vw_cis.getRecord().newFieldInGroup("cis_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_TIAA_NBR");
        cis_Cis_Cert_Nbr = vw_cis.getRecord().newFieldInGroup("cis_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_CERT_NBR");
        cis_Cis_Tiaa_Doi = vw_cis.getRecord().newFieldInGroup("cis_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_TIAA_DOI");
        cis_Cis_Cref_Doi = vw_cis.getRecord().newFieldInGroup("cis_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CREF_DOI");
        cis_Cis_Annty_Option = vw_cis.getRecord().newFieldInGroup("cis_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CIS_ANNTY_OPTION");
        cis_Cis_Cntrct_Print_Dte = vw_cis.getRecord().newFieldInGroup("cis_Cis_Cntrct_Print_Dte", "CIS-CNTRCT-PRINT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CIS_CNTRCT_PRINT_DTE");
        cis_Cis_Cntrct_Type = vw_cis.getRecord().newFieldInGroup("cis_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_CNTRCT_TYPE");
        cis_Cis_Pymnt_Mode = vw_cis.getRecord().newFieldInGroup("cis_Cis_Pymnt_Mode", "CIS-PYMNT-MODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_PYMNT_MODE");
        cis_Cis_Grnted_Grd_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Grnted_Grd_Amt", "CIS-GRNTED-GRD-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "CIS_GRNTED_GRD_AMT");
        cis_Cis_Grnted_Std_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Grnted_Std_Amt", "CIS-GRNTED-STD-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "CIS_GRNTED_STD_AMT");

        cis_Cis_Da_Tiaa_Cntrcts_Rqst = vw_cis.getRecord().newGroupArrayInGroup("cis_Cis_Da_Tiaa_Cntrcts_Rqst", "CIS-DA-TIAA-CNTRCTS-RQST", new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Cis_Da_Tiaa_Nbr = cis_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldInGroup("cis_Cis_Da_Tiaa_Nbr", "CIS-DA-TIAA-NBR", FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_DA_TIAA_NBR", "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Cis_Da_Tiaa_Proceeds_Amt = cis_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldInGroup("cis_Cis_Da_Tiaa_Proceeds_Amt", "CIS-DA-TIAA-PROCEEDS-AMT", FieldType.NUMERIC, 
            11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_TIAA_PROCEEDS_AMT", "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Cis_Da_Rea_Proceeds_Amt = cis_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldInGroup("cis_Cis_Da_Rea_Proceeds_Amt", "CIS-DA-REA-PROCEEDS-AMT", FieldType.NUMERIC, 
            11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_REA_PROCEEDS_AMT", "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");

        cis_Cis_Da_Cref_Cntrcts_Rqst = vw_cis.getRecord().newGroupArrayInGroup("cis_Cis_Da_Cref_Cntrcts_Rqst", "CIS-DA-CREF-CNTRCTS-RQST", new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Cis_Da_Cref_Proceeds_Amt = cis_Cis_Da_Cref_Cntrcts_Rqst.newFieldInGroup("cis_Cis_Da_Cref_Proceeds_Amt", "CIS-DA-CREF-PROCEEDS-AMT", FieldType.NUMERIC, 
            11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_CREF_PROCEEDS_AMT", "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");

        cis_Cis_Cref_Annty_Pymnt = vw_cis.getRecord().newGroupArrayInGroup("cis_Cis_Cref_Annty_Pymnt", "CIS-CREF-ANNTY-PYMNT", new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Cis_Cref_Acct_Cde = cis_Cis_Cref_Annty_Pymnt.newFieldInGroup("cis_Cis_Cref_Acct_Cde", "CIS-CREF-ACCT-CDE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_CREF_ACCT_CDE", "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Cis_Cref_Mnthly_Nbr_Units = cis_Cis_Cref_Annty_Pymnt.newFieldInGroup("cis_Cis_Cref_Mnthly_Nbr_Units", "CIS-CREF-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_MNTHLY_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Cis_Cref_Annual_Nbr_Units = cis_Cis_Cref_Annty_Pymnt.newFieldInGroup("cis_Cis_Cref_Annual_Nbr_Units", "CIS-CREF-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_ANNUAL_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Cis_Rea_Mnthly_Nbr_Units = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rea_Mnthly_Nbr_Units", "CIS-REA-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, RepeatingFieldStrategy.None, "CIS_REA_MNTHLY_NBR_UNITS");
        cis_Cis_Rea_Annual_Nbr_Units = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rea_Annual_Nbr_Units", "CIS-REA-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, RepeatingFieldStrategy.None, "CIS_REA_ANNUAL_NBR_UNITS");
        cis_Cis_Rea_Annty_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rea_Annty_Amt", "CIS-REA-ANNTY-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "CIS_REA_ANNTY_AMT");
        cis_Cis_Mdo_Contract_Cash_Status = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Contract_Cash_Status", "CIS-MDO-CONTRACT-CASH-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_MDO_CONTRACT_CASH_STATUS");
        cis_Cis_Mdo_Contract_Type = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Contract_Type", "CIS-MDO-CONTRACT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_MDO_CONTRACT_TYPE");
        cis_Cis_Mdo_Traditional_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Traditional_Amt", "CIS-MDO-TRADITIONAL-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TRADITIONAL_AMT");
        cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt", "CIS-MDO-TIAA-INT-PYMNT-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TIAA_INT_PYMNT_AMT");
        cis_Cis_Mdo_Cref_Int_Pymnt_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Cref_Int_Pymnt_Amt", "CIS-MDO-CREF-INT-PYMNT-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_MDO_CREF_INT_PYMNT_AMT");
        cis_Cis_Mdo_Tiaa_Excluded_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Tiaa_Excluded_Amt", "CIS-MDO-TIAA-EXCLUDED-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TIAA_EXCLUDED_AMT");
        cis_Cis_Mdo_Cref_Excluded_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Cref_Excluded_Amt", "CIS-MDO-CREF-EXCLUDED-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_MDO_CREF_EXCLUDED_AMT");
        cis_Cis_Rtb_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rtb_Amt", "CIS-RTB-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "CIS_RTB_AMT");
        cis_Cis_Trnsf_Flag = vw_cis.getRecord().newFieldInGroup("cis_Cis_Trnsf_Flag", "CIS-TRNSF-FLAG", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_TRNSF_FLAG");
        cis_Cis_Corp_Sync_Ind = vw_cis.getRecord().newFieldInGroup("cis_Cis_Corp_Sync_Ind", "CIS-CORP-SYNC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_CORP_SYNC_IND");
        cis_Cis_Tacc_Annty_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Tacc_Annty_Amt", "CIS-TACC-ANNTY-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "CIS_TACC_ANNTY_AMT");

        cis_Cis_Tacc_Fund_Info = vw_cis.getRecord().newGroupArrayInGroup("cis_Cis_Tacc_Fund_Info", "CIS-TACC-FUND-INFO", new DbsArrayController(1, 100) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Cis_Tacc_Ind = cis_Cis_Tacc_Fund_Info.newFieldInGroup("cis_Cis_Tacc_Ind", "CIS-TACC-IND", FieldType.STRING, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_TACC_IND", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Cis_Tacc_Account = cis_Cis_Tacc_Fund_Info.newFieldInGroup("cis_Cis_Tacc_Account", "CIS-TACC-ACCOUNT", FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_TACC_ACCOUNT", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Cis_Tacc_Mnthly_Nbr_Units = cis_Cis_Tacc_Fund_Info.newFieldInGroup("cis_Cis_Tacc_Mnthly_Nbr_Units", "CIS-TACC-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_MNTHLY_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Cis_Tacc_Annual_Nbr_Units = cis_Cis_Tacc_Fund_Info.newFieldInGroup("cis_Cis_Tacc_Annual_Nbr_Units", "CIS-TACC-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_ANNUAL_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        registerRecord(vw_cis);

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        registerRecord(vw_iaa_Trans_Rcrd);

        pnd_Trans_Cntrct_Key = localVariables.newFieldInRecord("pnd_Trans_Cntrct_Key", "#TRANS-CNTRCT-KEY", FieldType.STRING, 27);

        pnd_Trans_Cntrct_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Trans_Cntrct_Key__R_Field_1", "REDEFINE", pnd_Trans_Cntrct_Key);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr = pnd_Trans_Cntrct_Key__R_Field_1.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr", "#TRANS-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde = pnd_Trans_Cntrct_Key__R_Field_1.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde", "#TRANS-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte = pnd_Trans_Cntrct_Key__R_Field_1.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12);
        pnd_Trans_Cntrct_Key_Pnd_Trans_Cde = pnd_Trans_Cntrct_Key__R_Field_1.newFieldInGroup("pnd_Trans_Cntrct_Key_Pnd_Trans_Cde", "#TRANS-CDE", FieldType.NUMERIC, 
            3);
        pnd_Omni = localVariables.newFieldInRecord("pnd_Omni", "#OMNI", FieldType.BOOLEAN, 1);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.INTEGER, 2);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.INTEGER, 2);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.INTEGER, 2);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Grnted_Grd_Amt = localVariables.newFieldArrayInRecord("pnd_Grnted_Grd_Amt", "#GRNTED-GRD-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Grnted_Grd_Amt_Tpa = localVariables.newFieldArrayInRecord("pnd_Grnted_Grd_Amt_Tpa", "#GRNTED-GRD-AMT-TPA", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Grnted_Grd_Amt_Ipro = localVariables.newFieldArrayInRecord("pnd_Grnted_Grd_Amt_Ipro", "#GRNTED-GRD-AMT-IPRO", FieldType.NUMERIC, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Grnted_Std_Amt = localVariables.newFieldArrayInRecord("pnd_Grnted_Std_Amt", "#GRNTED-STD-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Grnted_Std_Amt_Tpa = localVariables.newFieldArrayInRecord("pnd_Grnted_Std_Amt_Tpa", "#GRNTED-STD-AMT-TPA", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Grnted_Std_Amt_Ipro = localVariables.newFieldArrayInRecord("pnd_Grnted_Std_Amt_Ipro", "#GRNTED-STD-AMT-IPRO", FieldType.NUMERIC, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Grnted_Grd_Amt_Tot = localVariables.newFieldInRecord("pnd_Grnted_Grd_Amt_Tot", "#GRNTED-GRD-AMT-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Grnted_Std_Amt_Tot = localVariables.newFieldInRecord("pnd_Grnted_Std_Amt_Tot", "#GRNTED-STD-AMT-TOT", FieldType.NUMERIC, 11, 2);
        pnd_M_Tiaa_Rea = localVariables.newFieldArrayInRecord("pnd_M_Tiaa_Rea", "#M-TIAA-REA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Mma = localVariables.newFieldArrayInRecord("pnd_M_Mma", "#M-MMA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Stock = localVariables.newFieldArrayInRecord("pnd_M_Stock", "#M-STOCK", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Social = localVariables.newFieldArrayInRecord("pnd_M_Social", "#M-SOCIAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Global = localVariables.newFieldArrayInRecord("pnd_M_Global", "#M-GLOBAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Bond = localVariables.newFieldArrayInRecord("pnd_M_Bond", "#M-BOND", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Grth = localVariables.newFieldArrayInRecord("pnd_M_Grth", "#M-GRTH", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Equ = localVariables.newFieldArrayInRecord("pnd_M_Equ", "#M-EQU", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Ilb = localVariables.newFieldArrayInRecord("pnd_M_Ilb", "#M-ILB", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Taccess = localVariables.newFieldArrayInRecord("pnd_M_Taccess", "#M-TACCESS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Tiaa_Rea = localVariables.newFieldArrayInRecord("pnd_A_Tiaa_Rea", "#A-TIAA-REA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Mma = localVariables.newFieldArrayInRecord("pnd_A_Mma", "#A-MMA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Stock = localVariables.newFieldArrayInRecord("pnd_A_Stock", "#A-STOCK", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Social = localVariables.newFieldArrayInRecord("pnd_A_Social", "#A-SOCIAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Global = localVariables.newFieldArrayInRecord("pnd_A_Global", "#A-GLOBAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Bond = localVariables.newFieldArrayInRecord("pnd_A_Bond", "#A-BOND", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Grth = localVariables.newFieldArrayInRecord("pnd_A_Grth", "#A-GRTH", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Equ = localVariables.newFieldArrayInRecord("pnd_A_Equ", "#A-EQU", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Ilb = localVariables.newFieldArrayInRecord("pnd_A_Ilb", "#A-ILB", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Taccess = localVariables.newFieldArrayInRecord("pnd_A_Taccess", "#A-TACCESS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_Trad_Annuity = localVariables.newFieldArrayInRecord("pnd_Trad_Annuity", "#TRAD-ANNUITY", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Trad_Annuity_Tpa = localVariables.newFieldArrayInRecord("pnd_Trad_Annuity_Tpa", "#TRAD-ANNUITY-TPA", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Trad_Annuity_Ipro = localVariables.newFieldArrayInRecord("pnd_Trad_Annuity_Ipro", "#TRAD-ANNUITY-IPRO", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Real_Annuity = localVariables.newFieldArrayInRecord("pnd_Real_Annuity", "#REAL-ANNUITY", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Cref_Annuity = localVariables.newFieldArrayInRecord("pnd_Cref_Annuity", "#CREF-ANNUITY", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Tacc_Annuity = localVariables.newFieldArrayInRecord("pnd_Tacc_Annuity", "#TACC-ANNUITY", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Da_Tiaa_Proceeds_Amt = localVariables.newFieldArrayInRecord("pnd_Da_Tiaa_Proceeds_Amt", "#DA-TIAA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, 
            new DbsArrayController(1, 5));
        pnd_Da_Tiaa_Proceeds_Amt_Tpa = localVariables.newFieldArrayInRecord("pnd_Da_Tiaa_Proceeds_Amt_Tpa", "#DA-TIAA-PROCEEDS-AMT-TPA", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 5));
        pnd_Da_Tiaa_Proceeds_Amt_Ipro = localVariables.newFieldArrayInRecord("pnd_Da_Tiaa_Proceeds_Amt_Ipro", "#DA-TIAA-PROCEEDS-AMT-IPRO", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 5));
        pnd_Da_Rea_Proceeds_Amt = localVariables.newFieldArrayInRecord("pnd_Da_Rea_Proceeds_Amt", "#DA-REA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_Da_Cref_Proceeds_Amt = localVariables.newFieldArrayInRecord("pnd_Da_Cref_Proceeds_Amt", "#DA-CREF-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, 
            new DbsArrayController(1, 5));
        pnd_Contract_Issued = localVariables.newFieldArrayInRecord("pnd_Contract_Issued", "#CONTRACT-ISSUED", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_Contract_Issued_Tpa = localVariables.newFieldArrayInRecord("pnd_Contract_Issued_Tpa", "#CONTRACT-ISSUED-TPA", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_Contract_Issued_Ipro = localVariables.newFieldArrayInRecord("pnd_Contract_Issued_Ipro", "#CONTRACT-ISSUED-IPRO", FieldType.NUMERIC, 6, new 
            DbsArrayController(1, 5));
        pnd_Certif_Issued = localVariables.newFieldArrayInRecord("pnd_Certif_Issued", "#CERTIF-ISSUED", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_Certif_Issued2 = localVariables.newFieldArrayInRecord("pnd_Certif_Issued2", "#CERTIF-ISSUED2", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_No_Rec = localVariables.newFieldArrayInRecord("pnd_No_Rec", "#NO-REC", FieldType.NUMERIC, 6, new DbsArrayController(1, 5));
        pnd_No_Rec_Ipro = localVariables.newFieldArrayInRecord("pnd_No_Rec_Ipro", "#NO-REC-IPRO", FieldType.NUMERIC, 6, new DbsArrayController(1, 5));
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.STRING, 8);
        pnd_Print_Date = localVariables.newFieldInRecord("pnd_Print_Date", "#PRINT-DATE", FieldType.STRING, 8);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.INTEGER, 2);
        pnd_Apprvl = localVariables.newFieldArrayInRecord("pnd_Apprvl", "#APPRVL", FieldType.NUMERIC, 6, new DbsArrayController(1, 2));
        pnd_N_Apprvl = localVariables.newFieldInRecord("pnd_N_Apprvl", "#N-APPRVL", FieldType.NUMERIC, 6);

        pnd_Mdo_Fields = localVariables.newGroupInRecord("pnd_Mdo_Fields", "#MDO-FIELDS");
        pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units", "#MDO-REA-ANNTY-UNITS", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt", "#MDO-REA-ANNTY-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt", "#MDO-TRADITIONAL-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt", "#MDO-TIAA-INT-PYMNT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Cref_Int_Pymnt = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Cref_Int_Pymnt", "#MDO-CREF-INT-PYMNT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl", "#MDO-TIAA-EXCL", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Cref_Excl = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Cref_Excl", "#MDO-CREF-EXCL", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Mma = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Mma", "#MDO-MMA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            2));
        pnd_Mdo_Fields_Pnd_Mdo_Stock = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Stock", "#MDO-STOCK", FieldType.NUMERIC, 11, 3, new 
            DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Social = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Social", "#MDO-SOCIAL", FieldType.NUMERIC, 11, 3, 
            new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Global = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Global", "#MDO-GLOBAL", FieldType.NUMERIC, 11, 3, 
            new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Bond = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Bond", "#MDO-BOND", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            2));
        pnd_Mdo_Fields_Pnd_Mdo_Grth = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Grth", "#MDO-GRTH", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            2));
        pnd_Mdo_Fields_Pnd_Mdo_Equ = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Equ", "#MDO-EQU", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            2));
        pnd_Mdo_Fields_Pnd_Mdo_Ilb = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Ilb", "#MDO-ILB", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            2));
        pnd_Mdo_Fields_Pnd_Mdo_Units = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Units", "#MDO-UNITS", FieldType.NUMERIC, 12, 3, new 
            DbsArrayController(1, 2));
        pnd_O_Grnted_Grd_Amt = localVariables.newFieldArrayInRecord("pnd_O_Grnted_Grd_Amt", "#O-GRNTED-GRD-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_O_Grnted_Grd_Amt_Tpa = localVariables.newFieldArrayInRecord("pnd_O_Grnted_Grd_Amt_Tpa", "#O-GRNTED-GRD-AMT-TPA", FieldType.NUMERIC, 11, 2, 
            new DbsArrayController(1, 4));
        pnd_O_Grnted_Grd_Amt_Ipro = localVariables.newFieldArrayInRecord("pnd_O_Grnted_Grd_Amt_Ipro", "#O-GRNTED-GRD-AMT-IPRO", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 4));
        pnd_O_Grnted_Std_Amt = localVariables.newFieldArrayInRecord("pnd_O_Grnted_Std_Amt", "#O-GRNTED-STD-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_O_Grnted_Std_Amt_Tpa = localVariables.newFieldArrayInRecord("pnd_O_Grnted_Std_Amt_Tpa", "#O-GRNTED-STD-AMT-TPA", FieldType.NUMERIC, 11, 2, 
            new DbsArrayController(1, 4));
        pnd_O_Grnted_Std_Amt_Ipro = localVariables.newFieldArrayInRecord("pnd_O_Grnted_Std_Amt_Ipro", "#O-GRNTED-STD-AMT-IPRO", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 4));
        pnd_O_Grnted_Grd_Amt_Tot = localVariables.newFieldInRecord("pnd_O_Grnted_Grd_Amt_Tot", "#O-GRNTED-GRD-AMT-TOT", FieldType.NUMERIC, 11, 2);
        pnd_O_Grnted_Std_Amt_Tot = localVariables.newFieldInRecord("pnd_O_Grnted_Std_Amt_Tot", "#O-GRNTED-STD-AMT-TOT", FieldType.NUMERIC, 11, 2);
        pnd_O_M_Tiaa_Rea = localVariables.newFieldArrayInRecord("pnd_O_M_Tiaa_Rea", "#O-M-TIAA-REA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_O_M_Mma = localVariables.newFieldArrayInRecord("pnd_O_M_Mma", "#O-M-MMA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_M_Stock = localVariables.newFieldArrayInRecord("pnd_O_M_Stock", "#O-M-STOCK", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_M_Social = localVariables.newFieldArrayInRecord("pnd_O_M_Social", "#O-M-SOCIAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_M_Global = localVariables.newFieldArrayInRecord("pnd_O_M_Global", "#O-M-GLOBAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_M_Bond = localVariables.newFieldArrayInRecord("pnd_O_M_Bond", "#O-M-BOND", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_M_Grth = localVariables.newFieldArrayInRecord("pnd_O_M_Grth", "#O-M-GRTH", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_M_Equ = localVariables.newFieldArrayInRecord("pnd_O_M_Equ", "#O-M-EQU", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_M_Ilb = localVariables.newFieldArrayInRecord("pnd_O_M_Ilb", "#O-M-ILB", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_M_Taccess = localVariables.newFieldArrayInRecord("pnd_O_M_Taccess", "#O-M-TACCESS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_O_A_Tiaa_Rea = localVariables.newFieldArrayInRecord("pnd_O_A_Tiaa_Rea", "#O-A-TIAA-REA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_O_A_Mma = localVariables.newFieldArrayInRecord("pnd_O_A_Mma", "#O-A-MMA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_A_Stock = localVariables.newFieldArrayInRecord("pnd_O_A_Stock", "#O-A-STOCK", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_A_Social = localVariables.newFieldArrayInRecord("pnd_O_A_Social", "#O-A-SOCIAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_A_Global = localVariables.newFieldArrayInRecord("pnd_O_A_Global", "#O-A-GLOBAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_A_Bond = localVariables.newFieldArrayInRecord("pnd_O_A_Bond", "#O-A-BOND", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_A_Grth = localVariables.newFieldArrayInRecord("pnd_O_A_Grth", "#O-A-GRTH", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_A_Equ = localVariables.newFieldArrayInRecord("pnd_O_A_Equ", "#O-A-EQU", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_A_Ilb = localVariables.newFieldArrayInRecord("pnd_O_A_Ilb", "#O-A-ILB", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_O_A_Taccess = localVariables.newFieldArrayInRecord("pnd_O_A_Taccess", "#O-A-TACCESS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_O_Trad_Annuity = localVariables.newFieldArrayInRecord("pnd_O_Trad_Annuity", "#O-TRAD-ANNUITY", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            5));
        pnd_O_Trad_Annuity_Tpa = localVariables.newFieldArrayInRecord("pnd_O_Trad_Annuity_Tpa", "#O-TRAD-ANNUITY-TPA", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            5));
        pnd_O_Trad_Annuity_Ipro = localVariables.newFieldArrayInRecord("pnd_O_Trad_Annuity_Ipro", "#O-TRAD-ANNUITY-IPRO", FieldType.NUMERIC, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_O_Real_Annuity = localVariables.newFieldArrayInRecord("pnd_O_Real_Annuity", "#O-REAL-ANNUITY", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_O_Cref_Annuity = localVariables.newFieldArrayInRecord("pnd_O_Cref_Annuity", "#O-CREF-ANNUITY", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_O_Tacc_Annuity = localVariables.newFieldArrayInRecord("pnd_O_Tacc_Annuity", "#O-TACC-ANNUITY", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_O_Da_Tiaa_Proceeds_Amt = localVariables.newFieldArrayInRecord("pnd_O_Da_Tiaa_Proceeds_Amt", "#O-DA-TIAA-PROCEEDS-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 5));
        pnd_O_Da_Tiaa_Proceeds_Amt_Tpa = localVariables.newFieldArrayInRecord("pnd_O_Da_Tiaa_Proceeds_Amt_Tpa", "#O-DA-TIAA-PROCEEDS-AMT-TPA", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 5));
        pnd_O_Da_Tiaa_Proceeds_Amt_Ipro = localVariables.newFieldArrayInRecord("pnd_O_Da_Tiaa_Proceeds_Amt_Ipro", "#O-DA-TIAA-PROCEEDS-AMT-IPRO", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 5));
        pnd_O_Da_Rea_Proceeds_Amt = localVariables.newFieldArrayInRecord("pnd_O_Da_Rea_Proceeds_Amt", "#O-DA-REA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 5));
        pnd_O_Da_Cref_Proceeds_Amt = localVariables.newFieldArrayInRecord("pnd_O_Da_Cref_Proceeds_Amt", "#O-DA-CREF-PROCEEDS-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 5));
        pnd_O_Contract_Issued = localVariables.newFieldArrayInRecord("pnd_O_Contract_Issued", "#O-CONTRACT-ISSUED", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_O_Contract_Issued_Tpa = localVariables.newFieldArrayInRecord("pnd_O_Contract_Issued_Tpa", "#O-CONTRACT-ISSUED-TPA", FieldType.NUMERIC, 6, 
            new DbsArrayController(1, 5));
        pnd_O_Contract_Issued_Ipro = localVariables.newFieldArrayInRecord("pnd_O_Contract_Issued_Ipro", "#O-CONTRACT-ISSUED-IPRO", FieldType.NUMERIC, 
            6, new DbsArrayController(1, 5));
        pnd_O_Certif_Issued = localVariables.newFieldArrayInRecord("pnd_O_Certif_Issued", "#O-CERTIF-ISSUED", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_O_Certif_Issued2 = localVariables.newFieldArrayInRecord("pnd_O_Certif_Issued2", "#O-CERTIF-ISSUED2", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_O_No_Rec = localVariables.newFieldArrayInRecord("pnd_O_No_Rec", "#O-NO-REC", FieldType.NUMERIC, 6, new DbsArrayController(1, 5));
        pnd_O_Apprvl = localVariables.newFieldArrayInRecord("pnd_O_Apprvl", "#O-APPRVL", FieldType.NUMERIC, 6, new DbsArrayController(1, 2));
        pnd_O_N_Apprvl = localVariables.newFieldInRecord("pnd_O_N_Apprvl", "#O-N-APPRVL", FieldType.NUMERIC, 6);

        pnd_O_Mdo_Fields = localVariables.newGroupInRecord("pnd_O_Mdo_Fields", "#O-MDO-FIELDS");
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Units = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Units", "#O-MDO-REA-ANNTY-UNITS", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Amt = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Amt", "#O-MDO-REA-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Traditional_Amt = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Traditional_Amt", "#O-MDO-TRADITIONAL-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Int_Pymnt = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Int_Pymnt", "#O-MDO-TIAA-INT-PYMNT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Cref_Int_Pymnt = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Cref_Int_Pymnt", "#O-MDO-CREF-INT-PYMNT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Excl = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Excl", "#O-MDO-TIAA-EXCL", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Cref_Excl = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Cref_Excl", "#O-MDO-CREF-EXCL", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Mma = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Mma", "#O-MDO-MMA", FieldType.NUMERIC, 11, 
            3, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Stock = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Stock", "#O-MDO-STOCK", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Social = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Social", "#O-MDO-SOCIAL", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Global = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Global", "#O-MDO-GLOBAL", FieldType.NUMERIC, 
            11, 3, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Bond = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Bond", "#O-MDO-BOND", FieldType.NUMERIC, 11, 
            3, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Grth = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Grth", "#O-MDO-GRTH", FieldType.NUMERIC, 11, 
            3, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Equ = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Equ", "#O-MDO-EQU", FieldType.NUMERIC, 11, 
            3, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Ilb = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Ilb", "#O-MDO-ILB", FieldType.NUMERIC, 11, 
            3, new DbsArrayController(1, 2));
        pnd_O_Mdo_Fields_Pnd_O_Mdo_Units = pnd_O_Mdo_Fields.newFieldArrayInGroup("pnd_O_Mdo_Fields_Pnd_O_Mdo_Units", "#O-MDO-UNITS", FieldType.NUMERIC, 
            12, 3, new DbsArrayController(1, 2));
        pnd_Mdo_Cref = localVariables.newFieldArrayInRecord("pnd_Mdo_Cref", "#MDO-CREF", FieldType.BOOLEAN, 1, new DbsArrayController(1, 2));
        pnd_O_Mdo_Cref = localVariables.newFieldArrayInRecord("pnd_O_Mdo_Cref", "#O-MDO-CREF", FieldType.BOOLEAN, 1, new DbsArrayController(1, 2));
        pnd_Todays_Business_Dt = localVariables.newFieldInRecord("pnd_Todays_Business_Dt", "#TODAYS-BUSINESS-DT", FieldType.NUMERIC, 8);

        pnd_Todays_Business_Dt__R_Field_2 = localVariables.newGroupInRecord("pnd_Todays_Business_Dt__R_Field_2", "REDEFINE", pnd_Todays_Business_Dt);
        pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A = pnd_Todays_Business_Dt__R_Field_2.newFieldInGroup("pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A", 
            "#TODAYS-BUSINESS-DT-A", FieldType.STRING, 8);

        pnd_Todays_Business_Dt__R_Field_3 = localVariables.newGroupInRecord("pnd_Todays_Business_Dt__R_Field_3", "REDEFINE", pnd_Todays_Business_Dt);
        pnd_Todays_Business_Dt_Pnd_Todays_Bus_Cc = pnd_Todays_Business_Dt__R_Field_3.newFieldInGroup("pnd_Todays_Business_Dt_Pnd_Todays_Bus_Cc", "#TODAYS-BUS-CC", 
            FieldType.NUMERIC, 2);
        pnd_Todays_Business_Dt_Pnd_Todays_Bus_Yy = pnd_Todays_Business_Dt__R_Field_3.newFieldInGroup("pnd_Todays_Business_Dt_Pnd_Todays_Bus_Yy", "#TODAYS-BUS-YY", 
            FieldType.NUMERIC, 2);
        pnd_Todays_Business_Dt_Pnd_Todays_Bus_Mm = pnd_Todays_Business_Dt__R_Field_3.newFieldInGroup("pnd_Todays_Business_Dt_Pnd_Todays_Bus_Mm", "#TODAYS-BUS-MM", 
            FieldType.NUMERIC, 2);
        pnd_Todays_Business_Dt_Pnd_Todays_Bus_Dd = pnd_Todays_Business_Dt__R_Field_3.newFieldInGroup("pnd_Todays_Business_Dt_Pnd_Todays_Bus_Dd", "#TODAYS-BUS-DD", 
            FieldType.NUMERIC, 2);
        pnd_Cis_Open_Status = localVariables.newFieldInRecord("pnd_Cis_Open_Status", "#CIS-OPEN-STATUS", FieldType.STRING, 2);
        pnd_Aprl_Y_N = localVariables.newFieldArrayInRecord("pnd_Aprl_Y_N", "#APRL-Y-N", FieldType.STRING, 20, new DbsArrayController(1, 2));
        pnd_O_Aprl_Y_N = localVariables.newFieldArrayInRecord("pnd_O_Aprl_Y_N", "#O-APRL-Y-N", FieldType.STRING, 20, new DbsArrayController(1, 2));
        pnd_Cis_Rqst_Id = localVariables.newFieldInRecord("pnd_Cis_Rqst_Id", "#CIS-RQST-ID", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cis.reset();
        vw_iaa_Trans_Rcrd.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cisb100x() throws Exception
    {
        super("Cisb100x");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*  IA LEGACY
        //*  IA OMNI
        //*  MDO LEGACY
        //*  MDO OMNI
        //*  LEGACY DETAIL BALANCING
        //*  OMNI DETAIL BALANCING
        //*  SG=OFF                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 2 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 3 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 4 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 5 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 6 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF
        //*                                                                                                                                                               //Natural: FORMAT LS = 80 PS = 60 ZP = ON IS = OFF ES = OFF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 4 )
        //*  TACC KG ST
        //*  TACC KG END
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 5 ) TITLE LEFT 30T ' LEGACY DETAIL LISTING OF ALL CONTRACTS AND AMOUNTS ' // 1T 'OPTION' 12T 'TIAA NUMBER' 25T 'ID' 29T 'MONTHLY UNITS' 44T 'ANNUAL UNITS' 59T 'TIAA AMOUNT' 74T 'CREF AMOUNT' 89T 'REA AMOUNT' 104T 'ACCESS AMT'
        //*  TACC KG ST
        //*  TACC KG END
        //*  IPFS
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 6 ) TITLE LEFT 30T ' OMNI DETAIL LISTING OF ALL CONTRACTS AND AMOUNTS ' // 1T 'OPTION' 12T 'TIAA NUMBER' 25T 'ID' 29T 'MONTHLY UNITS' 44T 'ANNUAL UNITS' 59T 'TIAA AMOUNT' 74T 'CREF AMOUNT' 89T 'REA AMOUNT' 104T 'ACCESS AMT'
        //* ***************************************************************
        pnd_Todays_Business_Dt.setValue(Global.getDATN());                                                                                                                //Natural: ASSIGN #TODAYS-BUSINESS-DT := *DATN
        //*  IPFS
        DbsUtil.callnat(Scin8888.class , getCurrentProcessState(), pnd_Todays_Business_Dt);                                                                               //Natural: CALLNAT 'SCIN8888' #TODAYS-BUSINESS-DT
        if (condition(Global.isEscape())) return;
        //*  IPFS
        if (condition(pnd_Todays_Business_Dt.equals(getZero())))                                                                                                          //Natural: IF #TODAYS-BUSINESS-DT = 0
        {
            //*  IPFS
            //*  IPFS
            //*  IPFS
            //*  IPFS
            getReports().write(0, "* * * * * * * * * * * * * * * * * * *",NEWLINE,"   TODAYS BUSINESS DATE IS INVALID",NEWLINE,"         FOR THE DATE:",                  //Natural: WRITE '* * * * * * * * * * * * * * * * * * *' / '   TODAYS BUSINESS DATE IS INVALID' / '         FOR THE DATE:' *DATN '* * * * * * * * * * * * * * * * * * *'
                Global.getDATN(),"* * * * * * * * * * * * * * * * * * *");
            if (Global.isEscape()) return;
            //*  IPFS
            DbsUtil.terminate(12);  if (true) return;                                                                                                                     //Natural: TERMINATE 12
            //*  IPFS
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ(1) CTRL BY CNTRL-SUPER-DE-1 = 'C000000'
        //*   IF CTRL.CNTRL-TODAYS-BUSINESS-DT = 0
        //*    WRITE '* * * * * * * * * * * * * * * * * * *' /
        //*      '   TODAYS BUSINESS DATE IS INVALID' /
        //*      '         FILE = 217 ' /
        //*      '         DBID = 008 ' /
        //*      '* * * * * * * * * * * * * * * * * * *'
        //*    TERMINATE 12
        //*  END-IF
        //*  IF #CNTRL-TODAYS-BUS-YY < 97
        //*    MOVE 20 TO #TODAYS-BUS-CC
        //*  ELSE
        //*    MOVE 19 TO #TODAYS-BUS-CC
        //*  END-IF
        //*  MOVE #CNTRL-TODAYS-BUS-YY TO #TODAYS-BUS-YY
        //*  MOVE #CNTRL-TODAYS-BUS-MM TO #TODAYS-BUS-MM
        //*  MOVE #CNTRL-TODAYS-BUS-DD TO #TODAYS-BUS-DD
        //*  END-READ
        //* ***************************************************************
        pnd_Cis_Open_Status.setValue("OI");                                                                                                                               //Natural: ASSIGN #CIS-OPEN-STATUS := 'OI'
        vw_cis.startDatabaseRead                                                                                                                                          //Natural: READ CIS BY CIS-OPEN-STATUS EQ #CIS-OPEN-STATUS
        (
        "READ01",
        new Wc[] { new Wc("CIS_OPEN_STATUS", ">=", pnd_Cis_Open_Status, WcType.BY) },
        new Oc[] { new Oc("CIS_OPEN_STATUS", "ASC") }
        );
        READ01:
        while (condition(vw_cis.readNextRow("READ01")))
        {
            //* *******************************************************************
            if (condition(cis_Cis_Status_Cd.notEquals("I") && cis_Cis_Opn_Clsd_Ind.notEquals("O")))                                                                       //Natural: IF CIS-STATUS-CD NE 'I' AND CIS-OPN-CLSD-IND NE 'O'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  *
            pnd_Run_Date.setValue(pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A);                                                                                       //Natural: MOVE #TODAYS-BUSINESS-DT-A TO #RUN-DATE
            pnd_Print_Date.setValueEdited(cis_Cis_Cntrct_Print_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED CIS.CIS-CNTRCT-PRINT-DTE ( EM = YYYYMMDD ) TO #PRINT-DATE
            if (condition(pnd_Print_Date.greater(pnd_Run_Date) || pnd_Print_Date.equals(" ")))                                                                            //Natural: IF #PRINT-DATE GT #RUN-DATE OR #PRINT-DATE = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  *****************************************************
            pnd_Omni.reset();                                                                                                                                             //Natural: RESET #OMNI
            short decideConditionsMet367 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE CIS-RQST-ID;//Natural: VALUE 'IA'
            if (condition((cis_Cis_Rqst_Id.equals("IA"))))
            {
                decideConditionsMet367++;
                if (condition(cis_Cis_Trnsf_Flag.equals("Y")))                                                                                                            //Natural: IF CIS-TRNSF-FLAG = 'Y'
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM IA-PROCESS
                    sub_Ia_Process();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'TPA','IPRO'
            else if (condition((cis_Cis_Rqst_Id.equals("TPA") || cis_Cis_Rqst_Id.equals("IPRO"))))
            {
                decideConditionsMet367++;
                                                                                                                                                                          //Natural: PERFORM TPA-IPRO-PROCESSING
                sub_Tpa_Ipro_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'MDO'
            else if (condition((cis_Cis_Rqst_Id.equals("MDO"))))
            {
                decideConditionsMet367++;
                                                                                                                                                                          //Natural: PERFORM MDO-PROCESS
                sub_Mdo_Process();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            getSort().writeSortInData(cis_Cis_Annty_Option, cis_Cis_Tiaa_Doi, cis_Cis_Pin_Nbr, cis_Cis_Da_Tiaa_Nbr.getValue(1), cis_Cis_Da_Tiaa_Nbr.getValue(2),          //Natural: END-ALL
                cis_Cis_Da_Tiaa_Nbr.getValue(3), cis_Cis_Da_Tiaa_Nbr.getValue(4), cis_Cis_Da_Tiaa_Nbr.getValue(5), cis_Cis_Da_Tiaa_Nbr.getValue(6), cis_Cis_Da_Tiaa_Nbr.getValue(7), 
                cis_Cis_Da_Tiaa_Nbr.getValue(8), cis_Cis_Da_Tiaa_Nbr.getValue(9), cis_Cis_Da_Tiaa_Nbr.getValue(10), cis_Cis_Da_Tiaa_Nbr.getValue(11), cis_Cis_Da_Tiaa_Nbr.getValue(12), 
                cis_Cis_Da_Tiaa_Nbr.getValue(13), cis_Cis_Da_Tiaa_Nbr.getValue(14), cis_Cis_Da_Tiaa_Nbr.getValue(15), cis_Cis_Da_Tiaa_Nbr.getValue(16), 
                cis_Cis_Da_Tiaa_Nbr.getValue(17), cis_Cis_Da_Tiaa_Nbr.getValue(18), cis_Cis_Da_Tiaa_Nbr.getValue(19), cis_Cis_Da_Tiaa_Nbr.getValue(20), 
                cis_Cis_Rqst_Id, cis_Cis_Opn_Clsd_Ind, cis_Cis_Status_Cd, cis_Cis_Rqst_Id_Key, cis_Cis_Cntrct_Apprvl_Ind, cis_Cis_Tiaa_Nbr, cis_Cis_Cert_Nbr, 
                cis_Cis_Cref_Doi, cis_Cis_Cntrct_Print_Dte, cis_Cis_Cntrct_Type, cis_Cis_Pymnt_Mode, cis_Cis_Grnted_Grd_Amt, cis_Cis_Grnted_Std_Amt, cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(1), 
                cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(2), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(3), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(4), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(5), 
                cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(6), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(7), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(8), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(9), 
                cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(10), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(11), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(12), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(13), 
                cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(14), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(15), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(16), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(17), 
                cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(18), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(19), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(20), cis_Cis_Da_Rea_Proceeds_Amt.getValue(1), 
                cis_Cis_Da_Rea_Proceeds_Amt.getValue(2), cis_Cis_Da_Rea_Proceeds_Amt.getValue(3), cis_Cis_Da_Rea_Proceeds_Amt.getValue(4), cis_Cis_Da_Rea_Proceeds_Amt.getValue(5), 
                cis_Cis_Da_Rea_Proceeds_Amt.getValue(6), cis_Cis_Da_Rea_Proceeds_Amt.getValue(7), cis_Cis_Da_Rea_Proceeds_Amt.getValue(8), cis_Cis_Da_Rea_Proceeds_Amt.getValue(9), 
                cis_Cis_Da_Rea_Proceeds_Amt.getValue(10), cis_Cis_Da_Rea_Proceeds_Amt.getValue(11), cis_Cis_Da_Rea_Proceeds_Amt.getValue(12), cis_Cis_Da_Rea_Proceeds_Amt.getValue(13), 
                cis_Cis_Da_Rea_Proceeds_Amt.getValue(14), cis_Cis_Da_Rea_Proceeds_Amt.getValue(15), cis_Cis_Da_Rea_Proceeds_Amt.getValue(16), cis_Cis_Da_Rea_Proceeds_Amt.getValue(17), 
                cis_Cis_Da_Rea_Proceeds_Amt.getValue(18), cis_Cis_Da_Rea_Proceeds_Amt.getValue(19), cis_Cis_Da_Rea_Proceeds_Amt.getValue(20), cis_Cis_Da_Cref_Proceeds_Amt.getValue(1), 
                cis_Cis_Da_Cref_Proceeds_Amt.getValue(2), cis_Cis_Da_Cref_Proceeds_Amt.getValue(3), cis_Cis_Da_Cref_Proceeds_Amt.getValue(4), cis_Cis_Da_Cref_Proceeds_Amt.getValue(5), 
                cis_Cis_Da_Cref_Proceeds_Amt.getValue(6), cis_Cis_Da_Cref_Proceeds_Amt.getValue(7), cis_Cis_Da_Cref_Proceeds_Amt.getValue(8), cis_Cis_Da_Cref_Proceeds_Amt.getValue(9), 
                cis_Cis_Da_Cref_Proceeds_Amt.getValue(10), cis_Cis_Da_Cref_Proceeds_Amt.getValue(11), cis_Cis_Da_Cref_Proceeds_Amt.getValue(12), cis_Cis_Da_Cref_Proceeds_Amt.getValue(13), 
                cis_Cis_Da_Cref_Proceeds_Amt.getValue(14), cis_Cis_Da_Cref_Proceeds_Amt.getValue(15), cis_Cis_Da_Cref_Proceeds_Amt.getValue(16), cis_Cis_Da_Cref_Proceeds_Amt.getValue(17), 
                cis_Cis_Da_Cref_Proceeds_Amt.getValue(18), cis_Cis_Da_Cref_Proceeds_Amt.getValue(19), cis_Cis_Da_Cref_Proceeds_Amt.getValue(20), cis_Cis_Cref_Acct_Cde.getValue(1), 
                cis_Cis_Cref_Acct_Cde.getValue(2), cis_Cis_Cref_Acct_Cde.getValue(3), cis_Cis_Cref_Acct_Cde.getValue(4), cis_Cis_Cref_Acct_Cde.getValue(5), 
                cis_Cis_Cref_Acct_Cde.getValue(6), cis_Cis_Cref_Acct_Cde.getValue(7), cis_Cis_Cref_Acct_Cde.getValue(8), cis_Cis_Cref_Acct_Cde.getValue(9), 
                cis_Cis_Cref_Acct_Cde.getValue(10), cis_Cis_Cref_Acct_Cde.getValue(11), cis_Cis_Cref_Acct_Cde.getValue(12), cis_Cis_Cref_Acct_Cde.getValue(13), 
                cis_Cis_Cref_Acct_Cde.getValue(14), cis_Cis_Cref_Acct_Cde.getValue(15), cis_Cis_Cref_Acct_Cde.getValue(16), cis_Cis_Cref_Acct_Cde.getValue(17), 
                cis_Cis_Cref_Acct_Cde.getValue(18), cis_Cis_Cref_Acct_Cde.getValue(19), cis_Cis_Cref_Acct_Cde.getValue(20), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(1), 
                cis_Cis_Cref_Mnthly_Nbr_Units.getValue(2), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(3), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(4), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(5), 
                cis_Cis_Cref_Mnthly_Nbr_Units.getValue(6), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(7), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(8), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(9), 
                cis_Cis_Cref_Mnthly_Nbr_Units.getValue(10), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(11), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(12), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(13), 
                cis_Cis_Cref_Mnthly_Nbr_Units.getValue(14), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(15), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(16), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(17), 
                cis_Cis_Cref_Mnthly_Nbr_Units.getValue(18), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(19), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(20), cis_Cis_Cref_Annual_Nbr_Units.getValue(1), 
                cis_Cis_Cref_Annual_Nbr_Units.getValue(2), cis_Cis_Cref_Annual_Nbr_Units.getValue(3), cis_Cis_Cref_Annual_Nbr_Units.getValue(4), cis_Cis_Cref_Annual_Nbr_Units.getValue(5), 
                cis_Cis_Cref_Annual_Nbr_Units.getValue(6), cis_Cis_Cref_Annual_Nbr_Units.getValue(7), cis_Cis_Cref_Annual_Nbr_Units.getValue(8), cis_Cis_Cref_Annual_Nbr_Units.getValue(9), 
                cis_Cis_Cref_Annual_Nbr_Units.getValue(10), cis_Cis_Cref_Annual_Nbr_Units.getValue(11), cis_Cis_Cref_Annual_Nbr_Units.getValue(12), cis_Cis_Cref_Annual_Nbr_Units.getValue(13), 
                cis_Cis_Cref_Annual_Nbr_Units.getValue(14), cis_Cis_Cref_Annual_Nbr_Units.getValue(15), cis_Cis_Cref_Annual_Nbr_Units.getValue(16), cis_Cis_Cref_Annual_Nbr_Units.getValue(17), 
                cis_Cis_Cref_Annual_Nbr_Units.getValue(18), cis_Cis_Cref_Annual_Nbr_Units.getValue(19), cis_Cis_Cref_Annual_Nbr_Units.getValue(20), cis_Cis_Rea_Mnthly_Nbr_Units, 
                cis_Cis_Rea_Annual_Nbr_Units, cis_Cis_Rea_Annty_Amt, cis_Cis_Mdo_Contract_Cash_Status, cis_Cis_Mdo_Contract_Type, cis_Cis_Mdo_Traditional_Amt, 
                cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt, cis_Cis_Mdo_Cref_Int_Pymnt_Amt, cis_Cis_Mdo_Tiaa_Excluded_Amt, cis_Cis_Mdo_Cref_Excluded_Amt, cis_Cis_Rtb_Amt, 
                cis_Cis_Trnsf_Flag, cis_Cis_Corp_Sync_Ind, cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(1), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(2), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(3), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(4), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(5), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(6), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(7), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(8), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(9), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(10), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(11), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(12), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(13), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(14), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(15), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(16), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(17), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(18), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(19), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(20), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(21), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(22), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(23), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(24), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(25), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(26), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(27), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(28), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(29), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(30), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(31), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(32), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(33), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(34), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(35), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(36), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(37), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(38), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(39), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(40), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(41), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(42), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(43), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(44), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(45), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(46), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(47), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(48), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(49), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(50), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(51), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(52), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(53), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(54), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(55), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(56), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(57), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(58), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(59), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(60), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(61), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(62), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(63), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(64), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(65), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(66), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(67), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(68), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(69), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(70), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(71), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(72), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(73), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(74), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(75), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(76), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(77), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(78), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(79), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(80), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(81), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(82), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(83), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(84), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(85), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(86), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(87), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(88), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(89), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(90), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(91), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(92), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(93), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(94), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(95), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(96), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(97), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(98), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(99), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(100), cis_Cis_Tacc_Annual_Nbr_Units.getValue(1), cis_Cis_Tacc_Annual_Nbr_Units.getValue(2), cis_Cis_Tacc_Annual_Nbr_Units.getValue(3), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(4), cis_Cis_Tacc_Annual_Nbr_Units.getValue(5), cis_Cis_Tacc_Annual_Nbr_Units.getValue(6), cis_Cis_Tacc_Annual_Nbr_Units.getValue(7), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(8), cis_Cis_Tacc_Annual_Nbr_Units.getValue(9), cis_Cis_Tacc_Annual_Nbr_Units.getValue(10), cis_Cis_Tacc_Annual_Nbr_Units.getValue(11), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(12), cis_Cis_Tacc_Annual_Nbr_Units.getValue(13), cis_Cis_Tacc_Annual_Nbr_Units.getValue(14), cis_Cis_Tacc_Annual_Nbr_Units.getValue(15), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(16), cis_Cis_Tacc_Annual_Nbr_Units.getValue(17), cis_Cis_Tacc_Annual_Nbr_Units.getValue(18), cis_Cis_Tacc_Annual_Nbr_Units.getValue(19), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(20), cis_Cis_Tacc_Annual_Nbr_Units.getValue(21), cis_Cis_Tacc_Annual_Nbr_Units.getValue(22), cis_Cis_Tacc_Annual_Nbr_Units.getValue(23), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(24), cis_Cis_Tacc_Annual_Nbr_Units.getValue(25), cis_Cis_Tacc_Annual_Nbr_Units.getValue(26), cis_Cis_Tacc_Annual_Nbr_Units.getValue(27), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(28), cis_Cis_Tacc_Annual_Nbr_Units.getValue(29), cis_Cis_Tacc_Annual_Nbr_Units.getValue(30), cis_Cis_Tacc_Annual_Nbr_Units.getValue(31), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(32), cis_Cis_Tacc_Annual_Nbr_Units.getValue(33), cis_Cis_Tacc_Annual_Nbr_Units.getValue(34), cis_Cis_Tacc_Annual_Nbr_Units.getValue(35), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(36), cis_Cis_Tacc_Annual_Nbr_Units.getValue(37), cis_Cis_Tacc_Annual_Nbr_Units.getValue(38), cis_Cis_Tacc_Annual_Nbr_Units.getValue(39), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(40), cis_Cis_Tacc_Annual_Nbr_Units.getValue(41), cis_Cis_Tacc_Annual_Nbr_Units.getValue(42), cis_Cis_Tacc_Annual_Nbr_Units.getValue(43), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(44), cis_Cis_Tacc_Annual_Nbr_Units.getValue(45), cis_Cis_Tacc_Annual_Nbr_Units.getValue(46), cis_Cis_Tacc_Annual_Nbr_Units.getValue(47), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(48), cis_Cis_Tacc_Annual_Nbr_Units.getValue(49), cis_Cis_Tacc_Annual_Nbr_Units.getValue(50), cis_Cis_Tacc_Annual_Nbr_Units.getValue(51), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(52), cis_Cis_Tacc_Annual_Nbr_Units.getValue(53), cis_Cis_Tacc_Annual_Nbr_Units.getValue(54), cis_Cis_Tacc_Annual_Nbr_Units.getValue(55), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(56), cis_Cis_Tacc_Annual_Nbr_Units.getValue(57), cis_Cis_Tacc_Annual_Nbr_Units.getValue(58), cis_Cis_Tacc_Annual_Nbr_Units.getValue(59), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(60), cis_Cis_Tacc_Annual_Nbr_Units.getValue(61), cis_Cis_Tacc_Annual_Nbr_Units.getValue(62), cis_Cis_Tacc_Annual_Nbr_Units.getValue(63), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(64), cis_Cis_Tacc_Annual_Nbr_Units.getValue(65), cis_Cis_Tacc_Annual_Nbr_Units.getValue(66), cis_Cis_Tacc_Annual_Nbr_Units.getValue(67), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(68), cis_Cis_Tacc_Annual_Nbr_Units.getValue(69), cis_Cis_Tacc_Annual_Nbr_Units.getValue(70), cis_Cis_Tacc_Annual_Nbr_Units.getValue(71), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(72), cis_Cis_Tacc_Annual_Nbr_Units.getValue(73), cis_Cis_Tacc_Annual_Nbr_Units.getValue(74), cis_Cis_Tacc_Annual_Nbr_Units.getValue(75), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(76), cis_Cis_Tacc_Annual_Nbr_Units.getValue(77), cis_Cis_Tacc_Annual_Nbr_Units.getValue(78), cis_Cis_Tacc_Annual_Nbr_Units.getValue(79), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(80), cis_Cis_Tacc_Annual_Nbr_Units.getValue(81), cis_Cis_Tacc_Annual_Nbr_Units.getValue(82), cis_Cis_Tacc_Annual_Nbr_Units.getValue(83), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(84), cis_Cis_Tacc_Annual_Nbr_Units.getValue(85), cis_Cis_Tacc_Annual_Nbr_Units.getValue(86), cis_Cis_Tacc_Annual_Nbr_Units.getValue(87), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(88), cis_Cis_Tacc_Annual_Nbr_Units.getValue(89), cis_Cis_Tacc_Annual_Nbr_Units.getValue(90), cis_Cis_Tacc_Annual_Nbr_Units.getValue(91), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(92), cis_Cis_Tacc_Annual_Nbr_Units.getValue(93), cis_Cis_Tacc_Annual_Nbr_Units.getValue(94), cis_Cis_Tacc_Annual_Nbr_Units.getValue(95), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(96), cis_Cis_Tacc_Annual_Nbr_Units.getValue(97), cis_Cis_Tacc_Annual_Nbr_Units.getValue(98), cis_Cis_Tacc_Annual_Nbr_Units.getValue(99), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(100), cis_Cis_Tacc_Annty_Amt, pnd_Omni);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(cis_Cis_Annty_Option, cis_Cis_Tiaa_Doi);                                                                                                       //Natural: SORT CIS-ANNTY-OPTION CIS-TIAA-DOI USING CIS-PIN-NBR CIS-DA-TIAA-NBR ( 1:20 ) CIS-RQST-ID CIS-OPN-CLSD-IND CIS-STATUS-CD CIS-RQST-ID-KEY CIS-CNTRCT-APPRVL-IND CIS-TIAA-NBR CIS-CERT-NBR CIS-CREF-DOI CIS-CNTRCT-PRINT-DTE CIS-CNTRCT-TYPE CIS-PYMNT-MODE CIS-GRNTED-GRD-AMT CIS-GRNTED-STD-AMT CIS-DA-TIAA-PROCEEDS-AMT ( 1:20 ) CIS-DA-REA-PROCEEDS-AMT ( 1:20 ) CIS-DA-CREF-PROCEEDS-AMT ( 1:20 ) CIS-CREF-ACCT-CDE ( 1:20 ) CIS-CREF-MNTHLY-NBR-UNITS ( 1:20 ) CIS-CREF-ANNUAL-NBR-UNITS ( 1:20 ) CIS-REA-MNTHLY-NBR-UNITS CIS-REA-ANNUAL-NBR-UNITS CIS-REA-ANNTY-AMT CIS-MDO-CONTRACT-CASH-STATUS CIS-MDO-CONTRACT-TYPE CIS-MDO-TRADITIONAL-AMT CIS-MDO-TIAA-INT-PYMNT-AMT CIS-MDO-CREF-INT-PYMNT-AMT CIS-MDO-TIAA-EXCLUDED-AMT CIS-MDO-CREF-EXCLUDED-AMT CIS-RTB-AMT CIS-TRNSF-FLAG CIS-CORP-SYNC-IND CIS-TACC-MNTHLY-NBR-UNITS ( 1:100 ) CIS-TACC-ANNUAL-NBR-UNITS ( 1:100 ) CIS-TACC-ANNTY-AMT #OMNI
        SORT01:
        while (condition(getSort().readSortOutData(cis_Cis_Annty_Option, cis_Cis_Tiaa_Doi, cis_Cis_Pin_Nbr, cis_Cis_Da_Tiaa_Nbr.getValue(1), cis_Cis_Da_Tiaa_Nbr.getValue(2), 
            cis_Cis_Da_Tiaa_Nbr.getValue(3), cis_Cis_Da_Tiaa_Nbr.getValue(4), cis_Cis_Da_Tiaa_Nbr.getValue(5), cis_Cis_Da_Tiaa_Nbr.getValue(6), cis_Cis_Da_Tiaa_Nbr.getValue(7), 
            cis_Cis_Da_Tiaa_Nbr.getValue(8), cis_Cis_Da_Tiaa_Nbr.getValue(9), cis_Cis_Da_Tiaa_Nbr.getValue(10), cis_Cis_Da_Tiaa_Nbr.getValue(11), cis_Cis_Da_Tiaa_Nbr.getValue(12), 
            cis_Cis_Da_Tiaa_Nbr.getValue(13), cis_Cis_Da_Tiaa_Nbr.getValue(14), cis_Cis_Da_Tiaa_Nbr.getValue(15), cis_Cis_Da_Tiaa_Nbr.getValue(16), cis_Cis_Da_Tiaa_Nbr.getValue(17), 
            cis_Cis_Da_Tiaa_Nbr.getValue(18), cis_Cis_Da_Tiaa_Nbr.getValue(19), cis_Cis_Da_Tiaa_Nbr.getValue(20), cis_Cis_Rqst_Id, cis_Cis_Opn_Clsd_Ind, 
            cis_Cis_Status_Cd, cis_Cis_Rqst_Id_Key, cis_Cis_Cntrct_Apprvl_Ind, cis_Cis_Tiaa_Nbr, cis_Cis_Cert_Nbr, cis_Cis_Cref_Doi, cis_Cis_Cntrct_Print_Dte, 
            cis_Cis_Cntrct_Type, cis_Cis_Pymnt_Mode, cis_Cis_Grnted_Grd_Amt, cis_Cis_Grnted_Std_Amt, cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(1), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(2), 
            cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(3), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(4), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(5), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(6), 
            cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(7), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(8), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(9), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(10), 
            cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(11), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(12), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(13), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(14), 
            cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(15), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(16), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(17), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(18), 
            cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(19), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(20), cis_Cis_Da_Rea_Proceeds_Amt.getValue(1), cis_Cis_Da_Rea_Proceeds_Amt.getValue(2), 
            cis_Cis_Da_Rea_Proceeds_Amt.getValue(3), cis_Cis_Da_Rea_Proceeds_Amt.getValue(4), cis_Cis_Da_Rea_Proceeds_Amt.getValue(5), cis_Cis_Da_Rea_Proceeds_Amt.getValue(6), 
            cis_Cis_Da_Rea_Proceeds_Amt.getValue(7), cis_Cis_Da_Rea_Proceeds_Amt.getValue(8), cis_Cis_Da_Rea_Proceeds_Amt.getValue(9), cis_Cis_Da_Rea_Proceeds_Amt.getValue(10), 
            cis_Cis_Da_Rea_Proceeds_Amt.getValue(11), cis_Cis_Da_Rea_Proceeds_Amt.getValue(12), cis_Cis_Da_Rea_Proceeds_Amt.getValue(13), cis_Cis_Da_Rea_Proceeds_Amt.getValue(14), 
            cis_Cis_Da_Rea_Proceeds_Amt.getValue(15), cis_Cis_Da_Rea_Proceeds_Amt.getValue(16), cis_Cis_Da_Rea_Proceeds_Amt.getValue(17), cis_Cis_Da_Rea_Proceeds_Amt.getValue(18), 
            cis_Cis_Da_Rea_Proceeds_Amt.getValue(19), cis_Cis_Da_Rea_Proceeds_Amt.getValue(20), cis_Cis_Da_Cref_Proceeds_Amt.getValue(1), cis_Cis_Da_Cref_Proceeds_Amt.getValue(2), 
            cis_Cis_Da_Cref_Proceeds_Amt.getValue(3), cis_Cis_Da_Cref_Proceeds_Amt.getValue(4), cis_Cis_Da_Cref_Proceeds_Amt.getValue(5), cis_Cis_Da_Cref_Proceeds_Amt.getValue(6), 
            cis_Cis_Da_Cref_Proceeds_Amt.getValue(7), cis_Cis_Da_Cref_Proceeds_Amt.getValue(8), cis_Cis_Da_Cref_Proceeds_Amt.getValue(9), cis_Cis_Da_Cref_Proceeds_Amt.getValue(10), 
            cis_Cis_Da_Cref_Proceeds_Amt.getValue(11), cis_Cis_Da_Cref_Proceeds_Amt.getValue(12), cis_Cis_Da_Cref_Proceeds_Amt.getValue(13), cis_Cis_Da_Cref_Proceeds_Amt.getValue(14), 
            cis_Cis_Da_Cref_Proceeds_Amt.getValue(15), cis_Cis_Da_Cref_Proceeds_Amt.getValue(16), cis_Cis_Da_Cref_Proceeds_Amt.getValue(17), cis_Cis_Da_Cref_Proceeds_Amt.getValue(18), 
            cis_Cis_Da_Cref_Proceeds_Amt.getValue(19), cis_Cis_Da_Cref_Proceeds_Amt.getValue(20), cis_Cis_Cref_Acct_Cde.getValue(1), cis_Cis_Cref_Acct_Cde.getValue(2), 
            cis_Cis_Cref_Acct_Cde.getValue(3), cis_Cis_Cref_Acct_Cde.getValue(4), cis_Cis_Cref_Acct_Cde.getValue(5), cis_Cis_Cref_Acct_Cde.getValue(6), 
            cis_Cis_Cref_Acct_Cde.getValue(7), cis_Cis_Cref_Acct_Cde.getValue(8), cis_Cis_Cref_Acct_Cde.getValue(9), cis_Cis_Cref_Acct_Cde.getValue(10), 
            cis_Cis_Cref_Acct_Cde.getValue(11), cis_Cis_Cref_Acct_Cde.getValue(12), cis_Cis_Cref_Acct_Cde.getValue(13), cis_Cis_Cref_Acct_Cde.getValue(14), 
            cis_Cis_Cref_Acct_Cde.getValue(15), cis_Cis_Cref_Acct_Cde.getValue(16), cis_Cis_Cref_Acct_Cde.getValue(17), cis_Cis_Cref_Acct_Cde.getValue(18), 
            cis_Cis_Cref_Acct_Cde.getValue(19), cis_Cis_Cref_Acct_Cde.getValue(20), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(1), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(2), 
            cis_Cis_Cref_Mnthly_Nbr_Units.getValue(3), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(4), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(5), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(6), 
            cis_Cis_Cref_Mnthly_Nbr_Units.getValue(7), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(8), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(9), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(10), 
            cis_Cis_Cref_Mnthly_Nbr_Units.getValue(11), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(12), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(13), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(14), 
            cis_Cis_Cref_Mnthly_Nbr_Units.getValue(15), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(16), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(17), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(18), 
            cis_Cis_Cref_Mnthly_Nbr_Units.getValue(19), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(20), cis_Cis_Cref_Annual_Nbr_Units.getValue(1), cis_Cis_Cref_Annual_Nbr_Units.getValue(2), 
            cis_Cis_Cref_Annual_Nbr_Units.getValue(3), cis_Cis_Cref_Annual_Nbr_Units.getValue(4), cis_Cis_Cref_Annual_Nbr_Units.getValue(5), cis_Cis_Cref_Annual_Nbr_Units.getValue(6), 
            cis_Cis_Cref_Annual_Nbr_Units.getValue(7), cis_Cis_Cref_Annual_Nbr_Units.getValue(8), cis_Cis_Cref_Annual_Nbr_Units.getValue(9), cis_Cis_Cref_Annual_Nbr_Units.getValue(10), 
            cis_Cis_Cref_Annual_Nbr_Units.getValue(11), cis_Cis_Cref_Annual_Nbr_Units.getValue(12), cis_Cis_Cref_Annual_Nbr_Units.getValue(13), cis_Cis_Cref_Annual_Nbr_Units.getValue(14), 
            cis_Cis_Cref_Annual_Nbr_Units.getValue(15), cis_Cis_Cref_Annual_Nbr_Units.getValue(16), cis_Cis_Cref_Annual_Nbr_Units.getValue(17), cis_Cis_Cref_Annual_Nbr_Units.getValue(18), 
            cis_Cis_Cref_Annual_Nbr_Units.getValue(19), cis_Cis_Cref_Annual_Nbr_Units.getValue(20), cis_Cis_Rea_Mnthly_Nbr_Units, cis_Cis_Rea_Annual_Nbr_Units, 
            cis_Cis_Rea_Annty_Amt, cis_Cis_Mdo_Contract_Cash_Status, cis_Cis_Mdo_Contract_Type, cis_Cis_Mdo_Traditional_Amt, cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt, 
            cis_Cis_Mdo_Cref_Int_Pymnt_Amt, cis_Cis_Mdo_Tiaa_Excluded_Amt, cis_Cis_Mdo_Cref_Excluded_Amt, cis_Cis_Rtb_Amt, cis_Cis_Trnsf_Flag, cis_Cis_Corp_Sync_Ind, 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(1), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(2), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(3), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(4), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(5), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(6), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(7), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(8), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(9), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(10), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(11), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(12), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(13), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(14), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(15), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(16), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(17), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(18), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(19), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(20), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(21), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(22), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(23), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(24), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(25), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(26), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(27), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(28), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(29), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(30), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(31), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(32), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(33), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(34), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(35), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(36), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(37), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(38), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(39), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(40), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(41), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(42), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(43), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(44), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(45), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(46), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(47), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(48), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(49), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(50), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(51), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(52), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(53), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(54), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(55), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(56), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(57), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(58), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(59), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(60), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(61), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(62), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(63), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(64), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(65), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(66), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(67), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(68), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(69), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(70), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(71), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(72), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(73), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(74), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(75), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(76), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(77), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(78), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(79), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(80), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(81), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(82), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(83), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(84), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(85), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(86), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(87), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(88), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(89), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(90), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(91), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(92), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(93), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(94), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(95), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(96), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(97), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(98), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(99), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(100), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(1), cis_Cis_Tacc_Annual_Nbr_Units.getValue(2), cis_Cis_Tacc_Annual_Nbr_Units.getValue(3), cis_Cis_Tacc_Annual_Nbr_Units.getValue(4), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(5), cis_Cis_Tacc_Annual_Nbr_Units.getValue(6), cis_Cis_Tacc_Annual_Nbr_Units.getValue(7), cis_Cis_Tacc_Annual_Nbr_Units.getValue(8), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(9), cis_Cis_Tacc_Annual_Nbr_Units.getValue(10), cis_Cis_Tacc_Annual_Nbr_Units.getValue(11), cis_Cis_Tacc_Annual_Nbr_Units.getValue(12), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(13), cis_Cis_Tacc_Annual_Nbr_Units.getValue(14), cis_Cis_Tacc_Annual_Nbr_Units.getValue(15), cis_Cis_Tacc_Annual_Nbr_Units.getValue(16), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(17), cis_Cis_Tacc_Annual_Nbr_Units.getValue(18), cis_Cis_Tacc_Annual_Nbr_Units.getValue(19), cis_Cis_Tacc_Annual_Nbr_Units.getValue(20), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(21), cis_Cis_Tacc_Annual_Nbr_Units.getValue(22), cis_Cis_Tacc_Annual_Nbr_Units.getValue(23), cis_Cis_Tacc_Annual_Nbr_Units.getValue(24), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(25), cis_Cis_Tacc_Annual_Nbr_Units.getValue(26), cis_Cis_Tacc_Annual_Nbr_Units.getValue(27), cis_Cis_Tacc_Annual_Nbr_Units.getValue(28), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(29), cis_Cis_Tacc_Annual_Nbr_Units.getValue(30), cis_Cis_Tacc_Annual_Nbr_Units.getValue(31), cis_Cis_Tacc_Annual_Nbr_Units.getValue(32), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(33), cis_Cis_Tacc_Annual_Nbr_Units.getValue(34), cis_Cis_Tacc_Annual_Nbr_Units.getValue(35), cis_Cis_Tacc_Annual_Nbr_Units.getValue(36), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(37), cis_Cis_Tacc_Annual_Nbr_Units.getValue(38), cis_Cis_Tacc_Annual_Nbr_Units.getValue(39), cis_Cis_Tacc_Annual_Nbr_Units.getValue(40), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(41), cis_Cis_Tacc_Annual_Nbr_Units.getValue(42), cis_Cis_Tacc_Annual_Nbr_Units.getValue(43), cis_Cis_Tacc_Annual_Nbr_Units.getValue(44), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(45), cis_Cis_Tacc_Annual_Nbr_Units.getValue(46), cis_Cis_Tacc_Annual_Nbr_Units.getValue(47), cis_Cis_Tacc_Annual_Nbr_Units.getValue(48), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(49), cis_Cis_Tacc_Annual_Nbr_Units.getValue(50), cis_Cis_Tacc_Annual_Nbr_Units.getValue(51), cis_Cis_Tacc_Annual_Nbr_Units.getValue(52), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(53), cis_Cis_Tacc_Annual_Nbr_Units.getValue(54), cis_Cis_Tacc_Annual_Nbr_Units.getValue(55), cis_Cis_Tacc_Annual_Nbr_Units.getValue(56), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(57), cis_Cis_Tacc_Annual_Nbr_Units.getValue(58), cis_Cis_Tacc_Annual_Nbr_Units.getValue(59), cis_Cis_Tacc_Annual_Nbr_Units.getValue(60), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(61), cis_Cis_Tacc_Annual_Nbr_Units.getValue(62), cis_Cis_Tacc_Annual_Nbr_Units.getValue(63), cis_Cis_Tacc_Annual_Nbr_Units.getValue(64), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(65), cis_Cis_Tacc_Annual_Nbr_Units.getValue(66), cis_Cis_Tacc_Annual_Nbr_Units.getValue(67), cis_Cis_Tacc_Annual_Nbr_Units.getValue(68), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(69), cis_Cis_Tacc_Annual_Nbr_Units.getValue(70), cis_Cis_Tacc_Annual_Nbr_Units.getValue(71), cis_Cis_Tacc_Annual_Nbr_Units.getValue(72), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(73), cis_Cis_Tacc_Annual_Nbr_Units.getValue(74), cis_Cis_Tacc_Annual_Nbr_Units.getValue(75), cis_Cis_Tacc_Annual_Nbr_Units.getValue(76), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(77), cis_Cis_Tacc_Annual_Nbr_Units.getValue(78), cis_Cis_Tacc_Annual_Nbr_Units.getValue(79), cis_Cis_Tacc_Annual_Nbr_Units.getValue(80), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(81), cis_Cis_Tacc_Annual_Nbr_Units.getValue(82), cis_Cis_Tacc_Annual_Nbr_Units.getValue(83), cis_Cis_Tacc_Annual_Nbr_Units.getValue(84), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(85), cis_Cis_Tacc_Annual_Nbr_Units.getValue(86), cis_Cis_Tacc_Annual_Nbr_Units.getValue(87), cis_Cis_Tacc_Annual_Nbr_Units.getValue(88), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(89), cis_Cis_Tacc_Annual_Nbr_Units.getValue(90), cis_Cis_Tacc_Annual_Nbr_Units.getValue(91), cis_Cis_Tacc_Annual_Nbr_Units.getValue(92), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(93), cis_Cis_Tacc_Annual_Nbr_Units.getValue(94), cis_Cis_Tacc_Annual_Nbr_Units.getValue(95), cis_Cis_Tacc_Annual_Nbr_Units.getValue(96), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(97), cis_Cis_Tacc_Annual_Nbr_Units.getValue(98), cis_Cis_Tacc_Annual_Nbr_Units.getValue(99), cis_Cis_Tacc_Annual_Nbr_Units.getValue(100), 
            cis_Cis_Tacc_Annty_Amt, pnd_Omni)))
        {
                                                                                                                                                                          //Natural: PERFORM S700-PRINT-ARRAY
            sub_S700_Print_Array();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
                                                                                                                                                                          //Natural: PERFORM ALL-MODES
        sub_All_Modes();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM IA-REPORT
        sub_Ia_Report();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MDO-REPORT
        sub_Mdo_Report();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM TOTAL-DETAIL-REPORT
        sub_Total_Detail_Report();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-PROCESS
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TPA-IPRO-PROCESSING
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ALL-MODES
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRNTED-AMT
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRNTED-AMT-IPRO
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRNTED-AMT-TPA
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALUE-CODE
        //* *
        //* *
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DA-PROCEED
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DA-PROCEED-TPA
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DA-PROCEED-IPRO
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-REPORT
        //*  TPA TOTALS
        //*  IPRO TOTALS
        //*  TPA TOTALS
        //*  IPRO TOTALS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ISSUED-TOTALS
        //* *********************************************************************
        //*  IA
        //*  TPA
        //*  IPRO
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDO-PROCESS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDO-REPORT
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S700-PRINT-ARRAY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PLATFORM-IND
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-DETAIL-REPORT
        //* ***********************************************************************
    }
    private void sub_Ia_Process() throws Exception                                                                                                                        //Natural: IA-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
                                                                                                                                                                          //Natural: PERFORM GET-PLATFORM-IND
        sub_Get_Platform_Ind();
        if (condition(Global.isEscape())) {return;}
        //*  MONTHLY
        //*  QUARTERLY
        //*  SEMI-ANNUAL
        //*  ANNUAL
        short decideConditionsMet499 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CIS.CIS-PYMNT-MODE;//Natural: VALUE 'M'
        if (condition((cis_Cis_Pymnt_Mode.equals("M"))))
        {
            decideConditionsMet499++;
            pnd_P.setValue(1);                                                                                                                                            //Natural: ASSIGN #P := 1
        }                                                                                                                                                                 //Natural: VALUE 'Q'
        else if (condition((cis_Cis_Pymnt_Mode.equals("Q"))))
        {
            decideConditionsMet499++;
            pnd_P.setValue(2);                                                                                                                                            //Natural: ASSIGN #P := 2
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((cis_Cis_Pymnt_Mode.equals("S"))))
        {
            decideConditionsMet499++;
            pnd_P.setValue(3);                                                                                                                                            //Natural: ASSIGN #P := 3
        }                                                                                                                                                                 //Natural: VALUE 'A'
        else if (condition((cis_Cis_Pymnt_Mode.equals("A"))))
        {
            decideConditionsMet499++;
            pnd_P.setValue(4);                                                                                                                                            //Natural: ASSIGN #P := 4
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet499 > 0))
        {
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT
            sub_Grnted_Amt();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM VALUE-CODE
            sub_Value_Code();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED
            sub_Da_Proceed();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Omni.getBoolean()))                                                                                                                         //Natural: IF #OMNI
            {
                pnd_O_No_Rec.getValue(pnd_P).nadd(1);                                                                                                                     //Natural: ADD 1 TO #O-NO-REC ( #P )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_No_Rec.getValue(pnd_P).nadd(1);                                                                                                                       //Natural: ADD 1 TO #NO-REC ( #P )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Tpa_Ipro_Processing() throws Exception                                                                                                               //Natural: TPA-IPRO-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        //*  MONTHLY
        //*  QUARTERLY
        //*  SEMI-ANNUAL
        //*  ANNUAL
        short decideConditionsMet529 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CIS.CIS-PYMNT-MODE;//Natural: VALUE 'M'
        if (condition((cis_Cis_Pymnt_Mode.equals("M"))))
        {
            decideConditionsMet529++;
            pnd_P.setValue(1);                                                                                                                                            //Natural: ASSIGN #P := 1
        }                                                                                                                                                                 //Natural: VALUE 'Q'
        else if (condition((cis_Cis_Pymnt_Mode.equals("Q"))))
        {
            decideConditionsMet529++;
            pnd_P.setValue(2);                                                                                                                                            //Natural: ASSIGN #P := 2
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((cis_Cis_Pymnt_Mode.equals("S"))))
        {
            decideConditionsMet529++;
            pnd_P.setValue(3);                                                                                                                                            //Natural: ASSIGN #P := 3
        }                                                                                                                                                                 //Natural: VALUE 'A'
        else if (condition((cis_Cis_Pymnt_Mode.equals("A"))))
        {
            decideConditionsMet529++;
            pnd_P.setValue(4);                                                                                                                                            //Natural: ASSIGN #P := 4
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet529 > 0))
        {
            if (condition(cis_Cis_Rqst_Id.equals("TPA")))                                                                                                                 //Natural: IF CIS-RQST-ID = 'TPA'
            {
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-TPA
                sub_Grnted_Amt_Tpa();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-TPA
                sub_Da_Proceed_Tpa();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-IPRO
                sub_Grnted_Amt_Ipro();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-IPRO
                sub_Da_Proceed_Ipro();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  TACC KG
    //*  TACC KG
    private void sub_All_Modes() throws Exception                                                                                                                         //Natural: ALL-MODES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_O_Grnted_Grd_Amt_Tot.compute(new ComputeParameters(false, pnd_O_Grnted_Grd_Amt_Tot), DbsField.add(getZero(),pnd_O_Grnted_Grd_Amt.getValue("*")));             //Natural: ASSIGN #O-GRNTED-GRD-AMT-TOT := 0 + #O-GRNTED-GRD-AMT ( * )
        pnd_O_Grnted_Std_Amt_Tot.compute(new ComputeParameters(false, pnd_O_Grnted_Std_Amt_Tot), DbsField.add(getZero(),pnd_O_Grnted_Std_Amt.getValue("*")));             //Natural: ASSIGN #O-GRNTED-STD-AMT-TOT := 0 + #O-GRNTED-STD-AMT ( * )
        pnd_O_Real_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_O_Real_Annuity.getValue(5)), DbsField.add(getZero(),pnd_O_Real_Annuity.getValue(1,        //Natural: ASSIGN #O-REAL-ANNUITY ( 5 ) := 0 + #O-REAL-ANNUITY ( 1:4 )
            ":",4)));
        pnd_O_Tacc_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_O_Tacc_Annuity.getValue(5)), DbsField.add(getZero(),pnd_O_Tacc_Annuity.getValue(1,        //Natural: ASSIGN #O-TACC-ANNUITY ( 5 ) := 0 + #O-TACC-ANNUITY ( 1:4 )
            ":",4)));
        pnd_O_Cref_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_O_Cref_Annuity.getValue(5)), DbsField.add(getZero(),pnd_O_Cref_Annuity.getValue(1,        //Natural: ASSIGN #O-CREF-ANNUITY ( 5 ) := 0 + #O-CREF-ANNUITY ( 1:4 )
            ":",4)));
        pnd_O_Trad_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_O_Trad_Annuity.getValue(5)), DbsField.add(getZero(),pnd_O_Trad_Annuity.getValue(1,        //Natural: ASSIGN #O-TRAD-ANNUITY ( 5 ) := 0 + #O-TRAD-ANNUITY ( 1:4 )
            ":",4)));
        pnd_O_Trad_Annuity_Tpa.getValue(5).compute(new ComputeParameters(false, pnd_O_Trad_Annuity_Tpa.getValue(5)), DbsField.add(getZero(),pnd_O_Trad_Annuity_Tpa.getValue(1, //Natural: ASSIGN #O-TRAD-ANNUITY-TPA ( 5 ) := 0 + #O-TRAD-ANNUITY-TPA ( 1:4 )
            ":",4)));
        pnd_O_Trad_Annuity_Ipro.getValue(5).compute(new ComputeParameters(false, pnd_O_Trad_Annuity_Ipro.getValue(5)), DbsField.add(getZero(),pnd_O_Trad_Annuity_Ipro.getValue(1, //Natural: ASSIGN #O-TRAD-ANNUITY-IPRO ( 5 ) := 0 + #O-TRAD-ANNUITY-IPRO ( 1:4 )
            ":",4)));
        pnd_O_No_Rec.getValue(5).compute(new ComputeParameters(false, pnd_O_No_Rec.getValue(5)), DbsField.add(getZero(),pnd_O_No_Rec.getValue(1,":",4)));                 //Natural: ASSIGN #O-NO-REC ( 5 ) := 0 + #O-NO-REC ( 1:4 )
        pnd_Grnted_Grd_Amt_Tot.compute(new ComputeParameters(false, pnd_Grnted_Grd_Amt_Tot), DbsField.add(getZero(),pnd_Grnted_Grd_Amt.getValue("*")));                   //Natural: ASSIGN #GRNTED-GRD-AMT-TOT := 0 + #GRNTED-GRD-AMT ( * )
        pnd_Grnted_Std_Amt_Tot.compute(new ComputeParameters(false, pnd_Grnted_Std_Amt_Tot), DbsField.add(getZero(),pnd_Grnted_Std_Amt.getValue("*")));                   //Natural: ASSIGN #GRNTED-STD-AMT-TOT := 0 + #GRNTED-STD-AMT ( * )
        pnd_Real_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_Real_Annuity.getValue(5)), DbsField.add(getZero(),pnd_Real_Annuity.getValue(1,              //Natural: ASSIGN #REAL-ANNUITY ( 5 ) := 0 + #REAL-ANNUITY ( 1:4 )
            ":",4)));
        pnd_Tacc_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_Tacc_Annuity.getValue(5)), DbsField.add(getZero(),pnd_Tacc_Annuity.getValue(1,              //Natural: ASSIGN #TACC-ANNUITY ( 5 ) := 0 + #TACC-ANNUITY ( 1:4 )
            ":",4)));
        pnd_Cref_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_Cref_Annuity.getValue(5)), DbsField.add(getZero(),pnd_Cref_Annuity.getValue(1,              //Natural: ASSIGN #CREF-ANNUITY ( 5 ) := 0 + #CREF-ANNUITY ( 1:4 )
            ":",4)));
        pnd_Trad_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_Trad_Annuity.getValue(5)), DbsField.add(getZero(),pnd_Trad_Annuity.getValue(1,              //Natural: ASSIGN #TRAD-ANNUITY ( 5 ) := 0 + #TRAD-ANNUITY ( 1:4 )
            ":",4)));
        pnd_Trad_Annuity_Tpa.getValue(5).compute(new ComputeParameters(false, pnd_Trad_Annuity_Tpa.getValue(5)), DbsField.add(getZero(),pnd_Trad_Annuity_Tpa.getValue(1,  //Natural: ASSIGN #TRAD-ANNUITY-TPA ( 5 ) := 0 + #TRAD-ANNUITY-TPA ( 1:4 )
            ":",4)));
        pnd_Trad_Annuity_Ipro.getValue(5).compute(new ComputeParameters(false, pnd_Trad_Annuity_Ipro.getValue(5)), DbsField.add(getZero(),pnd_Trad_Annuity_Ipro.getValue(1, //Natural: ASSIGN #TRAD-ANNUITY-IPRO ( 5 ) := 0 + #TRAD-ANNUITY-IPRO ( 1:4 )
            ":",4)));
        pnd_No_Rec.getValue(5).compute(new ComputeParameters(false, pnd_No_Rec.getValue(5)), DbsField.add(getZero(),pnd_No_Rec.getValue(1,":",4)));                       //Natural: ASSIGN #NO-REC ( 5 ) := 0 + #NO-REC ( 1:4 )
    }
    private void sub_Grnted_Amt() throws Exception                                                                                                                        //Natural: GRNTED-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Omni.getBoolean()))                                                                                                                             //Natural: IF #OMNI
        {
            pnd_O_Grnted_Grd_Amt.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                            //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #O-GRNTED-GRD-AMT ( #P )
            pnd_O_Grnted_Std_Amt.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                            //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #O-GRNTED-STD-AMT ( #P )
            pnd_O_Trad_Annuity.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                              //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #O-TRAD-ANNUITY ( #P )
            pnd_O_Trad_Annuity.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                              //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #O-TRAD-ANNUITY ( #P )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Grnted_Grd_Amt.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                              //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #GRNTED-GRD-AMT ( #P )
            pnd_Grnted_Std_Amt.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                              //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #GRNTED-STD-AMT ( #P )
            pnd_Trad_Annuity.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                                //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #TRAD-ANNUITY ( #P )
            pnd_Trad_Annuity.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                                //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #TRAD-ANNUITY ( #P )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Grnted_Amt_Ipro() throws Exception                                                                                                                   //Natural: GRNTED-AMT-IPRO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(cis_Cis_Corp_Sync_Ind.equals("S") || cis_Cis_Corp_Sync_Ind.equals("G")))                                                                            //Natural: IF CIS-CORP-SYNC-IND = 'S' OR = 'G'
        {
            pnd_O_Grnted_Std_Amt_Ipro.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                       //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #O-GRNTED-STD-AMT-IPRO ( #P )
            pnd_O_Grnted_Grd_Amt_Ipro.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                       //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #O-GRNTED-GRD-AMT-IPRO ( #P )
            pnd_O_Trad_Annuity_Ipro.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                         //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #O-TRAD-ANNUITY-IPRO ( #P )
            pnd_O_Trad_Annuity_Ipro.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                         //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #O-TRAD-ANNUITY-IPRO ( #P )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Grnted_Std_Amt_Ipro.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                         //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #GRNTED-STD-AMT-IPRO ( #P )
            pnd_Grnted_Grd_Amt_Ipro.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                         //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #GRNTED-GRD-AMT-IPRO ( #P )
            pnd_Trad_Annuity_Ipro.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                           //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #TRAD-ANNUITY-IPRO ( #P )
            pnd_Trad_Annuity_Ipro.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                           //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #TRAD-ANNUITY-IPRO ( #P )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Grnted_Amt_Tpa() throws Exception                                                                                                                    //Natural: GRNTED-AMT-TPA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(cis_Cis_Corp_Sync_Ind.equals("S") || cis_Cis_Corp_Sync_Ind.equals("G")))                                                                            //Natural: IF CIS-CORP-SYNC-IND = 'S' OR = 'G'
        {
            pnd_O_Grnted_Std_Amt_Tpa.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                        //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #O-GRNTED-STD-AMT-TPA ( #P )
            pnd_O_Grnted_Grd_Amt_Tpa.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                        //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #O-GRNTED-GRD-AMT-TPA ( #P )
            pnd_O_Trad_Annuity_Tpa.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                          //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #O-TRAD-ANNUITY-TPA ( #P )
            pnd_O_Trad_Annuity_Tpa.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                          //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #O-TRAD-ANNUITY-TPA ( #P )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Grnted_Std_Amt_Tpa.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                          //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #GRNTED-STD-AMT-TPA ( #P )
            pnd_Grnted_Grd_Amt_Tpa.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                          //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #GRNTED-GRD-AMT-TPA ( #P )
            pnd_Trad_Annuity_Tpa.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                            //Natural: ADD CIS.CIS-GRNTED-STD-AMT TO #TRAD-ANNUITY-TPA ( #P )
            pnd_Trad_Annuity_Tpa.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                            //Natural: ADD CIS.CIS-GRNTED-GRD-AMT TO #TRAD-ANNUITY-TPA ( #P )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Value_Code() throws Exception                                                                                                                        //Natural: VALUE-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Omni.getBoolean()))                                                                                                                             //Natural: IF #OMNI
        {
            pnd_O_M_Tiaa_Rea.getValue(pnd_P).nadd(cis_Cis_Rea_Mnthly_Nbr_Units);                                                                                          //Natural: ADD CIS.CIS-REA-MNTHLY-NBR-UNITS TO #O-M-TIAA-REA ( #P )
            pnd_O_M_Tiaa_Rea.getValue(5).nadd(cis_Cis_Rea_Mnthly_Nbr_Units);                                                                                              //Natural: ADD CIS.CIS-REA-MNTHLY-NBR-UNITS TO #O-M-TIAA-REA ( 5 )
            pnd_O_A_Tiaa_Rea.getValue(pnd_P).nadd(cis_Cis_Rea_Annual_Nbr_Units);                                                                                          //Natural: ADD CIS.CIS-REA-ANNUAL-NBR-UNITS TO #O-A-TIAA-REA ( #P )
            pnd_O_A_Tiaa_Rea.getValue(5).nadd(cis_Cis_Rea_Annual_Nbr_Units);                                                                                              //Natural: ADD CIS.CIS-REA-ANNUAL-NBR-UNITS TO #O-A-TIAA-REA ( 5 )
            pnd_O_Real_Annuity.getValue(pnd_P).compute(new ComputeParameters(false, pnd_O_Real_Annuity.getValue(pnd_P)), pnd_O_M_Tiaa_Rea.getValue(pnd_P).add(pnd_O_A_Tiaa_Rea.getValue(pnd_P))); //Natural: ASSIGN #O-REAL-ANNUITY ( #P ) := #O-M-TIAA-REA ( #P ) + #O-A-TIAA-REA ( #P )
            FOR01:                                                                                                                                                        //Natural: FOR #C 1 20
            for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
            {
                //*  MMA
                short decideConditionsMet625 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF CIS.CIS-CREF-ACCT-CDE ( #C );//Natural: VALUE 'M'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("M")))
                {
                    decideConditionsMet625++;
                    pnd_O_M_Mma.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                      //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-MMA ( #P )
                    pnd_O_A_Mma.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                      //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-MMA ( #P )
                    pnd_O_M_Mma.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-MMA ( 5 )
                    //*  STOCK
                    pnd_O_A_Mma.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-MMA ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'C'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("C")))
                {
                    decideConditionsMet625++;
                    pnd_O_M_Stock.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                    //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-STOCK ( #P )
                    pnd_O_A_Stock.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                    //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-STOCK ( #P )
                    pnd_O_M_Stock.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                        //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-STOCK ( 5 )
                    //*  SOCIAL CHOICE
                    pnd_O_A_Stock.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                        //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-STOCK ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'S'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("S")))
                {
                    decideConditionsMet625++;
                    pnd_O_M_Social.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                   //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-SOCIAL ( #P )
                    pnd_O_A_Social.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                   //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-SOCIAL ( #P )
                    pnd_O_M_Social.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-SOCIAL ( 5 )
                    //*  GLOBAL
                    pnd_O_A_Social.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-SOCIAL ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'W'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("W")))
                {
                    decideConditionsMet625++;
                    pnd_O_M_Global.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                   //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-GLOBAL ( #P )
                    pnd_O_A_Global.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                   //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-GLOBAL ( #P )
                    pnd_O_M_Global.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-GLOBAL ( 5 )
                    //*  BOND
                    pnd_O_A_Global.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-GLOBAL ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'B'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("B")))
                {
                    decideConditionsMet625++;
                    pnd_O_M_Bond.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                     //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-BOND ( #P )
                    pnd_O_A_Bond.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                     //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-BOND ( #P )
                    pnd_O_M_Bond.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-BOND ( 5 )
                    //*  GROWTH
                    pnd_O_A_Bond.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-BOND ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'L'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("L")))
                {
                    decideConditionsMet625++;
                    pnd_O_M_Grth.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                     //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-GRTH ( #P )
                    pnd_O_A_Grth.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                     //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-GRTH ( #P )
                    pnd_O_M_Grth.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-GRTH ( 5 )
                    //*  EQUITY INDEX
                    pnd_O_A_Grth.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-GRTH ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'E'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("E")))
                {
                    decideConditionsMet625++;
                    pnd_O_M_Equ.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                      //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-EQU ( #P )
                    pnd_O_A_Equ.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                      //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-EQU ( #P )
                    pnd_O_M_Equ.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-EQU ( 5 )
                    //*  ILB
                    pnd_O_A_Equ.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-EQU ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("I")))
                {
                    decideConditionsMet625++;
                    pnd_O_M_Ilb.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                      //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-ILB ( #P )
                    pnd_O_A_Ilb.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                      //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-ILB ( #P )
                    pnd_O_M_Ilb.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #O-M-ILB ( 5 )
                    pnd_O_A_Ilb.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-A-ILB ( 5 )
                }                                                                                                                                                         //Natural: NONE VALUE
                if (condition(decideConditionsMet625 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  TACC KG START
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_O_Cref_Annuity.getValue(pnd_P).compute(new ComputeParameters(false, pnd_O_Cref_Annuity.getValue(pnd_P)), pnd_O_M_Mma.getValue(pnd_P).add(pnd_O_M_Stock.getValue(pnd_P)).add(pnd_O_M_Social.getValue(pnd_P)).add(pnd_O_M_Global.getValue(pnd_P)).add(pnd_O_M_Bond.getValue(pnd_P)).add(pnd_O_M_Grth.getValue(pnd_P)).add(pnd_O_M_Equ.getValue(pnd_P)).add(pnd_O_M_Ilb.getValue(pnd_P)).add(pnd_O_A_Mma.getValue(pnd_P)).add(pnd_O_A_Stock.getValue(pnd_P)).add(pnd_O_A_Social.getValue(pnd_P)).add(pnd_O_A_Global.getValue(pnd_P)).add(pnd_O_A_Bond.getValue(pnd_P)).add(pnd_O_A_Grth.getValue(pnd_P)).add(pnd_O_A_Equ.getValue(pnd_P)).add(pnd_O_A_Ilb.getValue(pnd_P))); //Natural: ASSIGN #O-CREF-ANNUITY ( #P ) := #O-M-MMA ( #P ) + #O-M-STOCK ( #P ) + #O-M-SOCIAL ( #P ) + #O-M-GLOBAL ( #P ) + #O-M-BOND ( #P ) + #O-M-GRTH ( #P ) + #O-M-EQU ( #P ) + #O-M-ILB ( #P ) + #O-A-MMA ( #P ) + #O-A-STOCK ( #P ) + #O-A-SOCIAL ( #P ) + #O-A-GLOBAL ( #P ) + #O-A-BOND ( #P ) + #O-A-GRTH ( #P ) + #O-A-EQU ( #P ) + #O-A-ILB ( #P )
            FOR02:                                                                                                                                                        //Natural: FOR #D 1 100
            for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(100)); pnd_D.nadd(1))
            {
                pnd_O_M_Taccess.getValue(pnd_P).nadd(cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D));                                                                      //Natural: ASSIGN #O-M-TACCESS ( #P ) := #O-M-TACCESS ( #P ) + CIS.CIS-TACC-MNTHLY-NBR-UNITS ( #D )
                pnd_O_A_Taccess.getValue(pnd_P).nadd(cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D));                                                                      //Natural: ASSIGN #O-A-TACCESS ( #P ) := #O-A-TACCESS ( #P ) + CIS.CIS-TACC-ANNUAL-NBR-UNITS ( #D )
                pnd_O_M_Taccess.getValue(5).nadd(cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ADD CIS.CIS-TACC-MNTHLY-NBR-UNITS ( #C ) TO #O-M-TACCESS ( 5 )
                pnd_O_A_Taccess.getValue(5).nadd(cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ADD CIS.CIS-TACC-ANNUAL-NBR-UNITS ( #C ) TO #O-A-TACCESS ( 5 )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_O_Tacc_Annuity.getValue(pnd_P).compute(new ComputeParameters(false, pnd_O_Tacc_Annuity.getValue(pnd_P)), pnd_O_M_Taccess.getValue(pnd_P).add(pnd_O_A_Taccess.getValue(pnd_P))); //Natural: ASSIGN #O-TACC-ANNUITY ( #P ) := #O-M-TACCESS ( #P ) + #O-A-TACCESS ( #P )
            //*                                                        /* TACC KG END
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_M_Tiaa_Rea.getValue(pnd_P).nadd(cis_Cis_Rea_Mnthly_Nbr_Units);                                                                                            //Natural: ADD CIS.CIS-REA-MNTHLY-NBR-UNITS TO #M-TIAA-REA ( #P )
            pnd_M_Tiaa_Rea.getValue(5).nadd(cis_Cis_Rea_Mnthly_Nbr_Units);                                                                                                //Natural: ADD CIS.CIS-REA-MNTHLY-NBR-UNITS TO #M-TIAA-REA ( 5 )
            pnd_A_Tiaa_Rea.getValue(pnd_P).nadd(cis_Cis_Rea_Annual_Nbr_Units);                                                                                            //Natural: ADD CIS.CIS-REA-ANNUAL-NBR-UNITS TO #A-TIAA-REA ( #P )
            pnd_A_Tiaa_Rea.getValue(5).nadd(cis_Cis_Rea_Annual_Nbr_Units);                                                                                                //Natural: ADD CIS.CIS-REA-ANNUAL-NBR-UNITS TO #A-TIAA-REA ( 5 )
            pnd_Real_Annuity.getValue(pnd_P).compute(new ComputeParameters(false, pnd_Real_Annuity.getValue(pnd_P)), pnd_M_Tiaa_Rea.getValue(pnd_P).add(pnd_A_Tiaa_Rea.getValue(pnd_P))); //Natural: ASSIGN #REAL-ANNUITY ( #P ) := #M-TIAA-REA ( #P ) + #A-TIAA-REA ( #P )
            FOR03:                                                                                                                                                        //Natural: FOR #C 1 20
            for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
            {
                //*  MMA
                short decideConditionsMet695 = 0;                                                                                                                         //Natural: DECIDE ON EVERY VALUE OF CIS.CIS-CREF-ACCT-CDE ( #C );//Natural: VALUE 'M'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("M")))
                {
                    decideConditionsMet695++;
                    pnd_M_Mma.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                        //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-MMA ( #P )
                    pnd_A_Mma.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                        //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-MMA ( #P )
                    pnd_M_Mma.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-MMA ( 5 )
                    //*  STOCK
                    pnd_A_Mma.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-MMA ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'C'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("C")))
                {
                    decideConditionsMet695++;
                    pnd_M_Stock.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                      //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-STOCK ( #P )
                    pnd_A_Stock.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                      //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-STOCK ( #P )
                    pnd_M_Stock.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-STOCK ( 5 )
                    //*  SOCIAL CHOICE
                    pnd_A_Stock.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-STOCK ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'S'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("S")))
                {
                    decideConditionsMet695++;
                    pnd_M_Social.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                     //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-SOCIAL ( #P )
                    pnd_A_Social.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                     //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-SOCIAL ( #P )
                    pnd_M_Social.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-SOCIAL ( 5 )
                    //*  GLOBAL
                    pnd_A_Social.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-SOCIAL ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'W'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("W")))
                {
                    decideConditionsMet695++;
                    pnd_M_Global.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                     //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-GLOBAL ( #P )
                    pnd_A_Global.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                     //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-GLOBAL ( #P )
                    pnd_M_Global.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-GLOBAL ( 5 )
                    //*  BOND
                    pnd_A_Global.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-GLOBAL ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'B'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("B")))
                {
                    decideConditionsMet695++;
                    pnd_M_Bond.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-BOND ( #P )
                    pnd_A_Bond.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-BOND ( #P )
                    pnd_M_Bond.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-BOND ( 5 )
                    //*  GROWTH
                    pnd_A_Bond.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-BOND ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'L'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("L")))
                {
                    decideConditionsMet695++;
                    pnd_M_Grth.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-GRTH ( #P )
                    pnd_A_Grth.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-GRTH ( #P )
                    pnd_M_Grth.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-GRTH ( 5 )
                    //*  EQUITY INDEX
                    pnd_A_Grth.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-GRTH ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'E'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("E")))
                {
                    decideConditionsMet695++;
                    pnd_M_Equ.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                        //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-EQU ( #P )
                    pnd_A_Equ.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                        //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-EQU ( #P )
                    pnd_M_Equ.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-EQU ( 5 )
                    //*  ILB
                    pnd_A_Equ.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-EQU ( 5 )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("I")))
                {
                    decideConditionsMet695++;
                    pnd_M_Ilb.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                        //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-ILB ( #P )
                    pnd_A_Ilb.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                        //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-ILB ( #P )
                    pnd_M_Ilb.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-ILB ( 5 )
                    pnd_A_Ilb.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-ILB ( 5 )
                }                                                                                                                                                         //Natural: NONE VALUE
                if (condition(decideConditionsMet695 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  TACC KG START
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Cref_Annuity.getValue(pnd_P).compute(new ComputeParameters(false, pnd_Cref_Annuity.getValue(pnd_P)), pnd_M_Mma.getValue(pnd_P).add(pnd_M_Stock.getValue(pnd_P)).add(pnd_M_Social.getValue(pnd_P)).add(pnd_M_Global.getValue(pnd_P)).add(pnd_M_Bond.getValue(pnd_P)).add(pnd_M_Grth.getValue(pnd_P)).add(pnd_M_Equ.getValue(pnd_P)).add(pnd_M_Ilb.getValue(pnd_P)).add(pnd_A_Mma.getValue(pnd_P)).add(pnd_A_Stock.getValue(pnd_P)).add(pnd_A_Social.getValue(pnd_P)).add(pnd_A_Global.getValue(pnd_P)).add(pnd_A_Bond.getValue(pnd_P)).add(pnd_A_Grth.getValue(pnd_P)).add(pnd_A_Equ.getValue(pnd_P)).add(pnd_A_Ilb.getValue(pnd_P))); //Natural: ASSIGN #CREF-ANNUITY ( #P ) := #M-MMA ( #P ) + #M-STOCK ( #P ) + #M-SOCIAL ( #P ) + #M-GLOBAL ( #P ) + #M-BOND ( #P ) + #M-GRTH ( #P ) + #M-EQU ( #P ) + #M-ILB ( #P ) + #A-MMA ( #P ) + #A-STOCK ( #P ) + #A-SOCIAL ( #P ) + #A-GLOBAL ( #P ) + #A-BOND ( #P ) + #A-GRTH ( #P ) + #A-EQU ( #P ) + #A-ILB ( #P )
            FOR04:                                                                                                                                                        //Natural: FOR #D 1 100
            for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(100)); pnd_D.nadd(1))
            {
                pnd_M_Taccess.getValue(pnd_P).nadd(cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D));                                                                        //Natural: ASSIGN #M-TACCESS ( #P ) := #M-TACCESS ( #P ) + CIS.CIS-TACC-MNTHLY-NBR-UNITS ( #D )
                pnd_A_Taccess.getValue(pnd_P).nadd(cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D));                                                                        //Natural: ASSIGN #A-TACCESS ( #P ) := #A-TACCESS ( #P ) + CIS.CIS-TACC-ANNUAL-NBR-UNITS ( #D )
                pnd_M_Taccess.getValue(5).nadd(cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D));                                                                            //Natural: ADD CIS.CIS-TACC-MNTHLY-NBR-UNITS ( #D ) TO #M-TACCESS ( 5 )
                pnd_A_Taccess.getValue(5).nadd(cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D));                                                                            //Natural: ADD CIS.CIS-TACC-ANNUAL-NBR-UNITS ( #D ) TO #A-TACCESS ( 5 )
                //*  TACC KG END
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Tacc_Annuity.getValue(pnd_P).compute(new ComputeParameters(false, pnd_Tacc_Annuity.getValue(pnd_P)), pnd_M_Taccess.getValue(pnd_P).add(pnd_A_Taccess.getValue(pnd_P))); //Natural: ASSIGN #TACC-ANNUITY ( #P ) := #M-TACCESS ( #P ) + #A-TACCESS ( #P )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Da_Proceed() throws Exception                                                                                                                        //Natural: DA-PROCEED
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Omni.getBoolean()))                                                                                                                             //Natural: IF #OMNI
        {
            if (condition(cis_Cis_Cntrct_Type.equals("T")))                                                                                                               //Natural: IF CIS.CIS-CNTRCT-TYPE = 'T'
            {
                pnd_O_Contract_Issued.getValue(pnd_P).nadd(1);                                                                                                            //Natural: ADD 1 TO #O-CONTRACT-ISSUED ( #P )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cis_Cis_Cntrct_Type.equals("B")))                                                                                                           //Natural: IF CIS.CIS-CNTRCT-TYPE = 'B'
                {
                    pnd_O_Contract_Issued.getValue(pnd_P).nadd(1);                                                                                                        //Natural: ADD 1 TO #O-CONTRACT-ISSUED ( #P )
                    pnd_O_Certif_Issued.getValue(pnd_P).nadd(1);                                                                                                          //Natural: ADD 1 TO #O-CERTIF-ISSUED ( #P )
                    pnd_O_Certif_Issued2.getValue(pnd_P).nadd(1);                                                                                                         //Natural: ADD 1 TO #O-CERTIF-ISSUED2 ( #P )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            FOR05:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                         //Natural: IF CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) NE 0
                {
                    pnd_O_Da_Tiaa_Proceeds_Amt.getValue(pnd_P).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                        //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #O-DA-TIAA-PROCEEDS-AMT ( #P )
                    pnd_O_Da_Tiaa_Proceeds_Amt.getValue(5).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                            //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #O-DA-TIAA-PROCEEDS-AMT ( 5 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                          //Natural: IF CIS.CIS-DA-REA-PROCEEDS-AMT ( #I ) NE 0
                {
                    pnd_O_Da_Rea_Proceeds_Amt.getValue(pnd_P).nadd(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I));                                                          //Natural: ADD CIS.CIS-DA-REA-PROCEEDS-AMT ( #I ) TO #O-DA-REA-PROCEEDS-AMT ( #P )
                    pnd_O_Da_Rea_Proceeds_Amt.getValue(5).nadd(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I));                                                              //Natural: ADD CIS.CIS-DA-REA-PROCEEDS-AMT ( #I ) TO #O-DA-REA-PROCEEDS-AMT ( 5 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                         //Natural: IF CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I ) NE 0
                {
                    pnd_O_Da_Cref_Proceeds_Amt.getValue(pnd_P).nadd(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I));                                                        //Natural: ADD CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I ) TO #O-DA-CREF-PROCEEDS-AMT ( #P )
                    pnd_O_Da_Cref_Proceeds_Amt.getValue(5).nadd(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I));                                                            //Natural: ADD CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I ) TO #O-DA-CREF-PROCEEDS-AMT ( 5 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cis_Cis_Cntrct_Type.equals("T")))                                                                                                               //Natural: IF CIS.CIS-CNTRCT-TYPE = 'T'
            {
                pnd_Contract_Issued.getValue(pnd_P).nadd(1);                                                                                                              //Natural: ADD 1 TO #CONTRACT-ISSUED ( #P )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cis_Cis_Cntrct_Type.equals("B")))                                                                                                           //Natural: IF CIS.CIS-CNTRCT-TYPE = 'B'
                {
                    pnd_Contract_Issued.getValue(pnd_P).nadd(1);                                                                                                          //Natural: ADD 1 TO #CONTRACT-ISSUED ( #P )
                    pnd_Certif_Issued.getValue(pnd_P).nadd(1);                                                                                                            //Natural: ADD 1 TO #CERTIF-ISSUED ( #P )
                    pnd_Certif_Issued2.getValue(pnd_P).nadd(1);                                                                                                           //Natural: ADD 1 TO #CERTIF-ISSUED2 ( #P )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            FOR06:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                         //Natural: IF CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) NE 0
                {
                    pnd_Da_Tiaa_Proceeds_Amt.getValue(pnd_P).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                          //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #DA-TIAA-PROCEEDS-AMT ( #P )
                    pnd_Da_Tiaa_Proceeds_Amt.getValue(5).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                              //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #DA-TIAA-PROCEEDS-AMT ( 5 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                          //Natural: IF CIS.CIS-DA-REA-PROCEEDS-AMT ( #I ) NE 0
                {
                    pnd_Da_Rea_Proceeds_Amt.getValue(pnd_P).nadd(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I));                                                            //Natural: ADD CIS.CIS-DA-REA-PROCEEDS-AMT ( #I ) TO #DA-REA-PROCEEDS-AMT ( #P )
                    pnd_Da_Rea_Proceeds_Amt.getValue(5).nadd(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I));                                                                //Natural: ADD CIS.CIS-DA-REA-PROCEEDS-AMT ( #I ) TO #DA-REA-PROCEEDS-AMT ( 5 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                         //Natural: IF CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I ) NE 0
                {
                    pnd_Da_Cref_Proceeds_Amt.getValue(pnd_P).nadd(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I));                                                          //Natural: ADD CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I ) TO #DA-CREF-PROCEEDS-AMT ( #P )
                    pnd_Da_Cref_Proceeds_Amt.getValue(5).nadd(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I));                                                              //Natural: ADD CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I ) TO #DA-CREF-PROCEEDS-AMT ( 5 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Da_Proceed_Tpa() throws Exception                                                                                                                    //Natural: DA-PROCEED-TPA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(cis_Cis_Corp_Sync_Ind.equals("S") || cis_Cis_Corp_Sync_Ind.equals("G")))                                                                            //Natural: IF CIS-CORP-SYNC-IND = 'S' OR = 'G'
        {
            if (condition(cis_Cis_Cntrct_Type.equals("T") || cis_Cis_Cntrct_Type.equals("B")))                                                                            //Natural: IF CIS.CIS-CNTRCT-TYPE = 'T' OR = 'B'
            {
                pnd_O_Contract_Issued_Tpa.getValue(pnd_P).nadd(1);                                                                                                        //Natural: ADD 1 TO #O-CONTRACT-ISSUED-TPA ( #P )
            }                                                                                                                                                             //Natural: END-IF
            FOR07:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                         //Natural: IF CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) NE 0
                {
                    pnd_O_Da_Tiaa_Proceeds_Amt_Tpa.getValue(pnd_P).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                    //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #O-DA-TIAA-PROCEEDS-AMT-TPA ( #P )
                    pnd_O_Da_Tiaa_Proceeds_Amt_Tpa.getValue(5).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                        //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #O-DA-TIAA-PROCEEDS-AMT-TPA ( 5 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cis_Cis_Cntrct_Type.equals("T") || cis_Cis_Cntrct_Type.equals("B")))                                                                            //Natural: IF CIS.CIS-CNTRCT-TYPE = 'T' OR = 'B'
            {
                pnd_Contract_Issued_Tpa.getValue(pnd_P).nadd(1);                                                                                                          //Natural: ADD 1 TO #CONTRACT-ISSUED-TPA ( #P )
            }                                                                                                                                                             //Natural: END-IF
            FOR08:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                         //Natural: IF CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) NE 0
                {
                    pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(pnd_P).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                      //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #DA-TIAA-PROCEEDS-AMT-TPA ( #P )
                    pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(5).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                          //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #DA-TIAA-PROCEEDS-AMT-TPA ( 5 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Da_Proceed_Ipro() throws Exception                                                                                                                   //Natural: DA-PROCEED-IPRO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(cis_Cis_Corp_Sync_Ind.equals("S") || cis_Cis_Corp_Sync_Ind.equals("G")))                                                                            //Natural: IF CIS-CORP-SYNC-IND = 'S' OR = 'G'
        {
            if (condition(cis_Cis_Cntrct_Type.equals("T") || cis_Cis_Cntrct_Type.equals("B")))                                                                            //Natural: IF CIS.CIS-CNTRCT-TYPE = 'T' OR = 'B'
            {
                pnd_O_Contract_Issued_Ipro.getValue(pnd_P).nadd(1);                                                                                                       //Natural: ADD 1 TO #O-CONTRACT-ISSUED-IPRO ( #P )
            }                                                                                                                                                             //Natural: END-IF
            FOR09:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                         //Natural: IF CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) NE 0
                {
                    pnd_O_Da_Tiaa_Proceeds_Amt_Ipro.getValue(pnd_P).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                   //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #O-DA-TIAA-PROCEEDS-AMT-IPRO ( #P )
                    pnd_O_Da_Tiaa_Proceeds_Amt_Ipro.getValue(5).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                       //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #O-DA-TIAA-PROCEEDS-AMT-IPRO ( 5 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cis_Cis_Cntrct_Type.equals("T") || cis_Cis_Cntrct_Type.equals("B")))                                                                            //Natural: IF CIS.CIS-CNTRCT-TYPE = 'T' OR = 'B'
            {
                pnd_Contract_Issued_Ipro.getValue(pnd_P).nadd(1);                                                                                                         //Natural: ADD 1 TO #CONTRACT-ISSUED-IPRO ( #P )
            }                                                                                                                                                             //Natural: END-IF
            FOR10:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                         //Natural: IF CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) NE 0
                {
                    pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(pnd_P).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                     //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #DA-TIAA-PROCEEDS-AMT-IPRO ( #P )
                    pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(5).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                         //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #DA-TIAA-PROCEEDS-AMT-IPRO ( 5 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ia_Report() throws Exception                                                                                                                         //Natural: IA-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
                                                                                                                                                                          //Natural: PERFORM ISSUED-TOTALS
        sub_Issued_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  LEGACY
        if (condition(pnd_No_Rec.getValue(5).greater(getZero())))                                                                                                         //Natural: IF #NO-REC ( 5 ) GT 0
        {
            getReports().write(1, ReportOption.NOTITLE,"INCOME BENEFITS", new FieldAttributes ("AD=I"),NEWLINE,"---------------");                                        //Natural: WRITE ( 1 ) 'INCOME BENEFITS' ( AD = I ) / '---------------'
            if (Global.isEscape()) return;
            getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Standard",                                                          //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 'TRADITIONAL ANNUITY - Standard' 1X #GRNTED-STD-AMT ( 1 ) ( IC = $ ) #GRNTED-STD-AMT ( 2 ) ( IC = $ ) #GRNTED-STD-AMT ( 3 ) ( IC = $ ) #GRNTED-STD-AMT ( 4 ) ( IC = $ ) 4X #GRNTED-STD-AMT-TOT ( IC = $ )
            		new ColumnSpacing(1),pnd_Grnted_Std_Amt.getValue(1), new InsertionCharacter("$"),pnd_Grnted_Std_Amt.getValue(2), new InsertionCharacter("$"),pnd_Grnted_Std_Amt.getValue(3), 
                new InsertionCharacter("$"),pnd_Grnted_Std_Amt.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Grnted_Std_Amt_Tot, new 
                InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Graded  ",                                                          //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 'TRADITIONAL ANNUITY - Graded  ' 1X #GRNTED-GRD-AMT ( 1 ) ( IC = $ ) #GRNTED-GRD-AMT ( 2 ) ( IC = $ ) #GRNTED-GRD-AMT ( 3 ) ( IC = $ ) #GRNTED-GRD-AMT ( 4 ) ( IC = $ ) 4X #GRNTED-GRD-AMT-TOT ( IC = $ )
            		new ColumnSpacing(1),pnd_Grnted_Grd_Amt.getValue(1), new InsertionCharacter("$"),pnd_Grnted_Grd_Amt.getValue(2), new InsertionCharacter("$"),pnd_Grnted_Grd_Amt.getValue(3), 
                new InsertionCharacter("$"),pnd_Grnted_Grd_Amt.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Grnted_Grd_Amt_Tot, new 
                InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,"ANNUAL REVALUATION", new FieldAttributes ("AD=I"),NEWLINE,"------------------",NEWLINE,"TIAA REA          ",      //Natural: WRITE ( 1 ) 'ANNUAL REVALUATION' ( AD = I ) / '------------------' / 'TIAA REA          ' ( AD = I ) 15X #A-TIAA-REA ( 1 ) 2X #A-TIAA-REA ( 2 ) 2X #A-TIAA-REA ( 3 ) 2X #A-TIAA-REA ( 4 ) 5X #A-TIAA-REA ( 5 ) / 'TIAA ACCESS       ' 15X #A-TACCESS ( 1 ) 2X #A-TACCESS ( 2 ) 2X #A-TACCESS ( 3 ) 2X #A-TACCESS ( 4 ) 5X #A-TACCESS ( 5 ) // 'CREF Stock        ' 15X #A-STOCK ( 1 ) 2X #A-STOCK ( 2 ) 2X #A-STOCK ( 3 ) 2X #A-STOCK ( 4 ) 5X #A-STOCK ( 5 ) / 'CREF MMA          ' 15X #A-MMA ( 1 ) 2X #A-MMA ( 2 ) 2X #A-MMA ( 3 ) 2X #A-MMA ( 4 ) 5X #A-MMA ( 5 ) / 'CREF Social Choice' 15X #A-SOCIAL ( 1 ) 2X #A-SOCIAL ( 2 ) 2X #A-SOCIAL ( 3 ) 2X #A-SOCIAL ( 4 ) 5X #A-SOCIAL ( 5 ) / 'CREF Bond         ' 15X #A-BOND ( 1 ) 2X #A-BOND ( 2 ) 2X #A-BOND ( 3 ) 2X #A-BOND ( 4 ) 5X #A-BOND ( 5 ) / 'CREF Global       ' 15X #A-GLOBAL ( 1 ) 2X #A-GLOBAL ( 2 ) 2X #A-GLOBAL ( 3 ) 2X #A-GLOBAL ( 4 ) 5X #A-GLOBAL ( 5 ) / 'CREF Growth       ' 15X #A-GRTH ( 1 ) 2X #A-GRTH ( 2 ) 2X #A-GRTH ( 3 ) 2X #A-GRTH ( 4 ) 5X #A-GRTH ( 5 ) / 'CREF Equity Index ' 15X #A-EQU ( 1 ) 2X #A-EQU ( 2 ) 2X #A-EQU ( 3 ) 2X #A-EQU ( 4 ) 5X #A-EQU ( 5 ) / 'CREF ILB          ' 15X #A-ILB ( 1 ) 2X #A-ILB ( 2 ) 2X #A-ILB ( 3 ) 2X #A-ILB ( 4 ) 5X #A-ILB ( 5 )
                new FieldAttributes ("AD=I"),new ColumnSpacing(15),pnd_A_Tiaa_Rea.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Tiaa_Rea.getValue(2), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Tiaa_Rea.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Tiaa_Rea.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Tiaa_Rea.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA ACCESS       ",new 
                ColumnSpacing(15),pnd_A_Taccess.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Taccess.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Taccess.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Taccess.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Taccess.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"CREF Stock        ",new 
                ColumnSpacing(15),pnd_A_Stock.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Stock.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Stock.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Stock.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Stock.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF MMA          ",new 
                ColumnSpacing(15),pnd_A_Mma.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Mma.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Mma.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Mma.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Mma.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Social Choice",new 
                ColumnSpacing(15),pnd_A_Social.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Social.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Social.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Social.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Social.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Bond         ",new 
                ColumnSpacing(15),pnd_A_Bond.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Bond.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Bond.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Bond.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Bond.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Global       ",new 
                ColumnSpacing(15),pnd_A_Global.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Global.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Global.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Global.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Global.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Growth       ",new 
                ColumnSpacing(15),pnd_A_Grth.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Grth.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Grth.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Grth.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Grth.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Equity Index ",new 
                ColumnSpacing(15),pnd_A_Equ.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Equ.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Equ.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Equ.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Equ.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF ILB          ",new 
                ColumnSpacing(15),pnd_A_Ilb.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Ilb.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Ilb.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Ilb.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Ilb.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            //*  HK 12/21/00
            getReports().write(1, ReportOption.NOTITLE,"MONTHLY REVALUATION",NEWLINE,"-------------------",NEWLINE,"TIAA REA          ",new ColumnSpacing(15),pnd_M_Tiaa_Rea.getValue(1),  //Natural: WRITE ( 1 ) 'MONTHLY REVALUATION' / '-------------------' / 'TIAA REA          ' 15X #M-TIAA-REA ( 1 ) 2X #M-TIAA-REA ( 2 ) 2X #M-TIAA-REA ( 3 ) 2X #M-TIAA-REA ( 4 ) 5X #M-TIAA-REA ( 5 ) / 'TIAA ACCESS       ' 15X #M-TACCESS ( 1 ) 2X #M-TACCESS ( 2 ) 2X #M-TACCESS ( 3 ) 2X #M-TACCESS ( 4 ) 5X #M-TACCESS ( 5 ) // 'CREF Stock        ' 15X #M-STOCK ( 1 ) 2X #M-STOCK ( 2 ) 2X #M-STOCK ( 3 ) 2X #M-STOCK ( 4 ) 5X #M-STOCK ( 5 ) / 'CREF MMA          ' 15X #M-MMA ( 1 ) 2X #M-MMA ( 2 ) 2X #M-MMA ( 3 ) 2X #M-MMA ( 4 ) 5X #M-MMA ( 5 ) / 'CREF Social Choice' 15X #M-SOCIAL ( 1 ) 2X #M-SOCIAL ( 2 ) 2X #M-SOCIAL ( 3 ) 2X #M-SOCIAL ( 4 ) 5X #M-SOCIAL ( 5 ) / 'CREF Bond         ' 15X #M-BOND ( 1 ) 2X #M-BOND ( 2 ) 2X #M-BOND ( 3 ) 2X #M-BOND ( 4 ) 5X #M-BOND ( 5 ) / 'CREF Global       ' 15X #M-GLOBAL ( 1 ) 2X #M-GLOBAL ( 2 ) 2X #M-GLOBAL ( 3 ) 2X #M-GLOBAL ( 4 ) 5X #M-GLOBAL ( 5 ) / 'CREF Growth       ' 15X #M-GRTH ( 1 ) 2X #M-GRTH ( 2 ) 2X #M-GRTH ( 3 ) 2X #M-GRTH ( 4 ) 5X #M-GRTH ( 5 ) / 'CREF Equity Index ' 15X #M-EQU ( 1 ) 2X #M-EQU ( 2 ) 2X #M-EQU ( 3 ) 2X #M-EQU ( 4 ) 5X #M-EQU ( 5 ) / 'CREF ILB          ' 15X #M-ILB ( 1 ) 2X #M-ILB ( 2 ) 2X #M-ILB ( 3 ) 2X #M-ILB ( 4 ) 5X #M-ILB ( 5 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Tiaa_Rea.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Tiaa_Rea.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Tiaa_Rea.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Tiaa_Rea.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA ACCESS       ",new ColumnSpacing(15),pnd_M_Taccess.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_M_Taccess.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Taccess.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Taccess.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Taccess.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"CREF Stock        ",new ColumnSpacing(15),pnd_M_Stock.getValue(1), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Stock.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Stock.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Stock.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Stock.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF MMA          ",new ColumnSpacing(15),pnd_M_Mma.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_M_Mma.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Mma.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Mma.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Mma.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Social Choice",new ColumnSpacing(15),pnd_M_Social.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_M_Social.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Social.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Social.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Social.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Bond         ",new ColumnSpacing(15),pnd_M_Bond.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_M_Bond.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Bond.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Bond.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Bond.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Global       ",new ColumnSpacing(15),pnd_M_Global.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_M_Global.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Global.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Global.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Global.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Growth       ",new ColumnSpacing(15),pnd_M_Grth.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_M_Grth.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Grth.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Grth.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Grth.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Equity Index ",new ColumnSpacing(15),pnd_M_Equ.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_M_Equ.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Equ.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Equ.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Equ.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF ILB          ",new ColumnSpacing(15),pnd_M_Ilb.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_M_Ilb.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Ilb.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Ilb.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Ilb.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,"SUMMARY TOTALS",new TabSetting(103),"GRAND TOTALS",NEWLINE,"--------------",new TabSetting(103),"------------",NEWLINE,"TRADITIONAL ANNUITY",NEWLINE,"-------------------",NEWLINE,new  //Natural: WRITE ( 1 ) 'SUMMARY TOTALS' 103T 'GRAND TOTALS' / '--------------' 103T '------------' / 'TRADITIONAL ANNUITY' / '-------------------' / 2X 'Contracts Issued      ' 16X #CONTRACT-ISSUED ( 1 ) 9X #CONTRACT-ISSUED ( 2 ) 9X #CONTRACT-ISSUED ( 3 ) 9X #CONTRACT-ISSUED ( 4 ) 12X #CONTRACT-ISSUED ( 5 )
                ColumnSpacing(2),"Contracts Issued      ",new ColumnSpacing(16),pnd_Contract_Issued.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued.getValue(2), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued.getValue(3), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued.getValue(4), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Contract_Issued.getValue(5), new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Accumulation Applied  ",                                             //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Accumulation Applied  ' 07X #DA-TIAA-PROCEEDS-AMT ( 1 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT ( 2 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT ( 3 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT ( 4 ) ( IC = $ ) 4X #DA-TIAA-PROCEEDS-AMT ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_Da_Tiaa_Proceeds_Amt.getValue(1), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt.getValue(2), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt.getValue(3), 
                new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Da_Tiaa_Proceeds_Amt.getValue(5), 
                new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Annuity Income ($)    ",                                             //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Annuity Income ($)    ' 07X #TRAD-ANNUITY ( 1 ) ( IC = $ ) #TRAD-ANNUITY ( 2 ) ( IC = $ ) #TRAD-ANNUITY ( 3 ) ( IC = $ ) #TRAD-ANNUITY ( 4 ) ( IC = $ ) 4X #TRAD-ANNUITY ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_Trad_Annuity.getValue(1), new InsertionCharacter("$"),pnd_Trad_Annuity.getValue(2), new InsertionCharacter("$"),pnd_Trad_Annuity.getValue(3), 
                new InsertionCharacter("$"),pnd_Trad_Annuity.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Trad_Annuity.getValue(5), 
                new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TPA",NEWLINE,"----",NEWLINE,new ColumnSpacing(2),"Contracts Issued      ",new ColumnSpacing(16),pnd_Contract_Issued_Tpa.getValue(1),  //Natural: WRITE ( 1 ) / 'TPA' / '----' / 2X 'Contracts Issued      ' 16X #CONTRACT-ISSUED-TPA ( 1 ) 9X #CONTRACT-ISSUED-TPA ( 2 ) 9X #CONTRACT-ISSUED-TPA ( 3 ) 9X #CONTRACT-ISSUED-TPA ( 4 ) 12X #CONTRACT-ISSUED-TPA ( 5 )
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Tpa.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Tpa.getValue(3), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Tpa.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Contract_Issued_Tpa.getValue(5), 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Accumulation Applied  ",                                             //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Accumulation Applied  ' 07X #DA-TIAA-PROCEEDS-AMT-TPA ( 1 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-TPA ( 2 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-TPA ( 3 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-TPA ( 4 ) ( IC = $ ) 4X #DA-TIAA-PROCEEDS-AMT-TPA ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(1), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(2), new 
                InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(3), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(4), 
                new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(5), new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Annuity Income ($)    ",                                             //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Annuity Income ($)    ' 07X #TRAD-ANNUITY-TPA ( 1 ) ( IC = $ ) #TRAD-ANNUITY-TPA ( 2 ) ( IC = $ ) #TRAD-ANNUITY-TPA ( 3 ) ( IC = $ ) #TRAD-ANNUITY-TPA ( 4 ) ( IC = $ ) 4X #TRAD-ANNUITY-TPA ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_Trad_Annuity_Tpa.getValue(1), new InsertionCharacter("$"),pnd_Trad_Annuity_Tpa.getValue(2), new InsertionCharacter("$"),pnd_Trad_Annuity_Tpa.getValue(3), 
                new InsertionCharacter("$"),pnd_Trad_Annuity_Tpa.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Trad_Annuity_Tpa.getValue(5), 
                new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"IPRO",NEWLINE,"----",NEWLINE,new ColumnSpacing(2),"Contracts Issued      ",new ColumnSpacing(16),pnd_Contract_Issued_Ipro.getValue(1),  //Natural: WRITE ( 1 ) / 'IPRO' / '----' / 2X 'Contracts Issued      ' 16X #CONTRACT-ISSUED-IPRO ( 1 ) 9X #CONTRACT-ISSUED-IPRO ( 2 ) 9X #CONTRACT-ISSUED-IPRO ( 3 ) 9X #CONTRACT-ISSUED-IPRO ( 4 ) 12X #CONTRACT-ISSUED-IPRO ( 5 )
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Ipro.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Ipro.getValue(3), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Ipro.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Contract_Issued_Ipro.getValue(5), 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Accumulation Applied  ",                                             //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Accumulation Applied  ' 07X #DA-TIAA-PROCEEDS-AMT-IPRO ( 1 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-IPRO ( 2 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-IPRO ( 3 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-IPRO ( 4 ) ( IC = $ ) 4X #DA-TIAA-PROCEEDS-AMT-IPRO ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(1), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(2), new 
                InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(3), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(4), 
                new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(5), new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Annuity Income ($)    ",                                             //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Annuity Income ($)    ' 07X #TRAD-ANNUITY-IPRO ( 1 ) ( IC = $ ) #TRAD-ANNUITY-IPRO ( 2 ) ( IC = $ ) #TRAD-ANNUITY-IPRO ( 3 ) ( IC = $ ) #TRAD-ANNUITY-IPRO ( 4 ) ( IC = $ ) 4X #TRAD-ANNUITY-IPRO ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_Trad_Annuity_Ipro.getValue(1), new InsertionCharacter("$"),pnd_Trad_Annuity_Ipro.getValue(2), new InsertionCharacter("$"),pnd_Trad_Annuity_Ipro.getValue(3), 
                new InsertionCharacter("$"),pnd_Trad_Annuity_Ipro.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Trad_Annuity_Ipro.getValue(5), 
                new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            //*  TACC KG
            //*  TACC KG
            //*  TACC KG
            //*  TACC KG
            getReports().write(1, ReportOption.NOTITLE,"REAL ESTATE AND ACCESS ACCOUNTS",NEWLINE,"-------------------------------",NEWLINE,new ColumnSpacing(2),"CERTIFICATE ISSUED    ",new  //Natural: WRITE ( 1 ) 'REAL ESTATE AND ACCESS ACCOUNTS' / '-------------------------------' / 2X 'CERTIFICATE ISSUED    ' 16X #CERTIF-ISSUED ( 1 ) 9X #CERTIF-ISSUED ( 2 ) 9X #CERTIF-ISSUED ( 3 ) 9X #CERTIF-ISSUED ( 4 ) 12X #CERTIF-ISSUED ( 5 ) / 2X 'Accumulation Applied  ' 09X #DA-REA-PROCEEDS-AMT ( 1 ) 2X #DA-REA-PROCEEDS-AMT ( 2 ) 2X #DA-REA-PROCEEDS-AMT ( 3 ) 2X #DA-REA-PROCEEDS-AMT ( 4 ) 5X #DA-REA-PROCEEDS-AMT ( 5 ) / 2X 'Annuity Income (units)- REA' 04X #REAL-ANNUITY ( 1 ) 2X #REAL-ANNUITY ( 2 ) 2X #REAL-ANNUITY ( 3 ) 2X #REAL-ANNUITY ( 4 ) 5X #REAL-ANNUITY ( 5 ) / 2X 'Annuity Income (units)- ACC' 04X #TACC-ANNUITY ( 1 ) 2X #TACC-ANNUITY ( 2 ) 2X #TACC-ANNUITY ( 3 ) 2X #TACC-ANNUITY ( 4 ) 5X #TACC-ANNUITY ( 5 ) / 'CREF' / '----' / 2X 'Certificate Issued    ' 16X #CERTIF-ISSUED2 ( 1 ) 9X #CERTIF-ISSUED2 ( 2 ) 9X #CERTIF-ISSUED2 ( 3 ) 9X #CERTIF-ISSUED2 ( 4 ) 12X #CERTIF-ISSUED2 ( 5 ) / 2X 'Accumulation Applied  ' 09X #DA-CREF-PROCEEDS-AMT ( 1 ) 2X #DA-CREF-PROCEEDS-AMT ( 2 ) 2X #DA-CREF-PROCEEDS-AMT ( 3 ) 2X #DA-CREF-PROCEEDS-AMT ( 4 ) 5X #DA-CREF-PROCEEDS-AMT ( 5 ) / 2X 'Annuity Income (units)' 09X #CREF-ANNUITY ( 1 ) 2X #CREF-ANNUITY ( 2 ) 2X #CREF-ANNUITY ( 3 ) 2X #CREF-ANNUITY ( 4 ) 5X #CREF-ANNUITY ( 5 )
                ColumnSpacing(16),pnd_Certif_Issued.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued.getValue(2), new 
                ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued.getValue(3), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued.getValue(4), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Certif_Issued.getValue(5), new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(2),"Accumulation Applied  ",new 
                ColumnSpacing(9),pnd_Da_Rea_Proceeds_Amt.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Rea_Proceeds_Amt.getValue(2), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Rea_Proceeds_Amt.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(2),pnd_Da_Rea_Proceeds_Amt.getValue(4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),pnd_Da_Rea_Proceeds_Amt.getValue(5), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(2),"Annuity Income (units)- REA",new ColumnSpacing(4),pnd_Real_Annuity.getValue(1), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Real_Annuity.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Real_Annuity.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Real_Annuity.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_Real_Annuity.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,new ColumnSpacing(2),"Annuity Income (units)- ACC",new ColumnSpacing(4),pnd_Tacc_Annuity.getValue(1), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Tacc_Annuity.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Tacc_Annuity.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Tacc_Annuity.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_Tacc_Annuity.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF",NEWLINE,"----",NEWLINE,new ColumnSpacing(2),"Certificate Issued    ",new ColumnSpacing(16),pnd_Certif_Issued2.getValue(1), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued2.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued2.getValue(3), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued2.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Certif_Issued2.getValue(5), 
                new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(2),"Accumulation Applied  ",new ColumnSpacing(9),pnd_Da_Cref_Proceeds_Amt.getValue(1), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Cref_Proceeds_Amt.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(2),pnd_Da_Cref_Proceeds_Amt.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Cref_Proceeds_Amt.getValue(4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),pnd_Da_Cref_Proceeds_Amt.getValue(5), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
                ColumnSpacing(2),"Annuity Income (units)",new ColumnSpacing(9),pnd_Cref_Annuity.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Cref_Annuity.getValue(2), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Cref_Annuity.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Cref_Annuity.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_Cref_Annuity.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1
            getReports().write(1, ReportOption.NOTITLE,"CONTRACT SETS          ",new ColumnSpacing(17),pnd_No_Rec.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new         //Natural: WRITE ( 1 ) 'CONTRACT SETS          ' 17X #NO-REC ( 1 ) 9X #NO-REC ( 2 ) 9X #NO-REC ( 3 ) 9X #NO-REC ( 4 ) 12X #NO-REC ( 5 )
                ColumnSpacing(9),pnd_No_Rec.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_No_Rec.getValue(3), new ReportEditMask 
                ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_No_Rec.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_No_Rec.getValue(5), new 
                ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(10),"*****************************************************",NEWLINE,new  //Natural: WRITE ( 1 ) /// / 10X '*****************************************************' / 10X '*                                                   *' / 10X '*                                                   *' / 10X '*     NO LEGACY PAYOUT TRANSACTIONS IN THIS RUN     *' / 10X '*                                                   *' / 10X '*                                                   *' / 10X '*****************************************************'
                ColumnSpacing(10),"*                                                   *",NEWLINE,new ColumnSpacing(10),"*                                                   *",NEWLINE,new 
                ColumnSpacing(10),"*     NO LEGACY PAYOUT TRANSACTIONS IN THIS RUN     *",NEWLINE,new ColumnSpacing(10),"*                                                   *",NEWLINE,new 
                ColumnSpacing(10),"*                                                   *",NEWLINE,new ColumnSpacing(10),"*****************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  OMNI
        if (condition(pnd_O_No_Rec.getValue(5).greater(getZero())))                                                                                                       //Natural: IF #O-NO-REC ( 5 ) GT 0
        {
            getReports().write(2, ReportOption.NOTITLE,"INCOME BENEFITS", new FieldAttributes ("AD=I"),NEWLINE,"---------------");                                        //Natural: WRITE ( 2 ) 'INCOME BENEFITS' ( AD = I ) / '---------------'
            if (Global.isEscape()) return;
            getReports().display(2, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Standard",                                                          //Natural: DISPLAY ( 2 ) NOHDR ( HW = OFF ) 'TRADITIONAL ANNUITY - Standard' 1X #O-GRNTED-STD-AMT ( 1 ) ( IC = $ ) #O-GRNTED-STD-AMT ( 2 ) ( IC = $ ) #O-GRNTED-STD-AMT ( 3 ) ( IC = $ ) #O-GRNTED-STD-AMT ( 4 ) ( IC = $ ) 4X #O-GRNTED-STD-AMT-TOT ( IC = $ )
            		new ColumnSpacing(1),pnd_O_Grnted_Std_Amt.getValue(1), new InsertionCharacter("$"),pnd_O_Grnted_Std_Amt.getValue(2), new InsertionCharacter("$"),pnd_O_Grnted_Std_Amt.getValue(3), 
                new InsertionCharacter("$"),pnd_O_Grnted_Std_Amt.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_O_Grnted_Std_Amt_Tot, 
                new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().display(2, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Graded  ",                                                          //Natural: DISPLAY ( 2 ) NOHDR ( HW = OFF ) 'TRADITIONAL ANNUITY - Graded  ' 1X #O-GRNTED-GRD-AMT ( 1 ) ( IC = $ ) #O-GRNTED-GRD-AMT ( 2 ) ( IC = $ ) #O-GRNTED-GRD-AMT ( 3 ) ( IC = $ ) #O-GRNTED-GRD-AMT ( 4 ) ( IC = $ ) 4X #O-GRNTED-GRD-AMT-TOT ( IC = $ )
            		new ColumnSpacing(1),pnd_O_Grnted_Grd_Amt.getValue(1), new InsertionCharacter("$"),pnd_O_Grnted_Grd_Amt.getValue(2), new InsertionCharacter("$"),pnd_O_Grnted_Grd_Amt.getValue(3), 
                new InsertionCharacter("$"),pnd_O_Grnted_Grd_Amt.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_O_Grnted_Grd_Amt_Tot, 
                new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().skip(2, 1);                                                                                                                                      //Natural: SKIP ( 2 ) 1
            getReports().write(2, ReportOption.NOTITLE,"ANNUAL REVALUATION", new FieldAttributes ("AD=I"),NEWLINE,"------------------",NEWLINE,"TIAA REA          ",      //Natural: WRITE ( 2 ) 'ANNUAL REVALUATION' ( AD = I ) / '------------------' / 'TIAA REA          ' ( AD = I ) 15X #O-A-TIAA-REA ( 1 ) 2X #O-A-TIAA-REA ( 2 ) 2X #O-A-TIAA-REA ( 3 ) 2X #O-A-TIAA-REA ( 4 ) 5X #O-A-TIAA-REA ( 5 ) / 'TIAA ACCESS       ' 15X #O-A-TACCESS ( 1 ) 2X #O-A-TACCESS ( 2 ) 2X #O-A-TACCESS ( 3 ) 2X #O-A-TACCESS ( 4 ) 5X #O-A-TACCESS ( 5 ) // 'CREF Stock        ' 15X #O-A-STOCK ( 1 ) 2X #O-A-STOCK ( 2 ) 2X #O-A-STOCK ( 3 ) 2X #O-A-STOCK ( 4 ) 5X #O-A-STOCK ( 5 ) / 'CREF MMA          ' 15X #O-A-MMA ( 1 ) 2X #O-A-MMA ( 2 ) 2X #O-A-MMA ( 3 ) 2X #O-A-MMA ( 4 ) 5X #O-A-MMA ( 5 ) / 'CREF Social Choice' 15X #O-A-SOCIAL ( 1 ) 2X #O-A-SOCIAL ( 2 ) 2X #O-A-SOCIAL ( 3 ) 2X #O-A-SOCIAL ( 4 ) 5X #O-A-SOCIAL ( 5 ) / 'CREF Bond         ' 15X #O-A-BOND ( 1 ) 2X #O-A-BOND ( 2 ) 2X #O-A-BOND ( 3 ) 2X #O-A-BOND ( 4 ) 5X #O-A-BOND ( 5 ) / 'CREF Global       ' 15X #O-A-GLOBAL ( 1 ) 2X #O-A-GLOBAL ( 2 ) 2X #O-A-GLOBAL ( 3 ) 2X #O-A-GLOBAL ( 4 ) 5X #O-A-GLOBAL ( 5 ) / 'CREF Growth       ' 15X #O-A-GRTH ( 1 ) 2X #O-A-GRTH ( 2 ) 2X #O-A-GRTH ( 3 ) 2X #O-A-GRTH ( 4 ) 5X #O-A-GRTH ( 5 ) / 'CREF Equity Index ' 15X #O-A-EQU ( 1 ) 2X #O-A-EQU ( 2 ) 2X #O-A-EQU ( 3 ) 2X #O-A-EQU ( 4 ) 5X #O-A-EQU ( 5 ) / 'CREF ILB          ' 15X #O-A-ILB ( 1 ) 2X #O-A-ILB ( 2 ) 2X #O-A-ILB ( 3 ) 2X #O-A-ILB ( 4 ) 5X #O-A-ILB ( 5 )
                new FieldAttributes ("AD=I"),new ColumnSpacing(15),pnd_O_A_Tiaa_Rea.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Tiaa_Rea.getValue(2), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Tiaa_Rea.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Tiaa_Rea.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_A_Tiaa_Rea.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA ACCESS       ",new 
                ColumnSpacing(15),pnd_O_A_Taccess.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Taccess.getValue(2), new 
                ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Taccess.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Taccess.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_A_Taccess.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"CREF Stock        ",new 
                ColumnSpacing(15),pnd_O_A_Stock.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Stock.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Stock.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Stock.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_A_Stock.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF MMA          ",new 
                ColumnSpacing(15),pnd_O_A_Mma.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Mma.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Mma.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Mma.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_A_Mma.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Social Choice",new 
                ColumnSpacing(15),pnd_O_A_Social.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Social.getValue(2), new 
                ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Social.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Social.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_A_Social.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Bond         ",new 
                ColumnSpacing(15),pnd_O_A_Bond.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Bond.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Bond.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Bond.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_A_Bond.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Global       ",new 
                ColumnSpacing(15),pnd_O_A_Global.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Global.getValue(2), new 
                ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Global.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Global.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_A_Global.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Growth       ",new 
                ColumnSpacing(15),pnd_O_A_Grth.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Grth.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Grth.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Grth.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_A_Grth.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Equity Index ",new 
                ColumnSpacing(15),pnd_O_A_Equ.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Equ.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Equ.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Equ.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_A_Equ.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF ILB          ",new 
                ColumnSpacing(15),pnd_O_A_Ilb.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Ilb.getValue(2), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Ilb.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_A_Ilb.getValue(4), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_A_Ilb.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (Global.isEscape()) return;
            getReports().skip(2, 1);                                                                                                                                      //Natural: SKIP ( 2 ) 1
            //*  HK 12/21/00
            getReports().write(2, ReportOption.NOTITLE,"MONTHLY REVALUATION",NEWLINE,"-------------------",NEWLINE,"TIAA REA          ",new ColumnSpacing(15),pnd_O_M_Tiaa_Rea.getValue(1),  //Natural: WRITE ( 2 ) 'MONTHLY REVALUATION' / '-------------------' / 'TIAA REA          ' 15X #O-M-TIAA-REA ( 1 ) 2X #O-M-TIAA-REA ( 2 ) 2X #O-M-TIAA-REA ( 3 ) 2X #O-M-TIAA-REA ( 4 ) 5X #O-M-TIAA-REA ( 5 ) / 'TIAA ACCESS       ' 15X #O-M-TACCESS ( 1 ) 2X #O-M-TACCESS ( 2 ) 2X #O-M-TACCESS ( 3 ) 2X #O-M-TACCESS ( 4 ) 5X #O-M-TACCESS ( 5 ) // 'CREF Stock        ' 15X #O-M-STOCK ( 1 ) 2X #O-M-STOCK ( 2 ) 2X #O-M-STOCK ( 3 ) 2X #O-M-STOCK ( 4 ) 5X #O-M-STOCK ( 5 ) / 'CREF MMA          ' 15X #O-M-MMA ( 1 ) 2X #O-M-MMA ( 2 ) 2X #O-M-MMA ( 3 ) 2X #O-M-MMA ( 4 ) 5X #O-M-MMA ( 5 ) / 'CREF Social Choice' 15X #O-M-SOCIAL ( 1 ) 2X #O-M-SOCIAL ( 2 ) 2X #O-M-SOCIAL ( 3 ) 2X #O-M-SOCIAL ( 4 ) 5X #O-M-SOCIAL ( 5 ) / 'CREF Bond         ' 15X #O-M-BOND ( 1 ) 2X #O-M-BOND ( 2 ) 2X #O-M-BOND ( 3 ) 2X #O-M-BOND ( 4 ) 5X #O-M-BOND ( 5 ) / 'CREF Global       ' 15X #O-M-GLOBAL ( 1 ) 2X #O-M-GLOBAL ( 2 ) 2X #O-M-GLOBAL ( 3 ) 2X #O-M-GLOBAL ( 4 ) 5X #O-M-GLOBAL ( 5 ) / 'CREF Growth       ' 15X #O-M-GRTH ( 1 ) 2X #O-M-GRTH ( 2 ) 2X #O-M-GRTH ( 3 ) 2X #O-M-GRTH ( 4 ) 5X #O-M-GRTH ( 5 ) / 'CREF Equity Index ' 15X #O-M-EQU ( 1 ) 2X #O-M-EQU ( 2 ) 2X #O-M-EQU ( 3 ) 2X #O-M-EQU ( 4 ) 5X #O-M-EQU ( 5 ) / 'CREF ILB          ' 15X #O-M-ILB ( 1 ) 2X #O-M-ILB ( 2 ) 2X #O-M-ILB ( 3 ) 2X #O-M-ILB ( 4 ) 5X #O-M-ILB ( 5 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Tiaa_Rea.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Tiaa_Rea.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Tiaa_Rea.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_M_Tiaa_Rea.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA ACCESS       ",new ColumnSpacing(15),pnd_O_M_Taccess.getValue(1), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Taccess.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Taccess.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Taccess.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_M_Taccess.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"CREF Stock        ",new ColumnSpacing(15),pnd_O_M_Stock.getValue(1), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Stock.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Stock.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Stock.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_M_Stock.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF MMA          ",new ColumnSpacing(15),pnd_O_M_Mma.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_O_M_Mma.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Mma.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Mma.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_M_Mma.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Social Choice",new ColumnSpacing(15),pnd_O_M_Social.getValue(1), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Social.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Social.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Social.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_M_Social.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Bond         ",new ColumnSpacing(15),pnd_O_M_Bond.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_O_M_Bond.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Bond.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Bond.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_M_Bond.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Global       ",new ColumnSpacing(15),pnd_O_M_Global.getValue(1), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Global.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Global.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Global.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_M_Global.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Growth       ",new ColumnSpacing(15),pnd_O_M_Grth.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_O_M_Grth.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Grth.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Grth.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_M_Grth.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Equity Index ",new ColumnSpacing(15),pnd_O_M_Equ.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_O_M_Equ.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Equ.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Equ.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_M_Equ.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF ILB          ",new ColumnSpacing(15),pnd_O_M_Ilb.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_O_M_Ilb.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Ilb.getValue(3), new ReportEditMask 
                ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_M_Ilb.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_M_Ilb.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE,"SUMMARY TOTALS",new TabSetting(103),"GRAND TOTALS",NEWLINE,"--------------",new TabSetting(103),"------------",NEWLINE,"TRADITIONAL ANNUITY",NEWLINE,"-------------------",NEWLINE,new  //Natural: WRITE ( 2 ) 'SUMMARY TOTALS' 103T 'GRAND TOTALS' / '--------------' 103T '------------' / 'TRADITIONAL ANNUITY' / '-------------------' / 2X 'Contracts Issued      ' 16X #O-CONTRACT-ISSUED ( 1 ) 9X #O-CONTRACT-ISSUED ( 2 ) 9X #O-CONTRACT-ISSUED ( 3 ) 9X #O-CONTRACT-ISSUED ( 4 ) 12X #O-CONTRACT-ISSUED ( 5 )
                ColumnSpacing(2),"Contracts Issued      ",new ColumnSpacing(16),pnd_O_Contract_Issued.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Contract_Issued.getValue(2), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Contract_Issued.getValue(3), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Contract_Issued.getValue(4), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_O_Contract_Issued.getValue(5), new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            getReports().display(2, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Accumulation Applied  ",                                             //Natural: DISPLAY ( 2 ) NOHDR ( HW = OFF ) 2X 'Accumulation Applied  ' 07X #O-DA-TIAA-PROCEEDS-AMT ( 1 ) ( IC = $ ) #O-DA-TIAA-PROCEEDS-AMT ( 2 ) ( IC = $ ) #O-DA-TIAA-PROCEEDS-AMT ( 3 ) ( IC = $ ) #O-DA-TIAA-PROCEEDS-AMT ( 4 ) ( IC = $ ) 4X #O-DA-TIAA-PROCEEDS-AMT ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_O_Da_Tiaa_Proceeds_Amt.getValue(1), new InsertionCharacter("$"),pnd_O_Da_Tiaa_Proceeds_Amt.getValue(2), new InsertionCharacter("$"),pnd_O_Da_Tiaa_Proceeds_Amt.getValue(3), 
                new InsertionCharacter("$"),pnd_O_Da_Tiaa_Proceeds_Amt.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_O_Da_Tiaa_Proceeds_Amt.getValue(5), 
                new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().display(2, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Annuity Income ($)    ",                                             //Natural: DISPLAY ( 2 ) NOHDR ( HW = OFF ) 2X 'Annuity Income ($)    ' 07X #O-TRAD-ANNUITY ( 1 ) ( IC = $ ) #O-TRAD-ANNUITY ( 2 ) ( IC = $ ) #O-TRAD-ANNUITY ( 3 ) ( IC = $ ) #O-TRAD-ANNUITY ( 4 ) ( IC = $ ) 4X #O-TRAD-ANNUITY ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_O_Trad_Annuity.getValue(1), new InsertionCharacter("$"),pnd_O_Trad_Annuity.getValue(2), new InsertionCharacter("$"),pnd_O_Trad_Annuity.getValue(3), 
                new InsertionCharacter("$"),pnd_O_Trad_Annuity.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_O_Trad_Annuity.getValue(5), 
                new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().skip(2, 1);                                                                                                                                      //Natural: SKIP ( 2 ) 1
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,"TPA",NEWLINE,"----",NEWLINE,new ColumnSpacing(2),"Contracts Issued      ",new ColumnSpacing(16),pnd_O_Contract_Issued_Tpa.getValue(1),  //Natural: WRITE ( 2 ) / 'TPA' / '----' / 2X 'Contracts Issued      ' 16X #O-CONTRACT-ISSUED-TPA ( 1 ) 9X #O-CONTRACT-ISSUED-TPA ( 2 ) 9X #O-CONTRACT-ISSUED-TPA ( 3 ) 9X #O-CONTRACT-ISSUED-TPA ( 4 ) 12X #O-CONTRACT-ISSUED-TPA ( 5 )
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Contract_Issued_Tpa.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Contract_Issued_Tpa.getValue(3), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Contract_Issued_Tpa.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_O_Contract_Issued_Tpa.getValue(5), 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            getReports().display(2, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Accumulation Applied  ",                                             //Natural: DISPLAY ( 2 ) NOHDR ( HW = OFF ) 2X 'Accumulation Applied  ' 07X #O-DA-TIAA-PROCEEDS-AMT-TPA ( 1 ) ( IC = $ ) #O-DA-TIAA-PROCEEDS-AMT-TPA ( 2 ) ( IC = $ ) #O-DA-TIAA-PROCEEDS-AMT-TPA ( 3 ) ( IC = $ ) #O-DA-TIAA-PROCEEDS-AMT-TPA ( 4 ) ( IC = $ ) 4X #O-DA-TIAA-PROCEEDS-AMT-TPA ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_O_Da_Tiaa_Proceeds_Amt_Tpa.getValue(1), new InsertionCharacter("$"),pnd_O_Da_Tiaa_Proceeds_Amt_Tpa.getValue(2), 
                new InsertionCharacter("$"),pnd_O_Da_Tiaa_Proceeds_Amt_Tpa.getValue(3), new InsertionCharacter("$"),pnd_O_Da_Tiaa_Proceeds_Amt_Tpa.getValue(4), 
                new InsertionCharacter("$"),new ColumnSpacing(4),pnd_O_Da_Tiaa_Proceeds_Amt_Tpa.getValue(5), new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().display(2, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Annuity Income ($)    ",                                             //Natural: DISPLAY ( 2 ) NOHDR ( HW = OFF ) 2X 'Annuity Income ($)    ' 07X #O-TRAD-ANNUITY-TPA ( 1 ) ( IC = $ ) #O-TRAD-ANNUITY-TPA ( 2 ) ( IC = $ ) #O-TRAD-ANNUITY-TPA ( 3 ) ( IC = $ ) #O-TRAD-ANNUITY-TPA ( 4 ) ( IC = $ ) 4X #O-TRAD-ANNUITY-TPA ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_O_Trad_Annuity_Tpa.getValue(1), new InsertionCharacter("$"),pnd_O_Trad_Annuity_Tpa.getValue(2), new InsertionCharacter("$"),pnd_O_Trad_Annuity_Tpa.getValue(3), 
                new InsertionCharacter("$"),pnd_O_Trad_Annuity_Tpa.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_O_Trad_Annuity_Tpa.getValue(5), 
                new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().skip(2, 1);                                                                                                                                      //Natural: SKIP ( 2 ) 1
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,"IPRO",NEWLINE,"----",NEWLINE,new ColumnSpacing(2),"Contracts Issued      ",new ColumnSpacing(16),pnd_O_Contract_Issued_Ipro.getValue(1),  //Natural: WRITE ( 2 ) / 'IPRO' / '----' / 2X 'Contracts Issued      ' 16X #O-CONTRACT-ISSUED-IPRO ( 1 ) 9X #O-CONTRACT-ISSUED-IPRO ( 2 ) 9X #O-CONTRACT-ISSUED-IPRO ( 3 ) 9X #O-CONTRACT-ISSUED-IPRO ( 4 ) 12X #O-CONTRACT-ISSUED-IPRO ( 5 )
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Contract_Issued_Ipro.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Contract_Issued_Ipro.getValue(3), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Contract_Issued_Ipro.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_O_Contract_Issued_Ipro.getValue(5), 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
            getReports().display(2, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Accumulation Applied  ",                                             //Natural: DISPLAY ( 2 ) NOHDR ( HW = OFF ) 2X 'Accumulation Applied  ' 07X #O-DA-TIAA-PROCEEDS-AMT-IPRO ( 1 ) ( IC = $ ) #O-DA-TIAA-PROCEEDS-AMT-IPRO ( 2 ) ( IC = $ ) #O-DA-TIAA-PROCEEDS-AMT-IPRO ( 3 ) ( IC = $ ) #O-DA-TIAA-PROCEEDS-AMT-IPRO ( 4 ) ( IC = $ ) 4X #O-DA-TIAA-PROCEEDS-AMT-IPRO ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_O_Da_Tiaa_Proceeds_Amt_Ipro.getValue(1), new InsertionCharacter("$"),pnd_O_Da_Tiaa_Proceeds_Amt_Ipro.getValue(2), 
                new InsertionCharacter("$"),pnd_O_Da_Tiaa_Proceeds_Amt_Ipro.getValue(3), new InsertionCharacter("$"),pnd_O_Da_Tiaa_Proceeds_Amt_Ipro.getValue(4), 
                new InsertionCharacter("$"),new ColumnSpacing(4),pnd_O_Da_Tiaa_Proceeds_Amt_Ipro.getValue(5), new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().display(2, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Annuity Income ($)    ",                                             //Natural: DISPLAY ( 2 ) NOHDR ( HW = OFF ) 2X 'Annuity Income ($)    ' 07X #O-TRAD-ANNUITY-IPRO ( 1 ) ( IC = $ ) #O-TRAD-ANNUITY-IPRO ( 2 ) ( IC = $ ) #O-TRAD-ANNUITY-IPRO ( 3 ) ( IC = $ ) #O-TRAD-ANNUITY-IPRO ( 4 ) ( IC = $ ) 4X #O-TRAD-ANNUITY-IPRO ( 5 ) ( IC = $ )
            		new ColumnSpacing(7),pnd_O_Trad_Annuity_Ipro.getValue(1), new InsertionCharacter("$"),pnd_O_Trad_Annuity_Ipro.getValue(2), new InsertionCharacter("$"),pnd_O_Trad_Annuity_Ipro.getValue(3), 
                new InsertionCharacter("$"),pnd_O_Trad_Annuity_Ipro.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_O_Trad_Annuity_Ipro.getValue(5), 
                new InsertionCharacter("$"));
            if (Global.isEscape()) return;
            getReports().skip(2, 1);                                                                                                                                      //Natural: SKIP ( 2 ) 1
            //*  TACC KG
            //*  TACC KG
            //*  TACC KG
            //*  TACC KG
            //*  TACC KG
            //*  TACC KG
            getReports().write(2, ReportOption.NOTITLE,"REAL ESTATE AND ACCESS ACCOUNTS",NEWLINE,"-------------------------------",NEWLINE,new ColumnSpacing(2),"CERTIFICATE ISSUED    ",new  //Natural: WRITE ( 2 ) 'REAL ESTATE AND ACCESS ACCOUNTS' / '-------------------------------' / 2X 'CERTIFICATE ISSUED    ' 16X #O-CERTIF-ISSUED ( 1 ) 9X #O-CERTIF-ISSUED ( 2 ) 9X #O-CERTIF-ISSUED ( 3 ) 9X #O-CERTIF-ISSUED ( 4 ) 12X #O-CERTIF-ISSUED ( 5 ) / 2X 'Accumulation Applied  ' 09X #O-DA-REA-PROCEEDS-AMT ( 1 ) 2X #O-DA-REA-PROCEEDS-AMT ( 2 ) 2X #O-DA-REA-PROCEEDS-AMT ( 3 ) 2X #O-DA-REA-PROCEEDS-AMT ( 4 ) 5X #O-DA-REA-PROCEEDS-AMT ( 5 ) / 2X 'Annuity Income (units)- REA' 04X #O-REAL-ANNUITY ( 1 ) 2X #O-REAL-ANNUITY ( 2 ) 2X #O-REAL-ANNUITY ( 3 ) 2X #O-REAL-ANNUITY ( 4 ) 5X #O-REAL-ANNUITY ( 5 ) / 2X 'Annuity Income (units)- ACC' 04X #O-TACC-ANNUITY ( 1 ) 2X #O-TACC-ANNUITY ( 2 ) 2X #O-TACC-ANNUITY ( 3 ) 2X #O-TACC-ANNUITY ( 4 ) 5X #O-TACC-ANNUITY ( 5 ) / 'CREF' / '----' / 2X 'Certificate Issued    ' 16X #O-CERTIF-ISSUED2 ( 1 ) 9X #O-CERTIF-ISSUED2 ( 2 ) 9X #O-CERTIF-ISSUED2 ( 3 ) 9X #O-CERTIF-ISSUED2 ( 4 ) 12X #O-CERTIF-ISSUED2 ( 5 ) / 2X 'Accumulation Applied  ' 09X #O-DA-CREF-PROCEEDS-AMT ( 1 ) 2X #O-DA-CREF-PROCEEDS-AMT ( 2 ) 2X #O-DA-CREF-PROCEEDS-AMT ( 3 ) 2X #O-DA-CREF-PROCEEDS-AMT ( 4 ) 5X #O-DA-CREF-PROCEEDS-AMT ( 5 ) / 2X 'Annuity Income (units)' 09X #O-CREF-ANNUITY ( 1 ) 2X #O-CREF-ANNUITY ( 2 ) 2X #O-CREF-ANNUITY ( 3 ) 2X #O-CREF-ANNUITY ( 4 ) 5X #O-CREF-ANNUITY ( 5 )
                ColumnSpacing(16),pnd_O_Certif_Issued.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Certif_Issued.getValue(2), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Certif_Issued.getValue(3), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Certif_Issued.getValue(4), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_O_Certif_Issued.getValue(5), new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(2),"Accumulation Applied  ",new 
                ColumnSpacing(9),pnd_O_Da_Rea_Proceeds_Amt.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_O_Da_Rea_Proceeds_Amt.getValue(2), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_O_Da_Rea_Proceeds_Amt.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(2),pnd_O_Da_Rea_Proceeds_Amt.getValue(4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),pnd_O_Da_Rea_Proceeds_Amt.getValue(5), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(2),"Annuity Income (units)- REA",new ColumnSpacing(4),pnd_O_Real_Annuity.getValue(1), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_Real_Annuity.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_Real_Annuity.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_Real_Annuity.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_Real_Annuity.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,new ColumnSpacing(2),"Annuity Income (units)- ACC",new ColumnSpacing(4),pnd_O_Tacc_Annuity.getValue(1), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_Tacc_Annuity.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_Tacc_Annuity.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_Tacc_Annuity.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_Tacc_Annuity.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF",NEWLINE,"----",NEWLINE,new ColumnSpacing(2),"Certificate Issued    ",new ColumnSpacing(16),pnd_O_Certif_Issued2.getValue(1), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Certif_Issued2.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Certif_Issued2.getValue(3), 
                new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_Certif_Issued2.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_O_Certif_Issued2.getValue(5), 
                new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(2),"Accumulation Applied  ",new ColumnSpacing(9),pnd_O_Da_Cref_Proceeds_Amt.getValue(1), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_O_Da_Cref_Proceeds_Amt.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                ColumnSpacing(2),pnd_O_Da_Cref_Proceeds_Amt.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_O_Da_Cref_Proceeds_Amt.getValue(4), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),pnd_O_Da_Cref_Proceeds_Amt.getValue(5), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
                ColumnSpacing(2),"Annuity Income (units)",new ColumnSpacing(9),pnd_O_Cref_Annuity.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
                ColumnSpacing(2),pnd_O_Cref_Annuity.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_Cref_Annuity.getValue(3), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_O_Cref_Annuity.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_O_Cref_Annuity.getValue(5), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (Global.isEscape()) return;
            getReports().skip(2, 1);                                                                                                                                      //Natural: SKIP ( 2 ) 1
            getReports().write(2, ReportOption.NOTITLE,"CONTRACT SETS          ",new ColumnSpacing(17),pnd_O_No_Rec.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new       //Natural: WRITE ( 2 ) 'CONTRACT SETS          ' 17X #O-NO-REC ( 1 ) 9X #O-NO-REC ( 2 ) 9X #O-NO-REC ( 3 ) 9X #O-NO-REC ( 4 ) 12X #O-NO-REC ( 5 )
                ColumnSpacing(9),pnd_O_No_Rec.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_No_Rec.getValue(3), new ReportEditMask 
                ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_O_No_Rec.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_O_No_Rec.getValue(5), 
                new ReportEditMask ("ZZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(10),"*****************************************************",NEWLINE,new  //Natural: WRITE ( 1 ) /// / 10X '*****************************************************' / 10X '*                                                   *' / 10X '*                                                   *' / 10X '*      NO OMNI PAYOUT TRANSACTIONS IN THIS RUN      *' / 10X '*                                                   *' / 10X '*                                                   *' / 10X '*****************************************************'
                ColumnSpacing(10),"*                                                   *",NEWLINE,new ColumnSpacing(10),"*                                                   *",NEWLINE,new 
                ColumnSpacing(10),"*      NO OMNI PAYOUT TRANSACTIONS IN THIS RUN      *",NEWLINE,new ColumnSpacing(10),"*                                                   *",NEWLINE,new 
                ColumnSpacing(10),"*                                                   *",NEWLINE,new ColumnSpacing(10),"*****************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Issued_Totals() throws Exception                                                                                                                     //Natural: ISSUED-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_O_Contract_Issued.getValue(5).compute(new ComputeParameters(false, pnd_O_Contract_Issued.getValue(5)), DbsField.add(getZero(),pnd_O_Contract_Issued.getValue(1, //Natural: ASSIGN #O-CONTRACT-ISSUED ( 5 ) := 0 + #O-CONTRACT-ISSUED ( 1:4 )
            ":",4)));
        pnd_O_Certif_Issued.getValue(5).compute(new ComputeParameters(false, pnd_O_Certif_Issued.getValue(5)), DbsField.add(getZero(),pnd_O_Certif_Issued.getValue(1,     //Natural: ASSIGN #O-CERTIF-ISSUED ( 5 ) := 0 + #O-CERTIF-ISSUED ( 1:4 )
            ":",4)));
        pnd_O_Certif_Issued2.getValue(5).compute(new ComputeParameters(false, pnd_O_Certif_Issued2.getValue(5)), DbsField.add(getZero(),pnd_O_Certif_Issued2.getValue(1,  //Natural: ASSIGN #O-CERTIF-ISSUED2 ( 5 ) := 0 + #O-CERTIF-ISSUED2 ( 1:4 )
            ":",4)));
        pnd_Contract_Issued.getValue(5).compute(new ComputeParameters(false, pnd_Contract_Issued.getValue(5)), DbsField.add(getZero(),pnd_Contract_Issued.getValue(1,     //Natural: ASSIGN #CONTRACT-ISSUED ( 5 ) := 0 + #CONTRACT-ISSUED ( 1:4 )
            ":",4)));
        pnd_Certif_Issued.getValue(5).compute(new ComputeParameters(false, pnd_Certif_Issued.getValue(5)), DbsField.add(getZero(),pnd_Certif_Issued.getValue(1,           //Natural: ASSIGN #CERTIF-ISSUED ( 5 ) := 0 + #CERTIF-ISSUED ( 1:4 )
            ":",4)));
        pnd_Certif_Issued2.getValue(5).compute(new ComputeParameters(false, pnd_Certif_Issued2.getValue(5)), DbsField.add(getZero(),pnd_Certif_Issued2.getValue(1,        //Natural: ASSIGN #CERTIF-ISSUED2 ( 5 ) := 0 + #CERTIF-ISSUED2 ( 1:4 )
            ":",4)));
        pnd_O_Contract_Issued_Tpa.getValue(5).compute(new ComputeParameters(false, pnd_O_Contract_Issued_Tpa.getValue(5)), DbsField.add(getZero(),pnd_O_Contract_Issued_Tpa.getValue(1, //Natural: ASSIGN #O-CONTRACT-ISSUED-TPA ( 5 ) := 0 + #O-CONTRACT-ISSUED-TPA ( 1:4 )
            ":",4)));
        pnd_Contract_Issued_Tpa.getValue(5).compute(new ComputeParameters(false, pnd_Contract_Issued_Tpa.getValue(5)), DbsField.add(getZero(),pnd_Contract_Issued_Tpa.getValue(1, //Natural: ASSIGN #CONTRACT-ISSUED-TPA ( 5 ) := 0 + #CONTRACT-ISSUED-TPA ( 1:4 )
            ":",4)));
        pnd_O_Contract_Issued_Ipro.getValue(5).compute(new ComputeParameters(false, pnd_O_Contract_Issued_Ipro.getValue(5)), DbsField.add(getZero(),pnd_O_Contract_Issued_Ipro.getValue(1, //Natural: ASSIGN #O-CONTRACT-ISSUED-IPRO ( 5 ) := 0 + #O-CONTRACT-ISSUED-IPRO ( 1:4 )
            ":",4)));
        pnd_Contract_Issued_Ipro.getValue(5).compute(new ComputeParameters(false, pnd_Contract_Issued_Ipro.getValue(5)), DbsField.add(getZero(),pnd_Contract_Issued_Ipro.getValue(1, //Natural: ASSIGN #CONTRACT-ISSUED-IPRO ( 5 ) := 0 + #CONTRACT-ISSUED-IPRO ( 1:4 )
            ":",4)));
    }
    private void sub_Mdo_Process() throws Exception                                                                                                                       //Natural: MDO-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(cis_Cis_Corp_Sync_Ind.equals("S") || cis_Cis_Corp_Sync_Ind.equals("G")))                                                                            //Natural: IF CIS-CORP-SYNC-IND = 'S' OR = 'G'
        {
            if (condition(cis_Cis_Cntrct_Type.equals("B")))                                                                                                               //Natural: IF CIS.CIS-CNTRCT-TYPE = 'B'
            {
                if (condition(cis_Cis_Cntrct_Apprvl_Ind.equals("Y")))                                                                                                     //Natural: IF CIS.CIS-CNTRCT-APPRVL-IND = 'Y'
                {
                    pnd_A.setValue(1);                                                                                                                                    //Natural: ASSIGN #A := 1
                    pnd_O_Aprl_Y_N.getValue(pnd_A).setValue("APPROVED STATES");                                                                                           //Natural: ASSIGN #O-APRL-Y-N ( #A ) := 'APPROVED STATES'
                    pnd_O_Apprvl.getValue(pnd_A).nadd(1);                                                                                                                 //Natural: ADD 1 TO #O-APPRVL ( #A )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cis_Cis_Cntrct_Apprvl_Ind.equals("N")))                                                                                                     //Natural: IF CIS.CIS-CNTRCT-APPRVL-IND = 'N'
                {
                    pnd_A.setValue(2);                                                                                                                                    //Natural: ASSIGN #A := 2
                    pnd_O_Aprl_Y_N.getValue(pnd_A).setValue("UNAPPROVED STATES");                                                                                         //Natural: ASSIGN #O-APRL-Y-N ( #A ) := 'UNAPPROVED STATES'
                    pnd_O_Apprvl.getValue(pnd_A).nadd(1);                                                                                                                 //Natural: ADD 1 TO #O-APPRVL ( #A )
                }                                                                                                                                                         //Natural: END-IF
                pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Units.getValue(pnd_A).nadd(cis_Cis_Rea_Annual_Nbr_Units);                                                            //Natural: ADD CIS.CIS-REA-ANNUAL-NBR-UNITS TO #O-MDO-REA-ANNTY-UNITS ( #A )
                pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Amt.getValue(pnd_A).nadd(cis_Cis_Rea_Annty_Amt);                                                                     //Natural: ADD CIS.CIS-REA-ANNTY-AMT TO #O-MDO-REA-ANNTY-AMT ( #A )
                pnd_O_Mdo_Fields_Pnd_O_Mdo_Traditional_Amt.getValue(pnd_A).nadd(cis_Cis_Mdo_Traditional_Amt);                                                             //Natural: ADD CIS.CIS-MDO-TRADITIONAL-AMT TO #O-MDO-TRADITIONAL-AMT ( #A )
                FOR11:                                                                                                                                                    //Natural: FOR #C 1 20
                for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
                {
                    //*  MMA
                    short decideConditionsMet967 = 0;                                                                                                                     //Natural: DECIDE ON EVERY VALUE OF CIS.CIS-CREF-ACCT-CDE ( #C );//Natural: VALUE 'M'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("M")))
                    {
                        decideConditionsMet967++;
                        //*  STOCK
                        pnd_O_Mdo_Fields_Pnd_O_Mdo_Mma.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                               //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-MDO-MMA ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'C'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("C")))
                    {
                        decideConditionsMet967++;
                        //*  SOCIAL CHOICE
                        pnd_O_Mdo_Fields_Pnd_O_Mdo_Stock.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                             //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-MDO-STOCK ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'S'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("S")))
                    {
                        decideConditionsMet967++;
                        //*  GLOBAL
                        pnd_O_Mdo_Fields_Pnd_O_Mdo_Social.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                            //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-MDO-SOCIAL ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'W'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("W")))
                    {
                        decideConditionsMet967++;
                        //*  BOND
                        pnd_O_Mdo_Fields_Pnd_O_Mdo_Global.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                            //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-MDO-GLOBAL ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'B'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("B")))
                    {
                        decideConditionsMet967++;
                        //*  GROWTH
                        pnd_O_Mdo_Fields_Pnd_O_Mdo_Bond.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                              //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-MDO-BOND ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'L'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("L")))
                    {
                        decideConditionsMet967++;
                        //*  EQUITY INDEX
                        pnd_O_Mdo_Fields_Pnd_O_Mdo_Grth.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                              //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-MDO-GRTH ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'E'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("E")))
                    {
                        decideConditionsMet967++;
                        //*  ILB
                        pnd_O_Mdo_Fields_Pnd_O_Mdo_Equ.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                               //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-MDO-EQU ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'I'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("I")))
                    {
                        decideConditionsMet967++;
                        pnd_O_Mdo_Fields_Pnd_O_Mdo_Ilb.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                               //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-MDO-ILB ( #A )
                    }                                                                                                                                                     //Natural: ANY VALUE
                    if (condition(decideConditionsMet967 > 0))
                    {
                        pnd_O_Mdo_Cref.getValue(pnd_A).setValue(true);                                                                                                    //Natural: ASSIGN #O-MDO-CREF ( #A ) := TRUE
                        pnd_O_Mdo_Fields_Pnd_O_Mdo_Units.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                             //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #O-MDO-UNITS ( #A )
                    }                                                                                                                                                     //Natural: NONE VALUE
                    if (condition(decideConditionsMet967 == 0))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Int_Pymnt.getValue(pnd_A).nadd(cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt);                                                           //Natural: ADD CIS.CIS-MDO-TIAA-INT-PYMNT-AMT TO #O-MDO-TIAA-INT-PYMNT ( #A )
                pnd_O_Mdo_Fields_Pnd_O_Mdo_Cref_Int_Pymnt.getValue(pnd_A).nadd(cis_Cis_Mdo_Cref_Int_Pymnt_Amt);                                                           //Natural: ADD CIS.CIS-MDO-CREF-INT-PYMNT-AMT TO #O-MDO-CREF-INT-PYMNT ( #A )
                pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Excl.getValue(pnd_A).nadd(cis_Cis_Mdo_Tiaa_Excluded_Amt);                                                                 //Natural: ADD CIS.CIS-MDO-TIAA-EXCLUDED-AMT TO #O-MDO-TIAA-EXCL ( #A )
                pnd_O_Mdo_Fields_Pnd_O_Mdo_Cref_Excl.getValue(pnd_A).nadd(cis_Cis_Mdo_Cref_Excluded_Amt);                                                                 //Natural: ADD CIS.CIS-MDO-CREF-EXCLUDED-AMT TO #O-MDO-CREF-EXCL ( #A )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cis_Cis_Cntrct_Type.equals("B")))                                                                                                               //Natural: IF CIS.CIS-CNTRCT-TYPE = 'B'
            {
                if (condition(cis_Cis_Cntrct_Apprvl_Ind.equals("Y")))                                                                                                     //Natural: IF CIS.CIS-CNTRCT-APPRVL-IND = 'Y'
                {
                    pnd_A.setValue(1);                                                                                                                                    //Natural: ASSIGN #A := 1
                    pnd_Aprl_Y_N.getValue(pnd_A).setValue("APPROVED STATES");                                                                                             //Natural: ASSIGN #APRL-Y-N ( #A ) := 'APPROVED STATES'
                    pnd_Apprvl.getValue(pnd_A).nadd(1);                                                                                                                   //Natural: ADD 1 TO #APPRVL ( #A )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cis_Cis_Cntrct_Apprvl_Ind.equals("N")))                                                                                                     //Natural: IF CIS.CIS-CNTRCT-APPRVL-IND = 'N'
                {
                    pnd_A.setValue(2);                                                                                                                                    //Natural: ASSIGN #A := 2
                    pnd_Aprl_Y_N.getValue(pnd_A).setValue("UNAPPROVED STATES");                                                                                           //Natural: ASSIGN #APRL-Y-N ( #A ) := 'UNAPPROVED STATES'
                    pnd_Apprvl.getValue(pnd_A).nadd(1);                                                                                                                   //Natural: ADD 1 TO #APPRVL ( #A )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units.getValue(pnd_A).nadd(cis_Cis_Rea_Annual_Nbr_Units);                                                                //Natural: ADD CIS.CIS-REA-ANNUAL-NBR-UNITS TO #MDO-REA-ANNTY-UNITS ( #A )
                pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt.getValue(pnd_A).nadd(cis_Cis_Rea_Annty_Amt);                                                                         //Natural: ADD CIS.CIS-REA-ANNTY-AMT TO #MDO-REA-ANNTY-AMT ( #A )
                pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt.getValue(pnd_A).nadd(cis_Cis_Mdo_Traditional_Amt);                                                                 //Natural: ADD CIS.CIS-MDO-TRADITIONAL-AMT TO #MDO-TRADITIONAL-AMT ( #A )
                FOR12:                                                                                                                                                    //Natural: FOR #C 1 20
                for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
                {
                    //*  MMA
                    short decideConditionsMet1022 = 0;                                                                                                                    //Natural: DECIDE ON EVERY VALUE OF CIS.CIS-CREF-ACCT-CDE ( #C );//Natural: VALUE 'M'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("M")))
                    {
                        decideConditionsMet1022++;
                        //*  STOCK
                        pnd_Mdo_Fields_Pnd_Mdo_Mma.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                   //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #MDO-MMA ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'C'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("C")))
                    {
                        decideConditionsMet1022++;
                        //*  SOCIAL CHOICE
                        pnd_Mdo_Fields_Pnd_Mdo_Stock.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                 //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #MDO-STOCK ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'S'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("S")))
                    {
                        decideConditionsMet1022++;
                        //*  GLOBAL
                        pnd_Mdo_Fields_Pnd_Mdo_Social.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #MDO-SOCIAL ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'W'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("W")))
                    {
                        decideConditionsMet1022++;
                        //*  BOND
                        pnd_Mdo_Fields_Pnd_Mdo_Global.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #MDO-GLOBAL ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'B'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("B")))
                    {
                        decideConditionsMet1022++;
                        //*  GROWTH
                        pnd_Mdo_Fields_Pnd_Mdo_Bond.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                  //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #MDO-BOND ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'L'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("L")))
                    {
                        decideConditionsMet1022++;
                        //*  EQUITY INDEX
                        pnd_Mdo_Fields_Pnd_Mdo_Grth.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                  //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #MDO-GRTH ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'E'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("E")))
                    {
                        decideConditionsMet1022++;
                        //*  ILB
                        pnd_Mdo_Fields_Pnd_Mdo_Equ.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                   //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #MDO-EQU ( #A )
                    }                                                                                                                                                     //Natural: VALUE 'I'
                    if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("I")))
                    {
                        decideConditionsMet1022++;
                        pnd_Mdo_Fields_Pnd_Mdo_Ilb.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                   //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #MDO-ILB ( #A )
                    }                                                                                                                                                     //Natural: ANY VALUE
                    if (condition(decideConditionsMet1022 > 0))
                    {
                        pnd_Mdo_Cref.getValue(pnd_A).setValue(true);                                                                                                      //Natural: ASSIGN #MDO-CREF ( #A ) := TRUE
                        pnd_Mdo_Fields_Pnd_Mdo_Units.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                 //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #MDO-UNITS ( #A )
                    }                                                                                                                                                     //Natural: NONE VALUE
                    if (condition(decideConditionsMet1022 == 0))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt.getValue(pnd_A).nadd(cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt);                                                               //Natural: ADD CIS.CIS-MDO-TIAA-INT-PYMNT-AMT TO #MDO-TIAA-INT-PYMNT ( #A )
                pnd_Mdo_Fields_Pnd_Mdo_Cref_Int_Pymnt.getValue(pnd_A).nadd(cis_Cis_Mdo_Cref_Int_Pymnt_Amt);                                                               //Natural: ADD CIS.CIS-MDO-CREF-INT-PYMNT-AMT TO #MDO-CREF-INT-PYMNT ( #A )
                pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl.getValue(pnd_A).nadd(cis_Cis_Mdo_Tiaa_Excluded_Amt);                                                                     //Natural: ADD CIS.CIS-MDO-TIAA-EXCLUDED-AMT TO #MDO-TIAA-EXCL ( #A )
                pnd_Mdo_Fields_Pnd_Mdo_Cref_Excl.getValue(pnd_A).nadd(cis_Cis_Mdo_Cref_Excluded_Amt);                                                                     //Natural: ADD CIS.CIS-MDO-CREF-EXCLUDED-AMT TO #MDO-CREF-EXCL ( #A )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mdo_Report() throws Exception                                                                                                                        //Natural: MDO-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************************
        //*  WRITE MDO REPORT
        if (condition(pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt.getValue("*").greater(getZero()) || pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt.getValue("*").greater(getZero())    //Natural: IF #MDO-TRADITIONAL-AMT ( * ) GT 0 OR #MDO-REA-ANNTY-AMT ( * ) GT 0 OR #MDO-REA-ANNTY-UNITS ( * ) GT 0 OR #MDO-CREF ( * ) = TRUE
            || pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units.getValue("*").greater(getZero()) || pnd_Mdo_Cref.getValue("*").equals(true)))
        {
            pnd_P.setValue(1);                                                                                                                                            //Natural: ASSIGN #P := 1
            if (condition(pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt.getValue(2).greater(getZero()) || pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt.getValue(2).greater(getZero())    //Natural: IF #MDO-TRADITIONAL-AMT ( 2 ) GT 0 OR #MDO-REA-ANNTY-AMT ( 2 ) GT 0 OR #MDO-REA-ANNTY-UNITS ( 2 ) GT 0 OR #MDO-CREF ( 2 ) = TRUE
                || pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units.getValue(2).greater(getZero()) || pnd_Mdo_Cref.getValue(2).equals(true)))
            {
                pnd_P.setValue(2);                                                                                                                                        //Natural: ASSIGN #P := 2
            }                                                                                                                                                             //Natural: END-IF
            FOR13:                                                                                                                                                        //Natural: FOR #A 1 #P
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_P)); pnd_A.nadd(1))
            {
                if (condition(pnd_A.equals(2)))                                                                                                                           //Natural: IF #A = 2
                {
                    getReports().newPage(new ReportSpecification(3));                                                                                                     //Natural: NEWPAGE ( 3 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"   Contract/Certificate Sets  ",new ColumnSpacing(55),pnd_Apprvl.getValue(pnd_A),           //Natural: WRITE ( 3 ) 1T '   Contract/Certificate Sets  ' 55X #APPRVL ( #A ) //
                    new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(3, ReportOption.NOTITLE,"TIAA MDO",NEWLINE,new ColumnSpacing(2),"Traditional Annuity  ",new ColumnSpacing(17),pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt.getValue(pnd_A),  //Natural: WRITE ( 3 ) 'TIAA MDO' / 2X 'Traditional Annuity  ' 17X #MDO-TRADITIONAL-AMT ( #A ) / 2X 'Real Estate Account  ' 17X #MDO-REA-ANNTY-AMT ( #A ) 7X #MDO-REA-ANNTY-UNITS ( #A ) /
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(2),"Real Estate Account  ",new ColumnSpacing(17),pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt.getValue(pnd_A), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units.getValue(pnd_A), new ReportEditMask 
                    ("ZZ,ZZZ,ZZ9.999"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(3, ReportOption.NOTITLE,NEWLINE,"CREF MDO",NEWLINE,"  Stock                ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Stock.getValue(pnd_A),  //Natural: WRITE ( 3 ) / 'CREF MDO' / '  Stock                ' 38X #MDO-STOCK ( #A ) / '  Money Market         ' 38X #MDO-MMA ( #A ) / '  Social Choice        ' 38X #MDO-SOCIAL ( #A ) / '  Bond                 ' 38X #MDO-BOND ( #A ) / '  Global Equities      ' 38X #MDO-GLOBAL ( #A ) / '  Growth               ' 38X #MDO-GRTH ( #A ) / '  Equity Index         ' 38X #MDO-EQU ( #A ) / '  Inflation Linked Bond' 38X #MDO-ILB ( #A )
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Money Market         ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Mma.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Social Choice        ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Social.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Bond                 ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Bond.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Global Equities      ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Global.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Growth               ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Grth.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Equity Index         ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Equ.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Inflation Linked Bond",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Ilb.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(35),"'First Required Payment'",new ColumnSpacing(12),"'Initial Excluded Amount'",NEWLINE,new  //Natural: WRITE ( 3 ) // 35T '"First Required Payment"' 12X '"Initial Excluded Amount"' / 35T '------------------------' 12X '-------------------------' /
                    TabSetting(35),"------------------------",new ColumnSpacing(12),"-------------------------",NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().display(3, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(1),"TIAA MDO",                                                          //Natural: DISPLAY ( 3 ) NOHDR ( HW = OFF ) 1T 'TIAA MDO' 30X #MDO-TIAA-INT-PYMNT ( #A ) ( IC = $ ) 21X #MDO-TIAA-EXCL ( #A ) ( IC = $ ) /
                		new ColumnSpacing(30),pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt.getValue(pnd_A), new InsertionCharacter("$"),new ColumnSpacing(21),pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl.getValue(pnd_A), 
                    new InsertionCharacter("$"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().display(3, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(1),"CREF MDO",                                                          //Natural: DISPLAY ( 3 ) NOHDR ( HW = OFF ) 1T 'CREF MDO' 30X #MDO-CREF-INT-PYMNT ( #A ) ( IC = $ ) 21X #MDO-CREF-EXCL ( #A ) ( IC = $ ) /
                		new ColumnSpacing(30),pnd_Mdo_Fields_Pnd_Mdo_Cref_Int_Pymnt.getValue(pnd_A), new InsertionCharacter("$"),new ColumnSpacing(21),pnd_Mdo_Fields_Pnd_Mdo_Cref_Excl.getValue(pnd_A), 
                    new InsertionCharacter("$"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_A.setValue(1);                                                                                                                                            //Natural: ASSIGN #A := 1
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(10),"*****************************************************",NEWLINE,new  //Natural: WRITE ( 3 ) /// / 10X '*****************************************************' / 10X '*                                                   *' / 10X '*                                                   *' / 10X '*      NO LEGACY MDO TRANSACTIONS IN THIS RUN       *' / 10X '*                                                   *' / 10X '*                                                   *' / 10X '*****************************************************'
                ColumnSpacing(10),"*                                                   *",NEWLINE,new ColumnSpacing(10),"*                                                   *",NEWLINE,new 
                ColumnSpacing(10),"*      NO LEGACY MDO TRANSACTIONS IN THIS RUN       *",NEWLINE,new ColumnSpacing(10),"*                                                   *",NEWLINE,new 
                ColumnSpacing(10),"*                                                   *",NEWLINE,new ColumnSpacing(10),"*****************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_O_Mdo_Fields_Pnd_O_Mdo_Traditional_Amt.getValue("*").greater(getZero()) || pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Amt.getValue("*").greater(getZero())  //Natural: IF #O-MDO-TRADITIONAL-AMT ( * ) GT 0 OR #O-MDO-REA-ANNTY-AMT ( * ) GT 0 OR #O-MDO-REA-ANNTY-UNITS ( * ) GT 0 OR #O-MDO-CREF ( * ) = TRUE
            || pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Units.getValue("*").greater(getZero()) || pnd_O_Mdo_Cref.getValue("*").equals(true)))
        {
            pnd_P.setValue(1);                                                                                                                                            //Natural: ASSIGN #P := 1
            if (condition(pnd_O_Mdo_Fields_Pnd_O_Mdo_Traditional_Amt.getValue(2).greater(getZero()) || pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Amt.getValue(2).greater(getZero())  //Natural: IF #O-MDO-TRADITIONAL-AMT ( 2 ) GT 0 OR #O-MDO-REA-ANNTY-AMT ( 2 ) GT 0 OR #O-MDO-REA-ANNTY-UNITS ( 2 ) GT 0 OR #O-MDO-CREF ( 2 ) = TRUE
                || pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Units.getValue(2).greater(getZero()) || pnd_O_Mdo_Cref.getValue(2).equals(true)))
            {
                pnd_P.setValue(2);                                                                                                                                        //Natural: ASSIGN #P := 2
            }                                                                                                                                                             //Natural: END-IF
            FOR14:                                                                                                                                                        //Natural: FOR #A 1 #P
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_P)); pnd_A.nadd(1))
            {
                if (condition(pnd_A.equals(2)))                                                                                                                           //Natural: IF #A = 2
                {
                    getReports().newPage(new ReportSpecification(4));                                                                                                     //Natural: NEWPAGE ( 4 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),"   Contract/Certificate Sets  ",new ColumnSpacing(55),pnd_O_Apprvl.getValue(pnd_A),         //Natural: WRITE ( 4 ) 1T '   Contract/Certificate Sets  ' 55X #O-APPRVL ( #A ) //
                    new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(4, ReportOption.NOTITLE,"TIAA MDO",NEWLINE,new ColumnSpacing(2),"Traditional Annuity  ",new ColumnSpacing(17),pnd_O_Mdo_Fields_Pnd_O_Mdo_Traditional_Amt.getValue(pnd_A),  //Natural: WRITE ( 4 ) 'TIAA MDO' / 2X 'Traditional Annuity  ' 17X #O-MDO-TRADITIONAL-AMT ( #A ) / 2X 'Real Estate Account  ' 17X #O-MDO-REA-ANNTY-AMT ( #A ) 7X #O-MDO-REA-ANNTY-UNITS ( #A ) /
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(2),"Real Estate Account  ",new ColumnSpacing(17),pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Amt.getValue(pnd_A), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Units.getValue(pnd_A), new ReportEditMask 
                    ("ZZ,ZZZ,ZZ9.999"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(4, ReportOption.NOTITLE,NEWLINE,"CREF MDO",NEWLINE,"  Stock                ",new ColumnSpacing(38),pnd_O_Mdo_Fields_Pnd_O_Mdo_Stock.getValue(pnd_A),  //Natural: WRITE ( 4 ) / 'CREF MDO' / '  Stock                ' 38X #O-MDO-STOCK ( #A ) / '  Money Market         ' 38X #O-MDO-MMA ( #A ) / '  Social Choice        ' 38X #O-MDO-SOCIAL ( #A ) / '  Bond                 ' 38X #O-MDO-BOND ( #A ) / '  Global Equities      ' 38X #O-MDO-GLOBAL ( #A ) / '  Growth               ' 38X #O-MDO-GRTH ( #A ) / '  Equity Index         ' 38X #O-MDO-EQU ( #A ) / '  Inflation Linked Bond' 38X #O-MDO-ILB ( #A )
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Money Market         ",new ColumnSpacing(38),pnd_O_Mdo_Fields_Pnd_O_Mdo_Mma.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Social Choice        ",new ColumnSpacing(38),pnd_O_Mdo_Fields_Pnd_O_Mdo_Social.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Bond                 ",new ColumnSpacing(38),pnd_O_Mdo_Fields_Pnd_O_Mdo_Bond.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Global Equities      ",new ColumnSpacing(38),pnd_O_Mdo_Fields_Pnd_O_Mdo_Global.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Growth               ",new ColumnSpacing(38),pnd_O_Mdo_Fields_Pnd_O_Mdo_Grth.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Equity Index         ",new ColumnSpacing(38),pnd_O_Mdo_Fields_Pnd_O_Mdo_Equ.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Inflation Linked Bond",new ColumnSpacing(38),pnd_O_Mdo_Fields_Pnd_O_Mdo_Ilb.getValue(pnd_A), 
                    new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(35),"'First Required Payment'",new ColumnSpacing(12),"'Initial Excluded Amount'",NEWLINE,new  //Natural: WRITE ( 4 ) // 35T '"First Required Payment"' 12X '"Initial Excluded Amount"' / 35T '------------------------' 12X '-------------------------' /
                    TabSetting(35),"------------------------",new ColumnSpacing(12),"-------------------------",NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().display(4, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(1),"TIAA MDO",                                                          //Natural: DISPLAY ( 4 ) NOHDR ( HW = OFF ) 1T 'TIAA MDO' 30X #O-MDO-TIAA-INT-PYMNT ( #A ) ( IC = $ ) 21X #O-MDO-TIAA-EXCL ( #A ) ( IC = $ ) /
                		new ColumnSpacing(30),pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Int_Pymnt.getValue(pnd_A), new InsertionCharacter("$"),new ColumnSpacing(21),pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Excl.getValue(pnd_A), 
                    new InsertionCharacter("$"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().display(4, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(1),"CREF MDO",                                                          //Natural: DISPLAY ( 4 ) NOHDR ( HW = OFF ) 1T 'CREF MDO' 30X #O-MDO-CREF-INT-PYMNT ( #A ) ( IC = $ ) 21X #O-MDO-CREF-EXCL ( #A ) ( IC = $ ) /
                		new ColumnSpacing(30),pnd_O_Mdo_Fields_Pnd_O_Mdo_Cref_Int_Pymnt.getValue(pnd_A), new InsertionCharacter("$"),new ColumnSpacing(21),pnd_O_Mdo_Fields_Pnd_O_Mdo_Cref_Excl.getValue(pnd_A), 
                    new InsertionCharacter("$"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_A.setValue(1);                                                                                                                                            //Natural: ASSIGN #A := 1
            getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(10),"*****************************************************",NEWLINE,new  //Natural: WRITE ( 4 ) /// / 10X '*****************************************************' / 10X '*                                                   *' / 10X '*                                                   *' / 10X '*       NO OMNI MDO TRANSACTIONS IN THIS RUN        *' / 10X '*                                                   *' / 10X '*                                                   *' / 10X '*****************************************************'
                ColumnSpacing(10),"*                                                   *",NEWLINE,new ColumnSpacing(10),"*                                                   *",NEWLINE,new 
                ColumnSpacing(10),"*       NO OMNI MDO TRANSACTIONS IN THIS RUN        *",NEWLINE,new ColumnSpacing(10),"*                                                   *",NEWLINE,new 
                ColumnSpacing(10),"*                                                   *",NEWLINE,new ColumnSpacing(10),"*****************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*   #C IS ARRAY
    private void sub_S700_Print_Array() throws Exception                                                                                                                  //Natural: S700-PRINT-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************************8******
        if (condition(cis_Cis_Corp_Sync_Ind.equals("S") || cis_Cis_Corp_Sync_Ind.equals("G") || pnd_Omni.getBoolean()))                                                   //Natural: IF CIS-CORP-SYNC-IND = 'S' OR = 'G' OR #OMNI
        {
            if (condition(cis_Cis_Rtb_Amt.notEquals(getZero())))                                                                                                          //Natural: IF CIS-RTB-AMT NE 0
            {
                //*  TACC KG
                getReports().write(6, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Rtb_Amt,new       //Natural: WRITE ( 6 ) '-' / 01T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-RTB-AMT 120T 'RTB'
                    TabSetting(120),"RTB");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Mdo_Traditional_Amt.notEquals(getZero())))                                                                                              //Natural: IF CIS-MDO-TRADITIONAL-AMT NE 0
            {
                //*  TACC KG
                getReports().write(6, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Mdo_Traditional_Amt,new  //Natural: WRITE ( 6 ) '-' / 01T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-MDO-TRADITIONAL-AMT 120T 'MDO'
                    TabSetting(120),"MDO");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Rea_Mnthly_Nbr_Units.notEquals(getZero()) || cis_Cis_Rea_Annual_Nbr_Units.notEquals(getZero()) || cis_Cis_Rea_Annty_Amt.notEquals(getZero()))) //Natural: IF CIS-REA-MNTHLY-NBR-UNITS NE 0 OR CIS-REA-ANNUAL-NBR-UNITS NE 0 OR CIS-REA-ANNTY-AMT NE 0
            {
                //*  TACC KG ST
                //*  TACC KG END
                getReports().write(6, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(29),cis_Cis_Rea_Mnthly_Nbr_Units,new  //Natural: WRITE ( 6 ) '-' / 01T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 29T CIS-REA-MNTHLY-NBR-UNITS 44T CIS-REA-ANNUAL-NBR-UNITS 89T CIS-REA-ANNTY-AMT 120T 'REA'
                    TabSetting(44),cis_Cis_Rea_Annual_Nbr_Units,new TabSetting(89),cis_Cis_Rea_Annty_Amt,new TabSetting(120),"REA");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Grnted_Grd_Amt.notEquals(getZero())))                                                                                                   //Natural: IF CIS-GRNTED-GRD-AMT NE 0
            {
                //*  TACC KG
                getReports().write(6, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Grnted_Grd_Amt,new  //Natural: WRITE ( 6 ) '-' / 01T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-GRNTED-GRD-AMT 120T 'GRD AMT'
                    TabSetting(120),"GRD AMT");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Grnted_Std_Amt.notEquals(getZero())))                                                                                                   //Natural: IF CIS-GRNTED-STD-AMT NE 0
            {
                //*  TACC KG
                getReports().write(6, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Grnted_Std_Amt,new  //Natural: WRITE ( 6 ) '-' / 01T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-GRNTED-STD-AMT 120T 'STD AMT'
                    TabSetting(120),"STD AMT");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            FOR15:                                                                                                                                                        //Natural: FOR #C FROM 1 TO 20
            for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
            {
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).notEquals(" ") || cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C).notEquals(getZero())                  //Natural: IF CIS.CIS-CREF-ACCT-CDE ( #C ) NE ' ' OR CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) NE 0 OR CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) NE 0 OR CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #C ) NE 0 OR CIS.CIS-DA-CREF-PROCEEDS-AMT ( #C ) NE 0 OR CIS.CIS-DA-REA-PROCEEDS-AMT ( #C ) NE 0
                    || cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C).notEquals(getZero()) || cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_C).notEquals(getZero()) 
                    || cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_C).notEquals(getZero()) || cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_C).notEquals(getZero())))
                {
                    //*  TACC KG ST
                    //*  TACC KG END
                    getReports().write(6, new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(25),cis_Cis_Cref_Acct_Cde.getValue(pnd_C),new  //Natural: WRITE ( 6 ) 1T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 25T CIS.CIS-CREF-ACCT-CDE ( #C ) 29T CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) 44T CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) 59T CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #C ) 74T CIS.CIS-DA-CREF-PROCEEDS-AMT ( #C ) 89T CIS.CIS-DA-REA-PROCEEDS-AMT ( #C ) 120T CIS-DA-TIAA-NBR ( #C )
                        TabSetting(29),cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C),new TabSetting(44),cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C),new 
                        TabSetting(59),cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_C),new TabSetting(74),cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_C),new 
                        TabSetting(89),cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_C),new TabSetting(120),cis_Cis_Da_Tiaa_Nbr.getValue(pnd_C));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  TACC KG ST
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            FOR16:                                                                                                                                                        //Natural: FOR #D 1 100
            for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(100)); pnd_D.nadd(1))
            {
                if (condition(cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D).notEquals(getZero()) || cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D).notEquals(getZero())))  //Natural: IF CIS-TACC-MNTHLY-NBR-UNITS ( #D ) NE 0 OR CIS-TACC-ANNUAL-NBR-UNITS ( #D ) NE 0
                {
                    getReports().write(6, new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(29),cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D),new  //Natural: WRITE ( 6 ) 1T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 29T CIS-TACC-MNTHLY-NBR-UNITS ( #D ) 44T CIS-TACC-ANNUAL-NBR-UNITS ( #D ) 104T CIS-TACC-ANNTY-AMT 120T 'T ACCESS'
                        TabSetting(44),cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D),new TabSetting(104),cis_Cis_Tacc_Annty_Amt,new TabSetting(120),"T ACCESS");
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  TACC KG END
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cis_Cis_Rtb_Amt.notEquals(getZero())))                                                                                                          //Natural: IF CIS-RTB-AMT NE 0
            {
                //*  TACC KG
                getReports().write(5, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Rtb_Amt,new       //Natural: WRITE ( 5 ) '-' / 01T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-RTB-AMT 120T 'RTB'
                    TabSetting(120),"RTB");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Mdo_Traditional_Amt.notEquals(getZero())))                                                                                              //Natural: IF CIS-MDO-TRADITIONAL-AMT NE 0
            {
                //*  TACC KG
                getReports().write(5, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Mdo_Traditional_Amt,new  //Natural: WRITE ( 5 ) '-' / 01T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-MDO-TRADITIONAL-AMT 120T 'MDO'
                    TabSetting(120),"MDO");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Rea_Mnthly_Nbr_Units.notEquals(getZero()) || cis_Cis_Rea_Annual_Nbr_Units.notEquals(getZero()) || cis_Cis_Rea_Annty_Amt.notEquals(getZero()))) //Natural: IF CIS-REA-MNTHLY-NBR-UNITS NE 0 OR CIS-REA-ANNUAL-NBR-UNITS NE 0 OR CIS-REA-ANNTY-AMT NE 0
            {
                //*  TACC KG ST
                //*  TACC KG END
                getReports().write(5, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(29),cis_Cis_Rea_Mnthly_Nbr_Units,new  //Natural: WRITE ( 5 ) '-' / 01T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 29T CIS-REA-MNTHLY-NBR-UNITS 44T CIS-REA-ANNUAL-NBR-UNITS 89T CIS-REA-ANNTY-AMT 120T 'REA'
                    TabSetting(44),cis_Cis_Rea_Annual_Nbr_Units,new TabSetting(89),cis_Cis_Rea_Annty_Amt,new TabSetting(120),"REA");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Grnted_Grd_Amt.notEquals(getZero())))                                                                                                   //Natural: IF CIS-GRNTED-GRD-AMT NE 0
            {
                //*  TACC KG
                getReports().write(5, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Grnted_Grd_Amt,new  //Natural: WRITE ( 5 ) '-' / 01T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-GRNTED-GRD-AMT 120T 'GRD AMT'
                    TabSetting(120),"GRD AMT");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Grnted_Std_Amt.notEquals(getZero())))                                                                                                   //Natural: IF CIS-GRNTED-STD-AMT NE 0
            {
                //*  TACC KG
                getReports().write(5, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Grnted_Std_Amt,new  //Natural: WRITE ( 5 ) '-' / 01T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-GRNTED-STD-AMT 120T 'STD AMT'
                    TabSetting(120),"STD AMT");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            FOR17:                                                                                                                                                        //Natural: FOR #C FROM 1 TO 20
            for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
            {
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).notEquals(" ") || cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C).notEquals(getZero())                  //Natural: IF CIS.CIS-CREF-ACCT-CDE ( #C ) NE ' ' OR CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) NE 0 OR CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) NE 0 OR CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #C ) NE 0 OR CIS.CIS-DA-CREF-PROCEEDS-AMT ( #C ) NE 0 OR CIS.CIS-DA-REA-PROCEEDS-AMT ( #C ) NE 0
                    || cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C).notEquals(getZero()) || cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_C).notEquals(getZero()) 
                    || cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_C).notEquals(getZero()) || cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_C).notEquals(getZero())))
                {
                    //*  TACC KG ST
                    //*  TACC KG END
                    getReports().write(5, new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(25),cis_Cis_Cref_Acct_Cde.getValue(pnd_C),new  //Natural: WRITE ( 5 ) 1T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 25T CIS.CIS-CREF-ACCT-CDE ( #C ) 29T CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) 44T CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) 59T CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #C ) 74T CIS.CIS-DA-CREF-PROCEEDS-AMT ( #C ) 89T CIS.CIS-DA-REA-PROCEEDS-AMT ( #C ) 120T CIS-DA-TIAA-NBR ( #C )
                        TabSetting(29),cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C),new TabSetting(44),cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C),new 
                        TabSetting(59),cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_C),new TabSetting(74),cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_C),new 
                        TabSetting(89),cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_C),new TabSetting(120),cis_Cis_Da_Tiaa_Nbr.getValue(pnd_C));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  TACC KG ST
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            FOR18:                                                                                                                                                        //Natural: FOR #D 1 100
            for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(100)); pnd_D.nadd(1))
            {
                if (condition(cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D).notEquals(getZero()) || cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D).notEquals(getZero())))  //Natural: IF CIS-TACC-MNTHLY-NBR-UNITS ( #D ) NE 0 OR CIS-TACC-ANNUAL-NBR-UNITS ( #D ) NE 0
                {
                    getReports().write(5, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(29),cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D),new  //Natural: WRITE ( 5 ) '-' / 1T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 29T CIS-TACC-MNTHLY-NBR-UNITS ( #D ) 44T CIS-TACC-ANNUAL-NBR-UNITS ( #D ) 104T CIS-TACC-ANNTY-AMT 120T 'T ACCESS'
                        TabSetting(44),cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D),new TabSetting(104),cis_Cis_Tacc_Annty_Amt,new TabSetting(120),"T ACCESS");
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  TACC KG END
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Platform_Ind() throws Exception                                                                                                                  //Natural: GET-PLATFORM-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(cis_Cis_Cntrct_Type.equals("T") || cis_Cis_Cntrct_Type.equals("B")))                                                                                //Natural: IF CIS.CIS-CNTRCT-TYPE = 'T' OR = 'B'
        {
            pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr.setValue(cis_Cis_Tiaa_Nbr);                                                                                           //Natural: ASSIGN #TRANS-PPCN-NBR := CIS-TIAA-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr.setValue(cis_Cis_Cert_Nbr);                                                                                           //Natural: ASSIGN #TRANS-PPCN-NBR := CIS-CERT-NBR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde.setValue(1);                                                                                                             //Natural: ASSIGN #TRANS-PAYEE-CDE := 01
        pnd_Trans_Cntrct_Key_Pnd_Invrse_Trans_Dte.reset();                                                                                                                //Natural: RESET #INVRSE-TRANS-DTE #TRANS-CDE
        pnd_Trans_Cntrct_Key_Pnd_Trans_Cde.reset();
        vw_iaa_Trans_Rcrd.startDatabaseRead                                                                                                                               //Natural: READ IAA-TRANS-RCRD BY TRANS-CNTRCT-KEY STARTING FROM #TRANS-CNTRCT-KEY
        (
        "READ02",
        new Wc[] { new Wc("TRANS_CNTRCT_KEY", ">=", pnd_Trans_Cntrct_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CNTRCT_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_iaa_Trans_Rcrd.readNextRow("READ02")))
        {
            if (condition(iaa_Trans_Rcrd_Trans_Ppcn_Nbr.notEquals(pnd_Trans_Cntrct_Key_Pnd_Trans_Ppcn_Nbr) || iaa_Trans_Rcrd_Trans_Payee_Cde.notEquals(pnd_Trans_Cntrct_Key_Pnd_Trans_Payee_Cde))) //Natural: IF TRANS-PPCN-NBR NE #TRANS-PPCN-NBR OR TRANS-PAYEE-CDE NE #TRANS-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(iaa_Trans_Rcrd_Trans_Cde.equals(31)))                                                                                                           //Natural: IF TRANS-CDE = 31
            {
                pnd_Omni.setValue(true);                                                                                                                                  //Natural: ASSIGN #OMNI := TRUE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    //*  TACC KG
    //*  TACC KG
    //*  TACC KG
    //*  TACC KG
    //*  TACC KG
    //*  TACC KG
    //*  TACC KG
    private void sub_Total_Detail_Report() throws Exception                                                                                                               //Natural: TOTAL-DETAIL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(5));                                                                                                                 //Natural: NEWPAGE ( 5 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(5, 5);                                                                                                                                          //Natural: SKIP ( 5 ) 5
        getReports().write(5, NEWLINE,NEWLINE,"SUMMARY TOTALS",NEWLINE,"==============",NEWLINE,NEWLINE);                                                                 //Natural: WRITE ( 5 ) // 'SUMMARY TOTALS' / '==============' //
        if (Global.isEscape()) return;
        getReports().write(5, "TRADITIONAL ANNUITY",NEWLINE,"-------------------",NEWLINE,"  Legacy Accumulation Applied       ($)",new TabSetting(41),pnd_Da_Tiaa_Proceeds_Amt.getValue(5),  //Natural: WRITE ( 5 ) 'TRADITIONAL ANNUITY' / '-------------------' / '  Legacy Accumulation Applied       ($)' 41T #DA-TIAA-PROCEEDS-AMT ( 5 ) / '  Legacy Annuity Income             ($)' 41T #TRAD-ANNUITY ( 5 ) // '  Legacy MDO Accumulation Applied   ($)' 41T #MDO-TRADITIONAL-AMT ( 1 ) //
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  Legacy Annuity Income             ($)",new TabSetting(41),pnd_Trad_Annuity.getValue(5), new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,"  Legacy MDO Accumulation Applied   ($)",new TabSetting(41),pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(5, "TPA",NEWLINE,"---",NEWLINE,"  Legacy Accumulation Applied       ($)",new TabSetting(41),pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(5),          //Natural: WRITE ( 5 ) 'TPA' / '---' / '  Legacy Accumulation Applied       ($)' 41T #DA-TIAA-PROCEEDS-AMT-TPA ( 5 ) / '  Legacy Annuity Income             ($)' 41T #TRAD-ANNUITY-TPA ( 5 ) //
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  Legacy Annuity Income             ($)",new TabSetting(41),pnd_Trad_Annuity_Tpa.getValue(5), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(5, "IPRO",NEWLINE,"----",NEWLINE,"  Legacy Accumulation Applied       ($)",new TabSetting(41),pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(5),       //Natural: WRITE ( 5 ) 'IPRO' / '----' / '  Legacy Accumulation Applied       ($)' 41T #DA-TIAA-PROCEEDS-AMT-IPRO ( 5 ) / '  Legacy Annuity Income             ($)' 41T #TRAD-ANNUITY-IPRO ( 5 ) //
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  Legacy Annuity Income             ($)",new TabSetting(41),pnd_Trad_Annuity_Ipro.getValue(5), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(5, "REAL ESTATE AND ACCESS ACCOUNTS",NEWLINE,"-------------------------------",NEWLINE,"  Legacy Accumulation Applied       ($)",new           //Natural: WRITE ( 5 ) 'REAL ESTATE AND ACCESS ACCOUNTS' / '-------------------------------' / '  Legacy Accumulation Applied       ($)' 41T #DA-REA-PROCEEDS-AMT ( 5 ) / '  Legacy Annuity Income - REA       ($)' 41T #REAL-ANNUITY ( 5 ) / '  Legacy Annuity Income - ACC       ($)' 41T #TACC-ANNUITY ( 5 ) // '  Legacy MDO Accumulation Applied   ($)' 41T #MDO-REA-ANNTY-AMT ( 1 ) / '  Legacy MDO Annuity Income (Units)   ' 41T #MDO-REA-ANNTY-UNITS ( 1 ) //
            TabSetting(41),pnd_Da_Rea_Proceeds_Amt.getValue(5), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  Legacy Annuity Income - REA       ($)",new 
            TabSetting(41),pnd_Real_Annuity.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Legacy Annuity Income - ACC       ($)",new TabSetting(41),pnd_Tacc_Annuity.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"  Legacy MDO Accumulation Applied   ($)",new TabSetting(41),pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  Legacy MDO Annuity Income (Units)   ",new TabSetting(41),pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(5, "CREF",NEWLINE,"----",NEWLINE,"  Legacy Accumulation Applied       ($)",new TabSetting(41),pnd_Da_Cref_Proceeds_Amt.getValue(5),            //Natural: WRITE ( 5 ) 'CREF' / '----' / '  Legacy Accumulation Applied       ($)' 41T #DA-CREF-PROCEEDS-AMT ( 5 ) / '  Legacy Annuity Income             ($)' 41T #CREF-ANNUITY ( 5 ) // '  CREF MDO' / '    Stock                ' 42T #MDO-STOCK ( 1 ) / '    Money Market         ' 42T #MDO-MMA ( 1 ) / '    Social Choice        ' 42T #MDO-SOCIAL ( 1 ) / '    Bond                 ' 42T #MDO-BOND ( 1 ) / '    Global Equities      ' 42T #MDO-GLOBAL ( 1 ) / '    Growth               ' 42T #MDO-GRTH ( 1 ) / '    Equity Index         ' 42T #MDO-EQU ( 1 ) / '    Inflation Linked Bond' 42T #MDO-ILB ( 1 ) / '  Total MDO Annuity Income      (Units)' 41T #MDO-UNITS ( 1 ) //
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  Legacy Annuity Income             ($)",new TabSetting(41),pnd_Cref_Annuity.getValue(5), new 
            ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"  CREF MDO",NEWLINE,"    Stock                ",new TabSetting(42),pnd_Mdo_Fields_Pnd_Mdo_Stock.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Money Market         ",new TabSetting(42),pnd_Mdo_Fields_Pnd_Mdo_Mma.getValue(1), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Social Choice        ",new TabSetting(42),pnd_Mdo_Fields_Pnd_Mdo_Social.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Bond                 ",new 
            TabSetting(42),pnd_Mdo_Fields_Pnd_Mdo_Bond.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Global Equities      ",new TabSetting(42),pnd_Mdo_Fields_Pnd_Mdo_Global.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Growth               ",new TabSetting(42),pnd_Mdo_Fields_Pnd_Mdo_Grth.getValue(1), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Equity Index         ",new TabSetting(42),pnd_Mdo_Fields_Pnd_Mdo_Equ.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Inflation Linked Bond",new 
            TabSetting(42),pnd_Mdo_Fields_Pnd_Mdo_Ilb.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Total MDO Annuity Income      (Units)",new 
            TabSetting(41),pnd_Mdo_Fields_Pnd_Mdo_Units.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(6));                                                                                                                 //Natural: NEWPAGE ( 6 )
        if (condition(Global.isEscape())){return;}
        getReports().skip(6, 5);                                                                                                                                          //Natural: SKIP ( 6 ) 5
        getReports().write(6, NEWLINE,NEWLINE,"SUMMARY TOTALS",NEWLINE,"==============",NEWLINE,NEWLINE);                                                                 //Natural: WRITE ( 6 ) // 'SUMMARY TOTALS' / '==============' //
        if (Global.isEscape()) return;
        getReports().write(6, "TRADITIONAL ANNUITY",NEWLINE,"-------------------",NEWLINE,"  OMNI   Accumulation Applied      ($)",new TabSetting(40),pnd_O_Da_Tiaa_Proceeds_Amt.getValue(5),  //Natural: WRITE ( 6 ) 'TRADITIONAL ANNUITY' / '-------------------' / '  OMNI   Accumulation Applied      ($)' 40T #O-DA-TIAA-PROCEEDS-AMT ( 5 ) / '  OMNI   Annuity Income            ($)' 40T #O-TRAD-ANNUITY ( 5 ) // '  OMNI   MDO Accumulation Applied  ($)' 40T #O-MDO-TRADITIONAL-AMT ( 1 ) //
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  OMNI   Annuity Income            ($)",new TabSetting(40),pnd_O_Trad_Annuity.getValue(5), new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,"  OMNI   MDO Accumulation Applied  ($)",new TabSetting(40),pnd_O_Mdo_Fields_Pnd_O_Mdo_Traditional_Amt.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(6, "TPA",NEWLINE,"---",NEWLINE,"  OMNI   Accumulation Applied      ($)",new TabSetting(40),pnd_O_Da_Tiaa_Proceeds_Amt_Tpa.getValue(5),         //Natural: WRITE ( 6 ) 'TPA' / '---' / '  OMNI   Accumulation Applied      ($)' 40T #O-DA-TIAA-PROCEEDS-AMT-TPA ( 5 ) / '  OMNI   Annuity Income            ($)' 40T #O-TRAD-ANNUITY-TPA ( 5 ) //
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  OMNI   Annuity Income            ($)",new TabSetting(40),pnd_O_Trad_Annuity_Tpa.getValue(5), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(6, "IPRO",NEWLINE,"----",NEWLINE,"  OMNI   Accumulation Applied      ($)",new TabSetting(40),pnd_O_Da_Tiaa_Proceeds_Amt_Ipro.getValue(5),      //Natural: WRITE ( 6 ) 'IPRO' / '----' / '  OMNI   Accumulation Applied      ($)' 40T #O-DA-TIAA-PROCEEDS-AMT-IPRO ( 5 ) / '  OMNI   Annuity Income            ($)' 40T #O-TRAD-ANNUITY-IPRO ( 5 ) //
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  OMNI   Annuity Income            ($)",new TabSetting(40),pnd_O_Trad_Annuity_Ipro.getValue(5), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(6, "REAL ESTATE AND ACCESS ACCOUNTS",NEWLINE,"-------------------------------",NEWLINE,"  OMNI   Accumulation Applied      ($)",new            //Natural: WRITE ( 6 ) 'REAL ESTATE AND ACCESS ACCOUNTS' / '-------------------------------' / '  OMNI   Accumulation Applied      ($)' 40T #O-DA-REA-PROCEEDS-AMT ( 5 ) / '  OMNI   Annuity Income - REA      ($)' 40T #O-REAL-ANNUITY ( 5 ) / '  OMNI   Annuity Income - ACC      ($)' 40T #O-TACC-ANNUITY ( 5 ) // '  OMNI   MDO Accumulation Applied  ($)' 40T #O-MDO-REA-ANNTY-AMT ( 1 ) / '  OMNI   MDO Annuity Income (Units)   ' 40T #O-MDO-REA-ANNTY-UNITS ( 1 ) //
            TabSetting(40),pnd_O_Da_Rea_Proceeds_Amt.getValue(5), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  OMNI   Annuity Income - REA      ($)",new 
            TabSetting(40),pnd_O_Real_Annuity.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  OMNI   Annuity Income - ACC      ($)",new TabSetting(40),pnd_O_Tacc_Annuity.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"  OMNI   MDO Accumulation Applied  ($)",new TabSetting(40),pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Amt.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  OMNI   MDO Annuity Income (Units)   ",new TabSetting(40),pnd_O_Mdo_Fields_Pnd_O_Mdo_Rea_Annty_Units.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(6, "CREF",NEWLINE,"----",NEWLINE,"  OMNI   Accumulation Applied      ($)",new TabSetting(40),pnd_O_Da_Cref_Proceeds_Amt.getValue(5),           //Natural: WRITE ( 6 ) 'CREF' / '----' / '  OMNI   Accumulation Applied      ($)' 40T #O-DA-CREF-PROCEEDS-AMT ( 5 ) / '  OMNI   Annuity Income            ($)' 40T #O-CREF-ANNUITY ( 5 ) // '  CREF MDO' / '    Stock                ' 41T #O-MDO-STOCK ( 1 ) / '    Money Market         ' 41T #O-MDO-MMA ( 1 ) / '    Social Choice        ' 41T #O-MDO-SOCIAL ( 1 ) / '    Bond                 ' 41T #O-MDO-BOND ( 1 ) / '    Global Equities      ' 41T #O-MDO-GLOBAL ( 1 ) / '    Growth               ' 41T #O-MDO-GRTH ( 1 ) / '    Equity Index         ' 41T #O-MDO-EQU ( 1 ) / '    Inflation Linked Bond' 41T #O-MDO-ILB ( 1 ) / '  Total OMNI MDO Annuity Income(Units)' 40T #O-MDO-UNITS ( 1 ) //
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,"  OMNI   Annuity Income            ($)",new TabSetting(40),pnd_O_Cref_Annuity.getValue(5), new 
            ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"  CREF MDO",NEWLINE,"    Stock                ",new TabSetting(41),pnd_O_Mdo_Fields_Pnd_O_Mdo_Stock.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Money Market         ",new TabSetting(41),pnd_O_Mdo_Fields_Pnd_O_Mdo_Mma.getValue(1), new 
            ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Social Choice        ",new TabSetting(41),pnd_O_Mdo_Fields_Pnd_O_Mdo_Social.getValue(1), new 
            ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Bond                 ",new TabSetting(41),pnd_O_Mdo_Fields_Pnd_O_Mdo_Bond.getValue(1), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Global Equities      ",new TabSetting(41),pnd_O_Mdo_Fields_Pnd_O_Mdo_Global.getValue(1), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Growth               ",new TabSetting(41),pnd_O_Mdo_Fields_Pnd_O_Mdo_Grth.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Equity Index         ",new 
            TabSetting(41),pnd_O_Mdo_Fields_Pnd_O_Mdo_Equ.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"    Inflation Linked Bond",new TabSetting(41),pnd_O_Mdo_Fields_Pnd_O_Mdo_Ilb.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Total OMNI MDO Annuity Income(Units)",new TabSetting(40),pnd_O_Mdo_Fields_Pnd_O_Mdo_Units.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM());                                                                                      //Natural: WRITE ( 1 ) NOTITLE *PROGRAM
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(36),"LEGACY CONTRACT PRODUCTION CONTROL TOTALS - PAYOUT ANNUITES",new TabSetting(105),"PROCESSING DATE:",Global.getDATU(),NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 36T 'LEGACY CONTRACT PRODUCTION CONTROL TOTALS - PAYOUT ANNUITES' 105T 'PROCESSING DATE:' *DATU // 38T '  MONTHLY       QUARTERLY       SEMI-ANNUAL       ANNUAL' 105T 'ALL MODES' / 38T '$ or Units      $ or Units      $ or Units      $ or Units' 105T '$ or Units' / 38T '----------      ----------      ----------      ----------' 105T '----------'
                        TabSetting(38),"  MONTHLY       QUARTERLY       SEMI-ANNUAL       ANNUAL",new TabSetting(105),"ALL MODES",NEWLINE,new TabSetting(38),"$ or Units      $ or Units      $ or Units      $ or Units",new 
                        TabSetting(105),"$ or Units",NEWLINE,new TabSetting(38),"----------      ----------      ----------      ----------",new TabSetting(105),
                        "----------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM());                                                                                      //Natural: WRITE ( 2 ) NOTITLE *PROGRAM
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(36),"OMNI CONTRACT PRODUCTION CONTROL TOTALS - PAYOUT ANNUITES",new TabSetting(105),"PROCESSING DATE:",Global.getDATU(),NEWLINE,NEWLINE,new  //Natural: WRITE ( 2 ) 36T 'OMNI CONTRACT PRODUCTION CONTROL TOTALS - PAYOUT ANNUITES' 105T 'PROCESSING DATE:' *DATU // 38T '  MONTHLY       QUARTERLY       SEMI-ANNUAL       ANNUAL' 105T 'ALL MODES' / 38T '$ or Units      $ or Units      $ or Units      $ or Units' 105T '$ or Units' / 38T '----------      ----------      ----------      ----------' 105T '----------'
                        TabSetting(38),"  MONTHLY       QUARTERLY       SEMI-ANNUAL       ANNUAL",new TabSetting(105),"ALL MODES",NEWLINE,new TabSetting(38),"$ or Units      $ or Units      $ or Units      $ or Units",new 
                        TabSetting(105),"$ or Units",NEWLINE,new TabSetting(38),"----------      ----------      ----------      ----------",new TabSetting(105),
                        "----------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 3 ) ' '
                    getReports().write(3, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(70),"PROCESSING DATE:",Global.getDATU(),NEWLINE,NEWLINE);               //Natural: WRITE ( 3 ) NOTITLE *PROGRAM 70T 'PROCESSING DATE:' *DATU //
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(20),"LEGACY CONTRACT PRODUCTION CONTROL TOTALS - MDO",pnd_Aprl_Y_N.getValue(pnd_A),NEWLINE,NEWLINE,new  //Natural: WRITE ( 3 ) 20T 'LEGACY CONTRACT PRODUCTION CONTROL TOTALS - MDO' #APRL-Y-N ( #A ) // 85T 'Contract &' / 85T 'Certificate' / 45T '  Dollars             Units               Counts' / 45T '----------         ------------         ----------' / 1T 'Considerations Applied To' / 1T '-------------------------' /
                        TabSetting(85),"Contract &",NEWLINE,new TabSetting(85),"Certificate",NEWLINE,new TabSetting(45),"  Dollars             Units               Counts",NEWLINE,new 
                        TabSetting(45),"----------         ------------         ----------",NEWLINE,new TabSetting(1),"Considerations Applied To",NEWLINE,new 
                        TabSetting(1),"-------------------------",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 4 ) ' '
                    getReports().write(4, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(70),"PROCESSING DATE:",Global.getDATU(),NEWLINE,NEWLINE);               //Natural: WRITE ( 4 ) NOTITLE *PROGRAM 70T 'PROCESSING DATE:' *DATU //
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(20),"OMNI CONTRACT PRODUCTION CONTROL TOTALS - MDO",pnd_O_Aprl_Y_N.getValue(pnd_A),NEWLINE,NEWLINE,new  //Natural: WRITE ( 4 ) 20T 'OMNI CONTRACT PRODUCTION CONTROL TOTALS - MDO' #O-APRL-Y-N ( #A ) // 85T 'Contract &' / 85T 'Certificate' / 45T '  Dollars             Units               Counts' / 45T '----------         ------------         ----------' / 1T 'Considerations Applied To' / 1T '-------------------------' /
                        TabSetting(85),"Contract &",NEWLINE,new TabSetting(85),"Certificate",NEWLINE,new TabSetting(45),"  Dollars             Units               Counts",NEWLINE,new 
                        TabSetting(45),"----------         ------------         ----------",NEWLINE,new TabSetting(1),"Considerations Applied To",NEWLINE,new 
                        TabSetting(1),"-------------------------",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(2, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(3, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(4, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(5, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(6, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(0, "LS=80 PS=60 ZP=ON IS=OFF ES=OFF");

        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(30)," LEGACY DETAIL LISTING OF ALL CONTRACTS AND AMOUNTS ",NEWLINE,NEWLINE,new 
            TabSetting(1),"OPTION",new TabSetting(12),"TIAA NUMBER",new TabSetting(25),"ID",new TabSetting(29),"MONTHLY UNITS",new TabSetting(44),"ANNUAL UNITS",new 
            TabSetting(59),"TIAA AMOUNT",new TabSetting(74),"CREF AMOUNT",new TabSetting(89),"REA AMOUNT",new TabSetting(104),"ACCESS AMT");
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(30)," OMNI DETAIL LISTING OF ALL CONTRACTS AND AMOUNTS ",NEWLINE,NEWLINE,new 
            TabSetting(1),"OPTION",new TabSetting(12),"TIAA NUMBER",new TabSetting(25),"ID",new TabSetting(29),"MONTHLY UNITS",new TabSetting(44),"ANNUAL UNITS",new 
            TabSetting(59),"TIAA AMOUNT",new TabSetting(74),"CREF AMOUNT",new TabSetting(89),"REA AMOUNT",new TabSetting(104),"ACCESS AMT");

        getReports().setDisplayColumns(1, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Standard",
        		new ColumnSpacing(1),pnd_Grnted_Std_Amt, new InsertionCharacter("$"),pnd_Grnted_Std_Amt, new InsertionCharacter("$"),pnd_Grnted_Std_Amt, new 
            InsertionCharacter("$"),pnd_Grnted_Std_Amt, new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Grnted_Std_Amt_Tot, new InsertionCharacter("$"));
        getReports().setDisplayColumns(2, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Standard",
        		new ColumnSpacing(1),pnd_O_Grnted_Std_Amt, new InsertionCharacter("$"),pnd_O_Grnted_Std_Amt, new InsertionCharacter("$"),pnd_O_Grnted_Std_Amt, 
            new InsertionCharacter("$"),pnd_O_Grnted_Std_Amt, new InsertionCharacter("$"),new ColumnSpacing(4),pnd_O_Grnted_Std_Amt_Tot, new InsertionCharacter("$"));
        getReports().setDisplayColumns(3, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(1),"TIAA MDO",
        		new ColumnSpacing(30),pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt, new InsertionCharacter("$"),new ColumnSpacing(21),pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl, 
            new InsertionCharacter("$"),NEWLINE);
        getReports().setDisplayColumns(4, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(1),"TIAA MDO",
        		new ColumnSpacing(30),pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Int_Pymnt, new InsertionCharacter("$"),new ColumnSpacing(21),pnd_O_Mdo_Fields_Pnd_O_Mdo_Tiaa_Excl, 
            new InsertionCharacter("$"),NEWLINE);
    }
}
