/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:54 PM
**        * FROM NATURAL PROGRAM : Scib9002
************************************************************
**        * FILE NAME            : Scib9002.java
**        * CLASS NAME           : Scib9002
**        * INSTANCE NAME        : Scib9002
************************************************************
************************************************************************
* PROGRAM  : SCIB9002
* SYSTEM   : INTERFACE MODULE
* AUTHOR   : NICK CVETKOVIC FEBRUARY 2008
*
* FUNCTION : THIS PROGRAM CREATES T813 CARDS WHICH THEN GETS PICKED UP
*            BY POMA172D TO UPDATE SUNGARD FOR FIELD PH301
*
*            CLONED FROM SCIB9001
* MOD DATE   MOD BY       DESCRIPTION OF CHANGES
* 09/01/2015 ELLO         INCLUDE CONTRACTS FOR BENE IN THE PLAN WHEN
*                         CREATING THE T813 TO UPDATE OMNI WITH THE PIN
*                         (BIP)
* 05/17/2017 B. NEWSOM    PIN EXPANSION PROJECT                 (PINEXP)
*
* 05/23/17 B.ELLO      PIN EXPANSION CHANGES
*                      RESTOWED FOR UPDATED PDAS AND LDAS
*                      CHANGED TO POINT TO NEW VIEW OF CIS-PARTICIPANT-
*                      FILE    (C420007)                          (PINE)
*
************************************************************************

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Scib9002 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pls_Error_Occurence;
    private DbsField pls_Dar_Program;
    private DbsField ltfp0740;

    private DbsGroup ltfp0740__R_Field_1;
    private DbsField ltfp0740_Pnd_P_Ltfp0740;

    private DbsGroup ltfp0740__R_Field_2;
    private DbsField ltfp0740_Pnd_P_Sip_Letter_Type;
    private DbsField ltfp0740_Pnd_P_Filler_1;

    private DbsGroup ltfp0740_Pnd_P_Omni_Rec_Key;
    private DbsField ltfp0740_Pnd_P_Omni_Plan_Id;
    private DbsField ltfp0740_Pnd_P_Omni_Sub_Plan_Id;

    private DbsGroup ltfp0740__R_Field_3;
    private DbsField ltfp0740_Pnd_P_Omni_Ssn;
    private DbsField ltfp0740_Pnd_P_Omni_Sub_Plan;
    private DbsField ltfp0740_Pnd_P_Pin;
    private DbsField ltfp0740_Pnd_P_Sip_Tiaa_Contract;
    private DbsField ltfp0740_Pnd_P_Sip_Cref_Cert;
    private DbsField ltfp0740_Pnd_P_Omni_Plan_Name;
    private DbsField ltfp0740_Pnd_P_Participant_Name;
    private DbsField ltfp0740_Pnd_P_Frst_Annt_Ssn;
    private DbsField ltfp0740_Pnd_P_Frst_Annt_Frst_Nme;
    private DbsField ltfp0740_Pnd_P_Frst_Annt_Lst_Nme;
    private DbsField ltfp0740_Pnd_P_Frst_Annt_Dob;
    private DbsField ltfp0740_Pnd_P_Frst_Annt_Rsdnc_Cde;
    private DbsField ltfp0740_Pnd_P_Frst_Annt_Ctznshp;
    private DbsField ltfp0740_Pnd_P_Frst_Annt_Sex_Cde;
    private DbsField ltfp0740_Pnd_P_Frst_Annt_Calc_Meth;
    private DbsField ltfp0740_Pnd_P_Sip_Payment_Mode;
    private DbsField ltfp0740_Pnd_P_Annty_Strt_Dte;
    private DbsField ltfp0740_Pnd_P_Next_Pay_Date;
    private DbsField ltfp0740_Pnd_P_Pro_Rata;

    private DbsGroup ltfp0740_Pnd_P_Table_1;
    private DbsField ltfp0740_Pnd_P_Ticker_Symbol;
    private DbsField ltfp0740_Pnd_P_Allocation_Ind;
    private DbsField ltfp0740_Pnd_P_Allocation_Amt;
    private DbsField ltfp0740_Pnd_P_Dollar_Amount;
    private DbsField ltfp0740_Pnd_P_Sip_Contract_Type;
    private DbsField ltfp0740_Pnd_P_Settled_Tiaa_Contract_Nbr;
    private DbsField ltfp0740_Pnd_P_Settled_Cref_Contract_Nbr;
    private DbsField ltfp0740_Pnd_P_Settlement_Ind;
    private DbsField ltfp0740_Pnd_P_Tiaa_Accum_Settle_Amt;
    private DbsField ltfp0740_Pnd_P_Tiaa_Accum_Settle_Typ;
    private DbsField ltfp0740_Pnd_P_Rea_Accum_Settle_Amt;
    private DbsField ltfp0740_Pnd_P_Rea_Accum_Settle_Typ;
    private DbsField ltfp0740_Pnd_P_Cref_Accum_Settle_Amt;
    private DbsField ltfp0740_Pnd_P_Cref_Accum_Settle_Typ;
    private DbsField ltfp0740_Pnd_P_Cref_Annty_Payment;
    private DbsField ltfp0740_Pnd_P_Cref_Mnthly_Nbr_Units;
    private DbsField ltfp0740_Pnd_P_Rea_Mnthly_Nbr_Units;
    private DbsField ltfp0740_Pnd_P_Nra_Flag;
    private DbsField ltfp0740_Pnd_P_Fed_Election_Option;
    private DbsField ltfp0740_Pnd_P_Fed_Tax_Amt_Tiaa_Rea;
    private DbsField ltfp0740_Pnd_P_Fed_Tax_Amt_Ind_Tiaa_Rea;
    private DbsField ltfp0740_Pnd_P_Fed_Tax_Amt_Cref;
    private DbsField ltfp0740_Pnd_P_Fed_Tax_Amt_Ind_Cref;
    private DbsField ltfp0740_Pnd_P_Sta_Election_Option;
    private DbsField ltfp0740_Pnd_P_Sta_Tax_Amt_Tiaa_Rea;
    private DbsField ltfp0740_Pnd_P_Sta_Tax_Amt_Ind_Tiaa_Rea;
    private DbsField ltfp0740_Pnd_P_Sta_Tax_Amt_Cref;
    private DbsField ltfp0740_Pnd_P_Sta_Tax_Amt_Ind_Cref;
    private DbsField ltfp0740_Pnd_P_Rqst_Id_Key;
    private DbsField ltfp0740_Pnd_P_Opn_Clsd_Ind;
    private DbsField ltfp0740_Pnd_P_Status_Cd;
    private DbsField ltfp0740_Pnd_P_Cntrct_Type;
    private DbsField ltfp0740_Pnd_P_Cntrct_Apprvl_Ind;
    private DbsField ltfp0740_Pnd_P_Rqst_Id;
    private DbsField ltfp0740_Pnd_P_Cntrct_Print_Dte;
    private DbsField ltfp0740_Pnd_P_Extract_Date;
    private DbsField ltfp0740_Pnd_P_Appl_Rcvd_Dte;
    private DbsField ltfp0740_Pnd_P_Appl_Rcvd_User_Id;
    private DbsField ltfp0740_Pnd_P_Appl_Entry_User_Id;
    private DbsField ltfp0740_Pnd_P_Annty_Option;
    private DbsField ltfp0740_Pnd_P_Grnted_Int_Rate;
    private DbsField ltfp0740_Pnd_P_Sip_Traditional_Amt;
    private DbsField ltfp0740_Pnd_P_Sip_Tiaa_Int_Pymnt_Amt;
    private DbsField ltfp0740_Pnd_P_Sip_Cref_Int_Pymnt_Amt;
    private DbsField ltfp0740_Pnd_P_Pi_Task_Id_Old;

    private DbsGroup ltfp0740_Pnd_P_Ppymt_Dest_Name;
    private DbsField ltfp0740_Pnd_P_Ppymt_Dest_Name_35;
    private DbsField ltfp0740_Pnd_P_Ppymt_Dest_Name_5;
    private DbsField ltfp0740_Pnd_P_Ppymt_Dest_Addr1;
    private DbsField ltfp0740_Pnd_P_Ppymt_Dest_Addr2;
    private DbsField ltfp0740_Pnd_P_Ppymt_Dest_Addr3;
    private DbsField ltfp0740_Pnd_P_Ppymt_Dest_City;
    private DbsField ltfp0740_Pnd_P_Ppymt_Dest_State;
    private DbsField ltfp0740_Pnd_P_Ppymt_Zip;
    private DbsField ltfp0740_Pnd_P_Spymt_Dest_Name;
    private DbsField ltfp0740_Pnd_P_Spymt_Dest_Addr1;
    private DbsField ltfp0740_Pnd_P_Spymt_Dest_Addr2;
    private DbsField ltfp0740_Pnd_P_Spymt_Dest_Addr3;
    private DbsField ltfp0740_Pnd_P_Spymt_Dest_City;
    private DbsField ltfp0740_Pnd_P_Spymt_Dest_State;
    private DbsField ltfp0740_Pnd_P_Spymt_Zip;
    private DbsField ltfp0740_Pnd_P_Spymt_Dest_Acct_Nbr;
    private DbsField ltfp0740_Pnd_P_Spymt_Dest_Trnst_Cde;
    private DbsField ltfp0740_Pnd_P_Spymt_Dest_Acct_Typ;
    private DbsField ltfp0740_Pnd_P_Cpymt_Dest_Name;
    private DbsField ltfp0740_Pnd_P_Cpymt_Dest_Addr1;
    private DbsField ltfp0740_Pnd_P_Cpymt_Dest_Addr2;
    private DbsField ltfp0740_Pnd_P_Cpymt_Dest_Addr3;
    private DbsField ltfp0740_Pnd_P_Cpymt_Dest_City;
    private DbsField ltfp0740_Pnd_P_Cpymt_Dest_State;
    private DbsField ltfp0740_Pnd_P_Cpymt_Zip;
    private DbsField ltfp0740_Pnd_P_Cpymt_Dest_Acct_Nbr;
    private DbsField ltfp0740_Pnd_P_Cpymt_Dest_Trnst_Cde;
    private DbsField ltfp0740_Pnd_P_Cpymt_Dest_Acct_Typ;
    private DbsField ltfp0740_Pnd_P_Cpymt_Payee_Amt;
    private DbsField ltfp0740_Pnd_P_Cpymt_Payee_Type;
    private DbsField ltfp0740_Pnd_P_Two_Payments_In_One_Year;
    private DbsField ltfp0740_Pnd_P_Fed_Exemptions;
    private DbsField ltfp0740_Pnd_P_State_Exemptions;
    private DbsField ltfp0740_Pnd_P_Mailing_Country_Code;
    private DbsField ltfp0740_Pnd_P_Cod_Id;
    private DbsField ltfp0740_Pnd_P_Minimum_Req_Amt;
    private DbsField ltfp0740_Pnd_P_Payee_Cde;
    private DbsField ltfp0740_Pnd_P_Has_Roth_Money_Ind;
    private DbsField ltfp0740_Pnd_P_Issue_State;
    private DbsField ltfp0740_Pnd_P_Fed_Marital_Status;
    private DbsField ltfp0740_Pnd_P_St_Marital_Status;
    private DbsField ltfp0740_Pnd_P_Date_Of_Death_A;

    private DbsGroup ltfp0740__R_Field_4;
    private DbsField ltfp0740_Pnd_P_Date_Of_Death;
    private DbsField ltfp0740_Pnd_P_Date_Of_Birth_A;

    private DbsGroup ltfp0740__R_Field_5;
    private DbsField ltfp0740_Pnd_P_Date_Of_Birth;

    private DbsGroup ltfp0740__R_Field_6;
    private DbsField ltfp0740_Pnd_B_Ltfp0740;

    private DbsGroup ltfp0740__R_Field_7;
    private DbsField ltfp0740_Pnd_B_Sip_Letter_Type;
    private DbsField ltfp0740_Pnd_B_Filler;

    private DbsGroup ltfp0740_Pnd_B_Omni_Rec_Key;
    private DbsField ltfp0740_Pnd_B_Omni_Plan_Id;
    private DbsField ltfp0740_Pnd_B_Omni_Sub_Plan_Id;

    private DbsGroup ltfp0740__R_Field_8;
    private DbsField ltfp0740_Pnd_B_Omni_Ssn;
    private DbsField ltfp0740_Pnd_B_Omni_Sub_Plan;
    private DbsField ltfp0740_Pnd_B_Pin;
    private DbsField ltfp0740_Pnd_B_Sip_Tiaa_Contract;
    private DbsField ltfp0740_Pnd_B_Sip_Cref_Cert;
    private DbsField ltfp0740_Pnd_B_Omni_Plan_Name;
    private DbsField ltfp0740_Pnd_B_Participant_Name;
    private DbsField ltfp0740_Pnd_B_Bene_Category;
    private DbsField ltfp0740_Pnd_B_Bene_Name;
    private DbsField ltfp0740_Pnd_B_Bene_Birth_Date;
    private DbsField ltfp0740_Pnd_B_Bene_Ssn;
    private DbsField ltfp0740_Pnd_B_Bene_Relationship;
    private DbsField ltfp0740_Pnd_B_Bene_Allocation_Pct;
    private DbsField ltfp0740_Pnd_B_Prim_Child_Prov;
    private DbsField ltfp0740_Pnd_B_Cont_Child_Prov;
    private DbsField ltfp0740_Pnd_B_Cal_Bene_Meth;
    private DbsField ltfp0740_Pnd_B_Filler1;
    private DbsField ltfp0740_Pnd_B_Filler2;
    private DbsField ltfp0740_Pnd_B_Filler3;
    private DbsField ltfp0740_Pnd_B_Filler4;
    private DbsField ltfp0740_Pnd_B_Filler5;
    private DbsField ltfp0740_Pnd_B_Filler6;
    private DbsField ltfp0740_Pnd_B_Filler7;
    private DbsField ltfp0740_Pnd_B_Filler8;
    private DbsField ltfp0740_Pnd_B_Filler9;
    private DbsField ltfp0740_Pnd_B_Filler10;
    private DbsField ltfp0740_Pnd_B_Filler11;
    private DbsField ltfp0740_Pnd_B_Filler12;
    private DbsField ltfp0740_Pnd_B_Filler14;
    private DbsField pnd_Baseext_Rec;

    private DbsGroup pnd_Baseext_Rec__R_Field_9;
    private DbsField pnd_Baseext_Rec_Pnd_Baseext_Rectype;
    private DbsField pnd_Input_Part_Aa;

    private DbsGroup pnd_Input_Part_Aa__R_Field_10;
    private DbsField pnd_Input_Part_Aa_Pnd_Rec_Iden;
    private DbsField pnd_Input_Part_Aa_Pnd_Rec_Type;
    private DbsField pnd_Input_Part_Aa_Pnd_Plan_Name;
    private DbsField pnd_Input_Part_Aa_Pnd_Plan_Id;
    private DbsField pnd_Input_Part_Aa_Pnd_Erisa_Ind;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Tpa_Tiaa_Cntrct;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Tpa_Cref_Cntrct;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Pin_Nbr;

    private DbsGroup pnd_Input_Part_Aa__R_Field_11;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Pin_Nbr_N;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Rqst_Id_Key;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Tpa_Cntrct;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Requestor;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Cntrct_Type;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Opn_Clsd_Ind;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Status_Cd;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Annty_Option;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Cntrct_Print_Dte;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Frst_Nme;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Mid_Nme;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Lst_Nme;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Prf_Nme;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Sex_Cde;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn;

    private DbsGroup pnd_Input_Part_Aa__R_Field_12;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn_N;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Dob;
    private DbsField pnd_Input_Part_Aa_Pnd_Addrss_Line_1;
    private DbsField pnd_Input_Part_Aa_Pnd_Addrss_Line_2;
    private DbsField pnd_Input_Part_Aa_Pnd_Addrss_Line_2_3;
    private DbsField pnd_Input_Part_Aa_Pnd_Addrss_Line_3;
    private DbsField pnd_Input_Part_Aa_Pnd_City;
    private DbsField pnd_Input_Part_Aa_Pnd_Resid_Stae;
    private DbsField pnd_Input_Part_Aa_Pnd_Zip;
    private DbsField pnd_Input_Part_Aa_Pnd_Term_Date;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ctznshp_Cd;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Tiaa_Doi;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Annty_Start_Dte;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Annty_End_Dte;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Pymnt_Mode;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Grnted_Periods_Yrs;

    private DbsGroup pnd_Input_Part_Aa__R_Field_13;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Grnted_Periods_Yrs_N;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Grnted_Grd_Amt;

    private DbsGroup pnd_Input_Part_Aa__R_Field_14;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Grnted_Grd_Amt_N;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Lob;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Lob_Type;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Da_Tiaa_Nbr_Sttl;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Da_Proceeds_Amt;

    private DbsGroup pnd_Input_Part_Aa__R_Field_15;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Da_Proceeds_Amt_N;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Rtb_Amt;

    private DbsGroup pnd_Input_Part_Aa__R_Field_16;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Rtb_Amt_N;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Rtb_Pct;

    private DbsGroup pnd_Input_Part_Aa__R_Field_17;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Rtb_Pct_N;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Trnsf_Tiaa_Rea_Nbr;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Trnsf_Tiaa_Cer_Nbr;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Handle_Code;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Org_Issue_St;
    private DbsField pnd_Input_Part_Aa_Pnd_Fill;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Amt;

    private DbsGroup pnd_Input_Part_Aa__R_Field_18;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Amt_N;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Pct;

    private DbsGroup pnd_Input_Part_Aa__R_Field_19;
    private DbsField pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Pct_N;
    private DbsField pnd_Input_Part_Aa_Pnd_Dest_Name;
    private DbsField pnd_Input_Part_Aa_Pnd_Dest_Addr_Line;
    private DbsField pnd_Input_Part_Aa_Pnd_Dest_Addr_Line_2;
    private DbsField pnd_Input_Part_Aa_Pnd_Dest_Addr_Line_3;
    private DbsField pnd_Input_Part_Aa_Pnd_Dest_Addr_City;
    private DbsField pnd_Input_Part_Aa_Pnd_Dest_Addr_St;
    private DbsField pnd_Input_Part_Aa_Pnd_Dest_Addr_Zip;
    private DbsField pnd_Input_Part_Aa_Pnd_Dest_Bank_Type;
    private DbsField pnd_Input_Part_Aa_Pnd_Dest_Bank_Acct_Nbr;
    private DbsField pnd_Input_Part_Aa_Pnd_Dest_Bank_Trst_Nbr;
    private DbsField pnd_Input_Part_Aa_Pnd_Rollover_Dest_Name;
    private DbsField pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Line;
    private DbsField pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Line_2;
    private DbsField pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Line_3;
    private DbsField pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_City;
    private DbsField pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_St;
    private DbsField pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Zip;
    private DbsField pnd_Input_Part_Aa_Pnd_Rollover_Dest_Bank_Acct_Nbr;
    private DbsField pnd_Input_Part_Aa_Pnd_Power_Image_Id;
    private DbsField pnd_Input_Part_Aa_Pnd_Fed_Ovr_Or_Add;
    private DbsField pnd_Input_Part_Aa_Pnd_Fed_Dol_Or_Pct;
    private DbsField pnd_Input_Part_Aa_Pnd_Fed_Pct_Amt_Or_Dol;

    private DbsGroup pnd_Input_Part_Aa__R_Field_20;
    private DbsField pnd_Input_Part_Aa_Pnd_Fed_Pct_Amt_Or_Dol_N;
    private DbsField pnd_Input_Part_Aa_Pnd_Fed_No_Of_Exempt;

    private DbsGroup pnd_Input_Part_Aa__R_Field_21;
    private DbsField pnd_Input_Part_Aa_Pnd_Fed_No_Of_Exempt_N;
    private DbsField pnd_Input_Part_Aa_Pnd_St_Ovr_Or_Add;
    private DbsField pnd_Input_Part_Aa_Pnd_St_Dol_Or_Pct;
    private DbsField pnd_Input_Part_Aa_Pnd_St_Pct_Amt_Or_Dol;

    private DbsGroup pnd_Input_Part_Aa__R_Field_22;
    private DbsField pnd_Input_Part_Aa_Pnd_St_Pct_Amt_Or_Dol_N;
    private DbsField pnd_Input_Part_Aa_Pnd_St_No_Of_Exempt;

    private DbsGroup pnd_Input_Part_Aa__R_Field_23;
    private DbsField pnd_Input_Part_Aa_Pnd_St_No_Of_Exempt_N;
    private DbsField pnd_Input_Part_Aa_Pnd_Fed_Opt;
    private DbsField pnd_Input_Part_Aa_Pnd_St_Opt;
    private DbsField pnd_Input_Part_Aa_Pnd_Fed_Marital_Sts;
    private DbsField pnd_Input_Part_Aa_Pnd_St_Marital_Sts;
    private DbsField pnd_Input_Part_Aa_Pnd_Dollar_Or_Pcnt;
    private DbsField pnd_Input_Part_Aa_Pnd_Dollar_Or_Pcnt_Amt;
    private DbsField pnd_Input_Part_Aa_Pnd_Sub_Plan;
    private DbsField pnd_Input_Part_Aa_Pnd_Foreign_Or_Domestic;
    private DbsField pnd_Input_Part_Aa_Pnd_Country_For_Dometic;
    private DbsField pnd_Input_Part_Aa_Pnd_Payee_Cde;
    private DbsField pnd_Input_Part_Aa_Pnd_Has_Roth_Money_Ind;
    private DbsField pnd_Input_Part_Aa_Pnd_Fund_Allocation;

    private DbsGroup pnd_Input_Part_Aa__R_Field_24;
    private DbsField pnd_Input_Part_Aa_Pnd_Fund_Ticker;
    private DbsField pnd_Input_Part_Aa_Pnd_Fund_Alloc;
    private DbsField pnd_Input_Part_Aa_Pnd_Acceptance_Ind;
    private DbsField pnd_Input_Part_Aa_Pnd_Relation_To_Decedent;
    private DbsField pnd_Input_Part_Aa_Pnd_Ssn_Tin_Ind;
    private DbsField pnd_Input_Part_Aa_Pnd_Decedent_Contract;

    private DataAccessProgramView vw_cis_Prtcpnt_File;
    private DbsField cis_Prtcpnt_File_Cis_Pin_Nbr;
    private DbsField cis_Prtcpnt_File_Cis_Tiaa_Nbr;
    private DbsField pnd_Tiaa_Cntrct_Found;

    private DataAccessProgramView vw_batch_Restart;
    private DbsField batch_Restart_Rst_Actve_Ind;
    private DbsField batch_Restart_Rst_Job_Nme;
    private DbsField batch_Restart_Rst_Jobstep_Nme;
    private DbsField batch_Restart_Rst_Pgm_Nme;
    private DbsField batch_Restart_Rst_Curr_Dte;
    private DbsField batch_Restart_Rst_Curr_Tme;
    private DbsField batch_Restart_Rst_Start_Dte;
    private DbsField batch_Restart_Rst_Start_Tme;
    private DbsField batch_Restart_Rst_End_Dte;
    private DbsField batch_Restart_Rst_End_Tme;
    private DbsField batch_Restart_Rst_Key;
    private DbsField batch_Restart_Rst_Cnt;
    private DbsField batch_Restart_Rst_Isn_Nbr;
    private DbsField batch_Restart_Rst_Rstrt_Data_Ind;

    private DbsGroup batch_Restart_Rst_Data_Grp;
    private DbsField batch_Restart_Rst_Data_Txt;
    private DbsField batch_Restart_Rst_Frst_Run_Dte;
    private DbsField batch_Restart_Rst_Last_Rstrt_Dte;
    private DbsField batch_Restart_Rst_Last_Rstrt_Tme;
    private DbsField batch_Restart_Rst_Updt_Dte;
    private DbsField batch_Restart_Rst_Updt_Tme;
    private DbsField pnd_Restart_Key;

    private DbsGroup pnd_Restart_Key__R_Field_25;

    private DbsGroup pnd_Restart_Key_Pnd_Restart_Structure;
    private DbsField pnd_Restart_Key_Pnd_Restart_Plan_No;
    private DbsField pnd_Restart_Key_Pnd_Restart_Part_Id;

    private DbsGroup pnd_Restart_Key__R_Field_26;
    private DbsField pnd_Restart_Key_Pnd_Restart_Ssn;
    private DbsField pnd_Restart_Key_Pnd_Restart_Subplan_No;
    private DbsField pnd_Sp_Actve_Job_Step;

    private DbsGroup pnd_Sp_Actve_Job_Step__R_Field_27;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme;
    private DbsField pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme;
    private DbsField pnd_Restart_Isn;
    private DbsField pnd_Omni_Header;

    private DbsGroup pnd_Omni_Header__R_Field_28;
    private DbsField pnd_Omni_Header_Pnd_Tran_Code;
    private DbsField pnd_Omni_Header_Pnd_Seq_Code;
    private DbsField pnd_Omni_Header_Pnd_Notused1;
    private DbsField pnd_Omni_Header_Pnd_Rec_Type;
    private DbsField pnd_Omni_Header_Pnd_P_Omni_Plan_Id;
    private DbsField pnd_Omni_Header_Pnd_Part_Id;
    private DbsField pnd_Omni_Header_Pnd_Notused2;
    private DbsField pnd_Omni_Header_Pnd_Trade_Date;

    private DbsGroup pnd_Omni_Header__R_Field_29;
    private DbsField pnd_Omni_Header_Pnd_Trade_Date_A;
    private DbsField pnd_Omni_Detail;

    private DbsGroup pnd_Omni_Detail__R_Field_30;
    private DbsField pnd_Omni_Detail_Pnd_Tran_Code;
    private DbsField pnd_Omni_Detail_Pnd_Seq_Code;
    private DbsField pnd_Omni_Detail_Pnd_Notused1;
    private DbsField pnd_Omni_Detail_Pnd_Data_Elem;
    private DbsField pnd_Omni_Detail_Pnd_De_Value;
    private DbsField pnd_Compare_Key;

    private DbsGroup pnd_Compare_Key__R_Field_31;
    private DbsField pnd_Compare_Key_Pnd_Comp_Plan_No;
    private DbsField pnd_Compare_Key_Pnd_Comp_Part_Id;

    private DbsGroup pnd_Compare_Key__R_Field_32;
    private DbsField pnd_Compare_Key_Pnd_Comp_Ssn;
    private DbsField pnd_Compare_Key_Pnd_Comp_Subplan_No;
    private DbsField pnd_Compare_Key_Pnd_Comp_Part_Ext;

    private DbsGroup pnd_Accepted_Report_Rec;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Divsub;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Ssn;
    private DbsField pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_No;

    private DbsGroup pnd_Rejected_Report_Rec;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Plan_No;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Divsub;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Ssn;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Subplan_No;
    private DbsField pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg;

    private DbsGroup pnd_Summary_Report_Rec;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt;
    private DbsField pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt;
    private DbsField pnd_Restarted;
    private DbsField pnd_Skip_Restart_Record;
    private DbsField pnd_Hold_Part_Id;
    private DbsField pnd_Address_Standardized;
    private DbsField pnd_Record_Cnt;
    private DbsField pnd_Error_Rec_Cnt;
    private DbsField pnd_Max_Cards;
    private DbsField pnd_Card_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_Retry_Cnt;
    private DbsField intvl;
    private DbsField reqid;
    private DbsField pnd_Jobname;
    private DbsField pnd_Tiaa_Contract;
    private DbsField pnd_Tpa_Part_Id;

    private DbsGroup pnd_Tpa_Part_Id__R_Field_33;
    private DbsField pnd_Tpa_Part_Id_Pnd_Tpa_Part_Ssn;
    private DbsField pnd_Tpa_Part_Id_Pnd_Tpa_Part_Subplan;
    private int cmrollReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pls_Error_Occurence = WsIndependent.getInstance().newFieldInRecord("pls_Error_Occurence", "+ERROR-OCCURENCE", FieldType.BOOLEAN, 1);
        pls_Dar_Program = WsIndependent.getInstance().newFieldInRecord("pls_Dar_Program", "+DAR-PROGRAM", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        // Local Variables
        localVariables = new DbsRecord();
        ltfp0740 = localVariables.newFieldArrayInRecord("ltfp0740", "LTFP0740", FieldType.STRING, 1, new DbsArrayController(1, 3242));

        ltfp0740__R_Field_1 = localVariables.newGroupInRecord("ltfp0740__R_Field_1", "REDEFINE", ltfp0740);
        ltfp0740_Pnd_P_Ltfp0740 = ltfp0740__R_Field_1.newFieldArrayInGroup("ltfp0740_Pnd_P_Ltfp0740", "#P-LTFP0740", FieldType.STRING, 1, new DbsArrayController(1, 
            3242));

        ltfp0740__R_Field_2 = ltfp0740__R_Field_1.newGroupInGroup("ltfp0740__R_Field_2", "REDEFINE", ltfp0740_Pnd_P_Ltfp0740);
        ltfp0740_Pnd_P_Sip_Letter_Type = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sip_Letter_Type", "#P-SIP-LETTER-TYPE", FieldType.STRING, 
            4);
        ltfp0740_Pnd_P_Filler_1 = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Filler_1", "#P-FILLER-1", FieldType.STRING, 1);

        ltfp0740_Pnd_P_Omni_Rec_Key = ltfp0740__R_Field_2.newGroupInGroup("ltfp0740_Pnd_P_Omni_Rec_Key", "#P-OMNI-REC-KEY");
        ltfp0740_Pnd_P_Omni_Plan_Id = ltfp0740_Pnd_P_Omni_Rec_Key.newFieldInGroup("ltfp0740_Pnd_P_Omni_Plan_Id", "#P-OMNI-PLAN-ID", FieldType.STRING, 
            6);
        ltfp0740_Pnd_P_Omni_Sub_Plan_Id = ltfp0740_Pnd_P_Omni_Rec_Key.newFieldInGroup("ltfp0740_Pnd_P_Omni_Sub_Plan_Id", "#P-OMNI-SUB-PLAN-ID", FieldType.STRING, 
            17);

        ltfp0740__R_Field_3 = ltfp0740_Pnd_P_Omni_Rec_Key.newGroupInGroup("ltfp0740__R_Field_3", "REDEFINE", ltfp0740_Pnd_P_Omni_Sub_Plan_Id);
        ltfp0740_Pnd_P_Omni_Ssn = ltfp0740__R_Field_3.newFieldInGroup("ltfp0740_Pnd_P_Omni_Ssn", "#P-OMNI-SSN", FieldType.NUMERIC, 9);
        ltfp0740_Pnd_P_Omni_Sub_Plan = ltfp0740__R_Field_3.newFieldInGroup("ltfp0740_Pnd_P_Omni_Sub_Plan", "#P-OMNI-SUB-PLAN", FieldType.STRING, 8);
        ltfp0740_Pnd_P_Pin = ltfp0740_Pnd_P_Omni_Rec_Key.newFieldInGroup("ltfp0740_Pnd_P_Pin", "#P-PIN", FieldType.STRING, 13);
        ltfp0740_Pnd_P_Sip_Tiaa_Contract = ltfp0740_Pnd_P_Omni_Rec_Key.newFieldInGroup("ltfp0740_Pnd_P_Sip_Tiaa_Contract", "#P-SIP-TIAA-CONTRACT", FieldType.STRING, 
            10);
        ltfp0740_Pnd_P_Sip_Cref_Cert = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sip_Cref_Cert", "#P-SIP-CREF-CERT", FieldType.STRING, 10);
        ltfp0740_Pnd_P_Omni_Plan_Name = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Omni_Plan_Name", "#P-OMNI-PLAN-NAME", FieldType.STRING, 50);
        ltfp0740_Pnd_P_Participant_Name = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Participant_Name", "#P-PARTICIPANT-NAME", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Frst_Annt_Ssn = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Frst_Annt_Ssn", "#P-FRST-ANNT-SSN", FieldType.NUMERIC, 9);
        ltfp0740_Pnd_P_Frst_Annt_Frst_Nme = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Frst_Annt_Frst_Nme", "#P-FRST-ANNT-FRST-NME", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Frst_Annt_Lst_Nme = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Frst_Annt_Lst_Nme", "#P-FRST-ANNT-LST-NME", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Frst_Annt_Dob = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Frst_Annt_Dob", "#P-FRST-ANNT-DOB", FieldType.NUMERIC, 8);
        ltfp0740_Pnd_P_Frst_Annt_Rsdnc_Cde = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Frst_Annt_Rsdnc_Cde", "#P-FRST-ANNT-RSDNC-CDE", FieldType.STRING, 
            2);
        ltfp0740_Pnd_P_Frst_Annt_Ctznshp = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Frst_Annt_Ctznshp", "#P-FRST-ANNT-CTZNSHP", FieldType.STRING, 
            2);
        ltfp0740_Pnd_P_Frst_Annt_Sex_Cde = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Frst_Annt_Sex_Cde", "#P-FRST-ANNT-SEX-CDE", FieldType.NUMERIC, 
            1);
        ltfp0740_Pnd_P_Frst_Annt_Calc_Meth = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Frst_Annt_Calc_Meth", "#P-FRST-ANNT-CALC-METH", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Sip_Payment_Mode = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sip_Payment_Mode", "#P-SIP-PAYMENT-MODE", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Annty_Strt_Dte = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Annty_Strt_Dte", "#P-ANNTY-STRT-DTE", FieldType.NUMERIC, 8);
        ltfp0740_Pnd_P_Next_Pay_Date = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Next_Pay_Date", "#P-NEXT-PAY-DATE", FieldType.NUMERIC, 8);
        ltfp0740_Pnd_P_Pro_Rata = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Pro_Rata", "#P-PRO-RATA", FieldType.STRING, 1);

        ltfp0740_Pnd_P_Table_1 = ltfp0740__R_Field_2.newGroupArrayInGroup("ltfp0740_Pnd_P_Table_1", "#P-TABLE-1", new DbsArrayController(1, 60));
        ltfp0740_Pnd_P_Ticker_Symbol = ltfp0740_Pnd_P_Table_1.newFieldInGroup("ltfp0740_Pnd_P_Ticker_Symbol", "#P-TICKER-SYMBOL", FieldType.STRING, 10);
        ltfp0740_Pnd_P_Allocation_Ind = ltfp0740_Pnd_P_Table_1.newFieldInGroup("ltfp0740_Pnd_P_Allocation_Ind", "#P-ALLOCATION-IND", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Allocation_Amt = ltfp0740_Pnd_P_Table_1.newFieldInGroup("ltfp0740_Pnd_P_Allocation_Amt", "#P-ALLOCATION-AMT", FieldType.NUMERIC, 
            11, 2);
        ltfp0740_Pnd_P_Dollar_Amount = ltfp0740_Pnd_P_Table_1.newFieldInGroup("ltfp0740_Pnd_P_Dollar_Amount", "#P-DOLLAR-AMOUNT", FieldType.NUMERIC, 11, 
            2);
        ltfp0740_Pnd_P_Sip_Contract_Type = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sip_Contract_Type", "#P-SIP-CONTRACT-TYPE", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Settled_Tiaa_Contract_Nbr = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Settled_Tiaa_Contract_Nbr", "#P-SETTLED-TIAA-CONTRACT-NBR", 
            FieldType.STRING, 10);
        ltfp0740_Pnd_P_Settled_Cref_Contract_Nbr = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Settled_Cref_Contract_Nbr", "#P-SETTLED-CREF-CONTRACT-NBR", 
            FieldType.STRING, 10);
        ltfp0740_Pnd_P_Settlement_Ind = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Settlement_Ind", "#P-SETTLEMENT-IND", FieldType.STRING, 1);
        ltfp0740_Pnd_P_Tiaa_Accum_Settle_Amt = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Tiaa_Accum_Settle_Amt", "#P-TIAA-ACCUM-SETTLE-AMT", 
            FieldType.NUMERIC, 11, 2);
        ltfp0740_Pnd_P_Tiaa_Accum_Settle_Typ = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Tiaa_Accum_Settle_Typ", "#P-TIAA-ACCUM-SETTLE-TYP", 
            FieldType.STRING, 1);
        ltfp0740_Pnd_P_Rea_Accum_Settle_Amt = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Rea_Accum_Settle_Amt", "#P-REA-ACCUM-SETTLE-AMT", FieldType.NUMERIC, 
            11, 2);
        ltfp0740_Pnd_P_Rea_Accum_Settle_Typ = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Rea_Accum_Settle_Typ", "#P-REA-ACCUM-SETTLE-TYP", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Cref_Accum_Settle_Amt = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cref_Accum_Settle_Amt", "#P-CREF-ACCUM-SETTLE-AMT", 
            FieldType.NUMERIC, 11, 2);
        ltfp0740_Pnd_P_Cref_Accum_Settle_Typ = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cref_Accum_Settle_Typ", "#P-CREF-ACCUM-SETTLE-TYP", 
            FieldType.STRING, 1);
        ltfp0740_Pnd_P_Cref_Annty_Payment = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cref_Annty_Payment", "#P-CREF-ANNTY-PAYMENT", FieldType.NUMERIC, 
            11, 2);
        ltfp0740_Pnd_P_Cref_Mnthly_Nbr_Units = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cref_Mnthly_Nbr_Units", "#P-CREF-MNTHLY-NBR-UNITS", 
            FieldType.NUMERIC, 7, 4);
        ltfp0740_Pnd_P_Rea_Mnthly_Nbr_Units = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Rea_Mnthly_Nbr_Units", "#P-REA-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 
            7, 4);
        ltfp0740_Pnd_P_Nra_Flag = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Nra_Flag", "#P-NRA-FLAG", FieldType.STRING, 1);
        ltfp0740_Pnd_P_Fed_Election_Option = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Fed_Election_Option", "#P-FED-ELECTION-OPTION", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Fed_Tax_Amt_Tiaa_Rea = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Fed_Tax_Amt_Tiaa_Rea", "#P-FED-TAX-AMT-TIAA-REA", FieldType.NUMERIC, 
            11, 2);
        ltfp0740_Pnd_P_Fed_Tax_Amt_Ind_Tiaa_Rea = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Fed_Tax_Amt_Ind_Tiaa_Rea", "#P-FED-TAX-AMT-IND-TIAA-REA", 
            FieldType.STRING, 1);
        ltfp0740_Pnd_P_Fed_Tax_Amt_Cref = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Fed_Tax_Amt_Cref", "#P-FED-TAX-AMT-CREF", FieldType.NUMERIC, 
            11, 2);
        ltfp0740_Pnd_P_Fed_Tax_Amt_Ind_Cref = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Fed_Tax_Amt_Ind_Cref", "#P-FED-TAX-AMT-IND-CREF", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Sta_Election_Option = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sta_Election_Option", "#P-STA-ELECTION-OPTION", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Sta_Tax_Amt_Tiaa_Rea = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sta_Tax_Amt_Tiaa_Rea", "#P-STA-TAX-AMT-TIAA-REA", FieldType.NUMERIC, 
            11, 2);
        ltfp0740_Pnd_P_Sta_Tax_Amt_Ind_Tiaa_Rea = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sta_Tax_Amt_Ind_Tiaa_Rea", "#P-STA-TAX-AMT-IND-TIAA-REA", 
            FieldType.STRING, 1);
        ltfp0740_Pnd_P_Sta_Tax_Amt_Cref = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sta_Tax_Amt_Cref", "#P-STA-TAX-AMT-CREF", FieldType.NUMERIC, 
            11, 2);
        ltfp0740_Pnd_P_Sta_Tax_Amt_Ind_Cref = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sta_Tax_Amt_Ind_Cref", "#P-STA-TAX-AMT-IND-CREF", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Rqst_Id_Key = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Rqst_Id_Key", "#P-RQST-ID-KEY", FieldType.STRING, 24);
        ltfp0740_Pnd_P_Opn_Clsd_Ind = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Opn_Clsd_Ind", "#P-OPN-CLSD-IND", FieldType.STRING, 1);
        ltfp0740_Pnd_P_Status_Cd = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Status_Cd", "#P-STATUS-CD", FieldType.STRING, 1);
        ltfp0740_Pnd_P_Cntrct_Type = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cntrct_Type", "#P-CNTRCT-TYPE", FieldType.STRING, 1);
        ltfp0740_Pnd_P_Cntrct_Apprvl_Ind = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cntrct_Apprvl_Ind", "#P-CNTRCT-APPRVL-IND", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Rqst_Id = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Rqst_Id", "#P-RQST-ID", FieldType.STRING, 3);
        ltfp0740_Pnd_P_Cntrct_Print_Dte = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cntrct_Print_Dte", "#P-CNTRCT-PRINT-DTE", FieldType.NUMERIC, 
            8);
        ltfp0740_Pnd_P_Extract_Date = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Extract_Date", "#P-EXTRACT-DATE", FieldType.NUMERIC, 8);
        ltfp0740_Pnd_P_Appl_Rcvd_Dte = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Appl_Rcvd_Dte", "#P-APPL-RCVD-DTE", FieldType.NUMERIC, 8);
        ltfp0740_Pnd_P_Appl_Rcvd_User_Id = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Appl_Rcvd_User_Id", "#P-APPL-RCVD-USER-ID", FieldType.STRING, 
            8);
        ltfp0740_Pnd_P_Appl_Entry_User_Id = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Appl_Entry_User_Id", "#P-APPL-ENTRY-USER-ID", FieldType.STRING, 
            8);
        ltfp0740_Pnd_P_Annty_Option = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Annty_Option", "#P-ANNTY-OPTION", FieldType.STRING, 2);
        ltfp0740_Pnd_P_Grnted_Int_Rate = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Grnted_Int_Rate", "#P-GRNTED-INT-RATE", FieldType.NUMERIC, 
            7, 4);
        ltfp0740_Pnd_P_Sip_Traditional_Amt = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sip_Traditional_Amt", "#P-SIP-TRADITIONAL-AMT", FieldType.NUMERIC, 
            11, 2);
        ltfp0740_Pnd_P_Sip_Tiaa_Int_Pymnt_Amt = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sip_Tiaa_Int_Pymnt_Amt", "#P-SIP-TIAA-INT-PYMNT-AMT", 
            FieldType.NUMERIC, 11, 2);
        ltfp0740_Pnd_P_Sip_Cref_Int_Pymnt_Amt = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Sip_Cref_Int_Pymnt_Amt", "#P-SIP-CREF-INT-PYMNT-AMT", 
            FieldType.NUMERIC, 11, 2);
        ltfp0740_Pnd_P_Pi_Task_Id_Old = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Pi_Task_Id_Old", "#P-PI-TASK-ID-OLD", FieldType.STRING, 11);

        ltfp0740_Pnd_P_Ppymt_Dest_Name = ltfp0740__R_Field_2.newGroupInGroup("ltfp0740_Pnd_P_Ppymt_Dest_Name", "#P-PPYMT-DEST-NAME");
        ltfp0740_Pnd_P_Ppymt_Dest_Name_35 = ltfp0740_Pnd_P_Ppymt_Dest_Name.newFieldInGroup("ltfp0740_Pnd_P_Ppymt_Dest_Name_35", "#P-PPYMT-DEST-NAME-35", 
            FieldType.STRING, 35);
        ltfp0740_Pnd_P_Ppymt_Dest_Name_5 = ltfp0740_Pnd_P_Ppymt_Dest_Name.newFieldInGroup("ltfp0740_Pnd_P_Ppymt_Dest_Name_5", "#P-PPYMT-DEST-NAME-5", 
            FieldType.STRING, 5);
        ltfp0740_Pnd_P_Ppymt_Dest_Addr1 = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Ppymt_Dest_Addr1", "#P-PPYMT-DEST-ADDR1", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Ppymt_Dest_Addr2 = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Ppymt_Dest_Addr2", "#P-PPYMT-DEST-ADDR2", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Ppymt_Dest_Addr3 = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Ppymt_Dest_Addr3", "#P-PPYMT-DEST-ADDR3", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Ppymt_Dest_City = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Ppymt_Dest_City", "#P-PPYMT-DEST-CITY", FieldType.STRING, 
            28);
        ltfp0740_Pnd_P_Ppymt_Dest_State = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Ppymt_Dest_State", "#P-PPYMT-DEST-STATE", FieldType.STRING, 
            3);
        ltfp0740_Pnd_P_Ppymt_Zip = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Ppymt_Zip", "#P-PPYMT-ZIP", FieldType.STRING, 9);
        ltfp0740_Pnd_P_Spymt_Dest_Name = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Spymt_Dest_Name", "#P-SPYMT-DEST-NAME", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Spymt_Dest_Addr1 = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Spymt_Dest_Addr1", "#P-SPYMT-DEST-ADDR1", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Spymt_Dest_Addr2 = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Spymt_Dest_Addr2", "#P-SPYMT-DEST-ADDR2", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Spymt_Dest_Addr3 = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Spymt_Dest_Addr3", "#P-SPYMT-DEST-ADDR3", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Spymt_Dest_City = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Spymt_Dest_City", "#P-SPYMT-DEST-CITY", FieldType.STRING, 
            28);
        ltfp0740_Pnd_P_Spymt_Dest_State = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Spymt_Dest_State", "#P-SPYMT-DEST-STATE", FieldType.STRING, 
            3);
        ltfp0740_Pnd_P_Spymt_Zip = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Spymt_Zip", "#P-SPYMT-ZIP", FieldType.STRING, 9);
        ltfp0740_Pnd_P_Spymt_Dest_Acct_Nbr = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Spymt_Dest_Acct_Nbr", "#P-SPYMT-DEST-ACCT-NBR", FieldType.STRING, 
            21);
        ltfp0740_Pnd_P_Spymt_Dest_Trnst_Cde = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Spymt_Dest_Trnst_Cde", "#P-SPYMT-DEST-TRNST-CDE", FieldType.STRING, 
            9);
        ltfp0740_Pnd_P_Spymt_Dest_Acct_Typ = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Spymt_Dest_Acct_Typ", "#P-SPYMT-DEST-ACCT-TYP", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Cpymt_Dest_Name = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Dest_Name", "#P-CPYMT-DEST-NAME", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Cpymt_Dest_Addr1 = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Dest_Addr1", "#P-CPYMT-DEST-ADDR1", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Cpymt_Dest_Addr2 = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Dest_Addr2", "#P-CPYMT-DEST-ADDR2", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Cpymt_Dest_Addr3 = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Dest_Addr3", "#P-CPYMT-DEST-ADDR3", FieldType.STRING, 
            40);
        ltfp0740_Pnd_P_Cpymt_Dest_City = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Dest_City", "#P-CPYMT-DEST-CITY", FieldType.STRING, 
            28);
        ltfp0740_Pnd_P_Cpymt_Dest_State = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Dest_State", "#P-CPYMT-DEST-STATE", FieldType.STRING, 
            3);
        ltfp0740_Pnd_P_Cpymt_Zip = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Zip", "#P-CPYMT-ZIP", FieldType.STRING, 9);
        ltfp0740_Pnd_P_Cpymt_Dest_Acct_Nbr = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Dest_Acct_Nbr", "#P-CPYMT-DEST-ACCT-NBR", FieldType.STRING, 
            21);
        ltfp0740_Pnd_P_Cpymt_Dest_Trnst_Cde = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Dest_Trnst_Cde", "#P-CPYMT-DEST-TRNST-CDE", FieldType.STRING, 
            9);
        ltfp0740_Pnd_P_Cpymt_Dest_Acct_Typ = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Dest_Acct_Typ", "#P-CPYMT-DEST-ACCT-TYP", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Cpymt_Payee_Amt = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Payee_Amt", "#P-CPYMT-PAYEE-AMT", FieldType.NUMERIC, 
            11, 2);
        ltfp0740_Pnd_P_Cpymt_Payee_Type = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cpymt_Payee_Type", "#P-CPYMT-PAYEE-TYPE", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Two_Payments_In_One_Year = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Two_Payments_In_One_Year", "#P-TWO-PAYMENTS-IN-ONE-YEAR", 
            FieldType.STRING, 1);
        ltfp0740_Pnd_P_Fed_Exemptions = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Fed_Exemptions", "#P-FED-EXEMPTIONS", FieldType.STRING, 2);
        ltfp0740_Pnd_P_State_Exemptions = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_State_Exemptions", "#P-STATE-EXEMPTIONS", FieldType.STRING, 
            2);
        ltfp0740_Pnd_P_Mailing_Country_Code = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Mailing_Country_Code", "#P-MAILING-COUNTRY-CODE", FieldType.STRING, 
            3);
        ltfp0740_Pnd_P_Cod_Id = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Cod_Id", "#P-COD-ID", FieldType.STRING, 15);
        ltfp0740_Pnd_P_Minimum_Req_Amt = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Minimum_Req_Amt", "#P-MINIMUM-REQ-AMT", FieldType.STRING, 
            11);
        ltfp0740_Pnd_P_Payee_Cde = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Payee_Cde", "#P-PAYEE-CDE", FieldType.STRING, 1);
        ltfp0740_Pnd_P_Has_Roth_Money_Ind = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Has_Roth_Money_Ind", "#P-HAS-ROTH-MONEY-IND", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Issue_State = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Issue_State", "#P-ISSUE-STATE", FieldType.STRING, 2);
        ltfp0740_Pnd_P_Fed_Marital_Status = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Fed_Marital_Status", "#P-FED-MARITAL-STATUS", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_St_Marital_Status = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_St_Marital_Status", "#P-ST-MARITAL-STATUS", FieldType.STRING, 
            1);
        ltfp0740_Pnd_P_Date_Of_Death_A = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Date_Of_Death_A", "#P-DATE-OF-DEATH-A", FieldType.STRING, 
            8);

        ltfp0740__R_Field_4 = ltfp0740__R_Field_2.newGroupInGroup("ltfp0740__R_Field_4", "REDEFINE", ltfp0740_Pnd_P_Date_Of_Death_A);
        ltfp0740_Pnd_P_Date_Of_Death = ltfp0740__R_Field_4.newFieldInGroup("ltfp0740_Pnd_P_Date_Of_Death", "#P-DATE-OF-DEATH", FieldType.NUMERIC, 8);
        ltfp0740_Pnd_P_Date_Of_Birth_A = ltfp0740__R_Field_2.newFieldInGroup("ltfp0740_Pnd_P_Date_Of_Birth_A", "#P-DATE-OF-BIRTH-A", FieldType.STRING, 
            8);

        ltfp0740__R_Field_5 = ltfp0740__R_Field_2.newGroupInGroup("ltfp0740__R_Field_5", "REDEFINE", ltfp0740_Pnd_P_Date_Of_Birth_A);
        ltfp0740_Pnd_P_Date_Of_Birth = ltfp0740__R_Field_5.newFieldInGroup("ltfp0740_Pnd_P_Date_Of_Birth", "#P-DATE-OF-BIRTH", FieldType.NUMERIC, 8);

        ltfp0740__R_Field_6 = localVariables.newGroupInRecord("ltfp0740__R_Field_6", "REDEFINE", ltfp0740);
        ltfp0740_Pnd_B_Ltfp0740 = ltfp0740__R_Field_6.newFieldArrayInGroup("ltfp0740_Pnd_B_Ltfp0740", "#B-LTFP0740", FieldType.STRING, 1, new DbsArrayController(1, 
            3234));

        ltfp0740__R_Field_7 = ltfp0740__R_Field_6.newGroupInGroup("ltfp0740__R_Field_7", "REDEFINE", ltfp0740_Pnd_B_Ltfp0740);
        ltfp0740_Pnd_B_Sip_Letter_Type = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Sip_Letter_Type", "#B-SIP-LETTER-TYPE", FieldType.STRING, 
            4);
        ltfp0740_Pnd_B_Filler = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler", "#B-FILLER", FieldType.STRING, 1);

        ltfp0740_Pnd_B_Omni_Rec_Key = ltfp0740__R_Field_7.newGroupInGroup("ltfp0740_Pnd_B_Omni_Rec_Key", "#B-OMNI-REC-KEY");
        ltfp0740_Pnd_B_Omni_Plan_Id = ltfp0740_Pnd_B_Omni_Rec_Key.newFieldInGroup("ltfp0740_Pnd_B_Omni_Plan_Id", "#B-OMNI-PLAN-ID", FieldType.STRING, 
            6);
        ltfp0740_Pnd_B_Omni_Sub_Plan_Id = ltfp0740_Pnd_B_Omni_Rec_Key.newFieldInGroup("ltfp0740_Pnd_B_Omni_Sub_Plan_Id", "#B-OMNI-SUB-PLAN-ID", FieldType.STRING, 
            17);

        ltfp0740__R_Field_8 = ltfp0740_Pnd_B_Omni_Rec_Key.newGroupInGroup("ltfp0740__R_Field_8", "REDEFINE", ltfp0740_Pnd_B_Omni_Sub_Plan_Id);
        ltfp0740_Pnd_B_Omni_Ssn = ltfp0740__R_Field_8.newFieldInGroup("ltfp0740_Pnd_B_Omni_Ssn", "#B-OMNI-SSN", FieldType.NUMERIC, 9);
        ltfp0740_Pnd_B_Omni_Sub_Plan = ltfp0740__R_Field_8.newFieldInGroup("ltfp0740_Pnd_B_Omni_Sub_Plan", "#B-OMNI-SUB-PLAN", FieldType.STRING, 8);
        ltfp0740_Pnd_B_Pin = ltfp0740_Pnd_B_Omni_Rec_Key.newFieldInGroup("ltfp0740_Pnd_B_Pin", "#B-PIN", FieldType.STRING, 13);
        ltfp0740_Pnd_B_Sip_Tiaa_Contract = ltfp0740_Pnd_B_Omni_Rec_Key.newFieldInGroup("ltfp0740_Pnd_B_Sip_Tiaa_Contract", "#B-SIP-TIAA-CONTRACT", FieldType.STRING, 
            10);
        ltfp0740_Pnd_B_Sip_Cref_Cert = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Sip_Cref_Cert", "#B-SIP-CREF-CERT", FieldType.STRING, 10);
        ltfp0740_Pnd_B_Omni_Plan_Name = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Omni_Plan_Name", "#B-OMNI-PLAN-NAME", FieldType.STRING, 50);
        ltfp0740_Pnd_B_Participant_Name = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Participant_Name", "#B-PARTICIPANT-NAME", FieldType.STRING, 
            40);
        ltfp0740_Pnd_B_Bene_Category = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Bene_Category", "#B-BENE-CATEGORY", FieldType.STRING, 1);
        ltfp0740_Pnd_B_Bene_Name = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Bene_Name", "#B-BENE-NAME", FieldType.STRING, 70);
        ltfp0740_Pnd_B_Bene_Birth_Date = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Bene_Birth_Date", "#B-BENE-BIRTH-DATE", FieldType.NUMERIC, 
            8);
        ltfp0740_Pnd_B_Bene_Ssn = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Bene_Ssn", "#B-BENE-SSN", FieldType.NUMERIC, 9);
        ltfp0740_Pnd_B_Bene_Relationship = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Bene_Relationship", "#B-BENE-RELATIONSHIP", FieldType.STRING, 
            1);
        ltfp0740_Pnd_B_Bene_Allocation_Pct = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Bene_Allocation_Pct", "#B-BENE-ALLOCATION-PCT", FieldType.NUMERIC, 
            5, 2);
        ltfp0740_Pnd_B_Prim_Child_Prov = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Prim_Child_Prov", "#B-PRIM-CHILD-PROV", FieldType.STRING, 
            1);
        ltfp0740_Pnd_B_Cont_Child_Prov = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Cont_Child_Prov", "#B-CONT-CHILD-PROV", FieldType.STRING, 
            1);
        ltfp0740_Pnd_B_Cal_Bene_Meth = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Cal_Bene_Meth", "#B-CAL-BENE-METH", FieldType.STRING, 1);
        ltfp0740_Pnd_B_Filler1 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler1", "#B-FILLER1", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler2 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler2", "#B-FILLER2", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler3 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler3", "#B-FILLER3", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler4 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler4", "#B-FILLER4", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler5 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler5", "#B-FILLER5", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler6 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler6", "#B-FILLER6", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler7 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler7", "#B-FILLER7", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler8 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler8", "#B-FILLER8", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler9 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler9", "#B-FILLER9", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler10 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler10", "#B-FILLER10", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler11 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler11", "#B-FILLER11", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler12 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler12", "#B-FILLER12", FieldType.STRING, 227);
        ltfp0740_Pnd_B_Filler14 = ltfp0740__R_Field_7.newFieldInGroup("ltfp0740_Pnd_B_Filler14", "#B-FILLER14", FieldType.STRING, 229);
        pnd_Baseext_Rec = localVariables.newFieldArrayInRecord("pnd_Baseext_Rec", "#BASEEXT-REC", FieldType.STRING, 1, new DbsArrayController(1, 4000));

        pnd_Baseext_Rec__R_Field_9 = localVariables.newGroupInRecord("pnd_Baseext_Rec__R_Field_9", "REDEFINE", pnd_Baseext_Rec);
        pnd_Baseext_Rec_Pnd_Baseext_Rectype = pnd_Baseext_Rec__R_Field_9.newFieldInGroup("pnd_Baseext_Rec_Pnd_Baseext_Rectype", "#BASEEXT-RECTYPE", FieldType.STRING, 
            4);
        pnd_Input_Part_Aa = localVariables.newFieldArrayInRecord("pnd_Input_Part_Aa", "#INPUT-PART-AA", FieldType.STRING, 1, new DbsArrayController(1, 
            1266));

        pnd_Input_Part_Aa__R_Field_10 = localVariables.newGroupInRecord("pnd_Input_Part_Aa__R_Field_10", "REDEFINE", pnd_Input_Part_Aa);
        pnd_Input_Part_Aa_Pnd_Rec_Iden = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Rec_Iden", "#REC-IDEN", FieldType.STRING, 
            4);
        pnd_Input_Part_Aa_Pnd_Rec_Type = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Rec_Type", "#REC-TYPE", FieldType.STRING, 
            1);
        pnd_Input_Part_Aa_Pnd_Plan_Name = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Plan_Name", "#PLAN-NAME", FieldType.STRING, 
            32);
        pnd_Input_Part_Aa_Pnd_Plan_Id = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Plan_Id", "#PLAN-ID", FieldType.STRING, 6);
        pnd_Input_Part_Aa_Pnd_Erisa_Ind = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Erisa_Ind", "#ERISA-IND", FieldType.STRING, 
            1);
        pnd_Input_Part_Aa_Pnd_Cis_Tpa_Tiaa_Cntrct = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Tpa_Tiaa_Cntrct", "#CIS-TPA-TIAA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Input_Part_Aa_Pnd_Cis_Tpa_Cref_Cntrct = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Tpa_Cref_Cntrct", "#CIS-TPA-CREF-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Input_Part_Aa_Pnd_Cis_Pin_Nbr = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Pin_Nbr", "#CIS-PIN-NBR", FieldType.STRING, 
            12);

        pnd_Input_Part_Aa__R_Field_11 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_11", "REDEFINE", pnd_Input_Part_Aa_Pnd_Cis_Pin_Nbr);
        pnd_Input_Part_Aa_Pnd_Cis_Pin_Nbr_N = pnd_Input_Part_Aa__R_Field_11.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Pin_Nbr_N", "#CIS-PIN-NBR-N", FieldType.NUMERIC, 
            12);
        pnd_Input_Part_Aa_Pnd_Cis_Rqst_Id_Key = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Rqst_Id_Key", "#CIS-RQST-ID-KEY", 
            FieldType.STRING, 30);
        pnd_Input_Part_Aa_Pnd_Cis_Tpa_Cntrct = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Tpa_Cntrct", "#CIS-TPA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Input_Part_Aa_Pnd_Cis_Requestor = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Requestor", "#CIS-REQUESTOR", FieldType.STRING, 
            3);
        pnd_Input_Part_Aa_Pnd_Cis_Cntrct_Type = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Cntrct_Type", "#CIS-CNTRCT-TYPE", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Cis_Opn_Clsd_Ind = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Opn_Clsd_Ind", "#CIS-OPN-CLSD-IND", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Cis_Status_Cd = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Status_Cd", "#CIS-STATUS-CD", FieldType.STRING, 
            1);
        pnd_Input_Part_Aa_Pnd_Cis_Annty_Option = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Annty_Option", "#CIS-ANNTY-OPTION", 
            FieldType.STRING, 4);
        pnd_Input_Part_Aa_Pnd_Cis_Cntrct_Print_Dte = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Cntrct_Print_Dte", "#CIS-CNTRCT-PRINT-DTE", 
            FieldType.STRING, 8);
        pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Frst_Nme = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Frst_Nme", "#CIS-FRST-ANNT-FRST-NME", 
            FieldType.STRING, 20);
        pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Mid_Nme = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Mid_Nme", "#CIS-FRST-ANNT-MID-NME", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Lst_Nme = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Lst_Nme", "#CIS-FRST-ANNT-LST-NME", 
            FieldType.STRING, 20);
        pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Prf_Nme = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Prf_Nme", "#CIS-FRST-ANNT-PRF-NME", 
            FieldType.STRING, 3);
        pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Sex_Cde = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Sex_Cde", "#CIS-FRST-ANNT-SEX-CDE", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn", "#CIS-FRST-ANNT-SSN", 
            FieldType.STRING, 9);

        pnd_Input_Part_Aa__R_Field_12 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_12", "REDEFINE", pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn);
        pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn_N = pnd_Input_Part_Aa__R_Field_12.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn_N", "#CIS-FRST-ANNT-SSN-N", 
            FieldType.NUMERIC, 9);
        pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Dob = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Dob", "#CIS-FRST-ANNT-DOB", 
            FieldType.STRING, 8);
        pnd_Input_Part_Aa_Pnd_Addrss_Line_1 = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Addrss_Line_1", "#ADDRSS-LINE-1", FieldType.STRING, 
            40);
        pnd_Input_Part_Aa_Pnd_Addrss_Line_2 = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Addrss_Line_2", "#ADDRSS-LINE-2", FieldType.STRING, 
            33);
        pnd_Input_Part_Aa_Pnd_Addrss_Line_2_3 = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Addrss_Line_2_3", "#ADDRSS-LINE-2-3", 
            FieldType.STRING, 7);
        pnd_Input_Part_Aa_Pnd_Addrss_Line_3 = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Addrss_Line_3", "#ADDRSS-LINE-3", FieldType.STRING, 
            40);
        pnd_Input_Part_Aa_Pnd_City = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_City", "#CITY", FieldType.STRING, 28);
        pnd_Input_Part_Aa_Pnd_Resid_Stae = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Resid_Stae", "#RESID-STAE", FieldType.STRING, 
            2);
        pnd_Input_Part_Aa_Pnd_Zip = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Zip", "#ZIP", FieldType.STRING, 9);
        pnd_Input_Part_Aa_Pnd_Term_Date = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Term_Date", "#TERM-DATE", FieldType.STRING, 
            8);
        pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ctznshp_Cd = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ctznshp_Cd", 
            "#CIS-FRST-ANNT-CTZNSHP-CD", FieldType.STRING, 2);
        pnd_Input_Part_Aa_Pnd_Cis_Tiaa_Doi = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Tiaa_Doi", "#CIS-TIAA-DOI", FieldType.STRING, 
            8);
        pnd_Input_Part_Aa_Pnd_Cis_Annty_Start_Dte = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Annty_Start_Dte", "#CIS-ANNTY-START-DTE", 
            FieldType.STRING, 8);
        pnd_Input_Part_Aa_Pnd_Cis_Annty_End_Dte = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Annty_End_Dte", "#CIS-ANNTY-END-DTE", 
            FieldType.STRING, 8);
        pnd_Input_Part_Aa_Pnd_Cis_Pymnt_Mode = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Pymnt_Mode", "#CIS-PYMNT-MODE", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Cis_Grnted_Periods_Yrs = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Grnted_Periods_Yrs", "#CIS-GRNTED-PERIODS-YRS", 
            FieldType.STRING, 2);

        pnd_Input_Part_Aa__R_Field_13 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_13", "REDEFINE", pnd_Input_Part_Aa_Pnd_Cis_Grnted_Periods_Yrs);
        pnd_Input_Part_Aa_Pnd_Cis_Grnted_Periods_Yrs_N = pnd_Input_Part_Aa__R_Field_13.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Grnted_Periods_Yrs_N", 
            "#CIS-GRNTED-PERIODS-YRS-N", FieldType.NUMERIC, 2);
        pnd_Input_Part_Aa_Pnd_Cis_Grnted_Grd_Amt = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Grnted_Grd_Amt", "#CIS-GRNTED-GRD-AMT", 
            FieldType.STRING, 11);

        pnd_Input_Part_Aa__R_Field_14 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_14", "REDEFINE", pnd_Input_Part_Aa_Pnd_Cis_Grnted_Grd_Amt);
        pnd_Input_Part_Aa_Pnd_Cis_Grnted_Grd_Amt_N = pnd_Input_Part_Aa__R_Field_14.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Grnted_Grd_Amt_N", "#CIS-GRNTED-GRD-AMT-N", 
            FieldType.NUMERIC, 11, 2);
        pnd_Input_Part_Aa_Pnd_Cis_Lob = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Lob", "#CIS-LOB", FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Cis_Lob_Type = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Lob_Type", "#CIS-LOB-TYPE", FieldType.STRING, 
            1);
        pnd_Input_Part_Aa_Pnd_Cis_Da_Tiaa_Nbr_Sttl = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Da_Tiaa_Nbr_Sttl", "#CIS-DA-TIAA-NBR-STTL", 
            FieldType.STRING, 10);
        pnd_Input_Part_Aa_Pnd_Cis_Da_Proceeds_Amt = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Da_Proceeds_Amt", "#CIS-DA-PROCEEDS-AMT", 
            FieldType.STRING, 11);

        pnd_Input_Part_Aa__R_Field_15 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_15", "REDEFINE", pnd_Input_Part_Aa_Pnd_Cis_Da_Proceeds_Amt);
        pnd_Input_Part_Aa_Pnd_Cis_Da_Proceeds_Amt_N = pnd_Input_Part_Aa__R_Field_15.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Da_Proceeds_Amt_N", "#CIS-DA-PROCEEDS-AMT-N", 
            FieldType.NUMERIC, 11, 2);
        pnd_Input_Part_Aa_Pnd_Cis_Rtb_Amt = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Rtb_Amt", "#CIS-RTB-AMT", FieldType.STRING, 
            11);

        pnd_Input_Part_Aa__R_Field_16 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_16", "REDEFINE", pnd_Input_Part_Aa_Pnd_Cis_Rtb_Amt);
        pnd_Input_Part_Aa_Pnd_Cis_Rtb_Amt_N = pnd_Input_Part_Aa__R_Field_16.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Rtb_Amt_N", "#CIS-RTB-AMT-N", FieldType.NUMERIC, 
            11, 2);
        pnd_Input_Part_Aa_Pnd_Cis_Rtb_Pct = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Rtb_Pct", "#CIS-RTB-PCT", FieldType.STRING, 
            4);

        pnd_Input_Part_Aa__R_Field_17 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_17", "REDEFINE", pnd_Input_Part_Aa_Pnd_Cis_Rtb_Pct);
        pnd_Input_Part_Aa_Pnd_Cis_Rtb_Pct_N = pnd_Input_Part_Aa__R_Field_17.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Rtb_Pct_N", "#CIS-RTB-PCT-N", FieldType.NUMERIC, 
            4, 2);
        pnd_Input_Part_Aa_Pnd_Cis_Trnsf_Tiaa_Rea_Nbr = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Trnsf_Tiaa_Rea_Nbr", "#CIS-TRNSF-TIAA-REA-NBR", 
            FieldType.STRING, 10);
        pnd_Input_Part_Aa_Pnd_Cis_Trnsf_Tiaa_Cer_Nbr = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Trnsf_Tiaa_Cer_Nbr", "#CIS-TRNSF-TIAA-CER-NBR", 
            FieldType.STRING, 10);
        pnd_Input_Part_Aa_Pnd_Cis_Handle_Code = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Handle_Code", "#CIS-HANDLE-CODE", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Cis_Org_Issue_St = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Org_Issue_St", "#CIS-ORG-ISSUE-ST", 
            FieldType.STRING, 2);
        pnd_Input_Part_Aa_Pnd_Fill = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Fill", "#FILL", FieldType.STRING, 5);
        pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Amt = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Amt", "#CIS-COMUT-GRNTED-AMT", 
            FieldType.STRING, 11);

        pnd_Input_Part_Aa__R_Field_18 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_18", "REDEFINE", pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Amt);
        pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Amt_N = pnd_Input_Part_Aa__R_Field_18.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Amt_N", "#CIS-COMUT-GRNTED-AMT-N", 
            FieldType.NUMERIC, 11, 2);
        pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Pct = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Pct", "#CIS-COMUT-GRNTED-PCT", 
            FieldType.STRING, 4);

        pnd_Input_Part_Aa__R_Field_19 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_19", "REDEFINE", pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Pct);
        pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Pct_N = pnd_Input_Part_Aa__R_Field_19.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Cis_Comut_Grnted_Pct_N", "#CIS-COMUT-GRNTED-PCT-N", 
            FieldType.NUMERIC, 4, 2);
        pnd_Input_Part_Aa_Pnd_Dest_Name = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dest_Name", "#DEST-NAME", FieldType.STRING, 
            32);
        pnd_Input_Part_Aa_Pnd_Dest_Addr_Line = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dest_Addr_Line", "#DEST-ADDR-LINE", 
            FieldType.STRING, 40);
        pnd_Input_Part_Aa_Pnd_Dest_Addr_Line_2 = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dest_Addr_Line_2", "#DEST-ADDR-LINE-2", 
            FieldType.STRING, 40);
        pnd_Input_Part_Aa_Pnd_Dest_Addr_Line_3 = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dest_Addr_Line_3", "#DEST-ADDR-LINE-3", 
            FieldType.STRING, 40);
        pnd_Input_Part_Aa_Pnd_Dest_Addr_City = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dest_Addr_City", "#DEST-ADDR-CITY", 
            FieldType.STRING, 28);
        pnd_Input_Part_Aa_Pnd_Dest_Addr_St = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dest_Addr_St", "#DEST-ADDR-ST", FieldType.STRING, 
            2);
        pnd_Input_Part_Aa_Pnd_Dest_Addr_Zip = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dest_Addr_Zip", "#DEST-ADDR-ZIP", FieldType.STRING, 
            9);
        pnd_Input_Part_Aa_Pnd_Dest_Bank_Type = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dest_Bank_Type", "#DEST-BANK-TYPE", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Dest_Bank_Acct_Nbr = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dest_Bank_Acct_Nbr", "#DEST-BANK-ACCT-NBR", 
            FieldType.STRING, 17);
        pnd_Input_Part_Aa_Pnd_Dest_Bank_Trst_Nbr = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dest_Bank_Trst_Nbr", "#DEST-BANK-TRST-NBR", 
            FieldType.STRING, 10);
        pnd_Input_Part_Aa_Pnd_Rollover_Dest_Name = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Rollover_Dest_Name", "#ROLLOVER-DEST-NAME", 
            FieldType.STRING, 30);
        pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Line = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Line", 
            "#ROLLOVER-DEST-ADDR-LINE", FieldType.STRING, 40);
        pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Line_2 = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Line_2", 
            "#ROLLOVER-DEST-ADDR-LINE-2", FieldType.STRING, 40);
        pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Line_3 = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Line_3", 
            "#ROLLOVER-DEST-ADDR-LINE-3", FieldType.STRING, 40);
        pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_City = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_City", 
            "#ROLLOVER-DEST-ADDR-CITY", FieldType.STRING, 28);
        pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_St = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_St", "#ROLLOVER-DEST-ADDR-ST", 
            FieldType.STRING, 2);
        pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Zip = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Rollover_Dest_Addr_Zip", "#ROLLOVER-DEST-ADDR-ZIP", 
            FieldType.STRING, 9);
        pnd_Input_Part_Aa_Pnd_Rollover_Dest_Bank_Acct_Nbr = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Rollover_Dest_Bank_Acct_Nbr", 
            "#ROLLOVER-DEST-BANK-ACCT-NBR", FieldType.STRING, 17);
        pnd_Input_Part_Aa_Pnd_Power_Image_Id = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Power_Image_Id", "#POWER-IMAGE-ID", 
            FieldType.STRING, 15);
        pnd_Input_Part_Aa_Pnd_Fed_Ovr_Or_Add = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Fed_Ovr_Or_Add", "#FED-OVR-OR-ADD", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Fed_Dol_Or_Pct = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Fed_Dol_Or_Pct", "#FED-DOL-OR-PCT", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Fed_Pct_Amt_Or_Dol = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Fed_Pct_Amt_Or_Dol", "#FED-PCT-AMT-OR-DOL", 
            FieldType.STRING, 11);

        pnd_Input_Part_Aa__R_Field_20 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_20", "REDEFINE", pnd_Input_Part_Aa_Pnd_Fed_Pct_Amt_Or_Dol);
        pnd_Input_Part_Aa_Pnd_Fed_Pct_Amt_Or_Dol_N = pnd_Input_Part_Aa__R_Field_20.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Fed_Pct_Amt_Or_Dol_N", "#FED-PCT-AMT-OR-DOL-N", 
            FieldType.NUMERIC, 11, 2);
        pnd_Input_Part_Aa_Pnd_Fed_No_Of_Exempt = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Fed_No_Of_Exempt", "#FED-NO-OF-EXEMPT", 
            FieldType.STRING, 1);

        pnd_Input_Part_Aa__R_Field_21 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_21", "REDEFINE", pnd_Input_Part_Aa_Pnd_Fed_No_Of_Exempt);
        pnd_Input_Part_Aa_Pnd_Fed_No_Of_Exempt_N = pnd_Input_Part_Aa__R_Field_21.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Fed_No_Of_Exempt_N", "#FED-NO-OF-EXEMPT-N", 
            FieldType.NUMERIC, 1);
        pnd_Input_Part_Aa_Pnd_St_Ovr_Or_Add = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_St_Ovr_Or_Add", "#ST-OVR-OR-ADD", FieldType.STRING, 
            1);
        pnd_Input_Part_Aa_Pnd_St_Dol_Or_Pct = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_St_Dol_Or_Pct", "#ST-DOL-OR-PCT", FieldType.STRING, 
            1);
        pnd_Input_Part_Aa_Pnd_St_Pct_Amt_Or_Dol = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_St_Pct_Amt_Or_Dol", "#ST-PCT-AMT-OR-DOL", 
            FieldType.STRING, 11);

        pnd_Input_Part_Aa__R_Field_22 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_22", "REDEFINE", pnd_Input_Part_Aa_Pnd_St_Pct_Amt_Or_Dol);
        pnd_Input_Part_Aa_Pnd_St_Pct_Amt_Or_Dol_N = pnd_Input_Part_Aa__R_Field_22.newFieldInGroup("pnd_Input_Part_Aa_Pnd_St_Pct_Amt_Or_Dol_N", "#ST-PCT-AMT-OR-DOL-N", 
            FieldType.NUMERIC, 11, 2);
        pnd_Input_Part_Aa_Pnd_St_No_Of_Exempt = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_St_No_Of_Exempt", "#ST-NO-OF-EXEMPT", 
            FieldType.STRING, 1);

        pnd_Input_Part_Aa__R_Field_23 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_23", "REDEFINE", pnd_Input_Part_Aa_Pnd_St_No_Of_Exempt);
        pnd_Input_Part_Aa_Pnd_St_No_Of_Exempt_N = pnd_Input_Part_Aa__R_Field_23.newFieldInGroup("pnd_Input_Part_Aa_Pnd_St_No_Of_Exempt_N", "#ST-NO-OF-EXEMPT-N", 
            FieldType.NUMERIC, 1);
        pnd_Input_Part_Aa_Pnd_Fed_Opt = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Fed_Opt", "#FED-OPT", FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_St_Opt = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_St_Opt", "#ST-OPT", FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Fed_Marital_Sts = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Fed_Marital_Sts", "#FED-MARITAL-STS", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_St_Marital_Sts = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_St_Marital_Sts", "#ST-MARITAL-STS", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Dollar_Or_Pcnt = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dollar_Or_Pcnt", "#DOLLAR-OR-PCNT", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Dollar_Or_Pcnt_Amt = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Dollar_Or_Pcnt_Amt", "#DOLLAR-OR-PCNT-AMT", 
            FieldType.STRING, 5);
        pnd_Input_Part_Aa_Pnd_Sub_Plan = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Sub_Plan", "#SUB-PLAN", FieldType.STRING, 
            6);
        pnd_Input_Part_Aa_Pnd_Foreign_Or_Domestic = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Foreign_Or_Domestic", "#FOREIGN-OR-DOMESTIC", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Country_For_Dometic = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Country_For_Dometic", "#COUNTRY-FOR-DOMETIC", 
            FieldType.STRING, 2);
        pnd_Input_Part_Aa_Pnd_Payee_Cde = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Payee_Cde", "#PAYEE-CDE", FieldType.STRING, 
            1);
        pnd_Input_Part_Aa_Pnd_Has_Roth_Money_Ind = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Has_Roth_Money_Ind", "#HAS-ROTH-MONEY-IND", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Fund_Allocation = pnd_Input_Part_Aa__R_Field_10.newFieldArrayInGroup("pnd_Input_Part_Aa_Pnd_Fund_Allocation", "#FUND-ALLOCATION", 
            FieldType.STRING, 14, new DbsArrayController(1, 20));

        pnd_Input_Part_Aa__R_Field_24 = pnd_Input_Part_Aa__R_Field_10.newGroupInGroup("pnd_Input_Part_Aa__R_Field_24", "REDEFINE", pnd_Input_Part_Aa_Pnd_Fund_Allocation);
        pnd_Input_Part_Aa_Pnd_Fund_Ticker = pnd_Input_Part_Aa__R_Field_24.newFieldArrayInGroup("pnd_Input_Part_Aa_Pnd_Fund_Ticker", "#FUND-TICKER", FieldType.STRING, 
            10, new DbsArrayController(1, 20));
        pnd_Input_Part_Aa_Pnd_Fund_Alloc = pnd_Input_Part_Aa__R_Field_24.newFieldArrayInGroup("pnd_Input_Part_Aa_Pnd_Fund_Alloc", "#FUND-ALLOC", FieldType.NUMERIC, 
            4, 1, new DbsArrayController(1, 20));
        pnd_Input_Part_Aa_Pnd_Acceptance_Ind = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Acceptance_Ind", "#ACCEPTANCE-IND", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Relation_To_Decedent = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Relation_To_Decedent", "#RELATION-TO-DECEDENT", 
            FieldType.STRING, 1);
        pnd_Input_Part_Aa_Pnd_Ssn_Tin_Ind = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Ssn_Tin_Ind", "#SSN-TIN-IND", FieldType.STRING, 
            1);
        pnd_Input_Part_Aa_Pnd_Decedent_Contract = pnd_Input_Part_Aa__R_Field_10.newFieldInGroup("pnd_Input_Part_Aa_Pnd_Decedent_Contract", "#DECEDENT-CONTRACT", 
            FieldType.STRING, 10);

        vw_cis_Prtcpnt_File = new DataAccessProgramView(new NameInfo("vw_cis_Prtcpnt_File", "CIS-PRTCPNT-FILE"), "CIS_PRTCPNT_FILE_12", "CIS_PRTCPNT_FILE");
        cis_Prtcpnt_File_Cis_Pin_Nbr = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CIS_PIN_NBR");
        cis_Prtcpnt_File_Cis_Tiaa_Nbr = vw_cis_Prtcpnt_File.getRecord().newFieldInGroup("cis_Prtcpnt_File_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CIS_TIAA_NBR");
        registerRecord(vw_cis_Prtcpnt_File);

        pnd_Tiaa_Cntrct_Found = localVariables.newFieldInRecord("pnd_Tiaa_Cntrct_Found", "#TIAA-CNTRCT-FOUND", FieldType.BOOLEAN, 1);

        vw_batch_Restart = new DataAccessProgramView(new NameInfo("vw_batch_Restart", "BATCH-RESTART"), "BATCH_RESTART", "BATCH_RESTART", DdmPeriodicGroups.getInstance().getGroups("BATCH_RESTART"));
        batch_Restart_Rst_Actve_Ind = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Actve_Ind", "RST-ACTVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RST_ACTVE_IND");
        batch_Restart_Rst_Actve_Ind.setDdmHeader("ACTIVE IND");
        batch_Restart_Rst_Job_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Job_Nme", "RST-JOB-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_JOB_NME");
        batch_Restart_Rst_Job_Nme.setDdmHeader("JOB NAME");
        batch_Restart_Rst_Jobstep_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Jobstep_Nme", "RST-JOBSTEP-NME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_JOBSTEP_NME");
        batch_Restart_Rst_Jobstep_Nme.setDdmHeader("STEP NAME");
        batch_Restart_Rst_Pgm_Nme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Pgm_Nme", "RST-PGM-NME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_PGM_NME");
        batch_Restart_Rst_Pgm_Nme.setDdmHeader("PROGRAM NAME");
        batch_Restart_Rst_Curr_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Curr_Dte", "RST-CURR-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_CURR_DTE");
        batch_Restart_Rst_Curr_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Curr_Tme", "RST-CURR-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_CURR_TME");
        batch_Restart_Rst_Start_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Start_Dte", "RST-START-DTE", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RST_START_DTE");
        batch_Restart_Rst_Start_Dte.setDdmHeader("START/DATE");
        batch_Restart_Rst_Start_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Start_Tme", "RST-START-TME", FieldType.STRING, 8, 
            RepeatingFieldStrategy.None, "RST_START_TME");
        batch_Restart_Rst_Start_Tme.setDdmHeader("START/TIME");
        batch_Restart_Rst_End_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_End_Dte", "RST-END-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_END_DTE");
        batch_Restart_Rst_End_Dte.setDdmHeader("END/DATE");
        batch_Restart_Rst_End_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_End_Tme", "RST-END-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_END_TME");
        batch_Restart_Rst_End_Tme.setDdmHeader("END/TIME");
        batch_Restart_Rst_Key = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Key", "RST-KEY", FieldType.STRING, 50, RepeatingFieldStrategy.None, 
            "RST_KEY");
        batch_Restart_Rst_Cnt = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Cnt", "RST-CNT", FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, 
            "RST_CNT");
        batch_Restart_Rst_Isn_Nbr = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Isn_Nbr", "RST-ISN-NBR", FieldType.PACKED_DECIMAL, 
            10, RepeatingFieldStrategy.None, "RST_ISN_NBR");
        batch_Restart_Rst_Rstrt_Data_Ind = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Rstrt_Data_Ind", "RST-RSTRT-DATA-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RST_RSTRT_DATA_IND");
        batch_Restart_Rst_Rstrt_Data_Ind.setDdmHeader("FAILED");

        batch_Restart_Rst_Data_Grp = vw_batch_Restart.getRecord().newGroupInGroup("batch_Restart_Rst_Data_Grp", "RST-DATA-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BATCH_RESTART_RST_DATA_GRP");
        batch_Restart_Rst_Data_Txt = batch_Restart_Rst_Data_Grp.newFieldArrayInGroup("batch_Restart_Rst_Data_Txt", "RST-DATA-TXT", FieldType.STRING, 250, 
            new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "RST_DATA_TXT", "BATCH_RESTART_RST_DATA_GRP");
        batch_Restart_Rst_Frst_Run_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Frst_Run_Dte", "RST-FRST-RUN-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_FRST_RUN_DTE");
        batch_Restart_Rst_Frst_Run_Dte.setDdmHeader("FIRST/RUN DTE");
        batch_Restart_Rst_Last_Rstrt_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Last_Rstrt_Dte", "RST-LAST-RSTRT-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_LAST_RSTRT_DTE");
        batch_Restart_Rst_Last_Rstrt_Dte.setDdmHeader("LAST RSTRT/DATE");
        batch_Restart_Rst_Last_Rstrt_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Last_Rstrt_Tme", "RST-LAST-RSTRT-TME", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "RST_LAST_RSTRT_TME");
        batch_Restart_Rst_Last_Rstrt_Tme.setDdmHeader("LAST RSTRT/TIME");
        batch_Restart_Rst_Updt_Dte = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Updt_Dte", "RST-UPDT-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_UPDT_DTE");
        batch_Restart_Rst_Updt_Tme = vw_batch_Restart.getRecord().newFieldInGroup("batch_Restart_Rst_Updt_Tme", "RST-UPDT-TME", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RST_UPDT_TME");
        registerRecord(vw_batch_Restart);

        pnd_Restart_Key = localVariables.newFieldInRecord("pnd_Restart_Key", "#RESTART-KEY", FieldType.STRING, 50);

        pnd_Restart_Key__R_Field_25 = localVariables.newGroupInRecord("pnd_Restart_Key__R_Field_25", "REDEFINE", pnd_Restart_Key);

        pnd_Restart_Key_Pnd_Restart_Structure = pnd_Restart_Key__R_Field_25.newGroupInGroup("pnd_Restart_Key_Pnd_Restart_Structure", "#RESTART-STRUCTURE");
        pnd_Restart_Key_Pnd_Restart_Plan_No = pnd_Restart_Key_Pnd_Restart_Structure.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Plan_No", "#RESTART-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Restart_Key_Pnd_Restart_Part_Id = pnd_Restart_Key_Pnd_Restart_Structure.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Part_Id", "#RESTART-PART-ID", 
            FieldType.STRING, 17);

        pnd_Restart_Key__R_Field_26 = pnd_Restart_Key_Pnd_Restart_Structure.newGroupInGroup("pnd_Restart_Key__R_Field_26", "REDEFINE", pnd_Restart_Key_Pnd_Restart_Part_Id);
        pnd_Restart_Key_Pnd_Restart_Ssn = pnd_Restart_Key__R_Field_26.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Ssn", "#RESTART-SSN", FieldType.STRING, 
            9);
        pnd_Restart_Key_Pnd_Restart_Subplan_No = pnd_Restart_Key__R_Field_26.newFieldInGroup("pnd_Restart_Key_Pnd_Restart_Subplan_No", "#RESTART-SUBPLAN-NO", 
            FieldType.STRING, 8);
        pnd_Sp_Actve_Job_Step = localVariables.newFieldInRecord("pnd_Sp_Actve_Job_Step", "#SP-ACTVE-JOB-STEP", FieldType.STRING, 17);

        pnd_Sp_Actve_Job_Step__R_Field_27 = localVariables.newGroupInRecord("pnd_Sp_Actve_Job_Step__R_Field_27", "REDEFINE", pnd_Sp_Actve_Job_Step);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind = pnd_Sp_Actve_Job_Step__R_Field_27.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind", "#RST-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme = pnd_Sp_Actve_Job_Step__R_Field_27.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme", "#RST-JOB-NME", 
            FieldType.STRING, 8);
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme = pnd_Sp_Actve_Job_Step__R_Field_27.newFieldInGroup("pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme", "#RST-STEP-NME", 
            FieldType.STRING, 8);
        pnd_Restart_Isn = localVariables.newFieldInRecord("pnd_Restart_Isn", "#RESTART-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Omni_Header = localVariables.newFieldInRecord("pnd_Omni_Header", "#OMNI-HEADER", FieldType.STRING, 80);

        pnd_Omni_Header__R_Field_28 = localVariables.newGroupInRecord("pnd_Omni_Header__R_Field_28", "REDEFINE", pnd_Omni_Header);
        pnd_Omni_Header_Pnd_Tran_Code = pnd_Omni_Header__R_Field_28.newFieldInGroup("pnd_Omni_Header_Pnd_Tran_Code", "#TRAN-CODE", FieldType.STRING, 3);
        pnd_Omni_Header_Pnd_Seq_Code = pnd_Omni_Header__R_Field_28.newFieldInGroup("pnd_Omni_Header_Pnd_Seq_Code", "#SEQ-CODE", FieldType.STRING, 2);
        pnd_Omni_Header_Pnd_Notused1 = pnd_Omni_Header__R_Field_28.newFieldInGroup("pnd_Omni_Header_Pnd_Notused1", "#NOTUSED1", FieldType.STRING, 3);
        pnd_Omni_Header_Pnd_Rec_Type = pnd_Omni_Header__R_Field_28.newFieldInGroup("pnd_Omni_Header_Pnd_Rec_Type", "#REC-TYPE", FieldType.STRING, 7);
        pnd_Omni_Header_Pnd_P_Omni_Plan_Id = pnd_Omni_Header__R_Field_28.newFieldInGroup("pnd_Omni_Header_Pnd_P_Omni_Plan_Id", "#P-OMNI-PLAN-ID", FieldType.STRING, 
            6);
        pnd_Omni_Header_Pnd_Part_Id = pnd_Omni_Header__R_Field_28.newFieldInGroup("pnd_Omni_Header_Pnd_Part_Id", "#PART-ID", FieldType.STRING, 17);
        pnd_Omni_Header_Pnd_Notused2 = pnd_Omni_Header__R_Field_28.newFieldInGroup("pnd_Omni_Header_Pnd_Notused2", "#NOTUSED2", FieldType.STRING, 3);
        pnd_Omni_Header_Pnd_Trade_Date = pnd_Omni_Header__R_Field_28.newFieldInGroup("pnd_Omni_Header_Pnd_Trade_Date", "#TRADE-DATE", FieldType.NUMERIC, 
            8);

        pnd_Omni_Header__R_Field_29 = pnd_Omni_Header__R_Field_28.newGroupInGroup("pnd_Omni_Header__R_Field_29", "REDEFINE", pnd_Omni_Header_Pnd_Trade_Date);
        pnd_Omni_Header_Pnd_Trade_Date_A = pnd_Omni_Header__R_Field_29.newFieldInGroup("pnd_Omni_Header_Pnd_Trade_Date_A", "#TRADE-DATE-A", FieldType.STRING, 
            8);
        pnd_Omni_Detail = localVariables.newFieldInRecord("pnd_Omni_Detail", "#OMNI-DETAIL", FieldType.STRING, 80);

        pnd_Omni_Detail__R_Field_30 = localVariables.newGroupInRecord("pnd_Omni_Detail__R_Field_30", "REDEFINE", pnd_Omni_Detail);
        pnd_Omni_Detail_Pnd_Tran_Code = pnd_Omni_Detail__R_Field_30.newFieldInGroup("pnd_Omni_Detail_Pnd_Tran_Code", "#TRAN-CODE", FieldType.STRING, 3);
        pnd_Omni_Detail_Pnd_Seq_Code = pnd_Omni_Detail__R_Field_30.newFieldInGroup("pnd_Omni_Detail_Pnd_Seq_Code", "#SEQ-CODE", FieldType.STRING, 2);
        pnd_Omni_Detail_Pnd_Notused1 = pnd_Omni_Detail__R_Field_30.newFieldInGroup("pnd_Omni_Detail_Pnd_Notused1", "#NOTUSED1", FieldType.STRING, 3);
        pnd_Omni_Detail_Pnd_Data_Elem = pnd_Omni_Detail__R_Field_30.newFieldInGroup("pnd_Omni_Detail_Pnd_Data_Elem", "#DATA-ELEM", FieldType.STRING, 3);
        pnd_Omni_Detail_Pnd_De_Value = pnd_Omni_Detail__R_Field_30.newFieldInGroup("pnd_Omni_Detail_Pnd_De_Value", "#DE-VALUE", FieldType.STRING, 40);
        pnd_Compare_Key = localVariables.newFieldInRecord("pnd_Compare_Key", "#COMPARE-KEY", FieldType.STRING, 23);

        pnd_Compare_Key__R_Field_31 = localVariables.newGroupInRecord("pnd_Compare_Key__R_Field_31", "REDEFINE", pnd_Compare_Key);
        pnd_Compare_Key_Pnd_Comp_Plan_No = pnd_Compare_Key__R_Field_31.newFieldInGroup("pnd_Compare_Key_Pnd_Comp_Plan_No", "#COMP-PLAN-NO", FieldType.STRING, 
            6);
        pnd_Compare_Key_Pnd_Comp_Part_Id = pnd_Compare_Key__R_Field_31.newFieldInGroup("pnd_Compare_Key_Pnd_Comp_Part_Id", "#COMP-PART-ID", FieldType.STRING, 
            17);

        pnd_Compare_Key__R_Field_32 = pnd_Compare_Key__R_Field_31.newGroupInGroup("pnd_Compare_Key__R_Field_32", "REDEFINE", pnd_Compare_Key_Pnd_Comp_Part_Id);
        pnd_Compare_Key_Pnd_Comp_Ssn = pnd_Compare_Key__R_Field_32.newFieldInGroup("pnd_Compare_Key_Pnd_Comp_Ssn", "#COMP-SSN", FieldType.STRING, 9);
        pnd_Compare_Key_Pnd_Comp_Subplan_No = pnd_Compare_Key__R_Field_32.newFieldInGroup("pnd_Compare_Key_Pnd_Comp_Subplan_No", "#COMP-SUBPLAN-NO", FieldType.STRING, 
            6);
        pnd_Compare_Key_Pnd_Comp_Part_Ext = pnd_Compare_Key__R_Field_32.newFieldInGroup("pnd_Compare_Key_Pnd_Comp_Part_Ext", "#COMP-PART-EXT", FieldType.STRING, 
            2);

        pnd_Accepted_Report_Rec = localVariables.newGroupInRecord("pnd_Accepted_Report_Rec", "#ACCEPTED-REPORT-REC");
        pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No", "#REPT-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Accepted_Report_Rec_Pnd_Rept_Divsub = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Divsub", "#REPT-DIVSUB", FieldType.STRING, 
            4);
        pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract", "#REPT-TIAA-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract", "#REPT-CREF-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Accepted_Report_Rec_Pnd_Rept_Ssn = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Ssn", "#REPT-SSN", FieldType.STRING, 
            9);
        pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_No = pnd_Accepted_Report_Rec.newFieldInGroup("pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_No", "#REPT-SUBPLAN-NO", 
            FieldType.STRING, 6);

        pnd_Rejected_Report_Rec = localVariables.newGroupInRecord("pnd_Rejected_Report_Rec", "#REJECTED-REPORT-REC");
        pnd_Rejected_Report_Rec_Pnd_Error_Plan_No = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Plan_No", "#ERROR-PLAN-NO", 
            FieldType.STRING, 6);
        pnd_Rejected_Report_Rec_Pnd_Error_Divsub = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Divsub", "#ERROR-DIVSUB", 
            FieldType.STRING, 4);
        pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract", "#ERROR-TIAA-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract", "#ERROR-CREF-CONTRACT", 
            FieldType.STRING, 10);
        pnd_Rejected_Report_Rec_Pnd_Error_Ssn = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Ssn", "#ERROR-SSN", FieldType.STRING, 
            9);
        pnd_Rejected_Report_Rec_Pnd_Error_Subplan_No = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Subplan_No", "#ERROR-SUBPLAN-NO", 
            FieldType.STRING, 6);
        pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg = pnd_Rejected_Report_Rec.newFieldInGroup("pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg", "#ERROR-RETURN-MSG", 
            FieldType.STRING, 50);

        pnd_Summary_Report_Rec = localVariables.newGroupInRecord("pnd_Summary_Report_Rec", "#SUMMARY-REPORT-REC");
        pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt", "#TOT-TRANS-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt", "#TOT-ACCEPT-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt = pnd_Summary_Report_Rec.newFieldInGroup("pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt", "#TOT-REJECT-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Restarted = localVariables.newFieldInRecord("pnd_Restarted", "#RESTARTED", FieldType.BOOLEAN, 1);
        pnd_Skip_Restart_Record = localVariables.newFieldInRecord("pnd_Skip_Restart_Record", "#SKIP-RESTART-RECORD", FieldType.BOOLEAN, 1);
        pnd_Hold_Part_Id = localVariables.newFieldInRecord("pnd_Hold_Part_Id", "#HOLD-PART-ID", FieldType.STRING, 17);
        pnd_Address_Standardized = localVariables.newFieldInRecord("pnd_Address_Standardized", "#ADDRESS-STANDARDIZED", FieldType.BOOLEAN, 1);
        pnd_Record_Cnt = localVariables.newFieldInRecord("pnd_Record_Cnt", "#RECORD-CNT", FieldType.NUMERIC, 7);
        pnd_Error_Rec_Cnt = localVariables.newFieldInRecord("pnd_Error_Rec_Cnt", "#ERROR-REC-CNT", FieldType.NUMERIC, 7);
        pnd_Max_Cards = localVariables.newFieldInRecord("pnd_Max_Cards", "#MAX-CARDS", FieldType.NUMERIC, 2);
        pnd_Card_Cnt = localVariables.newFieldInRecord("pnd_Card_Cnt", "#CARD-CNT", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Retry_Cnt = localVariables.newFieldInRecord("pnd_Retry_Cnt", "#RETRY-CNT", FieldType.NUMERIC, 3);
        intvl = localVariables.newFieldInRecord("intvl", "INTVL", FieldType.INTEGER, 1);
        reqid = localVariables.newFieldInRecord("reqid", "REQID", FieldType.STRING, 8);
        pnd_Jobname = localVariables.newFieldInRecord("pnd_Jobname", "#JOBNAME", FieldType.STRING, 10);
        pnd_Tiaa_Contract = localVariables.newFieldInRecord("pnd_Tiaa_Contract", "#TIAA-CONTRACT", FieldType.STRING, 10);
        pnd_Tpa_Part_Id = localVariables.newFieldInRecord("pnd_Tpa_Part_Id", "#TPA-PART-ID", FieldType.STRING, 17);

        pnd_Tpa_Part_Id__R_Field_33 = localVariables.newGroupInRecord("pnd_Tpa_Part_Id__R_Field_33", "REDEFINE", pnd_Tpa_Part_Id);
        pnd_Tpa_Part_Id_Pnd_Tpa_Part_Ssn = pnd_Tpa_Part_Id__R_Field_33.newFieldInGroup("pnd_Tpa_Part_Id_Pnd_Tpa_Part_Ssn", "#TPA-PART-SSN", FieldType.STRING, 
            9);
        pnd_Tpa_Part_Id_Pnd_Tpa_Part_Subplan = pnd_Tpa_Part_Id__R_Field_33.newFieldInGroup("pnd_Tpa_Part_Id_Pnd_Tpa_Part_Subplan", "#TPA-PART-SUBPLAN", 
            FieldType.STRING, 6);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cis_Prtcpnt_File.reset();
        vw_batch_Restart.reset();

        localVariables.reset();
        pnd_Restarted.setInitialValue(false);
        pnd_Skip_Restart_Record.setInitialValue(false);
        pnd_Max_Cards.setInitialValue(14);
        intvl.setInitialValue(1);
        reqid.setInitialValue("MYREQID");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Scib9002() throws Exception
    {
        super("Scib9002");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SCIB9002", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*  OVERRIDES NATURAL ERROR ROUTINE
        //*  THIS REPLACES *STARTUP LOGIC.
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 60;//Natural: FORMAT ( 2 ) LS = 133 PS = 60;//Natural: FORMAT ( 3 ) LS = 133 PS = 60
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        pls_Dar_Program.setValue(Global.getPROGRAM());                                                                                                                    //Natural: ASSIGN +DAR-PROGRAM := *PROGRAM
        //*  CAUSES A TERMINATION IN THE EVENT OF
        if (condition(pls_Error_Occurence.getBoolean()))                                                                                                                  //Natural: IF +ERROR-OCCURENCE
        {
            //*  AN ERROR. NEEDED TO FORCE OPERATOR RESTART.
            DbsUtil.terminate(5);  if (true) return;                                                                                                                      //Natural: TERMINATE 5
        }                                                                                                                                                                 //Natural: END-IF
        //*  PERFORM CHECK-FOR-RESTART
        Global.getERROR_TA().setValue(" ");                                                                                                                               //Natural: ASSIGN *ERROR-TA := ' '
        //* *======================================================================
        //* *    M A I N    P R O C E S S
        //* *======================================================================
        pnd_Omni_Detail.reset();                                                                                                                                          //Natural: RESET #OMNI-DETAIL #COMPARE-KEY
        pnd_Compare_Key.reset();
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            //*  READ WORK FILE 1 ONCE LTFP0740(*)
            //*  BIP
            getWorkFiles().read(1, pnd_Baseext_Rec.getValue("*"));                                                                                                        //Natural: READ WORK FILE 1 ONCE #BASEEXT-REC ( * )
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
                sub_Write_Summary_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-ENDFILE
            //*  IF #P-SIP-LETTER-TYPE NE 'SPAP'                          /* BIP
            //*  BIP
            if (condition(! (pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPAP") || pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPBP") || pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("TPBP")))) //Natural: IF NOT ( #BASEEXT-RECTYPE = 'SPAP' OR = 'SPBP' OR = 'TPBP' )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  BIP
            if (condition(pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPAP") || pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPBP")))                                      //Natural: IF #BASEEXT-RECTYPE = 'SPAP' OR = 'SPBP'
            {
                //*  BIP
                ltfp0740.getValue(1,":",3242).setValue(pnd_Baseext_Rec.getValue(1,":",3242));                                                                             //Natural: MOVE #BASEEXT-REC ( 1:3242 ) TO LTFP0740 ( 1:3242 )
                //*  TPBP                                           /* BIP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  PINEXP
                pnd_Input_Part_Aa.getValue(1,":",1266).setValue(pnd_Baseext_Rec.getValue(1,":",1266));                                                                    //Natural: MOVE #BASEEXT-REC ( 1:1266 ) TO #INPUT-PART-AA ( 1:1266 )
                //*  BIP
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 2 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 3 )
                                                                                                                                                                          //Natural: PERFORM GET-CIS-PRTCPNT
            sub_Get_Cis_Prtcpnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(! (pnd_Tiaa_Cntrct_Found.getBoolean())))                                                                                                        //Natural: IF NOT #TIAA-CNTRCT-FOUND
            {
                pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg.setValue("No PIN found on CIS");                                                                             //Natural: ASSIGN #ERROR-RETURN-MSG := 'No PIN found on CIS'
                pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #TOT-REJECT-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REC
                sub_Write_Error_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Restarted.getBoolean()))                                                                                                                    //Natural: IF #RESTARTED
            {
                //*  BIP
                if (condition(pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPAP") || pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPBP")))                                  //Natural: IF #BASEEXT-RECTYPE = 'SPAP' OR = 'SPBP'
                {
                    if (condition(ltfp0740_Pnd_P_Omni_Sub_Plan_Id.equals(pnd_Restart_Key_Pnd_Restart_Part_Id)))                                                           //Natural: IF #P-OMNI-SUB-PLAN-ID = #RESTART-PART-ID
                    {
                        pnd_Skip_Restart_Record.setValue(true);                                                                                                           //Natural: ASSIGN #SKIP-RESTART-RECORD := TRUE
                        pnd_Restarted.reset();                                                                                                                            //Natural: RESET #RESTARTED
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  TPBP                                          /* BIP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  BIP
                    pnd_Tpa_Part_Id_Pnd_Tpa_Part_Ssn.setValue(pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn);                                                                   //Natural: ASSIGN #TPA-PART-SSN = #CIS-FRST-ANNT-SSN
                    //*  BIP
                    pnd_Tpa_Part_Id_Pnd_Tpa_Part_Subplan.setValue(pnd_Input_Part_Aa_Pnd_Sub_Plan);                                                                        //Natural: ASSIGN #TPA-PART-SUBPLAN = #SUB-PLAN
                    //*  BIP
                    //*  BIP
                    if (condition(pnd_Tpa_Part_Id.equals(pnd_Restart_Key_Pnd_Restart_Part_Id)))                                                                           //Natural: IF #TPA-PART-ID = #RESTART-PART-ID
                    {
                        pnd_Skip_Restart_Record.setValue(true);                                                                                                           //Natural: ASSIGN #SKIP-RESTART-RECORD := TRUE
                        //*  BIP
                        pnd_Restarted.reset();                                                                                                                            //Natural: RESET #RESTARTED
                        //*  BIP
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  BIP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  BIP
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  BIP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BIP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Compare_Key_Pnd_Comp_Part_Id.reset();                                                                                                                     //Natural: RESET #COMP-PART-ID
            //*  BIP
            if (condition(pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPAP") || pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPBP")))                                      //Natural: IF #BASEEXT-RECTYPE = 'SPAP' OR = 'SPBP'
            {
                pnd_Compare_Key_Pnd_Comp_Plan_No.setValue(ltfp0740_Pnd_P_Omni_Plan_Id);                                                                                   //Natural: ASSIGN #COMP-PLAN-NO = LTFP0740.#P-OMNI-PLAN-ID
                pnd_Compare_Key_Pnd_Comp_Subplan_No.setValue(ltfp0740_Pnd_P_Omni_Sub_Plan);                                                                               //Natural: ASSIGN #COMP-SUBPLAN-NO = #P-OMNI-SUB-PLAN
                pnd_Compare_Key_Pnd_Comp_Ssn.setValue(ltfp0740_Pnd_P_Omni_Ssn);                                                                                           //Natural: ASSIGN #COMP-SSN = #P-OMNI-SSN
                //*  BIP - TPBP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  BIP
                pnd_Compare_Key_Pnd_Comp_Plan_No.setValue(pnd_Input_Part_Aa_Pnd_Plan_Id);                                                                                 //Natural: ASSIGN #COMP-PLAN-NO = #PLAN-ID
                //*  BIP
                pnd_Compare_Key_Pnd_Comp_Subplan_No.setValue(pnd_Input_Part_Aa_Pnd_Sub_Plan);                                                                             //Natural: ASSIGN #COMP-SUBPLAN-NO = #SUB-PLAN
                //*  BIP
                pnd_Compare_Key_Pnd_Comp_Ssn.setValue(pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn);                                                                           //Natural: ASSIGN #COMP-SSN = #CIS-FRST-ANNT-SSN
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-T813-HEADER
            sub_Write_T813_Header();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CREATE-T813-TRAILERS
            sub_Create_T813_Trailers();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-REC
            sub_Write_Report_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  NO INPUT RECORDS TO PROCESS
        if (condition(pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt.equals(getZero())))                                                                                        //Natural: IF #TOT-TRANS-CNT = 0
        {
            getReports().display(0, "***** INTERFACE FILE IS EMPTY *****");                                                                                               //Natural: DISPLAY '***** INTERFACE FILE IS EMPTY *****'
            if (Global.isEscape()) return;
            DbsUtil.terminate(1);  if (true) return;                                                                                                                      //Natural: TERMINATE 1
        }                                                                                                                                                                 //Natural: END-IF
        //* *======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-T813-HEADER
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-T813-TRAILERS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CIS-PRTCPNT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-RESTART
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DELETE-RESTART-RECORD
        //*  RETRY ERROR LOGIC
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Write_T813_Header() throws Exception                                                                                                                 //Natural: WRITE-T813-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *======================================================================
        pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TOT-TRANS-CNT
        pnd_Omni_Header.reset();                                                                                                                                          //Natural: RESET #OMNI-HEADER #HOLD-PART-ID #CARD-CNT
        pnd_Hold_Part_Id.reset();
        pnd_Card_Cnt.reset();
        pnd_Omni_Header_Pnd_Tran_Code.setValue("813");                                                                                                                    //Natural: ASSIGN #OMNI-HEADER.#TRAN-CODE = '813'
        pnd_Omni_Header_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-HEADER.#SEQ-CODE
        pnd_Omni_Header_Pnd_Rec_Type.setValue("$TIHDR");                                                                                                                  //Natural: ASSIGN #OMNI-HEADER.#REC-TYPE = '$TIHDR'
        //*  ASSIGN #OMNI-HEADER.#P-OMNI-PLAN-ID  = LTFP0740.#P-OMNI-PLAN-ID /* BIP
        //*  BIP
        if (condition(pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPAP") || pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPBP")))                                          //Natural: IF #BASEEXT-RECTYPE = 'SPAP' OR = 'SPBP'
        {
            pnd_Omni_Header_Pnd_P_Omni_Plan_Id.setValue(ltfp0740_Pnd_P_Omni_Plan_Id);                                                                                     //Natural: ASSIGN #OMNI-HEADER.#P-OMNI-PLAN-ID = LTFP0740.#P-OMNI-PLAN-ID
            pnd_Omni_Header_Pnd_Part_Id.setValue(ltfp0740_Pnd_P_Omni_Sub_Plan_Id);                                                                                        //Natural: ASSIGN #OMNI-HEADER.#PART-ID = LTFP0740.#P-OMNI-SUB-PLAN-ID
            pnd_Omni_Header_Pnd_Trade_Date_A.setValue(ltfp0740_Pnd_P_Annty_Strt_Dte);                                                                                     //Natural: ASSIGN #OMNI-HEADER.#TRADE-DATE-A = LTFP0740.#P-ANNTY-STRT-DTE
            //*  TPBP                                                      /* BIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  BIP
            pnd_Omni_Header_Pnd_P_Omni_Plan_Id.setValue(pnd_Input_Part_Aa_Pnd_Plan_Id);                                                                                   //Natural: ASSIGN #OMNI-HEADER.#P-OMNI-PLAN-ID = #PLAN-ID
            //*  BIP
            pnd_Tpa_Part_Id_Pnd_Tpa_Part_Ssn.setValue(pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn);                                                                           //Natural: ASSIGN #TPA-PART-SSN = #CIS-FRST-ANNT-SSN
            //*  BIP
            pnd_Tpa_Part_Id_Pnd_Tpa_Part_Subplan.setValue(pnd_Input_Part_Aa_Pnd_Sub_Plan);                                                                                //Natural: ASSIGN #TPA-PART-SUBPLAN = #SUB-PLAN
            //*  BIP
            pnd_Omni_Header_Pnd_Part_Id.setValue(pnd_Tpa_Part_Id);                                                                                                        //Natural: ASSIGN #OMNI-HEADER.#PART-ID = #TPA-PART-ID
            //*  BIP
            pnd_Omni_Header_Pnd_Trade_Date_A.setValue(pnd_Input_Part_Aa_Pnd_Cis_Annty_Start_Dte);                                                                         //Natural: ASSIGN #OMNI-HEADER.#TRADE-DATE-A = #CIS-ANNTY-START-DTE
            //*  BIP
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(2, false, pnd_Omni_Header);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-HEADER
        //*  WRITE-T813-HEADER
    }
    private void sub_Create_T813_Trailers() throws Exception                                                                                                              //Natural: CREATE-T813-TRAILERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Omni_Detail.reset();                                                                                                                                          //Natural: RESET #OMNI-DETAIL
        pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOT-ACCEPT-CNT
        pnd_Omni_Detail_Pnd_Tran_Code.setValue("813");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#TRAN-CODE := '813'
        pnd_Card_Cnt.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #CARD-CNT
        pnd_Omni_Detail_Pnd_Seq_Code.setValueEdited(pnd_Card_Cnt,new ReportEditMask("99"));                                                                               //Natural: MOVE EDITED #CARD-CNT ( EM = 99 ) TO #OMNI-DETAIL.#SEQ-CODE
        pnd_Omni_Detail_Pnd_De_Value.setValue(cis_Prtcpnt_File_Cis_Pin_Nbr);                                                                                              //Natural: ASSIGN #OMNI-DETAIL.#DE-VALUE := CIS-PIN-NBR
        //*  PH301
        pnd_Omni_Detail_Pnd_Data_Elem.setValue("301");                                                                                                                    //Natural: ASSIGN #OMNI-DETAIL.#DATA-ELEM := '301'
        getWorkFiles().write(2, false, pnd_Omni_Detail);                                                                                                                  //Natural: WRITE WORK FILE 2 #OMNI-DETAIL
        //*  CREATE-T813-TRAILERS
    }
    private void sub_Write_Report_Rec() throws Exception                                                                                                                  //Natural: WRITE-REPORT-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Accepted_Report_Rec.reset();                                                                                                                                  //Natural: RESET #ACCEPTED-REPORT-REC
        //*  BIP
        if (condition(pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPAP") || pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPBP")))                                          //Natural: IF #BASEEXT-RECTYPE = 'SPAP' OR = 'SPBP'
        {
            pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No.setValue(ltfp0740_Pnd_P_Omni_Plan_Id);                                                                               //Natural: ASSIGN #REPT-PLAN-NO := LTFP0740.#P-OMNI-PLAN-ID
            pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_No.setValue(ltfp0740_Pnd_P_Omni_Sub_Plan);                                                                           //Natural: ASSIGN #REPT-SUBPLAN-NO := #P-OMNI-SUB-PLAN
            pnd_Accepted_Report_Rec_Pnd_Rept_Ssn.setValue(ltfp0740_Pnd_P_Omni_Ssn);                                                                                       //Natural: ASSIGN #REPT-SSN := #P-OMNI-SSN
            pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract.setValue(ltfp0740_Pnd_P_Sip_Tiaa_Contract);                                                                    //Natural: ASSIGN #REPT-TIAA-CONTRACT := #P-SIP-TIAA-CONTRACT
            pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract.setValue(ltfp0740_Pnd_P_Sip_Cref_Cert);                                                                        //Natural: ASSIGN #REPT-CREF-CONTRACT := #P-SIP-CREF-CERT
            //*  BIP - TBPB
            //*  BIP
            //*  BIP
            //*  BIP
            //*  BIP
            //*  BIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No.setValue(pnd_Input_Part_Aa_Pnd_Plan_Id);                                                                             //Natural: ASSIGN #REPT-PLAN-NO := #PLAN-ID
            pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_No.setValue(pnd_Input_Part_Aa_Pnd_Sub_Plan);                                                                         //Natural: ASSIGN #REPT-SUBPLAN-NO := #SUB-PLAN
            pnd_Accepted_Report_Rec_Pnd_Rept_Ssn.setValue(pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn);                                                                       //Natural: ASSIGN #REPT-SSN := #CIS-FRST-ANNT-SSN
            pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract.setValue(pnd_Input_Part_Aa_Pnd_Cis_Tpa_Tiaa_Cntrct);                                                           //Natural: ASSIGN #REPT-TIAA-CONTRACT := #CIS-TPA-TIAA-CNTRCT
            pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract.setValue(pnd_Input_Part_Aa_Pnd_Cis_Tpa_Cref_Cntrct);                                                           //Natural: ASSIGN #REPT-CREF-CONTRACT := #CIS-TPA-CREF-CNTRCT
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,"Plan Number:",pnd_Accepted_Report_Rec_Pnd_Rept_Plan_No,"SubPlan:",pnd_Accepted_Report_Rec_Pnd_Rept_Subplan_No,NEWLINE,"SSN:",pnd_Accepted_Report_Rec_Pnd_Rept_Ssn,  //Natural: WRITE ( 1 ) NOTITLE 'Plan Number:' #REPT-PLAN-NO 'SubPlan:' #REPT-SUBPLAN-NO / 'SSN:' #REPT-SSN ( EM = XXX-XX-XXXX ) / 'TIAA Contract:' #REPT-TIAA-CONTRACT 'CREF Contract:' #REPT-CREF-CONTRACT / '-----------------------------------------------' /
            new ReportEditMask ("XXX-XX-XXXX"),NEWLINE,"TIAA Contract:",pnd_Accepted_Report_Rec_Pnd_Rept_Tiaa_Contract,"CREF Contract:",pnd_Accepted_Report_Rec_Pnd_Rept_Cref_Contract,
            NEWLINE,"-----------------------------------------------",NEWLINE);
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(1).less(10)))                                                                                                          //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 10 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        //*  WRITE-REPORT-REC
    }
    private void sub_Write_Error_Rec() throws Exception                                                                                                                   //Natural: WRITE-ERROR-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Error_Rec_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ERROR-REC-CNT
        //*  RESTART RECORD WILL NOT BE PROCESSED
        if (condition(pnd_Skip_Restart_Record.getBoolean()))                                                                                                              //Natural: IF #SKIP-RESTART-RECORD
        {
            pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg.setValue("A PROCESSING ERROR HAS OCCURRED - RESTART RECORD");                                                    //Natural: MOVE 'A PROCESSING ERROR HAS OCCURRED - RESTART RECORD' TO #ERROR-RETURN-MSG
        }                                                                                                                                                                 //Natural: END-IF
        //*  BIP
        if (condition(pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPAP") || pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPBP")))                                          //Natural: IF #BASEEXT-RECTYPE = 'SPAP' OR = 'SPBP'
        {
            pnd_Rejected_Report_Rec_Pnd_Error_Plan_No.setValue(ltfp0740_Pnd_P_Omni_Plan_Id);                                                                              //Natural: ASSIGN #ERROR-PLAN-NO := LTFP0740.#P-OMNI-PLAN-ID
            pnd_Rejected_Report_Rec_Pnd_Error_Subplan_No.setValue(ltfp0740_Pnd_P_Omni_Sub_Plan);                                                                          //Natural: ASSIGN #ERROR-SUBPLAN-NO := #P-OMNI-SUB-PLAN
            pnd_Rejected_Report_Rec_Pnd_Error_Ssn.setValue(ltfp0740_Pnd_P_Omni_Ssn);                                                                                      //Natural: ASSIGN #ERROR-SSN := #P-OMNI-SSN
            pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract.setValue(ltfp0740_Pnd_P_Sip_Tiaa_Contract);                                                                   //Natural: ASSIGN #ERROR-TIAA-CONTRACT := #P-SIP-TIAA-CONTRACT
            pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract.setValue(ltfp0740_Pnd_P_Sip_Cref_Cert);                                                                       //Natural: ASSIGN #ERROR-CREF-CONTRACT := #P-SIP-CREF-CERT
            //*  BIP
            //*  BIP
            //*  BIP
            //*  BIP
            //*  BIP
            //*  BIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rejected_Report_Rec_Pnd_Error_Plan_No.setValue(pnd_Input_Part_Aa_Pnd_Plan_Id);                                                                            //Natural: ASSIGN #ERROR-PLAN-NO := #PLAN-ID
            pnd_Rejected_Report_Rec_Pnd_Error_Subplan_No.setValue(pnd_Input_Part_Aa_Pnd_Sub_Plan);                                                                        //Natural: ASSIGN #ERROR-SUBPLAN-NO := #SUB-PLAN
            pnd_Rejected_Report_Rec_Pnd_Error_Ssn.setValue(pnd_Input_Part_Aa_Pnd_Cis_Frst_Annt_Ssn);                                                                      //Natural: ASSIGN #ERROR-SSN := #CIS-FRST-ANNT-SSN
            pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract.setValue(pnd_Input_Part_Aa_Pnd_Cis_Tpa_Tiaa_Cntrct);                                                          //Natural: ASSIGN #ERROR-TIAA-CONTRACT := #CIS-TPA-TIAA-CNTRCT
            pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract.setValue(pnd_Input_Part_Aa_Pnd_Cis_Tpa_Cref_Cntrct);                                                          //Natural: ASSIGN #ERROR-CREF-CONTRACT := #CIS-TPA-CREF-CNTRCT
            //*  BIP
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,"Plan Number:",pnd_Rejected_Report_Rec_Pnd_Error_Plan_No,"SubPlan:",pnd_Rejected_Report_Rec_Pnd_Error_Subplan_No,NEWLINE,"Div/Sub:",pnd_Rejected_Report_Rec_Pnd_Error_Divsub,NEWLINE,"SSN:",pnd_Rejected_Report_Rec_Pnd_Error_Ssn,  //Natural: WRITE ( 2 ) NOTITLE 'Plan Number:' #ERROR-PLAN-NO 'SubPlan:' #ERROR-SUBPLAN-NO / 'Div/Sub:' #ERROR-DIVSUB / 'SSN:' #ERROR-SSN ( EM = XXX-XX-XXXX ) / 'TIAA Contract:' #ERROR-TIAA-CONTRACT 'CREF Contract:' #ERROR-CREF-CONTRACT / 'Error Message' #ERROR-RETURN-MSG // '-----------------------------------------------' /
            new ReportEditMask ("XXX-XX-XXXX"),NEWLINE,"TIAA Contract:",pnd_Rejected_Report_Rec_Pnd_Error_Tiaa_Contract,"CREF Contract:",pnd_Rejected_Report_Rec_Pnd_Error_Cref_Contract,
            NEWLINE,"Error Message",pnd_Rejected_Report_Rec_Pnd_Error_Return_Msg,NEWLINE,NEWLINE,"-----------------------------------------------",NEWLINE);
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(2).less(10)))                                                                                                          //Natural: NEWPAGE ( 2 ) WHEN LESS THAN 10 LINES LEFT
        {
            getReports().newPage(2);
            if (condition(Global.isEscape())){return;}
        }
        //*  WRITE-ERROR-REC
    }
    private void sub_Write_Summary_Report() throws Exception                                                                                                              //Natural: WRITE-SUMMARY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(3, ReportOption.NOTITLE,"Total Records Read    :",pnd_Summary_Report_Rec_Pnd_Tot_Trans_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Records Written :",pnd_Summary_Report_Rec_Pnd_Tot_Accept_Cnt,  //Natural: WRITE ( 3 ) NOTITLE 'Total Records Read    :' #TOT-TRANS-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total Records Written :' #TOT-ACCEPT-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'Total Records Rejected:' #TOT-REJECT-CNT ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"Total Records Rejected:",pnd_Summary_Report_Rec_Pnd_Tot_Reject_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  WRITE-SUMMARY-REPORT
    }
    private void sub_Get_Cis_Prtcpnt() throws Exception                                                                                                                   //Natural: GET-CIS-PRTCPNT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  BIP
        if (condition(pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPAP") || pnd_Baseext_Rec_Pnd_Baseext_Rectype.equals("SPBP")))                                          //Natural: IF #BASEEXT-RECTYPE = 'SPAP' OR = 'SPBP'
        {
            //*  BIP
            getReports().write(0, "GET PARTICIPANT",ltfp0740_Pnd_P_Sip_Tiaa_Contract);                                                                                    //Natural: WRITE 'GET PARTICIPANT' #P-SIP-TIAA-CONTRACT
            if (Global.isEscape()) return;
            pnd_Tiaa_Contract.setValue(ltfp0740_Pnd_P_Sip_Tiaa_Contract);                                                                                                 //Natural: ASSIGN #TIAA-CONTRACT := #P-SIP-TIAA-CONTRACT
            //*  BIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  BIP
            //*  BIP
            getReports().write(0, "GET PARTICIPANT",pnd_Input_Part_Aa_Pnd_Cis_Tpa_Tiaa_Cntrct);                                                                           //Natural: WRITE 'GET PARTICIPANT' #CIS-TPA-TIAA-CNTRCT
            if (Global.isEscape()) return;
            pnd_Tiaa_Contract.setValue(pnd_Input_Part_Aa_Pnd_Cis_Tpa_Tiaa_Cntrct);                                                                                        //Natural: ASSIGN #TIAA-CONTRACT := #CIS-TPA-TIAA-CNTRCT
            //*  BIP
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tiaa_Cntrct_Found.setValue(false);                                                                                                                            //Natural: ASSIGN #TIAA-CNTRCT-FOUND := FALSE
        //*  READ CIS-PRTCPNT-FILE BY CIS-TIAA-NBR EQ #P-SIP-TIAA-CONTRACT
        vw_cis_Prtcpnt_File.startDatabaseRead                                                                                                                             //Natural: READ CIS-PRTCPNT-FILE BY CIS-TIAA-NBR EQ #TIAA-CONTRACT
        (
        "READ1",
        new Wc[] { new Wc("CIS_TIAA_NBR", ">=", pnd_Tiaa_Contract, WcType.BY) },
        new Oc[] { new Oc("CIS_TIAA_NBR", "ASC") }
        );
        READ1:
        while (condition(vw_cis_Prtcpnt_File.readNextRow("READ1")))
        {
            //*   IF CIS-TIAA-NBR NE #P-SIP-TIAA-CONTRACT
            //*  BIP
            //*  BIP
            if (condition(cis_Prtcpnt_File_Cis_Tiaa_Nbr.notEquals(pnd_Tiaa_Contract)))                                                                                    //Natural: IF CIS-TIAA-NBR NE #TIAA-CONTRACT
            {
                pnd_Tiaa_Cntrct_Found.setValue(false);                                                                                                                    //Natural: ASSIGN #TIAA-CNTRCT-FOUND := FALSE
                if (true) break READ1;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ1. )
            }                                                                                                                                                             //Natural: END-IF
            //*   IF CIS-TIAA-NBR EQ #P-SIP-TIAA-CONTRACT    /* BIP
            //*  BIP
            if (condition(cis_Prtcpnt_File_Cis_Tiaa_Nbr.equals(pnd_Tiaa_Contract)))                                                                                       //Natural: IF CIS-TIAA-NBR EQ #TIAA-CONTRACT
            {
                pnd_Tiaa_Cntrct_Found.setValue(true);                                                                                                                     //Natural: ASSIGN #TIAA-CNTRCT-FOUND := TRUE
                if (true) break READ1;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Tiaa_Cntrct_Found);                                                                                                                 //Natural: WRITE '=' #TIAA-CNTRCT-FOUND
        if (Global.isEscape()) return;
    }
    private void sub_Check_For_Restart() throws Exception                                                                                                                 //Natural: CHECK-FOR-RESTART
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  SP-ACTVE-JOB-STEPIS UNIQUE IDENTIFIER FOR A GIVEN BATCH EXECUTION
        //*   *INIT-USER CONTAINS THE JOB NAME IN BATCH MODE AND
        //*   *INIT-ID   CONTAINS THE STEP NAME
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Actve_Ind.setValue("A");                                                                                                            //Natural: ASSIGN #RST-ACTVE-IND = 'A'
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Job_Nme.setValue(Global.getINIT_USER());                                                                                            //Natural: ASSIGN #RST-JOB-NME = *INIT-USER
        pnd_Sp_Actve_Job_Step_Pnd_Rst_Step_Nme.setValue(Global.getINIT_ID());                                                                                             //Natural: ASSIGN #RST-STEP-NME = *INIT-ID
        vw_batch_Restart.startDatabaseFind                                                                                                                                //Natural: FIND ( 1 ) BATCH-RESTART WITH SP-ACTVE-JOB-STEP = #SP-ACTVE-JOB-STEP
        (
        "FIND_R",
        new Wc[] { new Wc("SP_ACTVE_JOB_STEP", "=", pnd_Sp_Actve_Job_Step, WcType.WITH) },
        1
        );
        FIND_R:
        while (condition(vw_batch_Restart.readNextRow("FIND_R", true)))
        {
            vw_batch_Restart.setIfNotFoundControlFlag(false);
            //*  CREATE A RECORD IF NONE EXISTS.
            if (condition(vw_batch_Restart.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS FOUND
            {
                batch_Restart_Rst_Actve_Ind.setValue("A");                                                                                                                //Natural: ASSIGN BATCH-RESTART.RST-ACTVE-IND := 'A'
                batch_Restart_Rst_Job_Nme.setValue(Global.getINIT_USER());                                                                                                //Natural: ASSIGN BATCH-RESTART.RST-JOB-NME := *INIT-USER
                batch_Restart_Rst_Jobstep_Nme.setValue(Global.getINIT_ID());                                                                                              //Natural: ASSIGN BATCH-RESTART.RST-JOBSTEP-NME := *INIT-ID
                batch_Restart_Rst_Pgm_Nme.setValue(Global.getPROGRAM());                                                                                                  //Natural: ASSIGN BATCH-RESTART.RST-PGM-NME := *PROGRAM
                batch_Restart_Rst_Curr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BATCH-RESTART.RST-CURR-DTE
                batch_Restart_Rst_Curr_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                                               //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO BATCH-RESTART.RST-CURR-TME
                batch_Restart_Rst_Start_Dte.setValue(batch_Restart_Rst_Curr_Dte);                                                                                         //Natural: ASSIGN BATCH-RESTART.RST-START-DTE := BATCH-RESTART.RST-CURR-DTE
                batch_Restart_Rst_Start_Tme.setValue(batch_Restart_Rst_Curr_Tme);                                                                                         //Natural: ASSIGN BATCH-RESTART.RST-START-TME := BATCH-RESTART.RST-CURR-TME
                batch_Restart_Rst_Frst_Run_Dte.setValue(batch_Restart_Rst_Start_Dte);                                                                                     //Natural: ASSIGN BATCH-RESTART.RST-FRST-RUN-DTE := BATCH-RESTART.RST-START-DTE
                pnd_Restart_Key_Pnd_Restart_Plan_No.setValue(" ");                                                                                                        //Natural: ASSIGN #RESTART-KEY.#RESTART-PLAN-NO := ' '
                pnd_Restart_Key_Pnd_Restart_Subplan_No.setValue(" ");                                                                                                     //Natural: ASSIGN #RESTART-KEY.#RESTART-SUBPLAN-NO := ' '
                pnd_Restart_Key_Pnd_Restart_Ssn.setValue(" ");                                                                                                            //Natural: ASSIGN #RESTART-KEY.#RESTART-SSN := ' '
                batch_Restart_Rst_Key.setValue(pnd_Restart_Key);                                                                                                          //Natural: ASSIGN BATCH-RESTART.RST-KEY := #RESTART-KEY
                STORE_REST:                                                                                                                                               //Natural: STORE BATCH-RESTART
                vw_batch_Restart.insertDBRow("STORE_REST");
                pnd_Restart_Isn.setValue(vw_batch_Restart.getAstISN("FIND_R"));                                                                                           //Natural: ASSIGN #RESTART-ISN := *ISN ( STORE-REST. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                getReports().write(0, "********************************************************");                                                                        //Natural: WRITE '********************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "***NOT FOUND***BATCH RESTART RECORD ADDED***NOT FOUND***");                                                                        //Natural: WRITE '***NOT FOUND***BATCH RESTART RECORD ADDED***NOT FOUND***'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "********************************************************");                                                                        //Natural: WRITE '********************************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, NEWLINE,NEWLINE);                                                                                                                   //Natural: WRITE //
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_R"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break FIND_R;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FIND-R. )
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Restart_Isn.setValue(vw_batch_Restart.getAstISN("FIND_R"));                                                                                               //Natural: ASSIGN #RESTART-ISN := *ISN ( FIND-R. )
            pnd_Restart_Key.setValue(batch_Restart_Rst_Key);                                                                                                              //Natural: ASSIGN #RESTART-KEY := BATCH-RESTART.RST-KEY
            getReports().write(0, "*********************************************************");                                                                           //Natural: WRITE '*********************************************************'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***FOUND***BATCH RESTART RECORD***FOUND***");                                                                                          //Natural: WRITE '***FOUND***BATCH RESTART RECORD***FOUND***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Key,"***");                                                                                                 //Natural: WRITE '***' '=' BATCH-RESTART.RST-KEY '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Updt_Dte,"***");                                                                                            //Natural: WRITE '***' '=' BATCH-RESTART.RST-UPDT-DTE '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Updt_Tme,"***");                                                                                            //Natural: WRITE '***' '=' BATCH-RESTART.RST-UPDT-TME '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",batch_Restart_Rst_Last_Rstrt_Tme,"***");                                                                                      //Natural: WRITE '***' '=' BATCH-RESTART.RST-LAST-RSTRT-TME '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***","=",pnd_Restart_Isn,"***");                                                                                                       //Natural: WRITE '***' '=' #RESTART-ISN '***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "***FOUND***BATCH RESTART RECORD***FOUND***");                                                                                          //Natural: WRITE '***FOUND***BATCH RESTART RECORD***FOUND***'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "*********************************************************");                                                                           //Natural: WRITE '*********************************************************'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, NEWLINE,NEWLINE);                                                                                                                       //Natural: WRITE //
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_R"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_R"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Restarted.setValue(true);                                                                                                                                 //Natural: ASSIGN #RESTARTED := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  CHECK-FOR-RESTART
    }
    private void sub_Delete_Restart_Record() throws Exception                                                                                                             //Natural: DELETE-RESTART-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  THIS LOGIC DELETES THE BATCH RESTART RECORD, INDICATING THAT THE
        //*  JOB HAS BEEN RUN SUCESSFULLY.
        GET_R:                                                                                                                                                            //Natural: GET BATCH-RESTART #RESTART-ISN
        vw_batch_Restart.readByID(pnd_Restart_Isn.getLong(), "GET_R");
        batch_Restart_Rst_Key.reset();                                                                                                                                    //Natural: RESET BATCH-RESTART.RST-KEY BATCH-RESTART.RST-CNT BATCH-RESTART.RST-ISN-NBR BATCH-RESTART.RST-CURR-DTE BATCH-RESTART.RST-CURR-TME BATCH-RESTART.RST-DATA-GRP ( * ) BATCH-RESTART.RST-RSTRT-DATA-IND BATCH-RESTART.RST-ACTVE-IND
        batch_Restart_Rst_Cnt.reset();
        batch_Restart_Rst_Isn_Nbr.reset();
        batch_Restart_Rst_Curr_Dte.reset();
        batch_Restart_Rst_Curr_Tme.reset();
        batch_Restart_Rst_Data_Grp.getValue("*").reset();
        batch_Restart_Rst_Rstrt_Data_Ind.reset();
        batch_Restart_Rst_Actve_Ind.reset();
        batch_Restart_Rst_End_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO BATCH-RESTART.RST-END-DTE
        batch_Restart_Rst_End_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                                                        //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO BATCH-RESTART.RST-END-TME
        vw_batch_Restart.deleteDBRow("GET_R");                                                                                                                            //Natural: DELETE ( GET-R. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  RESET-RESTART-RECORD
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"ACIS To OMNI Interface Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 25T 'ACIS To OMNI Interface Report' 63T 'Run Date:' *DATX / 25T '     Accepted Records        ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"     Accepted Records        ",new TabSetting(63),"Run Time:",Global.getTIMX());
                    getReports().skip(1, 2);                                                                                                                              //Natural: SKIP ( 1 ) 2
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"ACIS To OMNI Error Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 02 ) NOTITLE NOHDR 25T 'ACIS To OMNI Error Report' 63T 'Run Date:' *DATX / 25T '    Rejected Records     ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"    Rejected Records     ",new TabSetting(63),"Run Time:",Global.getTIMX());
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(25),"ACIS To OMNI Summary Report",new TabSetting(63),"Run Date:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 03 ) NOTITLE NOHDR 25T 'ACIS To OMNI Summary Report' 63T 'Run Date:' *DATX / 25T '      Process Totals       ' 63T 'Run Time:' *TIMX
                        TabSetting(25),"      Process Totals       ",new TabSetting(63),"Run Time:",Global.getTIMX());
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        short decideConditionsMet685 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF *ERROR-NR;//Natural: VALUE 3145
        if (condition((Global.getERROR_NR().equals(3145))))
        {
            decideConditionsMet685++;
            if (condition(pnd_Retry_Cnt.less(25)))                                                                                                                        //Natural: IF #RETRY-CNT < 25
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.equals(25)))                                                                                                                      //Natural: IF #RETRY-CNT = 25
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                cmrollReturnCode = DbsUtil.callExternalProgram("CMROLL",intvl,reqid);                                                                                     //Natural: CALL 'CMROLL' INTVL REQID
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.greater(25) && pnd_Retry_Cnt.lessOrEqual(50)))                                                                                    //Natural: IF #RETRY-CNT > 25 AND #RETRY-CNT <= 50
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.greater(50)))                                                                                                                     //Natural: IF #RETRY-CNT > 50
            {
                getReports().write(0, ReportOption.NOHDR,"*****************************",NEWLINE);                                                                        //Natural: WRITE NOHDR '*****************************' /
                getReports().write(0, ReportOption.NOHDR,"* ADABAS RECORD ON HOLD     *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* ADABAS RECORD ON HOLD     *' /
                getReports().write(0, ReportOption.NOHDR,"* RETRY COUNT EXCEEDED.     *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* RETRY COUNT EXCEEDED.     *' /
                getReports().write(0, ReportOption.NOHDR,"* BACKOUT UPDATES.          *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* BACKOUT UPDATES.          *' /
                getReports().write(0, ReportOption.NOHDR,"*****************************");                                                                                //Natural: WRITE NOHDR '*****************************'
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
                DbsUtil.terminate(5);  if (true) return;                                                                                                                  //Natural: TERMINATE 05
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"ERROR IN     ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER ",                  //Natural: WRITE NOHDR '****************************' / 'ERROR IN     ' *PROGRAM / 'ERROR NUMBER ' *ERROR-NR / 'ERROR LINE   ' *ERROR-LINE / '****************************' /
                Global.getERROR_NR(),NEWLINE,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE,"****************************",NEWLINE);
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-DECIDE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60");
        Global.format(2, "LS=133 PS=60");
        Global.format(3, "LS=133 PS=60");

        getReports().setDisplayColumns(0, "***** INTERFACE FILE IS EMPTY *****");
    }
}
