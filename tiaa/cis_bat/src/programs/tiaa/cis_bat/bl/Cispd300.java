/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:23:37 PM
**        * FROM NATURAL PROGRAM : Cispd300
************************************************************
**        * FILE NAME            : Cispd300.java
**        * CLASS NAME           : Cispd300
**        * INSTANCE NAME        : Cispd300
************************************************************
**======================================================================
** SYSTEM         : CIS SYSTEM
** AUTHOR         : TED LOSCHIAVO
** DESCRIPTION    : THIS PROGRAM WILL DELETE ALL RECORDS FROM AN ADABAS
**                : FILE.
** PRIME KEY      : PHYSICAL READ
** INPUT FILE     : FILE 120 DATA BASE 21 OR IN PRODUCTION 45
** OUTPUT FILE    : NONE
** PROGRAM        : CISPD300
** GENERATED      :
** FUNCTION       : THIS PROGRAM READS THE CIS-REPRT-FILE AND DELETES
**                : ALL THE RECORDS WHEN A CONDITION CODE OF O IS
**                : RETURNED IN THE JCL FOR STEP01. IF CONDITION CODE
**                : DOES NOT EQUAL O THIS PROGRAM WILL NOT RUN.
** MODIFICATIONS  :       DATE            MOD BY      DESC OF CHANGE
**======================================================================

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cispd300 extends BLNatBase
{
    // Data Areas
    private LdaCisview1 ldaCisview1;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Read_Counter;
    private DbsField pnd_Records_Read;
    private DbsField pnd_Records_Deleted;
    private DbsField pnd_Work_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCisview1 = new LdaCisview1();
        registerRecord(ldaCisview1);
        registerRecord(ldaCisview1.getVw_cis_Prt_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Read_Counter = localVariables.newFieldInRecord("pnd_Read_Counter", "#READ-COUNTER", FieldType.NUMERIC, 7);
        pnd_Records_Read = localVariables.newFieldInRecord("pnd_Records_Read", "#RECORDS-READ", FieldType.NUMERIC, 7);
        pnd_Records_Deleted = localVariables.newFieldInRecord("pnd_Records_Deleted", "#RECORDS-DELETED", FieldType.NUMERIC, 7);
        pnd_Work_Cnt = localVariables.newFieldInRecord("pnd_Work_Cnt", "#WORK-CNT", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCisview1.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cispd300() throws Exception
    {
        super("Cispd300");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *---------------------------MAIN LOGIC---------------------------------
        //*   CIS-PART-VIEW
        //*   CIS-PIN-NBR
        //*   CIS-RQST-ID-KEY
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 2 ) PS = 58 LS = 132 ZP = ON
        ldaCisview1.getVw_cis_Prt_View().startDatabaseRead                                                                                                                //Natural: READ CIS-PRT-VIEW
        (
        "PND_PND_L0320",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        PND_PND_L0320:
        while (condition(ldaCisview1.getVw_cis_Prt_View().readNextRow("PND_PND_L0320")))
        {
            getReports().write(1, "TIAA CNTRCT NUM     = ",new TabSetting(30),ldaCisview1.getCis_Prt_View_R_Tiaa_Cntrct(),NEWLINE,NEWLINE,"CREF CNTRCT NUM     = ",new    //Natural: WRITE ( 1 ) 'TIAA CNTRCT NUM     = ' 30T R-TIAA-CNTRCT // 'CREF CNTRCT NUM     = ' 30T R-CREF-CNTRCT // 'KEY FOR CIS         = ' 30T R-REQ-ID-KEY // 'NAME                = ' 30T R-NAME // 'MIT STATUS CODE     = ' 30T R-MIT-STATUS-CODE // 'MIT WPID            = ' 30T R-MIT-WPID // 'MIT LOG DTE TIM     = ' 30T R-MIT-LOG-DATE-TIME ///
                TabSetting(30),ldaCisview1.getCis_Prt_View_R_Cref_Cntrct(),NEWLINE,NEWLINE,"KEY FOR CIS         = ",new TabSetting(30),ldaCisview1.getCis_Prt_View_R_Req_Id_Key(),NEWLINE,NEWLINE,"NAME                = ",new 
                TabSetting(30),ldaCisview1.getCis_Prt_View_R_Name(),NEWLINE,NEWLINE,"MIT STATUS CODE     = ",new TabSetting(30),ldaCisview1.getCis_Prt_View_R_Mit_Status_Code(),NEWLINE,NEWLINE,"MIT WPID            = ",new 
                TabSetting(30),ldaCisview1.getCis_Prt_View_R_Mit_Wpid(),NEWLINE,NEWLINE,"MIT LOG DTE TIM     = ",new TabSetting(30),ldaCisview1.getCis_Prt_View_R_Mit_Log_Date_Time(),
                NEWLINE,NEWLINE,NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0320"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0320"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Read_Counter.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #READ-COUNTER
            ldaCisview1.getVw_cis_Prt_View().deleteDBRow("PND_PND_L0320");                                                                                                //Natural: DELETE ( ##L0320. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Records_Deleted.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #RECORDS-DELETED
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ****************  TOTALS AFTER DELETE **********************
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 60T 'CIS RE-PRINT SYSTEM DELETE' 105T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM ///
        //* *
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, "*****  TOTAL CONTRACTS READ FROM THE REPRINT FILE             =  ",pnd_Read_Counter, new ReportEditMask ("Z,ZZZ,ZZ9"));                    //Natural: WRITE ( 1 ) '*****  TOTAL CONTRACTS READ FROM THE REPRINT FILE             =  ' #READ-COUNTER ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        getReports().write(1, "*****  TOTAL CONTRACTS DELETED FROM THE REPRINT FILE          =  ",pnd_Records_Deleted, new ReportEditMask ("Z,ZZZ,ZZ9"));                 //Natural: WRITE ( 1 ) '*****  TOTAL CONTRACTS DELETED FROM THE REPRINT FILE          =  ' #RECORDS-DELETED ( EM = Z,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=132 ZP=ON");
        Global.format(2, "PS=58 LS=132 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(60),"CIS RE-PRINT SYSTEM DELETE",new TabSetting(105),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,
            Global.getINIT_USER(),"-",Global.getPROGRAM(),NEWLINE,NEWLINE,NEWLINE);
    }
}
