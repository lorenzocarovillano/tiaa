/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:21:14 PM
**        * FROM NATURAL PROGRAM : Cisb300
************************************************************
**        * FILE NAME            : Cisb300.java
**        * CLASS NAME           : Cisb300
**        * INSTANCE NAME        : Cisb300
************************************************************
**======================================================================
** SYSTEM         : CIS
** DESCRIPTION    : THIS PROGRAM WILL LOAD DATA FROM A WORK FILE TO AN
**                : ADABAS FILE.
** PRIME KEY      : SEQ READ
** INPUT FILE     : WORK FILE OR TAPE
** OUTPUT FILE    : THIS WILL BE THE ADABAS FILE
** PROGRAM        :
** GENERATED      :
** FUNCTION       : THIS PROGRAM WILL LOAD RECORDS THAT WERE WRITTEN
**                : TO A WORK FILE BY ANOTHER PROGRAM.
** MODIFICATIONS  :       DATE            MOD BY      DESC OF CHANGE
**------------------------------------------------------------------
** 06/07/06 O SOTTO CHANGED AS PART OF MDO LEGAL SPLIT. SC 060706.
** 08/07/07 K.GATES - CHANGED FIND STMT AS PART OF INDICATIVE DATA
**          COORDINATION PROJECT - CSAP (CHG36851) SEE CSAP KG
** 02/02/09 K.GATES - RESTOWED FOR CISL200 FOR IA TIAA ACCESS PROJECT.
** 09/01/15 B.ELLO  - ADDED THE BENE IN THE PLAN FIELDS IN THE WORKFILE
**                    LAYOUT AND TO LDA CISL200.    (BIP)
** 12/12/15 MUKHER  - INDICATORS CIS-CORP-SYNC-IND AND CIS-COR-SYNC-IND
**                    SHALL NOT BE MODIFIED IF ZERO PIN IS DETECTED.
**                    THIS CHANGE IS IN CONJUNCTION WITH THE CHANGES
**                    EFFECTED IN CISB600. SEE TAG (C367955)
** 05/09/17 (GHOSABE) PIN EXPANSION CHANGES.(           )      PINE.
** 04/18/2017  B. NEWSOM  PROGRAM EXTRACTS RECORDS FROM THE CIS
**                        PARTICIPANT FILE THAT NEED TO BE SYNCHED UP IN
**                        MDM.                                   (IARPF)
**======================================================================

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb300 extends BLNatBase
{
    // Data Areas
    private LdaCisl200 ldaCisl200;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_File1;
    private DbsField pnd_Work_File1_Cis_Pin_Nbr;
    private DbsField pnd_Work_File1_Cis_Rqst_Id_Key;
    private DbsField pnd_Work_File1_Cis_Tiaa_Nbr;
    private DbsField pnd_Work_File1_Cis_Cert_Nbr;
    private DbsField pnd_Work_File1_Cis_Tiaa_Doi;

    private DbsGroup pnd_Work_File1__R_Field_1;
    private DbsField pnd_Work_File1_Cis_Tiaa_Doi_A;
    private DbsField pnd_Work_File1_Cis_Cref_Doi;

    private DbsGroup pnd_Work_File1__R_Field_2;
    private DbsField pnd_Work_File1_Cis_Cref_Doi_A;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Ssn;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Prfx;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Frst_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Mid_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Lst_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Sffx;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Dob;

    private DbsGroup pnd_Work_File1__R_Field_3;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Dob_A;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Sex_Cde;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Calc_Method;
    private DbsField pnd_Work_File1_Cis_Opn_Clsd_Ind;
    private DbsField pnd_Work_File1_Cis_Status_Cd;
    private DbsField pnd_Work_File1_Cis_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Cntrct_Apprvl_Ind;
    private DbsField pnd_Work_File1_Cis_Rqst_Id;
    private DbsField pnd_Work_File1_Cis_Hold_Cde;
    private DbsField pnd_Work_File1_Cis_Cntrct_Print_Dte;

    private DbsGroup pnd_Work_File1__R_Field_4;
    private DbsField pnd_Work_File1_Cis_Cntrct_Print_Dte_A;
    private DbsField pnd_Work_File1_Cis_Extract_Date;

    private DbsGroup pnd_Work_File1__R_Field_5;
    private DbsField pnd_Work_File1_Cis_Extract_Date_A;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_Dte;

    private DbsGroup pnd_Work_File1__R_Field_6;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_Dte_A;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_User_Id;
    private DbsField pnd_Work_File1_Cis_Appl_Entry_User_Id;
    private DbsField pnd_Work_File1_Cis_Annty_Option;
    private DbsField pnd_Work_File1_Cis_Pymnt_Mode;
    private DbsField pnd_Work_File1_Cis_Annty_Start_Dte;

    private DbsGroup pnd_Work_File1__R_Field_7;
    private DbsField pnd_Work_File1_Cis_Annty_Start_Dte_A;
    private DbsField pnd_Work_File1_Cis_Annty_End_Dte;

    private DbsGroup pnd_Work_File1__R_Field_8;
    private DbsField pnd_Work_File1_Cis_Annty_End_Dte_A;
    private DbsField pnd_Work_File1_Cis_Grnted_Period_Yrs;
    private DbsField pnd_Work_File1_Cis_Grnted_Period_Dys;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Prfx;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Sffx;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Ssn;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Dob;

    private DbsGroup pnd_Work_File1__R_Field_9;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Dob_A;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde;

    private DbsGroup pnd_Work_File1_Cis_Cref_Annty_Pymnt;
    private DbsField pnd_Work_File1_Cis_Cref_Acct_Cde;
    private DbsField pnd_Work_File1_Cis_Orig_Issue_State;
    private DbsField pnd_Work_File1_Cis_Issue_State_Name;
    private DbsField pnd_Work_File1_Cis_Issue_State_Cd;
    private DbsField pnd_Work_File1_Cis_Ppg_Code;
    private DbsField pnd_Work_File1_Cis_Region_Code;
    private DbsField pnd_Work_File1_Cis_Ownership_Cd;
    private DbsField pnd_Work_File1_Cis_Bill_Code;
    private DbsField pnd_Work_File1_Cis_Lob;
    private DbsField pnd_Work_File1_Cis_Lob_Type;
    private DbsField pnd_Work_File1_Cis_Addr_Syn_Ind;
    private DbsField pnd_Work_File1_Cis_Addr_Process_Env;

    private DbsGroup pnd_Work_File1_Cis_Address_Info;
    private DbsField pnd_Work_File1_Cis_Address_Chg_Ind;
    private DbsField pnd_Work_File1_Cis_Address_Dest_Name;
    private DbsField pnd_Work_File1_Cis_Address_Txt;
    private DbsField pnd_Work_File1_Cis_Zip_Code;
    private DbsField pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Work_File1_Cis_Stndrd_Trn_Cd;
    private DbsField pnd_Work_File1_Cis_Finalist_Reason_Cd;
    private DbsField pnd_Work_File1_Cis_Addr_Usage_Code;
    private DbsField pnd_Work_File1_Cis_Checking_Saving_Cd;
    private DbsField pnd_Work_File1_Cis_Addr_Stndrd_Code;
    private DbsField pnd_Work_File1_Cis_Stndrd_Overide;
    private DbsField pnd_Work_File1_Cis_Postal_Data_Fields;
    private DbsField pnd_Work_File1_Cis_Geographic_Cd;
    private DbsField pnd_Work_File1_Cis_Corp_Sync_Ind;

    private DbsGroup pnd_Work_File1_Cis_Rqst_System_Mit_Dta;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Function;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env;
    private DbsField pnd_Work_File1_Cis_Mit_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Mit_Error_Msg;

    private DbsGroup pnd_Work_File1_Cis_Non_Premium_Dta;
    private DbsField pnd_Work_File1_Cis_Nonp_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Nonp_Process_Env;
    private DbsField pnd_Work_File1_Cis_Nonp_Effective_Dte;

    private DbsGroup pnd_Work_File1__R_Field_10;
    private DbsField pnd_Work_File1_Cis_Nonp_Effective_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Product_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte;

    private DbsGroup pnd_Work_File1__R_Field_11;
    private DbsField pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Currency;
    private DbsField pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt;
    private DbsField pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt;
    private DbsField pnd_Work_File1_Cis_Nonp_Deletion_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Vesting_Dte;

    private DbsGroup pnd_Work_File1__R_Field_12;
    private DbsField pnd_Work_File1_Cis_Nonp_Vesting_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Error_Msg;

    private DbsGroup pnd_Work_File1_Cis_Allocation_Dta;
    private DbsField pnd_Work_File1_Cis_Alloc_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Alloc_Process_Env;
    private DbsField pnd_Work_File1_Cis_Alloc_Effective_Dte;

    private DbsGroup pnd_Work_File1__R_Field_13;
    private DbsField pnd_Work_File1_Cis_Alloc_Effective_Dte_A;
    private DbsField pnd_Work_File1_Cis_Alloc_Percentage;
    private DbsField pnd_Work_File1_Cis_Alloc_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Alloc_Error_Msg;
    private DbsField pnd_Work_File1_Cis_Cor_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Process_Env;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Xref_Pin;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt;

    private DbsGroup pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Mail_Table_Cnt;

    private DbsGroup pnd_Work_File1_Cis_Cor_Ph_Mail_Grp;
    private DbsField pnd_Work_File1_Cis_Cor_Phmail_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin;
    private DbsField pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte;

    private DbsGroup pnd_Work_File1__R_Field_14;
    private DbsField pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Pull_Code;
    private DbsField pnd_Work_File1_Cis_Trnsf_Flag;
    private DbsField pnd_Work_File1_Cis_Trnsf_From_Company;
    private DbsField pnd_Work_File1_Cis_Trnsf_To_Company;
    private DbsField pnd_Work_File1_Cis_Tiaa_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Rea_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Cref_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Decedent_Contract;
    private DbsField pnd_Work_File1_Cis_Relation_To_Decedent;
    private DbsField pnd_Work_File1_Cis_Ssn_Tin_Ind;
    private DbsField pnd_Work_File1_Cis_Isn;
    private DbsField pnd_Work_File1_Cis_Mdo_Contract_Type;
    private DbsField pnd_Work_File1_Cis_Sg_Text_Udf_3;
    private DbsField pnd_Isn;
    private DbsField pnd_Total_Rec;
    private DbsField pnd_Rec_Updated;
    private DbsField pnd_Rec_Updated_Ia;
    private DbsField pnd_Rec_Updated_L;
    private DbsField pnd_Rec_Updated_Mdo;
    private DbsField pnd_Rec_Notfound;
    private DbsField pnd_Rec_Notfound_Ia;
    private DbsField pnd_Rec_Notfound_L;
    private DbsField pnd_Rec_Notfound_Mdo;
    private DbsField pnd_Rec_Found;
    private DbsField pnd_Rec_Found_Ia;
    private DbsField pnd_Rec_Found_L;
    private DbsField pnd_Rec_Found_Mdo;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCisl200 = new LdaCisl200();
        registerRecord(ldaCisl200);
        registerRecord(ldaCisl200.getVw_cis_Prtcpnt_File());

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_File1 = localVariables.newGroupInRecord("pnd_Work_File1", "#WORK-FILE1");
        pnd_Work_File1_Cis_Pin_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Work_File1_Cis_Rqst_Id_Key = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 35);
        pnd_Work_File1_Cis_Tiaa_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Cert_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Tiaa_Doi = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_1 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_1", "REDEFINE", pnd_Work_File1_Cis_Tiaa_Doi);
        pnd_Work_File1_Cis_Tiaa_Doi_A = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Doi_A", "CIS-TIAA-DOI-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Cref_Doi = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_2 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_2", "REDEFINE", pnd_Work_File1_Cis_Cref_Doi);
        pnd_Work_File1_Cis_Cref_Doi_A = pnd_Work_File1__R_Field_2.newFieldInGroup("pnd_Work_File1_Cis_Cref_Doi_A", "CIS-CREF-DOI-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Ssn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Ssn", "CIS-FRST-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Work_File1_Cis_Frst_Annt_Prfx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Prfx", "CIS-FRST-ANNT-PRFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Frst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Frst_Nme", "CIS-FRST-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Mid_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Mid_Nme", "CIS-FRST-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Lst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Lst_Nme", "CIS-FRST-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Sffx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Sffx", "CIS-FRST-ANNT-SFFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd", "CIS-FRST-ANNT-CTZNSHP-CD", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde", "CIS-FRST-ANNT-RSDNC-CDE", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Frst_Annt_Dob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Dob", "CIS-FRST-ANNT-DOB", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_3 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_3", "REDEFINE", pnd_Work_File1_Cis_Frst_Annt_Dob);
        pnd_Work_File1_Cis_Frst_Annt_Dob_A = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Dob_A", "CIS-FRST-ANNT-DOB-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Sex_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Sex_Cde", "CIS-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Frst_Annt_Calc_Method = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Calc_Method", "CIS-FRST-ANNT-CALC-METHOD", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Opn_Clsd_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Opn_Clsd_Ind", "CIS-OPN-CLSD-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Status_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Status_Cd", "CIS-STATUS-CD", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cntrct_Apprvl_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Apprvl_Ind", "CIS-CNTRCT-APPRVL-IND", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Rqst_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Hold_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Hold_Cde", "CIS-HOLD-CDE", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Cntrct_Print_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Print_Dte", "CIS-CNTRCT-PRINT-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_4 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_4", "REDEFINE", pnd_Work_File1_Cis_Cntrct_Print_Dte);
        pnd_Work_File1_Cis_Cntrct_Print_Dte_A = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Print_Dte_A", "CIS-CNTRCT-PRINT-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Extract_Date = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Extract_Date", "CIS-EXTRACT-DATE", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_5 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_5", "REDEFINE", pnd_Work_File1_Cis_Extract_Date);
        pnd_Work_File1_Cis_Extract_Date_A = pnd_Work_File1__R_Field_5.newFieldInGroup("pnd_Work_File1_Cis_Extract_Date_A", "CIS-EXTRACT-DATE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Rcvd_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_Dte", "CIS-APPL-RCVD-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_6 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_6", "REDEFINE", pnd_Work_File1_Cis_Appl_Rcvd_Dte);
        pnd_Work_File1_Cis_Appl_Rcvd_Dte_A = pnd_Work_File1__R_Field_6.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_Dte_A", "CIS-APPL-RCVD-DTE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Rcvd_User_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_User_Id", "CIS-APPL-RCVD-USER-ID", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Entry_User_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Entry_User_Id", "CIS-APPL-ENTRY-USER-ID", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Annty_Option = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Pymnt_Mode = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pymnt_Mode", "CIS-PYMNT-MODE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Annty_Start_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_Start_Dte", "CIS-ANNTY-START-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_7 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_7", "REDEFINE", pnd_Work_File1_Cis_Annty_Start_Dte);
        pnd_Work_File1_Cis_Annty_Start_Dte_A = pnd_Work_File1__R_Field_7.newFieldInGroup("pnd_Work_File1_Cis_Annty_Start_Dte_A", "CIS-ANNTY-START-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Annty_End_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_End_Dte", "CIS-ANNTY-END-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_8 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_8", "REDEFINE", pnd_Work_File1_Cis_Annty_End_Dte);
        pnd_Work_File1_Cis_Annty_End_Dte_A = pnd_Work_File1__R_Field_8.newFieldInGroup("pnd_Work_File1_Cis_Annty_End_Dte_A", "CIS-ANNTY-END-DTE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Grnted_Period_Yrs = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Grnted_Period_Yrs", "CIS-GRNTED-PERIOD-YRS", FieldType.NUMERIC, 
            2);
        pnd_Work_File1_Cis_Grnted_Period_Dys = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Grnted_Period_Dys", "CIS-GRNTED-PERIOD-DYS", FieldType.NUMERIC, 
            3);
        pnd_Work_File1_Cis_Scnd_Annt_Prfx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Prfx", "CIS-SCND-ANNT-PRFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme", "CIS-SCND-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme", "CIS-SCND-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme", "CIS-SCND-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Sffx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Sffx", "CIS-SCND-ANNT-SFFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Ssn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Work_File1_Cis_Scnd_Annt_Dob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Dob", "CIS-SCND-ANNT-DOB", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_9 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_9", "REDEFINE", pnd_Work_File1_Cis_Scnd_Annt_Dob);
        pnd_Work_File1_Cis_Scnd_Annt_Dob_A = pnd_Work_File1__R_Field_9.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Dob_A", "CIS-SCND-ANNT-DOB-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde", "CIS-SCND-ANNT-SEX-CDE", FieldType.STRING, 
            1);

        pnd_Work_File1_Cis_Cref_Annty_Pymnt = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cref_Annty_Pymnt", "CIS-CREF-ANNTY-PYMNT", new DbsArrayController(1, 
            20));
        pnd_Work_File1_Cis_Cref_Acct_Cde = pnd_Work_File1_Cis_Cref_Annty_Pymnt.newFieldInGroup("pnd_Work_File1_Cis_Cref_Acct_Cde", "CIS-CREF-ACCT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Orig_Issue_State = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Orig_Issue_State", "CIS-ORIG-ISSUE-STATE", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Issue_State_Name = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Issue_State_Name", "CIS-ISSUE-STATE-NAME", FieldType.STRING, 
            15);
        pnd_Work_File1_Cis_Issue_State_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Issue_State_Cd", "CIS-ISSUE-STATE-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Ppg_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ppg_Code", "CIS-PPG-CODE", FieldType.STRING, 6);
        pnd_Work_File1_Cis_Region_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Region_Code", "CIS-REGION-CODE", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Ownership_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ownership_Cd", "CIS-OWNERSHIP-CD", FieldType.NUMERIC, 1);
        pnd_Work_File1_Cis_Bill_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Bill_Code", "CIS-BILL-CODE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Lob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Lob", "CIS-LOB", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Lob_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Lob_Type", "CIS-LOB-TYPE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Syn_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Addr_Syn_Ind", "CIS-ADDR-SYN-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Process_Env = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Addr_Process_Env", "CIS-ADDR-PROCESS-ENV", FieldType.STRING, 
            1);

        pnd_Work_File1_Cis_Address_Info = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Address_Info", "CIS-ADDRESS-INFO", new DbsArrayController(1, 
            3));
        pnd_Work_File1_Cis_Address_Chg_Ind = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Address_Chg_Ind", "CIS-ADDRESS-CHG-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Address_Dest_Name = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Address_Dest_Name", "CIS-ADDRESS-DEST-NAME", 
            FieldType.STRING, 35);
        pnd_Work_File1_Cis_Address_Txt = pnd_Work_File1_Cis_Address_Info.newFieldArrayInGroup("pnd_Work_File1_Cis_Address_Txt", "CIS-ADDRESS-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 5));
        pnd_Work_File1_Cis_Zip_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Zip_Code", "CIS-ZIP-CODE", FieldType.STRING, 
            9);
        pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr", "CIS-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr", "CIS-BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9);
        pnd_Work_File1_Cis_Stndrd_Trn_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Stndrd_Trn_Cd", "CIS-STNDRD-TRN-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Finalist_Reason_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Finalist_Reason_Cd", "CIS-FINALIST-REASON-CD", 
            FieldType.STRING, 10);
        pnd_Work_File1_Cis_Addr_Usage_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Addr_Usage_Code", "CIS-ADDR-USAGE-CODE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Checking_Saving_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Checking_Saving_Cd", "CIS-CHECKING-SAVING-CD", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Stndrd_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Addr_Stndrd_Code", "CIS-ADDR-STNDRD-CODE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Stndrd_Overide = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Stndrd_Overide", "CIS-STNDRD-OVERIDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Postal_Data_Fields = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Postal_Data_Fields", "CIS-POSTAL-DATA-FIELDS", 
            FieldType.STRING, 44);
        pnd_Work_File1_Cis_Geographic_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Geographic_Cd", "CIS-GEOGRAPHIC-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Corp_Sync_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Corp_Sync_Ind", "CIS-CORP-SYNC-IND", FieldType.STRING, 1);

        pnd_Work_File1_Cis_Rqst_System_Mit_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Rqst_System_Mit_Dta", "CIS-RQST-SYSTEM-MIT-DTA");
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind", 
            "CIS-RQST-SYS-MIT-SYNC-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme", 
            "CIS-RQST-SYS-MIT-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Function = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Function", 
            "CIS-RQST-SYS-MIT-FUNCTION", FieldType.STRING, 2);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env", 
            "CIS-RQST-SYS-MIT-PROCESS-ENV", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Mit_Error_Cde = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Mit_Error_Cde", "CIS-MIT-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Mit_Error_Msg = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Mit_Error_Msg", "CIS-MIT-ERROR-MSG", 
            FieldType.STRING, 72);

        pnd_Work_File1_Cis_Non_Premium_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Non_Premium_Dta", "CIS-NON-PREMIUM-DTA");
        pnd_Work_File1_Cis_Nonp_Sync_Ind = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Sync_Ind", "CIS-NONP-SYNC-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Process_Env = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Process_Env", "CIS-NONP-PROCESS-ENV", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Effective_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Effective_Dte", "CIS-NONP-EFFECTIVE-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_10 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_10", "REDEFINE", pnd_Work_File1_Cis_Nonp_Effective_Dte);
        pnd_Work_File1_Cis_Nonp_Effective_Dte_A = pnd_Work_File1__R_Field_10.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Effective_Dte_A", "CIS-NONP-EFFECTIVE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Product_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Product_Cde", "CIS-NONP-PRODUCT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte", "CIS-NONP-CONTRACT-RETR-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_11 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_11", "REDEFINE", pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte);
        pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A = pnd_Work_File1__R_Field_11.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A", "CIS-NONP-CONTRACT-RETR-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt", 
            "CIS-NONP-RATE-ACCUM-ADJ-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Currency = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Currency", "CIS-NONP-CURRENCY", 
            FieldType.NUMERIC, 1);
        pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt", 
            "CIS-NONP-TIAA-AGE-FIRST-PYMNT", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt", 
            "CIS-NONP-CREF-AGE-FIRST-PYMNT", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Deletion_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Deletion_Cde", "CIS-NONP-DELETION-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Vesting_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Vesting_Dte", "CIS-NONP-VESTING-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_12 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_12", "REDEFINE", pnd_Work_File1_Cis_Nonp_Vesting_Dte);
        pnd_Work_File1_Cis_Nonp_Vesting_Dte_A = pnd_Work_File1__R_Field_12.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Vesting_Dte_A", "CIS-NONP-VESTING-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt", 
            "CIS-NONP-CNTR-RATE38-ACCUM-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt", 
            "CIS-NONP-CNTR-RATE42-ACCUM-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Error_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Error_Cde", "CIS-NONP-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Error_Msg = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Error_Msg", "CIS-NONP-ERROR-MSG", 
            FieldType.STRING, 72);

        pnd_Work_File1_Cis_Allocation_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Allocation_Dta", "CIS-ALLOCATION-DTA");
        pnd_Work_File1_Cis_Alloc_Sync_Ind = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Sync_Ind", "CIS-ALLOC-SYNC-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Alloc_Process_Env = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Process_Env", "CIS-ALLOC-PROCESS-ENV", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Alloc_Effective_Dte = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Effective_Dte", "CIS-ALLOC-EFFECTIVE-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_13 = pnd_Work_File1_Cis_Allocation_Dta.newGroupInGroup("pnd_Work_File1__R_Field_13", "REDEFINE", pnd_Work_File1_Cis_Alloc_Effective_Dte);
        pnd_Work_File1_Cis_Alloc_Effective_Dte_A = pnd_Work_File1__R_Field_13.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Effective_Dte_A", "CIS-ALLOC-EFFECTIVE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Alloc_Percentage = pnd_Work_File1_Cis_Allocation_Dta.newFieldArrayInGroup("pnd_Work_File1_Cis_Alloc_Percentage", "CIS-ALLOC-PERCENTAGE", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20));
        pnd_Work_File1_Cis_Alloc_Error_Cde = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Error_Cde", "CIS-ALLOC-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Alloc_Error_Msg = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Error_Msg", "CIS-ALLOC-ERROR-MSG", 
            FieldType.STRING, 72);
        pnd_Work_File1_Cis_Cor_Sync_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Sync_Ind", "CIS-COR-SYNC-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Process_Env = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Process_Env", "CIS-COR-PROCESS-ENV", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde", "CIS-COR-PH-RCD-TYP-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde", "CIS-COR-PH-NEG-ELECTION-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme", "CIS-COR-PH-OCCUPNT-NME", FieldType.STRING, 
            10);
        pnd_Work_File1_Cis_Cor_Ph_Xref_Pin = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Xref_Pin", "CIS-COR-PH-XREF-PIN", FieldType.NUMERIC, 
            12);
        pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt", "CIS-COR-PH-ACTIVE-VIP-CNT", 
            FieldType.NUMERIC, 2);

        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp", "CIS-COR-PH-ACTVE-VIP-GRP", 
            new DbsArrayController(1, 8));
        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde = pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde", "CIS-COR-PH-ACTVE-VIP-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde = pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde", 
            "CIS-COR-PH-ACTVE-VIP-AREA-CDE", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Cor_Mail_Table_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Mail_Table_Cnt", "CIS-COR-MAIL-TABLE-CNT", FieldType.NUMERIC, 
            2);

        pnd_Work_File1_Cis_Cor_Ph_Mail_Grp = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cor_Ph_Mail_Grp", "CIS-COR-PH-MAIL-GRP", new DbsArrayController(1, 
            25));
        pnd_Work_File1_Cis_Cor_Phmail_Cde = pnd_Work_File1_Cis_Cor_Ph_Mail_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Phmail_Cde", "CIS-COR-PHMAIL-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin = pnd_Work_File1_Cis_Cor_Ph_Mail_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin", 
            "CIS-COR-PHMAIL-AREA-OF-ORIGIN", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte", "CIS-COR-INST-PH-RMTTNG-INSTN-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_14 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_14", "REDEFINE", pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte);
        pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta = pnd_Work_File1__R_Field_14.newFieldInGroup("pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta", 
            "CIS-COR-INST-PH-RMTTNG-INSTN-DTA", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr", "CIS-COR-PH-RMTTNG-INSTN-NBR", 
            FieldType.NUMERIC, 5);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind", "CIS-COR-PH-RMTTNG-INSTN-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind", "CIS-COR-PH-RMMTNG-INSTN-PDUP-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde", "CIS-COR-PH-RMTTNG-NMBR-RANGE-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind", "CIS-COR-PH-MCRJCK-MEDIA-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde", "CIS-COR-PH-MCRJCK-CNTNTS-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt", "CIS-COR-CNTRCT-TABLE-CNT", 
            FieldType.NUMERIC, 2);
        pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde", "CIS-COR-CNTRCT-STATUS-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte", "CIS-COR-CNTRCT-STATUS-YR-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Error_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Error_Cde", "CIS-COR-ERROR-CDE", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Pull_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pull_Code", "CIS-PULL-CODE", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Trnsf_Flag = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_Flag", "CIS-TRNSF-FLAG", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Trnsf_From_Company = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_From_Company", "CIS-TRNSF-FROM-COMPANY", FieldType.STRING, 
            4);
        pnd_Work_File1_Cis_Trnsf_To_Company = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_To_Company", "CIS-TRNSF-TO-COMPANY", FieldType.STRING, 
            4);
        pnd_Work_File1_Cis_Tiaa_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Cntrct_Type", "CIS-TIAA-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Rea_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rea_Cntrct_Type", "CIS-REA-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Cref_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cref_Cntrct_Type", "CIS-CREF-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Decedent_Contract = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Decedent_Contract", "CIS-DECEDENT-CONTRACT", FieldType.STRING, 
            10);
        pnd_Work_File1_Cis_Relation_To_Decedent = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Relation_To_Decedent", "CIS-RELATION-TO-DECEDENT", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Ssn_Tin_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ssn_Tin_Ind", "CIS-SSN-TIN-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Isn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Isn", "CIS-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Work_File1_Cis_Mdo_Contract_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Mdo_Contract_Type", "CIS-MDO-CONTRACT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Sg_Text_Udf_3 = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", FieldType.STRING, 10);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Total_Rec = localVariables.newFieldInRecord("pnd_Total_Rec", "#TOTAL-REC", FieldType.PACKED_DECIMAL, 15);
        pnd_Rec_Updated = localVariables.newFieldInRecord("pnd_Rec_Updated", "#REC-UPDATED", FieldType.PACKED_DECIMAL, 15);
        pnd_Rec_Updated_Ia = localVariables.newFieldInRecord("pnd_Rec_Updated_Ia", "#REC-UPDATED-IA", FieldType.PACKED_DECIMAL, 15);
        pnd_Rec_Updated_L = localVariables.newFieldInRecord("pnd_Rec_Updated_L", "#REC-UPDATED-L", FieldType.BOOLEAN, 1);
        pnd_Rec_Updated_Mdo = localVariables.newFieldInRecord("pnd_Rec_Updated_Mdo", "#REC-UPDATED-MDO", FieldType.PACKED_DECIMAL, 15);
        pnd_Rec_Notfound = localVariables.newFieldInRecord("pnd_Rec_Notfound", "#REC-NOTFOUND", FieldType.PACKED_DECIMAL, 15);
        pnd_Rec_Notfound_Ia = localVariables.newFieldInRecord("pnd_Rec_Notfound_Ia", "#REC-NOTFOUND-IA", FieldType.PACKED_DECIMAL, 15);
        pnd_Rec_Notfound_L = localVariables.newFieldInRecord("pnd_Rec_Notfound_L", "#REC-NOTFOUND-L", FieldType.BOOLEAN, 1);
        pnd_Rec_Notfound_Mdo = localVariables.newFieldInRecord("pnd_Rec_Notfound_Mdo", "#REC-NOTFOUND-MDO", FieldType.PACKED_DECIMAL, 15);
        pnd_Rec_Found = localVariables.newFieldInRecord("pnd_Rec_Found", "#REC-FOUND", FieldType.PACKED_DECIMAL, 15);
        pnd_Rec_Found_Ia = localVariables.newFieldInRecord("pnd_Rec_Found_Ia", "#REC-FOUND-IA", FieldType.PACKED_DECIMAL, 15);
        pnd_Rec_Found_L = localVariables.newFieldInRecord("pnd_Rec_Found_L", "#REC-FOUND-L", FieldType.BOOLEAN, 1);
        pnd_Rec_Found_Mdo = localVariables.newFieldInRecord("pnd_Rec_Found_Mdo", "#REC-FOUND-MDO", FieldType.PACKED_DECIMAL, 15);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCisl200.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisb300() throws Exception
    {
        super("Cisb300");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CISB300", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-FILE1
        while (condition(getWorkFiles().read(1, pnd_Work_File1)))
        {
            pnd_Rec_Found_L.reset();                                                                                                                                      //Natural: RESET #REC-FOUND-L #REC-NOTFOUND-L #REC-UPDATED-L
            pnd_Rec_Notfound_L.reset();
            pnd_Rec_Updated_L.reset();
            pnd_Total_Rec.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOTAL-REC
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 2 )
            pnd_Isn.reset();                                                                                                                                              //Natural: RESET #ISN
            //*  CSAP KG
            //*  CSAP KG
            ldaCisl200.getVw_cis_Prtcpnt_File().startDatabaseFind                                                                                                         //Natural: FIND CIS-PRTCPNT-FILE WITH CIS-TIAA-NBR EQ #WORK-FILE1.CIS-TIAA-NBR
            (
            "F1",
            new Wc[] { new Wc("CIS_TIAA_NBR", "=", pnd_Work_File1_Cis_Tiaa_Nbr, WcType.WITH) }
            );
            F1:
            while (condition(ldaCisl200.getVw_cis_Prtcpnt_File().readNextRow("F1", true)))
            {
                ldaCisl200.getVw_cis_Prtcpnt_File().setIfNotFoundControlFlag(false);
                //*  FIND CIS-PRTCPNT-FILE WITH CIS-RQST-ID-KEY EQ     /* CSAP KG
                //*      #WORK-FILE1.CIS-RQST-ID-KEY                   /* CSAP KG
                if (condition(ldaCisl200.getVw_cis_Prtcpnt_File().getAstCOUNTER().equals(0)))                                                                             //Natural: IF NO RECORDS FOUND
                {
                    pnd_Rec_Notfound.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REC-NOTFOUND
                    pnd_Rec_Notfound_L.setValue(true);                                                                                                                    //Natural: ASSIGN #REC-NOTFOUND-L := TRUE
                                                                                                                                                                          //Natural: PERFORM NO-RECORD-REPORT
                    sub_No_Record_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM TOTAL-IA-AND-MDO
                    sub_Total_Ia_And_Mdo();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("F1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break F1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F1. )
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Rec_Found_L.setValue(true);                                                                                                                           //Natural: ASSIGN #REC-FOUND-L := TRUE
                pnd_Rec_Found.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #REC-FOUND
                                                                                                                                                                          //Natural: PERFORM TOTAL-IA-AND-MDO
                sub_Total_Ia_And_Mdo();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("F1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Isn.setValue(ldaCisl200.getVw_cis_Prtcpnt_File().getAstISN("F1"));                                                                                    //Natural: ASSIGN #ISN := *ISN
                if (true) break F1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F1. )
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-RECORD-REPORT
            sub_Update_Record_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-PRTCPNT-FILE
            sub_Update_Prtcpnt_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Total_Rec.equals(getZero())))                                                                                                                   //Natural: IF #TOTAL-REC EQ 0
        {
            getReports().write(1, ReportOption.NOTITLE," NO RECORDS TO BE PROCESSED FOR THE DAY ",Global.getDATN());                                                      //Natural: WRITE ( 1 ) ' NO RECORDS TO BE PROCESSED FOR THE DAY ' *DATN
            if (Global.isEscape()) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM TOTAL-IA-AND-MDO
        sub_Total_Ia_And_Mdo();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GRAND-TOTAL
        sub_Grand_Total();
        if (condition(Global.isEscape())) {return;}
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PRTCPNT-FILE
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-RECORD-REPORT
        //*    CIS-PIN-NBR
        //*  42T CIS-PRTCPNT-FILE.CIS-TIAA-NBR
        //*  63T CIS-PRTCPNT-FILE.CIS-CERT-NBR
        //*  75T CIS-PRTCPNT-FILE.CIS-OPN-CLSD-IND
        //*  85T CIS-PRTCPNT-FILE.CIS-STATUS-CD
        //*  92T CIS-PRTCPNT-FILE.CIS-CORP-SYNC-IND
        //*  97T CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND
        //*  101T CIS-PRTCPNT-FILE.CIS-RQST-SYS-MIT-SYNC-IND
        //*  105T CIS-PRTCPNT-FILE.CIS-ALLOC-SYNC-IND
        //*  111T CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND
        //*  116T CIS-PRTCPNT-FILE.CIS-NONP-SYNC-IND
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NO-RECORD-REPORT
        //*  42T #WORK-FILE1.CIS-TIAA-NBR
        //*  63T #WORK-FILE1.CIS-CERT-NBR
        //*  75T #WORK-FILE1.CIS-OPN-CLSD-IND
        //*  85T #WORK-FILE1.CIS-STATUS-CD
        //*  92T #WORK-FILE1.CIS-CORP-SYNC-IND
        //*  97T #WORK-FILE1.CIS-COR-SYNC-IND
        //*  101T #WORK-FILE1.CIS-RQST-SYS-MIT-SYNC-IND
        //*  105T #WORK-FILE1.CIS-ALLOC-SYNC-IND
        //*  111T #WORK-FILE1.CIS-ADDR-SYN-IND
        //*  116T #WORK-FILE1.CIS-NONP-SYNC-IND
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRAND-TOTAL
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-IA-AND-MDO
    }                                                                                                                                                                     //Natural: ON ERROR
    private void sub_Update_Prtcpnt_File() throws Exception                                                                                                               //Natural: UPDATE-PRTCPNT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        if (condition(pnd_Isn.greater(getZero())))                                                                                                                        //Natural: IF #ISN GT 0
        {
            //*  IF CIS-PRTCPNT-FILE.CIS-CORP-SYNC-IND EQ 'Y'        /* C367955
            //*  C367955
            //*  C367955
            if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Corp_Sync_Ind().equals("Y") && ldaCisl200.getCis_Prtcpnt_File_Cis_Pin_Nbr().notEquals(getZero())))           //Natural: IF CIS-PRTCPNT-FILE.CIS-CORP-SYNC-IND EQ 'Y' AND CIS-PRTCPNT-FILE.CIS-PIN-NBR NE 0
            {
                GET:                                                                                                                                                      //Natural: GET CIS-PRTCPNT-FILE #ISN
                ldaCisl200.getVw_cis_Prtcpnt_File().readByID(pnd_Isn.getLong(), "GET");
                ldaCisl200.getCis_Prtcpnt_File_Cis_Corp_Sync_Ind().setValue("M");                                                                                         //Natural: ASSIGN CIS-PRTCPNT-FILE.CIS-CORP-SYNC-IND := 'M'
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().equals("Y")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND EQ 'Y'
                {
                    ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().setValue("M");                                                                                      //Natural: ASSIGN CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND := 'M'
                }                                                                                                                                                         //Natural: END-IF
                //*    IF CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND EQ 'S'        /* C367955
                //*  C367955
                //*  C367955
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().equals("S") && ldaCisl200.getCis_Prtcpnt_File_Cis_Pin_Nbr().notEquals(getZero())))        //Natural: IF CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND EQ 'S' AND CIS-PRTCPNT-FILE.CIS-PIN-NBR NE 0
                {
                    ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().setValue("G");                                                                                      //Natural: ASSIGN CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND := 'G'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind().equals("Y")))                                                                    //Natural: IF CIS-PRTCPNT-FILE.CIS-RQST-SYS-MIT-SYNC-IND EQ 'Y'
                {
                    ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind().setValue("M");                                                                             //Natural: ASSIGN CIS-PRTCPNT-FILE.CIS-RQST-SYS-MIT-SYNC-IND := 'M'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().equals("Y")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND EQ 'Y'
                {
                    ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().setValue("M");                                                                                      //Natural: ASSIGN CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND := 'M'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().equals("S")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND EQ 'S'
                {
                    ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().setValue("G");                                                                                      //Natural: ASSIGN CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND := 'G'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Alloc_Sync_Ind().equals("Y")))                                                                           //Natural: IF CIS-PRTCPNT-FILE.CIS-ALLOC-SYNC-IND EQ 'Y'
                {
                    ldaCisl200.getCis_Prtcpnt_File_Cis_Alloc_Sync_Ind().setValue("M");                                                                                    //Natural: ASSIGN CIS-PRTCPNT-FILE.CIS-ALLOC-SYNC-IND := 'M'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Nonp_Sync_Ind().equals("Y")))                                                                            //Natural: IF CIS-PRTCPNT-FILE.CIS-NONP-SYNC-IND EQ 'Y'
                {
                    ldaCisl200.getCis_Prtcpnt_File_Cis_Nonp_Sync_Ind().setValue("M");                                                                                     //Natural: ASSIGN CIS-PRTCPNT-FILE.CIS-NONP-SYNC-IND := 'M'
                }                                                                                                                                                         //Natural: END-IF
                ldaCisl200.getVw_cis_Prtcpnt_File().updateDBRow("GET");                                                                                                   //Natural: UPDATE ( GET. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Rec_Updated.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-UPDATED
                pnd_Rec_Updated_L.setValue(true);                                                                                                                         //Natural: ASSIGN #REC-UPDATED-L := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Record_Report() throws Exception                                                                                                              //Natural: UPDATE-RECORD-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id(),new TabSetting(10),ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id_Key(),new  //Natural: WRITE ( 1 ) 1T CIS-PRTCPNT-FILE.CIS-RQST-ID 10T CIS-PRTCPNT-FILE.CIS-RQST-ID-KEY 47T CIS-PRTCPNT-FILE.CIS-TIAA-NBR 68T CIS-PRTCPNT-FILE.CIS-CERT-NBR 80T CIS-PRTCPNT-FILE.CIS-OPN-CLSD-IND 90T CIS-PRTCPNT-FILE.CIS-STATUS-CD 97T CIS-PRTCPNT-FILE.CIS-CORP-SYNC-IND 102T CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND 106T CIS-PRTCPNT-FILE.CIS-RQST-SYS-MIT-SYNC-IND 110T CIS-PRTCPNT-FILE.CIS-ALLOC-SYNC-IND 116T CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND 121T CIS-PRTCPNT-FILE.CIS-NONP-SYNC-IND
            TabSetting(47),ldaCisl200.getCis_Prtcpnt_File_Cis_Tiaa_Nbr(),new TabSetting(68),ldaCisl200.getCis_Prtcpnt_File_Cis_Cert_Nbr(),new TabSetting(80),ldaCisl200.getCis_Prtcpnt_File_Cis_Opn_Clsd_Ind(),new 
            TabSetting(90),ldaCisl200.getCis_Prtcpnt_File_Cis_Status_Cd(),new TabSetting(97),ldaCisl200.getCis_Prtcpnt_File_Cis_Corp_Sync_Ind(),new TabSetting(102),ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind(),new 
            TabSetting(106),ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind(),new TabSetting(110),ldaCisl200.getCis_Prtcpnt_File_Cis_Alloc_Sync_Ind(),new 
            TabSetting(116),ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind(),new TabSetting(121),ldaCisl200.getCis_Prtcpnt_File_Cis_Nonp_Sync_Ind());
        if (Global.isEscape()) return;
    }
    private void sub_No_Record_Report() throws Exception                                                                                                                  //Natural: NO-RECORD-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        //*  PINE
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_Work_File1_Cis_Rqst_Id,new TabSetting(10),pnd_Work_File1_Cis_Rqst_Id_Key,new                     //Natural: WRITE ( 2 ) 1T #WORK-FILE1.CIS-RQST-ID 10T #WORK-FILE1.CIS-RQST-ID-KEY 47T #WORK-FILE1.CIS-TIAA-NBR 68T #WORK-FILE1.CIS-CERT-NBR 80T #WORK-FILE1.CIS-OPN-CLSD-IND 90T #WORK-FILE1.CIS-STATUS-CD 97T #WORK-FILE1.CIS-CORP-SYNC-IND 102T #WORK-FILE1.CIS-COR-SYNC-IND 106T #WORK-FILE1.CIS-RQST-SYS-MIT-SYNC-IND 110T #WORK-FILE1.CIS-ALLOC-SYNC-IND 116T #WORK-FILE1.CIS-ADDR-SYN-IND 121T #WORK-FILE1.CIS-NONP-SYNC-IND
            TabSetting(47),pnd_Work_File1_Cis_Tiaa_Nbr,new TabSetting(68),pnd_Work_File1_Cis_Cert_Nbr,new TabSetting(80),pnd_Work_File1_Cis_Opn_Clsd_Ind,new 
            TabSetting(90),pnd_Work_File1_Cis_Status_Cd,new TabSetting(97),pnd_Work_File1_Cis_Corp_Sync_Ind,new TabSetting(102),pnd_Work_File1_Cis_Cor_Sync_Ind,new 
            TabSetting(106),pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind,new TabSetting(110),pnd_Work_File1_Cis_Alloc_Sync_Ind,new TabSetting(116),pnd_Work_File1_Cis_Addr_Syn_Ind,new 
            TabSetting(121),pnd_Work_File1_Cis_Nonp_Sync_Ind);
        if (Global.isEscape()) return;
    }
    private void sub_Grand_Total() throws Exception                                                                                                                       //Natural: GRAND-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"TOTAL RECORDS EXTRACTED FROM THE PRTCPNT FILE   : ",pnd_Total_Rec,NEWLINE,"TOTAL RECORDS FOUND ON THE PRTCPNT FILE FOR IA  : ", //Natural: WRITE ( 1 ) 'TOTAL RECORDS EXTRACTED FROM THE PRTCPNT FILE   : ' #TOTAL-REC / 'TOTAL RECORDS FOUND ON THE PRTCPNT FILE FOR IA  : ' #REC-FOUND-IA / 'TOTAL RECORS FOUND ON THE PRTCPNT FILE FOR MDO  : ' #REC-FOUND-MDO / 'TOTAL RECORDS FOUND ON THE PRTCPNT FILE         : ' #REC-FOUND / 'TOTAL RECORDS NOT FOUND ON PRTCPNT FILE         : ' #REC-NOTFOUND / 'TOTAL RECORDS UPDATED ON PRTCPNT FILE           : ' #REC-UPDATED /
            pnd_Rec_Found_Ia,NEWLINE,"TOTAL RECORS FOUND ON THE PRTCPNT FILE FOR MDO  : ",pnd_Rec_Found_Mdo,NEWLINE,"TOTAL RECORDS FOUND ON THE PRTCPNT FILE         : ",
            pnd_Rec_Found,NEWLINE,"TOTAL RECORDS NOT FOUND ON PRTCPNT FILE         : ",pnd_Rec_Notfound,NEWLINE,"TOTAL RECORDS UPDATED ON PRTCPNT FILE           : ",
            pnd_Rec_Updated,NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Total_Ia_And_Mdo() throws Exception                                                                                                                  //Natural: TOTAL-IA-AND-MDO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA")))                                                                                                           //Natural: IF #WORK-FILE1.CIS-RQST-ID EQ 'IA'
        {
            if (condition(pnd_Rec_Found_L.getBoolean()))                                                                                                                  //Natural: IF #REC-FOUND-L
            {
                pnd_Rec_Found_Ia.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-FOUND-IA
                pnd_Rec_Found_L.reset();                                                                                                                                  //Natural: RESET #REC-FOUND-L
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Rec_Notfound_L.getBoolean()))                                                                                                               //Natural: IF #REC-NOTFOUND-L
            {
                pnd_Rec_Notfound_Ia.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REC-NOTFOUND-IA
                pnd_Rec_Notfound_L.reset();                                                                                                                               //Natural: RESET #REC-NOTFOUND-L
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Rec_Updated_L.getBoolean()))                                                                                                                //Natural: IF #REC-UPDATED-L
            {
                pnd_Rec_Updated_Ia.nadd(1);                                                                                                                               //Natural: ADD 1 TO #REC-UPDATED-IA
                pnd_Rec_Updated_L.reset();                                                                                                                                //Natural: RESET #REC-UPDATED-L
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  060706
        if (condition(DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'")))                                                                                           //Natural: IF #WORK-FILE1.CIS-RQST-ID EQ MASK ( 'MDO' )
        {
            if (condition(pnd_Rec_Found_L.getBoolean()))                                                                                                                  //Natural: IF #REC-FOUND-L
            {
                pnd_Rec_Found_Mdo.nadd(1);                                                                                                                                //Natural: ADD 1 TO #REC-FOUND-MDO
                pnd_Rec_Found_L.reset();                                                                                                                                  //Natural: RESET #REC-FOUND-L
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Rec_Notfound_L.getBoolean()))                                                                                                               //Natural: IF #REC-NOTFOUND-L
            {
                pnd_Rec_Notfound_Mdo.nadd(1);                                                                                                                             //Natural: ADD 1 TO #REC-NOTFOUND-MDO
                pnd_Rec_Notfound_L.reset();                                                                                                                               //Natural: RESET #REC-NOTFOUND-L
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Rec_Updated_L.getBoolean()))                                                                                                                //Natural: IF #REC-UPDATED-L
            {
                pnd_Rec_Updated_Mdo.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REC-UPDATED-MDO
                pnd_Rec_Updated_L.reset();                                                                                                                                //Natural: RESET #REC-UPDATED-L
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),NEWLINE,new TabSetting(53),"CONSOLIDATED ISSUE SYSTEM",NEWLINE,new                     //Natural: WRITE ( 1 ) NOTITLE *PROGRAM / 53T 'CONSOLIDATED ISSUE SYSTEM' / 50T 'PRTCPNT RECORD UPDATE REPORT' 110T 'RUN DATE:' *DATU / 1T '-' ( 131 ) / 92T '<----- SYNC INDCATORS ------>' / 1T 'REQ ID' 10T 'RQST ID KEY' 42T 'TIAA#' 63T '#CREF#' 75T 'OPN CLSD' 85T 'STATUS' 92T 'CORP' 97T 'COR' 101T 'MIT' 105T 'ALLOC' 111T 'ADDR' 116T 'NONP' / 1T '-' ( 131 ) /
                        TabSetting(50),"PRTCPNT RECORD UPDATE REPORT",new TabSetting(110),"RUN DATE:",Global.getDATU(),NEWLINE,new TabSetting(1),"-",new 
                        RepeatItem(131),NEWLINE,new TabSetting(92),"<----- SYNC INDCATORS ------>",NEWLINE,new TabSetting(1),"REQ ID",new TabSetting(10),"RQST ID KEY",new 
                        TabSetting(42),"TIAA#",new TabSetting(63),"#CREF#",new TabSetting(75),"OPN CLSD",new TabSetting(85),"STATUS",new TabSetting(92),"CORP",new 
                        TabSetting(97),"COR",new TabSetting(101),"MIT",new TabSetting(105),"ALLOC",new TabSetting(111),"ADDR",new TabSetting(116),"NONP",NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(131),NEWLINE);
                    //*     69T 'DOB' 79T 'TIAA#' 90T 'CREF#' 100T 'ISS DATE'
                    //*     110T ' PAYEE / PRD CD / OPT CDE' /
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),NEWLINE,new TabSetting(53),"CENTRALIZED ISSUE SYSTEM",NEWLINE,new TabSetting(50),"PRTCPNT RECORD UPDATE REPORT",new  //Natural: WRITE ( 2 ) NOTITLE *PROGRAM / 53T 'CENTRALIZED ISSUE SYSTEM' / 50T 'PRTCPNT RECORD UPDATE REPORT' 110T 'RUN DATE:' *DATU / 1T '-' ( 131 ) / 92T '<-------------- SYNC INDCATORS ----------------->' / 1T 'REQ ID' 10T 'RQST ID KEY' 42T 'TIAA#' 63T '#CREF#' 75T 'OPN CLSD' 85T 'STATUS' 92T 'CORP' 97T 'COR' 101T 'MIT' 105T 'ALLOC' 111T 'ADDR' 116T 'NONP' / 1T '-' ( 131 ) /
                        TabSetting(110),"RUN DATE:",Global.getDATU(),NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(92),"<-------------- SYNC INDCATORS ----------------->",NEWLINE,new 
                        TabSetting(1),"REQ ID",new TabSetting(10),"RQST ID KEY",new TabSetting(42),"TIAA#",new TabSetting(63),"#CREF#",new TabSetting(75),"OPN CLSD",new 
                        TabSetting(85),"STATUS",new TabSetting(92),"CORP",new TabSetting(97),"COR",new TabSetting(101),"MIT",new TabSetting(105),"ALLOC",new 
                        TabSetting(111),"ADDR",new TabSetting(116),"NONP",NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE);
                    //*     69T 'DOB' 79T 'TIAA#' 90T 'CREF#' 100T 'ISS DATE'
                    //*     110T ' PAYEE / PRD CD / OPT CDE' /
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
                                                                                                                                                                          //Natural: PERFORM NO-RECORD-REPORT
        sub_No_Record_Report();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,"ERROR #",Global.getERROR_NR(),"ERROR LINE",Global.getERROR_LINE());                                                   //Natural: WRITE ( 1 ) 'ERROR #' *ERROR-NR 'ERROR LINE' *ERROR-LINE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
    }
}
