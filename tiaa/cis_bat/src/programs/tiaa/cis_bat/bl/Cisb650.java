/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:21:42 PM
**        * FROM NATURAL PROGRAM : Cisb650
************************************************************
**        * FILE NAME            : Cisb650.java
**        * CLASS NAME           : Cisb650
**        * INSTANCE NAME        : Cisb650
************************************************************
**======================================================================
** SYSTEM         : CIS
** DESCRIPTION    :
**                :
** PRIME KEY      :
** INPUT FILE     :
** OUTPUT FILE    :
** PROGRAM        : CISB650
** GENERATED      :
** FUNCTION       :
**                :
** MODIFICATIONS  :  DATE     MOD BY      DESC OF CHANGE
**                :  2-02-09  K. GATES    RESTOW FOR CISLPART
**                :  6-04-09  K. GATES    RESTOW FOR CISLPART
**                :  3-24-10  C. MASON    RESTOW FOR CISLPART
**                : 10-25-10  G. GUERRERO RESTOW FOR UPDATE OF CISLPART
**                : 12-13-14  W. BAUER    RESTOW FOR CISLPART UPDATES
**                :  5-09-17  GHOSABE     PIN EXPANSION CHANGES(C420007)
**                :                       PINE.
**                : 11-16-19  L. SHU      RESTOW FOR CISLPART UPDATES
**======================================================================

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb650 extends BLNatBase
{
    // Data Areas
    private LdaCislpart ldaCislpart;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Extract_File;
    private DbsField pnd_Extract_File_Pnd_Cntrct_Type;
    private DbsField pnd_Extract_File_Pnd_F1;
    private DbsField pnd_Extract_File_Pnd_Cntrct_Option;
    private DbsField pnd_Extract_File_Pnd_F2;
    private DbsField pnd_Extract_File_Pnd_Tiaa_Cntrct_Number;
    private DbsField pnd_Extract_File_Pnd_F3;
    private DbsField pnd_Extract_File_Pnd_Tiaa_Real_Estate_Num;
    private DbsField pnd_Extract_File_Pnd_F4;
    private DbsField pnd_Extract_File_Pnd_Cref_Cntrct_Number;
    private DbsField pnd_Extract_File_Pnd_F5;
    private DbsField pnd_Extract_File_Pnd_Tiaa_Issue_Date;
    private DbsField pnd_Extract_File_Pnd_F6;
    private DbsField pnd_Extract_File_Pnd_Cref_Issue_Date;
    private DbsField pnd_Extract_File_Pnd_F7;
    private DbsField pnd_Extract_File_Pnd_Freq_Of_Payment;
    private DbsField pnd_Extract_File_Pnd_F8;
    private DbsField pnd_Extract_File_Pnd_First_Payment_Date;
    private DbsField pnd_Extract_File_Pnd_F9;
    private DbsField pnd_Extract_File_Pnd_Last_Payment_Date;
    private DbsField pnd_Extract_File_Pnd_F10;
    private DbsField pnd_Extract_File_Pnd_Guaranteed_Period;
    private DbsField pnd_Extract_File_Pnd_F11;
    private DbsField pnd_Extract_File_Pnd_First_Guaranteed_P_Date;
    private DbsField pnd_Extract_File_Pnd_F12;
    private DbsField pnd_Extract_File_Pnd_End_Guaranteed_P_Date;
    private DbsField pnd_Extract_File_Pnd_F13;
    private DbsField pnd_Extract_File_Pnd_Pin_Number;
    private DbsField pnd_Extract_File_Pnd_F14;
    private DbsField pnd_Extract_File_Pnd_First_Annu_Cntrct_Name;
    private DbsField pnd_Extract_File_Pnd_F15;
    private DbsField pnd_Extract_File_Pnd_First_Annu_Last_Name;
    private DbsField pnd_Extract_File_Pnd_F16;
    private DbsField pnd_Extract_File_Pnd_First_Annu_First_Name;
    private DbsField pnd_Extract_File_Pnd_F17;
    private DbsField pnd_Extract_File_Pnd_First_Annu_Mid_Name;
    private DbsField pnd_Extract_File_Pnd_F18;
    private DbsField pnd_Extract_File_Pnd_First_Annu_Title;
    private DbsField pnd_Extract_File_Pnd_F19;
    private DbsField pnd_Extract_File_Pnd_First_Annu_Suffix;
    private DbsField pnd_Extract_File_Pnd_F20;
    private DbsField pnd_Extract_File_Pnd_First_Annu_Ssn;
    private DbsField pnd_Extract_File_Pnd_F21;
    private DbsField pnd_Extract_File_Pnd_First_Annu_Dob;
    private DbsField pnd_Extract_File_Pnd_F22;
    private DbsField pnd_Extract_File_Pnd_Second_Annuit_Cntrct_Name;
    private DbsField pnd_Extract_File_Pnd_F23;
    private DbsField pnd_Extract_File_Pnd_Second_Annuit_Ssn;
    private DbsField pnd_Extract_File_Pnd_F24;
    private DbsField pnd_Extract_File_Pnd_Second_Annuit_Dob;
    private DbsField pnd_Extract_File_Pnd_F25;
    private DbsField pnd_Extract_File_Pnd_Dollar_G;
    private DbsField pnd_Extract_File_Pnd_Tiaa_Graded_Amt;
    private DbsField pnd_Extract_File_Pnd_F27;
    private DbsField pnd_Extract_File_Pnd_Dollar_Tot;
    private DbsField pnd_Extract_File_Pnd_Tiaa_Standard_Amt;
    private DbsField pnd_Extract_File_Pnd_F29;
    private DbsField pnd_Extract_File_Pnd_Dollar_S;
    private DbsField pnd_Extract_File_Pnd_Tiaa_Tot_Grd_Stnd_Amt;
    private DbsField pnd_Extract_File_Pnd_F31;
    private DbsField pnd_Extract_File_Pnd_Tiaa_Com_Info;

    private DbsGroup pnd_Extract_File__R_Field_1;

    private DbsGroup pnd_Extract_File_Pnd_Redefine_Tiaa_Info;
    private DbsField pnd_Extract_File_Pnd_Dollar_Ci;
    private DbsField pnd_Extract_File_Pnd_Comm_Amt_Ci;
    private DbsField pnd_Extract_File_Pnd_F32a;
    private DbsField pnd_Extract_File_Pnd_Comm_Int_Rate_Ci;
    private DbsField pnd_Extract_File_Pnd_F32b;
    private DbsField pnd_Extract_File_Pnd_Comm_Method_Ci;
    private DbsField pnd_Extract_File_Pnd_F32;
    private DbsField pnd_Extract_File_Pnd_Surivor_Reduction_Literal;
    private DbsField pnd_Extract_File_Pnd_F33;
    private DbsField pnd_Extract_File_Pnd_Dollar_Pros_Sign;
    private DbsField pnd_Extract_File_Pnd_Tot_Tiaa_Paying_Pros_Amt;
    private DbsField pnd_Extract_File_Pnd_F35;
    private DbsField pnd_Extract_File_Pnd_Tiaa_Payin_Cntrct_Info;

    private DbsGroup pnd_Extract_File__R_Field_2;

    private DbsGroup pnd_Extract_File_Pnd_Redefine_Tiaa_C_I;
    private DbsField pnd_Extract_File_Pnd_Da_Payin_Cntrct;

    private DbsGroup pnd_Extract_File__R_Field_3;
    private DbsField pnd_Extract_File_Pnd_Da_Payin_Cntrct_N;
    private DbsField pnd_Extract_File_Pnd_F36a;
    private DbsField pnd_Extract_File_Pnd_Process_Dollar;
    private DbsField pnd_Extract_File_Pnd_Process_Amt;
    private DbsField pnd_Extract_File_Pnd_F36;
    private DbsField pnd_Extract_File_Pnd_Cref_Payout_Info;

    private DbsGroup pnd_Extract_File__R_Field_4;

    private DbsGroup pnd_Extract_File_Pnd_Redefine_Cref_P_Info;
    private DbsField pnd_Extract_File_Pnd_Account;
    private DbsField pnd_Extract_File_Pnd_F37a;
    private DbsField pnd_Extract_File_Pnd_Monthly_Units;
    private DbsField pnd_Extract_File_Pnd_F37b;
    private DbsField pnd_Extract_File_Pnd_Annual_Units;
    private DbsField pnd_Extract_File_Pnd_F37;
    private DbsField pnd_Extract_File_Pnd_Dollar_Tot_Cref_Dollar;
    private DbsField pnd_Extract_File_Pnd_Tot_Cref_Payin_Amt;
    private DbsField pnd_Extract_File_Pnd_F39;
    private DbsField pnd_Extract_File_Pnd_Cref_Payin_Cntrct_Info;

    private DbsGroup pnd_Extract_File__R_Field_5;

    private DbsGroup pnd_Extract_File_Pnd_Redefine_Cref_P_C_Info;
    private DbsField pnd_Extract_File_Pnd_Da_Payin_Contract;
    private DbsField pnd_Extract_File_Pnd_F40a;
    private DbsField pnd_Extract_File_Pnd_Da_Dollar;
    private DbsField pnd_Extract_File_Pnd_Process_Amount;
    private DbsField pnd_Extract_File_Pnd_F40;
    private DbsField pnd_Extract_File_Pnd_Tot_Tiaa_Rea_Dollar;
    private DbsField pnd_Extract_File_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt;
    private DbsField pnd_Extract_File_Pnd_F42;
    private DbsField pnd_Extract_File_Pnd_Tiaa_Rea_Payin_Cntrct_Info;

    private DbsGroup pnd_Extract_File__R_Field_6;

    private DbsGroup pnd_Extract_File_Pnd_Redefine_Tiaa_R_P_C_Info;
    private DbsField pnd_Extract_File_Pnd_Da_Payin_Cntrct_Info;
    private DbsField pnd_Extract_File_Pnd_F43a;
    private DbsField pnd_Extract_File_Pnd_Tiaa_Rea_Payin_Dollar;
    private DbsField pnd_Extract_File_Pnd_Proceeds_Amt_Info;
    private DbsField pnd_Extract_File_Pnd_F43;
    private DbsField pnd_Extract_File_Pnd_Rea_First_Annu_Dollar;
    private DbsField pnd_Extract_File_Pnd_Rea_First_Annuity_Payment;
    private DbsField pnd_Extract_File_Pnd_F45;
    private DbsField pnd_Extract_File_Pnd_Rea_Mnth_Units_Payable;
    private DbsField pnd_Extract_File_Pnd_F46;
    private DbsField pnd_Extract_File_Pnd_Rea_Annu_Units_Payable;
    private DbsField pnd_Extract_File_Pnd_F47;
    private DbsField pnd_Extract_File_Pnd_Rea_Survivor_Units_Payable;
    private DbsField pnd_Extract_File_Pnd_F48;
    private DbsField pnd_Extract_File_Pnd_Cref_Frst_Sign;
    private DbsField pnd_Extract_File_Pnd_Cref_Frst_Pymt_Amt;
    private DbsField pnd_Extract_File_Pnd_F49;
    private DbsField pnd_Extract_File_Pnd_Mit_Log_Dte_Tme;
    private DbsField pnd_Extract_File_Pnd_F50;
    private DbsField pnd_Extract_File_Pnd_Mit_Wpid;
    private DbsField pnd_Extract_File_Pnd_F51;
    private DbsField pnd_Extract_File_Pnd_Pullout_Code;
    private DbsField pnd_Extract_File_Pnd_F52;
    private DbsField pnd_Extract_File_Pnd_Stat_Of_Issue_Code;
    private DbsField pnd_Extract_File_Pnd_F53;
    private DbsField pnd_Extract_File_Pnd_Original_Issue_Stat_Cd;
    private DbsField pnd_Extract_File_Pnd_F54;
    private DbsField pnd_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCislpart = new LdaCislpart();
        registerRecord(ldaCislpart);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Extract_File = localVariables.newGroupInRecord("pnd_Extract_File", "#EXTRACT-FILE");
        pnd_Extract_File_Pnd_Cntrct_Type = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Cntrct_Type", "#CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_F1 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Cntrct_Option = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Cntrct_Option", "#CNTRCT-OPTION", FieldType.STRING, 
            10);
        pnd_Extract_File_Pnd_F2 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Tiaa_Cntrct_Number = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Tiaa_Cntrct_Number", "#TIAA-CNTRCT-NUMBER", FieldType.STRING, 
            12);
        pnd_Extract_File_Pnd_F3 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Tiaa_Real_Estate_Num = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Tiaa_Real_Estate_Num", "#TIAA-REAL-ESTATE-NUM", 
            FieldType.STRING, 16);
        pnd_Extract_File_Pnd_F4 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Cref_Cntrct_Number = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Cref_Cntrct_Number", "#CREF-CNTRCT-NUMBER", FieldType.STRING, 
            12);
        pnd_Extract_File_Pnd_F5 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F5", "#F5", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Tiaa_Issue_Date = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Tiaa_Issue_Date", "#TIAA-ISSUE-DATE", FieldType.STRING, 
            8);
        pnd_Extract_File_Pnd_F6 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F6", "#F6", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Cref_Issue_Date = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Cref_Issue_Date", "#CREF-ISSUE-DATE", FieldType.STRING, 
            8);
        pnd_Extract_File_Pnd_F7 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F7", "#F7", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Freq_Of_Payment = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Freq_Of_Payment", "#FREQ-OF-PAYMENT", FieldType.STRING, 
            13);
        pnd_Extract_File_Pnd_F8 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F8", "#F8", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_First_Payment_Date = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_First_Payment_Date", "#FIRST-PAYMENT-DATE", FieldType.STRING, 
            8);
        pnd_Extract_File_Pnd_F9 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F9", "#F9", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Last_Payment_Date = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Last_Payment_Date", "#LAST-PAYMENT-DATE", FieldType.STRING, 
            8);
        pnd_Extract_File_Pnd_F10 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F10", "#F10", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Guaranteed_Period = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Guaranteed_Period", "#GUARANTEED-PERIOD", FieldType.STRING, 
            18);
        pnd_Extract_File_Pnd_F11 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F11", "#F11", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_First_Guaranteed_P_Date = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_First_Guaranteed_P_Date", "#FIRST-GUARANTEED-P-DATE", 
            FieldType.STRING, 14);
        pnd_Extract_File_Pnd_F12 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F12", "#F12", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_End_Guaranteed_P_Date = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_End_Guaranteed_P_Date", "#END-GUARANTEED-P-DATE", 
            FieldType.STRING, 14);
        pnd_Extract_File_Pnd_F13 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F13", "#F13", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Pin_Number = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.STRING, 12);
        pnd_Extract_File_Pnd_F14 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F14", "#F14", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_First_Annu_Cntrct_Name = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_First_Annu_Cntrct_Name", "#FIRST-ANNU-CNTRCT-NAME", 
            FieldType.STRING, 72);
        pnd_Extract_File_Pnd_F15 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F15", "#F15", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_First_Annu_Last_Name = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_First_Annu_Last_Name", "#FIRST-ANNU-LAST-NAME", 
            FieldType.STRING, 30);
        pnd_Extract_File_Pnd_F16 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F16", "#F16", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_First_Annu_First_Name = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_First_Annu_First_Name", "#FIRST-ANNU-FIRST-NAME", 
            FieldType.STRING, 30);
        pnd_Extract_File_Pnd_F17 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F17", "#F17", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_First_Annu_Mid_Name = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_First_Annu_Mid_Name", "#FIRST-ANNU-MID-NAME", 
            FieldType.STRING, 30);
        pnd_Extract_File_Pnd_F18 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F18", "#F18", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_First_Annu_Title = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_First_Annu_Title", "#FIRST-ANNU-TITLE", FieldType.STRING, 
            8);
        pnd_Extract_File_Pnd_F19 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F19", "#F19", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_First_Annu_Suffix = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_First_Annu_Suffix", "#FIRST-ANNU-SUFFIX", FieldType.STRING, 
            8);
        pnd_Extract_File_Pnd_F20 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F20", "#F20", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_First_Annu_Ssn = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_First_Annu_Ssn", "#FIRST-ANNU-SSN", FieldType.STRING, 
            11);
        pnd_Extract_File_Pnd_F21 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F21", "#F21", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_First_Annu_Dob = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_First_Annu_Dob", "#FIRST-ANNU-DOB", FieldType.STRING, 
            8);
        pnd_Extract_File_Pnd_F22 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F22", "#F22", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Second_Annuit_Cntrct_Name = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Second_Annuit_Cntrct_Name", "#SECOND-ANNUIT-CNTRCT-NAME", 
            FieldType.STRING, 63);
        pnd_Extract_File_Pnd_F23 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F23", "#F23", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Second_Annuit_Ssn = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Second_Annuit_Ssn", "#SECOND-ANNUIT-SSN", FieldType.STRING, 
            11);
        pnd_Extract_File_Pnd_F24 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F24", "#F24", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Second_Annuit_Dob = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Second_Annuit_Dob", "#SECOND-ANNUIT-DOB", FieldType.STRING, 
            8);
        pnd_Extract_File_Pnd_F25 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F25", "#F25", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Dollar_G = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Dollar_G", "#DOLLAR-G", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Tiaa_Graded_Amt = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Tiaa_Graded_Amt", "#TIAA-GRADED-AMT", FieldType.STRING, 
            14);
        pnd_Extract_File_Pnd_F27 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F27", "#F27", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Dollar_Tot = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Dollar_Tot", "#DOLLAR-TOT", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Tiaa_Standard_Amt = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Tiaa_Standard_Amt", "#TIAA-STANDARD-AMT", FieldType.STRING, 
            14);
        pnd_Extract_File_Pnd_F29 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F29", "#F29", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Dollar_S = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Dollar_S", "#DOLLAR-S", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Tiaa_Tot_Grd_Stnd_Amt = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Tiaa_Tot_Grd_Stnd_Amt", "#TIAA-TOT-GRD-STND-AMT", 
            FieldType.STRING, 14);
        pnd_Extract_File_Pnd_F31 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F31", "#F31", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Tiaa_Com_Info = pnd_Extract_File.newFieldArrayInGroup("pnd_Extract_File_Pnd_Tiaa_Com_Info", "#TIAA-COM-INFO", FieldType.STRING, 
            31, new DbsArrayController(1, 5));

        pnd_Extract_File__R_Field_1 = pnd_Extract_File.newGroupInGroup("pnd_Extract_File__R_Field_1", "REDEFINE", pnd_Extract_File_Pnd_Tiaa_Com_Info);

        pnd_Extract_File_Pnd_Redefine_Tiaa_Info = pnd_Extract_File__R_Field_1.newGroupArrayInGroup("pnd_Extract_File_Pnd_Redefine_Tiaa_Info", "#REDEFINE-TIAA-INFO", 
            new DbsArrayController(1, 5));
        pnd_Extract_File_Pnd_Dollar_Ci = pnd_Extract_File_Pnd_Redefine_Tiaa_Info.newFieldInGroup("pnd_Extract_File_Pnd_Dollar_Ci", "#DOLLAR-CI", FieldType.STRING, 
            1);
        pnd_Extract_File_Pnd_Comm_Amt_Ci = pnd_Extract_File_Pnd_Redefine_Tiaa_Info.newFieldInGroup("pnd_Extract_File_Pnd_Comm_Amt_Ci", "#COMM-AMT-CI", 
            FieldType.STRING, 14);
        pnd_Extract_File_Pnd_F32a = pnd_Extract_File_Pnd_Redefine_Tiaa_Info.newFieldInGroup("pnd_Extract_File_Pnd_F32a", "#F32A", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Comm_Int_Rate_Ci = pnd_Extract_File_Pnd_Redefine_Tiaa_Info.newFieldInGroup("pnd_Extract_File_Pnd_Comm_Int_Rate_Ci", "#COMM-INT-RATE-CI", 
            FieldType.STRING, 5);
        pnd_Extract_File_Pnd_F32b = pnd_Extract_File_Pnd_Redefine_Tiaa_Info.newFieldInGroup("pnd_Extract_File_Pnd_F32b", "#F32B", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Comm_Method_Ci = pnd_Extract_File_Pnd_Redefine_Tiaa_Info.newFieldInGroup("pnd_Extract_File_Pnd_Comm_Method_Ci", "#COMM-METHOD-CI", 
            FieldType.STRING, 8);
        pnd_Extract_File_Pnd_F32 = pnd_Extract_File_Pnd_Redefine_Tiaa_Info.newFieldInGroup("pnd_Extract_File_Pnd_F32", "#F32", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Surivor_Reduction_Literal = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Surivor_Reduction_Literal", "#SURIVOR-REDUCTION-LITERAL", 
            FieldType.STRING, 10);
        pnd_Extract_File_Pnd_F33 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F33", "#F33", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Dollar_Pros_Sign = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Dollar_Pros_Sign", "#DOLLAR-PROS-SIGN", FieldType.STRING, 
            1);
        pnd_Extract_File_Pnd_Tot_Tiaa_Paying_Pros_Amt = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Tot_Tiaa_Paying_Pros_Amt", "#TOT-TIAA-PAYING-PROS-AMT", 
            FieldType.STRING, 14);
        pnd_Extract_File_Pnd_F35 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F35", "#F35", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Tiaa_Payin_Cntrct_Info = pnd_Extract_File.newFieldArrayInGroup("pnd_Extract_File_Pnd_Tiaa_Payin_Cntrct_Info", "#TIAA-PAYIN-CNTRCT-INFO", 
            FieldType.STRING, 29, new DbsArrayController(1, 20));

        pnd_Extract_File__R_Field_2 = pnd_Extract_File.newGroupInGroup("pnd_Extract_File__R_Field_2", "REDEFINE", pnd_Extract_File_Pnd_Tiaa_Payin_Cntrct_Info);

        pnd_Extract_File_Pnd_Redefine_Tiaa_C_I = pnd_Extract_File__R_Field_2.newGroupArrayInGroup("pnd_Extract_File_Pnd_Redefine_Tiaa_C_I", "#REDEFINE-TIAA-C-I", 
            new DbsArrayController(1, 20));
        pnd_Extract_File_Pnd_Da_Payin_Cntrct = pnd_Extract_File_Pnd_Redefine_Tiaa_C_I.newFieldInGroup("pnd_Extract_File_Pnd_Da_Payin_Cntrct", "#DA-PAYIN-CNTRCT", 
            FieldType.STRING, 12);

        pnd_Extract_File__R_Field_3 = pnd_Extract_File_Pnd_Redefine_Tiaa_C_I.newGroupInGroup("pnd_Extract_File__R_Field_3", "REDEFINE", pnd_Extract_File_Pnd_Da_Payin_Cntrct);
        pnd_Extract_File_Pnd_Da_Payin_Cntrct_N = pnd_Extract_File__R_Field_3.newFieldInGroup("pnd_Extract_File_Pnd_Da_Payin_Cntrct_N", "#DA-PAYIN-CNTRCT-N", 
            FieldType.NUMERIC, 12);
        pnd_Extract_File_Pnd_F36a = pnd_Extract_File_Pnd_Redefine_Tiaa_C_I.newFieldInGroup("pnd_Extract_File_Pnd_F36a", "#F36A", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Process_Dollar = pnd_Extract_File_Pnd_Redefine_Tiaa_C_I.newFieldInGroup("pnd_Extract_File_Pnd_Process_Dollar", "#PROCESS-$", 
            FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Process_Amt = pnd_Extract_File_Pnd_Redefine_Tiaa_C_I.newFieldInGroup("pnd_Extract_File_Pnd_Process_Amt", "#PROCESS-AMT", 
            FieldType.STRING, 14);
        pnd_Extract_File_Pnd_F36 = pnd_Extract_File_Pnd_Redefine_Tiaa_C_I.newFieldInGroup("pnd_Extract_File_Pnd_F36", "#F36", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Cref_Payout_Info = pnd_Extract_File.newFieldArrayInGroup("pnd_Extract_File_Pnd_Cref_Payout_Info", "#CREF-PAYOUT-INFO", FieldType.STRING, 
            57, new DbsArrayController(1, 20));

        pnd_Extract_File__R_Field_4 = pnd_Extract_File.newGroupInGroup("pnd_Extract_File__R_Field_4", "REDEFINE", pnd_Extract_File_Pnd_Cref_Payout_Info);

        pnd_Extract_File_Pnd_Redefine_Cref_P_Info = pnd_Extract_File__R_Field_4.newGroupArrayInGroup("pnd_Extract_File_Pnd_Redefine_Cref_P_Info", "#REDEFINE-CREF-P-INFO", 
            new DbsArrayController(1, 20));
        pnd_Extract_File_Pnd_Account = pnd_Extract_File_Pnd_Redefine_Cref_P_Info.newFieldInGroup("pnd_Extract_File_Pnd_Account", "#ACCOUNT", FieldType.STRING, 
            30);
        pnd_Extract_File_Pnd_F37a = pnd_Extract_File_Pnd_Redefine_Cref_P_Info.newFieldInGroup("pnd_Extract_File_Pnd_F37a", "#F37A", FieldType.STRING, 
            1);
        pnd_Extract_File_Pnd_Monthly_Units = pnd_Extract_File_Pnd_Redefine_Cref_P_Info.newFieldInGroup("pnd_Extract_File_Pnd_Monthly_Units", "#MONTHLY-UNITS", 
            FieldType.STRING, 12);
        pnd_Extract_File_Pnd_F37b = pnd_Extract_File_Pnd_Redefine_Cref_P_Info.newFieldInGroup("pnd_Extract_File_Pnd_F37b", "#F37B", FieldType.STRING, 
            1);
        pnd_Extract_File_Pnd_Annual_Units = pnd_Extract_File_Pnd_Redefine_Cref_P_Info.newFieldInGroup("pnd_Extract_File_Pnd_Annual_Units", "#ANNUAL-UNITS", 
            FieldType.STRING, 12);
        pnd_Extract_File_Pnd_F37 = pnd_Extract_File_Pnd_Redefine_Cref_P_Info.newFieldInGroup("pnd_Extract_File_Pnd_F37", "#F37", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Dollar_Tot_Cref_Dollar = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Dollar_Tot_Cref_Dollar", "#DOLLAR-TOT-CREF-$", 
            FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Tot_Cref_Payin_Amt = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Tot_Cref_Payin_Amt", "#TOT-CREF-PAYIN-AMT", FieldType.STRING, 
            14);
        pnd_Extract_File_Pnd_F39 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F39", "#F39", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Cref_Payin_Cntrct_Info = pnd_Extract_File.newFieldArrayInGroup("pnd_Extract_File_Pnd_Cref_Payin_Cntrct_Info", "#CREF-PAYIN-CNTRCT-INFO", 
            FieldType.STRING, 29, new DbsArrayController(1, 20));

        pnd_Extract_File__R_Field_5 = pnd_Extract_File.newGroupInGroup("pnd_Extract_File__R_Field_5", "REDEFINE", pnd_Extract_File_Pnd_Cref_Payin_Cntrct_Info);

        pnd_Extract_File_Pnd_Redefine_Cref_P_C_Info = pnd_Extract_File__R_Field_5.newGroupArrayInGroup("pnd_Extract_File_Pnd_Redefine_Cref_P_C_Info", 
            "#REDEFINE-CREF-P-C-INFO", new DbsArrayController(1, 20));
        pnd_Extract_File_Pnd_Da_Payin_Contract = pnd_Extract_File_Pnd_Redefine_Cref_P_C_Info.newFieldInGroup("pnd_Extract_File_Pnd_Da_Payin_Contract", 
            "#DA-PAYIN-CONTRACT", FieldType.STRING, 12);
        pnd_Extract_File_Pnd_F40a = pnd_Extract_File_Pnd_Redefine_Cref_P_C_Info.newFieldInGroup("pnd_Extract_File_Pnd_F40a", "#F40A", FieldType.STRING, 
            1);
        pnd_Extract_File_Pnd_Da_Dollar = pnd_Extract_File_Pnd_Redefine_Cref_P_C_Info.newFieldInGroup("pnd_Extract_File_Pnd_Da_Dollar", "#DA-DOLLAR", FieldType.STRING, 
            1);
        pnd_Extract_File_Pnd_Process_Amount = pnd_Extract_File_Pnd_Redefine_Cref_P_C_Info.newFieldInGroup("pnd_Extract_File_Pnd_Process_Amount", "#PROCESS-AMOUNT", 
            FieldType.STRING, 14);
        pnd_Extract_File_Pnd_F40 = pnd_Extract_File_Pnd_Redefine_Cref_P_C_Info.newFieldInGroup("pnd_Extract_File_Pnd_F40", "#F40", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Tot_Tiaa_Rea_Dollar = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Tot_Tiaa_Rea_Dollar", "#TOT-TIAA-REA-$", FieldType.STRING, 
            1);
        pnd_Extract_File_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt", "#TOT-TIAA-REA-PAYIN-PROC-AMT", 
            FieldType.STRING, 14);
        pnd_Extract_File_Pnd_F42 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F42", "#F42", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Tiaa_Rea_Payin_Cntrct_Info = pnd_Extract_File.newFieldArrayInGroup("pnd_Extract_File_Pnd_Tiaa_Rea_Payin_Cntrct_Info", "#TIAA-REA-PAYIN-CNTRCT-INFO", 
            FieldType.STRING, 29, new DbsArrayController(1, 20));

        pnd_Extract_File__R_Field_6 = pnd_Extract_File.newGroupInGroup("pnd_Extract_File__R_Field_6", "REDEFINE", pnd_Extract_File_Pnd_Tiaa_Rea_Payin_Cntrct_Info);

        pnd_Extract_File_Pnd_Redefine_Tiaa_R_P_C_Info = pnd_Extract_File__R_Field_6.newGroupArrayInGroup("pnd_Extract_File_Pnd_Redefine_Tiaa_R_P_C_Info", 
            "#REDEFINE-TIAA-R-P-C-INFO", new DbsArrayController(1, 20));
        pnd_Extract_File_Pnd_Da_Payin_Cntrct_Info = pnd_Extract_File_Pnd_Redefine_Tiaa_R_P_C_Info.newFieldInGroup("pnd_Extract_File_Pnd_Da_Payin_Cntrct_Info", 
            "#DA-PAYIN-CNTRCT-INFO", FieldType.STRING, 12);
        pnd_Extract_File_Pnd_F43a = pnd_Extract_File_Pnd_Redefine_Tiaa_R_P_C_Info.newFieldInGroup("pnd_Extract_File_Pnd_F43a", "#F43A", FieldType.STRING, 
            1);
        pnd_Extract_File_Pnd_Tiaa_Rea_Payin_Dollar = pnd_Extract_File_Pnd_Redefine_Tiaa_R_P_C_Info.newFieldInGroup("pnd_Extract_File_Pnd_Tiaa_Rea_Payin_Dollar", 
            "#TIAA-REA-PAYIN-$", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Proceeds_Amt_Info = pnd_Extract_File_Pnd_Redefine_Tiaa_R_P_C_Info.newFieldInGroup("pnd_Extract_File_Pnd_Proceeds_Amt_Info", 
            "#PROCEEDS-AMT-INFO", FieldType.STRING, 14);
        pnd_Extract_File_Pnd_F43 = pnd_Extract_File_Pnd_Redefine_Tiaa_R_P_C_Info.newFieldInGroup("pnd_Extract_File_Pnd_F43", "#F43", FieldType.STRING, 
            1);
        pnd_Extract_File_Pnd_Rea_First_Annu_Dollar = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Rea_First_Annu_Dollar", "#REA-FIRST-ANNU-$", 
            FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Rea_First_Annuity_Payment = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Rea_First_Annuity_Payment", "#REA-FIRST-ANNUITY-PAYMENT", 
            FieldType.STRING, 14);
        pnd_Extract_File_Pnd_F45 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F45", "#F45", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Rea_Mnth_Units_Payable = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Rea_Mnth_Units_Payable", "#REA-MNTH-UNITS-PAYABLE", 
            FieldType.STRING, 12);
        pnd_Extract_File_Pnd_F46 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F46", "#F46", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Rea_Annu_Units_Payable = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Rea_Annu_Units_Payable", "#REA-ANNU-UNITS-PAYABLE", 
            FieldType.STRING, 12);
        pnd_Extract_File_Pnd_F47 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F47", "#F47", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Rea_Survivor_Units_Payable = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Rea_Survivor_Units_Payable", "#REA-SURVIVOR-UNITS-PAYABLE", 
            FieldType.STRING, 12);
        pnd_Extract_File_Pnd_F48 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F48", "#F48", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Cref_Frst_Sign = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Cref_Frst_Sign", "#CREF-FRST-SIGN", FieldType.STRING, 
            1);
        pnd_Extract_File_Pnd_Cref_Frst_Pymt_Amt = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Cref_Frst_Pymt_Amt", "#CREF-FRST-PYMT-AMT", FieldType.STRING, 
            14);
        pnd_Extract_File_Pnd_F49 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F49", "#F49", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Mit_Log_Dte_Tme = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Mit_Log_Dte_Tme", "#MIT-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_Extract_File_Pnd_F50 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F50", "#F50", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Mit_Wpid = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Mit_Wpid", "#MIT-WPID", FieldType.STRING, 6);
        pnd_Extract_File_Pnd_F51 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F51", "#F51", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Pullout_Code = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Pullout_Code", "#PULLOUT-CODE", FieldType.STRING, 4);
        pnd_Extract_File_Pnd_F52 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F52", "#F52", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Stat_Of_Issue_Code = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Stat_Of_Issue_Code", "#STAT-OF-ISSUE-CODE", FieldType.STRING, 
            2);
        pnd_Extract_File_Pnd_F53 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F53", "#F53", FieldType.STRING, 1);
        pnd_Extract_File_Pnd_Original_Issue_Stat_Cd = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_Original_Issue_Stat_Cd", "#ORIGINAL-ISSUE-STAT-CD", 
            FieldType.STRING, 2);
        pnd_Extract_File_Pnd_F54 = pnd_Extract_File.newFieldInGroup("pnd_Extract_File_Pnd_F54", "#F54", FieldType.STRING, 1);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCislpart.initializeValues();

        localVariables.reset();
        pnd_Extract_File_Pnd_F1.setInitialValue("�");
        pnd_Extract_File_Pnd_F2.setInitialValue("�");
        pnd_Extract_File_Pnd_F3.setInitialValue("�");
        pnd_Extract_File_Pnd_F4.setInitialValue("�");
        pnd_Extract_File_Pnd_F5.setInitialValue("�");
        pnd_Extract_File_Pnd_F6.setInitialValue("�");
        pnd_Extract_File_Pnd_F7.setInitialValue("�");
        pnd_Extract_File_Pnd_F8.setInitialValue("�");
        pnd_Extract_File_Pnd_F9.setInitialValue("�");
        pnd_Extract_File_Pnd_F10.setInitialValue("�");
        pnd_Extract_File_Pnd_F11.setInitialValue("�");
        pnd_Extract_File_Pnd_F12.setInitialValue("�");
        pnd_Extract_File_Pnd_F13.setInitialValue("�");
        pnd_Extract_File_Pnd_F14.setInitialValue("�");
        pnd_Extract_File_Pnd_F15.setInitialValue("�");
        pnd_Extract_File_Pnd_F16.setInitialValue("�");
        pnd_Extract_File_Pnd_F17.setInitialValue("�");
        pnd_Extract_File_Pnd_F18.setInitialValue("�");
        pnd_Extract_File_Pnd_F19.setInitialValue("�");
        pnd_Extract_File_Pnd_F20.setInitialValue("�");
        pnd_Extract_File_Pnd_F21.setInitialValue("�");
        pnd_Extract_File_Pnd_F22.setInitialValue("�");
        pnd_Extract_File_Pnd_F23.setInitialValue("�");
        pnd_Extract_File_Pnd_F24.setInitialValue("�");
        pnd_Extract_File_Pnd_F25.setInitialValue("�");
        pnd_Extract_File_Pnd_F27.setInitialValue("�");
        pnd_Extract_File_Pnd_F29.setInitialValue("�");
        pnd_Extract_File_Pnd_F31.setInitialValue("�");
        pnd_Extract_File_Pnd_F33.setInitialValue("�");
        pnd_Extract_File_Pnd_F35.setInitialValue("�");
        pnd_Extract_File_Pnd_F39.setInitialValue("�");
        pnd_Extract_File_Pnd_F42.setInitialValue("�");
        pnd_Extract_File_Pnd_F45.setInitialValue("�");
        pnd_Extract_File_Pnd_F46.setInitialValue("�");
        pnd_Extract_File_Pnd_F47.setInitialValue("�");
        pnd_Extract_File_Pnd_F48.setInitialValue("�");
        pnd_Extract_File_Pnd_F49.setInitialValue("�");
        pnd_Extract_File_Pnd_F50.setInitialValue("�");
        pnd_Extract_File_Pnd_F51.setInitialValue("�");
        pnd_Extract_File_Pnd_F52.setInitialValue("�");
        pnd_Extract_File_Pnd_F53.setInitialValue("�");
        pnd_Extract_File_Pnd_F54.setInitialValue("�");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisb650() throws Exception
    {
        super("Cisb650");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        READWORK01:                                                                                                                                                       //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: READ WORK FILE 1 #PARTICIPANT-WORK-FILE
        while (condition(getWorkFiles().read(1, ldaCislpart.getPnd_Participant_Work_File())))
        {
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 1 ) TITLE 'IA UNAPPROVED STATE REPORT' 15X *DATU
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(((ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Type().equals("S")) || (ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Type().equals("S"))  //Natural: IF ( ( #PARTICIPANT-WORK-FILE.#TIAA-CNTRCT-TYPE = 'S' ) OR ( #PARTICIPANT-WORK-FILE.#CREF-CNTRCT-TYPE = 'S' ) OR ( #PARTICIPANT-WORK-FILE.#CNTRCT-APPROVAL = 'N' ) ) AND #PARTICIPANT-WORK-FILE.#CNTRCT-SYS-ID = 'IA'
                || (ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Approval().equals("N"))) && ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Sys_Id().equals("IA")))
            {
                pnd_N.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #N
                //*    MOVE BY NAME #PARTICIPANT-WORK-FILE TO #EXTRACT-FILE
                pnd_Extract_File_Pnd_Cntrct_Type.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Type());                                                    //Natural: MOVE #PARTICIPANT-WORK-FILE.#CNTRCT-TYPE TO #EXTRACT-FILE.#CNTRCT-TYPE
                pnd_Extract_File_Pnd_Cntrct_Option.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Option());                                                //Natural: MOVE #PARTICIPANT-WORK-FILE.#CNTRCT-OPTION TO #EXTRACT-FILE.#CNTRCT-OPTION
                pnd_Extract_File_Pnd_Tiaa_Cntrct_Number.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Number());                                      //Natural: MOVE #PARTICIPANT-WORK-FILE.#TIAA-CNTRCT-NUMBER TO #EXTRACT-FILE.#TIAA-CNTRCT-NUMBER
                pnd_Extract_File_Pnd_Tiaa_Real_Estate_Num.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Real_Estate_Num());                                  //Natural: MOVE #PARTICIPANT-WORK-FILE.#TIAA-REAL-ESTATE-NUM TO #EXTRACT-FILE.#TIAA-REAL-ESTATE-NUM
                pnd_Extract_File_Pnd_Cref_Cntrct_Number.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Number());                                      //Natural: MOVE #PARTICIPANT-WORK-FILE.#CREF-CNTRCT-NUMBER TO #EXTRACT-FILE.#CREF-CNTRCT-NUMBER
                pnd_Extract_File_Pnd_Tiaa_Issue_Date.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Issue_Date());                                            //Natural: MOVE #PARTICIPANT-WORK-FILE.#TIAA-ISSUE-DATE TO #EXTRACT-FILE.#TIAA-ISSUE-DATE
                pnd_Extract_File_Pnd_Cref_Issue_Date.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Issue_Date());                                            //Natural: MOVE #PARTICIPANT-WORK-FILE.#CREF-ISSUE-DATE TO #EXTRACT-FILE.#CREF-ISSUE-DATE
                pnd_Extract_File_Pnd_Freq_Of_Payment.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Freq_Of_Payment());                                            //Natural: MOVE #PARTICIPANT-WORK-FILE.#FREQ-OF-PAYMENT TO #EXTRACT-FILE.#FREQ-OF-PAYMENT
                pnd_Extract_File_Pnd_First_Payment_Date.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Payment_Date());                                      //Natural: MOVE #PARTICIPANT-WORK-FILE.#FIRST-PAYMENT-DATE TO #EXTRACT-FILE.#FIRST-PAYMENT-DATE
                pnd_Extract_File_Pnd_Last_Payment_Date.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Payment_Date());                                        //Natural: MOVE #PARTICIPANT-WORK-FILE.#LAST-PAYMENT-DATE TO #EXTRACT-FILE.#LAST-PAYMENT-DATE
                pnd_Extract_File_Pnd_Guaranteed_Period.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Guaranteed_Period());                                        //Natural: MOVE #PARTICIPANT-WORK-FILE.#GUARANTEED-PERIOD TO #EXTRACT-FILE.#GUARANTEED-PERIOD
                pnd_Extract_File_Pnd_First_Guaranteed_P_Date.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Guaranteed_P_Date());                            //Natural: MOVE #PARTICIPANT-WORK-FILE.#FIRST-GUARANTEED-P-DATE TO #EXTRACT-FILE.#FIRST-GUARANTEED-P-DATE
                pnd_Extract_File_Pnd_End_Guaranteed_P_Date.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_End_Guaranteed_P_Date());                                //Natural: MOVE #PARTICIPANT-WORK-FILE.#END-GUARANTEED-P-DATE TO #EXTRACT-FILE.#END-GUARANTEED-P-DATE
                pnd_Extract_File_Pnd_Pin_Number.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Pin_Number());                                                      //Natural: MOVE #PARTICIPANT-WORK-FILE.#PIN-NUMBER TO #EXTRACT-FILE.#PIN-NUMBER
                pnd_Extract_File_Pnd_First_Annu_Cntrct_Name.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Cntrct_Name());                              //Natural: MOVE #PARTICIPANT-WORK-FILE.#FIRST-ANNU-CNTRCT-NAME TO #EXTRACT-FILE.#FIRST-ANNU-CNTRCT-NAME
                pnd_Extract_File_Pnd_First_Annu_Last_Name.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Last_Name());                                  //Natural: MOVE #PARTICIPANT-WORK-FILE.#FIRST-ANNU-LAST-NAME TO #EXTRACT-FILE.#FIRST-ANNU-LAST-NAME
                pnd_Extract_File_Pnd_First_Annu_First_Name.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_First_Name());                                //Natural: MOVE #PARTICIPANT-WORK-FILE.#FIRST-ANNU-FIRST-NAME TO #EXTRACT-FILE.#FIRST-ANNU-FIRST-NAME
                pnd_Extract_File_Pnd_First_Annu_Mid_Name.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Mid_Name());                                    //Natural: MOVE #PARTICIPANT-WORK-FILE.#FIRST-ANNU-MID-NAME TO #EXTRACT-FILE.#FIRST-ANNU-MID-NAME
                pnd_Extract_File_Pnd_First_Annu_Title.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Title());                                          //Natural: MOVE #PARTICIPANT-WORK-FILE.#FIRST-ANNU-TITLE TO #EXTRACT-FILE.#FIRST-ANNU-TITLE
                pnd_Extract_File_Pnd_First_Annu_Suffix.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Suffix());                                        //Natural: MOVE #PARTICIPANT-WORK-FILE.#FIRST-ANNU-SUFFIX TO #EXTRACT-FILE.#FIRST-ANNU-SUFFIX
                pnd_Extract_File_Pnd_First_Annu_Ssn.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Ssn());                                              //Natural: MOVE #PARTICIPANT-WORK-FILE.#FIRST-ANNU-SSN TO #EXTRACT-FILE.#FIRST-ANNU-SSN
                pnd_Extract_File_Pnd_First_Annu_Dob.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Dob());                                              //Natural: MOVE #PARTICIPANT-WORK-FILE.#FIRST-ANNU-DOB TO #EXTRACT-FILE.#FIRST-ANNU-DOB
                pnd_Extract_File_Pnd_Second_Annuit_Cntrct_Name.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Cntrct_Name());                        //Natural: MOVE #PARTICIPANT-WORK-FILE.#SECOND-ANNUIT-CNTRCT-NAME TO #EXTRACT-FILE.#SECOND-ANNUIT-CNTRCT-NAME
                pnd_Extract_File_Pnd_Second_Annuit_Ssn.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Ssn());                                        //Natural: MOVE #PARTICIPANT-WORK-FILE.#SECOND-ANNUIT-SSN TO #EXTRACT-FILE.#SECOND-ANNUIT-SSN
                pnd_Extract_File_Pnd_Second_Annuit_Dob.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Dob());                                        //Natural: MOVE #PARTICIPANT-WORK-FILE.#SECOND-ANNUIT-DOB TO #EXTRACT-FILE.#SECOND-ANNUIT-DOB
                pnd_Extract_File_Pnd_Dollar_G.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_G());                                                          //Natural: MOVE #PARTICIPANT-WORK-FILE.#DOLLAR-G TO #EXTRACT-FILE.#DOLLAR-G
                pnd_Extract_File_Pnd_Tiaa_Graded_Amt.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Graded_Amt());                                            //Natural: MOVE #PARTICIPANT-WORK-FILE.#TIAA-GRADED-AMT TO #EXTRACT-FILE.#TIAA-GRADED-AMT
                pnd_Extract_File_Pnd_Dollar_Tot.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_Tot());                                                      //Natural: MOVE #PARTICIPANT-WORK-FILE.#DOLLAR-TOT TO #EXTRACT-FILE.#DOLLAR-TOT
                pnd_Extract_File_Pnd_Tiaa_Standard_Amt.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Standard_Amt());                                        //Natural: MOVE #PARTICIPANT-WORK-FILE.#TIAA-STANDARD-AMT TO #EXTRACT-FILE.#TIAA-STANDARD-AMT
                pnd_Extract_File_Pnd_Dollar_S.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_S());                                                          //Natural: MOVE #PARTICIPANT-WORK-FILE.#DOLLAR-S TO #EXTRACT-FILE.#DOLLAR-S
                pnd_Extract_File_Pnd_Tiaa_Tot_Grd_Stnd_Amt.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Tot_Grd_Stnd_Amt());                                //Natural: MOVE #PARTICIPANT-WORK-FILE.#TIAA-TOT-GRD-STND-AMT TO #EXTRACT-FILE.#TIAA-TOT-GRD-STND-AMT
                pnd_Extract_File_Pnd_Tiaa_Com_Info.getValue(1,":",5).setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Com_Info().getValue(1,                    //Natural: MOVE #PARTICIPANT-WORK-FILE.#TIAA-COM-INFO ( 1:5 ) TO #EXTRACT-FILE.#TIAA-COM-INFO ( 1:5 )
                    ":",5));
                pnd_Extract_File_Pnd_Surivor_Reduction_Literal.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Surivor_Reduction_Literal());                        //Natural: MOVE #PARTICIPANT-WORK-FILE.#SURIVOR-REDUCTION-LITERAL TO #EXTRACT-FILE.#SURIVOR-REDUCTION-LITERAL
                pnd_Extract_File_Pnd_Dollar_Pros_Sign.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_Pros_Sign());                                          //Natural: MOVE #PARTICIPANT-WORK-FILE.#DOLLAR-PROS-SIGN TO #EXTRACT-FILE.#DOLLAR-PROS-SIGN
                pnd_Extract_File_Pnd_Tot_Tiaa_Paying_Pros_Amt.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tot_Tiaa_Paying_Pros_Amt());                          //Natural: MOVE #PARTICIPANT-WORK-FILE.#TOT-TIAA-PAYING-PROS-AMT TO #EXTRACT-FILE.#TOT-TIAA-PAYING-PROS-AMT
                pnd_Extract_File_Pnd_Tiaa_Payin_Cntrct_Info.getValue(1,":",20).setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Payin_Cntrct_Info().getValue(1, //Natural: MOVE #PARTICIPANT-WORK-FILE.#TIAA-PAYIN-CNTRCT-INFO ( 1:20 ) TO #EXTRACT-FILE.#TIAA-PAYIN-CNTRCT-INFO ( 1:20 )
                    ":",20));
                pnd_Extract_File_Pnd_Cref_Payout_Info.getValue(1,":",20).setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Payout_Info().getValue(1,             //Natural: MOVE #PARTICIPANT-WORK-FILE.#CREF-PAYOUT-INFO ( 1:20 ) TO #EXTRACT-FILE.#CREF-PAYOUT-INFO ( 1:20 )
                    ":",20));
                pnd_Extract_File_Pnd_Dollar_Tot_Cref_Dollar.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_Tot_Cref_Dollar());                              //Natural: MOVE #PARTICIPANT-WORK-FILE.#DOLLAR-TOT-CREF-$ TO #EXTRACT-FILE.#DOLLAR-TOT-CREF-$
                pnd_Extract_File_Pnd_Tot_Cref_Payin_Amt.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tot_Cref_Payin_Amt());                                      //Natural: MOVE #PARTICIPANT-WORK-FILE.#TOT-CREF-PAYIN-AMT TO #EXTRACT-FILE.#TOT-CREF-PAYIN-AMT
                pnd_Extract_File_Pnd_Cref_Payin_Cntrct_Info.getValue(1,":",20).setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Payin_Cntrct_Info().getValue(1, //Natural: MOVE #PARTICIPANT-WORK-FILE.#CREF-PAYIN-CNTRCT-INFO ( 1:20 ) TO #EXTRACT-FILE.#CREF-PAYIN-CNTRCT-INFO ( 1:20 )
                    ":",20));
                pnd_Extract_File_Pnd_Tot_Tiaa_Rea_Dollar.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tot_Tiaa_Rea_Dollar());                                    //Natural: MOVE #PARTICIPANT-WORK-FILE.#TOT-TIAA-REA-$ TO #EXTRACT-FILE.#TOT-TIAA-REA-$
                pnd_Extract_File_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt());                    //Natural: MOVE #PARTICIPANT-WORK-FILE.#TOT-TIAA-REA-PAYIN-PROC-AMT TO #EXTRACT-FILE.#TOT-TIAA-REA-PAYIN-PROC-AMT
                pnd_Extract_File_Pnd_Tiaa_Rea_Payin_Cntrct_Info.getValue(1,":",20).setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Payin_Cntrct_Info().getValue(1, //Natural: MOVE #PARTICIPANT-WORK-FILE.#TIAA-REA-PAYIN-CNTRCT-INFO ( 1:20 ) TO #EXTRACT-FILE.#TIAA-REA-PAYIN-CNTRCT-INFO ( 1:20 )
                    ":",20));
                pnd_Extract_File_Pnd_Rea_First_Annu_Dollar.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_First_Annu_Dollar());                                //Natural: MOVE #PARTICIPANT-WORK-FILE.#REA-FIRST-ANNU-$ TO #EXTRACT-FILE.#REA-FIRST-ANNU-$
                pnd_Extract_File_Pnd_Rea_First_Annuity_Payment.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_First_Annuity_Payment());                        //Natural: MOVE #PARTICIPANT-WORK-FILE.#REA-FIRST-ANNUITY-PAYMENT TO #EXTRACT-FILE.#REA-FIRST-ANNUITY-PAYMENT
                pnd_Extract_File_Pnd_Rea_Mnth_Units_Payable.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Mnth_Units_Payable());                              //Natural: MOVE #PARTICIPANT-WORK-FILE.#REA-MNTH-UNITS-PAYABLE TO #EXTRACT-FILE.#REA-MNTH-UNITS-PAYABLE
                pnd_Extract_File_Pnd_Rea_Annu_Units_Payable.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Annu_Units_Payable());                              //Natural: MOVE #PARTICIPANT-WORK-FILE.#REA-ANNU-UNITS-PAYABLE TO #EXTRACT-FILE.#REA-ANNU-UNITS-PAYABLE
                pnd_Extract_File_Pnd_Rea_Survivor_Units_Payable.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Survivor_Units_Payable());                      //Natural: MOVE #PARTICIPANT-WORK-FILE.#REA-SURVIVOR-UNITS-PAYABLE TO #EXTRACT-FILE.#REA-SURVIVOR-UNITS-PAYABLE
                pnd_Extract_File_Pnd_Cref_Frst_Sign.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Req_Dis_Dollar());                                       //Natural: MOVE #PARTICIPANT-WORK-FILE.#ANNUAL-REQ-DIS-$ TO #EXTRACT-FILE.#CREF-FRST-SIGN
                pnd_Extract_File_Pnd_Cref_Frst_Pymt_Amt.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Req_Dist());                                         //Natural: MOVE #PARTICIPANT-WORK-FILE.#ANNUAL-REQ-DIST TO #EXTRACT-FILE.#CREF-FRST-PYMT-AMT
                pnd_Extract_File_Pnd_Mit_Log_Dte_Tme.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Log_Dte_Tme());                                            //Natural: MOVE #PARTICIPANT-WORK-FILE.#MIT-LOG-DTE-TME TO #EXTRACT-FILE.#MIT-LOG-DTE-TME
                pnd_Extract_File_Pnd_Mit_Wpid.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Wpid());                                                          //Natural: MOVE #PARTICIPANT-WORK-FILE.#MIT-WPID TO #EXTRACT-FILE.#MIT-WPID
                pnd_Extract_File_Pnd_Pullout_Code.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Pullout_Code());                                                  //Natural: MOVE #PARTICIPANT-WORK-FILE.#PULLOUT-CODE TO #EXTRACT-FILE.#PULLOUT-CODE
                //*  HK >>>>
                pnd_Extract_File_Pnd_Stat_Of_Issue_Code.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code());                                      //Natural: MOVE #PARTICIPANT-WORK-FILE.#STAT-OF-ISSUE-CODE TO #EXTRACT-FILE.#STAT-OF-ISSUE-CODE
                //*  HK <<<<<<
                pnd_Extract_File_Pnd_Original_Issue_Stat_Cd.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Original_Issue_Stat_Cd());                              //Natural: MOVE #PARTICIPANT-WORK-FILE.#ORIGINAL-ISSUE-STAT-CD TO #EXTRACT-FILE.#ORIGINAL-ISSUE-STAT-CD
                pnd_Extract_File_Pnd_F32.getValue("*").setValue("�");                                                                                                     //Natural: ASSIGN #F32 ( * ) := #F36 ( * ) := #F37 ( * ) := #F40 ( * ) := #F43 ( * ) := '�'
                pnd_Extract_File_Pnd_F36.getValue("*").setValue("�");
                pnd_Extract_File_Pnd_F37.getValue("*").setValue("�");
                pnd_Extract_File_Pnd_F40.getValue("*").setValue("�");
                pnd_Extract_File_Pnd_F43.getValue("*").setValue("�");
                pnd_Extract_File_Pnd_F32a.getValue("*").setValue("�");                                                                                                    //Natural: ASSIGN #F32A ( * ) := #F32B ( * ) := #F36A ( * ) := #F37A ( * ) := #F37B ( * ) := '�'
                pnd_Extract_File_Pnd_F32b.getValue("*").setValue("�");
                pnd_Extract_File_Pnd_F36a.getValue("*").setValue("�");
                pnd_Extract_File_Pnd_F37a.getValue("*").setValue("�");
                pnd_Extract_File_Pnd_F37b.getValue("*").setValue("�");
                pnd_Extract_File_Pnd_F40a.getValue("*").setValue("�");                                                                                                    //Natural: ASSIGN #F40A ( * ) := #F43A ( * ) := '�'
                pnd_Extract_File_Pnd_F43a.getValue("*").setValue("�");
                getWorkFiles().write(2, false, pnd_Extract_File);                                                                                                         //Natural: WRITE WORK FILE 2 #EXTRACT-FILE
                getReports().display(1, "SSN NUMBER",                                                                                                                     //Natural: DISPLAY ( 1 ) 'SSN NUMBER' #EXTRACT-FILE.#FIRST-ANNU-SSN 'PIN NUMBER' #EXTRACT-FILE.#PIN-NUMBER 'LAST NAME' #EXTRACT-FILE.#FIRST-ANNU-LAST-NAME ( AL = 15 ) 'FIRST NAME' #EXTRACT-FILE.#FIRST-ANNU-FIRST-NAME ( AL = 15 ) 'TIAA NUMBER' #EXTRACT-FILE.#TIAA-CNTRCT-NUMBER 'CNTRCT TYPE' #EXTRACT-FILE.#CNTRCT-TYPE 'ISSUE STATE' #EXTRACT-FILE.#ORIGINAL-ISSUE-STAT-CD 'OPTION' #EXTRACT-FILE.#CNTRCT-OPTION 'GRNT PERIOD' #EXTRACT-FILE.#GUARANTEED-PERIOD
                		pnd_Extract_File_Pnd_First_Annu_Ssn,"PIN NUMBER",
                		pnd_Extract_File_Pnd_Pin_Number,"LAST NAME",
                		pnd_Extract_File_Pnd_First_Annu_Last_Name, new AlphanumericLength (15),"FIRST NAME",
                		pnd_Extract_File_Pnd_First_Annu_First_Name, new AlphanumericLength (15),"TIAA NUMBER",
                		pnd_Extract_File_Pnd_Tiaa_Cntrct_Number,"CNTRCT TYPE",
                		pnd_Extract_File_Pnd_Cntrct_Type,"ISSUE STATE",
                		pnd_Extract_File_Pnd_Original_Issue_Stat_Cd,"OPTION",
                		pnd_Extract_File_Pnd_Cntrct_Option,"GRNT PERIOD",
                		pnd_Extract_File_Pnd_Guaranteed_Period);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, "TOTAL CONTRACT SENT TO FILE ====> ",pnd_N);                                                                                                //Natural: WRITE ( 1 ) 'TOTAL CONTRACT SENT TO FILE ====> ' #N
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");

        getReports().write(1, ReportOption.TITLE,"IA UNAPPROVED STATE REPORT",new ColumnSpacing(15),Global.getDATU());

        getReports().setDisplayColumns(1, "SSN NUMBER",
        		pnd_Extract_File_Pnd_First_Annu_Ssn,"PIN NUMBER",
        		pnd_Extract_File_Pnd_Pin_Number,"LAST NAME",
        		pnd_Extract_File_Pnd_First_Annu_Last_Name, new AlphanumericLength (15),"FIRST NAME",
        		pnd_Extract_File_Pnd_First_Annu_First_Name, new AlphanumericLength (15),"TIAA NUMBER",
        		pnd_Extract_File_Pnd_Tiaa_Cntrct_Number,"CNTRCT TYPE",
        		pnd_Extract_File_Pnd_Cntrct_Type,"ISSUE STATE",
        		pnd_Extract_File_Pnd_Original_Issue_Stat_Cd,"OPTION",
        		pnd_Extract_File_Pnd_Cntrct_Option,"GRNT PERIOD",
        		pnd_Extract_File_Pnd_Guaranteed_Period);
    }
}
