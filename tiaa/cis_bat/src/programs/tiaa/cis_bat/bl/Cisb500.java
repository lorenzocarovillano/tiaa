/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:21:24 PM
**        * FROM NATURAL PROGRAM : Cisb500
************************************************************
**        * FILE NAME            : Cisb500.java
**        * CLASS NAME           : Cisb500
**        * INSTANCE NAME        : Cisb500
************************************************************
******************************************************************
* PROGRAM     : CISB500
* DESCRIPTION: NAAD FEED FROM CIS IN BATCH.
**----------------------------------------------------------------------
**                       M O D I F I C A T I O N S
**----------------------------------------------------------------------
**    DATE        MOD BY                 DESC OF CHANGE
* 06/02/2006  : DO NOT PASS CHECK MAILNG ADDRESS TO NAAD FOR ICAP
*
* 09/06/2006 O SOTTO CHANGED AS PART OF MDO LEGAL SPLIT. SC 090606.
*
* 02/29/08 DEVELBISS - UPDATE PIN FOR MDO SIP CONTRACTS - SCAN 022908.
* 11/21/08 SOTTO       FIX PROBLEM WITH CHECK MAILING ADDRESS.
*                      SC 112108.
** 02/02/2009  K. GATES    RESTOWED FOR CISPARTV FOR IA TIAA ACCESS PROJ
* 10/25/10 GUERRERO   RESTOW FOR UPDATE OF CISPARTV
* 07/15/11 ELLO       RESIDENCE ADDRESS CAPTURE PROJECT.  SEE BE1.
*                     THE CHANGE IS TO RETRIEVE THE RESIDENCE ADDRESS IN
*                     THE CIS-PARTICIPANT FILE AND WRITE IT AS PART OF
*                     WORK FILE 3 WHICH GOES TO MDM.
* 10/2014 B. NEWSOM  - ACIS/CIS CREF REDESIGN COMMUNICATIONS    (ACCRC)*
*                      RE-STOW FOR CISPARTV                            *
* 09/2015 B. ELLO    - RESTOWED FOR CISPARTV FOR BENE IN PLAN   (BIP)
* 05/09/2017 GHOSABE - PIN EXPANSION CHANGES.  (C420007)    PINE.
* 04/26/2018 L SHU   - DO NOT PASS IA DATA TO MDM IF UDF FIELD (IARPF1)
*                      CIS-SG-TEXT-UDF-3 = 'MDM', IE REQUEST FROM APC
*                      OR FOR AN ACCELERATED ANNUITIZATION
************************************************************************

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb500 extends BLNatBase
{
    // Data Areas
    private LdaCisl500 ldaCisl500;
    private LdaCisl501 ldaCisl501;
    private LdaCispartv ldaCispartv;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_File1;
    private DbsField pnd_Work_File1_Cis_Pin_Nbr;
    private DbsField pnd_Work_File1_Cis_Rqst_Id_Key;
    private DbsField pnd_Work_File1_Cis_Tiaa_Nbr;
    private DbsField pnd_Work_File1_Cis_Cert_Nbr;
    private DbsField pnd_Work_File1_Cis_Tiaa_Doi;

    private DbsGroup pnd_Work_File1__R_Field_1;
    private DbsField pnd_Work_File1_Cis_Tiaa_Doi_A;
    private DbsField pnd_Work_File1_Cis_Cref_Doi;

    private DbsGroup pnd_Work_File1__R_Field_2;
    private DbsField pnd_Work_File1_Cis_Cref_Doi_A;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Ssn;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Prfx;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Frst_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Mid_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Lst_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Sffx;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Dob;

    private DbsGroup pnd_Work_File1__R_Field_3;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Dob_A;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Sex_Cde;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Calc_Method;
    private DbsField pnd_Work_File1_Cis_Opn_Clsd_Ind;
    private DbsField pnd_Work_File1_Cis_Status_Cd;
    private DbsField pnd_Work_File1_Cis_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Cntrct_Apprvl_Ind;
    private DbsField pnd_Work_File1_Cis_Rqst_Id;
    private DbsField pnd_Work_File1_Cis_Hold_Cde;
    private DbsField pnd_Work_File1_Cis_Cntrct_Print_Dte;

    private DbsGroup pnd_Work_File1__R_Field_4;
    private DbsField pnd_Work_File1_Cis_Cntrct_Print_Dte_A;
    private DbsField pnd_Work_File1_Cis_Extract_Date;

    private DbsGroup pnd_Work_File1__R_Field_5;
    private DbsField pnd_Work_File1_Cis_Extract_Date_A;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_Dte;

    private DbsGroup pnd_Work_File1__R_Field_6;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_Dte_A;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_User_Id;
    private DbsField pnd_Work_File1_Cis_Appl_Entry_User_Id;
    private DbsField pnd_Work_File1_Cis_Annty_Option;
    private DbsField pnd_Work_File1_Cis_Pymnt_Mode;
    private DbsField pnd_Work_File1_Cis_Annty_Start_Dte;

    private DbsGroup pnd_Work_File1__R_Field_7;
    private DbsField pnd_Work_File1_Cis_Annty_Start_Dte_A;
    private DbsField pnd_Work_File1_Cis_Annty_End_Dte;

    private DbsGroup pnd_Work_File1__R_Field_8;
    private DbsField pnd_Work_File1_Cis_Annty_End_Dte_A;
    private DbsField pnd_Work_File1_Cis_Grnted_Period_Yrs;
    private DbsField pnd_Work_File1_Cis_Grnted_Period_Dys;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Prfx;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Sffx;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Ssn;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Dob;

    private DbsGroup pnd_Work_File1__R_Field_9;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Dob_A;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde;

    private DbsGroup pnd_Work_File1_Cis_Cref_Annty_Pymnt;
    private DbsField pnd_Work_File1_Cis_Cref_Acct_Cde;
    private DbsField pnd_Work_File1_Cis_Orig_Issue_State;
    private DbsField pnd_Work_File1_Cis_Issue_State_Name;
    private DbsField pnd_Work_File1_Cis_Issue_State_Cd;
    private DbsField pnd_Work_File1_Cis_Ppg_Code;
    private DbsField pnd_Work_File1_Cis_Region_Code;
    private DbsField pnd_Work_File1_Cis_Ownership_Cd;
    private DbsField pnd_Work_File1_Cis_Bill_Code;
    private DbsField pnd_Work_File1_Cis_Lob;
    private DbsField pnd_Work_File1_Cis_Lob_Type;
    private DbsField pnd_Work_File1_Cis_Addr_Syn_Ind;
    private DbsField pnd_Work_File1_Cis_Addr_Process_Env;

    private DbsGroup pnd_Work_File1_Cis_Address_Info;
    private DbsField pnd_Work_File1_Cis_Address_Chg_Ind;
    private DbsField pnd_Work_File1_Cis_Address_Dest_Name;
    private DbsField pnd_Work_File1_Cis_Address_Txt;
    private DbsField pnd_Work_File1_Cis_Zip_Code;
    private DbsField pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Work_File1_Cis_Stndrd_Trn_Cd;
    private DbsField pnd_Work_File1_Cis_Finalist_Reason_Cd;
    private DbsField pnd_Work_File1_Cis_Addr_Usage_Code;
    private DbsField pnd_Work_File1_Cis_Checking_Saving_Cd;
    private DbsField pnd_Work_File1_Cis_Addr_Stndrd_Code;
    private DbsField pnd_Work_File1_Cis_Stndrd_Overide;
    private DbsField pnd_Work_File1_Cis_Postal_Data_Fields;
    private DbsField pnd_Work_File1_Cis_Geographic_Cd;
    private DbsField pnd_Work_File1_Cis_Corp_Sync_Ind;

    private DbsGroup pnd_Work_File1_Cis_Rqst_System_Mit_Dta;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Function;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env;
    private DbsField pnd_Work_File1_Cis_Mit_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Mit_Error_Msg;

    private DbsGroup pnd_Work_File1_Cis_Non_Premium_Dta;
    private DbsField pnd_Work_File1_Cis_Nonp_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Nonp_Process_Env;
    private DbsField pnd_Work_File1_Cis_Nonp_Effective_Dte;

    private DbsGroup pnd_Work_File1__R_Field_10;
    private DbsField pnd_Work_File1_Cis_Nonp_Effective_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Product_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte;

    private DbsGroup pnd_Work_File1__R_Field_11;
    private DbsField pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Currency;
    private DbsField pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt;
    private DbsField pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt;
    private DbsField pnd_Work_File1_Cis_Nonp_Deletion_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Vesting_Dte;

    private DbsGroup pnd_Work_File1__R_Field_12;
    private DbsField pnd_Work_File1_Cis_Nonp_Vesting_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Error_Msg;

    private DbsGroup pnd_Work_File1_Cis_Allocation_Dta;
    private DbsField pnd_Work_File1_Cis_Alloc_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Alloc_Process_Env;
    private DbsField pnd_Work_File1_Cis_Alloc_Effective_Dte;

    private DbsGroup pnd_Work_File1__R_Field_13;
    private DbsField pnd_Work_File1_Cis_Alloc_Effective_Dte_A;
    private DbsField pnd_Work_File1_Cis_Alloc_Percentage;
    private DbsField pnd_Work_File1_Cis_Alloc_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Alloc_Error_Msg;
    private DbsField pnd_Work_File1_Cis_Cor_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Process_Env;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Xref_Pin;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt;

    private DbsGroup pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Mail_Table_Cnt;

    private DbsGroup pnd_Work_File1_Cis_Cor_Ph_Mail_Grp;
    private DbsField pnd_Work_File1_Cis_Cor_Phmail_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin;
    private DbsField pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte;

    private DbsGroup pnd_Work_File1__R_Field_14;
    private DbsField pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Pull_Code;
    private DbsField pnd_Work_File1_Cis_Trnsf_Flag;
    private DbsField pnd_Work_File1_Cis_Trnsf_From_Company;
    private DbsField pnd_Work_File1_Cis_Trnsf_To_Company;
    private DbsField pnd_Work_File1_Cis_Tiaa_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Rea_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Cref_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Decedent_Contract;
    private DbsField pnd_Work_File1_Cis_Relation_To_Decedent;
    private DbsField pnd_Work_File1_Cis_Ssn_Tin_Ind;
    private DbsField pnd_Work_File1_Cis_Isn;
    private DbsField pnd_Work_File1_Cis_Mdo_Contract_Type;
    private DbsField pnd_Work_File1_Cis_Sg_Text_Udf_3;
    private DbsField pnd_J;
    private DbsField pnd_Dob_N;

    private DbsGroup pnd_Dob_N__R_Field_15;
    private DbsField pnd_Dob_N_Pnd_Dob_A;
    private DbsField pnd_Dob;

    private DbsGroup pnd_Dob__R_Field_16;
    private DbsField pnd_Dob_Pnd_Dob_Cc;
    private DbsField pnd_Dob_Pnd_Dob_Yy;
    private DbsField pnd_Dob_Pnd_Dob_Mm;
    private DbsField pnd_Dob_Pnd_Dob_Dd;
    private DbsField pnd_Full_Name;
    private DbsField pnd_N;
    private DbsField pnd_Fr_Nx;
    private DbsField pnd_Lst_Len;
    private DbsField pnd_Frst_Len;
    private DbsField pnd_Mid_Len;
    private DbsField pnd_Addr_Len;
    private DbsField pnd_Begin;
    private DbsField pnd_Begin1;
    private DbsField pnd_Begin2;
    private DbsField pnd_Last_Name;
    private DbsField pnd_Frst_Name;
    private DbsField pnd_Mid_Name;
    private DbsField pnd_Addr_Zp;
    private DbsField pnd_Ext_Rec;
    private DbsField pnd_Tiaa_Rec_Ia;
    private DbsField pnd_Tiaa_Rec_Tpa;
    private DbsField pnd_Tiaa_Rec_Ipro;
    private DbsField pnd_Cref_Rec_Ia;
    private DbsField pnd_Tiaa_Rec_Mdo;
    private DbsField pnd_Cref_Rec_Mdo;
    private DbsField pnd_Ia_Rec;
    private DbsField pnd_Mdo_Rec;
    private DbsField pnd_Total_Ia;
    private DbsField pnd_Total_Tpa;
    private DbsField pnd_Total_Ipro;
    private DbsField pnd_Total_Mdo;
    private DbsField pnd_Hold_Rqst_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCisl500 = new LdaCisl500();
        registerRecord(ldaCisl500);
        ldaCisl501 = new LdaCisl501();
        registerRecord(ldaCisl501);
        ldaCispartv = new LdaCispartv();
        registerRecord(ldaCispartv);
        registerRecord(ldaCispartv.getVw_cis_Part_View());

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_File1 = localVariables.newGroupInRecord("pnd_Work_File1", "#WORK-FILE1");
        pnd_Work_File1_Cis_Pin_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Work_File1_Cis_Rqst_Id_Key = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 35);
        pnd_Work_File1_Cis_Tiaa_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Cert_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Tiaa_Doi = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_1 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_1", "REDEFINE", pnd_Work_File1_Cis_Tiaa_Doi);
        pnd_Work_File1_Cis_Tiaa_Doi_A = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Doi_A", "CIS-TIAA-DOI-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Cref_Doi = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_2 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_2", "REDEFINE", pnd_Work_File1_Cis_Cref_Doi);
        pnd_Work_File1_Cis_Cref_Doi_A = pnd_Work_File1__R_Field_2.newFieldInGroup("pnd_Work_File1_Cis_Cref_Doi_A", "CIS-CREF-DOI-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Ssn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Ssn", "CIS-FRST-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Work_File1_Cis_Frst_Annt_Prfx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Prfx", "CIS-FRST-ANNT-PRFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Frst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Frst_Nme", "CIS-FRST-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Mid_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Mid_Nme", "CIS-FRST-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Lst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Lst_Nme", "CIS-FRST-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Sffx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Sffx", "CIS-FRST-ANNT-SFFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd", "CIS-FRST-ANNT-CTZNSHP-CD", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde", "CIS-FRST-ANNT-RSDNC-CDE", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Frst_Annt_Dob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Dob", "CIS-FRST-ANNT-DOB", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_3 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_3", "REDEFINE", pnd_Work_File1_Cis_Frst_Annt_Dob);
        pnd_Work_File1_Cis_Frst_Annt_Dob_A = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Dob_A", "CIS-FRST-ANNT-DOB-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Sex_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Sex_Cde", "CIS-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Frst_Annt_Calc_Method = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Calc_Method", "CIS-FRST-ANNT-CALC-METHOD", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Opn_Clsd_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Opn_Clsd_Ind", "CIS-OPN-CLSD-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Status_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Status_Cd", "CIS-STATUS-CD", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cntrct_Apprvl_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Apprvl_Ind", "CIS-CNTRCT-APPRVL-IND", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Rqst_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Hold_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Hold_Cde", "CIS-HOLD-CDE", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Cntrct_Print_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Print_Dte", "CIS-CNTRCT-PRINT-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_4 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_4", "REDEFINE", pnd_Work_File1_Cis_Cntrct_Print_Dte);
        pnd_Work_File1_Cis_Cntrct_Print_Dte_A = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Print_Dte_A", "CIS-CNTRCT-PRINT-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Extract_Date = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Extract_Date", "CIS-EXTRACT-DATE", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_5 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_5", "REDEFINE", pnd_Work_File1_Cis_Extract_Date);
        pnd_Work_File1_Cis_Extract_Date_A = pnd_Work_File1__R_Field_5.newFieldInGroup("pnd_Work_File1_Cis_Extract_Date_A", "CIS-EXTRACT-DATE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Rcvd_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_Dte", "CIS-APPL-RCVD-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_6 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_6", "REDEFINE", pnd_Work_File1_Cis_Appl_Rcvd_Dte);
        pnd_Work_File1_Cis_Appl_Rcvd_Dte_A = pnd_Work_File1__R_Field_6.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_Dte_A", "CIS-APPL-RCVD-DTE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Rcvd_User_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_User_Id", "CIS-APPL-RCVD-USER-ID", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Entry_User_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Entry_User_Id", "CIS-APPL-ENTRY-USER-ID", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Annty_Option = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Pymnt_Mode = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pymnt_Mode", "CIS-PYMNT-MODE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Annty_Start_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_Start_Dte", "CIS-ANNTY-START-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_7 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_7", "REDEFINE", pnd_Work_File1_Cis_Annty_Start_Dte);
        pnd_Work_File1_Cis_Annty_Start_Dte_A = pnd_Work_File1__R_Field_7.newFieldInGroup("pnd_Work_File1_Cis_Annty_Start_Dte_A", "CIS-ANNTY-START-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Annty_End_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_End_Dte", "CIS-ANNTY-END-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_8 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_8", "REDEFINE", pnd_Work_File1_Cis_Annty_End_Dte);
        pnd_Work_File1_Cis_Annty_End_Dte_A = pnd_Work_File1__R_Field_8.newFieldInGroup("pnd_Work_File1_Cis_Annty_End_Dte_A", "CIS-ANNTY-END-DTE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Grnted_Period_Yrs = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Grnted_Period_Yrs", "CIS-GRNTED-PERIOD-YRS", FieldType.NUMERIC, 
            2);
        pnd_Work_File1_Cis_Grnted_Period_Dys = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Grnted_Period_Dys", "CIS-GRNTED-PERIOD-DYS", FieldType.NUMERIC, 
            3);
        pnd_Work_File1_Cis_Scnd_Annt_Prfx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Prfx", "CIS-SCND-ANNT-PRFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme", "CIS-SCND-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme", "CIS-SCND-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme", "CIS-SCND-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Sffx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Sffx", "CIS-SCND-ANNT-SFFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Ssn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Work_File1_Cis_Scnd_Annt_Dob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Dob", "CIS-SCND-ANNT-DOB", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_9 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_9", "REDEFINE", pnd_Work_File1_Cis_Scnd_Annt_Dob);
        pnd_Work_File1_Cis_Scnd_Annt_Dob_A = pnd_Work_File1__R_Field_9.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Dob_A", "CIS-SCND-ANNT-DOB-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde", "CIS-SCND-ANNT-SEX-CDE", FieldType.STRING, 
            1);

        pnd_Work_File1_Cis_Cref_Annty_Pymnt = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cref_Annty_Pymnt", "CIS-CREF-ANNTY-PYMNT", new DbsArrayController(1, 
            20));
        pnd_Work_File1_Cis_Cref_Acct_Cde = pnd_Work_File1_Cis_Cref_Annty_Pymnt.newFieldInGroup("pnd_Work_File1_Cis_Cref_Acct_Cde", "CIS-CREF-ACCT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Orig_Issue_State = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Orig_Issue_State", "CIS-ORIG-ISSUE-STATE", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Issue_State_Name = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Issue_State_Name", "CIS-ISSUE-STATE-NAME", FieldType.STRING, 
            15);
        pnd_Work_File1_Cis_Issue_State_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Issue_State_Cd", "CIS-ISSUE-STATE-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Ppg_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ppg_Code", "CIS-PPG-CODE", FieldType.STRING, 6);
        pnd_Work_File1_Cis_Region_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Region_Code", "CIS-REGION-CODE", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Ownership_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ownership_Cd", "CIS-OWNERSHIP-CD", FieldType.NUMERIC, 1);
        pnd_Work_File1_Cis_Bill_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Bill_Code", "CIS-BILL-CODE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Lob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Lob", "CIS-LOB", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Lob_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Lob_Type", "CIS-LOB-TYPE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Syn_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Addr_Syn_Ind", "CIS-ADDR-SYN-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Process_Env = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Addr_Process_Env", "CIS-ADDR-PROCESS-ENV", FieldType.STRING, 
            1);

        pnd_Work_File1_Cis_Address_Info = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Address_Info", "CIS-ADDRESS-INFO", new DbsArrayController(1, 
            3));
        pnd_Work_File1_Cis_Address_Chg_Ind = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Address_Chg_Ind", "CIS-ADDRESS-CHG-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Address_Dest_Name = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Address_Dest_Name", "CIS-ADDRESS-DEST-NAME", 
            FieldType.STRING, 35);
        pnd_Work_File1_Cis_Address_Txt = pnd_Work_File1_Cis_Address_Info.newFieldArrayInGroup("pnd_Work_File1_Cis_Address_Txt", "CIS-ADDRESS-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 5));
        pnd_Work_File1_Cis_Zip_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Zip_Code", "CIS-ZIP-CODE", FieldType.STRING, 
            9);
        pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr", "CIS-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr", "CIS-BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9);
        pnd_Work_File1_Cis_Stndrd_Trn_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Stndrd_Trn_Cd", "CIS-STNDRD-TRN-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Finalist_Reason_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Finalist_Reason_Cd", "CIS-FINALIST-REASON-CD", 
            FieldType.STRING, 10);
        pnd_Work_File1_Cis_Addr_Usage_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Addr_Usage_Code", "CIS-ADDR-USAGE-CODE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Checking_Saving_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Checking_Saving_Cd", "CIS-CHECKING-SAVING-CD", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Stndrd_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Addr_Stndrd_Code", "CIS-ADDR-STNDRD-CODE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Stndrd_Overide = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Stndrd_Overide", "CIS-STNDRD-OVERIDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Postal_Data_Fields = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Postal_Data_Fields", "CIS-POSTAL-DATA-FIELDS", 
            FieldType.STRING, 44);
        pnd_Work_File1_Cis_Geographic_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Geographic_Cd", "CIS-GEOGRAPHIC-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Corp_Sync_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Corp_Sync_Ind", "CIS-CORP-SYNC-IND", FieldType.STRING, 1);

        pnd_Work_File1_Cis_Rqst_System_Mit_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Rqst_System_Mit_Dta", "CIS-RQST-SYSTEM-MIT-DTA");
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind", 
            "CIS-RQST-SYS-MIT-SYNC-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme", 
            "CIS-RQST-SYS-MIT-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Function = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Function", 
            "CIS-RQST-SYS-MIT-FUNCTION", FieldType.STRING, 2);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env", 
            "CIS-RQST-SYS-MIT-PROCESS-ENV", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Mit_Error_Cde = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Mit_Error_Cde", "CIS-MIT-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Mit_Error_Msg = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Mit_Error_Msg", "CIS-MIT-ERROR-MSG", 
            FieldType.STRING, 72);

        pnd_Work_File1_Cis_Non_Premium_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Non_Premium_Dta", "CIS-NON-PREMIUM-DTA");
        pnd_Work_File1_Cis_Nonp_Sync_Ind = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Sync_Ind", "CIS-NONP-SYNC-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Process_Env = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Process_Env", "CIS-NONP-PROCESS-ENV", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Effective_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Effective_Dte", "CIS-NONP-EFFECTIVE-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_10 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_10", "REDEFINE", pnd_Work_File1_Cis_Nonp_Effective_Dte);
        pnd_Work_File1_Cis_Nonp_Effective_Dte_A = pnd_Work_File1__R_Field_10.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Effective_Dte_A", "CIS-NONP-EFFECTIVE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Product_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Product_Cde", "CIS-NONP-PRODUCT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte", "CIS-NONP-CONTRACT-RETR-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_11 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_11", "REDEFINE", pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte);
        pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A = pnd_Work_File1__R_Field_11.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A", "CIS-NONP-CONTRACT-RETR-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt", 
            "CIS-NONP-RATE-ACCUM-ADJ-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Currency = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Currency", "CIS-NONP-CURRENCY", 
            FieldType.NUMERIC, 1);
        pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt", 
            "CIS-NONP-TIAA-AGE-FIRST-PYMNT", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt", 
            "CIS-NONP-CREF-AGE-FIRST-PYMNT", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Deletion_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Deletion_Cde", "CIS-NONP-DELETION-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Vesting_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Vesting_Dte", "CIS-NONP-VESTING-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_12 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_12", "REDEFINE", pnd_Work_File1_Cis_Nonp_Vesting_Dte);
        pnd_Work_File1_Cis_Nonp_Vesting_Dte_A = pnd_Work_File1__R_Field_12.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Vesting_Dte_A", "CIS-NONP-VESTING-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt", 
            "CIS-NONP-CNTR-RATE38-ACCUM-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt", 
            "CIS-NONP-CNTR-RATE42-ACCUM-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Error_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Error_Cde", "CIS-NONP-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Error_Msg = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Error_Msg", "CIS-NONP-ERROR-MSG", 
            FieldType.STRING, 72);

        pnd_Work_File1_Cis_Allocation_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Allocation_Dta", "CIS-ALLOCATION-DTA");
        pnd_Work_File1_Cis_Alloc_Sync_Ind = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Sync_Ind", "CIS-ALLOC-SYNC-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Alloc_Process_Env = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Process_Env", "CIS-ALLOC-PROCESS-ENV", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Alloc_Effective_Dte = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Effective_Dte", "CIS-ALLOC-EFFECTIVE-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_13 = pnd_Work_File1_Cis_Allocation_Dta.newGroupInGroup("pnd_Work_File1__R_Field_13", "REDEFINE", pnd_Work_File1_Cis_Alloc_Effective_Dte);
        pnd_Work_File1_Cis_Alloc_Effective_Dte_A = pnd_Work_File1__R_Field_13.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Effective_Dte_A", "CIS-ALLOC-EFFECTIVE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Alloc_Percentage = pnd_Work_File1_Cis_Allocation_Dta.newFieldArrayInGroup("pnd_Work_File1_Cis_Alloc_Percentage", "CIS-ALLOC-PERCENTAGE", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20));
        pnd_Work_File1_Cis_Alloc_Error_Cde = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Error_Cde", "CIS-ALLOC-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Alloc_Error_Msg = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Error_Msg", "CIS-ALLOC-ERROR-MSG", 
            FieldType.STRING, 72);
        pnd_Work_File1_Cis_Cor_Sync_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Sync_Ind", "CIS-COR-SYNC-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Process_Env = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Process_Env", "CIS-COR-PROCESS-ENV", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde", "CIS-COR-PH-RCD-TYP-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde", "CIS-COR-PH-NEG-ELECTION-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme", "CIS-COR-PH-OCCUPNT-NME", FieldType.STRING, 
            10);
        pnd_Work_File1_Cis_Cor_Ph_Xref_Pin = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Xref_Pin", "CIS-COR-PH-XREF-PIN", FieldType.NUMERIC, 
            12);
        pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt", "CIS-COR-PH-ACTIVE-VIP-CNT", 
            FieldType.NUMERIC, 2);

        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp", "CIS-COR-PH-ACTVE-VIP-GRP", 
            new DbsArrayController(1, 8));
        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde = pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde", "CIS-COR-PH-ACTVE-VIP-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde = pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde", 
            "CIS-COR-PH-ACTVE-VIP-AREA-CDE", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Cor_Mail_Table_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Mail_Table_Cnt", "CIS-COR-MAIL-TABLE-CNT", FieldType.NUMERIC, 
            2);

        pnd_Work_File1_Cis_Cor_Ph_Mail_Grp = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cor_Ph_Mail_Grp", "CIS-COR-PH-MAIL-GRP", new DbsArrayController(1, 
            25));
        pnd_Work_File1_Cis_Cor_Phmail_Cde = pnd_Work_File1_Cis_Cor_Ph_Mail_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Phmail_Cde", "CIS-COR-PHMAIL-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin = pnd_Work_File1_Cis_Cor_Ph_Mail_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin", 
            "CIS-COR-PHMAIL-AREA-OF-ORIGIN", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte", "CIS-COR-INST-PH-RMTTNG-INSTN-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_14 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_14", "REDEFINE", pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte);
        pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta = pnd_Work_File1__R_Field_14.newFieldInGroup("pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta", 
            "CIS-COR-INST-PH-RMTTNG-INSTN-DTA", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr", "CIS-COR-PH-RMTTNG-INSTN-NBR", 
            FieldType.NUMERIC, 5);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind", "CIS-COR-PH-RMTTNG-INSTN-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind", "CIS-COR-PH-RMMTNG-INSTN-PDUP-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde", "CIS-COR-PH-RMTTNG-NMBR-RANGE-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind", "CIS-COR-PH-MCRJCK-MEDIA-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde", "CIS-COR-PH-MCRJCK-CNTNTS-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt", "CIS-COR-CNTRCT-TABLE-CNT", 
            FieldType.NUMERIC, 2);
        pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde", "CIS-COR-CNTRCT-STATUS-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte", "CIS-COR-CNTRCT-STATUS-YR-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Error_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Error_Cde", "CIS-COR-ERROR-CDE", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Pull_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pull_Code", "CIS-PULL-CODE", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Trnsf_Flag = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_Flag", "CIS-TRNSF-FLAG", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Trnsf_From_Company = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_From_Company", "CIS-TRNSF-FROM-COMPANY", FieldType.STRING, 
            4);
        pnd_Work_File1_Cis_Trnsf_To_Company = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_To_Company", "CIS-TRNSF-TO-COMPANY", FieldType.STRING, 
            4);
        pnd_Work_File1_Cis_Tiaa_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Cntrct_Type", "CIS-TIAA-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Rea_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rea_Cntrct_Type", "CIS-REA-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Cref_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cref_Cntrct_Type", "CIS-CREF-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Decedent_Contract = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Decedent_Contract", "CIS-DECEDENT-CONTRACT", FieldType.STRING, 
            10);
        pnd_Work_File1_Cis_Relation_To_Decedent = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Relation_To_Decedent", "CIS-RELATION-TO-DECEDENT", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Ssn_Tin_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ssn_Tin_Ind", "CIS-SSN-TIN-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Isn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Isn", "CIS-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Work_File1_Cis_Mdo_Contract_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Mdo_Contract_Type", "CIS-MDO-CONTRACT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Sg_Text_Udf_3 = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", FieldType.STRING, 10);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_Dob_N = localVariables.newFieldInRecord("pnd_Dob_N", "#DOB-N", FieldType.NUMERIC, 6);

        pnd_Dob_N__R_Field_15 = localVariables.newGroupInRecord("pnd_Dob_N__R_Field_15", "REDEFINE", pnd_Dob_N);
        pnd_Dob_N_Pnd_Dob_A = pnd_Dob_N__R_Field_15.newFieldInGroup("pnd_Dob_N_Pnd_Dob_A", "#DOB-A", FieldType.STRING, 6);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.STRING, 8);

        pnd_Dob__R_Field_16 = localVariables.newGroupInRecord("pnd_Dob__R_Field_16", "REDEFINE", pnd_Dob);
        pnd_Dob_Pnd_Dob_Cc = pnd_Dob__R_Field_16.newFieldInGroup("pnd_Dob_Pnd_Dob_Cc", "#DOB-CC", FieldType.STRING, 2);
        pnd_Dob_Pnd_Dob_Yy = pnd_Dob__R_Field_16.newFieldInGroup("pnd_Dob_Pnd_Dob_Yy", "#DOB-YY", FieldType.STRING, 2);
        pnd_Dob_Pnd_Dob_Mm = pnd_Dob__R_Field_16.newFieldInGroup("pnd_Dob_Pnd_Dob_Mm", "#DOB-MM", FieldType.STRING, 2);
        pnd_Dob_Pnd_Dob_Dd = pnd_Dob__R_Field_16.newFieldInGroup("pnd_Dob_Pnd_Dob_Dd", "#DOB-DD", FieldType.STRING, 2);
        pnd_Full_Name = localVariables.newFieldInRecord("pnd_Full_Name", "#FULL-NAME", FieldType.STRING, 35);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.INTEGER, 2);
        pnd_Fr_Nx = localVariables.newFieldInRecord("pnd_Fr_Nx", "#FR-NX", FieldType.INTEGER, 2);
        pnd_Lst_Len = localVariables.newFieldInRecord("pnd_Lst_Len", "#LST-LEN", FieldType.INTEGER, 2);
        pnd_Frst_Len = localVariables.newFieldInRecord("pnd_Frst_Len", "#FRST-LEN", FieldType.INTEGER, 2);
        pnd_Mid_Len = localVariables.newFieldInRecord("pnd_Mid_Len", "#MID-LEN", FieldType.INTEGER, 2);
        pnd_Addr_Len = localVariables.newFieldInRecord("pnd_Addr_Len", "#ADDR-LEN", FieldType.INTEGER, 2);
        pnd_Begin = localVariables.newFieldInRecord("pnd_Begin", "#BEGIN", FieldType.INTEGER, 2);
        pnd_Begin1 = localVariables.newFieldInRecord("pnd_Begin1", "#BEGIN1", FieldType.INTEGER, 2);
        pnd_Begin2 = localVariables.newFieldInRecord("pnd_Begin2", "#BEGIN2", FieldType.INTEGER, 2);
        pnd_Last_Name = localVariables.newFieldInRecord("pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 30);
        pnd_Frst_Name = localVariables.newFieldInRecord("pnd_Frst_Name", "#FRST-NAME", FieldType.STRING, 30);
        pnd_Mid_Name = localVariables.newFieldInRecord("pnd_Mid_Name", "#MID-NAME", FieldType.STRING, 30);
        pnd_Addr_Zp = localVariables.newFieldInRecord("pnd_Addr_Zp", "#ADDR-ZP", FieldType.STRING, 35);
        pnd_Ext_Rec = localVariables.newFieldInRecord("pnd_Ext_Rec", "#EXT-REC", FieldType.NUMERIC, 5);
        pnd_Tiaa_Rec_Ia = localVariables.newFieldInRecord("pnd_Tiaa_Rec_Ia", "#TIAA-REC-IA", FieldType.NUMERIC, 5);
        pnd_Tiaa_Rec_Tpa = localVariables.newFieldInRecord("pnd_Tiaa_Rec_Tpa", "#TIAA-REC-TPA", FieldType.NUMERIC, 5);
        pnd_Tiaa_Rec_Ipro = localVariables.newFieldInRecord("pnd_Tiaa_Rec_Ipro", "#TIAA-REC-IPRO", FieldType.NUMERIC, 5);
        pnd_Cref_Rec_Ia = localVariables.newFieldInRecord("pnd_Cref_Rec_Ia", "#CREF-REC-IA", FieldType.NUMERIC, 5);
        pnd_Tiaa_Rec_Mdo = localVariables.newFieldInRecord("pnd_Tiaa_Rec_Mdo", "#TIAA-REC-MDO", FieldType.NUMERIC, 5);
        pnd_Cref_Rec_Mdo = localVariables.newFieldInRecord("pnd_Cref_Rec_Mdo", "#CREF-REC-MDO", FieldType.NUMERIC, 5);
        pnd_Ia_Rec = localVariables.newFieldInRecord("pnd_Ia_Rec", "#IA-REC", FieldType.NUMERIC, 5);
        pnd_Mdo_Rec = localVariables.newFieldInRecord("pnd_Mdo_Rec", "#MDO-REC", FieldType.NUMERIC, 5);
        pnd_Total_Ia = localVariables.newFieldInRecord("pnd_Total_Ia", "#TOTAL-IA", FieldType.NUMERIC, 5);
        pnd_Total_Tpa = localVariables.newFieldInRecord("pnd_Total_Tpa", "#TOTAL-TPA", FieldType.NUMERIC, 5);
        pnd_Total_Ipro = localVariables.newFieldInRecord("pnd_Total_Ipro", "#TOTAL-IPRO", FieldType.NUMERIC, 5);
        pnd_Total_Mdo = localVariables.newFieldInRecord("pnd_Total_Mdo", "#TOTAL-MDO", FieldType.NUMERIC, 5);
        pnd_Hold_Rqst_Id = localVariables.newFieldInRecord("pnd_Hold_Rqst_Id", "#HOLD-RQST-ID", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCisl500.initializeValues();
        ldaCisl501.initializeValues();
        ldaCispartv.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisb500() throws Exception
    {
        super("Cisb500");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  PINE
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        RD:                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 );//Natural: READ WORK 1 #WORK-FILE1
        while (condition(getWorkFiles().read(1, pnd_Work_File1)))
        {
            ldaCisl500.getCisl500().reset();                                                                                                                              //Natural: RESET CISL500 #BEGIN #BEGIN1 #BEGIN2 #FULL-NAME #FR-NX #FRST-NAME #LAST-NAME #MID-NAME #ADDR-ZP
            pnd_Begin.reset();
            pnd_Begin1.reset();
            pnd_Begin2.reset();
            pnd_Full_Name.reset();
            pnd_Fr_Nx.reset();
            pnd_Frst_Name.reset();
            pnd_Last_Name.reset();
            pnd_Mid_Name.reset();
            pnd_Addr_Zp.reset();
            //*  IARPF1
            if (condition(pnd_Work_File1_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                                //Natural: REJECT IF #WORK-FILE1.CIS-SG-TEXT-UDF-3 = 'MDM'
            {
                continue;
            }
            if (condition(!(pnd_Work_File1_Cis_Addr_Syn_Ind.equals("Y"))))                                                                                                //Natural: ACCEPT IF #WORK-FILE1.CIS-ADDR-SYN-IND = 'Y'
            {
                continue;
            }
            //* HK
            if (condition(!(pnd_Work_File1_Cis_Addr_Syn_Ind.equals("S"))))                                                                                                //Natural: ACCEPT IF #WORK-FILE1.CIS-ADDR-SYN-IND = 'S'
            {
                continue;
            }
            pnd_Ext_Rec.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #EXT-REC
            if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("IPRO")                //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IA' OR #WORK-FILE1.CIS-RQST-ID = 'TPA' OR #WORK-FILE1.CIS-RQST-ID = 'IPRO' OR #WORK-FILE1.CIS-RQST-ID = 'TPI' OR #WORK-FILE1.CIS-RQST-ID = 'IPI'
                || pnd_Work_File1_Cis_Rqst_Id.equals("TPI") || pnd_Work_File1_Cis_Rqst_Id.equals("IPI")))
            {
                if (condition(pnd_Ext_Rec.equals(1)))                                                                                                                     //Natural: IF #EXT-REC EQ 1
                {
                    pnd_Hold_Rqst_Id.setValue(pnd_Work_File1_Cis_Rqst_Id);                                                                                                //Natural: ASSIGN #HOLD-RQST-ID := #WORK-FILE1.CIS-RQST-ID
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("IPRO")                //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IA' OR #WORK-FILE1.CIS-RQST-ID = 'TPA' OR #WORK-FILE1.CIS-RQST-ID = 'IPRO' OR #WORK-FILE1.CIS-RQST-ID = 'TPI' OR #WORK-FILE1.CIS-RQST-ID = 'IPI'
                || pnd_Work_File1_Cis_Rqst_Id.equals("TPI") || pnd_Work_File1_Cis_Rqst_Id.equals("IPI")))
            {
                if (condition(pnd_Hold_Rqst_Id.notEquals(pnd_Work_File1_Cis_Rqst_Id)))                                                                                    //Natural: IF #HOLD-RQST-ID NE #WORK-FILE1.CIS-RQST-ID
                {
                    if (condition(pnd_Total_Ia.greater(getZero())))                                                                                                       //Natural: IF #TOTAL-IA GT 0
                    {
                                                                                                                                                                          //Natural: PERFORM IA-TOTALS-END-OF-RPT
                        sub_Ia_Totals_End_Of_Rpt();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Total_Ia.reset();                                                                                                                                 //Natural: RESET #TOTAL-IA #IA-REC #TIAA-REC-IA #CREF-REC-IA
                    pnd_Ia_Rec.reset();
                    pnd_Tiaa_Rec_Ia.reset();
                    pnd_Cref_Rec_Ia.reset();
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Hold_Rqst_Id.setValue(pnd_Work_File1_Cis_Rqst_Id);                                                                                                //Natural: ASSIGN #HOLD-RQST-ID := #WORK-FILE1.CIS-RQST-ID
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("IPRO")                //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IA' OR #WORK-FILE1.CIS-RQST-ID = 'TPA' OR #WORK-FILE1.CIS-RQST-ID = 'IPRO' OR #WORK-FILE1.CIS-RQST-ID = 'TPI' OR #WORK-FILE1.CIS-RQST-ID = 'IPI'
                || pnd_Work_File1_Cis_Rqst_Id.equals("TPI") || pnd_Work_File1_Cis_Rqst_Id.equals("IPI")))
            {
                pnd_Total_Ia.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #TOTAL-IA
            }                                                                                                                                                             //Natural: END-IF
            //*  090606
            if (condition(DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'")))                                                                                       //Natural: IF #WORK-FILE1.CIS-RQST-ID = MASK ( 'MDO' )
            {
                pnd_Total_Mdo.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOTAL-MDO
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Cntrct_Type.equals("T") || pnd_Work_File1_Cis_Cntrct_Type.equals("B")))                                                      //Natural: IF #WORK-FILE1.CIS-CNTRCT-TYPE = 'T' OR = 'B'
            {
                if (condition(pnd_Work_File1_Cis_Addr_Usage_Code.getValue(1).equals("1") || pnd_Work_File1_Cis_Addr_Usage_Code.getValue(1).equals("3")))                  //Natural: IF #WORK-FILE1.CIS-ADDR-USAGE-CODE ( 1 ) = '1' OR = '3'
                {
                    ldaCisl500.getCisl500_Cis_Contract_Nbr().setValue(pnd_Work_File1_Cis_Tiaa_Nbr);                                                                       //Natural: ASSIGN CISL500.CIS-CONTRACT-NBR := #WORK-FILE1.CIS-TIAA-NBR
                    if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("IPRO")        //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IA' OR #WORK-FILE1.CIS-RQST-ID = 'TPA' OR #WORK-FILE1.CIS-RQST-ID = 'IPRO' OR #WORK-FILE1.CIS-RQST-ID = 'TPI' OR #WORK-FILE1.CIS-RQST-ID = 'IPI'
                        || pnd_Work_File1_Cis_Rqst_Id.equals("TPI") || pnd_Work_File1_Cis_Rqst_Id.equals("IPI")))
                    {
                        pnd_Tiaa_Rec_Ia.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TIAA-REC-IA
                    }                                                                                                                                                     //Natural: END-IF
                    //*  090606
                    if (condition(DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'")))                                                                               //Natural: IF #WORK-FILE1.CIS-RQST-ID = MASK ( 'MDO' )
                    {
                        pnd_Tiaa_Rec_Mdo.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TIAA-REC-MDO
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CORRESPONDING-ADDRESS
                    sub_Corresponding_Address();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM ASSIGN-VAR
                    sub_Assign_Var();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Work_File1_Cis_Addr_Usage_Code.getValue(2).equals("2")))                                                                                //Natural: IF #WORK-FILE1.CIS-ADDR-USAGE-CODE ( 2 ) = '2'
                {
                    ldaCisl500.getCisl500_Cis_Contract_Nbr().setValue(pnd_Work_File1_Cis_Tiaa_Nbr);                                                                       //Natural: ASSIGN CISL500.CIS-CONTRACT-NBR := #WORK-FILE1.CIS-TIAA-NBR
                    if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("IPRO")        //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IA' OR #WORK-FILE1.CIS-RQST-ID = 'TPA' OR #WORK-FILE1.CIS-RQST-ID = 'IPRO' OR #WORK-FILE1.CIS-RQST-ID = 'TPI' OR #WORK-FILE1.CIS-RQST-ID = 'IPI'
                        || pnd_Work_File1_Cis_Rqst_Id.equals("TPI") || pnd_Work_File1_Cis_Rqst_Id.equals("IPI")))
                    {
                        pnd_Tiaa_Rec_Ia.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TIAA-REC-IA
                    }                                                                                                                                                     //Natural: END-IF
                    //*  090606
                    if (condition(DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'")))                                                                               //Natural: IF #WORK-FILE1.CIS-RQST-ID = MASK ( 'MDO' )
                    {
                        pnd_Tiaa_Rec_Mdo.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TIAA-REC-MDO
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Work_File1_Cis_Rqst_Id.notEquals("TPI") && pnd_Work_File1_Cis_Rqst_Id.notEquals("IPI")))                                            //Natural: IF #WORK-FILE1.CIS-RQST-ID NE 'TPI' AND #WORK-FILE1.CIS-RQST-ID NE 'IPI'
                    {
                                                                                                                                                                          //Natural: PERFORM CHECK-MAILING-ADDRESS
                        sub_Check_Mailing_Address();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ASSIGN-VAR
                        sub_Assign_Var();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  090606
            if (condition(! (DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'"))))                                                                                   //Natural: IF #WORK-FILE1.CIS-RQST-ID NE MASK ( 'MDO' )
            {
                if (condition(pnd_Work_File1_Cis_Cntrct_Type.equals("C") || pnd_Work_File1_Cis_Cntrct_Type.equals("B")))                                                  //Natural: IF #WORK-FILE1.CIS-CNTRCT-TYPE = 'C' OR = 'B'
                {
                    if (condition(pnd_Work_File1_Cis_Addr_Usage_Code.getValue(1).equals("1") || pnd_Work_File1_Cis_Addr_Usage_Code.getValue(1).equals("3")))              //Natural: IF #WORK-FILE1.CIS-ADDR-USAGE-CODE ( 1 ) = '1' OR = '3'
                    {
                        ldaCisl500.getCisl500_Cis_Contract_Nbr().setValue(pnd_Work_File1_Cis_Cert_Nbr);                                                                   //Natural: ASSIGN CISL500.CIS-CONTRACT-NBR := #WORK-FILE1.CIS-CERT-NBR
                        if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("IPRO")    //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IA' OR #WORK-FILE1.CIS-RQST-ID = 'TPA' OR #WORK-FILE1.CIS-RQST-ID = 'IPRO' OR #WORK-FILE1.CIS-RQST-ID = 'TPI' OR #WORK-FILE1.CIS-RQST-ID = 'IPI'
                            || pnd_Work_File1_Cis_Rqst_Id.equals("TPI") || pnd_Work_File1_Cis_Rqst_Id.equals("IPI")))
                        {
                            pnd_Cref_Rec_Ia.nadd(1);                                                                                                                      //Natural: ADD 1 TO #CREF-REC-IA
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Work_File1_Cis_Rqst_Id.notEquals("TPI") && pnd_Work_File1_Cis_Rqst_Id.notEquals("IPI")))                                        //Natural: IF #WORK-FILE1.CIS-RQST-ID NE 'TPI' AND #WORK-FILE1.CIS-RQST-ID NE 'IPI'
                        {
                                                                                                                                                                          //Natural: PERFORM CORRESPONDING-ADDRESS
                            sub_Corresponding_Address();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                                                                                                                                                                          //Natural: PERFORM ASSIGN-VAR
                            sub_Assign_Var();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Work_File1_Cis_Addr_Usage_Code.getValue(2).equals("2")))                                                                            //Natural: IF #WORK-FILE1.CIS-ADDR-USAGE-CODE ( 2 ) = '2'
                    {
                        ldaCisl500.getCisl500_Cis_Contract_Nbr().setValue(pnd_Work_File1_Cis_Cert_Nbr);                                                                   //Natural: ASSIGN CISL500.CIS-CONTRACT-NBR := #WORK-FILE1.CIS-CERT-NBR
                        if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("IPRO")    //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IA' OR #WORK-FILE1.CIS-RQST-ID = 'TPA' OR #WORK-FILE1.CIS-RQST-ID = 'IPRO' OR #WORK-FILE1.CIS-RQST-ID = 'TPI' OR #WORK-FILE1.CIS-RQST-ID = 'IPI'
                            || pnd_Work_File1_Cis_Rqst_Id.equals("TPI") || pnd_Work_File1_Cis_Rqst_Id.equals("IPI")))
                        {
                            pnd_Cref_Rec_Ia.nadd(1);                                                                                                                      //Natural: ADD 1 TO #CREF-REC-IA
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-MAILING-ADDRESS
                        sub_Check_Mailing_Address();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ASSIGN-VAR
                        sub_Assign_Var();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        RD_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM IA-TOTALS-END-OF-RPT
            sub_Ia_Totals_End_Of_Rpt();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MDO-TOTALS-END-OF-RPT
            sub_Mdo_Totals_End_Of_Rpt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CORRESPONDING-ADDRESS
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-MAILING-ADDRESS
        //* *********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-VAR
        //* * CISL500.CIS-CNTRCT-OWNERSHIP      := #WORK-FILE1.CIS-OWNERSHIP-CD
        //* *********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUBSTRING-NAME
        //* ************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-MID-NAME
        //* *********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SCND-ANNT-SUBSTRING-NAME
        //* ************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-SCND-ANNT-MID-NAME
        //* *********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPRESS-ADDR-ZIPCODE
        //* *********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-IA-REPORT
        //* *********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-MDO-REPORT
        //* *********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-TOTALS-END-OF-RPT
        //* *********************************************************
        //* *********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDO-TOTALS-END-OF-RPT
        //* *********************************************************
        //*             10X ' * Number of CREF Records:     ' #CREF-REC-MDO  '*' /
        //* *********************************************************
    }
    private void sub_Corresponding_Address() throws Exception                                                                                                             //Natural: CORRESPONDING-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        //*  HK >>>
        //*  HK << 09/09/97
        ldaCisl500.getCisl500_Cis_Ph_Bank_Pymnt_Acct_Nbr().reset();                                                                                                       //Natural: RESET CISL500.CIS-PH-BANK-PYMNT-ACCT-NBR CISL500.CIS-BANK-ABA-ACCT-NBR CISL500.CIS-CHECKING-SAVING-CD
        ldaCisl500.getCisl500_Cis_Bank_Aba_Acct_Nbr().reset();
        ldaCisl500.getCisl500_Cis_Checking_Saving_Cd().reset();
        FOR01:                                                                                                                                                            //Natural: FOR #J 1 5
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(5)); pnd_J.nadd(1))
        {
            ldaCisl500.getCisl500_Cis_Address_Line().getValue(pnd_J).setValue(pnd_Work_File1_Cis_Address_Txt.getValue(1,pnd_J));                                          //Natural: ASSIGN CISL500.CIS-ADDRESS-LINE ( #J ) := #WORK-FILE1.CIS-ADDRESS-TXT ( 1,#J )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaCisl500.getCisl500_Cis_Zip_Code().setValue(pnd_Work_File1_Cis_Zip_Code.getValue(1));                                                                           //Natural: ASSIGN CISL500.CIS-ZIP-CODE := #WORK-FILE1.CIS-ZIP-CODE ( 1 )
        ldaCisl500.getCisl500_Cis_Addr_Usage_Cd().setValue(pnd_Work_File1_Cis_Addr_Usage_Code.getValue(1));                                                               //Natural: ASSIGN CISL500.CIS-ADDR-USAGE-CD := #WORK-FILE1.CIS-ADDR-USAGE-CODE ( 1 )
    }
    private void sub_Check_Mailing_Address() throws Exception                                                                                                             //Natural: CHECK-MAILING-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        //*  112108 START
        ldaCisl500.getCisl500_Cis_Address_Line().getValue("*").reset();                                                                                                   //Natural: RESET CISL500.CIS-ADDRESS-LINE ( * )
        if (condition(pnd_Work_File1_Cis_Address_Dest_Name.getValue(2).equals(" ")))                                                                                      //Natural: IF #WORK-FILE1.CIS-ADDRESS-DEST-NAME ( 2 ) EQ ' '
        {
            ldaCisl500.getCisl500_Cis_Address_Line().getValue(1).setValue(pnd_Work_File1_Cis_Address_Txt.getValue(2,1));                                                  //Natural: ASSIGN CISL500.CIS-ADDRESS-LINE ( 1 ) := #WORK-FILE1.CIS-ADDRESS-TXT ( 2,1 )
            ldaCisl500.getCisl500_Cis_Address_Line().getValue(2).setValue(pnd_Work_File1_Cis_Address_Txt.getValue(2,2));                                                  //Natural: ASSIGN CISL500.CIS-ADDRESS-LINE ( 2 ) := #WORK-FILE1.CIS-ADDRESS-TXT ( 2,2 )
            ldaCisl500.getCisl500_Cis_Address_Line().getValue(3).setValue(pnd_Work_File1_Cis_Address_Txt.getValue(2,3));                                                  //Natural: ASSIGN CISL500.CIS-ADDRESS-LINE ( 3 ) := #WORK-FILE1.CIS-ADDRESS-TXT ( 2,3 )
            ldaCisl500.getCisl500_Cis_Address_Line().getValue(4).setValue(pnd_Work_File1_Cis_Address_Txt.getValue(2,4));                                                  //Natural: ASSIGN CISL500.CIS-ADDRESS-LINE ( 4 ) := #WORK-FILE1.CIS-ADDRESS-TXT ( 2,4 )
            //*  112108 END
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCisl500.getCisl500_Cis_Address_Line().getValue(1).setValue(pnd_Work_File1_Cis_Address_Dest_Name.getValue(2));                                              //Natural: ASSIGN CISL500.CIS-ADDRESS-LINE ( 1 ) := #WORK-FILE1.CIS-ADDRESS-DEST-NAME ( 2 )
            ldaCisl500.getCisl500_Cis_Address_Line().getValue(2).setValue(pnd_Work_File1_Cis_Address_Txt.getValue(2,1));                                                  //Natural: ASSIGN CISL500.CIS-ADDRESS-LINE ( 2 ) := #WORK-FILE1.CIS-ADDRESS-TXT ( 2,1 )
            ldaCisl500.getCisl500_Cis_Address_Line().getValue(3).setValue(pnd_Work_File1_Cis_Address_Txt.getValue(2,2));                                                  //Natural: ASSIGN CISL500.CIS-ADDRESS-LINE ( 3 ) := #WORK-FILE1.CIS-ADDRESS-TXT ( 2,2 )
            ldaCisl500.getCisl500_Cis_Address_Line().getValue(4).setValue(pnd_Work_File1_Cis_Address_Txt.getValue(2,3));                                                  //Natural: ASSIGN CISL500.CIS-ADDRESS-LINE ( 4 ) := #WORK-FILE1.CIS-ADDRESS-TXT ( 2,3 )
            ldaCisl500.getCisl500_Cis_Address_Line().getValue(5).setValue(pnd_Work_File1_Cis_Address_Txt.getValue(2,4));                                                  //Natural: ASSIGN CISL500.CIS-ADDRESS-LINE ( 5 ) := #WORK-FILE1.CIS-ADDRESS-TXT ( 2,4 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl500.getCisl500_Cis_Zip_Code().setValue(pnd_Work_File1_Cis_Zip_Code.getValue(2));                                                                           //Natural: ASSIGN CISL500.CIS-ZIP-CODE := #WORK-FILE1.CIS-ZIP-CODE ( 2 )
        ldaCisl500.getCisl500_Cis_Addr_Usage_Cd().setValue(pnd_Work_File1_Cis_Addr_Usage_Code.getValue(2));                                                               //Natural: ASSIGN CISL500.CIS-ADDR-USAGE-CD := #WORK-FILE1.CIS-ADDR-USAGE-CODE ( 2 )
        ldaCisl500.getCisl500_Cis_Ph_Bank_Pymnt_Acct_Nbr().setValue(pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr.getValue(2));                                                 //Natural: ASSIGN CISL500.CIS-PH-BANK-PYMNT-ACCT-NBR := #WORK-FILE1.CIS-BANK-PYMNT-ACCT-NMBR ( 2 )
        ldaCisl500.getCisl500_Cis_Bank_Aba_Acct_Nbr().setValue(pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr.getValue(2));                                                        //Natural: ASSIGN CISL500.CIS-BANK-ABA-ACCT-NBR := #WORK-FILE1.CIS-BANK-ABA-ACCT-NMBR ( 2 )
        ldaCisl500.getCisl500_Cis_Checking_Saving_Cd().setValue(pnd_Work_File1_Cis_Checking_Saving_Cd.getValue(2));                                                       //Natural: ASSIGN CISL500.CIS-CHECKING-SAVING-CD := #WORK-FILE1.CIS-CHECKING-SAVING-CD ( 2 )
    }
    private void sub_Assign_Var() throws Exception                                                                                                                        //Natural: ASSIGN-VAR
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************
        //*  090606
        if (condition(DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'")))                                                                                           //Natural: IF #WORK-FILE1.CIS-RQST-ID = MASK ( 'MDO' )
        {
            ldaCisl500.getCisl500_Cis_Addr_Src_Cd().setValue("10");                                                                                                       //Natural: ASSIGN CISL500.CIS-ADDR-SRC-CD := '10'
            ldaCisl500.getCisl500_Cis_Cntrct_Payee_Cd().setValue("00");                                                                                                   //Natural: ASSIGN CISL500.CIS-CNTRCT-PAYEE-CD := '00'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("TPI") || pnd_Work_File1_Cis_Rqst_Id.equals("IPI")))                                                              //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'TPI' OR #WORK-FILE1.CIS-RQST-ID = 'IPI'
        {
            ldaCisl500.getCisl500_Cis_Addr_Src_Cd().setValue("02");                                                                                                       //Natural: ASSIGN CISL500.CIS-ADDR-SRC-CD := '02'
            ldaCisl500.getCisl500_Cis_Cntrct_Payee_Cd().setValue("00");                                                                                                   //Natural: ASSIGN CISL500.CIS-CNTRCT-PAYEE-CD := '00'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("IPRO")))                  //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IA' OR #WORK-FILE1.CIS-RQST-ID = 'TPA' OR #WORK-FILE1.CIS-RQST-ID = 'IPRO'
        {
            ldaCisl500.getCisl500_Cis_Addr_Src_Cd().setValue("11");                                                                                                       //Natural: ASSIGN CISL500.CIS-ADDR-SRC-CD := '11'
            ldaCisl500.getCisl500_Cis_Cntrct_Payee_Cd().setValue("01");                                                                                                   //Natural: ASSIGN CISL500.CIS-CNTRCT-PAYEE-CD := '01'
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SUBSTRING-NAME
        sub_Substring_Name();
        if (condition(Global.isEscape())) {return;}
        pnd_Dob.setValue(pnd_Work_File1_Cis_Frst_Annt_Dob_A);                                                                                                             //Natural: ASSIGN #DOB := #WORK-FILE1.CIS-FRST-ANNT-DOB-A
        pnd_Dob_N_Pnd_Dob_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dob_Pnd_Dob_Mm, pnd_Dob_Pnd_Dob_Dd, pnd_Dob_Pnd_Dob_Yy));                        //Natural: COMPRESS #DOB-MM #DOB-DD #DOB-YY INTO #DOB-A LEAVING NO
        ldaCisl500.getCisl500_Cis_Dob().setValue(pnd_Dob_N);                                                                                                              //Natural: ASSIGN CISL500.CIS-DOB := #DOB-N
        ldaCisl500.getCisl500_Cis_Ssn().setValue(pnd_Work_File1_Cis_Frst_Annt_Ssn);                                                                                       //Natural: ASSIGN CISL500.CIS-SSN := #WORK-FILE1.CIS-FRST-ANNT-SSN
        ldaCisl500.getCisl500_Cis_Trans_Cd().setValue("01");                                                                                                              //Natural: ASSIGN CISL500.CIS-TRANS-CD := '01'
        //*  022908 START
        if (condition(pnd_Work_File1_Cis_Pin_Nbr.notEquals(getZero())))                                                                                                   //Natural: IF #WORK-FILE1.CIS-PIN-NBR NE 0
        {
            ldaCisl500.getCisl500_Cis_Ph_Unque_Id_Nbr().setValue(pnd_Work_File1_Cis_Pin_Nbr);                                                                             //Natural: ASSIGN CISL500.CIS-PH-UNQUE-ID-NBR := #WORK-FILE1.CIS-PIN-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCispartv.getVw_cis_Part_View().startDatabaseFind                                                                                                           //Natural: FIND ( 1 ) CIS-PART-VIEW WITH CIS-TIAA-NBR = #WORK-FILE1.CIS-TIAA-NBR
            (
            "FIND01",
            new Wc[] { new Wc("CIS_TIAA_NBR", "=", pnd_Work_File1_Cis_Tiaa_Nbr, WcType.WITH) },
            1
            );
            FIND01:
            while (condition(ldaCispartv.getVw_cis_Part_View().readNextRow("FIND01")))
            {
                ldaCispartv.getVw_cis_Part_View().setIfNotFoundControlFlag(false);
                ldaCisl500.getCisl500_Cis_Ph_Unque_Id_Nbr().setValue(ldaCispartv.getCis_Part_View_Cis_Pin_Nbr());                                                         //Natural: ASSIGN CISL500.CIS-PH-UNQUE-ID-NBR := CIS-PART-VIEW.CIS-PIN-NBR
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            //*  022908 END
            //*  TRANSFER CHANGES
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl500.getCisl500_Cis_Cntrct_Ownership().setValue("0");                                                                                                       //Natural: ASSIGN CISL500.CIS-CNTRCT-OWNERSHIP := '0'
        ldaCisl500.getCisl500_Cis_Stnd_Rtn_Cd().setValue("ZZ");                                                                                                           //Natural: ASSIGN CISL500.CIS-STND-RTN-CD := 'ZZ'
        ldaCisl500.getCisl500_Cis_Finalist_Reason_Codes().setValue(" ");                                                                                                  //Natural: ASSIGN CISL500.CIS-FINALIST-REASON-CODES := ' '
        ldaCisl500.getCisl500_Cis_Addr_Stnd_Cd().setValue("N");                                                                                                           //Natural: ASSIGN CISL500.CIS-ADDR-STND-CD := 'N'
        ldaCisl500.getCisl500_Cis_Stnd_Overide().setValue(" ");                                                                                                           //Natural: ASSIGN CISL500.CIS-STND-OVERIDE := ' '
        ldaCisl500.getCisl500_Cis_Postal_Data_Fields().setValue(" ");                                                                                                     //Natural: ASSIGN CISL500.CIS-POSTAL-DATA-FIELDS := ' '
        ldaCisl500.getCisl500_Cis_Addr_Geographic_Cd().setValue(" ");                                                                                                     //Natural: ASSIGN CISL500.CIS-ADDR-GEOGRAPHIC-CD := ' '
        //* * CHANGES FOR IA TRANSFER WHEN THE FIRST OR SECOND ANNUITANT IS DEAD
        if (condition((pnd_Work_File1_Cis_Rqst_Id.equals("IA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("IPRO"))                  //Natural: IF ( #WORK-FILE1.CIS-RQST-ID = 'IA' OR #WORK-FILE1.CIS-RQST-ID = 'TPA' OR #WORK-FILE1.CIS-RQST-ID = 'IPRO' ) AND #WORK-FILE1.CIS-TRNSF-FLAG = 'Y' AND #WORK-FILE1.CIS-OWNERSHIP-CD > 0
            && pnd_Work_File1_Cis_Trnsf_Flag.equals("Y") && pnd_Work_File1_Cis_Ownership_Cd.greater(getZero())))
        {
            ldaCisl500.getCisl500_Cis_Cntrct_Payee_Cd().setValue("02");                                                                                                   //Natural: ASSIGN CISL500.CIS-CNTRCT-PAYEE-CD := '02'
            if (condition(pnd_Work_File1_Cis_Ownership_Cd.equals(2)))                                                                                                     //Natural: IF #WORK-FILE1.CIS-OWNERSHIP-CD = 2
            {
                                                                                                                                                                          //Natural: PERFORM SCND-ANNT-SUBSTRING-NAME
                sub_Scnd_Annt_Substring_Name();
                if (condition(Global.isEscape())) {return;}
                pnd_Dob.setValue(pnd_Work_File1_Cis_Scnd_Annt_Dob_A);                                                                                                     //Natural: ASSIGN #DOB := #WORK-FILE1.CIS-SCND-ANNT-DOB-A
                pnd_Dob_N_Pnd_Dob_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dob_Pnd_Dob_Mm, pnd_Dob_Pnd_Dob_Dd, pnd_Dob_Pnd_Dob_Yy));                //Natural: COMPRESS #DOB-MM #DOB-DD #DOB-YY INTO #DOB-A LEAVING NO
                ldaCisl500.getCisl500_Cis_Dob().setValue(pnd_Dob_N);                                                                                                      //Natural: ASSIGN CISL500.CIS-DOB := #DOB-N
                ldaCisl500.getCisl500_Cis_Ssn().setValue(pnd_Work_File1_Cis_Scnd_Annt_Ssn);                                                                               //Natural: ASSIGN CISL500.CIS-SSN := #WORK-FILE1.CIS-SCND-ANNT-SSN
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("IPRO")                    //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IA' OR #WORK-FILE1.CIS-RQST-ID = 'TPA' OR #WORK-FILE1.CIS-RQST-ID = 'IPRO' OR #WORK-FILE1.CIS-RQST-ID = 'TPI' OR #WORK-FILE1.CIS-RQST-ID = 'IPI'
            || pnd_Work_File1_Cis_Rqst_Id.equals("TPI") || pnd_Work_File1_Cis_Rqst_Id.equals("IPI")))
        {
            pnd_Ia_Rec.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #IA-REC
            getWorkFiles().write(2, false, ldaCisl500.getCisl500());                                                                                                      //Natural: WRITE WORK FILE 2 CISL500
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-REPORT
            sub_Write_Ia_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  090606
        if (condition(DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'")))                                                                                           //Natural: IF #WORK-FILE1.CIS-RQST-ID = MASK ( 'MDO' )
        {
            pnd_Mdo_Rec.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #MDO-REC
            //*  BE1. ELLO - RESIDENCE ADDRESS CAPTURE STARTS HERE...
            ldaCisl501.getCisl501().reset();                                                                                                                              //Natural: RESET CISL501
            ldaCisl501.getCisl501().setValuesByName(ldaCisl500.getCisl500());                                                                                             //Natural: MOVE BY NAME CISL500 TO CISL501
            //*  RES. ADDRESS EXISTS
            if (condition(pnd_Work_File1_Cis_Address_Txt.getValue(3,1).greater(" ")))                                                                                     //Natural: IF #WORK-FILE1.CIS-ADDRESS-TXT ( 3,1 ) GT ' '
            {
                ldaCisl501.getCisl501_Cis_Res_Address_Line().getValue(1,":",4).setValue(pnd_Work_File1_Cis_Address_Txt.getValue(3,1,":",4));                              //Natural: ASSIGN CISL501.CIS-RES-ADDRESS-LINE ( 1:4 ) := #WORK-FILE1.CIS-ADDRESS-TXT ( 3,1:4 )
                ldaCisl501.getCisl501_Cis_Res_Zip().setValue(pnd_Work_File1_Cis_Zip_Code.getValue(3));                                                                    //Natural: ASSIGN CISL501.CIS-RES-ZIP := #WORK-FILE1.CIS-ZIP-CODE ( 3 )
                if (condition((pnd_Work_File1_Cis_Address_Txt.getValue(3,4).greater(" ") && (pnd_Work_File1_Cis_Zip_Code.getValue(3).equals("FORGN") ||                   //Natural: IF #WORK-FILE1.CIS-ADDRESS-TXT ( 3,4 ) GT ' ' AND ( #WORK-FILE1.CIS-ZIP-CODE ( 3 ) = 'FORGN' OR = 'CANAD' )
                    pnd_Work_File1_Cis_Zip_Code.getValue(3).equals("CANAD")))))
                {
                    ldaCisl501.getCisl501_Cis_Res_Country_Code().setValue(pnd_Work_File1_Cis_Address_Txt.getValue(3,4));                                                  //Natural: ASSIGN CISL501.CIS-RES-COUNTRY-CODE := #WORK-FILE1.CIS-ADDRESS-TXT ( 3,4 )
                    pnd_Work_File1_Cis_Address_Txt.getValue(3,4).reset();                                                                                                 //Natural: RESET #WORK-FILE1.CIS-ADDRESS-TXT ( 3,4 ) CISL501.CIS-RES-ADDRESS-LINE ( 4 )
                    ldaCisl501.getCisl501_Cis_Res_Address_Line().getValue(4).reset();
                }                                                                                                                                                         //Natural: END-IF
                ldaCisl501.getCisl501_Cis_Res_Country_Code_Type().setValue("2");                                                                                          //Natural: ASSIGN CISL501.CIS-RES-COUNTRY-CODE-TYPE := '2'
            }                                                                                                                                                             //Natural: END-IF
            //*  BE1. ELLO - RESIDENCE ADDRESS CAPTURE ENDS HERE.
            getWorkFiles().write(3, false, ldaCisl501.getCisl501());                                                                                                      //Natural: WRITE WORK FILE 3 CISL501
                                                                                                                                                                          //Natural: PERFORM WRITE-MDO-REPORT
            sub_Write_Mdo_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Substring_Name() throws Exception                                                                                                                    //Natural: SUBSTRING-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Cis_Frst_Annt_Frst_Nme,1,1), new ExamineSearch(pnd_Work_File1_Cis_Frst_Annt_Frst_Nme), new ExamineGivingLength(pnd_Frst_Len)); //Natural: EXAMINE SUBSTRING ( #WORK-FILE1.CIS-FRST-ANNT-FRST-NME,1 ) FOR #WORK-FILE1.CIS-FRST-ANNT-FRST-NME GIVING LENGTH #FRST-LEN
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Cis_Frst_Annt_Mid_Nme,1,1), new ExamineSearch(pnd_Work_File1_Cis_Frst_Annt_Mid_Nme), new ExamineGivingLength(pnd_Mid_Len)); //Natural: EXAMINE SUBSTRING ( #WORK-FILE1.CIS-FRST-ANNT-MID-NME,1 ) FOR #WORK-FILE1.CIS-FRST-ANNT-MID-NME GIVING LENGTH #MID-LEN
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Cis_Frst_Annt_Lst_Nme,1,1), new ExamineSearch(pnd_Work_File1_Cis_Frst_Annt_Lst_Nme), new ExamineGivingLength(pnd_Lst_Len)); //Natural: EXAMINE SUBSTRING ( #WORK-FILE1.CIS-FRST-ANNT-LST-NME,1 ) FOR #WORK-FILE1.CIS-FRST-ANNT-LST-NME GIVING LENGTH #LST-LEN
        pnd_Last_Name.setValue(pnd_Work_File1_Cis_Frst_Annt_Lst_Nme.getSubstring(1,pnd_Lst_Len.getInt()));                                                                //Natural: MOVE SUBSTRING ( #WORK-FILE1.CIS-FRST-ANNT-LST-NME,1,#LST-LEN ) TO #LAST-NAME
        pnd_Begin.compute(new ComputeParameters(false, pnd_Begin), pnd_Frst_Len.add(pnd_Lst_Len).add(1));                                                                 //Natural: ASSIGN #BEGIN := #FRST-LEN + #LST-LEN + 1
        //*  CS \/
        if (condition(pnd_Begin.lessOrEqual(35) && pnd_Frst_Len.greater(getZero())))                                                                                      //Natural: IF #BEGIN LE 35 AND #FRST-LEN GT 0
        {
            pnd_Frst_Name.setValue(pnd_Work_File1_Cis_Frst_Annt_Frst_Nme.getSubstring(1,pnd_Frst_Len.getInt()));                                                          //Natural: MOVE SUBSTRING ( #WORK-FILE1.CIS-FRST-ANNT-FRST-NME,1,#FRST-LEN ) TO #FRST-NAME
            pnd_Fr_Nx.setValue(pnd_Frst_Len);                                                                                                                             //Natural: ASSIGN #FR-NX := #FRST-LEN
                                                                                                                                                                          //Natural: PERFORM LOAD-MID-NAME
            sub_Load_Mid_Name();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Frst_Len.greater(getZero())))                                                                                                               //Natural: IF #FRST-LEN GT 0
            {
                FOR:                                                                                                                                                      //Natural: FOR #N 1 30
                for (pnd_N.setValue(1); condition(pnd_N.lessOrEqual(30)); pnd_N.nadd(1))
                {
                    if (condition(((pnd_N.add(pnd_Lst_Len).add(1)).greater(35)) || pnd_Work_File1_Cis_Frst_Annt_Frst_Nme.getSubstring(pnd_N.getInt(),1).equals(" ")))     //Natural: IF ( ( #N + #LST-LEN + 1 ) GT 35 ) OR SUBSTRING ( #WORK-FILE1.CIS-FRST-ANNT-FRST-NME,#N,1 ) = ' '
                    {
                        if (true) break FOR;                                                                                                                              //Natural: ESCAPE BOTTOM ( FOR. )
                    }                                                                                                                                                     //Natural: END-IF
                    //*     #FR-NX := #N
                    setValueToSubstring(pnd_Work_File1_Cis_Frst_Annt_Frst_Nme.getSubstring(pnd_N.getInt(),1),pnd_Frst_Name,pnd_N.getInt());                               //Natural: MOVE SUBSTRING ( #WORK-FILE1.CIS-FRST-ANNT-FRST-NME,#N,1 ) TO SUBSTRING ( #FRST-NAME,#N )
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Frst_Name.greater(" ")))                                                                                                                    //Natural: IF #FRST-NAME GT ' '
            {
                pnd_Full_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Last_Name));                                                                              //Natural: COMPRESS #FRST-NAME ' ' #LAST-NAME INTO #FULL-NAME LEAVING
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Full_Name.setValue(pnd_Last_Name);                                                                                                                    //Natural: MOVE #LAST-NAME TO #FULL-NAME
                //*  CS /\
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl500.getCisl500_Cis_Cntrct_Name_Free().setValue(pnd_Full_Name);                                                                                             //Natural: ASSIGN CISL500.CIS-CNTRCT-NAME-FREE := #FULL-NAME
    }
    private void sub_Load_Mid_Name() throws Exception                                                                                                                     //Natural: LOAD-MID-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************************************
        if (condition(pnd_Work_File1_Cis_Frst_Annt_Mid_Nme.equals(" ")))                                                                                                  //Natural: IF #WORK-FILE1.CIS-FRST-ANNT-MID-NME = ' '
        {
            pnd_Full_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Last_Name));                                                                                  //Natural: COMPRESS #FRST-NAME ' ' #LAST-NAME INTO #FULL-NAME LEAVING
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Begin1.compute(new ComputeParameters(false, pnd_Begin1), pnd_Fr_Nx.add(pnd_Lst_Len).add(pnd_Mid_Len).add(2));                                                 //Natural: ASSIGN #BEGIN1 := #FR-NX + #LST-LEN + #MID-LEN + 2
        if (condition(pnd_Begin1.lessOrEqual(35)))                                                                                                                        //Natural: IF #BEGIN1 LE 35
        {
            pnd_Mid_Name.setValue(pnd_Work_File1_Cis_Frst_Annt_Mid_Nme.getSubstring(1,pnd_Mid_Len.getInt()));                                                             //Natural: MOVE SUBSTRING ( #WORK-FILE1.CIS-FRST-ANNT-MID-NME,1,#MID-LEN ) TO #MID-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Mid_Name.setValue(pnd_Work_File1_Cis_Frst_Annt_Mid_Nme.getSubstring(1,1));                                                                                //Natural: MOVE SUBSTRING ( #WORK-FILE1.CIS-FRST-ANNT-MID-NME,1,1 ) TO #MID-NAME
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Full_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Mid_Name, " ", pnd_Last_Name));                                                                   //Natural: COMPRESS #FRST-NAME ' ' #MID-NAME ' ' #LAST-NAME INTO #FULL-NAME LEAVING
    }
    private void sub_Scnd_Annt_Substring_Name() throws Exception                                                                                                          //Natural: SCND-ANNT-SUBSTRING-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme,1,1), new ExamineSearch(pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme), new ExamineGivingLength(pnd_Frst_Len)); //Natural: EXAMINE SUBSTRING ( #WORK-FILE1.CIS-SCND-ANNT-FRST-NME,1 ) FOR #WORK-FILE1.CIS-SCND-ANNT-FRST-NME GIVING LENGTH #FRST-LEN
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme,1,1), new ExamineSearch(pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme), new ExamineGivingLength(pnd_Mid_Len)); //Natural: EXAMINE SUBSTRING ( #WORK-FILE1.CIS-SCND-ANNT-MID-NME,1 ) FOR #WORK-FILE1.CIS-SCND-ANNT-MID-NME GIVING LENGTH #MID-LEN
        DbsUtil.examine(new ExamineSource(pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme,1,1), new ExamineSearch(pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme), new ExamineGivingLength(pnd_Lst_Len)); //Natural: EXAMINE SUBSTRING ( #WORK-FILE1.CIS-SCND-ANNT-LST-NME,1 ) FOR #WORK-FILE1.CIS-SCND-ANNT-LST-NME GIVING LENGTH #LST-LEN
        pnd_Last_Name.setValue(pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme.getSubstring(1,pnd_Lst_Len.getInt()));                                                                //Natural: MOVE SUBSTRING ( #WORK-FILE1.CIS-SCND-ANNT-LST-NME,1,#LST-LEN ) TO #LAST-NAME
        pnd_Begin.compute(new ComputeParameters(false, pnd_Begin), pnd_Frst_Len.add(pnd_Lst_Len).add(1));                                                                 //Natural: ASSIGN #BEGIN := #FRST-LEN + #LST-LEN + 1
        //*  CS \/
        if (condition(pnd_Begin.lessOrEqual(35) && pnd_Frst_Len.greater(getZero())))                                                                                      //Natural: IF #BEGIN LE 35 AND #FRST-LEN GT 0
        {
            pnd_Frst_Name.setValue(pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme.getSubstring(1,pnd_Frst_Len.getInt()));                                                          //Natural: MOVE SUBSTRING ( #WORK-FILE1.CIS-SCND-ANNT-FRST-NME,1,#FRST-LEN ) TO #FRST-NAME
            pnd_Fr_Nx.setValue(pnd_Frst_Len);                                                                                                                             //Natural: ASSIGN #FR-NX := #FRST-LEN
                                                                                                                                                                          //Natural: PERFORM LOAD-SCND-ANNT-MID-NAME
            sub_Load_Scnd_Annt_Mid_Name();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Frst_Len.greater(getZero())))                                                                                                               //Natural: IF #FRST-LEN GT 0
            {
                FOR1:                                                                                                                                                     //Natural: FOR #N 1 30
                for (pnd_N.setValue(1); condition(pnd_N.lessOrEqual(30)); pnd_N.nadd(1))
                {
                    if (condition(((pnd_N.add(pnd_Lst_Len).add(1)).greater(35)) || pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme.getSubstring(pnd_N.getInt(),1).equals(" ")))     //Natural: IF ( ( #N + #LST-LEN + 1 ) GT 35 ) OR SUBSTRING ( #WORK-FILE1.CIS-SCND-ANNT-FRST-NME,#N,1 ) = ' '
                    {
                        if (true) break FOR1;                                                                                                                             //Natural: ESCAPE BOTTOM ( FOR1. )
                    }                                                                                                                                                     //Natural: END-IF
                    //*     #FR-NX := #N
                    setValueToSubstring(pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme.getSubstring(pnd_N.getInt(),1),pnd_Frst_Name,pnd_N.getInt());                               //Natural: MOVE SUBSTRING ( #WORK-FILE1.CIS-SCND-ANNT-FRST-NME,#N,1 ) TO SUBSTRING ( #FRST-NAME,#N )
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Frst_Name.greater(" ")))                                                                                                                    //Natural: IF #FRST-NAME GT ' '
            {
                pnd_Full_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Last_Name));                                                                              //Natural: COMPRESS #FRST-NAME ' ' #LAST-NAME INTO #FULL-NAME LEAVING
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Full_Name.setValue(pnd_Last_Name);                                                                                                                    //Natural: MOVE #LAST-NAME TO #FULL-NAME
                //*  CS /\
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl500.getCisl500_Cis_Cntrct_Name_Free().setValue(pnd_Full_Name);                                                                                             //Natural: ASSIGN CISL500.CIS-CNTRCT-NAME-FREE := #FULL-NAME
    }
    private void sub_Load_Scnd_Annt_Mid_Name() throws Exception                                                                                                           //Natural: LOAD-SCND-ANNT-MID-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************************************
        if (condition(pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme.equals(" ")))                                                                                                  //Natural: IF #WORK-FILE1.CIS-SCND-ANNT-MID-NME = ' '
        {
            pnd_Full_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Last_Name));                                                                                  //Natural: COMPRESS #FRST-NAME ' ' #LAST-NAME INTO #FULL-NAME LEAVING
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Begin1.compute(new ComputeParameters(false, pnd_Begin1), pnd_Fr_Nx.add(pnd_Lst_Len).add(pnd_Mid_Len).add(2));                                                 //Natural: ASSIGN #BEGIN1 := #FR-NX + #LST-LEN + #MID-LEN + 2
        if (condition(pnd_Begin1.lessOrEqual(35)))                                                                                                                        //Natural: IF #BEGIN1 LE 35
        {
            pnd_Mid_Name.setValue(pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme.getSubstring(1,pnd_Mid_Len.getInt()));                                                             //Natural: MOVE SUBSTRING ( #WORK-FILE1.CIS-SCND-ANNT-MID-NME,1,#MID-LEN ) TO #MID-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Mid_Name.setValue(pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme.getSubstring(1,1));                                                                                //Natural: MOVE SUBSTRING ( #WORK-FILE1.CIS-SCND-ANNT-MID-NME,1,1 ) TO #MID-NAME
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Full_Name.setValue(DbsUtil.compress(pnd_Frst_Name, " ", pnd_Mid_Name, " ", pnd_Last_Name));                                                                   //Natural: COMPRESS #FRST-NAME ' ' #MID-NAME ' ' #LAST-NAME INTO #FULL-NAME LEAVING
    }
    private void sub_Compress_Addr_Zipcode() throws Exception                                                                                                             //Natural: COMPRESS-ADDR-ZIPCODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************
        F1:                                                                                                                                                               //Natural: FOR #J 5 1 -1
        for (pnd_J.setValue(5); condition(pnd_J.greaterOrEqual(1)); pnd_J.nsubtract(1))
        {
            if (condition(ldaCisl500.getCisl500_Cis_Address_Line().getValue(pnd_J).notEquals(" ")))                                                                       //Natural: IF CISL500.CIS-ADDRESS-LINE ( #J ) NE ' '
            {
                pnd_Addr_Zp.setValue(ldaCisl500.getCisl500_Cis_Address_Line().getValue(pnd_J));                                                                           //Natural: ASSIGN #ADDR-ZP := CISL500.CIS-ADDRESS-LINE ( #J )
                DbsUtil.examine(new ExamineSource(pnd_Addr_Zp,1,1), new ExamineSearch(pnd_Addr_Zp), new ExamineGivingLength(pnd_Addr_Len));                               //Natural: EXAMINE SUBSTRING ( #ADDR-ZP,1 ) FOR #ADDR-ZP GIVING LENGTH #ADDR-LEN
                if (condition((pnd_Addr_Len.add(6)).greater(35) && pnd_J.less(5)))                                                                                        //Natural: IF ( #ADDR-LEN + 6 ) GT 35 AND #J LT 5
                {
                    if (condition(ldaCisl500.getCisl500_Cis_Address_Line().getValue(pnd_J.getDec().add(1)).equals(" ")))                                                  //Natural: IF CISL500.CIS-ADDRESS-LINE ( #J + 1 ) = ' '
                    {
                        ldaCisl500.getCisl500_Cis_Address_Line().getValue(pnd_J.getDec().add(1)).setValue(ldaCisl500.getCisl500_Cis_Zip_Code());                          //Natural: ASSIGN CISL500.CIS-ADDRESS-LINE ( #J + 1 ) := CISL500.CIS-ZIP-CODE
                        if (true) break F1;                                                                                                                               //Natural: ESCAPE BOTTOM ( F1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_J.equals(5) && (pnd_Addr_Len.add(6)).greater(35)))                                                                                  //Natural: IF #J = 5 AND ( #ADDR-LEN + 6 ) GT 35
                    {
                        if (true) break F1;                                                                                                                               //Natural: ESCAPE BOTTOM ( F1. )
                    }                                                                                                                                                     //Natural: END-IF
                    ldaCisl500.getCisl500_Cis_Address_Line().getValue(pnd_J).setValue(DbsUtil.compress(ldaCisl500.getCisl500_Cis_Address_Line().getValue(pnd_J),          //Natural: COMPRESS CISL500.CIS-ADDRESS-LINE ( #J ) ' ' CISL500.CIS-ZIP-CODE INTO CISL500.CIS-ADDRESS-LINE ( #J ) LEAVING
                        " ", ldaCisl500.getCisl500_Cis_Zip_Code()));
                    if (true) break F1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F1. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Ia_Report() throws Exception                                                                                                                   //Natural: WRITE-IA-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************
                                                                                                                                                                          //Natural: PERFORM COMPRESS-ADDR-ZIPCODE
        sub_Compress_Addr_Zipcode();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ldaCisl500.getCisl500_Cis_Contract_Nbr(),ldaCisl500.getCisl500_Cis_Cntrct_Name_Free(),ldaCisl500.getCisl500_Cis_Ssn(), new                  //Natural: WRITE ( 1 ) CISL500.CIS-CONTRACT-NBR CISL500.CIS-CNTRCT-NAME-FREE CISL500.CIS-SSN ( EM = 999-99-9999 ) CISL500.CIS-DOB ( EM = 99/99/99 ) 4X CISL500.CIS-ADDR-USAGE-CD 4X CISL500.CIS-PH-UNQUE-ID-NBR / 12T CISL500.CIS-ADDRESS-LINE ( 1 ) / 12T CISL500.CIS-ADDRESS-LINE ( 2 ) / 12T CISL500.CIS-ADDRESS-LINE ( 3 ) / 12T CISL500.CIS-ADDRESS-LINE ( 4 ) / 12T CISL500.CIS-ADDRESS-LINE ( 5 )
            ReportEditMask ("999-99-9999"),ldaCisl500.getCisl500_Cis_Dob(), new ReportEditMask ("99/99/99"),new ColumnSpacing(4),ldaCisl500.getCisl500_Cis_Addr_Usage_Cd(),new 
            ColumnSpacing(4),ldaCisl500.getCisl500_Cis_Ph_Unque_Id_Nbr(),NEWLINE,new TabSetting(12),ldaCisl500.getCisl500_Cis_Address_Line().getValue(1),NEWLINE,new 
            TabSetting(12),ldaCisl500.getCisl500_Cis_Address_Line().getValue(2),NEWLINE,new TabSetting(12),ldaCisl500.getCisl500_Cis_Address_Line().getValue(3),NEWLINE,new 
            TabSetting(12),ldaCisl500.getCisl500_Cis_Address_Line().getValue(4),NEWLINE,new TabSetting(12),ldaCisl500.getCisl500_Cis_Address_Line().getValue(5));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Mdo_Report() throws Exception                                                                                                                  //Natural: WRITE-MDO-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************
                                                                                                                                                                          //Natural: PERFORM COMPRESS-ADDR-ZIPCODE
        sub_Compress_Addr_Zipcode();
        if (condition(Global.isEscape())) {return;}
        getReports().write(2, ldaCisl500.getCisl500_Cis_Contract_Nbr(),ldaCisl500.getCisl500_Cis_Cntrct_Name_Free(),ldaCisl500.getCisl500_Cis_Ssn(), new                  //Natural: WRITE ( 2 ) CISL500.CIS-CONTRACT-NBR CISL500.CIS-CNTRCT-NAME-FREE CISL500.CIS-SSN ( EM = 999-99-9999 ) CISL500.CIS-DOB ( EM = 99/99/99 ) 4X CISL500.CIS-ADDR-USAGE-CD 4X CISL500.CIS-PH-UNQUE-ID-NBR / 12T CISL500.CIS-ADDRESS-LINE ( 1 ) / 12T CISL500.CIS-ADDRESS-LINE ( 2 ) / 12T CISL500.CIS-ADDRESS-LINE ( 3 ) / 12T CISL500.CIS-ADDRESS-LINE ( 4 ) / 12T CISL500.CIS-ADDRESS-LINE ( 5 )
            ReportEditMask ("999-99-9999"),ldaCisl500.getCisl500_Cis_Dob(), new ReportEditMask ("99/99/99"),new ColumnSpacing(4),ldaCisl500.getCisl500_Cis_Addr_Usage_Cd(),new 
            ColumnSpacing(4),ldaCisl500.getCisl500_Cis_Ph_Unque_Id_Nbr(),NEWLINE,new TabSetting(12),ldaCisl500.getCisl500_Cis_Address_Line().getValue(1),NEWLINE,new 
            TabSetting(12),ldaCisl500.getCisl500_Cis_Address_Line().getValue(2),NEWLINE,new TabSetting(12),ldaCisl500.getCisl500_Cis_Address_Line().getValue(3),NEWLINE,new 
            TabSetting(12),ldaCisl500.getCisl500_Cis_Address_Line().getValue(4),NEWLINE,new TabSetting(12),ldaCisl500.getCisl500_Cis_Address_Line().getValue(5));
        if (Global.isEscape()) return;
    }
    private void sub_Ia_Totals_End_Of_Rpt() throws Exception                                                                                                              //Natural: IA-TOTALS-END-OF-RPT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, NEWLINE,NEWLINE,new ColumnSpacing(10)," * * * * * * * * * * * * * * * * * * * *",NEWLINE,new ColumnSpacing(10)," * Number of Records Extracted:",pnd_Total_Ia,  //Natural: WRITE ( 1 ) // 10X ' * * * * * * * * * * * * * * * * * * * *' / 10X ' * Number of Records Extracted:' #TOTAL-IA '*' / 10X ' * Number of Records Written:  ' #IA-REC '*' / 10X ' * Number of TEACHERS Records: ' #TIAA-REC-IA '*' / 10X ' * Number of CREF Records:     ' #CREF-REC-IA '*' / 10X ' * * * * * * * * * * * * * * * * * * * *' // 6X ' ** T H E   E N D   O F   R E P O R T  **' /
            new ReportEditMask ("ZZ,ZZ9"),"*",NEWLINE,new ColumnSpacing(10)," * Number of Records Written:  ",pnd_Ia_Rec, new ReportEditMask ("ZZ,ZZ9"),"*",NEWLINE,new 
            ColumnSpacing(10)," * Number of TEACHERS Records: ",pnd_Tiaa_Rec_Ia, new ReportEditMask ("ZZ,ZZ9"),"*",NEWLINE,new ColumnSpacing(10)," * Number of CREF Records:     ",pnd_Cref_Rec_Ia, 
            new ReportEditMask ("ZZ,ZZ9"),"*",NEWLINE,new ColumnSpacing(10)," * * * * * * * * * * * * * * * * * * * *",NEWLINE,NEWLINE,new ColumnSpacing(6),
            " ** T H E   E N D   O F   R E P O R T  **",NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Mdo_Totals_End_Of_Rpt() throws Exception                                                                                                             //Natural: MDO-TOTALS-END-OF-RPT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(2, NEWLINE,NEWLINE,new ColumnSpacing(10)," * * * * * * * * * * * * * * * * * * * *",NEWLINE,new ColumnSpacing(10)," * Number of Records Extracted:",pnd_Total_Mdo,  //Natural: WRITE ( 2 ) // 10X ' * * * * * * * * * * * * * * * * * * * *' / 10X ' * Number of Records Extracted:' #TOTAL-MDO '*' / 10X ' * Number of Records Written:  ' #MDO-REC '*' / 10X ' * Number of TEACHERS Records: ' #TIAA-REC-MDO '*' / 10X ' * * * * * * * * * * * * * * * * * * * *' // 6X ' ** T H E   E N D   O F   R E P O R T  **' /
            new ReportEditMask ("ZZ,ZZ9"),"*",NEWLINE,new ColumnSpacing(10)," * Number of Records Written:  ",pnd_Mdo_Rec, new ReportEditMask ("ZZ,ZZ9"),"*",NEWLINE,new 
            ColumnSpacing(10)," * Number of TEACHERS Records: ",pnd_Tiaa_Rec_Mdo, new ReportEditMask ("ZZ,ZZ9"),"*",NEWLINE,new ColumnSpacing(10)," * * * * * * * * * * * * * * * * * * * *",NEWLINE,NEWLINE,new 
            ColumnSpacing(6)," ** T H E   E N D   O F   R E P O R T  **",NEWLINE);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM(),new ColumnSpacing(30),"NAME/ADDRESS REPORT",new ColumnSpacing(30),"RUN DATE:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 1 ) *PROGRAM 30X 'NAME/ADDRESS REPORT' 30X 'RUN DATE:' *DATX / 40X 'FOR RQST ID = ' #WORK-FILE1.CIS-RQST-ID // ' Contract                ' 23X '            Birth Of  Addr ' 'Unque Id' / '   Nbr     Name/Address ' 23X 'SSN           Date    Usg Cd' '  Nbr   ' / '---------- ----------------------------------- -----------' '--------- ------ ------------'
                        ColumnSpacing(40),"FOR RQST ID = ",pnd_Work_File1_Cis_Rqst_Id,NEWLINE,NEWLINE," Contract                ",new ColumnSpacing(23),"            Birth Of  Addr ","Unque Id",NEWLINE,"   Nbr     Name/Address ",new 
                        ColumnSpacing(23),"SSN           Date    Usg Cd","  Nbr   ",NEWLINE,"---------- ----------------------------------- -----------",
                        "--------- ------ ------------");
                    //*    '--------- ------ -------'
                    //*  PINE
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, Global.getPROGRAM(),new ColumnSpacing(30),"NAME/ADDRESS REPORT",new ColumnSpacing(30),"RUN DATE:",Global.getDATX(),NEWLINE,new  //Natural: WRITE ( 2 ) *PROGRAM 30X 'NAME/ADDRESS REPORT' 30X 'RUN DATE:' *DATX / 40X 'FOR RQST ID = MDO' // ' Contract                ' 23X '            Birth Of  Addr ' 'Unque Id' / '   Nbr     Name/Address ' 23X 'SSN           Date    Usg Cd' '  Nbr   ' / '---------- ----------------------------------- -----------' '--------- ------ ------------'
                        ColumnSpacing(40),"FOR RQST ID = MDO",NEWLINE,NEWLINE," Contract                ",new ColumnSpacing(23),"            Birth Of  Addr ","Unque Id",NEWLINE,"   Nbr     Name/Address ",new 
                        ColumnSpacing(23),"SSN           Date    Usg Cd","  Nbr   ",NEWLINE,"---------- ----------------------------------- -----------",
                        "--------- ------ ------------");
                    //*    '--------- ------ -------'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
    }
}
