/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:23:35 PM
**        * FROM NATURAL PROGRAM : Sgdb4001
************************************************************
**        * FILE NAME            : Sgdb4001.java
**        * CLASS NAME           : Sgdb4001
**        * INSTANCE NAME        : Sgdb4001
************************************************************
************************************************************************
* PROGRAM : SGDB4001
* SYSTEM  : TOPS RELEASE 27
* TITLE   : MDO UPDATE TO CIS
* FUNCTION: TO CREATE THE WELCOME PACKAGE FOR OMNI ISSUED MDO/SIPS
* CREATED : AUGUST, 2005
* HISTORY
* 08/21/07 HKAKADIA :  ADDED REPORT FOR ERROR AND DUPLCATE.  ALSO ADD
*                      CIS RECORD IF THEY DON'T EXIS ON CIS FILE
* 02/2008  NCVETKOVIC: MODIFIED TO SUPPORT SIP AS WELL AS MDO
*                      REMOVED MULTIPLE UNUSED LDAS
*
* 04/2008 HKAKADIA : MODIFY THE WAY ADDRESS IS STORED AND POPULATE
*                    GENDER CODE
* 08/2008  NCVETKOVIC: MODIFIED TO SUPPORT NEW SIP ATRA SUBPLANS
*                    INCORPORATES JJG CHANGE:
*                  : ADDED CHECK OF '#P-MINIMUM-REQ-AMT-N' FOR
*                    NUMERIC (SCAN JJG)
* 10/2008 T.SHINE  : ALLOW PROGRAM TO ESCAPE TOP AFTER ADDED BENE'S
* 03/2009 JJG      : INC501276 - ADDED EDITING FOR ANNUITY START DATE
* 06/2009 C. AVE   : ROTH 403(B)/401(K) PROJECT - ADDED NEW FIELDS TO
*                SUPPORT ROTH PROJECT - CHANGES ARE MARKED BY CA-06/2009
* 11/2009 Z. KHAN  : 2ND LINE OF ADDRESS CORRECTED     (ZAK1)
*
* 02/2010 K CYRUS  : BENEFICIARIES FROM A REJECTED PARTICIPANT WERE
*                    BEING ADDED TO THE NEXT 'GOOD' PARTICIPANT,
*                    BECAUSE PROGRAM WAS NOT CLEARING THE DATA AREA FOR
*                    CSI-BENE-FILE-01 OF THE BENES FOR THE REJECTED
*                    PARTID BEFORE PROCEEDING. CHANGES MARKED BY
*                    KC-02
*
* 02/2010 G GUERRER: CHANGED THE VALUATION OF THE FIELD CIS-PULL-CODE
*                    FROM A VALUE OF 'SPEC' TO BLANK IN ALL CASES.
*                    THIS CAUSES ALL MDO PACKETS TO MAIL DEFAULT.
*                    INC944712 - PRB44157
*                    ALSO MODIFIED CODE TO FIX BUG CAUSING INCOMPLTE
*                    ADDRESS RECORDS BEING WRITTEN TO THE CIS DATABASE.
*                    PRB44117
*
* 10/2010 B.HOLLOWAY ADD THE ADDITIONAL DATA BEING SENT FROM OMNI FOR
*                    MDO CONTRACTS TO THE INPUT RECORD DEFINITION AND
*                    UPDATE THE CIS PARTICIPANT FILE WITH THE NEW DATA.
*
* 04/2011 B.HOLLOWAY COMMENT OUT LOGIC TO UPDATE THE CIS PARTICIPANT
*                    FILE WITH INVESTMENT DATA ADDED IN THE 10/2010
*                    CHANGE.  THIS DATA NO LONGER NEEDS TO BE STORED ON
*                    THE CIS-PRTCPNT-FILE.
*
* 09/2011 B.HOLLOWAY - CORRECT THE LOGIC THAT POPULATES THE ADDRESS
*                      FIELDS ON THE CIS-PARTCPNT-FILE
*                    - ADDED #DEBUG(L) FIELD TO FACILITATE TESTING -
*                      THE INIT VALUE OF #DEBUG MUST BE FALSE BEFORE
*                      MOVING THIS MODULE TO PRODUCTION
* 01/2012 D.WOLF DEW ADD KEOGH ("KE") TO POPULATE ANNTY-OPT AS "SRA"
*                    MDO-CASH-STAT = "C" (CASHABLE) ON CIS-PRTCPNT-FILE.
*
* 03/2012 ELLO       RESIDENTIAL ADDRESS PROJECT
*                    NEW RESIDENTIAL ADDRESS FIELDS RECEIVED FROM OMNI
*                    FOR SIP CONTRACTS WILL BE STORED IN THE 3RD OCCUR.
*                    OF CIS-ADDRESS-TXT(3,*) FIELDS IN THE
*                    CIS-PRTCPNT-FILE. THE CIS-ADDR-USAGE-CODE 3RD OCC.
*                    WILL BE ASSIGNED A VALUE OF '3' FOR MDO SIP
*                    OMNI CONTRACTS.
*
* 03/2012 D.WOLF DW1 CHECK NEXT-PAY-DATE FOR ZERO BEFORE MOVE EDITED TO
*                    PREVENT ABEND, AND USE ANNTY-STRT-DTE IF PRESENT &
*                    VALID FORMAT INSTEAD. SIPS ONLY. SCAN INC1615517.
*
* 06/2012 ELLO   BE1 MDO-SIP PROJECT
*                    THE OMNI INPUT FILE LTFP0710 WAS CHANGED TO INCLUDE
*                    4 NEW FIELDS.  THE FIELDS ARE NOT REQUIRED TO BE
*                    STORED IN CIS.  SCAN MDOSIP
*
* 03/2013 FRANKLIN   CHANGE CHG277892- PRODUCTION FIX.
*                    WHEN ENOUGH CONCECUTIVE ERRORS OCCUR THEN THE
*                    PROGRAM CAN FAIL BECAUSE OF EXCEEDING THE 20 BENE
*                    CAPABILITY.  CHANGE BY 02/2010 K CYRUS SOLVED THE
*                    MOVING OF BENE'S TO THE WRONG CONTRACTS BUT LEFT
*                    IN PLACE THE POSITIONING PROBLEMS WHERE AFTER AN
*                    ERROR THE BENE'S WOULD NOT START IN THE FIRST
*                    POSITION.                        SCAN C277892
* 06/2014 B. NEWSOM  - INTRODUCED A NEW PARM FILE (WORKFILE 2)    (CREA)
*                      CONTAINING THE FUND/TICKER INFORMATION     (CREA)
*                      NEEDED TO PROCESS THE RECORDS. THIS ELIMI- (CREA)
*                      NATES THE HARDCODING OF THE TICKER SYMBOLS.(CREA)
*                    - ADDED AN ON ERROR ROUTINE TO TRAP SYSTEM   (CREA)
*                      ERRORS AND DISPLAY A MESSAGE BEFORE THE    (CREA)
*                      PROGRAM ABENDS.                            (CREA)
* 10/2014 B. NEWSOM  - ACIS/CIS CREF REDESIGN COMMUNICATIONS     (ACCRC)
*                      MOVE THE PLAN NUMBER FROM THE INPUT LAYOUT TO
*                      THE CIS-PRTCPNT-FILE.
* 05/2015 B. NEWSOM  - PRODUCTION FIX FOR SHARE CLASS         CHG348394
* 09/2015 B. ELLO    - ADDED NEW FIELDS FOR BENE IN THE PLAN ACCOUNTS IN
*                      THE CIS PARTICIPANT FILE. THE INPUT BASEEVENT
*                      FILE FROM OMNI IS BEING CHANGED TO ADD THE NEW
*                      DATA ELEMENTS.                              (BIP)
* 10/2015 RJ FRANKLIN- RESET THE WORKFILE STORAGE AREA IN THE EVENT OF
*                      BYPASSED RECORDS.                     (CHG365909)
* 05/2016 L. SHU     - ADDED ATRA FLAG LOGIC FOR MDO/SIP          (ATRA)
*                      BY USING PLAN SIP401
* 05/15/17  (BABRE)   PIN EXPANSION CHANGES. (C420007)         PINE
* 03/12/2019 ELLO     REMOVED PDA ALCA911 SINCE IT IS NOT USED.
************************************************************************

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Sgdb4001 extends BLNatBase
{
    // Data Areas
    private LdaCispartv ldaCispartv;
    private LdaCisl2020 ldaCisl2020;
    private LdaAppl170 ldaAppl170;
    private PdaCisa4000 pdaCisa4000;
    private PdaCwfpda_M pdaCwfpda_M;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_In_Translte_File;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Ticker;
    private DbsField pnd_In_Translte_File_Pnd_F1;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name;
    private DbsField pnd_In_Translte_File_Pnd_F2;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq;
    private DbsField pnd_In_Translte_File_Pnd_F3;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Pull_Code;
    private DbsField pnd_In_Translte_File_Pnd_F4;
    private DbsField pnd_In_Translte_File_Pnd_In_Translte_Fund_Type;
    private DbsField pnd_In_Translte_File_Pnd_F5;

    private DbsGroup pnd_Translte_File;
    private DbsField pnd_Translte_File_Pnd_Translte_Ticker;
    private DbsField pnd_Translte_File_Pnd_F1;
    private DbsField pnd_Translte_File_Pnd_Translte_Ticker_Name;
    private DbsField pnd_Translte_File_Pnd_F2;
    private DbsField pnd_Translte_File_Pnd_Translte_Fund_Seq;
    private DbsField pnd_Translte_File_Pnd_F3;
    private DbsField pnd_Translte_File_Pnd_Translte_Pull_Code;
    private DbsField pnd_Translte_File_Pnd_F4;
    private DbsField pnd_Translte_File_Pnd_Translte_Fund_Type;
    private DbsField pnd_Translte_File_Pnd_F5;
    private DbsField pnd_Translte_File_Pnd_Translte_Error_Message;

    private DbsGroup pnd_In_Bene_Relationship;
    private DbsField pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Seq;
    private DbsField pnd_In_Bene_Relationship_Pnd_F1;
    private DbsField pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Type;
    private DbsField pnd_In_Bene_Relationship_Pnd_F2;
    private DbsField pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Name;
    private DbsField pnd_In_Bene_Relationship_Pnd_F3;
    private DbsField pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Code;
    private DbsField pnd_In_Bene_Relationship_Pnd_F4;

    private DbsGroup pnd_Bene_Relationship;
    private DbsField pnd_Bene_Relationship_Pnd_Bene_Relationship_Seq;
    private DbsField pnd_Bene_Relationship_Pnd_F1;
    private DbsField pnd_Bene_Relationship_Pnd_Bene_Relationship_Type;
    private DbsField pnd_Bene_Relationship_Pnd_F2;
    private DbsField pnd_Bene_Relationship_Pnd_Bene_Relationship_Name;
    private DbsField pnd_Bene_Relationship_Pnd_F3;
    private DbsField pnd_Bene_Relationship_Pnd_Bene_Relationship_Code;
    private DbsField pnd_Bene_Relationship_Pnd_F4;
    private DbsField pnd_Bene_Relationship_Pnd_Bene_Relationship_Message;
    private DbsField pnd_Bene_Alloc;
    private DbsField pnd_Bene_Cnt_Prmry;
    private DbsField pnd_Bene_Cnt_Cntgnt;
    private DbsField pnd_X;
    private DbsField pnd_Rsdnce_St;

    private DbsGroup pnd_Rsdnce_St__R_Field_1;
    private DbsField pnd_Rsdnce_St_Pnd_Rsdnce_St_A;
    private DbsField pnd_Issue_St;

    private DbsGroup pnd_Issue_St__R_Field_2;
    private DbsField pnd_Issue_St_Pnd_Issue_St_A;
    private DbsField pnd_Escape_Top;
    private DbsField pnd_City_Done;
    private DbsField pnd_Y;
    private DbsField pnd_D;
    private DbsField pnd_C;
    private DbsField pnd_Err_Found;
    private DbsField pnd_Err_Fatal;
    private DbsField pnd_Bypass_Record;
    private DbsField pnd_New_Set_Of_Text;
    private DbsField pnd_First_Omni_Record;
    private DbsField pnd_Bene_Info_3_Already_Saved;
    private DbsField pnd_Err_Total;
    private DbsField pnd_Byp_Total;
    private DbsField pnd_Error_String;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Rollovr_Ira;
    private DbsField ltfp0710;

    private DbsGroup ltfp0710__R_Field_3;
    private DbsField ltfp0710_Pnd_P_Ltfp0710;

    private DbsGroup ltfp0710__R_Field_4;
    private DbsField ltfp0710_Pnd_P_Mdo_Letter_Type;
    private DbsField ltfp0710_Pnd_P_Mdo_Letter_Add_Chg;

    private DbsGroup ltfp0710_Pnd_P_Omni_Rec_Key;
    private DbsField ltfp0710_Pnd_P_Omni_Plan_Id;
    private DbsField ltfp0710_Pnd_P_Omni_Sub_Plan_Id;

    private DbsGroup ltfp0710__R_Field_5;
    private DbsField ltfp0710_Pnd_P_Omni_Ssn;
    private DbsField ltfp0710_Pnd_P_Omni_Sub_Plan;
    private DbsField ltfp0710_Pnd_P_Pin;

    private DbsGroup ltfp0710__R_Field_6;

    private DbsGroup ltfp0710_Pnd_P_Pin_N;
    private DbsField ltfp0710_Pnd_P_Pin_N_7;
    private DbsField ltfp0710_Pnd_P_Pin_Filler;
    private DbsField ltfp0710_Pnd_P_Mdo_Tiaa_Contract;
    private DbsField ltfp0710_Pnd_P_Mdo_Cref_Cert;
    private DbsField ltfp0710_Pnd_P_Omni_Plan_Name;
    private DbsField ltfp0710_Pnd_P_Participant_Name;
    private DbsField ltfp0710_Pnd_P_Frst_Annt_Ssn;
    private DbsField ltfp0710_Pnd_P_Frst_Annt_Frst_Nme;
    private DbsField ltfp0710_Pnd_P_Frst_Annt_Lst_Nme;
    private DbsField ltfp0710_Pnd_P_Frst_Annt_Dob;

    private DbsGroup ltfp0710__R_Field_7;
    private DbsField ltfp0710_Pnd_P_Frst_Annt_Dob_N;
    private DbsField ltfp0710_Pnd_P_Frst_Annt_Rsdnc_Cde;
    private DbsField ltfp0710_Pnd_P_Frst_Annt_Ctznshp;
    private DbsField ltfp0710_Pnd_P_Frst_Annt_Sex_Cde;
    private DbsField ltfp0710_Pnd_P_Frst_Annt_Calc_Meth;
    private DbsField ltfp0710_Pnd_P_Mdo_Payment_Mode;
    private DbsField ltfp0710_Pnd_P_Annty_Strt_Dte;

    private DbsGroup ltfp0710__R_Field_8;
    private DbsField ltfp0710_Pnd_P_Annty_Strt_Dte_N;
    private DbsField ltfp0710_Pnd_P_Next_Pay_Date;

    private DbsGroup ltfp0710__R_Field_9;
    private DbsField ltfp0710_Pnd_P_Next_Pay_Date_N;
    private DbsField ltfp0710_Pnd_P_Pro_Rata;

    private DbsGroup ltfp0710_Pnd_P_Table_1;
    private DbsField ltfp0710_Pnd_P_Ticker_Symbol;
    private DbsField ltfp0710_Pnd_P_Allocation_Ind;
    private DbsField ltfp0710_Pnd_P_Percent_Amt;
    private DbsField ltfp0710_Pnd_P_Allocation_Amt;
    private DbsField ltfp0710_Pnd_P_Mdo_Contract_Type;
    private DbsField ltfp0710_Pnd_P_Settled_Tiaa_Contract_Nbr;
    private DbsField ltfp0710_Pnd_P_Settled_Cref_Contract_Nbr;
    private DbsField ltfp0710_Pnd_P_Settlement_Ind;
    private DbsField ltfp0710_Pnd_P_Tiaa_Accum_Settle_Amt;
    private DbsField ltfp0710_Pnd_P_Tiaa_Accum_Settle_Typ;
    private DbsField ltfp0710_Pnd_P_Rea_Accum_Settle_Amt;
    private DbsField ltfp0710_Pnd_P_Rea_Accum_Settle_Typ;
    private DbsField ltfp0710_Pnd_P_Cref_Accum_Settle_Amt;
    private DbsField ltfp0710_Pnd_P_Cref_Accum_Settle_Typ;
    private DbsField ltfp0710_Pnd_P_Cref_Annty_Payment;
    private DbsField ltfp0710_Pnd_P_Cref_Mnthly_Nbr_Units;
    private DbsField ltfp0710_Pnd_P_Rea_Mnthly_Nbr_Units;
    private DbsField ltfp0710_Pnd_P_Nra_Flag;
    private DbsField ltfp0710_Pnd_P_Fed_Election_Option;
    private DbsField ltfp0710_Pnd_P_Fed_Tax_Amt_Tiaa_Rea;
    private DbsField ltfp0710_Pnd_P_Fed_Tax_Amt_Ind_Tiaa_Rea;
    private DbsField ltfp0710_Pnd_P_Fed_Tax_Amt_Cref;
    private DbsField ltfp0710_Pnd_P_Fed_Tax_Amt_Ind_Cref;
    private DbsField ltfp0710_Pnd_P_Sta_Election_Option;
    private DbsField ltfp0710_Pnd_P_Sta_Tax_Amt_Tiaa_Rea;
    private DbsField ltfp0710_Pnd_P_Sta_Tax_Amt_Ind_Tiaa_Rea;
    private DbsField ltfp0710_Pnd_P_Sta_Tax_Amt_Cref;
    private DbsField ltfp0710_Pnd_P_Sta_Tax_Amt_Ind_Cref;
    private DbsField ltfp0710_Pnd_P_Rqst_Id_Key;
    private DbsField ltfp0710_Pnd_P_Opn_Clsd_Ind;
    private DbsField ltfp0710_Pnd_P_Status_Cd;
    private DbsField ltfp0710_Pnd_P_Cntrct_Type;
    private DbsField ltfp0710_Pnd_P_Cntrct_Apprvl_Ind;
    private DbsField ltfp0710_Pnd_P_Rqst_Id;
    private DbsField ltfp0710_Pnd_P_Cntrct_Print_Dte;
    private DbsField ltfp0710_Pnd_P_Extract_Date;
    private DbsField ltfp0710_Pnd_P_Appl_Rcvd_Dte;
    private DbsField ltfp0710_Pnd_P_Appl_Rcvd_User_Id;
    private DbsField ltfp0710_Pnd_P_Appl_Entry_User_Id;
    private DbsField ltfp0710_Pnd_P_Annty_Option;
    private DbsField ltfp0710_Pnd_P_Grnted_Int_Rate;
    private DbsField ltfp0710_Pnd_P_Mdo_Traditional_Amt;
    private DbsField ltfp0710_Pnd_P_Mdo_Tiaa_Int_Pymnt_Amt;
    private DbsField ltfp0710_Pnd_P_Mdo_Cref_Int_Pymnt_Amt;
    private DbsField ltfp0710_Pnd_P_Pi_Task_Id;

    private DbsGroup ltfp0710_Pnd_P_Ppymt_Dest_Name;
    private DbsField ltfp0710_Pnd_P_Ppymt_Dest_Name_35;
    private DbsField ltfp0710_Pnd_P_Ppymt_Dest_Name_5;
    private DbsField ltfp0710_Pnd_P_Ppymt_Dest_Addr1;
    private DbsField ltfp0710_Pnd_P_Ppymt_Dest_Addr2;
    private DbsField ltfp0710_Pnd_P_Ppymt_Dest_Addr3;
    private DbsField ltfp0710_Pnd_P_Ppymt_Dest_City;
    private DbsField ltfp0710_Pnd_P_Ppymt_Dest_State;
    private DbsField ltfp0710_Pnd_P_Ppymt_Zip;
    private DbsField ltfp0710_Pnd_P_Spymt_Dest_Name;
    private DbsField ltfp0710_Pnd_P_Spymt_Dest_Addr1;
    private DbsField ltfp0710_Pnd_P_Spymt_Dest_Addr2;
    private DbsField ltfp0710_Pnd_P_Spymt_Dest_Addr3;
    private DbsField ltfp0710_Pnd_P_Spymt_Dest_City;
    private DbsField ltfp0710_Pnd_P_Spymt_Dest_State;
    private DbsField ltfp0710_Pnd_P_Spymt_Zip;
    private DbsField ltfp0710_Pnd_P_Spymt_Dest_Acct_Nbr;
    private DbsField ltfp0710_Pnd_P_Spymt_Dest_Trnst_Cde;
    private DbsField ltfp0710_Pnd_P_Spymt_Dest_Acct_Typ;
    private DbsField ltfp0710_Pnd_P_Cpymt_Dest_Name;
    private DbsField ltfp0710_Pnd_P_Cpymt_Dest_Addr1;
    private DbsField ltfp0710_Pnd_P_Cpymt_Dest_Addr2;
    private DbsField ltfp0710_Pnd_P_Cpymt_Dest_Addr3;
    private DbsField ltfp0710_Pnd_P_Cpymt_Dest_City;
    private DbsField ltfp0710_Pnd_P_Cpymt_Dest_State;
    private DbsField ltfp0710_Pnd_P_Cpymt_Zip;
    private DbsField ltfp0710_Pnd_P_Cpymt_Dest_Acct_Nbr;
    private DbsField ltfp0710_Pnd_P_Cpymt_Dest_Trnst_Cde;
    private DbsField ltfp0710_Pnd_P_Cpymt_Dest_Acct_Typ;
    private DbsField ltfp0710_Pnd_P_Cpymt_Payee_Amt;
    private DbsField ltfp0710_Pnd_P_Cpymt_Payee_Type;
    private DbsField ltfp0710_Pnd_P_Two_Payments_In_One_Year;
    private DbsField ltfp0710_Pnd_P_Fed_Exemption;
    private DbsField ltfp0710_Pnd_P_St_Exemption;
    private DbsField ltfp0710_Pnd_P_Mailing_Country_Code;
    private DbsField ltfp0710_Pnd_P_Cod_Id;
    private DbsField ltfp0710_Pnd_P_Minimum_Req_Amt;

    private DbsGroup ltfp0710__R_Field_10;
    private DbsField ltfp0710_Pnd_P_Minimum_Req_Amt_N;
    private DbsField ltfp0710_Pnd_P_Payee_Cde;
    private DbsField ltfp0710_Pnd_P_Has_Roth_Money_Ind;
    private DbsField ltfp0710_Pnd_P_Original_Issue;
    private DbsField ltfp0710_Pnd_P_Fed_Marital_Status;
    private DbsField ltfp0710_Pnd_P_St_Marital_Status;
    private DbsField ltfp0710_Pnd_P_Org_Prt_Date_Of_Death;

    private DbsGroup ltfp0710__R_Field_11;
    private DbsField ltfp0710_Pnd_P_Org_Prt_Date_Of_Death_N;
    private DbsField ltfp0710_Pnd_P_Org_Prt_Date_Of_Birth;

    private DbsGroup ltfp0710__R_Field_12;
    private DbsField ltfp0710_Pnd_P_Org_Prt_Date_Of_Birth_N;
    private DbsField ltfp0710_Pnd_P_Orgnl_Prtcpnt_Frst_Nme;
    private DbsField ltfp0710_Pnd_P_Orgnl_Prtcpnt_Lst_Nme;
    private DbsField ltfp0710_Pnd_P_Invest_Process_Dt;
    private DbsField ltfp0710_Pnd_P_Invest_Amt_Total;

    private DbsGroup ltfp0710_Pnd_P_Invest_From_Info;
    private DbsField ltfp0710_Pnd_P_Invest_Num;
    private DbsField ltfp0710_Pnd_P_Invest_Orig_Amt;
    private DbsField ltfp0710_Pnd_P_Invest_Tiaa_Num_1;
    private DbsField ltfp0710_Pnd_P_Invest_Cref_Num_1;
    private DbsField ltfp0710_Pnd_P_Invest_Plan_Name;
    private DbsField ltfp0710_Pnd_P_Invest_Data;

    private DbsGroup ltfp0710__R_Field_13;

    private DbsGroup ltfp0710_Pnd_P_Invest_Info;
    private DbsField ltfp0710_Pnd_P_Invest_Ticker;
    private DbsField ltfp0710_Pnd_P_Invest_Amt;
    private DbsField ltfp0710_Pnd_P_Invest_Unit_Price;
    private DbsField ltfp0710_Pnd_P_Invest_Units;
    private DbsField ltfp0710_Pnd_P_Res_Addr1;
    private DbsField ltfp0710_Pnd_P_Res_Addr2;
    private DbsField ltfp0710_Pnd_P_Res_Addr3;
    private DbsField ltfp0710_Pnd_P_Res_City;
    private DbsField ltfp0710_Pnd_P_Res_State;
    private DbsField ltfp0710_Pnd_P_Res_Zip;
    private DbsField ltfp0710_Pnd_P_Res_Country_Code;
    private DbsField ltfp0710_Pnd_Acceptance_Ind;
    private DbsField ltfp0710_Pnd_Relation_To_Decedent;
    private DbsField ltfp0710_Pnd_Ssn_Tin_Ind;
    private DbsField ltfp0710_Pnd_Decedent_Contract;

    private DbsGroup ltfp0710__R_Field_14;
    private DbsField ltfp0710_Pnd_B_Ltfp0710;

    private DbsGroup ltfp0710__R_Field_15;
    private DbsField ltfp0710_Pnd_B_Mdo_Letter_Type;
    private DbsField ltfp0710_Pnd_B_Mdo_Letter_Add_Chg;

    private DbsGroup ltfp0710_Pnd_B_Omni_Rec_Key;
    private DbsField ltfp0710_Pnd_B_Omni_Plan_Id;
    private DbsField ltfp0710_Pnd_B_Omni_Sub_Plan_Id;
    private DbsField ltfp0710_Pnd_B_Pin;

    private DbsGroup ltfp0710__R_Field_16;
    private DbsField ltfp0710_Pnd_Pnd_B_Pin_N;
    private DbsField ltfp0710_Pnd_B_Pin_Filler;
    private DbsField ltfp0710_Pnd_B_Mdo_Tiaa_Contract;
    private DbsField ltfp0710_Pnd_B_Mdo_Cref_Cert;
    private DbsField ltfp0710_Pnd_B_Omni_Plan_Name;
    private DbsField ltfp0710_Pnd_B_Participant_Name;
    private DbsField ltfp0710_Pnd_B_Bene_Category;
    private DbsField ltfp0710_Pnd_B_Bene_Name;
    private DbsField ltfp0710_Pnd_B_Bene_Birth_Date;

    private DbsGroup ltfp0710__R_Field_17;
    private DbsField ltfp0710_Pnd_B_Bene_Birth_Date_N;
    private DbsField ltfp0710_Pnd_B_Bene_Ssn;
    private DbsField ltfp0710_Pnd_B_Bene_Relationship;
    private DbsField ltfp0710_Pnd_B_Bene_Allocation_Pct;
    private DbsField ltfp0710_Pnd_B_Prim_Child_Prov;
    private DbsField ltfp0710_Pnd_B_Cont_Child_Prov;
    private DbsField ltfp0710_Pnd_B_Cal_Bene_Meth;
    private DbsField ltfp0710_Pnd_B_Filler1;
    private DbsField ltfp0710_Pnd_B_Filler2;
    private DbsField ltfp0710_Pnd_B_Filler3;
    private DbsField ltfp0710_Pnd_B_Filler4;
    private DbsField ltfp0710_Pnd_B_Filler5;
    private DbsField ltfp0710_Pnd_B_Filler6;
    private DbsField ltfp0710_Pnd_B_Filler7;
    private DbsField ltfp0710_Pnd_B_Filler8;
    private DbsField ltfp0710_Pnd_B_Filler9;
    private DbsField ltfp0710_Pnd_B_Filler10;
    private DbsField ltfp0710_Pnd_B_Filler14;
    private DbsField pnd_P_Pin_A_12;

    private DbsGroup pnd_P_Pin_A_12__R_Field_18;
    private DbsField pnd_P_Pin_A_12_Pnd_P_Pin_N_12;
    private DbsField pnd_Start;
    private DbsField pnd_Lcl_Contract;
    private DbsField pnd_Acct_Cde;
    private DbsField pnd_Rltn_Cde;
    private DbsField pnd_R_Cde;
    private DbsField pnd_Found;
    private DbsField pnd_Lcl_Bene_Calc;
    private DbsField pnd_Err_Message;
    private DbsField pnd_Total_Add;
    private DbsField pnd_Total_Dup;
    private DbsField pnd_Total_Err;
    private DbsField pnd_Fund_Count;
    private DbsField pnd_Bene_Count;
    private DbsField pnd_Fund_Idx;
    private DbsField pnd_Bene_Idx;
    private DbsField pnd_Cis_Sg_Text_Udf_2;

    private DbsGroup pnd_Cis_Sg_Text_Udf_2__R_Field_19;
    private DbsField pnd_Cis_Sg_Text_Udf_2_Pnd_Sg_Udf2_F1_Roth_Ind;
    private DbsField pnd_Cis_Sg_Text_Udf_2_Pnd_Sg_Udf2_F2_Payee_Cde;
    private DbsField pnd_Cnt1;
    private DbsField pnd_Cnt2;
    private DbsField pnd_Cnt3;
    private DbsField pnd_Cnt4;
    private DbsField pnd_Cntx;

    private DbsGroup pnd_Hold_Address;
    private DbsField pnd_Hold_Address_Pnd_Hdest_Addr1;
    private DbsField pnd_Hold_Address_Pnd_Hdest_Addr2;
    private DbsField pnd_Hold_Address_Pnd_Hdest_Addr3;
    private DbsField pnd_Hold_Address_Pnd_Hdest_City;
    private DbsField pnd_Hold_Address_Pnd_Hdest_State;
    private DbsField pnd_Hold_Address_Pnd_Hzip;
    private DbsField pnd_Foreign_Or_Canada;
    private DbsField pnd_Addr_Indx;
    private DbsField pnd_Debug;
    private DbsField pnd_Hold_Res_State;
    private DbsField pnd_Save_Omni_Plan_Id;
    private DbsField pnd_Save_Omni_Sub_Plan_Id;

    private DbsGroup pnd_Save_Omni_Sub_Plan_Id__R_Field_20;
    private DbsField pnd_Save_Omni_Sub_Plan_Id_Pnd_Save_Omni_Ssn;
    private DbsField pnd_Save_Omni_Sub_Plan_Id_Pnd_Save_Omni_Sub_Plan;
    private DbsField pnd_Save_Mdo_Tiaa_Contract;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCispartv = new LdaCispartv();
        registerRecord(ldaCispartv);
        registerRecord(ldaCispartv.getVw_cis_Part_View());
        ldaCisl2020 = new LdaCisl2020();
        registerRecord(ldaCisl2020);
        registerRecord(ldaCisl2020.getVw_cis_Bene_File_01());
        ldaAppl170 = new LdaAppl170();
        registerRecord(ldaAppl170);
        localVariables = new DbsRecord();
        pdaCisa4000 = new PdaCisa4000(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);

        // Local Variables

        pnd_In_Translte_File = localVariables.newGroupInRecord("pnd_In_Translte_File", "#IN-TRANSLTE-FILE");
        pnd_In_Translte_File_Pnd_In_Translte_Ticker = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Ticker", "#IN-TRANSLTE-TICKER", 
            FieldType.STRING, 10);
        pnd_In_Translte_File_Pnd_F1 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name", "#IN-TRANSLTE-TICKER-NAME", 
            FieldType.STRING, 26);
        pnd_In_Translte_File_Pnd_F2 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq", "#IN-TRANSLTE-FUND-SEQ", 
            FieldType.NUMERIC, 2);
        pnd_In_Translte_File_Pnd_F3 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Pull_Code = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Pull_Code", "#IN-TRANSLTE-PULL-CODE", 
            FieldType.STRING, 4);
        pnd_In_Translte_File_Pnd_F4 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_In_Translte_Fund_Type = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_In_Translte_Fund_Type", "#IN-TRANSLTE-FUND-TYPE", 
            FieldType.STRING, 1);
        pnd_In_Translte_File_Pnd_F5 = pnd_In_Translte_File.newFieldInGroup("pnd_In_Translte_File_Pnd_F5", "#F5", FieldType.STRING, 153);

        pnd_Translte_File = localVariables.newGroupArrayInRecord("pnd_Translte_File", "#TRANSLTE-FILE", new DbsArrayController(1, 20));
        pnd_Translte_File_Pnd_Translte_Ticker = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Ticker", "#TRANSLTE-TICKER", FieldType.STRING, 
            10);
        pnd_Translte_File_Pnd_F1 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Ticker_Name = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Ticker_Name", "#TRANSLTE-TICKER-NAME", 
            FieldType.STRING, 26);
        pnd_Translte_File_Pnd_F2 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Fund_Seq = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Fund_Seq", "#TRANSLTE-FUND-SEQ", FieldType.NUMERIC, 
            2);
        pnd_Translte_File_Pnd_F3 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Pull_Code = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Pull_Code", "#TRANSLTE-PULL-CODE", 
            FieldType.STRING, 4);
        pnd_Translte_File_Pnd_F4 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Fund_Type = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Fund_Type", "#TRANSLTE-FUND-TYPE", 
            FieldType.STRING, 1);
        pnd_Translte_File_Pnd_F5 = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_F5", "#F5", FieldType.STRING, 1);
        pnd_Translte_File_Pnd_Translte_Error_Message = pnd_Translte_File.newFieldInGroup("pnd_Translte_File_Pnd_Translte_Error_Message", "#TRANSLTE-ERROR-MESSAGE", 
            FieldType.STRING, 40);

        pnd_In_Bene_Relationship = localVariables.newGroupInRecord("pnd_In_Bene_Relationship", "#IN-BENE-RELATIONSHIP");
        pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Seq = pnd_In_Bene_Relationship.newFieldInGroup("pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Seq", 
            "#IN-BENE-RELATIONSHIP-SEQ", FieldType.NUMERIC, 2);
        pnd_In_Bene_Relationship_Pnd_F1 = pnd_In_Bene_Relationship.newFieldInGroup("pnd_In_Bene_Relationship_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Type = pnd_In_Bene_Relationship.newFieldInGroup("pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Type", 
            "#IN-BENE-RELATIONSHIP-TYPE", FieldType.STRING, 1);
        pnd_In_Bene_Relationship_Pnd_F2 = pnd_In_Bene_Relationship.newFieldInGroup("pnd_In_Bene_Relationship_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Name = pnd_In_Bene_Relationship.newFieldInGroup("pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Name", 
            "#IN-BENE-RELATIONSHIP-NAME", FieldType.STRING, 10);
        pnd_In_Bene_Relationship_Pnd_F3 = pnd_In_Bene_Relationship.newFieldInGroup("pnd_In_Bene_Relationship_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Code = pnd_In_Bene_Relationship.newFieldInGroup("pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Code", 
            "#IN-BENE-RELATIONSHIP-CODE", FieldType.STRING, 2);
        pnd_In_Bene_Relationship_Pnd_F4 = pnd_In_Bene_Relationship.newFieldInGroup("pnd_In_Bene_Relationship_Pnd_F4", "#F4", FieldType.STRING, 182);

        pnd_Bene_Relationship = localVariables.newGroupArrayInRecord("pnd_Bene_Relationship", "#BENE-RELATIONSHIP", new DbsArrayController(1, 50));
        pnd_Bene_Relationship_Pnd_Bene_Relationship_Seq = pnd_Bene_Relationship.newFieldInGroup("pnd_Bene_Relationship_Pnd_Bene_Relationship_Seq", "#BENE-RELATIONSHIP-SEQ", 
            FieldType.NUMERIC, 2);
        pnd_Bene_Relationship_Pnd_F1 = pnd_Bene_Relationship.newFieldInGroup("pnd_Bene_Relationship_Pnd_F1", "#F1", FieldType.STRING, 1);
        pnd_Bene_Relationship_Pnd_Bene_Relationship_Type = pnd_Bene_Relationship.newFieldInGroup("pnd_Bene_Relationship_Pnd_Bene_Relationship_Type", "#BENE-RELATIONSHIP-TYPE", 
            FieldType.STRING, 1);
        pnd_Bene_Relationship_Pnd_F2 = pnd_Bene_Relationship.newFieldInGroup("pnd_Bene_Relationship_Pnd_F2", "#F2", FieldType.STRING, 1);
        pnd_Bene_Relationship_Pnd_Bene_Relationship_Name = pnd_Bene_Relationship.newFieldInGroup("pnd_Bene_Relationship_Pnd_Bene_Relationship_Name", "#BENE-RELATIONSHIP-NAME", 
            FieldType.STRING, 10);
        pnd_Bene_Relationship_Pnd_F3 = pnd_Bene_Relationship.newFieldInGroup("pnd_Bene_Relationship_Pnd_F3", "#F3", FieldType.STRING, 1);
        pnd_Bene_Relationship_Pnd_Bene_Relationship_Code = pnd_Bene_Relationship.newFieldInGroup("pnd_Bene_Relationship_Pnd_Bene_Relationship_Code", "#BENE-RELATIONSHIP-CODE", 
            FieldType.STRING, 2);
        pnd_Bene_Relationship_Pnd_F4 = pnd_Bene_Relationship.newFieldInGroup("pnd_Bene_Relationship_Pnd_F4", "#F4", FieldType.STRING, 1);
        pnd_Bene_Relationship_Pnd_Bene_Relationship_Message = pnd_Bene_Relationship.newFieldInGroup("pnd_Bene_Relationship_Pnd_Bene_Relationship_Message", 
            "#BENE-RELATIONSHIP-MESSAGE", FieldType.STRING, 40);
        pnd_Bene_Alloc = localVariables.newFieldInRecord("pnd_Bene_Alloc", "#BENE-ALLOC", FieldType.NUMERIC, 5, 2);
        pnd_Bene_Cnt_Prmry = localVariables.newFieldInRecord("pnd_Bene_Cnt_Prmry", "#BENE-CNT-PRMRY", FieldType.NUMERIC, 9);
        pnd_Bene_Cnt_Cntgnt = localVariables.newFieldInRecord("pnd_Bene_Cnt_Cntgnt", "#BENE-CNT-CNTGNT", FieldType.NUMERIC, 9);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 9);
        pnd_Rsdnce_St = localVariables.newFieldInRecord("pnd_Rsdnce_St", "#RSDNCE-ST", FieldType.NUMERIC, 2);

        pnd_Rsdnce_St__R_Field_1 = localVariables.newGroupInRecord("pnd_Rsdnce_St__R_Field_1", "REDEFINE", pnd_Rsdnce_St);
        pnd_Rsdnce_St_Pnd_Rsdnce_St_A = pnd_Rsdnce_St__R_Field_1.newFieldInGroup("pnd_Rsdnce_St_Pnd_Rsdnce_St_A", "#RSDNCE-ST-A", FieldType.STRING, 2);
        pnd_Issue_St = localVariables.newFieldInRecord("pnd_Issue_St", "#ISSUE-ST", FieldType.NUMERIC, 2);

        pnd_Issue_St__R_Field_2 = localVariables.newGroupInRecord("pnd_Issue_St__R_Field_2", "REDEFINE", pnd_Issue_St);
        pnd_Issue_St_Pnd_Issue_St_A = pnd_Issue_St__R_Field_2.newFieldInGroup("pnd_Issue_St_Pnd_Issue_St_A", "#ISSUE-ST-A", FieldType.STRING, 2);
        pnd_Escape_Top = localVariables.newFieldInRecord("pnd_Escape_Top", "#ESCAPE-TOP", FieldType.BOOLEAN, 1);
        pnd_City_Done = localVariables.newFieldInRecord("pnd_City_Done", "#CITY-DONE", FieldType.BOOLEAN, 1);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_Err_Found = localVariables.newFieldInRecord("pnd_Err_Found", "#ERR-FOUND", FieldType.BOOLEAN, 1);
        pnd_Err_Fatal = localVariables.newFieldInRecord("pnd_Err_Fatal", "#ERR-FATAL", FieldType.BOOLEAN, 1);
        pnd_Bypass_Record = localVariables.newFieldInRecord("pnd_Bypass_Record", "#BYPASS-RECORD", FieldType.BOOLEAN, 1);
        pnd_New_Set_Of_Text = localVariables.newFieldInRecord("pnd_New_Set_Of_Text", "#NEW-SET-OF-TEXT", FieldType.BOOLEAN, 1);
        pnd_First_Omni_Record = localVariables.newFieldInRecord("pnd_First_Omni_Record", "#FIRST-OMNI-RECORD", FieldType.BOOLEAN, 1);
        pnd_Bene_Info_3_Already_Saved = localVariables.newFieldInRecord("pnd_Bene_Info_3_Already_Saved", "#BENE-INFO-3-ALREADY-SAVED", FieldType.BOOLEAN, 
            1);
        pnd_Err_Total = localVariables.newFieldInRecord("pnd_Err_Total", "#ERR-TOTAL", FieldType.PACKED_DECIMAL, 5);
        pnd_Byp_Total = localVariables.newFieldInRecord("pnd_Byp_Total", "#BYP-TOTAL", FieldType.PACKED_DECIMAL, 5);
        pnd_Error_String = localVariables.newFieldInRecord("pnd_Error_String", "#ERROR-STRING", FieldType.STRING, 20);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 65);
        pnd_Rollovr_Ira = localVariables.newFieldInRecord("pnd_Rollovr_Ira", "#ROLLOVR-IRA", FieldType.STRING, 1);
        ltfp0710 = localVariables.newFieldArrayInRecord("ltfp0710", "LTFP0710", FieldType.STRING, 1, new DbsArrayController(1, 9119));

        ltfp0710__R_Field_3 = localVariables.newGroupInRecord("ltfp0710__R_Field_3", "REDEFINE", ltfp0710);
        ltfp0710_Pnd_P_Ltfp0710 = ltfp0710__R_Field_3.newFieldArrayInGroup("ltfp0710_Pnd_P_Ltfp0710", "#P-LTFP0710", FieldType.STRING, 1, new DbsArrayController(1, 
            9119));

        ltfp0710__R_Field_4 = ltfp0710__R_Field_3.newGroupInGroup("ltfp0710__R_Field_4", "REDEFINE", ltfp0710_Pnd_P_Ltfp0710);
        ltfp0710_Pnd_P_Mdo_Letter_Type = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Mdo_Letter_Type", "#P-MDO-LETTER-TYPE", FieldType.STRING, 
            4);
        ltfp0710_Pnd_P_Mdo_Letter_Add_Chg = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Mdo_Letter_Add_Chg", "#P-MDO-LETTER-ADD-CHG", FieldType.STRING, 
            1);

        ltfp0710_Pnd_P_Omni_Rec_Key = ltfp0710__R_Field_4.newGroupInGroup("ltfp0710_Pnd_P_Omni_Rec_Key", "#P-OMNI-REC-KEY");
        ltfp0710_Pnd_P_Omni_Plan_Id = ltfp0710_Pnd_P_Omni_Rec_Key.newFieldInGroup("ltfp0710_Pnd_P_Omni_Plan_Id", "#P-OMNI-PLAN-ID", FieldType.STRING, 
            6);
        ltfp0710_Pnd_P_Omni_Sub_Plan_Id = ltfp0710_Pnd_P_Omni_Rec_Key.newFieldInGroup("ltfp0710_Pnd_P_Omni_Sub_Plan_Id", "#P-OMNI-SUB-PLAN-ID", FieldType.STRING, 
            17);

        ltfp0710__R_Field_5 = ltfp0710_Pnd_P_Omni_Rec_Key.newGroupInGroup("ltfp0710__R_Field_5", "REDEFINE", ltfp0710_Pnd_P_Omni_Sub_Plan_Id);
        ltfp0710_Pnd_P_Omni_Ssn = ltfp0710__R_Field_5.newFieldInGroup("ltfp0710_Pnd_P_Omni_Ssn", "#P-OMNI-SSN", FieldType.NUMERIC, 9);
        ltfp0710_Pnd_P_Omni_Sub_Plan = ltfp0710__R_Field_5.newFieldInGroup("ltfp0710_Pnd_P_Omni_Sub_Plan", "#P-OMNI-SUB-PLAN", FieldType.STRING, 8);
        ltfp0710_Pnd_P_Pin = ltfp0710_Pnd_P_Omni_Rec_Key.newFieldInGroup("ltfp0710_Pnd_P_Pin", "#P-PIN", FieldType.STRING, 13);

        ltfp0710__R_Field_6 = ltfp0710_Pnd_P_Omni_Rec_Key.newGroupInGroup("ltfp0710__R_Field_6", "REDEFINE", ltfp0710_Pnd_P_Pin);

        ltfp0710_Pnd_P_Pin_N = ltfp0710__R_Field_6.newGroupInGroup("ltfp0710_Pnd_P_Pin_N", "#P-PIN-N");
        ltfp0710_Pnd_P_Pin_N_7 = ltfp0710_Pnd_P_Pin_N.newFieldInGroup("ltfp0710_Pnd_P_Pin_N_7", "#P-PIN-N-7", FieldType.NUMERIC, 7);
        ltfp0710_Pnd_P_Pin_Filler = ltfp0710_Pnd_P_Pin_N.newFieldInGroup("ltfp0710_Pnd_P_Pin_Filler", "#P-PIN-FILLER", FieldType.STRING, 6);
        ltfp0710_Pnd_P_Mdo_Tiaa_Contract = ltfp0710_Pnd_P_Omni_Rec_Key.newFieldInGroup("ltfp0710_Pnd_P_Mdo_Tiaa_Contract", "#P-MDO-TIAA-CONTRACT", FieldType.STRING, 
            10);
        ltfp0710_Pnd_P_Mdo_Cref_Cert = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Mdo_Cref_Cert", "#P-MDO-CREF-CERT", FieldType.STRING, 10);
        ltfp0710_Pnd_P_Omni_Plan_Name = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Omni_Plan_Name", "#P-OMNI-PLAN-NAME", FieldType.STRING, 50);
        ltfp0710_Pnd_P_Participant_Name = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Participant_Name", "#P-PARTICIPANT-NAME", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Frst_Annt_Ssn = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Frst_Annt_Ssn", "#P-FRST-ANNT-SSN", FieldType.NUMERIC, 9);
        ltfp0710_Pnd_P_Frst_Annt_Frst_Nme = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Frst_Annt_Frst_Nme", "#P-FRST-ANNT-FRST-NME", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Frst_Annt_Lst_Nme = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Frst_Annt_Lst_Nme", "#P-FRST-ANNT-LST-NME", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Frst_Annt_Dob = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Frst_Annt_Dob", "#P-FRST-ANNT-DOB", FieldType.STRING, 8);

        ltfp0710__R_Field_7 = ltfp0710__R_Field_4.newGroupInGroup("ltfp0710__R_Field_7", "REDEFINE", ltfp0710_Pnd_P_Frst_Annt_Dob);
        ltfp0710_Pnd_P_Frst_Annt_Dob_N = ltfp0710__R_Field_7.newFieldInGroup("ltfp0710_Pnd_P_Frst_Annt_Dob_N", "#P-FRST-ANNT-DOB-N", FieldType.NUMERIC, 
            8);
        ltfp0710_Pnd_P_Frst_Annt_Rsdnc_Cde = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Frst_Annt_Rsdnc_Cde", "#P-FRST-ANNT-RSDNC-CDE", FieldType.STRING, 
            2);
        ltfp0710_Pnd_P_Frst_Annt_Ctznshp = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Frst_Annt_Ctznshp", "#P-FRST-ANNT-CTZNSHP", FieldType.STRING, 
            2);
        ltfp0710_Pnd_P_Frst_Annt_Sex_Cde = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Frst_Annt_Sex_Cde", "#P-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Frst_Annt_Calc_Meth = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Frst_Annt_Calc_Meth", "#P-FRST-ANNT-CALC-METH", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Mdo_Payment_Mode = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Mdo_Payment_Mode", "#P-MDO-PAYMENT-MODE", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Annty_Strt_Dte = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Annty_Strt_Dte", "#P-ANNTY-STRT-DTE", FieldType.STRING, 8);

        ltfp0710__R_Field_8 = ltfp0710__R_Field_4.newGroupInGroup("ltfp0710__R_Field_8", "REDEFINE", ltfp0710_Pnd_P_Annty_Strt_Dte);
        ltfp0710_Pnd_P_Annty_Strt_Dte_N = ltfp0710__R_Field_8.newFieldInGroup("ltfp0710_Pnd_P_Annty_Strt_Dte_N", "#P-ANNTY-STRT-DTE-N", FieldType.NUMERIC, 
            8);
        ltfp0710_Pnd_P_Next_Pay_Date = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Next_Pay_Date", "#P-NEXT-PAY-DATE", FieldType.STRING, 8);

        ltfp0710__R_Field_9 = ltfp0710__R_Field_4.newGroupInGroup("ltfp0710__R_Field_9", "REDEFINE", ltfp0710_Pnd_P_Next_Pay_Date);
        ltfp0710_Pnd_P_Next_Pay_Date_N = ltfp0710__R_Field_9.newFieldInGroup("ltfp0710_Pnd_P_Next_Pay_Date_N", "#P-NEXT-PAY-DATE-N", FieldType.NUMERIC, 
            8);
        ltfp0710_Pnd_P_Pro_Rata = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Pro_Rata", "#P-PRO-RATA", FieldType.STRING, 1);

        ltfp0710_Pnd_P_Table_1 = ltfp0710__R_Field_4.newGroupArrayInGroup("ltfp0710_Pnd_P_Table_1", "#P-TABLE-1", new DbsArrayController(1, 60));
        ltfp0710_Pnd_P_Ticker_Symbol = ltfp0710_Pnd_P_Table_1.newFieldInGroup("ltfp0710_Pnd_P_Ticker_Symbol", "#P-TICKER-SYMBOL", FieldType.STRING, 10);
        ltfp0710_Pnd_P_Allocation_Ind = ltfp0710_Pnd_P_Table_1.newFieldInGroup("ltfp0710_Pnd_P_Allocation_Ind", "#P-ALLOCATION-IND", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Percent_Amt = ltfp0710_Pnd_P_Table_1.newFieldInGroup("ltfp0710_Pnd_P_Percent_Amt", "#P-PERCENT-AMT", FieldType.NUMERIC, 11, 2);
        ltfp0710_Pnd_P_Allocation_Amt = ltfp0710_Pnd_P_Table_1.newFieldInGroup("ltfp0710_Pnd_P_Allocation_Amt", "#P-ALLOCATION-AMT", FieldType.NUMERIC, 
            11, 2);
        ltfp0710_Pnd_P_Mdo_Contract_Type = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Mdo_Contract_Type", "#P-MDO-CONTRACT-TYPE", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Settled_Tiaa_Contract_Nbr = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Settled_Tiaa_Contract_Nbr", "#P-SETTLED-TIAA-CONTRACT-NBR", 
            FieldType.STRING, 10);
        ltfp0710_Pnd_P_Settled_Cref_Contract_Nbr = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Settled_Cref_Contract_Nbr", "#P-SETTLED-CREF-CONTRACT-NBR", 
            FieldType.STRING, 10);
        ltfp0710_Pnd_P_Settlement_Ind = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Settlement_Ind", "#P-SETTLEMENT-IND", FieldType.STRING, 1);
        ltfp0710_Pnd_P_Tiaa_Accum_Settle_Amt = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Tiaa_Accum_Settle_Amt", "#P-TIAA-ACCUM-SETTLE-AMT", 
            FieldType.NUMERIC, 11, 2);
        ltfp0710_Pnd_P_Tiaa_Accum_Settle_Typ = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Tiaa_Accum_Settle_Typ", "#P-TIAA-ACCUM-SETTLE-TYP", 
            FieldType.STRING, 1);
        ltfp0710_Pnd_P_Rea_Accum_Settle_Amt = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Rea_Accum_Settle_Amt", "#P-REA-ACCUM-SETTLE-AMT", FieldType.NUMERIC, 
            11, 2);
        ltfp0710_Pnd_P_Rea_Accum_Settle_Typ = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Rea_Accum_Settle_Typ", "#P-REA-ACCUM-SETTLE-TYP", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Cref_Accum_Settle_Amt = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cref_Accum_Settle_Amt", "#P-CREF-ACCUM-SETTLE-AMT", 
            FieldType.NUMERIC, 11, 2);
        ltfp0710_Pnd_P_Cref_Accum_Settle_Typ = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cref_Accum_Settle_Typ", "#P-CREF-ACCUM-SETTLE-TYP", 
            FieldType.STRING, 1);
        ltfp0710_Pnd_P_Cref_Annty_Payment = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cref_Annty_Payment", "#P-CREF-ANNTY-PAYMENT", FieldType.NUMERIC, 
            11, 2);
        ltfp0710_Pnd_P_Cref_Mnthly_Nbr_Units = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cref_Mnthly_Nbr_Units", "#P-CREF-MNTHLY-NBR-UNITS", 
            FieldType.NUMERIC, 7, 4);
        ltfp0710_Pnd_P_Rea_Mnthly_Nbr_Units = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Rea_Mnthly_Nbr_Units", "#P-REA-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 
            7, 4);
        ltfp0710_Pnd_P_Nra_Flag = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Nra_Flag", "#P-NRA-FLAG", FieldType.STRING, 1);
        ltfp0710_Pnd_P_Fed_Election_Option = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Fed_Election_Option", "#P-FED-ELECTION-OPTION", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Fed_Tax_Amt_Tiaa_Rea = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Fed_Tax_Amt_Tiaa_Rea", "#P-FED-TAX-AMT-TIAA-REA", FieldType.NUMERIC, 
            11, 2);
        ltfp0710_Pnd_P_Fed_Tax_Amt_Ind_Tiaa_Rea = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Fed_Tax_Amt_Ind_Tiaa_Rea", "#P-FED-TAX-AMT-IND-TIAA-REA", 
            FieldType.STRING, 1);
        ltfp0710_Pnd_P_Fed_Tax_Amt_Cref = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Fed_Tax_Amt_Cref", "#P-FED-TAX-AMT-CREF", FieldType.NUMERIC, 
            11, 2);
        ltfp0710_Pnd_P_Fed_Tax_Amt_Ind_Cref = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Fed_Tax_Amt_Ind_Cref", "#P-FED-TAX-AMT-IND-CREF", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Sta_Election_Option = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Sta_Election_Option", "#P-STA-ELECTION-OPTION", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Sta_Tax_Amt_Tiaa_Rea = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Sta_Tax_Amt_Tiaa_Rea", "#P-STA-TAX-AMT-TIAA-REA", FieldType.NUMERIC, 
            11, 2);
        ltfp0710_Pnd_P_Sta_Tax_Amt_Ind_Tiaa_Rea = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Sta_Tax_Amt_Ind_Tiaa_Rea", "#P-STA-TAX-AMT-IND-TIAA-REA", 
            FieldType.STRING, 1);
        ltfp0710_Pnd_P_Sta_Tax_Amt_Cref = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Sta_Tax_Amt_Cref", "#P-STA-TAX-AMT-CREF", FieldType.NUMERIC, 
            11, 2);
        ltfp0710_Pnd_P_Sta_Tax_Amt_Ind_Cref = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Sta_Tax_Amt_Ind_Cref", "#P-STA-TAX-AMT-IND-CREF", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Rqst_Id_Key = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Rqst_Id_Key", "#P-RQST-ID-KEY", FieldType.STRING, 24);
        ltfp0710_Pnd_P_Opn_Clsd_Ind = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Opn_Clsd_Ind", "#P-OPN-CLSD-IND", FieldType.STRING, 1);
        ltfp0710_Pnd_P_Status_Cd = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Status_Cd", "#P-STATUS-CD", FieldType.STRING, 1);
        ltfp0710_Pnd_P_Cntrct_Type = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cntrct_Type", "#P-CNTRCT-TYPE", FieldType.STRING, 1);
        ltfp0710_Pnd_P_Cntrct_Apprvl_Ind = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cntrct_Apprvl_Ind", "#P-CNTRCT-APPRVL-IND", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Rqst_Id = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Rqst_Id", "#P-RQST-ID", FieldType.STRING, 3);
        ltfp0710_Pnd_P_Cntrct_Print_Dte = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cntrct_Print_Dte", "#P-CNTRCT-PRINT-DTE", FieldType.NUMERIC, 
            8);
        ltfp0710_Pnd_P_Extract_Date = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Extract_Date", "#P-EXTRACT-DATE", FieldType.NUMERIC, 8);
        ltfp0710_Pnd_P_Appl_Rcvd_Dte = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Appl_Rcvd_Dte", "#P-APPL-RCVD-DTE", FieldType.NUMERIC, 8);
        ltfp0710_Pnd_P_Appl_Rcvd_User_Id = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Appl_Rcvd_User_Id", "#P-APPL-RCVD-USER-ID", FieldType.STRING, 
            8);
        ltfp0710_Pnd_P_Appl_Entry_User_Id = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Appl_Entry_User_Id", "#P-APPL-ENTRY-USER-ID", FieldType.STRING, 
            8);
        ltfp0710_Pnd_P_Annty_Option = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Annty_Option", "#P-ANNTY-OPTION", FieldType.STRING, 2);
        ltfp0710_Pnd_P_Grnted_Int_Rate = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Grnted_Int_Rate", "#P-GRNTED-INT-RATE", FieldType.NUMERIC, 
            7, 4);
        ltfp0710_Pnd_P_Mdo_Traditional_Amt = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Mdo_Traditional_Amt", "#P-MDO-TRADITIONAL-AMT", FieldType.NUMERIC, 
            11, 2);
        ltfp0710_Pnd_P_Mdo_Tiaa_Int_Pymnt_Amt = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Mdo_Tiaa_Int_Pymnt_Amt", "#P-MDO-TIAA-INT-PYMNT-AMT", 
            FieldType.NUMERIC, 11, 2);
        ltfp0710_Pnd_P_Mdo_Cref_Int_Pymnt_Amt = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Mdo_Cref_Int_Pymnt_Amt", "#P-MDO-CREF-INT-PYMNT-AMT", 
            FieldType.NUMERIC, 11, 2);
        ltfp0710_Pnd_P_Pi_Task_Id = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Pi_Task_Id", "#P-PI-TASK-ID", FieldType.STRING, 11);

        ltfp0710_Pnd_P_Ppymt_Dest_Name = ltfp0710__R_Field_4.newGroupInGroup("ltfp0710_Pnd_P_Ppymt_Dest_Name", "#P-PPYMT-DEST-NAME");
        ltfp0710_Pnd_P_Ppymt_Dest_Name_35 = ltfp0710_Pnd_P_Ppymt_Dest_Name.newFieldInGroup("ltfp0710_Pnd_P_Ppymt_Dest_Name_35", "#P-PPYMT-DEST-NAME-35", 
            FieldType.STRING, 35);
        ltfp0710_Pnd_P_Ppymt_Dest_Name_5 = ltfp0710_Pnd_P_Ppymt_Dest_Name.newFieldInGroup("ltfp0710_Pnd_P_Ppymt_Dest_Name_5", "#P-PPYMT-DEST-NAME-5", 
            FieldType.STRING, 5);
        ltfp0710_Pnd_P_Ppymt_Dest_Addr1 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Ppymt_Dest_Addr1", "#P-PPYMT-DEST-ADDR1", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Ppymt_Dest_Addr2 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Ppymt_Dest_Addr2", "#P-PPYMT-DEST-ADDR2", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Ppymt_Dest_Addr3 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Ppymt_Dest_Addr3", "#P-PPYMT-DEST-ADDR3", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Ppymt_Dest_City = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Ppymt_Dest_City", "#P-PPYMT-DEST-CITY", FieldType.STRING, 
            28);
        ltfp0710_Pnd_P_Ppymt_Dest_State = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Ppymt_Dest_State", "#P-PPYMT-DEST-STATE", FieldType.STRING, 
            3);
        ltfp0710_Pnd_P_Ppymt_Zip = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Ppymt_Zip", "#P-PPYMT-ZIP", FieldType.STRING, 9);
        ltfp0710_Pnd_P_Spymt_Dest_Name = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Spymt_Dest_Name", "#P-SPYMT-DEST-NAME", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Spymt_Dest_Addr1 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Spymt_Dest_Addr1", "#P-SPYMT-DEST-ADDR1", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Spymt_Dest_Addr2 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Spymt_Dest_Addr2", "#P-SPYMT-DEST-ADDR2", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Spymt_Dest_Addr3 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Spymt_Dest_Addr3", "#P-SPYMT-DEST-ADDR3", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Spymt_Dest_City = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Spymt_Dest_City", "#P-SPYMT-DEST-CITY", FieldType.STRING, 
            28);
        ltfp0710_Pnd_P_Spymt_Dest_State = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Spymt_Dest_State", "#P-SPYMT-DEST-STATE", FieldType.STRING, 
            3);
        ltfp0710_Pnd_P_Spymt_Zip = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Spymt_Zip", "#P-SPYMT-ZIP", FieldType.STRING, 9);
        ltfp0710_Pnd_P_Spymt_Dest_Acct_Nbr = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Spymt_Dest_Acct_Nbr", "#P-SPYMT-DEST-ACCT-NBR", FieldType.STRING, 
            21);
        ltfp0710_Pnd_P_Spymt_Dest_Trnst_Cde = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Spymt_Dest_Trnst_Cde", "#P-SPYMT-DEST-TRNST-CDE", FieldType.STRING, 
            9);
        ltfp0710_Pnd_P_Spymt_Dest_Acct_Typ = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Spymt_Dest_Acct_Typ", "#P-SPYMT-DEST-ACCT-TYP", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Cpymt_Dest_Name = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Dest_Name", "#P-CPYMT-DEST-NAME", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Cpymt_Dest_Addr1 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Dest_Addr1", "#P-CPYMT-DEST-ADDR1", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Cpymt_Dest_Addr2 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Dest_Addr2", "#P-CPYMT-DEST-ADDR2", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Cpymt_Dest_Addr3 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Dest_Addr3", "#P-CPYMT-DEST-ADDR3", FieldType.STRING, 
            40);
        ltfp0710_Pnd_P_Cpymt_Dest_City = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Dest_City", "#P-CPYMT-DEST-CITY", FieldType.STRING, 
            28);
        ltfp0710_Pnd_P_Cpymt_Dest_State = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Dest_State", "#P-CPYMT-DEST-STATE", FieldType.STRING, 
            3);
        ltfp0710_Pnd_P_Cpymt_Zip = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Zip", "#P-CPYMT-ZIP", FieldType.STRING, 9);
        ltfp0710_Pnd_P_Cpymt_Dest_Acct_Nbr = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Dest_Acct_Nbr", "#P-CPYMT-DEST-ACCT-NBR", FieldType.STRING, 
            21);
        ltfp0710_Pnd_P_Cpymt_Dest_Trnst_Cde = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Dest_Trnst_Cde", "#P-CPYMT-DEST-TRNST-CDE", FieldType.STRING, 
            9);
        ltfp0710_Pnd_P_Cpymt_Dest_Acct_Typ = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Dest_Acct_Typ", "#P-CPYMT-DEST-ACCT-TYP", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Cpymt_Payee_Amt = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Payee_Amt", "#P-CPYMT-PAYEE-AMT", FieldType.NUMERIC, 
            11, 2);
        ltfp0710_Pnd_P_Cpymt_Payee_Type = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cpymt_Payee_Type", "#P-CPYMT-PAYEE-TYPE", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Two_Payments_In_One_Year = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Two_Payments_In_One_Year", "#P-TWO-PAYMENTS-IN-ONE-YEAR", 
            FieldType.STRING, 1);
        ltfp0710_Pnd_P_Fed_Exemption = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Fed_Exemption", "#P-FED-EXEMPTION", FieldType.STRING, 2);
        ltfp0710_Pnd_P_St_Exemption = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_St_Exemption", "#P-ST-EXEMPTION", FieldType.STRING, 2);
        ltfp0710_Pnd_P_Mailing_Country_Code = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Mailing_Country_Code", "#P-MAILING-COUNTRY-CODE", FieldType.STRING, 
            3);
        ltfp0710_Pnd_P_Cod_Id = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Cod_Id", "#P-COD-ID", FieldType.STRING, 15);
        ltfp0710_Pnd_P_Minimum_Req_Amt = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Minimum_Req_Amt", "#P-MINIMUM-REQ-AMT", FieldType.STRING, 
            11);

        ltfp0710__R_Field_10 = ltfp0710__R_Field_4.newGroupInGroup("ltfp0710__R_Field_10", "REDEFINE", ltfp0710_Pnd_P_Minimum_Req_Amt);
        ltfp0710_Pnd_P_Minimum_Req_Amt_N = ltfp0710__R_Field_10.newFieldInGroup("ltfp0710_Pnd_P_Minimum_Req_Amt_N", "#P-MINIMUM-REQ-AMT-N", FieldType.NUMERIC, 
            11, 2);
        ltfp0710_Pnd_P_Payee_Cde = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Payee_Cde", "#P-PAYEE-CDE", FieldType.STRING, 1);
        ltfp0710_Pnd_P_Has_Roth_Money_Ind = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Has_Roth_Money_Ind", "#P-HAS-ROTH-MONEY-IND", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Original_Issue = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Original_Issue", "#P-ORIGINAL-ISSUE", FieldType.STRING, 2);
        ltfp0710_Pnd_P_Fed_Marital_Status = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Fed_Marital_Status", "#P-FED-MARITAL-STATUS", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_St_Marital_Status = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_St_Marital_Status", "#P-ST-MARITAL-STATUS", FieldType.STRING, 
            1);
        ltfp0710_Pnd_P_Org_Prt_Date_Of_Death = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Org_Prt_Date_Of_Death", "#P-ORG-PRT-DATE-OF-DEATH", 
            FieldType.STRING, 8);

        ltfp0710__R_Field_11 = ltfp0710__R_Field_4.newGroupInGroup("ltfp0710__R_Field_11", "REDEFINE", ltfp0710_Pnd_P_Org_Prt_Date_Of_Death);
        ltfp0710_Pnd_P_Org_Prt_Date_Of_Death_N = ltfp0710__R_Field_11.newFieldInGroup("ltfp0710_Pnd_P_Org_Prt_Date_Of_Death_N", "#P-ORG-PRT-DATE-OF-DEATH-N", 
            FieldType.NUMERIC, 8);
        ltfp0710_Pnd_P_Org_Prt_Date_Of_Birth = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Org_Prt_Date_Of_Birth", "#P-ORG-PRT-DATE-OF-BIRTH", 
            FieldType.STRING, 8);

        ltfp0710__R_Field_12 = ltfp0710__R_Field_4.newGroupInGroup("ltfp0710__R_Field_12", "REDEFINE", ltfp0710_Pnd_P_Org_Prt_Date_Of_Birth);
        ltfp0710_Pnd_P_Org_Prt_Date_Of_Birth_N = ltfp0710__R_Field_12.newFieldInGroup("ltfp0710_Pnd_P_Org_Prt_Date_Of_Birth_N", "#P-ORG-PRT-DATE-OF-BIRTH-N", 
            FieldType.NUMERIC, 8);
        ltfp0710_Pnd_P_Orgnl_Prtcpnt_Frst_Nme = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Orgnl_Prtcpnt_Frst_Nme", "#P-ORGNL-PRTCPNT-FRST-NME", 
            FieldType.STRING, 40);
        ltfp0710_Pnd_P_Orgnl_Prtcpnt_Lst_Nme = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Orgnl_Prtcpnt_Lst_Nme", "#P-ORGNL-PRTCPNT-LST-NME", 
            FieldType.STRING, 40);
        ltfp0710_Pnd_P_Invest_Process_Dt = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Invest_Process_Dt", "#P-INVEST-PROCESS-DT", FieldType.NUMERIC, 
            8);
        ltfp0710_Pnd_P_Invest_Amt_Total = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Invest_Amt_Total", "#P-INVEST-AMT-TOTAL", FieldType.NUMERIC, 
            13, 2);

        ltfp0710_Pnd_P_Invest_From_Info = ltfp0710__R_Field_4.newGroupArrayInGroup("ltfp0710_Pnd_P_Invest_From_Info", "#P-INVEST-FROM-INFO", new DbsArrayController(1, 
            3));
        ltfp0710_Pnd_P_Invest_Num = ltfp0710_Pnd_P_Invest_From_Info.newFieldInGroup("ltfp0710_Pnd_P_Invest_Num", "#P-INVEST-NUM", FieldType.NUMERIC, 2);
        ltfp0710_Pnd_P_Invest_Orig_Amt = ltfp0710_Pnd_P_Invest_From_Info.newFieldInGroup("ltfp0710_Pnd_P_Invest_Orig_Amt", "#P-INVEST-ORIG-AMT", FieldType.NUMERIC, 
            13, 2);
        ltfp0710_Pnd_P_Invest_Tiaa_Num_1 = ltfp0710_Pnd_P_Invest_From_Info.newFieldInGroup("ltfp0710_Pnd_P_Invest_Tiaa_Num_1", "#P-INVEST-TIAA-NUM-1", 
            FieldType.STRING, 10);
        ltfp0710_Pnd_P_Invest_Cref_Num_1 = ltfp0710_Pnd_P_Invest_From_Info.newFieldInGroup("ltfp0710_Pnd_P_Invest_Cref_Num_1", "#P-INVEST-CREF-NUM-1", 
            FieldType.STRING, 10);
        ltfp0710_Pnd_P_Invest_Plan_Name = ltfp0710_Pnd_P_Invest_From_Info.newFieldInGroup("ltfp0710_Pnd_P_Invest_Plan_Name", "#P-INVEST-PLAN-NAME", FieldType.STRING, 
            65);
        ltfp0710_Pnd_P_Invest_Data = ltfp0710__R_Field_4.newFieldArrayInGroup("ltfp0710_Pnd_P_Invest_Data", "#P-INVEST-DATA", FieldType.STRING, 1, new 
            DbsArrayController(1, 5300));

        ltfp0710__R_Field_13 = ltfp0710__R_Field_4.newGroupInGroup("ltfp0710__R_Field_13", "REDEFINE", ltfp0710_Pnd_P_Invest_Data);

        ltfp0710_Pnd_P_Invest_Info = ltfp0710__R_Field_13.newGroupArrayInGroup("ltfp0710_Pnd_P_Invest_Info", "#P-INVEST-INFO", new DbsArrayController(1, 
            100));
        ltfp0710_Pnd_P_Invest_Ticker = ltfp0710_Pnd_P_Invest_Info.newFieldInGroup("ltfp0710_Pnd_P_Invest_Ticker", "#P-INVEST-TICKER", FieldType.STRING, 
            10);
        ltfp0710_Pnd_P_Invest_Amt = ltfp0710_Pnd_P_Invest_Info.newFieldInGroup("ltfp0710_Pnd_P_Invest_Amt", "#P-INVEST-AMT", FieldType.NUMERIC, 13, 2);
        ltfp0710_Pnd_P_Invest_Unit_Price = ltfp0710_Pnd_P_Invest_Info.newFieldInGroup("ltfp0710_Pnd_P_Invest_Unit_Price", "#P-INVEST-UNIT-PRICE", FieldType.NUMERIC, 
            13, 4);
        ltfp0710_Pnd_P_Invest_Units = ltfp0710_Pnd_P_Invest_Info.newFieldInGroup("ltfp0710_Pnd_P_Invest_Units", "#P-INVEST-UNITS", FieldType.NUMERIC, 
            15, 4);
        ltfp0710_Pnd_P_Res_Addr1 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Res_Addr1", "#P-RES-ADDR1", FieldType.STRING, 40);
        ltfp0710_Pnd_P_Res_Addr2 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Res_Addr2", "#P-RES-ADDR2", FieldType.STRING, 40);
        ltfp0710_Pnd_P_Res_Addr3 = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Res_Addr3", "#P-RES-ADDR3", FieldType.STRING, 40);
        ltfp0710_Pnd_P_Res_City = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Res_City", "#P-RES-CITY", FieldType.STRING, 28);
        ltfp0710_Pnd_P_Res_State = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Res_State", "#P-RES-STATE", FieldType.STRING, 3);
        ltfp0710_Pnd_P_Res_Zip = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Res_Zip", "#P-RES-ZIP", FieldType.STRING, 9);
        ltfp0710_Pnd_P_Res_Country_Code = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_P_Res_Country_Code", "#P-RES-COUNTRY-CODE", FieldType.STRING, 
            5);
        ltfp0710_Pnd_Acceptance_Ind = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_Acceptance_Ind", "#ACCEPTANCE-IND", FieldType.STRING, 1);
        ltfp0710_Pnd_Relation_To_Decedent = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_Relation_To_Decedent", "#RELATION-TO-DECEDENT", FieldType.STRING, 
            1);
        ltfp0710_Pnd_Ssn_Tin_Ind = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_Ssn_Tin_Ind", "#SSN-TIN-IND", FieldType.STRING, 1);
        ltfp0710_Pnd_Decedent_Contract = ltfp0710__R_Field_4.newFieldInGroup("ltfp0710_Pnd_Decedent_Contract", "#DECEDENT-CONTRACT", FieldType.STRING, 
            10);

        ltfp0710__R_Field_14 = localVariables.newGroupInRecord("ltfp0710__R_Field_14", "REDEFINE", ltfp0710);
        ltfp0710_Pnd_B_Ltfp0710 = ltfp0710__R_Field_14.newFieldArrayInGroup("ltfp0710_Pnd_B_Ltfp0710", "#B-LTFP0710", FieldType.STRING, 1, new DbsArrayController(1, 
            3316));

        ltfp0710__R_Field_15 = ltfp0710__R_Field_14.newGroupInGroup("ltfp0710__R_Field_15", "REDEFINE", ltfp0710_Pnd_B_Ltfp0710);
        ltfp0710_Pnd_B_Mdo_Letter_Type = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Mdo_Letter_Type", "#B-MDO-LETTER-TYPE", FieldType.STRING, 
            4);
        ltfp0710_Pnd_B_Mdo_Letter_Add_Chg = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Mdo_Letter_Add_Chg", "#B-MDO-LETTER-ADD-CHG", FieldType.STRING, 
            1);

        ltfp0710_Pnd_B_Omni_Rec_Key = ltfp0710__R_Field_15.newGroupInGroup("ltfp0710_Pnd_B_Omni_Rec_Key", "#B-OMNI-REC-KEY");
        ltfp0710_Pnd_B_Omni_Plan_Id = ltfp0710_Pnd_B_Omni_Rec_Key.newFieldInGroup("ltfp0710_Pnd_B_Omni_Plan_Id", "#B-OMNI-PLAN-ID", FieldType.STRING, 
            6);
        ltfp0710_Pnd_B_Omni_Sub_Plan_Id = ltfp0710_Pnd_B_Omni_Rec_Key.newFieldInGroup("ltfp0710_Pnd_B_Omni_Sub_Plan_Id", "#B-OMNI-SUB-PLAN-ID", FieldType.STRING, 
            17);
        ltfp0710_Pnd_B_Pin = ltfp0710_Pnd_B_Omni_Rec_Key.newFieldInGroup("ltfp0710_Pnd_B_Pin", "#B-PIN", FieldType.STRING, 13);

        ltfp0710__R_Field_16 = ltfp0710_Pnd_B_Omni_Rec_Key.newGroupInGroup("ltfp0710__R_Field_16", "REDEFINE", ltfp0710_Pnd_B_Pin);
        ltfp0710_Pnd_Pnd_B_Pin_N = ltfp0710__R_Field_16.newFieldInGroup("ltfp0710_Pnd_Pnd_B_Pin_N", "##B-PIN-N", FieldType.NUMERIC, 12);
        ltfp0710_Pnd_B_Pin_Filler = ltfp0710__R_Field_16.newFieldInGroup("ltfp0710_Pnd_B_Pin_Filler", "#B-PIN-FILLER", FieldType.STRING, 1);
        ltfp0710_Pnd_B_Mdo_Tiaa_Contract = ltfp0710_Pnd_B_Omni_Rec_Key.newFieldInGroup("ltfp0710_Pnd_B_Mdo_Tiaa_Contract", "#B-MDO-TIAA-CONTRACT", FieldType.STRING, 
            10);
        ltfp0710_Pnd_B_Mdo_Cref_Cert = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Mdo_Cref_Cert", "#B-MDO-CREF-CERT", FieldType.STRING, 10);
        ltfp0710_Pnd_B_Omni_Plan_Name = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Omni_Plan_Name", "#B-OMNI-PLAN-NAME", FieldType.STRING, 50);
        ltfp0710_Pnd_B_Participant_Name = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Participant_Name", "#B-PARTICIPANT-NAME", FieldType.STRING, 
            40);
        ltfp0710_Pnd_B_Bene_Category = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Bene_Category", "#B-BENE-CATEGORY", FieldType.STRING, 1);
        ltfp0710_Pnd_B_Bene_Name = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Bene_Name", "#B-BENE-NAME", FieldType.STRING, 70);
        ltfp0710_Pnd_B_Bene_Birth_Date = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Bene_Birth_Date", "#B-BENE-BIRTH-DATE", FieldType.STRING, 
            8);

        ltfp0710__R_Field_17 = ltfp0710__R_Field_15.newGroupInGroup("ltfp0710__R_Field_17", "REDEFINE", ltfp0710_Pnd_B_Bene_Birth_Date);
        ltfp0710_Pnd_B_Bene_Birth_Date_N = ltfp0710__R_Field_17.newFieldInGroup("ltfp0710_Pnd_B_Bene_Birth_Date_N", "#B-BENE-BIRTH-DATE-N", FieldType.NUMERIC, 
            8);
        ltfp0710_Pnd_B_Bene_Ssn = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Bene_Ssn", "#B-BENE-SSN", FieldType.NUMERIC, 9);
        ltfp0710_Pnd_B_Bene_Relationship = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Bene_Relationship", "#B-BENE-RELATIONSHIP", FieldType.STRING, 
            1);
        ltfp0710_Pnd_B_Bene_Allocation_Pct = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Bene_Allocation_Pct", "#B-BENE-ALLOCATION-PCT", FieldType.NUMERIC, 
            5, 2);
        ltfp0710_Pnd_B_Prim_Child_Prov = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Prim_Child_Prov", "#B-PRIM-CHILD-PROV", FieldType.STRING, 
            1);
        ltfp0710_Pnd_B_Cont_Child_Prov = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Cont_Child_Prov", "#B-CONT-CHILD-PROV", FieldType.STRING, 
            1);
        ltfp0710_Pnd_B_Cal_Bene_Meth = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Cal_Bene_Meth", "#B-CAL-BENE-METH", FieldType.STRING, 1);
        ltfp0710_Pnd_B_Filler1 = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Filler1", "#B-FILLER1", FieldType.STRING, 227);
        ltfp0710_Pnd_B_Filler2 = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Filler2", "#B-FILLER2", FieldType.STRING, 227);
        ltfp0710_Pnd_B_Filler3 = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Filler3", "#B-FILLER3", FieldType.STRING, 227);
        ltfp0710_Pnd_B_Filler4 = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Filler4", "#B-FILLER4", FieldType.STRING, 227);
        ltfp0710_Pnd_B_Filler5 = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Filler5", "#B-FILLER5", FieldType.STRING, 227);
        ltfp0710_Pnd_B_Filler6 = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Filler6", "#B-FILLER6", FieldType.STRING, 227);
        ltfp0710_Pnd_B_Filler7 = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Filler7", "#B-FILLER7", FieldType.STRING, 227);
        ltfp0710_Pnd_B_Filler8 = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Filler8", "#B-FILLER8", FieldType.STRING, 227);
        ltfp0710_Pnd_B_Filler9 = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Filler9", "#B-FILLER9", FieldType.STRING, 227);
        ltfp0710_Pnd_B_Filler10 = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Filler10", "#B-FILLER10", FieldType.STRING, 227);
        ltfp0710_Pnd_B_Filler14 = ltfp0710__R_Field_15.newFieldInGroup("ltfp0710_Pnd_B_Filler14", "#B-FILLER14", FieldType.STRING, 12);
        pnd_P_Pin_A_12 = localVariables.newFieldInRecord("pnd_P_Pin_A_12", "#P-PIN-A-12", FieldType.STRING, 12);

        pnd_P_Pin_A_12__R_Field_18 = localVariables.newGroupInRecord("pnd_P_Pin_A_12__R_Field_18", "REDEFINE", pnd_P_Pin_A_12);
        pnd_P_Pin_A_12_Pnd_P_Pin_N_12 = pnd_P_Pin_A_12__R_Field_18.newFieldInGroup("pnd_P_Pin_A_12_Pnd_P_Pin_N_12", "#P-PIN-N-12", FieldType.NUMERIC, 
            12);
        pnd_Start = localVariables.newFieldInRecord("pnd_Start", "#START", FieldType.NUMERIC, 9);
        pnd_Lcl_Contract = localVariables.newFieldInRecord("pnd_Lcl_Contract", "#LCL-CONTRACT", FieldType.STRING, 10);
        pnd_Acct_Cde = localVariables.newFieldInRecord("pnd_Acct_Cde", "#ACCT-CDE", FieldType.NUMERIC, 9);
        pnd_Rltn_Cde = localVariables.newFieldInRecord("pnd_Rltn_Cde", "#RLTN-CDE", FieldType.STRING, 10);
        pnd_R_Cde = localVariables.newFieldInRecord("pnd_R_Cde", "#R-CDE", FieldType.STRING, 2);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Lcl_Bene_Calc = localVariables.newFieldInRecord("pnd_Lcl_Bene_Calc", "#LCL-BENE-CALC", FieldType.STRING, 1);
        pnd_Err_Message = localVariables.newFieldInRecord("pnd_Err_Message", "#ERR-MESSAGE", FieldType.STRING, 72);
        pnd_Total_Add = localVariables.newFieldInRecord("pnd_Total_Add", "#TOTAL-ADD", FieldType.NUMERIC, 9);
        pnd_Total_Dup = localVariables.newFieldInRecord("pnd_Total_Dup", "#TOTAL-DUP", FieldType.NUMERIC, 9);
        pnd_Total_Err = localVariables.newFieldInRecord("pnd_Total_Err", "#TOTAL-ERR", FieldType.NUMERIC, 9);
        pnd_Fund_Count = localVariables.newFieldInRecord("pnd_Fund_Count", "#FUND-COUNT", FieldType.NUMERIC, 3);
        pnd_Bene_Count = localVariables.newFieldInRecord("pnd_Bene_Count", "#BENE-COUNT", FieldType.NUMERIC, 3);
        pnd_Fund_Idx = localVariables.newFieldInRecord("pnd_Fund_Idx", "#FUND-IDX", FieldType.NUMERIC, 3);
        pnd_Bene_Idx = localVariables.newFieldInRecord("pnd_Bene_Idx", "#BENE-IDX", FieldType.NUMERIC, 3);
        pnd_Cis_Sg_Text_Udf_2 = localVariables.newFieldInRecord("pnd_Cis_Sg_Text_Udf_2", "#CIS-SG-TEXT-UDF-2", FieldType.STRING, 10);

        pnd_Cis_Sg_Text_Udf_2__R_Field_19 = localVariables.newGroupInRecord("pnd_Cis_Sg_Text_Udf_2__R_Field_19", "REDEFINE", pnd_Cis_Sg_Text_Udf_2);
        pnd_Cis_Sg_Text_Udf_2_Pnd_Sg_Udf2_F1_Roth_Ind = pnd_Cis_Sg_Text_Udf_2__R_Field_19.newFieldInGroup("pnd_Cis_Sg_Text_Udf_2_Pnd_Sg_Udf2_F1_Roth_Ind", 
            "#SG-UDF2-F1-ROTH-IND", FieldType.STRING, 1);
        pnd_Cis_Sg_Text_Udf_2_Pnd_Sg_Udf2_F2_Payee_Cde = pnd_Cis_Sg_Text_Udf_2__R_Field_19.newFieldInGroup("pnd_Cis_Sg_Text_Udf_2_Pnd_Sg_Udf2_F2_Payee_Cde", 
            "#SG-UDF2-F2-PAYEE-CDE", FieldType.STRING, 1);
        pnd_Cnt1 = localVariables.newFieldInRecord("pnd_Cnt1", "#CNT1", FieldType.INTEGER, 1);
        pnd_Cnt2 = localVariables.newFieldInRecord("pnd_Cnt2", "#CNT2", FieldType.INTEGER, 1);
        pnd_Cnt3 = localVariables.newFieldInRecord("pnd_Cnt3", "#CNT3", FieldType.INTEGER, 1);
        pnd_Cnt4 = localVariables.newFieldInRecord("pnd_Cnt4", "#CNT4", FieldType.INTEGER, 1);
        pnd_Cntx = localVariables.newFieldInRecord("pnd_Cntx", "#CNTX", FieldType.INTEGER, 1);

        pnd_Hold_Address = localVariables.newGroupInRecord("pnd_Hold_Address", "#HOLD-ADDRESS");
        pnd_Hold_Address_Pnd_Hdest_Addr1 = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hdest_Addr1", "#HDEST-ADDR1", FieldType.STRING, 40);
        pnd_Hold_Address_Pnd_Hdest_Addr2 = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hdest_Addr2", "#HDEST-ADDR2", FieldType.STRING, 40);
        pnd_Hold_Address_Pnd_Hdest_Addr3 = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hdest_Addr3", "#HDEST-ADDR3", FieldType.STRING, 40);
        pnd_Hold_Address_Pnd_Hdest_City = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hdest_City", "#HDEST-CITY", FieldType.STRING, 28);
        pnd_Hold_Address_Pnd_Hdest_State = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hdest_State", "#HDEST-STATE", FieldType.STRING, 3);
        pnd_Hold_Address_Pnd_Hzip = pnd_Hold_Address.newFieldInGroup("pnd_Hold_Address_Pnd_Hzip", "#HZIP", FieldType.STRING, 9);
        pnd_Foreign_Or_Canada = localVariables.newFieldArrayInRecord("pnd_Foreign_Or_Canada", "#FOREIGN-OR-CANADA", FieldType.STRING, 5, new DbsArrayController(1, 
            2));
        pnd_Addr_Indx = localVariables.newFieldInRecord("pnd_Addr_Indx", "#ADDR-INDX", FieldType.NUMERIC, 1);
        pnd_Debug = localVariables.newFieldInRecord("pnd_Debug", "#DEBUG", FieldType.BOOLEAN, 1);
        pnd_Hold_Res_State = localVariables.newFieldInRecord("pnd_Hold_Res_State", "#HOLD-RES-STATE", FieldType.STRING, 3);
        pnd_Save_Omni_Plan_Id = localVariables.newFieldInRecord("pnd_Save_Omni_Plan_Id", "#SAVE-OMNI-PLAN-ID", FieldType.STRING, 6);
        pnd_Save_Omni_Sub_Plan_Id = localVariables.newFieldInRecord("pnd_Save_Omni_Sub_Plan_Id", "#SAVE-OMNI-SUB-PLAN-ID", FieldType.STRING, 17);

        pnd_Save_Omni_Sub_Plan_Id__R_Field_20 = localVariables.newGroupInRecord("pnd_Save_Omni_Sub_Plan_Id__R_Field_20", "REDEFINE", pnd_Save_Omni_Sub_Plan_Id);
        pnd_Save_Omni_Sub_Plan_Id_Pnd_Save_Omni_Ssn = pnd_Save_Omni_Sub_Plan_Id__R_Field_20.newFieldInGroup("pnd_Save_Omni_Sub_Plan_Id_Pnd_Save_Omni_Ssn", 
            "#SAVE-OMNI-SSN", FieldType.NUMERIC, 9);
        pnd_Save_Omni_Sub_Plan_Id_Pnd_Save_Omni_Sub_Plan = pnd_Save_Omni_Sub_Plan_Id__R_Field_20.newFieldInGroup("pnd_Save_Omni_Sub_Plan_Id_Pnd_Save_Omni_Sub_Plan", 
            "#SAVE-OMNI-SUB-PLAN", FieldType.STRING, 8);
        pnd_Save_Mdo_Tiaa_Contract = localVariables.newFieldInRecord("pnd_Save_Mdo_Tiaa_Contract", "#SAVE-MDO-TIAA-CONTRACT", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCispartv.initializeValues();
        ldaCisl2020.initializeValues();
        ldaAppl170.initializeValues();

        localVariables.reset();
        pnd_City_Done.setInitialValue(false);
        pnd_Y.setInitialValue(0);
        pnd_D.setInitialValue(1);
        pnd_C.setInitialValue(1);
        pnd_Err_Found.setInitialValue(false);
        pnd_Err_Fatal.setInitialValue(false);
        pnd_Bypass_Record.setInitialValue(false);
        pnd_New_Set_Of_Text.setInitialValue(true);
        pnd_First_Omni_Record.setInitialValue(true);
        pnd_Bene_Info_3_Already_Saved.setInitialValue(false);
        pnd_Foreign_Or_Canada.getValue(1).setInitialValue("FORGN");
        pnd_Foreign_Or_Canada.getValue(2).setInitialValue("CANAD");
        pnd_Debug.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Sgdb4001() throws Exception
    {
        super("Sgdb4001");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("SGDB4001", onError);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        setupReports();
        //*  ========================================================
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 132 ZP = OFF SG = OFF;//Natural: FORMAT ( 2 ) PS = 60 LS = 132 ZP = OFF SG = OFF;//Natural: FORMAT ( 3 ) PS = 60 LS = 132 ZP = OFF SG = OFF;//Natural: FORMAT ( 4 ) PS = 60 LS = 132 ZP = OFF SG = OFF
        //* CREA
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 #IN-TRANSLTE-FILE
        while (condition(getWorkFiles().read(2, pnd_In_Translte_File)))
        {
            //* CREA
            //* CREA
            //* CREA
            //* CREA
            //* CREA
            //* CREA
            //* CREA
            if (condition(!(pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq.greater(0))))                                                                                   //Natural: ACCEPT IF #IN-TRANSLTE-FUND-SEQ > 00
            {
                continue;
            }
            pnd_Fund_Count.nadd(1);                                                                                                                                       //Natural: ASSIGN #FUND-COUNT := #FUND-COUNT + 1
            pnd_Translte_File_Pnd_Translte_Ticker.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Ticker);                                         //Natural: ASSIGN #TRANSLTE-TICKER ( #FUND-COUNT ) := #IN-TRANSLTE-TICKER
            pnd_Translte_File_Pnd_Translte_Ticker_Name.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Ticker_Name);                               //Natural: ASSIGN #TRANSLTE-TICKER-NAME ( #FUND-COUNT ) := #IN-TRANSLTE-TICKER-NAME
            pnd_Translte_File_Pnd_Translte_Fund_Seq.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Fund_Seq);                                     //Natural: ASSIGN #TRANSLTE-FUND-SEQ ( #FUND-COUNT ) := #IN-TRANSLTE-FUND-SEQ
            pnd_Translte_File_Pnd_Translte_Pull_Code.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Pull_Code);                                   //Natural: ASSIGN #TRANSLTE-PULL-CODE ( #FUND-COUNT ) := #IN-TRANSLTE-PULL-CODE
            pnd_Translte_File_Pnd_Translte_Fund_Type.getValue(pnd_Fund_Count).setValue(pnd_In_Translte_File_Pnd_In_Translte_Fund_Type);                                   //Natural: ASSIGN #TRANSLTE-FUND-TYPE ( #FUND-COUNT ) := #IN-TRANSLTE-FUND-TYPE
            //* CREA
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Fund_Count);                                                                                                                        //Natural: WRITE '=' #FUND-COUNT
        if (Global.isEscape()) return;
        //* CREA
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 3 #IN-BENE-RELATIONSHIP
        while (condition(getWorkFiles().read(3, pnd_In_Bene_Relationship)))
        {
            //* CREA
            //* CREA
            //* CREA
            if (condition(!(pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Seq.greater(0))))                                                                           //Natural: ACCEPT IF #IN-BENE-RELATIONSHIP-SEQ > 00
            {
                continue;
            }
            pnd_Bene_Count.nadd(1);                                                                                                                                       //Natural: ASSIGN #BENE-COUNT := #BENE-COUNT + 1
            pnd_Bene_Relationship_Pnd_Bene_Relationship_Seq.getValue(pnd_Bene_Count).setValue(pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Seq);                     //Natural: ASSIGN #BENE-RELATIONSHIP-SEQ ( #BENE-COUNT ) := #IN-BENE-RELATIONSHIP-SEQ
            pnd_Bene_Relationship_Pnd_Bene_Relationship_Type.getValue(pnd_Bene_Count).setValue(pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Type);                   //Natural: ASSIGN #BENE-RELATIONSHIP-TYPE ( #BENE-COUNT ) := #IN-BENE-RELATIONSHIP-TYPE
            pnd_Bene_Relationship_Pnd_Bene_Relationship_Name.getValue(pnd_Bene_Count).setValue(pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Name);                   //Natural: ASSIGN #BENE-RELATIONSHIP-NAME ( #BENE-COUNT ) := #IN-BENE-RELATIONSHIP-NAME
            pnd_Bene_Relationship_Pnd_Bene_Relationship_Code.getValue(pnd_Bene_Count).setValue(pnd_In_Bene_Relationship_Pnd_In_Bene_Relationship_Code);                   //Natural: ASSIGN #BENE-RELATIONSHIP-CODE ( #BENE-COUNT ) := #IN-BENE-RELATIONSHIP-CODE
            //* CREA
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Bene_Count);                                                                                                                        //Natural: WRITE '=' #BENE-COUNT
        if (Global.isEscape()) return;
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT JUSTIFIED 001T *PROGRAM '-' *INIT-USER 045T 'MDO WELCOME PACKAGE ' 100T 'Page No :' 110T *PAGE-NUMBER ( 1 ) ( AD = OD CD = NE ZP = ON ) / 001T *DATU '-' *TIMX ( EM = HH:IIAP ) 045T '  OMNI Rejection Report' / 001T ' Plan     Sub Plan          Pin              Name           ' 062T '                      TIAA #    CREF #' / 001T '------ ----------------- -------------' 040T '-----------------------------------------' 082T '---------- ----------'
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 );//Natural: AT TOP OF PAGE ( 3 );//Natural: AT TOP OF PAGE ( 4 )
        READWORK03:                                                                                                                                                       //Natural: READ WORK 1 LTFP0710 ( * )
        while (condition(getWorkFiles().read(1, ltfp0710.getValue("*"))))
        {
            //*  BIP
            if (condition(!(ltfp0710_Pnd_P_Mdo_Letter_Type.equals("MDAP") || ltfp0710_Pnd_P_Mdo_Letter_Type.equals("MDAB") || ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPAP")  //Natural: ACCEPT IF #P-MDO-LETTER-TYPE = 'MDAP' OR = 'MDAB' OR = 'SPAP' OR = 'SPAB' OR = 'SPBP'
                || ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPAB") || ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPBP"))))
            {
                continue;
            }
            pnd_Start.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #START
            if (condition(pnd_Start.equals(1)))                                                                                                                           //Natural: IF #START = 1
            {
                pnd_Lcl_Contract.setValue(ltfp0710_Pnd_P_Mdo_Tiaa_Contract);                                                                                              //Natural: ASSIGN #LCL-CONTRACT := #P-MDO-TIAA-CONTRACT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Mdo_Letter_Add_Chg.equals(" ")))                                                                                                 //Natural: IF #P-MDO-LETTER-ADD-CHG EQ ' '
            {
                //*  CHG365909
                ltfp0710.getValue("*").reset();                                                                                                                           //Natural: RESET LTFP0710 ( * )
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  BIP
            if (condition(ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPAP")))                                                                                                 //Natural: IF #P-MDO-LETTER-TYPE = 'SPAP'
            {
                //*  BIP
                //*  BIP
                //*  BIP
                pnd_Save_Omni_Plan_Id.reset();                                                                                                                            //Natural: RESET #SAVE-OMNI-PLAN-ID #SAVE-OMNI-SUB-PLAN-ID #SAVE-MDO-TIAA-CONTRACT
                pnd_Save_Omni_Sub_Plan_Id.reset();
                pnd_Save_Mdo_Tiaa_Contract.reset();
                //*  BIP -IGNORE REC FOR
                if (condition(ltfp0710_Pnd_Decedent_Contract.greater(" ")))                                                                                               //Natural: IF #DECEDENT-CONTRACT GT ' '
                {
                    //*  CHG365909  /* BIP -SIP LETTER
                    //*  CHG365909
                    //*  BIP
                    //*  BIP
                    //*  BIP
                    getReports().write(0, "SKIPPING PARTICIPANT",ltfp0710_Pnd_P_Mdo_Tiaa_Contract);                                                                       //Natural: WRITE 'SKIPPING PARTICIPANT' #P-MDO-TIAA-CONTRACT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Save_Omni_Plan_Id.setValue(ltfp0710_Pnd_P_Omni_Plan_Id);                                                                                          //Natural: ASSIGN #SAVE-OMNI-PLAN-ID := #P-OMNI-PLAN-ID
                    pnd_Save_Omni_Sub_Plan_Id.setValue(ltfp0710_Pnd_P_Omni_Sub_Plan_Id);                                                                                  //Natural: ASSIGN #SAVE-OMNI-SUB-PLAN-ID := #P-OMNI-SUB-PLAN-ID
                    pnd_Save_Mdo_Tiaa_Contract.setValue(ltfp0710_Pnd_P_Mdo_Tiaa_Contract);                                                                                //Natural: ASSIGN #SAVE-MDO-TIAA-CONTRACT := #P-MDO-TIAA-CONTRACT
                    //*  CHG365909
                    ltfp0710.getValue("*").reset();                                                                                                                       //Natural: RESET LTFP0710 ( * )
                    //*  BIP
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  BIP
                }                                                                                                                                                         //Natural: END-IF
                //*  BIP
            }                                                                                                                                                             //Natural: END-IF
            //*  BIP
            if (condition(ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPAB")))                                                                                                 //Natural: IF #P-MDO-LETTER-TYPE = 'SPAB'
            {
                //*  BIP
                //*  BIP
                //*  BIP
                if (condition(pnd_Save_Omni_Plan_Id.equals(ltfp0710_Pnd_B_Omni_Plan_Id) && pnd_Save_Omni_Sub_Plan_Id.equals(ltfp0710_Pnd_B_Omni_Sub_Plan_Id)              //Natural: IF #SAVE-OMNI-PLAN-ID = #B-OMNI-PLAN-ID AND #SAVE-OMNI-SUB-PLAN-ID = #B-OMNI-SUB-PLAN-ID AND #SAVE-MDO-TIAA-CONTRACT = #B-MDO-TIAA-CONTRACT
                    && pnd_Save_Mdo_Tiaa_Contract.equals(ltfp0710_Pnd_B_Mdo_Tiaa_Contract)))
                {
                    //*  BIP
                    getReports().write(0, "SKIPPING BENE",ltfp0710_Pnd_B_Mdo_Tiaa_Contract);                                                                              //Natural: WRITE 'SKIPPING BENE' #B-MDO-TIAA-CONTRACT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  CHG365909
                    ltfp0710.getValue("*").reset();                                                                                                                       //Natural: RESET LTFP0710 ( * )
                    //*  BIP
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  BIP
                }                                                                                                                                                         //Natural: END-IF
                //*  BIP
            }                                                                                                                                                             //Natural: END-IF
            //*  BIP
            if (condition(ltfp0710_Pnd_P_Mdo_Letter_Type.equals("MDAP") || ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPAP") || ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPBP"))) //Natural: IF #P-MDO-LETTER-TYPE = 'MDAP' OR #P-MDO-LETTER-TYPE = 'SPAP' OR #P-MDO-LETTER-TYPE = 'SPBP'
            {
                //*  KC-02
                if (condition(pnd_Escape_Top.getBoolean()))                                                                                                               //Natural: IF #ESCAPE-TOP
                {
                    //*  KC-02
                    ldaCisl2020.getVw_cis_Bene_File_01().reset();                                                                                                         //Natural: RESET CIS-BENE-FILE-01
                    //*  C277892
                    pnd_Bene_Cnt_Prmry.reset();                                                                                                                           //Natural: RESET #BENE-CNT-PRMRY #BENE-CNT-CNTGNT #CITY-DONE
                    pnd_Bene_Cnt_Cntgnt.reset();
                    pnd_City_Done.reset();
                    //*  KC-02
                    pnd_Escape_Top.reset();                                                                                                                               //Natural: RESET #ESCAPE-TOP
                    //*  KC-02
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM STORE-BENE
                    sub_Store_Bene();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  C277892
                    pnd_Bene_Cnt_Prmry.reset();                                                                                                                           //Natural: RESET #BENE-CNT-PRMRY #BENE-CNT-CNTGNT #CITY-DONE
                    pnd_Bene_Cnt_Cntgnt.reset();
                    pnd_City_Done.reset();
                    pnd_Escape_Top.reset();                                                                                                                               //Natural: RESET #ESCAPE-TOP
                }                                                                                                                                                         //Natural: END-IF
                //*    WRITE '=' #P-MDO-CONTRACT-TYPE
                if (condition(ltfp0710_Pnd_P_Mdo_Contract_Type.equals("R")))                                                                                              //Natural: IF #P-MDO-CONTRACT-TYPE = 'R'
                {
                    if (condition(ltfp0710_Pnd_P_Pin_N_7.equals(getZero()) || ltfp0710_Pnd_P_Mdo_Tiaa_Contract.equals(" ") || ltfp0710_Pnd_P_Mdo_Cref_Cert.equals(" ")    //Natural: IF #P-PIN-N-7 EQ 0 OR #P-MDO-TIAA-CONTRACT EQ ' ' OR #P-MDO-CREF-CERT EQ ' ' OR #P-ANNTY-STRT-DTE EQ ' ' OR #P-FRST-ANNT-SSN EQ 0 OR #P-FRST-ANNT-FRST-NME EQ ' ' OR #P-FRST-ANNT-LST-NME EQ ' '
                        || ltfp0710_Pnd_P_Annty_Strt_Dte.equals(" ") || ltfp0710_Pnd_P_Frst_Annt_Ssn.equals(getZero()) || ltfp0710_Pnd_P_Frst_Annt_Frst_Nme.equals(" ") 
                        || ltfp0710_Pnd_P_Frst_Annt_Lst_Nme.equals(" ")))
                    {
                        pnd_Err_Message.setValue("OMNI KEY INFO MISSING");                                                                                                //Natural: ASSIGN #ERR-MESSAGE := "OMNI KEY INFO MISSING"
                                                                                                                                                                          //Natural: PERFORM GENERATE-ERROR-REPORT
                        sub_Generate_Error_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Escape_Top.setValue(true);                                                                                                                    //Natural: ASSIGN #ESCAPE-TOP := TRUE
                        //* *    PERFORM STORE-BENE
                        pnd_Total_Err.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-ERR
                        //*  CHG365909
                        ltfp0710.getValue("*").reset();                                                                                                                   //Natural: RESET LTFP0710 ( * )
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "=",ltfp0710_Pnd_P_Mdo_Contract_Type,"=",ltfp0710_Pnd_P_Mdo_Tiaa_Contract,"=",ltfp0710_Pnd_P_Mdo_Cref_Cert,"=",ltfp0710_Pnd_P_Annty_Strt_Dte, //Natural: WRITE '=' #P-MDO-CONTRACT-TYPE '=' #P-MDO-TIAA-CONTRACT '=' #P-MDO-CREF-CERT '=' #P-ANNTY-STRT-DTE '=' #P-FRST-ANNT-SSN '=' #P-FRST-ANNT-FRST-NME '=' #P-FRST-ANNT-LST-NME
                    "=",ltfp0710_Pnd_P_Frst_Annt_Ssn,"=",ltfp0710_Pnd_P_Frst_Annt_Frst_Nme,"=",ltfp0710_Pnd_P_Frst_Annt_Lst_Nme);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ltfp0710_Pnd_P_Mdo_Contract_Type.equals("S")))                                                                                              //Natural: IF #P-MDO-CONTRACT-TYPE = 'S'
                {
                    if (condition(ltfp0710_Pnd_P_Mdo_Tiaa_Contract.equals(" ") || ltfp0710_Pnd_P_Mdo_Cref_Cert.equals(" ") || ltfp0710_Pnd_P_Annty_Strt_Dte.equals(" ")   //Natural: IF #P-MDO-TIAA-CONTRACT EQ ' ' OR #P-MDO-CREF-CERT EQ ' ' OR #P-ANNTY-STRT-DTE EQ ' ' OR #P-FRST-ANNT-SSN EQ 0 OR #P-FRST-ANNT-LST-NME EQ ' '
                        || ltfp0710_Pnd_P_Frst_Annt_Ssn.equals(getZero()) || ltfp0710_Pnd_P_Frst_Annt_Lst_Nme.equals(" ")))
                    {
                        //*        OR  #P-FRST-ANNT-FRST-NME EQ ' '
                        pnd_Err_Message.setValue("OMNI KEY INFO MISS");                                                                                                   //Natural: ASSIGN #ERR-MESSAGE := "OMNI KEY INFO MISS"
                                                                                                                                                                          //Natural: PERFORM GENERATE-ERROR-REPORT
                        sub_Generate_Error_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Escape_Top.setValue(true);                                                                                                                    //Natural: ASSIGN #ESCAPE-TOP := TRUE
                        //* *    PERFORM STORE-BENE
                        pnd_Total_Err.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-ERR
                        //*  CHG365909
                        ltfp0710.getValue("*").reset();                                                                                                                   //Natural: RESET LTFP0710 ( * )
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* PRB44117 RESET
                pnd_Bene_Cnt_Prmry.reset();                                                                                                                               //Natural: RESET #BENE-CNT-PRMRY #BENE-CNT-CNTGNT #CITY-DONE
                pnd_Bene_Cnt_Cntgnt.reset();
                pnd_City_Done.reset();
                                                                                                                                                                          //Natural: PERFORM DISPLAY-INPUT
                sub_Display_Input();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM LOAD-PARTICIPANT-DATA
                sub_Load_Participant_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ltfp0710.getValue("*").reset();                                                                                                                           //Natural: RESET LTFP0710 ( * )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Mdo_Letter_Type.equals("MDAB") || ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPAB")))                                                //Natural: IF #P-MDO-LETTER-TYPE = 'MDAB' OR #P-MDO-LETTER-TYPE = 'SPAB'
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-INPUT-BENE
                sub_Display_Input_Bene();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT END OF DATA
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK03_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            if (condition(! (pnd_Escape_Top.getBoolean())))                                                                                                               //Natural: IF NOT #ESCAPE-TOP
            {
                                                                                                                                                                          //Natural: PERFORM STORE-BENE
                sub_Store_Bene();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOTITLE,"TOTAL MDO PARTICIPANT DUPLICATE ON CIS ==>",pnd_Total_Dup);                                                       //Natural: WRITE ( 2 ) 'TOTAL MDO PARTICIPANT DUPLICATE ON CIS ==>' #TOTAL-DUP
            if (condition(Global.isEscape())) return;
            getReports().write(3, ReportOption.NOTITLE,"TOTAL MDO PARTICIPANT ADDED TO CIS ==>",pnd_Total_Add);                                                           //Natural: WRITE ( 3 ) 'TOTAL MDO PARTICIPANT ADDED TO CIS ==>' #TOTAL-ADD
            if (condition(Global.isEscape())) return;
            getReports().write(4, ReportOption.NOTITLE,"TOTAL MDO PARTICIPANT ERRORED  OUT ==>",pnd_Total_Err);                                                           //Natural: WRITE ( 4 ) 'TOTAL MDO PARTICIPANT ERRORED  OUT ==>' #TOTAL-ERR
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* *-------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-PARTICIPANT-DATA
        //*  CIS-ADDRESS-DEST-NAME(#ADDR-INDX) := #P-PPYMT-DEST-NAME-35
        //*  BE1. ENDS HERE
        //*   IF  #CIS-HANDLE-CODE    EQ 'O'
        //*  ==================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-INPUT
        //*      WRITE(1) '=' #P-TICKER-SYMBOL
        //*      WRITE(1) '=' #P-ALLOCATION-IND
        //*      WRITE(1) '=' #P-ALLOCATION-AMT
        //* *ITE(1) '='#P-CPYMT-PAYEE-TYPE
        //*  WRITE(1) '='  #P-MA-NUMBER-OF-EXEMPTIONS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-INPUT-BENE
        //*  CIS-BENE-ANNT-PRFX
        //*  CIS-BENE-ANNT-MID-NME
        //*  CIS-BENE-ANNT-LST-NME := CIS-FRST-ANNT-LST-NME
        //*  CIS-BENE-ANNT-SFFX
        //*     COMPRESS #CIS-BENE-ALLOC-1-N #CIS-BENE-ALLOC-4-N INTO #ALLOC
        //*       LEAVING NO SPACE
        //*     MOVE LEFT JUSTIFIED #ALLOC TO #ALLOC
        //*     EXAMINE #ALLOC FOR ' ' REPLACE  WITH '0'
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-BENE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BENE-RLTN-CDE
        //* *---------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-CIS-REPORT-DUPLICATE
        //* *---------------------------------------------
        //* *---------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PARTICIPANT-CIS-ADDED
        //* *---------------------------------------------
        //*  09/2011 B.HOLLOWAY START NEW ADDRESS REFORMAT ROUTINE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REFORMAT-ADDRESS
        //* **********************************************************************
        //*  09/2011 B.HOLLOWAY END
        //* *-------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-ERROR-REPORT
        //* *-------------------------------------
        //* ***********************************************************************
        //*  CREA
        //* ***********************************************************************                                                                                       //Natural: ON ERROR
    }
    private void sub_Load_Participant_Data() throws Exception                                                                                                             //Natural: LOAD-PARTICIPANT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        pnd_Found.reset();                                                                                                                                                //Natural: RESET #FOUND
        ldaCispartv.getVw_cis_Part_View().startDatabaseRead                                                                                                               //Natural: READ CIS-PART-VIEW BY CIS-TIAA-NBR EQ #P-MDO-TIAA-CONTRACT
        (
        "READ1",
        new Wc[] { new Wc("CIS_TIAA_NBR", ">=", ltfp0710_Pnd_P_Mdo_Tiaa_Contract, WcType.BY) },
        new Oc[] { new Oc("CIS_TIAA_NBR", "ASC") }
        );
        READ1:
        while (condition(ldaCispartv.getVw_cis_Part_View().readNextRow("READ1")))
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr().notEquals(ltfp0710_Pnd_P_Mdo_Tiaa_Contract)))                                                       //Natural: IF CIS-TIAA-NBR NE #P-MDO-TIAA-CONTRACT
            {
                if (true) break READ1;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ1. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Found.setValue(true);                                                                                                                                     //Natural: ASSIGN #FOUND := TRUE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  09/2011 B.HOLLOWAY START - RESET TO ALLOW DUPS IF TESTING
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            pnd_Found.reset();                                                                                                                                            //Natural: RESET #FOUND
        }                                                                                                                                                                 //Natural: END-IF
        //*  09/2011 B.HOLLOWAY END
        if (condition(! (pnd_Found.getBoolean())))                                                                                                                        //Natural: IF NOT #FOUND
        {
            //*  #CIS-PIN-NBR-N := 2664185
            ldaCispartv.getVw_cis_Part_View().reset();                                                                                                                    //Natural: RESET CIS-PART-VIEW
            if (condition(ltfp0710_Pnd_P_Mdo_Contract_Type.equals("R")))                                                                                                  //Natural: IF #P-MDO-CONTRACT-TYPE = 'R'
            {
                //*    CIS-PIN-NBR     := #P-PIN-N-7           /* PINE
                //*  PINE
                //*  PINE
                if (condition(ltfp0710_Pnd_P_Pin_Filler.equals(" ")))                                                                                                     //Natural: IF #P-PIN-FILLER = ' '
                {
                    ldaCispartv.getCis_Part_View_Cis_Pin_Nbr().setValue(ltfp0710_Pnd_P_Pin_N_7);                                                                          //Natural: ASSIGN CIS-PIN-NBR := #P-PIN-N-7
                    //*  PINE
                    //*  PINE
                    //*  PINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_P_Pin_A_12.setValue(ltfp0710_Pnd_P_Pin);                                                                                                          //Natural: ASSIGN #P-PIN-A-12 := #P-PIN
                    ldaCispartv.getCis_Part_View_Cis_Pin_Nbr().setValue(pnd_P_Pin_A_12_Pnd_P_Pin_N_12);                                                                   //Natural: ASSIGN CIS-PIN-NBR := #P-PIN-N-12
                    //*  PINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaCispartv.getCis_Part_View_Cis_Rqst_Id_Key().setValue(ltfp0710_Pnd_P_Rqst_Id_Key);                                                                          //Natural: ASSIGN CIS-RQST-ID-KEY := #P-RQST-ID-KEY
            ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr().setValue(ltfp0710_Pnd_P_Mdo_Tiaa_Contract);                                                                       //Natural: ASSIGN CIS-TIAA-NBR := #P-MDO-TIAA-CONTRACT
            ldaCispartv.getCis_Part_View_Cis_Cert_Nbr().setValue(ltfp0710_Pnd_P_Mdo_Cref_Cert);                                                                           //Natural: ASSIGN CIS-CERT-NBR := #P-MDO-CREF-CERT
            getReports().write(0, "=",ltfp0710_Pnd_P_Annty_Strt_Dte);                                                                                                     //Natural: WRITE '=' #P-ANNTY-STRT-DTE
            if (Global.isEscape()) return;
            //* ** INC501276
            if (condition(! (DbsUtil.maskMatches(ltfp0710_Pnd_P_Annty_Strt_Dte,"YYYYMMDD"))))                                                                             //Natural: IF #P-ANNTY-STRT-DTE NE MASK ( YYYYMMDD )
            {
                //* ** INC501276
                getReports().write(0, "BAD ANNUITY START DATE");                                                                                                          //Natural: WRITE 'BAD ANNUITY START DATE'
                if (Global.isEscape()) return;
                //* ** INC501276
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCispartv.getCis_Part_View_Cis_Tiaa_Doi().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Annty_Strt_Dte);                                 //Natural: MOVE EDITED #P-ANNTY-STRT-DTE TO CIS-TIAA-DOI ( EM = YYYYMMDD )
                ldaCispartv.getCis_Part_View_Cis_Cref_Doi().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Annty_Strt_Dte);                                 //Natural: MOVE EDITED #P-ANNTY-STRT-DTE TO CIS-CREF-DOI ( EM = YYYYMMDD )
                //* ** INC501276
            }                                                                                                                                                             //Natural: END-IF
            ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Ssn().setValue(ltfp0710_Pnd_P_Frst_Annt_Ssn);                                                                      //Natural: ASSIGN CIS-FRST-ANNT-SSN := #P-FRST-ANNT-SSN
            ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme().setValue(ltfp0710_Pnd_P_Frst_Annt_Frst_Nme);                                                            //Natural: ASSIGN CIS-FRST-ANNT-FRST-NME := #P-FRST-ANNT-FRST-NME
            ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme().setValue(ltfp0710_Pnd_P_Frst_Annt_Lst_Nme);                                                              //Natural: ASSIGN CIS-FRST-ANNT-LST-NME := #P-FRST-ANNT-LST-NME
            ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Ctznshp_Cd().setValue(ltfp0710_Pnd_P_Frst_Annt_Ctznshp);                                                           //Natural: ASSIGN CIS-FRST-ANNT-CTZNSHP-CD := #P-FRST-ANNT-CTZNSHP
            ldaCispartv.getCis_Part_View_Cis_Pymnt_Mode().setValue(ltfp0710_Pnd_P_Mdo_Payment_Mode);                                                                      //Natural: ASSIGN CIS-PYMNT-MODE := #P-MDO-PAYMENT-MODE
            if (condition(ltfp0710_Pnd_P_Frst_Annt_Sex_Cde.equals("1") || ltfp0710_Pnd_P_Frst_Annt_Sex_Cde.equals("M")))                                                  //Natural: IF #P-FRST-ANNT-SEX-CDE EQ '1' OR #P-FRST-ANNT-SEX-CDE EQ 'M'
            {
                ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sex_Cde().setValue("M");                                                                                       //Natural: ASSIGN CIS-FRST-ANNT-SEX-CDE := 'M'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Frst_Annt_Sex_Cde.equals("2") || ltfp0710_Pnd_P_Frst_Annt_Sex_Cde.equals("F")))                                                  //Natural: IF #P-FRST-ANNT-SEX-CDE EQ '2' OR #P-FRST-ANNT-SEX-CDE EQ 'F'
            {
                ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sex_Cde().setValue("F");                                                                                       //Natural: ASSIGN CIS-FRST-ANNT-SEX-CDE := 'F'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Frst_Annt_Sex_Cde.equals(" ") || ltfp0710_Pnd_P_Frst_Annt_Sex_Cde.equals("0")))                                                  //Natural: IF #P-FRST-ANNT-SEX-CDE EQ ' ' OR #P-FRST-ANNT-SEX-CDE EQ '0'
            {
                ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sex_Cde().setValue("U");                                                                                       //Natural: ASSIGN CIS-FRST-ANNT-SEX-CDE := 'U'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Mdo_Contract_Type.equals("S")))                                                                                                  //Natural: IF #P-MDO-CONTRACT-TYPE = 'S'
            {
                ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Frst_Nme().setValue(ltfp0710_Pnd_P_Orgnl_Prtcpnt_Frst_Nme);                                                //Natural: ASSIGN CIS-ORGNL-PRTCPNT-FRST-NME := #P-ORGNL-PRTCPNT-FRST-NME
                ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Lst_Nme().setValue(ltfp0710_Pnd_P_Orgnl_Prtcpnt_Lst_Nme);                                                  //Natural: ASSIGN CIS-ORGNL-PRTCPNT-LST-NME := #P-ORGNL-PRTCPNT-LST-NME
                if (condition(ltfp0710_Pnd_P_Org_Prt_Date_Of_Birth_N.greater(getZero())))                                                                                 //Natural: IF #P-ORG-PRT-DATE-OF-BIRTH-N GT 0
                {
                    ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Birth_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Org_Prt_Date_Of_Birth);       //Natural: MOVE EDITED #P-ORG-PRT-DATE-OF-BIRTH TO CIS-ORGNL-PRTCPNT-BIRTH-DTE ( EM = YYYYMMDD )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ltfp0710_Pnd_P_Org_Prt_Date_Of_Death_N.greater(getZero())))                                                                                 //Natural: IF #P-ORG-PRT-DATE-OF-DEATH-N GT 0
                {
                    ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Death_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Org_Prt_Date_Of_Death);       //Natural: MOVE EDITED #P-ORG-PRT-DATE-OF-DEATH TO CIS-ORGNL-PRTCPNT-DEATH-DTE ( EM = YYYYMMDD )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  BE1 STARTS HERE...
            //*  CHECK SIP CONTRACTS TO SEE IF RESIDENCE STATE IS A MILITARY STATE
            //*  BIP
            if (condition(ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPAP") || ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPBP")))                                                //Natural: IF #P-MDO-LETTER-TYPE = 'SPAP' OR = 'SPBP'
            {
                if (condition(ltfp0710_Pnd_P_Res_State.greater(" ")))                                                                                                     //Natural: IF #P-RES-STATE GT ' '
                {
                    pnd_Hold_Res_State.setValue(ltfp0710_Pnd_P_Res_State);                                                                                                //Natural: MOVE #P-RES-STATE TO #HOLD-RES-STATE
                    short decideConditionsMet1553 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF #HOLD-RES-STATE;//Natural: VALUE 'AE'
                    if (condition((pnd_Hold_Res_State.equals("AE"))))
                    {
                        decideConditionsMet1553++;
                        pnd_Hold_Res_State.setValue("NY");                                                                                                                //Natural: MOVE 'NY' TO #HOLD-RES-STATE
                    }                                                                                                                                                     //Natural: VALUE 'AP'
                    else if (condition((pnd_Hold_Res_State.equals("AP"))))
                    {
                        decideConditionsMet1553++;
                        pnd_Hold_Res_State.setValue("CA");                                                                                                                //Natural: MOVE 'CA' TO #HOLD-RES-STATE
                    }                                                                                                                                                     //Natural: VALUE 'AA'
                    else if (condition((pnd_Hold_Res_State.equals("AA"))))
                    {
                        decideConditionsMet1553++;
                        pnd_Hold_Res_State.setValue("FL");                                                                                                                //Natural: MOVE 'FL' TO #HOLD-RES-STATE
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*  CONVERT ALPHA STATE TO NUMERIC STATE CODE
                }                                                                                                                                                         //Natural: END-IF
                FOR01:                                                                                                                                                    //Natural: FOR #X 1 62
                for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(62)); pnd_X.nadd(1))
                {
                    if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_X).equals(pnd_Hold_Res_State)))                                      //Natural: IF #STATE-CODE-A ( #X ) = #HOLD-RES-STATE
                    {
                        pnd_Rsdnce_St_Pnd_Rsdnce_St_A.setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(pnd_X));                                    //Natural: MOVE #STATE-CODE-N ( #X ) TO #RSDNCE-ST-A
                        pnd_Rsdnce_St_Pnd_Rsdnce_St_A.setValue(pnd_Rsdnce_St_Pnd_Rsdnce_St_A, MoveOption.RightJustified);                                                 //Natural: MOVE RIGHT JUSTIFIED #RSDNCE-ST-A TO #RSDNCE-ST-A
                        DbsUtil.examine(new ExamineSource(pnd_Rsdnce_St_Pnd_Rsdnce_St_A,true), new ExamineSearch(" "), new ExamineReplace("0"));                          //Natural: EXAMINE FULL #RSDNCE-ST-A FOR ' ' REPLACE WITH '0'
                        ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Rsdnc_Cde().setValue(pnd_Rsdnce_St_Pnd_Rsdnce_St_A);                                                   //Natural: MOVE #RSDNCE-ST-A TO CIS-FRST-ANNT-RSDNC-CDE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                if (condition(pnd_Hold_Res_State.equals("NY")))                                                                                                           //Natural: IF #HOLD-RES-STATE EQ 'NY'
                {
                    pnd_Rsdnce_St.setValue(35);                                                                                                                           //Natural: ASSIGN #RSDNCE-ST := 35
                    ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Rsdnc_Cde().setValue(pnd_Rsdnce_St);                                                                       //Natural: MOVE #RSDNCE-ST TO CIS-FRST-ANNT-RSDNC-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  BE1 ENDS HERE...
            //*   IF #P-MDO-LETTER-TYPE NE 'SPAP'   /* BE1.  ADDED THIS IF STATEMENT
            //*  BIP
            //*  CONVERT ALPHA STATE TO NUMERIC STATE CODE
            if (condition(! (ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPAP") || ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPBP"))))                                            //Natural: IF NOT ( #P-MDO-LETTER-TYPE = 'SPAP' OR = 'SPBP' )
            {
                FOR02:                                                                                                                                                    //Natural: FOR #X 1 62
                for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(62)); pnd_X.nadd(1))
                {
                    if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_X).equals(ltfp0710_Pnd_P_Frst_Annt_Rsdnc_Cde)))                      //Natural: IF #STATE-CODE-A ( #X ) = #P-FRST-ANNT-RSDNC-CDE
                    {
                        pnd_Rsdnce_St_Pnd_Rsdnce_St_A.setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(pnd_X));                                    //Natural: MOVE #STATE-CODE-N ( #X ) TO #RSDNCE-ST-A
                        pnd_Rsdnce_St_Pnd_Rsdnce_St_A.setValue(pnd_Rsdnce_St_Pnd_Rsdnce_St_A, MoveOption.RightJustified);                                                 //Natural: MOVE RIGHT JUSTIFIED #RSDNCE-ST-A TO #RSDNCE-ST-A
                        DbsUtil.examine(new ExamineSource(pnd_Rsdnce_St_Pnd_Rsdnce_St_A,true), new ExamineSearch(" "), new ExamineReplace("0"));                          //Natural: EXAMINE FULL #RSDNCE-ST-A FOR ' ' REPLACE WITH '0'
                        ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Rsdnc_Cde().setValue(pnd_Rsdnce_St_Pnd_Rsdnce_St_A);                                                   //Natural: MOVE #RSDNCE-ST-A TO CIS-FRST-ANNT-RSDNC-CDE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                if (condition(ltfp0710_Pnd_P_Frst_Annt_Rsdnc_Cde.equals("NY")))                                                                                           //Natural: IF #P-FRST-ANNT-RSDNC-CDE EQ 'NY'
                {
                    pnd_Rsdnce_St.setValue(35);                                                                                                                           //Natural: ASSIGN #RSDNCE-ST := 35
                    ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Rsdnc_Cde().setValue(pnd_Rsdnce_St);                                                                       //Natural: MOVE #RSDNCE-ST TO CIS-FRST-ANNT-RSDNC-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Original_Issue.equals(" ")))                                                                                                     //Natural: IF #P-ORIGINAL-ISSUE EQ ' '
            {
                //*  BE1. ADDED IF STATEMENT
                //*  BIP
                //*  BE1. USE RES. STATE FOR SIP
                if (condition(ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPAP") || ltfp0710_Pnd_P_Mdo_Letter_Type.equals("SPBP")))                                            //Natural: IF #P-MDO-LETTER-TYPE = 'SPAP' OR = 'SPBP'
                {
                    ltfp0710_Pnd_P_Original_Issue.setValue(ltfp0710_Pnd_P_Res_State);                                                                                     //Natural: ASSIGN #P-ORIGINAL-ISSUE := #P-RES-STATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ltfp0710_Pnd_P_Original_Issue.setValue(ltfp0710_Pnd_P_Frst_Annt_Rsdnc_Cde);                                                                           //Natural: ASSIGN #P-ORIGINAL-ISSUE := #P-FRST-ANNT-RSDNC-CDE
                }                                                                                                                                                         //Natural: END-IF
                //*  CONVERT ALPHA STATE TO NUMERIC STATE CODE
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR #X 1 62
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(62)); pnd_X.nadd(1))
            {
                getReports().write(0, "=",ltfp0710_Pnd_P_Original_Issue,"TTTTTTTTTTT");                                                                                   //Natural: WRITE '=' #P-ORIGINAL-ISSUE 'TTTTTTTTTTT'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_X).equals(ltfp0710_Pnd_P_Original_Issue)))                               //Natural: IF #STATE-CODE-A ( #X ) = #P-ORIGINAL-ISSUE
                {
                    pnd_Issue_St_Pnd_Issue_St_A.setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(pnd_X));                                          //Natural: MOVE #STATE-CODE-N ( #X ) TO #ISSUE-ST-A
                    pnd_Issue_St_Pnd_Issue_St_A.setValue(pnd_Issue_St_Pnd_Issue_St_A, MoveOption.RightJustified);                                                         //Natural: MOVE RIGHT JUSTIFIED #ISSUE-ST-A TO #ISSUE-ST-A
                    DbsUtil.examine(new ExamineSource(pnd_Issue_St_Pnd_Issue_St_A,true), new ExamineSearch(" "), new ExamineReplace("0"));                                //Natural: EXAMINE FULL #ISSUE-ST-A FOR ' ' REPLACE WITH '0'
                    ldaCispartv.getCis_Part_View_Cis_Orig_Issue_State().setValue(pnd_Issue_St_Pnd_Issue_St_A);                                                            //Natural: MOVE #ISSUE-ST-A TO CIS-ORIG-ISSUE-STATE
                    ldaCispartv.getCis_Part_View_Cis_Issue_State_Cd().setValue(pnd_Issue_St_Pnd_Issue_St_A);                                                              //Natural: MOVE #ISSUE-ST-A TO CIS-ISSUE-STATE-CD
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(ltfp0710_Pnd_P_Original_Issue.equals("NY")))                                                                                                    //Natural: IF #P-ORIGINAL-ISSUE EQ 'NY'
            {
                pnd_Issue_St.setValue(35);                                                                                                                                //Natural: ASSIGN #ISSUE-ST := 35
                ldaCispartv.getCis_Part_View_Cis_Orig_Issue_State().setValue(pnd_Issue_St);                                                                               //Natural: MOVE #ISSUE-ST TO CIS-ORIG-ISSUE-STATE
                ldaCispartv.getCis_Part_View_Cis_Issue_State_Cd().setValue(pnd_Issue_St);                                                                                 //Natural: MOVE #ISSUE-ST TO CIS-ISSUE-STATE-CD
            }                                                                                                                                                             //Natural: END-IF
            pnd_Lcl_Bene_Calc.setValue(ltfp0710_Pnd_P_Frst_Annt_Calc_Meth);                                                                                               //Natural: ASSIGN #LCL-BENE-CALC := #P-FRST-ANNT-CALC-METH
            ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Calc_Method().setValue("R");                                                                                       //Natural: ASSIGN CIS-FRST-ANNT-CALC-METHOD := 'R'
            ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Dob().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Frst_Annt_Dob);                                 //Natural: MOVE EDITED #P-FRST-ANNT-DOB TO CIS-FRST-ANNT-DOB ( EM = YYYYMMDD )
            ldaCispartv.getCis_Part_View_Cis_Opn_Clsd_Ind().setValue(ltfp0710_Pnd_P_Opn_Clsd_Ind);                                                                        //Natural: ASSIGN CIS-OPN-CLSD-IND := #P-OPN-CLSD-IND
            ldaCispartv.getCis_Part_View_Cis_Status_Cd().setValue(ltfp0710_Pnd_P_Status_Cd);                                                                              //Natural: ASSIGN CIS-STATUS-CD := #P-STATUS-CD
            ldaCispartv.getCis_Part_View_Cis_Cntrct_Type().setValue(ltfp0710_Pnd_P_Cntrct_Type);                                                                          //Natural: ASSIGN CIS-CNTRCT-TYPE := #P-CNTRCT-TYPE
            ldaCispartv.getCis_Part_View_Cis_Cntrct_Apprvl_Ind().setValue("Y");                                                                                           //Natural: ASSIGN CIS-CNTRCT-APPRVL-IND := 'Y'
            ldaCispartv.getCis_Part_View_Cis_Rqst_Id().setValue("MDO");                                                                                                   //Natural: ASSIGN CIS-RQST-ID := 'MDO'
            //* ** INC501276
            if (condition(DbsUtil.maskMatches(ltfp0710_Pnd_P_Annty_Strt_Dte,"YYYYMMDD")))                                                                                 //Natural: IF #P-ANNTY-STRT-DTE EQ MASK ( YYYYMMDD )
            {
                ldaCispartv.getCis_Part_View_Cis_Cntrct_Print_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Annty_Strt_Dte);                         //Natural: MOVE EDITED #P-ANNTY-STRT-DTE TO CIS-CNTRCT-PRINT-DTE ( EM = YYYYMMDD )
                ldaCispartv.getCis_Part_View_Cis_Extract_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Annty_Strt_Dte);                             //Natural: MOVE EDITED #P-ANNTY-STRT-DTE TO CIS-EXTRACT-DATE ( EM = YYYYMMDD )
                ldaCispartv.getCis_Part_View_Cis_Appl_Rcvd_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Annty_Strt_Dte);                            //Natural: MOVE EDITED #P-ANNTY-STRT-DTE TO CIS-APPL-RCVD-DTE ( EM = YYYYMMDD )
                //* ** INC501276
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Annty_Option.getSubstring(1,2).equals("IR")))                                                                                    //Natural: IF SUBSTRING ( #P-ANNTY-OPTION,1,2 ) EQ 'IR'
            {
                ldaCispartv.getCis_Part_View_Cis_Annty_Option().setValue("IRA");                                                                                          //Natural: ASSIGN CIS-ANNTY-OPTION := 'IRA'
                ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Cash_Status().setValue("C");                                                                                //Natural: ASSIGN CIS-MDO-CONTRACT-CASH-STATUS := 'C'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Annty_Option.getSubstring(1,2).equals("RA")))                                                                                    //Natural: IF SUBSTRING ( #P-ANNTY-OPTION,1,2 ) EQ 'RA'
            {
                ldaCispartv.getCis_Part_View_Cis_Annty_Option().setValue("RA");                                                                                           //Natural: ASSIGN CIS-ANNTY-OPTION := 'RA'
                ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Cash_Status().setValue("N");                                                                                //Natural: ASSIGN CIS-MDO-CONTRACT-CASH-STATUS := 'N'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Annty_Option.getSubstring(1,2).equals("GR")))                                                                                    //Natural: IF SUBSTRING ( #P-ANNTY-OPTION,1,2 ) EQ 'GR'
            {
                ldaCispartv.getCis_Part_View_Cis_Annty_Option().setValue("GRA");                                                                                          //Natural: ASSIGN CIS-ANNTY-OPTION := 'GRA'
                ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Cash_Status().setValue("N");                                                                                //Natural: ASSIGN CIS-MDO-CONTRACT-CASH-STATUS := 'N'
            }                                                                                                                                                             //Natural: END-IF
            //*  DEW
            if (condition(ltfp0710_Pnd_P_Annty_Option.getSubstring(1,2).equals("SR") || ltfp0710_Pnd_P_Annty_Option.getSubstring(1,2).equals("KE")))                      //Natural: IF SUBSTRING ( #P-ANNTY-OPTION,1,2 ) EQ 'SR' OR = 'KE'
            {
                ldaCispartv.getCis_Part_View_Cis_Annty_Option().setValue("SRA");                                                                                          //Natural: ASSIGN CIS-ANNTY-OPTION := 'SRA'
                ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Cash_Status().setValue("C");                                                                                //Natural: ASSIGN CIS-MDO-CONTRACT-CASH-STATUS := 'C'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Annty_Option.getSubstring(1,2).equals("GS")))                                                                                    //Natural: IF SUBSTRING ( #P-ANNTY-OPTION,1,2 ) EQ 'GS'
            {
                ldaCispartv.getCis_Part_View_Cis_Annty_Option().setValue("GSRA");                                                                                         //Natural: ASSIGN CIS-ANNTY-OPTION := 'GSRA'
                ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Cash_Status().setValue("C");                                                                                //Natural: ASSIGN CIS-MDO-CONTRACT-CASH-STATUS := 'C'
            }                                                                                                                                                             //Natural: END-IF
            //*  START NBC 8/2008
            if (condition(ltfp0710_Pnd_P_Annty_Option.getSubstring(1,2).equals("AN")))                                                                                    //Natural: IF SUBSTRING ( #P-ANNTY-OPTION,1,2 ) EQ 'AN'
            {
                ldaCispartv.getCis_Part_View_Cis_Annty_Option().setValue("RA");                                                                                           //Natural: ASSIGN CIS-ANNTY-OPTION := 'RA'
                ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Cash_Status().setValue("N");                                                                                //Natural: ASSIGN CIS-MDO-CONTRACT-CASH-STATUS := 'N'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Annty_Option.getSubstring(1,2).equals("AC")))                                                                                    //Natural: IF SUBSTRING ( #P-ANNTY-OPTION,1,2 ) EQ 'AC'
            {
                ldaCispartv.getCis_Part_View_Cis_Annty_Option().setValue("SRA");                                                                                          //Natural: ASSIGN CIS-ANNTY-OPTION := 'SRA'
                ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Cash_Status().setValue("C");                                                                                //Natural: ASSIGN CIS-MDO-CONTRACT-CASH-STATUS := 'C'
            }                                                                                                                                                             //Natural: END-IF
            //*  END NBC 8/2008
            getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Annty_Option(),"DDDDDDDDDD");                                                                      //Natural: WRITE '=' CIS-ANNTY-OPTION 'DDDDDDDDDD'
            if (Global.isEscape()) return;
            //*  CIS-ANNTY-OPTION := #P-ANNTY-OPTION
            //*  MOVE EDITED #P-ANNTY-STRT-DTE TO CIS-ANNTY-START-DTE(EM=YYYYMMDD)
            //*   MOVE EDITED #CIS-ANNTY-END-DTE TO CIS-ANNTY-END-DTE(EM=YYYYMMDD)
            if (condition(ltfp0710_Pnd_P_Mdo_Contract_Type.equals("R")))                                                                                                  //Natural: IF #P-MDO-CONTRACT-TYPE = 'R'
            {
                //* ** INC501276
                if (condition(DbsUtil.maskMatches(ltfp0710_Pnd_P_Annty_Strt_Dte,"YYYYMMDD")))                                                                             //Natural: IF #P-ANNTY-STRT-DTE EQ MASK ( YYYYMMDD )
                {
                    ldaCispartv.getCis_Part_View_Cis_Annty_Start_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Annty_Strt_Dte);                      //Natural: MOVE EDITED #P-ANNTY-STRT-DTE TO CIS-ANNTY-START-DTE ( EM = YYYYMMDD )
                    //* ** INC501276
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Mdo_Contract_Type.equals("S")))                                                                                                  //Natural: IF #P-MDO-CONTRACT-TYPE = 'S'
            {
                //*  DW1 INC1615517 START
                if (condition(ltfp0710_Pnd_P_Next_Pay_Date_N.equals(getZero())))                                                                                          //Natural: IF #P-NEXT-PAY-DATE-N EQ 0
                {
                    if (condition(DbsUtil.maskMatches(ltfp0710_Pnd_P_Annty_Strt_Dte,"YYYYMMDD")))                                                                         //Natural: IF #P-ANNTY-STRT-DTE EQ MASK ( YYYYMMDD )
                    {
                        ldaCispartv.getCis_Part_View_Cis_Annty_Start_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Annty_Strt_Dte);                  //Natural: MOVE EDITED #P-ANNTY-STRT-DTE TO CIS-ANNTY-START-DTE ( EM = YYYYMMDD )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DW1 INC1615517 END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCispartv.getCis_Part_View_Cis_Annty_Start_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Next_Pay_Date);                       //Natural: MOVE EDITED #P-NEXT-PAY-DATE TO CIS-ANNTY-START-DTE ( EM = YYYYMMDD )
                    //*  DW1 INC1615517 ADDED
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Issue_St_Pnd_Issue_St_A.setValue(pnd_Issue_St_Pnd_Issue_St_A, MoveOption.RightJustified);                                                                 //Natural: MOVE RIGHT JUSTIFIED #ISSUE-ST-A TO #ISSUE-ST-A
            DbsUtil.examine(new ExamineSource(pnd_Issue_St_Pnd_Issue_St_A), new ExamineSearch(" "), new ExamineReplace("0"));                                             //Natural: EXAMINE #ISSUE-ST-A FOR ' ' REPLACE WITH '0'
            ldaCispartv.getCis_Part_View_Cis_Orig_Issue_State().setValue(pnd_Issue_St_Pnd_Issue_St_A);                                                                    //Natural: ASSIGN CIS-ORIG-ISSUE-STATE := #ISSUE-ST-A
            ldaCispartv.getCis_Part_View_Cis_Issue_State_Cd().setValue(pnd_Issue_St_Pnd_Issue_St_A);                                                                      //Natural: ASSIGN CIS-ISSUE-STATE-CD := #ISSUE-ST-A
            ldaCispartv.getCis_Part_View_Cis_Lob().setValue("D");                                                                                                         //Natural: ASSIGN CIS-LOB := 'D'
            ldaCispartv.getCis_Part_View_Cis_Lob_Type().setValue("2");                                                                                                    //Natural: ASSIGN CIS-LOB-TYPE := '2'
            ldaCispartv.getCis_Part_View_Cis_Addr_Syn_Ind().setValue("S");                                                                                                //Natural: ASSIGN CIS-ADDR-SYN-IND := 'S'
            ldaCispartv.getCis_Part_View_Cis_Addr_Process_Env().setValue("YB");                                                                                           //Natural: ASSIGN CIS-ADDR-PROCESS-ENV := 'YB'
            ldaCispartv.getCis_Part_View_Cis_Address_Chg_Ind().getValue(1).setValue("Y");                                                                                 //Natural: ASSIGN CIS-ADDRESS-CHG-IND ( 1 ) := 'Y'
            pnd_Hold_Address_Pnd_Hdest_Addr1.setValue(ltfp0710_Pnd_P_Ppymt_Dest_Addr1);                                                                                   //Natural: ASSIGN #HDEST-ADDR1 := #P-PPYMT-DEST-ADDR1
            pnd_Hold_Address_Pnd_Hdest_Addr2.setValue(ltfp0710_Pnd_P_Ppymt_Dest_Addr2);                                                                                   //Natural: ASSIGN #HDEST-ADDR2 := #P-PPYMT-DEST-ADDR2
            pnd_Hold_Address_Pnd_Hdest_Addr3.setValue(ltfp0710_Pnd_P_Ppymt_Dest_Addr3);                                                                                   //Natural: ASSIGN #HDEST-ADDR3 := #P-PPYMT-DEST-ADDR3
            pnd_Hold_Address_Pnd_Hdest_City.setValue(ltfp0710_Pnd_P_Ppymt_Dest_City);                                                                                     //Natural: ASSIGN #HDEST-CITY := #P-PPYMT-DEST-CITY
            pnd_Hold_Address_Pnd_Hdest_State.setValue(ltfp0710_Pnd_P_Ppymt_Dest_State);                                                                                   //Natural: ASSIGN #HDEST-STATE := #P-PPYMT-DEST-STATE
            pnd_Hold_Address_Pnd_Hzip.setValue(ltfp0710_Pnd_P_Ppymt_Zip);                                                                                                 //Natural: ASSIGN #HZIP := #P-PPYMT-ZIP
            pnd_Addr_Indx.setValue(1);                                                                                                                                    //Natural: ASSIGN #ADDR-INDX := 1
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
            sub_Reformat_Address();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getReports().write(0, "REFORMAT #P-PPYMT ADDRESS");                                                                                                       //Natural: WRITE 'REFORMAT #P-PPYMT ADDRESS'
                if (Global.isEscape()) return;
                getReports().write(0, "=",ltfp0710_Pnd_P_Ppymt_Dest_Addr1);                                                                                               //Natural: WRITE '=' #P-PPYMT-DEST-ADDR1
                if (Global.isEscape()) return;
                getReports().write(0, "=",ltfp0710_Pnd_P_Ppymt_Dest_Addr2);                                                                                               //Natural: WRITE '=' #P-PPYMT-DEST-ADDR2
                if (Global.isEscape()) return;
                getReports().write(0, "=",ltfp0710_Pnd_P_Ppymt_Dest_Addr3);                                                                                               //Natural: WRITE '=' #P-PPYMT-DEST-ADDR3
                if (Global.isEscape()) return;
                getReports().write(0, "=",ltfp0710_Pnd_P_Ppymt_Dest_City);                                                                                                //Natural: WRITE '=' #P-PPYMT-DEST-CITY
                if (Global.isEscape()) return;
                getReports().write(0, "=",ltfp0710_Pnd_P_Ppymt_Dest_State);                                                                                               //Natural: WRITE '=' #P-PPYMT-DEST-STATE
                if (Global.isEscape()) return;
                getReports().write(0, "=",ltfp0710_Pnd_P_Ppymt_Zip);                                                                                                      //Natural: WRITE '=' #P-PPYMT-ZIP
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,1));                                                      //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,1 )
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,2));                                                      //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,2 )
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,3));                                                      //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,3 )
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,4));                                                      //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,4 )
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Zip_Code().getValue(pnd_Addr_Indx));                                                           //Natural: WRITE '=' CIS-ZIP-CODE ( #ADDR-INDX )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            ldaCispartv.getCis_Part_View_Cis_Addr_Usage_Code().getValue(pnd_Addr_Indx).setValue("1");                                                                     //Natural: ASSIGN CIS-ADDR-USAGE-CODE ( #ADDR-INDX ) := '1'
            if (condition(ltfp0710_Pnd_P_Spymt_Dest_Addr1.notEquals(" ")))                                                                                                //Natural: IF #P-SPYMT-DEST-ADDR1 NE ' '
            {
                pnd_Hold_Address_Pnd_Hdest_Addr1.setValue(ltfp0710_Pnd_P_Spymt_Dest_Addr1);                                                                               //Natural: ASSIGN #HDEST-ADDR1 := #P-SPYMT-DEST-ADDR1
                pnd_Hold_Address_Pnd_Hdest_Addr2.setValue(ltfp0710_Pnd_P_Spymt_Dest_Addr2);                                                                               //Natural: ASSIGN #HDEST-ADDR2 := #P-SPYMT-DEST-ADDR2
                pnd_Hold_Address_Pnd_Hdest_Addr3.setValue(ltfp0710_Pnd_P_Spymt_Dest_Addr3);                                                                               //Natural: ASSIGN #HDEST-ADDR3 := #P-SPYMT-DEST-ADDR3
                pnd_Hold_Address_Pnd_Hdest_City.setValue(ltfp0710_Pnd_P_Spymt_Dest_City);                                                                                 //Natural: ASSIGN #HDEST-CITY := #P-SPYMT-DEST-CITY
                pnd_Hold_Address_Pnd_Hdest_State.setValue(ltfp0710_Pnd_P_Spymt_Dest_State);                                                                               //Natural: ASSIGN #HDEST-STATE := #P-SPYMT-DEST-STATE
                pnd_Hold_Address_Pnd_Hzip.setValue(ltfp0710_Pnd_P_Spymt_Zip);                                                                                             //Natural: ASSIGN #HZIP := #P-SPYMT-ZIP
                pnd_Addr_Indx.setValue(2);                                                                                                                                //Natural: ASSIGN #ADDR-INDX := 2
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
                sub_Reformat_Address();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getReports().write(0, "REFORMAT #P-SPYMT ADDRESS");                                                                                                   //Natural: WRITE 'REFORMAT #P-SPYMT ADDRESS'
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Spymt_Dest_Addr1);                                                                                           //Natural: WRITE '=' #P-SPYMT-DEST-ADDR1
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Spymt_Dest_Addr2);                                                                                           //Natural: WRITE '=' #P-SPYMT-DEST-ADDR2
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Spymt_Dest_Addr3);                                                                                           //Natural: WRITE '=' #P-SPYMT-DEST-ADDR3
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Spymt_Dest_City);                                                                                            //Natural: WRITE '=' #P-SPYMT-DEST-CITY
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Spymt_Dest_State);                                                                                           //Natural: WRITE '=' #P-SPYMT-DEST-STATE
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Spymt_Zip);                                                                                                  //Natural: WRITE '=' #P-SPYMT-ZIP
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,1));                                                  //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,1 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,2));                                                  //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,2 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,3));                                                  //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,3 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,4));                                                  //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,4 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Zip_Code().getValue(pnd_Addr_Indx));                                                       //Natural: WRITE '=' CIS-ZIP-CODE ( #ADDR-INDX )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                ldaCispartv.getCis_Part_View_Cis_Addr_Usage_Code().getValue(pnd_Addr_Indx).setValue("2");                                                                 //Natural: ASSIGN CIS-ADDR-USAGE-CODE ( #ADDR-INDX ) := '2'
                ldaCispartv.getCis_Part_View_Cis_Address_Dest_Name().getValue(pnd_Addr_Indx).setValue(ltfp0710_Pnd_P_Spymt_Dest_Name);                                    //Natural: ASSIGN CIS-ADDRESS-DEST-NAME ( #ADDR-INDX ) := #P-SPYMT-DEST-NAME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ltfp0710_Pnd_P_Cpymt_Dest_Addr1.notEquals(" ")))                                                                                            //Natural: IF #P-CPYMT-DEST-ADDR1 NE ' '
                {
                    pnd_Hold_Address_Pnd_Hdest_Addr1.setValue(ltfp0710_Pnd_P_Cpymt_Dest_Addr1);                                                                           //Natural: ASSIGN #HDEST-ADDR1 := #P-CPYMT-DEST-ADDR1
                    pnd_Hold_Address_Pnd_Hdest_Addr2.setValue(ltfp0710_Pnd_P_Cpymt_Dest_Addr2);                                                                           //Natural: ASSIGN #HDEST-ADDR2 := #P-CPYMT-DEST-ADDR2
                    pnd_Hold_Address_Pnd_Hdest_Addr3.setValue(ltfp0710_Pnd_P_Cpymt_Dest_Addr3);                                                                           //Natural: ASSIGN #HDEST-ADDR3 := #P-CPYMT-DEST-ADDR3
                    pnd_Hold_Address_Pnd_Hdest_City.setValue(ltfp0710_Pnd_P_Cpymt_Dest_City);                                                                             //Natural: ASSIGN #HDEST-CITY := #P-CPYMT-DEST-CITY
                    pnd_Hold_Address_Pnd_Hdest_State.setValue(ltfp0710_Pnd_P_Cpymt_Dest_State);                                                                           //Natural: ASSIGN #HDEST-STATE := #P-CPYMT-DEST-STATE
                    pnd_Hold_Address_Pnd_Hzip.setValue(ltfp0710_Pnd_P_Cpymt_Zip);                                                                                         //Natural: ASSIGN #HZIP := #P-CPYMT-ZIP
                    pnd_Addr_Indx.setValue(2);                                                                                                                            //Natural: ASSIGN #ADDR-INDX := 2
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
                    sub_Reformat_Address();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(pnd_Debug.getBoolean()))                                                                                                                //Natural: IF #DEBUG
                    {
                        getReports().write(0, "REFORMAT #P-CPYMT ADDRESS");                                                                                               //Natural: WRITE 'REFORMAT #P-CPYMT ADDRESS'
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ltfp0710_Pnd_P_Cpymt_Dest_Addr1);                                                                                       //Natural: WRITE '=' #P-CPYMT-DEST-ADDR1
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ltfp0710_Pnd_P_Cpymt_Dest_Addr2);                                                                                       //Natural: WRITE '=' #P-CPYMT-DEST-ADDR2
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ltfp0710_Pnd_P_Cpymt_Dest_Addr3);                                                                                       //Natural: WRITE '=' #P-CPYMT-DEST-ADDR3
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ltfp0710_Pnd_P_Cpymt_Dest_City);                                                                                        //Natural: WRITE '=' #P-CPYMT-DEST-CITY
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ltfp0710_Pnd_P_Cpymt_Dest_State);                                                                                       //Natural: WRITE '=' #P-CPYMT-DEST-STATE
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ltfp0710_Pnd_P_Cpymt_Zip);                                                                                              //Natural: WRITE '=' #P-CPYMT-ZIP
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,1));                                              //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,1 )
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,2));                                              //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,2 )
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,3));                                              //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,3 )
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,4));                                              //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,4 )
                        if (Global.isEscape()) return;
                        getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Zip_Code().getValue(pnd_Addr_Indx));                                                   //Natural: WRITE '=' CIS-ZIP-CODE ( #ADDR-INDX )
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                    ldaCispartv.getCis_Part_View_Cis_Addr_Usage_Code().getValue(pnd_Addr_Indx).setValue("2");                                                             //Natural: ASSIGN CIS-ADDR-USAGE-CODE ( #ADDR-INDX ) := '2'
                    ldaCispartv.getCis_Part_View_Cis_Address_Dest_Name().getValue(pnd_Addr_Indx).setValue(ltfp0710_Pnd_P_Cpymt_Dest_Name);                                //Natural: ASSIGN CIS-ADDRESS-DEST-NAME ( #ADDR-INDX ) := #P-CPYMT-DEST-NAME
                    ldaCispartv.getCis_Part_View_Cis_Bank_Pymnt_Acct_Nmbr().getValue(pnd_Addr_Indx).setValue(ltfp0710_Pnd_P_Cpymt_Dest_Acct_Nbr);                         //Natural: ASSIGN CIS-BANK-PYMNT-ACCT-NMBR ( #ADDR-INDX ) := #P-CPYMT-DEST-ACCT-NBR
                    ldaCispartv.getCis_Part_View_Cis_Bank_Aba_Acct_Nmbr().getValue(pnd_Addr_Indx).setValue(ltfp0710_Pnd_P_Cpymt_Dest_Trnst_Cde);                          //Natural: ASSIGN CIS-BANK-ABA-ACCT-NMBR ( #ADDR-INDX ) := #P-CPYMT-DEST-TRNST-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  09/2011 B.HOLLOWAY END
            //*  BE1. STARTS HERE...  MOVE RESIDENCE ADDR TO ARRAY 3 OF CIS-PART FILE
            if (condition(ltfp0710_Pnd_P_Res_Addr1.notEquals(" ")))                                                                                                       //Natural: IF #P-RES-ADDR1 NE ' '
            {
                pnd_Hold_Address_Pnd_Hdest_Addr1.setValue(ltfp0710_Pnd_P_Res_Addr1);                                                                                      //Natural: ASSIGN #HDEST-ADDR1 := #P-RES-ADDR1
                pnd_Hold_Address_Pnd_Hdest_Addr2.setValue(ltfp0710_Pnd_P_Res_Addr2);                                                                                      //Natural: ASSIGN #HDEST-ADDR2 := #P-RES-ADDR2
                pnd_Hold_Address_Pnd_Hdest_Addr3.setValue(ltfp0710_Pnd_P_Res_Addr3);                                                                                      //Natural: ASSIGN #HDEST-ADDR3 := #P-RES-ADDR3
                pnd_Hold_Address_Pnd_Hdest_City.setValue(ltfp0710_Pnd_P_Res_City);                                                                                        //Natural: ASSIGN #HDEST-CITY := #P-RES-CITY
                pnd_Hold_Address_Pnd_Hdest_State.setValue(ltfp0710_Pnd_P_Res_State);                                                                                      //Natural: ASSIGN #HDEST-STATE := #P-RES-STATE
                pnd_Hold_Address_Pnd_Hzip.setValue(ltfp0710_Pnd_P_Res_Zip);                                                                                               //Natural: ASSIGN #HZIP := #P-RES-ZIP
                pnd_Addr_Indx.setValue(3);                                                                                                                                //Natural: ASSIGN #ADDR-INDX := 3
                                                                                                                                                                          //Natural: PERFORM REFORMAT-ADDRESS
                sub_Reformat_Address();
                if (condition(Global.isEscape())) {return;}
                //*  BE1. STORE RES. COUNTRY IF VALUED
                if (condition((ltfp0710_Pnd_P_Res_Country_Code.greater(" ") && (ltfp0710_Pnd_P_Res_Zip.equals("FORGN") || ltfp0710_Pnd_P_Res_Zip.equals("CANAD")))))      //Natural: IF #P-RES-COUNTRY-CODE GT ' ' AND ( #P-RES-ZIP = 'FORGN' OR = 'CANAD' )
                {
                    ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,4).setValue(ltfp0710_Pnd_P_Res_Country_Code);                                   //Natural: ASSIGN CIS-ADDRESS-TXT ( #ADDR-INDX,4 ) := #P-RES-COUNTRY-CODE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getReports().write(0, "REFORMAT #P-RES ADDRESS");                                                                                                     //Natural: WRITE 'REFORMAT #P-RES ADDRESS'
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Res_Addr1);                                                                                                  //Natural: WRITE '=' #P-RES-ADDR1
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Res_Addr2);                                                                                                  //Natural: WRITE '=' #P-RES-ADDR2
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Res_Addr3);                                                                                                  //Natural: WRITE '=' #P-RES-ADDR3
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Res_City);                                                                                                   //Natural: WRITE '=' #P-RES-CITY
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Res_State);                                                                                                  //Natural: WRITE '=' #P-RES-STATE
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ltfp0710_Pnd_P_Res_Zip);                                                                                                    //Natural: WRITE '=' #P-RES-ZIP
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,1));                                                  //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,1 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,2));                                                  //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,2 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,3));                                                  //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,3 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,4));                                                  //Natural: WRITE '=' CIS-ADDRESS-TXT ( #ADDR-INDX,4 )
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Zip_Code().getValue(pnd_Addr_Indx));                                                       //Natural: WRITE '=' CIS-ZIP-CODE ( #ADDR-INDX )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                ldaCispartv.getCis_Part_View_Cis_Addr_Usage_Code().getValue(pnd_Addr_Indx).setValue("3");                                                                 //Natural: ASSIGN CIS-ADDR-USAGE-CODE ( #ADDR-INDX ) := '3'
                //*  PRB44157 SET FOR MAILING.
            }                                                                                                                                                             //Natural: END-IF
            ldaCispartv.getCis_Part_View_Cis_Corp_Sync_Ind().setValue("Y");                                                                                               //Natural: ASSIGN CIS-CORP-SYNC-IND := 'Y'
            ldaCispartv.getCis_Part_View_Cis_Cor_Sync_Ind().setValue("S");                                                                                                //Natural: ASSIGN CIS-COR-SYNC-IND := 'S'
            ldaCispartv.getCis_Part_View_Cis_Cor_Process_Env().setValue("YB");                                                                                            //Natural: ASSIGN CIS-COR-PROCESS-ENV := 'YB'
            ldaCispartv.getCis_Part_View_Cis_Cor_Ph_Rcd_Typ_Cde().setValue(1);                                                                                            //Natural: ASSIGN CIS-COR-PH-RCD-TYP-CDE := 01
            ldaCispartv.getCis_Part_View_Cis_Cor_Cntrct_Payee_Cde().setValue("01");                                                                                       //Natural: ASSIGN CIS-COR-CNTRCT-PAYEE-CDE := '01'
            ldaCispartv.getCis_Part_View_Cis_Pull_Code().setValue("    ");                                                                                                //Natural: ASSIGN CIS-PULL-CODE := '    '
            //*   END-IF
            //*   IF #CIS-HANDLE-CODE EQ 'P'
            //*     CIS-PULL-CODE := 'SPEC'
            //*   END-IF
            //*   IF #CIS-ANNTY-OPTION EQ 'CASI'
            //*     CIS-PULL-CODE := 'SPEC'
            //*   END-IF
            if (condition(ltfp0710_Pnd_P_Tiaa_Accum_Settle_Amt.greater(getZero()) || ltfp0710_Pnd_P_Rea_Accum_Settle_Amt.greater(getZero())))                             //Natural: IF #P-TIAA-ACCUM-SETTLE-AMT GT 0 OR #P-REA-ACCUM-SETTLE-AMT GT 0
            {
                ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Nbr().getValue(1).setValue(ltfp0710_Pnd_P_Settled_Tiaa_Contract_Nbr);                                            //Natural: ASSIGN CIS-DA-TIAA-NBR ( 1 ) := #P-SETTLED-TIAA-CONTRACT-NBR
                ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Proceeds_Amt().getValue(1).compute(new ComputeParameters(false, ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Proceeds_Amt().getValue(1)),  //Natural: ASSIGN CIS-DA-TIAA-PROCEEDS-AMT ( 1 ) := #P-TIAA-ACCUM-SETTLE-AMT + #P-REA-ACCUM-SETTLE-AMT
                    ltfp0710_Pnd_P_Tiaa_Accum_Settle_Amt.add(ltfp0710_Pnd_P_Rea_Accum_Settle_Amt));
                ldaCispartv.getCis_Part_View_Cis_Mdo_Traditional_Amt().setValue(ltfp0710_Pnd_P_Tiaa_Accum_Settle_Amt);                                                    //Natural: ASSIGN CIS-MDO-TRADITIONAL-AMT := #P-TIAA-ACCUM-SETTLE-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Rea_Accum_Settle_Amt.greater(getZero())))                                                                                        //Natural: IF #P-REA-ACCUM-SETTLE-AMT GT 0
            {
                ldaCispartv.getCis_Part_View_Cis_Rea_Annty_Amt().setValue(ltfp0710_Pnd_P_Rea_Accum_Settle_Amt);                                                           //Natural: ASSIGN CIS-REA-ANNTY-AMT := #P-REA-ACCUM-SETTLE-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Cref_Accum_Settle_Amt.greater(getZero())))                                                                                       //Natural: IF #P-CREF-ACCUM-SETTLE-AMT GT 0
            {
                ldaCispartv.getCis_Part_View_Cis_Da_Cert_Nbr().getValue(1).setValue(ltfp0710_Pnd_P_Settled_Cref_Contract_Nbr);                                            //Natural: ASSIGN CIS-DA-CERT-NBR ( 1 ) := #P-SETTLED-CREF-CONTRACT-NBR
                ldaCispartv.getCis_Part_View_Cis_Da_Cref_Proceeds_Amt().getValue(1).setValue(ltfp0710_Pnd_P_Cref_Accum_Settle_Amt);                                       //Natural: ASSIGN CIS-DA-CREF-PROCEEDS-AMT ( 1 ) := #P-CREF-ACCUM-SETTLE-AMT
            }                                                                                                                                                             //Natural: END-IF
            ldaCispartv.getCis_Part_View_Cis_Grnted_Int_Rate().setValue(3);                                                                                               //Natural: ASSIGN CIS-GRNTED-INT-RATE := 3.00
            if (condition(! (DbsUtil.maskMatches(ltfp0710_Pnd_P_Minimum_Req_Amt,"NNNNNNNNNNN"))))                                                                         //Natural: IF #P-MINIMUM-REQ-AMT NE MASK ( NNNNNNNNNNN )
            {
                getReports().write(0, "=",ltfp0710_Pnd_P_Minimum_Req_Amt,"BAD VALUE NOT NUMERIC");                                                                        //Natural: WRITE '=' #P-MINIMUM-REQ-AMT 'BAD VALUE NOT NUMERIC'
                if (Global.isEscape()) return;
                pnd_Err_Message.setValue("OMNI MIN REQ AMT NOT NUMERIC");                                                                                                 //Natural: ASSIGN #ERR-MESSAGE := "OMNI MIN REQ AMT NOT NUMERIC"
                                                                                                                                                                          //Natural: PERFORM GENERATE-ERROR-REPORT
                sub_Generate_Error_Report();
                if (condition(Global.isEscape())) {return;}
                pnd_Escape_Top.setValue(true);                                                                                                                            //Natural: ASSIGN #ESCAPE-TOP := TRUE
                pnd_Total_Err.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOTAL-ERR
                Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                   //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "=",ltfp0710_Pnd_P_Minimum_Req_Amt_N,"DDDDDDD");                                                                                    //Natural: WRITE '=' #P-MINIMUM-REQ-AMT-N 'DDDDDDD'
                if (Global.isEscape()) return;
                if (condition(ltfp0710_Pnd_P_Minimum_Req_Amt_N.greater(getZero())))                                                                                       //Natural: IF #P-MINIMUM-REQ-AMT-N GT 0
                {
                    ldaCispartv.getCis_Part_View_Cis_Annual_Required_Dist_Amt().setValue(ltfp0710_Pnd_P_Minimum_Req_Amt_N);                                               //Natural: ASSIGN CIS-ANNUAL-REQUIRED-DIST-AMT := #P-MINIMUM-REQ-AMT-N
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Mdo_Contract_Type.equals("R")))                                                                                                  //Natural: IF #P-MDO-CONTRACT-TYPE = 'R'
            {
                if (condition(ltfp0710_Pnd_P_Next_Pay_Date_N.equals(getZero())))                                                                                          //Natural: IF #P-NEXT-PAY-DATE-N EQ 0
                {
                    //* ** INC501276
                    if (condition(DbsUtil.maskMatches(ltfp0710_Pnd_P_Annty_Strt_Dte,"YYYYMMDD")))                                                                         //Natural: IF #P-ANNTY-STRT-DTE EQ MASK ( YYYYMMDD )
                    {
                        ldaCispartv.getCis_Part_View_Cis_Required_Begin_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Annty_Strt_Dte);              //Natural: MOVE EDITED #P-ANNTY-STRT-DTE TO CIS-REQUIRED-BEGIN-DATE ( EM = YYYYMMDD )
                        //* ** INC501276
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCispartv.getCis_Part_View_Cis_Required_Begin_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Next_Pay_Date);                   //Natural: MOVE EDITED #P-NEXT-PAY-DATE TO CIS-REQUIRED-BEGIN-DATE ( EM = YYYYMMDD )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Mdo_Contract_Type.equals("S")))                                                                                                  //Natural: IF #P-MDO-CONTRACT-TYPE = 'S'
            {
                //*  DW1 INC1615517 START
                if (condition(ltfp0710_Pnd_P_Next_Pay_Date_N.equals(getZero())))                                                                                          //Natural: IF #P-NEXT-PAY-DATE-N EQ 0
                {
                    if (condition(DbsUtil.maskMatches(ltfp0710_Pnd_P_Annty_Strt_Dte,"YYYYMMDD")))                                                                         //Natural: IF #P-ANNTY-STRT-DTE EQ MASK ( YYYYMMDD )
                    {
                        ldaCispartv.getCis_Part_View_Cis_Required_Begin_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Annty_Strt_Dte);              //Natural: MOVE EDITED #P-ANNTY-STRT-DTE TO CIS-REQUIRED-BEGIN-DATE ( EM = YYYYMMDD )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DW1 INC1615517 END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCispartv.getCis_Part_View_Cis_Required_Begin_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_P_Next_Pay_Date);                   //Natural: MOVE EDITED #P-NEXT-PAY-DATE TO CIS-REQUIRED-BEGIN-DATE ( EM = YYYYMMDD )
                    //*  DW1 INC1615517 ADDED
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Acct_Cde.reset();                                                                                                                                         //Natural: RESET #ACCT-CDE
            FOR04:                                                                                                                                                        //Natural: FOR #I 1 TO 20
            for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(20)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
            {
                if (condition(ltfp0710_Pnd_P_Ticker_Symbol.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).notEquals(" ")))                                               //Natural: IF #P-TICKER-SYMBOL ( #I ) NE ' '
                {
                    //* CREA
                    //* CREA
                    DbsUtil.examine(new ExamineSource(pnd_Translte_File_Pnd_Translte_Ticker.getValue("*")), new ExamineSearch(ltfp0710_Pnd_P_Ticker_Symbol.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())),  //Natural: EXAMINE #TRANSLTE-TICKER ( * ) #P-TICKER-SYMBOL ( #I ) GIVING INDEX #FUND-IDX
                        new ExamineGivingIndex(pnd_Fund_Idx));
                    //* CREA
                    if (condition(pnd_Fund_Idx.greater(getZero())))                                                                                                       //Natural: IF #FUND-IDX > 0
                    {
                        //* CREA
                        //* CREA
                        if (condition(pnd_Translte_File_Pnd_Translte_Fund_Type.getValue(pnd_Fund_Idx).notEquals("T") && pnd_Translte_File_Pnd_Translte_Fund_Type.getValue(pnd_Fund_Idx).notEquals("R"))) //Natural: IF #TRANSLTE-FUND-TYPE ( #FUND-IDX ) NE 'T' AND #TRANSLTE-FUND-TYPE ( #FUND-IDX ) NE 'R'
                        {
                            pnd_Acct_Cde.nadd(1);                                                                                                                         //Natural: ADD 1 TO #ACCT-CDE
                            ldaCispartv.getCis_Part_View_Cis_Sg_Fund_Ticker().getValue(pnd_Acct_Cde).setValue(ltfp0710_Pnd_P_Ticker_Symbol.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN CIS-SG-FUND-TICKER ( #ACCT-CDE ) := #P-TICKER-SYMBOL ( #I )
                            ldaCispartv.getCis_Part_View_Cis_Sg_Fund_Amt().getValue(pnd_Acct_Cde).setValue(ltfp0710_Pnd_P_Allocation_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN CIS-SG-FUND-AMT ( #ACCT-CDE ) := #P-ALLOCATION-AMT ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                        //* CREA
                        if (condition(pnd_Translte_File_Pnd_Translte_Fund_Type.getValue(pnd_Fund_Idx).equals("R")))                                                       //Natural: IF #TRANSLTE-FUND-TYPE ( #FUND-IDX ) EQ 'R'
                        {
                            ldaCispartv.getCis_Part_View_Cis_Rea_Annual_Nbr_Units().setValue(ltfp0710_Pnd_P_Allocation_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN CIS-REA-ANNUAL-NBR-UNITS := #P-ALLOCATION-AMT ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //* CHG348394
                        //* CHG348394
                        //* CHG348394
                        //* CHG348394
                        pnd_Acct_Cde.nadd(1);                                                                                                                             //Natural: ADD 1 TO #ACCT-CDE
                        ldaCispartv.getCis_Part_View_Cis_Sg_Fund_Ticker().getValue(pnd_Acct_Cde).setValue(ltfp0710_Pnd_P_Ticker_Symbol.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN CIS-SG-FUND-TICKER ( #ACCT-CDE ) := #P-TICKER-SYMBOL ( #I )
                        ldaCispartv.getCis_Part_View_Cis_Sg_Fund_Amt().getValue(pnd_Acct_Cde).setValue(ltfp0710_Pnd_P_Allocation_Amt.getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN CIS-SG-FUND-AMT ( #ACCT-CDE ) := #P-ALLOCATION-AMT ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Type().setValue(ltfp0710_Pnd_P_Mdo_Contract_Type);                                                              //Natural: ASSIGN CIS-MDO-CONTRACT-TYPE := #P-MDO-CONTRACT-TYPE
            ldaCispartv.getCis_Part_View_Cis_Mdo_Traditional_Amt().setValue(ltfp0710_Pnd_P_Mdo_Traditional_Amt);                                                          //Natural: ASSIGN CIS-MDO-TRADITIONAL-AMT := #P-MDO-TRADITIONAL-AMT
            ldaCispartv.getCis_Part_View_Cis_Mdo_Tiaa_Int_Pymnt_Amt().setValue(0);                                                                                        //Natural: ASSIGN CIS-MDO-TIAA-INT-PYMNT-AMT := 0.0
            ldaCispartv.getCis_Part_View_Cis_Mdo_Cref_Int_Pymnt_Amt().setValue(0);                                                                                        //Natural: ASSIGN CIS-MDO-CREF-INT-PYMNT-AMT := 0.0
            if (condition(ltfp0710_Pnd_P_Two_Payments_In_One_Year.equals("Y")))                                                                                           //Natural: IF #P-TWO-PAYMENTS-IN-ONE-YEAR EQ 'Y'
            {
                ldaCispartv.getCis_Part_View_Cis_Mdo_Tiaa_Int_Pymnt_Amt().setValue(ltfp0710_Pnd_P_Mdo_Tiaa_Int_Pymnt_Amt);                                                //Natural: ASSIGN CIS-MDO-TIAA-INT-PYMNT-AMT := #P-MDO-TIAA-INT-PYMNT-AMT
                ldaCispartv.getCis_Part_View_Cis_Mdo_Cref_Int_Pymnt_Amt().setValue(ltfp0710_Pnd_P_Mdo_Cref_Int_Pymnt_Amt);                                                //Natural: ASSIGN CIS-MDO-CREF-INT-PYMNT-AMT := #P-MDO-CREF-INT-PYMNT-AMT
            }                                                                                                                                                             //Natural: END-IF
            //* *CIS-ANNUAL-REQUIRED-DIST-AMT
            //* *8CIS-REQUIRED-BEGIN-DATE  :=
            //* * ADD ATRA FLAG FOR CIS EXTRACT   /* ATRA  >>>
            if (condition(ltfp0710_Pnd_P_Omni_Plan_Id.equals("SIP401")))                                                                                                  //Natural: IF #P-OMNI-PLAN-ID = 'SIP401'
            {
                ldaCispartv.getCis_Part_View_Cis_Sg_Text_Udf_1().setValue("A");                                                                                           //Natural: ASSIGN CIS-SG-TEXT-UDF-1 := 'A'
                //*  ATRA  <<<
            }                                                                                                                                                             //Natural: END-IF
            //*  PASS ROTH INFORMATION /* CA-06/2009 START
            pnd_Cis_Sg_Text_Udf_2.reset();                                                                                                                                //Natural: RESET #CIS-SG-TEXT-UDF-2
            if (condition(ltfp0710_Pnd_P_Has_Roth_Money_Ind.equals("Y") || ltfp0710_Pnd_P_Has_Roth_Money_Ind.equals("y")))                                                //Natural: IF #P-HAS-ROTH-MONEY-IND = 'Y' OR = 'y'
            {
                pnd_Cis_Sg_Text_Udf_2_Pnd_Sg_Udf2_F1_Roth_Ind.setValue("R");                                                                                              //Natural: ASSIGN #SG-UDF2-F1-ROTH-IND := 'R'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_P_Payee_Cde.equals("1") || ltfp0710_Pnd_P_Payee_Cde.equals("2") || ltfp0710_Pnd_P_Payee_Cde.equals("3")))                          //Natural: IF #P-PAYEE-CDE = '1' OR = '2' OR = '3'
            {
                pnd_Cis_Sg_Text_Udf_2_Pnd_Sg_Udf2_F2_Payee_Cde.setValue("B");                                                                                             //Natural: ASSIGN #SG-UDF2-F2-PAYEE-CDE := 'B'
                //* ACCRC
                //* BIP
                //* BIP
                //* BIP
            }                                                                                                                                                             //Natural: END-IF
            ldaCispartv.getCis_Part_View_Cis_Sg_Text_Udf_2().setValue(pnd_Cis_Sg_Text_Udf_2);                                                                             //Natural: ASSIGN CIS-SG-TEXT-UDF-2 := #CIS-SG-TEXT-UDF-2
            ldaCispartv.getCis_Part_View_Cis_Plan_Num().setValue(ltfp0710_Pnd_P_Omni_Plan_Id);                                                                            //Natural: ASSIGN CIS-PLAN-NUM := #P-OMNI-PLAN-ID
            ldaCispartv.getCis_Part_View_Cis_Decedent_Contract().setValue(ltfp0710_Pnd_Decedent_Contract);                                                                //Natural: ASSIGN CIS-DECEDENT-CONTRACT := #DECEDENT-CONTRACT
            ldaCispartv.getCis_Part_View_Cis_Relation_To_Decedent().setValue(ltfp0710_Pnd_Relation_To_Decedent);                                                          //Natural: ASSIGN CIS-RELATION-TO-DECEDENT := #RELATION-TO-DECEDENT
            ldaCispartv.getCis_Part_View_Cis_Ssn_Tin_Ind().setValue(ltfp0710_Pnd_Ssn_Tin_Ind);                                                                            //Natural: ASSIGN CIS-SSN-TIN-IND := #SSN-TIN-IND
            ldaCispartv.getVw_cis_Part_View().insertDBRow();                                                                                                              //Natural: STORE CIS-PART-VIEW
            //*  09/2011 B.HOLLOWAY START - BACKOUT IF TESTING
            if (condition(pnd_Debug.getBoolean()))                                                                                                                        //Natural: IF #DEBUG
            {
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            //*  09/2011 B.HOLLOWAY END
            //*  TS120408
            pnd_Escape_Top.reset();                                                                                                                                       //Natural: RESET #ESCAPE-TOP
            pnd_Total_Add.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOTAL-ADD
                                                                                                                                                                          //Natural: PERFORM PARTICIPANT-CIS-ADDED
            sub_Participant_Cis_Added();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Found.getBoolean()))                                                                                                                            //Natural: IF #FOUND
        {
            pnd_Total_Dup.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOTAL-DUP
                                                                                                                                                                          //Natural: PERFORM GENERATE-CIS-REPORT-DUPLICATE
            sub_Generate_Cis_Report_Duplicate();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*   PERFORM UPDATE-MIT
        //*  09/2011 B.HOLLOWAY START - BACKOUT IF TESTING
        if (condition(pnd_Debug.getBoolean()))                                                                                                                            //Natural: IF #DEBUG
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  09/2011 B.HOLLOWAY END
    }
    private void sub_Error() throws Exception                                                                                                                             //Natural: ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  ==================================
        DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                        //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        //* *MOVE TRUE TO #ERR-FATAL
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        getReports().write(1, " Error Field:",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field(),NEWLINE,"         Msg:",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());       //Natural: WRITE ( 1 ) ' Error Field:' MSG-INFO-SUB.##ERROR-FIELD / '         Msg:' MSG-INFO-SUB.##MSG
        if (Global.isEscape()) return;
        pnd_Byp_Total.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #BYP-TOTAL
    }
    //*  PINE
    private void sub_Display_Input() throws Exception                                                                                                                     //Natural: DISPLAY-INPUT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, "=",ltfp0710_Pnd_P_Mdo_Letter_Type);                                                                                                        //Natural: WRITE ( 1 ) '=' #P-MDO-LETTER-TYPE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Omni_Plan_Id);                                                                                                           //Natural: WRITE ( 1 ) '=' #P-OMNI-PLAN-ID
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Omni_Sub_Plan_Id);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-OMNI-SUB-PLAN-ID
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Pin);                                                                                                                    //Natural: WRITE ( 1 ) '=' #P-PIN
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Mdo_Tiaa_Contract);                                                                                                      //Natural: WRITE ( 1 ) '=' #P-MDO-TIAA-CONTRACT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Mdo_Cref_Cert);                                                                                                          //Natural: WRITE ( 1 ) '=' #P-MDO-CREF-CERT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Omni_Plan_Name);                                                                                                         //Natural: WRITE ( 1 ) '=' #P-OMNI-PLAN-NAME
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Participant_Name);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-PARTICIPANT-NAME
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Frst_Annt_Ssn);                                                                                                          //Natural: WRITE ( 1 ) '=' #P-FRST-ANNT-SSN
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Frst_Annt_Frst_Nme);                                                                                                     //Natural: WRITE ( 1 ) '=' #P-FRST-ANNT-FRST-NME
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Frst_Annt_Lst_Nme);                                                                                                      //Natural: WRITE ( 1 ) '=' #P-FRST-ANNT-LST-NME
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Frst_Annt_Dob);                                                                                                          //Natural: WRITE ( 1 ) '=' #P-FRST-ANNT-DOB
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Frst_Annt_Rsdnc_Cde);                                                                                                    //Natural: WRITE ( 1 ) '=' #P-FRST-ANNT-RSDNC-CDE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Frst_Annt_Ctznshp);                                                                                                      //Natural: WRITE ( 1 ) '=' #P-FRST-ANNT-CTZNSHP
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Frst_Annt_Sex_Cde);                                                                                                      //Natural: WRITE ( 1 ) '=' #P-FRST-ANNT-SEX-CDE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Frst_Annt_Calc_Meth);                                                                                                    //Natural: WRITE ( 1 ) '=' #P-FRST-ANNT-CALC-METH
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Mdo_Payment_Mode);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-MDO-PAYMENT-MODE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Annty_Strt_Dte);                                                                                                         //Natural: WRITE ( 1 ) '=' #P-ANNTY-STRT-DTE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Next_Pay_Date);                                                                                                          //Natural: WRITE ( 1 ) '=' #P-NEXT-PAY-DATE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Pro_Rata);                                                                                                               //Natural: WRITE ( 1 ) '=' #P-PRO-RATA
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Mdo_Contract_Type);                                                                                                      //Natural: WRITE ( 1 ) '=' #P-MDO-CONTRACT-TYPE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Settled_Tiaa_Contract_Nbr);                                                                                              //Natural: WRITE ( 1 ) '=' #P-SETTLED-TIAA-CONTRACT-NBR
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Settled_Cref_Contract_Nbr);                                                                                              //Natural: WRITE ( 1 ) '=' #P-SETTLED-CREF-CONTRACT-NBR
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Settlement_Ind);                                                                                                         //Natural: WRITE ( 1 ) '=' #P-SETTLEMENT-IND
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Tiaa_Accum_Settle_Amt);                                                                                                  //Natural: WRITE ( 1 ) '=' #P-TIAA-ACCUM-SETTLE-AMT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Tiaa_Accum_Settle_Typ);                                                                                                  //Natural: WRITE ( 1 ) '=' #P-TIAA-ACCUM-SETTLE-TYP
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Rea_Accum_Settle_Amt);                                                                                                   //Natural: WRITE ( 1 ) '=' #P-REA-ACCUM-SETTLE-AMT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Rea_Accum_Settle_Typ);                                                                                                   //Natural: WRITE ( 1 ) '=' #P-REA-ACCUM-SETTLE-TYP
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cref_Accum_Settle_Amt);                                                                                                  //Natural: WRITE ( 1 ) '=' #P-CREF-ACCUM-SETTLE-AMT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cref_Accum_Settle_Typ);                                                                                                  //Natural: WRITE ( 1 ) '=' #P-CREF-ACCUM-SETTLE-TYP
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cref_Annty_Payment);                                                                                                     //Natural: WRITE ( 1 ) '=' #P-CREF-ANNTY-PAYMENT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cref_Mnthly_Nbr_Units);                                                                                                  //Natural: WRITE ( 1 ) '=' #P-CREF-MNTHLY-NBR-UNITS
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Rea_Mnthly_Nbr_Units);                                                                                                   //Natural: WRITE ( 1 ) '=' #P-REA-MNTHLY-NBR-UNITS
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Nra_Flag);                                                                                                               //Natural: WRITE ( 1 ) '=' #P-NRA-FLAG
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Rqst_Id_Key);                                                                                                            //Natural: WRITE ( 1 ) '=' #P-RQST-ID-KEY
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Opn_Clsd_Ind);                                                                                                           //Natural: WRITE ( 1 ) '=' #P-OPN-CLSD-IND
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Status_Cd);                                                                                                              //Natural: WRITE ( 1 ) '=' #P-STATUS-CD
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cntrct_Type);                                                                                                            //Natural: WRITE ( 1 ) '=' #P-CNTRCT-TYPE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cntrct_Apprvl_Ind);                                                                                                      //Natural: WRITE ( 1 ) '=' #P-CNTRCT-APPRVL-IND
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Rqst_Id);                                                                                                                //Natural: WRITE ( 1 ) '=' #P-RQST-ID
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cntrct_Print_Dte);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-CNTRCT-PRINT-DTE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Extract_Date);                                                                                                           //Natural: WRITE ( 1 ) '=' #P-EXTRACT-DATE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Appl_Rcvd_Dte);                                                                                                          //Natural: WRITE ( 1 ) '=' #P-APPL-RCVD-DTE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Appl_Rcvd_User_Id);                                                                                                      //Natural: WRITE ( 1 ) '=' #P-APPL-RCVD-USER-ID
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Appl_Entry_User_Id);                                                                                                     //Natural: WRITE ( 1 ) '=' #P-APPL-ENTRY-USER-ID
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Annty_Option);                                                                                                           //Natural: WRITE ( 1 ) '=' #P-ANNTY-OPTION
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Grnted_Int_Rate);                                                                                                        //Natural: WRITE ( 1 ) '=' #P-GRNTED-INT-RATE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Mdo_Traditional_Amt);                                                                                                    //Natural: WRITE ( 1 ) '=' #P-MDO-TRADITIONAL-AMT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Mdo_Tiaa_Int_Pymnt_Amt);                                                                                                 //Natural: WRITE ( 1 ) '=' #P-MDO-TIAA-INT-PYMNT-AMT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Mdo_Cref_Int_Pymnt_Amt);                                                                                                 //Natural: WRITE ( 1 ) '=' #P-MDO-CREF-INT-PYMNT-AMT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Fed_Election_Option);                                                                                                    //Natural: WRITE ( 1 ) '=' #P-FED-ELECTION-OPTION
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Fed_Tax_Amt_Tiaa_Rea);                                                                                                   //Natural: WRITE ( 1 ) '=' #P-FED-TAX-AMT-TIAA-REA
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Fed_Tax_Amt_Ind_Tiaa_Rea);                                                                                               //Natural: WRITE ( 1 ) '=' #P-FED-TAX-AMT-IND-TIAA-REA
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Fed_Tax_Amt_Cref);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-FED-TAX-AMT-CREF
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Fed_Tax_Amt_Ind_Cref);                                                                                                   //Natural: WRITE ( 1 ) '=' #P-FED-TAX-AMT-IND-CREF
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Sta_Election_Option);                                                                                                    //Natural: WRITE ( 1 ) '=' #P-STA-ELECTION-OPTION
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Sta_Tax_Amt_Tiaa_Rea);                                                                                                   //Natural: WRITE ( 1 ) '=' #P-STA-TAX-AMT-TIAA-REA
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Sta_Tax_Amt_Ind_Tiaa_Rea);                                                                                               //Natural: WRITE ( 1 ) '=' #P-STA-TAX-AMT-IND-TIAA-REA
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Sta_Tax_Amt_Cref);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-STA-TAX-AMT-CREF
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Sta_Tax_Amt_Ind_Cref);                                                                                                   //Natural: WRITE ( 1 ) '=' #P-STA-TAX-AMT-IND-CREF
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Pi_Task_Id);                                                                                                             //Natural: WRITE ( 1 ) '=' #P-PI-TASK-ID
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Ppymt_Dest_Name);                                                                                                        //Natural: WRITE ( 1 ) '=' #P-PPYMT-DEST-NAME
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Ppymt_Dest_Addr1);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-PPYMT-DEST-ADDR1
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Ppymt_Dest_Addr2);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-PPYMT-DEST-ADDR2
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Ppymt_Dest_Addr3);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-PPYMT-DEST-ADDR3
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Ppymt_Dest_City);                                                                                                        //Natural: WRITE ( 1 ) '=' #P-PPYMT-DEST-CITY
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Ppymt_Dest_State);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-PPYMT-DEST-STATE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Ppymt_Zip);                                                                                                              //Natural: WRITE ( 1 ) '=' #P-PPYMT-ZIP
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Spymt_Dest_Name);                                                                                                        //Natural: WRITE ( 1 ) '=' #P-SPYMT-DEST-NAME
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Spymt_Dest_Addr1);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-SPYMT-DEST-ADDR1
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Spymt_Dest_Addr2);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-SPYMT-DEST-ADDR2
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Spymt_Dest_Addr3);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-SPYMT-DEST-ADDR3
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Spymt_Dest_City);                                                                                                        //Natural: WRITE ( 1 ) '=' #P-SPYMT-DEST-CITY
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Spymt_Dest_State);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-SPYMT-DEST-STATE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Spymt_Zip);                                                                                                              //Natural: WRITE ( 1 ) '=' #P-SPYMT-ZIP
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Spymt_Dest_Acct_Nbr);                                                                                                    //Natural: WRITE ( 1 ) '=' #P-SPYMT-DEST-ACCT-NBR
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Spymt_Dest_Trnst_Cde);                                                                                                   //Natural: WRITE ( 1 ) '=' #P-SPYMT-DEST-TRNST-CDE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Spymt_Dest_Acct_Typ);                                                                                                    //Natural: WRITE ( 1 ) '=' #P-SPYMT-DEST-ACCT-TYP
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cpymt_Dest_Name);                                                                                                        //Natural: WRITE ( 1 ) '=' #P-CPYMT-DEST-NAME
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cpymt_Dest_Addr1);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-CPYMT-DEST-ADDR1
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cpymt_Dest_Addr2);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-CPYMT-DEST-ADDR2
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cpymt_Dest_Addr3);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-CPYMT-DEST-ADDR3
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cpymt_Dest_City);                                                                                                        //Natural: WRITE ( 1 ) '=' #P-CPYMT-DEST-CITY
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cpymt_Dest_State);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-CPYMT-DEST-STATE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cpymt_Zip);                                                                                                              //Natural: WRITE ( 1 ) '=' #P-CPYMT-ZIP
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cpymt_Dest_Acct_Nbr);                                                                                                    //Natural: WRITE ( 1 ) '=' #P-CPYMT-DEST-ACCT-NBR
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cpymt_Dest_Trnst_Cde);                                                                                                   //Natural: WRITE ( 1 ) '=' #P-CPYMT-DEST-TRNST-CDE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cpymt_Dest_Acct_Typ);                                                                                                    //Natural: WRITE ( 1 ) '=' #P-CPYMT-DEST-ACCT-TYP
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Cpymt_Payee_Amt);                                                                                                        //Natural: WRITE ( 1 ) '=' #P-CPYMT-PAYEE-AMT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Two_Payments_In_One_Year);                                                                                               //Natural: WRITE ( 1 ) '=' #P-TWO-PAYMENTS-IN-ONE-YEAR
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Mailing_Country_Code);                                                                                                   //Natural: WRITE ( 1 ) '=' #P-MAILING-COUNTRY-CODE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Minimum_Req_Amt);                                                                                                        //Natural: WRITE ( 1 ) '=' #P-MINIMUM-REQ-AMT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Payee_Cde);                                                                                                              //Natural: WRITE ( 1 ) '=' #P-PAYEE-CDE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Has_Roth_Money_Ind);                                                                                                     //Natural: WRITE ( 1 ) '=' #P-HAS-ROTH-MONEY-IND
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Original_Issue);                                                                                                         //Natural: WRITE ( 1 ) '=' #P-ORIGINAL-ISSUE
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Org_Prt_Date_Of_Death);                                                                                                  //Natural: WRITE ( 1 ) '=' #P-ORG-PRT-DATE-OF-DEATH
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Org_Prt_Date_Of_Birth);                                                                                                  //Natural: WRITE ( 1 ) '=' #P-ORG-PRT-DATE-OF-BIRTH
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Orgnl_Prtcpnt_Frst_Nme);                                                                                                 //Natural: WRITE ( 1 ) '=' #P-ORGNL-PRTCPNT-FRST-NME
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Orgnl_Prtcpnt_Lst_Nme);                                                                                                  //Natural: WRITE ( 1 ) '=' #P-ORGNL-PRTCPNT-LST-NME
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Invest_Process_Dt);                                                                                                      //Natural: WRITE ( 1 ) '=' #P-INVEST-PROCESS-DT
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Invest_Amt_Total);                                                                                                       //Natural: WRITE ( 1 ) '=' #P-INVEST-AMT-TOTAL
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Invest_Num.getValue("*"));                                                                                               //Natural: WRITE ( 1 ) '=' #P-INVEST-NUM ( * )
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Invest_Orig_Amt.getValue("*"));                                                                                          //Natural: WRITE ( 1 ) '=' #P-INVEST-ORIG-AMT ( * )
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Invest_Tiaa_Num_1.getValue("*"));                                                                                        //Natural: WRITE ( 1 ) '=' #P-INVEST-TIAA-NUM-1 ( * )
        if (Global.isEscape()) return;
        getReports().write(1, "=",ltfp0710_Pnd_P_Invest_Cref_Num_1.getValue("*"));                                                                                        //Natural: WRITE ( 1 ) '=' #P-INVEST-CREF-NUM-1 ( * )
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #CNTX = 1 TO 100
        for (pnd_Cntx.setValue(1); condition(pnd_Cntx.lessOrEqual(100)); pnd_Cntx.nadd(1))
        {
            if (condition(ltfp0710_Pnd_P_Invest_Ticker.getValue(pnd_Cntx).equals(" ")))                                                                                   //Natural: IF #P-INVEST-TICKER ( #CNTX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, pnd_Cntx,"=",ltfp0710_Pnd_P_Invest_Ticker.getValue(pnd_Cntx));                                                                          //Natural: WRITE ( 1 ) #CNTX '=' #P-INVEST-TICKER ( #CNTX )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, pnd_Cntx,"=",ltfp0710_Pnd_P_Invest_Amt.getValue(pnd_Cntx));                                                                             //Natural: WRITE ( 1 ) #CNTX '=' #P-INVEST-AMT ( #CNTX )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, pnd_Cntx,"=",ltfp0710_Pnd_P_Invest_Unit_Price.getValue(pnd_Cntx));                                                                      //Natural: WRITE ( 1 ) #CNTX '=' #P-INVEST-UNIT-PRICE ( #CNTX )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, pnd_Cntx,"=",ltfp0710_Pnd_P_Invest_Units.getValue(pnd_Cntx));                                                                           //Natural: WRITE ( 1 ) #CNTX '=' #P-INVEST-UNITS ( #CNTX )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Display_Input_Bene() throws Exception                                                                                                                //Natural: DISPLAY-INPUT-BENE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Pin_Nbr(),"YYYYYYYY");                                                                                 //Natural: WRITE '=' CIS-PIN-NBR 'YYYYYYYY'
        if (Global.isEscape()) return;
        ldaCisl2020.getCis_Bene_File_01_Cis_Rcrd_Type_Cde().setValue("1");                                                                                                //Natural: ASSIGN CIS-RCRD-TYPE-CDE := '1'
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr().setValue(ltfp0710_Pnd_B_Mdo_Tiaa_Contract);                                                                   //Natural: ASSIGN CIS-BENE-TIAA-NBR := #B-MDO-TIAA-CONTRACT
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Cref_Nbr().setValue(ltfp0710_Pnd_B_Mdo_Cref_Cert);                                                                       //Natural: ASSIGN CIS-BENE-CREF-NBR := #B-MDO-CREF-CERT
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Rqst_Id().setValue("MDO");                                                                                               //Natural: ASSIGN CIS-BENE-RQST-ID := 'MDO'
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Unique_Id_Nbr().setValue(ldaCispartv.getCis_Part_View_Cis_Pin_Nbr());                                                    //Natural: ASSIGN CIS-BENE-UNIQUE-ID-NBR := CIS-PIN-NBR
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Ssn().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Ssn());                                                   //Natural: ASSIGN CIS-BENE-ANNT-SSN := CIS-FRST-ANNT-SSN
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Frst_Nme().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme());                                         //Natural: ASSIGN CIS-BENE-ANNT-FRST-NME := CIS-FRST-ANNT-FRST-NME
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Rqst_Id_Key().setValue(ldaCispartv.getCis_Part_View_Cis_Rqst_Id_Key());                                                  //Natural: ASSIGN CIS-BENE-RQST-ID-KEY := CIS-RQST-ID-KEY
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Std_Free().setValue("N");                                                                                                //Natural: ASSIGN CIS-BENE-STD-FREE := 'N'
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Estate().setValue("N");                                                                                                  //Natural: ASSIGN CIS-BENE-ESTATE := 'N'
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Trust().setValue("N");                                                                                                   //Natural: ASSIGN CIS-BENE-TRUST := 'N'
        ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Category().setValue(ltfp0710_Pnd_B_Bene_Category);                                                                       //Natural: ASSIGN CIS-BENE-CATEGORY := #B-BENE-CATEGORY
        ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn().setValue("N");                                                                                       //Natural: ASSIGN CIS-PRMRY-BENE-CHILD-PRVSN := 'N'
        ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn().setValue("N");                                                                                      //Natural: ASSIGN CIS-CNTGNT-BENE-CHILD-PRVSN := 'N'
        if (condition(ltfp0710_Pnd_B_Bene_Category.equals("P")))                                                                                                          //Natural: IF #B-BENE-CATEGORY EQ 'P'
        {
            pnd_Bene_Cnt_Prmry.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #BENE-CNT-PRMRY
            if (condition(pnd_Bene_Cnt_Prmry.equals(1)))                                                                                                                  //Natural: IF #BENE-CNT-PRMRY = 1
            {
                getReports().write(0, "=",ltfp0710_Pnd_P_Frst_Annt_Calc_Meth,"DDDDDDDDDDD");                                                                              //Natural: WRITE '=' #P-FRST-ANNT-CALC-METH 'DDDDDDDDDDD'
                if (Global.isEscape()) return;
                if (condition(pnd_Lcl_Bene_Calc.equals("Y")))                                                                                                             //Natural: IF #LCL-BENE-CALC EQ 'Y'
                {
                    ldaCisl2020.getCis_Bene_File_01_Cis_Clcltn_Bene_Name().setValue(ltfp0710_Pnd_B_Bene_Name);                                                            //Natural: ASSIGN CIS-CLCLTN-BENE-NAME := #B-BENE-NAME
                    ldaCisl2020.getCis_Bene_File_01_Cis_Clcltn_Bene_Rltnshp_Cde().setValue(pnd_R_Cde);                                                                    //Natural: ASSIGN CIS-CLCLTN-BENE-RLTNSHP-CDE := #R-CDE
                    if (condition(ltfp0710_Pnd_B_Bene_Ssn.greater(getZero())))                                                                                            //Natural: IF #B-BENE-SSN GT 0
                    {
                        ldaCisl2020.getCis_Bene_File_01_Cis_Clcltn_Bene_Ssn_Nbr().setValue(ltfp0710_Pnd_B_Bene_Ssn);                                                      //Natural: ASSIGN CIS-CLCLTN-BENE-SSN-NBR := #B-BENE-SSN
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ltfp0710_Pnd_B_Bene_Birth_Date_N.greater(getZero())))                                                                                   //Natural: IF #B-BENE-BIRTH-DATE-N GT 0
                    {
                        ldaCisl2020.getCis_Bene_File_01_Cis_Clcltn_Bene_Dob().setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_B_Bene_Birth_Date);              //Natural: MOVE EDITED #B-BENE-BIRTH-DATE TO CIS-CLCLTN-BENE-DOB ( EM = YYYYMMDD )
                    }                                                                                                                                                     //Natural: END-IF
                    ldaCisl2020.getCis_Bene_File_01_Cis_Clcltn_Bene_Calc_Method().setValue("R");                                                                          //Natural: ASSIGN CIS-CLCLTN-BENE-CALC-METHOD := 'R'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Std_Txt().getValue(pnd_Bene_Cnt_Prmry).setValue("N");                                                          //Natural: ASSIGN CIS-PRMRY-BENE-STD-TXT ( #BENE-CNT-PRMRY ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Lump_Sum().getValue(pnd_Bene_Cnt_Prmry).setValue("N");                                                         //Natural: ASSIGN CIS-PRMRY-BENE-LUMP-SUM ( #BENE-CNT-PRMRY ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Auto_Cmnt_Val().getValue(pnd_Bene_Cnt_Prmry).setValue("N");                                                    //Natural: ASSIGN CIS-PRMRY-BENE-AUTO-CMNT-VAL ( #BENE-CNT-PRMRY ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Name().getValue(pnd_Bene_Cnt_Prmry).setValue(ltfp0710_Pnd_B_Bene_Name);                                        //Natural: ASSIGN CIS-PRMRY-BENE-NAME ( #BENE-CNT-PRMRY ) := #B-BENE-NAME
                                                                                                                                                                          //Natural: PERFORM BENE-RLTN-CDE
            sub_Bene_Rltn_Cde();
            if (condition(Global.isEscape())) {return;}
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde().getValue(pnd_Bene_Cnt_Prmry).setValue(pnd_R_Cde);                                                   //Natural: ASSIGN CIS-PRMRY-BENE-RLTN-CDE ( #BENE-CNT-PRMRY ) := #R-CDE
            ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn().getValue(pnd_Bene_Cnt_Prmry).setValue(pnd_Rltn_Cde);                                                    //Natural: ASSIGN CIS-PRMRY-BENE-RLTN ( #BENE-CNT-PRMRY ) := #RLTN-CDE
            if (condition(ltfp0710_Pnd_B_Bene_Ssn.greater(getZero())))                                                                                                    //Natural: IF #B-BENE-SSN GT 0
            {
                ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr().getValue(pnd_Bene_Cnt_Prmry).setValue(ltfp0710_Pnd_B_Bene_Ssn);                                  //Natural: ASSIGN CIS-PRMRY-BENE-SSN-NBR ( #BENE-CNT-PRMRY ) := #B-BENE-SSN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_B_Bene_Birth_Date_N.greater(getZero())))                                                                                           //Natural: IF #B-BENE-BIRTH-DATE-N GT 0
            {
                getReports().write(0, "=",ltfp0710_Pnd_B_Bene_Birth_Date);                                                                                                //Natural: WRITE '=' #B-BENE-BIRTH-DATE
                if (Global.isEscape()) return;
                ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Dob().getValue(pnd_Bene_Cnt_Prmry).setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_B_Bene_Birth_Date); //Natural: MOVE EDITED #B-BENE-BIRTH-DATE TO CIS-PRMRY-BENE-DOB ( #BENE-CNT-PRMRY ) ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            //*  RESET #ALLOC #BENE-ALLOC
            pnd_Bene_Alloc.reset();                                                                                                                                       //Natural: RESET #BENE-ALLOC
            pnd_Bene_Alloc.compute(new ComputeParameters(false, pnd_Bene_Alloc), ltfp0710_Pnd_B_Bene_Allocation_Pct.multiply(100));                                       //Natural: ASSIGN #BENE-ALLOC := #B-BENE-ALLOCATION-PCT * 100
            if (condition(pnd_Bene_Alloc.greater(getZero())))                                                                                                             //Natural: IF #BENE-ALLOC GT 0
            {
                ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Allctn_Pct().getValue(pnd_Bene_Cnt_Prmry).setValue(pnd_Bene_Alloc);                                        //Natural: ASSIGN CIS-PRMRY-BENE-ALLCTN-PCT ( #BENE-CNT-PRMRY ) := #BENE-ALLOC
                ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(pnd_Bene_Cnt_Prmry).setValue(pnd_Bene_Alloc);                                         //Natural: ASSIGN CIS-PRMRY-BENE-NMRTR-NBR ( #BENE-CNT-PRMRY ) := #BENE-ALLOC
                ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_Bene_Cnt_Prmry).setValue(100);                                                   //Natural: ASSIGN CIS-PRMRY-BENE-DNMNTR-NBR ( #BENE-CNT-PRMRY ) := 100
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ltfp0710_Pnd_B_Bene_Category.equals("C")))                                                                                                          //Natural: IF #B-BENE-CATEGORY EQ 'C'
        {
            pnd_Bene_Cnt_Cntgnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #BENE-CNT-CNTGNT
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Std_Txt().getValue(pnd_Bene_Cnt_Cntgnt).setValue("N");                                                        //Natural: ASSIGN CIS-CNTGNT-BENE-STD-TXT ( #BENE-CNT-CNTGNT ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Lump_Sum().getValue(pnd_Bene_Cnt_Cntgnt).setValue("N");                                                       //Natural: ASSIGN CIS-CNTGNT-BENE-LUMP-SUM ( #BENE-CNT-CNTGNT ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Auto_Cmnt_Val().getValue(pnd_Bene_Cnt_Cntgnt).setValue("N");                                                  //Natural: ASSIGN CIS-CNTGNT-BENE-AUTO-CMNT-VAL ( #BENE-CNT-CNTGNT ) := 'N'
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Name().getValue(pnd_Bene_Cnt_Cntgnt).setValue(ltfp0710_Pnd_B_Bene_Name);                                      //Natural: ASSIGN CIS-CNTGNT-BENE-NAME ( #BENE-CNT-CNTGNT ) := #B-BENE-NAME
                                                                                                                                                                          //Natural: PERFORM BENE-RLTN-CDE
            sub_Bene_Rltn_Cde();
            if (condition(Global.isEscape())) {return;}
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde().getValue(pnd_Bene_Cnt_Cntgnt).setValue(pnd_R_Cde);                                                 //Natural: ASSIGN CIS-CNTGNT-BENE-RLTN-CDE ( #BENE-CNT-CNTGNT ) := #R-CDE
            ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn().getValue(pnd_Bene_Cnt_Cntgnt).setValue(pnd_Rltn_Cde);                                                  //Natural: ASSIGN CIS-CNTGNT-BENE-RLTN ( #BENE-CNT-CNTGNT ) := #RLTN-CDE
            if (condition(ltfp0710_Pnd_B_Bene_Ssn.greater(getZero())))                                                                                                    //Natural: IF #B-BENE-SSN GT 0
            {
                ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr().getValue(pnd_Bene_Cnt_Cntgnt).setValue(ltfp0710_Pnd_B_Bene_Ssn);                                //Natural: ASSIGN CIS-CNTGNT-BENE-SSN-NBR ( #BENE-CNT-CNTGNT ) := #B-BENE-SSN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ltfp0710_Pnd_B_Bene_Birth_Date_N.greater(getZero())))                                                                                           //Natural: IF #B-BENE-BIRTH-DATE-N GT 0
            {
                ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dob().getValue(pnd_Bene_Cnt_Cntgnt).setValueEdited(new ReportEditMask("YYYYMMDD"),ltfp0710_Pnd_B_Bene_Birth_Date); //Natural: MOVE EDITED #B-BENE-BIRTH-DATE TO CIS-CNTGNT-BENE-DOB ( #BENE-CNT-CNTGNT ) ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Bene_Alloc.compute(new ComputeParameters(false, pnd_Bene_Alloc), ltfp0710_Pnd_B_Bene_Allocation_Pct.multiply(100));                                       //Natural: ASSIGN #BENE-ALLOC := #B-BENE-ALLOCATION-PCT *100
            if (condition(pnd_Bene_Alloc.greater(getZero())))                                                                                                             //Natural: IF #BENE-ALLOC GT 0
            {
                ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Allctn_Pct().getValue(pnd_Bene_Cnt_Cntgnt).setValue(pnd_Bene_Alloc);                                      //Natural: ASSIGN CIS-CNTGNT-BENE-ALLCTN-PCT ( #BENE-CNT-CNTGNT ) := #BENE-ALLOC
                ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(pnd_Bene_Cnt_Cntgnt).setValue(pnd_Bene_Alloc);                                       //Natural: ASSIGN CIS-CNTGNT-BENE-NMRTR-NBR ( #BENE-CNT-CNTGNT ) := #BENE-ALLOC
                ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_Bene_Cnt_Cntgnt).setValue(100);                                                 //Natural: ASSIGN CIS-CNTGNT-BENE-DNMNTR-NBR ( #BENE-CNT-CNTGNT ) := 100
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().eject(0, true);                                                                                                                                      //Natural: EJECT
        getReports().write(0, "BENE INFORMATION");                                                                                                                        //Natural: WRITE 'BENE INFORMATION'
        if (Global.isEscape()) return;
    }
    private void sub_Store_Bene() throws Exception                                                                                                                        //Natural: STORE-BENE
    {
        if (BLNatReinput.isReinput()) return;

        //*  BIP - SUPPRESS BENE ADD
        if (condition(ltfp0710_Pnd_Decedent_Contract.greater(" ")))                                                                                                       //Natural: IF #DECEDENT-CONTRACT GT ' '
        {
            //*  BIP - SUPPRESS BENE ADD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  BIP - SUPPRESS BENE ADD
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "=",pnd_Bene_Cnt_Prmry,"=",pnd_Bene_Cnt_Cntgnt,"FROM STORE");                                                                               //Natural: WRITE '=' #BENE-CNT-PRMRY '=' #BENE-CNT-CNTGNT 'FROM STORE'
        if (Global.isEscape()) return;
        if (condition(pnd_Bene_Cnt_Prmry.greater(getZero()) || pnd_Bene_Cnt_Cntgnt.greater(getZero())))                                                                   //Natural: IF #BENE-CNT-PRMRY GT 0 OR #BENE-CNT-CNTGNT GT 0
        {
            if (condition(! (pnd_Found.getBoolean())))                                                                                                                    //Natural: IF NOT #FOUND
            {
                getReports().write(1, "=",ltfp0710_Pnd_B_Mdo_Letter_Type);                                                                                                //Natural: WRITE ( 1 ) '=' #B-MDO-LETTER-TYPE
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Omni_Plan_Id);                                                                                                   //Natural: WRITE ( 1 ) '=' #B-OMNI-PLAN-ID
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Omni_Sub_Plan_Id);                                                                                               //Natural: WRITE ( 1 ) '=' #B-OMNI-SUB-PLAN-ID
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Pin);                                                                                                            //Natural: WRITE ( 1 ) '=' #B-PIN
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Mdo_Tiaa_Contract);                                                                                              //Natural: WRITE ( 1 ) '=' #B-MDO-TIAA-CONTRACT
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Mdo_Cref_Cert);                                                                                                  //Natural: WRITE ( 1 ) '=' #B-MDO-CREF-CERT
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Omni_Plan_Name);                                                                                                 //Natural: WRITE ( 1 ) '=' #B-OMNI-PLAN-NAME
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Participant_Name);                                                                                               //Natural: WRITE ( 1 ) '=' #B-PARTICIPANT-NAME
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Bene_Category);                                                                                                  //Natural: WRITE ( 1 ) '=' #B-BENE-CATEGORY
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Bene_Name);                                                                                                      //Natural: WRITE ( 1 ) '=' #B-BENE-NAME
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Bene_Birth_Date);                                                                                                //Natural: WRITE ( 1 ) '=' #B-BENE-BIRTH-DATE
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Bene_Ssn);                                                                                                       //Natural: WRITE ( 1 ) '=' #B-BENE-SSN
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Bene_Relationship);                                                                                              //Natural: WRITE ( 1 ) '=' #B-BENE-RELATIONSHIP
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Bene_Allocation_Pct);                                                                                            //Natural: WRITE ( 1 ) '=' #B-BENE-ALLOCATION-PCT
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Prim_Child_Prov);                                                                                                //Natural: WRITE ( 1 ) '=' #B-PRIM-CHILD-PROV
                if (Global.isEscape()) return;
                getReports().write(1, "=",ltfp0710_Pnd_B_Cont_Child_Prov);                                                                                                //Natural: WRITE ( 1 ) '=' #B-CONT-CHILD-PROV
                if (Global.isEscape()) return;
                getReports().write(0, "=",NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Rcrd_Type_Cde(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr(), //Natural: WRITE '=' /'=' CIS-RCRD-TYPE-CDE /'=' CIS-BENE-TIAA-NBR /'=' CIS-BENE-CREF-NBR /'=' CIS-BENE-RQST-ID /'=' CIS-BENE-UNIQUE-ID-NBR /'=' CIS-BENE-ANNT-SSN /'=' CIS-BENE-ANNT-PRFX /'=' CIS-BENE-ANNT-FRST-NME /'=' CIS-BENE-ANNT-MID-NME /'=' CIS-BENE-ANNT-LST-NME /'=' CIS-BENE-ANNT-SFFX /'=' CIS-BENE-RQST-ID-KEY /'=' CIS-BENE-STD-FREE /'=' CIS-BENE-ESTATE /'=' CIS-BENE-TRUST /'=' CIS-BENE-CATEGORY /'=' CIS-PRMRY-BENE-CHILD-PRVSN /'=' CIS-CNTGNT-BENE-CHILD-PRVSN / '=' CIS-PRMRY-BENE-NAME ( 1:2 )
                    NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Cref_Nbr(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Rqst_Id(),NEWLINE,
                    "=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Unique_Id_Nbr(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Ssn(),NEWLINE,
                    "=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Prfx(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Frst_Nme(),NEWLINE,
                    "=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Mid_Nme(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Lst_Nme(),NEWLINE,
                    "=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Annt_Sffx(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Rqst_Id_Key(),NEWLINE,
                    "=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Std_Free(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Estate(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Trust(),
                    NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Bene_Category(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn(),
                    NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn(),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Name().getValue(1,
                    ":",2));
                if (Global.isEscape()) return;
                getReports().write(0, NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Extndd_Nme().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn().getValue(1, //Natural: WRITE / '=' CIS-PRMRY-BENE-EXTNDD-NME ( 1:2 ) / '=' CIS-PRMRY-BENE-RLTN ( 1:2 ) / '=' CIS-PRMRY-BENE-RLTN-CDE ( 1:2 ) / '=' CIS-PRMRY-BENE-SSN-NBR ( 1:2 ) / '=' CIS-PRMRY-BENE-DOB ( 1:2 ) / '=' CIS-PRMRY-BENE-ALLCTN-PCT ( 1:2 ) / '=' CIS-PRMRY-BENE-NMRTR-NBR ( 1:2 ) / '=' CIS-PRMRY-BENE-DNMNTR-NBR ( 1:2 ) / '=' CIS-CNTGNT-BENE-NAME ( 1:2 ) / '=' CIS-CNTGNT-BENE-EXTNDD-NME ( 1:2 ) / '=' CIS-CNTGNT-BENE-RLTN ( 1:2 ) / '=' CIS-CNTGNT-BENE-RLTN-CDE ( 1:2 ) / '=' CIS-CNTGNT-BENE-SSN-NBR ( 1:2 ) / '=' CIS-CNTGNT-BENE-DOB ( 1:2 ) / '=' CIS-CNTGNT-BENE-ALLCTN-PCT ( 1:2 ) / '=' CIS-CNTGNT-BENE-NMRTR-NBR ( 1:2 ) / '=' CIS-CNTGNT-BENE-DNMNTR-NBR ( 1:2 )
                    ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr().getValue(1,
                    ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Dob().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Allctn_Pct().getValue(1,
                    ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(1,
                    ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Name().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Extndd_Nme().getValue(1,
                    ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde().getValue(1,
                    ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dob().getValue(1,
                    ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Allctn_Pct().getValue(1,":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(1,
                    ":",2),NEWLINE,"=",ldaCisl2020.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(1,":",2));
                if (Global.isEscape()) return;
                ldaCisl2020.getVw_cis_Bene_File_01().insertDBRow();                                                                                                       //Natural: STORE CIS-BENE-FILE-01
                //*  09/2011 B.HOLLOWAY START - BACKOUT IF TESTING
                if (condition(pnd_Debug.getBoolean()))                                                                                                                    //Natural: IF #DEBUG
                {
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                }                                                                                                                                                         //Natural: END-IF
                //*  09/2011 B.HOLLOWAY END
            }                                                                                                                                                             //Natural: END-IF
            ldaCisl2020.getVw_cis_Bene_File_01().reset();                                                                                                                 //Natural: RESET CIS-BENE-FILE-01
            pnd_Bene_Cnt_Prmry.reset();                                                                                                                                   //Natural: RESET #BENE-CNT-PRMRY #BENE-CNT-CNTGNT
            pnd_Bene_Cnt_Cntgnt.reset();
            getReports().write(0, "STORE DONE ***************");                                                                                                          //Natural: WRITE 'STORE DONE ***************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Bene_Rltn_Cde() throws Exception                                                                                                                     //Natural: BENE-RLTN-CDE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_R_Cde.reset();                                                                                                                                                //Natural: RESET #R-CDE #RLTN-CDE
        pnd_Rltn_Cde.reset();
        //* CREA
        //* CREA
        DbsUtil.examine(new ExamineSource(pnd_Bene_Relationship_Pnd_Bene_Relationship_Type.getValue("*")), new ExamineSearch(ltfp0710_Pnd_B_Bene_Relationship),           //Natural: EXAMINE #BENE-RELATIONSHIP-TYPE ( * ) #B-BENE-RELATIONSHIP GIVING INDEX #BENE-IDX
            new ExamineGivingIndex(pnd_Bene_Idx));
        //*                                                                  /*CREA
        //* CREA
        //* CREA
        //* CREA
        if (condition(pnd_Bene_Idx.greater(getZero())))                                                                                                                   //Natural: IF #BENE-IDX > 0
        {
            pnd_Rltn_Cde.setValue(pnd_Bene_Relationship_Pnd_Bene_Relationship_Name.getValue(pnd_Bene_Idx));                                                               //Natural: ASSIGN #RLTN-CDE := #BENE-RELATIONSHIP-NAME ( #BENE-IDX )
            pnd_R_Cde.setValue(pnd_Bene_Relationship_Pnd_Bene_Relationship_Code.getValue(pnd_Bene_Idx));                                                                  //Natural: ASSIGN #R-CDE := #BENE-RELATIONSHIP-CODE ( #BENE-IDX )
            //* CREA
            //* CREA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rltn_Cde.setValue("OTHER");                                                                                                                               //Natural: ASSIGN #RLTN-CDE := 'OTHER'
            //* CREA
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  PINE
    private void sub_Generate_Cis_Report_Duplicate() throws Exception                                                                                                     //Natural: GENERATE-CIS-REPORT-DUPLICATE
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(2, "Plan Id",                                                                                                                                //Natural: DISPLAY ( 2 ) 'Plan Id' #P-OMNI-PLAN-ID 'Sub Plan' #P-OMNI-SUB-PLAN 'TIAA Cntrct' #P-MDO-TIAA-CONTRACT 'CREF Cntrct' #P-MDO-CREF-CERT 'PIN NBR' #P-PIN 'SSN' #P-FRST-ANNT-SSN 'Last Name' #P-FRST-ANNT-LST-NME ( AL = 10 ) 'Dt of Iss' #P-ANNTY-STRT-DTE 'Annty OPT' #P-ANNTY-OPTION
        		ltfp0710_Pnd_P_Omni_Plan_Id,"Sub Plan",
        		ltfp0710_Pnd_P_Omni_Sub_Plan,"TIAA Cntrct",
        		ltfp0710_Pnd_P_Mdo_Tiaa_Contract,"CREF Cntrct",
        		ltfp0710_Pnd_P_Mdo_Cref_Cert,"PIN NBR",
        		ltfp0710_Pnd_P_Pin,"SSN",
        		ltfp0710_Pnd_P_Frst_Annt_Ssn,"Last Name",
        		ltfp0710_Pnd_P_Frst_Annt_Lst_Nme, new AlphanumericLength (10),"Dt of Iss",
        		ltfp0710_Pnd_P_Annty_Strt_Dte,"Annty OPT",
        		ltfp0710_Pnd_P_Annty_Option);
        if (Global.isEscape()) return;
    }
    //*  PINE
    private void sub_Participant_Cis_Added() throws Exception                                                                                                             //Natural: PARTICIPANT-CIS-ADDED
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(3, "Plan Id",                                                                                                                                //Natural: DISPLAY ( 3 ) 'Plan Id' #P-OMNI-PLAN-ID 'Sub Plan' #P-OMNI-SUB-PLAN 'TIAA Cntrct' #P-MDO-TIAA-CONTRACT 'CREF Cntrct' #P-MDO-CREF-CERT 'PIN NBR' #P-PIN 'SSN' #P-FRST-ANNT-SSN 'Last Name' #P-FRST-ANNT-LST-NME ( AL = 10 ) 'Dt of Iss' #P-ANNTY-STRT-DTE 'Annty OPT' #P-ANNTY-OPTION
        		ltfp0710_Pnd_P_Omni_Plan_Id,"Sub Plan",
        		ltfp0710_Pnd_P_Omni_Sub_Plan,"TIAA Cntrct",
        		ltfp0710_Pnd_P_Mdo_Tiaa_Contract,"CREF Cntrct",
        		ltfp0710_Pnd_P_Mdo_Cref_Cert,"PIN NBR",
        		ltfp0710_Pnd_P_Pin,"SSN",
        		ltfp0710_Pnd_P_Frst_Annt_Ssn,"Last Name",
        		ltfp0710_Pnd_P_Frst_Annt_Lst_Nme, new AlphanumericLength (10),"Dt of Iss",
        		ltfp0710_Pnd_P_Annty_Strt_Dte,"Annty OPT",
        		ltfp0710_Pnd_P_Annty_Option);
        if (Global.isEscape()) return;
    }
    private void sub_Reformat_Address() throws Exception                                                                                                                  //Natural: REFORMAT-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,1).setValue(pnd_Hold_Address_Pnd_Hdest_Addr1);                                              //Natural: ASSIGN CIS-ADDRESS-TXT ( #ADDR-INDX,1 ) := #HDEST-ADDR1
        ldaCispartv.getCis_Part_View_Cis_Zip_Code().getValue(pnd_Addr_Indx).setValue(pnd_Hold_Address_Pnd_Hzip);                                                          //Natural: ASSIGN CIS-ZIP-CODE ( #ADDR-INDX ) := #HZIP
        if (condition(pnd_Hold_Address_Pnd_Hzip.equals(pnd_Foreign_Or_Canada.getValue("*"))))                                                                             //Natural: IF #HZIP = #FOREIGN-OR-CANADA ( * )
        {
            ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,2).setValue(pnd_Hold_Address_Pnd_Hdest_Addr2);                                          //Natural: ASSIGN CIS-ADDRESS-TXT ( #ADDR-INDX,2 ) := #HDEST-ADDR2
            ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,3).setValue(pnd_Hold_Address_Pnd_Hdest_Addr3);                                          //Natural: ASSIGN CIS-ADDRESS-TXT ( #ADDR-INDX,3 ) := #HDEST-ADDR3
            //*  DON'T MOVE CITY AND STATE TO ADDR LINE FOR FORGN OR CANAD /* RAC BE1.
            //*  COMPRESS #HDEST-CITY ' ' #HDEST-STATE
            //*    INTO CIS-ADDRESS-TXT(#ADDR-INDX,4)
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Hold_Address_Pnd_Hdest_Addr2.equals(" ")))                                                                                                      //Natural: IF #HDEST-ADDR2 = ' '
        {
            ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,2).setValue(DbsUtil.compress(pnd_Hold_Address_Pnd_Hdest_City, " ", pnd_Hold_Address_Pnd_Hdest_State)); //Natural: COMPRESS #HDEST-CITY ' ' #HDEST-STATE INTO CIS-ADDRESS-TXT ( #ADDR-INDX,2 )
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,2).setValue(pnd_Hold_Address_Pnd_Hdest_Addr2);                                              //Natural: ASSIGN CIS-ADDRESS-TXT ( #ADDR-INDX,2 ) := #HDEST-ADDR2
        if (condition(pnd_Hold_Address_Pnd_Hdest_Addr3.equals(" ")))                                                                                                      //Natural: IF #HDEST-ADDR3 = ' '
        {
            ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,3).setValue(DbsUtil.compress(pnd_Hold_Address_Pnd_Hdest_City, " ", pnd_Hold_Address_Pnd_Hdest_State)); //Natural: COMPRESS #HDEST-CITY ' ' #HDEST-STATE INTO CIS-ADDRESS-TXT ( #ADDR-INDX,3 )
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,3).setValue(pnd_Hold_Address_Pnd_Hdest_Addr3);                                              //Natural: ASSIGN CIS-ADDRESS-TXT ( #ADDR-INDX,3 ) := #HDEST-ADDR3
        ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(pnd_Addr_Indx,4).setValue(DbsUtil.compress(pnd_Hold_Address_Pnd_Hdest_City, " ", pnd_Hold_Address_Pnd_Hdest_State)); //Natural: COMPRESS #HDEST-CITY ' ' #HDEST-STATE INTO CIS-ADDRESS-TXT ( #ADDR-INDX,4 )
    }
    //*  PINE
    //*  KC-02
    private void sub_Generate_Error_Report() throws Exception                                                                                                             //Natural: GENERATE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(4, "Plan Id",                                                                                                                                //Natural: DISPLAY ( 4 ) 'Plan Id' #P-OMNI-PLAN-ID 'Sub Plan' #P-OMNI-SUB-PLAN 'TIAA Cntrct' #P-MDO-TIAA-CONTRACT 'CREF Cntrct' #P-MDO-CREF-CERT 'PIN NBR' #P-PIN 'SSN' #P-FRST-ANNT-SSN 'Dt of Iss' #P-ANNTY-STRT-DTE 'First Name' #P-FRST-ANNT-FRST-NME ( AL = 10 ) 'Last Name' #P-FRST-ANNT-LST-NME ( AL = 10 ) 'ERR Msg' #ERR-MESSAGE ( AL = 20 ) 'Annty OPT' #P-ANNTY-OPTION
        		ltfp0710_Pnd_P_Omni_Plan_Id,"Sub Plan",
        		ltfp0710_Pnd_P_Omni_Sub_Plan,"TIAA Cntrct",
        		ltfp0710_Pnd_P_Mdo_Tiaa_Contract,"CREF Cntrct",
        		ltfp0710_Pnd_P_Mdo_Cref_Cert,"PIN NBR",
        		ltfp0710_Pnd_P_Pin,"SSN",
        		ltfp0710_Pnd_P_Frst_Annt_Ssn,"Dt of Iss",
        		ltfp0710_Pnd_P_Annty_Strt_Dte,"First Name",
        		ltfp0710_Pnd_P_Frst_Annt_Frst_Nme, new AlphanumericLength (10),"Last Name",
        		ltfp0710_Pnd_P_Frst_Annt_Lst_Nme, new AlphanumericLength (10),"ERR Msg",
        		pnd_Err_Message, new AlphanumericLength (20),"Annty OPT",
        		ltfp0710_Pnd_P_Annty_Option);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(100),"Page No :",new TabSetting(110),getReports().getPageNumberDbs(2),  //Natural: WRITE ( 2 ) 001T *PROGRAM 100T 'Page No :' 110T *PAGE-NUMBER ( 2 ) ( AD = OD CD = NE ZP = ON ) / 001T *DATU '-' *TIMX ( EM = HH:IIAP )
                        new FieldAttributes ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,new TabSetting(1),Global.getDATU(),"-",Global.getTIMX(), 
                        new ReportEditMask ("HH:IIAP"));
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(42),"DUPLICATE MDO CONTRACTS ON CIS");                                                      //Natural: WRITE ( 2 ) NOTITLE 42T 'DUPLICATE MDO CONTRACTS ON CIS'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(100),"Page No :",new TabSetting(110),getReports().getPageNumberDbs(3),  //Natural: WRITE ( 3 ) 001T *PROGRAM 100T 'Page No :' 110T *PAGE-NUMBER ( 3 ) ( AD = OD CD = NE ZP = ON ) / 001T *DATU '-' *TIMX ( EM = HH:IIAP )
                        new FieldAttributes ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,new TabSetting(1),Global.getDATU(),"-",Global.getTIMX(), 
                        new ReportEditMask ("HH:IIAP"));
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(42),"MDO PARTICIPANT ADD ON CIS");                                                          //Natural: WRITE ( 3 ) NOTITLE 42T 'MDO PARTICIPANT ADD ON CIS'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(100),"Page No :",new TabSetting(110),getReports().getPageNumberDbs(4),  //Natural: WRITE ( 4 ) 001T *PROGRAM 100T 'Page No :' 110T *PAGE-NUMBER ( 4 ) ( AD = OD CD = NE ZP = ON ) / 001T *DATU '-' *TIMX ( EM = HH:IIAP )
                        new FieldAttributes ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,new TabSetting(1),Global.getDATU(),"-",Global.getTIMX(), 
                        new ReportEditMask ("HH:IIAP"));
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(42),"ERROR KEY OMNI INFOR MISSING FOR MDO");                                                //Natural: WRITE ( 4 ) NOTITLE 42T 'ERROR KEY OMNI INFOR MISSING FOR MDO'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        //*  CREA
        //*  CREA
        //*  CREA
        //*  CREA
        //*  CREA
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"ERROR:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"LINE:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'ERROR:' 25T *ERROR-NR / 'LINE:' 25T *ERROR-LINE / 'MDO TIAA:' 25T #P-MDO-TIAA-CONTRACT / 'MDO CREF:' 25T #P-MDO-CREF-CERT
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,"MDO TIAA:",new TabSetting(25),ltfp0710_Pnd_P_Mdo_Tiaa_Contract,NEWLINE,"MDO CREF:",new TabSetting(25),
            ltfp0710_Pnd_P_Mdo_Cref_Cert);
        //*  CREA
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132 ZP=OFF SG=OFF");
        Global.format(2, "PS=60 LS=132 ZP=OFF SG=OFF");
        Global.format(3, "PS=60 LS=132 ZP=OFF SG=OFF");
        Global.format(4, "PS=60 LS=132 ZP=OFF SG=OFF");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),Global.getPROGRAM(),"-",Global.getINIT_USER(),new 
            TabSetting(45),"MDO WELCOME PACKAGE ",new TabSetting(100),"Page No :",new TabSetting(110),getReports().getPageNumberDbs(1), new FieldAttributes 
            ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,new TabSetting(1),Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(45),"  OMNI Rejection Report",NEWLINE,new TabSetting(1)," Plan     Sub Plan          Pin              Name           ",new TabSetting(62),"                      TIAA #    CREF #",NEWLINE,new 
            TabSetting(1),"------ ----------------- -------------",new TabSetting(40),"-----------------------------------------",new TabSetting(82),"---------- ----------");

        getReports().setDisplayColumns(2, "Plan Id",
        		ltfp0710_Pnd_P_Omni_Plan_Id,"Sub Plan",
        		ltfp0710_Pnd_P_Omni_Sub_Plan,"TIAA Cntrct",
        		ltfp0710_Pnd_P_Mdo_Tiaa_Contract,"CREF Cntrct",
        		ltfp0710_Pnd_P_Mdo_Cref_Cert,"PIN NBR",
        		ltfp0710_Pnd_P_Pin,"SSN",
        		ltfp0710_Pnd_P_Frst_Annt_Ssn,"Last Name",
        		ltfp0710_Pnd_P_Frst_Annt_Lst_Nme, new AlphanumericLength (10),"Dt of Iss",
        		ltfp0710_Pnd_P_Annty_Strt_Dte,"Annty OPT",
        		ltfp0710_Pnd_P_Annty_Option);
        getReports().setDisplayColumns(3, "Plan Id",
        		ltfp0710_Pnd_P_Omni_Plan_Id,"Sub Plan",
        		ltfp0710_Pnd_P_Omni_Sub_Plan,"TIAA Cntrct",
        		ltfp0710_Pnd_P_Mdo_Tiaa_Contract,"CREF Cntrct",
        		ltfp0710_Pnd_P_Mdo_Cref_Cert,"PIN NBR",
        		ltfp0710_Pnd_P_Pin,"SSN",
        		ltfp0710_Pnd_P_Frst_Annt_Ssn,"Last Name",
        		ltfp0710_Pnd_P_Frst_Annt_Lst_Nme, new AlphanumericLength (10),"Dt of Iss",
        		ltfp0710_Pnd_P_Annty_Strt_Dte,"Annty OPT",
        		ltfp0710_Pnd_P_Annty_Option);
        getReports().setDisplayColumns(4, "Plan Id",
        		ltfp0710_Pnd_P_Omni_Plan_Id,"Sub Plan",
        		ltfp0710_Pnd_P_Omni_Sub_Plan,"TIAA Cntrct",
        		ltfp0710_Pnd_P_Mdo_Tiaa_Contract,"CREF Cntrct",
        		ltfp0710_Pnd_P_Mdo_Cref_Cert,"PIN NBR",
        		ltfp0710_Pnd_P_Pin,"SSN",
        		ltfp0710_Pnd_P_Frst_Annt_Ssn,"Dt of Iss",
        		ltfp0710_Pnd_P_Annty_Strt_Dte,"First Name",
        		ltfp0710_Pnd_P_Frst_Annt_Frst_Nme, new AlphanumericLength (10),"Last Name",
        		ltfp0710_Pnd_P_Frst_Annt_Lst_Nme, new AlphanumericLength (10),"ERR Msg",
        		pnd_Err_Message, new AlphanumericLength (20),"Annty OPT",
        		ltfp0710_Pnd_P_Annty_Option);
    }
}
