/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:21:28 PM
**        * FROM NATURAL PROGRAM : Cisb600
************************************************************
**        * FILE NAME            : Cisb600.java
**        * CLASS NAME           : Cisb600
**        * INSTANCE NAME        : Cisb600
************************************************************
**====================================================================**
* SYSTEM      : CIS SYSTEM                                             *
* AUTHOR      : TED LOSCHIAVO                                          *
* DESCRIPTION : THIS PROGRAM WILL READ TWO ADABAS FILES AND WRITE      *
*             : TO A WORK FILE.                                        *
* PRIME KEY   : PHYSICAL READ ON FIRST READ LOGICAL ON THE SECOND      *
*             : READ BY KEY                                            *
* INPUT FILE  : FILE 120 DATA BASE 45                                  *
* OUTPUT FILE : WORK FILE                                              *
* PROGRAM     : CISB600                                                *
* GENERATED   :                                                        *
* FUNCTION    : THIS PROGRAM READS ALL PARTICIPANT RECORDS ON          *
*             : CIS-REPRT-FILE AND GOES OUT TO THE PARTICIPANT         *
*             : FILE AND MOVES THE INFORMATION TO AN EXTRACT FILE      *
*             : WHICH IS A WORK FILE.                                  *
**--------------------------------------------------------------------**
**                     M O D I F I C A T I O N S                      **
**--------------------------------------------------------------------**
*    DATE   |    MOD BY   |             DESC OF CHANGE
**--------------------------------------------------------------------**
* 10/26/2001  C. SINGLETON  ADD SUBPROGRAM CISN601 TO CALL BENN970     *
*                           (BENE INTERFACE) ALSO ADDED 2 REPORTS FOR  *
*                           IA TRANSFERS.                              *
* 04/04/2006  H.KAKADIA     BYPASS ICAP CASE                           *
* 06/14/2006  H.KAKADIA     ADDED NEW FILED FOR TICKER TO GET THE NAME *
*                           OF FUND FFROM EXTERNALIZATION              *
* 06/07/06    O SOTTO       CHANGED AS PART OF MDO LEGAL SPLIT.        *
*                           MASK CHECK FOR MDO. SC 060706.             *
* 09/25/2006  J BERGHEISER  ADD EFFECTIVE DATE LOGIC FOR CA PRINTING   *
*                           'NONE' FOR REAL ESTATE (LT 20061118) /* JB1*
* 07/31/2007  O. SOTTO      NEW ANNUITY (75%) OPTION CHANGES. SC 073107*
* 02/21/2008  DEVELBISS     WRITE POSITION 1 OF CIS-SG-TEXT-UDF-1      *
*                           TO THE #SPEC-CNTRCT-TYPE AND POSITIONS 2-9 *
*                           TO #ORIG-ISSUE-DATE. (ATRA) /* BD1         *
* 03/30/2009  BERGHEISER    INC653890 ADD LOGIC TO PARTICIPANT READ TO *
*             (JRB2)        MAKE SURE CONTRACT MATCHES REPRINT CONTRACT*
* 03/2009     K. GATES      CHANGES FOR IA TIAA ACCESS PROJECT         *
*                           NOTE - THIS PGM WILL REQUIRE CHANGES IF    *
*                           TIAA ACCESS PACKAGE STATES ARE APPROVED    *
*                           ADDED TIAA ACCESS FUND LOGIC /* KG TACC    *
*                           ADDED TRANSFER LOGIC         /* KG TRAN    *
* 06/2009     K. GATES      CHANGES FOR IA TIAA ACCESS PROJECT ADDED   *
*                           TIAA IA ATRA LOGIC /* KG ATRA              *
* 06/2009     K. GATES      CHANGES FOR ROTH PROJECT                   *
*                           ADDED ROTH INDICATOR LOGIC /* KG ROTH      *
* 09/26/2009  J BERGHEISER  IA ACCESS FOR ADDITIONAL APPROVED STATES   *
*                           FOR 12/01/09 MODIFY HEADER LOGIC /* JRB3   *
* 02/12/2010  DEVELBISS     UPDATE #GRA-SURR-RIGHT TO 'M' IF ORIGINAL  *
*                           CONTRACT IS A GRA MDO OR A GRA SIP /* BJD1 *
* 03/05/2010  C. MASON      ADD 457 INDICATOR AND INST. NAME & TRANSFER*
*                           INFO TO EXTRACT                            *
* 10/25/2010  G GUERRERO    CONFIRMS 2B PROJECT SEE - 'GGG 10/10'      *
* 10/25/2010  G GUERRERO    CHANGED UNIT SHARES FROM 13.4 TO 7.4 FOR   *
*                           BOTH 'from' & 'to' SHARE AMOUNTS ON THE    *
*                           MDO/SIP LETTERS - SEE 'GGG-12/10           *
* 12/21/2010  G GUERRERO    ADDED NUMERIC CHECK FOR NEWL DATE FIELD    *
*                           CIS-INVEST-PROCESS-DT ON PARTICIPANT FILE  *
*                           FOR OLD CONTRACTS THAT MAY NOT HAVE THIS   *
*                           FIELD POPULATED.                           *
* 11/13/2012  P GOLDEN      MODIFIED CODE THAT POPULATES GUARANTEED    *
*                           DATE FOR CREF EQUITES AND POST             *
*                           SETTLEMENT TRANSFERS. THIS IS DUE TO THE   *
*                           FACT THAT THE INCORRECT CONTRACT TYPE      *
*                           WAS BEING CHECKED....SCAN FOR PG1          *
* 06/20/2014  L SHU         FOR IA, TO RETRIEVE FUND NAME WHEN BOTH    *
*                           CIS-SG-FUND-TICKER AND CIS-CREF-ACCT-CDE   *
*                           ARE POPULATED                              *
*                           USE REFERENCE CODE 18 TO GET THE FUND NAME *
*                           FOR IA FROM NEW EXTERNALISATION ... CREA   *
*                                                                      *
* 12/2014 W. BAUER          CREF REDESIGN CHANGES. (ACCRC)             *
*                           MDO CONTRACT CHANGES.                      *
*                           1.ADD THE PLAN NUMBER AND SET THE MULTI    *
*                             PLAN IND TO AN 'N' FOR SINGLE PLAN       *
*                             CONTRACTS ON OUTPUT LAYOUT CISLPART.     *
*                           2.ADD THE MULTIPLE PLANS AND SUBPLANS      *
*                             THAT A CONTRACT RESIDES IN AND SET THE   *
*                             MULTI PLAN IND TO A 'Y' FOR MULTI PLAN   *
*                             CONTRACTS ON OUTPUT LAYOUT CISLPART.     *
*                           IA CONTRACT CHANGES.                       *
*                           1.ADD CALL TO ADSN888 TO RETRIEVE SHARE    *
*                             CLASS. SET THE MULTI PLAN IND TO AN 'N'  *
*                             AND MOVE THE SHARE CLASS TO OUTPUT       *
*                             LAYOUT CISLPART.                         *
* 05/2015  B. NEWSOM        REMOVE THE SHARE CLASS FOR FUND LIST.      *
*                                                           (20150511) *
* 09/2015 B. ELLO         - EXCLUDE/SUPPRESS BENE IN THE PLAN ACCOUNTS *
*                           FOR PKG EXTRACT AND UPDATE THE RECORD WITH *
*                           CIS-OPN-CLSD-IND='C' & CIS-STATUS-CD='P'.  *
*                         - RESTOW FOR CISPARTV FOR BENE IN THE PLAN   *
*                           PROJECT. (BIP)                             *
* 12/12/2015 - BUDDY NEWSOM - INSTITUTIONAL PREMIUM FILE SUNSET (IPFS) *
* REMOVE VIEW INSIDE THE PROGRAM AS IT IS NOT BEING USED OR REFERENCED.*
* REPLACE IT WITH A CALL TO NECN4000 TO RETREVE THE BUSINESS DATE.     *
* 12/12/2015 - MUKHER     - BYPASS RECORDS WITH ZERO PIN.  THEY ARE    *
*                           ALREADY BEEN NOTED IN THE CI1010D6 REPORT  *
*                           WHICH WILL BE MONITORED BY CIS SUPPORT GRP.*
*                           THERE WILL BE A NOTIFICATION EACH DAY TO   *
*                           CORRECT THE PIN UNTIL THE PIN IS FIXED IN  *
*                           THE CIS DATABASE. (C367955)                *
* 05/10/2017 - GHOSABE    - PIN EXPANSION CHANGES.(C420007)       PINE.*
* 11/16/2019 - BISWP/SHU  - CONTRACT STRATEGY PROJECT      CNTRSTRG    *
*                         - TO ASSIGN FIRST TPA/IPRO IND AND WRITE OUT *
*                           TO EXTRACT FILE                            *
**====================================================================**
*

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb600 extends BLNatBase
{
    // Data Areas
    private PdaCisa4000 pdaCisa4000;
    private PdaCisa200 pdaCisa200;
    private PdaNazpda_M pdaNazpda_M;
    private LdaAppl170 ldaAppl170;
    private LdaCisview1 ldaCisview1;
    private LdaCispartv ldaCispartv;
    private LdaCislpart ldaCislpart;
    private PdaCisa1000 pdaCisa1000;
    private PdaCisa600 pdaCisa600;
    private PdaCisa601 pdaCisa601;
    private PdaCisa2081 pdaCisa2081;
    private PdaNeca4000 pdaNeca4000;
    private PdaAdsa888 pdaAdsa888;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_acis_Reprint_Fl;
    private DbsField acis_Reprint_Fl_Rp_Tiaa_Contr;
    private DbsField acis_Reprint_Fl_Count_Castrp_Related_Contract_Info;

    private DbsGroup acis_Reprint_Fl_Rp_Related_Contract_Info;
    private DbsField acis_Reprint_Fl_Rp_Related_Contract_Type;
    private DbsField acis_Reprint_Fl_Rp_Related_Tiaa_No;
    private DbsField acis_Reprint_Fl_Rp_Related_Tiaa_Issue_Date;
    private DbsField acis_Reprint_Fl_Rp_Related_First_Payment_Date;
    private DbsField acis_Reprint_Fl_Rp_Related_Tiaa_Total_Amt;
    private DbsField acis_Reprint_Fl_Rp_Related_Last_Payment_Date;
    private DbsField acis_Reprint_Fl_Rp_Related_Payment_Frequency;
    private DbsField acis_Reprint_Fl_Rp_Related_Tiaa_Issue_State;
    private DbsField pnd_Mdo_Cash_Surv;
    private DbsField pnd_Report_Ssn;
    private DbsField pnd_Write_Sw;
    private DbsField pnd_Tot_Tiaa_Rea_Payin_Proc_Amt_H;
    private DbsField pnd_Tot_Tiaa_Paying_Pros_Amt_H;
    private DbsField pnd_Tot_Cref_Payin_Amt_N;
    private DbsField pnd_Update_Cnt;
    private DbsField pnd_Reprint_Update_Cnt;
    private DbsField pnd_Reprt_Counter;
    private DbsField pnd_Part_Counter;
    private DbsField pnd_Records_Written;
    private DbsField pnd_Work_Cnt;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_M;
    private DbsField pnd_N;
    private DbsField pnd_W;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Error_Mesg;
    private DbsField pnd_Msg;
    private DbsField pnd_Frst_Annt_Mid_I;

    private DbsGroup pnd_Frst_Annt_Mid_I__R_Field_1;
    private DbsField pnd_Frst_Annt_Mid_I_Pnd_Frst_Annt_Mid_Init;
    private DbsField pnd_Frst_Annt_Mid_I_Pnd_Frst_Annt_Mid_Rest;
    private DbsField pnd_Da_Tiaa_Nbr;

    private DbsGroup pnd_Da_Tiaa_Nbr__R_Field_2;
    private DbsField pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_1;
    private DbsField pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6;
    private DbsField pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_Rest;
    private DbsField pnd_Cref_Nbr;

    private DbsGroup pnd_Cref_Nbr__R_Field_3;
    private DbsField pnd_Cref_Nbr_Pnd_Cref_Num_1;
    private DbsField pnd_Cref_Nbr_Pnd_Cref_Num_6;
    private DbsField pnd_Cref_Nbr_Pnd_Cref_Num_Rest;
    private DbsField pnd_Tiaa_Contract_Num;

    private DbsGroup pnd_Tiaa_Contract_Num__R_Field_4;
    private DbsField pnd_Tiaa_Contract_Num_Pnd_Tiaa_1;
    private DbsField pnd_Tiaa_Contract_Num_Pnd_Tiaa_2_6;
    private DbsField pnd_Tiaa_Contract_Num_Pnd_Tiaa_Last;
    private DbsField pnd_Tiaa_Contract_Num_Pnd_Tiaa_Rest;
    private DbsField pnd_Cref_Contract_No;

    private DbsGroup pnd_Cref_Contract_No__R_Field_5;
    private DbsField pnd_Cref_Contract_No_Pnd_Cref_1;
    private DbsField pnd_Cref_Contract_No_Pnd_Cref_2_6;
    private DbsField pnd_Cref_Contract_No_Pnd_Cref_Last;
    private DbsField pnd_Cref_Contract_No_Pnd_Cref_Rest;
    private DbsField pnd_Tiaa_Real_Est_Num;

    private DbsGroup pnd_Tiaa_Real_Est_Num__R_Field_6;
    private DbsField pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_F;
    private DbsField pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_6;
    private DbsField pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_7;
    private DbsField pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Rea_E;
    private DbsField pnd_First_Guarant_P_Date;

    private DbsGroup pnd_First_Guarant_P_Date__R_Field_7;
    private DbsField pnd_First_Guarant_P_Date_Pnd_Mm_F;
    private DbsField pnd_First_Guarant_P_Date_Pnd_Dd_F;
    private DbsField pnd_First_Guarant_P_Date_Pnd_Ccyy_F;
    private DbsField pnd_End_Guarant_P_Date;

    private DbsGroup pnd_End_Guarant_P_Date__R_Field_8;
    private DbsField pnd_End_Guarant_P_Date_Pnd_Mm_E;
    private DbsField pnd_End_Guarant_P_Date_Pnd_Dd_E;
    private DbsField pnd_End_Guarant_P_Date_Pnd_Ccyy_E;
    private DbsField pnd_Input_Part_Code;
    private DbsField pnd_Requst_Begin_Dte;

    private DbsGroup pnd_Requst_Begin_Dte__R_Field_9;
    private DbsField pnd_Requst_Begin_Dte_Pnd_Ccyy_R;
    private DbsField pnd_Requst_Begin_Dte_Pnd_Mm_R;
    private DbsField pnd_Requst_Begin_Dte_Pnd_Dd_R;
    private DbsField pnd_Date_Of_Issue;

    private DbsGroup pnd_Date_Of_Issue__R_Field_10;
    private DbsField pnd_Date_Of_Issue_Pnd_Ccyy_I;
    private DbsField pnd_Date_Of_Issue_Pnd_Mm_I;
    private DbsField pnd_Date_Of_Issue_Pnd_Dd_I;
    private DbsField pnd_Input_Date;

    private DbsGroup pnd_Input_Date__R_Field_11;
    private DbsField pnd_Input_Date_Pnd_I_Cc;
    private DbsField pnd_Input_Date_Pnd_I_Yy;
    private DbsField pnd_Input_Date_Pnd_I_Mm;
    private DbsField pnd_Input_Date_Pnd_I_Dd;
    private DbsField pnd_Print_Date;

    private DbsGroup pnd_Print_Date__R_Field_12;
    private DbsField pnd_Print_Date_Pnd_P_Cc;
    private DbsField pnd_Print_Date_Pnd_P_Yy;
    private DbsField pnd_Print_Date_Pnd_P_Mm;
    private DbsField pnd_Print_Date_Pnd_P_Dd;
    private DbsField pnd_Business_Date;

    private DbsGroup pnd_Business_Date__R_Field_13;
    private DbsField pnd_Business_Date_Pnd_Cc;
    private DbsField pnd_Business_Date_Pnd_Yy;
    private DbsField pnd_Business_Date_Pnd_Mm;
    private DbsField pnd_Business_Date_Pnd_Dd;
    private DbsField pnd_Hold_Int_Rate;
    private DbsField pnd_Found_Different;
    private DbsField pnd_Mit_Effective_Dt_N;

    private DbsGroup pnd_Mit_Effective_Dt_N__R_Field_14;
    private DbsField pnd_Mit_Effective_Dt_N_Pnd_Mit_Effective_Dt;

    private DbsGroup pnd_Header_Record;
    private DbsField pnd_Header_Record_Pnd_Header_Text;
    private DbsField pnd_Header_Record_Pnd_Date_Time;
    private DbsField pnd_Tot_Tiaa_Cref_Mdo_Amt_N;
    private DbsField pnd_Cref_Annual_Units;
    private DbsField pnd_No_Bene_Ctrct;
    private DbsField pnd_Addr_Zp;
    private DbsField pnd_Addr_Len;

    private DbsGroup pnd_Len_All;
    private DbsField pnd_Len_All_Pnd_Lst_Len;
    private DbsField pnd_Len_All_Pnd_Frst_Len;
    private DbsField pnd_Len_All_Pnd_Mid_Len;
    private DbsField pnd_Len_All_Pnd_Sffx_Len;
    private DbsField pnd_Len_All_Pnd_Prfx_Len;
    private DbsField pnd_Begin;

    private DbsGroup pnd_Cis_Tiaa_Cntrct_Nbr_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data;

    private DbsGroup pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_15;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_State_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_Approval_Ind_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input;
    private DbsField pnd_Tiaa_Date_N;

    private DbsGroup pnd_Tiaa_Date_N__R_Field_16;
    private DbsField pnd_Tiaa_Date_N_Pnd_Tiaa_Date_A;
    private DbsField pnd_Cref_Mnth_Unts;
    private DbsField pnd_Cref_Annl_Unts;
    private DbsField pnd_Rea_Mnth_Unts;
    private DbsField pnd_Rea_Annl_Unts;
    private DbsField pnd_C;
    private DbsField pnd_B;
    private DbsField pnd_D;
    private DbsField pnd_Today;
    private DbsField pnd_Part_Name;
    private DbsField pnd_Error_Message;
    private DbsField pnd_Rqstng_System;
    private DbsField pnd_Rqst_System;
    private DbsField pnd_No_Bene;
    private DbsField pnd_Total_Xfer;

    private DbsGroup pnd_Mdo_Fields;
    private DbsField pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Num;

    private DbsGroup pnd_Mdo_Fields__R_Field_17;
    private DbsField pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Num_A;
    private DbsField pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker;
    private DbsField pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt;
    private DbsField pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Unit_Price;
    private DbsField pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Units;
    private DbsField pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_N;

    private DbsGroup pnd_Mdo_Fields__R_Field_18;
    private DbsField pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_A;
    private DbsField pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_D;
    private DbsField pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Fund_Name;
    private DbsField pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt_Field;
    private DbsField pnd_Cref_Date_Of_Issue;

    private DbsGroup pnd_Cref_Date_Of_Issue__R_Field_19;
    private DbsField pnd_Cref_Date_Of_Issue_Pnd_Cref_Date_Of_Issue_N;
    private DbsField pnd_Max_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCisa4000 = new PdaCisa4000(localVariables);
        pdaCisa200 = new PdaCisa200(localVariables);
        pdaNazpda_M = new PdaNazpda_M(localVariables);
        ldaAppl170 = new LdaAppl170();
        registerRecord(ldaAppl170);
        ldaCisview1 = new LdaCisview1();
        registerRecord(ldaCisview1);
        registerRecord(ldaCisview1.getVw_cis_Prt_View());
        ldaCispartv = new LdaCispartv();
        registerRecord(ldaCispartv);
        registerRecord(ldaCispartv.getVw_cis_Part_View());
        ldaCislpart = new LdaCislpart();
        registerRecord(ldaCislpart);
        pdaCisa1000 = new PdaCisa1000(localVariables);
        pdaCisa600 = new PdaCisa600(localVariables);
        pdaCisa601 = new PdaCisa601(localVariables);
        pdaCisa2081 = new PdaCisa2081(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaAdsa888 = new PdaAdsa888(localVariables);

        // Local Variables

        vw_acis_Reprint_Fl = new DataAccessProgramView(new NameInfo("vw_acis_Reprint_Fl", "ACIS-REPRINT-FL"), "ACIS_REPRINT_FL_12", "ACIS_RPRNT_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("ACIS_REPRINT_FL_12"));
        acis_Reprint_Fl_Rp_Tiaa_Contr = vw_acis_Reprint_Fl.getRecord().newFieldInGroup("acis_Reprint_Fl_Rp_Tiaa_Contr", "RP-TIAA-CONTR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "RP_TIAA_CONTR");
        acis_Reprint_Fl_Rp_Tiaa_Contr.setDdmHeader("TIAA/CONTR");
        acis_Reprint_Fl_Count_Castrp_Related_Contract_Info = vw_acis_Reprint_Fl.getRecord().newFieldInGroup("acis_Reprint_Fl_Count_Castrp_Related_Contract_Info", 
            "C*RP-RELATED-CONTRACT-INFO", RepeatingFieldStrategy.CAsteriskVariable, "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");

        acis_Reprint_Fl_Rp_Related_Contract_Info = vw_acis_Reprint_Fl.getRecord().newGroupArrayInGroup("acis_Reprint_Fl_Rp_Related_Contract_Info", "RP-RELATED-CONTRACT-INFO", 
            new DbsArrayController(1, 30) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Contract_Type = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Contract_Type", 
            "RP-RELATED-CONTRACT-TYPE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_CONTRACT_TYPE", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Tiaa_No = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Tiaa_No", "RP-RELATED-TIAA-NO", 
            FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_NO", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Tiaa_Issue_Date = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Tiaa_Issue_Date", 
            "RP-RELATED-TIAA-ISSUE-DATE", FieldType.NUMERIC, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_ISSUE_DATE", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_First_Payment_Date = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_First_Payment_Date", 
            "RP-RELATED-FIRST-PAYMENT-DATE", FieldType.NUMERIC, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_FIRST_PAYMENT_DATE", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Tiaa_Total_Amt = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Tiaa_Total_Amt", 
            "RP-RELATED-TIAA-TOTAL-AMT", FieldType.NUMERIC, 10, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_TOTAL_AMT", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Last_Payment_Date = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Last_Payment_Date", 
            "RP-RELATED-LAST-PAYMENT-DATE", FieldType.NUMERIC, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_LAST_PAYMENT_DATE", 
            "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Payment_Frequency = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Payment_Frequency", 
            "RP-RELATED-PAYMENT-FREQUENCY", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_PAYMENT_FREQUENCY", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        acis_Reprint_Fl_Rp_Related_Tiaa_Issue_State = acis_Reprint_Fl_Rp_Related_Contract_Info.newFieldInGroup("acis_Reprint_Fl_Rp_Related_Tiaa_Issue_State", 
            "RP-RELATED-TIAA-ISSUE-STATE", FieldType.STRING, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "RP_RELATED_TIAA_ISSUE_STATE", "ACIS_RPRNT_FILE_RP_RELATED_CONTRACT_INFO");
        registerRecord(vw_acis_Reprint_Fl);

        pnd_Mdo_Cash_Surv = localVariables.newFieldInRecord("pnd_Mdo_Cash_Surv", "#MDO-CASH-SURV", FieldType.STRING, 1);
        pnd_Report_Ssn = localVariables.newFieldInRecord("pnd_Report_Ssn", "#REPORT-SSN", FieldType.STRING, 11);
        pnd_Write_Sw = localVariables.newFieldInRecord("pnd_Write_Sw", "#WRITE-SW", FieldType.STRING, 1);
        pnd_Tot_Tiaa_Rea_Payin_Proc_Amt_H = localVariables.newFieldInRecord("pnd_Tot_Tiaa_Rea_Payin_Proc_Amt_H", "#TOT-TIAA-REA-PAYIN-PROC-AMT-H", FieldType.NUMERIC, 
            14, 2);
        pnd_Tot_Tiaa_Paying_Pros_Amt_H = localVariables.newFieldInRecord("pnd_Tot_Tiaa_Paying_Pros_Amt_H", "#TOT-TIAA-PAYING-PROS-AMT-H", FieldType.NUMERIC, 
            14, 2);
        pnd_Tot_Cref_Payin_Amt_N = localVariables.newFieldInRecord("pnd_Tot_Cref_Payin_Amt_N", "#TOT-CREF-PAYIN-AMT-N", FieldType.NUMERIC, 14, 2);
        pnd_Update_Cnt = localVariables.newFieldInRecord("pnd_Update_Cnt", "#UPDATE-CNT", FieldType.NUMERIC, 7);
        pnd_Reprint_Update_Cnt = localVariables.newFieldInRecord("pnd_Reprint_Update_Cnt", "#REPRINT-UPDATE-CNT", FieldType.NUMERIC, 7);
        pnd_Reprt_Counter = localVariables.newFieldInRecord("pnd_Reprt_Counter", "#REPRT-COUNTER", FieldType.NUMERIC, 7);
        pnd_Part_Counter = localVariables.newFieldInRecord("pnd_Part_Counter", "#PART-COUNTER", FieldType.NUMERIC, 7);
        pnd_Records_Written = localVariables.newFieldInRecord("pnd_Records_Written", "#RECORDS-WRITTEN", FieldType.NUMERIC, 7);
        pnd_Work_Cnt = localVariables.newFieldInRecord("pnd_Work_Cnt", "#WORK-CNT", FieldType.NUMERIC, 7);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.PACKED_DECIMAL, 3);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.PACKED_DECIMAL, 3);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.PACKED_DECIMAL, 3);
        pnd_W = localVariables.newFieldInRecord("pnd_W", "#W", FieldType.PACKED_DECIMAL, 3);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 40);
        pnd_Error_Mesg = localVariables.newFieldInRecord("pnd_Error_Mesg", "#ERROR-MESG", FieldType.STRING, 40);
        pnd_Msg = localVariables.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 40);
        pnd_Frst_Annt_Mid_I = localVariables.newFieldInRecord("pnd_Frst_Annt_Mid_I", "#FRST-ANNT-MID-I", FieldType.STRING, 30);

        pnd_Frst_Annt_Mid_I__R_Field_1 = localVariables.newGroupInRecord("pnd_Frst_Annt_Mid_I__R_Field_1", "REDEFINE", pnd_Frst_Annt_Mid_I);
        pnd_Frst_Annt_Mid_I_Pnd_Frst_Annt_Mid_Init = pnd_Frst_Annt_Mid_I__R_Field_1.newFieldInGroup("pnd_Frst_Annt_Mid_I_Pnd_Frst_Annt_Mid_Init", "#FRST-ANNT-MID-INIT", 
            FieldType.STRING, 1);
        pnd_Frst_Annt_Mid_I_Pnd_Frst_Annt_Mid_Rest = pnd_Frst_Annt_Mid_I__R_Field_1.newFieldInGroup("pnd_Frst_Annt_Mid_I_Pnd_Frst_Annt_Mid_Rest", "#FRST-ANNT-MID-REST", 
            FieldType.STRING, 29);
        pnd_Da_Tiaa_Nbr = localVariables.newFieldInRecord("pnd_Da_Tiaa_Nbr", "#DA-TIAA-NBR", FieldType.STRING, 10);

        pnd_Da_Tiaa_Nbr__R_Field_2 = localVariables.newGroupInRecord("pnd_Da_Tiaa_Nbr__R_Field_2", "REDEFINE", pnd_Da_Tiaa_Nbr);
        pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_1 = pnd_Da_Tiaa_Nbr__R_Field_2.newFieldInGroup("pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_1", "#DA-TIAA-1", FieldType.STRING, 1);
        pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6 = pnd_Da_Tiaa_Nbr__R_Field_2.newFieldInGroup("pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6", "#DA-TIAA-6", FieldType.STRING, 6);
        pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_Rest = pnd_Da_Tiaa_Nbr__R_Field_2.newFieldInGroup("pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_Rest", "#DA-TIAA-REST", FieldType.STRING, 
            3);
        pnd_Cref_Nbr = localVariables.newFieldInRecord("pnd_Cref_Nbr", "#CREF-NBR", FieldType.STRING, 10);

        pnd_Cref_Nbr__R_Field_3 = localVariables.newGroupInRecord("pnd_Cref_Nbr__R_Field_3", "REDEFINE", pnd_Cref_Nbr);
        pnd_Cref_Nbr_Pnd_Cref_Num_1 = pnd_Cref_Nbr__R_Field_3.newFieldInGroup("pnd_Cref_Nbr_Pnd_Cref_Num_1", "#CREF-NUM-1", FieldType.STRING, 1);
        pnd_Cref_Nbr_Pnd_Cref_Num_6 = pnd_Cref_Nbr__R_Field_3.newFieldInGroup("pnd_Cref_Nbr_Pnd_Cref_Num_6", "#CREF-NUM-6", FieldType.STRING, 6);
        pnd_Cref_Nbr_Pnd_Cref_Num_Rest = pnd_Cref_Nbr__R_Field_3.newFieldInGroup("pnd_Cref_Nbr_Pnd_Cref_Num_Rest", "#CREF-NUM-REST", FieldType.STRING, 
            3);
        pnd_Tiaa_Contract_Num = localVariables.newFieldInRecord("pnd_Tiaa_Contract_Num", "#TIAA-CONTRACT-NUM", FieldType.STRING, 10);

        pnd_Tiaa_Contract_Num__R_Field_4 = localVariables.newGroupInRecord("pnd_Tiaa_Contract_Num__R_Field_4", "REDEFINE", pnd_Tiaa_Contract_Num);
        pnd_Tiaa_Contract_Num_Pnd_Tiaa_1 = pnd_Tiaa_Contract_Num__R_Field_4.newFieldInGroup("pnd_Tiaa_Contract_Num_Pnd_Tiaa_1", "#TIAA-1", FieldType.STRING, 
            1);
        pnd_Tiaa_Contract_Num_Pnd_Tiaa_2_6 = pnd_Tiaa_Contract_Num__R_Field_4.newFieldInGroup("pnd_Tiaa_Contract_Num_Pnd_Tiaa_2_6", "#TIAA-2-6", FieldType.STRING, 
            6);
        pnd_Tiaa_Contract_Num_Pnd_Tiaa_Last = pnd_Tiaa_Contract_Num__R_Field_4.newFieldInGroup("pnd_Tiaa_Contract_Num_Pnd_Tiaa_Last", "#TIAA-LAST", FieldType.STRING, 
            1);
        pnd_Tiaa_Contract_Num_Pnd_Tiaa_Rest = pnd_Tiaa_Contract_Num__R_Field_4.newFieldInGroup("pnd_Tiaa_Contract_Num_Pnd_Tiaa_Rest", "#TIAA-REST", FieldType.STRING, 
            2);
        pnd_Cref_Contract_No = localVariables.newFieldInRecord("pnd_Cref_Contract_No", "#CREF-CONTRACT-NO", FieldType.STRING, 10);

        pnd_Cref_Contract_No__R_Field_5 = localVariables.newGroupInRecord("pnd_Cref_Contract_No__R_Field_5", "REDEFINE", pnd_Cref_Contract_No);
        pnd_Cref_Contract_No_Pnd_Cref_1 = pnd_Cref_Contract_No__R_Field_5.newFieldInGroup("pnd_Cref_Contract_No_Pnd_Cref_1", "#CREF-1", FieldType.STRING, 
            1);
        pnd_Cref_Contract_No_Pnd_Cref_2_6 = pnd_Cref_Contract_No__R_Field_5.newFieldInGroup("pnd_Cref_Contract_No_Pnd_Cref_2_6", "#CREF-2-6", FieldType.STRING, 
            6);
        pnd_Cref_Contract_No_Pnd_Cref_Last = pnd_Cref_Contract_No__R_Field_5.newFieldInGroup("pnd_Cref_Contract_No_Pnd_Cref_Last", "#CREF-LAST", FieldType.STRING, 
            1);
        pnd_Cref_Contract_No_Pnd_Cref_Rest = pnd_Cref_Contract_No__R_Field_5.newFieldInGroup("pnd_Cref_Contract_No_Pnd_Cref_Rest", "#CREF-REST", FieldType.STRING, 
            2);
        pnd_Tiaa_Real_Est_Num = localVariables.newFieldInRecord("pnd_Tiaa_Real_Est_Num", "#TIAA-REAL-EST-NUM", FieldType.STRING, 10);

        pnd_Tiaa_Real_Est_Num__R_Field_6 = localVariables.newGroupInRecord("pnd_Tiaa_Real_Est_Num__R_Field_6", "REDEFINE", pnd_Tiaa_Real_Est_Num);
        pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_F = pnd_Tiaa_Real_Est_Num__R_Field_6.newFieldInGroup("pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_F", "#TIAA-RE-F", FieldType.STRING, 
            1);
        pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_6 = pnd_Tiaa_Real_Est_Num__R_Field_6.newFieldInGroup("pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_6", "#TIAA-RE-6", FieldType.STRING, 
            6);
        pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_7 = pnd_Tiaa_Real_Est_Num__R_Field_6.newFieldInGroup("pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_7", "#TIAA-RE-7", FieldType.STRING, 
            1);
        pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Rea_E = pnd_Tiaa_Real_Est_Num__R_Field_6.newFieldInGroup("pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Rea_E", "#TIAA-REA-E", 
            FieldType.STRING, 2);
        pnd_First_Guarant_P_Date = localVariables.newFieldInRecord("pnd_First_Guarant_P_Date", "#FIRST-GUARANT-P-DATE", FieldType.STRING, 8);

        pnd_First_Guarant_P_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_First_Guarant_P_Date__R_Field_7", "REDEFINE", pnd_First_Guarant_P_Date);
        pnd_First_Guarant_P_Date_Pnd_Mm_F = pnd_First_Guarant_P_Date__R_Field_7.newFieldInGroup("pnd_First_Guarant_P_Date_Pnd_Mm_F", "#MM-F", FieldType.STRING, 
            2);
        pnd_First_Guarant_P_Date_Pnd_Dd_F = pnd_First_Guarant_P_Date__R_Field_7.newFieldInGroup("pnd_First_Guarant_P_Date_Pnd_Dd_F", "#DD-F", FieldType.STRING, 
            2);
        pnd_First_Guarant_P_Date_Pnd_Ccyy_F = pnd_First_Guarant_P_Date__R_Field_7.newFieldInGroup("pnd_First_Guarant_P_Date_Pnd_Ccyy_F", "#CCYY-F", FieldType.STRING, 
            4);
        pnd_End_Guarant_P_Date = localVariables.newFieldInRecord("pnd_End_Guarant_P_Date", "#END-GUARANT-P-DATE", FieldType.STRING, 8);

        pnd_End_Guarant_P_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_End_Guarant_P_Date__R_Field_8", "REDEFINE", pnd_End_Guarant_P_Date);
        pnd_End_Guarant_P_Date_Pnd_Mm_E = pnd_End_Guarant_P_Date__R_Field_8.newFieldInGroup("pnd_End_Guarant_P_Date_Pnd_Mm_E", "#MM-E", FieldType.STRING, 
            2);
        pnd_End_Guarant_P_Date_Pnd_Dd_E = pnd_End_Guarant_P_Date__R_Field_8.newFieldInGroup("pnd_End_Guarant_P_Date_Pnd_Dd_E", "#DD-E", FieldType.STRING, 
            2);
        pnd_End_Guarant_P_Date_Pnd_Ccyy_E = pnd_End_Guarant_P_Date__R_Field_8.newFieldInGroup("pnd_End_Guarant_P_Date_Pnd_Ccyy_E", "#CCYY-E", FieldType.STRING, 
            4);
        pnd_Input_Part_Code = localVariables.newFieldInRecord("pnd_Input_Part_Code", "#INPUT-PART-CODE", FieldType.STRING, 4);
        pnd_Requst_Begin_Dte = localVariables.newFieldInRecord("pnd_Requst_Begin_Dte", "#REQUST-BEGIN-DTE", FieldType.STRING, 8);

        pnd_Requst_Begin_Dte__R_Field_9 = localVariables.newGroupInRecord("pnd_Requst_Begin_Dte__R_Field_9", "REDEFINE", pnd_Requst_Begin_Dte);
        pnd_Requst_Begin_Dte_Pnd_Ccyy_R = pnd_Requst_Begin_Dte__R_Field_9.newFieldInGroup("pnd_Requst_Begin_Dte_Pnd_Ccyy_R", "#CCYY-R", FieldType.STRING, 
            4);
        pnd_Requst_Begin_Dte_Pnd_Mm_R = pnd_Requst_Begin_Dte__R_Field_9.newFieldInGroup("pnd_Requst_Begin_Dte_Pnd_Mm_R", "#MM-R", FieldType.STRING, 2);
        pnd_Requst_Begin_Dte_Pnd_Dd_R = pnd_Requst_Begin_Dte__R_Field_9.newFieldInGroup("pnd_Requst_Begin_Dte_Pnd_Dd_R", "#DD-R", FieldType.STRING, 2);
        pnd_Date_Of_Issue = localVariables.newFieldInRecord("pnd_Date_Of_Issue", "#DATE-OF-ISSUE", FieldType.STRING, 8);

        pnd_Date_Of_Issue__R_Field_10 = localVariables.newGroupInRecord("pnd_Date_Of_Issue__R_Field_10", "REDEFINE", pnd_Date_Of_Issue);
        pnd_Date_Of_Issue_Pnd_Ccyy_I = pnd_Date_Of_Issue__R_Field_10.newFieldInGroup("pnd_Date_Of_Issue_Pnd_Ccyy_I", "#CCYY-I", FieldType.STRING, 4);
        pnd_Date_Of_Issue_Pnd_Mm_I = pnd_Date_Of_Issue__R_Field_10.newFieldInGroup("pnd_Date_Of_Issue_Pnd_Mm_I", "#MM-I", FieldType.STRING, 2);
        pnd_Date_Of_Issue_Pnd_Dd_I = pnd_Date_Of_Issue__R_Field_10.newFieldInGroup("pnd_Date_Of_Issue_Pnd_Dd_I", "#DD-I", FieldType.STRING, 2);
        pnd_Input_Date = localVariables.newFieldInRecord("pnd_Input_Date", "#INPUT-DATE", FieldType.NUMERIC, 8);

        pnd_Input_Date__R_Field_11 = localVariables.newGroupInRecord("pnd_Input_Date__R_Field_11", "REDEFINE", pnd_Input_Date);
        pnd_Input_Date_Pnd_I_Cc = pnd_Input_Date__R_Field_11.newFieldInGroup("pnd_Input_Date_Pnd_I_Cc", "#I-CC", FieldType.NUMERIC, 2);
        pnd_Input_Date_Pnd_I_Yy = pnd_Input_Date__R_Field_11.newFieldInGroup("pnd_Input_Date_Pnd_I_Yy", "#I-YY", FieldType.NUMERIC, 2);
        pnd_Input_Date_Pnd_I_Mm = pnd_Input_Date__R_Field_11.newFieldInGroup("pnd_Input_Date_Pnd_I_Mm", "#I-MM", FieldType.NUMERIC, 2);
        pnd_Input_Date_Pnd_I_Dd = pnd_Input_Date__R_Field_11.newFieldInGroup("pnd_Input_Date_Pnd_I_Dd", "#I-DD", FieldType.NUMERIC, 2);
        pnd_Print_Date = localVariables.newFieldInRecord("pnd_Print_Date", "#PRINT-DATE", FieldType.STRING, 8);

        pnd_Print_Date__R_Field_12 = localVariables.newGroupInRecord("pnd_Print_Date__R_Field_12", "REDEFINE", pnd_Print_Date);
        pnd_Print_Date_Pnd_P_Cc = pnd_Print_Date__R_Field_12.newFieldInGroup("pnd_Print_Date_Pnd_P_Cc", "#P-CC", FieldType.STRING, 2);
        pnd_Print_Date_Pnd_P_Yy = pnd_Print_Date__R_Field_12.newFieldInGroup("pnd_Print_Date_Pnd_P_Yy", "#P-YY", FieldType.STRING, 2);
        pnd_Print_Date_Pnd_P_Mm = pnd_Print_Date__R_Field_12.newFieldInGroup("pnd_Print_Date_Pnd_P_Mm", "#P-MM", FieldType.STRING, 2);
        pnd_Print_Date_Pnd_P_Dd = pnd_Print_Date__R_Field_12.newFieldInGroup("pnd_Print_Date_Pnd_P_Dd", "#P-DD", FieldType.STRING, 2);
        pnd_Business_Date = localVariables.newFieldInRecord("pnd_Business_Date", "#BUSINESS-DATE", FieldType.STRING, 8);

        pnd_Business_Date__R_Field_13 = localVariables.newGroupInRecord("pnd_Business_Date__R_Field_13", "REDEFINE", pnd_Business_Date);
        pnd_Business_Date_Pnd_Cc = pnd_Business_Date__R_Field_13.newFieldInGroup("pnd_Business_Date_Pnd_Cc", "#CC", FieldType.NUMERIC, 2);
        pnd_Business_Date_Pnd_Yy = pnd_Business_Date__R_Field_13.newFieldInGroup("pnd_Business_Date_Pnd_Yy", "#YY", FieldType.NUMERIC, 2);
        pnd_Business_Date_Pnd_Mm = pnd_Business_Date__R_Field_13.newFieldInGroup("pnd_Business_Date_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Business_Date_Pnd_Dd = pnd_Business_Date__R_Field_13.newFieldInGroup("pnd_Business_Date_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Hold_Int_Rate = localVariables.newFieldInRecord("pnd_Hold_Int_Rate", "#HOLD-INT-RATE", FieldType.NUMERIC, 5, 3);
        pnd_Found_Different = localVariables.newFieldInRecord("pnd_Found_Different", "#FOUND-DIFFERENT", FieldType.BOOLEAN, 1);
        pnd_Mit_Effective_Dt_N = localVariables.newFieldInRecord("pnd_Mit_Effective_Dt_N", "#MIT-EFFECTIVE-DT-N", FieldType.NUMERIC, 8);

        pnd_Mit_Effective_Dt_N__R_Field_14 = localVariables.newGroupInRecord("pnd_Mit_Effective_Dt_N__R_Field_14", "REDEFINE", pnd_Mit_Effective_Dt_N);
        pnd_Mit_Effective_Dt_N_Pnd_Mit_Effective_Dt = pnd_Mit_Effective_Dt_N__R_Field_14.newFieldInGroup("pnd_Mit_Effective_Dt_N_Pnd_Mit_Effective_Dt", 
            "#MIT-EFFECTIVE-DT", FieldType.STRING, 8);

        pnd_Header_Record = localVariables.newGroupInRecord("pnd_Header_Record", "#HEADER-RECORD");
        pnd_Header_Record_Pnd_Header_Text = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Header_Text", "#HEADER-TEXT", FieldType.STRING, 2);
        pnd_Header_Record_Pnd_Date_Time = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Date_Time", "#DATE-TIME", FieldType.STRING, 21);
        pnd_Tot_Tiaa_Cref_Mdo_Amt_N = localVariables.newFieldInRecord("pnd_Tot_Tiaa_Cref_Mdo_Amt_N", "#TOT-TIAA-CREF-MDO-AMT-N", FieldType.NUMERIC, 14, 
            2);
        pnd_Cref_Annual_Units = localVariables.newFieldInRecord("pnd_Cref_Annual_Units", "#CREF-ANNUAL-UNITS", FieldType.STRING, 12);
        pnd_No_Bene_Ctrct = localVariables.newFieldInRecord("pnd_No_Bene_Ctrct", "#NO-BENE-CTRCT", FieldType.STRING, 10);
        pnd_Addr_Zp = localVariables.newFieldInRecord("pnd_Addr_Zp", "#ADDR-ZP", FieldType.STRING, 35);
        pnd_Addr_Len = localVariables.newFieldInRecord("pnd_Addr_Len", "#ADDR-LEN", FieldType.NUMERIC, 2);

        pnd_Len_All = localVariables.newGroupInRecord("pnd_Len_All", "#LEN-ALL");
        pnd_Len_All_Pnd_Lst_Len = pnd_Len_All.newFieldInGroup("pnd_Len_All_Pnd_Lst_Len", "#LST-LEN", FieldType.INTEGER, 2);
        pnd_Len_All_Pnd_Frst_Len = pnd_Len_All.newFieldInGroup("pnd_Len_All_Pnd_Frst_Len", "#FRST-LEN", FieldType.INTEGER, 2);
        pnd_Len_All_Pnd_Mid_Len = pnd_Len_All.newFieldInGroup("pnd_Len_All_Pnd_Mid_Len", "#MID-LEN", FieldType.INTEGER, 2);
        pnd_Len_All_Pnd_Sffx_Len = pnd_Len_All.newFieldInGroup("pnd_Len_All_Pnd_Sffx_Len", "#SFFX-LEN", FieldType.INTEGER, 2);
        pnd_Len_All_Pnd_Prfx_Len = pnd_Len_All.newFieldInGroup("pnd_Len_All_Pnd_Prfx_Len", "#PRFX-LEN", FieldType.INTEGER, 2);
        pnd_Begin = localVariables.newFieldInRecord("pnd_Begin", "#BEGIN", FieldType.INTEGER, 2);

        pnd_Cis_Tiaa_Cntrct_Nbr_Input = localVariables.newGroupArrayInRecord("pnd_Cis_Tiaa_Cntrct_Nbr_Input", "#CIS-TIAA-CNTRCT-NBR-INPUT", new DbsArrayController(1, 
            20));
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data = pnd_Cis_Tiaa_Cntrct_Nbr_Input.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data", 
            "#CIS-TIAA-CNTRCT-NBR-INPUT-DATA", FieldType.STRING, 10);

        pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_15 = pnd_Cis_Tiaa_Cntrct_Nbr_Input.newGroupInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_15", "REDEFINE", 
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_15.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input", 
            "#FROM-STATE-INPUT", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_15.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input", 
            "#FROM-APPROVAL-IND-INPUT", FieldType.STRING, 1);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_State_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_15.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_State_Input", 
            "#TO-STATE-INPUT", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_Approval_Ind_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_15.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_Approval_Ind_Input", 
            "#TO-APPROVAL-IND-INPUT", FieldType.STRING, 1);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_15.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input", 
            "#RES-STATE-INPUT", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_15.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input", 
            "#RES-APPROVAL-IND-INPUT", FieldType.STRING, 1);
        pnd_Tiaa_Date_N = localVariables.newFieldInRecord("pnd_Tiaa_Date_N", "#TIAA-DATE-N", FieldType.NUMERIC, 8);

        pnd_Tiaa_Date_N__R_Field_16 = localVariables.newGroupInRecord("pnd_Tiaa_Date_N__R_Field_16", "REDEFINE", pnd_Tiaa_Date_N);
        pnd_Tiaa_Date_N_Pnd_Tiaa_Date_A = pnd_Tiaa_Date_N__R_Field_16.newFieldInGroup("pnd_Tiaa_Date_N_Pnd_Tiaa_Date_A", "#TIAA-DATE-A", FieldType.STRING, 
            8);
        pnd_Cref_Mnth_Unts = localVariables.newFieldInRecord("pnd_Cref_Mnth_Unts", "#CREF-MNTH-UNTS", FieldType.BOOLEAN, 1);
        pnd_Cref_Annl_Unts = localVariables.newFieldInRecord("pnd_Cref_Annl_Unts", "#CREF-ANNL-UNTS", FieldType.BOOLEAN, 1);
        pnd_Rea_Mnth_Unts = localVariables.newFieldInRecord("pnd_Rea_Mnth_Unts", "#REA-MNTH-UNTS", FieldType.BOOLEAN, 1);
        pnd_Rea_Annl_Unts = localVariables.newFieldInRecord("pnd_Rea_Annl_Unts", "#REA-ANNL-UNTS", FieldType.BOOLEAN, 1);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.NUMERIC, 3);
        pnd_Today = localVariables.newFieldInRecord("pnd_Today", "#TODAY", FieldType.DATE);
        pnd_Part_Name = localVariables.newFieldInRecord("pnd_Part_Name", "#PART-NAME", FieldType.STRING, 35);
        pnd_Error_Message = localVariables.newFieldInRecord("pnd_Error_Message", "#ERROR-MESSAGE", FieldType.STRING, 45);
        pnd_Rqstng_System = localVariables.newFieldInRecord("pnd_Rqstng_System", "#RQSTNG-SYSTEM", FieldType.STRING, 8);
        pnd_Rqst_System = localVariables.newFieldInRecord("pnd_Rqst_System", "#RQST-SYSTEM", FieldType.STRING, 8);
        pnd_No_Bene = localVariables.newFieldInRecord("pnd_No_Bene", "#NO-BENE", FieldType.NUMERIC, 9);
        pnd_Total_Xfer = localVariables.newFieldInRecord("pnd_Total_Xfer", "#TOTAL-XFER", FieldType.NUMERIC, 9);

        pnd_Mdo_Fields = localVariables.newGroupInRecord("pnd_Mdo_Fields", "#MDO-FIELDS");
        pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Num = pnd_Mdo_Fields.newFieldInGroup("pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Num", "#WS-CIS-INVEST-NUM", FieldType.NUMERIC, 
            2);

        pnd_Mdo_Fields__R_Field_17 = pnd_Mdo_Fields.newGroupInGroup("pnd_Mdo_Fields__R_Field_17", "REDEFINE", pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Num);
        pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Num_A = pnd_Mdo_Fields__R_Field_17.newFieldInGroup("pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Num_A", "#WS-CIS-INVEST-NUM-A", 
            FieldType.STRING, 2);
        pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker = pnd_Mdo_Fields.newFieldInGroup("pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker", "#WS-CIS-INVEST-TICKER", FieldType.STRING, 
            10);
        pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt = pnd_Mdo_Fields.newFieldInGroup("pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt", "#WS-CIS-INVEST-AMT", FieldType.NUMERIC, 
            13, 2);
        pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Unit_Price = pnd_Mdo_Fields.newFieldInGroup("pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Unit_Price", "#WS-CIS-INVEST-UNIT-PRICE", 
            FieldType.NUMERIC, 13, 4);
        pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Units = pnd_Mdo_Fields.newFieldInGroup("pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Units", "#WS-CIS-INVEST-UNITS", FieldType.NUMERIC, 
            11, 4);
        pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_N = pnd_Mdo_Fields.newFieldInGroup("pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_N", "#WS-CIS-INVEST-PROCESS-DT-N", 
            FieldType.NUMERIC, 8);

        pnd_Mdo_Fields__R_Field_18 = pnd_Mdo_Fields.newGroupInGroup("pnd_Mdo_Fields__R_Field_18", "REDEFINE", pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_N);
        pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_A = pnd_Mdo_Fields__R_Field_18.newFieldInGroup("pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_A", "#WS-CIS-INVEST-PROCESS-DT-A", 
            FieldType.STRING, 8);
        pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_D = pnd_Mdo_Fields.newFieldInGroup("pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_D", "#WS-CIS-INVEST-PROCESS-DT-D", 
            FieldType.DATE);
        pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Fund_Name = pnd_Mdo_Fields.newFieldInGroup("pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Fund_Name", "#WS-CIS-INVEST-FUND-NAME", 
            FieldType.STRING, 30);
        pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt_Field = pnd_Mdo_Fields.newFieldInGroup("pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt_Field", "#WS-CIS-INVEST-AMT-FIELD", 
            FieldType.STRING, 19);
        pnd_Cref_Date_Of_Issue = localVariables.newFieldInRecord("pnd_Cref_Date_Of_Issue", "#CREF-DATE-OF-ISSUE", FieldType.STRING, 8);

        pnd_Cref_Date_Of_Issue__R_Field_19 = localVariables.newGroupInRecord("pnd_Cref_Date_Of_Issue__R_Field_19", "REDEFINE", pnd_Cref_Date_Of_Issue);
        pnd_Cref_Date_Of_Issue_Pnd_Cref_Date_Of_Issue_N = pnd_Cref_Date_Of_Issue__R_Field_19.newFieldInGroup("pnd_Cref_Date_Of_Issue_Pnd_Cref_Date_Of_Issue_N", 
            "#CREF-DATE-OF-ISSUE-N", FieldType.NUMERIC, 8);
        pnd_Max_Count = localVariables.newFieldInRecord("pnd_Max_Count", "#MAX-COUNT", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_acis_Reprint_Fl.reset();

        ldaAppl170.initializeValues();
        ldaCisview1.initializeValues();
        ldaCispartv.initializeValues();
        ldaCislpart.initializeValues();

        localVariables.reset();
        pnd_Mdo_Cash_Surv.setInitialValue("N");
        pnd_Header_Record_Pnd_Header_Text.setInitialValue("HD");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisb600() throws Exception
    {
        super("Cisb600");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cisb600|Main");
        OnErrorManager.pushEvent("CISB600", onError);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atTopOfPage(atTopEventRpt6, 6);
        setupReports();
        while(true)
        {
            try
            {
                //* *---------------------------MAIN LOGIC-------------------------------**
                //*  ZP=ON
                //*  ZP=ON
                //*  ZP=ON
                //*  ZP=ON
                //*  ZP=ON
                //*  PINE
                //*  PINE
                //*  PINE
                //*  PINE
                //*  PINE
                //* *                                                                                                                                                     //Natural: FORMAT ( 1 ) PS = 62 LS = 130;//Natural: FORMAT ( 2 ) PS = 62 LS = 75 ZP = ON;//Natural: FORMAT ( 3 ) PS = 58 LS = 130;//Natural: FORMAT ( 4 ) PS = 62 LS = 130;//Natural: FORMAT ( 5 ) PS = 62 LS = 130;//Natural: FORMAT ( 6 ) PS = 62 LS = 130
                //* *                                                                                                                                                     //Natural: AT TOP OF PAGE ( 4 )
                //* *                                                                                                                                                     //Natural: AT TOP OF PAGE ( 5 )
                //* *                                                                                                                                                     //Natural: AT TOP OF PAGE ( 6 )
                                                                                                                                                                          //Natural: PERFORM GET-CURRENT-BUSINESS-DAY
                sub_Get_Current_Business_Day();
                if (condition(Global.isEscape())) {return;}
                //*  #TODAY := #BUSINESS-DATE
                pnd_Today.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Business_Date);                                                                               //Natural: MOVE EDITED #BUSINESS-DATE TO #TODAY ( EM = YYYYMMDD )
                //* *
                getReports().write(0, "=",pnd_Business_Date);                                                                                                             //Natural: WRITE '=' #BUSINESS-DATE
                if (Global.isEscape()) return;
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 2 ) TITLE LEFT JUSTIFIED 1T *DATU '-' *TIMX ( EM = HH:IIAP ) 29T 'CIS RE-PRINT SYSTEM' 60T 'PG:  ' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / 1T *INIT-USER '-' *PROGRAM 60T 'REPORT: RPT1' /
                //*  WB - ACCRC START
                //*  NEW EXTERNALIZATION + SEQUENCING OF PROD CDE
                //*  THIS INPUT IS USED TO SELECT THE
                DbsUtil.callnat(Adsn888.class , getCurrentProcessState(), pdaAdsa888.getPnd_Parm_Area(), pdaAdsa888.getPnd_Nbr_Acct());                                   //Natural: CALLNAT 'ADSN888' #PARM-AREA #NBR-ACCT
                if (condition(Global.isEscape())) return;
                //*  WB - ACCRC END
                //* ***********************************************************************
                //* * INPUT - THIS ALLOWS THIS PGM TO RUN TWO DIFFERENT JOBS             **
                //* ***********************************************************************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Part_Code);                                                                                  //Natural: INPUT #INPUT-PART-CODE
                //*  TYPE OF RUN WE ARE REQUESTING.
                if (condition(pnd_Input_Part_Code.equals("REPT")))                                                                                                        //Natural: IF #INPUT-PART-CODE = 'REPT'
                {
                    //*  EITHER REPRINT OR PARTICIPANT.
                    pnd_Write_Sw.setValue("R");                                                                                                                           //Natural: MOVE 'R' TO #WRITE-SW
                    //*  IF 'REPT' ONLY THE REPRINT RUN WILL
                                                                                                                                                                          //Natural: PERFORM READ-POST-PARTICIP-RTN
                    sub_Read_Post_Particip_Rtn();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  BE EXEC. IF 'PART',FOR PARTICIPANT,
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IS IN THE JCL PARM ONLY PARTICIP-
                    if (condition(pnd_Input_Part_Code.equals("PART")))                                                                                                    //Natural: IF #INPUT-PART-CODE = 'PART'
                    {
                        //*  RTN WILL RUN.
                        pnd_Write_Sw.setValue(" ");                                                                                                                       //Natural: MOVE ' ' TO #WRITE-SW
                        //*  THERE ARE TWO SETS OF JCL.
                                                                                                                                                                          //Natural: PERFORM READ-PARTICIP-RTN
                        sub_Read_Particip_Rtn();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-POST-PARTICIP-RTN
                //*    * --------------------------------------------------------------- *
                //*    * R-REQ-ID-KEY READS ALL CONTRACTS FOR A PARTICIPANT              *
                //*    * --------------------------------------------------------------- *
                //*    * --------------------------------------------------------------- *
                //*    * MAKE SURE PARTICIPANT CONTRACT MATCHES REPRINT CONTRACT NUMBER  *
                //*    * --------------------------------------------------------------- *
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PARTICIP-RTN
                //* *--------------------------------------------------------------------**
                //* ********************************************************************
                //* * THIS RTN WILL PRINT OUT A REPORT ONLY IF CIS-OPN-CLSD-IND = O AND
                //* * THE CIS-STATUS-CD IS = TO I AND THE DATE IS LE THE BUSINESS DAY.
                //* ********************************************************************
                //* *
                //* *-----------***
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-AND-WRITE-RTN
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-FIRST-TPAIPRO-IND
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CURRENT-BUSINESS-DAY
                //* *--------------------------------------------------------------------**
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-BENEFICIARY
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-PDA
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NOBENE-REPORT
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-IA-TRANS-REPORT
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXCEPTION-REPORT
                //* ************* TOTALS AT END OF WORK FOR REPRINT JOB  ************
                if (condition(pnd_Write_Sw.equals("R")))                                                                                                                  //Natural: IF #WRITE-SW = 'R'
                {
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape())){return;}
                    getReports().write(2, new TabSetting(10),NEWLINE,"THESE RECORDS WERE READ FROM THE REPRINT FILE AND THE PARTICIPANT ","        FILE AND WRITTEN TO THE DOCUMERGE EXTRACT FILE.", //Natural: WRITE ( 2 ) 10T / 'THESE RECORDS WERE READ FROM THE REPRINT FILE AND THE PARTICIPANT ' '        FILE AND WRITTEN TO THE DOCUMERGE EXTRACT FILE.' //
                        NEWLINE,NEWLINE);
                    if (Global.isEscape()) return;
                    getReports().write(2, new TabSetting(1),"*****  TOTAL RE-PRINT RECORDS READ        =  ",pnd_Reprt_Counter, new ReportEditMask ("Z,ZZZ,ZZ9"),          //Natural: WRITE ( 2 ) 1T '*****  TOTAL RE-PRINT RECORDS READ        =  ' #REPRT-COUNTER ( EM = Z,ZZZ,ZZ9 ) /
                        NEWLINE);
                    if (Global.isEscape()) return;
                    getReports().write(2, new TabSetting(1),"*****  TOTAL PARTICIPANT RECORDS READ     =  ",pnd_Part_Counter, new ReportEditMask ("Z,ZZZ,ZZ9"),           //Natural: WRITE ( 2 ) 1T '*****  TOTAL PARTICIPANT RECORDS READ     =  ' #PART-COUNTER ( EM = Z,ZZZ,ZZ9 ) //
                        NEWLINE,NEWLINE);
                    if (Global.isEscape()) return;
                    //* ***********  TOTALS AT END OF WORK FOR PARTICIPANT JOB **********
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape())){return;}
                    getReports().write(2, new TabSetting(10),NEWLINE,"THESE RECORDS WERE READ FROM THE PARTICIPANT FILE ","    AND WRITTEN TO THE DOCUMERGE EXTRACT FILE.", //Natural: WRITE ( 2 ) 10T / 'THESE RECORDS WERE READ FROM THE PARTICIPANT FILE ' '    AND WRITTEN TO THE DOCUMERGE EXTRACT FILE.' //
                        NEWLINE,NEWLINE);
                    if (Global.isEscape()) return;
                    getReports().write(2, new TabSetting(1),"*****  TOTAL PARTICIPANT RECORDS READ     =  ",pnd_Part_Counter, new ReportEditMask ("Z,ZZZ,ZZ9"),           //Natural: WRITE ( 2 ) 1T '*****  TOTAL PARTICIPANT RECORDS READ     =  ' #PART-COUNTER ( EM = Z,ZZZ,ZZ9 ) /
                        NEWLINE);
                    if (Global.isEscape()) return;
                    getReports().write(2, new TabSetting(1),"*****  TOTAL PARTICIPANT RECORDS WRITTEN  =  ",pnd_Records_Written, new ReportEditMask ("Z,ZZZ,ZZ9"),        //Natural: WRITE ( 2 ) 1T '*****  TOTAL PARTICIPANT RECORDS WRITTEN  =  ' #RECORDS-WRITTEN ( EM = Z,ZZZ,ZZ9 ) //
                        NEWLINE,NEWLINE);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-NAME
                //* *--------------------------------------------------------------------**
                //* *---------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-NAME-IA
                //* *---------------------------------------------------------------------
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-MDO-TABLE
                //*  ITE(1) '='    #CIS-INVEST-AMT-TOTAL 'TOT FOR ALL ORIG CONTRACTS'
                //*   WRITE(1) '=' #CIS-INVEST-CREF-NUM-1(#X)
                //*  ITE(1) '=' #CIS-INVEST-TIAA-NUM-TO
                //*  ITE(1) '=' #CIS-INVEST-CREF-NUM-TO
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CORRECT-MDO-TABLE
                //* *--------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-NAME-MDO
                //* *--------------------------------------------------------------------**
            }                                                                                                                                                             //Natural: ON ERROR
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Read_Post_Particip_Rtn() throws Exception                                                                                                            //Natural: READ-POST-PARTICIP-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        //* ***********************************************************************
        //* * THIS RTN WILL PRINT OUT A REPORT IF THE TWO KEYS MATCH. IT DOES NOT
        //* * CARE IF THE STATUS IS OPEN, CLOSED, ETC ON THE PARTICIPANT FILE.
        //* ***********************************************************************
        //*  BY R-STATUS-IND = 'R'   /* REPRINT FILE
        ldaCisview1.getVw_cis_Prt_View().startDatabaseRead                                                                                                                //Natural: READ CIS-PRT-VIEW
        (
        "READ01",
        new Oc[] { new Oc("ISN", "ASC") }
        );
        READ01:
        while (condition(ldaCisview1.getVw_cis_Prt_View().readNextRow("READ01")))
        {
            //* *                        /* IF R-STATUS-IND NE 'R'  /* IF WE WANT TO
            //* *                        /*    ESCAPE BOTTOM        /* READ BY 'R' ONLY
            //* *                        /* END-IF                  /* USE THIS + THE
            //*  C367955 >>
            if (condition(ldaCisview1.getCis_Prt_View_R_Pin_Num().equals(getZero())))                                                                                     //Natural: IF R-PIN-NUM EQ 0
            {
                getWorkFiles().write(2, false, "ZERO PIN CONTRACT IN CIS:", ldaCisview1.getCis_Prt_View_R_Tiaa_Cntrct());                                                 //Natural: WRITE WORK FILE 2 "ZERO PIN CONTRACT IN CIS:" R-TIAA-CNTRCT
                getReports().write(0, "CONTRACT :",ldaCisview1.getCis_Prt_View_R_Tiaa_Cntrct(),"NOT extracted due to zero PIN.Bypassed until next batch cycle");          //Natural: WRITE "CONTRACT :" R-TIAA-CNTRCT "NOT extracted due to zero PIN.Bypassed until next batch cycle"
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  C367955 <<
            }                                                                                                                                                             //Natural: END-IF
            //*  GET BELOW.
            pnd_Reprt_Counter.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #REPRT-COUNTER
            //*  READ BY KEY PARTICIPANT FILE
            ldaCispartv.getVw_cis_Part_View().startDatabaseRead                                                                                                           //Natural: READ CIS-PART-VIEW BY CIS-RQST-ID-KEY = CIS-PRT-VIEW.R-REQ-ID-KEY
            (
            "READ02",
            new Wc[] { new Wc("CIS_RQST_ID_KEY", ">=", ldaCisview1.getCis_Prt_View_R_Req_Id_Key(), WcType.BY) },
            new Oc[] { new Oc("CIS_RQST_ID_KEY", "ASC") }
            );
            READ02:
            while (condition(ldaCispartv.getVw_cis_Part_View().readNextRow("READ02")))
            {
                //* *  IF CIS-RQST-ID-KEY NE CIS-PRT-VIEW.R-REQ-ID-KEY
                //* *    ESCAPE BOTTOM
                //* *  END-IF
                //*  JRB2 (BEG)
                //*  END SUPER
                short decideConditionsMet2751 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CIS-RQST-ID-KEY NE CIS-PRT-VIEW.R-REQ-ID-KEY
                if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id_Key().notEquals(ldaCisview1.getCis_Prt_View_R_Req_Id_Key())))
                {
                    decideConditionsMet2751++;
                    //*  TIAA ONLY OR BOTH
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: WHEN CIS-PRT-VIEW.R-TIAA-CNTRCT NE ' '
                else if (condition(ldaCisview1.getCis_Prt_View_R_Tiaa_Cntrct().notEquals(" ")))
                {
                    decideConditionsMet2751++;
                    if (condition(ldaCisview1.getCis_Prt_View_R_Tiaa_Cntrct().notEquals(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr())))                                    //Natural: IF CIS-PRT-VIEW.R-TIAA-CNTRCT NE CIS-PART-VIEW.CIS-TIAA-NBR
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  CREF ONLY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN CIS-PRT-VIEW.R-CREF-CNTRCT NE ' '
                else if (condition(ldaCisview1.getCis_Prt_View_R_Cref_Cntrct().notEquals(" ")))
                {
                    decideConditionsMet2751++;
                    if (condition(ldaCisview1.getCis_Prt_View_R_Cref_Cntrct().notEquals(ldaCispartv.getCis_Part_View_Cis_Cert_Nbr())))                                    //Natural: IF CIS-PRT-VIEW.R-CREF-CNTRCT NE CIS-PART-VIEW.CIS-CERT-NBR
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                    //*  JRB2 (END)
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Part_Counter.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PART-COUNTER
                if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().notEquals("TPI") && ldaCispartv.getCis_Part_View_Cis_Rqst_Id().notEquals("IPI")))                //Natural: IF CIS-PART-VIEW.CIS-RQST-ID NE 'TPI' AND CIS-PART-VIEW.CIS-RQST-ID NE 'IPI'
                {
                                                                                                                                                                          //Natural: PERFORM MOVE-AND-WRITE-RTN
                    sub_Move_And_Write_Rtn();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    getWorkFiles().write(1, false, ldaCislpart.getPnd_Participant_Work_File());                                                                           //Natural: WRITE WORK FILE 1 #PARTICIPANT-WORK-FILE
                    pnd_Records_Written.nadd(1);                                                                                                                          //Natural: ADD 1 TO #RECORDS-WRITTEN
                    //* *
                    getReports().write(2, "TIAA CNTRCT NUM           = ",new TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr(),NEWLINE,"CREF CNTRCT NUM           = ",new  //Natural: WRITE ( 2 ) 'TIAA CNTRCT NUM           = ' 30T CIS-TIAA-NBR / 'CREF CNTRCT NUM           = ' 30T CIS-CERT-NBR / 'KEY FOR CIS               = ' 30T CIS-RQST-ID-KEY / 'SOCIAL SECURITY NUM       = ' 30T #REPORT-SSN / 'CIS STATUS IND            = ' 30T CIS-STATUS-CD / 'MIT STATUS CODE           = ' 30T R-MIT-STATUS-CODE / 'MIT WPID                  = ' 30T R-MIT-WPID / 'MIT-ORG-UNIT-CODE         = ' 30T CIS-MIT-ORIGINAL-UNIT-CDE / 'MIT LOG DTE TIM           = ' 30T R-MIT-LOG-DATE-TIME / 'CITIZENSHIP CODE          = ' 30T #MSG ///
                        TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Cert_Nbr(),NEWLINE,"KEY FOR CIS               = ",new TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Rqst_Id_Key(),NEWLINE,"SOCIAL SECURITY NUM       = ",new 
                        TabSetting(30),pnd_Report_Ssn,NEWLINE,"CIS STATUS IND            = ",new TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Status_Cd(),NEWLINE,"MIT STATUS CODE           = ",new 
                        TabSetting(30),ldaCisview1.getCis_Prt_View_R_Mit_Status_Code(),NEWLINE,"MIT WPID                  = ",new TabSetting(30),ldaCisview1.getCis_Prt_View_R_Mit_Wpid(),NEWLINE,"MIT-ORG-UNIT-CODE         = ",new 
                        TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Mit_Original_Unit_Cde(),NEWLINE,"MIT LOG DTE TIM           = ",new TabSetting(30),ldaCisview1.getCis_Prt_View_R_Mit_Log_Date_Time(),NEWLINE,"CITIZENSHIP CODE          = ",new 
                        TabSetting(30),pnd_Msg,NEWLINE,NEWLINE,NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Particip_Rtn() throws Exception                                                                                                                 //Natural: READ-PARTICIP-RTN
    {
        if (BLNatReinput.isReinput()) return;

        ldaCispartv.getVw_cis_Part_View().startDatabaseRead                                                                                                               //Natural: READ CIS-PART-VIEW BY CIS-OPEN-STATUS STARTING FROM 'OI'
        (
        "PND_PND_L0980",
        new Wc[] { new Wc("CIS_OPEN_STATUS", ">=", "OI", WcType.BY) },
        new Oc[] { new Oc("CIS_OPEN_STATUS", "ASC") }
        );
        PND_PND_L0980:
        while (condition(ldaCispartv.getVw_cis_Part_View().readNextRow("PND_PND_L0980")))
        {
            //*  C367955 >>
            if (condition(ldaCispartv.getCis_Part_View_Cis_Pin_Nbr().equals(getZero())))                                                                                  //Natural: IF CIS-PIN-NBR EQ 0
            {
                getWorkFiles().write(2, false, "ZERO PIN CONTRACT IN CIS:", ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr());                                                 //Natural: WRITE WORK FILE 2 "ZERO PIN CONTRACT IN CIS:" CIS-TIAA-NBR
                getReports().write(0, "CONTRACT :",ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr(),"NOT extracted due to zero PIN.Bypassed until next batch cycle");          //Natural: WRITE "CONTRACT :" CIS-TIAA-NBR "NOT extracted due to zero PIN.Bypassed until next batch cycle"
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L0980"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0980"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*   C367955 <<
            }                                                                                                                                                             //Natural: END-IF
            pnd_Print_Date.setValueEdited(ldaCispartv.getCis_Part_View_Cis_Cntrct_Print_Dte(),new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED CIS-CNTRCT-PRINT-DTE ( EM = YYYYMMDD ) TO #PRINT-DATE
            if (condition(pnd_Print_Date.equals("0") || pnd_Print_Date.equals(" ")))                                                                                      //Natural: IF #PRINT-DATE = '0' OR = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //* ************************************
            if (condition(ldaCispartv.getCis_Part_View_Cis_Opn_Clsd_Ind().equals("O") && ldaCispartv.getCis_Part_View_Cis_Status_Cd().equals("I")))                       //Natural: IF CIS-OPN-CLSD-IND = 'O' AND CIS-STATUS-CD = 'I'
            {
                ignore();
                //*  IF  CIS-OPN-CLSD-IND = 'O' &
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  CIS-STATUS-CD = 'I', AND THE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  DATE IS LE BUSINESS DATE
            }                                                                                                                                                             //Natural: END-IF
            //* **                               /* WE WANT TO WRITE THE RECORD.
            //*   WRITE '=' #PRINT-DATE '=' #BUSINESS-DATE
            //*  IF THE PRINT DATE GT BUSINESS DATE
            //*  WE WANT TO READ THE NEXT RECORD,
            if (condition(pnd_Print_Date.lessOrEqual(pnd_Business_Date)))                                                                                                 //Natural: IF #PRINT-DATE LE #BUSINESS-DATE
            {
                ignore();
                //*  SO WE ESCAPE TOP. IF NOT 'O' & 'I'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  WE WILL ESCAPE BOTTOM.
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //* ************************************
            }                                                                                                                                                             //Natural: END-IF
            //*  BIP  UPDATE THE BIP ACCOUNTS TO
            //*  BIP  STATUS OF CLOSE AND PRINTED.
            //*  BIP  BIP ACCOUNTS WILL BE EXCLUDED
            //*  BIP  IN THE PACKAGE GENERATION.
            if (condition(ldaCispartv.getCis_Part_View_Cis_Decedent_Contract().greater(" ")))                                                                             //Natural: IF CIS-DECEDENT-CONTRACT GT ' '
            {
                PND_PND_L1034:                                                                                                                                            //Natural: GET CIS-PART-VIEW *ISN ( ##L0980. )
                ldaCispartv.getVw_cis_Part_View().readByID(ldaCispartv.getVw_cis_Part_View().getAstISN("PND_PND_L0980"), "PND_PND_L1034");
                ldaCispartv.getCis_Part_View_Cis_Opn_Clsd_Ind().setValue("C");                                                                                            //Natural: ASSIGN CIS-OPN-CLSD-IND := 'C'
                ldaCispartv.getCis_Part_View_Cis_Status_Cd().setValue("P");                                                                                               //Natural: ASSIGN CIS-STATUS-CD := 'P'
                //*  BIP
                ldaCispartv.getVw_cis_Part_View().updateDBRow("PND_PND_L1034");                                                                                           //Natural: UPDATE ( ##L1034. )
                //*  BIP
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                //*  BIP
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  BIP
            }                                                                                                                                                             //Natural: END-IF
            //*                                                             /* 20150511
            pnd_Cref_Date_Of_Issue.setValueEdited(ldaCispartv.getCis_Part_View_Cis_Cref_Doi(),new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED CIS-CREF-DOI ( EM = YYYYMMDD ) TO #CREF-DATE-OF-ISSUE
            //*                                                             /* 20150511
            if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().notEquals("TPI") && ldaCispartv.getCis_Part_View_Cis_Rqst_Id().notEquals("IPI")))                    //Natural: IF CIS-PART-VIEW.CIS-RQST-ID NE 'TPI' AND CIS-PART-VIEW.CIS-RQST-ID NE 'IPI'
            {
                pnd_Part_Counter.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #PART-COUNTER
                                                                                                                                                                          //Natural: PERFORM MOVE-AND-WRITE-RTN
                sub_Move_And_Write_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L0980"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0980"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                getWorkFiles().write(1, false, ldaCislpart.getPnd_Participant_Work_File());                                                                               //Natural: WRITE WORK FILE 1 #PARTICIPANT-WORK-FILE
                                                                                                                                                                          //Natural: PERFORM UPDATE-BENEFICIARY
                sub_Update_Beneficiary();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L0980"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0980"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                pnd_Records_Written.nadd(1);                                                                                                                              //Natural: ADD 1 TO #RECORDS-WRITTEN
                PND_PND_L1068:                                                                                                                                            //Natural: GET CIS-PART-VIEW *ISN ( ##L0980. )
                ldaCispartv.getVw_cis_Part_View().readByID(ldaCispartv.getVw_cis_Part_View().getAstISN("PND_PND_L0980"), "PND_PND_L1068");
                ldaCispartv.getCis_Part_View_Cis_Mit_Work_Process_Id().setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Wpid());                                 //Natural: MOVE #MIT-WPID TO CIS-MIT-WORK-PROCESS-ID
                ldaCispartv.getCis_Part_View_Cis_Mit_Status_Cde().setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Status_Cde());                                //Natural: MOVE #MIT-STATUS-CDE TO CIS-MIT-STATUS-CDE
                ldaCispartv.getCis_Part_View_Cis_Mit_Original_Unit_Cde().setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Org_Unit_Cde());                       //Natural: MOVE #MIT-ORG-UNIT-CDE TO CIS-MIT-ORIGINAL-UNIT-CDE
                ldaCispartv.getCis_Part_View_Cis_Mit_Step_Id().setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Step_Id());                                      //Natural: MOVE #MIT-STEP-ID TO CIS-MIT-STEP-ID
                ldaCispartv.getCis_Part_View_Cis_Extract_Date().setValue(Global.getDATX());                                                                               //Natural: MOVE *DATX TO CIS-EXTRACT-DATE
                ldaCispartv.getCis_Part_View_Cis_Tiaa_Cntrct_Type().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1));                      //Natural: MOVE #FROM-APPROVAL-IND-INPUT ( 1 ) TO CIS-PART-VIEW.CIS-TIAA-CNTRCT-TYPE
                ldaCispartv.getCis_Part_View_Cis_Cref_Cntrct_Type().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2));                      //Natural: MOVE #FROM-APPROVAL-IND-INPUT ( 2 ) TO CIS-PART-VIEW.CIS-CREF-CNTRCT-TYPE
                ldaCispartv.getCis_Part_View_Cis_Rea_Cntrct_Type().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input.getValue(3));                        //Natural: MOVE #RES-APPROVAL-IND-INPUT ( 3 ) TO CIS-PART-VIEW.CIS-REA-CNTRCT-TYPE
                ldaCispartv.getVw_cis_Part_View().updateDBRow("PND_PND_L1068");                                                                                           //Natural: UPDATE ( ##L1068. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                //*  ADD 1 TO #UPDATE-CNT
                getReports().write(2, "TIAA CNTRCT NUM           = ",new TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr(),NEWLINE,"CREF CNTRCT NUM           = ",new  //Natural: WRITE ( 2 ) 'TIAA CNTRCT NUM           = ' 30T CIS-TIAA-NBR / 'CREF CNTRCT NUM           = ' 30T CIS-CERT-NBR / 'KEY FOR CIS               = ' 30T CIS-RQST-ID-KEY / 'SOCIAL SECURITY NUM       = ' 30T #REPORT-SSN / 'CIS STATUS IND            = ' 30T CIS-STATUS-CD / 'MIT STATUS CODE           = ' 30T #MIT-STATUS-CDE / 'MIT WPID                  = ' 30T #MIT-WPID / 'MIT-ORG-UNIT-CODE         = ' 30T #MIT-ORG-UNIT-CDE / 'MIT LOG DTE TIM           = ' 30T #MIT-LOG-DTE-TME / 'CITIZENSHIP CODE          = ' 30T #MSG ///
                    TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Cert_Nbr(),NEWLINE,"KEY FOR CIS               = ",new TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Rqst_Id_Key(),NEWLINE,"SOCIAL SECURITY NUM       = ",new 
                    TabSetting(30),pnd_Report_Ssn,NEWLINE,"CIS STATUS IND            = ",new TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Status_Cd(),NEWLINE,"MIT STATUS CODE           = ",new 
                    TabSetting(30),ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Status_Cde(),NEWLINE,"MIT WPID                  = ",new TabSetting(30),ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Wpid(),NEWLINE,"MIT-ORG-UNIT-CODE         = ",new 
                    TabSetting(30),ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Org_Unit_Cde(),NEWLINE,"MIT LOG DTE TIM           = ",new TabSetting(30),ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Log_Dte_Tme(),NEWLINE,"CITIZENSHIP CODE          = ",new 
                    TabSetting(30),pnd_Msg,NEWLINE,NEWLINE,NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L0980"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0980"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 4 ) ' '
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 4 ) ' '
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,"Total Number of No Beneficiary Records : ",pnd_No_Bene);                                                              //Natural: WRITE ( 4 ) 'Total Number of No Beneficiary Records : ' #NO-BENE
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(52),"*** E N D  O F  R E P O R T ***");                                                 //Natural: WRITE ( 4 ) // 52T '*** E N D  O F  R E P O R T ***'
        if (Global.isEscape()) return;
        getReports().write(5, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 5 ) ' '
        if (Global.isEscape()) return;
        getReports().write(5, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 5 ) ' '
        if (Global.isEscape()) return;
        getReports().write(5, ReportOption.NOTITLE,"Total Number of IA Transfer Records : ",pnd_Total_Xfer);                                                              //Natural: WRITE ( 5 ) 'Total Number of IA Transfer Records : ' #TOTAL-XFER
        if (Global.isEscape()) return;
        getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(52),"*** E N D  O F  R E P O R T ***");                                                 //Natural: WRITE ( 5 ) // 52T '*** E N D  O F  R E P O R T ***'
        if (Global.isEscape()) return;
    }
    private void sub_Move_And_Write_Rtn() throws Exception                                                                                                                //Natural: MOVE-AND-WRITE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Type().setValue(ldaCispartv.getCis_Part_View_Cis_Cntrct_Type());                                              //Natural: MOVE CIS-CNTRCT-TYPE TO #CNTRCT-TYPE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Approval().setValue(ldaCispartv.getCis_Part_View_Cis_Cntrct_Apprvl_Ind());                                    //Natural: MOVE CIS-CNTRCT-APPRVL-IND TO #CNTRCT-APPROVAL
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Sys_Id().setValue(ldaCispartv.getCis_Part_View_Cis_Rqst_Id());                                                //Natural: MOVE CIS-RQST-ID TO #CNTRCT-SYS-ID
        //*  WB - ACCRC
        //*  WB - ACCRC
        //*  WB - ACCRC
        //*  WB - ACCRC
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Ia_Share_Class().reset();                                                                                        //Natural: RESET #CIS-IA-SHARE-CLASS #CIS-PLAN-NUM #CIS-MULTI-PLAN-NO ( * ) #CIS-MULTI-SUB-PLAN ( * )
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Plan_Num().reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Multi_Plan_No().getValue("*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Multi_Sub_Plan().getValue("*").reset();
        //*  WB - ACCRC
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Multi_Plan_Ind().setValue("N");                                                                                  //Natural: MOVE 'N' TO #CIS-MULTI-PLAN-IND
        //*  060706 START
        if (condition(DbsUtil.maskMatches(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Sys_Id(),"'MDOM'") || DbsUtil.maskMatches(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Sys_Id(), //Natural: IF #CNTRCT-SYS-ID = MASK ( 'MDOM' ) OR = MASK ( 'MDOL' )
            "'MDOL'")))
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Sys_Id().setValue("MDO");                                                                                 //Natural: ASSIGN #CNTRCT-SYS-ID := 'MDO'
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Filler().setValue(ldaCispartv.getCis_Part_View_Cis_Rqst_Id());                                               //Natural: ASSIGN #CIS-FILLER := CIS-RQST-ID
            //*  060706 END
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Option().setValue(ldaCispartv.getCis_Part_View_Cis_Annty_Option());                                           //Natural: MOVE CIS-ANNTY-OPTION TO #CNTRCT-OPTION
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Cntrct_Cash_Status().setValue(ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Cash_Status());                      //Natural: MOVE CIS-MDO-CONTRACT-CASH-STATUS TO #MDO-CNTRCT-CASH-STATUS
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Retr_Surv_Status().setValue(ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Type());                            //Natural: MOVE CIS-MDO-CONTRACT-TYPE TO #CNTRCT-RETR-SURV-STATUS
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Original_Issue_Stat_Cd().setValue(" ");                                                                              //Natural: MOVE ' ' TO #ORIGINAL-ISSUE-STAT-CD
        //*  CONVERT ORIG-STATE-CODE TO ALPHA & GET STATE NAME
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Name().setValue(" ");                                                                                  //Natural: MOVE ' ' TO #STAT-OF-ISSUE-NAME
        FOR01:                                                                                                                                                            //Natural: FOR #X 1 62
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(62)); pnd_X.nadd(1))
        {
            if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(pnd_X).equals(ldaCispartv.getCis_Part_View_Cis_Orig_Issue_State())))             //Natural: IF #STATE-CODE-N ( #X ) = CIS-ORIG-ISSUE-STATE
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Original_Issue_Stat_Cd().setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_X));    //Natural: MOVE #STATE-CODE-A ( #X ) TO #ORIGINAL-ISSUE-STAT-CD
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Name().setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Name().getValue(pnd_X));          //Natural: MOVE #STATE-NAME ( #X ) TO #STAT-OF-ISSUE-NAME
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_X).equals(ldaCispartv.getCis_Part_View_Cis_Orig_Issue_State())))         //Natural: IF #STATE-CODE-A ( #X ) = CIS-ORIG-ISSUE-STATE
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Original_Issue_Stat_Cd().setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_X)); //Natural: MOVE #STATE-CODE-A ( #X ) TO #ORIGINAL-ISSUE-STAT-CD
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Name().setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Name().getValue(pnd_X));      //Natural: MOVE #STATE-NAME ( #X ) TO #STAT-OF-ISSUE-NAME
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CONVERT STATE-CODE TO ALPHA
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code().setValue(" ");                                                                                  //Natural: MOVE ' ' TO #STAT-OF-ISSUE-CODE
        FOR02:                                                                                                                                                            //Natural: FOR #X 1 62
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(62)); pnd_X.nadd(1))
        {
            if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_N().getValue(pnd_X).equals(ldaCispartv.getCis_Part_View_Cis_Issue_State_Cd())))               //Natural: IF #STATE-CODE-N ( #X ) = CIS-ISSUE-STATE-CD
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code().setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_X));        //Natural: MOVE #STATE-CODE-A ( #X ) TO #STAT-OF-ISSUE-CODE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_X).equals(ldaCispartv.getCis_Part_View_Cis_Issue_State_Cd())))           //Natural: IF #STATE-CODE-A ( #X ) = CIS-ISSUE-STATE-CD
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code().setValue(ldaAppl170.getPnd_State_Code_Table_Pnd_State_Code_A().getValue(pnd_X));    //Natural: MOVE #STATE-CODE-A ( #X ) TO #STAT-OF-ISSUE-CODE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ******** CM 3/05/10
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Four_Fifty_Seven_Ind().setValue(ldaCispartv.getCis_Part_View_Cis_Four_Fifty_Seven_Ind());                        //Natural: MOVE CIS-FOUR-FIFTY-SEVEN-IND TO #CIS-FOUR-FIFTY-SEVEN-IND
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Inst_Name().setValue(ldaCispartv.getCis_Part_View_Cis_Institution_Name());                                           //Natural: MOVE CIS-INSTITUTION-NAME TO #INST-NAME
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Cnt_Tiaa().reset();                                                                                        //Natural: RESET #CIS-TRNSF-CNT-TIAA #CIS-TRNSF-CNT-SEPA #CIS-TRNSF-INFO ( * ) #CIS-TRNSF-INFO-TBL ( * )
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Cnt_Sepa().reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Info().getValue("*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Info_Tbl().getValue("*").reset();
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(20)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Trnsf_Ticker().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).notEquals(" ")))                                //Natural: IF CIS-TRNSF-TICKER ( #I ) NE ' '
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Ticker().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(ldaCispartv.getCis_Part_View_Cis_Trnsf_Ticker().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-TRNSF-TICKER ( #I ) := CIS-TRNSF-TICKER ( #I )
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Reval_Type().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(ldaCispartv.getCis_Part_View_Cis_Trnsf_Reval_Type().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-TRNSF-REVAL-TYPE ( #I ) := CIS-TRNSF-REVAL-TYPE ( #I )
                if (condition(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Reval_Type().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("A")))           //Natural: IF #CIS-TRNSF-REVAL-TYPE ( #I ) = 'A'
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Reval_Type_Name().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue("Annually");              //Natural: ASSIGN #CIS-REVAL-TYPE-NAME ( #I ) := 'Annually'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Reval_Type().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("M")))           //Natural: IF #CIS-TRNSF-REVAL-TYPE ( #I ) = 'M'
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Reval_Type_Name().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue("Monthly");               //Natural: ASSIGN #CIS-REVAL-TYPE-NAME ( #I ) := 'Monthly'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Reval_Type().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("S")))           //Natural: IF #CIS-TRNSF-REVAL-TYPE ( #I ) = 'S'
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Reval_Type_Name().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue("Semi-Annually");         //Natural: ASSIGN #CIS-REVAL-TYPE-NAME ( #I ) := 'Semi-Annually'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Reval_Type().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("B")))           //Natural: IF #CIS-TRNSF-REVAL-TYPE ( #I ) = 'B'
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Reval_Type_Name().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue("Semi-Monthly");          //Natural: ASSIGN #CIS-REVAL-TYPE-NAME ( #I ) := 'Semi-Monthly'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Reval_Type().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("Q")))           //Natural: IF #CIS-TRNSF-REVAL-TYPE ( #I ) = 'Q'
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Reval_Type_Name().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue("Quarterly");             //Natural: ASSIGN #CIS-REVAL-TYPE-NAME ( #I ) := 'Quarterly'
                }                                                                                                                                                         //Natural: END-IF
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Cref_Units().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Trnsf_Cref_Units().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),new  //Natural: MOVE EDITED CIS-TRNSF-CREF-UNITS ( #I ) ( EM = ZZZ9.999 ) TO #CIS-TRNSF-CREF-UNITS ( #I )
                    ReportEditMask("ZZZ9.999"));
                pdaNeca4000.getNeca4000().reset();                                                                                                                        //Natural: RESET NECA4000
                pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                      //Natural: ASSIGN REQUEST-IND := ' '
                pdaNeca4000.getNeca4000_Function_Cde().setValue("FDS");                                                                                                   //Natural: ASSIGN FUNCTION-CDE := 'FDS'
                pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                             //Natural: ASSIGN INPT-KEY-OPTION-CDE := '01'
                //*  CREA
                getReports().write(0, "=",ldaCispartv.getCis_Part_View_Cis_Trnsf_Ticker().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()),"=",ldaCispartv.getCis_Part_View_Cis_Trnsf_Rece_Company().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: WRITE '=' CIS-TRNSF-TICKER ( #I ) '=' CIS-TRNSF-RECE-COMPANY ( #I )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaNeca4000.getNeca4000_Fds_Key_Ticker_Symbol().setValue(ldaCispartv.getCis_Part_View_Cis_Trnsf_Ticker().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN FDS-KEY-TICKER-SYMBOL := CIS-TRNSF-TICKER ( #I )
                pdaNeca4000.getNeca4000_Fds_Key_Fund_Name_Ref_Cde().setValue("18");                                                                                       //Natural: ASSIGN FDS-KEY-FUND-NAME-REF-CDE := '18'
                DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                    //Natural: CALLNAT 'NECN4000' NECA4000
                if (condition(Global.isEscape())) return;
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Ticker_Name().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1)); //Natural: ASSIGN #CIS-TRNSF-TICKER-NAME ( #I ) := NECA4000.FDS-FUND-NME ( 1 )
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Rece_Company().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).setValue(ldaCispartv.getCis_Part_View_Cis_Trnsf_Rece_Company().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: ASSIGN #CIS-TRNSF-RECE-COMPANY ( #I ) := CIS-TRNSF-RECE-COMPANY ( #I )
                getReports().write(0, "=",pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1),NEWLINE,"=",ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Ticker_Name().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I())); //Natural: WRITE '=' NECA4000.FDS-FUND-NME ( 1 ) / '=' #CIS-TRNSF-TICKER-NAME ( #I )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaCispartv.getCis_Part_View_Cis_Trnsf_Rece_Company().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("TIAA")))                      //Natural: IF CIS-TRNSF-RECE-COMPANY ( #I ) = 'TIAA'
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Cnt_Tiaa().nadd(1);                                                                            //Natural: ADD 1 TO #CIS-TRNSF-CNT-TIAA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCispartv.getCis_Part_View_Cis_Trnsf_Rece_Company().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).equals("SEPA")))                      //Natural: IF CIS-TRNSF-RECE-COMPANY ( #I ) = 'SEPA'
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Trnsf_Cnt_Sepa().nadd(1);                                                                            //Natural: ADD 1 TO #CIS-TRNSF-CNT-SEPA
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ******** CM - END
        //* ******** BY KAR MOD BY 1/13/98
        pdaCisa1000.getCisa1000().reset();                                                                                                                                //Natural: RESET CISA1000 #CIS-TIAA-CNTRCT-NBR-INPUT ( * ) #TIAA-CNTRCT-TYPE #REA-CNTRCT-TYPE #CREF-CNTRCT-TYPE #TIAA-DATE-N
        pnd_Cis_Tiaa_Cntrct_Nbr_Input.getValue("*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Type().reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Cntrct_Type().reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Type().reset();
        pnd_Tiaa_Date_N.reset();
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().equals("N")))                              //Natural: IF CIS-RQST-ID = 'IA' AND CIS-TRNSF-FLAG EQ 'N'
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Cntrct_Apprvl_Ind().equals("Y") || ldaCispartv.getCis_Part_View_Cis_Cntrct_Apprvl_Ind().equals("N")))          //Natural: IF CIS-CNTRCT-APPRVL-IND = 'Y' OR = 'N'
            {
                short decideConditionsMet2980 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF CIS-CNTRCT-APPRVL-IND;//Natural: VALUE 'Y'
                if (condition((ldaCispartv.getCis_Part_View_Cis_Cntrct_Apprvl_Ind().equals("Y"))))
                {
                    decideConditionsMet2980++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Type().setValue("A");                                                                        //Natural: MOVE 'A' TO #TIAA-CNTRCT-TYPE CIS-PART-VIEW.CIS-TIAA-CNTRCT-TYPE
                    ldaCispartv.getCis_Part_View_Cis_Tiaa_Cntrct_Type().setValue("A");
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Type().setValue("A");                                                                        //Natural: MOVE 'A' TO #CREF-CNTRCT-TYPE CIS-PART-VIEW.CIS-CREF-CNTRCT-TYPE
                    ldaCispartv.getCis_Part_View_Cis_Cref_Cntrct_Type().setValue("A");
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Cntrct_Type().setValue("A");                                                                         //Natural: MOVE 'A' TO #REA-CNTRCT-TYPE CIS-PART-VIEW.CIS-REA-CNTRCT-TYPE
                    ldaCispartv.getCis_Part_View_Cis_Rea_Cntrct_Type().setValue("A");
                }                                                                                                                                                         //Natural: VALUE 'N'
                else if (condition((ldaCispartv.getCis_Part_View_Cis_Cntrct_Apprvl_Ind().equals("N"))))
                {
                    decideConditionsMet2980++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Type().setValue("S");                                                                        //Natural: MOVE 'S' TO #TIAA-CNTRCT-TYPE CIS-PART-VIEW.CIS-TIAA-CNTRCT-TYPE
                    ldaCispartv.getCis_Part_View_Cis_Tiaa_Cntrct_Type().setValue("S");
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Type().setValue("S");                                                                        //Natural: MOVE 'S' TO #CREF-CNTRCT-TYPE CIS-PART-VIEW.CIS-CREF-CNTRCT-TYPE
                    ldaCispartv.getCis_Part_View_Cis_Cref_Cntrct_Type().setValue("S");
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Cntrct_Type().setValue("A");                                                                         //Natural: MOVE 'A' TO #REA-CNTRCT-TYPE CIS-PART-VIEW.CIS-REA-CNTRCT-TYPE
                    ldaCispartv.getCis_Part_View_Cis_Rea_Cntrct_Type().setValue("A");
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input.getValue(1).setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code());                //Natural: MOVE #PARTICIPANT-WORK-FILE.#STAT-OF-ISSUE-CODE TO #RES-STATE-INPUT ( 1 )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Original_Issue_Stat_Cd());           //Natural: MOVE #PARTICIPANT-WORK-FILE.#ORIGINAL-ISSUE-STAT-CD TO #FROM-STATE-INPUT ( 1 )
                pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*")); //Natural: MOVE #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * ) TO CISA1000.#CIS-TIAA-CNTRCT-NBR ( * )
                pdaCisa1000.getCisa1000_Pnd_Cis_Requestor().setValue("IA");                                                                                               //Natural: ASSIGN CISA1000.#CIS-REQUESTOR := 'IA'
                pdaCisa1000.getCisa1000_Pnd_Cis_Assign_Issue_Cntrct_Ind().setValue("T");                                                                                  //Natural: ASSIGN CISA1000.#CIS-ASSIGN-ISSUE-CNTRCT-IND := 'T'
                pnd_Tiaa_Date_N_Pnd_Tiaa_Date_A.setValueEdited(ldaCispartv.getCis_Part_View_Cis_Tiaa_Doi(),new ReportEditMask("YYYYMMDD"));                               //Natural: MOVE EDITED CIS-TIAA-DOI ( EM = YYYYMMDD ) TO #TIAA-DATE-A
                pdaCisa1000.getCisa1000_Pnd_Cis_Issue_Dt().setValue(pnd_Tiaa_Date_N);                                                                                     //Natural: ASSIGN CISA1000.#CIS-ISSUE-DT := #TIAA-DATE-N
                pdaCisa1000.getCisa1000_Pnd_Cis_Functions().getValue(10).setValue("YO");                                                                                  //Natural: ASSIGN CISA1000.#CIS-FUNCTIONS ( 10 ) := 'YO'
                DbsUtil.callnat(Cisn1000.class , getCurrentProcessState(), pdaCisa1000.getCisa1000(), pdaCisa1000.getPnd_Cis_Misc());                                     //Natural: CALLNAT 'CISN1000' CISA1000 #CIS-MISC
                if (condition(Global.isEscape())) return;
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*").setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*")); //Natural: MOVE CISA1000.#CIS-TIAA-CNTRCT-NBR ( * ) TO #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Type().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1));          //Natural: MOVE #FROM-APPROVAL-IND-INPUT ( 1 ) TO #TIAA-CNTRCT-TYPE CIS-PART-VIEW.CIS-TIAA-CNTRCT-TYPE
                ldaCispartv.getCis_Part_View_Cis_Tiaa_Cntrct_Type().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1));
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Type().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2));          //Natural: MOVE #FROM-APPROVAL-IND-INPUT ( 2 ) TO #CREF-CNTRCT-TYPE CIS-PART-VIEW.CIS-CREF-CNTRCT-TYPE
                ldaCispartv.getCis_Part_View_Cis_Cref_Cntrct_Type().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2));
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Cntrct_Type().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input.getValue(3));            //Natural: MOVE #RES-APPROVAL-IND-INPUT ( 3 ) TO #REA-CNTRCT-TYPE CIS-PART-VIEW.CIS-REA-CNTRCT-TYPE
                ldaCispartv.getCis_Part_View_Cis_Rea_Cntrct_Type().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input.getValue(3));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().equals("Y")))                              //Natural: IF CIS-RQST-ID = 'IA' AND CIS-TRNSF-FLAG EQ 'Y'
        {
            //*  KG TRAN START
            if (condition(ldaCispartv.getCis_Part_View_Cis_Tiaa_Cntrct_Type().equals("C")))                                                                               //Natural: IF CIS-TIAA-CNTRCT-TYPE = 'C'
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input.getValue(1).setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code());                //Natural: MOVE #PARTICIPANT-WORK-FILE.#STAT-OF-ISSUE-CODE TO #RES-STATE-INPUT ( 1 )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Original_Issue_Stat_Cd());           //Natural: MOVE #PARTICIPANT-WORK-FILE.#ORIGINAL-ISSUE-STAT-CD TO #FROM-STATE-INPUT ( 1 )
                pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*")); //Natural: MOVE #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * ) TO CISA1000.#CIS-TIAA-CNTRCT-NBR ( * )
                pdaCisa1000.getCisa1000_Pnd_Cis_Requestor().setValue("IA");                                                                                               //Natural: ASSIGN CISA1000.#CIS-REQUESTOR := 'IA'
                pdaCisa1000.getCisa1000_Pnd_Cis_Assign_Issue_Cntrct_Ind().setValue("T");                                                                                  //Natural: ASSIGN CISA1000.#CIS-ASSIGN-ISSUE-CNTRCT-IND := 'T'
                pnd_Tiaa_Date_N_Pnd_Tiaa_Date_A.setValueEdited(ldaCispartv.getCis_Part_View_Cis_Tiaa_Doi(),new ReportEditMask("YYYYMMDD"));                               //Natural: MOVE EDITED CIS-TIAA-DOI ( EM = YYYYMMDD ) TO #TIAA-DATE-A
                pdaCisa1000.getCisa1000_Pnd_Cis_Issue_Dt().setValue(pnd_Tiaa_Date_N);                                                                                     //Natural: ASSIGN CISA1000.#CIS-ISSUE-DT := #TIAA-DATE-N
                pdaCisa1000.getCisa1000_Pnd_Cis_Functions().getValue(10).setValue("YO");                                                                                  //Natural: ASSIGN CISA1000.#CIS-FUNCTIONS ( 10 ) := 'YO'
                DbsUtil.callnat(Cisn1000.class , getCurrentProcessState(), pdaCisa1000.getCisa1000(), pdaCisa1000.getPnd_Cis_Misc());                                     //Natural: CALLNAT 'CISN1000' CISA1000 #CIS-MISC
                if (condition(Global.isEscape())) return;
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*").setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*")); //Natural: MOVE CISA1000.#CIS-TIAA-CNTRCT-NBR ( * ) TO #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Type().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1));          //Natural: MOVE #FROM-APPROVAL-IND-INPUT ( 1 ) TO #TIAA-CNTRCT-TYPE
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Cntrct_Type().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input.getValue(3));            //Natural: MOVE #RES-APPROVAL-IND-INPUT ( 3 ) TO #REA-CNTRCT-TYPE
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(ldaCispartv.getCis_Part_View_Cis_Cref_Cntrct_Type());                      //Natural: MOVE CIS-PART-VIEW.CIS-CREF-CNTRCT-TYPE TO #FROM-APPROVAL-IND-INPUT ( 2 ) #CREF-CNTRCT-TYPE
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Type().setValue(ldaCispartv.getCis_Part_View_Cis_Cref_Cntrct_Type());
                //*  KG TRAN END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Cntrct_Type());                      //Natural: MOVE CIS-PART-VIEW.CIS-TIAA-CNTRCT-TYPE TO #FROM-APPROVAL-IND-INPUT ( 1 ) #TIAA-CNTRCT-TYPE
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Type().setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Cntrct_Type());
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(ldaCispartv.getCis_Part_View_Cis_Cref_Cntrct_Type());                      //Natural: MOVE CIS-PART-VIEW.CIS-CREF-CNTRCT-TYPE TO #FROM-APPROVAL-IND-INPUT ( 2 ) #CREF-CNTRCT-TYPE
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Type().setValue(ldaCispartv.getCis_Part_View_Cis_Cref_Cntrct_Type());
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input.getValue(3).setValue(ldaCispartv.getCis_Part_View_Cis_Rea_Cntrct_Type());                        //Natural: MOVE CIS-PART-VIEW.CIS-REA-CNTRCT-TYPE TO #RES-APPROVAL-IND-INPUT ( 3 ) #REA-CNTRCT-TYPE
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Cntrct_Type().setValue(ldaCispartv.getCis_Part_View_Cis_Rea_Cntrct_Type());
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ***************** BY KAR 01/14/98
        pdaCisa600.getCisa600().reset();                                                                                                                                  //Natural: RESET CISA600
        pdaCisa600.getCisa600_Pnd_Cis_Rqst_Id().setValue(ldaCispartv.getCis_Part_View_Cis_Rqst_Id());                                                                     //Natural: ASSIGN CISA600.#CIS-RQST-ID := CIS-RQST-ID
        pdaCisa600.getCisa600_Pnd_Cis_Annty_Option().setValue(ldaCispartv.getCis_Part_View_Cis_Annty_Option());                                                           //Natural: ASSIGN CISA600.#CIS-ANNTY-OPTION := CIS-ANNTY-OPTION
        pdaCisa600.getCisa600_Pnd_Cis_Mdo_Contract_Type().setValue(ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Type());                                                 //Natural: ASSIGN CISA600.#CIS-MDO-CONTRACT-TYPE := CIS-MDO-CONTRACT-TYPE
        pdaCisa600.getCisa600_Pnd_Cis_Mdo_Contract_Cash_Status().setValue(ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Cash_Status());                                   //Natural: ASSIGN CISA600.#CIS-MDO-CONTRACT-CASH-STATUS := CIS-MDO-CONTRACT-CASH-STATUS
        pdaCisa600.getCisa600_Pnd_Cis_Tiaa_Cntrct_Type().setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Type());                                       //Natural: ASSIGN CISA600.#CIS-TIAA-CNTRCT-TYPE := #TIAA-CNTRCT-TYPE
        pdaCisa600.getCisa600_Pnd_Cis_Rea_Cntrct_Type().setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Cntrct_Type());                                         //Natural: ASSIGN CISA600.#CIS-REA-CNTRCT-TYPE := #REA-CNTRCT-TYPE
        pdaCisa600.getCisa600_Pnd_Cis_Cref_Cntrct_Type().setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Type());                                       //Natural: ASSIGN CISA600.#CIS-CREF-CNTRCT-TYPE := #CREF-CNTRCT-TYPE
        DbsUtil.callnat(Cisn600.class , getCurrentProcessState(), pdaCisa600.getCisa600());                                                                               //Natural: CALLNAT 'CISN600' CISA600
        if (condition(Global.isEscape())) return;
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Header_1().setValue(pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_1());                                   //Natural: MOVE CISA600.#CREF-CNTRCT-HEADER-1 TO #PARTICIPANT-WORK-FILE.#CREF-CNTRCT-HEADER-1
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Header_2().setValue(pdaCisa600.getCisa600_Pnd_Cref_Cntrct_Header_2());                                   //Natural: MOVE CISA600.#CREF-CNTRCT-HEADER-2 TO #PARTICIPANT-WORK-FILE.#CREF-CNTRCT-HEADER-2
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Income_Option().setValue(pdaCisa600.getCisa600_Pnd_Cref_Income_Option());                                       //Natural: MOVE CISA600.#CREF-INCOME-OPTION TO #PARTICIPANT-WORK-FILE.#CREF-INCOME-OPTION
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Form_Num_Pg3_4().setValue(pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg3_4());                                     //Natural: MOVE CISA600.#CREF-FORM-NUM-PG3-4 TO #PARTICIPANT-WORK-FILE.#CREF-FORM-NUM-PG3-4
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Form_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Form_Num_Pg5_6());                                     //Natural: MOVE CISA600.#CREF-FORM-NUM-PG5-6 TO #PARTICIPANT-WORK-FILE.#CREF-FORM-NUM-PG5-6
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Product_Cde_3_4().setValue(pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_3_4());                                   //Natural: MOVE CISA600.#CREF-PRODUCT-CDE-3-4 TO #PARTICIPANT-WORK-FILE.#CREF-PRODUCT-CDE-3-4
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Product_Cde_5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Product_Cde_5_6());                                   //Natural: MOVE CISA600.#CREF-PRODUCT-CDE-5-6 TO #PARTICIPANT-WORK-FILE.#CREF-PRODUCT-CDE-5-6
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Edition_Num_Pg3_4().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg3_4());                               //Natural: MOVE CISA600.#CREF-EDITION-NUM-PG3-4 TO #PARTICIPANT-WORK-FILE.#CREF-EDITION-NUM-PG3-4
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Cref_Edition_Num_Pg5_6());                               //Natural: MOVE CISA600.#CREF-EDITION-NUM-PG5-6 TO #PARTICIPANT-WORK-FILE.#CREF-EDITION-NUM-PG5-6
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Cntrct_Header_1().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_1());                           //Natural: MOVE CISA600.#TIAA-REA-CNTRCT-HEADER-1 TO #PARTICIPANT-WORK-FILE.#TIAA-REA-CNTRCT-HEADER-1
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Cntrct_Header_2());                           //Natural: MOVE CISA600.#TIAA-REA-CNTRCT-HEADER-2 TO #PARTICIPANT-WORK-FILE.#TIAA-REA-CNTRCT-HEADER-2
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Form_Num_Pg3_4().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg3_4());                             //Natural: MOVE CISA600.#TIAA-REA-FORM-NUM-PG3-4 TO #PARTICIPANT-WORK-FILE.#TIAA-REA-FORM-NUM-PG3-4
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Form_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Form_Num_Pg5_6());                             //Natural: MOVE CISA600.#TIAA-REA-FORM-NUM-PG5-6 TO #PARTICIPANT-WORK-FILE.#TIAA-REA-FORM-NUM-PG5-6
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Product_Cde_3_4().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_3_4());                           //Natural: MOVE CISA600.#TIAA-REA-PRODUCT-CDE-3-4 TO #PARTICIPANT-WORK-FILE.#TIAA-REA-PRODUCT-CDE-3-4
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Product_Cde_5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Product_Cde_5_6());                           //Natural: MOVE CISA600.#TIAA-REA-PRODUCT-CDE-5-6 TO #PARTICIPANT-WORK-FILE.#TIAA-REA-PRODUCT-CDE-5-6
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Edition_Num_Pg3_4().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg3_4());                       //Natural: MOVE CISA600.#TIAA-REA-EDITION-NUM-PG3-4 TO #PARTICIPANT-WORK-FILE.#TIAA-REA-EDITION-NUM-PG3-4
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Rea_Edition_Num_Pg5_6());                       //Natural: MOVE CISA600.#TIAA-REA-EDITION-NUM-PG5-6 TO #PARTICIPANT-WORK-FILE.#TIAA-REA-EDITION-NUM-PG5-6
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Header_1().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_1());                                   //Natural: MOVE CISA600.#TIAA-CNTRCT-HEADER-1 TO #PARTICIPANT-WORK-FILE.#TIAA-CNTRCT-HEADER-1
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Header_2().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Cntrct_Header_2());                                   //Natural: MOVE CISA600.#TIAA-CNTRCT-HEADER-2 TO #PARTICIPANT-WORK-FILE.#TIAA-CNTRCT-HEADER-2
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Income_Option().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Income_Option());                                       //Natural: MOVE CISA600.#TIAA-INCOME-OPTION TO #PARTICIPANT-WORK-FILE.#TIAA-INCOME-OPTION
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Form_Num_Pg3_4().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg3_4());                                     //Natural: MOVE CISA600.#TIAA-FORM-NUM-PG3-4 TO #PARTICIPANT-WORK-FILE.#TIAA-FORM-NUM-PG3-4
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Form_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Form_Num_Pg5_6());                                     //Natural: MOVE CISA600.#TIAA-FORM-NUM-PG5-6 TO #PARTICIPANT-WORK-FILE.#TIAA-FORM-NUM-PG5-6
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Product_Cde_3_4().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_3_4());                                   //Natural: MOVE CISA600.#TIAA-PRODUCT-CDE-3-4 TO #PARTICIPANT-WORK-FILE.#TIAA-PRODUCT-CDE-3-4
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Product_Cde_5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Product_Cde_5_6());                                   //Natural: MOVE CISA600.#TIAA-PRODUCT-CDE-5-6 TO #PARTICIPANT-WORK-FILE.#TIAA-PRODUCT-CDE-5-6
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Edition_Num_Pg3_4().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg3_4());                               //Natural: MOVE CISA600.#TIAA-EDITION-NUM-PG3-4 TO #PARTICIPANT-WORK-FILE.#TIAA-EDITION-NUM-PG3-4
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Edition_Num_Pg5_6().setValue(pdaCisa600.getCisa600_Pnd_Tiaa_Edition_Num_Pg5_6());                               //Natural: MOVE CISA600.#TIAA-EDITION-NUM-PG5-6 TO #PARTICIPANT-WORK-FILE.#TIAA-EDITION-NUM-PG5-6
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_A().setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr());                                               //Natural: MOVE CIS-TIAA-NBR TO #TIAA-CNTRCT-A
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Iss_Dte().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Tiaa_Doi(),new ReportEditMask("YYYYMMDD"));           //Natural: MOVE EDITED CIS-TIAA-DOI ( EM = YYYYMMDD ) TO #TIAA-ISS-DTE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cntrct_Mergeset_Id().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_A(),  //Natural: COMPRESS #TIAA-CNTRCT-A #TIAA-ISS-DTE TO #CNTRCT-MERGESET-ID LEAVING NO SPACE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Iss_Dte()));
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Req_Begin_Dte().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Required_Begin_Date(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED CIS-REQUIRED-BEGIN-DATE ( EM = YYYYMMDD ) TO #REQ-BEGIN-DTE
        pnd_Requst_Begin_Dte.setValueEdited(ldaCispartv.getCis_Part_View_Cis_Required_Begin_Date(),new ReportEditMask("YYYYMMDD"));                                       //Natural: MOVE EDITED CIS-REQUIRED-BEGIN-DATE ( EM = YYYYMMDD ) TO #REQUST-BEGIN-DTE
        pnd_Date_Of_Issue.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Issue_Date());                                                                       //Natural: MOVE #TIAA-ISSUE-DATE TO #DATE-OF-ISSUE
        if (condition(pnd_Requst_Begin_Dte_Pnd_Ccyy_R.greater(pnd_Date_Of_Issue_Pnd_Ccyy_I)))                                                                             //Natural: IF #CCYY-R GT #CCYY-I
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Payment_Aft_Issue().setValue("Y");                                                                         //Natural: MOVE 'Y' TO #FIRST-PAYMENT-AFT-ISSUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Payment_Aft_Issue().setValue("N");                                                                         //Natural: MOVE 'N' TO #FIRST-PAYMENT-AFT-ISSUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr().equals(" ")))                                                                                           //Natural: IF CIS-TIAA-NBR = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tiaa_Contract_Num.setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr());                                                                                  //Natural: MOVE CIS-TIAA-NBR TO #TIAA-CONTRACT-NUM
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_F().setValue(pnd_Tiaa_Contract_Num_Pnd_Tiaa_1);                                                             //Natural: MOVE #TIAA-1 TO #TIAA-F
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Dash_1().setValue("-");                                                                                     //Natural: MOVE '-' TO #TIAA-DASH-1
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_6().setValue(pnd_Tiaa_Contract_Num_Pnd_Tiaa_2_6);                                                           //Natural: MOVE #TIAA-2-6 TO #TIAA-6
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Dash_2().setValue("-");                                                                                     //Natural: MOVE '-' TO #TIAA-DASH-2
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_L().setValue(pnd_Tiaa_Contract_Num_Pnd_Tiaa_Last);                                                          //Natural: MOVE #TIAA-LAST TO #TIAA-L
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("TPA") || ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IPRO")))                             //Natural: IF CIS-RQST-ID = 'TPA' OR = 'IPRO'
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Number().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr(),new ReportEditMask("XXXXXXX-X")); //Natural: MOVE EDITED CIS-TIAA-NBR ( EM = XXXXXXX-X ) TO #TIAA-CNTRCT-NUMBER
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Cert_Nbr().equals(" ")))                                                                                           //Natural: IF CIS-CERT-NBR = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Cref_Contract_No.setValue(ldaCispartv.getCis_Part_View_Cis_Cert_Nbr());                                                                                   //Natural: MOVE CIS-CERT-NBR TO #CREF-CONTRACT-NO
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_F().setValue(pnd_Cref_Contract_No_Pnd_Cref_1);                                                              //Natural: MOVE #CREF-1 TO #CREF-F
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Dash_1().setValue("-");                                                                                     //Natural: MOVE '-' TO #CREF-DASH-1
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_6().setValue(pnd_Cref_Contract_No_Pnd_Cref_2_6);                                                            //Natural: MOVE #CREF-2-6 TO #CREF-6
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Dash_2().setValue("-");                                                                                     //Natural: MOVE '-' TO #CREF-DASH-2
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_L().setValue(pnd_Cref_Contract_No_Pnd_Cref_Last);                                                           //Natural: MOVE #CREF-LAST TO #CREF-L
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr().equals(" ")))                                                                                           //Natural: IF CIS-TIAA-NBR = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  A-123456-7-REA
            pnd_Tiaa_Real_Est_Num.setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr());                                                                                  //Natural: MOVE CIS-TIAA-NBR TO #TIAA-REAL-EST-NUM
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_R_F().setValue(pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_F);                                                        //Natural: MOVE #TIAA-RE-F TO #TIAA-R-F
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Dash_R_1().setValue("-");                                                                                   //Natural: MOVE '-' TO #TIAA-DASH-R-1
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_R_6().setValue(pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_6);                                                        //Natural: MOVE #TIAA-RE-6 TO #TIAA-R-6
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Dash_R_2().setValue("-");                                                                                   //Natural: MOVE '-' TO #TIAA-DASH-R-2
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_R_L().setValue(pnd_Tiaa_Real_Est_Num_Pnd_Tiaa_Re_7);                                                        //Natural: MOVE #TIAA-RE-7 TO #TIAA-R-L
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Dash_R_3().setValue("-");                                                                                   //Natural: MOVE '-' TO #TIAA-DASH-R-3
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_R_Rea().setValue("REA");                                                                                    //Natural: MOVE 'REA' TO #TIAA-R-REA
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Issue_Date().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Tiaa_Doi(),new ReportEditMask("YYYYMMDD"));        //Natural: MOVE EDITED CIS-TIAA-DOI ( EM = YYYYMMDD ) TO #TIAA-ISSUE-DATE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Issue_Date().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Cref_Doi(),new ReportEditMask("YYYYMMDD"));        //Natural: MOVE EDITED CIS-CREF-DOI ( EM = YYYYMMDD ) TO #CREF-ISSUE-DATE
        //* * KG ATRA - ADDED ATRA TO GET ORIGINAL REA HEADER
        //* * KG TACC - CHANGED REA HEADER FOR TIAA SEPARATE ACCOUNT PACKAGE
        //* * NOTE - THIS WILL NEED TO BE CHANGED WHENEVER A STATE IS APPROVED
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Issue_Date().greaterOrEqual("20090508"))) //Natural: IF CIS-RQST-ID = 'IA' AND #TIAA-ISSUE-DATE GE '20090508'
        {
            //*  JRB3
            //*  KG ATRA
            //*  (05/08/2009 - 12/01/2009)
            //*  JRB3
            //*  JRB3
            //*  UNAPPROVED STATE       /* JRB3
            //*  JRB3
            short decideConditionsMet3143 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTRING ( CIS-SG-TEXT-UDF-1,1,1 ) = 'A'
            if (condition(ldaCispartv.getCis_Part_View_Cis_Sg_Text_Udf_1().getSubstring(1,1).equals("A")))
            {
                decideConditionsMet3143++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN #TIAA-ISSUE-DATE LT '20091201' AND ( #STAT-OF-ISSUE-CODE = 'DE' OR = 'MS' OR = 'NV' OR = 'NJ' OR = 'HI' OR = 'OR' )
            else if (condition((ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Issue_Date().less("20091201") && (((((ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code().equals("DE") 
                || ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code().equals("MS")) || ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code().equals("NV")) 
                || ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code().equals("NJ")) || ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code().equals("HI")) 
                || ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code().equals("OR")))))
            {
                decideConditionsMet3143++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN #STAT-OF-ISSUE-CODE = 'PR'
            else if (condition(ldaCislpart.getPnd_Participant_Work_File_Pnd_Stat_Of_Issue_Code().equals("PR")))
            {
                decideConditionsMet3143++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                short decideConditionsMet3151 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CIS-ANNTY-OPTION = 'OL'
                if (condition(ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("OL")))
                {
                    decideConditionsMet3151++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue(DbsUtil.compress("Your TIAA Separate Account One-Life",              //Natural: COMPRESS 'Your TIAA Separate Account One-Life' 'Unit-Annuity Contract' INTO #PARTICIPANT-WORK-FILE.#TIAA-REA-CNTRCT-HEADER-2
                        "Unit-Annuity Contract"));
                }                                                                                                                                                         //Natural: WHEN CIS-ANNTY-OPTION = 'JS' OR = 'LS' OR = 'LSF' OR = 'LST'
                else if (condition(ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("JS") || ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("LS") 
                    || ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("LSF") || ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("LST")))
                {
                    decideConditionsMet3151++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue(DbsUtil.compress("Your TIAA Separate Account Two-Life",              //Natural: COMPRESS 'Your TIAA Separate Account Two-Life' 'Unit-Annuity Contract' INTO #PARTICIPANT-WORK-FILE.#TIAA-REA-CNTRCT-HEADER-2
                        "Unit-Annuity Contract"));
                }                                                                                                                                                         //Natural: WHEN CIS-ANNTY-OPTION = 'AC'
                else if (condition(ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("AC")))
                {
                    decideConditionsMet3151++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Cntrct_Header_2().setValue(DbsUtil.compress("Your TIAA Separate Account Fixed-Period",          //Natural: COMPRESS 'Your TIAA Separate Account Fixed-Period' 'Unit-Annuity Contract' INTO #PARTICIPANT-WORK-FILE.#TIAA-REA-CNTRCT-HEADER-2
                        "Unit-Annuity Contract"));
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                    //*  1394
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  1378                                           /* JRB3
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  1376
        }                                                                                                                                                                 //Natural: END-IF
        //* * KG TACC - END
        //* * KG TRAN - ALTERED IF TO POPULATE REA NUMBER FOR TIAA TRANSFERS
        if (condition(ldaCispartv.getCis_Part_View_Cis_Cntrct_Type().equals("T")))                                                                                        //Natural: IF CIS-CNTRCT-TYPE = 'T'
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Number().setValue("None");                                                                           //Natural: MOVE 'None' TO #CREF-CNTRCT-NUMBER
            if (condition(ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().notEquals("Y")))                                                                                  //Natural: IF CIS-TRNSF-FLAG NE 'Y'
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Real_Estate_Num().setValue("None");                                                                     //Natural: MOVE 'None' TO #TIAA-REAL-ESTATE-NUM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  JB1
        if (condition(ldaCispartv.getCis_Part_View_Cis_Issue_State_Cd().equals("CA") && ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Issue_Date().less("20061118"))) //Natural: IF CIS-ISSUE-STATE-CD = 'CA' AND #TIAA-ISSUE-DATE < '20061118'
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Real_Estate_Num().setValue("None");                                                                         //Natural: MOVE 'None' TO #TIAA-REAL-ESTATE-NUM
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("TPA") || ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IPRO")))                             //Natural: IF CIS-RQST-ID = 'TPA' OR = 'IPRO'
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Real_Estate_Num().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Trnsf_Tiaa_Rea_Nbr(),new                  //Natural: MOVE EDITED CIS-TRNSF-TIAA-REA-NBR ( EM = X-XXXXXX-X ) TO #TIAA-REAL-ESTATE-NUM
                ReportEditMask("X-XXXXXX-X"));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Number().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Trnsf_Cert_Nbr(),new ReportEditMask("X-XXXXXX-X")); //Natural: MOVE EDITED CIS-TRNSF-CERT-NBR ( EM = X-XXXXXX-X ) TO #CREF-CNTRCT-NUMBER
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Freq_Of_Payment().setValue(" ");                                                                                     //Natural: MOVE ' ' TO #FREQ-OF-PAYMENT #TRANSFER-MODE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Mode().setValue(" ");
        short decideConditionsMet3187 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF CIS-PYMNT-MODE;//Natural: VALUE 'M'
        if (condition((ldaCispartv.getCis_Part_View_Cis_Pymnt_Mode().equals("M"))))
        {
            decideConditionsMet3187++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Freq_Of_Payment().setValue("Monthly");                                                                           //Natural: MOVE 'Monthly' TO #FREQ-OF-PAYMENT #TRANSFER-MODE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Mode().setValue("Monthly");
        }                                                                                                                                                                 //Natural: VALUE 'Q'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Pymnt_Mode().equals("Q"))))
        {
            decideConditionsMet3187++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Freq_Of_Payment().setValue("Quarterly");                                                                         //Natural: MOVE 'Quarterly' TO #FREQ-OF-PAYMENT #TRANSFER-MODE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Mode().setValue("Quarterly");
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Pymnt_Mode().equals("S"))))
        {
            decideConditionsMet3187++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Freq_Of_Payment().setValue("Semi-Annually");                                                                     //Natural: MOVE 'Semi-Annually' TO #FREQ-OF-PAYMENT #TRANSFER-MODE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Mode().setValue("Semi-Annually");
        }                                                                                                                                                                 //Natural: VALUE 'A'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Pymnt_Mode().equals("A"))))
        {
            decideConditionsMet3187++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Freq_Of_Payment().setValue("Annually");                                                                          //Natural: MOVE 'Annually' TO #FREQ-OF-PAYMENT #TRANSFER-MODE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Mode().setValue("Annually");
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Pymnt_Mode().equals("B"))))
        {
            decideConditionsMet3187++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Freq_Of_Payment().setValue("Semi-Monthly");                                                                      //Natural: MOVE 'Semi-Monthly' TO #FREQ-OF-PAYMENT #TRANSFER-MODE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Mode().setValue("Semi-Monthly");
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  KG TRAN - ADDED MODE FOR TRANSFERS
        if (condition(ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().equals("Y") && ldaCispartv.getCis_Part_View_Cis_Trnsf_Pymnt_Mode().notEquals(" ")))                   //Natural: IF CIS-TRNSF-FLAG EQ 'Y' AND CIS-TRNSF-PYMNT-MODE NE ' '
        {
            short decideConditionsMet3203 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF CIS-TRNSF-PYMNT-MODE;//Natural: VALUE 'M'
            if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Pymnt_Mode().equals("M"))))
            {
                decideConditionsMet3203++;
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Mode().setValue("Monthly");                                                                         //Natural: MOVE 'Monthly' TO #TRANSFER-MODE
            }                                                                                                                                                             //Natural: VALUE 'Q'
            else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Pymnt_Mode().equals("Q"))))
            {
                decideConditionsMet3203++;
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Mode().setValue("Quarterly");                                                                       //Natural: MOVE 'Quarterly' TO #TRANSFER-MODE
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Pymnt_Mode().equals("S"))))
            {
                decideConditionsMet3203++;
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Mode().setValue("Semi-Annually");                                                                   //Natural: MOVE 'Semi-Annually' TO #TRANSFER-MODE
            }                                                                                                                                                             //Natural: VALUE 'A'
            else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Pymnt_Mode().equals("A"))))
            {
                decideConditionsMet3203++;
                //*      MOVE 'Annually'        TO #TRANSFER-MODE            /* BD
                //*  BD
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Mode().setValue("Annual");                                                                          //Natural: MOVE 'Annual' TO #TRANSFER-MODE
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Pymnt_Mode().equals("B"))))
            {
                decideConditionsMet3203++;
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Mode().setValue("Semi-Monthly");                                                                    //Natural: MOVE 'Semi-Monthly' TO #TRANSFER-MODE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Payment_Date().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Annty_Start_Dte(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED CIS-ANNTY-START-DTE ( EM = YYYYMMDD ) TO #FIRST-PAYMENT-DATE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Payment_Date().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Annty_End_Dte(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED CIS-ANNTY-END-DTE ( EM = YYYYMMDD ) TO #LAST-PAYMENT-DATE
        if (condition(ldaCispartv.getCis_Part_View_Cis_Grnted_Period_Yrs().equals(0) && ldaCispartv.getCis_Part_View_Cis_Grnted_Period_Dys().equals(0)))                  //Natural: IF CIS-GRNTED-PERIOD-YRS = 00 AND CIS-GRNTED-PERIOD-DYS = 00
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Guaranteed_Period().setValue("None");                                                                            //Natural: MOVE 'None' TO #GUARANTEED-PERIOD
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Guaranteed_P_Date().setValue("Not Applicable");                                                            //Natural: MOVE 'Not Applicable' TO #FIRST-GUARANTEED-P-DATE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_End_Guaranteed_P_Date().setValue("Not Applicable");                                                              //Natural: MOVE 'Not Applicable' TO #END-GUARANTEED-P-DATE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Guarant_Period_Flag().setValue("N");                                                                        //Natural: MOVE 'N' TO #TIAA-GUARANT-PERIOD-FLAG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Guaranteed_P_Date().setValue(" ");                                                                         //Natural: MOVE ' ' TO #FIRST-GUARANTEED-P-DATE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_End_Guaranteed_P_Date().setValue(" ");                                                                           //Natural: MOVE ' ' TO #END-GUARANTEED-P-DATE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Guarant_Period_Flag().setValue("Y");                                                                        //Natural: MOVE 'Y' TO #TIAA-GUARANT-PERIOD-FLAG
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_Yy().setValue(ldaCispartv.getCis_Part_View_Cis_Grnted_Period_Yrs());                                           //Natural: MOVE CIS-GRNTED-PERIOD-YRS TO #G-YY
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_Filler1().setValue(" ");                                                                                       //Natural: MOVE ' ' TO #G-FILLER1
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_Years().setValue("Years");                                                                                     //Natural: MOVE 'Years' TO #G-YEARS
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_Filler2().setValue(" ");                                                                                       //Natural: MOVE ' ' TO #G-FILLER2
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_Dd().setValue(ldaCispartv.getCis_Part_View_Cis_Grnted_Period_Dys());                                           //Natural: MOVE CIS-GRNTED-PERIOD-DYS TO #G-DD
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_Filler3().setValue(" ");                                                                                       //Natural: MOVE ' ' TO #G-FILLER3
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_Days().setValue("Days");                                                                                       //Natural: MOVE 'Days' TO #G-DAYS
            if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().equals("Y")))                          //Natural: IF CIS-RQST-ID = 'IA' AND CIS-TRNSF-FLAG EQ 'Y'
            {
                //*  CREF EQUITY             /*PG1
                if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Cntrct_Type().equals("T")))                                                                           //Natural: IF CIS-CREF-CNTRCT-TYPE = 'T'
                {
                    //* PG1
                    //* PG1
                    pnd_First_Guarant_P_Date.setValueEdited(ldaCispartv.getCis_Part_View_Cis_Cref_Doi(),new ReportEditMask("MMDDYYYY"));                                  //Natural: MOVE EDITED CIS-CREF-DOI ( EM = MMDDYYYY ) TO #FIRST-GUARANT-P-DATE
                    //*  POST SETLMNT TRANSFER    /*PG1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //* PG1
                    //* PG1
                    pnd_First_Guarant_P_Date.setValueEdited(ldaCispartv.getCis_Part_View_Cis_Tiaa_Doi(),new ReportEditMask("MMDDYYYY"));                                  //Natural: MOVE EDITED CIS-TIAA-DOI ( EM = MMDDYYYY ) TO #FIRST-GUARANT-P-DATE
                    //* PG1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_First_Guarant_P_Date.setValueEdited(ldaCispartv.getCis_Part_View_Cis_Annty_Start_Dte(),new ReportEditMask("MMDDYYYY"));                               //Natural: MOVE EDITED CIS-ANNTY-START-DTE ( EM = MMDDYYYY ) TO #FIRST-GUARANT-P-DATE
            }                                                                                                                                                             //Natural: END-IF
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_P_Mm().setValue(pnd_First_Guarant_P_Date_Pnd_Mm_F);                                                            //Natural: MOVE #MM-F TO #G-P-MM
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_P_B1().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #G-P-B1
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_P_Dd().setValue(pnd_First_Guarant_P_Date_Pnd_Dd_F);                                                            //Natural: MOVE #DD-F TO #G-P-DD
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_P_B2().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #G-P-B2
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_P_Ccyy().setValue(pnd_First_Guarant_P_Date_Pnd_Ccyy_F);                                                        //Natural: MOVE #CCYY-F TO #G-P-CCYY
            pnd_End_Guarant_P_Date.setValueEdited(ldaCispartv.getCis_Part_View_Cis_Annty_End_Dte(),new ReportEditMask("MMDDYYYY"));                                       //Natural: MOVE EDITED CIS-ANNTY-END-DTE ( EM = MMDDYYYY ) TO #END-GUARANT-P-DATE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_E_G_Mm().setValue(pnd_End_Guarant_P_Date_Pnd_Mm_E);                                                              //Natural: MOVE #MM-E TO #E-G-MM
            ldaCislpart.getPnd_Participant_Work_File_Pnd_E_G_B1().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #E-G-B1
            ldaCislpart.getPnd_Participant_Work_File_Pnd_E_G_Dd().setValue(pnd_End_Guarant_P_Date_Pnd_Dd_E);                                                              //Natural: MOVE #DD-E TO #E-G-DD
            ldaCislpart.getPnd_Participant_Work_File_Pnd_E_G_B2().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #E-G-B2
            ldaCislpart.getPnd_Participant_Work_File_Pnd_E_G_Ccyy().setValue(pnd_End_Guarant_P_Date_Pnd_Ccyy_E);                                                          //Natural: MOVE #CCYY-E TO #E-G-CCYY
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("IR")))                                                                                      //Natural: IF CIS-ANNTY-OPTION EQ 'IR'
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Guarant_Period_Flag().setValue("Y");                                                                        //Natural: MOVE 'Y' TO #TIAA-GUARANT-PERIOD-FLAG
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF CIS-GRNTED-PERIOD-DYS = 0
        if (condition(ldaCispartv.getCis_Part_View_Cis_Grnted_Period_Dys().equals(getZero())))                                                                            //Natural: IF CIS-GRNTED-PERIOD-DYS = 0
        {
            //*  MOVE ' ' TO THE OUTPUT FIELD
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_Dd().setValue(" ");                                                                                            //Natural: MOVE ' ' TO #G-DD
            ldaCislpart.getPnd_Participant_Work_File_Pnd_G_Days().setValue(" ");                                                                                          //Natural: MOVE ' ' TO #G-DAYS
        }                                                                                                                                                                 //Natural: END-IF
        //* ********** MOVE ALL THESE FIELDS AS UPPER CASE ** ITEM 2************
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Last_Name().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme());                               //Natural: MOVE CIS-FRST-ANNT-LST-NME TO #FIRST-ANNU-LAST-NAME
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_First_Name().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme());                             //Natural: MOVE CIS-FRST-ANNT-FRST-NME TO #FIRST-ANNU-FIRST-NAME
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Mid_Name().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme());                                //Natural: MOVE CIS-FRST-ANNT-MID-NME TO #FIRST-ANNU-MID-NAME
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Title().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Prfx());                                      //Natural: MOVE CIS-FRST-ANNT-PRFX TO #FIRST-ANNU-TITLE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Suffix().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx());                                     //Natural: MOVE CIS-FRST-ANNT-SFFX TO #FIRST-ANNU-SUFFIX
        //* ********** MOVE ALL THESE FIELDS AS UPPER CASE ** ITEM 3************
        //* * MDO WITH LAST NAME = TRUST OR TRUSTS ONLY *** BY KAR 11/19/97
        //*  060706
        if (condition((DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'") && (ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme().equals("TRUST")   //Natural: IF CIS-RQST-ID = MASK ( 'MDO' ) AND ( CIS-FRST-ANNT-LST-NME = 'TRUST' OR CIS-FRST-ANNT-LST-NME = 'TRUSTS' )
            || ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme().equals("TRUSTS")))))
        {
            pnd_Begin.reset();                                                                                                                                            //Natural: RESET #BEGIN #LEN-ALL
            pnd_Len_All.reset();
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Prfx(),1,1), new ExamineSearch(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Prfx()),  //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-PRFX,1 ) FOR CIS-FRST-ANNT-PRFX GIVING LENGTH #PRFX-LEN
                new ExamineGivingLength(pnd_Len_All_Pnd_Prfx_Len));
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx(),1,1), new ExamineSearch(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx()),  //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-SFFX,1 ) FOR CIS-FRST-ANNT-SFFX GIVING LENGTH #SFFX-LEN
                new ExamineGivingLength(pnd_Len_All_Pnd_Sffx_Len));
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme(),1,1), new ExamineSearch(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme()),  //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-FRST-NME,1 ) FOR CIS-FRST-ANNT-FRST-NME GIVING LENGTH #FRST-LEN
                new ExamineGivingLength(pnd_Len_All_Pnd_Frst_Len));
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme(),1,1), new ExamineSearch(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme()),  //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-MID-NME,1 ) FOR CIS-FRST-ANNT-MID-NME GIVING LENGTH #MID-LEN
                new ExamineGivingLength(pnd_Len_All_Pnd_Mid_Len));
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme(),1,1), new ExamineSearch(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme()),  //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-LST-NME,1 ) FOR CIS-FRST-ANNT-LST-NME GIVING LENGTH #LST-LEN
                new ExamineGivingLength(pnd_Len_All_Pnd_Lst_Len));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Prefix_A().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Prfx());                                          //Natural: MOVE CIS-FRST-ANNT-PRFX TO #PREFIX-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_A().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme());                                  //Natural: MOVE CIS-FRST-ANNT-FRST-NME TO #FIRST-NAME-A
            pnd_Frst_Annt_Mid_I.setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme());                                                                           //Natural: MOVE CIS-FRST-ANNT-MID-NME TO #FRST-ANNT-MID-I
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_A().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme());                                    //Natural: MOVE CIS-FRST-ANNT-LST-NME TO #LAST-NAME-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix_A().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx());                                          //Natural: MOVE CIS-FRST-ANNT-SFFX TO #SUFFIX-A
            pnd_Begin.compute(new ComputeParameters(false, pnd_Begin), pnd_Len_All_Pnd_Frst_Len.add(pnd_Len_All_Pnd_Lst_Len).add(pnd_Len_All_Pnd_Mid_Len).add(pnd_Len_All_Pnd_Prfx_Len).add(pnd_Len_All_Pnd_Sffx_Len).add(4)); //Natural: ASSIGN #BEGIN := #FRST-LEN + #LST-LEN + #MID-LEN + #PRFX-LEN + #SFFX-LEN + 4
            if (condition(pnd_Begin.greater(81)))                                                                                                                         //Natural: IF #BEGIN > 81
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Address_Name().setValue(DbsUtil.compress(ldaCislpart.getPnd_Participant_Work_File_Pnd_Prefix_A(),       //Natural: COMPRESS #PREFIX-A ' ' #FIRST-NAME-A ' ' #FRST-ANNT-MID-I ' ' #LAST-NAME-A INTO #ANNU-ADDRESS-NAME LEAVING
                    " ", ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_A(), " ", pnd_Frst_Annt_Mid_I, " ", ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_A()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Address_Name().setValue(DbsUtil.compress(ldaCislpart.getPnd_Participant_Work_File_Pnd_Prefix_A(),       //Natural: COMPRESS #PREFIX-A ' ' #FIRST-NAME-A ' ' #FRST-ANNT-MID-I ' ' #SUFFIX-A ' ' #LAST-NAME-A INTO #ANNU-ADDRESS-NAME LEAVING
                    " ", ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_A(), " ", pnd_Frst_Annt_Mid_I, " ", ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix_A(), 
                    " ", ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_A()));
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Address_Name(),1,81), new ExamineTranslate(TranslateOption.Upper));       //Natural: EXAMINE SUBSTRING ( #ANNU-ADDRESS-NAME,1,81 ) TRANSLATE INTO UPPER CASE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Prefix_A().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Prfx());                                          //Natural: MOVE CIS-FRST-ANNT-PRFX TO #PREFIX-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A1().setValue("@");                                                                                       //Natural: MOVE '@' TO #FILLER-A1
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_A().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme());                                  //Natural: MOVE CIS-FRST-ANNT-FRST-NME TO #FIRST-NAME-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A2().setValue("@");                                                                                       //Natural: MOVE '@' TO #FILLER-A2
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Middle_Init_A().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme());                                  //Natural: MOVE CIS-FRST-ANNT-MID-NME TO #MIDDLE-INIT-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A3().setValue("@");                                                                                       //Natural: MOVE '@' TO #FILLER-A3
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_A().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme());                                    //Natural: MOVE CIS-FRST-ANNT-LST-NME TO #LAST-NAME-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A4().setValue("@");                                                                                       //Natural: MOVE '@' TO #FILLER-A4
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix_A().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx());                                          //Natural: MOVE CIS-FRST-ANNT-SFFX TO #SUFFIX-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Address_Name().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCislpart.getPnd_Participant_Work_File_Pnd_Prefix_A(),  //Natural: COMPRESS #PREFIX-A #FILLER-A1 #FIRST-NAME-A #FILLER-A2 #MIDDLE-INIT-A #FILLER-A3 #LAST-NAME-A #FILLER-A4 #SUFFIX-A TO #ANNU-ADDRESS-NAME LEAVING NO SPACE
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A1(), ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_A(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A2(), 
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Middle_Init_A(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A3(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_A(), 
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A4(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix_A()));
            DbsUtil.examine(new ExamineSource(ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Address_Name()), new ExamineSearch("@"), new ExamineReplace(" "));        //Natural: EXAMINE #ANNU-ADDRESS-NAME FOR '@' REPLACE WITH ' '
            DbsUtil.examine(new ExamineSource(ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Address_Name(),1,81), new ExamineTranslate(TranslateOption.Upper));       //Natural: EXAMINE SUBSTRING ( #ANNU-ADDRESS-NAME,1,81 ) TRANSLATE INTO UPPER CASE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().equals("Y") && ldaCispartv.getCis_Part_View_Cis_Ownership_Cd().equals(2))) //Natural: IF CIS-RQST-ID = 'IA' AND CIS-TRNSF-FLAG = 'Y' AND CIS-OWNERSHIP-CD = 2
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Prefix_A().setValue(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Prfx());                                          //Natural: MOVE CIS-SCND-ANNT-PRFX TO #PREFIX-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A1().setValue("@");                                                                                       //Natural: MOVE '@' TO #FILLER-A1
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_A().setValue(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Frst_Nme());                                  //Natural: MOVE CIS-SCND-ANNT-FRST-NME TO #FIRST-NAME-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A2().setValue("@");                                                                                       //Natural: MOVE '@' TO #FILLER-A2
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Middle_Init_A().setValue(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Mid_Nme());                                  //Natural: MOVE CIS-SCND-ANNT-MID-NME TO #MIDDLE-INIT-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A3().setValue("@");                                                                                       //Natural: MOVE '@' TO #FILLER-A3
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_A().setValue(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Lst_Nme());                                    //Natural: MOVE CIS-SCND-ANNT-LST-NME TO #LAST-NAME-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A4().setValue("@");                                                                                       //Natural: MOVE '@' TO #FILLER-A4
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix_A().setValue(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Sffx());                                          //Natural: MOVE CIS-SCND-ANNT-SFFX TO #SUFFIX-A
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Address_Name().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCislpart.getPnd_Participant_Work_File_Pnd_Prefix_A(),  //Natural: COMPRESS #PREFIX-A #FILLER-A1 #FIRST-NAME-A #FILLER-A2 #MIDDLE-INIT-A #FILLER-A3 #LAST-NAME-A #FILLER-A4 #SUFFIX-A TO #ANNU-ADDRESS-NAME LEAVING NO SPACE
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A1(), ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_A(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A2(), 
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Middle_Init_A(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A3(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_A(), 
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_A4(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix_A()));
            DbsUtil.examine(new ExamineSource(ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Address_Name()), new ExamineSearch("@"), new ExamineReplace(" "));        //Natural: EXAMINE #ANNU-ADDRESS-NAME FOR '@' REPLACE WITH ' '
            DbsUtil.examine(new ExamineSource(ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Address_Name(),1,81), new ExamineTranslate(TranslateOption.Upper));       //Natural: EXAMINE SUBSTRING ( #ANNU-ADDRESS-NAME,1,81 ) TRANSLATE INTO UPPER CASE
        }                                                                                                                                                                 //Natural: END-IF
        //* ********** MOVE ALL THESE FIELDS AS UPPER CASE ** ITEM 5************
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_D().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme());                                      //Natural: MOVE CIS-FRST-ANNT-FRST-NME TO #FIRST-NAME-D
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_D1().setValue("@");                                                                                           //Natural: MOVE '@' TO #FILLER-D1
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mid_Name_D().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme());                                         //Natural: MOVE CIS-FRST-ANNT-MID-NME TO #MID-NAME-D
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_D2().setValue("@");                                                                                           //Natural: MOVE '@' TO #FILLER-D2
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_D().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme());                                        //Natural: MOVE CIS-FRST-ANNT-LST-NME TO #LAST-NAME-D
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Dir_Name().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_D(),  //Natural: COMPRESS #FIRST-NAME-D #FILLER-D1 #MID-NAME-D #FILLER-D2 #LAST-NAME-D TO #ANNU-DIR-NAME LEAVING NO SPACE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_D1(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Mid_Name_D(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_D2(), 
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_D()));
        DbsUtil.examine(new ExamineSource(ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Dir_Name()), new ExamineSearch("@"), new ExamineReplace(" "));                //Natural: EXAMINE #ANNU-DIR-NAME FOR '@' REPLACE WITH ' '
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Pin_Number().setValue(ldaCispartv.getCis_Part_View_Cis_Pin_Nbr());                                                   //Natural: MOVE CIS-PIN-NBR TO #PIN-NUMBER
        //* * MDO WITH LAST NAME = TRUST OR TRUSTS ONLY *** BY KAR 11/19/97
        //*  060706
        if (condition((DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'") && (ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme().equals("TRUST")   //Natural: IF CIS-RQST-ID = MASK ( 'MDO' ) AND ( CIS-FRST-ANNT-LST-NME = 'TRUST' OR CIS-FRST-ANNT-LST-NME = 'TRUSTS' )
            || ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme().equals("TRUSTS")))))
        {
            pnd_Begin.reset();                                                                                                                                            //Natural: RESET #BEGIN #LEN-ALL
            pnd_Len_All.reset();
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx(),1,1), new ExamineSearch(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx()),  //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-SFFX,1 ) FOR CIS-FRST-ANNT-SFFX GIVING LENGTH #SFFX-LEN
                new ExamineGivingLength(pnd_Len_All_Pnd_Sffx_Len));
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme(),1,1), new ExamineSearch(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme()),  //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-FRST-NME,1 ) FOR CIS-FRST-ANNT-FRST-NME GIVING LENGTH #FRST-LEN
                new ExamineGivingLength(pnd_Len_All_Pnd_Frst_Len));
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme(),1,1), new ExamineSearch(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme()),  //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-MID-NME,1 ) FOR CIS-FRST-ANNT-MID-NME GIVING LENGTH #MID-LEN
                new ExamineGivingLength(pnd_Len_All_Pnd_Mid_Len));
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme(),1,1), new ExamineSearch(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme()),  //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-LST-NME,1 ) FOR CIS-FRST-ANNT-LST-NME GIVING LENGTH #LST-LEN
                new ExamineGivingLength(pnd_Len_All_Pnd_Lst_Len));
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                  //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-FRST-NME,2,29 ) TRANSLATE INTO LOWER CASE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Name().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme());                               //Natural: MOVE CIS-FRST-ANNT-FRST-NME TO #FIRST-ANNU-NAME
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                   //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-MID-NME,2,29 ) TRANSLATE INTO LOWER CASE
            pnd_Frst_Annt_Mid_I.setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme());                                                                           //Natural: MOVE CIS-FRST-ANNT-MID-NME TO #FRST-ANNT-MID-I
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Middle_Int().setValue(pnd_Frst_Annt_Mid_I_Pnd_Frst_Annt_Mid_Init);                                               //Natural: MOVE #FRST-ANNT-MID-INIT TO #MIDDLE-INT
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                   //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-LST-NME,2,29 ) TRANSLATE INTO LOWER CASE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Annu_Name().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme());                                 //Natural: MOVE CIS-FRST-ANNT-LST-NME TO #LAST-ANNU-NAME
            short decideConditionsMet3368 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF CIS-FRST-ANNT-SFFX;//Natural: VALUE 'SR' , 'Sr'
            if (condition((ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx().equals("SR") || ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx().equals("Sr"))))
            {
                decideConditionsMet3368++;
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix().setValue("Sr.");                                                                                    //Natural: MOVE 'Sr.' TO #SUFFIX
            }                                                                                                                                                             //Natural: VALUE 'JR' , 'Jr'
            else if (condition((ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx().equals("JR") || ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx().equals("Jr"))))
            {
                decideConditionsMet3368++;
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix().setValue("Jr.");                                                                                    //Natural: MOVE 'Jr.' TO #SUFFIX
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx());                                        //Natural: MOVE CIS-FRST-ANNT-SFFX TO #SUFFIX
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Begin.compute(new ComputeParameters(false, pnd_Begin), pnd_Len_All_Pnd_Frst_Len.add(pnd_Len_All_Pnd_Lst_Len).add(pnd_Len_All_Pnd_Mid_Len).add(pnd_Len_All_Pnd_Sffx_Len).add(3)); //Natural: ASSIGN #BEGIN := #FRST-LEN + #LST-LEN + #MID-LEN + #SFFX-LEN + 3
            if (condition(pnd_Begin.greater(72)))                                                                                                                         //Natural: IF #BEGIN > 72
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Cntrct_Name().setValue(DbsUtil.compress(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Name(),  //Natural: COMPRESS #FIRST-ANNU-NAME ' ' #FRST-ANNT-MID-I ' ' #LAST-ANNU-NAME INTO #FIRST-ANNU-CNTRCT-NAME LEAVING
                    " ", pnd_Frst_Annt_Mid_I, " ", ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Annu_Name()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Cntrct_Name().setValue(DbsUtil.compress(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Name(),  //Natural: COMPRESS #FIRST-ANNU-NAME ' ' #FRST-ANNT-MID-I ' ' #SUFFIX ' ' #LAST-ANNU-NAME INTO #FIRST-ANNU-CNTRCT-NAME LEAVING
                    " ", pnd_Frst_Annt_Mid_I, " ", ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix(), " ", ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Annu_Name()));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                  //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-FRST-NME,2,29 ) TRANSLATE INTO LOWER CASE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Name().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme());                               //Natural: MOVE CIS-FRST-ANNT-FRST-NME TO #FIRST-ANNU-NAME
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_1().setValue("@");                                                                                        //Natural: MOVE '@' TO #FILLER-1
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                   //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-MID-NME,2,29 ) TRANSLATE INTO LOWER CASE
            pnd_Frst_Annt_Mid_I.setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme());                                                                           //Natural: MOVE CIS-FRST-ANNT-MID-NME TO #FRST-ANNT-MID-I
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Middle_Int().setValue(pnd_Frst_Annt_Mid_I_Pnd_Frst_Annt_Mid_Init);                                               //Natural: MOVE #FRST-ANNT-MID-INIT TO #MIDDLE-INT
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_2().setValue("@");                                                                                        //Natural: MOVE '@' TO #FILLER-2
            DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                   //Natural: EXAMINE SUBSTRING ( CIS-FRST-ANNT-LST-NME,2,29 ) TRANSLATE INTO LOWER CASE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Annu_Name().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme());                                 //Natural: MOVE CIS-FRST-ANNT-LST-NME TO #LAST-ANNU-NAME
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_3().setValue("@");                                                                                        //Natural: MOVE '@' TO #FILLER-3
            short decideConditionsMet3394 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF CIS-FRST-ANNT-SFFX;//Natural: VALUE 'SR' , 'Sr'
            if (condition((ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx().equals("SR") || ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx().equals("Sr"))))
            {
                decideConditionsMet3394++;
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix().setValue("Sr.");                                                                                    //Natural: MOVE 'Sr.' TO #SUFFIX
            }                                                                                                                                                             //Natural: VALUE 'JR' , 'Jr'
            else if (condition((ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx().equals("JR") || ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx().equals("Jr"))))
            {
                decideConditionsMet3394++;
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix().setValue("Jr.");                                                                                    //Natural: MOVE 'Jr.' TO #SUFFIX
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx());                                        //Natural: MOVE CIS-FRST-ANNT-SFFX TO #SUFFIX
            }                                                                                                                                                             //Natural: END-DECIDE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Cntrct_Name().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Name(),  //Natural: COMPRESS #FIRST-ANNU-NAME #FILLER-1 #MIDDLE-INT #FILLER-2 #LAST-ANNU-NAME #FILLER-3 #SUFFIX TO #FIRST-ANNU-CNTRCT-NAME LEAVING NO SPACE
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_1(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Middle_Int(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_2(), 
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Annu_Name(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_3(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix()));
            DbsUtil.examine(new ExamineSource(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Cntrct_Name()), new ExamineSearch("@"), new ExamineReplace(" "));   //Natural: EXAMINE #FIRST-ANNU-CNTRCT-NAME FOR '@' REPLACE WITH ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().equals("Y") && ldaCispartv.getCis_Part_View_Cis_Ownership_Cd().equals(2))) //Natural: IF CIS-RQST-ID = 'IA' AND CIS-TRNSF-FLAG = 'Y' AND CIS-OWNERSHIP-CD = 2
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Cntrct_Name().setValue(DbsUtil.compress(ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Cntrct_Name(),  //Natural: COMPRESS #FIRST-ANNU-CNTRCT-NAME '- Deceased' INTO #FIRST-ANNU-CNTRCT-NAME
                "- Deceased"));
        }                                                                                                                                                                 //Natural: END-IF
        //* *******************************************************************
        pnd_Error_Msg.setValue(" ");                                                                                                                                      //Natural: MOVE ' ' TO #ERROR-MSG #CIS-DESC
        pdaCisa200.getPnd_Parm_Data_Pnd_Cis_Desc().setValue(" ");
        pdaCisa200.getPnd_Parm_Data_Pnd_Cis_Type_Call().setValue("C");                                                                                                    //Natural: MOVE 'C' TO #CIS-TYPE-CALL
        pdaCisa200.getPnd_Parm_Data_Pnd_Cis_Code().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Ctznshp_Cd());                                                     //Natural: MOVE CIS-FRST-ANNT-CTZNSHP-CD TO #CIS-CODE
        DbsUtil.callnat(Nazn035.class , getCurrentProcessState(), pdaNazpda_M.getMsg_Info_Sub(), pdaCisa200.getPnd_Parm_Data());                                          //Natural: CALLNAT 'NAZN035' MSG-INFO-SUB #PARM-DATA
        if (condition(Global.isEscape())) return;
        if (condition(pdaNazpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                     //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
        {
            pnd_Error_Msg.setValue(DbsUtil.compress("INVALID CITIZENSHIP CODE", pdaCisa200.getPnd_Parm_Data_Pnd_Cis_Code(), "ENTERED"));                                  //Natural: COMPRESS 'INVALID CITIZENSHIP CODE' #CIS-CODE 'ENTERED' INTO #ERROR-MSG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Citizenship().setValue(pdaCisa200.getPnd_Parm_Data_Pnd_Cis_Desc());                                   //Natural: MOVE #CIS-DESC TO #FIRST-ANNU-CITIZENSHIP #MSG
            pnd_Msg.setValue(pdaCisa200.getPnd_Parm_Data_Pnd_Cis_Desc());
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Ssn().greater(getZero())))                                                                               //Natural: IF CIS-FRST-ANNT-SSN GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Ssn().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Ssn(),new ReportEditMask("999-99-9999")); //Natural: MOVE EDITED CIS-FRST-ANNT-SSN ( EM = 999-99-9999 ) TO #FIRST-ANNU-SSN
            pnd_Report_Ssn.setValueEdited(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Ssn(),new ReportEditMask("999-99-9999"));                                            //Natural: MOVE EDITED CIS-FRST-ANNT-SSN ( EM = 999-99-9999 ) TO #REPORT-SSN
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Dob().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Dob(),new ReportEditMask("YYYYMMDD"));    //Natural: MOVE EDITED CIS-FRST-ANNT-DOB ( EM = YYYYMMDD ) TO #FIRST-ANNU-DOB
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annu_Res_State().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Rsdnc_Cde());                             //Natural: MOVE CIS-FRST-ANNT-RSDNC-CDE TO #FIRST-ANNU-RES-STATE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Annu_Welcome_Name().setValue("Participant:");                                                                        //Natural: MOVE 'Participant:' TO #ANNU-WELCOME-NAME
        if (condition(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Calc_Method().equals("R") || ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Calc_Method().equals("Y")))      //Natural: IF CIS-FRST-ANNT-CALC-METHOD = 'R' OR = 'Y'
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_A_C_M().setValue(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Calc_Method());                                //Natural: MOVE CIS-FRST-ANNT-CALC-METHOD TO #FIRST-A-C-M
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_A_C_M().setValue(" ");                                                                                     //Natural: MOVE ' ' TO #FIRST-A-C-M
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Calc_Method().equals("R")))                                                                              //Natural: IF CIS-FRST-ANNT-CALC-METHOD = 'R'
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_A_C_M_Literal().setValue("Recalculation");                                                                 //Natural: MOVE 'Recalculation' TO #FIRST-A-C-M-LITERAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Calc_Method().equals("Y")))                                                                          //Natural: IF CIS-FRST-ANNT-CALC-METHOD = 'Y'
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_First_A_C_M_Literal().setValue("One-Year-Less");                                                             //Natural: MOVE 'One-Year-Less' TO #FIRST-A-C-M-LITERAL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Calc_Method().equals("R") || ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Calc_Method().equals("Y")))      //Natural: IF CIS-FRST-ANNT-CALC-METHOD = 'R' OR = 'Y'
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Calc_Participant().setValue("- Participant");                                                                    //Natural: MOVE '- Participant' TO #CALC-PARTICIPANT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Calc_Participant().setValue(" ");                                                                                //Natural: MOVE ' ' TO #CALC-PARTICIPANT
        }                                                                                                                                                                 //Natural: END-IF
        //* ** CIS-ADDRESS-TXT FOR FIRST 5 OCCUR OF FIRST ANNUITANT
        //* ** CHECK TO SEE IF TEMP ADDR HAS DATA IN THE 3RD OCC OF THE 1ST LINE
        //*  TEMP ADDRESS
        if (condition(ldaCispartv.getCis_Part_View_Cis_Addr_Usage_Code().getValue(1).equals("4") && ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(3,            //Natural: IF CIS-ADDR-USAGE-CODE ( 1 ) = '4' AND CIS-ADDRESS-TXT ( 3,1 ) NE ' '
            1).notEquals(" ")))
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Mail_Addr1().getValue(1).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(3,        //Natural: MOVE CIS-ADDRESS-TXT ( 3,1 ) TO #FIRST-ANNUIT-MAIL-ADDR1 ( 1 )
                1));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Mail_Addr1().getValue(2).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(3,        //Natural: MOVE CIS-ADDRESS-TXT ( 3,2 ) TO #FIRST-ANNUIT-MAIL-ADDR1 ( 2 )
                2));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Mail_Addr1().getValue(3).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(3,        //Natural: MOVE CIS-ADDRESS-TXT ( 3,3 ) TO #FIRST-ANNUIT-MAIL-ADDR1 ( 3 )
                3));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Mail_Addr1().getValue(4).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(3,        //Natural: MOVE CIS-ADDRESS-TXT ( 3,4 ) TO #FIRST-ANNUIT-MAIL-ADDR1 ( 4 )
                4));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Mail_Addr1().getValue(5).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(3,        //Natural: MOVE CIS-ADDRESS-TXT ( 3,5 ) TO #FIRST-ANNUIT-MAIL-ADDR1 ( 5 )
                5));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Zipcde().setValue(ldaCispartv.getCis_Part_View_Cis_Zip_Code().getValue(3));                         //Natural: MOVE CIS-ZIP-CODE ( 3 ) TO #FIRST-ANNUIT-ZIPCDE
            //*  PERM ADDRESS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Mail_Addr1().getValue(1).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(1,        //Natural: MOVE CIS-ADDRESS-TXT ( 1,1 ) TO #FIRST-ANNUIT-MAIL-ADDR1 ( 1 )
                1));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Mail_Addr1().getValue(2).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(1,        //Natural: MOVE CIS-ADDRESS-TXT ( 1,2 ) TO #FIRST-ANNUIT-MAIL-ADDR1 ( 2 )
                2));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Mail_Addr1().getValue(3).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(1,        //Natural: MOVE CIS-ADDRESS-TXT ( 1,3 ) TO #FIRST-ANNUIT-MAIL-ADDR1 ( 3 )
                3));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Mail_Addr1().getValue(4).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(1,        //Natural: MOVE CIS-ADDRESS-TXT ( 1,4 ) TO #FIRST-ANNUIT-MAIL-ADDR1 ( 4 )
                4));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Mail_Addr1().getValue(5).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(1,        //Natural: MOVE CIS-ADDRESS-TXT ( 1,5 ) TO #FIRST-ANNUIT-MAIL-ADDR1 ( 5 )
                5));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Annuit_Zipcde().setValue(ldaCispartv.getCis_Part_View_Cis_Zip_Code().getValue(1));                         //Natural: MOVE CIS-ZIP-CODE ( 1 ) TO #FIRST-ANNUIT-ZIPCDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Ownership_Cde().reset();                                                                                             //Natural: RESET #OWNERSHIP-CDE #CHECK-MAILING-ADDR ( * ) #BANK-ACCNT-NO #BANK-TRANSIT-CODE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue("*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Bank_Accnt_No().reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Bank_Transit_Code().reset();
        if (condition(((ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("TPA") || ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IPRO")) && ((ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("CASH")  //Natural: IF ( ( CIS-RQST-ID = 'TPA' OR = 'IPRO' ) AND ( CIS-ANNTY-OPTION = 'CASH' OR = 'EXTR' OR = 'CASX' ) )
            || ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("EXTR")) || ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("CASX")))))
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Bank_Accnt_No().setValue(ldaCispartv.getCis_Part_View_Cis_Bank_Pymnt_Acct_Nmbr().getValue(2));                   //Natural: MOVE CIS-BANK-PYMNT-ACCT-NMBR ( 2 ) TO #BANK-ACCNT-NO
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Bank_Transit_Code().setValue(ldaCispartv.getCis_Part_View_Cis_Bank_Aba_Acct_Nmbr().getValue(2));                 //Natural: MOVE CIS-BANK-ABA-ACCT-NMBR ( 2 ) TO #BANK-TRANSIT-CODE
            if (condition(ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("CASH")))                                                                                //Natural: IF CIS-ANNTY-OPTION = 'CASH'
            {
                short decideConditionsMet3470 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF CIS-ADDR-USAGE-CODE ( 1 );//Natural: VALUE '3'
                if (condition((ldaCispartv.getCis_Part_View_Cis_Addr_Usage_Code().getValue(1).equals("3"))))
                {
                    decideConditionsMet3470++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Ownership_Cde().setValue(1);                                                                             //Natural: MOVE 1 TO #OWNERSHIP-CDE
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    if (condition(ldaCispartv.getCis_Part_View_Cis_Bank_Pymnt_Acct_Nmbr().getValue(2).equals(" ")))                                                       //Natural: IF CIS-BANK-PYMNT-ACCT-NMBR ( 2 ) = ' '
                    {
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Ownership_Cde().setValue(1);                                                                         //Natural: MOVE 1 TO #OWNERSHIP-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Ownership_Cde().setValue(2);                                                                         //Natural: MOVE 2 TO #OWNERSHIP-CDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet3482 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF CIS-ADDR-USAGE-CODE ( 1 );//Natural: VALUE '3'
            if (condition((ldaCispartv.getCis_Part_View_Cis_Addr_Usage_Code().getValue(1).equals("3"))))
            {
                decideConditionsMet3482++;
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(1).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(1,         //Natural: MOVE CIS-ADDRESS-TXT ( 1,1 ) TO #CHECK-MAILING-ADDR ( 1 )
                    1));
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(2).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(1,         //Natural: MOVE CIS-ADDRESS-TXT ( 1,2 ) TO #CHECK-MAILING-ADDR ( 2 )
                    2));
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(3).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(1,         //Natural: MOVE CIS-ADDRESS-TXT ( 1,3 ) TO #CHECK-MAILING-ADDR ( 3 )
                    3));
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(4).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(1,         //Natural: MOVE CIS-ADDRESS-TXT ( 1,4 ) TO #CHECK-MAILING-ADDR ( 4 )
                    4));
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(5).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(1,         //Natural: MOVE CIS-ADDRESS-TXT ( 1,5 ) TO #CHECK-MAILING-ADDR ( 5 )
                    5));
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                if (condition(ldaCispartv.getCis_Part_View_Cis_Address_Dest_Name().getValue(2).notEquals(" ")))                                                           //Natural: IF CIS-ADDRESS-DEST-NAME ( 2 ) NE ' '
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(1).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Dest_Name().getValue(2)); //Natural: MOVE CIS-ADDRESS-DEST-NAME ( 2 ) TO #CHECK-MAILING-ADDR ( 1 )
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(2).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(2,     //Natural: MOVE CIS-ADDRESS-TXT ( 2,1 ) TO #CHECK-MAILING-ADDR ( 2 )
                        1));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(3).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(2,     //Natural: MOVE CIS-ADDRESS-TXT ( 2,2 ) TO #CHECK-MAILING-ADDR ( 3 )
                        2));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(4).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(2,     //Natural: MOVE CIS-ADDRESS-TXT ( 2,3 ) TO #CHECK-MAILING-ADDR ( 4 )
                        3));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(5).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(2,     //Natural: MOVE CIS-ADDRESS-TXT ( 2,4 ) TO #CHECK-MAILING-ADDR ( 5 )
                        4));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(1).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(2,     //Natural: MOVE CIS-ADDRESS-TXT ( 2,1 ) TO #CHECK-MAILING-ADDR ( 1 )
                        1));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(2).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(2,     //Natural: MOVE CIS-ADDRESS-TXT ( 2,2 ) TO #CHECK-MAILING-ADDR ( 2 )
                        2));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(3).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(2,     //Natural: MOVE CIS-ADDRESS-TXT ( 2,3 ) TO #CHECK-MAILING-ADDR ( 3 )
                        3));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(4).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(2,     //Natural: MOVE CIS-ADDRESS-TXT ( 2,4 ) TO #CHECK-MAILING-ADDR ( 4 )
                        4));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(5).setValue(ldaCispartv.getCis_Part_View_Cis_Address_Txt().getValue(2,     //Natural: MOVE CIS-ADDRESS-TXT ( 2,5 ) TO #CHECK-MAILING-ADDR ( 5 )
                        5));
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            F1:                                                                                                                                                           //Natural: FOR #J 5 1 -1
            for (pnd_J.setValue(5); condition(pnd_J.greaterOrEqual(1)); pnd_J.nsubtract(1))
            {
                if (condition(ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(pnd_J).notEquals(" ")))                                          //Natural: IF #CHECK-MAILING-ADDR ( #J ) NE ' '
                {
                    pnd_Addr_Zp.setValue(ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(pnd_J));                                              //Natural: ASSIGN #ADDR-ZP := #CHECK-MAILING-ADDR ( #J )
                    DbsUtil.examine(new ExamineSource(pnd_Addr_Zp,1,1), new ExamineSearch(pnd_Addr_Zp), new ExamineGivingLength(pnd_Addr_Len));                           //Natural: EXAMINE SUBSTRING ( #ADDR-ZP,1 ) FOR #ADDR-ZP GIVING LENGTH #ADDR-LEN
                    if (condition((pnd_Addr_Len.add(6)).greater(35) && pnd_J.less(5)))                                                                                    //Natural: IF ( #ADDR-LEN + 6 ) GT 35 AND #J LT 5
                    {
                        if (condition(ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(pnd_J.getDec().add(1)).equals(" ")))                     //Natural: IF #CHECK-MAILING-ADDR ( #J + 1 ) = ' '
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(pnd_J.getDec().add(1)).setValue(ldaCispartv.getCis_Part_View_Cis_Zip_Code().getValue(2)); //Natural: ASSIGN #CHECK-MAILING-ADDR ( #J + 1 ) := CIS-ZIP-CODE ( 2 )
                            if (true) break F1;                                                                                                                           //Natural: ESCAPE BOTTOM ( F1. )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_J.equals(5) && (pnd_Addr_Len.add(6)).greater(35)))                                                                              //Natural: IF #J = 5 AND ( #ADDR-LEN + 6 ) GT 35
                        {
                            if (true) break F1;                                                                                                                           //Natural: ESCAPE BOTTOM ( F1. )
                        }                                                                                                                                                 //Natural: END-IF
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(pnd_J).setValue(DbsUtil.compress(ldaCislpart.getPnd_Participant_Work_File_Pnd_Check_Mailing_Addr().getValue(pnd_J),  //Natural: COMPRESS #CHECK-MAILING-ADDR ( #J ) ' ' CIS-ZIP-CODE ( 2 ) INTO #CHECK-MAILING-ADDR ( #J ) LEAVING
                            " ", ldaCispartv.getCis_Part_View_Cis_Zip_Code().getValue(2)));
                        if (true) break F1;                                                                                                                               //Natural: ESCAPE BOTTOM ( F1. )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Frst_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                      //Natural: EXAMINE SUBSTRING ( CIS-SCND-ANNT-FRST-NME,2,29 ) TRANSLATE INTO LOWER CASE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_S().setValue(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Frst_Nme());                                      //Natural: MOVE CIS-SCND-ANNT-FRST-NME TO #FIRST-NAME-S
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_S1().setValue("@");                                                                                           //Natural: MOVE '@' TO #FILLER-S1
        DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Mid_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                       //Natural: EXAMINE SUBSTRING ( CIS-SCND-ANNT-MID-NME,2,29 ) TRANSLATE INTO LOWER CASE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mid_Name_S().setValue(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Mid_Nme());                                         //Natural: MOVE CIS-SCND-ANNT-MID-NME TO #MID-NAME-S
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_S2().setValue("@");                                                                                           //Natural: MOVE '@' TO #FILLER-S2
        DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Lst_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                       //Natural: EXAMINE SUBSTRING ( CIS-SCND-ANNT-LST-NME,2,29 ) TRANSLATE INTO LOWER CASE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_S().setValue(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Lst_Nme());                                        //Natural: MOVE CIS-SCND-ANNT-LST-NME TO #LAST-NAME-S
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Cntrct_Name().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_S(),  //Natural: COMPRESS #FIRST-NAME-S #FILLER-S1 #MID-NAME-S #FILLER-S2 #LAST-NAME-S TO #SECOND-ANNUIT-CNTRCT-NAME LEAVING NO SPACE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_S1(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Mid_Name_S(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_S2(), 
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_S()));
        DbsUtil.examine(new ExamineSource(ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Cntrct_Name()), new ExamineSearch("@"), new ExamineReplace(" "));    //Natural: EXAMINE #SECOND-ANNUIT-CNTRCT-NAME FOR '@' REPLACE WITH ' '
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().equals("Y") && ldaCispartv.getCis_Part_View_Cis_Ownership_Cd().equals(1))) //Natural: IF CIS-RQST-ID = 'IA' AND CIS-TRNSF-FLAG = 'Y' AND CIS-OWNERSHIP-CD = 1
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Cntrct_Name().setValue(DbsUtil.compress(ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Cntrct_Name(),  //Natural: COMPRESS #SECOND-ANNUIT-CNTRCT-NAME '- Deceased' INTO #SECOND-ANNUIT-CNTRCT-NAME
                "- Deceased"));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Cntrct_Name().equals(" ")))                                                              //Natural: IF #SECOND-ANNUIT-CNTRCT-NAME = ' '
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Cntrct_Name().setValue("None");                                                                    //Natural: MOVE 'None' TO #SECOND-ANNUIT-CNTRCT-NAME
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Ssn().setValue(" ");                                                                                   //Natural: MOVE ' ' TO #SECOND-ANNUIT-SSN
        if (condition(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Ssn().greater(getZero())))                                                                               //Natural: IF CIS-SCND-ANNT-SSN GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Ssn().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Ssn(),new ReportEditMask("999-99-9999")); //Natural: MOVE EDITED CIS-SCND-ANNT-SSN ( EM = 999-99-9999 ) TO #SECOND-ANNUIT-SSN
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Second_Annuit_Dob().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Scnd_Annt_Dob(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED CIS-SCND-ANNT-DOB ( EM = YYYYMMDD ) TO #SECOND-ANNUIT-DOB
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Grade_Amt_Ind().setValue(" ");                                                                                       //Natural: MOVE ' ' TO #GRADE-AMT-IND
        if (condition(ldaCispartv.getCis_Part_View_Cis_Cntrct_Type().equals("T") && ldaCispartv.getCis_Part_View_Cis_Grnted_Grd_Amt().greater(getZero())))                //Natural: IF CIS-CNTRCT-TYPE = 'T' AND CIS-GRNTED-GRD-AMT > 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Grade_Amt_Ind().setValue("G");                                                                                   //Natural: MOVE 'G' TO #GRADE-AMT-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Cntrct_Type().equals("T") && ldaCispartv.getCis_Part_View_Cis_Grnted_Grd_Amt().equals(getZero())               //Natural: IF CIS-CNTRCT-TYPE = 'T' AND CIS-GRNTED-GRD-AMT = 0 AND CIS-GRNTED-STD-AMT > 0
                && ldaCispartv.getCis_Part_View_Cis_Grnted_Std_Amt().greater(getZero())))
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Grade_Amt_Ind().setValue("S");                                                                               //Natural: MOVE 'S' TO #GRADE-AMT-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Grnted_Grd_Amt().equals(getZero())))                                                                               //Natural: IF CIS-GRNTED-GRD-AMT = 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Graded_Amt().setValue("          0.00");                                                                    //Natural: MOVE '          0.00' TO #TIAA-GRADED-AMT
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_G().setValue("$");                                                                                        //Natural: MOVE '$' TO #DOLLAR-G
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_G().setValue("$");                                                                                        //Natural: MOVE '$' TO #DOLLAR-G
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Graded_Amt().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Grnted_Grd_Amt(),new ReportEditMask("ZZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED CIS-GRNTED-GRD-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #TIAA-GRADED-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Grnted_Std_Amt().equals(getZero())))                                                                               //Natural: IF CIS-GRNTED-STD-AMT = 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Standard_Amt().setValue("          0.00");                                                                  //Natural: MOVE '          0.00' TO #TIAA-STANDARD-AMT
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_Tot().setValue("$");                                                                                      //Natural: MOVE '$' TO #DOLLAR-TOT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_Tot().setValue("$");                                                                                      //Natural: MOVE '$' TO #DOLLAR-TOT
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Standard_Amt().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Grnted_Std_Amt(),new ReportEditMask("ZZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED CIS-GRNTED-STD-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #TIAA-STANDARD-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Grnted_Std_Amt().equals(getZero()) && ldaCispartv.getCis_Part_View_Cis_Grnted_Grd_Amt().equals(getZero())))        //Natural: IF CIS-GRNTED-STD-AMT = 0 AND CIS-GRNTED-GRD-AMT = 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Tot_Grd_Stnd_Amt().setValue("          0.00");                                                              //Natural: MOVE '          0.00' TO #TIAA-TOT-GRD-STND-AMT
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_S().setValue("$");                                                                                        //Natural: MOVE '$' TO #DOLLAR-S
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Tot_Grd_Stnd_Amt_N().compute(new ComputeParameters(false, ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Tot_Grd_Stnd_Amt_N()),  //Natural: ADD CIS-GRNTED-STD-AMT CIS-GRNTED-GRD-AMT GIVING #TIAA-TOT-GRD-STND-AMT-N
                ldaCispartv.getCis_Part_View_Cis_Grnted_Std_Amt().add(ldaCispartv.getCis_Part_View_Cis_Grnted_Grd_Amt()));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_S().setValue("$");                                                                                        //Natural: MOVE '$' TO #DOLLAR-S
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Tot_Grd_Stnd_Amt().setValueEdited(ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Tot_Grd_Stnd_Amt_N(),new  //Natural: MOVE EDITED #TIAA-TOT-GRD-STND-AMT-N ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #TIAA-TOT-GRD-STND-AMT
                ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************* FOR CIS-TIAA-COMMUTED-INFO ***********************
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Comm_Amt_Ci().getValue("*").setValue(" ");                                                                           //Natural: MOVE ' ' TO #COMM-AMT-CI ( * ) #COMM-INT-RATE-CI ( * ) #COMM-METHOD-CI ( * )
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Comm_Int_Rate_Ci().getValue("*").setValue(" ");
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Comm_Method_Ci().getValue("*").setValue(" ");
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_Ci().getValue("*").setValue(" ");                                                                             //Natural: MOVE ' ' TO #DOLLAR-CI ( * )
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Multiple_Com_Int_Rate().setValue(" ");                                                                               //Natural: MOVE ' ' TO #MULTIPLE-COM-INT-RATE #TRADITIONAL-G-I-R
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Traditional_G_I_R().setValue(" ");
        pnd_Found_Different.reset();                                                                                                                                      //Natural: RESET #FOUND-DIFFERENT
        if (condition(ldaCispartv.getCis_Part_View_Cis_Comut_Grnted_Amt().getValue(1).greater(getZero()) && ldaCispartv.getCis_Part_View_Cis_Comut_Grnted_Amt().getValue(2).greater(getZero()))) //Natural: IF CIS-COMUT-GRNTED-AMT ( 1 ) GT 0 AND CIS-COMUT-GRNTED-AMT ( 2 ) GT 0
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Comut_Int_Rate().getValue(1).greater(getZero())))                                                              //Natural: IF CIS-COMUT-INT-RATE ( 1 ) GT 0
            {
                pnd_Hold_Int_Rate.setValue(ldaCispartv.getCis_Part_View_Cis_Comut_Int_Rate().getValue(1));                                                                //Natural: ASSIGN #HOLD-INT-RATE := CIS-COMUT-INT-RATE ( 1 )
                FOR04:                                                                                                                                                    //Natural: FOR #I = 1 TO 5
                for (ldaAppl170.getPnd_Work_Fields_Pnd_I().setValue(1); condition(ldaAppl170.getPnd_Work_Fields_Pnd_I().lessOrEqual(5)); ldaAppl170.getPnd_Work_Fields_Pnd_I().nadd(1))
                {
                    if (condition(ldaCispartv.getCis_Part_View_Cis_Comut_Int_Rate().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).greater(getZero())))                  //Natural: IF CIS-COMUT-INT-RATE ( #I ) GT 0
                    {
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Comut_Int_Rate().getValue(ldaAppl170.getPnd_Work_Fields_Pnd_I()).notEquals(pnd_Hold_Int_Rate)))    //Natural: IF CIS-COMUT-INT-RATE ( #I ) NE #HOLD-INT-RATE
                        {
                            pnd_Found_Different.setValue(true);                                                                                                           //Natural: ASSIGN #FOUND-DIFFERENT := TRUE
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Found_Different.getBoolean()))                                                                                                              //Natural: IF #FOUND-DIFFERENT
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Multiple_Com_Int_Rate().setValue("Y");                                                                       //Natural: MOVE 'Y' TO #MULTIPLE-COM-INT-RATE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Comut_Grnted_Amt().getValue(1).greater(getZero()) && ! (pnd_Found_Different.getBoolean())))                        //Natural: IF CIS-COMUT-GRNTED-AMT ( 1 ) GT 0 AND NOT #FOUND-DIFFERENT
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Multiple_Com_Int_Rate().setValue("N");                                                                           //Natural: MOVE 'N' TO #MULTIPLE-COM-INT-RATE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Traditional_G_I_R().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Comut_Int_Rate().getValue(1),new             //Natural: MOVE EDITED CIS-COMUT-INT-RATE ( 1 ) ( EM = 99.99 ) TO #TRADITIONAL-G-I-R
                ReportEditMask("99.99"));
        }                                                                                                                                                                 //Natural: END-IF
        FOR05:                                                                                                                                                            //Natural: FOR #X 1 5
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(5)); pnd_X.nadd(1))
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Comut_Grnted_Amt().getValue(pnd_X).greater(getZero())))                                                        //Natural: IF CIS-COMUT-GRNTED-AMT ( #X ) GT 0
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_Ci().getValue(pnd_X).setValue("$");                                                                   //Natural: MOVE '$' TO #DOLLAR-CI ( #X )
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Comm_Amt_Ci().getValue(pnd_X).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Comut_Grnted_Amt().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-COMUT-GRNTED-AMT ( #X ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #COMM-AMT-CI ( #X )
                    ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Comm_Int_Rate_Ci().getValue(pnd_X).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Comut_Int_Rate().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-COMUT-INT-RATE ( #X ) ( EM = 99.99 ) TO #COMM-INT-RATE-CI ( #X )
                    ReportEditMask("99.99"));
                DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Comut_Pymt_Method().getValue(pnd_X),2,7), new ExamineTranslate(TranslateOption.Lower)); //Natural: EXAMINE SUBSTRING ( CIS-COMUT-PYMT-METHOD ( #X ) ,2,7 ) TRANSLATE INTO LOWER CASE
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Comm_Method_Ci().getValue(pnd_X).setValue(ldaCispartv.getCis_Part_View_Cis_Comut_Pymt_Method().getValue(pnd_X)); //Natural: MOVE CIS-COMUT-PYMT-METHOD ( #X ) TO #COMM-METHOD-CI ( #X )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *
        //* *  NEW CODE FOR NEW MORTILITY BASIS FOR IA TIAA PHASE III
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Commuted_Intr_Num().reset();                                                                                         //Natural: RESET #COMMUTED-INTR-NUM #Y #COMUT-GRNTED-AMT-DOLLAR ( * ) #COMUT-GRNTED-AMT ( * ) #COMUT-INTR-RATE ( * ) #COMUT-PYMT-METHOD ( * ) #COMUT-MORTALITY-BASIS ( * )
        pnd_Y.reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Comut_Grnted_Amt_Dollar().getValue("*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Comut_Grnted_Amt().getValue("*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Comut_Intr_Rate().getValue("*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Comut_Pymt_Method().getValue("*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Comut_Mortality_Basis().getValue("*").reset();
        FOR06:                                                                                                                                                            //Natural: FOR #X 1 20
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Comut_Grnted_Amt_2().getValue(pnd_X).greater(getZero())))                                                      //Natural: IF CIS-COMUT-GRNTED-AMT-2 ( #X ) GT 0
            {
                pnd_Y.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #Y
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Commuted_Intr_Num().nadd(1);                                                                                 //Natural: ADD 1 TO #COMMUTED-INTR-NUM
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Comut_Grnted_Amt_Dollar().getValue(pnd_Y).setValue("$");                                                     //Natural: MOVE '$' TO #COMUT-GRNTED-AMT-DOLLAR ( #Y )
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Comut_Grnted_Amt().getValue(pnd_Y).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Comut_Grnted_Amt_2().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-COMUT-GRNTED-AMT-2 ( #X ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #COMUT-GRNTED-AMT ( #Y )
                    ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Comut_Intr_Rate().getValue(pnd_Y).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Comut_Int_Rate_2().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-COMUT-INT-RATE-2 ( #X ) ( EM = 99.99 ) TO #COMUT-INTR-RATE ( #Y )
                    ReportEditMask("99.99"));
                DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Comut_Pymt_Method_2().getValue(pnd_X),2,7), new ExamineTranslate(TranslateOption.Lower)); //Natural: EXAMINE SUBSTRING ( CIS-COMUT-PYMT-METHOD-2 ( #X ) ,2,7 ) TRANSLATE INTO LOWER CASE
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Comut_Pymt_Method().getValue(pnd_Y).setValue(ldaCispartv.getCis_Part_View_Cis_Comut_Pymt_Method_2().getValue(pnd_X)); //Natural: MOVE CIS-COMUT-PYMT-METHOD-2 ( #X ) TO #COMUT-PYMT-METHOD ( #Y )
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Comut_Mortality_Basis().getValue(pnd_Y).setValue(ldaCispartv.getCis_Part_View_Cis_Comut_Mortality_Basis().getValue(pnd_X)); //Natural: MOVE CIS-COMUT-MORTALITY-BASIS ( #X ) TO #COMUT-MORTALITY-BASIS ( #Y )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* **
        //* *
        if (condition(ldaCispartv.getCis_Part_View_Cis_Tiaa_Doi().equals(ldaCispartv.getCis_Part_View_Cis_Annty_Start_Dte())))                                            //Natural: IF CIS-TIAA-DOI EQ CIS-ANNTY-START-DTE
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Issue_Equal_Frst_Pymt_Dte().setValue("Y");                                                                       //Natural: MOVE 'Y' TO #ISSUE-EQUAL-FRST-PYMT-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Issue_Equal_Frst_Pymt_Dte().setValue("N");                                                                       //Natural: MOVE 'N' TO #ISSUE-EQUAL-FRST-PYMT-DTE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Surivor_Reduction_Literal().reset();                                                                                 //Natural: RESET #SURIVOR-REDUCTION-LITERAL
        if (condition(ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("LS")))                                                                                      //Natural: IF CIS-ANNTY-OPTION = 'LS'
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Surivor_Reduction_Literal().setValue("One-Half");                                                                //Natural: MOVE 'One-Half' TO #SURIVOR-REDUCTION-LITERAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("JS")))                                                                                  //Natural: IF CIS-ANNTY-OPTION = 'JS'
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Surivor_Reduction_Literal().setValue("Two-Thirds");                                                          //Natural: MOVE 'Two-Thirds' TO #SURIVOR-REDUCTION-LITERAL
                //*  073107 START
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("LST")))                                                                             //Natural: IF CIS-ANNTY-OPTION = 'LST'
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Surivor_Reduction_Literal().setValue("75%");                                                             //Natural: MOVE '75%' TO #SURIVOR-REDUCTION-LITERAL
                    //*  073107 END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().resetInitial();                                                                                     //Natural: RESET INITIAL #GRA-SURR-RIGHT
        FOR07:                                                                                                                                                            //Natural: FOR #X 1 20
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
        {
            //*  IF #GRA-SURR-RIGHT = 'Y'                           /* BJD1
            if (condition(ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().equals("Y") || ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().equals("M"))) //Natural: IF #GRA-SURR-RIGHT = 'Y' OR = 'M'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Da_Tiaa_Nbr.setValue(ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Nbr().getValue(pnd_X));                                                                 //Natural: MOVE CIS-DA-TIAA-NBR ( #X ) TO #DA-TIAA-NBR
                pnd_Cref_Nbr.setValue(ldaCispartv.getCis_Part_View_Cis_Da_Cert_Nbr().getValue(pnd_X));                                                                    //Natural: MOVE CIS-DA-CERT-NBR ( #X ) TO #CREF-NBR
                //* *** BJD1 START ****
                //*    IF #DA-TIAA-1 = '2' AND #DA-TIAA-6 = '000001' THRU '009999'
                //*      MOVE 'Y' TO #GRA-SURR-RIGHT
                //*    ELSE
                //*      IF #DA-TIAA-1 = '2' AND #DA-TIAA-6 = '025000' THRU '999998'
                //*        MOVE 'Y' TO #GRA-SURR-RIGHT
                //*      ELSE
                //*        IF #DA-TIAA-1 = '3' AND #DA-TIAA-6 = '000010' THRU '999999'
                //*          MOVE 'Y' TO #GRA-SURR-RIGHT
                //*        ELSE
                //*          IF #CREF-NUM-1 = '1' AND #CREF-NUM-6 = '000001' THRU '009999'
                //*            MOVE 'Y' TO #GRA-SURR-RIGHT
                //*          ELSE
                //*           IF #CREF-NUM-1 = '1' AND #CREF-NUM-6 = '025000' THRU '999998'
                //*              MOVE 'Y' TO #GRA-SURR-RIGHT
                //*            ELSE
                //*           IF #CREF-NUM-1 = '4' AND #CREF-NUM-6 = '000010' THRU '999999'
                //*                MOVE 'Y' TO #GRA-SURR-RIGHT
                //*              END-IF END-IF END-IF END-IF END-IF END-IF END-IF
                short decideConditionsMet3679 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #DA-TIAA-1 = '2' AND #DA-TIAA-6 = '000001' THRU '009999'
                if (condition(pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_1.equals("2") && pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6.greaterOrEqual("000001") && pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6.lessOrEqual("009999")))
                {
                    decideConditionsMet3679++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().setValue("Y");                                                                          //Natural: MOVE 'Y' TO #GRA-SURR-RIGHT
                }                                                                                                                                                         //Natural: WHEN #DA-TIAA-1 = '2' AND #DA-TIAA-6 = '025000' THRU '999998'
                else if (condition(pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_1.equals("2") && pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6.greaterOrEqual("025000") && pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6.lessOrEqual("999998")))
                {
                    decideConditionsMet3679++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().setValue("Y");                                                                          //Natural: MOVE 'Y' TO #GRA-SURR-RIGHT
                }                                                                                                                                                         //Natural: WHEN #DA-TIAA-1 = '3' AND #DA-TIAA-6 = '000010' THRU '999999'
                else if (condition(pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_1.equals("3") && pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6.greaterOrEqual("000010") && pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6.lessOrEqual("999999")))
                {
                    decideConditionsMet3679++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().setValue("Y");                                                                          //Natural: MOVE 'Y' TO #GRA-SURR-RIGHT
                }                                                                                                                                                         //Natural: WHEN #DA-TIAA-1 = 'C' AND #DA-TIAA-6 = '875010' THRU '891999'
                else if (condition(pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_1.equals("C") && pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6.greaterOrEqual("875010") && pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6.lessOrEqual("891999")))
                {
                    decideConditionsMet3679++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().setValue("M");                                                                          //Natural: MOVE 'M' TO #GRA-SURR-RIGHT
                }                                                                                                                                                         //Natural: WHEN #DA-TIAA-1 = 'C' AND #DA-TIAA-6 = '894030' THRU '898999'
                else if (condition(pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_1.equals("C") && pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6.greaterOrEqual("894030") && pnd_Da_Tiaa_Nbr_Pnd_Da_Tiaa_6.lessOrEqual("898999")))
                {
                    decideConditionsMet3679++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().setValue("M");                                                                          //Natural: MOVE 'M' TO #GRA-SURR-RIGHT
                }                                                                                                                                                         //Natural: WHEN #CREF-NUM-1 = '1' AND #CREF-NUM-6 = '000001' THRU '009999'
                else if (condition(pnd_Cref_Nbr_Pnd_Cref_Num_1.equals("1") && pnd_Cref_Nbr_Pnd_Cref_Num_6.greaterOrEqual("000001") && pnd_Cref_Nbr_Pnd_Cref_Num_6.lessOrEqual("009999")))
                {
                    decideConditionsMet3679++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().setValue("Y");                                                                          //Natural: MOVE 'Y' TO #GRA-SURR-RIGHT
                }                                                                                                                                                         //Natural: WHEN #CREF-NUM-1 = '1' AND #CREF-NUM-6 = '025000' THRU '999998'
                else if (condition(pnd_Cref_Nbr_Pnd_Cref_Num_1.equals("1") && pnd_Cref_Nbr_Pnd_Cref_Num_6.greaterOrEqual("025000") && pnd_Cref_Nbr_Pnd_Cref_Num_6.lessOrEqual("999998")))
                {
                    decideConditionsMet3679++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().setValue("Y");                                                                          //Natural: MOVE 'Y' TO #GRA-SURR-RIGHT
                }                                                                                                                                                         //Natural: WHEN #CREF-NUM-1 = '4' AND #CREF-NUM-6 = '000010' THRU '999999'
                else if (condition(pnd_Cref_Nbr_Pnd_Cref_Num_1.equals("4") && pnd_Cref_Nbr_Pnd_Cref_Num_6.greaterOrEqual("000010") && pnd_Cref_Nbr_Pnd_Cref_Num_6.lessOrEqual("999999")))
                {
                    decideConditionsMet3679++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().setValue("Y");                                                                          //Natural: MOVE 'Y' TO #GRA-SURR-RIGHT
                }                                                                                                                                                         //Natural: WHEN #CREF-NUM-1 = 'U' AND #CREF-NUM-6 = '875010' THRU '891999'
                else if (condition(pnd_Cref_Nbr_Pnd_Cref_Num_1.equals("U") && pnd_Cref_Nbr_Pnd_Cref_Num_6.greaterOrEqual("875010") && pnd_Cref_Nbr_Pnd_Cref_Num_6.lessOrEqual("891999")))
                {
                    decideConditionsMet3679++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().setValue("M");                                                                          //Natural: MOVE 'M' TO #GRA-SURR-RIGHT
                }                                                                                                                                                         //Natural: WHEN #CREF-NUM-1 = 'U' AND #CREF-NUM-6 = '894030' THRU '898999'
                else if (condition(pnd_Cref_Nbr_Pnd_Cref_Num_1.equals("U") && pnd_Cref_Nbr_Pnd_Cref_Num_6.greaterOrEqual("894030") && pnd_Cref_Nbr_Pnd_Cref_Num_6.lessOrEqual("898999")))
                {
                    decideConditionsMet3679++;
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Gra_Surr_Right().setValue("M");                                                                          //Natural: MOVE 'M' TO #GRA-SURR-RIGHT
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //* *** BJD1 END ****
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ***********************************************************************
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Contract_Settled_Flag_N().setValue(0);                                                                          //Natural: MOVE 0 TO #TIAA-CONTRACT-SETTLED-FLAG-N #PROCEEDS-AMT-INFO ( * ) #TIAA-REA-CNT-SETTLED-N #PROCESS-AMT ( * ) #TOT-TIAA-PAYING-PROS-AMT-H #TOT-TIAA-REA-PAYIN-PROC-AMT-H
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Proceeds_Amt_Info().getValue("*").setValue(0);
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Cnt_Settled_N().setValue(0);
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Process_Amt().getValue("*").setValue(0);
        pnd_Tot_Tiaa_Paying_Pros_Amt_H.setValue(0);
        pnd_Tot_Tiaa_Rea_Payin_Proc_Amt_H.setValue(0);
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Payin_Cntrct_Info().getValue("*").setValue(" ");                                                                  //Natural: MOVE ' ' TO #DA-PAYIN-CNTRCT-INFO ( * ) #DA-PAYIN-CNTRCT ( * )
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Payin_Cntrct().getValue("*").setValue(" ");
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Process_Dollar().getValue("*").setValue(" ");                                                                        //Natural: MOVE ' ' TO #PROCESS-$ ( * ) #TIAA-REA-PAYIN-$ ( * )
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Payin_Dollar().getValue("*").setValue(" ");
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J #K
        pnd_K.reset();
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA")))                                                                                           //Natural: IF CIS-RQST-ID EQ 'IA'
        {
            FOR08:                                                                                                                                                        //Natural: FOR #X 1 20
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
            {
                if (condition(ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Nbr().getValue(pnd_X).greater(" ")))                                                               //Natural: IF CIS-DA-TIAA-NBR ( #X ) GT ' '
                {
                    if (condition(ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Proceeds_Amt().getValue(pnd_X).greater(getZero())))                                            //Natural: IF CIS-DA-TIAA-PROCEEDS-AMT ( #X ) GT 0
                    {
                        pnd_J.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #J
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Payin_Cntrct().getValue(pnd_J).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Nbr().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-DA-TIAA-NBR ( #X ) ( EM = X-XXXXXX-X ) TO #DA-PAYIN-CNTRCT ( #J )
                            ReportEditMask("X-XXXXXX-X"));
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Process_Dollar().getValue(pnd_J).setValue("$");                                                      //Natural: MOVE '$' TO #PROCESS-$ ( #J )
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Process_Amt().getValue(pnd_J).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Proceeds_Amt().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-DA-TIAA-PROCEEDS-AMT ( #X ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #PROCESS-AMT ( #J )
                            ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
                        pnd_Tot_Tiaa_Paying_Pros_Amt_H.nadd(ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Proceeds_Amt().getValue(pnd_X));                                     //Natural: ADD CIS-DA-TIAA-PROCEEDS-AMT ( #X ) TO #TOT-TIAA-PAYING-PROS-AMT-H
                        //* *                                   #TOT-TIAA-PAYING-PROS-AMT-N
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Contract_Settled_Flag_N().nadd(1);                                                              //Natural: ADD 1 TO #TIAA-CONTRACT-SETTLED-FLAG-N
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCispartv.getCis_Part_View_Cis_Da_Rea_Proceeds_Amt().getValue(pnd_X).greater(getZero())))                                             //Natural: IF CIS-DA-REA-PROCEEDS-AMT ( #X ) GT 0
                    {
                        pnd_K.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #K
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Payin_Cntrct_Info().getValue(pnd_K).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Nbr().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-DA-TIAA-NBR ( #X ) ( EM = X-XXXXXX-X ) TO #DA-PAYIN-CNTRCT-INFO ( #K )
                            ReportEditMask("X-XXXXXX-X"));
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Payin_Dollar().getValue(pnd_K).setValue("$");                                               //Natural: MOVE '$' TO #TIAA-REA-PAYIN-$ ( #K )
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Proceeds_Amt_Info().getValue(pnd_K).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Da_Rea_Proceeds_Amt().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-DA-REA-PROCEEDS-AMT ( #X ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #PROCEEDS-AMT-INFO ( #K )
                            ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
                        pnd_Tot_Tiaa_Rea_Payin_Proc_Amt_H.nadd(ldaCispartv.getCis_Part_View_Cis_Da_Rea_Proceeds_Amt().getValue(pnd_X));                                   //Natural: ADD CIS-DA-REA-PROCEEDS-AMT ( #X ) TO #TOT-TIAA-REA-PAYIN-PROC-AMT-H
                        //* *                               #TOT-TIAA-REA-PAYIN-PROC-AMT-N
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Rea_Cnt_Settled_N().nadd(1);                                                                    //Natural: ADD 1 TO #TIAA-REA-CNT-SETTLED-N
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                 /* WB - ACCRC START
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA")))                                                                                           //Natural: IF CIS-RQST-ID = 'IA'
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Ia_Share_Class().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "R", pdaAdsa888.getPnd_Parm_Area_Pnd_Ia_Fund_Class())); //Natural: COMPRESS 'R' #IA-FUND-CLASS INTO #CIS-IA-SHARE-CLASS LEAVING NO SPACE
            //*  WB - ACCRC - END
        }                                                                                                                                                                 //Natural: END-IF
        //*  060706
        if (condition(DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'") || ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("TPA")                 //Natural: IF CIS-RQST-ID EQ MASK ( 'MDO' ) OR = 'TPA' OR = 'IPRO'
            || ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IPRO")))
        {
            FOR09:                                                                                                                                                        //Natural: FOR #X 1 20
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
            {
                if (condition(ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Nbr().getValue(pnd_X).greater(" ")))                                                               //Natural: IF CIS-DA-TIAA-NBR ( #X ) GT ' '
                {
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Payin_Cntrct().getValue(pnd_J).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Nbr().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-DA-TIAA-NBR ( #X ) ( EM = X-XXXXXX-X ) TO #DA-PAYIN-CNTRCT ( #J )
                        ReportEditMask("X-XXXXXX-X"));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Process_Dollar().getValue(pnd_J).setValue("$");                                                          //Natural: MOVE '$' TO #PROCESS-$ ( #J )
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Process_Amt().getValue(pnd_J).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Proceeds_Amt().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-DA-TIAA-PROCEEDS-AMT ( #X ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #PROCESS-AMT ( #J )
                        ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
                    pnd_Tot_Tiaa_Paying_Pros_Amt_H.nadd(ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Proceeds_Amt().getValue(pnd_X));                                         //Natural: ADD CIS-DA-TIAA-PROCEEDS-AMT ( #X ) TO #TOT-TIAA-PAYING-PROS-AMT-H
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Contract_Settled_Flag_N().nadd(1);                                                                  //Natural: ADD 1 TO #TIAA-CONTRACT-SETTLED-FLAG-N
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_Pros_Sign().setValue("$");                                                                                    //Natural: MOVE '$' TO #DOLLAR-PROS-SIGN
        if (condition(pnd_Tot_Tiaa_Paying_Pros_Amt_H.greater(getZero())))                                                                                                 //Natural: IF #TOT-TIAA-PAYING-PROS-AMT-H GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tot_Tiaa_Paying_Pros_Amt().setValueEdited(pnd_Tot_Tiaa_Paying_Pros_Amt_H,new ReportEditMask("ZZZ,ZZZ,ZZ9.99"));  //Natural: MOVE EDITED #TOT-TIAA-PAYING-PROS-AMT-H ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #TOT-TIAA-PAYING-PROS-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tot_Tiaa_Paying_Pros_Amt().setValue("          0.00");                                                           //Natural: MOVE '          0.00' TO #TOT-TIAA-PAYING-PROS-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tot_Tiaa_Rea_Dollar().setValue("$");                                                                                 //Natural: MOVE '$' TO #TOT-TIAA-REA-$
        if (condition(pnd_Tot_Tiaa_Rea_Payin_Proc_Amt_H.greater(getZero())))                                                                                              //Natural: IF #TOT-TIAA-REA-PAYIN-PROC-AMT-H GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt().setValueEdited(pnd_Tot_Tiaa_Rea_Payin_Proc_Amt_H,new ReportEditMask("ZZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED #TOT-TIAA-REA-PAYIN-PROC-AMT-H ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #TOT-TIAA-REA-PAYIN-PROC-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tot_Tiaa_Rea_Payin_Proc_Amt().setValue("          0.00");                                                        //Natural: MOVE '          0.00' TO #TOT-TIAA-REA-PAYIN-PROC-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Post_Ret_Trans_Flag().setValue(ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag());                                       //Natural: MOVE CIS-TRNSF-FLAG TO #POST-RET-TRANS-FLAG
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Units().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Trnsf_Units(),new ReportEditMask("ZZZZ9.999"));     //Natural: MOVE EDITED CIS-TRNSF-UNITS ( EM = ZZZZ9.999 ) TO #TRANSFER-UNITS
        //*  KG TRAN - ADDED ORIGINAL CREF NUMBER FOR TRANSFERS
        //*  IF CIS-TRNSF-TIAA-REA-NBR = ' '
        //*   IGNORE
        //*  ELSE
        //*   MOVE EDITED CIS-TRNSF-TIAA-REA-NBR (EM=X-XXXXXX-X)
        //*     TO #TRANSFER-PAYOUT-CNTRCT
        //*  END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Payout_Cntrct().setValue(" ");                                                                              //Natural: MOVE ' ' TO #TRANSFER-PAYOUT-CNTRCT
        if (condition(ldaCispartv.getCis_Part_View_Cis_Trnsf_Tiaa_Rea_Nbr().notEquals(" ")))                                                                              //Natural: IF CIS-TRNSF-TIAA-REA-NBR NE ' '
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Payout_Cntrct().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Trnsf_Tiaa_Rea_Nbr(),new                //Natural: MOVE EDITED CIS-TRNSF-TIAA-REA-NBR ( EM = X-XXXXXX-X ) TO #TRANSFER-PAYOUT-CNTRCT
                ReportEditMask("X-XXXXXX-X"));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Trnsf_Cert_Nbr().notEquals(" ")))                                                                              //Natural: IF CIS-TRNSF-CERT-NBR NE ' '
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Payout_Cntrct().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Trnsf_Cert_Nbr(),new                //Natural: MOVE EDITED CIS-TRNSF-CERT-NBR ( EM = X-XXXXXX-X ) TO #TRANSFER-PAYOUT-CNTRCT
                    ReportEditMask("X-XXXXXX-X"));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Fund_Name().setValue(" ");                                                                                  //Natural: MOVE ' ' TO #TRANSFER-FUND-NAME
        short decideConditionsMet3790 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF CIS-TRNSF-ACCT-CDE;//Natural: VALUE 'C'
        if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Acct_Cde().equals("C"))))
        {
            decideConditionsMet3790++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Fund_Name().setValue("Stock");                                                                          //Natural: MOVE 'Stock' TO #TRANSFER-FUND-NAME
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Acct_Cde().equals("S"))))
        {
            decideConditionsMet3790++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Fund_Name().setValue("Social Choice");                                                                  //Natural: MOVE 'Social Choice' TO #TRANSFER-FUND-NAME
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Acct_Cde().equals("E"))))
        {
            decideConditionsMet3790++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Fund_Name().setValue("Equity Index");                                                                   //Natural: MOVE 'Equity Index' TO #TRANSFER-FUND-NAME
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Acct_Cde().equals("I"))))
        {
            decideConditionsMet3790++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Fund_Name().setValue("Inflation-Linked Bond");                                                          //Natural: MOVE 'Inflation-Linked Bond' TO #TRANSFER-FUND-NAME
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Acct_Cde().equals("R"))))
        {
            decideConditionsMet3790++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Fund_Name().setValue("Real Estate");                                                                    //Natural: MOVE 'Real Estate' TO #TRANSFER-FUND-NAME
        }                                                                                                                                                                 //Natural: VALUE 'M'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Acct_Cde().equals("M"))))
        {
            decideConditionsMet3790++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Fund_Name().setValue("Money Market");                                                                   //Natural: MOVE 'Money Market' TO #TRANSFER-FUND-NAME
        }                                                                                                                                                                 //Natural: VALUE 'W'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Acct_Cde().equals("W"))))
        {
            decideConditionsMet3790++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Fund_Name().setValue("Global Equities");                                                                //Natural: MOVE 'Global Equities' TO #TRANSFER-FUND-NAME
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Acct_Cde().equals("L"))))
        {
            decideConditionsMet3790++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Fund_Name().setValue("Growth");                                                                         //Natural: MOVE 'Growth' TO #TRANSFER-FUND-NAME
        }                                                                                                                                                                 //Natural: VALUE 'B'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_Acct_Cde().equals("B"))))
        {
            decideConditionsMet3790++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Fund_Name().setValue("Bond");                                                                           //Natural: MOVE 'Bond' TO #TRANSFER-FUND-NAME
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* **
        //*  TIAA, CREF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Transfer_Cmp_Name().setValue(ldaCispartv.getCis_Part_View_Cis_Trnsf_From_Company());                                 //Natural: MOVE CIS-TRNSF-FROM-COMPANY TO #TRANSFER-CMP-NAME
        //* **
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue(" ");                                                                                //Natural: MOVE ' ' TO #TRNSF-CNTRCT-SETTLED
        short decideConditionsMet3817 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF CIS-TRNSF-TO-COMPANY;//Natural: VALUE 'IR00'
        if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("IR00"))))
        {
            decideConditionsMet3817++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("IR");                                                                           //Natural: MOVE 'IR' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: VALUE 'OL00' , 'OL10' , 'OL15' , 'OL20'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("OL00") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("OL10") 
            || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("OL15") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("OL20"))))
        {
            decideConditionsMet3817++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("ON");                                                                           //Natural: MOVE 'ON' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: VALUE 'SL00'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("SL00"))))
        {
            decideConditionsMet3817++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("SL");                                                                           //Natural: MOVE 'SL' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: VALUE 'SL10' , 'SL15' , 'SL20'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("SL10") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("SL15") 
            || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("SL20"))))
        {
            decideConditionsMet3817++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("SG");                                                                           //Natural: MOVE 'SG' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: VALUE 'J 00'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("J 00"))))
        {
            decideConditionsMet3817++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("JS");                                                                           //Natural: MOVE 'JS' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: VALUE 'J 10' , 'J 15' , 'J 20'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("J 10") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("J 15") 
            || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("J 20"))))
        {
            decideConditionsMet3817++;
            //*  073107
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("JG");                                                                           //Natural: MOVE 'JG' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: VALUE 'LH00' , 'LF00' , 'LT00'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LH00") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LF00") 
            || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LT00"))))
        {
            decideConditionsMet3817++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("LS");                                                                           //Natural: MOVE 'LS' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: VALUE 'LH10' , 'LH15' , 'LH20' ,'LT10' , 'LT15' , 'LT20'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LH10") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LH15") 
            || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LH20") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LT10") 
            || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LT15") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LT20"))))
        {
            decideConditionsMet3817++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("LG");                                                                           //Natural: MOVE 'LG' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: VALUE 'LF10' , 'LF15' , 'LF20'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LF10") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LF15") 
            || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("LF20"))))
        {
            decideConditionsMet3817++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("LG");                                                                           //Natural: MOVE 'LG' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: VALUE 'TH00' , 'TH10' , 'TH15' , 'TH20'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TH00") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TH10") 
            || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TH15") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TH20"))))
        {
            decideConditionsMet3817++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("TL");                                                                           //Natural: MOVE 'TL' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: VALUE 'TF00' , 'TF10' , 'TF15' , 'TF20'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TF00") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TF10") 
            || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TF15") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TF20"))))
        {
            decideConditionsMet3817++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("TL");                                                                           //Natural: MOVE 'TL' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: VALUE 'TJ00' , 'TJ10' , 'TJ15' , 'TJ20'
        else if (condition((ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TJ00") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TJ10") 
            || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TJ15") || ldaCispartv.getCis_Part_View_Cis_Trnsf_To_Company().equals("TJ20"))))
        {
            decideConditionsMet3817++;
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Trnsf_Cntrct_Settled().setValue("TL");                                                                           //Natural: MOVE 'TL' TO #TRNSF-CNTRCT-SETTLED
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* **
        //* ************** FOR CIS-CREF-ANNTY-PYMNT (A-B-C)*********************
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Num_Cref_Payout_Account_N().setValue(0);                                                                             //Natural: MOVE 0 TO #NUM-CREF-PAYOUT-ACCOUNT-N #W
        pnd_W.setValue(0);
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue("*").setValue(" ");                                                                               //Natural: MOVE ' ' TO #ACCOUNT ( * ) #MONTHLY-UNITS ( * ) #ANNUAL-UNITS ( * ) #CREF-ANNUAL-UNITS #TIAA-ACCESS-ACCOUNT #TIAA-ACCESS-MTHLY-UNIT #TIAA-ACCESS-ANNUAL-UNIT
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Monthly_Units().getValue("*").setValue(" ");
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Units().getValue("*").setValue(" ");
        pnd_Cref_Annual_Units.setValue(" ");
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Account().setValue(" ");
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Mthly_Unit().setValue(" ");
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Annual_Unit().setValue(" ");
        //*  060706
        if (condition(DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'") || ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA")))                //Natural: IF CIS-RQST-ID = MASK ( 'MDO' ) OR = 'IA'
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Cor_Sync_Ind().equals("G") || ldaCispartv.getCis_Part_View_Cis_Cor_Sync_Ind().equals("S")))                    //Natural: IF CIS-COR-SYNC-IND EQ 'G' OR CIS-COR-SYNC-IND EQ 'S'
            {
                                                                                                                                                                          //Natural: PERFORM GET-FUND-NAME
                sub_Get_Fund_Name();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  KG TACC
                if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Tacc_Ind().getValue(1).equals("A")))            //Natural: IF CIS-RQST-ID = 'IA' AND CIS-TACC-IND ( 1 ) = 'A'
                {
                    //*  KG TACC
                                                                                                                                                                          //Natural: PERFORM GET-FUND-NAME-IA
                    sub_Get_Fund_Name_Ia();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  KG TACC
                }                                                                                                                                                         //Natural: END-IF
                //*  CREA
                if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Sg_Fund_Ticker().getValue(1).notEquals(" ")))   //Natural: IF CIS-RQST-ID = 'IA' AND CIS-SG-FUND-TICKER ( 1 ) NE ' '
                {
                    //*  STARTS
                                                                                                                                                                          //Natural: PERFORM GET-FUND-NAME
                    sub_Get_Fund_Name();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    FOR10:                                                                                                                                                //Natural: FOR #X = 1 TO 20
                    for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
                    {
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Sg_Fund_Ticker().getValue(pnd_X).notEquals(" ")))                                                  //Natural: IF CIS-SG-FUND-TICKER ( #X ) NE ' '
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Monthly_Units().getValue(pnd_X).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Cref_Mnthly_Nbr_Units().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-CREF-MNTHLY-NBR-UNITS ( #X ) ( EM = ZZZZZZZ9.999 ) TO #MONTHLY-UNITS ( #X )
                                ReportEditMask("ZZZZZZZ9.999"));
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Units().getValue(pnd_X).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Cref_Annual_Nbr_Units().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-CREF-ANNUAL-NBR-UNITS ( #X ) ( EM = ZZZZZZZ9.999 ) TO #ANNUAL-UNITS ( #X )
                                ReportEditMask("ZZZZZZZ9.999"));
                            //*  (3356)
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                    //*  FOR CIS PRINT WHEN NO TICKER    /* CREF END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR11:                                                                                                                                                //Natural: FOR #X = 1 TO 20
                    for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
                    {
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("0") || ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals(" "))) //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = '0' OR = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Num_Cref_Payout_Account_N().nadd(1);                                                             //Natural: ADD 1 TO #NUM-CREF-PAYOUT-ACCOUNT-N
                            if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("C")))                                                  //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'C'
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_X).setValue("Stock");                                                 //Natural: MOVE 'Stock' TO #ACCOUNT ( #X )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("S")))                                                  //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'S'
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_X).setValue("Social Choice");                                         //Natural: MOVE 'Social Choice' TO #ACCOUNT ( #X )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("E")))                                                  //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'E'
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_X).setValue("Equity Index");                                          //Natural: MOVE 'Equity Index' TO #ACCOUNT ( #X )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("I")))                                                  //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'I'
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_X).setValue("Inflation-Linked Bond");                                 //Natural: MOVE 'Inflation-Linked Bond' TO #ACCOUNT ( #X )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("R")))                                                  //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'R'
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_X).setValue("Real Estate");                                           //Natural: MOVE 'Real Estate' TO #ACCOUNT ( #X )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("M")))                                                  //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'M'
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_X).setValue("Money Market");                                          //Natural: MOVE 'Money Market' TO #ACCOUNT ( #X )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("W")))                                                  //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'W'
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_X).setValue("Global Equities");                                       //Natural: MOVE 'Global Equities' TO #ACCOUNT ( #X )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("L")))                                                  //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'L'
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_X).setValue("Growth");                                                //Natural: MOVE 'Growth' TO #ACCOUNT ( #X )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("B")))                                                  //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'B'
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_X).setValue("Bond Market");                                           //Natural: MOVE 'Bond Market' TO #ACCOUNT ( #X )
                            }                                                                                                                                             //Natural: END-IF
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Monthly_Units().getValue(pnd_X).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Cref_Mnthly_Nbr_Units().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-CREF-MNTHLY-NBR-UNITS ( #X ) ( EM = ZZZZZZZ9.999 ) TO #MONTHLY-UNITS ( #X )
                                ReportEditMask("ZZZZZZZ9.999"));
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Units().getValue(pnd_X).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Cref_Annual_Nbr_Units().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-CREF-ANNUAL-NBR-UNITS ( #X ) ( EM = ZZZZZZZ9.999 ) TO #ANNUAL-UNITS ( #X )
                                ReportEditMask("ZZZZZZZ9.999"));
                            //*  IF ACCT-CDE = '0' OR ' '
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                    //*  (3356) IF IA AND TICKER(1) NE ' '            /* CREA
                }                                                                                                                                                         //Natural: END-IF
                //*  IF COR-SYNC-IND = G OR S
            }                                                                                                                                                             //Natural: END-IF
            //*  IF MDO AND IA
        }                                                                                                                                                                 //Natural: END-IF
        //*  060706
        if (condition(! (DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'")) && ldaCispartv.getCis_Part_View_Cis_Rqst_Id().notEquals("IA")))         //Natural: IF CIS-RQST-ID NE MASK ( 'MDO' ) AND CIS-RQST-ID NE 'IA'
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Cor_Sync_Ind().equals("S") || ldaCispartv.getCis_Part_View_Cis_Cor_Sync_Ind().equals("G")))                    //Natural: IF CIS-COR-SYNC-IND EQ 'S' OR CIS-COR-SYNC-IND EQ 'G'
            {
                                                                                                                                                                          //Natural: PERFORM GET-FUND-NAME
                sub_Get_Fund_Name();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR12:                                                                                                                                                    //Natural: FOR #X = 1 TO 20
                for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
                {
                    if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("0") || ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals(" "))) //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = '0' OR = ' '
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Num_Cref_Payout_Account_N().nadd(1);                                                                 //Natural: ADD 1 TO #NUM-CREF-PAYOUT-ACCOUNT-N
                        pnd_W.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #W
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("T")))                                                      //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'T'
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_W).setValue("TIAA Traditional");                                          //Natural: MOVE 'TIAA Traditional' TO #ACCOUNT ( #W )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("C")))                                                      //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'C'
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_W).setValue("CREF Stock");                                                //Natural: MOVE 'CREF Stock' TO #ACCOUNT ( #W )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("S")))                                                      //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'S'
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_W).setValue("CREF Social Choice");                                        //Natural: MOVE 'CREF Social Choice' TO #ACCOUNT ( #W )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("E")))                                                      //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'E'
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_W).setValue("CREF Equity Index");                                         //Natural: MOVE 'CREF Equity Index' TO #ACCOUNT ( #W )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("I")))                                                      //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'I'
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_W).setValue("CREF Inflation-Linked Bond");                                //Natural: MOVE 'CREF Inflation-Linked Bond' TO #ACCOUNT ( #W )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("R")))                                                      //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'R'
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_W).setValue("TIAA Real Estate");                                          //Natural: MOVE 'TIAA Real Estate' TO #ACCOUNT ( #W )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("M")))                                                      //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'M'
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_W).setValue("CREF Money Market");                                         //Natural: MOVE 'CREF Money Market' TO #ACCOUNT ( #W )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("W")))                                                      //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'W'
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_W).setValue("CREF Global Equities");                                      //Natural: MOVE 'CREF Global Equities' TO #ACCOUNT ( #W )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("L")))                                                      //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'L'
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_W).setValue("CREF Growth");                                               //Natural: MOVE 'CREF Growth' TO #ACCOUNT ( #W )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Acct_Cde().getValue(pnd_X).equals("B")))                                                      //Natural: IF CIS-CREF-ACCT-CDE ( #X ) = 'B'
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_W).setValue("CREF Bond Market");                                          //Natural: MOVE 'CREF Bond Market' TO #ACCOUNT ( #W )
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Cref_Annual_Units.setValueEdited(ldaCispartv.getCis_Part_View_Cis_Cref_Annual_Nbr_Units().getValue(pnd_X),new ReportEditMask("ZZZZZZZ9%"));   //Natural: MOVE EDITED CIS-CREF-ANNUAL-NBR-UNITS ( #X ) ( EM = ZZZZZZZ9% ) TO #CREF-ANNUAL-UNITS
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Units().getValue(pnd_W).setValue(pnd_Cref_Annual_Units, MoveOption.RightJustified);           //Natural: MOVE RIGHT JUSTIFIED #CREF-ANNUAL-UNITS TO #ANNUAL-UNITS ( #W )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                       /* HK >>>
        pnd_Cref_Mnth_Unts.reset();                                                                                                                                       //Natural: RESET #CREF-MNTH-UNTS #CREF-ANNL-UNTS #REA-MNTH-UNTS #REA-ANNL-UNTS #REVAL-CDE #C
        pnd_Cref_Annl_Unts.reset();
        pnd_Rea_Mnth_Unts.reset();
        pnd_Rea_Annl_Unts.reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Reval_Cde().reset();
        pnd_C.reset();
        if (condition(ldaCispartv.getCis_Part_View_Cis_Cntrct_Type().equals("B")))                                                                                        //Natural: IF CIS-PART-VIEW.CIS-CNTRCT-TYPE EQ 'B'
        {
            REP1:                                                                                                                                                         //Natural: REPEAT
            while (condition(whileTrue))
            {
                pnd_C.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #C
                if (condition(pnd_C.greater(1)))                                                                                                                          //Natural: IF #C GT 1
                {
                    if (true) break REP1;                                                                                                                                 //Natural: ESCAPE BOTTOM ( REP1. )
                }                                                                                                                                                         //Natural: END-IF
                FOR13:                                                                                                                                                    //Natural: FOR #B = 1 TO 20
                for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(20)); pnd_B.nadd(1))
                {
                    if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Mnthly_Nbr_Units().getValue(pnd_B).equals(getZero())))                                            //Natural: IF CIS-PART-VIEW.CIS-CREF-MNTHLY-NBR-UNITS ( #B ) EQ 0
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Mnthly_Nbr_Units().getValue(pnd_B).greater(getZero())))                                           //Natural: IF CIS-PART-VIEW.CIS-CREF-MNTHLY-NBR-UNITS ( #B ) GT 0
                    {
                        pnd_Cref_Mnth_Unts.setValue(true);                                                                                                                //Natural: ASSIGN #CREF-MNTH-UNTS := TRUE
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Reval_Cde().setValue("M");                                                                           //Natural: ASSIGN #REVAL-CDE := 'M'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("REP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR14:                                                                                                                                                    //Natural: FOR #D = 1 TO 20
                for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(20)); pnd_D.nadd(1))
                {
                    if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Annual_Nbr_Units().getValue(pnd_D).equals(getZero())))                                            //Natural: IF CIS-PART-VIEW.CIS-CREF-ANNUAL-NBR-UNITS ( #D ) EQ 0
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Annual_Nbr_Units().getValue(pnd_D).greater(getZero())))                                           //Natural: IF CIS-PART-VIEW.CIS-CREF-ANNUAL-NBR-UNITS ( #D ) GT 0
                    {
                        pnd_Cref_Annl_Unts.setValue(true);                                                                                                                //Natural: ASSIGN #CREF-ANNL-UNTS := TRUE
                        if (condition(pnd_Cref_Mnth_Unts.getBoolean()))                                                                                                   //Natural: IF #CREF-MNTH-UNTS
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Reval_Cde().setValue("B");                                                                       //Natural: ASSIGN #REVAL-CDE := 'B'
                            if (true) break REP1;                                                                                                                         //Natural: ESCAPE BOTTOM ( REP1. )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaCislpart.getPnd_Participant_Work_File_Pnd_Reval_Cde().setValue("A");                                                                       //Natural: ASSIGN #REVAL-CDE := 'A'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("REP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("REP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(ldaCispartv.getCis_Part_View_Cis_Rea_Mnthly_Nbr_Units().greater(getZero())))                                                                //Natural: IF CIS-PART-VIEW.CIS-REA-MNTHLY-NBR-UNITS GT 0
                {
                    pnd_Rea_Mnth_Unts.setValue(true);                                                                                                                     //Natural: ASSIGN #REA-MNTH-UNTS := TRUE
                    if (condition(pnd_Cref_Annl_Unts.getBoolean()))                                                                                                       //Natural: IF #CREF-ANNL-UNTS
                    {
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Reval_Cde().setValue("B");                                                                           //Natural: ASSIGN #REVAL-CDE := 'B'
                        if (true) break REP1;                                                                                                                             //Natural: ESCAPE BOTTOM ( REP1. )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Reval_Cde().setValue("M");                                                                           //Natural: ASSIGN #REVAL-CDE := 'M'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCispartv.getCis_Part_View_Cis_Rea_Annual_Nbr_Units().greater(getZero())))                                                                //Natural: IF CIS-PART-VIEW.CIS-REA-ANNUAL-NBR-UNITS GT 0
                {
                    if (condition(pnd_Cref_Mnth_Unts.getBoolean()))                                                                                                       //Natural: IF #CREF-MNTH-UNTS
                    {
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Reval_Cde().setValue("B");                                                                           //Natural: ASSIGN #REVAL-CDE := 'B'
                        if (true) break REP1;                                                                                                                             //Natural: ESCAPE BOTTOM ( REP1. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Rea_Mnth_Unts.getBoolean()))                                                                                                        //Natural: IF #REA-MNTH-UNTS
                    {
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Reval_Cde().setValue("B");                                                                           //Natural: ASSIGN #REVAL-CDE := 'B'
                        if (true) break REP1;                                                                                                                             //Natural: ESCAPE BOTTOM ( REP1. )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaCislpart.getPnd_Participant_Work_File_Pnd_Reval_Cde().setValue("A");                                                                           //Natural: ASSIGN #REVAL-CDE := 'A'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                       /* HK <<<
        //* ***** D ******   FOR CIS-DA-CREF-CNTRCTS-RQST  **********************
        pnd_Tot_Cref_Payin_Amt_N.setValue(0);                                                                                                                             //Natural: MOVE 0 TO #TOT-CREF-PAYIN-AMT-N #CREF-CNTRCT-SETTLED-N #PROCESS-AMOUNT ( * )
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Settled_N().setValue(0);
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Process_Amount().getValue("*").setValue(0);
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Payin_Contract().getValue("*").setValue(" ");                                                                     //Natural: MOVE ' ' TO #DA-PAYIN-CONTRACT ( * ) #DA-DOLLAR ( * )
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Dollar().getValue("*").setValue(" ");
        pnd_J.reset();                                                                                                                                                    //Natural: RESET #J
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA")))                                                                                           //Natural: IF CIS-RQST-ID = 'IA'
        {
            FOR15:                                                                                                                                                        //Natural: FOR #X 1 TO 20
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
            {
                if (condition(ldaCispartv.getCis_Part_View_Cis_Da_Cert_Nbr().getValue(pnd_X).greater(" ") && ldaCispartv.getCis_Part_View_Cis_Da_Cref_Proceeds_Amt().getValue(pnd_X).greater(getZero()))) //Natural: IF CIS-DA-CERT-NBR ( #X ) GT ' ' AND CIS-DA-CREF-PROCEEDS-AMT ( #X ) > 0
                {
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Payin_Contract().getValue(pnd_J).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Da_Cert_Nbr().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-DA-CERT-NBR ( #X ) ( EM = X-XXXXXX-X ) TO #DA-PAYIN-CONTRACT ( #J )
                        ReportEditMask("X-XXXXXX-X"));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Dollar().getValue(pnd_J).setValue("$");                                                               //Natural: MOVE '$' TO #DA-DOLLAR ( #J )
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Process_Amount().getValue(pnd_J).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Da_Cref_Proceeds_Amt().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-DA-CREF-PROCEEDS-AMT ( #X ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #PROCESS-AMOUNT ( #J )
                        ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
                    pnd_Tot_Cref_Payin_Amt_N.nadd(ldaCispartv.getCis_Part_View_Cis_Da_Cref_Proceeds_Amt().getValue(pnd_X));                                               //Natural: ADD CIS-DA-CREF-PROCEEDS-AMT ( #X ) TO #TOT-CREF-PAYIN-AMT-N
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Settled_N().nadd(1);                                                                         //Natural: ADD 1 TO #CREF-CNTRCT-SETTLED-N
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  060706
        if (condition(DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'")))                                                                           //Natural: IF CIS-RQST-ID = MASK ( 'MDO' )
        {
            FOR16:                                                                                                                                                        //Natural: FOR #X 1 TO 20
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
            {
                if (condition(ldaCispartv.getCis_Part_View_Cis_Da_Cert_Nbr().getValue(pnd_X).greater(" ")))                                                               //Natural: IF CIS-DA-CERT-NBR ( #X ) GT ' '
                {
                    pnd_J.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #J
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Payin_Contract().getValue(pnd_J).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Da_Cert_Nbr().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-DA-CERT-NBR ( #X ) ( EM = X-XXXXXX-X ) TO #DA-PAYIN-CONTRACT ( #J )
                        ReportEditMask("X-XXXXXX-X"));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Dollar().getValue(pnd_J).setValue("$");                                                               //Natural: MOVE '$' TO #DA-DOLLAR ( #J )
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Process_Amount().getValue(pnd_J).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Da_Cref_Proceeds_Amt().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-DA-CREF-PROCEEDS-AMT ( #X ) ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #PROCESS-AMOUNT ( #J )
                        ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
                    pnd_Tot_Cref_Payin_Amt_N.nadd(ldaCispartv.getCis_Part_View_Cis_Da_Cref_Proceeds_Amt().getValue(pnd_X));                                               //Natural: ADD CIS-DA-CREF-PROCEEDS-AMT ( #X ) TO #TOT-CREF-PAYIN-AMT-N
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Settled_N().nadd(1);                                                                         //Natural: ADD 1 TO #CREF-CNTRCT-SETTLED-N
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Dollar_Tot_Cref_Dollar().setValue("$");                                                                              //Natural: MOVE '$' TO #DOLLAR-TOT-CREF-$
        if (condition(pnd_Tot_Cref_Payin_Amt_N.greater(getZero())))                                                                                                       //Natural: IF #TOT-CREF-PAYIN-AMT-N GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tot_Cref_Payin_Amt().setValueEdited(pnd_Tot_Cref_Payin_Amt_N,new ReportEditMask("ZZZ,ZZZ,ZZ9.99"));              //Natural: MOVE EDITED #TOT-CREF-PAYIN-AMT-N ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #TOT-CREF-PAYIN-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tot_Cref_Payin_Amt().setValue("          0.00");                                                                 //Natural: MOVE '          0.00' TO #TOT-CREF-PAYIN-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_First_Annu_Dollar().setValue("$");                                                                               //Natural: MOVE '$' TO #REA-FIRST-ANNU-$
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rea_Annty_Amt().greater(getZero())))                                                                               //Natural: IF CIS-REA-ANNTY-AMT GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_First_Annuity_Payment().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Rea_Annty_Amt(),new                  //Natural: MOVE EDITED CIS-REA-ANNTY-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #REA-FIRST-ANNUITY-PAYMENT
                ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_First_Annuity_Payment().setValue("          0.00");                                                          //Natural: MOVE '          0.00' TO #REA-FIRST-ANNUITY-PAYMENT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rea_Mnthly_Nbr_Units().greater(getZero())))                                                                        //Natural: IF CIS-REA-MNTHLY-NBR-UNITS GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Mnth_Units_Payable().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Rea_Mnthly_Nbr_Units(),new              //Natural: MOVE EDITED CIS-REA-MNTHLY-NBR-UNITS ( EM = ZZZZZZZZ.999 ) TO #REA-MNTH-UNITS-PAYABLE
                ReportEditMask("ZZZZZZZZ.999"));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Mnth_Units_Payable().setValue(" 0.000");                                                                     //Natural: MOVE ' 0.000' TO #REA-MNTH-UNITS-PAYABLE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Annu_Units_Payable().setValue(" 0.000");                                                                         //Natural: MOVE ' 0.000' TO #REA-ANNU-UNITS-PAYABLE
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rea_Annual_Nbr_Units().greater(getZero())))                                                                        //Natural: IF CIS-REA-ANNUAL-NBR-UNITS GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Annu_Units_Payable().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Rea_Annual_Nbr_Units(),new              //Natural: MOVE EDITED CIS-REA-ANNUAL-NBR-UNITS ( EM = ZZZZZZZZ.999 ) TO #REA-ANNU-UNITS-PAYABLE
                ReportEditMask("ZZZZZZZZ.999"));
            short decideConditionsMet4074 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF CIS-ANNTY-OPTION;//Natural: VALUE 'LSF'
            if (condition((ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("LSF"))))
            {
                decideConditionsMet4074++;
                ldaCispartv.getCis_Part_View_Cis_Rea_Surv_Nbr_Units().setValue(ldaCispartv.getCis_Part_View_Cis_Rea_Annual_Nbr_Units());                                  //Natural: MOVE CIS-REA-ANNUAL-NBR-UNITS TO CIS-REA-SURV-NBR-UNITS
            }                                                                                                                                                             //Natural: VALUE 'LS'
            else if (condition((ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("LS"))))
            {
                decideConditionsMet4074++;
                //*  073107 START
                ldaCispartv.getCis_Part_View_Cis_Rea_Surv_Nbr_Units().compute(new ComputeParameters(true, ldaCispartv.getCis_Part_View_Cis_Rea_Surv_Nbr_Units()),         //Natural: COMPUTE ROUNDED CIS-REA-SURV-NBR-UNITS = ( CIS-REA-ANNUAL-NBR-UNITS * .50 )
                    (ldaCispartv.getCis_Part_View_Cis_Rea_Annual_Nbr_Units().multiply(new DbsDecimal(".50"))));
            }                                                                                                                                                             //Natural: VALUE 'LST'
            else if (condition((ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("LST"))))
            {
                decideConditionsMet4074++;
                //*  073107 END
                ldaCispartv.getCis_Part_View_Cis_Rea_Surv_Nbr_Units().compute(new ComputeParameters(true, ldaCispartv.getCis_Part_View_Cis_Rea_Surv_Nbr_Units()),         //Natural: COMPUTE ROUNDED CIS-REA-SURV-NBR-UNITS = ( CIS-REA-ANNUAL-NBR-UNITS * .75 )
                    (ldaCispartv.getCis_Part_View_Cis_Rea_Annual_Nbr_Units().multiply(new DbsDecimal(".75"))));
            }                                                                                                                                                             //Natural: VALUE 'JS'
            else if (condition((ldaCispartv.getCis_Part_View_Cis_Annty_Option().equals("JS"))))
            {
                decideConditionsMet4074++;
                ldaCispartv.getCis_Part_View_Cis_Rea_Surv_Nbr_Units().compute(new ComputeParameters(true, ldaCispartv.getCis_Part_View_Cis_Rea_Surv_Nbr_Units()),         //Natural: COMPUTE ROUNDED CIS-REA-SURV-NBR-UNITS = ( ( CIS-REA-ANNUAL-NBR-UNITS * 2 ) / 3 )
                    ((ldaCispartv.getCis_Part_View_Cis_Rea_Annual_Nbr_Units().multiply(2)).divide(3)));
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Survivor_Units_Payable().setValue(" 0.000");                                                                     //Natural: MOVE ' 0.000' TO #REA-SURVIVOR-UNITS-PAYABLE
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rea_Surv_Nbr_Units().greater(getZero())))                                                                          //Natural: IF CIS-REA-SURV-NBR-UNITS GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Rea_Survivor_Units_Payable().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Rea_Surv_Nbr_Units(),new            //Natural: MOVE EDITED CIS-REA-SURV-NBR-UNITS ( EM = ZZZZZZZZ.999 ) TO #REA-SURVIVOR-UNITS-PAYABLE
                ReportEditMask("ZZZZZZZZ.999"));
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Trad_Annu_Dollar().setValue("$");                                                                                //Natural: MOVE '$' TO #MDO-TRAD-ANNU-$
        if (condition(ldaCispartv.getCis_Part_View_Cis_Mdo_Traditional_Amt().greater(getZero())))                                                                         //Natural: IF CIS-MDO-TRADITIONAL-AMT GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Traditional_Annu_Amt().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Mdo_Traditional_Amt(),new             //Natural: MOVE EDITED CIS-MDO-TRADITIONAL-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #MDO-TRADITIONAL-ANNU-AMT
                ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Traditional_Annu_Amt().setValue("          0.00");                                                           //Natural: MOVE '          0.00' TO #MDO-TRADITIONAL-ANNU-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Tiaa_Initial_Dollar().setValue("$");                                                                             //Natural: MOVE '$' TO #MDO-TIAA-INITIAL-$
        if (condition(ldaCispartv.getCis_Part_View_Cis_Mdo_Tiaa_Int_Pymnt_Amt().greater(getZero())))                                                                      //Natural: IF CIS-MDO-TIAA-INT-PYMNT-AMT GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Initial_Paymeny_Amt().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Mdo_Tiaa_Int_Pymnt_Amt(),new           //Natural: MOVE EDITED CIS-MDO-TIAA-INT-PYMNT-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #MDO-INITIAL-PAYMENY-AMT
                ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Initial_Paymeny_Amt().setValue("          0.00");                                                            //Natural: MOVE '          0.00' TO #MDO-INITIAL-PAYMENY-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Cref_Initial_Dollar().setValue("$");                                                                             //Natural: MOVE '$' TO #MDO-CREF-INITIAL-$
        if (condition(ldaCispartv.getCis_Part_View_Cis_Mdo_Cref_Int_Pymnt_Amt().greater(getZero())))                                                                      //Natural: IF CIS-MDO-CREF-INT-PYMNT-AMT GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Cref_Init_Paymeny_Amt().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Mdo_Cref_Int_Pymnt_Amt(),new         //Natural: MOVE EDITED CIS-MDO-CREF-INT-PYMNT-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #MDO-CREF-INIT-PAYMENY-AMT
                ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Cref_Init_Paymeny_Amt().setValue("          0.00");                                                          //Natural: MOVE '          0.00' TO #MDO-CREF-INIT-PAYMENY-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tot_Tiaa_Cref_Mdo_Amt_N.compute(new ComputeParameters(false, pnd_Tot_Tiaa_Cref_Mdo_Amt_N), ldaCispartv.getCis_Part_View_Cis_Mdo_Tiaa_Int_Pymnt_Amt().add(ldaCispartv.getCis_Part_View_Cis_Mdo_Cref_Int_Pymnt_Amt())); //Natural: ADD CIS-MDO-TIAA-INT-PYMNT-AMT CIS-MDO-CREF-INT-PYMNT-AMT GIVING #TOT-TIAA-CREF-MDO-AMT-N
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Req_Pymt_Amt_Dollar().setValue("$");                                                                           //Natural: MOVE '$' TO #FIRST-REQ-PYMT-AMT-$
        if (condition(pnd_Tot_Tiaa_Cref_Mdo_Amt_N.greater(getZero())))                                                                                                    //Natural: IF #TOT-TIAA-CREF-MDO-AMT-N GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Req_Pymt_Amt().setValueEdited(pnd_Tot_Tiaa_Cref_Mdo_Amt_N,new ReportEditMask("ZZZ,ZZZ,ZZ9.99"));           //Natural: MOVE EDITED #TOT-TIAA-CREF-MDO-AMT-N ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #FIRST-REQ-PYMT-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Req_Pymt_Amt().setValue("          0.00");                                                                 //Natural: MOVE '          0.00' TO #FIRST-REQ-PYMT-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Tiaa_Exclude_Dollar().setValue("$");                                                                             //Natural: MOVE '$' TO #MDO-TIAA-EXCLUDE-$
        if (condition(ldaCispartv.getCis_Part_View_Cis_Mdo_Tiaa_Excluded_Amt().greater(getZero())))                                                                       //Natural: IF CIS-MDO-TIAA-EXCLUDED-AMT GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Tiaa_Exclude_Amt().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Mdo_Tiaa_Excluded_Amt(),new               //Natural: MOVE EDITED CIS-MDO-TIAA-EXCLUDED-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #MDO-TIAA-EXCLUDE-AMT
                ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Tiaa_Exclude_Amt().setValue("          0.00");                                                               //Natural: MOVE '          0.00' TO #MDO-TIAA-EXCLUDE-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Cref_Exclude_Dollar().setValue("$");                                                                             //Natural: MOVE '$' TO #MDO-CREF-EXCLUDE-$
        if (condition(ldaCispartv.getCis_Part_View_Cis_Mdo_Cref_Excluded_Amt().greater(getZero())))                                                                       //Natural: IF CIS-MDO-CREF-EXCLUDED-AMT GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Cref_Exclude_Amt().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Mdo_Cref_Excluded_Amt(),new               //Natural: MOVE EDITED CIS-MDO-CREF-EXCLUDED-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #MDO-CREF-EXCLUDE-AMT
                ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Mdo_Cref_Exclude_Amt().setValue("          0.00");                                                               //Natural: MOVE '          0.00' TO #MDO-CREF-EXCLUDE-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  060706
        if (condition(DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'")))                                                                           //Natural: IF CIS-RQST-ID = MASK ( 'MDO' )
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Traditional_G_I_R().setValue(0);                                                                                 //Natural: MOVE 0 TO #TRADITIONAL-G-I-R
            if (condition(ldaCispartv.getCis_Part_View_Cis_Grnted_Int_Rate().greater(getZero())))                                                                         //Natural: IF CIS-GRNTED-INT-RATE GT 0
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Traditional_G_I_R().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Grnted_Int_Rate(),new                    //Natural: MOVE EDITED CIS-GRNTED-INT-RATE ( EM = ZZ.99% ) TO #TRADITIONAL-G-I-R
                    ReportEditMask("ZZ.99%"));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Contigent_Charge().setValue(0);                                                                                      //Natural: MOVE 0 TO #CONTIGENT-CHARGE
        if (condition(ldaCispartv.getCis_Part_View_Cis_Contingencies_Chg().greater(getZero())))                                                                           //Natural: IF CIS-CONTINGENCIES-CHG GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Contigent_Charge().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Contingencies_Chg(),new ReportEditMask("ZZ.99%")); //Natural: MOVE EDITED CIS-CONTINGENCIES-CHG ( EM = ZZ.99% ) TO #CONTIGENT-CHARGE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Surrender_Charge().setValue(0);                                                                                      //Natural: MOVE 0 TO #SURRENDER-CHARGE
        if (condition(ldaCispartv.getCis_Part_View_Cis_Surrender_Chg().greater(getZero())))                                                                               //Natural: IF CIS-SURRENDER-CHG GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Surrender_Charge().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Surrender_Chg(),new ReportEditMask("Z.99%")); //Natural: MOVE EDITED CIS-SURRENDER-CHG ( EM = Z.99% ) TO #SURRENDER-CHARGE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Separate_Charge().setValue("0.00%");                                                                                 //Natural: MOVE '0.00%' TO #SEPARATE-CHARGE
        //* ****************** ORIG PARTICIPANT NAME *************************
        DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Frst_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                  //Natural: EXAMINE SUBSTRING ( CIS-ORGNL-PRTCPNT-FRST-NME,2,29 ) TRANSLATE INTO LOWER CASE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_O().setValue(ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Frst_Nme());                                  //Natural: MOVE CIS-ORGNL-PRTCPNT-FRST-NME TO #FIRST-NAME-O
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_O1().setValue("@");                                                                                           //Natural: MOVE '@' TO #FILLER-O1
        DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Mid_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                   //Natural: EXAMINE SUBSTRING ( CIS-ORGNL-PRTCPNT-MID-NME,2,29 ) TRANSLATE INTO LOWER CASE
        pnd_Frst_Annt_Mid_I.setValue(ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Mid_Nme());                                                                           //Natural: MOVE CIS-ORGNL-PRTCPNT-MID-NME TO #FRST-ANNT-MID-I
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Middle_Init_O().setValue(pnd_Frst_Annt_Mid_I_Pnd_Frst_Annt_Mid_Init);                                                //Natural: MOVE #FRST-ANNT-MID-INIT TO #MIDDLE-INIT-O
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_O2().setValue("@");                                                                                           //Natural: MOVE '@' TO #FILLER-O2
        DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Lst_Nme(),2,29), new ExamineTranslate(TranslateOption.Lower));                   //Natural: EXAMINE SUBSTRING ( CIS-ORGNL-PRTCPNT-LST-NME,2,29 ) TRANSLATE INTO LOWER CASE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_O().setValue(ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Lst_Nme());                                    //Natural: MOVE CIS-ORGNL-PRTCPNT-LST-NME TO #LAST-NAME-O
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_O3().setValue("@");                                                                                           //Natural: MOVE '@' TO #FILLER-O3
        DbsUtil.examine(new ExamineSource(ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Sffx(),2,7), new ExamineTranslate(TranslateOption.Lower));                       //Natural: EXAMINE SUBSTRING ( CIS-ORGNL-PRTCPNT-SFFX,2,7 ) TRANSLATE INTO LOWER CASE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix_O().setValue(ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Sffx());                                          //Natural: MOVE CIS-ORGNL-PRTCPNT-SFFX TO #SUFFIX-O
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Orig_Part_Name().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Name_O(),  //Natural: COMPRESS #FIRST-NAME-O #FILLER-O1 #MIDDLE-INIT-O #FILLER-O2 #LAST-NAME-O #FILLER-O3 #SUFFIX-O TO #ORIG-PART-NAME LEAVING NO SPACE
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_O1(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Middle_Init_O(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_O2(), 
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Last_Name_O(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Filler_O3(), ldaCislpart.getPnd_Participant_Work_File_Pnd_Suffix_O()));
        DbsUtil.examine(new ExamineSource(ldaCislpart.getPnd_Participant_Work_File_Pnd_Orig_Part_Name()), new ExamineSearch("@"), new ExamineReplace(" "));               //Natural: EXAMINE #ORIG-PART-NAME FOR '@' REPLACE WITH ' '
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Orig_Part_Ssn().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Ssn(),new ReportEditMask("999-99-9999")); //Natural: MOVE EDITED CIS-ORGNL-PRTCPNT-SSN ( EM = 999-99-9999 ) TO #ORIG-PART-SSN
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Orig_Part_Dob().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Birth_Dte(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED CIS-ORGNL-PRTCPNT-BIRTH-DTE ( EM = YYYYMMDD ) TO #ORIG-PART-DOB
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Orig_Part_Dod().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Orgnl_Prtcpnt_Death_Dte(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED CIS-ORGNL-PRTCPNT-DEATH-DTE ( EM = YYYYMMDD ) TO #ORIG-PART-DOD
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rtb_Pct().greater(getZero()) || ldaCispartv.getCis_Part_View_Cis_Rtb_Amt().greater(getZero())))                    //Natural: IF CIS-RTB-PCT GT 0 OR CIS-RTB-AMT GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Rbt_Requester().setValue("Y");                                                                                   //Natural: MOVE 'Y' TO #RBT-REQUESTER
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Rbt_Requester().setValue("N");                                                                                   //Natural: MOVE 'N' TO #RBT-REQUESTER
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Retir_Trans_Bene_Per().setValue(0);                                                                                  //Natural: MOVE 0 TO #RETIR-TRANS-BENE-PER
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rtb_Pct().greater(getZero())))                                                                                     //Natural: IF CIS-RTB-PCT GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Retir_Trans_Bene_Per().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Rtb_Pct(),new ReportEditMask("99%"));     //Natural: MOVE EDITED CIS-RTB-PCT ( EM = 99% ) TO #RETIR-TRANS-BENE-PER
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Retir_Trans_Bene_Dollar().setValue(" ");                                                                             //Natural: MOVE ' ' TO #RETIR-TRANS-BENE-$
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rtb_Amt().greater(getZero())))                                                                                     //Natural: IF CIS-RTB-AMT GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Retir_Trans_Bene_Dollar().setValue("$");                                                                         //Natural: MOVE '$' TO #RETIR-TRANS-BENE-$
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Retir_Trans_Bene_Amt().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Rtb_Amt(),new ReportEditMask("ZZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED CIS-RTB-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #RETIR-TRANS-BENE-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Retir_Trans_Bene_Amt().setValue("$ 0.00");                                                                       //Natural: MOVE '$ 0.00' TO #RETIR-TRANS-BENE-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Gsra_Ira_Surr_Chg().setValue("N");                                                                                   //Natural: MOVE 'N' TO #GSRA-IRA-SURR-CHG
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Da_Death_Surr_Right().setValue("N");                                                                                 //Natural: MOVE 'N' TO #DA-DEATH-SURR-RIGHT
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Cntrct_Num().setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr());                                             //Natural: MOVE CIS-TIAA-NBR TO #TIAA-CNTRCT-NUM
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cref_Cntrct_Num().setValue(ldaCispartv.getCis_Part_View_Cis_Cert_Nbr());                                             //Natural: MOVE CIS-CERT-NBR TO #CREF-CNTRCT-NUM
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Rqst_Id_Key().setValue(ldaCispartv.getCis_Part_View_Cis_Rqst_Id_Key());                                              //Natural: MOVE CIS-RQST-ID-KEY TO #RQST-ID-KEY
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Log_Dte_Tme().setValue(ldaCispartv.getCis_Part_View_Cis_Mit_Rqst_Log_Dte_Tme());                                 //Natural: MOVE CIS-MIT-RQST-LOG-DTE-TME TO #MIT-LOG-DTE-TME
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Log_Opr_Cde().setValue(ldaCispartv.getCis_Part_View_Cis_Mit_Rqst_Log_Oprtr_Cde());                               //Natural: MOVE CIS-MIT-RQST-LOG-OPRTR-CDE TO #MIT-LOG-OPR-CDE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Org_Unit_Cde().setValue(ldaCispartv.getCis_Part_View_Cis_Mit_Original_Unit_Cde());                               //Natural: MOVE CIS-MIT-ORIGINAL-UNIT-CDE TO #MIT-ORG-UNIT-CDE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Step_Id().setValue(ldaCispartv.getCis_Part_View_Cis_Mit_Step_Id());                                              //Natural: MOVE CIS-MIT-STEP-ID TO #MIT-STEP-ID
        ldaCispartv.getCis_Part_View_Cis_Extract_Date().setValue(Global.getDATX());                                                                                       //Natural: MOVE *DATX TO CIS-EXTRACT-DATE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Extract_Dte().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Extract_Date(),new ReportEditMask("YYYYMMDD"));        //Natural: MOVE EDITED CIS-EXTRACT-DATE ( EM = YYYYMMDD ) TO #EXTRACT-DTE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mailing_Inst().setValue(ldaCispartv.getCis_Part_View_Cis_Mail_Instructions());                                       //Natural: MOVE CIS-MAIL-INSTRUCTIONS TO #MAILING-INST
        pdaCisa4000.getCisa4000().reset();                                                                                                                                //Natural: RESET CISA4000
        //*  060706
        if (condition((ldaCispartv.getCis_Part_View_Cis_Pull_Code().equals(" ") && ((DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'")              //Natural: IF CIS-PULL-CODE = ' ' AND ( ( CIS-RQST-ID = MASK ( 'MDO' ) AND CIS-CNTRCT-APPRVL-IND = 'Y' ) OR ( CIS-RQST-ID = 'IA' AND CIS-TIAA-CNTRCT-TYPE NE 'S' AND CIS-CREF-CNTRCT-TYPE NE 'S' ) )
            && ldaCispartv.getCis_Part_View_Cis_Cntrct_Apprvl_Ind().equals("Y")) || ((ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Tiaa_Cntrct_Type().notEquals("S")) 
            && ldaCispartv.getCis_Part_View_Cis_Cref_Cntrct_Type().notEquals("S"))))))
        {
            pdaCisa4000.getCisa4000_Mit_Stts_Cde().getValue(1).setValue("I6");                                                                                            //Natural: ASSIGN CISA4000.MIT-STTS-CDE ( 1 ) := 'I6'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  060706
            if (condition(((ldaCispartv.getCis_Part_View_Cis_Cntrct_Apprvl_Ind().equals("N") && DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'"))  //Natural: IF ( CIS-CNTRCT-APPRVL-IND = 'N' AND CIS-RQST-ID = MASK ( 'MDO' ) ) OR ( CIS-RQST-ID = 'IA' AND ( CIS-TIAA-CNTRCT-TYPE EQ 'S' OR CIS-CREF-CNTRCT-TYPE EQ 'S' ) )
                || (ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && (ldaCispartv.getCis_Part_View_Cis_Tiaa_Cntrct_Type().equals("S") || ldaCispartv.getCis_Part_View_Cis_Cref_Cntrct_Type().equals("S"))))))
            {
                pdaCisa4000.getCisa4000_Mit_Stts_Cde().getValue(1).setValue("I8");                                                                                        //Natural: ASSIGN CISA4000.MIT-STTS-CDE ( 1 ) := 'I8'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaCispartv.getCis_Part_View_Cis_Pull_Code().equals("SPEC")))                                                                               //Natural: IF CIS-PULL-CODE = 'SPEC'
                {
                    pdaCisa4000.getCisa4000_Mit_Stts_Cde().getValue(1).setValue("I3");                                                                                    //Natural: ASSIGN CISA4000.MIT-STTS-CDE ( 1 ) := 'I3'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaCispartv.getCis_Part_View_Cis_Pull_Code().equals("OVER")))                                                                           //Natural: IF CIS-PULL-CODE = 'OVER'
                    {
                        pdaCisa4000.getCisa4000_Mit_Stts_Cde().getValue(1).setValue("I5");                                                                                //Natural: ASSIGN CISA4000.MIT-STTS-CDE ( 1 ) := 'I5'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("TPA") || ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IPRO")))                             //Natural: IF CIS-RQST-ID = 'TPA' OR = 'IPRO'
        {
            pdaCisa4000.getCisa4000_Mit_Stts_Cde().getValue(1).setValue("I6");                                                                                            //Natural: ASSIGN CISA4000.MIT-STTS-CDE ( 1 ) := 'I6'
            if (condition(ldaCispartv.getCis_Part_View_Cis_Pull_Code().equals("SPEC")))                                                                                   //Natural: IF CIS-PULL-CODE = 'SPEC'
            {
                pdaCisa4000.getCisa4000_Mit_Stts_Cde().getValue(1).setValue("I3");                                                                                        //Natural: ASSIGN CISA4000.MIT-STTS-CDE ( 1 ) := 'I3'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaCispartv.getCis_Part_View_Cis_Pull_Code().equals("OVER")))                                                                               //Natural: IF CIS-PULL-CODE = 'OVER'
                {
                    pdaCisa4000.getCisa4000_Mit_Stts_Cde().getValue(1).setValue("I5");                                                                                    //Natural: ASSIGN CISA4000.MIT-STTS-CDE ( 1 ) := 'I5'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  CNTRSTRG
            ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Tpa_Or_Ipro_Ind().setValue(" ");                                                                           //Natural: ASSIGN #FIRST-TPA-OR-IPRO-IND = ' '
            //*  CNTRSTRG
                                                                                                                                                                          //Natural: PERFORM ASSIGN-FIRST-TPAIPRO-IND
            sub_Assign_First_Tpaipro_Ind();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pdaCisa4000.getCisa4000_Cis_Mit_Requestor().setValue(ldaCispartv.getCis_Part_View_Cis_Rqst_Id());                                                                 //Natural: ASSIGN CISA4000.CIS-MIT-REQUESTOR := CIS-RQST-ID
        pdaCisa4000.getCisa4000_Mit_Requestor().setValue("CIS");                                                                                                          //Natural: ASSIGN CISA4000.MIT-REQUESTOR := 'CIS'
        pdaCisa4000.getCisa4000_Mit_Table_Max().setValue(5);                                                                                                              //Natural: ASSIGN CISA4000.MIT-TABLE-MAX := 5
        DbsUtil.callnat(Cisn4030.class , getCurrentProcessState(), pdaCisa4000.getCisa4000());                                                                            //Natural: CALLNAT 'CISN4030' CISA4000
        if (condition(Global.isEscape())) return;
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Status_Cde().setValue(pdaCisa4000.getCisa4000_Mit_Status().getValue(1));                                         //Natural: MOVE CISA4000.MIT-STATUS ( 1 ) TO #MIT-STATUS-CDE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Wpid().setValue(pdaCisa4000.getCisa4000_Mit_Wpid().getValue(1));                                                 //Natural: MOVE CISA4000.MIT-WPID ( 1 ) TO #MIT-WPID
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Org_Unit_Cde().setValue(pdaCisa4000.getCisa4000_Mit_Unit().getValue(1));                                         //Natural: MOVE CISA4000.MIT-UNIT ( 1 ) TO #MIT-ORG-UNIT-CDE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Mit_Step_Id().setValue(pdaCisa4000.getCisa4000_Mit_Stepid().getValue(1));                                            //Natural: MOVE CISA4000.MIT-STEPID ( 1 ) TO #MIT-STEP-ID
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Pullout_Code().setValue(ldaCispartv.getCis_Part_View_Cis_Pull_Code());                                               //Natural: MOVE CIS-PULL-CODE TO #PULLOUT-CODE
        //*  BD1
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Spec_Cntrct_Type().setValue(ldaCispartv.getCis_Part_View_Cis_Sg_Text_Udf_1().getSubstring(1,1));                     //Natural: MOVE SUBSTR ( CIS-SG-TEXT-UDF-1,1,1 ) TO #SPEC-CNTRCT-TYPE
        //*  BD1
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Orig_Issue_Date().setValue(ldaCispartv.getCis_Part_View_Cis_Sg_Text_Udf_1().getSubstring(2,9));                      //Natural: MOVE SUBSTR ( CIS-SG-TEXT-UDF-1,2,9 ) TO #ORIG-ISSUE-DATE
        //*  KG ROTH
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Roth_Ind().setValue(ldaCispartv.getCis_Part_View_Cis_Sg_Text_Udf_2().getSubstring(1,1));                             //Natural: MOVE SUBSTR ( CIS-SG-TEXT-UDF-2,1,1 ) TO #ROTH-IND
        //*  KG ROTH
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Roth_Payee_Ind().setValue(ldaCispartv.getCis_Part_View_Cis_Sg_Text_Udf_2().getSubstring(2,1));                       //Natural: MOVE SUBSTR ( CIS-SG-TEXT-UDF-2,2,1 ) TO #ROTH-PAYEE-IND
        //*  KG TACC START
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Ind().setValue(ldaCispartv.getCis_Part_View_Cis_Tacc_Ind().getValue(1));                                 //Natural: MOVE CIS-TACC-IND ( 1 ) TO #TIAA-ACCESS-IND
        if (condition(ldaCispartv.getCis_Part_View_Cis_Tacc_Annty_Amt().greater(getZero())))                                                                              //Natural: IF CIS-TACC-ANNTY-AMT GT 0
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Annty_Amt().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Tacc_Annty_Amt(),new                     //Natural: MOVE EDITED CIS-TACC-ANNTY-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #TIAA-ACCESS-ANNTY-AMT
                ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Annty_Amt().setValue("          0.00");                                                              //Natural: MOVE '          0.00' TO #TIAA-ACCESS-ANNTY-AMT
            //*  KG TACC END
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Lob_Code().setValue(ldaCispartv.getCis_Part_View_Cis_Lob());                                                         //Natural: MOVE CIS-LOB TO #LOB-CODE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Lob_Type().setValue(ldaCispartv.getCis_Part_View_Cis_Lob_Type());                                                    //Natural: MOVE CIS-LOB-TYPE TO #LOB-TYPE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Region_Code().setValue(ldaCispartv.getCis_Part_View_Cis_Region_Code());                                              //Natural: MOVE CIS-REGION-CODE TO #REGION-CODE
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Personal_Annuity().setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Personal_Annuity());                               //Natural: MOVE CIS-TIAA-PERSONAL-ANNUITY TO #PERSONAL-ANNUITY
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Req_Dis_Dollar().setValue("$");                                                                               //Natural: MOVE '$' TO #ANNUAL-REQ-DIS-$
        //*  060706
        if (condition(DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'")))                                                                           //Natural: IF CIS-RQST-ID = MASK ( 'MDO' )
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Annual_Required_Dist_Amt().greater(getZero())))                                                                //Natural: IF CIS-ANNUAL-REQUIRED-DIST-AMT GT 0
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Req_Dist().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Annual_Required_Dist_Amt(),new             //Natural: MOVE EDITED CIS-ANNUAL-REQUIRED-DIST-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #ANNUAL-REQ-DIST
                    ReportEditMask("ZZZ,ZZZ,ZZ9.99"));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Req_Dist().setValue("          0.00");                                                                //Natural: MOVE '          0.00' TO #ANNUAL-REQ-DIST
            }                                                                                                                                                             //Natural: END-IF
            //*  GGG 10/10
                                                                                                                                                                          //Natural: PERFORM PROCESS-MDO-TABLE
            sub_Process_Mdo_Table();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA")))                                                                                           //Natural: IF CIS-RQST-ID = 'IA'
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Cref_Annty_Amt().greater(getZero())))                                                                          //Natural: IF CIS-CREF-ANNTY-AMT GT 0
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Req_Dist().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Cref_Annty_Amt(),new ReportEditMask("ZZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED CIS-CREF-ANNTY-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) TO #ANNUAL-REQ-DIST
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Req_Dist().setValue("          0.00");                                                                //Natural: MOVE '          0.00' TO #ANNUAL-REQ-DIST
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((ldaCispartv.getCis_Part_View_Cis_Rqst_Id().notEquals("TPA") && ldaCispartv.getCis_Part_View_Cis_Rqst_Id().notEquals("IPRO"))))                     //Natural: IF ( CIS-RQST-ID NE 'TPA' AND CIS-RQST-ID NE 'IPRO' )
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Ownership_Cde().setValue(ldaCispartv.getCis_Part_View_Cis_Ownership_Cd());                                       //Natural: MOVE CIS-OWNERSHIP-CD TO #OWNERSHIP-CDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  WB - ACCRC START
        if (condition(DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'")))                                                                           //Natural: IF CIS-RQST-ID = MASK ( 'MDO' )
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Multi_Plan_No().getValue(2).notEquals(" ")))                                                                   //Natural: IF CIS-MULTI-PLAN-NO ( 2 ) NE ' '
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Multi_Plan_No().getValue("*").setValue(ldaCispartv.getCis_Part_View_Cis_Multi_Plan_No().getValue("*"));  //Natural: MOVE CIS-MULTI-PLAN-NO ( * ) TO #CIS-MULTI-PLAN-NO ( * )
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Multi_Sub_Plan().getValue("*").setValue(ldaCispartv.getCis_Part_View_Cis_Multi_Sub_Plan().getValue("*")); //Natural: MOVE CIS-MULTI-SUB-PLAN ( * ) TO #CIS-MULTI-SUB-PLAN ( * )
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Multi_Plan_Ind().setValue("Y");                                                                          //Natural: MOVE 'Y' TO #CIS-MULTI-PLAN-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Plan_Num().setValue(ldaCispartv.getCis_Part_View_Cis_Plan_Num());                                        //Natural: MOVE CIS-PLAN-NUM TO #CIS-PLAN-NUM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                 /* WB - ACCRC END
        //*  060706
        if (condition(! (DbsUtil.maskMatches(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Filler(),"'MDOM'") || DbsUtil.maskMatches(ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Filler(), //Natural: IF NOT #CIS-FILLER = MASK ( 'MDOM' ) OR = MASK ( 'MDOL' )
            "'MDOL'"))))
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Filler().setValue(" ");                                                                                      //Natural: MOVE ' ' TO #CIS-FILLER
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  CNTRSTRG  >>>
    private void sub_Assign_First_Tpaipro_Ind() throws Exception                                                                                                          //Natural: ASSIGN-FIRST-TPAIPRO-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        vw_acis_Reprint_Fl.startDatabaseFind                                                                                                                              //Natural: FIND ( 1 ) ACIS-REPRINT-FL WITH RP-TIAA-CONTR = CIS-DA-TIAA-NBR ( 1 )
        (
        "L_FIND_REP",
        new Wc[] { new Wc("RP_TIAA_CONTR", "=", ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Nbr().getValue(1), WcType.WITH) },
        1
        );
        L_FIND_REP:
        while (condition(vw_acis_Reprint_Fl.readNextRow("L_FIND_REP", true)))
        {
            vw_acis_Reprint_Fl.setIfNotFoundControlFlag(false);
            if (condition(vw_acis_Reprint_Fl.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "ACIS REPRINT RECORD NOT FOUND FOR CONTRACT:",ldaCispartv.getCis_Part_View_Cis_Da_Tiaa_Nbr().getValue(1));                          //Natural: WRITE 'ACIS REPRINT RECORD NOT FOUND FOR CONTRACT:' CIS-DA-TIAA-NBR ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("L_FIND_REP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("L_FIND_REP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_M.reset();                                                                                                                                                //Natural: RESET #M #N
            pnd_N.reset();
            if (condition(acis_Reprint_Fl_Count_Castrp_Related_Contract_Info.greater(getZero())))                                                                         //Natural: IF C*RP-RELATED-CONTRACT-INFO > 0
            {
                FOR17:                                                                                                                                                    //Natural: FOR #L = 1 TO C*RP-RELATED-CONTRACT-INFO
                for (pnd_L.setValue(1); condition(pnd_L.lessOrEqual(acis_Reprint_Fl_Count_Castrp_Related_Contract_Info)); pnd_L.nadd(1))
                {
                    if (condition(acis_Reprint_Fl_Rp_Related_Tiaa_No.getValue(pnd_L).equals(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr())))                                //Natural: IF RP-RELATED-TIAA-NO ( #L ) = CIS-TIAA-NBR
                    {
                        //*  TPA
                        short decideConditionsMet4308 = 0;                                                                                                                //Natural: DECIDE ON FIRST VALUE OF RP-RELATED-CONTRACT-TYPE ( #L );//Natural: VALUE 'T'
                        if (condition((acis_Reprint_Fl_Rp_Related_Contract_Type.getValue(pnd_L).equals("T"))))
                        {
                            decideConditionsMet4308++;
                            pnd_M.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M
                            if (condition(pnd_M.equals(1)))                                                                                                               //Natural: IF #M = 1
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Tpa_Or_Ipro_Ind().setValue("Y");                                                       //Natural: ASSIGN #FIRST-TPA-OR-IPRO-IND := 'Y'
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Tpa_Or_Ipro_Ind().setValue("N");                                                       //Natural: ASSIGN #FIRST-TPA-OR-IPRO-IND := 'N'
                                //*  IPRO
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: VALUE 'I'
                        else if (condition((acis_Reprint_Fl_Rp_Related_Contract_Type.getValue(pnd_L).equals("I"))))
                        {
                            decideConditionsMet4308++;
                            pnd_N.nadd(1);                                                                                                                                //Natural: ADD 1 TO #N
                            if (condition(pnd_N.equals(1)))                                                                                                               //Natural: IF #N = 1
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Tpa_Or_Ipro_Ind().setValue("Y");                                                       //Natural: ASSIGN #FIRST-TPA-OR-IPRO-IND := 'Y'
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaCislpart.getPnd_Participant_Work_File_Pnd_First_Tpa_Or_Ipro_Ind().setValue("N");                                                       //Natural: ASSIGN #FIRST-TPA-OR-IPRO-IND := 'N'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  TPA
                        short decideConditionsMet4330 = 0;                                                                                                                //Natural: DECIDE ON FIRST VALUE OF RP-RELATED-CONTRACT-TYPE ( #L );//Natural: VALUE 'T'
                        if (condition((acis_Reprint_Fl_Rp_Related_Contract_Type.getValue(pnd_L).equals("T"))))
                        {
                            decideConditionsMet4330++;
                            //*  IPRO
                            pnd_M.nadd(1);                                                                                                                                //Natural: ADD 1 TO #M
                        }                                                                                                                                                 //Natural: VALUE 'I'
                        else if (condition((acis_Reprint_Fl_Rp_Related_Contract_Type.getValue(pnd_L).equals("I"))))
                        {
                            decideConditionsMet4330++;
                            pnd_N.nadd(1);                                                                                                                                //Natural: ADD 1 TO #N
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("L_FIND_REP"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("L_FIND_REP"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  END OF CNTRSTRG   <<<
    }
    //*  IPFS
    private void sub_Get_Current_Business_Day() throws Exception                                                                                                          //Natural: GET-CURRENT-BUSINESS-DAY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Input_Date.setValue(Global.getDATN());                                                                                                                        //Natural: ASSIGN #INPUT-DATE := *DATN
        //*  IPFS
        DbsUtil.callnat(Scin8888.class , getCurrentProcessState(), pnd_Input_Date);                                                                                       //Natural: CALLNAT 'SCIN8888' #INPUT-DATE
        if (condition(Global.isEscape())) return;
        //*  IPFS
        if (condition(pnd_Input_Date.equals(getZero())))                                                                                                                  //Natural: IF #INPUT-DATE = 0
        {
            //*  IPFS
            getReports().write(2, "*",new RepeatItem(73),NEWLINE);                                                                                                        //Natural: WRITE ( 2 ) '*' ( 73 ) /
            if (Global.isEscape()) return;
            getReports().write(2, "******* THERE IS NO BUSINESS DATE ON THE FILE *******",NEWLINE);                                                                       //Natural: WRITE ( 2 ) '******* THERE IS NO BUSINESS DATE ON THE FILE *******' /
            if (Global.isEscape()) return;
            //*  IPFS
            getReports().write(2, "*",new RepeatItem(73));                                                                                                                //Natural: WRITE ( 2 ) '*' ( 73 )
            if (Global.isEscape()) return;
            //*  IPFS
            DbsUtil.terminate(12);  if (true) return;                                                                                                                     //Natural: TERMINATE 12
            //*  IPFS
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ (1) CNTRL BY CNTRL-SUPER-DE-1 = 'C000000'                 /* IPFS
        //*   IF CNTRL-TODAYS-BUSINESS-DT =  0                              /* IPFS
        //*     WRITE (2) '*' (73) /                                        /* IPFS
        //*     WRITE (2) '******* THERE IS NO BUSINESS DATE ON THE FILE *******' /
        //*     WRITE (2) '*' (73)                                          /* IPFS
        //*     TERMINATE 12                                                /* IPFS
        //*  END-IF                                                         /* IPFS
        //*  TEMP TO ADJUST BUSINESS DATE
        //*  MOVE 101029  TO CNTRL-TODAYS-BUSINESS-DT
        //*  UPDATE (4388)
        //*  MOVE CNTRL-TODAYS-BUSINESS-DT TO  #INPUT-DATE                  /* IPFS
        //*  IPFS
        pnd_Business_Date.setValue(pnd_Input_Date);                                                                                                                       //Natural: MOVE #INPUT-DATE TO #BUSINESS-DATE
        //*  IF #I-YY LT 97                                                 /* IPFS
        //*    MOVE 20 TO #CC                                               /* IPFS
        //*  ELSE                                                           /* IPFS
        //*    MOVE 19 TO #CC                                               /* IPFS
        //*  END-IF                                                         /* IPFS
        pnd_Business_Date_Pnd_Yy.setValue(pnd_Input_Date_Pnd_I_Yy);                                                                                                       //Natural: MOVE #I-YY TO #YY
        pnd_Business_Date_Pnd_Mm.setValue(pnd_Input_Date_Pnd_I_Mm);                                                                                                       //Natural: MOVE #I-MM TO #MM
        pnd_Business_Date_Pnd_Dd.setValue(pnd_Input_Date_Pnd_I_Dd);                                                                                                       //Natural: MOVE #I-DD TO #DD
        //*  END-READ                                                       /* IPFS
    }
    //*  HK >>>
    private void sub_Update_Beneficiary() throws Exception                                                                                                                //Natural: UPDATE-BENEFICIARY
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pdaCisa2081.getCisa2081().reset();                                                                                                                                //Natural: RESET CISA2081
        pdaCisa2081.getCisa2081_Cis_Bene_Rqst_Id_Key().setValue(ldaCispartv.getCis_Part_View_Cis_Rqst_Id_Key());                                                          //Natural: ASSIGN CIS-BENE-RQST-ID-KEY := CIS-PART-VIEW.CIS-RQST-ID-KEY
        pdaCisa2081.getCisa2081_Cis_Bene_Tiaa_Nbr().setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr());                                                                //Natural: ASSIGN CIS-BENE-TIAA-NBR := CIS-PART-VIEW.CIS-TIAA-NBR
        if (condition(ldaCispartv.getCis_Part_View_Cis_Cntrct_Type().equals("T")))                                                                                        //Natural: IF CIS-CNTRCT-TYPE EQ 'T'
        {
            pdaCisa2081.getCisa2081_Cis_Bene_Sync_Upd_Date().setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Doi());                                                       //Natural: ASSIGN CISA2081.CIS-BENE-SYNC-UPD-DATE := CIS-PART-VIEW.CIS-TIAA-DOI
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Cntrct_Type().equals("C")))                                                                                        //Natural: IF CIS-CNTRCT-TYPE EQ 'C'
        {
            pdaCisa2081.getCisa2081_Cis_Bene_Sync_Upd_Date().setValue(ldaCispartv.getCis_Part_View_Cis_Cref_Doi());                                                       //Natural: ASSIGN CISA2081.CIS-BENE-SYNC-UPD-DATE := CIS-PART-VIEW.CIS-CREF-DOI
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCispartv.getCis_Part_View_Cis_Cntrct_Type().equals("B")))                                                                                        //Natural: IF CIS-CNTRCT-TYPE EQ 'B'
        {
            pdaCisa2081.getCisa2081_Cis_Bene_Sync_Upd_Date().setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Doi());                                                       //Natural: ASSIGN CISA2081.CIS-BENE-SYNC-UPD-DATE := CIS-PART-VIEW.CIS-TIAA-DOI
        }                                                                                                                                                                 //Natural: END-IF
        pdaCisa2081.getCisa2081_Cis_Bene_Sync_Ind().setValue("Y");                                                                                                        //Natural: ASSIGN CISA2081.CIS-BENE-SYNC-IND := 'Y'
        //* *WRITE 'GOING UPDATE'
        DbsUtil.callnat(Cisn2081.class , getCurrentProcessState(), pdaCisa2081.getCisa2081());                                                                            //Natural: CALLNAT 'CISN2081' CISA2081
        if (condition(Global.isEscape())) return;
        if (condition(pdaCisa2081.getCisa2081_Cis_Isn_01().equals(getZero())))                                                                                            //Natural: IF CISA2081.CIS-ISN-01 EQ 0
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Grnted_Period_Yrs().equals(0) && ldaCispartv.getCis_Part_View_Cis_Grnted_Period_Dys().equals(0)))              //Natural: IF CIS-GRNTED-PERIOD-YRS = 00 AND CIS-GRNTED-PERIOD-DYS = 00
            {
                pnd_Error_Message.setValue("Guaranteed period is zero");                                                                                                  //Natural: ASSIGN #ERROR-MESSAGE := 'Guaranteed period is zero'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().equals("Y")))                                                                                     //Natural: IF CIS-TRNSF-FLAG EQ 'Y'
            {
                //*     AND CIS-GRNTED-PERIOD-YRS = 00  AND  CIS-GRNTED-PERIOD- = 00
                pnd_Error_Message.setValue(DbsUtil.compress("Same Beneficiaries as", ldaCispartv.getCis_Part_View_Cis_Da_Cert_Nbr().getValue(1)));                        //Natural: COMPRESS 'Same Beneficiaries as' CIS-DA-CERT-NBR ( 1 ) INTO #ERROR-MESSAGE
                                                                                                                                                                          //Natural: PERFORM LOAD-PDA
                sub_Load_Pda();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  BENE INTERFACE
                DbsUtil.callnat(Cisn601.class , getCurrentProcessState(), pdaCisa601.getCisa601());                                                                       //Natural: CALLNAT 'CISN601' CISA601
                if (condition(Global.isEscape())) return;
                if (condition(pdaCisa601.getCisa601_Pnd_Error_Code().equals("E")))                                                                                        //Natural: IF #ERROR-CODE EQ 'E'
                {
                    pnd_Error_Mesg.setValue("Error in BENN970 ");                                                                                                         //Natural: MOVE 'Error in BENN970 ' TO #ERROR-MESG
                                                                                                                                                                          //Natural: PERFORM WRITE-EXCEPTION-REPORT
                    sub_Write_Exception_Report();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-TRANS-REPORT
                sub_Write_Ia_Trans_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Total_Xfer.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TOTAL-XFER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-NOBENE-REPORT
                sub_Write_Nobene_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_No_Bene.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #NO-BENE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  HK <<<
    }
    private void sub_Load_Pda() throws Exception                                                                                                                          //Natural: LOAD-PDA
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pdaCisa601.getCisa601().reset();                                                                                                                                  //Natural: RESET CISA601
        pdaCisa601.getCisa601_Pnd_Todays_Date().setValue(pnd_Business_Date);                                                                                              //Natural: ASSIGN CISA601.#TODAYS-DATE := #BUSINESS-DATE
        pdaCisa601.getCisa601_Pnd_Tiaa_Cntrct().setValue(ldaCispartv.getCis_Part_View_Cis_Cert_Nbr());                                                                    //Natural: ASSIGN CISA601.#TIAA-CNTRCT := CIS-PART-VIEW.CIS-CERT-NBR
        pdaCisa601.getCisa601_Pnd_Eff_Date().setValue(pnd_Today);                                                                                                         //Natural: ASSIGN CISA601.#EFF-DATE := #TODAY
        pdaCisa601.getCisa601_Pnd_Bene_Name().setValue(DbsUtil.compress("Same as Contract ", ldaCispartv.getCis_Part_View_Cis_Da_Cert_Nbr().getValue(1)));                //Natural: COMPRESS 'Same as Contract ' CIS-DA-CERT-NBR ( 1 ) INTO CISA601.#BENE-NAME
    }
    private void sub_Write_Nobene_Report() throws Exception                                                                                                               //Natural: WRITE-NOBENE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pnd_Part_Name.reset();                                                                                                                                            //Natural: RESET #PART-NAME #RQST-SYSTEM #NO-BENE-CTRCT
        pnd_Rqst_System.reset();
        pnd_No_Bene_Ctrct.reset();
        pnd_No_Bene_Ctrct.setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr());                                                                                          //Natural: MOVE CIS-TIAA-NBR TO #NO-BENE-CTRCT
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().equals("N")))                              //Natural: IF CIS-PART-VIEW.CIS-RQST-ID EQ 'IA' AND CIS-TRNSF-FLAG EQ 'N'
        {
            pnd_Rqst_System.setValue("ADAM");                                                                                                                             //Natural: ASSIGN #RQST-SYSTEM := 'ADAM'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rqst_System.setValue(ldaCispartv.getCis_Part_View_Cis_Rqst_Id());                                                                                         //Natural: ASSIGN #RQST-SYSTEM := CIS-PART-VIEW.CIS-RQST-ID
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Part_Name.setValue(DbsUtil.compress(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Prfx(), " ", ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme(),            //Natural: COMPRESS CIS-PART-VIEW.CIS-FRST-ANNT-PRFX ' ' CIS-PART-VIEW.CIS-FRST-ANNT-FRST-NME ' ' CIS-PART-VIEW.CIS-FRST-ANNT-MID-NME ' ' CIS-PART-VIEW.CIS-FRST-ANNT-LST-NME ' ' CIS-PART-VIEW.CIS-FRST-ANNT-SFFX INTO #PART-NAME
            " ", ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme(), " ", ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme(), " ", ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx()));
        getReports().write(4, ReportOption.NOTITLE,ldaCispartv.getCis_Part_View_Cis_Pin_Nbr(),"/",pnd_No_Bene_Ctrct,new ColumnSpacing(4),pnd_Rqst_System,new              //Natural: WRITE ( 4 ) CIS-PIN-NBR '/' #NO-BENE-CTRCT 4X #RQST-SYSTEM 4X #PART-NAME 2X #ERROR-MESSAGE
            ColumnSpacing(4),pnd_Part_Name,new ColumnSpacing(2),pnd_Error_Message);
        if (Global.isEscape()) return;
    }
    private void sub_Write_Ia_Trans_Report() throws Exception                                                                                                             //Natural: WRITE-IA-TRANS-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pnd_Part_Name.reset();                                                                                                                                            //Natural: RESET #PART-NAME #RQST-SYSTEM #NO-BENE-CTRCT
        pnd_Rqst_System.reset();
        pnd_No_Bene_Ctrct.reset();
        //* * KG TRAN - MODIFIED IF STMT FOR TIAA TRANSFERS
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().equals("Y")))                              //Natural: IF CIS-PART-VIEW.CIS-RQST-ID EQ 'IA' AND CIS-TRNSF-FLAG EQ 'Y'
        {
            pnd_Rqst_System.setValue("IA XFER");                                                                                                                          //Natural: ASSIGN #RQST-SYSTEM := 'IA XFER'
            //*  TIAA TRAN
            if (condition(ldaCispartv.getCis_Part_View_Cis_Tiaa_Cntrct_Type().equals("C")))                                                                               //Natural: IF CIS-PART-VIEW.CIS-TIAA-CNTRCT-TYPE = 'C'
            {
                pnd_No_Bene_Ctrct.setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr());                                                                                  //Natural: MOVE CIS-TIAA-NBR TO #NO-BENE-CTRCT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_No_Bene_Ctrct.setValue(ldaCispartv.getCis_Part_View_Cis_Cert_Nbr());                                                                                  //Natural: MOVE CIS-CERT-NBR TO #NO-BENE-CTRCT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_No_Bene_Ctrct.setValue(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr());                                                                                      //Natural: MOVE CIS-TIAA-NBR TO #NO-BENE-CTRCT
            pnd_Rqst_System.setValue(ldaCispartv.getCis_Part_View_Cis_Rqst_Id());                                                                                         //Natural: ASSIGN #RQST-SYSTEM := CIS-PART-VIEW.CIS-RQST-ID
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Part_Name.setValue(DbsUtil.compress(ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Prfx(), " ", ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Frst_Nme(),            //Natural: COMPRESS CIS-PART-VIEW.CIS-FRST-ANNT-PRFX ' ' CIS-PART-VIEW.CIS-FRST-ANNT-FRST-NME ' ' CIS-PART-VIEW.CIS-FRST-ANNT-MID-NME ' ' CIS-PART-VIEW.CIS-FRST-ANNT-LST-NME ' ' CIS-PART-VIEW.CIS-FRST-ANNT-SFFX INTO #PART-NAME
            " ", ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Mid_Nme(), " ", ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Lst_Nme(), " ", ldaCispartv.getCis_Part_View_Cis_Frst_Annt_Sffx()));
        getReports().write(5, ReportOption.NOTITLE,ldaCispartv.getCis_Part_View_Cis_Pin_Nbr(),"/",pnd_No_Bene_Ctrct,new ColumnSpacing(4),pnd_Rqst_System,new              //Natural: WRITE ( 5 ) CIS-PIN-NBR '/' #NO-BENE-CTRCT 4X #RQST-SYSTEM 4X #PART-NAME 2X #ERROR-MESSAGE
            ColumnSpacing(4),pnd_Part_Name,new ColumnSpacing(2),pnd_Error_Message);
        if (Global.isEscape()) return;
    }
    private void sub_Write_Exception_Report() throws Exception                                                                                                            //Natural: WRITE-EXCEPTION-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA") && ldaCispartv.getCis_Part_View_Cis_Trnsf_Flag().equals("Y")))                              //Natural: IF CIS-PART-VIEW.CIS-RQST-ID EQ 'IA' AND CIS-TRNSF-FLAG EQ 'Y'
        {
            pnd_Rqst_System.setValue("IA XFER");                                                                                                                          //Natural: ASSIGN #RQST-SYSTEM := 'IA XFER'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rqst_System.setValue(ldaCispartv.getCis_Part_View_Cis_Rqst_Id());                                                                                         //Natural: ASSIGN #RQST-SYSTEM := CIS-PART-VIEW.CIS-RQST-ID
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(6, ReportOption.NOTITLE,ldaCispartv.getCis_Part_View_Cis_Pin_Nbr(),"/",pdaCisa601.getCisa601_Pnd_Tiaa_Cntrct(),new ColumnSpacing(4),pnd_Rqst_System,new  //Natural: WRITE ( 6 ) CIS-PIN-NBR '/' #TIAA-CNTRCT 4X #RQST-SYSTEM 2X #ERROR-MESG
            ColumnSpacing(2),pnd_Error_Mesg);
        if (Global.isEscape()) return;
    }
    private void sub_Get_Fund_Name() throws Exception                                                                                                                     //Natural: GET-FUND-NAME
    {
        if (BLNatReinput.isReinput()) return;

        FOR18:                                                                                                                                                            //Natural: FOR #X = 1 TO 20
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
        {
            //*  THIS CHECK IS NECESSARY
            if (condition(ldaCispartv.getCis_Part_View_Cis_Sg_Fund_Ticker().getValue(pnd_X).equals(" ")))                                                                 //Natural: IF CIS-SG-FUND-TICKER ( #X ) EQ ' '
            {
                //*  IN CASE THERE IS NO TICKER
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Num_Cref_Payout_Account_N().nadd(1);                                                                         //Natural: ADD 1 TO #NUM-CREF-PAYOUT-ACCOUNT-N
                pdaNeca4000.getNeca4000().reset();                                                                                                                        //Natural: RESET NECA4000
                pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                      //Natural: ASSIGN REQUEST-IND := ' '
                pdaNeca4000.getNeca4000_Function_Cde().setValue("FDS");                                                                                                   //Natural: ASSIGN FUNCTION-CDE := 'FDS'
                pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                             //Natural: ASSIGN INPT-KEY-OPTION-CDE := '01'
                pdaNeca4000.getNeca4000_Fds_Key_Ticker_Symbol().setValue(ldaCispartv.getCis_Part_View_Cis_Sg_Fund_Ticker().getValue(pnd_X));                              //Natural: ASSIGN FDS-KEY-TICKER-SYMBOL := CIS-SG-FUND-TICKER ( #X )
                //*  CREA    START
                if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA")))                                                                                   //Natural: IF CIS-RQST-ID = 'IA'
                {
                    pdaNeca4000.getNeca4000_Fds_Key_Fund_Name_Ref_Cde().setValue("18");                                                                                   //Natural: ASSIGN FDS-KEY-FUND-NAME-REF-CDE := '18'
                    //*  CREA    END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaNeca4000.getNeca4000_Fds_Key_Fund_Name_Ref_Cde().setValue("16");                                                                                   //Natural: ASSIGN FDS-KEY-FUND-NAME-REF-CDE := '16'
                    //*  CREA
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                    //Natural: CALLNAT 'NECN4000' NECA4000
                if (condition(Global.isEscape())) return;
                //*  20150511
                if (condition(pnd_Cref_Date_Of_Issue_Pnd_Cref_Date_Of_Issue_N.less(20150424)))                                                                            //Natural: IF #CREF-DATE-OF-ISSUE-N < 20150424
                {
                    DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1)), new ExamineSearch(" R1 "), new ExamineReplace("   "));         //Natural: EXAMINE NECA4000.FDS-FUND-NME ( 1 ) FOR ' R1 ' REPLACE WITH '   '
                    DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1)), new ExamineSearch(" R2 "), new ExamineReplace("   "));         //Natural: EXAMINE NECA4000.FDS-FUND-NME ( 1 ) FOR ' R2 ' REPLACE WITH '   '
                    DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1)), new ExamineSearch(" R3 "), new ExamineReplace("   "));         //Natural: EXAMINE NECA4000.FDS-FUND-NME ( 1 ) FOR ' R3 ' REPLACE WITH '   '
                    //*  20150511
                }                                                                                                                                                         //Natural: END-IF
                //*  060706
                if (condition(! (DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'")) && ldaCispartv.getCis_Part_View_Cis_Rqst_Id().notEquals("IA"))) //Natural: IF CIS-RQST-ID NE MASK ( 'MDO' ) AND CIS-RQST-ID NE 'IA'
                {
                    pnd_W.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #W
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_W).setValue(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1));                  //Natural: ASSIGN #ACCOUNT ( #W ) := NECA4000.FDS-FUND-NME ( 1 )
                }                                                                                                                                                         //Natural: END-IF
                //*  060706
                if (condition(DbsUtil.maskMatches(ldaCispartv.getCis_Part_View_Cis_Rqst_Id(),"'MDO'") || ldaCispartv.getCis_Part_View_Cis_Rqst_Id().equals("IA")))        //Natural: IF CIS-RQST-ID = MASK ( 'MDO' ) OR CIS-RQST-ID = 'IA'
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_X).setValue(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1));                  //Natural: ASSIGN #ACCOUNT ( #X ) := NECA4000.FDS-FUND-NME ( 1 )
                    getReports().write(0, "=",ldaCislpart.getPnd_Participant_Work_File_Pnd_Account().getValue(pnd_X),"DDDDDDDDDDDDDDDD");                                 //Natural: WRITE '=' #ACCOUNT ( #X ) 'DDDDDDDDDDDDDDDD'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCispartv.getCis_Part_View_Cis_Pymnt_Mode().equals("M")))                                                                                 //Natural: IF CIS-PYMNT-MODE EQ 'M'
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Units().getValue(pnd_X).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Sg_Fund_Amt().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-SG-FUND-AMT ( #X ) ( EM = ZZZZZZZ9.999 ) TO #ANNUAL-UNITS ( #X )
                        ReportEditMask("ZZZZZZZ9.999"));
                    getReports().write(0, "=",ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Units().getValue(pnd_X),"DDDDDSSSSSSS");                                //Natural: WRITE '=' #ANNUAL-UNITS ( #X ) 'DDDDDSSSSSSS'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCispartv.getCis_Part_View_Cis_Pymnt_Mode().equals("A") || ldaCispartv.getCis_Part_View_Cis_Pymnt_Mode().equals("Q") ||                   //Natural: IF CIS-PYMNT-MODE EQ 'A' OR CIS-PYMNT-MODE EQ 'Q' OR CIS-PYMNT-MODE EQ 'S'
                    ldaCispartv.getCis_Part_View_Cis_Pymnt_Mode().equals("S")))
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Units().getValue(pnd_X).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Sg_Fund_Amt().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-SG-FUND-AMT ( #X ) ( EM = ZZZZZZZ9.999 ) TO #ANNUAL-UNITS ( #X )
                        ReportEditMask("ZZZZZZZ9.999"));
                    getReports().write(0, "=",ldaCislpart.getPnd_Participant_Work_File_Pnd_Annual_Units().getValue(pnd_X));                                               //Natural: WRITE '=' #ANNUAL-UNITS ( #X )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  ADDED KG TACC
    private void sub_Get_Fund_Name_Ia() throws Exception                                                                                                                  //Natural: GET-FUND-NAME-IA
    {
        if (BLNatReinput.isReinput()) return;

        FOR19:                                                                                                                                                            //Natural: FOR #X = 1 TO 100
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(100)); pnd_X.nadd(1))
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Tacc_Account().getValue(pnd_X).equals(" ")))                                                                   //Natural: IF CIS-TACC-ACCOUNT ( #X ) EQ ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Num_Cref_Payout_Account_N().nadd(1);                                                                         //Natural: ADD 1 TO #NUM-CREF-PAYOUT-ACCOUNT-N
                //*  CREA
                pdaNeca4000.getNeca4000().reset();                                                                                                                        //Natural: RESET NECA4000
                pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                      //Natural: ASSIGN REQUEST-IND := ' '
                pdaNeca4000.getNeca4000_Function_Cde().setValue("FDS");                                                                                                   //Natural: ASSIGN FUNCTION-CDE := 'FDS'
                pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                             //Natural: ASSIGN INPT-KEY-OPTION-CDE := '01'
                pdaNeca4000.getNeca4000_Fds_Key_Ticker_Symbol().setValue(ldaCispartv.getCis_Part_View_Cis_Tacc_Account().getValue(pnd_X));                                //Natural: ASSIGN FDS-KEY-TICKER-SYMBOL := CIS-TACC-ACCOUNT ( #X )
                pdaNeca4000.getNeca4000_Fds_Key_Fund_Name_Ref_Cde().setValue("18");                                                                                       //Natural: ASSIGN FDS-KEY-FUND-NAME-REF-CDE := '18'
                DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                    //Natural: CALLNAT 'NECN4000' NECA4000
                if (condition(Global.isEscape())) return;
                //*  20150511
                if (condition(pnd_Cref_Date_Of_Issue_Pnd_Cref_Date_Of_Issue_N.less(20150424)))                                                                            //Natural: IF #CREF-DATE-OF-ISSUE-N < 20150424
                {
                    DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1)), new ExamineSearch(" R1 "), new ExamineReplace("   "));         //Natural: EXAMINE NECA4000.FDS-FUND-NME ( 1 ) FOR ' R1 ' REPLACE WITH '   '
                    DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1)), new ExamineSearch(" R2 "), new ExamineReplace("   "));         //Natural: EXAMINE NECA4000.FDS-FUND-NME ( 1 ) FOR ' R2 ' REPLACE WITH '   '
                    DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1)), new ExamineSearch(" R3 "), new ExamineReplace("   "));         //Natural: EXAMINE NECA4000.FDS-FUND-NME ( 1 ) FOR ' R3 ' REPLACE WITH '   '
                    //*  20150511
                }                                                                                                                                                         //Natural: END-IF
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Account().setValue(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1));                          //Natural: ASSIGN #TIAA-ACCESS-ACCOUNT := NECA4000.FDS-FUND-NME ( 1 )
                getReports().write(0, "=",ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Account());                                                            //Natural: WRITE '=' #TIAA-ACCESS-ACCOUNT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Mthly_Unit().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Tacc_Mnthly_Nbr_Units().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-TACC-MNTHLY-NBR-UNITS ( #X ) ( EM = ZZZZZZZ9.999 ) TO #TIAA-ACCESS-MTHLY-UNIT
                ReportEditMask("ZZZZZZZ9.999"));
            getReports().write(0, "=",ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Mthly_Unit(),"MONTHLY IND");                                               //Natural: WRITE '=' #TIAA-ACCESS-MTHLY-UNIT 'MONTHLY IND'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Annual_Unit().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Tacc_Annual_Nbr_Units().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-TACC-ANNUAL-NBR-UNITS ( #X ) ( EM = ZZZZZZZ9.999 ) TO #TIAA-ACCESS-ANNUAL-UNIT
                ReportEditMask("ZZZZZZZ9.999"));
            getReports().write(0, "=",ldaCislpart.getPnd_Participant_Work_File_Pnd_Tiaa_Access_Annual_Unit(),"OTHER IND");                                                //Natural: WRITE '=' #TIAA-ACCESS-ANNUAL-UNIT 'OTHER IND'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  GET-FUND-NAME-IA
    }
    //*  GGG 10/10
    private void sub_Process_Mdo_Table() throws Exception                                                                                                                 //Natural: PROCESS-MDO-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Tbl().getValue("*").reset();                                                                              //Natural: RESET #CIS-INVEST-TBL ( * ) #CIS-INVEST-TABLE-3 ( * ) #CIS-INVEST-FUND-AMT ( *,* ) #CIS-INVEST-UNIT-PRICE ( *,* ) #CIS-INVEST-UNITS ( *,* ) #CIS-INVEST-FUND-NAME-FROM ( *,* )
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Table_3().getValue("*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Fund_Amt().getValue("*","*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Unit_Price().getValue("*","*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Units().getValue("*","*").reset();
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Fund_Name_From().getValue("*","*").reset();
        if (condition(ldaCispartv.getCis_Part_View_Cis_Invest_Process_Dt().notEquals(getZero())))                                                                         //Natural: IF CIS-INVEST-PROCESS-DT NE 0
        {
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_N.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Process_Dt());                                                 //Natural: MOVE CIS-INVEST-PROCESS-DT TO #WS-CIS-INVEST-PROCESS-DT-N
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_A);                   //Natural: MOVE EDITED #WS-CIS-INVEST-PROCESS-DT-A TO #WS-CIS-INVEST-PROCESS-DT-D ( EM = YYYYMMDD )
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Process_Dt().setValueEdited(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Process_Dt_D,new ReportEditMask("MMDDYYYY")); //Natural: MOVE EDITED #WS-CIS-INVEST-PROCESS-DT-D ( EM = MMDDYYYY ) TO #CIS-INVEST-PROCESS-DT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Process_Dt().setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Process_Dt());                          //Natural: MOVE CIS-INVEST-PROCESS-DT TO #CIS-INVEST-PROCESS-DT
        }                                                                                                                                                                 //Natural: END-IF
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Amt_Total().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Invest_Amt_Total(),new ReportEditMask("ZZ,ZZZ,ZZZ,ZZ9.99")); //Natural: MOVE EDITED CIS-INVEST-AMT-TOTAL ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) TO #CIS-INVEST-AMT-TOTAL
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Amt_Total().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "$", ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Amt_Total())); //Natural: COMPRESS '$' #CIS-INVEST-AMT-TOTAL INTO #CIS-INVEST-AMT-TOTAL LEAVING NO SPACE
        FOR20:                                                                                                                                                            //Natural: FOR #X = 1 TO 3
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(3)); pnd_X.nadd(1))
        {
            if (condition(ldaCispartv.getCis_Part_View_Cis_Invest_Tiaa_Num_1().getValue(pnd_X).equals(" ")))                                                              //Natural: IF CIS-INVEST-TIAA-NUM-1 ( #X ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Num().getValue(pnd_X).setValue(pnd_X);                                                                //Natural: ASSIGN #CIS-INVEST-NUM ( #X ) := #X
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Num_Tot().setValue(pnd_X);                                                                            //Natural: ASSIGN #CIS-INVEST-NUM-TOT := #X
            //*   WRITE(1) '=' #CIS-INVEST-NUM(#X)
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Orig_Amt().getValue(pnd_X).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Invest_Orig_Amt().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-INVEST-ORIG-AMT ( #X ) ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) TO #CIS-INVEST-ORIG-AMT ( #X )
                ReportEditMask("ZZ,ZZZ,ZZZ,ZZ9.99"));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Orig_Amt().getValue(pnd_X).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,                   //Natural: COMPRESS '$' #CIS-INVEST-ORIG-AMT ( #X ) INTO #CIS-INVEST-ORIG-AMT ( #X ) LEAVING NO SPACE
                "$", ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Orig_Amt().getValue(pnd_X)));
            //*   WRITE(1) '='     #CIS-INVEST-ORIG-AMT(#X)
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Tiaa_Num_1().getValue(pnd_X).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Invest_Tiaa_Num_1().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-INVEST-TIAA-NUM-1 ( #X ) ( EM = XXXXXXX-X ) TO #CIS-INVEST-TIAA-NUM-1 ( #X )
                ReportEditMask("XXXXXXX-X"));
            //*   WRITE(1) '=' #CIS-INVEST-TIAA-NUM-1(#X)
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Cref_Num_1().getValue(pnd_X).setValueEdited(ldaCispartv.getCis_Part_View_Cis_Invest_Cref_Num_1().getValue(pnd_X),new  //Natural: MOVE EDITED CIS-INVEST-CREF-NUM-1 ( #X ) ( EM = XXXXXXX-X ) TO #CIS-INVEST-CREF-NUM-1 ( #X )
                ReportEditMask("XXXXXXX-X"));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Plan_Name().getValue(pnd_X).setValue(ldaCispartv.getCis_Part_View_Cis_Plan_Name().getValue(pnd_X));          //Natural: ASSIGN #CIS-PLAN-NAME ( #X ) := CIS-PLAN-NAME ( #X )
            FOR21:                                                                                                                                                        //Natural: FOR #Y = 1 TO 25
            for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(25)); pnd_Y.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM GET-CORRECT-MDO-TABLE
                sub_Get_Correct_Mdo_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                if (condition(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.equals(" ")))                                                                                       //Natural: IF #WS-CIS-INVEST-TICKER = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Fund_Amt().getValue(pnd_X,pnd_Y).setValueEdited(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt,new          //Natural: MOVE EDITED #WS-CIS-INVEST-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) TO #CIS-INVEST-FUND-AMT ( #X,#Y )
                    ReportEditMask("ZZ,ZZZ,ZZZ,ZZ9.99"));
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Fund_Amt().getValue(pnd_X,pnd_Y).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,         //Natural: COMPRESS '-$' #CIS-INVEST-FUND-AMT ( #X,#Y ) INTO #CIS-INVEST-FUND-AMT ( #X,#Y ) LEAVING NO SPACE
                    "-$", ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Fund_Amt().getValue(pnd_X,pnd_Y)));
                //*      WRITE(1) '=' #CIS-INVEST-FUND-AMT(#X,#Y) #X #Y
                if (condition(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.equals("TIAA#") || pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.equals("TSVX#") || pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.equals("TSAF#"))) //Natural: IF #WS-CIS-INVEST-TICKER = 'TIAA#' OR = 'TSVX#' OR = 'TSAF#'
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Unit_Price().getValue(pnd_X,pnd_Y).setValue("N/A");                                           //Natural: MOVE 'N/A' TO #CIS-INVEST-UNIT-PRICE ( #X,#Y ) #CIS-INVEST-UNITS ( #X,#Y )
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Units().getValue(pnd_X,pnd_Y).setValue("N/A");
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Unit_Price().getValue(pnd_X,pnd_Y).setValueEdited(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Unit_Price,new  //Natural: MOVE EDITED #WS-CIS-INVEST-UNIT-PRICE ( EM = ZZZ,ZZZ,ZZZ.9999 ) TO #CIS-INVEST-UNIT-PRICE ( #X,#Y )
                        ReportEditMask("ZZZ,ZZZ,ZZZ.9999"));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Unit_Price().getValue(pnd_X,pnd_Y).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,   //Natural: COMPRESS '$' #CIS-INVEST-UNIT-PRICE ( #X,#Y ) INTO #CIS-INVEST-UNIT-PRICE ( #X,#Y ) LEAVING NO SPACE
                        "$", ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Unit_Price().getValue(pnd_X,pnd_Y)));
                    //*                                                 /* GGG-12/10
                    //*         MOVE EDITED #WS-CIS-INVEST-UNITS (EM=Z,ZZZ,ZZZ,ZZZ,ZZ9.9999) TO
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Units().getValue(pnd_X,pnd_Y).setValueEdited(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Units,new       //Natural: MOVE EDITED #WS-CIS-INVEST-UNITS ( EM = Z,ZZZ,ZZZ.9999 ) TO #CIS-INVEST-UNITS ( #X,#Y )
                        ReportEditMask("Z,ZZZ,ZZZ.9999"));
                    ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Units().getValue(pnd_X,pnd_Y).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,        //Natural: COMPRESS '-' #CIS-INVEST-UNITS ( #X,#Y ) INTO #CIS-INVEST-UNITS ( #X,#Y ) LEAVING NO SPACE
                        "-", ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Units().getValue(pnd_X,pnd_Y)));
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-FUND-NAME-MDO
                sub_Get_Fund_Name_Mdo();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Fund_Name_From().getValue(pnd_X,pnd_Y).setValue(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Fund_Name);      //Natural: ASSIGN #CIS-INVEST-FUND-NAME-FROM ( #X,#Y ) := #WS-CIS-INVEST-FUND-NAME
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  ITE(1) '=' #CIS-INVEST-NUM-TOT
        //*  ITE(1) '=' CIS-INVEST-TICKER-TO(1)
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Tiaa_Num_To().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr(),new ReportEditMask("XXXXXXX-X")); //Natural: MOVE EDITED CIS-TIAA-NBR ( EM = XXXXXXX-X ) TO #CIS-INVEST-TIAA-NUM-TO
        ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Cref_Num_To().setValueEdited(ldaCispartv.getCis_Part_View_Cis_Cert_Nbr(),new ReportEditMask("XXXXXXX-X")); //Natural: MOVE EDITED CIS-CERT-NBR ( EM = XXXXXXX-X ) TO #CIS-INVEST-CREF-NUM-TO
        FOR22:                                                                                                                                                            //Natural: FOR #Y = 1 TO 25
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(25)); pnd_Y.nadd(1))
        {
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Ticker_To().getValue(pnd_Y));                                        //Natural: ASSIGN #WS-CIS-INVEST-TICKER := CIS-INVEST-TICKER-TO ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Amt_To().getValue(pnd_Y));                                              //Natural: ASSIGN #WS-CIS-INVEST-AMT := CIS-INVEST-AMT-TO ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Unit_Price.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Unit_Price_To().getValue(pnd_Y));                                //Natural: ASSIGN #WS-CIS-INVEST-UNIT-PRICE := CIS-INVEST-UNIT-PRICE-TO ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Units.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Units_To().getValue(pnd_Y));                                          //Natural: ASSIGN #WS-CIS-INVEST-UNITS := CIS-INVEST-UNITS-TO ( #Y )
            if (condition(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.equals(" ")))                                                                                           //Natural: IF #WS-CIS-INVEST-TICKER = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Fund_Amt_To().getValue(pnd_Y).setValueEdited(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt,new                 //Natural: MOVE EDITED #WS-CIS-INVEST-AMT ( EM = ZZ,ZZZ,ZZZ,ZZ9.99 ) TO #CIS-INVEST-FUND-AMT-TO ( #Y )
                ReportEditMask("ZZ,ZZZ,ZZZ,ZZ9.99"));
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Fund_Amt_To().getValue(pnd_Y).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,                //Natural: COMPRESS '$' #CIS-INVEST-FUND-AMT-TO ( #Y ) INTO #CIS-INVEST-FUND-AMT-TO ( #Y ) LEAVING NO SPACE
                "$", ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Fund_Amt_To().getValue(pnd_Y)));
            //*    WRITE(1) '='       #CIS-INVEST-FUND-AMT-TO(#Y) #Y
            if (condition(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.equals("TIAA#") || pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.equals("TSVX#") || pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.equals("TSAF#"))) //Natural: IF #WS-CIS-INVEST-TICKER = 'TIAA#' OR = 'TSVX#' OR = 'TSAF#'
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Unit_Price_To().getValue(pnd_Y).setValue("N/A");                                                  //Natural: MOVE 'N/A' TO #CIS-INVEST-UNIT-PRICE-TO ( #Y ) #CIS-INVEST-UNITS-TO ( #Y )
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Units_To().getValue(pnd_Y).setValue("N/A");
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Unit_Price_To().getValue(pnd_Y).setValueEdited(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Unit_Price,new    //Natural: MOVE EDITED #WS-CIS-INVEST-UNIT-PRICE ( EM = ZZZ,ZZZ,ZZZ.9999 ) TO #CIS-INVEST-UNIT-PRICE-TO ( #Y )
                    ReportEditMask("ZZZ,ZZZ,ZZZ.9999"));
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Unit_Price_To().getValue(pnd_Y).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace,          //Natural: COMPRESS '$' #CIS-INVEST-UNIT-PRICE-TO ( #Y ) INTO #CIS-INVEST-UNIT-PRICE-TO ( #Y ) LEAVING NO SPACE
                    "$", ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Unit_Price_To().getValue(pnd_Y)));
                //*                                                 /* GGG-12/10
                //*       MOVE EDITED     #WS-CIS-INVEST-UNITS (EM=Z,ZZZ,ZZZ,ZZZ,ZZ9.9999)
                ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Units_To().getValue(pnd_Y).setValueEdited(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Units,new              //Natural: MOVE EDITED #WS-CIS-INVEST-UNITS ( EM = Z,ZZZ,ZZZ.9999 ) TO #CIS-INVEST-UNITS-TO ( #Y )
                    ReportEditMask("Z,ZZZ,ZZZ.9999"));
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-FUND-NAME-MDO
            sub_Get_Fund_Name_Mdo();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            ldaCislpart.getPnd_Participant_Work_File_Pnd_Cis_Invest_Fund_Name_To().getValue(pnd_Y).setValue(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Fund_Name);                  //Natural: ASSIGN #CIS-INVEST-FUND-NAME-TO ( #Y ) := #WS-CIS-INVEST-FUND-NAME
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Correct_Mdo_Table() throws Exception                                                                                                             //Natural: GET-CORRECT-MDO-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        if (condition(pnd_X.equals(1)))                                                                                                                                   //Natural: IF #X = 1
        {
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Ticker_1().getValue(pnd_Y));                                         //Natural: ASSIGN #WS-CIS-INVEST-TICKER := CIS-INVEST-TICKER-1 ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Amt_1().getValue(pnd_Y));                                               //Natural: ASSIGN #WS-CIS-INVEST-AMT := CIS-INVEST-AMT-1 ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Unit_Price.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Unit_Price_1().getValue(pnd_Y));                                 //Natural: ASSIGN #WS-CIS-INVEST-UNIT-PRICE := CIS-INVEST-UNIT-PRICE-1 ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Units.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Units_1().getValue(pnd_Y));                                           //Natural: ASSIGN #WS-CIS-INVEST-UNITS := CIS-INVEST-UNITS-1 ( #Y )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_X.equals(2)))                                                                                                                                   //Natural: IF #X = 2
        {
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Ticker_2().getValue(pnd_Y));                                         //Natural: ASSIGN #WS-CIS-INVEST-TICKER := CIS-INVEST-TICKER-2 ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Amt_2().getValue(pnd_Y));                                               //Natural: ASSIGN #WS-CIS-INVEST-AMT := CIS-INVEST-AMT-2 ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Unit_Price.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Unit_Price_2().getValue(pnd_Y));                                 //Natural: ASSIGN #WS-CIS-INVEST-UNIT-PRICE := CIS-INVEST-UNIT-PRICE-2 ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Units.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Units_2().getValue(pnd_Y));                                           //Natural: ASSIGN #WS-CIS-INVEST-UNITS := CIS-INVEST-UNITS-2 ( #Y )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_X.equals(3)))                                                                                                                                   //Natural: IF #X = 3
        {
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Ticker_3().getValue(pnd_Y));                                         //Natural: ASSIGN #WS-CIS-INVEST-TICKER := CIS-INVEST-TICKER-3 ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Amt.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Amt_3().getValue(pnd_Y));                                               //Natural: ASSIGN #WS-CIS-INVEST-AMT := CIS-INVEST-AMT-3 ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Unit_Price.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Unit_Price_3().getValue(pnd_Y));                                 //Natural: ASSIGN #WS-CIS-INVEST-UNIT-PRICE := CIS-INVEST-UNIT-PRICE-3 ( #Y )
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Units.setValue(ldaCispartv.getCis_Part_View_Cis_Invest_Units_3().getValue(pnd_Y));                                           //Natural: ASSIGN #WS-CIS-INVEST-UNITS := CIS-INVEST-UNITS-3 ( #Y )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Fund_Name_Mdo() throws Exception                                                                                                                 //Natural: GET-FUND-NAME-MDO
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------------------------------**
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                              //Natural: ASSIGN REQUEST-IND := ' '
        pdaNeca4000.getNeca4000_Function_Cde().setValue("FDS");                                                                                                           //Natural: ASSIGN FUNCTION-CDE := 'FDS'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                     //Natural: ASSIGN INPT-KEY-OPTION-CDE := '01'
        pdaNeca4000.getNeca4000_Fds_Key_Fund_Name_Ref_Cde().setValue("16");                                                                                               //Natural: ASSIGN FDS-KEY-FUND-NAME-REF-CDE := '16'
        pdaNeca4000.getNeca4000_Fds_Key_Ticker_Symbol().setValue(pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Ticker);                                                                //Natural: ASSIGN FDS-KEY-TICKER-SYMBOL := #WS-CIS-INVEST-TICKER
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        if (condition(pdaNeca4000.getNeca4000_Output_Cnt().greater(getZero())))                                                                                           //Natural: IF NECA4000.OUTPUT-CNT > 0
        {
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Fund_Name.setValue(pdaNeca4000.getNeca4000_Fds_Fund_Nme().getValue(1));                                                      //Natural: ASSIGN #WS-CIS-INVEST-FUND-NAME := NECA4000.FDS-FUND-NME ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Mdo_Fields_Pnd_Ws_Cis_Invest_Fund_Name.setValue(" ");                                                                                                     //Natural: ASSIGN #WS-CIS-INVEST-FUND-NAME := ' '
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(120),pnd_Today, new ReportEditMask                    //Natural: WRITE ( 4 ) NOTITLE 1T *PROGRAM 120T #TODAY ( EM = MM/DD/YYYY ) // 1T 'Page :' *PAGE-NUMBER ( 4 ) 52T 'CIS' 120T *TIME / 40T 'BENEFICIARY INTERFACE SYSTEM' / 40T 'No Beneficiary Detail Report' / 40T 'Business Date: ' #TODAY ( EM = MM/DD/YYYY ) /// 'Sent Business Date:' #TODAY ( EM = MM/DD/YYYY ) // 'Pin' '/' 'Contract' 32T 'SYSTEM' 45T 'NAME ASSOCIATED WITH CONTRACT' 82T 'ERROR MESSAGE' / 1T '------------------------' 30T '-----------' 1X '-' ( 35 ) 1X '-' ( 45 )
                        ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(1),"Page :",getReports().getPageNumberDbs(4),new TabSetting(52),"CIS",new TabSetting(120),Global.getTIME(),NEWLINE,new 
                        TabSetting(40),"BENEFICIARY INTERFACE SYSTEM",NEWLINE,new TabSetting(40),"No Beneficiary Detail Report",NEWLINE,new TabSetting(40),"Business Date: ",pnd_Today, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE,"Sent Business Date:",pnd_Today, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,"Pin","/","Contract",new 
                        TabSetting(32),"SYSTEM",new TabSetting(45),"NAME ASSOCIATED WITH CONTRACT",new TabSetting(82),"ERROR MESSAGE",NEWLINE,new TabSetting(1),"------------------------",new 
                        TabSetting(30),"-----------",new ColumnSpacing(1),"-",new RepeatItem(35),new ColumnSpacing(1),"-",new RepeatItem(45));
                    //*    27T 'SYSTEM'
                    //*    40T 'NAME ASSOCIATED WITH CONTRACT'
                    //*    77T 'ERROR MESSAGE' /
                    //*    1T '---------------------'
                    //*    25T '-----------'
                    //*  PINE
                    //*  PINE
                    //*  PINE
                    //*  PINE
                    //*  PINE
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(120),pnd_Today, new ReportEditMask                    //Natural: WRITE ( 5 ) NOTITLE 1T *PROGRAM 120T #TODAY ( EM = MM/DD/YYYY ) // 1T 'Page :' *PAGE-NUMBER ( 5 ) 52T 'CIS' 120T *TIME / 40T 'BENEFICIARY INTERFACE SYSTEM' / 40T 'IA Transfers Detail Report' / 40T 'Business Date: ' #TODAY ( EM = MM/DD/YYYY ) /// 'Sent Business Date:' #TODAY ( EM = MM/DD/YYYY ) // 'Pin' '/' 'Contract' 32T 'SYSTEM' 45T 'NAME ASSOCIATED WITH CONTRACT' 82T 'ERROR MESSAGE' / 1T '------------------------' 30T '-----------' 1X '-' ( 35 ) 1X '-' ( 45 )
                        ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(1),"Page :",getReports().getPageNumberDbs(5),new TabSetting(52),"CIS",new TabSetting(120),Global.getTIME(),NEWLINE,new 
                        TabSetting(40),"BENEFICIARY INTERFACE SYSTEM",NEWLINE,new TabSetting(40),"IA Transfers Detail Report",NEWLINE,new TabSetting(40),"Business Date: ",pnd_Today, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE,"Sent Business Date:",pnd_Today, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,"Pin","/","Contract",new 
                        TabSetting(32),"SYSTEM",new TabSetting(45),"NAME ASSOCIATED WITH CONTRACT",new TabSetting(82),"ERROR MESSAGE",NEWLINE,new TabSetting(1),"------------------------",new 
                        TabSetting(30),"-----------",new ColumnSpacing(1),"-",new RepeatItem(35),new ColumnSpacing(1),"-",new RepeatItem(45));
                    //*    27T 'SYSTEM'
                    //*    40T 'NAME ASSOCIATED WITH CONTRACT'
                    //*    77T 'ERROR MESSAGE' /
                    //*    1T '---------------------'
                    //*    25T '-----------'
                    //*  CS
                    //*  PINE
                    //*  PINE
                    //*  PINE
                    //*  PINE
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(6, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(120),pnd_Today, new ReportEditMask                    //Natural: WRITE ( 6 ) NOTITLE 1T *PROGRAM 120T #TODAY ( EM = MM/DD/YYYY ) // 1T 'Page :' *PAGE-NUMBER ( 6 ) 52T 'CIS' 120T *TIME / 40T 'BENEFICIARY INTERFACE SYSTEM' / 40T 'IA Transfers Exception Report' / 40T 'Business Date: ' #TODAY ( EM = MM/DD/YYYY ) /// 'Sent Business Date:' #TODAY ( EM = MM/DD/YYYY ) // 'Pin' '/' 'Contract' 30T 'SYSTEM' 5X 'ERROR MESSAGE' / 1T '------------------------' 30T '-----------' 1X '-' ( 60 )
                        ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(1),"Page :",getReports().getPageNumberDbs(6),new TabSetting(52),"CIS",new TabSetting(120),Global.getTIME(),NEWLINE,new 
                        TabSetting(40),"BENEFICIARY INTERFACE SYSTEM",NEWLINE,new TabSetting(40),"IA Transfers Exception Report",NEWLINE,new TabSetting(40),"Business Date: ",pnd_Today, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE,"Sent Business Date:",pnd_Today, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,"Pin","/","Contract",new 
                        TabSetting(30),"SYSTEM",new ColumnSpacing(5),"ERROR MESSAGE",NEWLINE,new TabSetting(1),"------------------------",new TabSetting(30),"-----------",new 
                        ColumnSpacing(1),"-",new RepeatItem(60));
                    //*    25T 'SYSTEM'
                    //*    5X 'ERROR MESSAGE' /
                    //*    1T '---------------------'
                    //*    25T '-----------'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"PROGRAM:",new TabSetting(25),Global.getPROGRAM(),NEWLINE,"ERROR:",new TabSetting(25),Global.getERROR_NR(),NEWLINE,"LINE:",new  //Natural: WRITE // 'PROGRAM:' 25T *PROGRAM / 'ERROR:' 25T *ERROR-NR / 'LINE:' 25T *ERROR-LINE // 'CONTRACT:' 25T CIS-PRT-VIEW.R-TIAA-CNTRCT / 'REQID:' 25T CIS-PRT-VIEW.R-REQ-ID-KEY / 'EXTRACT RECORDS WRITTEN:' #REPRT-COUNTER ( EM = ZZZ,ZZ9 )
            TabSetting(25),Global.getERROR_LINE(),NEWLINE,NEWLINE,"CONTRACT:",new TabSetting(25),ldaCisview1.getCis_Prt_View_R_Tiaa_Cntrct(),NEWLINE,"REQID:",new 
            TabSetting(25),ldaCisview1.getCis_Prt_View_R_Req_Id_Key(),NEWLINE,"EXTRACT RECORDS WRITTEN:",pnd_Reprt_Counter, new ReportEditMask ("ZZZ,ZZ9"));
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=62 LS=130");
        Global.format(2, "PS=62 LS=75 ZP=ON");
        Global.format(3, "PS=58 LS=130");
        Global.format(4, "PS=62 LS=130");
        Global.format(5, "PS=62 LS=130");
        Global.format(6, "PS=62 LS=130");

        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(29),"CIS RE-PRINT SYSTEM",new TabSetting(60),"PG:  ",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new 
            TabSetting(1),Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(60),"REPORT: RPT1",NEWLINE);
    }
}
