/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:21:17 PM
**        * FROM NATURAL PROGRAM : Cisb400
************************************************************
**        * FILE NAME            : Cisb400.java
**        * CLASS NAME           : Cisb400
**        * INSTANCE NAME        : Cisb400
************************************************************
************************************************************************
* PROGRAM : CISB400                                                    *
* SYSTEM  : CIS                                                        *
* AUTHOR  : HITESH KAKADIA                                             *
* TITLE   : SUB DRIVER FOR DATABASE 45                                 *
* PURPOSE : WRITES THE COR FEED FOR NEW ISSUE FOR IA AND MDO           *
************************************************************************
*
************************************************************************
* -------------------------------------------------------------------- *
*   DATE   DEVELOPER  CHANGE DESCRIPTION                               *
* -------- ---------- ------------------------------------------------ *
* 09/13/04 H. KAKADIA CHANGE PROGRAM TO CALL COR API FOR ALL THE TPA   *
*                     ISSUED BY OMNI                                   *
* 04/04/06 H.KAKADIA  OMNI CHANGES FOR ICAP TPA/IPRO                   *
* 09/06/06 O SOTTO    CHANGED AS PART OF MDO LEGAL SPLIT. SC 090606.   *
* 07/31/07 K. GATES - MOVED COR ERRORS TO MOBIUS REPORT-SEE ERRRPT KG  *
* 08/01/07 O SOTTO    NEW ANNUITY (75%) OPTION CHANGES. SC 080107.     *
* 02/29/08 DEVELBISS  GET NEW PIN FOR MDO SIP CONTRACTS - SCAN 022908. *
* 06/18/08 DEVELBISS  CHECK COR FOR EXISTING PIN FOR MDO SIP CONTRACTS *
* 07/09/08 HKAKADIA   MADE CHANGES TO RETRIEVE PIN FOR SIP CONTRACTS   *
*                     SCAN 061808                                      *
* 07/21/09 BERGHEISER MODIFIED MDO SIP PARTICIPANT PIN UPDATE LOGIC    *
*                     ADD MDO SIP BENEFICIARY PIN UPDATE LOGIC         *
*                     CORRECT SSN REDEFINE SIZE (JRB1)                 *
* 07/22/09 C. MASON   CHANGES FOR MDM CONVERSION                       *
* 02/05/10 BERGHEISER WRITE ADDITIONAL MOBIUS ERROR REPORTS (JRB2)     *
*                     CI1010DA - SIP OMNI ERROR REPORT                 *
*                     CI1010DB - MDO TPA IPRO OMNI ERROR REPORT        *
* 10/25/10 GUERRERO   RESTOW FOR UPDATE OF CISPARTV                    *
* 12/16/11 ELLO       RESIDENCE ADDRESS CAPTURE PROJECT                *
*                     REPLACE CALLNAT OF THE FOLLOWING MODULES:        *
*                       MDMN100 TO MDMN100A                            *
*                       MDMN300 TO MDMN300A                            *
*                     AS PER MDM TEAM, THE 'A' VERSION OF THE SUBPGMS  *
*                     ARE THE CORRECT ONES TO USE FOR BATCH.  THIS     *
*                     WILL MAKE THE JOB RUN MORE EFFICIENTLY BECAUSE   *
*                     IT WILL ONLY PERFORM 1 OPEN AND 1 CLOSE ON MQUEUE*
*                     SEE: BE1 RACP                                    *
* 10/2014 B. NEWSOM  - ACIS/CIS CREF REDESIGN COMMUNICATIONS    (ACCRC)*
*                      RE-STOW FOR CISPARTV                            *
* 09/01/15 B.ELLO    - ADDED THE BENE IN THE PLAN FIELDS IN THE WORKFILE
*                      LAYOUT AND RESTOWED FOR CISPARTV
*                      PROGRAM WILL CALL NEW MDM MODULE
*                      MDMN370A FOR BIP INSTEAD OF MDMN300A.  PIN WILL
*                      BE UPDATED TO CIS PARTICIPANT FILE FOR BIP.(BIP)
* 05/10/17  (MUKHR)   PIN EXPANSION CHANGES. (C420007)         PINE
* 04/18/2017  B. NEWSOM  PROGRAM EXTRACTS RECORDS FROM THE CIS
*                        PARTICIPANT FILE THAT NEED TO BE SYNCHED UP IN
*                        MDM.                                    (IARPF)
* 01/10/19 JENABI    - FIX THE CODE IN THE NATURAL PROGRAM CISB400 TO
*                      REFER TO #MDO-INFO.CIS-SG-TEXT-UDF-3 VARIABLE
*                      RATHER THAN REFERRING TO
*                      #TIAA-INFO.CIS-SG-TEXT-UDF-3 VARIABLE IN THE
*                      SUB-ROUTINE CALL-COR-API FOR MDO/BIP PROCESSING.
*                                                      (SEE - C499005)
* 02/01/19 JENABI    - FIX THE CODE TO ASSIGN OPTION CODE FOR TPI AND
*                      IPI CONTRACTS BEFORE THE MDM CALL IN THE SUB-
*                      ROUTINE GET-OPTION-CODE.        (SEE - C504365)
************************************************************************
*

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb400 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaCisl400 ldaCisl400;
    private LdaCispartv ldaCispartv;
    private LdaCisl5000 ldaCisl5000;
    private PdaMdma101 pdaMdma101;
    private PdaMdma301 pdaMdma301;
    private PdaMdma371 pdaMdma371;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_File1;
    private DbsField pnd_Work_File1_Cis_Pin_Nbr;
    private DbsField pnd_Work_File1_Cis_Rqst_Id_Key;
    private DbsField pnd_Work_File1_Cis_Tiaa_Nbr;
    private DbsField pnd_Work_File1_Cis_Cert_Nbr;
    private DbsField pnd_Work_File1_Cis_Tiaa_Doi;

    private DbsGroup pnd_Work_File1__R_Field_1;
    private DbsField pnd_Work_File1_Cis_Tiaa_Doi_A;
    private DbsField pnd_Work_File1_Cis_Cref_Doi;

    private DbsGroup pnd_Work_File1__R_Field_2;
    private DbsField pnd_Work_File1_Cis_Cref_Doi_A;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Ssn;

    private DbsGroup pnd_Work_File1__R_Field_3;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Ssn_A;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Prfx;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Frst_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Mid_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Lst_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Sffx;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Dob;

    private DbsGroup pnd_Work_File1__R_Field_4;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Dob_A;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Sex_Cde;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Calc_Method;
    private DbsField pnd_Work_File1_Cis_Opn_Clsd_Ind;
    private DbsField pnd_Work_File1_Cis_Status_Cd;
    private DbsField pnd_Work_File1_Cis_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Cntrct_Apprvl_Ind;
    private DbsField pnd_Work_File1_Cis_Rqst_Id;
    private DbsField pnd_Work_File1_Cis_Hold_Cde;
    private DbsField pnd_Work_File1_Cis_Cntrct_Print_Dte;

    private DbsGroup pnd_Work_File1__R_Field_5;
    private DbsField pnd_Work_File1_Cis_Cntrct_Print_Dte_A;
    private DbsField pnd_Work_File1_Cis_Extract_Date;

    private DbsGroup pnd_Work_File1__R_Field_6;
    private DbsField pnd_Work_File1_Cis_Extract_Date_A;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_Dte;

    private DbsGroup pnd_Work_File1__R_Field_7;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_Dte_A;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_User_Id;
    private DbsField pnd_Work_File1_Cis_Appl_Entry_User_Id;
    private DbsField pnd_Work_File1_Cis_Annty_Option;
    private DbsField pnd_Work_File1_Cis_Pymnt_Mode;
    private DbsField pnd_Work_File1_Cis_Annty_Start_Dte;

    private DbsGroup pnd_Work_File1__R_Field_8;
    private DbsField pnd_Work_File1_Cis_Annty_Start_Dte_A;
    private DbsField pnd_Work_File1_Cis_Annty_End_Dte;

    private DbsGroup pnd_Work_File1__R_Field_9;
    private DbsField pnd_Work_File1_Cis_Annty_End_Dte_A;
    private DbsField pnd_Work_File1_Cis_Grnted_Period_Yrs;
    private DbsField pnd_Work_File1_Cis_Grnted_Period_Dys;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Prfx;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Sffx;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Ssn;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Dob;

    private DbsGroup pnd_Work_File1__R_Field_10;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Dob_A;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde;

    private DbsGroup pnd_Work_File1_Cis_Cref_Annty_Pymnt;
    private DbsField pnd_Work_File1_Cis_Cref_Acct_Cde;
    private DbsField pnd_Work_File1_Cis_Orig_Issue_State;
    private DbsField pnd_Work_File1_Cis_Issue_State_Name;
    private DbsField pnd_Work_File1_Cis_Issue_State_Cd;
    private DbsField pnd_Work_File1_Cis_Ppg_Code;
    private DbsField pnd_Work_File1_Cis_Region_Code;
    private DbsField pnd_Work_File1_Cis_Ownership_Cd;
    private DbsField pnd_Work_File1_Cis_Bill_Code;
    private DbsField pnd_Work_File1_Cis_Lob;
    private DbsField pnd_Work_File1_Cis_Lob_Type;
    private DbsField pnd_Work_File1_Cis_Addr_Syn_Ind;
    private DbsField pnd_Work_File1_Cis_Addr_Process_Env;

    private DbsGroup pnd_Work_File1_Cis_Address_Info;
    private DbsField pnd_Work_File1_Cis_Address_Chg_Ind;
    private DbsField pnd_Work_File1_Cis_Address_Dest_Name;
    private DbsField pnd_Work_File1_Cis_Address_Txt;
    private DbsField pnd_Work_File1_Cis_Zip_Code;
    private DbsField pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Work_File1_Cis_Stndrd_Trn_Cd;
    private DbsField pnd_Work_File1_Cis_Finalist_Reason_Cd;
    private DbsField pnd_Work_File1_Cis_Addr_Usage_Code;
    private DbsField pnd_Work_File1_Cis_Checking_Saving_Cd;
    private DbsField pnd_Work_File1_Cis_Addr_Stndrd_Code;
    private DbsField pnd_Work_File1_Cis_Stndrd_Overide;
    private DbsField pnd_Work_File1_Cis_Postal_Data_Fields;
    private DbsField pnd_Work_File1_Cis_Geographic_Cd;
    private DbsField pnd_Work_File1_Cis_Corp_Sync_Ind;

    private DbsGroup pnd_Work_File1_Cis_Rqst_System_Mit_Dta;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Function;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env;
    private DbsField pnd_Work_File1_Cis_Mit_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Mit_Error_Msg;

    private DbsGroup pnd_Work_File1_Cis_Non_Premium_Dta;
    private DbsField pnd_Work_File1_Cis_Nonp_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Nonp_Process_Env;
    private DbsField pnd_Work_File1_Cis_Nonp_Effective_Dte;

    private DbsGroup pnd_Work_File1__R_Field_11;
    private DbsField pnd_Work_File1_Cis_Nonp_Effective_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Product_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte;

    private DbsGroup pnd_Work_File1__R_Field_12;
    private DbsField pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Currency;
    private DbsField pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt;
    private DbsField pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt;
    private DbsField pnd_Work_File1_Cis_Nonp_Deletion_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Vesting_Dte;

    private DbsGroup pnd_Work_File1__R_Field_13;
    private DbsField pnd_Work_File1_Cis_Nonp_Vesting_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Error_Msg;

    private DbsGroup pnd_Work_File1_Cis_Allocation_Dta;
    private DbsField pnd_Work_File1_Cis_Alloc_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Alloc_Process_Env;
    private DbsField pnd_Work_File1_Cis_Alloc_Effective_Dte;

    private DbsGroup pnd_Work_File1__R_Field_14;
    private DbsField pnd_Work_File1_Cis_Alloc_Effective_Dte_A;
    private DbsField pnd_Work_File1_Cis_Alloc_Percentage;
    private DbsField pnd_Work_File1_Cis_Alloc_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Alloc_Error_Msg;
    private DbsField pnd_Work_File1_Cis_Cor_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Process_Env;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Xref_Pin;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt;

    private DbsGroup pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Mail_Table_Cnt;

    private DbsGroup pnd_Work_File1_Cis_Cor_Ph_Mail_Grp;
    private DbsField pnd_Work_File1_Cis_Cor_Phmail_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin;
    private DbsField pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte;

    private DbsGroup pnd_Work_File1__R_Field_15;
    private DbsField pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Pull_Code;
    private DbsField pnd_Work_File1_Cis_Trnsf_Flag;
    private DbsField pnd_Work_File1_Cis_Trnsf_From_Company;
    private DbsField pnd_Work_File1_Cis_Trnsf_To_Company;
    private DbsField pnd_Work_File1_Cis_Tiaa_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Rea_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Cref_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Decedent_Contract;
    private DbsField pnd_Work_File1_Cis_Relation_To_Decedent;
    private DbsField pnd_Work_File1_Cis_Ssn_Tin_Ind;
    private DbsField pnd_Work_File1_Cis_Isn;
    private DbsField pnd_Work_File1_Cis_Mdo_Contract_Type;
    private DbsField pnd_Work_File1_Cis_Sg_Text_Udf_3;

    private DataAccessProgramView vw_bene_Update;
    private DbsField bene_Update_Cis_Bene_Unique_Id_Nbr;
    private DbsField bene_Update_Cis_Bene_Rqst_Id_Key;
    private DbsField pnd_Cis_Bene_Super_2;

    private DbsGroup pnd_Cis_Bene_Super_2__R_Field_16;
    private DbsField pnd_Cis_Bene_Super_2_Pnd_Cis_Rcrd_Type_Cde;
    private DbsField pnd_Cis_Bene_Super_2_Pnd_Cis_Bene_Tiaa_Nbr;

    private DbsGroup pnd_Tiaa_Info;
    private DbsField pnd_Tiaa_Info_Cis_Rqst_Id;
    private DbsField pnd_Tiaa_Info_Cis_Function_Cde;
    private DbsField pnd_Tiaa_Info_Cis_Pin_Nbr;
    private DbsField pnd_Tiaa_Info_Cis_Frst_Annt_Ssn;
    private DbsField pnd_Tiaa_Info_Cis_Frst_Annt_Prfx;
    private DbsField pnd_Tiaa_Info_Cis_Frst_Annt_Frst_Nme;
    private DbsField pnd_Tiaa_Info_Cis_Frst_Annt_Mid_Nme;
    private DbsField pnd_Tiaa_Info_Cis_Frst_Annt_Lst_Nme;
    private DbsField pnd_Tiaa_Info_Cis_Frst_Annt_Sffx;
    private DbsField pnd_Tiaa_Info_Cis_Frst_Annt_Dob_A;

    private DbsGroup pnd_Tiaa_Info__R_Field_17;
    private DbsField pnd_Tiaa_Info_Cis_Frst_Annt_Dob;
    private DbsField pnd_Tiaa_Info_Cis_Frst_Annt_Sex_Cde;
    private DbsField pnd_Tiaa_Info_Cis_Tiaa_Nbr;
    private DbsField pnd_Tiaa_Info_Cis_Tiaa_Doi_N;

    private DbsGroup pnd_Tiaa_Info__R_Field_18;
    private DbsField pnd_Tiaa_Info_Cis_Tiaa_Doi;
    private DbsField pnd_Tiaa_Info_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField pnd_Tiaa_Info_Cis_Product_Code;
    private DbsField pnd_Tiaa_Info_Cis_Annty_Option;
    private DbsField pnd_Tiaa_Info_Cis_Rqst_Id_Key;

    private DbsGroup pnd_Tiaa_Info__R_Field_19;
    private DbsField pnd_Tiaa_Info_Cis_Rqst_Pin;
    private DbsField pnd_Tiaa_Info_Cis_Rqst_Rest;
    private DbsField pnd_Tiaa_Info_Cis_Sg_Text_Udf_3;

    private DbsGroup pnd_Tiaa_Scnd_Info;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Rqst_Id;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Function_Cde;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Ssn;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Prfx;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Frst_Nme;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Mid_Nme;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Lst_Nme;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Sffx;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Dob_A;

    private DbsGroup pnd_Tiaa_Scnd_Info__R_Field_20;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Dob;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Tiaa_Nbr;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Tiaa_Doi_N;

    private DbsGroup pnd_Tiaa_Scnd_Info__R_Field_21;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Tiaa_Doi;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Product_Code;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Annty_Option;
    private DbsField pnd_Tiaa_Scnd_Info_Cis_Sg_Text_Udf_3;

    private DbsGroup pnd_Cref_Info;
    private DbsField pnd_Cref_Info_Cis_Rqst_Id;
    private DbsField pnd_Cref_Info_Cis_Function_Cde;
    private DbsField pnd_Cref_Info_Cis_Pin_Nbr;
    private DbsField pnd_Cref_Info_Cis_Frst_Annt_Ssn;
    private DbsField pnd_Cref_Info_Cis_Frst_Annt_Prfx;
    private DbsField pnd_Cref_Info_Cis_Frst_Annt_Frst_Nme;
    private DbsField pnd_Cref_Info_Cis_Frst_Annt_Mid_Nme;
    private DbsField pnd_Cref_Info_Cis_Frst_Annt_Lst_Nme;
    private DbsField pnd_Cref_Info_Cis_Frst_Annt_Sffx;
    private DbsField pnd_Cref_Info_Cis_Frst_Annt_Dob_A;

    private DbsGroup pnd_Cref_Info__R_Field_22;
    private DbsField pnd_Cref_Info_Cis_Frst_Annt_Dob;
    private DbsField pnd_Cref_Info_Cis_Frst_Annt_Sex_Cde;
    private DbsField pnd_Cref_Info_Cis_Tiaa_Nbr;
    private DbsField pnd_Cref_Info_Cis_Cert_Nbr;
    private DbsField pnd_Cref_Info_Cis_Cref_Doi_N;

    private DbsGroup pnd_Cref_Info__R_Field_23;
    private DbsField pnd_Cref_Info_Cis_Cref_Doi;
    private DbsField pnd_Cref_Info_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField pnd_Cref_Info_Cis_Product_Code;
    private DbsField pnd_Cref_Info_Cis_Annty_Option;
    private DbsField pnd_Cref_Info_Cis_Sg_Text_Udf_3;

    private DbsGroup pnd_Cref_Scnd_Info;
    private DbsField pnd_Cref_Scnd_Info_Cis_Rqst_Id;
    private DbsField pnd_Cref_Scnd_Info_Cis_Function_Cde;
    private DbsField pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Ssn;
    private DbsField pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Prfx;
    private DbsField pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Frst_Nme;
    private DbsField pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Mid_Nme;
    private DbsField pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Lst_Nme;
    private DbsField pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Sffx;
    private DbsField pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Dob_A;

    private DbsGroup pnd_Cref_Scnd_Info__R_Field_24;
    private DbsField pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Dob;
    private DbsField pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Cref_Scnd_Info_Cis_Cert_Nbr;
    private DbsField pnd_Cref_Scnd_Info_Cis_Cref_Doi;
    private DbsField pnd_Cref_Scnd_Info_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField pnd_Cref_Scnd_Info_Cis_Product_Code;
    private DbsField pnd_Cref_Scnd_Info_Cis_Annty_Option;
    private DbsField pnd_Cref_Scnd_Info_Cis_Sg_Text_Udf_3;

    private DbsGroup pnd_Cref_Trns;
    private DbsField pnd_Cref_Trns_Cis_Rqst_Id;
    private DbsField pnd_Cref_Trns_Cis_Function_Cde;
    private DbsField pnd_Cref_Trns_Cis_Pin_Nbr;
    private DbsField pnd_Cref_Trns_Cis_Scnd_Annt_Ssn;
    private DbsField pnd_Cref_Trns_Cis_Scnd_Annt_Prfx;
    private DbsField pnd_Cref_Trns_Cis_Scnd_Annt_Frst_Nme;
    private DbsField pnd_Cref_Trns_Cis_Scnd_Annt_Mid_Nme;
    private DbsField pnd_Cref_Trns_Cis_Scnd_Annt_Lst_Nme;
    private DbsField pnd_Cref_Trns_Cis_Scnd_Annt_Sffx;
    private DbsField pnd_Cref_Trns_Cis_Scnd_Annt_Dob_A;

    private DbsGroup pnd_Cref_Trns__R_Field_25;
    private DbsField pnd_Cref_Trns_Cis_Scnd_Annt_Dob;
    private DbsField pnd_Cref_Trns_Cis_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Cref_Trns_Cis_Tiaa_Nbr;
    private DbsField pnd_Cref_Trns_Cis_Cert_Nbr;
    private DbsField pnd_Cref_Trns_Cis_Cref_Doi_N;

    private DbsGroup pnd_Cref_Trns__R_Field_26;
    private DbsField pnd_Cref_Trns_Cis_Cref_Doi;
    private DbsField pnd_Cref_Trns_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField pnd_Cref_Trns_Cis_Product_Code;
    private DbsField pnd_Cref_Trns_Cis_Annty_Option;
    private DbsField pnd_Cref_Trns_Cis_Sg_Text_Udf_3;

    private DbsGroup pnd_Mdo_Info;
    private DbsField pnd_Mdo_Info_Cis_Rqst_Id;
    private DbsField pnd_Mdo_Info_Cis_Function_Cde;
    private DbsField pnd_Mdo_Info_Cis_Pin_Nbr;
    private DbsField pnd_Mdo_Info_Cis_Frst_Annt_Ssn;

    private DbsGroup pnd_Mdo_Info__R_Field_27;
    private DbsField pnd_Mdo_Info_Cis_Frst_Annt_Ssn_A;
    private DbsField pnd_Mdo_Info_Cis_Frst_Annt_Prfx;
    private DbsField pnd_Mdo_Info_Cis_Frst_Annt_Frst_Nme;
    private DbsField pnd_Mdo_Info_Cis_Frst_Annt_Mid_Nme;
    private DbsField pnd_Mdo_Info_Cis_Frst_Annt_Lst_Nme;
    private DbsField pnd_Mdo_Info_Cis_Frst_Annt_Sffx;
    private DbsField pnd_Mdo_Info_Cis_Frst_Annt_Sex_Cde;
    private DbsField pnd_Mdo_Info_Cis_Frst_Annt_Dob_A;

    private DbsGroup pnd_Mdo_Info__R_Field_28;
    private DbsField pnd_Mdo_Info_Cis_Frst_Annt_Dob;
    private DbsField pnd_Mdo_Info_Cis_Tiaa_Nbr;
    private DbsField pnd_Mdo_Info_Cis_Tiaa_Doi_N;

    private DbsGroup pnd_Mdo_Info__R_Field_29;
    private DbsField pnd_Mdo_Info_Cis_Tiaa_Doi;
    private DbsField pnd_Mdo_Info_Cis_Cor_Cntrct_Status_Cde;
    private DbsField pnd_Mdo_Info_Cis_Cor_Cntrct_Status_Yr_Dte;
    private DbsField pnd_Mdo_Info_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField pnd_Mdo_Info_Cis_Cert_Nbr;
    private DbsField pnd_Mdo_Info_Cis_Ownership_Cd;
    private DbsField pnd_Mdo_Info_Addr_Line1;
    private DbsField pnd_Mdo_Info_Addr_Line2;
    private DbsField pnd_Mdo_Info_Addr_Line3;
    private DbsField pnd_Mdo_Info_Addr_Line4;
    private DbsField pnd_Mdo_Info_Addr_Line5;
    private DbsField pnd_Mdo_Info_Addr_Line6;
    private DbsField pnd_Mdo_Info_Cis_Zip_Code_Ia;
    private DbsField pnd_Mdo_Info_Cis_Rqst_Id_Key;

    private DbsGroup pnd_Mdo_Info__R_Field_30;
    private DbsField pnd_Mdo_Info_Cis_Rqst_Pin;
    private DbsField pnd_Mdo_Info_Cis_Rqst_Rest;
    private DbsField pnd_Mdo_Info_Cis_Sg_Text_Udf_3;

    private DbsGroup cth_Out_Record;
    private DbsField cth_Out_Record_Cth_Rqst_Id_Key;
    private DbsField cth_Out_Record_Cth_Tiaa_Nbr;
    private DbsField cth_Out_Record_Cth_Cert_Nbr;
    private DbsField cth_Out_Record_Cth_Status;
    private DbsField cth_Out_Record_Cth_Error_Message;
    private DbsField pnd_Init_Rqst_Id;
    private DbsField pnd_Next_Rqst_Id;
    private DbsField pnd_I;
    private DbsField pnd_Frst_Time;
    private DbsField pnd_Cor_Error;
    private DbsField pnd_Mdo_Cntr_Type_S;
    private DbsField pnd_Bene_In_Plan;
    private DbsField pnd_On_Cor;
    private DbsField pnd_Isn;
    private DbsField pnd_Ssn;
    private DbsField pnd_Name;
    private DbsField pnd_Short_Name;
    private DbsField pnd_C_Type;
    private DbsField pnd_Idat_A;
    private DbsField pnd_Error_Msg_Trunc;
    private DbsField pnd_Dat;

    private DbsGroup pnd_Dat__R_Field_31;
    private DbsField pnd_Dat_Pnd_D_Yyyy;
    private DbsField pnd_Dat_Pnd_D_Mm;
    private DbsField pnd_Dat_Pnd_D_Dd;
    private DbsField pnd_Dob_A;
    private DbsField pnd_Dob;

    private DbsGroup pnd_Dob__R_Field_32;
    private DbsField pnd_Dob_Pnd_Dob_Yyyy;
    private DbsField pnd_Dob_Pnd_Dob_Mm;
    private DbsField pnd_Dob_Pnd_Dob_Dd;
    private DbsField pnd_Bene_Rqst_Id_Key;

    private DbsGroup pnd_Bene_Rqst_Id_Key__R_Field_33;
    private DbsField pnd_Bene_Rqst_Id_Key_Pnd_Bene_Rqst_Pin;
    private DbsField pnd_Bene_Rqst_Id_Key_Pnd_Bene_Rqst_Rest;
    private DbsField pnd_Scnd_Annt_Added;
    private DbsField pnd_Ssn_Nbr;
    private DbsField pnd_Found;
    private DbsField pnd_Rcrd_Type;
    private DbsField pnd_Total_Tiaa;
    private DbsField pnd_Total_Cref;
    private DbsField pnd_Total_Tiaa_Scnd;
    private DbsField pnd_Total_Cref_Scnd;
    private DbsField pnd_Total;
    private DbsField pnd_Total_Ia;
    private DbsField pnd_Total_Tpa;
    private DbsField pnd_Total_Tpa_Omni;
    private DbsField pnd_Total_Tpi_Omni;
    private DbsField pnd_Total_Ipro;
    private DbsField pnd_Total_Ipro_Omni;
    private DbsField pnd_Total_Ipi_Omni;
    private DbsField pnd_Total_Mdo;
    private DbsField pnd_Total_Add_Tiaa_Scnd;
    private DbsField pnd_Total_Add_Cref_Scnd;
    private DbsField pnd_Total_Add_Cntrct_Tiaa_Scnd;
    private DbsField pnd_Total_Add_Cntrct_Cref_Scnd;
    private DbsField pnd_Ipro_Error;
    private DbsField pnd_Ipi_Error;
    private DbsField pnd_Tpa_Error;
    private DbsField pnd_Tpi_Error;
    private DbsField pnd_Mdo_Error;
    private DbsField pnd_Unkwn_Error;
    private DbsField pnd_Total_Errors;
    private DbsField pnd_Sip_Errors;
    private DbsField pnd_Non_Sip_Errors;
    private DbsField pnd_Mdo_Tpa_Ipro_Errors;
    private DbsField cth_Out_Record_Written;
    private DbsField pnd_Hold_Rqst_Id;
    private DbsField pnd_At_Start;
    private DbsField pnd_First_Time;
    private DbsField pnd_Option_Table;
    private DbsField pnd_Tbl_Size;
    private DbsField pnd_Option_Table_2;

    private DbsGroup pnd_Option_Table_2__R_Field_34;
    private DbsField pnd_Option_Table_2_Pnd_Option_Code;
    private DbsField pnd_Option_Table_2_Pnd_Option_Yrs;
    private DbsField pnd_Option_Table_2_Pnd_Option_Code_Cor;
    private DbsField pnd_Cis_Annty_Option;
    private DbsField pnd_Tiaa_Trnsf_Option;

    private DbsGroup pnd_Tiaa_Trnsf_Option__R_Field_35;
    private DbsField pnd_Tiaa_Trnsf_Option_Pnd_Trnsf_Option_Cde;
    private DbsField pnd_Tiaa_Trnsf_Option_Pnd_Trnsf_Option_Yrs;
    private DbsField pnd_Mdm_Cntrct_Universal_Data;

    private DbsGroup pnd_Mdm_Cntrct_Universal_Data__R_Field_36;
    private DbsField pnd_Mdm_Cntrct_Universal_Data_Pnd_Mdm_Cntrct_Universal_Arr;
    private DbsField pnd_Doi;

    private DbsGroup pnd_Doi__R_Field_37;
    private DbsField pnd_Doi_Pnd_Doi_Ccyy;
    private DbsField pnd_Doi_Pnd_Doi_Mm;
    private DbsField pnd_Doi_Pnd_Doi_Dd;
    private DbsField pnd_Icp_Contract;
    private DbsField pnd_Cor_Api_Pin;
    private DbsField pnd_Rc;
    private DbsField pnd_Retry_Cnt;
    private DbsField intvl;
    private DbsField reqid;
    private int cmrollReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaCisl400 = new LdaCisl400();
        registerRecord(ldaCisl400);
        ldaCispartv = new LdaCispartv();
        registerRecord(ldaCispartv);
        registerRecord(ldaCispartv.getVw_cis_Part_View());
        ldaCisl5000 = new LdaCisl5000();
        registerRecord(ldaCisl5000);
        registerRecord(ldaCisl5000.getVw_cis_Bene_File_01());
        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        pdaMdma301 = new PdaMdma301(localVariables);
        pdaMdma371 = new PdaMdma371(localVariables);

        // Local Variables

        pnd_Work_File1 = localVariables.newGroupInRecord("pnd_Work_File1", "#WORK-FILE1");
        pnd_Work_File1_Cis_Pin_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Work_File1_Cis_Rqst_Id_Key = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 35);
        pnd_Work_File1_Cis_Tiaa_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Cert_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Tiaa_Doi = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_1 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_1", "REDEFINE", pnd_Work_File1_Cis_Tiaa_Doi);
        pnd_Work_File1_Cis_Tiaa_Doi_A = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Doi_A", "CIS-TIAA-DOI-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Cref_Doi = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_2 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_2", "REDEFINE", pnd_Work_File1_Cis_Cref_Doi);
        pnd_Work_File1_Cis_Cref_Doi_A = pnd_Work_File1__R_Field_2.newFieldInGroup("pnd_Work_File1_Cis_Cref_Doi_A", "CIS-CREF-DOI-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Ssn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Ssn", "CIS-FRST-ANNT-SSN", FieldType.NUMERIC, 
            9);

        pnd_Work_File1__R_Field_3 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_3", "REDEFINE", pnd_Work_File1_Cis_Frst_Annt_Ssn);
        pnd_Work_File1_Cis_Frst_Annt_Ssn_A = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Ssn_A", "CIS-FRST-ANNT-SSN-A", FieldType.STRING, 
            9);
        pnd_Work_File1_Cis_Frst_Annt_Prfx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Prfx", "CIS-FRST-ANNT-PRFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Frst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Frst_Nme", "CIS-FRST-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Mid_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Mid_Nme", "CIS-FRST-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Lst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Lst_Nme", "CIS-FRST-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Sffx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Sffx", "CIS-FRST-ANNT-SFFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd", "CIS-FRST-ANNT-CTZNSHP-CD", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde", "CIS-FRST-ANNT-RSDNC-CDE", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Frst_Annt_Dob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Dob", "CIS-FRST-ANNT-DOB", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_4 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_4", "REDEFINE", pnd_Work_File1_Cis_Frst_Annt_Dob);
        pnd_Work_File1_Cis_Frst_Annt_Dob_A = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Dob_A", "CIS-FRST-ANNT-DOB-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Sex_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Sex_Cde", "CIS-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Frst_Annt_Calc_Method = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Calc_Method", "CIS-FRST-ANNT-CALC-METHOD", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Opn_Clsd_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Opn_Clsd_Ind", "CIS-OPN-CLSD-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Status_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Status_Cd", "CIS-STATUS-CD", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cntrct_Apprvl_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Apprvl_Ind", "CIS-CNTRCT-APPRVL-IND", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Rqst_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Hold_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Hold_Cde", "CIS-HOLD-CDE", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Cntrct_Print_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Print_Dte", "CIS-CNTRCT-PRINT-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_5 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_5", "REDEFINE", pnd_Work_File1_Cis_Cntrct_Print_Dte);
        pnd_Work_File1_Cis_Cntrct_Print_Dte_A = pnd_Work_File1__R_Field_5.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Print_Dte_A", "CIS-CNTRCT-PRINT-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Extract_Date = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Extract_Date", "CIS-EXTRACT-DATE", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_6 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_6", "REDEFINE", pnd_Work_File1_Cis_Extract_Date);
        pnd_Work_File1_Cis_Extract_Date_A = pnd_Work_File1__R_Field_6.newFieldInGroup("pnd_Work_File1_Cis_Extract_Date_A", "CIS-EXTRACT-DATE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Rcvd_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_Dte", "CIS-APPL-RCVD-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_7 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_7", "REDEFINE", pnd_Work_File1_Cis_Appl_Rcvd_Dte);
        pnd_Work_File1_Cis_Appl_Rcvd_Dte_A = pnd_Work_File1__R_Field_7.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_Dte_A", "CIS-APPL-RCVD-DTE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Rcvd_User_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_User_Id", "CIS-APPL-RCVD-USER-ID", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Entry_User_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Entry_User_Id", "CIS-APPL-ENTRY-USER-ID", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Annty_Option = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Pymnt_Mode = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pymnt_Mode", "CIS-PYMNT-MODE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Annty_Start_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_Start_Dte", "CIS-ANNTY-START-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_8 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_8", "REDEFINE", pnd_Work_File1_Cis_Annty_Start_Dte);
        pnd_Work_File1_Cis_Annty_Start_Dte_A = pnd_Work_File1__R_Field_8.newFieldInGroup("pnd_Work_File1_Cis_Annty_Start_Dte_A", "CIS-ANNTY-START-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Annty_End_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_End_Dte", "CIS-ANNTY-END-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_9 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_9", "REDEFINE", pnd_Work_File1_Cis_Annty_End_Dte);
        pnd_Work_File1_Cis_Annty_End_Dte_A = pnd_Work_File1__R_Field_9.newFieldInGroup("pnd_Work_File1_Cis_Annty_End_Dte_A", "CIS-ANNTY-END-DTE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Grnted_Period_Yrs = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Grnted_Period_Yrs", "CIS-GRNTED-PERIOD-YRS", FieldType.NUMERIC, 
            2);
        pnd_Work_File1_Cis_Grnted_Period_Dys = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Grnted_Period_Dys", "CIS-GRNTED-PERIOD-DYS", FieldType.NUMERIC, 
            3);
        pnd_Work_File1_Cis_Scnd_Annt_Prfx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Prfx", "CIS-SCND-ANNT-PRFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme", "CIS-SCND-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme", "CIS-SCND-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme", "CIS-SCND-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Sffx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Sffx", "CIS-SCND-ANNT-SFFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Ssn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Work_File1_Cis_Scnd_Annt_Dob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Dob", "CIS-SCND-ANNT-DOB", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_10 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_10", "REDEFINE", pnd_Work_File1_Cis_Scnd_Annt_Dob);
        pnd_Work_File1_Cis_Scnd_Annt_Dob_A = pnd_Work_File1__R_Field_10.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Dob_A", "CIS-SCND-ANNT-DOB-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde", "CIS-SCND-ANNT-SEX-CDE", FieldType.STRING, 
            1);

        pnd_Work_File1_Cis_Cref_Annty_Pymnt = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cref_Annty_Pymnt", "CIS-CREF-ANNTY-PYMNT", new DbsArrayController(1, 
            20));
        pnd_Work_File1_Cis_Cref_Acct_Cde = pnd_Work_File1_Cis_Cref_Annty_Pymnt.newFieldInGroup("pnd_Work_File1_Cis_Cref_Acct_Cde", "CIS-CREF-ACCT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Orig_Issue_State = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Orig_Issue_State", "CIS-ORIG-ISSUE-STATE", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Issue_State_Name = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Issue_State_Name", "CIS-ISSUE-STATE-NAME", FieldType.STRING, 
            15);
        pnd_Work_File1_Cis_Issue_State_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Issue_State_Cd", "CIS-ISSUE-STATE-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Ppg_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ppg_Code", "CIS-PPG-CODE", FieldType.STRING, 6);
        pnd_Work_File1_Cis_Region_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Region_Code", "CIS-REGION-CODE", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Ownership_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ownership_Cd", "CIS-OWNERSHIP-CD", FieldType.NUMERIC, 1);
        pnd_Work_File1_Cis_Bill_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Bill_Code", "CIS-BILL-CODE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Lob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Lob", "CIS-LOB", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Lob_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Lob_Type", "CIS-LOB-TYPE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Syn_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Addr_Syn_Ind", "CIS-ADDR-SYN-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Process_Env = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Addr_Process_Env", "CIS-ADDR-PROCESS-ENV", FieldType.STRING, 
            1);

        pnd_Work_File1_Cis_Address_Info = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Address_Info", "CIS-ADDRESS-INFO", new DbsArrayController(1, 
            3));
        pnd_Work_File1_Cis_Address_Chg_Ind = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Address_Chg_Ind", "CIS-ADDRESS-CHG-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Address_Dest_Name = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Address_Dest_Name", "CIS-ADDRESS-DEST-NAME", 
            FieldType.STRING, 35);
        pnd_Work_File1_Cis_Address_Txt = pnd_Work_File1_Cis_Address_Info.newFieldArrayInGroup("pnd_Work_File1_Cis_Address_Txt", "CIS-ADDRESS-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 5));
        pnd_Work_File1_Cis_Zip_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Zip_Code", "CIS-ZIP-CODE", FieldType.STRING, 
            9);
        pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr", "CIS-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr", "CIS-BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9);
        pnd_Work_File1_Cis_Stndrd_Trn_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Stndrd_Trn_Cd", "CIS-STNDRD-TRN-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Finalist_Reason_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Finalist_Reason_Cd", "CIS-FINALIST-REASON-CD", 
            FieldType.STRING, 10);
        pnd_Work_File1_Cis_Addr_Usage_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Addr_Usage_Code", "CIS-ADDR-USAGE-CODE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Checking_Saving_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Checking_Saving_Cd", "CIS-CHECKING-SAVING-CD", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Stndrd_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Addr_Stndrd_Code", "CIS-ADDR-STNDRD-CODE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Stndrd_Overide = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Stndrd_Overide", "CIS-STNDRD-OVERIDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Postal_Data_Fields = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Postal_Data_Fields", "CIS-POSTAL-DATA-FIELDS", 
            FieldType.STRING, 44);
        pnd_Work_File1_Cis_Geographic_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Geographic_Cd", "CIS-GEOGRAPHIC-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Corp_Sync_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Corp_Sync_Ind", "CIS-CORP-SYNC-IND", FieldType.STRING, 1);

        pnd_Work_File1_Cis_Rqst_System_Mit_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Rqst_System_Mit_Dta", "CIS-RQST-SYSTEM-MIT-DTA");
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind", 
            "CIS-RQST-SYS-MIT-SYNC-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme", 
            "CIS-RQST-SYS-MIT-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Function = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Function", 
            "CIS-RQST-SYS-MIT-FUNCTION", FieldType.STRING, 2);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env", 
            "CIS-RQST-SYS-MIT-PROCESS-ENV", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Mit_Error_Cde = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Mit_Error_Cde", "CIS-MIT-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Mit_Error_Msg = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Mit_Error_Msg", "CIS-MIT-ERROR-MSG", 
            FieldType.STRING, 72);

        pnd_Work_File1_Cis_Non_Premium_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Non_Premium_Dta", "CIS-NON-PREMIUM-DTA");
        pnd_Work_File1_Cis_Nonp_Sync_Ind = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Sync_Ind", "CIS-NONP-SYNC-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Process_Env = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Process_Env", "CIS-NONP-PROCESS-ENV", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Effective_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Effective_Dte", "CIS-NONP-EFFECTIVE-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_11 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_11", "REDEFINE", pnd_Work_File1_Cis_Nonp_Effective_Dte);
        pnd_Work_File1_Cis_Nonp_Effective_Dte_A = pnd_Work_File1__R_Field_11.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Effective_Dte_A", "CIS-NONP-EFFECTIVE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Product_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Product_Cde", "CIS-NONP-PRODUCT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte", "CIS-NONP-CONTRACT-RETR-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_12 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_12", "REDEFINE", pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte);
        pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A = pnd_Work_File1__R_Field_12.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A", "CIS-NONP-CONTRACT-RETR-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt", 
            "CIS-NONP-RATE-ACCUM-ADJ-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Currency = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Currency", "CIS-NONP-CURRENCY", 
            FieldType.NUMERIC, 1);
        pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt", 
            "CIS-NONP-TIAA-AGE-FIRST-PYMNT", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt", 
            "CIS-NONP-CREF-AGE-FIRST-PYMNT", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Deletion_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Deletion_Cde", "CIS-NONP-DELETION-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Vesting_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Vesting_Dte", "CIS-NONP-VESTING-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_13 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_13", "REDEFINE", pnd_Work_File1_Cis_Nonp_Vesting_Dte);
        pnd_Work_File1_Cis_Nonp_Vesting_Dte_A = pnd_Work_File1__R_Field_13.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Vesting_Dte_A", "CIS-NONP-VESTING-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt", 
            "CIS-NONP-CNTR-RATE38-ACCUM-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt", 
            "CIS-NONP-CNTR-RATE42-ACCUM-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Error_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Error_Cde", "CIS-NONP-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Error_Msg = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Error_Msg", "CIS-NONP-ERROR-MSG", 
            FieldType.STRING, 72);

        pnd_Work_File1_Cis_Allocation_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Allocation_Dta", "CIS-ALLOCATION-DTA");
        pnd_Work_File1_Cis_Alloc_Sync_Ind = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Sync_Ind", "CIS-ALLOC-SYNC-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Alloc_Process_Env = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Process_Env", "CIS-ALLOC-PROCESS-ENV", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Alloc_Effective_Dte = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Effective_Dte", "CIS-ALLOC-EFFECTIVE-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_14 = pnd_Work_File1_Cis_Allocation_Dta.newGroupInGroup("pnd_Work_File1__R_Field_14", "REDEFINE", pnd_Work_File1_Cis_Alloc_Effective_Dte);
        pnd_Work_File1_Cis_Alloc_Effective_Dte_A = pnd_Work_File1__R_Field_14.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Effective_Dte_A", "CIS-ALLOC-EFFECTIVE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Alloc_Percentage = pnd_Work_File1_Cis_Allocation_Dta.newFieldArrayInGroup("pnd_Work_File1_Cis_Alloc_Percentage", "CIS-ALLOC-PERCENTAGE", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20));
        pnd_Work_File1_Cis_Alloc_Error_Cde = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Error_Cde", "CIS-ALLOC-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Alloc_Error_Msg = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Error_Msg", "CIS-ALLOC-ERROR-MSG", 
            FieldType.STRING, 72);
        pnd_Work_File1_Cis_Cor_Sync_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Sync_Ind", "CIS-COR-SYNC-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Process_Env = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Process_Env", "CIS-COR-PROCESS-ENV", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde", "CIS-COR-PH-RCD-TYP-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde", "CIS-COR-PH-NEG-ELECTION-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme", "CIS-COR-PH-OCCUPNT-NME", FieldType.STRING, 
            10);
        pnd_Work_File1_Cis_Cor_Ph_Xref_Pin = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Xref_Pin", "CIS-COR-PH-XREF-PIN", FieldType.NUMERIC, 
            12);
        pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt", "CIS-COR-PH-ACTIVE-VIP-CNT", 
            FieldType.NUMERIC, 2);

        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp", "CIS-COR-PH-ACTVE-VIP-GRP", 
            new DbsArrayController(1, 8));
        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde = pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde", "CIS-COR-PH-ACTVE-VIP-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde = pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde", 
            "CIS-COR-PH-ACTVE-VIP-AREA-CDE", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Cor_Mail_Table_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Mail_Table_Cnt", "CIS-COR-MAIL-TABLE-CNT", FieldType.NUMERIC, 
            2);

        pnd_Work_File1_Cis_Cor_Ph_Mail_Grp = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cor_Ph_Mail_Grp", "CIS-COR-PH-MAIL-GRP", new DbsArrayController(1, 
            25));
        pnd_Work_File1_Cis_Cor_Phmail_Cde = pnd_Work_File1_Cis_Cor_Ph_Mail_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Phmail_Cde", "CIS-COR-PHMAIL-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin = pnd_Work_File1_Cis_Cor_Ph_Mail_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin", 
            "CIS-COR-PHMAIL-AREA-OF-ORIGIN", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte", "CIS-COR-INST-PH-RMTTNG-INSTN-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_15 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_15", "REDEFINE", pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte);
        pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta = pnd_Work_File1__R_Field_15.newFieldInGroup("pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta", 
            "CIS-COR-INST-PH-RMTTNG-INSTN-DTA", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr", "CIS-COR-PH-RMTTNG-INSTN-NBR", 
            FieldType.NUMERIC, 5);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind", "CIS-COR-PH-RMTTNG-INSTN-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind", "CIS-COR-PH-RMMTNG-INSTN-PDUP-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde", "CIS-COR-PH-RMTTNG-NMBR-RANGE-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind", "CIS-COR-PH-MCRJCK-MEDIA-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde", "CIS-COR-PH-MCRJCK-CNTNTS-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt", "CIS-COR-CNTRCT-TABLE-CNT", 
            FieldType.NUMERIC, 2);
        pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde", "CIS-COR-CNTRCT-STATUS-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte", "CIS-COR-CNTRCT-STATUS-YR-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Error_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Error_Cde", "CIS-COR-ERROR-CDE", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Pull_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pull_Code", "CIS-PULL-CODE", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Trnsf_Flag = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_Flag", "CIS-TRNSF-FLAG", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Trnsf_From_Company = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_From_Company", "CIS-TRNSF-FROM-COMPANY", FieldType.STRING, 
            4);
        pnd_Work_File1_Cis_Trnsf_To_Company = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_To_Company", "CIS-TRNSF-TO-COMPANY", FieldType.STRING, 
            4);
        pnd_Work_File1_Cis_Tiaa_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Cntrct_Type", "CIS-TIAA-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Rea_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rea_Cntrct_Type", "CIS-REA-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Cref_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cref_Cntrct_Type", "CIS-CREF-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Decedent_Contract = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Decedent_Contract", "CIS-DECEDENT-CONTRACT", FieldType.STRING, 
            10);
        pnd_Work_File1_Cis_Relation_To_Decedent = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Relation_To_Decedent", "CIS-RELATION-TO-DECEDENT", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Ssn_Tin_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ssn_Tin_Ind", "CIS-SSN-TIN-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Isn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Isn", "CIS-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Work_File1_Cis_Mdo_Contract_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Mdo_Contract_Type", "CIS-MDO-CONTRACT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Sg_Text_Udf_3 = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", FieldType.STRING, 10);

        vw_bene_Update = new DataAccessProgramView(new NameInfo("vw_bene_Update", "BENE-UPDATE"), "CIS_BENE_FILE_01_12", "CIS_BENE_FILE");
        bene_Update_Cis_Bene_Unique_Id_Nbr = vw_bene_Update.getRecord().newFieldInGroup("bene_Update_Cis_Bene_Unique_Id_Nbr", "CIS-BENE-UNIQUE-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CIS_BENE_UNIQUE_ID_NBR");
        bene_Update_Cis_Bene_Rqst_Id_Key = vw_bene_Update.getRecord().newFieldInGroup("bene_Update_Cis_Bene_Rqst_Id_Key", "CIS-BENE-RQST-ID-KEY", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "CIS_BENE_RQST_ID_KEY");
        registerRecord(vw_bene_Update);

        pnd_Cis_Bene_Super_2 = localVariables.newFieldInRecord("pnd_Cis_Bene_Super_2", "#CIS-BENE-SUPER-2", FieldType.STRING, 11);

        pnd_Cis_Bene_Super_2__R_Field_16 = localVariables.newGroupInRecord("pnd_Cis_Bene_Super_2__R_Field_16", "REDEFINE", pnd_Cis_Bene_Super_2);
        pnd_Cis_Bene_Super_2_Pnd_Cis_Rcrd_Type_Cde = pnd_Cis_Bene_Super_2__R_Field_16.newFieldInGroup("pnd_Cis_Bene_Super_2_Pnd_Cis_Rcrd_Type_Cde", "#CIS-RCRD-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Cis_Bene_Super_2_Pnd_Cis_Bene_Tiaa_Nbr = pnd_Cis_Bene_Super_2__R_Field_16.newFieldInGroup("pnd_Cis_Bene_Super_2_Pnd_Cis_Bene_Tiaa_Nbr", "#CIS-BENE-TIAA-NBR", 
            FieldType.STRING, 10);

        pnd_Tiaa_Info = localVariables.newGroupInRecord("pnd_Tiaa_Info", "#TIAA-INFO");
        pnd_Tiaa_Info_Cis_Rqst_Id = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8);
        pnd_Tiaa_Info_Cis_Function_Cde = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Function_Cde", "CIS-FUNCTION-CDE", FieldType.STRING, 3);
        pnd_Tiaa_Info_Cis_Pin_Nbr = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Tiaa_Info_Cis_Frst_Annt_Ssn = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Frst_Annt_Ssn", "CIS-FRST-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Tiaa_Info_Cis_Frst_Annt_Prfx = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Frst_Annt_Prfx", "CIS-FRST-ANNT-PRFX", FieldType.STRING, 8);
        pnd_Tiaa_Info_Cis_Frst_Annt_Frst_Nme = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Frst_Annt_Frst_Nme", "CIS-FRST-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Tiaa_Info_Cis_Frst_Annt_Mid_Nme = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Frst_Annt_Mid_Nme", "CIS-FRST-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Tiaa_Info_Cis_Frst_Annt_Lst_Nme = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Frst_Annt_Lst_Nme", "CIS-FRST-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Tiaa_Info_Cis_Frst_Annt_Sffx = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Frst_Annt_Sffx", "CIS-FRST-ANNT-SFFX", FieldType.STRING, 8);
        pnd_Tiaa_Info_Cis_Frst_Annt_Dob_A = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Frst_Annt_Dob_A", "CIS-FRST-ANNT-DOB-A", FieldType.STRING, 
            8);

        pnd_Tiaa_Info__R_Field_17 = pnd_Tiaa_Info.newGroupInGroup("pnd_Tiaa_Info__R_Field_17", "REDEFINE", pnd_Tiaa_Info_Cis_Frst_Annt_Dob_A);
        pnd_Tiaa_Info_Cis_Frst_Annt_Dob = pnd_Tiaa_Info__R_Field_17.newFieldInGroup("pnd_Tiaa_Info_Cis_Frst_Annt_Dob", "CIS-FRST-ANNT-DOB", FieldType.NUMERIC, 
            8);
        pnd_Tiaa_Info_Cis_Frst_Annt_Sex_Cde = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Frst_Annt_Sex_Cde", "CIS-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        pnd_Tiaa_Info_Cis_Tiaa_Nbr = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10);
        pnd_Tiaa_Info_Cis_Tiaa_Doi_N = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Tiaa_Doi_N", "CIS-TIAA-DOI-N", FieldType.NUMERIC, 8);

        pnd_Tiaa_Info__R_Field_18 = pnd_Tiaa_Info.newGroupInGroup("pnd_Tiaa_Info__R_Field_18", "REDEFINE", pnd_Tiaa_Info_Cis_Tiaa_Doi_N);
        pnd_Tiaa_Info_Cis_Tiaa_Doi = pnd_Tiaa_Info__R_Field_18.newFieldInGroup("pnd_Tiaa_Info_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.NUMERIC, 8);
        pnd_Tiaa_Info_Cis_Cor_Cntrct_Payee_Cde = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Tiaa_Info_Cis_Product_Code = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Product_Code", "CIS-PRODUCT-CODE", FieldType.STRING, 1);
        pnd_Tiaa_Info_Cis_Annty_Option = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 2);
        pnd_Tiaa_Info_Cis_Rqst_Id_Key = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 35);

        pnd_Tiaa_Info__R_Field_19 = pnd_Tiaa_Info.newGroupInGroup("pnd_Tiaa_Info__R_Field_19", "REDEFINE", pnd_Tiaa_Info_Cis_Rqst_Id_Key);
        pnd_Tiaa_Info_Cis_Rqst_Pin = pnd_Tiaa_Info__R_Field_19.newFieldInGroup("pnd_Tiaa_Info_Cis_Rqst_Pin", "CIS-RQST-PIN", FieldType.NUMERIC, 12);
        pnd_Tiaa_Info_Cis_Rqst_Rest = pnd_Tiaa_Info__R_Field_19.newFieldInGroup("pnd_Tiaa_Info_Cis_Rqst_Rest", "CIS-RQST-REST", FieldType.STRING, 23);
        pnd_Tiaa_Info_Cis_Sg_Text_Udf_3 = pnd_Tiaa_Info.newFieldInGroup("pnd_Tiaa_Info_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", FieldType.STRING, 10);

        pnd_Tiaa_Scnd_Info = localVariables.newGroupInRecord("pnd_Tiaa_Scnd_Info", "#TIAA-SCND-INFO");
        pnd_Tiaa_Scnd_Info_Cis_Rqst_Id = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8);
        pnd_Tiaa_Scnd_Info_Cis_Function_Cde = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Function_Cde", "CIS-FUNCTION-CDE", FieldType.STRING, 
            3);
        pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Ssn = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Prfx = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Prfx", "CIS-SCND-ANNT-PRFX", FieldType.STRING, 
            8);
        pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Frst_Nme = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Frst_Nme", "CIS-SCND-ANNT-FRST-NME", 
            FieldType.STRING, 30);
        pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Mid_Nme = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Mid_Nme", "CIS-SCND-ANNT-MID-NME", 
            FieldType.STRING, 30);
        pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Lst_Nme = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Lst_Nme", "CIS-SCND-ANNT-LST-NME", 
            FieldType.STRING, 30);
        pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Sffx = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Sffx", "CIS-SCND-ANNT-SFFX", FieldType.STRING, 
            8);
        pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Dob_A = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Dob_A", "CIS-SCND-ANNT-DOB-A", FieldType.STRING, 
            8);

        pnd_Tiaa_Scnd_Info__R_Field_20 = pnd_Tiaa_Scnd_Info.newGroupInGroup("pnd_Tiaa_Scnd_Info__R_Field_20", "REDEFINE", pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Dob_A);
        pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Dob = pnd_Tiaa_Scnd_Info__R_Field_20.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Dob", "CIS-SCND-ANNT-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Sex_Cde = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Sex_Cde", "CIS-SCND-ANNT-SEX-CDE", 
            FieldType.STRING, 1);
        pnd_Tiaa_Scnd_Info_Cis_Tiaa_Nbr = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10);
        pnd_Tiaa_Scnd_Info_Cis_Tiaa_Doi_N = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Tiaa_Doi_N", "CIS-TIAA-DOI-N", FieldType.NUMERIC, 
            8);

        pnd_Tiaa_Scnd_Info__R_Field_21 = pnd_Tiaa_Scnd_Info.newGroupInGroup("pnd_Tiaa_Scnd_Info__R_Field_21", "REDEFINE", pnd_Tiaa_Scnd_Info_Cis_Tiaa_Doi_N);
        pnd_Tiaa_Scnd_Info_Cis_Tiaa_Doi = pnd_Tiaa_Scnd_Info__R_Field_21.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.NUMERIC, 
            8);
        pnd_Tiaa_Scnd_Info_Cis_Cor_Cntrct_Payee_Cde = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Tiaa_Scnd_Info_Cis_Product_Code = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Product_Code", "CIS-PRODUCT-CODE", FieldType.STRING, 
            1);
        pnd_Tiaa_Scnd_Info_Cis_Annty_Option = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 
            2);
        pnd_Tiaa_Scnd_Info_Cis_Sg_Text_Udf_3 = pnd_Tiaa_Scnd_Info.newFieldInGroup("pnd_Tiaa_Scnd_Info_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", FieldType.STRING, 
            10);

        pnd_Cref_Info = localVariables.newGroupInRecord("pnd_Cref_Info", "#CREF-INFO");
        pnd_Cref_Info_Cis_Rqst_Id = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8);
        pnd_Cref_Info_Cis_Function_Cde = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Function_Cde", "CIS-FUNCTION-CDE", FieldType.STRING, 3);
        pnd_Cref_Info_Cis_Pin_Nbr = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Cref_Info_Cis_Frst_Annt_Ssn = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Frst_Annt_Ssn", "CIS-FRST-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Cref_Info_Cis_Frst_Annt_Prfx = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Frst_Annt_Prfx", "CIS-FRST-ANNT-PRFX", FieldType.STRING, 8);
        pnd_Cref_Info_Cis_Frst_Annt_Frst_Nme = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Frst_Annt_Frst_Nme", "CIS-FRST-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Cref_Info_Cis_Frst_Annt_Mid_Nme = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Frst_Annt_Mid_Nme", "CIS-FRST-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Cref_Info_Cis_Frst_Annt_Lst_Nme = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Frst_Annt_Lst_Nme", "CIS-FRST-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Cref_Info_Cis_Frst_Annt_Sffx = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Frst_Annt_Sffx", "CIS-FRST-ANNT-SFFX", FieldType.STRING, 8);
        pnd_Cref_Info_Cis_Frst_Annt_Dob_A = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Frst_Annt_Dob_A", "CIS-FRST-ANNT-DOB-A", FieldType.STRING, 
            8);

        pnd_Cref_Info__R_Field_22 = pnd_Cref_Info.newGroupInGroup("pnd_Cref_Info__R_Field_22", "REDEFINE", pnd_Cref_Info_Cis_Frst_Annt_Dob_A);
        pnd_Cref_Info_Cis_Frst_Annt_Dob = pnd_Cref_Info__R_Field_22.newFieldInGroup("pnd_Cref_Info_Cis_Frst_Annt_Dob", "CIS-FRST-ANNT-DOB", FieldType.NUMERIC, 
            8);
        pnd_Cref_Info_Cis_Frst_Annt_Sex_Cde = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Frst_Annt_Sex_Cde", "CIS-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        pnd_Cref_Info_Cis_Tiaa_Nbr = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10);
        pnd_Cref_Info_Cis_Cert_Nbr = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10);
        pnd_Cref_Info_Cis_Cref_Doi_N = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Cref_Doi_N", "CIS-CREF-DOI-N", FieldType.NUMERIC, 8);

        pnd_Cref_Info__R_Field_23 = pnd_Cref_Info.newGroupInGroup("pnd_Cref_Info__R_Field_23", "REDEFINE", pnd_Cref_Info_Cis_Cref_Doi_N);
        pnd_Cref_Info_Cis_Cref_Doi = pnd_Cref_Info__R_Field_23.newFieldInGroup("pnd_Cref_Info_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.NUMERIC, 8);
        pnd_Cref_Info_Cis_Cor_Cntrct_Payee_Cde = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Cref_Info_Cis_Product_Code = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Product_Code", "CIS-PRODUCT-CODE", FieldType.STRING, 1);
        pnd_Cref_Info_Cis_Annty_Option = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 2);
        pnd_Cref_Info_Cis_Sg_Text_Udf_3 = pnd_Cref_Info.newFieldInGroup("pnd_Cref_Info_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", FieldType.STRING, 10);

        pnd_Cref_Scnd_Info = localVariables.newGroupInRecord("pnd_Cref_Scnd_Info", "#CREF-SCND-INFO");
        pnd_Cref_Scnd_Info_Cis_Rqst_Id = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8);
        pnd_Cref_Scnd_Info_Cis_Function_Cde = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Function_Cde", "CIS-FUNCTION-CDE", FieldType.STRING, 
            3);
        pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Ssn = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Prfx = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Prfx", "CIS-SCND-ANNT-PRFX", FieldType.STRING, 
            8);
        pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Frst_Nme = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Frst_Nme", "CIS-SCND-ANNT-FRST-NME", 
            FieldType.STRING, 30);
        pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Mid_Nme = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Mid_Nme", "CIS-SCND-ANNT-MID-NME", 
            FieldType.STRING, 30);
        pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Lst_Nme = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Lst_Nme", "CIS-SCND-ANNT-LST-NME", 
            FieldType.STRING, 30);
        pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Sffx = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Sffx", "CIS-SCND-ANNT-SFFX", FieldType.STRING, 
            8);
        pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Dob_A = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Dob_A", "CIS-SCND-ANNT-DOB-A", FieldType.STRING, 
            8);

        pnd_Cref_Scnd_Info__R_Field_24 = pnd_Cref_Scnd_Info.newGroupInGroup("pnd_Cref_Scnd_Info__R_Field_24", "REDEFINE", pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Dob_A);
        pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Dob = pnd_Cref_Scnd_Info__R_Field_24.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Dob", "CIS-SCND-ANNT-DOB", 
            FieldType.NUMERIC, 8);
        pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Sex_Cde = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Sex_Cde", "CIS-SCND-ANNT-SEX-CDE", 
            FieldType.STRING, 1);
        pnd_Cref_Scnd_Info_Cis_Cert_Nbr = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10);
        pnd_Cref_Scnd_Info_Cis_Cref_Doi = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.NUMERIC, 8);
        pnd_Cref_Scnd_Info_Cis_Cor_Cntrct_Payee_Cde = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Cref_Scnd_Info_Cis_Product_Code = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Product_Code", "CIS-PRODUCT-CODE", FieldType.STRING, 
            1);
        pnd_Cref_Scnd_Info_Cis_Annty_Option = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 
            2);
        pnd_Cref_Scnd_Info_Cis_Sg_Text_Udf_3 = pnd_Cref_Scnd_Info.newFieldInGroup("pnd_Cref_Scnd_Info_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", FieldType.STRING, 
            10);

        pnd_Cref_Trns = localVariables.newGroupInRecord("pnd_Cref_Trns", "#CREF-TRNS");
        pnd_Cref_Trns_Cis_Rqst_Id = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8);
        pnd_Cref_Trns_Cis_Function_Cde = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Function_Cde", "CIS-FUNCTION-CDE", FieldType.STRING, 3);
        pnd_Cref_Trns_Cis_Pin_Nbr = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Cref_Trns_Cis_Scnd_Annt_Ssn = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Cref_Trns_Cis_Scnd_Annt_Prfx = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Scnd_Annt_Prfx", "CIS-SCND-ANNT-PRFX", FieldType.STRING, 8);
        pnd_Cref_Trns_Cis_Scnd_Annt_Frst_Nme = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Scnd_Annt_Frst_Nme", "CIS-SCND-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Cref_Trns_Cis_Scnd_Annt_Mid_Nme = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Scnd_Annt_Mid_Nme", "CIS-SCND-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Cref_Trns_Cis_Scnd_Annt_Lst_Nme = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Scnd_Annt_Lst_Nme", "CIS-SCND-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Cref_Trns_Cis_Scnd_Annt_Sffx = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Scnd_Annt_Sffx", "CIS-SCND-ANNT-SFFX", FieldType.STRING, 8);
        pnd_Cref_Trns_Cis_Scnd_Annt_Dob_A = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Scnd_Annt_Dob_A", "CIS-SCND-ANNT-DOB-A", FieldType.STRING, 
            8);

        pnd_Cref_Trns__R_Field_25 = pnd_Cref_Trns.newGroupInGroup("pnd_Cref_Trns__R_Field_25", "REDEFINE", pnd_Cref_Trns_Cis_Scnd_Annt_Dob_A);
        pnd_Cref_Trns_Cis_Scnd_Annt_Dob = pnd_Cref_Trns__R_Field_25.newFieldInGroup("pnd_Cref_Trns_Cis_Scnd_Annt_Dob", "CIS-SCND-ANNT-DOB", FieldType.NUMERIC, 
            8);
        pnd_Cref_Trns_Cis_Scnd_Annt_Sex_Cde = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Scnd_Annt_Sex_Cde", "CIS-SCND-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        pnd_Cref_Trns_Cis_Tiaa_Nbr = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10);
        pnd_Cref_Trns_Cis_Cert_Nbr = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10);
        pnd_Cref_Trns_Cis_Cref_Doi_N = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Cref_Doi_N", "CIS-CREF-DOI-N", FieldType.NUMERIC, 8);

        pnd_Cref_Trns__R_Field_26 = pnd_Cref_Trns.newGroupInGroup("pnd_Cref_Trns__R_Field_26", "REDEFINE", pnd_Cref_Trns_Cis_Cref_Doi_N);
        pnd_Cref_Trns_Cis_Cref_Doi = pnd_Cref_Trns__R_Field_26.newFieldInGroup("pnd_Cref_Trns_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.NUMERIC, 8);
        pnd_Cref_Trns_Cis_Cor_Cntrct_Payee_Cde = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Cref_Trns_Cis_Product_Code = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Product_Code", "CIS-PRODUCT-CODE", FieldType.STRING, 1);
        pnd_Cref_Trns_Cis_Annty_Option = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 2);
        pnd_Cref_Trns_Cis_Sg_Text_Udf_3 = pnd_Cref_Trns.newFieldInGroup("pnd_Cref_Trns_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", FieldType.STRING, 10);

        pnd_Mdo_Info = localVariables.newGroupInRecord("pnd_Mdo_Info", "#MDO-INFO");
        pnd_Mdo_Info_Cis_Rqst_Id = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8);
        pnd_Mdo_Info_Cis_Function_Cde = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Function_Cde", "CIS-FUNCTION-CDE", FieldType.STRING, 3);
        pnd_Mdo_Info_Cis_Pin_Nbr = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Mdo_Info_Cis_Frst_Annt_Ssn = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Frst_Annt_Ssn", "CIS-FRST-ANNT-SSN", FieldType.NUMERIC, 9);

        pnd_Mdo_Info__R_Field_27 = pnd_Mdo_Info.newGroupInGroup("pnd_Mdo_Info__R_Field_27", "REDEFINE", pnd_Mdo_Info_Cis_Frst_Annt_Ssn);
        pnd_Mdo_Info_Cis_Frst_Annt_Ssn_A = pnd_Mdo_Info__R_Field_27.newFieldInGroup("pnd_Mdo_Info_Cis_Frst_Annt_Ssn_A", "CIS-FRST-ANNT-SSN-A", FieldType.STRING, 
            9);
        pnd_Mdo_Info_Cis_Frst_Annt_Prfx = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Frst_Annt_Prfx", "CIS-FRST-ANNT-PRFX", FieldType.STRING, 8);
        pnd_Mdo_Info_Cis_Frst_Annt_Frst_Nme = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Frst_Annt_Frst_Nme", "CIS-FRST-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Mdo_Info_Cis_Frst_Annt_Mid_Nme = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Frst_Annt_Mid_Nme", "CIS-FRST-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Mdo_Info_Cis_Frst_Annt_Lst_Nme = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Frst_Annt_Lst_Nme", "CIS-FRST-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Mdo_Info_Cis_Frst_Annt_Sffx = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Frst_Annt_Sffx", "CIS-FRST-ANNT-SFFX", FieldType.STRING, 8);
        pnd_Mdo_Info_Cis_Frst_Annt_Sex_Cde = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Frst_Annt_Sex_Cde", "CIS-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        pnd_Mdo_Info_Cis_Frst_Annt_Dob_A = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Frst_Annt_Dob_A", "CIS-FRST-ANNT-DOB-A", FieldType.STRING, 8);

        pnd_Mdo_Info__R_Field_28 = pnd_Mdo_Info.newGroupInGroup("pnd_Mdo_Info__R_Field_28", "REDEFINE", pnd_Mdo_Info_Cis_Frst_Annt_Dob_A);
        pnd_Mdo_Info_Cis_Frst_Annt_Dob = pnd_Mdo_Info__R_Field_28.newFieldInGroup("pnd_Mdo_Info_Cis_Frst_Annt_Dob", "CIS-FRST-ANNT-DOB", FieldType.NUMERIC, 
            8);
        pnd_Mdo_Info_Cis_Tiaa_Nbr = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10);
        pnd_Mdo_Info_Cis_Tiaa_Doi_N = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Tiaa_Doi_N", "CIS-TIAA-DOI-N", FieldType.NUMERIC, 8);

        pnd_Mdo_Info__R_Field_29 = pnd_Mdo_Info.newGroupInGroup("pnd_Mdo_Info__R_Field_29", "REDEFINE", pnd_Mdo_Info_Cis_Tiaa_Doi_N);
        pnd_Mdo_Info_Cis_Tiaa_Doi = pnd_Mdo_Info__R_Field_29.newFieldInGroup("pnd_Mdo_Info_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.NUMERIC, 8);
        pnd_Mdo_Info_Cis_Cor_Cntrct_Status_Cde = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Cor_Cntrct_Status_Cde", "CIS-COR-CNTRCT-STATUS-CDE", FieldType.STRING, 
            1);
        pnd_Mdo_Info_Cis_Cor_Cntrct_Status_Yr_Dte = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Cor_Cntrct_Status_Yr_Dte", "CIS-COR-CNTRCT-STATUS-YR-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Mdo_Info_Cis_Cor_Cntrct_Payee_Cde = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", FieldType.STRING, 
            2);
        pnd_Mdo_Info_Cis_Cert_Nbr = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10);
        pnd_Mdo_Info_Cis_Ownership_Cd = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Ownership_Cd", "CIS-OWNERSHIP-CD", FieldType.STRING, 1);
        pnd_Mdo_Info_Addr_Line1 = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Addr_Line1", "ADDR-LINE1", FieldType.STRING, 35);
        pnd_Mdo_Info_Addr_Line2 = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Addr_Line2", "ADDR-LINE2", FieldType.STRING, 35);
        pnd_Mdo_Info_Addr_Line3 = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Addr_Line3", "ADDR-LINE3", FieldType.STRING, 35);
        pnd_Mdo_Info_Addr_Line4 = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Addr_Line4", "ADDR-LINE4", FieldType.STRING, 35);
        pnd_Mdo_Info_Addr_Line5 = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Addr_Line5", "ADDR-LINE5", FieldType.STRING, 35);
        pnd_Mdo_Info_Addr_Line6 = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Addr_Line6", "ADDR-LINE6", FieldType.STRING, 35);
        pnd_Mdo_Info_Cis_Zip_Code_Ia = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Zip_Code_Ia", "CIS-ZIP-CODE-IA", FieldType.STRING, 9);
        pnd_Mdo_Info_Cis_Rqst_Id_Key = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 35);

        pnd_Mdo_Info__R_Field_30 = pnd_Mdo_Info.newGroupInGroup("pnd_Mdo_Info__R_Field_30", "REDEFINE", pnd_Mdo_Info_Cis_Rqst_Id_Key);
        pnd_Mdo_Info_Cis_Rqst_Pin = pnd_Mdo_Info__R_Field_30.newFieldInGroup("pnd_Mdo_Info_Cis_Rqst_Pin", "CIS-RQST-PIN", FieldType.NUMERIC, 12);
        pnd_Mdo_Info_Cis_Rqst_Rest = pnd_Mdo_Info__R_Field_30.newFieldInGroup("pnd_Mdo_Info_Cis_Rqst_Rest", "CIS-RQST-REST", FieldType.STRING, 23);
        pnd_Mdo_Info_Cis_Sg_Text_Udf_3 = pnd_Mdo_Info.newFieldInGroup("pnd_Mdo_Info_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", FieldType.STRING, 10);

        cth_Out_Record = localVariables.newGroupInRecord("cth_Out_Record", "CTH-OUT-RECORD");
        cth_Out_Record_Cth_Rqst_Id_Key = cth_Out_Record.newFieldInGroup("cth_Out_Record_Cth_Rqst_Id_Key", "CTH-RQST-ID-KEY", FieldType.STRING, 35);
        cth_Out_Record_Cth_Tiaa_Nbr = cth_Out_Record.newFieldInGroup("cth_Out_Record_Cth_Tiaa_Nbr", "CTH-TIAA-NBR", FieldType.STRING, 10);
        cth_Out_Record_Cth_Cert_Nbr = cth_Out_Record.newFieldInGroup("cth_Out_Record_Cth_Cert_Nbr", "CTH-CERT-NBR", FieldType.STRING, 10);
        cth_Out_Record_Cth_Status = cth_Out_Record.newFieldInGroup("cth_Out_Record_Cth_Status", "CTH-STATUS", FieldType.STRING, 50);
        cth_Out_Record_Cth_Error_Message = cth_Out_Record.newFieldInGroup("cth_Out_Record_Cth_Error_Message", "CTH-ERROR-MESSAGE", FieldType.STRING, 256);
        pnd_Init_Rqst_Id = localVariables.newFieldInRecord("pnd_Init_Rqst_Id", "#INIT-RQST-ID", FieldType.STRING, 8);
        pnd_Next_Rqst_Id = localVariables.newFieldInRecord("pnd_Next_Rqst_Id", "#NEXT-RQST-ID", FieldType.STRING, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 5);
        pnd_Frst_Time = localVariables.newFieldInRecord("pnd_Frst_Time", "#FRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Cor_Error = localVariables.newFieldInRecord("pnd_Cor_Error", "#COR-ERROR", FieldType.BOOLEAN, 1);
        pnd_Mdo_Cntr_Type_S = localVariables.newFieldInRecord("pnd_Mdo_Cntr_Type_S", "#MDO-CNTR-TYPE-S", FieldType.BOOLEAN, 1);
        pnd_Bene_In_Plan = localVariables.newFieldInRecord("pnd_Bene_In_Plan", "#BENE-IN-PLAN", FieldType.BOOLEAN, 1);
        pnd_On_Cor = localVariables.newFieldInRecord("pnd_On_Cor", "#ON-COR", FieldType.BOOLEAN, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Ssn = localVariables.newFieldInRecord("pnd_Ssn", "#SSN", FieldType.NUMERIC, 11);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 30);
        pnd_Short_Name = localVariables.newFieldInRecord("pnd_Short_Name", "#SHORT-NAME", FieldType.STRING, 15);
        pnd_C_Type = localVariables.newFieldInRecord("pnd_C_Type", "#C-TYPE", FieldType.STRING, 4);
        pnd_Idat_A = localVariables.newFieldInRecord("pnd_Idat_A", "#IDAT-A", FieldType.STRING, 10);
        pnd_Error_Msg_Trunc = localVariables.newFieldInRecord("pnd_Error_Msg_Trunc", "#ERROR-MSG-TRUNC", FieldType.STRING, 45);
        pnd_Dat = localVariables.newFieldInRecord("pnd_Dat", "#DAT", FieldType.NUMERIC, 8);

        pnd_Dat__R_Field_31 = localVariables.newGroupInRecord("pnd_Dat__R_Field_31", "REDEFINE", pnd_Dat);
        pnd_Dat_Pnd_D_Yyyy = pnd_Dat__R_Field_31.newFieldInGroup("pnd_Dat_Pnd_D_Yyyy", "#D-YYYY", FieldType.STRING, 4);
        pnd_Dat_Pnd_D_Mm = pnd_Dat__R_Field_31.newFieldInGroup("pnd_Dat_Pnd_D_Mm", "#D-MM", FieldType.STRING, 2);
        pnd_Dat_Pnd_D_Dd = pnd_Dat__R_Field_31.newFieldInGroup("pnd_Dat_Pnd_D_Dd", "#D-DD", FieldType.STRING, 2);
        pnd_Dob_A = localVariables.newFieldInRecord("pnd_Dob_A", "#DOB-A", FieldType.STRING, 10);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.NUMERIC, 8);

        pnd_Dob__R_Field_32 = localVariables.newGroupInRecord("pnd_Dob__R_Field_32", "REDEFINE", pnd_Dob);
        pnd_Dob_Pnd_Dob_Yyyy = pnd_Dob__R_Field_32.newFieldInGroup("pnd_Dob_Pnd_Dob_Yyyy", "#DOB-YYYY", FieldType.STRING, 4);
        pnd_Dob_Pnd_Dob_Mm = pnd_Dob__R_Field_32.newFieldInGroup("pnd_Dob_Pnd_Dob_Mm", "#DOB-MM", FieldType.STRING, 2);
        pnd_Dob_Pnd_Dob_Dd = pnd_Dob__R_Field_32.newFieldInGroup("pnd_Dob_Pnd_Dob_Dd", "#DOB-DD", FieldType.STRING, 2);
        pnd_Bene_Rqst_Id_Key = localVariables.newFieldInRecord("pnd_Bene_Rqst_Id_Key", "#BENE-RQST-ID-KEY", FieldType.STRING, 35);

        pnd_Bene_Rqst_Id_Key__R_Field_33 = localVariables.newGroupInRecord("pnd_Bene_Rqst_Id_Key__R_Field_33", "REDEFINE", pnd_Bene_Rqst_Id_Key);
        pnd_Bene_Rqst_Id_Key_Pnd_Bene_Rqst_Pin = pnd_Bene_Rqst_Id_Key__R_Field_33.newFieldInGroup("pnd_Bene_Rqst_Id_Key_Pnd_Bene_Rqst_Pin", "#BENE-RQST-PIN", 
            FieldType.NUMERIC, 12);
        pnd_Bene_Rqst_Id_Key_Pnd_Bene_Rqst_Rest = pnd_Bene_Rqst_Id_Key__R_Field_33.newFieldInGroup("pnd_Bene_Rqst_Id_Key_Pnd_Bene_Rqst_Rest", "#BENE-RQST-REST", 
            FieldType.STRING, 23);
        pnd_Scnd_Annt_Added = localVariables.newFieldInRecord("pnd_Scnd_Annt_Added", "#SCND-ANNT-ADDED", FieldType.BOOLEAN, 1);
        pnd_Ssn_Nbr = localVariables.newFieldInRecord("pnd_Ssn_Nbr", "#SSN-NBR", FieldType.NUMERIC, 9);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Rcrd_Type = localVariables.newFieldInRecord("pnd_Rcrd_Type", "#RCRD-TYPE", FieldType.NUMERIC, 2);
        pnd_Total_Tiaa = localVariables.newFieldInRecord("pnd_Total_Tiaa", "#TOTAL-TIAA", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Cref = localVariables.newFieldInRecord("pnd_Total_Cref", "#TOTAL-CREF", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tiaa_Scnd = localVariables.newFieldInRecord("pnd_Total_Tiaa_Scnd", "#TOTAL-TIAA-SCND", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Cref_Scnd = localVariables.newFieldInRecord("pnd_Total_Cref_Scnd", "#TOTAL-CREF-SCND", FieldType.PACKED_DECIMAL, 12);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ia = localVariables.newFieldInRecord("pnd_Total_Ia", "#TOTAL-IA", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpa = localVariables.newFieldInRecord("pnd_Total_Tpa", "#TOTAL-TPA", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpa_Omni = localVariables.newFieldInRecord("pnd_Total_Tpa_Omni", "#TOTAL-TPA-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpi_Omni = localVariables.newFieldInRecord("pnd_Total_Tpi_Omni", "#TOTAL-TPI-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipro = localVariables.newFieldInRecord("pnd_Total_Ipro", "#TOTAL-IPRO", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipro_Omni = localVariables.newFieldInRecord("pnd_Total_Ipro_Omni", "#TOTAL-IPRO-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipi_Omni = localVariables.newFieldInRecord("pnd_Total_Ipi_Omni", "#TOTAL-IPI-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Mdo = localVariables.newFieldInRecord("pnd_Total_Mdo", "#TOTAL-MDO", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Add_Tiaa_Scnd = localVariables.newFieldInRecord("pnd_Total_Add_Tiaa_Scnd", "#TOTAL-ADD-TIAA-SCND", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Add_Cref_Scnd = localVariables.newFieldInRecord("pnd_Total_Add_Cref_Scnd", "#TOTAL-ADD-CREF-SCND", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Add_Cntrct_Tiaa_Scnd = localVariables.newFieldInRecord("pnd_Total_Add_Cntrct_Tiaa_Scnd", "#TOTAL-ADD-CNTRCT-TIAA-SCND", FieldType.PACKED_DECIMAL, 
            12);
        pnd_Total_Add_Cntrct_Cref_Scnd = localVariables.newFieldInRecord("pnd_Total_Add_Cntrct_Cref_Scnd", "#TOTAL-ADD-CNTRCT-CREF-SCND", FieldType.PACKED_DECIMAL, 
            12);
        pnd_Ipro_Error = localVariables.newFieldInRecord("pnd_Ipro_Error", "#IPRO-ERROR", FieldType.NUMERIC, 9);
        pnd_Ipi_Error = localVariables.newFieldInRecord("pnd_Ipi_Error", "#IPI-ERROR", FieldType.NUMERIC, 9);
        pnd_Tpa_Error = localVariables.newFieldInRecord("pnd_Tpa_Error", "#TPA-ERROR", FieldType.NUMERIC, 9);
        pnd_Tpi_Error = localVariables.newFieldInRecord("pnd_Tpi_Error", "#TPI-ERROR", FieldType.NUMERIC, 9);
        pnd_Mdo_Error = localVariables.newFieldInRecord("pnd_Mdo_Error", "#MDO-ERROR", FieldType.NUMERIC, 9);
        pnd_Unkwn_Error = localVariables.newFieldInRecord("pnd_Unkwn_Error", "#UNKWN-ERROR", FieldType.NUMERIC, 9);
        pnd_Total_Errors = localVariables.newFieldInRecord("pnd_Total_Errors", "#TOTAL-ERRORS", FieldType.NUMERIC, 9);
        pnd_Sip_Errors = localVariables.newFieldInRecord("pnd_Sip_Errors", "#SIP-ERRORS", FieldType.NUMERIC, 9);
        pnd_Non_Sip_Errors = localVariables.newFieldInRecord("pnd_Non_Sip_Errors", "#NON-SIP-ERRORS", FieldType.NUMERIC, 9);
        pnd_Mdo_Tpa_Ipro_Errors = localVariables.newFieldInRecord("pnd_Mdo_Tpa_Ipro_Errors", "#MDO-TPA-IPRO-ERRORS", FieldType.NUMERIC, 9);
        cth_Out_Record_Written = localVariables.newFieldInRecord("cth_Out_Record_Written", "CTH-OUT-RECORD-WRITTEN", FieldType.NUMERIC, 9);
        pnd_Hold_Rqst_Id = localVariables.newFieldInRecord("pnd_Hold_Rqst_Id", "#HOLD-RQST-ID", FieldType.STRING, 8);
        pnd_At_Start = localVariables.newFieldInRecord("pnd_At_Start", "#AT-START", FieldType.PACKED_DECIMAL, 10);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Option_Table = localVariables.newFieldArrayInRecord("pnd_Option_Table", "#OPTION-TABLE", FieldType.STRING, 6, new DbsArrayController(1, 24));
        pnd_Tbl_Size = localVariables.newFieldInRecord("pnd_Tbl_Size", "#TBL-SIZE", FieldType.PACKED_DECIMAL, 3);
        pnd_Option_Table_2 = localVariables.newFieldInRecord("pnd_Option_Table_2", "#OPTION-TABLE-2", FieldType.STRING, 6);

        pnd_Option_Table_2__R_Field_34 = localVariables.newGroupInRecord("pnd_Option_Table_2__R_Field_34", "REDEFINE", pnd_Option_Table_2);
        pnd_Option_Table_2_Pnd_Option_Code = pnd_Option_Table_2__R_Field_34.newFieldInGroup("pnd_Option_Table_2_Pnd_Option_Code", "#OPTION-CODE", FieldType.STRING, 
            2);
        pnd_Option_Table_2_Pnd_Option_Yrs = pnd_Option_Table_2__R_Field_34.newFieldInGroup("pnd_Option_Table_2_Pnd_Option_Yrs", "#OPTION-YRS", FieldType.NUMERIC, 
            2);
        pnd_Option_Table_2_Pnd_Option_Code_Cor = pnd_Option_Table_2__R_Field_34.newFieldInGroup("pnd_Option_Table_2_Pnd_Option_Code_Cor", "#OPTION-CODE-COR", 
            FieldType.STRING, 2);
        pnd_Cis_Annty_Option = localVariables.newFieldInRecord("pnd_Cis_Annty_Option", "#CIS-ANNTY-OPTION", FieldType.STRING, 10);
        pnd_Tiaa_Trnsf_Option = localVariables.newFieldInRecord("pnd_Tiaa_Trnsf_Option", "#TIAA-TRNSF-OPTION", FieldType.STRING, 4);

        pnd_Tiaa_Trnsf_Option__R_Field_35 = localVariables.newGroupInRecord("pnd_Tiaa_Trnsf_Option__R_Field_35", "REDEFINE", pnd_Tiaa_Trnsf_Option);
        pnd_Tiaa_Trnsf_Option_Pnd_Trnsf_Option_Cde = pnd_Tiaa_Trnsf_Option__R_Field_35.newFieldInGroup("pnd_Tiaa_Trnsf_Option_Pnd_Trnsf_Option_Cde", "#TRNSF-OPTION-CDE", 
            FieldType.STRING, 2);
        pnd_Tiaa_Trnsf_Option_Pnd_Trnsf_Option_Yrs = pnd_Tiaa_Trnsf_Option__R_Field_35.newFieldInGroup("pnd_Tiaa_Trnsf_Option_Pnd_Trnsf_Option_Yrs", "#TRNSF-OPTION-YRS", 
            FieldType.NUMERIC, 2);
        pnd_Mdm_Cntrct_Universal_Data = localVariables.newFieldInRecord("pnd_Mdm_Cntrct_Universal_Data", "#MDM-CNTRCT-UNIVERSAL-DATA", FieldType.STRING, 
            150);

        pnd_Mdm_Cntrct_Universal_Data__R_Field_36 = localVariables.newGroupInRecord("pnd_Mdm_Cntrct_Universal_Data__R_Field_36", "REDEFINE", pnd_Mdm_Cntrct_Universal_Data);
        pnd_Mdm_Cntrct_Universal_Data_Pnd_Mdm_Cntrct_Universal_Arr = pnd_Mdm_Cntrct_Universal_Data__R_Field_36.newFieldArrayInGroup("pnd_Mdm_Cntrct_Universal_Data_Pnd_Mdm_Cntrct_Universal_Arr", 
            "#MDM-CNTRCT-UNIVERSAL-ARR", FieldType.STRING, 1, new DbsArrayController(1, 150));
        pnd_Doi = localVariables.newFieldInRecord("pnd_Doi", "#DOI", FieldType.NUMERIC, 8);

        pnd_Doi__R_Field_37 = localVariables.newGroupInRecord("pnd_Doi__R_Field_37", "REDEFINE", pnd_Doi);
        pnd_Doi_Pnd_Doi_Ccyy = pnd_Doi__R_Field_37.newFieldInGroup("pnd_Doi_Pnd_Doi_Ccyy", "#DOI-CCYY", FieldType.NUMERIC, 4);
        pnd_Doi_Pnd_Doi_Mm = pnd_Doi__R_Field_37.newFieldInGroup("pnd_Doi_Pnd_Doi_Mm", "#DOI-MM", FieldType.NUMERIC, 2);
        pnd_Doi_Pnd_Doi_Dd = pnd_Doi__R_Field_37.newFieldInGroup("pnd_Doi_Pnd_Doi_Dd", "#DOI-DD", FieldType.NUMERIC, 2);
        pnd_Icp_Contract = localVariables.newFieldInRecord("pnd_Icp_Contract", "#ICP-CONTRACT", FieldType.STRING, 2);
        pnd_Cor_Api_Pin = localVariables.newFieldInRecord("pnd_Cor_Api_Pin", "#COR-API-PIN", FieldType.NUMERIC, 12);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_Retry_Cnt = localVariables.newFieldInRecord("pnd_Retry_Cnt", "#RETRY-CNT", FieldType.NUMERIC, 3);
        intvl = localVariables.newFieldInRecord("intvl", "INTVL", FieldType.INTEGER, 1);
        reqid = localVariables.newFieldInRecord("reqid", "REQID", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bene_Update.reset();

        ldaCisl400.initializeValues();
        ldaCispartv.initializeValues();
        ldaCisl5000.initializeValues();

        localVariables.reset();
        pnd_Cis_Bene_Super_2.setInitialValue("1XXXXXXXX");
        pnd_Option_Table.getValue(1).setInitialValue("SL0001");
        pnd_Option_Table.getValue(2).setInitialValue("IR0002");
        pnd_Option_Table.getValue(3).setInitialValue("LH0003");
        pnd_Option_Table.getValue(4).setInitialValue("LF0004");
        pnd_Option_Table.getValue(5).setInitialValue("SL1005");
        pnd_Option_Table.getValue(6).setInitialValue("SL2006");
        pnd_Option_Table.getValue(7).setInitialValue("J 0007");
        pnd_Option_Table.getValue(8).setInitialValue("J 1008");
        pnd_Option_Table.getValue(9).setInitialValue("SL1509");
        pnd_Option_Table.getValue(10).setInitialValue("J 2010");
        pnd_Option_Table.getValue(11).setInitialValue("LF2011");
        pnd_Option_Table.getValue(12).setInitialValue("LH2012");
        pnd_Option_Table.getValue(13).setInitialValue("J 1513");
        pnd_Option_Table.getValue(14).setInitialValue("LF1514");
        pnd_Option_Table.getValue(15).setInitialValue("LH1515");
        pnd_Option_Table.getValue(16).setInitialValue("LF1016");
        pnd_Option_Table.getValue(17).setInitialValue("LH1017");
        pnd_Option_Table.getValue(18).setInitialValue("LT0057");
        pnd_Option_Table.getValue(19).setInitialValue("LT1054");
        pnd_Option_Table.getValue(20).setInitialValue("LT1555");
        pnd_Option_Table.getValue(21).setInitialValue("LT2056");
        pnd_Option_Table.getValue(22).setInitialValue("AC0021");
        pnd_Tbl_Size.setInitialValue(24);
        intvl.setInitialValue(1);
        reqid.setInitialValue("MYREQID");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisb400() throws Exception
    {
        super("Cisb400");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CISB400", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* ***********************************************************************
        //*  MAIN LOGIC SECTION                                                   *
        //* ***********************************************************************
        //*  JRB2
        //*  JRB2    /* PINE <<
        //*  FORMAT(2) PS=60 LS=132                                                                                                                                       //Natural: FORMAT PS = 60 LS = 80;//Natural: FORMAT ( 1 ) PS = 60 LS = 132
        //*  FORMAT(3) PS=55 LS=133                                                                                                                                       //Natural: FORMAT ( 2 ) PS = 60 LS = 138
        //*  FORMAT(4) PS=55 LS=133                                                                                                                                       //Natural: FORMAT ( 3 ) PS = 55 LS = 139
        //*                                                                                                                                                               //Natural: FORMAT ( 4 ) PS = 55 LS = 139
        //*  BE. RAC PROJECT
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*  JRB2
        //*  JRB2
        //*  JRB2
        //*  JRB2
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE LEFT *PROGRAM 56T 'COR FEED ERROR REPORT' 110T 'RUN DATE:' *DATU / 54T 'CONSOLIDATED ISSUE SYSTEM' / 58T 'FOR SIP CONTRACTS' /
        //*  JRB2
        //*  JRB2
        //*  JRB2
        //*  JRB2
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 4 ) TITLE LEFT *PROGRAM 56T 'COR FEED ERROR REPORT' 110T 'RUN DATE:' *DATU / 54T 'CONSOLIDATED ISSUE SYSTEM' / 50T 'FOR MDO, TPA, AND IPRO CONTRACTS' /
        //* **
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WORK-FILE1
        while (condition(getWorkFiles().read(1, pnd_Work_File1)))
        {
            pnd_At_Start.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #AT-START
            if (condition(pnd_At_Start.equals(1)))                                                                                                                        //Natural: IF #AT-START = 1
            {
                pnd_Hold_Rqst_Id.setValue(pnd_Work_File1_Cis_Rqst_Id);                                                                                                    //Natural: ASSIGN #HOLD-RQST-ID := #WORK-FILE1.CIS-RQST-ID
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Hold_Rqst_Id.notEquals(pnd_Work_File1_Cis_Rqst_Id)))                                                                                        //Natural: IF #HOLD-RQST-ID NE #WORK-FILE1.CIS-RQST-ID
            {
                //* *IF NOT #FIRST-TIME
                //* *   #FIRST-TIME := TRUE
                if (condition(pnd_Total_Tiaa.greater(getZero())))                                                                                                         //Natural: IF #TOTAL-TIAA > 0
                {
                                                                                                                                                                          //Natural: PERFORM TOTAL-COR-FEED
                    sub_Total_Cor_Feed();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Total_Tiaa.reset();                                                                                                                                   //Natural: RESET #TOTAL-TIAA #TOTAL #TOTAL-CREF #TOTAL-TIAA-SCND #TOTAL-CREF-SCND
                pnd_Total.reset();
                pnd_Total_Cref.reset();
                pnd_Total_Tiaa_Scnd.reset();
                pnd_Total_Cref_Scnd.reset();
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *END-IF
                pnd_Hold_Rqst_Id.setValue(pnd_Work_File1_Cis_Rqst_Id);                                                                                                    //Natural: ASSIGN #HOLD-RQST-ID := #WORK-FILE1.CIS-RQST-ID
                //*  BIP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Bene_In_Plan.setValue(false);                                                                                                                             //Natural: ASSIGN #BENE-IN-PLAN := FALSE
            //*  BIP
            //*  BIP
            if (condition(pnd_Work_File1_Cis_Decedent_Contract.greater(" ")))                                                                                             //Natural: IF #WORK-FILE1.CIS-DECEDENT-CONTRACT GT ' '
            {
                pnd_Bene_In_Plan.setValue(true);                                                                                                                          //Natural: ASSIGN #BENE-IN-PLAN := TRUE
                //*  BIP
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1669 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( #WORK-FILE1.CIS-RQST-ID = 'IA' AND #WORK-FILE1.CIS-COR-SYNC-IND = 'Y' )
            if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA") && pnd_Work_File1_Cis_Cor_Sync_Ind.equals("Y")))
            {
                decideConditionsMet1669++;
                                                                                                                                                                          //Natural: PERFORM IA-RTRN
                sub_Ia_Rtrn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Total_Ia.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #TOTAL-IA
                //* *        PERFORM WRITE-REPORT
            }                                                                                                                                                             //Natural: WHEN ( #WORK-FILE1.CIS-RQST-ID = 'TPA' OR = 'TPI' ) AND ( #WORK-FILE1.CIS-COR-SYNC-IND = 'Y' OR = 'S' )
            else if (condition(((pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPI")) && (pnd_Work_File1_Cis_Cor_Sync_Ind.equals("Y") 
                || pnd_Work_File1_Cis_Cor_Sync_Ind.equals("S")))))
            {
                decideConditionsMet1669++;
                                                                                                                                                                          //Natural: PERFORM TPA-RTRN
                sub_Tpa_Rtrn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ERRRPT KG
                if (condition(pnd_Cor_Error.getBoolean()))                                                                                                                //Natural: IF #COR-ERROR
                {
                    //*  ERRRPT KG
                    pnd_Cor_Error.reset();                                                                                                                                //Natural: RESET #COR-ERROR
                    //*  (1326)                            /* ERRRPT KG
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Work_File1_Cis_Cor_Sync_Ind.equals("Y")))                                                                                           //Natural: IF #WORK-FILE1.CIS-COR-SYNC-IND = 'Y'
                    {
                        pnd_Total_Tpa.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-TPA
                    }                                                                                                                                                     //Natural: END-IF
                    //*  HK >>>>
                    if (condition(pnd_Work_File1_Cis_Cor_Sync_Ind.equals("S")))                                                                                           //Natural: IF #WORK-FILE1.CIS-COR-SYNC-IND = 'S'
                    {
                        if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("TPA")))                                                                                          //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'TPA'
                        {
                            pnd_Total_Tpa_Omni.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TOTAL-TPA-OMNI
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("TPI")))                                                                                          //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'TPI'
                        {
                            pnd_Total_Tpi_Omni.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TOTAL-TPI-OMNI
                        }                                                                                                                                                 //Natural: END-IF
                        //*  (1338)                         /* HK <<<
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (1326)                            /* ERRRPT KG
                }                                                                                                                                                         //Natural: END-IF
                //* *        PERFORM WRITE-REPORT
            }                                                                                                                                                             //Natural: WHEN ( #WORK-FILE1.CIS-RQST-ID = 'IPRO' OR = 'IPI' ) AND ( #WORK-FILE1.CIS-COR-SYNC-IND = 'Y' OR = 'S' )
            else if (condition(((pnd_Work_File1_Cis_Rqst_Id.equals("IPRO") || pnd_Work_File1_Cis_Rqst_Id.equals("IPI")) && (pnd_Work_File1_Cis_Cor_Sync_Ind.equals("Y") 
                || pnd_Work_File1_Cis_Cor_Sync_Ind.equals("S")))))
            {
                decideConditionsMet1669++;
                                                                                                                                                                          //Natural: PERFORM IPRO-RTRN
                sub_Ipro_Rtrn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ERRRPT KG
                if (condition(pnd_Cor_Error.getBoolean()))                                                                                                                //Natural: IF #COR-ERROR
                {
                    //*  ERRRPT KG
                    pnd_Cor_Error.reset();                                                                                                                                //Natural: RESET #COR-ERROR
                    //*  (1364)                              /* ERRRPT KG
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Work_File1_Cis_Cor_Sync_Ind.equals("Y")))                                                                                           //Natural: IF #WORK-FILE1.CIS-COR-SYNC-IND = 'Y'
                    {
                        pnd_Total_Ipro.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOTAL-IPRO
                    }                                                                                                                                                     //Natural: END-IF
                    //*  HK >>>>
                    if (condition(pnd_Work_File1_Cis_Cor_Sync_Ind.equals("S")))                                                                                           //Natural: IF #WORK-FILE1.CIS-COR-SYNC-IND = 'S'
                    {
                        if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IPRO")))                                                                                         //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IPRO'
                        {
                            pnd_Total_Ipro_Omni.nadd(1);                                                                                                                  //Natural: ADD 1 TO #TOTAL-IPRO-OMNI
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IPI")))                                                                                          //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IPI'
                        {
                            pnd_Total_Ipi_Omni.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TOTAL-IPI-OMNI
                        }                                                                                                                                                 //Natural: END-IF
                        //*  (1376)                         /* ERRRPT KG
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (1364)                            /* HK <<<<
                    //*  090606
                }                                                                                                                                                         //Natural: END-IF
                //* *        PERFORM WRITE-REPORT
            }                                                                                                                                                             //Natural: WHEN ( #WORK-FILE1.CIS-RQST-ID = MASK ( 'MDO' ) AND #WORK-FILE1.CIS-COR-SYNC-IND = 'Y' OR = 'S' )
            else if (condition((DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'") && (pnd_Work_File1_Cis_Cor_Sync_Ind.equals("Y") || pnd_Work_File1_Cis_Cor_Sync_Ind.equals("S")))))
            {
                decideConditionsMet1669++;
                                                                                                                                                                          //Natural: PERFORM MDO-RTRN
                sub_Mdo_Rtrn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ERRRPT KG
                if (condition(pnd_Cor_Error.getBoolean()))                                                                                                                //Natural: IF #COR-ERROR
                {
                    //*  ERRRPT KG
                    pnd_Cor_Error.reset();                                                                                                                                //Natural: RESET #COR-ERROR
                    //*  (1402)                             /* ERRRPT KG
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  MOVED SO ERRS NOT IN TOTAL
                    pnd_Total_Mdo.nadd(1);                                                                                                                                //Natural: ADD 1 TO #TOTAL-MDO
                    //*  (1402)                           /* ERRRPT KG
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
                //*  (1308)
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            pnd_Total.compute(new ComputeParameters(false, pnd_Total), pnd_Total_Ia.add(pnd_Total_Mdo));                                                                  //Natural: ASSIGN #TOTAL := #TOTAL-IA + #TOTAL-MDO
            if (condition(pnd_Total_Tiaa.greater(getZero())))                                                                                                             //Natural: IF #TOTAL-TIAA > 0
            {
                                                                                                                                                                          //Natural: PERFORM TOTAL-COR-FEED
                sub_Total_Cor_Feed();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  CLOSE MQ.  BE1 RACP
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        getReports().write(0, "LEAVING CISB400 :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                   //Natural: WRITE 'LEAVING CISB400 :' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        //*  ERRRPT KG
                                                                                                                                                                          //Natural: PERFORM ERROR-TOTALS
        sub_Error_Totals();
        if (condition(Global.isEscape())) {return;}
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-RTRN
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TPA-RTRN
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IPRO-RTRN
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TIAA-INFO
        //*        'PIN:' #MDMA300.#NEW-PIN
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TIAA-SCND-INFO
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CREF-INFO
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CREF-SCND-INFO
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TRNSF-CREF-SCND-INFO
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDO-RTRN
        //*        'PIN:' #MDMA300.#NEW-PIN
        //*          'PIN:' #MDMA300.#NEW-PIN
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-SCND-EXISTS
        //*    #MDMA300.#RECORD-TYPE :=  01
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
        //*  CISL400.CIS-FUNCTION-CDE 3X
        //*  #MDMA300.#FUNCTION-CODE 3X
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-REPORT
        //*  #ERROR-MSG-TRUNC := CORA805.#RETURN-TEXT
        //*  #ERROR-MSG-TRUNC := #MDMA300.#RETURN-TEXT
        //*  COMPRESS  SUBSTRING(CISL400.CIS-ANNT-FRST-NME,1,1)
        //*  CISL400.CIS-ANNT-LST-NME INTO #SHORT-NAME LEAVING
        //*  CISL400.CIS-FUNCTION-CDE 1X
        //*  #MDMA300.#FUNCTION-CODE 1X
        //*   CORA805.#RETURN-CODE
        //*  #MDMA300.#RETURN-CODE
        //*      '/FUNC'          #MDMA300.#FUNCTION-CODE
        //*      'ERR/CODE'       #MDMA300.#RETURN-CODE
        //*      '/FUNC'          #MDMA300.#FUNCTION-CODE
        //*      'ERR/CODE'       #MDMA300.#RETURN-CODE
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-TOTALS
        //* *****************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-COR-FEED
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-OPTION-CODE
        //*  WHEN #WORK-FILE1.CIS-RQST-ID = 'TPA'
        //*  WHEN #WORK-FILE1.CIS-RQST-ID = 'IPRO'
        //*   FOR #I = 1 TO 20
        //*  HK
        //*  ALL THE OMNI ISSUED CONTRACT WILL BE CALLED BY AN API FOR TPA
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-COR-API
        //*  #MDMA300.#REQUESTOR                    := 'CIS'
        //*    #MDMA300.#FUNCTION-CODE := '003'
        //*      #MDMA300.#SOC-SEC-NBR   := #MDO-INFO.CIS-FRST-ANNT-SSN
        //*      #MDMA300.#PREFIX        := #MDO-INFO.CIS-FRST-ANNT-PRFX
        //*      #MDMA300.#FIRST-NAME    := #MDO-INFO.CIS-FRST-ANNT-FRST-NME
        //*      #MDMA300.#MIDDLE-NAME   := #MDO-INFO.CIS-FRST-ANNT-MID-NME
        //*      #MDMA300.#LAST-NAME     := #MDO-INFO.CIS-FRST-ANNT-LST-NME
        //*      #MDMA300.#SUFFIX        := #MDO-INFO.CIS-FRST-ANNT-SFFX
        //*      #MDMA300.#DATE-OF-BIRTH := #MDO-INFO.CIS-FRST-ANNT-DOB
        //*        #MDMA300.#SEX-CODE := 'U'
        //*        #MDMA300.#SEX-CODE     := #MDO-INFO.CIS-FRST-ANNT-SEX-CDE
        //*      #MDMA300.#SOC-SEC-NBR   := #TIAA-INFO.CIS-FRST-ANNT-SSN
        //*      #MDMA300.#PREFIX        := #TIAA-INFO.CIS-FRST-ANNT-PRFX
        //*      #MDMA300.#FIRST-NAME    := #TIAA-INFO.CIS-FRST-ANNT-FRST-NME
        //*      #MDMA300.#MIDDLE-NAME   := #TIAA-INFO.CIS-FRST-ANNT-MID-NME
        //*      #MDMA300.#LAST-NAME     := #TIAA-INFO.CIS-FRST-ANNT-LST-NME
        //*      #MDMA300.#SUFFIX        := #TIAA-INFO.CIS-FRST-ANNT-SFFX
        //*      #MDMA300.#DATE-OF-BIRTH := #TIAA-INFO.CIS-FRST-ANNT-DOB
        //*        #MDMA300.#SEX-CODE := 'U'
        //*        #MDMA300.#SEX-CODE     := #TIAA-INFO.CIS-FRST-ANNT-SEX-CDE
        //*    #MDMA300.#RECORD-TYPE            := 01
        //*  #MDMA300.#RECORD-TYPE                := 01
        //*  #MDMA300.#PIN                          := CISL400.CIS-PIN-NBR
        //*  #MDMA300.#CONTRACT-TABLE-COUNT         := 1
        //*  #MDMA300.#CONTRACT(1)                  := CISL400.CIS-TIAA-NBR
        //*  #MDMA300.#ISSUE-DATE(1)                := CISL400.CIS-DOI
        //*  #MDMA300.#STATUS-CODE (1)              := 'H'
        //*  #MDMA300.#STATUS-YEAR (1)              := #DOI-CCYY
        //*  #MDMA300.#PAYEE-CODE (1)           := CISL400.CIS-COR-CNTRCT-PAYEE-CDE
        //*    #MDMA300.#IA-OPTION-CODE(1)
        //*  #MDMA300.#IA-PRODUCT-CODE(1) := CISL400.CIS-PRODUCT-CODE
        //*  #MDMA300.#IA-OPTION-CODE(1)  := CISL400.CIS-ANNTY-OPTION
        //*  #MDMA300.#PAYEE-CODE(1) := '01'
        //*  #MDMA300.#CNTRCT-UNIVERSAL-DATA (1)    := #MDM-CNTRCT-UNIVERSAL-DATA
        //*    #MDMA300.#O-RETURN-SECTION
        //*    #MDMA300.#O-DATA-SECTION (1:1361)
        //*  BIP NEW SUBROUTINE START HERE...
        //* ****************************************
        //*  DEFINE SUBROUTINE MOVE-MDMA300-TO-MDMA370                /* PINE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-MDMA301-TO-MDMA371
        //*  #MDMA370.#SSN-OR-TIN-IND       := #WORK-FILE1.CIS-SSN-TIN-IND
        //*  #MDMA370.#DECEDENT-CONTRACT(1) := #WORK-FILE1.CIS-DECEDENT-CONTRACT
        //*  #MDMA370.#ACCEPTANCE-IND   (1) := 'N'
        //*  #MDMA370.#RELATIONSHIP-TO-DECEDENT (1) :=
        //*  #MDMA370.#REQUESTOR             := #MDMA300.#REQUESTOR
        //*  #MDMA370.#FUNCTION-CODE         := #MDMA300.#FUNCTION-CODE
        //*  #MDMA370.#SOC-SEC-NBR           := #MDMA300.#SOC-SEC-NBR
        //*  #MDMA370.#PREFIX                := #MDMA300.#PREFIX
        //*  #MDMA370.#FIRST-NAME            := #MDMA300.#FIRST-NAME
        //*  #MDMA370.#MIDDLE-NAME           := #MDMA300.#MIDDLE-NAME
        //*  #MDMA370.#LAST-NAME             := #MDMA300.#LAST-NAME
        //*  #MDMA370.#SUFFIX                := #MDMA300.#SUFFIX
        //*  #MDMA370.#DATE-OF-BIRTH         := #MDMA300.#DATE-OF-BIRTH
        //*  #MDMA370.#SEX-CODE              := #MDMA300.#SEX-CODE
        //*  #MDMA370.#RECORD-TYPE           := #MDMA300.#RECORD-TYPE
        //*  #MDMA370.#FUNCTION-CODE         := #MDMA300.#FUNCTION-CODE
        //*  #MDMA370.#PIN                   := #MDMA300.#PIN
        //*  #MDMA370.#CONTRACT-TABLE-COUNT  := #MDMA300.#CONTRACT-TABLE-COUNT
        //*  #MDMA370.#CONTRACT         (1)  := #MDMA300.#CONTRACT         (1)
        //*  #MDMA370.#ISSUE-DATE       (1)  := #MDMA300.#ISSUE-DATE       (1)
        //*  #MDMA370.#STATUS-CODE      (1)  := #MDMA300.#STATUS-CODE      (1)
        //*  #MDMA370.#STATUS-YEAR      (1)  := #MDMA300.#STATUS-YEAR      (1)
        //*  #MDMA370.#PAYEE-CODE       (1)  := #MDMA300.#PAYEE-CODE       (1)
        //*  #MDMA370.#IA-PRODUCT-CODE  (1)  := #MDMA300.#IA-PRODUCT-CODE  (1)
        //*  #MDMA370.#IA-OPTION-CODE   (1)  := #MDMA300.#IA-OPTION-CODE   (1)
        //*  #MDMA370.#CREF-CONTRACT    (1)  := #MDMA300.#CREF-CONTRACT    (1)
        //*  #MDMA370.#CREF-ISSUED-IND  (1)  := #MDMA300.#CREF-ISSUED-IND  (1)
        //*  #MDMA370.#DA-OWNERSHIP-CODE(1)  := #MDMA300.#DA-OWNERSHIP-CODE(1)
        //*  #MDMA300.#CNTRCT-UNIVERSAL-DATA(1)
        //*  BIP NEW SUBROUTINE ENDS HERE
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-COR-BY-SSN
        //*  #COR-API-PIN := #O-PIN
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //* *======================================================================
        //*  RETRY ERROR LOGIC
        //* *======================================================================
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  PINE >>
        //*  FOR PIN EXPANSION WE  HAD TO REPLACE THE ITERATIONS OF MDMN370A WITH
        //*  MDMN371A AND MDMA370 WITH MDMA371. THUS, FOR THE SUBROUTINE
        //*  'DISPLAY-MDMA370' WE HAVE MODIFIED ALL THE REFERENCES OF 370 TO 371.
        //*  SINCE RETAINING OF OLD LINES OF CODE WERE INCREASING THE SIZE WE HAVE
        //*  NOT RETAINED THEM. THE CHANGES WERE MADE AS SHOWN BELOW AND IF A
        //*  FALLBACK IS REQUIRED, PLEASE CHANGE THE 371'S TO 370.
        //*  WRITE '=' #MDMA370.#REQUESTOR / HAS BEEN MODIFIED TO
        //*  WRITE '=' #MDMA371.#REQUESTOR /
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-MDMA371
    }
    private void sub_Ia_Rtrn() throws Exception                                                                                                                           //Natural: IA-RTRN
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        if (condition(pnd_Work_File1_Cis_Status_Cd.equals("I") && pnd_Work_File1_Cis_Cor_Sync_Ind.equals("Y")))                                                           //Natural: IF #WORK-FILE1.CIS-STATUS-CD = 'I' AND #WORK-FILE1.CIS-COR-SYNC-IND = 'Y'
        {
            if (condition(pnd_Work_File1_Cis_Cntrct_Type.equals("B")))                                                                                                    //Natural: IF #WORK-FILE1.CIS-CNTRCT-TYPE = 'B'
            {
                pnd_Tiaa_Info.reset();                                                                                                                                    //Natural: RESET #TIAA-INFO CISL400
                ldaCisl400.getCisl400().reset();
                pnd_Tiaa_Info.setValuesByName(pnd_Work_File1);                                                                                                            //Natural: MOVE BY NAME #WORK-FILE1 TO #TIAA-INFO
                ldaCisl400.getCisl400().setValuesByName(pnd_Tiaa_Info);                                                                                                   //Natural: MOVE BY NAME #TIAA-INFO TO CISL400
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-INFO
                sub_Write_Tiaa_Info();
                if (condition(Global.isEscape())) {return;}
                pnd_Cref_Info.reset();                                                                                                                                    //Natural: RESET #CREF-INFO CISL400
                ldaCisl400.getCisl400().reset();
                pnd_Cref_Info.setValuesByName(pnd_Work_File1);                                                                                                            //Natural: MOVE BY NAME #WORK-FILE1 TO #CREF-INFO
                ldaCisl400.getCisl400().setValuesByName(pnd_Cref_Info);                                                                                                   //Natural: MOVE BY NAME #CREF-INFO TO CISL400
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-INFO
                sub_Write_Cref_Info();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Work_File1_Cis_Scnd_Annt_Ssn.notEquals(getZero())))                                                                                     //Natural: IF #WORK-FILE1.CIS-SCND-ANNT-SSN NE 0
                {
                    pnd_Tiaa_Scnd_Info.reset();                                                                                                                           //Natural: RESET #TIAA-SCND-INFO CISL400 #SCND-ANNT-ADDED
                    ldaCisl400.getCisl400().reset();
                    pnd_Scnd_Annt_Added.reset();
                    pnd_Tiaa_Scnd_Info.setValuesByName(pnd_Work_File1);                                                                                                   //Natural: MOVE BY NAME #WORK-FILE1 TO #TIAA-SCND-INFO
                    ldaCisl400.getCisl400().setValuesByName(pnd_Tiaa_Scnd_Info);                                                                                          //Natural: MOVE BY NAME #TIAA-SCND-INFO TO CISL400
                    pnd_Ssn_Nbr.reset();                                                                                                                                  //Natural: RESET #SSN-NBR #FOUND #RCRD-TYPE
                    pnd_Found.reset();
                    pnd_Rcrd_Type.reset();
                    pnd_Ssn_Nbr.setValue(pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Ssn);                                                                                           //Natural: ASSIGN #SSN-NBR := #TIAA-SCND-INFO.CIS-SCND-ANNT-SSN
                                                                                                                                                                          //Natural: PERFORM CHECK-SCND-EXISTS
                    sub_Check_Scnd_Exists();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-SCND-INFO
                    sub_Write_Tiaa_Scnd_Info();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Cref_Scnd_Info.reset();                                                                                                                           //Natural: RESET #CREF-SCND-INFO CISL400
                    ldaCisl400.getCisl400().reset();
                    pnd_Cref_Scnd_Info.setValuesByName(pnd_Work_File1);                                                                                                   //Natural: MOVE BY NAME #WORK-FILE1 TO #CREF-SCND-INFO
                    ldaCisl400.getCisl400().setValuesByName(pnd_Cref_Scnd_Info);                                                                                          //Natural: MOVE BY NAME #CREF-SCND-INFO TO CISL400
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-SCND-INFO
                    sub_Write_Cref_Scnd_Info();
                    if (condition(Global.isEscape())) {return;}
                    //*  (1474)
                }                                                                                                                                                         //Natural: END-IF
                //*  (1456)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Cntrct_Type.equals("T")))                                                                                                    //Natural: IF #WORK-FILE1.CIS-CNTRCT-TYPE = 'T'
            {
                pnd_Tiaa_Info.reset();                                                                                                                                    //Natural: RESET #TIAA-INFO CISL400
                ldaCisl400.getCisl400().reset();
                pnd_Tiaa_Info.setValuesByName(pnd_Work_File1);                                                                                                            //Natural: MOVE BY NAME #WORK-FILE1 TO #TIAA-INFO
                ldaCisl400.getCisl400().setValuesByName(pnd_Tiaa_Info);                                                                                                   //Natural: MOVE BY NAME #TIAA-INFO TO CISL400
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-INFO
                sub_Write_Tiaa_Info();
                if (condition(Global.isEscape())) {return;}
                pnd_Tiaa_Scnd_Info.reset();                                                                                                                               //Natural: RESET #TIAA-SCND-INFO CISL400 #SCND-ANNT-ADDED
                ldaCisl400.getCisl400().reset();
                pnd_Scnd_Annt_Added.reset();
                pnd_Tiaa_Scnd_Info.setValuesByName(pnd_Work_File1);                                                                                                       //Natural: MOVE BY NAME #WORK-FILE1 TO #TIAA-SCND-INFO
                ldaCisl400.getCisl400().setValuesByName(pnd_Tiaa_Scnd_Info);                                                                                              //Natural: MOVE BY NAME #TIAA-SCND-INFO TO CISL400
                if (condition(pnd_Work_File1_Cis_Scnd_Annt_Ssn.notEquals(getZero())))                                                                                     //Natural: IF #WORK-FILE1.CIS-SCND-ANNT-SSN NE 0
                {
                    pnd_Ssn_Nbr.reset();                                                                                                                                  //Natural: RESET #SSN-NBR #FOUND #RCRD-TYPE
                    pnd_Found.reset();
                    pnd_Rcrd_Type.reset();
                    pnd_Ssn_Nbr.setValue(pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Ssn);                                                                                           //Natural: ASSIGN #SSN-NBR := #TIAA-SCND-INFO.CIS-SCND-ANNT-SSN
                                                                                                                                                                          //Natural: PERFORM CHECK-SCND-EXISTS
                    sub_Check_Scnd_Exists();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-SCND-INFO
                    sub_Write_Tiaa_Scnd_Info();
                    if (condition(Global.isEscape())) {return;}
                    //*  (1518)
                }                                                                                                                                                         //Natural: END-IF
                //*  (1502)
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Cntrct_Type.equals("C")))                                                                                                    //Natural: IF #WORK-FILE1.CIS-CNTRCT-TYPE = 'C'
            {
                pnd_Cref_Info.reset();                                                                                                                                    //Natural: RESET #CREF-INFO CISL400 #CREF-TRNS
                ldaCisl400.getCisl400().reset();
                pnd_Cref_Trns.reset();
                if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IA") && pnd_Work_File1_Cis_Trnsf_Flag.equals("Y") && pnd_Work_File1_Cis_Ownership_Cd.equals(2)))         //Natural: IF #WORK-FILE1.CIS-RQST-ID = 'IA' AND #WORK-FILE1.CIS-TRNSF-FLAG = 'Y' AND #WORK-FILE1.CIS-OWNERSHIP-CD = 2
                {
                    pnd_Cref_Trns.setValuesByName(pnd_Work_File1);                                                                                                        //Natural: MOVE BY NAME #WORK-FILE1 TO #CREF-TRNS
                    ldaCisl400.getCisl400().setValuesByName(pnd_Cref_Trns);                                                                                               //Natural: MOVE BY NAME #CREF-TRNS TO CISL400
                                                                                                                                                                          //Natural: PERFORM WRITE-TRNSF-CREF-SCND-INFO
                    sub_Write_Trnsf_Cref_Scnd_Info();
                    if (condition(Global.isEscape())) {return;}
                    //*  (1532)
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cref_Info.setValuesByName(pnd_Work_File1);                                                                                                        //Natural: MOVE BY NAME #WORK-FILE1 TO #CREF-INFO
                    ldaCisl400.getCisl400().setValuesByName(pnd_Cref_Info);                                                                                               //Natural: MOVE BY NAME #CREF-INFO TO CISL400
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-INFO
                    sub_Write_Cref_Info();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Cref_Scnd_Info.reset();                                                                                                                           //Natural: RESET #CREF-SCND-INFO CISL400 #SCND-ANNT-ADDED
                    ldaCisl400.getCisl400().reset();
                    pnd_Scnd_Annt_Added.reset();
                    pnd_Cref_Scnd_Info.setValuesByName(pnd_Work_File1);                                                                                                   //Natural: MOVE BY NAME #WORK-FILE1 TO #CREF-SCND-INFO
                    ldaCisl400.getCisl400().setValuesByName(pnd_Cref_Scnd_Info);                                                                                          //Natural: MOVE BY NAME #CREF-SCND-INFO TO CISL400
                    if (condition(pnd_Work_File1_Cis_Scnd_Annt_Ssn.notEquals(getZero())))                                                                                 //Natural: IF #WORK-FILE1.CIS-SCND-ANNT-SSN NE 0
                    {
                        pnd_Ssn_Nbr.reset();                                                                                                                              //Natural: RESET #SSN-NBR #FOUND #RCRD-TYPE
                        pnd_Found.reset();
                        pnd_Rcrd_Type.reset();
                        pnd_Ssn_Nbr.setValue(pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Ssn);                                                                                       //Natural: ASSIGN #SSN-NBR := #CREF-SCND-INFO.CIS-SCND-ANNT-SSN
                                                                                                                                                                          //Natural: PERFORM CHECK-SCND-EXISTS
                        sub_Check_Scnd_Exists();
                        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-SCND-INFO
                        sub_Write_Cref_Scnd_Info();
                        if (condition(Global.isEscape())) {return;}
                        //*  (1562)
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (1532)
                }                                                                                                                                                         //Natural: END-IF
                //*  (1456)
            }                                                                                                                                                             //Natural: END-IF
            //*  (1452)
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tiaa_Info_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                                     //Natural: IF #TIAA-INFO.CIS-SG-TEXT-UDF-3 = 'MDM'
        {
            //*  IARPF
            //*  IARPF
            //*  IARPF
            //*  IARPF
            //*  IARPF
            cth_Out_Record.reset();                                                                                                                                       //Natural: RESET CTH-OUT-RECORD
            cth_Out_Record_Cth_Rqst_Id_Key.setValue(pnd_Work_File1_Cis_Rqst_Id_Key);                                                                                      //Natural: ASSIGN CTH-RQST-ID-KEY := #WORK-FILE1.CIS-RQST-ID-KEY
            cth_Out_Record_Cth_Tiaa_Nbr.setValue(pnd_Work_File1_Cis_Tiaa_Nbr);                                                                                            //Natural: ASSIGN CTH-TIAA-NBR := #WORK-FILE1.CIS-TIAA-NBR
            cth_Out_Record_Cth_Cert_Nbr.setValue(pnd_Work_File1_Cis_Cert_Nbr);                                                                                            //Natural: ASSIGN CTH-CERT-NBR := #WORK-FILE1.CIS-CERT-NBR
            cth_Out_Record_Cth_Status.setValue("PLATFORM COMPLETE");                                                                                                      //Natural: ASSIGN CTH-STATUS := 'PLATFORM COMPLETE'
            //*  IARPF
            //*  IARPF
            getWorkFiles().write(3, false, cth_Out_Record);                                                                                                               //Natural: WRITE WORK FILE 3 CTH-OUT-RECORD
            cth_Out_Record_Written.nadd(1);                                                                                                                               //Natural: ASSIGN CTH-OUT-RECORD-WRITTEN := CTH-OUT-RECORD-WRITTEN + 1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Tpa_Rtrn() throws Exception                                                                                                                          //Natural: TPA-RTRN
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        if (condition((pnd_Work_File1_Cis_Status_Cd.equals("I") && (pnd_Work_File1_Cis_Cor_Sync_Ind.equals("Y") || pnd_Work_File1_Cis_Cor_Sync_Ind.equals("S")))))        //Natural: IF #WORK-FILE1.CIS-STATUS-CD = 'I' AND ( #WORK-FILE1.CIS-COR-SYNC-IND = 'Y' OR = 'S' )
        {
            if (condition(pnd_Work_File1_Cis_Cntrct_Type.equals("T")))                                                                                                    //Natural: IF #WORK-FILE1.CIS-CNTRCT-TYPE = 'T'
            {
                pnd_Tiaa_Info.reset();                                                                                                                                    //Natural: RESET #TIAA-INFO CISL400
                ldaCisl400.getCisl400().reset();
                pnd_Tiaa_Info.setValuesByName(pnd_Work_File1);                                                                                                            //Natural: MOVE BY NAME #WORK-FILE1 TO #TIAA-INFO
                ldaCisl400.getCisl400().setValuesByName(pnd_Tiaa_Info);                                                                                                   //Natural: MOVE BY NAME #TIAA-INFO TO CISL400
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-INFO
                sub_Write_Tiaa_Info();
                if (condition(Global.isEscape())) {return;}
                pnd_Tiaa_Scnd_Info.reset();                                                                                                                               //Natural: RESET #TIAA-SCND-INFO CISL400 #SCND-ANNT-ADDED
                ldaCisl400.getCisl400().reset();
                pnd_Scnd_Annt_Added.reset();
                //*  (1066)
            }                                                                                                                                                             //Natural: END-IF
            //*  (1618)
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ipro_Rtrn() throws Exception                                                                                                                         //Natural: IPRO-RTRN
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        if (condition((pnd_Work_File1_Cis_Status_Cd.equals("I") && (pnd_Work_File1_Cis_Cor_Sync_Ind.equals("Y") || pnd_Work_File1_Cis_Cor_Sync_Ind.equals("S")))))        //Natural: IF #WORK-FILE1.CIS-STATUS-CD = 'I' AND ( #WORK-FILE1.CIS-COR-SYNC-IND = 'Y' OR = 'S' )
        {
            if (condition(pnd_Work_File1_Cis_Cntrct_Type.equals("T")))                                                                                                    //Natural: IF #WORK-FILE1.CIS-CNTRCT-TYPE = 'T'
            {
                pnd_Tiaa_Info.reset();                                                                                                                                    //Natural: RESET #TIAA-INFO CISL400
                ldaCisl400.getCisl400().reset();
                pnd_Tiaa_Info.setValuesByName(pnd_Work_File1);                                                                                                            //Natural: MOVE BY NAME #WORK-FILE1 TO #TIAA-INFO
                ldaCisl400.getCisl400().setValuesByName(pnd_Tiaa_Info);                                                                                                   //Natural: MOVE BY NAME #TIAA-INFO TO CISL400
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-INFO
                sub_Write_Tiaa_Info();
                if (condition(Global.isEscape())) {return;}
                pnd_Tiaa_Scnd_Info.reset();                                                                                                                               //Natural: RESET #TIAA-SCND-INFO CISL400 #SCND-ANNT-ADDED
                ldaCisl400.getCisl400().reset();
                pnd_Scnd_Annt_Added.reset();
                //*  (1652)
            }                                                                                                                                                             //Natural: END-IF
            //*  (1648)
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Tiaa_Info() throws Exception                                                                                                                   //Natural: WRITE-TIAA-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Total_Tiaa.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #TOTAL-TIAA
        ldaCisl400.getCisl400_Cis_Annt_Ssn().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Ssn);                                                                                   //Natural: ASSIGN CISL400.CIS-ANNT-SSN := #TIAA-INFO.CIS-FRST-ANNT-SSN
        ldaCisl400.getCisl400_Cis_Annt_Prfx().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Prfx);                                                                                 //Natural: ASSIGN CISL400.CIS-ANNT-PRFX := #TIAA-INFO.CIS-FRST-ANNT-PRFX
        ldaCisl400.getCisl400_Cis_Annt_Frst_Nme().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Frst_Nme);                                                                         //Natural: ASSIGN CISL400.CIS-ANNT-FRST-NME := #TIAA-INFO.CIS-FRST-ANNT-FRST-NME
        ldaCisl400.getCisl400_Cis_Annt_Mid_Nme().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Mid_Nme);                                                                           //Natural: ASSIGN CISL400.CIS-ANNT-MID-NME := #TIAA-INFO.CIS-FRST-ANNT-MID-NME
        ldaCisl400.getCisl400_Cis_Annt_Lst_Nme().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Lst_Nme);                                                                           //Natural: ASSIGN CISL400.CIS-ANNT-LST-NME := #TIAA-INFO.CIS-FRST-ANNT-LST-NME
        ldaCisl400.getCisl400_Cis_Annt_Sffx().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Sffx);                                                                                 //Natural: ASSIGN CISL400.CIS-ANNT-SFFX := #TIAA-INFO.CIS-FRST-ANNT-SFFX
        ldaCisl400.getCisl400_Cis_Annt_Dob().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Dob);                                                                                   //Natural: ASSIGN CISL400.CIS-ANNT-DOB := #TIAA-INFO.CIS-FRST-ANNT-DOB
        ldaCisl400.getCisl400_Cis_Doi().setValue(pnd_Tiaa_Info_Cis_Tiaa_Doi_N);                                                                                           //Natural: ASSIGN CISL400.CIS-DOI := #TIAA-INFO.CIS-TIAA-DOI-N
        ldaCisl400.getCisl400_Cis_Cor_Cntrct_Payee_Cde().setValue("01");                                                                                                  //Natural: MOVE '01' TO CISL400.CIS-COR-CNTRCT-PAYEE-CDE
        //*  IARPF
        if (condition(pnd_Tiaa_Info_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                                     //Natural: IF #TIAA-INFO.CIS-SG-TEXT-UDF-3 = 'MDM'
        {
            //*  IARPF
            ldaCisl400.getCisl400_Cis_Function_Cde().setValue("010");                                                                                                     //Natural: MOVE '010' TO CISL400.CIS-FUNCTION-CDE
            //*  IARPF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IARPF
            ldaCisl400.getCisl400_Cis_Function_Cde().setValue("005");                                                                                                     //Natural: MOVE '005' TO CISL400.CIS-FUNCTION-CDE
            //*  IARPF
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl400.getCisl400_Cis_Product_Code().setValue("T");                                                                                                           //Natural: MOVE 'T' TO CISL400.CIS-PRODUCT-CODE
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Prfx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-PRFX TRANSLATE INTO UPPER CASE
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Sffx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-SFFX TRANSLATE INTO UPPER CASE
        ldaCisl400.getCisl400_Cis_Rqst_Id().setValue("CIS");                                                                                                              //Natural: MOVE 'CIS' TO CISL400.CIS-RQST-ID
        ldaCisl400.getCisl400_Addr_Line1().reset();                                                                                                                       //Natural: RESET CISL400.ADDR-LINE1 CISL400.ADDR-LINE2 CISL400.ADDR-LINE3 CISL400.ADDR-LINE4 CISL400.ADDR-LINE5 CISL400.ADDR-LINE6
        ldaCisl400.getCisl400_Addr_Line2().reset();
        ldaCisl400.getCisl400_Addr_Line3().reset();
        ldaCisl400.getCisl400_Addr_Line4().reset();
        ldaCisl400.getCisl400_Addr_Line5().reset();
        ldaCisl400.getCisl400_Addr_Line6().reset();
                                                                                                                                                                          //Natural: PERFORM GET-OPTION-CODE
        sub_Get_Option_Code();
        if (condition(Global.isEscape())) {return;}
        //* *PERFORM WRITE-REPORT       /* MOVED TO AFTER COR UPDATE - ERRRPT KG
        pnd_Total.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #TOTAL
        //*  HK >>>>>>>>
        if (condition(pnd_Work_File1_Cis_Cor_Sync_Ind.equals("Y")))                                                                                                       //Natural: IF #WORK-FILE1.CIS-COR-SYNC-IND = 'Y'
        {
            getWorkFiles().write(2, false, ldaCisl400.getCisl400());                                                                                                      //Natural: WRITE WORK FILE 2 CISL400
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_File1_Cis_Cor_Sync_Ind.equals("S")))                                                                                                       //Natural: IF #WORK-FILE1.CIS-COR-SYNC-IND = 'S'
        {
            //*  ADDED BIP LOGIC STARTS HERE...
            if (condition(pnd_Work_File1_Cis_Decedent_Contract.greater(" ")))                                                                                             //Natural: IF #WORK-FILE1.CIS-DECEDENT-CONTRACT GT ' '
            {
                ldaCispartv.getVw_cis_Part_View().startDatabaseFind                                                                                                       //Natural: FIND ( 1 ) CIS-PART-VIEW WITH CIS-TIAA-NBR = #TIAA-INFO.CIS-TIAA-NBR
                (
                "FIND01",
                new Wc[] { new Wc("CIS_TIAA_NBR", "=", pnd_Tiaa_Info_Cis_Tiaa_Nbr, WcType.WITH) },
                1
                );
                FIND01:
                while (condition(ldaCispartv.getVw_cis_Part_View().readNextRow("FIND01", true)))
                {
                    ldaCispartv.getVw_cis_Part_View().setIfNotFoundControlFlag(false);
                    if (condition(ldaCispartv.getVw_cis_Part_View().getAstCOUNTER().equals(0)))                                                                           //Natural: IF NO RECORD FOUND
                    {
                        getReports().write(0, "NO RECORD FOUND FOR CONTRACT:",pnd_Tiaa_Info_Cis_Tiaa_Nbr);                                                                //Natural: WRITE 'NO RECORD FOUND FOR CONTRACT:' #TIAA-INFO.CIS-TIAA-NBR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-NOREC
                    pnd_Isn.setValue(ldaCispartv.getVw_cis_Part_View().getAstISN("Find01"));                                                                              //Natural: ASSIGN #ISN := *ISN
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED BIP LOGIC ENDS HERE.
                                                                                                                                                                          //Natural: PERFORM CALL-COR-API
            sub_Call_Cor_Api();
            if (condition(Global.isEscape())) {return;}
            //*  ADDED BIP LOGIC STARTS HERE...
            //*  BIP
            if (condition(pnd_Bene_In_Plan.getBoolean() && pnd_Isn.greater(getZero()) && ! (pnd_Cor_Error.getBoolean())))                                                 //Natural: IF #BENE-IN-PLAN AND #ISN > 0 AND NOT #COR-ERROR
            {
                //*  PROD DEBUGS          /* BIP
                getReports().write(0, "TPA BIP UPDATES:");                                                                                                                //Natural: WRITE 'TPA BIP UPDATES:'
                if (Global.isEscape()) return;
                GETCIS2:                                                                                                                                                  //Natural: GET CIS-PART-VIEW #ISN
                ldaCispartv.getVw_cis_Part_View().readByID(pnd_Isn.getLong(), "GETCIS2");
                //*  061808
                if (condition(pnd_On_Cor.getBoolean()))                                                                                                                   //Natural: IF #ON-COR
                {
                    //*  JRB1
                    ldaCispartv.getCis_Part_View_Cis_Pin_Nbr().setValue(pnd_Cor_Api_Pin);                                                                                 //Natural: MOVE #COR-API-PIN TO CIS-PART-VIEW.CIS-PIN-NBR #TIAA-INFO.CIS-RQST-PIN
                    pnd_Tiaa_Info_Cis_Rqst_Pin.setValue(pnd_Cor_Api_Pin);
                    //*  JRB1
                    getReports().write(0, "PART -","CONTRACT:",pnd_Tiaa_Info_Cis_Tiaa_Nbr,"PIN:",pnd_Cor_Api_Pin,"REQID:",pnd_Tiaa_Info_Cis_Rqst_Id_Key);                 //Natural: WRITE 'PART -' 'CONTRACT:' #TIAA-INFO.CIS-TIAA-NBR 'PIN:' #COR-API-PIN 'REQID:' #TIAA-INFO.CIS-RQST-ID-KEY
                    if (Global.isEscape()) return;
                    //*  (1792)                                 /* 061808
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*      MOVE #MDMA300.#NEW-PIN                               /* PINE
                    //*  PINE
                    ldaCispartv.getCis_Part_View_Cis_Pin_Nbr().setValue(pdaMdma301.getPnd_Mdma301_Pnd_New_Pin());                                                         //Natural: MOVE #MDMA301.#NEW-PIN TO CIS-PART-VIEW.CIS-PIN-NBR #TIAA-INFO.CIS-RQST-PIN
                    pnd_Tiaa_Info_Cis_Rqst_Pin.setValue(pdaMdma301.getPnd_Mdma301_Pnd_New_Pin());
                    //*  JRB1
                    //*  PINE
                    getReports().write(0, "PART -","CONTRACT:",pnd_Tiaa_Info_Cis_Tiaa_Nbr,"PIN:",pdaMdma301.getPnd_Mdma301_Pnd_New_Pin(),"REQID:",pnd_Tiaa_Info_Cis_Rqst_Id_Key); //Natural: WRITE 'PART -' 'CONTRACT:' #TIAA-INFO.CIS-TIAA-NBR 'PIN:' #MDMA301.#NEW-PIN 'REQID:' #TIAA-INFO.CIS-RQST-ID-KEY
                    if (Global.isEscape()) return;
                    //*  (1792)                                /* 061808
                }                                                                                                                                                         //Natural: END-IF
                ldaCispartv.getCis_Part_View_Cis_Rqst_Id_Key().setValue(pnd_Tiaa_Info_Cis_Rqst_Id_Key);                                                                   //Natural: ASSIGN CIS-PART-VIEW.CIS-RQST-ID-KEY := #TIAA-INFO.CIS-RQST-ID-KEY
                ldaCispartv.getVw_cis_Part_View().updateDBRow("GETCIS2");                                                                                                 //Natural: UPDATE ( GETCIS2. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                //*  ADDED BIP LOGIC ENDS HERE.
                //*  HK <<<<<<<
            }                                                                                                                                                             //Natural: END-IF
            //*  HK <<<<<<<
        }                                                                                                                                                                 //Natural: END-IF
        //*  ERRRPT KG START
        if (condition(! (pnd_Cor_Error.getBoolean())))                                                                                                                    //Natural: IF NOT #COR-ERROR
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape())) {return;}
            //*  ERRRPT KG END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Tiaa_Scnd_Info() throws Exception                                                                                                              //Natural: WRITE-TIAA-SCND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_Total_Tiaa_Scnd.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-TIAA-SCND
        ldaCisl400.getCisl400_Cis_Annt_Ssn().setValue(pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Ssn);                                                                              //Natural: ASSIGN CISL400.CIS-ANNT-SSN := #TIAA-SCND-INFO.CIS-SCND-ANNT-SSN
        ldaCisl400.getCisl400_Cis_Annt_Prfx().setValue(pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Prfx);                                                                            //Natural: ASSIGN CISL400.CIS-ANNT-PRFX := #TIAA-SCND-INFO.CIS-SCND-ANNT-PRFX
        ldaCisl400.getCisl400_Cis_Annt_Frst_Nme().setValue(pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Frst_Nme);                                                                    //Natural: ASSIGN CISL400.CIS-ANNT-FRST-NME := #TIAA-SCND-INFO.CIS-SCND-ANNT-FRST-NME
        ldaCisl400.getCisl400_Cis_Annt_Mid_Nme().setValue(pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Mid_Nme);                                                                      //Natural: ASSIGN CISL400.CIS-ANNT-MID-NME := #TIAA-SCND-INFO.CIS-SCND-ANNT-MID-NME
        ldaCisl400.getCisl400_Cis_Annt_Lst_Nme().setValue(pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Lst_Nme);                                                                      //Natural: ASSIGN CISL400.CIS-ANNT-LST-NME := #TIAA-SCND-INFO.CIS-SCND-ANNT-LST-NME
        ldaCisl400.getCisl400_Cis_Annt_Sffx().setValue(pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Sffx);                                                                            //Natural: ASSIGN CISL400.CIS-ANNT-SFFX := #TIAA-SCND-INFO.CIS-SCND-ANNT-SFFX
        ldaCisl400.getCisl400_Cis_Annt_Dob().setValue(pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Dob);                                                                              //Natural: ASSIGN CISL400.CIS-ANNT-DOB := #TIAA-SCND-INFO.CIS-SCND-ANNT-DOB
        ldaCisl400.getCisl400_Cis_Doi().setValue(pnd_Tiaa_Scnd_Info_Cis_Tiaa_Doi_N);                                                                                      //Natural: ASSIGN CISL400.CIS-DOI := #TIAA-SCND-INFO.CIS-TIAA-DOI-N
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Prfx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-PRFX TRANSLATE INTO UPPER CASE
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Sffx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-SFFX TRANSLATE INTO UPPER CASE
        ldaCisl400.getCisl400_Cis_Cor_Cntrct_Payee_Cde().setValue("02");                                                                                                  //Natural: MOVE '02' TO CISL400.CIS-COR-CNTRCT-PAYEE-CDE
        if (condition(pnd_Found.getBoolean()))                                                                                                                            //Natural: IF #FOUND
        {
            pnd_Total_Add_Cntrct_Tiaa_Scnd.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-ADD-CNTRCT-TIAA-SCND
            //*  IARPF
            if (condition(pnd_Tiaa_Scnd_Info_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                            //Natural: IF #TIAA-SCND-INFO.CIS-SG-TEXT-UDF-3 = 'MDM'
            {
                //*  IARPF
                ldaCisl400.getCisl400_Cis_Function_Cde().setValue("010");                                                                                                 //Natural: MOVE '010' TO CISL400.CIS-FUNCTION-CDE
                //*  IARPF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  IARPF
                ldaCisl400.getCisl400_Cis_Function_Cde().setValue("005");                                                                                                 //Natural: MOVE '005' TO CISL400.CIS-FUNCTION-CDE
                //*  IARPF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Total_Add_Tiaa_Scnd.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOTAL-ADD-TIAA-SCND
            ldaCisl400.getCisl400_Cis_Annt_Sex_Cde().setValue(pnd_Tiaa_Scnd_Info_Cis_Scnd_Annt_Sex_Cde);                                                                  //Natural: ASSIGN CISL400.CIS-ANNT-SEX-CDE := #TIAA-SCND-INFO.CIS-SCND-ANNT-SEX-CDE
            ldaCisl400.getCisl400_Cis_Function_Cde().setValue("011");                                                                                                     //Natural: MOVE '011' TO CISL400.CIS-FUNCTION-CDE
            pnd_Scnd_Annt_Added.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #SCND-ANNT-ADDED
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl400.getCisl400_Cis_Product_Code().setValue("T");                                                                                                           //Natural: MOVE 'T' TO CISL400.CIS-PRODUCT-CODE
        ldaCisl400.getCisl400_Cis_Rqst_Id().setValue("CIS");                                                                                                              //Natural: MOVE 'CIS' TO CISL400.CIS-RQST-ID
        ldaCisl400.getCisl400_Addr_Line1().reset();                                                                                                                       //Natural: RESET CISL400.ADDR-LINE1 CISL400.ADDR-LINE2 CISL400.ADDR-LINE3 CISL400.ADDR-LINE4 CISL400.ADDR-LINE5 CISL400.ADDR-LINE6
        ldaCisl400.getCisl400_Addr_Line2().reset();
        ldaCisl400.getCisl400_Addr_Line3().reset();
        ldaCisl400.getCisl400_Addr_Line4().reset();
        ldaCisl400.getCisl400_Addr_Line5().reset();
        ldaCisl400.getCisl400_Addr_Line6().reset();
                                                                                                                                                                          //Natural: PERFORM GET-OPTION-CODE
        sub_Get_Option_Code();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        pnd_Total.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #TOTAL
        getWorkFiles().write(2, false, ldaCisl400.getCisl400());                                                                                                          //Natural: WRITE WORK FILE 2 CISL400
    }
    private void sub_Write_Cref_Info() throws Exception                                                                                                                   //Natural: WRITE-CREF-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Total_Cref.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #TOTAL-CREF
        ldaCisl400.getCisl400_Cis_Annt_Ssn().setValue(pnd_Cref_Info_Cis_Frst_Annt_Ssn);                                                                                   //Natural: ASSIGN CISL400.CIS-ANNT-SSN := #CREF-INFO.CIS-FRST-ANNT-SSN
        ldaCisl400.getCisl400_Cis_Annt_Prfx().setValue(pnd_Cref_Info_Cis_Frst_Annt_Prfx);                                                                                 //Natural: ASSIGN CISL400.CIS-ANNT-PRFX := #CREF-INFO.CIS-FRST-ANNT-PRFX
        ldaCisl400.getCisl400_Cis_Annt_Frst_Nme().setValue(pnd_Cref_Info_Cis_Frst_Annt_Frst_Nme);                                                                         //Natural: ASSIGN CISL400.CIS-ANNT-FRST-NME := #CREF-INFO.CIS-FRST-ANNT-FRST-NME
        ldaCisl400.getCisl400_Cis_Annt_Mid_Nme().setValue(pnd_Cref_Info_Cis_Frst_Annt_Mid_Nme);                                                                           //Natural: ASSIGN CISL400.CIS-ANNT-MID-NME := #CREF-INFO.CIS-FRST-ANNT-MID-NME
        ldaCisl400.getCisl400_Cis_Annt_Lst_Nme().setValue(pnd_Cref_Info_Cis_Frst_Annt_Lst_Nme);                                                                           //Natural: ASSIGN CISL400.CIS-ANNT-LST-NME := #CREF-INFO.CIS-FRST-ANNT-LST-NME
        ldaCisl400.getCisl400_Cis_Annt_Sffx().setValue(pnd_Cref_Info_Cis_Frst_Annt_Sffx);                                                                                 //Natural: ASSIGN CISL400.CIS-ANNT-SFFX := #CREF-INFO.CIS-FRST-ANNT-SFFX
        ldaCisl400.getCisl400_Cis_Annt_Dob().setValue(pnd_Cref_Info_Cis_Frst_Annt_Dob);                                                                                   //Natural: ASSIGN CISL400.CIS-ANNT-DOB := #CREF-INFO.CIS-FRST-ANNT-DOB
        ldaCisl400.getCisl400_Cis_Doi().setValue(pnd_Cref_Info_Cis_Cref_Doi_N);                                                                                           //Natural: ASSIGN CISL400.CIS-DOI := #CREF-INFO.CIS-CREF-DOI-N
        ldaCisl400.getCisl400_Cis_Tiaa_Nbr().setValue(pnd_Cref_Info_Cis_Cert_Nbr);                                                                                        //Natural: ASSIGN CISL400.CIS-TIAA-NBR := #CREF-INFO.CIS-CERT-NBR
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Prfx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-PRFX TRANSLATE INTO UPPER CASE
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Sffx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-SFFX TRANSLATE INTO UPPER CASE
        ldaCisl400.getCisl400_Cis_Cert_Nbr().reset();                                                                                                                     //Natural: RESET CISL400.CIS-CERT-NBR
        ldaCisl400.getCisl400_Cis_Cor_Cntrct_Payee_Cde().setValue("01");                                                                                                  //Natural: MOVE '01' TO CISL400.CIS-COR-CNTRCT-PAYEE-CDE
        //*  IARPF
        if (condition(pnd_Cref_Info_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                                     //Natural: IF #CREF-INFO.CIS-SG-TEXT-UDF-3 = 'MDM'
        {
            //*  IARPF
            ldaCisl400.getCisl400_Cis_Function_Cde().setValue("010");                                                                                                     //Natural: MOVE '010' TO CISL400.CIS-FUNCTION-CDE
            //*  IARPF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IARPF
            ldaCisl400.getCisl400_Cis_Function_Cde().setValue("005");                                                                                                     //Natural: MOVE '005' TO CISL400.CIS-FUNCTION-CDE
            //*  IARPF
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl400.getCisl400_Cis_Product_Code().setValue(pnd_Work_File1_Cis_Cref_Acct_Cde.getValue(1));                                                                  //Natural: MOVE #WORK-FILE1.CIS-CREF-ACCT-CDE ( 1 ) TO CISL400.CIS-PRODUCT-CODE
        if (condition(ldaCisl400.getCisl400_Cis_Product_Code().equals(" ")))                                                                                              //Natural: IF CISL400.CIS-PRODUCT-CODE = ' '
        {
            ldaCisl400.getCisl400_Cis_Product_Code().setValue("C");                                                                                                       //Natural: MOVE 'C' TO CISL400.CIS-PRODUCT-CODE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl400.getCisl400_Cis_Rqst_Id().setValue("CIS");                                                                                                              //Natural: MOVE 'CIS' TO CISL400.CIS-RQST-ID
        ldaCisl400.getCisl400_Addr_Line1().reset();                                                                                                                       //Natural: RESET CISL400.ADDR-LINE1 CISL400.ADDR-LINE2 CISL400.ADDR-LINE3 CISL400.ADDR-LINE4 CISL400.ADDR-LINE5 CISL400.ADDR-LINE6
        ldaCisl400.getCisl400_Addr_Line2().reset();
        ldaCisl400.getCisl400_Addr_Line3().reset();
        ldaCisl400.getCisl400_Addr_Line4().reset();
        ldaCisl400.getCisl400_Addr_Line5().reset();
        ldaCisl400.getCisl400_Addr_Line6().reset();
                                                                                                                                                                          //Natural: PERFORM GET-OPTION-CODE
        sub_Get_Option_Code();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        pnd_Total.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #TOTAL
        getWorkFiles().write(2, false, ldaCisl400.getCisl400());                                                                                                          //Natural: WRITE WORK FILE 2 CISL400
    }
    private void sub_Write_Cref_Scnd_Info() throws Exception                                                                                                              //Natural: WRITE-CREF-SCND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_Total_Cref_Scnd.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-CREF-SCND
        ldaCisl400.getCisl400_Cis_Annt_Ssn().setValue(pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Ssn);                                                                              //Natural: ASSIGN CISL400.CIS-ANNT-SSN := #CREF-SCND-INFO.CIS-SCND-ANNT-SSN
        ldaCisl400.getCisl400_Cis_Annt_Prfx().setValue(pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Prfx);                                                                            //Natural: ASSIGN CISL400.CIS-ANNT-PRFX := #CREF-SCND-INFO.CIS-SCND-ANNT-PRFX
        ldaCisl400.getCisl400_Cis_Annt_Frst_Nme().setValue(pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Frst_Nme);                                                                    //Natural: ASSIGN CISL400.CIS-ANNT-FRST-NME := #CREF-SCND-INFO.CIS-SCND-ANNT-FRST-NME
        ldaCisl400.getCisl400_Cis_Annt_Mid_Nme().setValue(pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Mid_Nme);                                                                      //Natural: ASSIGN CISL400.CIS-ANNT-MID-NME := #CREF-SCND-INFO.CIS-SCND-ANNT-MID-NME
        ldaCisl400.getCisl400_Cis_Annt_Lst_Nme().setValue(pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Lst_Nme);                                                                      //Natural: ASSIGN CISL400.CIS-ANNT-LST-NME := #CREF-SCND-INFO.CIS-SCND-ANNT-LST-NME
        ldaCisl400.getCisl400_Cis_Annt_Sffx().setValue(pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Sffx);                                                                            //Natural: ASSIGN CISL400.CIS-ANNT-SFFX := #CREF-SCND-INFO.CIS-SCND-ANNT-SFFX
        ldaCisl400.getCisl400_Cis_Annt_Dob().setValue(pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Dob);                                                                              //Natural: ASSIGN CISL400.CIS-ANNT-DOB := #CREF-SCND-INFO.CIS-SCND-ANNT-DOB
        ldaCisl400.getCisl400_Cis_Doi().setValue(pnd_Cref_Scnd_Info_Cis_Cref_Doi);                                                                                        //Natural: ASSIGN CISL400.CIS-DOI := #CREF-SCND-INFO.CIS-CREF-DOI
        ldaCisl400.getCisl400_Cis_Tiaa_Nbr().setValue(pnd_Cref_Info_Cis_Cert_Nbr);                                                                                        //Natural: ASSIGN CISL400.CIS-TIAA-NBR := #CREF-INFO.CIS-CERT-NBR
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Prfx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-PRFX TRANSLATE INTO UPPER CASE
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Sffx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-SFFX TRANSLATE INTO UPPER CASE
        ldaCisl400.getCisl400_Cis_Cert_Nbr().reset();                                                                                                                     //Natural: RESET CISL400.CIS-CERT-NBR
        ldaCisl400.getCisl400_Cis_Cor_Cntrct_Payee_Cde().setValue("02");                                                                                                  //Natural: MOVE '02' TO CISL400.CIS-COR-CNTRCT-PAYEE-CDE
        if (condition(pnd_Found.getBoolean()))                                                                                                                            //Natural: IF #FOUND
        {
            //*  IARPF
            if (condition(pnd_Cref_Scnd_Info_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                            //Natural: IF #CREF-SCND-INFO.CIS-SG-TEXT-UDF-3 = 'MDM'
            {
                //*  IARPF
                ldaCisl400.getCisl400_Cis_Function_Cde().setValue("010");                                                                                                 //Natural: MOVE '010' TO CISL400.CIS-FUNCTION-CDE
                //*  IARPF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  IARPF
                ldaCisl400.getCisl400_Cis_Function_Cde().setValue("005");                                                                                                 //Natural: MOVE '005' TO CISL400.CIS-FUNCTION-CDE
                //*  IARPF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Total_Add_Cntrct_Cref_Scnd.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-ADD-CNTRCT-CREF-SCND
            //*  (2076)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Scnd_Annt_Added.getBoolean()))                                                                                                              //Natural: IF #SCND-ANNT-ADDED
            {
                pnd_Total_Add_Cntrct_Cref_Scnd.nadd(1);                                                                                                                   //Natural: ADD 1 TO #TOTAL-ADD-CNTRCT-CREF-SCND
                //*  IARPF
                if (condition(pnd_Cref_Scnd_Info_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                        //Natural: IF #CREF-SCND-INFO.CIS-SG-TEXT-UDF-3 = 'MDM'
                {
                    //*  IARPF
                    ldaCisl400.getCisl400_Cis_Function_Cde().setValue("010");                                                                                             //Natural: MOVE '010' TO CISL400.CIS-FUNCTION-CDE
                    //*  IARPF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IARPF
                    ldaCisl400.getCisl400_Cis_Function_Cde().setValue("005");                                                                                             //Natural: MOVE '005' TO CISL400.CIS-FUNCTION-CDE
                    //*  IARPF
                }                                                                                                                                                         //Natural: END-IF
                //*  (2096)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Total_Add_Cref_Scnd.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-ADD-CREF-SCND
                ldaCisl400.getCisl400_Cis_Annt_Sex_Cde().setValue(pnd_Cref_Scnd_Info_Cis_Scnd_Annt_Sex_Cde);                                                              //Natural: ASSIGN CISL400.CIS-ANNT-SEX-CDE := #CREF-SCND-INFO.CIS-SCND-ANNT-SEX-CDE
                ldaCisl400.getCisl400_Cis_Function_Cde().setValue("011");                                                                                                 //Natural: MOVE '011' TO CISL400.CIS-FUNCTION-CDE
                //*  (2096)
            }                                                                                                                                                             //Natural: END-IF
            //*  (2076)
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl400.getCisl400_Cis_Product_Code().setValue(pnd_Work_File1_Cis_Cref_Acct_Cde.getValue(1));                                                                  //Natural: MOVE #WORK-FILE1.CIS-CREF-ACCT-CDE ( 1 ) TO CISL400.CIS-PRODUCT-CODE
        if (condition(ldaCisl400.getCisl400_Cis_Product_Code().equals(" ")))                                                                                              //Natural: IF CISL400.CIS-PRODUCT-CODE = ' '
        {
            ldaCisl400.getCisl400_Cis_Product_Code().setValue("C");                                                                                                       //Natural: MOVE 'C' TO CISL400.CIS-PRODUCT-CODE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl400.getCisl400_Cis_Rqst_Id().setValue("CIS");                                                                                                              //Natural: MOVE 'CIS' TO CISL400.CIS-RQST-ID
        ldaCisl400.getCisl400_Addr_Line1().reset();                                                                                                                       //Natural: RESET CISL400.ADDR-LINE1 CISL400.ADDR-LINE2 CISL400.ADDR-LINE3 CISL400.ADDR-LINE4 CISL400.ADDR-LINE5 CISL400.ADDR-LINE6
        ldaCisl400.getCisl400_Addr_Line2().reset();
        ldaCisl400.getCisl400_Addr_Line3().reset();
        ldaCisl400.getCisl400_Addr_Line4().reset();
        ldaCisl400.getCisl400_Addr_Line5().reset();
        ldaCisl400.getCisl400_Addr_Line6().reset();
                                                                                                                                                                          //Natural: PERFORM GET-OPTION-CODE
        sub_Get_Option_Code();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().write(2, false, ldaCisl400.getCisl400());                                                                                                          //Natural: WRITE WORK FILE 2 CISL400
    }
    private void sub_Write_Trnsf_Cref_Scnd_Info() throws Exception                                                                                                        //Natural: WRITE-TRNSF-CREF-SCND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        pnd_Total_Cref.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #TOTAL-CREF
        ldaCisl400.getCisl400_Cis_Annt_Ssn().setValue(pnd_Cref_Trns_Cis_Scnd_Annt_Ssn);                                                                                   //Natural: ASSIGN CISL400.CIS-ANNT-SSN := #CREF-TRNS.CIS-SCND-ANNT-SSN
        ldaCisl400.getCisl400_Cis_Annt_Prfx().setValue(pnd_Cref_Trns_Cis_Scnd_Annt_Prfx);                                                                                 //Natural: ASSIGN CISL400.CIS-ANNT-PRFX := #CREF-TRNS.CIS-SCND-ANNT-PRFX
        ldaCisl400.getCisl400_Cis_Annt_Frst_Nme().setValue(pnd_Cref_Trns_Cis_Scnd_Annt_Frst_Nme);                                                                         //Natural: ASSIGN CISL400.CIS-ANNT-FRST-NME := #CREF-TRNS.CIS-SCND-ANNT-FRST-NME
        ldaCisl400.getCisl400_Cis_Annt_Mid_Nme().setValue(pnd_Cref_Trns_Cis_Scnd_Annt_Mid_Nme);                                                                           //Natural: ASSIGN CISL400.CIS-ANNT-MID-NME := #CREF-TRNS.CIS-SCND-ANNT-MID-NME
        ldaCisl400.getCisl400_Cis_Annt_Lst_Nme().setValue(pnd_Cref_Trns_Cis_Scnd_Annt_Lst_Nme);                                                                           //Natural: ASSIGN CISL400.CIS-ANNT-LST-NME := #CREF-TRNS.CIS-SCND-ANNT-LST-NME
        ldaCisl400.getCisl400_Cis_Annt_Sffx().setValue(pnd_Cref_Trns_Cis_Scnd_Annt_Sffx);                                                                                 //Natural: ASSIGN CISL400.CIS-ANNT-SFFX := #CREF-TRNS.CIS-SCND-ANNT-SFFX
        ldaCisl400.getCisl400_Cis_Annt_Dob().setValue(pnd_Cref_Trns_Cis_Scnd_Annt_Dob);                                                                                   //Natural: ASSIGN CISL400.CIS-ANNT-DOB := #CREF-TRNS.CIS-SCND-ANNT-DOB
        ldaCisl400.getCisl400_Cis_Doi().setValue(pnd_Cref_Trns_Cis_Cref_Doi_N);                                                                                           //Natural: ASSIGN CISL400.CIS-DOI := #CREF-TRNS.CIS-CREF-DOI-N
        ldaCisl400.getCisl400_Cis_Tiaa_Nbr().setValue(pnd_Cref_Trns_Cis_Cert_Nbr);                                                                                        //Natural: ASSIGN CISL400.CIS-TIAA-NBR := #CREF-TRNS.CIS-CERT-NBR
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Prfx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-PRFX TRANSLATE INTO UPPER CASE
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Sffx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-SFFX TRANSLATE INTO UPPER CASE
        ldaCisl400.getCisl400_Cis_Cert_Nbr().reset();                                                                                                                     //Natural: RESET CISL400.CIS-CERT-NBR
        ldaCisl400.getCisl400_Cis_Cor_Cntrct_Payee_Cde().setValue("02");                                                                                                  //Natural: MOVE '02' TO CISL400.CIS-COR-CNTRCT-PAYEE-CDE
        //*  IARPF
        if (condition(pnd_Cref_Trns_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                                     //Natural: IF #CREF-TRNS.CIS-SG-TEXT-UDF-3 = 'MDM'
        {
            //*  IARPF
            ldaCisl400.getCisl400_Cis_Function_Cde().setValue("010");                                                                                                     //Natural: MOVE '010' TO CISL400.CIS-FUNCTION-CDE
            //*  IARPF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IARPF
            ldaCisl400.getCisl400_Cis_Function_Cde().setValue("005");                                                                                                     //Natural: MOVE '005' TO CISL400.CIS-FUNCTION-CDE
            //*  IARPF
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl400.getCisl400_Cis_Product_Code().setValue(pnd_Work_File1_Cis_Cref_Acct_Cde.getValue(1));                                                                  //Natural: MOVE #WORK-FILE1.CIS-CREF-ACCT-CDE ( 1 ) TO CISL400.CIS-PRODUCT-CODE
        if (condition(ldaCisl400.getCisl400_Cis_Product_Code().equals(" ")))                                                                                              //Natural: IF CISL400.CIS-PRODUCT-CODE = ' '
        {
            ldaCisl400.getCisl400_Cis_Product_Code().setValue("C");                                                                                                       //Natural: MOVE 'C' TO CISL400.CIS-PRODUCT-CODE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl400.getCisl400_Cis_Rqst_Id().setValue("CIS");                                                                                                              //Natural: MOVE 'CIS' TO CISL400.CIS-RQST-ID
        ldaCisl400.getCisl400_Addr_Line1().reset();                                                                                                                       //Natural: RESET CISL400.ADDR-LINE1 CISL400.ADDR-LINE2 CISL400.ADDR-LINE3 CISL400.ADDR-LINE4 CISL400.ADDR-LINE5 CISL400.ADDR-LINE6
        ldaCisl400.getCisl400_Addr_Line2().reset();
        ldaCisl400.getCisl400_Addr_Line3().reset();
        ldaCisl400.getCisl400_Addr_Line4().reset();
        ldaCisl400.getCisl400_Addr_Line5().reset();
        ldaCisl400.getCisl400_Addr_Line6().reset();
                                                                                                                                                                          //Natural: PERFORM GET-OPTION-CODE
        sub_Get_Option_Code();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
        sub_Write_Report();
        if (condition(Global.isEscape())) {return;}
        pnd_Total.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #TOTAL
        getWorkFiles().write(2, false, ldaCisl400.getCisl400());                                                                                                          //Natural: WRITE WORK FILE 2 CISL400
    }
    private void sub_Mdo_Rtrn() throws Exception                                                                                                                          //Natural: MDO-RTRN
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        //*  ERRRPT KG
        pnd_Total_Tiaa.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #TOTAL-TIAA
        pnd_Mdo_Info.reset();                                                                                                                                             //Natural: RESET #MDO-INFO CISL400
        ldaCisl400.getCisl400().reset();
        pnd_Mdo_Info.setValuesByName(pnd_Work_File1);                                                                                                                     //Natural: MOVE BY NAME #WORK-FILE1 TO #MDO-INFO
        ldaCisl400.getCisl400().setValuesByName(pnd_Mdo_Info);                                                                                                            //Natural: MOVE BY NAME #MDO-INFO TO CISL400
        ldaCisl400.getCisl400_Cis_Annt_Ssn().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Ssn);                                                                                    //Natural: ASSIGN CISL400.CIS-ANNT-SSN := #MDO-INFO.CIS-FRST-ANNT-SSN
        ldaCisl400.getCisl400_Cis_Annt_Prfx().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Prfx);                                                                                  //Natural: ASSIGN CISL400.CIS-ANNT-PRFX := #MDO-INFO.CIS-FRST-ANNT-PRFX
        ldaCisl400.getCisl400_Cis_Annt_Frst_Nme().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Frst_Nme);                                                                          //Natural: ASSIGN CISL400.CIS-ANNT-FRST-NME := #MDO-INFO.CIS-FRST-ANNT-FRST-NME
        ldaCisl400.getCisl400_Cis_Annt_Mid_Nme().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Mid_Nme);                                                                            //Natural: ASSIGN CISL400.CIS-ANNT-MID-NME := #MDO-INFO.CIS-FRST-ANNT-MID-NME
        ldaCisl400.getCisl400_Cis_Annt_Lst_Nme().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Lst_Nme);                                                                            //Natural: ASSIGN CISL400.CIS-ANNT-LST-NME := #MDO-INFO.CIS-FRST-ANNT-LST-NME
        ldaCisl400.getCisl400_Cis_Annt_Sffx().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Sffx);                                                                                  //Natural: ASSIGN CISL400.CIS-ANNT-SFFX := #MDO-INFO.CIS-FRST-ANNT-SFFX
        ldaCisl400.getCisl400_Cis_Annt_Dob().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Dob);                                                                                    //Natural: ASSIGN CISL400.CIS-ANNT-DOB := #MDO-INFO.CIS-FRST-ANNT-DOB
        ldaCisl400.getCisl400_Cis_Doi().setValue(pnd_Mdo_Info_Cis_Tiaa_Doi_N);                                                                                            //Natural: ASSIGN CISL400.CIS-DOI := #MDO-INFO.CIS-TIAA-DOI-N
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Prfx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-PRFX TRANSLATE INTO UPPER CASE
        DbsUtil.examine(new ExamineSource(ldaCisl400.getCisl400_Cis_Annt_Sffx()), new ExamineTranslate(TranslateOption.Upper));                                           //Natural: EXAMINE CISL400.CIS-ANNT-SFFX TRANSLATE INTO UPPER CASE
        //*  090606
        if (condition(DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'") && pnd_Work_File1_Cis_Annty_Option.equals("SURV")))                                         //Natural: IF #WORK-FILE1.CIS-RQST-ID = MASK ( 'MDO' ) AND #WORK-FILE1.CIS-ANNTY-OPTION = 'SURV'
        {
            ldaCisl400.getCisl400_Cis_Annt_Sex_Cde().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Sex_Cde);                                                                        //Natural: ASSIGN CISL400.CIS-ANNT-SEX-CDE := #MDO-INFO.CIS-FRST-ANNT-SEX-CDE
            ldaCisl400.getCisl400_Cis_Function_Cde().setValue("003");                                                                                                     //Natural: MOVE '003' TO CISL400.CIS-FUNCTION-CDE
            ldaCisl400.getCisl400_Cis_Pin_Nbr().reset();                                                                                                                  //Natural: RESET CISL400.CIS-PIN-NBR
            //*  (2302)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IARPF
            if (condition(pnd_Mdo_Info_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                                  //Natural: IF #MDO-INFO.CIS-SG-TEXT-UDF-3 = 'MDM'
            {
                //*  IARPF
                ldaCisl400.getCisl400_Cis_Function_Cde().setValue("010");                                                                                                 //Natural: MOVE '010' TO CISL400.CIS-FUNCTION-CDE
                //*  IARPF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  IARPF
                ldaCisl400.getCisl400_Cis_Function_Cde().setValue("005");                                                                                                 //Natural: MOVE '005' TO CISL400.CIS-FUNCTION-CDE
                //*  IARPF
            }                                                                                                                                                             //Natural: END-IF
            //*  (2302)
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_File1_Cis_Cert_Nbr.notEquals(" ") && pnd_Work_File1_Cis_Cref_Doi.notEquals(getZero())))                                                    //Natural: IF #WORK-FILE1.CIS-CERT-NBR NE ' ' AND #WORK-FILE1.CIS-CREF-DOI NE 0
        {
            ldaCisl400.getCisl400_Addr_Line1().setValue("Y");                                                                                                             //Natural: MOVE 'Y' TO CISL400.ADDR-LINE1
            //*  (2332)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCisl400.getCisl400_Addr_Line1().setValue("N");                                                                                                             //Natural: MOVE 'N' TO CISL400.ADDR-LINE1
            //*  (2332)
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl400.getCisl400_Cis_Cor_Cntrct_Payee_Cde().setValue("01");                                                                                                  //Natural: MOVE '01' TO CISL400.CIS-COR-CNTRCT-PAYEE-CDE
        ldaCisl400.getCisl400_Cis_Rqst_Id().setValue("CIS");                                                                                                              //Natural: MOVE 'CIS' TO CISL400.CIS-RQST-ID
        ldaCisl400.getCisl400_Addr_Line2().reset();                                                                                                                       //Natural: RESET CISL400.ADDR-LINE2 CISL400.ADDR-LINE3 CISL400.ADDR-LINE4 CISL400.ADDR-LINE5 CISL400.ADDR-LINE6
        ldaCisl400.getCisl400_Addr_Line3().reset();
        ldaCisl400.getCisl400_Addr_Line4().reset();
        ldaCisl400.getCisl400_Addr_Line5().reset();
        ldaCisl400.getCisl400_Addr_Line6().reset();
        pnd_Total.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #TOTAL
        //* *PERFORM WRITE-REPORT           /* MOVED TO BOTTOM - ERRRPT KG
        if (condition(pnd_Work_File1_Cis_Cor_Sync_Ind.equals("Y")))                                                                                                       //Natural: IF #WORK-FILE1.CIS-COR-SYNC-IND = 'Y'
        {
            getWorkFiles().write(2, false, ldaCisl400.getCisl400());                                                                                                      //Natural: WRITE WORK FILE 2 CISL400
        }                                                                                                                                                                 //Natural: END-IF
        //*  BIP
        if (condition(pnd_Work_File1_Cis_Cor_Sync_Ind.equals("S") || pnd_Bene_In_Plan.getBoolean()))                                                                      //Natural: IF #WORK-FILE1.CIS-COR-SYNC-IND = 'S' OR #BENE-IN-PLAN
        {
            ldaCispartv.getVw_cis_Part_View().startDatabaseFind                                                                                                           //Natural: FIND ( 1 ) CIS-PART-VIEW WITH CIS-TIAA-NBR = #MDO-INFO.CIS-TIAA-NBR
            (
            "FIND02",
            new Wc[] { new Wc("CIS_TIAA_NBR", "=", pnd_Mdo_Info_Cis_Tiaa_Nbr, WcType.WITH) },
            1
            );
            FIND02:
            while (condition(ldaCispartv.getVw_cis_Part_View().readNextRow("FIND02", true)))
            {
                ldaCispartv.getVw_cis_Part_View().setIfNotFoundControlFlag(false);
                if (condition(ldaCispartv.getVw_cis_Part_View().getAstCOUNTER().equals(0)))                                                                               //Natural: IF NO RECORD FOUND
                {
                    getReports().write(0, "NO RECORD FOUND FOR CONTRACT:",pnd_Mdo_Info_Cis_Tiaa_Nbr);                                                                     //Natural: WRITE 'NO RECORD FOUND FOR CONTRACT:' #MDO-INFO.CIS-TIAA-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                //*  BIP
                if (condition(ldaCispartv.getCis_Part_View_Cis_Mdo_Contract_Type().equals("S") || ldaCispartv.getCis_Part_View_Cis_Decedent_Contract().greater(" ")))     //Natural: IF CIS-PART-VIEW.CIS-MDO-CONTRACT-TYPE = 'S' OR CIS-PART-VIEW.CIS-DECEDENT-CONTRACT GT ' '
                {
                    pnd_Mdo_Cntr_Type_S.setValue(true);                                                                                                                   //Natural: ASSIGN #MDO-CNTR-TYPE-S := TRUE
                    pnd_Isn.setValue(ldaCispartv.getVw_cis_Part_View().getAstISN("Find02"));                                                                              //Natural: ASSIGN #ISN := *ISN
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM CALL-COR-API
            sub_Call_Cor_Api();
            if (condition(Global.isEscape())) {return;}
            //*  IF #MDO-CNTR-TYPE-S AND #ISN > 0 AND NOT #COR-ERROR       /* BIP
            //*  BIP
            //*  BIP
            if (condition((((pnd_Mdo_Cntr_Type_S.getBoolean() || pnd_Bene_In_Plan.getBoolean()) && pnd_Isn.greater(getZero())) && ! (pnd_Cor_Error.getBoolean()))))       //Natural: IF ( #MDO-CNTR-TYPE-S OR #BENE-IN-PLAN ) AND #ISN > 0 AND NOT #COR-ERROR
            {
                //*  PROD DEBUGS         /* JRB1
                getReports().write(0, "MDO SIP AND BIP UPDATES:");                                                                                                        //Natural: WRITE 'MDO SIP AND BIP UPDATES:'
                if (Global.isEscape()) return;
                GETCIS:                                                                                                                                                   //Natural: GET CIS-PART-VIEW #ISN
                ldaCispartv.getVw_cis_Part_View().readByID(pnd_Isn.getLong(), "GETCIS");
                //*  061808
                if (condition(pnd_On_Cor.getBoolean()))                                                                                                                   //Natural: IF #ON-COR
                {
                    //*  JRB1
                    ldaCispartv.getCis_Part_View_Cis_Pin_Nbr().setValue(pnd_Cor_Api_Pin);                                                                                 //Natural: MOVE #COR-API-PIN TO CIS-PART-VIEW.CIS-PIN-NBR #MDO-INFO.CIS-RQST-PIN
                    pnd_Mdo_Info_Cis_Rqst_Pin.setValue(pnd_Cor_Api_Pin);
                    //*  JRB1
                    getReports().write(0, "PART -","CONTRACT:",pnd_Mdo_Info_Cis_Tiaa_Nbr,"PIN:",pnd_Cor_Api_Pin,"REQID:",pnd_Mdo_Info_Cis_Rqst_Id_Key);                   //Natural: WRITE 'PART -' 'CONTRACT:' #MDO-INFO.CIS-TIAA-NBR 'PIN:' #COR-API-PIN 'REQID:' #MDO-INFO.CIS-RQST-ID-KEY
                    if (Global.isEscape()) return;
                    //*  (1792)                                 /* 061808
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*      MOVE #MDMA300.#NEW-PIN                               /* PINE
                    //*  PINE
                    ldaCispartv.getCis_Part_View_Cis_Pin_Nbr().setValue(pdaMdma301.getPnd_Mdma301_Pnd_New_Pin());                                                         //Natural: MOVE #MDMA301.#NEW-PIN TO CIS-PART-VIEW.CIS-PIN-NBR #MDO-INFO.CIS-RQST-PIN
                    pnd_Mdo_Info_Cis_Rqst_Pin.setValue(pdaMdma301.getPnd_Mdma301_Pnd_New_Pin());
                    //*  JRB1
                    //*  PINE
                    getReports().write(0, "PART -","CONTRACT:",pnd_Mdo_Info_Cis_Tiaa_Nbr,"PIN:",pdaMdma301.getPnd_Mdma301_Pnd_New_Pin(),"REQID:",pnd_Mdo_Info_Cis_Rqst_Id_Key); //Natural: WRITE 'PART -' 'CONTRACT:' #MDO-INFO.CIS-TIAA-NBR 'PIN:' #MDMA301.#NEW-PIN 'REQID:' #MDO-INFO.CIS-RQST-ID-KEY
                    if (Global.isEscape()) return;
                    //*  (1792)                                /* 061808
                }                                                                                                                                                         //Natural: END-IF
                ldaCispartv.getCis_Part_View_Cis_Rqst_Id_Key().setValue(pnd_Mdo_Info_Cis_Rqst_Id_Key);                                                                    //Natural: ASSIGN CIS-PART-VIEW.CIS-RQST-ID-KEY := #MDO-INFO.CIS-RQST-ID-KEY
                ldaCispartv.getVw_cis_Part_View().updateDBRow("GETCIS");                                                                                                  //Natural: UPDATE ( GETCIS. )
                pnd_Cis_Bene_Super_2_Pnd_Cis_Bene_Tiaa_Nbr.setValue(pnd_Mdo_Info_Cis_Tiaa_Nbr);                                                                           //Natural: ASSIGN #CIS-BENE-TIAA-NBR := #MDO-INFO.CIS-TIAA-NBR
                ldaCisl5000.getVw_cis_Bene_File_01().startDatabaseRead                                                                                                    //Natural: READ CIS-BENE-FILE-01 BY CIS-BEN-SUPER-2 FROM #CIS-BENE-SUPER-2
                (
                "READBENE",
                new Wc[] { new Wc("CIS_BEN_SUPER_2", ">=", pnd_Cis_Bene_Super_2, WcType.BY) },
                new Oc[] { new Oc("CIS_BEN_SUPER_2", "ASC") }
                );
                READBENE:
                while (condition(ldaCisl5000.getVw_cis_Bene_File_01().readNextRow("READBENE")))
                {
                    if (condition(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr().notEquals(pnd_Mdo_Info_Cis_Tiaa_Nbr)))                                              //Natural: IF CIS-BENE-TIAA-NBR NE #MDO-INFO.CIS-TIAA-NBR
                    {
                        //*  (2450)
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Bene_Rqst_Id_Key.setValue(ldaCisl5000.getCis_Bene_File_01_Cis_Bene_Rqst_Id_Key());                                                                //Natural: ASSIGN #BENE-RQST-ID-KEY := CIS-BENE-FILE-01.CIS-BENE-RQST-ID-KEY
                    GETBENE:                                                                                                                                              //Natural: GET BENE-UPDATE *ISN ( READBENE. )
                    vw_bene_Update.readByID(ldaCisl5000.getVw_cis_Bene_File_01().getAstISN("READBENE"), "GETBENE");
                    if (condition(pnd_On_Cor.getBoolean()))                                                                                                               //Natural: IF #ON-COR
                    {
                        bene_Update_Cis_Bene_Unique_Id_Nbr.setValue(pnd_Cor_Api_Pin);                                                                                     //Natural: MOVE #COR-API-PIN TO BENE-UPDATE.CIS-BENE-UNIQUE-ID-NBR #BENE-RQST-PIN
                        pnd_Bene_Rqst_Id_Key_Pnd_Bene_Rqst_Pin.setValue(pnd_Cor_Api_Pin);
                        //*  PROD DEBUG                    /* JRB1
                        getReports().write(0, "BENE -","CONTRACT:",pnd_Mdo_Info_Cis_Tiaa_Nbr,"PIN:",pnd_Cor_Api_Pin,"REQID:",pnd_Bene_Rqst_Id_Key);                       //Natural: WRITE 'BENE -' 'CONTRACT:' #MDO-INFO.CIS-TIAA-NBR 'PIN:' #COR-API-PIN 'REQID:' #BENE-RQST-ID-KEY
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READBENE"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READBENE"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  (2464)
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*        MOVE #MDMA300.#NEW-PIN                             /* PINE
                        //*  PINE
                        bene_Update_Cis_Bene_Unique_Id_Nbr.setValue(pdaMdma301.getPnd_Mdma301_Pnd_New_Pin());                                                             //Natural: MOVE #MDMA301.#NEW-PIN TO BENE-UPDATE.CIS-BENE-UNIQUE-ID-NBR #BENE-RQST-PIN
                        pnd_Bene_Rqst_Id_Key_Pnd_Bene_Rqst_Pin.setValue(pdaMdma301.getPnd_Mdma301_Pnd_New_Pin());
                        //*  PROD DEBUG                    /* JRB1
                        //*  PINE
                        getReports().write(0, "BENE -","CONTRACT:",pnd_Mdo_Info_Cis_Tiaa_Nbr,"PIN:",pdaMdma301.getPnd_Mdma301_Pnd_New_Pin(),"REQID:",pnd_Bene_Rqst_Id_Key); //Natural: WRITE 'BENE -' 'CONTRACT:' #MDO-INFO.CIS-TIAA-NBR 'PIN:' #MDMA301.#NEW-PIN 'REQID:' #BENE-RQST-ID-KEY
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READBENE"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READBENE"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  (2464)
                    }                                                                                                                                                     //Natural: END-IF
                    bene_Update_Cis_Bene_Rqst_Id_Key.setValue(pnd_Bene_Rqst_Id_Key);                                                                                      //Natural: ASSIGN BENE-UPDATE.CIS-BENE-RQST-ID-KEY := #BENE-RQST-ID-KEY
                    vw_bene_Update.updateDBRow("GETBENE");                                                                                                                //Natural: UPDATE ( GETBENE. )
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                //*  (2394)
            }                                                                                                                                                             //Natural: END-IF
            //*  022908 END
            pnd_Mdo_Cntr_Type_S.reset();                                                                                                                                  //Natural: RESET #MDO-CNTR-TYPE-S #ISN
            pnd_Isn.reset();
            //*  (2366)                    /* HK <<<<<<<
        }                                                                                                                                                                 //Natural: END-IF
        //*  ERRRPT KG START
        if (condition(! (pnd_Cor_Error.getBoolean())))                                                                                                                    //Natural: IF NOT #COR-ERROR
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape())) {return;}
            //*  ERRRPT KG END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Scnd_Exists() throws Exception                                                                                                                 //Natural: CHECK-SCND-EXISTS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  CALLNAT 'CISN410' #SSN-NBR #FOUND #RCRD-TYPE
        //*  RESET #MDMA100                                           /* PINE
        //*  PINE
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn().setValue(pnd_Ssn_Nbr);                                                                                                      //Natural: ASSIGN #I-SSN := #SSN-NBR
        //*  CALLNAT 'MDMN100A' #MDMA100                               /* PINE
        //*  PINE
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #O-RETURN-CODE = '0000'
        {
            //*  NOT USED BY CALLING PROGRAM /* PINE
            if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Relationship_Code().equals("S")))                                                                               //Natural: IF #O-RELATIONSHIP-CODE = 'S'
            {
                pnd_Found.setValue(true);                                                                                                                                 //Natural: ASSIGN #FOUND := TRUE
                pdaMdma301.getPnd_Mdma301_Pnd_Record_Type().setValue(1);                                                                                                  //Natural: ASSIGN #MDMA301.#RECORD-TYPE := 01
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pnd_Dat.reset();                                                                                                                                                  //Natural: RESET #DAT #SSN #DOB #NAME #IDAT-A #TOTAL
        pnd_Ssn.reset();
        pnd_Dob.reset();
        pnd_Name.reset();
        pnd_Idat_A.reset();
        pnd_Total.reset();
        pnd_Dat.setValue(ldaCisl400.getCisl400_Cis_Doi());                                                                                                                //Natural: ASSIGN #DAT := CISL400.CIS-DOI
        pnd_Idat_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dat_Pnd_D_Mm, "/", pnd_Dat_Pnd_D_Dd, "/", pnd_Dat_Pnd_D_Yyyy));                           //Natural: COMPRESS #D-MM '/' #D-DD '/' #D-YYYY INTO #IDAT-A LEAVING NO
        pnd_Ssn.setValue(ldaCisl400.getCisl400_Cis_Annt_Ssn());                                                                                                           //Natural: ASSIGN #SSN := CISL400.CIS-ANNT-SSN
        pnd_Dob.setValue(ldaCisl400.getCisl400_Cis_Annt_Dob());                                                                                                           //Natural: ASSIGN #DOB := CISL400.CIS-ANNT-DOB
        pnd_Dob_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dob_Pnd_Dob_Mm, "/", pnd_Dob_Pnd_Dob_Dd, "/", pnd_Dob_Pnd_Dob_Yyyy));                      //Natural: COMPRESS #DOB-MM '/' #DOB-DD '/' #DOB-YYYY INTO #DOB-A LEAVING NO
        //*  022908 /* PINE
        pnd_Name.setValue(DbsUtil.compress(ldaCisl400.getCisl400_Cis_Annt_Prfx(), ldaCisl400.getCisl400_Cis_Annt_Frst_Nme(), ldaCisl400.getCisl400_Cis_Annt_Mid_Nme(),    //Natural: COMPRESS CISL400.CIS-ANNT-PRFX CISL400.CIS-ANNT-FRST-NME CISL400.CIS-ANNT-MID-NME CISL400.CIS-ANNT-LST-NME CISL400.CIS-ANNT-SFFX INTO #NAME LEAVING
            ldaCisl400.getCisl400_Cis_Annt_Lst_Nme(), ldaCisl400.getCisl400_Cis_Annt_Sffx()));
        getReports().write(1, ReportOption.NOTITLE,ldaCisl400.getCisl400_Cis_Rqst_Id(),pdaMdma301.getPnd_Mdma301_Pnd_Function_Code(),new ColumnSpacing(3),ldaCisl400.getCisl400_Cis_Pin_Nbr(),pnd_Ssn,  //Natural: WRITE ( 1 ) CISL400.CIS-RQST-ID #MDMA301.#FUNCTION-CODE 3X CISL400.CIS-PIN-NBR #SSN #NAME #DOB-A CISL400.CIS-TIAA-NBR CISL400.CIS-CERT-NBR #IDAT-A 4X CISL400.CIS-COR-CNTRCT-PAYEE-CDE 2X CISL400.CIS-PRODUCT-CODE 2X CISL400.CIS-ANNTY-OPTION #WORK-FILE1.CIS-COR-SYNC-IND
            new ReportEditMask ("999-99-9999"),pnd_Name,pnd_Dob_A,ldaCisl400.getCisl400_Cis_Tiaa_Nbr(),ldaCisl400.getCisl400_Cis_Cert_Nbr(),pnd_Idat_A,new 
            ColumnSpacing(4),ldaCisl400.getCisl400_Cis_Cor_Cntrct_Payee_Cde(),new ColumnSpacing(2),ldaCisl400.getCisl400_Cis_Product_Code(),new ColumnSpacing(2),
            ldaCisl400.getCisl400_Cis_Annty_Option(),pnd_Work_File1_Cis_Cor_Sync_Ind);
        if (Global.isEscape()) return;
    }
    //*  ADDED - ERRRPT KG
    private void sub_Write_Error_Report() throws Exception                                                                                                                //Natural: WRITE-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        pnd_Dat.reset();                                                                                                                                                  //Natural: RESET #DAT #SSN #NAME #IDAT-A #TOTAL
        pnd_Ssn.reset();
        pnd_Name.reset();
        pnd_Idat_A.reset();
        pnd_Total.reset();
        short decideConditionsMet2585 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WORK-FILE1.CIS-RQST-ID;//Natural: VALUE 'IPRO'
        if (condition((pnd_Work_File1_Cis_Rqst_Id.equals("IPRO"))))
        {
            decideConditionsMet2585++;
            pnd_Ipro_Error.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #IPRO-ERROR
            pnd_C_Type.setValue("IPRO");                                                                                                                                  //Natural: ASSIGN #C-TYPE := 'IPRO'
        }                                                                                                                                                                 //Natural: VALUE 'IPI'
        else if (condition((pnd_Work_File1_Cis_Rqst_Id.equals("IPI"))))
        {
            decideConditionsMet2585++;
            pnd_Ipi_Error.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #IPI-ERROR
            pnd_C_Type.setValue("IPRO");                                                                                                                                  //Natural: ASSIGN #C-TYPE := 'IPRO'
        }                                                                                                                                                                 //Natural: VALUE 'TPA'
        else if (condition((pnd_Work_File1_Cis_Rqst_Id.equals("TPA"))))
        {
            decideConditionsMet2585++;
            pnd_Tpa_Error.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TPA-ERROR
            pnd_C_Type.setValue("TPA");                                                                                                                                   //Natural: ASSIGN #C-TYPE := 'TPA'
        }                                                                                                                                                                 //Natural: VALUE 'TPI'
        else if (condition((pnd_Work_File1_Cis_Rqst_Id.equals("TPI"))))
        {
            decideConditionsMet2585++;
            pnd_Tpi_Error.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TPI-ERROR
            pnd_C_Type.setValue("TPA");                                                                                                                                   //Natural: ASSIGN #C-TYPE := 'TPA'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            if (condition(DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'")))                                                                                       //Natural: IF #WORK-FILE1.CIS-RQST-ID = MASK ( 'MDO' )
            {
                pnd_Mdo_Error.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #MDO-ERROR
                pnd_C_Type.setValue("MDO");                                                                                                                               //Natural: ASSIGN #C-TYPE := 'MDO'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Unkwn_Error.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #UNKWN-ERROR
                pnd_C_Type.setValue("UNKW");                                                                                                                              //Natural: ASSIGN #C-TYPE := 'UNKW'
            }                                                                                                                                                             //Natural: END-IF
            //*  CM 07/23/09
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Error_Msg_Trunc.setValue(pdaMdma301.getPnd_Mdma301_Pnd_Return_Text());                                                                                        //Natural: ASSIGN #ERROR-MSG-TRUNC := #MDMA301.#RETURN-TEXT
        pnd_Dat.setValue(ldaCisl400.getCisl400_Cis_Doi());                                                                                                                //Natural: ASSIGN #DAT := CISL400.CIS-DOI
        //*  022908 /* PINE
        //*  CM 07/23/09
        pnd_Idat_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Dat_Pnd_D_Mm, "/", pnd_Dat_Pnd_D_Dd, "/", pnd_Dat_Pnd_D_Yyyy));                           //Natural: COMPRESS #D-MM '/' #D-DD '/' #D-YYYY INTO #IDAT-A LEAVING NO
        pnd_Ssn.setValue(ldaCisl400.getCisl400_Cis_Annt_Ssn());                                                                                                           //Natural: ASSIGN #SSN := CISL400.CIS-ANNT-SSN
        pnd_Short_Name.setValue(ldaCisl400.getCisl400_Cis_Annt_Lst_Nme());                                                                                                //Natural: ASSIGN #SHORT-NAME := CISL400.CIS-ANNT-LST-NME
        getReports().write(2, ReportOption.NOTITLE,pdaMdma301.getPnd_Mdma301_Pnd_Function_Code(),new ColumnSpacing(1),ldaCisl400.getCisl400_Cis_Pin_Nbr(),pnd_Ssn,        //Natural: WRITE ( 2 ) #MDMA301.#FUNCTION-CODE 1X CISL400.CIS-PIN-NBR #SSN #SHORT-NAME CISL400.CIS-TIAA-NBR CISL400.CIS-CERT-NBR #IDAT-A #C-TYPE #MDMA301.#RETURN-CODE #ERROR-MSG-TRUNC
            new ReportEditMask ("999-99-9999"),pnd_Short_Name,ldaCisl400.getCisl400_Cis_Tiaa_Nbr(),ldaCisl400.getCisl400_Cis_Cert_Nbr(),pnd_Idat_A,pnd_C_Type,
            pdaMdma301.getPnd_Mdma301_Pnd_Return_Code(),pnd_Error_Msg_Trunc);
        if (Global.isEscape()) return;
        //* **********************************************************************
        //*  WRITE ERROR REPORT TO FILE TO SEND EMAIL TO BUSINESS                *
        //* **********************************************************************
        //*  JRB2 BEG
        //*  MDO SIP
        short decideConditionsMet2622 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #MDO-CNTR-TYPE-S
        if (condition(pnd_Mdo_Cntr_Type_S.getBoolean()))
        {
            decideConditionsMet2622++;
            //*      OR #BENE-IN-PLAN                              /* BIP
            //*  PINE
            //*  PINE
            getReports().display(3, "/FUNC",                                                                                                                              //Natural: DISPLAY ( 3 ) '/FUNC' #MDMA301.#FUNCTION-CODE '/PIN' CISL400.CIS-PIN-NBR '/SSN' #SSN '/LAST NAME' #SHORT-NAME 'TIAA/CONTRACT' CISL400.CIS-TIAA-NBR 'CREF/CONTRACT' CISL400.CIS-CERT-NBR 'ISSUE/DATE' #IDAT-A 'CNTR/TYPE' #C-TYPE 'ERR/CODE' #MDMA301.#RETURN-CODE '/ERROR MESSAGE' #ERROR-MSG-TRUNC
            		pdaMdma301.getPnd_Mdma301_Pnd_Function_Code(),"/PIN",
            		ldaCisl400.getCisl400_Cis_Pin_Nbr(),"/SSN",
            		pnd_Ssn,"/LAST NAME",
            		pnd_Short_Name,"TIAA/CONTRACT",
            		ldaCisl400.getCisl400_Cis_Tiaa_Nbr(),"CREF/CONTRACT",
            		ldaCisl400.getCisl400_Cis_Cert_Nbr(),"ISSUE/DATE",
            		pnd_Idat_A,"CNTR/TYPE",
            		pnd_C_Type,"ERR/CODE",
            		pdaMdma301.getPnd_Mdma301_Pnd_Return_Code(),"/ERROR MESSAGE",
            		pnd_Error_Msg_Trunc);
            if (Global.isEscape()) return;
            //*  MDO NON-SIP
            //*  TPA, IPRO
            pnd_Sip_Errors.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #SIP-ERRORS
        }                                                                                                                                                                 //Natural: WHEN ( #C-TYPE = 'MDO' AND NOT #MDO-CNTR-TYPE-S ) OR ( #C-TYPE = 'TPA' OR = 'IPRO' )
        else if (condition(((pnd_C_Type.equals("MDO") && ! (pnd_Mdo_Cntr_Type_S.getBoolean())) || (pnd_C_Type.equals("TPA") || pnd_C_Type.equals("IPRO")))))
        {
            decideConditionsMet2622++;
            //*  PINE
            //*  PINE
            getReports().display(4, "/FUNC",                                                                                                                              //Natural: DISPLAY ( 4 ) '/FUNC' #MDMA301.#FUNCTION-CODE '/PIN' CISL400.CIS-PIN-NBR '/SSN' #SSN '/LAST NAME' #SHORT-NAME 'TIAA/CONTRACT' CISL400.CIS-TIAA-NBR 'CREF/CONTRACT' CISL400.CIS-CERT-NBR 'ISSUE/DATE' #IDAT-A 'CNTR/TYPE' #C-TYPE 'ERR/CODE' #MDMA301.#RETURN-CODE '/ERROR MESSAGE' #ERROR-MSG-TRUNC
            		pdaMdma301.getPnd_Mdma301_Pnd_Function_Code(),"/PIN",
            		ldaCisl400.getCisl400_Cis_Pin_Nbr(),"/SSN",
            		pnd_Ssn,"/LAST NAME",
            		pnd_Short_Name,"TIAA/CONTRACT",
            		ldaCisl400.getCisl400_Cis_Tiaa_Nbr(),"CREF/CONTRACT",
            		ldaCisl400.getCisl400_Cis_Cert_Nbr(),"ISSUE/DATE",
            		pnd_Idat_A,"CNTR/TYPE",
            		pnd_C_Type,"ERR/CODE",
            		pdaMdma301.getPnd_Mdma301_Pnd_Return_Code(),"/ERROR MESSAGE",
            		pnd_Error_Msg_Trunc);
            if (Global.isEscape()) return;
            pnd_Mdo_Tpa_Ipro_Errors.nadd(1);                                                                                                                              //Natural: ADD 1 TO #MDO-TPA-IPRO-ERRORS
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
            //*  JRB2 END
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  ADDED - ERRRPT KG
    private void sub_Error_Totals() throws Exception                                                                                                                      //Natural: ERROR-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Total_Errors.compute(new ComputeParameters(false, pnd_Total_Errors), pnd_Ipro_Error.add(pnd_Ipi_Error).add(pnd_Tpa_Error).add(pnd_Tpi_Error).add(pnd_Mdo_Error).add(pnd_Unkwn_Error)); //Natural: ASSIGN #TOTAL-ERRORS := #IPRO-ERROR + #IPI-ERROR + #TPA-ERROR + #TPI-ERROR + #MDO-ERROR + #UNKWN-ERROR
        if (condition(pnd_Total_Errors.notEquals(getZero())))                                                                                                             //Natural: IF #TOTAL-ERRORS NE 0
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(35),"SUMMARY TOTALS FOR COR ERROR REPORT ",NEWLINE,NEWLINE,new              //Natural: WRITE ( 2 ) /// 35T 'SUMMARY TOTALS FOR COR ERROR REPORT ' // 40T 'IPRO      : ' #IPRO-ERROR / 40T 'IPI       : ' #IPI-ERROR / 40T 'TPA       : ' #TPA-ERROR / 40T 'TPI       : ' #TPI-ERROR / 40T 'MDO       : ' #MDO-ERROR / 40T 'UNKNOWN   : ' #UNKWN-ERROR / 40T '              ______________' / 26T 'TOTAL COR ERROR RECORDS : ' #TOTAL-ERRORS //
                TabSetting(40),"IPRO      : ",pnd_Ipro_Error,NEWLINE,new TabSetting(40),"IPI       : ",pnd_Ipi_Error,NEWLINE,new TabSetting(40),"TPA       : ",pnd_Tpa_Error,NEWLINE,new 
                TabSetting(40),"TPI       : ",pnd_Tpi_Error,NEWLINE,new TabSetting(40),"MDO       : ",pnd_Mdo_Error,NEWLINE,new TabSetting(40),"UNKNOWN   : ",pnd_Unkwn_Error,NEWLINE,new 
                TabSetting(40),"              ______________",NEWLINE,new TabSetting(26),"TOTAL COR ERROR RECORDS : ",pnd_Total_Errors,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            //*  JRB2 BEG
            short decideConditionsMet2649 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #SIP-ERRORS > 0
            if (condition(pnd_Sip_Errors.greater(getZero())))
            {
                decideConditionsMet2649++;
                getReports().write(3, NEWLINE,NEWLINE,NEWLINE,new TabSetting(25),"SUMMARY TOTALS - SIP ERROR REPORT ",NEWLINE,NEWLINE,new TabSetting(30),"SIP ERRORS:",pnd_Sip_Errors,  //Natural: WRITE ( 3 ) /// 25T 'SUMMARY TOTALS - SIP ERROR REPORT ' // 30T 'SIP ERRORS:' #SIP-ERRORS ( EM = ZZZ,ZZZ,ZZ9 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: WHEN #MDO-TPA-IPRO-ERRORS > 0
            if (condition(pnd_Mdo_Tpa_Ipro_Errors.greater(getZero())))
            {
                decideConditionsMet2649++;
                pnd_Non_Sip_Errors.compute(new ComputeParameters(false, pnd_Non_Sip_Errors), pnd_Mdo_Error.subtract(pnd_Sip_Errors));                                     //Natural: COMPUTE #NON-SIP-ERRORS = #MDO-ERROR - #SIP-ERRORS
                getReports().write(4, NEWLINE,NEWLINE,NEWLINE,new TabSetting(25),"SUMMARY TOTALS - MDO TPA IPRO ERROR REPORT ",NEWLINE,NEWLINE,new TabSetting(30),"  MDO ERRORS:",pnd_Non_Sip_Errors,  //Natural: WRITE ( 4 ) /// 25T 'SUMMARY TOTALS - MDO TPA IPRO ERROR REPORT ' // 30T '  MDO ERRORS:' #NON-SIP-ERRORS ( EM = ZZZ,ZZZ,ZZ9 ) '(NON-SIP)' / 30T '  TPA ERRORS:' #TPA-ERROR ( EM = ZZZ,ZZZ,ZZ9 ) / 30T ' IPRO ERRORS:' #IPRO-ERROR ( EM = ZZZ,ZZZ,ZZ9 ) / 45T '==========' / 30T 'TOTAL ERRORS:' #MDO-TPA-IPRO-ERRORS ( EM = ZZZ,ZZZ,ZZ9 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9"),"(NON-SIP)",NEWLINE,new TabSetting(30),"  TPA ERRORS:",pnd_Tpa_Error, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,new 
                    TabSetting(30)," IPRO ERRORS:",pnd_Ipro_Error, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(45),"==========",NEWLINE,new 
                    TabSetting(30),"TOTAL ERRORS:",pnd_Mdo_Tpa_Ipro_Errors, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet2649 == 0))
            {
                ignore();
                //*  JRB2 END
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, ReportOption.NOTITLE,"NO COR ERRORS");                                                                                                  //Natural: WRITE ( 2 ) 'NO COR ERRORS'
            if (Global.isEscape()) return;
            //*  JRB2
            getReports().write(3, "NO COR ERRORS");                                                                                                                       //Natural: WRITE ( 3 ) 'NO COR ERRORS'
            if (Global.isEscape()) return;
            //*  JRB2
            getReports().write(4, "NO COR ERRORS");                                                                                                                       //Natural: WRITE ( 4 ) 'NO COR ERRORS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  IARPF
        getReports().write(0, "TOTAL RECORDS WRITTEN TO CTH: ",cth_Out_Record_Written);                                                                                   //Natural: WRITE 'TOTAL RECORDS WRITTEN TO CTH: ' CTH-OUT-RECORD-WRITTEN
        if (Global.isEscape()) return;
    }
    private void sub_Total_Cor_Feed() throws Exception                                                                                                                    //Natural: TOTAL-COR-FEED
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        //* *ITE (1)
        //* *'TOTAL RECORDS FEED TO COR FOR                           : ' #TOTAL /
        if (condition(pnd_Hold_Rqst_Id.equals("IA")))                                                                                                                     //Natural: IF #HOLD-RQST-ID = 'IA'
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"TOTAL RECORDS FEED TO COR FOR IA                        : ",pnd_Total_Ia,NEWLINE);             //Natural: WRITE ( 1 ) 30T 'TOTAL RECORDS FEED TO COR FOR IA                        : ' #TOTAL-IA /
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet2681 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #HOLD-RQST-ID = 'TPA' OR = 'TPI'
        if (condition(pnd_Hold_Rqst_Id.equals("TPA") || pnd_Hold_Rqst_Id.equals("TPI")))
        {
            decideConditionsMet2681++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"TOTAL RECORDS FEED TO COR FOR TPA                       : ",pnd_Total_Tpa,NEWLINE,new          //Natural: WRITE ( 1 ) 30T 'TOTAL RECORDS FEED TO COR FOR TPA                       : ' #TOTAL-TPA / 30T 'TOTAL RECORDS FEED TO COR FOR TPA OMNI                  : ' #TOTAL-TPA-OMNI / 30T 'TOTAL RECORDS FEED TO COR FOR ICAP TPA OMNI             : ' #TOTAL-TPI-OMNI /
                TabSetting(30),"TOTAL RECORDS FEED TO COR FOR TPA OMNI                  : ",pnd_Total_Tpa_Omni,NEWLINE,new TabSetting(30),"TOTAL RECORDS FEED TO COR FOR ICAP TPA OMNI             : ",
                pnd_Total_Tpi_Omni,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #HOLD-RQST-ID = 'IPRO' OR = 'IPI'
        else if (condition(pnd_Hold_Rqst_Id.equals("IPRO") || pnd_Hold_Rqst_Id.equals("IPI")))
        {
            decideConditionsMet2681++;
            //*  090606
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"TOTAL RECORDS FEED TO COR FOR IPRO                      : ",pnd_Total_Ipro,NEWLINE,new         //Natural: WRITE ( 1 ) 30T 'TOTAL RECORDS FEED TO COR FOR IPRO                      : ' #TOTAL-IPRO / 30T 'TOTAL RECORDS FEED TO COR FOR IPRO OMNI                 : ' #TOTAL-IPRO-OMNI / 30T 'TOTAL RECORDS FEED TO COR FOR ICAP IPRO OMNI            : ' #TOTAL-IPI-OMNI /
                TabSetting(30),"TOTAL RECORDS FEED TO COR FOR IPRO OMNI                 : ",pnd_Total_Ipro_Omni,NEWLINE,new TabSetting(30),"TOTAL RECORDS FEED TO COR FOR ICAP IPRO OMNI            : ",
                pnd_Total_Ipi_Omni,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #HOLD-RQST-ID = MASK ( 'MDO' )
        else if (condition(DbsUtil.maskMatches(pnd_Hold_Rqst_Id,"'MDO'")))
        {
            decideConditionsMet2681++;
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"TOTAL RECORDS FEED TO COR FOR MDO                       : ",pnd_Total_Mdo,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 30T 'TOTAL RECORDS FEED TO COR FOR MDO                       : ' #TOTAL-MDO / / 50T '                                      ______________' / 20T 'TOTAL                                                             : ' #TOTAL-MDO
                TabSetting(50),"                                      ______________",NEWLINE,new TabSetting(20),"TOTAL                                                             : ",
                pnd_Total_Mdo);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #HOLD-RQST-ID = 'IA'
        else if (condition(pnd_Hold_Rqst_Id.equals("IA")))
        {
            decideConditionsMet2681++;
            pnd_Total.compute(new ComputeParameters(false, pnd_Total), pnd_Total_Tiaa.add(pnd_Total_Cref).add(pnd_Total_Tiaa_Scnd).add(pnd_Total_Cref_Scnd));             //Natural: ASSIGN #TOTAL := #TOTAL-TIAA + #TOTAL-CREF + #TOTAL-TIAA-SCND + #TOTAL-CREF-SCND
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"TOTAL RECORDS FEED TO COR FOR TEACHERS CONTRACT         : ",pnd_Total_Tiaa,NEWLINE,new         //Natural: WRITE ( 1 ) 30T 'TOTAL RECORDS FEED TO COR FOR TEACHERS CONTRACT         : ' #TOTAL-TIAA / 30T 'TOTAL RECORDS FEED TO COR FOR CREF CERTIFICATE          : ' #TOTAL-CREF / 30T 'TOTAL RECORDS FEED TO COR FOR T-SECOND ANNUITANT        : ' #TOTAL-TIAA-SCND / 30T 'TOTAL RECORDS FEED TO COR FOR C-SECOND ANNUITANT        : ' #TOTAL-CREF-SCND / / 50T '                                      ______________' / 20T 'TOTAL                                                             : ' #TOTAL
                TabSetting(30),"TOTAL RECORDS FEED TO COR FOR CREF CERTIFICATE          : ",pnd_Total_Cref,NEWLINE,new TabSetting(30),"TOTAL RECORDS FEED TO COR FOR T-SECOND ANNUITANT        : ",pnd_Total_Tiaa_Scnd,NEWLINE,new 
                TabSetting(30),"TOTAL RECORDS FEED TO COR FOR C-SECOND ANNUITANT        : ",pnd_Total_Cref_Scnd,NEWLINE,NEWLINE,new TabSetting(50),"                                      ______________",NEWLINE,new 
                TabSetting(20),"TOTAL                                                             : ",pnd_Total);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #HOLD-RQST-ID = 'TPA' OR = 'TPI'
        else if (condition(pnd_Hold_Rqst_Id.equals("TPA") || pnd_Hold_Rqst_Id.equals("TPI")))
        {
            decideConditionsMet2681++;
            pnd_Total.setValue(pnd_Total_Tiaa);                                                                                                                           //Natural: ASSIGN #TOTAL := #TOTAL-TIAA
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"TOTAL RECORDS FEED TO COR FOR TEACHERS CONTRACT         : ",pnd_Total_Tiaa,NEWLINE,NEWLINE,new //Natural: WRITE ( 1 ) 30T 'TOTAL RECORDS FEED TO COR FOR TEACHERS CONTRACT         : ' #TOTAL-TIAA // 50T '                                      ______________' / 20T 'TOTAL                                                             : ' #TOTAL
                TabSetting(50),"                                      ______________",NEWLINE,new TabSetting(20),"TOTAL                                                             : ",
                pnd_Total);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #HOLD-RQST-ID = 'IPRO'
        else if (condition(pnd_Hold_Rqst_Id.equals("IPRO")))
        {
            decideConditionsMet2681++;
            pnd_Total.setValue(pnd_Total_Tiaa);                                                                                                                           //Natural: ASSIGN #TOTAL := #TOTAL-TIAA
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(30),"TOTAL RECORDS FEED TO COR FOR TEACHERS CONTRACT         : ",pnd_Total_Tiaa,NEWLINE,NEWLINE,new //Natural: WRITE ( 1 ) 30T 'TOTAL RECORDS FEED TO COR FOR TEACHERS CONTRACT         : ' #TOTAL-TIAA // 50T '                                      ______________' / 20T 'TOTAL                                                             : ' #TOTAL
                TabSetting(50),"                                      ______________",NEWLINE,new TabSetting(20),"TOTAL                                                             : ",
                pnd_Total);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
            //*  (2952)
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Option_Code() throws Exception                                                                                                                   //Natural: GET-OPTION-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Cis_Annty_Option.reset();                                                                                                                                     //Natural: RESET #CIS-ANNTY-OPTION
        //*  080107
        //*  080107
        //*  C504365
        short decideConditionsMet2709 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WORK-FILE1.CIS-ANNTY-OPTION = 'OL'
        if (condition(pnd_Work_File1_Cis_Annty_Option.equals("OL")))
        {
            decideConditionsMet2709++;
            pnd_Cis_Annty_Option.setValue("SL");                                                                                                                          //Natural: ASSIGN #CIS-ANNTY-OPTION := 'SL'
        }                                                                                                                                                                 //Natural: WHEN #WORK-FILE1.CIS-ANNTY-OPTION = 'LSF'
        else if (condition(pnd_Work_File1_Cis_Annty_Option.equals("LSF")))
        {
            decideConditionsMet2709++;
            pnd_Cis_Annty_Option.setValue("LF");                                                                                                                          //Natural: ASSIGN #CIS-ANNTY-OPTION := 'LF'
        }                                                                                                                                                                 //Natural: WHEN #WORK-FILE1.CIS-ANNTY-OPTION = 'JS'
        else if (condition(pnd_Work_File1_Cis_Annty_Option.equals("JS")))
        {
            decideConditionsMet2709++;
            pnd_Cis_Annty_Option.setValue("J ");                                                                                                                          //Natural: ASSIGN #CIS-ANNTY-OPTION := 'J '
        }                                                                                                                                                                 //Natural: WHEN #WORK-FILE1.CIS-ANNTY-OPTION = 'LS'
        else if (condition(pnd_Work_File1_Cis_Annty_Option.equals("LS")))
        {
            decideConditionsMet2709++;
            pnd_Cis_Annty_Option.setValue("LH");                                                                                                                          //Natural: ASSIGN #CIS-ANNTY-OPTION := 'LH'
        }                                                                                                                                                                 //Natural: WHEN #WORK-FILE1.CIS-ANNTY-OPTION = 'LST'
        else if (condition(pnd_Work_File1_Cis_Annty_Option.equals("LST")))
        {
            decideConditionsMet2709++;
            pnd_Cis_Annty_Option.setValue("LT");                                                                                                                          //Natural: ASSIGN #CIS-ANNTY-OPTION := 'LT'
        }                                                                                                                                                                 //Natural: WHEN #WORK-FILE1.CIS-ANNTY-OPTION = 'AC'
        else if (condition(pnd_Work_File1_Cis_Annty_Option.equals("AC")))
        {
            decideConditionsMet2709++;
            pnd_Cis_Annty_Option.setValue("AC");                                                                                                                          //Natural: ASSIGN #CIS-ANNTY-OPTION := 'AC'
            ldaCisl400.getCisl400_Cis_Annty_Option().setValue("21");                                                                                                      //Natural: ASSIGN CISL400.CIS-ANNTY-OPTION := '21'
        }                                                                                                                                                                 //Natural: WHEN #WORK-FILE1.CIS-ANNTY-OPTION = 'IR'
        else if (condition(pnd_Work_File1_Cis_Annty_Option.equals("IR")))
        {
            decideConditionsMet2709++;
            pnd_Cis_Annty_Option.setValue("IR");                                                                                                                          //Natural: ASSIGN #CIS-ANNTY-OPTION := 'IR'
            ldaCisl400.getCisl400_Cis_Annty_Option().setValue("02");                                                                                                      //Natural: ASSIGN CISL400.CIS-ANNTY-OPTION := '02'
        }                                                                                                                                                                 //Natural: WHEN #WORK-FILE1.CIS-RQST-ID = 'TPA' OR = 'TPI'
        else if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("TPA") || pnd_Work_File1_Cis_Rqst_Id.equals("TPI")))
        {
            decideConditionsMet2709++;
            if (condition(pnd_Work_File1_Cis_Lob.equals("D") && pnd_Work_File1_Cis_Lob_Type.equals("2")))                                                                 //Natural: IF #WORK-FILE1.CIS-LOB = 'D' AND #WORK-FILE1.CIS-LOB-TYPE = '2'
            {
                ldaCisl400.getCisl400_Cis_Annty_Option().setValue("28");                                                                                                  //Natural: ASSIGN CISL400.CIS-ANNTY-OPTION := '28'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Lob.equals("D") && pnd_Work_File1_Cis_Lob_Type.equals("7")))                                                                 //Natural: IF #WORK-FILE1.CIS-LOB = 'D' AND #WORK-FILE1.CIS-LOB-TYPE = '7'
            {
                ldaCisl400.getCisl400_Cis_Annty_Option().setValue("30");                                                                                                  //Natural: ASSIGN CISL400.CIS-ANNTY-OPTION := '30'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  C504365
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #WORK-FILE1.CIS-RQST-ID = 'IPRO' OR = 'IPI'
        else if (condition(pnd_Work_File1_Cis_Rqst_Id.equals("IPRO") || pnd_Work_File1_Cis_Rqst_Id.equals("IPI")))
        {
            decideConditionsMet2709++;
            if (condition(pnd_Work_File1_Cis_Lob.equals("D") && pnd_Work_File1_Cis_Lob_Type.equals("2")))                                                                 //Natural: IF #WORK-FILE1.CIS-LOB = 'D' AND #WORK-FILE1.CIS-LOB-TYPE = '2'
            {
                ldaCisl400.getCisl400_Cis_Annty_Option().setValue("25");                                                                                                  //Natural: ASSIGN CISL400.CIS-ANNTY-OPTION := '25'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Lob.equals("D") && pnd_Work_File1_Cis_Lob_Type.equals("7")))                                                                 //Natural: IF #WORK-FILE1.CIS-LOB = 'D' AND #WORK-FILE1.CIS-LOB-TYPE = '7'
            {
                ldaCisl400.getCisl400_Cis_Annty_Option().setValue("27");                                                                                                  //Natural: ASSIGN CISL400.CIS-ANNTY-OPTION := '27'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  HK 01/08/01
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Tiaa_Trnsf_Option.reset();                                                                                                                                    //Natural: RESET #TIAA-TRNSF-OPTION
        if (condition(pnd_Work_File1_Cis_Trnsf_Flag.equals("Y")))                                                                                                         //Natural: IF #WORK-FILE1.CIS-TRNSF-FLAG = 'Y'
        {
            pnd_Tiaa_Trnsf_Option.setValue(pnd_Work_File1_Cis_Trnsf_To_Company);                                                                                          //Natural: ASSIGN #TIAA-TRNSF-OPTION := #WORK-FILE1.CIS-TRNSF-TO-COMPANY
            pnd_Work_File1_Cis_Grnted_Period_Yrs.setValue(pnd_Tiaa_Trnsf_Option_Pnd_Trnsf_Option_Yrs);                                                                    //Natural: ASSIGN #WORK-FILE1.CIS-GRNTED-PERIOD-YRS := #TRNSF-OPTION-YRS
        }                                                                                                                                                                 //Natural: END-IF
        //*  080107
        if (condition(pnd_Work_File1_Cis_Annty_Option.notEquals("IR") && pnd_Work_File1_Cis_Annty_Option.notEquals("AC")))                                                //Natural: IF #WORK-FILE1.CIS-ANNTY-OPTION NE 'IR' AND #WORK-FILE1.CIS-ANNTY-OPTION NE 'AC'
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #TBL-SIZE
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Tbl_Size)); pnd_I.nadd(1))
            {
                pnd_Option_Table_2.setValue(pnd_Option_Table.getValue(pnd_I));                                                                                            //Natural: ASSIGN #OPTION-TABLE-2 := #OPTION-TABLE ( #I )
                if (condition(pnd_Cis_Annty_Option.equals(pnd_Option_Table_2_Pnd_Option_Code) && pnd_Work_File1_Cis_Grnted_Period_Yrs.equals(pnd_Option_Table_2_Pnd_Option_Yrs))) //Natural: IF #CIS-ANNTY-OPTION = #OPTION-CODE AND #WORK-FILE1.CIS-GRNTED-PERIOD-YRS = #OPTION-YRS
                {
                    ldaCisl400.getCisl400_Cis_Annty_Option().setValue(pnd_Option_Table_2_Pnd_Option_Code_Cor);                                                            //Natural: ASSIGN CISL400.CIS-ANNTY-OPTION := #OPTION-CODE-COR
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*  (3196)
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  (3190)
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_File1_Cis_Trnsf_Flag.equals("Y")))                                                                                                         //Natural: IF #WORK-FILE1.CIS-TRNSF-FLAG = 'Y'
        {
            if (condition(pnd_Cis_Annty_Option.equals("IR") || pnd_Tiaa_Trnsf_Option_Pnd_Trnsf_Option_Cde.equals("IR")))                                                  //Natural: IF #CIS-ANNTY-OPTION = 'IR' OR #TRNSF-OPTION-CDE = 'IR'
            {
                ldaCisl400.getCisl400_Cis_Annty_Option().setValue("09");                                                                                                  //Natural: ASSIGN CISL400.CIS-ANNTY-OPTION := '09'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Cor_Api() throws Exception                                                                                                                      //Natural: CALL-COR-API
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getReports().write(0, "CALL-MDM-API",pnd_Work_File1_Cis_Tiaa_Nbr);                                                                                                //Natural: WRITE 'CALL-MDM-API' #WORK-FILE1.CIS-TIAA-NBR
        if (Global.isEscape()) return;
        //*  RESET #COR-CNTRCT-UNIVERSAL-DATA CORA805 #DOI #COR-ERROR
        //*                                                           /* PINE >>
        //*  RESET #MDMA300 #DOI #COR-ERROR                         /* CM 07/22/09
        //*  CM 07/22/09
        //*  PINE <<
        pdaMdma301.getPnd_Mdma301().reset();                                                                                                                              //Natural: RESET #MDMA301 #DOI #COR-ERROR
        pnd_Doi.reset();
        pnd_Cor_Error.reset();
        pdaMdma301.getPnd_Mdma301_Pnd_Requestor().setValue("CIS");                                                                                                        //Natural: ASSIGN #MDMA301.#REQUESTOR := 'CIS'
        //*  022908 START
        //*  BIP
        if (condition(pnd_Mdo_Cntr_Type_S.getBoolean() || pnd_Bene_In_Plan.getBoolean()))                                                                                 //Natural: IF #MDO-CNTR-TYPE-S OR #BENE-IN-PLAN
        {
            //*  061808
                                                                                                                                                                          //Natural: PERFORM READ-COR-BY-SSN
            sub_Read_Cor_By_Ssn();
            if (condition(Global.isEscape())) {return;}
            //*  061808
            //*  PINE
            if (condition(! (pnd_On_Cor.getBoolean())))                                                                                                                   //Natural: IF NOT #ON-COR
            {
                pdaMdma301.getPnd_Mdma301_Pnd_Function_Code().setValue("003");                                                                                            //Natural: ASSIGN #MDMA301.#FUNCTION-CODE := '003'
                //*  BIP
                //*  PINE <<
                if (condition(pnd_Mdo_Cntr_Type_S.getBoolean()))                                                                                                          //Natural: IF #MDO-CNTR-TYPE-S
                {
                    pdaMdma301.getPnd_Mdma301_Pnd_Soc_Sec_Nbr().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Ssn);                                                                 //Natural: ASSIGN #MDMA301.#SOC-SEC-NBR := #MDO-INFO.CIS-FRST-ANNT-SSN
                    pdaMdma301.getPnd_Mdma301_Pnd_Prefix().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Prfx);                                                                     //Natural: ASSIGN #MDMA301.#PREFIX := #MDO-INFO.CIS-FRST-ANNT-PRFX
                    pdaMdma301.getPnd_Mdma301_Pnd_First_Name().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Frst_Nme);                                                             //Natural: ASSIGN #MDMA301.#FIRST-NAME := #MDO-INFO.CIS-FRST-ANNT-FRST-NME
                    pdaMdma301.getPnd_Mdma301_Pnd_Middle_Name().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Mid_Nme);                                                             //Natural: ASSIGN #MDMA301.#MIDDLE-NAME := #MDO-INFO.CIS-FRST-ANNT-MID-NME
                    pdaMdma301.getPnd_Mdma301_Pnd_Last_Name().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Lst_Nme);                                                               //Natural: ASSIGN #MDMA301.#LAST-NAME := #MDO-INFO.CIS-FRST-ANNT-LST-NME
                    pdaMdma301.getPnd_Mdma301_Pnd_Suffix().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Sffx);                                                                     //Natural: ASSIGN #MDMA301.#SUFFIX := #MDO-INFO.CIS-FRST-ANNT-SFFX
                    pdaMdma301.getPnd_Mdma301_Pnd_Date_Of_Birth().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Dob);                                                               //Natural: ASSIGN #MDMA301.#DATE-OF-BIRTH := #MDO-INFO.CIS-FRST-ANNT-DOB
                    //*  PINE
                    if (condition(pnd_Mdo_Info_Cis_Frst_Annt_Sex_Cde.equals(" ")))                                                                                        //Natural: IF #MDO-INFO.CIS-FRST-ANNT-SEX-CDE = ' '
                    {
                        pdaMdma301.getPnd_Mdma301_Pnd_Sex_Code().setValue("U");                                                                                           //Natural: ASSIGN #MDMA301.#SEX-CODE := 'U'
                        //* PINE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaMdma301.getPnd_Mdma301_Pnd_Sex_Code().setValue(pnd_Mdo_Info_Cis_Frst_Annt_Sex_Cde);                                                            //Natural: ASSIGN #MDMA301.#SEX-CODE := #MDO-INFO.CIS-FRST-ANNT-SEX-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  PINE <<
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaMdma301.getPnd_Mdma301_Pnd_Soc_Sec_Nbr().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Ssn);                                                                //Natural: ASSIGN #MDMA301.#SOC-SEC-NBR := #TIAA-INFO.CIS-FRST-ANNT-SSN
                    pdaMdma301.getPnd_Mdma301_Pnd_Prefix().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Prfx);                                                                    //Natural: ASSIGN #MDMA301.#PREFIX := #TIAA-INFO.CIS-FRST-ANNT-PRFX
                    pdaMdma301.getPnd_Mdma301_Pnd_First_Name().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Frst_Nme);                                                            //Natural: ASSIGN #MDMA301.#FIRST-NAME := #TIAA-INFO.CIS-FRST-ANNT-FRST-NME
                    pdaMdma301.getPnd_Mdma301_Pnd_Middle_Name().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Mid_Nme);                                                            //Natural: ASSIGN #MDMA301.#MIDDLE-NAME := #TIAA-INFO.CIS-FRST-ANNT-MID-NME
                    pdaMdma301.getPnd_Mdma301_Pnd_Last_Name().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Lst_Nme);                                                              //Natural: ASSIGN #MDMA301.#LAST-NAME := #TIAA-INFO.CIS-FRST-ANNT-LST-NME
                    pdaMdma301.getPnd_Mdma301_Pnd_Suffix().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Sffx);                                                                    //Natural: ASSIGN #MDMA301.#SUFFIX := #TIAA-INFO.CIS-FRST-ANNT-SFFX
                    pdaMdma301.getPnd_Mdma301_Pnd_Date_Of_Birth().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Dob);                                                              //Natural: ASSIGN #MDMA301.#DATE-OF-BIRTH := #TIAA-INFO.CIS-FRST-ANNT-DOB
                    //*  PINE
                    if (condition(pnd_Tiaa_Info_Cis_Frst_Annt_Sex_Cde.equals(" ")))                                                                                       //Natural: IF #TIAA-INFO.CIS-FRST-ANNT-SEX-CDE = ' '
                    {
                        pdaMdma301.getPnd_Mdma301_Pnd_Sex_Code().setValue("U");                                                                                           //Natural: ASSIGN #MDMA301.#SEX-CODE := 'U'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaMdma301.getPnd_Mdma301_Pnd_Sex_Code().setValue(pnd_Tiaa_Info_Cis_Frst_Annt_Sex_Cde);                                                           //Natural: ASSIGN #MDMA301.#SEX-CODE := #TIAA-INFO.CIS-FRST-ANNT-SEX-CDE
                        //*                                                           /* PINE <<
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BIP
                }                                                                                                                                                         //Natural: END-IF
                //*  (3262)                         /* 061808
                //*  061808
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaMdma301.getPnd_Mdma301_Pnd_Record_Type().setValue(1);                                                                                                  //Natural: ASSIGN #MDMA301.#RECORD-TYPE := 01
                //*    IF #TIAA-INFO.CIS-SG-TEXT-UDF-3 = 'MDM'                     /* IARPF
                //*  C499005
                if (condition(pnd_Mdo_Info_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                              //Natural: IF #MDO-INFO.CIS-SG-TEXT-UDF-3 = 'MDM'
                {
                    //*  IARPF
                    pdaMdma301.getPnd_Mdma301_Pnd_Function_Code().setValue("010");                                                                                        //Natural: MOVE '010' TO #MDMA301.#FUNCTION-CODE
                    //*  IARPF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IARPF
                    pdaMdma301.getPnd_Mdma301_Pnd_Function_Code().setValue("005");                                                                                        //Natural: MOVE '005' TO #MDMA301.#FUNCTION-CODE
                    //*  IARPF
                }                                                                                                                                                         //Natural: END-IF
                ldaCisl400.getCisl400_Cis_Pin_Nbr().setValue(pnd_Cor_Api_Pin);                                                                                            //Natural: ASSIGN CISL400.CIS-PIN-NBR := #COR-API-PIN
                //*  (3262)                         /* 061808
            }                                                                                                                                                             //Natural: END-IF
            //*  (3256)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaMdma301.getPnd_Mdma301_Pnd_Record_Type().setValue(1);                                                                                                      //Natural: ASSIGN #MDMA301.#RECORD-TYPE := 01
            //*  IARPF
            if (condition(pnd_Tiaa_Info_Cis_Sg_Text_Udf_3.equals("MDM")))                                                                                                 //Natural: IF #TIAA-INFO.CIS-SG-TEXT-UDF-3 = 'MDM'
            {
                //*  IARPF
                pdaMdma301.getPnd_Mdma301_Pnd_Function_Code().setValue("010");                                                                                            //Natural: MOVE '010' TO #MDMA301.#FUNCTION-CODE
                //*  IARPF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  IARPF
                pdaMdma301.getPnd_Mdma301_Pnd_Function_Code().setValue("005");                                                                                            //Natural: MOVE '005' TO #MDMA301.#FUNCTION-CODE
                //*  IARPF
            }                                                                                                                                                             //Natural: END-IF
            //*  (3256)                            /* 022908 END
            //*  PINE <<
        }                                                                                                                                                                 //Natural: END-IF
        pdaMdma301.getPnd_Mdma301_Pnd_Pin_A12().setValue(ldaCisl400.getCisl400_Cis_Pin_Nbr());                                                                            //Natural: ASSIGN #MDMA301.#PIN-A12 := CISL400.CIS-PIN-NBR
        pdaMdma301.getPnd_Mdma301_Pnd_Contract_Table_Count().setValue(1);                                                                                                 //Natural: ASSIGN #MDMA301.#CONTRACT-TABLE-COUNT := 1
        pdaMdma301.getPnd_Mdma301_Pnd_Contract().getValue(1).setValue(ldaCisl400.getCisl400_Cis_Tiaa_Nbr());                                                              //Natural: ASSIGN #MDMA301.#CONTRACT ( 1 ) := CISL400.CIS-TIAA-NBR
        pdaMdma301.getPnd_Mdma301_Pnd_Issue_Date().getValue(1).setValue(ldaCisl400.getCisl400_Cis_Doi());                                                                 //Natural: ASSIGN #MDMA301.#ISSUE-DATE ( 1 ) := CISL400.CIS-DOI
        pnd_Doi.setValue(ldaCisl400.getCisl400_Cis_Doi());                                                                                                                //Natural: ASSIGN #DOI := CISL400.CIS-DOI
        pdaMdma301.getPnd_Mdma301_Pnd_Status_Code().getValue(1).setValue("H");                                                                                            //Natural: ASSIGN #MDMA301.#STATUS-CODE ( 1 ) := 'H'
        pdaMdma301.getPnd_Mdma301_Pnd_Status_Year().getValue(1).setValue(pnd_Doi_Pnd_Doi_Ccyy);                                                                           //Natural: ASSIGN #MDMA301.#STATUS-YEAR ( 1 ) := #DOI-CCYY
        pdaMdma301.getPnd_Mdma301_Pnd_Payee_Code().getValue(1).setValue(ldaCisl400.getCisl400_Cis_Cor_Cntrct_Payee_Cde());                                                //Natural: ASSIGN #MDMA301.#PAYEE-CODE ( 1 ) := CISL400.CIS-COR-CNTRCT-PAYEE-CDE
        pnd_Icp_Contract.setValue(ldaCisl400.getCisl400_Cis_Tiaa_Nbr().getSubstring(1,2));                                                                                //Natural: ASSIGN #ICP-CONTRACT := SUBSTRING ( CISL400.CIS-TIAA-NBR,1,2 )
        //*  WRITE '=' #ICP-CONTRACT 'DDDDDDD' '=' #MDMA300.#PIN      /* PINE
        //*  PINE
        getReports().write(0, "=",pnd_Icp_Contract,"DDDDDDD","=",pdaMdma301.getPnd_Mdma301_Pnd_Pin_N12());                                                                //Natural: WRITE '=' #ICP-CONTRACT 'DDDDDDD' '=' #MDMA301.#PIN-N12
        if (Global.isEscape()) return;
        //*  090606
        if (condition(DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'")))                                                                                           //Natural: IF #WORK-FILE1.CIS-RQST-ID = MASK ( 'MDO' )
        {
            //*  RESET #MDMA300.#IA-PRODUCT-CODE(1)                       /* PINE >>
            //*  PINE <<
            pdaMdma301.getPnd_Mdma301_Pnd_Ia_Product_Code().getValue(1).reset();                                                                                          //Natural: RESET #MDMA301.#IA-PRODUCT-CODE ( 1 ) #MDMA301.#IA-OPTION-CODE ( 1 )
            pdaMdma301.getPnd_Mdma301_Pnd_Ia_Option_Code().getValue(1).reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*  PINE <<
        if (condition(pnd_Icp_Contract.notEquals("II") && pnd_Icp_Contract.notEquals("IT")))                                                                              //Natural: IF #ICP-CONTRACT NE 'II' AND #ICP-CONTRACT NE 'IT'
        {
            pdaMdma301.getPnd_Mdma301_Pnd_Ia_Product_Code().getValue(1).setValue(ldaCisl400.getCisl400_Cis_Product_Code());                                               //Natural: ASSIGN #MDMA301.#IA-PRODUCT-CODE ( 1 ) := CISL400.CIS-PRODUCT-CODE
            pdaMdma301.getPnd_Mdma301_Pnd_Ia_Option_Code().getValue(1).setValue(ldaCisl400.getCisl400_Cis_Annty_Option());                                                //Natural: ASSIGN #MDMA301.#IA-OPTION-CODE ( 1 ) := CISL400.CIS-ANNTY-OPTION
        }                                                                                                                                                                 //Natural: END-IF
        //*  PINE
        if (condition(pnd_Icp_Contract.notEquals("II") || pnd_Icp_Contract.notEquals("IT")))                                                                              //Natural: IF #ICP-CONTRACT NE 'II' OR #ICP-CONTRACT NE 'IT'
        {
            pdaMdma301.getPnd_Mdma301_Pnd_Payee_Code().getValue(1).setValue("01");                                                                                        //Natural: ASSIGN #MDMA301.#PAYEE-CODE ( 1 ) := '01'
        }                                                                                                                                                                 //Natural: END-IF
        //*  090606
        if (condition(DbsUtil.maskMatches(pnd_Work_File1_Cis_Rqst_Id,"'MDO'")))                                                                                           //Natural: IF #WORK-FILE1.CIS-RQST-ID = MASK ( 'MDO' )
        {
            //*                                                           /* PINE >>
            //*  RESET #MDMA300.#IA-PRODUCT-CODE(1) #MDMA300.#IA-OPTION-CODE(1)
            //*  #MDMA300.#PAYEE-CODE(1)        := '01'
            //*  #MDMA300.#CREF-CONTRACT(1)     := CISL400.CIS-CERT-NBR
            //*  #MDMA300.#CREF-ISSUED-IND(1)   := 'Y'
            //*  #MDMA300.#DA-OWNERSHIP-CODE(1) := '0'
            //*  PINE <<
            pdaMdma301.getPnd_Mdma301_Pnd_Ia_Product_Code().getValue(1).reset();                                                                                          //Natural: RESET #MDMA301.#IA-PRODUCT-CODE ( 1 ) #MDMA301.#IA-OPTION-CODE ( 1 )
            pdaMdma301.getPnd_Mdma301_Pnd_Ia_Option_Code().getValue(1).reset();
            pdaMdma301.getPnd_Mdma301_Pnd_Payee_Code().getValue(1).setValue("01");                                                                                        //Natural: ASSIGN #MDMA301.#PAYEE-CODE ( 1 ) := '01'
            pdaMdma301.getPnd_Mdma301_Pnd_Cref_Contract().getValue(1).setValue(ldaCisl400.getCisl400_Cis_Cert_Nbr());                                                     //Natural: ASSIGN #MDMA301.#CREF-CONTRACT ( 1 ) := CISL400.CIS-CERT-NBR
            pdaMdma301.getPnd_Mdma301_Pnd_Cref_Issued_Ind().getValue(1).setValue("Y");                                                                                    //Natural: ASSIGN #MDMA301.#CREF-ISSUED-IND ( 1 ) := 'Y'
            pdaMdma301.getPnd_Mdma301_Pnd_Da_Ownership_Code().getValue(1).setValue("0");                                                                                  //Natural: ASSIGN #MDMA301.#DA-OWNERSHIP-CODE ( 1 ) := '0'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Mdm_Cntrct_Universal_Data_Pnd_Mdm_Cntrct_Universal_Arr.getValue(3).setValue("S");                                                                             //Natural: ASSIGN #MDM-CNTRCT-UNIVERSAL-ARR ( 3 ) := 'S'
        pdaMdma301.getPnd_Mdma301_Pnd_Cntrct_Universal_Data().getValue(1).setValue(pnd_Mdm_Cntrct_Universal_Data);                                                        //Natural: ASSIGN #MDMA301.#CNTRCT-UNIVERSAL-DATA ( 1 ) := #MDM-CNTRCT-UNIVERSAL-DATA
        //*                                                           /* PINE <<
        //*  CALLNAT 'CORN805' CORA805
        //*  BIP
        if (condition(pnd_Work_File1_Cis_Decedent_Contract.greater(" ")))                                                                                                 //Natural: IF #WORK-FILE1.CIS-DECEDENT-CONTRACT GT ' '
        {
            //*  PERFORM MOVE-MDMA300-TO-MDMA370                          /* PINE
            //*  PINE
                                                                                                                                                                          //Natural: PERFORM MOVE-MDMA301-TO-MDMA371
            sub_Move_Mdma301_To_Mdma371();
            if (condition(Global.isEscape())) {return;}
            //*  WRITE  'BEFORE CALLING  MDMN370A' /              /* BIP  /* PINE
            //*  BIP  /* PINE
            getReports().write(0, "BEFORE CALLING  MDMN371A",NEWLINE);                                                                                                    //Natural: WRITE 'BEFORE CALLING  MDMN371A' /
            if (Global.isEscape()) return;
            //*  BIP
            getReports().write(0, "=======================",NEWLINE);                                                                                                     //Natural: WRITE '=======================' /
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DISPLAY-MDMA371
            sub_Display_Mdma371();
            if (condition(Global.isEscape())) {return;}
            //*  WRITE  'CALLING MDMN370A'                        /* BIP  /* PINE >>
            //*  CALLNAT 'MDMN370A' #MDMA370                      /* BIP
            //*  WRITE  'AFTER CALLING  MDMN370A' /               /* BIP
            //*  BIP
            getReports().write(0, "CALLING MDMN371A");                                                                                                                    //Natural: WRITE 'CALLING MDMN371A'
            if (Global.isEscape()) return;
            //*  BIP
            DbsUtil.callnat(Mdmn371a.class , getCurrentProcessState(), pdaMdma371.getPnd_Mdma371());                                                                      //Natural: CALLNAT 'MDMN371A' #MDMA371
            if (condition(Global.isEscape())) return;
            //*  BIP  /* PINE <<
            getReports().write(0, "AFTER CALLING  MDMN371A",NEWLINE);                                                                                                     //Natural: WRITE 'AFTER CALLING  MDMN371A' /
            if (Global.isEscape()) return;
            //*  BIP
            getReports().write(0, "=======================",NEWLINE);                                                                                                     //Natural: WRITE '=======================' /
            if (Global.isEscape()) return;
            //*  PERFORM DISPLAY-MDMA370                                  /* PINE
            //*  PINE
                                                                                                                                                                          //Natural: PERFORM DISPLAY-MDMA371
            sub_Display_Mdma371();
            if (condition(Global.isEscape())) {return;}
            //*  MOVE BY NAME #MDMA370.#O-RETURN-SECTION TO       /* BIP  /* PINE >>
            //*  BIP
            //*  BIP
            pdaMdma301.getPnd_Mdma301_Pnd_O_Return_Section().setValuesByName(pdaMdma371.getPnd_Mdma371_Pnd_O_Return_Section());                                           //Natural: MOVE BY NAME #MDMA371.#O-RETURN-SECTION TO #MDMA301.#O-RETURN-SECTION
            //*  MOVE #MDMA370.#O-DATA-SECTION (1:1361) TO        /* BIP
            //*  BIP
            //*  BIP /* PINE <<
            pdaMdma301.getPnd_Mdma301_Pnd_O_Data_Section().getValue(1,":",1361).setValue(pdaMdma371.getPnd_Mdma371_Pnd_O_Data_Section().getValue(1,":",                   //Natural: MOVE #MDMA371.#O-DATA-SECTION ( 1:1361 ) TO #MDMA301.#O-DATA-SECTION ( 1:1361 )
                1361));
            //*  BIP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  WRITE   'CALLING MDMN300A'                               /* PINE >>
            //*  CALLNAT 'MDMN300A' #MDMA300
            getReports().write(0, "CALLING MDMN301A");                                                                                                                    //Natural: WRITE 'CALLING MDMN301A'
            if (Global.isEscape()) return;
            //*  PINE <<
            DbsUtil.callnat(Mdmn301a.class , getCurrentProcessState(), pdaMdma301.getPnd_Mdma301());                                                                      //Natural: CALLNAT 'MDMN301A' #MDMA301
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #MDMA300.#RETURN-CODE NE 00                   /* PINE
        //*  PINE
        if (condition(pdaMdma301.getPnd_Mdma301_Pnd_Return_Code().notEquals(0)))                                                                                          //Natural: IF #MDMA301.#RETURN-CODE NE 00
        {
            //*  ERRRPT KG
            //*  ERRRPT KG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Cor_Error.setValue(true);                                                                                                                                 //Natural: ASSIGN #COR-ERROR := TRUE
            //*  WRITE '=' #MDMA300.#RETURN-CODE                          /* PINE >>
            //*  WRITE '=' #MDMA300.#RETURN-TEXT
            //*  WRITE '=' #MDMA300.#CONTRACT(1)
            getReports().write(0, "=",pdaMdma301.getPnd_Mdma301_Pnd_Return_Code());                                                                                       //Natural: WRITE '=' #MDMA301.#RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaMdma301.getPnd_Mdma301_Pnd_Return_Text());                                                                                       //Natural: WRITE '=' #MDMA301.#RETURN-TEXT
            if (Global.isEscape()) return;
            //*  PINE <<
            getReports().write(0, "=",pdaMdma301.getPnd_Mdma301_Pnd_Contract().getValue(1));                                                                              //Natural: WRITE '=' #MDMA301.#CONTRACT ( 1 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  PINE
    private void sub_Move_Mdma301_To_Mdma371() throws Exception                                                                                                           //Natural: MOVE-MDMA301-TO-MDMA371
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        //*  RESET #MDMA370                                           /* PINE >>
        //*  PINE <<
        //*  PINE
        pdaMdma371.getPnd_Mdma371().reset();                                                                                                                              //Natural: RESET #MDMA371
        pdaMdma371.getPnd_Mdma371_Pnd_Ssn_Or_Tin_Ind().setValue(pnd_Work_File1_Cis_Ssn_Tin_Ind);                                                                          //Natural: ASSIGN #MDMA371.#SSN-OR-TIN-IND := #WORK-FILE1.CIS-SSN-TIN-IND
        pdaMdma371.getPnd_Mdma371_Pnd_Decedent_Contract().getValue(1).setValue(pnd_Work_File1_Cis_Decedent_Contract);                                                     //Natural: ASSIGN #MDMA371.#DECEDENT-CONTRACT ( 1 ) := #WORK-FILE1.CIS-DECEDENT-CONTRACT
        pdaMdma371.getPnd_Mdma371_Pnd_Acceptance_Ind().getValue(1).setValue("N");                                                                                         //Natural: ASSIGN #MDMA371.#ACCEPTANCE-IND ( 1 ) := 'N'
        pdaMdma371.getPnd_Mdma371_Pnd_Relationship_To_Decedent().getValue(1).setValue(pnd_Work_File1_Cis_Relation_To_Decedent);                                           //Natural: ASSIGN #MDMA371.#RELATIONSHIP-TO-DECEDENT ( 1 ) := #WORK-FILE1.CIS-RELATION-TO-DECEDENT
        pdaMdma371.getPnd_Mdma371_Pnd_Requestor().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Requestor());                                                                    //Natural: ASSIGN #MDMA371.#REQUESTOR := #MDMA301.#REQUESTOR
        pdaMdma371.getPnd_Mdma371_Pnd_Function_Code().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Function_Code());                                                            //Natural: ASSIGN #MDMA371.#FUNCTION-CODE := #MDMA301.#FUNCTION-CODE
        pdaMdma371.getPnd_Mdma371_Pnd_Soc_Sec_Nbr().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Soc_Sec_Nbr());                                                                //Natural: ASSIGN #MDMA371.#SOC-SEC-NBR := #MDMA301.#SOC-SEC-NBR
        pdaMdma371.getPnd_Mdma371_Pnd_Prefix().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Prefix());                                                                          //Natural: ASSIGN #MDMA371.#PREFIX := #MDMA301.#PREFIX
        pdaMdma371.getPnd_Mdma371_Pnd_First_Name().setValue(pdaMdma301.getPnd_Mdma301_Pnd_First_Name());                                                                  //Natural: ASSIGN #MDMA371.#FIRST-NAME := #MDMA301.#FIRST-NAME
        pdaMdma371.getPnd_Mdma371_Pnd_Middle_Name().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Middle_Name());                                                                //Natural: ASSIGN #MDMA371.#MIDDLE-NAME := #MDMA301.#MIDDLE-NAME
        pdaMdma371.getPnd_Mdma371_Pnd_Last_Name().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Last_Name());                                                                    //Natural: ASSIGN #MDMA371.#LAST-NAME := #MDMA301.#LAST-NAME
        pdaMdma371.getPnd_Mdma371_Pnd_Suffix().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Suffix());                                                                          //Natural: ASSIGN #MDMA371.#SUFFIX := #MDMA301.#SUFFIX
        pdaMdma371.getPnd_Mdma371_Pnd_Date_Of_Birth().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Date_Of_Birth());                                                            //Natural: ASSIGN #MDMA371.#DATE-OF-BIRTH := #MDMA301.#DATE-OF-BIRTH
        pdaMdma371.getPnd_Mdma371_Pnd_Sex_Code().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Sex_Code());                                                                      //Natural: ASSIGN #MDMA371.#SEX-CODE := #MDMA301.#SEX-CODE
        pdaMdma371.getPnd_Mdma371_Pnd_Record_Type().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Record_Type());                                                                //Natural: ASSIGN #MDMA371.#RECORD-TYPE := #MDMA301.#RECORD-TYPE
        pdaMdma371.getPnd_Mdma371_Pnd_Function_Code().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Function_Code());                                                            //Natural: ASSIGN #MDMA371.#FUNCTION-CODE := #MDMA301.#FUNCTION-CODE
        pdaMdma371.getPnd_Mdma371_Pnd_Pin_N12().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Pin_N12());                                                                        //Natural: ASSIGN #MDMA371.#PIN-N12 := #MDMA301.#PIN-N12
        pdaMdma371.getPnd_Mdma371_Pnd_Contract_Table_Count().setValue(pdaMdma301.getPnd_Mdma301_Pnd_Contract_Table_Count());                                              //Natural: ASSIGN #MDMA371.#CONTRACT-TABLE-COUNT := #MDMA301.#CONTRACT-TABLE-COUNT
        pdaMdma371.getPnd_Mdma371_Pnd_Contract().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Contract().getValue(1));                                              //Natural: ASSIGN #MDMA371.#CONTRACT ( 1 ) := #MDMA301.#CONTRACT ( 1 )
        pdaMdma371.getPnd_Mdma371_Pnd_Issue_Date().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Issue_Date().getValue(1));                                          //Natural: ASSIGN #MDMA371.#ISSUE-DATE ( 1 ) := #MDMA301.#ISSUE-DATE ( 1 )
        pdaMdma371.getPnd_Mdma371_Pnd_Status_Code().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Status_Code().getValue(1));                                        //Natural: ASSIGN #MDMA371.#STATUS-CODE ( 1 ) := #MDMA301.#STATUS-CODE ( 1 )
        pdaMdma371.getPnd_Mdma371_Pnd_Status_Year().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Status_Year().getValue(1));                                        //Natural: ASSIGN #MDMA371.#STATUS-YEAR ( 1 ) := #MDMA301.#STATUS-YEAR ( 1 )
        pdaMdma371.getPnd_Mdma371_Pnd_Payee_Code().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Payee_Code().getValue(1));                                          //Natural: ASSIGN #MDMA371.#PAYEE-CODE ( 1 ) := #MDMA301.#PAYEE-CODE ( 1 )
        pdaMdma371.getPnd_Mdma371_Pnd_Payee_Code().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Payee_Code().getValue(1));                                          //Natural: ASSIGN #MDMA371.#PAYEE-CODE ( 1 ) := #MDMA301.#PAYEE-CODE ( 1 )
        pdaMdma371.getPnd_Mdma371_Pnd_Ia_Product_Code().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Ia_Product_Code().getValue(1));                                //Natural: ASSIGN #MDMA371.#IA-PRODUCT-CODE ( 1 ) := #MDMA301.#IA-PRODUCT-CODE ( 1 )
        pdaMdma371.getPnd_Mdma371_Pnd_Ia_Option_Code().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Ia_Option_Code().getValue(1));                                  //Natural: ASSIGN #MDMA371.#IA-OPTION-CODE ( 1 ) := #MDMA301.#IA-OPTION-CODE ( 1 )
        pdaMdma371.getPnd_Mdma371_Pnd_Cref_Contract().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Cref_Contract().getValue(1));                                    //Natural: ASSIGN #MDMA371.#CREF-CONTRACT ( 1 ) := #MDMA301.#CREF-CONTRACT ( 1 )
        pdaMdma371.getPnd_Mdma371_Pnd_Cref_Issued_Ind().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Cref_Issued_Ind().getValue(1));                                //Natural: ASSIGN #MDMA371.#CREF-ISSUED-IND ( 1 ) := #MDMA301.#CREF-ISSUED-IND ( 1 )
        pdaMdma371.getPnd_Mdma371_Pnd_Da_Ownership_Code().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Da_Ownership_Code().getValue(1));                            //Natural: ASSIGN #MDMA371.#DA-OWNERSHIP-CODE ( 1 ) := #MDMA301.#DA-OWNERSHIP-CODE ( 1 )
        pdaMdma371.getPnd_Mdma371_Pnd_Cntrct_Universal_Data().getValue(1).setValue(pdaMdma301.getPnd_Mdma301_Pnd_Cntrct_Universal_Data().getValue(1));                    //Natural: ASSIGN #MDMA371.#CNTRCT-UNIVERSAL-DATA ( 1 ) := #MDMA301.#CNTRCT-UNIVERSAL-DATA ( 1 )
    }
    //*  NEW SUBROUTINE 061808
    private void sub_Read_Cor_By_Ssn() throws Exception                                                                                                                   //Natural: READ-COR-BY-SSN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        //*  RESET #ON-COR #COR-API-PIN #MDMA100                      /* PINE
        //*  PINE
        pnd_On_Cor.reset();                                                                                                                                               //Natural: RESET #ON-COR #COR-API-PIN #MDMA101
        pnd_Cor_Api_Pin.reset();
        pdaMdma101.getPnd_Mdma101().reset();
        pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn().setValue(pnd_Work_File1_Cis_Frst_Annt_Ssn);                                                                                 //Natural: ASSIGN #I-SSN := #WORK-FILE1.CIS-FRST-ANNT-SSN
        //*  WRITE  'CALLING MDMN100A: WITH SSN' #I-SSN               /* PINE >>
        //*  CALLNAT 'MDMN100A' #MDMA100                              /* PINE >>
        getReports().write(0, "CALLING MDMN101A: WITH SSN",pdaMdma101.getPnd_Mdma101_Pnd_I_Ssn());                                                                        //Natural: WRITE 'CALLING MDMN101A: WITH SSN' #I-SSN
        if (Global.isEscape()) return;
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*  IF NOT (#MDMA100.#O-RETURN-CODE = '0000' OR= 'A003')
        //*  PINE <<
        if (condition(! (pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000") || pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("A003"))))                  //Natural: IF NOT ( #MDMA101.#O-RETURN-CODE = '0000' OR = 'A003' )
        {
            //*  ERRRPT KG  /* BIP
            //*  ERRRPT KG
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-REPORT
            sub_Write_Error_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Cor_Error.setValue(true);                                                                                                                                 //Natural: ASSIGN #COR-ERROR := TRUE
            //*  WRITE 'INFO FROM MDMN100A CALL'                          /* PINE
            //*  PINE
            getReports().write(0, "INFO FROM MDMN101A CALL");                                                                                                             //Natural: WRITE 'INFO FROM MDMN101A CALL'
            if (Global.isEscape()) return;
            //*  WRITE '=' #MDMA100.#O-RETURN-CODE         /* BIP         /* PINE >>
            //*  WRITE '=' #MDMA100.#O-RETURN-TEXT (AL=79) /* BIP
            //*  BIP
            getReports().write(0, "=",pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code());                                                                                     //Natural: WRITE '=' #MDMA101.#O-RETURN-CODE
            if (Global.isEscape()) return;
            //*  BIP         /* PINE <<
            getReports().write(0, "=",pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Text(), new AlphanumericLength (79));                                                        //Natural: WRITE '=' #MDMA101.#O-RETURN-TEXT ( AL = 79 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #MDMA100.#O-RETURN-CODE NE '0000'                     /* PINE
        //*  PINE
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().notEquals("0000")))                                                                                   //Natural: IF #MDMA101.#O-RETURN-CODE NE '0000'
        {
            ignore();
            //*  PINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_On_Cor.setValue(true);                                                                                                                                    //Natural: ASSIGN #ON-COR := TRUE
            pnd_Cor_Api_Pin.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12());                                                                                          //Natural: ASSIGN #COR-API-PIN := #O-PIN-N12
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE  'BIP DISPLAY MDMN100A' #O-PIN                     /* PINE
        //*  PINE
        getReports().write(0, "BIP DISPLAY MDMN101A",pdaMdma101.getPnd_Mdma101_Pnd_O_Pin_N12());                                                                          //Natural: WRITE 'BIP DISPLAY MDMN101A' #O-PIN-N12
        if (Global.isEscape()) return;
    }
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        getReports().write(0, "Invoking OPEN :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                     //Natural: WRITE 'Invoking OPEN :' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        getReports().write(0, "Finished OPEN:",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                      //Natural: WRITE 'Finished OPEN:' *TIMX ( EM = HH:II:SS.T )
        if (Global.isEscape()) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0")))                                                                   //Natural: IF ##DATA-RESPONSE ( 1 ) = '0'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 60
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I)));            //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }
    private void sub_Display_Mdma371() throws Exception                                                                                                                   //Natural: DISPLAY-MDMA371
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        getReports().write(0, "MDMN371A INPUT PARMS:");                                                                                                                   //Natural: WRITE 'MDMN371A INPUT PARMS:'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Requestor(),NEWLINE);                                                                                     //Natural: WRITE '=' #MDMA371.#REQUESTOR /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Function_Code(),NEWLINE);                                                                                 //Natural: WRITE '=' #MDMA371.#FUNCTION-CODE /
        if (Global.isEscape()) return;
        //*  WRITE '=' #MDMA370.#PIN-A7 /                             /* PINE
        //*  PINE
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Pin_A12(),NEWLINE);                                                                                       //Natural: WRITE '=' #MDMA371.#PIN-A12 /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Record_Type(),NEWLINE);                                                                                   //Natural: WRITE '=' #MDMA371.#RECORD-TYPE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Soc_Sec_Nbr_A9(),NEWLINE);                                                                                //Natural: WRITE '=' #MDMA371.#SOC-SEC-NBR-A9 /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Foreign_Soc_Sec_Nbr_A9(),NEWLINE);                                                                        //Natural: WRITE '=' #MDMA371.#FOREIGN-SOC-SEC-NBR-A9 /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Negative_Election_Code(),NEWLINE);                                                                        //Natural: WRITE '=' #MDMA371.#NEGATIVE-ELECTION-CODE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Date_Of_Birth(),NEWLINE);                                                                                 //Natural: WRITE '=' #MDMA371.#DATE-OF-BIRTH /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Date_Of_Death(),NEWLINE);                                                                                 //Natural: WRITE '=' #MDMA371.#DATE-OF-DEATH /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Sex_Code(),NEWLINE);                                                                                      //Natural: WRITE '=' #MDMA371.#SEX-CODE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Occupation(),NEWLINE);                                                                                    //Natural: WRITE '=' #MDMA371.#OCCUPATION /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Prefix(),NEWLINE);                                                                                        //Natural: WRITE '=' #MDMA371.#PREFIX /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Last_Name(),NEWLINE);                                                                                     //Natural: WRITE '=' #MDMA371.#LAST-NAME /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_First_Name(),NEWLINE);                                                                                    //Natural: WRITE '=' #MDMA371.#FIRST-NAME /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Middle_Name(),NEWLINE);                                                                                   //Natural: WRITE '=' #MDMA371.#MIDDLE-NAME /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Suffix(),NEWLINE);                                                                                        //Natural: WRITE '=' #MDMA371.#SUFFIX /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Tlc_Table_Count(),NEWLINE);                                                                               //Natural: WRITE '=' #MDMA371.#TLC-TABLE-COUNT /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Tlc_Category().getValue("*"),NEWLINE);                                                                    //Natural: WRITE '=' #MDMA371.#TLC-CATEGORY ( * ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Tlc_Area_Of_Origin().getValue("*"),NEWLINE);                                                              //Natural: WRITE '=' #MDMA371.#TLC-AREA-OF-ORIGIN ( * ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Mail_Table_Count(),NEWLINE);                                                                              //Natural: WRITE '=' #MDMA371.#MAIL-TABLE-COUNT /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Mail_Code().getValue("*"),NEWLINE);                                                                       //Natural: WRITE '=' #MDMA371.#MAIL-CODE ( * ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Mail_Area_Of_Origin().getValue("*"),NEWLINE);                                                             //Natural: WRITE '=' #MDMA371.#MAIL-AREA-OF-ORIGIN ( * ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Institution(),NEWLINE);                                                                                   //Natural: WRITE '=' #MDMA371.#INSTITUTION /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Institution_Last_Date(),NEWLINE);                                                                         //Natural: WRITE '=' #MDMA371.#INSTITUTION-LAST-DATE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Institution_Ind(),NEWLINE);                                                                               //Natural: WRITE '=' #MDMA371.#INSTITUTION-IND /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Institution_Paidup_Ind(),NEWLINE);                                                                        //Natural: WRITE '=' #MDMA371.#INSTITUTION-PAIDUP-IND /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Institution_Number_Range_Code(),NEWLINE);                                                                 //Natural: WRITE '=' #MDMA371.#INSTITUTION-NUMBER-RANGE-CODE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Mf_Ownr_Cde(),NEWLINE);                                                                                   //Natural: WRITE '=' #MDMA371.#MF-OWNR-CDE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Ph_Universal_Data(), new AlphanumericLength (70),NEWLINE);                                                //Natural: WRITE '=' #MDMA371.#PH-UNIVERSAL-DATA ( AL = 70 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Ssn_Or_Tin_Ind(),NEWLINE);                                                                                //Natural: WRITE '=' #MDMA371.#SSN-OR-TIN-IND /
        if (Global.isEscape()) return;
        getReports().skip(0, 1);                                                                                                                                          //Natural: SKIP 1
        getReports().write(0, "CONTRACT DATA:");                                                                                                                          //Natural: WRITE 'CONTRACT DATA:'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Contract_Table_Count(),NEWLINE);                                                                          //Natural: WRITE '=' #MDMA371.#CONTRACT-TABLE-COUNT /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Contract().getValue(1),NEWLINE);                                                                          //Natural: WRITE '=' #MDMA371.#CONTRACT ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Issue_Date().getValue(1),NEWLINE);                                                                        //Natural: WRITE '=' #MDMA371.#ISSUE-DATE ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Status_Code().getValue(1),NEWLINE);                                                                       //Natural: WRITE '=' #MDMA371.#STATUS-CODE ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Status_Year().getValue(1),NEWLINE);                                                                       //Natural: WRITE '=' #MDMA371.#STATUS-YEAR ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Payee_Code().getValue(1),NEWLINE);                                                                        //Natural: WRITE '=' #MDMA371.#PAYEE-CODE ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Cref_Contract().getValue(1),NEWLINE);                                                                     //Natural: WRITE '=' #MDMA371.#CREF-CONTRACT ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Cref_Issued_Ind().getValue(1),NEWLINE);                                                                   //Natural: WRITE '=' #MDMA371.#CREF-ISSUED-IND ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Da_Ownership_Code().getValue(1),NEWLINE);                                                                 //Natural: WRITE '=' #MDMA371.#DA-OWNERSHIP-CODE ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Ia_Product_Code().getValue(1),NEWLINE);                                                                   //Natural: WRITE '=' #MDMA371.#IA-PRODUCT-CODE ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Ia_Option_Code().getValue("*"),NEWLINE);                                                                  //Natural: WRITE '=' #MDMA371.#IA-OPTION-CODE ( * ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Ia_Tax_Id().getValue(1),NEWLINE);                                                                         //Natural: WRITE '=' #MDMA371.#IA-TAX-ID ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Ins_Plan_Code().getValue(1),NEWLINE);                                                                     //Natural: WRITE '=' #MDMA371.#INS-PLAN-CODE ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Mf_Social_Cde().getValue(1),NEWLINE);                                                                     //Natural: WRITE '=' #MDMA371.#MF-SOCIAL-CDE ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Mf_Invstr_Nbr().getValue(1),NEWLINE);                                                                     //Natural: WRITE '=' #MDMA371.#MF-INVSTR-NBR ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Cntrct_Universal_Data().getValue(1), new AlphanumericLength (70),NEWLINE);                                //Natural: WRITE '=' #MDMA371.#CNTRCT-UNIVERSAL-DATA ( 1 ) ( AL = 70 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Decedent_Contract().getValue(1),NEWLINE);                                                                 //Natural: WRITE '=' #MDMA371.#DECEDENT-CONTRACT ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Acceptance_Ind().getValue(1),NEWLINE);                                                                    //Natural: WRITE '=' #MDMA371.#ACCEPTANCE-IND ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Relationship_To_Decedent().getValue(1),NEWLINE);                                                          //Natural: WRITE '=' #MDMA371.#RELATIONSHIP-TO-DECEDENT ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Pin(),NEWLINE);                                                                                         //Natural: WRITE '=' #MDMA371.#T-PIN /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Record_Type(),NEWLINE);                                                                                 //Natural: WRITE '=' #MDMA371.#T-RECORD-TYPE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Soc_Sec_Nbr(),NEWLINE);                                                                                 //Natural: WRITE '=' #MDMA371.#T-SOC-SEC-NBR /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Foreign_Soc_Sec_Nbr(),NEWLINE);                                                                         //Natural: WRITE '=' #MDMA371.#T-FOREIGN-SOC-SEC-NBR /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Negative_Election_Code(),NEWLINE);                                                                      //Natural: WRITE '=' #MDMA371.#T-NEGATIVE-ELECTION-CODE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Date_Of_Birth(),NEWLINE);                                                                               //Natural: WRITE '=' #MDMA371.#T-DATE-OF-BIRTH /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Date_Of_Death(),NEWLINE);                                                                               //Natural: WRITE '=' #MDMA371.#T-DATE-OF-DEATH /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Sex_Code(),NEWLINE);                                                                                    //Natural: WRITE '=' #MDMA371.#T-SEX-CODE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Occupation(),NEWLINE);                                                                                  //Natural: WRITE '=' #MDMA371.#T-OCCUPATION /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Prefix(),NEWLINE);                                                                                      //Natural: WRITE '=' #MDMA371.#T-PREFIX /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Last_Name(),NEWLINE);                                                                                   //Natural: WRITE '=' #MDMA371.#T-LAST-NAME /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_First_Name(),NEWLINE);                                                                                  //Natural: WRITE '=' #MDMA371.#T-FIRST-NAME /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Middle_Name(),NEWLINE);                                                                                 //Natural: WRITE '=' #MDMA371.#T-MIDDLE-NAME /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Suffix(),NEWLINE);                                                                                      //Natural: WRITE '=' #MDMA371.#T-SUFFIX /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Tlc_Table_Count(),NEWLINE);                                                                             //Natural: WRITE '=' #MDMA371.#T-TLC-TABLE-COUNT /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Tlc_Category().getValue("*"),NEWLINE);                                                                  //Natural: WRITE '=' #MDMA371.#T-TLC-CATEGORY ( * ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Tlc_Area_Of_Origin().getValue("*"),NEWLINE);                                                            //Natural: WRITE '=' #MDMA371.#T-TLC-AREA-OF-ORIGIN ( * ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Mail_Table_Count(),NEWLINE);                                                                            //Natural: WRITE '=' #MDMA371.#T-MAIL-TABLE-COUNT /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Mail_Code().getValue("*"),NEWLINE);                                                                     //Natural: WRITE '=' #MDMA371.#T-MAIL-CODE ( * ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Mail_Area_Of_Origin().getValue("*"),NEWLINE);                                                           //Natural: WRITE '=' #MDMA371.#T-MAIL-AREA-OF-ORIGIN ( * ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Institution(),NEWLINE);                                                                                 //Natural: WRITE '=' #MDMA371.#T-INSTITUTION /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Institution_Last_Date(),NEWLINE);                                                                       //Natural: WRITE '=' #MDMA371.#T-INSTITUTION-LAST-DATE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Institution_Ind(),NEWLINE);                                                                             //Natural: WRITE '=' #MDMA371.#T-INSTITUTION-IND /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Institution_Paidup_Ind(),NEWLINE);                                                                      //Natural: WRITE '=' #MDMA371.#T-INSTITUTION-PAIDUP-IND /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Institution_Number_Range_Code(),NEWLINE);                                                               //Natural: WRITE '=' #MDMA371.#T-INSTITUTION-NUMBER-RANGE-CODE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Mf_Ownr_Cde(),NEWLINE);                                                                                 //Natural: WRITE '=' #MDMA371.#T-MF-OWNR-CDE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_T_Universal_Data(), new AlphanumericLength (70),NEWLINE);                                                 //Natural: WRITE '=' #MDMA371.#T-UNIVERSAL-DATA ( AL = 70 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Continuation_Order(),NEWLINE);                                                                            //Natural: WRITE '=' #MDMA371.#CONTINUATION-ORDER /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Continuation_End(),NEWLINE);                                                                              //Natural: WRITE '=' #MDMA371.#CONTINUATION-END /
        if (Global.isEscape()) return;
        getReports().skip(0, 1);                                                                                                                                          //Natural: SKIP 1
        getReports().write(0, "CONTRACT DATA:");                                                                                                                          //Natural: WRITE 'CONTRACT DATA:'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Return_Code(),NEWLINE);                                                                                   //Natural: WRITE '=' #MDMA371.#RETURN-CODE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Return_Text(),NEWLINE);                                                                                   //Natural: WRITE '=' #MDMA371.#RETURN-TEXT /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_New_Pin(),NEWLINE);                                                                                       //Natural: WRITE '=' #MDMA371.#NEW-PIN /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_New_Record_Type(),NEWLINE);                                                                               //Natural: WRITE '=' #MDMA371.#NEW-RECORD-TYPE /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Current_Policyholder_Count(),NEWLINE);                                                                    //Natural: WRITE '=' #MDMA371.#CURRENT-POLICYHOLDER-COUNT /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Current_Pin().getValue(1),NEWLINE);                                                                       //Natural: WRITE '=' #MDMA371.#CURRENT-PIN ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Current_Record_Type().getValue(1),NEWLINE);                                                               //Natural: WRITE '=' #MDMA371.#CURRENT-RECORD-TYPE ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Current_Soc_Sec_Nbr().getValue(1),NEWLINE);                                                               //Natural: WRITE '=' #MDMA371.#CURRENT-SOC-SEC-NBR ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Current_Prefix().getValue(1),NEWLINE);                                                                    //Natural: WRITE '=' #MDMA371.#CURRENT-PREFIX ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Current_Last_Name().getValue(1),NEWLINE);                                                                 //Natural: WRITE '=' #MDMA371.#CURRENT-LAST-NAME ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Current_First_Name().getValue(1),NEWLINE);                                                                //Natural: WRITE '=' #MDMA371.#CURRENT-FIRST-NAME ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Current_Middle_Name().getValue(1),NEWLINE);                                                               //Natural: WRITE '=' #MDMA371.#CURRENT-MIDDLE-NAME ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Current_Suffix().getValue(1),NEWLINE);                                                                    //Natural: WRITE '=' #MDMA371.#CURRENT-SUFFIX ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Current_Date_Of_Birth().getValue(1),NEWLINE);                                                             //Natural: WRITE '=' #MDMA371.#CURRENT-DATE-OF-BIRTH ( 1 ) /
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Current_Sex_Code().getValue(1),NEWLINE);                                                                  //Natural: WRITE '=' #MDMA371.#CURRENT-SEX-CODE ( 1 ) /
        if (Global.isEscape()) return;
        //*  PINE <<
        getReports().write(0, "=",pdaMdma371.getPnd_Mdma371_Pnd_Contract_Error_Occurrence(),NEWLINE);                                                                     //Natural: WRITE '=' #MDMA371.#CONTRACT-ERROR-OCCURRENCE /
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  PINE <<
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),NEWLINE,new TabSetting(53),"COR FEED REPORT",NEWLINE,new TabSetting(48),"CONSOLIDATED ISSUE SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM / 53T 'COR FEED REPORT' / 48T 'CONSOLIDATED ISSUE SYSTEM' 110T 'RUN DATE:' *DATU / 1T '-' ( 131 ) / 1T 'REQ ID' 10T 'FUNCT' 16T 'PIN#' 30T 'SSN NUMBER' 42T 'NAME' 74T 'DOB' 84T 'TIAA#' 95T 'CREF#' 106T 'ISS DATE' 117T 'PAYEE PRD OPT' / 1T '-' ( 131 ) /
                        TabSetting(110),"RUN DATE:",Global.getDATU(),NEWLINE,new TabSetting(1),"-",new RepeatItem(131),NEWLINE,new TabSetting(1),"REQ ID",new 
                        TabSetting(10),"FUNCT",new TabSetting(16),"PIN#",new TabSetting(30),"SSN NUMBER",new TabSetting(42),"NAME",new TabSetting(74),"DOB",new 
                        TabSetting(84),"TIAA#",new TabSetting(95),"CREF#",new TabSetting(106),"ISS DATE",new TabSetting(117),"PAYEE PRD OPT",NEWLINE,new 
                        TabSetting(1),"-",new RepeatItem(131),NEWLINE);
                    //*    1T 'REQ ID' 10T 'FUNCT' 16T 'PIN#' 25T 'SSN NUMBER' 37T 'NAME'
                    //*    69T 'DOB' 79T 'TIAA#' 90T 'CREF#' 101T 'ISS DATE'
                    //*    112T 'PAYEE PRD OPT' /
                    //*  ERRRPT KG START
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  PINE <<
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),NEWLINE,new TabSetting(51),"COR OMNI ERROR REPORT",NEWLINE,new TabSetting(48),"CONSOLIDATED ISSUE SYSTEM",new  //Natural: WRITE ( 2 ) NOTITLE *PROGRAM / 51T 'COR OMNI ERROR REPORT' / 48T 'CONSOLIDATED ISSUE SYSTEM' 110T 'RUN DATE:' *DATU / 1T '-' ( 137 ) / 80T 'CNTR' 86T 'ERR' 92T 'ERROR' / 1T 'FUNC' 7T 'PIN#' 20T 'SSN NUMBER' 32T 'LAST NAME' 48T 'TIAA#' 59T 'CREF#' 70T 'ISS DATE' 80T 'TYPE' 86T 'CODE' 92T 'MESSAGE' / 1T '-' ( 137 ) /
                        TabSetting(110),"RUN DATE:",Global.getDATU(),NEWLINE,new TabSetting(1),"-",new RepeatItem(137),NEWLINE,new TabSetting(80),"CNTR",new 
                        TabSetting(86),"ERR",new TabSetting(92),"ERROR",NEWLINE,new TabSetting(1),"FUNC",new TabSetting(7),"PIN#",new TabSetting(20),"SSN NUMBER",new 
                        TabSetting(32),"LAST NAME",new TabSetting(48),"TIAA#",new TabSetting(59),"CREF#",new TabSetting(70),"ISS DATE",new TabSetting(80),"TYPE",new 
                        TabSetting(86),"CODE",new TabSetting(92),"MESSAGE",NEWLINE,new TabSetting(1),"-",new RepeatItem(137),NEWLINE);
                    //*    1T '-' (131) /
                    //*    75T 'CNTR' 81T 'ERR' 87T 'ERROR'  /
                    //*    1T 'FUNC' 7T 'PIN#' 15T 'SSN NUMBER' 27T 'LAST NAME'
                    //*    43T 'TIAA#' 54T 'CREF#' 65T 'ISS DATE' 75T 'TYPE'
                    //*    81T 'CODE' 87T 'MESSAGE' /
                    //*    1T '-'(131) /
                    //*  ERRRPT KG END
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        short decideConditionsMet1930 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF *ERROR-NR;//Natural: VALUE 3145
        if (condition((Global.getERROR_NR().equals(3145))))
        {
            decideConditionsMet1930++;
            if (condition(pnd_Retry_Cnt.less(25)))                                                                                                                        //Natural: IF #RETRY-CNT < 25
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.equals(25)))                                                                                                                      //Natural: IF #RETRY-CNT = 25
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                cmrollReturnCode = DbsUtil.callExternalProgram("CMROLL",intvl,reqid);                                                                                     //Natural: CALL 'CMROLL' INTVL REQID
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.greater(25) && pnd_Retry_Cnt.lessOrEqual(50)))                                                                                    //Natural: IF #RETRY-CNT > 25 AND #RETRY-CNT <= 50
            {
                pnd_Retry_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #RETRY-CNT
                OnErrorManager.popRetry();                                                                                                                                //Natural: RETRY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Retry_Cnt.greater(50)))                                                                                                                     //Natural: IF #RETRY-CNT > 50
            {
                getReports().write(0, ReportOption.NOHDR,"*****************************",NEWLINE);                                                                        //Natural: WRITE NOHDR '*****************************' /
                getReports().write(0, ReportOption.NOHDR,"* ADABAS RECORD ON HOLD     *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* ADABAS RECORD ON HOLD     *' /
                getReports().write(0, ReportOption.NOHDR,"* RETRY COUNT EXCEEDED.     *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* RETRY COUNT EXCEEDED.     *' /
                getReports().write(0, ReportOption.NOHDR,"* BACKOUT UPDATES.          *",NEWLINE);                                                                        //Natural: WRITE NOHDR '* BACKOUT UPDATES.          *' /
                getReports().write(0, ReportOption.NOHDR,"*****************************");                                                                                //Natural: WRITE NOHDR '*****************************'
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
                DbsUtil.terminate(5);  if (true) return;                                                                                                                  //Natural: TERMINATE 05
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 3148
        else if (condition((Global.getERROR_NR().equals(3148))))
        {
            decideConditionsMet1930++;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, ReportOption.NOHDR,"*****************************",NEWLINE);                                                                            //Natural: WRITE NOHDR '*****************************' /
            getReports().write(0, ReportOption.NOHDR,"* ADABAS FILE NOT AVAILABLE *",NEWLINE);                                                                            //Natural: WRITE NOHDR '* ADABAS FILE NOT AVAILABLE *' /
            getReports().write(0, ReportOption.NOHDR,"* ANY UPDATES BACKED OUT    *",NEWLINE);                                                                            //Natural: WRITE NOHDR '* ANY UPDATES BACKED OUT    *' /
            getReports().write(0, ReportOption.NOHDR,"*****************************",NEWLINE);                                                                            //Natural: WRITE NOHDR '*****************************' /
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"ERROR IN     ",Global.getPROGRAM(),NEWLINE,"ERROR NUMBER ",                  //Natural: WRITE NOHDR '****************************' / 'ERROR IN     ' *PROGRAM / 'ERROR NUMBER ' *ERROR-NR / 'ERROR LINE   ' *ERROR-LINE / '****************************' /
                Global.getERROR_NR(),NEWLINE,"ERROR LINE   ",Global.getERROR_LINE(),NEWLINE,"****************************",NEWLINE);
            DbsUtil.terminate(4);  if (true) return;                                                                                                                      //Natural: TERMINATE 4
        }                                                                                                                                                                 //Natural: END-DECIDE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=80");
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=138");
        Global.format(3, "PS=55 LS=139");
        Global.format(4, "PS=55 LS=139");

        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(56),"COR FEED ERROR REPORT",new TabSetting(110),"RUN DATE:",Global.getDATU(),NEWLINE,new 
            TabSetting(54),"CONSOLIDATED ISSUE SYSTEM",NEWLINE,new TabSetting(58),"FOR SIP CONTRACTS",NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(56),"COR FEED ERROR REPORT",new TabSetting(110),"RUN DATE:",Global.getDATU(),NEWLINE,new 
            TabSetting(54),"CONSOLIDATED ISSUE SYSTEM",NEWLINE,new TabSetting(50),"FOR MDO, TPA, AND IPRO CONTRACTS",NEWLINE);

        getReports().setDisplayColumns(3, "/FUNC",
        		pdaMdma301.getPnd_Mdma301_Pnd_Function_Code(),"/PIN",
        		ldaCisl400.getCisl400_Cis_Pin_Nbr(),"/SSN",
        		pnd_Ssn,"/LAST NAME",
        		pnd_Short_Name,"TIAA/CONTRACT",
        		ldaCisl400.getCisl400_Cis_Tiaa_Nbr(),"CREF/CONTRACT",
        		ldaCisl400.getCisl400_Cis_Cert_Nbr(),"ISSUE/DATE",
        		pnd_Idat_A,"CNTR/TYPE",
        		pnd_C_Type,"ERR/CODE",
        		pdaMdma301.getPnd_Mdma301_Pnd_Return_Code(),"/ERROR MESSAGE",
        		pnd_Error_Msg_Trunc);
        getReports().setDisplayColumns(4, "/FUNC",
        		pdaMdma301.getPnd_Mdma301_Pnd_Function_Code(),"/PIN",
        		ldaCisl400.getCisl400_Cis_Pin_Nbr(),"/SSN",
        		pnd_Ssn,"/LAST NAME",
        		pnd_Short_Name,"TIAA/CONTRACT",
        		ldaCisl400.getCisl400_Cis_Tiaa_Nbr(),"CREF/CONTRACT",
        		ldaCisl400.getCisl400_Cis_Cert_Nbr(),"ISSUE/DATE",
        		pnd_Idat_A,"CNTR/TYPE",
        		pnd_C_Type,"ERR/CODE",
        		pdaMdma301.getPnd_Mdma301_Pnd_Return_Code(),"/ERROR MESSAGE",
        		pnd_Error_Msg_Trunc);
    }
}
