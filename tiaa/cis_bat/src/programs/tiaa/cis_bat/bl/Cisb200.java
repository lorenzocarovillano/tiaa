/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:21:11 PM
**        * FROM NATURAL PROGRAM : Cisb200
************************************************************
**        * FILE NAME            : Cisb200.java
**        * CLASS NAME           : Cisb200
**        * INSTANCE NAME        : Cisb200
************************************************************
*
**----------------------------------------------------------------------
**                       M O D I F I C A T I O N S
**----------------------------------------------------------------------
**    DATE        MOD BY                 DESC OF CHANGE
*
* H. KAKADIA  09/13/04 CHANGE WAS DONE TO EXTRACT OMNI ISSUED TPA WITH
* CIS-COR-SYNC-IND EQ 'S'
*
* 09/06/06 O SOTTO CHANGED AS PART OF MDO LEGAL SPLIT. SC 090606.
*
** 01/14/2009  K. GATES   RESTOW FOR CISL200 FOR IA TIAA ACCESS PROJECT.
** 09/01/2015  B. ELLO    ADDED THE BENE IN THE PLAN FIELDS TO CISL200
**                        AND TO THE WORK FILE                     (BIP)
** 05/09/2017  (GHOSABE)  PIN EXPANSION CHANGES.(C420007)      PINE.
** 04/18/2017  B. NEWSOM  PROGRAM EXTRACTS RECORDS FROM THE CIS
**                        PARTICIPANT FILE THAT NEED TO BE SYNCHED UP IN
**                        MDM.                                   (IARPF)
**======================================================================
*

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb200 extends BLNatBase
{
    // Data Areas
    private LdaCisl200 ldaCisl200;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_File1;
    private DbsField pnd_Work_File1_Cis_Pin_Nbr;
    private DbsField pnd_Work_File1_Cis_Rqst_Id_Key;
    private DbsField pnd_Work_File1_Cis_Tiaa_Nbr;
    private DbsField pnd_Work_File1_Cis_Cert_Nbr;
    private DbsField pnd_Work_File1_Cis_Tiaa_Doi;

    private DbsGroup pnd_Work_File1__R_Field_1;
    private DbsField pnd_Work_File1_Cis_Tiaa_Doi_A;
    private DbsField pnd_Work_File1_Cis_Cref_Doi;

    private DbsGroup pnd_Work_File1__R_Field_2;
    private DbsField pnd_Work_File1_Cis_Cref_Doi_A;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Ssn;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Prfx;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Frst_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Mid_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Lst_Nme;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Sffx;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Dob;

    private DbsGroup pnd_Work_File1__R_Field_3;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Dob_A;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Sex_Cde;
    private DbsField pnd_Work_File1_Cis_Frst_Annt_Calc_Method;
    private DbsField pnd_Work_File1_Cis_Opn_Clsd_Ind;
    private DbsField pnd_Work_File1_Cis_Status_Cd;
    private DbsField pnd_Work_File1_Cis_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Cntrct_Apprvl_Ind;
    private DbsField pnd_Work_File1_Cis_Rqst_Id;
    private DbsField pnd_Work_File1_Cis_Hold_Cde;
    private DbsField pnd_Work_File1_Cis_Cntrct_Print_Dte;

    private DbsGroup pnd_Work_File1__R_Field_4;
    private DbsField pnd_Work_File1_Cis_Cntrct_Print_Dte_A;
    private DbsField pnd_Work_File1_Cis_Extract_Date;

    private DbsGroup pnd_Work_File1__R_Field_5;
    private DbsField pnd_Work_File1_Cis_Extract_Date_A;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_Dte;

    private DbsGroup pnd_Work_File1__R_Field_6;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_Dte_A;
    private DbsField pnd_Work_File1_Cis_Appl_Rcvd_User_Id;
    private DbsField pnd_Work_File1_Cis_Appl_Entry_User_Id;
    private DbsField pnd_Work_File1_Cis_Annty_Option;
    private DbsField pnd_Work_File1_Cis_Pymnt_Mode;
    private DbsField pnd_Work_File1_Cis_Annty_Start_Dte;

    private DbsGroup pnd_Work_File1__R_Field_7;
    private DbsField pnd_Work_File1_Cis_Annty_Start_Dte_A;
    private DbsField pnd_Work_File1_Cis_Annty_End_Dte;

    private DbsGroup pnd_Work_File1__R_Field_8;
    private DbsField pnd_Work_File1_Cis_Annty_End_Dte_A;
    private DbsField pnd_Work_File1_Cis_Grnted_Period_Yrs;
    private DbsField pnd_Work_File1_Cis_Grnted_Period_Dys;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Prfx;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Sffx;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Ssn;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Dob;

    private DbsGroup pnd_Work_File1__R_Field_9;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Dob_A;
    private DbsField pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde;

    private DbsGroup pnd_Work_File1_Cis_Cref_Annty_Pymnt;
    private DbsField pnd_Work_File1_Cis_Cref_Acct_Cde;
    private DbsField pnd_Work_File1_Cis_Orig_Issue_State;
    private DbsField pnd_Work_File1_Cis_Issue_State_Name;
    private DbsField pnd_Work_File1_Cis_Issue_State_Cd;
    private DbsField pnd_Work_File1_Cis_Ppg_Code;
    private DbsField pnd_Work_File1_Cis_Region_Code;
    private DbsField pnd_Work_File1_Cis_Ownership_Cd;
    private DbsField pnd_Work_File1_Cis_Bill_Code;
    private DbsField pnd_Work_File1_Cis_Lob;
    private DbsField pnd_Work_File1_Cis_Lob_Type;
    private DbsField pnd_Work_File1_Cis_Addr_Syn_Ind;
    private DbsField pnd_Work_File1_Cis_Addr_Process_Env;

    private DbsGroup pnd_Work_File1_Cis_Address_Info;
    private DbsField pnd_Work_File1_Cis_Address_Chg_Ind;
    private DbsField pnd_Work_File1_Cis_Address_Dest_Name;
    private DbsField pnd_Work_File1_Cis_Address_Txt;
    private DbsField pnd_Work_File1_Cis_Zip_Code;
    private DbsField pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr;
    private DbsField pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr;
    private DbsField pnd_Work_File1_Cis_Stndrd_Trn_Cd;
    private DbsField pnd_Work_File1_Cis_Finalist_Reason_Cd;
    private DbsField pnd_Work_File1_Cis_Addr_Usage_Code;
    private DbsField pnd_Work_File1_Cis_Checking_Saving_Cd;
    private DbsField pnd_Work_File1_Cis_Addr_Stndrd_Code;
    private DbsField pnd_Work_File1_Cis_Stndrd_Overide;
    private DbsField pnd_Work_File1_Cis_Postal_Data_Fields;
    private DbsField pnd_Work_File1_Cis_Geographic_Cd;
    private DbsField pnd_Work_File1_Cis_Corp_Sync_Ind;

    private DbsGroup pnd_Work_File1_Cis_Rqst_System_Mit_Dta;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Function;
    private DbsField pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env;
    private DbsField pnd_Work_File1_Cis_Mit_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Mit_Error_Msg;

    private DbsGroup pnd_Work_File1_Cis_Non_Premium_Dta;
    private DbsField pnd_Work_File1_Cis_Nonp_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Nonp_Process_Env;
    private DbsField pnd_Work_File1_Cis_Nonp_Effective_Dte;

    private DbsGroup pnd_Work_File1__R_Field_10;
    private DbsField pnd_Work_File1_Cis_Nonp_Effective_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Product_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte;

    private DbsGroup pnd_Work_File1__R_Field_11;
    private DbsField pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Currency;
    private DbsField pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt;
    private DbsField pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt;
    private DbsField pnd_Work_File1_Cis_Nonp_Deletion_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Vesting_Dte;

    private DbsGroup pnd_Work_File1__R_Field_12;
    private DbsField pnd_Work_File1_Cis_Nonp_Vesting_Dte_A;
    private DbsField pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt;
    private DbsField pnd_Work_File1_Cis_Nonp_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Nonp_Error_Msg;

    private DbsGroup pnd_Work_File1_Cis_Allocation_Dta;
    private DbsField pnd_Work_File1_Cis_Alloc_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Alloc_Process_Env;
    private DbsField pnd_Work_File1_Cis_Alloc_Effective_Dte;

    private DbsGroup pnd_Work_File1__R_Field_13;
    private DbsField pnd_Work_File1_Cis_Alloc_Effective_Dte_A;
    private DbsField pnd_Work_File1_Cis_Alloc_Percentage;
    private DbsField pnd_Work_File1_Cis_Alloc_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Alloc_Error_Msg;
    private DbsField pnd_Work_File1_Cis_Cor_Sync_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Process_Env;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Xref_Pin;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt;

    private DbsGroup pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Mail_Table_Cnt;

    private DbsGroup pnd_Work_File1_Cis_Cor_Ph_Mail_Grp;
    private DbsField pnd_Work_File1_Cis_Cor_Phmail_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin;
    private DbsField pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte;

    private DbsGroup pnd_Work_File1__R_Field_14;
    private DbsField pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind;
    private DbsField pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte;
    private DbsField pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde;
    private DbsField pnd_Work_File1_Cis_Cor_Error_Cde;
    private DbsField pnd_Work_File1_Cis_Pull_Code;
    private DbsField pnd_Work_File1_Cis_Trnsf_Flag;
    private DbsField pnd_Work_File1_Cis_Trnsf_From_Company;
    private DbsField pnd_Work_File1_Cis_Trnsf_To_Company;
    private DbsField pnd_Work_File1_Cis_Tiaa_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Rea_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Cref_Cntrct_Type;
    private DbsField pnd_Work_File1_Cis_Decedent_Contract;
    private DbsField pnd_Work_File1_Cis_Relation_To_Decedent;
    private DbsField pnd_Work_File1_Cis_Ssn_Tin_Ind;
    private DbsField pnd_Work_File1_Cis_Isn;
    private DbsField pnd_Work_File1_Cis_Mdo_Contract_Type;
    private DbsField pnd_Work_File1_Cis_Sg_Text_Udf_3;
    private DbsField pnd_Total;
    private DbsField pnd_Total_Ia;
    private DbsField pnd_Total_Ia_Cor;
    private DbsField pnd_Total_Ia_Mit;
    private DbsField pnd_Total_Ia_Alloc;
    private DbsField pnd_Total_Ia_Addr;
    private DbsField pnd_Total_Ia_Nonp;
    private DbsField pnd_Total_Tpa;
    private DbsField pnd_Total_Tpa_Cor;
    private DbsField pnd_Total_Tpa_Cor_Omni;
    private DbsField pnd_Total_Tpi_Cor_Omni;
    private DbsField pnd_Total_Tpa_Mit;
    private DbsField pnd_Total_Tpa_Alloc;
    private DbsField pnd_Total_Tpa_Addr;
    private DbsField pnd_Total_Tpa_Addr_Omni;
    private DbsField pnd_Total_Tpi_Addr_Omni;
    private DbsField pnd_Total_Tpa_Nonp;
    private DbsField pnd_Total_Ipro;
    private DbsField pnd_Total_Ipro_Cor;
    private DbsField pnd_Total_Ipro_Cor_Omni;
    private DbsField pnd_Total_Ipi_Cor_Omni;
    private DbsField pnd_Total_Ipro_Mit;
    private DbsField pnd_Total_Ipro_Alloc;
    private DbsField pnd_Total_Ipro_Addr;
    private DbsField pnd_Total_Ipro_Addr_Omni;
    private DbsField pnd_Total_Ipi_Addr_Omni;
    private DbsField pnd_Total_Ipro_Nonp;
    private DbsField pnd_Total_Mdo_Cor;
    private DbsField pnd_Total_Mdo_Mit;
    private DbsField pnd_Total_Mdo_Alloc;
    private DbsField pnd_Total_Mdo_Addr;
    private DbsField pnd_Total_Mdo_Nonp;
    private DbsField pnd_Total_Mdo;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Reject;
    private DbsField pnd_Total_Ia_Corp;
    private DbsField pnd_Total_Tpa_Corp;
    private DbsField pnd_Total_Ipro_Corp;
    private DbsField pnd_Total_Mdo_Corp;
    private DbsField pnd_Open_Key;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCisl200 = new LdaCisl200();
        registerRecord(ldaCisl200);
        registerRecord(ldaCisl200.getVw_cis_Prtcpnt_File());

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_File1 = localVariables.newGroupInRecord("pnd_Work_File1", "#WORK-FILE1");
        pnd_Work_File1_Cis_Pin_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Work_File1_Cis_Rqst_Id_Key = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 35);
        pnd_Work_File1_Cis_Tiaa_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Cert_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Tiaa_Doi = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_1 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_1", "REDEFINE", pnd_Work_File1_Cis_Tiaa_Doi);
        pnd_Work_File1_Cis_Tiaa_Doi_A = pnd_Work_File1__R_Field_1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Doi_A", "CIS-TIAA-DOI-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Cref_Doi = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_2 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_2", "REDEFINE", pnd_Work_File1_Cis_Cref_Doi);
        pnd_Work_File1_Cis_Cref_Doi_A = pnd_Work_File1__R_Field_2.newFieldInGroup("pnd_Work_File1_Cis_Cref_Doi_A", "CIS-CREF-DOI-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Ssn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Ssn", "CIS-FRST-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Work_File1_Cis_Frst_Annt_Prfx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Prfx", "CIS-FRST-ANNT-PRFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Frst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Frst_Nme", "CIS-FRST-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Mid_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Mid_Nme", "CIS-FRST-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Lst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Lst_Nme", "CIS-FRST-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Frst_Annt_Sffx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Sffx", "CIS-FRST-ANNT-SFFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Ctznshp_Cd", "CIS-FRST-ANNT-CTZNSHP-CD", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Rsdnc_Cde", "CIS-FRST-ANNT-RSDNC-CDE", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Frst_Annt_Dob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Dob", "CIS-FRST-ANNT-DOB", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_3 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_3", "REDEFINE", pnd_Work_File1_Cis_Frst_Annt_Dob);
        pnd_Work_File1_Cis_Frst_Annt_Dob_A = pnd_Work_File1__R_Field_3.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Dob_A", "CIS-FRST-ANNT-DOB-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Frst_Annt_Sex_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Sex_Cde", "CIS-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Frst_Annt_Calc_Method = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Frst_Annt_Calc_Method", "CIS-FRST-ANNT-CALC-METHOD", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Opn_Clsd_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Opn_Clsd_Ind", "CIS-OPN-CLSD-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Status_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Status_Cd", "CIS-STATUS-CD", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cntrct_Apprvl_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Apprvl_Ind", "CIS-CNTRCT-APPRVL-IND", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Rqst_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Hold_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Hold_Cde", "CIS-HOLD-CDE", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Cntrct_Print_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Print_Dte", "CIS-CNTRCT-PRINT-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_4 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_4", "REDEFINE", pnd_Work_File1_Cis_Cntrct_Print_Dte);
        pnd_Work_File1_Cis_Cntrct_Print_Dte_A = pnd_Work_File1__R_Field_4.newFieldInGroup("pnd_Work_File1_Cis_Cntrct_Print_Dte_A", "CIS-CNTRCT-PRINT-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Extract_Date = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Extract_Date", "CIS-EXTRACT-DATE", FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_5 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_5", "REDEFINE", pnd_Work_File1_Cis_Extract_Date);
        pnd_Work_File1_Cis_Extract_Date_A = pnd_Work_File1__R_Field_5.newFieldInGroup("pnd_Work_File1_Cis_Extract_Date_A", "CIS-EXTRACT-DATE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Rcvd_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_Dte", "CIS-APPL-RCVD-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_6 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_6", "REDEFINE", pnd_Work_File1_Cis_Appl_Rcvd_Dte);
        pnd_Work_File1_Cis_Appl_Rcvd_Dte_A = pnd_Work_File1__R_Field_6.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_Dte_A", "CIS-APPL-RCVD-DTE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Rcvd_User_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Rcvd_User_Id", "CIS-APPL-RCVD-USER-ID", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Appl_Entry_User_Id = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Appl_Entry_User_Id", "CIS-APPL-ENTRY-USER-ID", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Annty_Option = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 10);
        pnd_Work_File1_Cis_Pymnt_Mode = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pymnt_Mode", "CIS-PYMNT-MODE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Annty_Start_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_Start_Dte", "CIS-ANNTY-START-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_7 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_7", "REDEFINE", pnd_Work_File1_Cis_Annty_Start_Dte);
        pnd_Work_File1_Cis_Annty_Start_Dte_A = pnd_Work_File1__R_Field_7.newFieldInGroup("pnd_Work_File1_Cis_Annty_Start_Dte_A", "CIS-ANNTY-START-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Annty_End_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Annty_End_Dte", "CIS-ANNTY-END-DTE", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_8 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_8", "REDEFINE", pnd_Work_File1_Cis_Annty_End_Dte);
        pnd_Work_File1_Cis_Annty_End_Dte_A = pnd_Work_File1__R_Field_8.newFieldInGroup("pnd_Work_File1_Cis_Annty_End_Dte_A", "CIS-ANNTY-END-DTE-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Grnted_Period_Yrs = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Grnted_Period_Yrs", "CIS-GRNTED-PERIOD-YRS", FieldType.NUMERIC, 
            2);
        pnd_Work_File1_Cis_Grnted_Period_Dys = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Grnted_Period_Dys", "CIS-GRNTED-PERIOD-DYS", FieldType.NUMERIC, 
            3);
        pnd_Work_File1_Cis_Scnd_Annt_Prfx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Prfx", "CIS-SCND-ANNT-PRFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Frst_Nme", "CIS-SCND-ANNT-FRST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Mid_Nme", "CIS-SCND-ANNT-MID-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Lst_Nme", "CIS-SCND-ANNT-LST-NME", FieldType.STRING, 
            30);
        pnd_Work_File1_Cis_Scnd_Annt_Sffx = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Sffx", "CIS-SCND-ANNT-SFFX", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Ssn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Work_File1_Cis_Scnd_Annt_Dob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Dob", "CIS-SCND-ANNT-DOB", FieldType.NUMERIC, 
            8);

        pnd_Work_File1__R_Field_9 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_9", "REDEFINE", pnd_Work_File1_Cis_Scnd_Annt_Dob);
        pnd_Work_File1_Cis_Scnd_Annt_Dob_A = pnd_Work_File1__R_Field_9.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Dob_A", "CIS-SCND-ANNT-DOB-A", FieldType.STRING, 
            8);
        pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Scnd_Annt_Sex_Cde", "CIS-SCND-ANNT-SEX-CDE", FieldType.STRING, 
            1);

        pnd_Work_File1_Cis_Cref_Annty_Pymnt = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cref_Annty_Pymnt", "CIS-CREF-ANNTY-PYMNT", new DbsArrayController(1, 
            20));
        pnd_Work_File1_Cis_Cref_Acct_Cde = pnd_Work_File1_Cis_Cref_Annty_Pymnt.newFieldInGroup("pnd_Work_File1_Cis_Cref_Acct_Cde", "CIS-CREF-ACCT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Orig_Issue_State = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Orig_Issue_State", "CIS-ORIG-ISSUE-STATE", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Issue_State_Name = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Issue_State_Name", "CIS-ISSUE-STATE-NAME", FieldType.STRING, 
            15);
        pnd_Work_File1_Cis_Issue_State_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Issue_State_Cd", "CIS-ISSUE-STATE-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Ppg_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ppg_Code", "CIS-PPG-CODE", FieldType.STRING, 6);
        pnd_Work_File1_Cis_Region_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Region_Code", "CIS-REGION-CODE", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Ownership_Cd = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ownership_Cd", "CIS-OWNERSHIP-CD", FieldType.NUMERIC, 1);
        pnd_Work_File1_Cis_Bill_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Bill_Code", "CIS-BILL-CODE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Lob = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Lob", "CIS-LOB", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Lob_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Lob_Type", "CIS-LOB-TYPE", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Syn_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Addr_Syn_Ind", "CIS-ADDR-SYN-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Process_Env = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Addr_Process_Env", "CIS-ADDR-PROCESS-ENV", FieldType.STRING, 
            1);

        pnd_Work_File1_Cis_Address_Info = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Address_Info", "CIS-ADDRESS-INFO", new DbsArrayController(1, 
            3));
        pnd_Work_File1_Cis_Address_Chg_Ind = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Address_Chg_Ind", "CIS-ADDRESS-CHG-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Address_Dest_Name = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Address_Dest_Name", "CIS-ADDRESS-DEST-NAME", 
            FieldType.STRING, 35);
        pnd_Work_File1_Cis_Address_Txt = pnd_Work_File1_Cis_Address_Info.newFieldArrayInGroup("pnd_Work_File1_Cis_Address_Txt", "CIS-ADDRESS-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 5));
        pnd_Work_File1_Cis_Zip_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Zip_Code", "CIS-ZIP-CODE", FieldType.STRING, 
            9);
        pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Bank_Pymnt_Acct_Nmbr", "CIS-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Bank_Aba_Acct_Nmbr", "CIS-BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9);
        pnd_Work_File1_Cis_Stndrd_Trn_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Stndrd_Trn_Cd", "CIS-STNDRD-TRN-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Finalist_Reason_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Finalist_Reason_Cd", "CIS-FINALIST-REASON-CD", 
            FieldType.STRING, 10);
        pnd_Work_File1_Cis_Addr_Usage_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Addr_Usage_Code", "CIS-ADDR-USAGE-CODE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Checking_Saving_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Checking_Saving_Cd", "CIS-CHECKING-SAVING-CD", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Addr_Stndrd_Code = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Addr_Stndrd_Code", "CIS-ADDR-STNDRD-CODE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Stndrd_Overide = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Stndrd_Overide", "CIS-STNDRD-OVERIDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Postal_Data_Fields = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Postal_Data_Fields", "CIS-POSTAL-DATA-FIELDS", 
            FieldType.STRING, 44);
        pnd_Work_File1_Cis_Geographic_Cd = pnd_Work_File1_Cis_Address_Info.newFieldInGroup("pnd_Work_File1_Cis_Geographic_Cd", "CIS-GEOGRAPHIC-CD", FieldType.STRING, 
            2);
        pnd_Work_File1_Cis_Corp_Sync_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Corp_Sync_Ind", "CIS-CORP-SYNC-IND", FieldType.STRING, 1);

        pnd_Work_File1_Cis_Rqst_System_Mit_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Rqst_System_Mit_Dta", "CIS-RQST-SYSTEM-MIT-DTA");
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Sync_Ind", 
            "CIS-RQST-SYS-MIT-SYNC-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Log_Dte_Tme", 
            "CIS-RQST-SYS-MIT-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Function = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Function", 
            "CIS-RQST-SYS-MIT-FUNCTION", FieldType.STRING, 2);
        pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Rqst_Sys_Mit_Process_Env", 
            "CIS-RQST-SYS-MIT-PROCESS-ENV", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Mit_Error_Cde = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Mit_Error_Cde", "CIS-MIT-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Mit_Error_Msg = pnd_Work_File1_Cis_Rqst_System_Mit_Dta.newFieldInGroup("pnd_Work_File1_Cis_Mit_Error_Msg", "CIS-MIT-ERROR-MSG", 
            FieldType.STRING, 72);

        pnd_Work_File1_Cis_Non_Premium_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Non_Premium_Dta", "CIS-NON-PREMIUM-DTA");
        pnd_Work_File1_Cis_Nonp_Sync_Ind = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Sync_Ind", "CIS-NONP-SYNC-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Process_Env = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Process_Env", "CIS-NONP-PROCESS-ENV", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Effective_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Effective_Dte", "CIS-NONP-EFFECTIVE-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_10 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_10", "REDEFINE", pnd_Work_File1_Cis_Nonp_Effective_Dte);
        pnd_Work_File1_Cis_Nonp_Effective_Dte_A = pnd_Work_File1__R_Field_10.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Effective_Dte_A", "CIS-NONP-EFFECTIVE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Product_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Product_Cde", "CIS-NONP-PRODUCT-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte", "CIS-NONP-CONTRACT-RETR-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_11 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_11", "REDEFINE", pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte);
        pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A = pnd_Work_File1__R_Field_11.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A", "CIS-NONP-CONTRACT-RETR-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Rate_Accum_Adj_Amt", 
            "CIS-NONP-RATE-ACCUM-ADJ-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Currency = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Currency", "CIS-NONP-CURRENCY", 
            FieldType.NUMERIC, 1);
        pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Tiaa_Age_First_Pymnt", 
            "CIS-NONP-TIAA-AGE-FIRST-PYMNT", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cref_Age_First_Pymnt", 
            "CIS-NONP-CREF-AGE-FIRST-PYMNT", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Deletion_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Deletion_Cde", "CIS-NONP-DELETION-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Nonp_Vesting_Dte = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Vesting_Dte", "CIS-NONP-VESTING-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_12 = pnd_Work_File1_Cis_Non_Premium_Dta.newGroupInGroup("pnd_Work_File1__R_Field_12", "REDEFINE", pnd_Work_File1_Cis_Nonp_Vesting_Dte);
        pnd_Work_File1_Cis_Nonp_Vesting_Dte_A = pnd_Work_File1__R_Field_12.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Vesting_Dte_A", "CIS-NONP-VESTING-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cntr_Rate38_Accum_Amt", 
            "CIS-NONP-CNTR-RATE38-ACCUM-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Cntr_Rate42_Accum_Amt", 
            "CIS-NONP-CNTR-RATE42-ACCUM-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Work_File1_Cis_Nonp_Error_Cde = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Error_Cde", "CIS-NONP-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Nonp_Error_Msg = pnd_Work_File1_Cis_Non_Premium_Dta.newFieldInGroup("pnd_Work_File1_Cis_Nonp_Error_Msg", "CIS-NONP-ERROR-MSG", 
            FieldType.STRING, 72);

        pnd_Work_File1_Cis_Allocation_Dta = pnd_Work_File1.newGroupInGroup("pnd_Work_File1_Cis_Allocation_Dta", "CIS-ALLOCATION-DTA");
        pnd_Work_File1_Cis_Alloc_Sync_Ind = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Sync_Ind", "CIS-ALLOC-SYNC-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Alloc_Process_Env = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Process_Env", "CIS-ALLOC-PROCESS-ENV", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Alloc_Effective_Dte = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Effective_Dte", "CIS-ALLOC-EFFECTIVE-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_13 = pnd_Work_File1_Cis_Allocation_Dta.newGroupInGroup("pnd_Work_File1__R_Field_13", "REDEFINE", pnd_Work_File1_Cis_Alloc_Effective_Dte);
        pnd_Work_File1_Cis_Alloc_Effective_Dte_A = pnd_Work_File1__R_Field_13.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Effective_Dte_A", "CIS-ALLOC-EFFECTIVE-DTE-A", 
            FieldType.STRING, 8);
        pnd_Work_File1_Cis_Alloc_Percentage = pnd_Work_File1_Cis_Allocation_Dta.newFieldArrayInGroup("pnd_Work_File1_Cis_Alloc_Percentage", "CIS-ALLOC-PERCENTAGE", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20));
        pnd_Work_File1_Cis_Alloc_Error_Cde = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Error_Cde", "CIS-ALLOC-ERROR-CDE", 
            FieldType.STRING, 4);
        pnd_Work_File1_Cis_Alloc_Error_Msg = pnd_Work_File1_Cis_Allocation_Dta.newFieldInGroup("pnd_Work_File1_Cis_Alloc_Error_Msg", "CIS-ALLOC-ERROR-MSG", 
            FieldType.STRING, 72);
        pnd_Work_File1_Cis_Cor_Sync_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Sync_Ind", "CIS-COR-SYNC-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Process_Env = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Process_Env", "CIS-COR-PROCESS-ENV", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rcd_Typ_Cde", "CIS-COR-PH-RCD-TYP-CDE", FieldType.NUMERIC, 
            2);
        pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Neg_Election_Cde", "CIS-COR-PH-NEG-ELECTION-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Occupnt_Nme", "CIS-COR-PH-OCCUPNT-NME", FieldType.STRING, 
            10);
        pnd_Work_File1_Cis_Cor_Ph_Xref_Pin = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Xref_Pin", "CIS-COR-PH-XREF-PIN", FieldType.NUMERIC, 
            12);
        pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Active_Vip_Cnt", "CIS-COR-PH-ACTIVE-VIP-CNT", 
            FieldType.NUMERIC, 2);

        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp", "CIS-COR-PH-ACTVE-VIP-GRP", 
            new DbsArrayController(1, 8));
        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde = pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Cde", "CIS-COR-PH-ACTVE-VIP-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde = pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Actve_Vip_Area_Cde", 
            "CIS-COR-PH-ACTVE-VIP-AREA-CDE", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Cor_Mail_Table_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Mail_Table_Cnt", "CIS-COR-MAIL-TABLE-CNT", FieldType.NUMERIC, 
            2);

        pnd_Work_File1_Cis_Cor_Ph_Mail_Grp = pnd_Work_File1.newGroupArrayInGroup("pnd_Work_File1_Cis_Cor_Ph_Mail_Grp", "CIS-COR-PH-MAIL-GRP", new DbsArrayController(1, 
            25));
        pnd_Work_File1_Cis_Cor_Phmail_Cde = pnd_Work_File1_Cis_Cor_Ph_Mail_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Phmail_Cde", "CIS-COR-PHMAIL-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin = pnd_Work_File1_Cis_Cor_Ph_Mail_Grp.newFieldInGroup("pnd_Work_File1_Cis_Cor_Phmail_Area_Of_Origin", 
            "CIS-COR-PHMAIL-AREA-OF-ORIGIN", FieldType.STRING, 3);
        pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte", "CIS-COR-INST-PH-RMTTNG-INSTN-DTE", 
            FieldType.NUMERIC, 8);

        pnd_Work_File1__R_Field_14 = pnd_Work_File1.newGroupInGroup("pnd_Work_File1__R_Field_14", "REDEFINE", pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte);
        pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta = pnd_Work_File1__R_Field_14.newFieldInGroup("pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta", 
            "CIS-COR-INST-PH-RMTTNG-INSTN-DTA", FieldType.STRING, 8);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Nbr", "CIS-COR-PH-RMTTNG-INSTN-NBR", 
            FieldType.NUMERIC, 5);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Instn_Ind", "CIS-COR-PH-RMTTNG-INSTN-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmmtng_Instn_Pdup_Ind", "CIS-COR-PH-RMMTNG-INSTN-PDUP-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Rmttng_Nmbr_Range_Cde", "CIS-COR-PH-RMTTNG-NMBR-RANGE-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Media_Ind", "CIS-COR-PH-MCRJCK-MEDIA-IND", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Ph_Mcrjck_Cntnts_Cde", "CIS-COR-PH-MCRJCK-CNTNTS-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Table_Cnt", "CIS-COR-CNTRCT-TABLE-CNT", 
            FieldType.NUMERIC, 2);
        pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Status_Cde", "CIS-COR-CNTRCT-STATUS-CDE", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Status_Yr_Dte", "CIS-COR-CNTRCT-STATUS-YR-DTE", 
            FieldType.NUMERIC, 4);
        pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Cntrct_Payee_Cde", "CIS-COR-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Work_File1_Cis_Cor_Error_Cde = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cor_Error_Cde", "CIS-COR-ERROR-CDE", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Pull_Code = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Pull_Code", "CIS-PULL-CODE", FieldType.STRING, 4);
        pnd_Work_File1_Cis_Trnsf_Flag = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_Flag", "CIS-TRNSF-FLAG", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Trnsf_From_Company = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_From_Company", "CIS-TRNSF-FROM-COMPANY", FieldType.STRING, 
            4);
        pnd_Work_File1_Cis_Trnsf_To_Company = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Trnsf_To_Company", "CIS-TRNSF-TO-COMPANY", FieldType.STRING, 
            4);
        pnd_Work_File1_Cis_Tiaa_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Tiaa_Cntrct_Type", "CIS-TIAA-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Rea_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Rea_Cntrct_Type", "CIS-REA-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Cref_Cntrct_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Cref_Cntrct_Type", "CIS-CREF-CNTRCT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Decedent_Contract = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Decedent_Contract", "CIS-DECEDENT-CONTRACT", FieldType.STRING, 
            10);
        pnd_Work_File1_Cis_Relation_To_Decedent = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Relation_To_Decedent", "CIS-RELATION-TO-DECEDENT", 
            FieldType.STRING, 1);
        pnd_Work_File1_Cis_Ssn_Tin_Ind = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Ssn_Tin_Ind", "CIS-SSN-TIN-IND", FieldType.STRING, 1);
        pnd_Work_File1_Cis_Isn = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Isn", "CIS-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Work_File1_Cis_Mdo_Contract_Type = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Mdo_Contract_Type", "CIS-MDO-CONTRACT-TYPE", FieldType.STRING, 
            1);
        pnd_Work_File1_Cis_Sg_Text_Udf_3 = pnd_Work_File1.newFieldInGroup("pnd_Work_File1_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", FieldType.STRING, 10);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ia = localVariables.newFieldInRecord("pnd_Total_Ia", "#TOTAL-IA", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ia_Cor = localVariables.newFieldInRecord("pnd_Total_Ia_Cor", "#TOTAL-IA-COR", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ia_Mit = localVariables.newFieldInRecord("pnd_Total_Ia_Mit", "#TOTAL-IA-MIT", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ia_Alloc = localVariables.newFieldInRecord("pnd_Total_Ia_Alloc", "#TOTAL-IA-ALLOC", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ia_Addr = localVariables.newFieldInRecord("pnd_Total_Ia_Addr", "#TOTAL-IA-ADDR", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ia_Nonp = localVariables.newFieldInRecord("pnd_Total_Ia_Nonp", "#TOTAL-IA-NONP", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpa = localVariables.newFieldInRecord("pnd_Total_Tpa", "#TOTAL-TPA", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpa_Cor = localVariables.newFieldInRecord("pnd_Total_Tpa_Cor", "#TOTAL-TPA-COR", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpa_Cor_Omni = localVariables.newFieldInRecord("pnd_Total_Tpa_Cor_Omni", "#TOTAL-TPA-COR-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpi_Cor_Omni = localVariables.newFieldInRecord("pnd_Total_Tpi_Cor_Omni", "#TOTAL-TPI-COR-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpa_Mit = localVariables.newFieldInRecord("pnd_Total_Tpa_Mit", "#TOTAL-TPA-MIT", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpa_Alloc = localVariables.newFieldInRecord("pnd_Total_Tpa_Alloc", "#TOTAL-TPA-ALLOC", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpa_Addr = localVariables.newFieldInRecord("pnd_Total_Tpa_Addr", "#TOTAL-TPA-ADDR", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpa_Addr_Omni = localVariables.newFieldInRecord("pnd_Total_Tpa_Addr_Omni", "#TOTAL-TPA-ADDR-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpi_Addr_Omni = localVariables.newFieldInRecord("pnd_Total_Tpi_Addr_Omni", "#TOTAL-TPI-ADDR-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpa_Nonp = localVariables.newFieldInRecord("pnd_Total_Tpa_Nonp", "#TOTAL-TPA-NONP", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipro = localVariables.newFieldInRecord("pnd_Total_Ipro", "#TOTAL-IPRO", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipro_Cor = localVariables.newFieldInRecord("pnd_Total_Ipro_Cor", "#TOTAL-IPRO-COR", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipro_Cor_Omni = localVariables.newFieldInRecord("pnd_Total_Ipro_Cor_Omni", "#TOTAL-IPRO-COR-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipi_Cor_Omni = localVariables.newFieldInRecord("pnd_Total_Ipi_Cor_Omni", "#TOTAL-IPI-COR-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipro_Mit = localVariables.newFieldInRecord("pnd_Total_Ipro_Mit", "#TOTAL-IPRO-MIT", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipro_Alloc = localVariables.newFieldInRecord("pnd_Total_Ipro_Alloc", "#TOTAL-IPRO-ALLOC", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipro_Addr = localVariables.newFieldInRecord("pnd_Total_Ipro_Addr", "#TOTAL-IPRO-ADDR", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipro_Addr_Omni = localVariables.newFieldInRecord("pnd_Total_Ipro_Addr_Omni", "#TOTAL-IPRO-ADDR-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipi_Addr_Omni = localVariables.newFieldInRecord("pnd_Total_Ipi_Addr_Omni", "#TOTAL-IPI-ADDR-OMNI", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipro_Nonp = localVariables.newFieldInRecord("pnd_Total_Ipro_Nonp", "#TOTAL-IPRO-NONP", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Mdo_Cor = localVariables.newFieldInRecord("pnd_Total_Mdo_Cor", "#TOTAL-MDO-COR", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Mdo_Mit = localVariables.newFieldInRecord("pnd_Total_Mdo_Mit", "#TOTAL-MDO-MIT", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Mdo_Alloc = localVariables.newFieldInRecord("pnd_Total_Mdo_Alloc", "#TOTAL-MDO-ALLOC", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Mdo_Addr = localVariables.newFieldInRecord("pnd_Total_Mdo_Addr", "#TOTAL-MDO-ADDR", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Mdo_Nonp = localVariables.newFieldInRecord("pnd_Total_Mdo_Nonp", "#TOTAL-MDO-NONP", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Mdo = localVariables.newFieldInRecord("pnd_Total_Mdo", "#TOTAL-MDO", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Reject = localVariables.newFieldInRecord("pnd_Total_Reject", "#TOTAL-REJECT", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ia_Corp = localVariables.newFieldInRecord("pnd_Total_Ia_Corp", "#TOTAL-IA-CORP", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Tpa_Corp = localVariables.newFieldInRecord("pnd_Total_Tpa_Corp", "#TOTAL-TPA-CORP", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Ipro_Corp = localVariables.newFieldInRecord("pnd_Total_Ipro_Corp", "#TOTAL-IPRO-CORP", FieldType.PACKED_DECIMAL, 12);
        pnd_Total_Mdo_Corp = localVariables.newFieldInRecord("pnd_Total_Mdo_Corp", "#TOTAL-MDO-CORP", FieldType.PACKED_DECIMAL, 12);
        pnd_Open_Key = localVariables.newFieldInRecord("pnd_Open_Key", "#OPEN-KEY", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCisl200.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisb200() throws Exception
    {
        super("Cisb200");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        ldaCisl200.getVw_cis_Prtcpnt_File().startDatabaseRead                                                                                                             //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: AT TOP OF PAGE ( 1 );//Natural: READ CIS-PRTCPNT-FILE BY CIS-CORP-SYNC-IND = 'Y'
        (
        "RD",
        new Wc[] { new Wc("CIS_CORP_SYNC_IND", ">=", "Y", WcType.BY) },
        new Oc[] { new Oc("CIS_CORP_SYNC_IND", "ASC") }
        );
        RD:
        while (condition(ldaCisl200.getVw_cis_Prtcpnt_File().readNextRow("RD")))
        {
            if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Corp_Sync_Ind().notEquals("Y")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-CORP-SYNC-IND NE 'Y'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Status_Cd().notEquals("I") && ldaCisl200.getCis_Prtcpnt_File_Cis_Status_Cd().notEquals("P")))                //Natural: REJECT IF CIS-PRTCPNT-FILE.CIS-STATUS-CD NE 'I' AND CIS-PRTCPNT-FILE.CIS-STATUS-CD NE 'P'
            {
                continue;
            }
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
            //*  REJECT IF CIS-PRTCPNT-FILE.CIS-TIAA-DOI     = 0
            //*  REJECT IF CIS-PRTCPNT-FILE.CIS-CREF-DOI     = 0
            pnd_Total.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #TOTAL
            pnd_Work_File1.setValuesByName(ldaCisl200.getVw_cis_Prtcpnt_File());                                                                                          //Natural: MOVE BY NAME CIS-PRTCPNT-FILE TO #WORK-FILE1
            pnd_Work_File1_Cis_Tiaa_Doi_A.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Tiaa_Doi(),new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED CIS-PRTCPNT-FILE.CIS-TIAA-DOI ( EM = YYYYMMDD ) TO #WORK-FILE1.CIS-TIAA-DOI-A
            pnd_Work_File1_Cis_Cref_Doi_A.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Cref_Doi(),new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED CIS-PRTCPNT-FILE.CIS-CREF-DOI ( EM = YYYYMMDD ) TO #WORK-FILE1.CIS-CREF-DOI-A
            pnd_Work_File1_Cis_Frst_Annt_Dob_A.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Frst_Annt_Dob(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED CIS-PRTCPNT-FILE.CIS-FRST-ANNT-DOB ( EM = YYYYMMDD ) TO #WORK-FILE1.CIS-FRST-ANNT-DOB-A
            pnd_Work_File1_Cis_Cntrct_Print_Dte_A.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Cntrct_Print_Dte(),new ReportEditMask("YYYYMMDD"));                   //Natural: MOVE EDITED CIS-PRTCPNT-FILE.CIS-CNTRCT-PRINT-DTE ( EM = YYYYMMDD ) TO #WORK-FILE1.CIS-CNTRCT-PRINT-DTE-A
            pnd_Work_File1_Cis_Appl_Rcvd_Dte_A.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Appl_Rcvd_Dte(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED CIS-PRTCPNT-FILE.CIS-APPL-RCVD-DTE ( EM = YYYYMMDD ) TO #WORK-FILE1.CIS-APPL-RCVD-DTE-A
            pnd_Work_File1_Cis_Scnd_Annt_Dob_A.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Scnd_Annt_Dob(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED CIS-PRTCPNT-FILE.CIS-SCND-ANNT-DOB ( EM = YYYYMMDD ) TO #WORK-FILE1.CIS-SCND-ANNT-DOB-A
            pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Nonp_Contract_Retr_Dte(),new ReportEditMask("YYYYMMDD"));       //Natural: MOVE EDITED CIS-PRTCPNT-FILE.CIS-NONP-CONTRACT-RETR-DTE ( EM = YYYYMMDD ) TO #WORK-FILE1.CIS-NONP-CONTRACT-RETR-DTE-A
            pnd_Work_File1_Cis_Alloc_Effective_Dte_A.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Alloc_Effective_Dte(),new ReportEditMask("YYYYMMDD"));             //Natural: MOVE EDITED CIS-PRTCPNT-FILE.CIS-ALLOC-EFFECTIVE-DTE ( EM = YYYYMMDD ) TO #WORK-FILE1.CIS-ALLOC-EFFECTIVE-DTE-A
            pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED CIS-PRTCPNT-FILE.CIS-COR-INST-PH-RMTTNG-INSTN-DTE ( EM = YYYYMMDD ) TO #WORK-FILE1.CIS-COR-INST-PH-RMTTNG-INSTN-DTA
            pnd_Work_File1_Cis_Nonp_Effective_Dte_A.setValueEdited(ldaCisl200.getCis_Prtcpnt_File_Cis_Nonp_Effective_Dte(),new ReportEditMask("YYYYMMDD"));               //Natural: MOVE EDITED CIS-PRTCPNT-FILE.CIS-NONP-EFFECTIVE-DTE ( EM = YYYYMMDD ) TO #WORK-FILE1.CIS-NONP-EFFECTIVE-DTE-A
            //* *
            if (condition(pnd_Work_File1_Cis_Frst_Annt_Dob.equals(getZero())))                                                                                            //Natural: IF #WORK-FILE1.CIS-FRST-ANNT-DOB = 0
            {
                pnd_Work_File1_Cis_Frst_Annt_Dob_A.moveAll("0");                                                                                                          //Natural: MOVE ALL '0' TO #WORK-FILE1.CIS-FRST-ANNT-DOB-A
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Cntrct_Print_Dte.equals(getZero())))                                                                                         //Natural: IF #WORK-FILE1.CIS-CNTRCT-PRINT-DTE = 0
            {
                pnd_Work_File1_Cis_Cntrct_Print_Dte_A.moveAll("0");                                                                                                       //Natural: MOVE ALL '0' TO #WORK-FILE1.CIS-CNTRCT-PRINT-DTE-A
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Appl_Rcvd_Dte.equals(getZero())))                                                                                            //Natural: IF #WORK-FILE1.CIS-APPL-RCVD-DTE = 0
            {
                pnd_Work_File1_Cis_Appl_Rcvd_Dte_A.moveAll("0");                                                                                                          //Natural: MOVE ALL '0' TO #WORK-FILE1.CIS-APPL-RCVD-DTE-A
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Scnd_Annt_Dob.equals(getZero())))                                                                                            //Natural: IF #WORK-FILE1.CIS-SCND-ANNT-DOB = 0
            {
                pnd_Work_File1_Cis_Scnd_Annt_Dob_A.moveAll("0");                                                                                                          //Natural: MOVE ALL '0' TO #WORK-FILE1.CIS-SCND-ANNT-DOB-A
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte.equals(getZero())))                                                                                   //Natural: IF #WORK-FILE1.CIS-NONP-CONTRACT-RETR-DTE = 0
            {
                pnd_Work_File1_Cis_Nonp_Contract_Retr_Dte_A.moveAll("0");                                                                                                 //Natural: MOVE ALL '0' TO #WORK-FILE1.CIS-NONP-CONTRACT-RETR-DTE-A
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Alloc_Effective_Dte.equals(getZero())))                                                                                      //Natural: IF #WORK-FILE1.CIS-ALLOC-EFFECTIVE-DTE = 0
            {
                pnd_Work_File1_Cis_Alloc_Effective_Dte_A.moveAll("0");                                                                                                    //Natural: MOVE ALL '0' TO #WORK-FILE1.CIS-ALLOC-EFFECTIVE-DTE-A
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dte.equals(getZero())))                                                                             //Natural: IF #WORK-FILE1.CIS-COR-INST-PH-RMTTNG-INSTN-DTE = 0
            {
                pnd_Work_File1_Cis_Cor_Inst_Ph_Rmttng_Instn_Dta.moveAll("0");                                                                                             //Natural: MOVE ALL '0' TO #WORK-FILE1.CIS-COR-INST-PH-RMTTNG-INSTN-DTA
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Nonp_Effective_Dte.equals(getZero())))                                                                                       //Natural: IF #WORK-FILE1.CIS-NONP-EFFECTIVE-DTE = 0
            {
                pnd_Work_File1_Cis_Nonp_Effective_Dte_A.moveAll("0");                                                                                                     //Natural: MOVE ALL '0' TO #WORK-FILE1.CIS-NONP-EFFECTIVE-DTE-A
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Tiaa_Doi.equals(getZero())))                                                                                                 //Natural: IF #WORK-FILE1.CIS-TIAA-DOI = 0
            {
                pnd_Work_File1_Cis_Tiaa_Doi_A.moveAll("0");                                                                                                               //Natural: MOVE ALL '0' TO #WORK-FILE1.CIS-TIAA-DOI-A
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File1_Cis_Cref_Doi.equals(getZero())))                                                                                                 //Natural: IF #WORK-FILE1.CIS-CREF-DOI = 0
            {
                pnd_Work_File1_Cis_Cref_Doi_A.moveAll("0");                                                                                                               //Natural: MOVE ALL '0' TO #WORK-FILE1.CIS-CREF-DOI-A
            }                                                                                                                                                             //Natural: END-IF
            //*  IARPF
            pnd_Work_File1_Cis_Isn.setValue(ldaCisl200.getVw_cis_Prtcpnt_File().getAstISN("RD"));                                                                         //Natural: MOVE *ISN ( RD. ) TO #WORK-FILE1.CIS-ISN
            getWorkFiles().write(1, false, pnd_Work_File1);                                                                                                               //Natural: WRITE WORK FILE 1 #WORK-FILE1
            //*  GET.
            //*  GET CIS-PRTCPNT-FILE *ISN(RD.)
            //*  CIS-PRTCPNT-FILE.CIS-CORP-SYNC-IND         := 'N'
            //*  CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND          := 'N'
            //*  CIS-PRTCPNT-FILE.CIS-RQST-SYS-MIT-SYNC-IND := 'N'
            //*  CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND          := 'N'
            //*  CIS-PRTCPNT-FILE.CIS-ALLOC-SYNC-IND        := 'N'
            //*  CIS-PRTCPNT-FILE.CIS-NONP-SYNC-IND         := 'N'
            //*  UPDATE(GET.)
            //*  END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM TOTAL-IA-N-MDO
            sub_Total_Ia_N_Mdo();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM GRAND-TOTAL
        sub_Grand_Total();
        if (condition(Global.isEscape())) {return;}
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-IA-N-MDO
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRAND-TOTAL
    }
    private void sub_Total_Ia_N_Mdo() throws Exception                                                                                                                    //Natural: TOTAL-IA-N-MDO
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Corp_Sync_Ind().equals("Y")))                                                                                    //Natural: IF CIS-PRTCPNT-FILE.CIS-CORP-SYNC-IND EQ 'Y'
        {
            if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id().equals("IA")))                                                                                     //Natural: IF CIS-PRTCPNT-FILE.CIS-RQST-ID EQ 'IA'
            {
                pnd_Total_Ia.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #TOTAL-IA
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().equals("Y")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Ia_Cor.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOTAL-IA-COR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind().equals("Y")))                                                                    //Natural: IF CIS-PRTCPNT-FILE.CIS-RQST-SYS-MIT-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Ia_Mit.nadd(1);                                                                                                                             //Natural: ADD 1 TO #TOTAL-IA-MIT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Alloc_Sync_Ind().equals("Y")))                                                                           //Natural: IF CIS-PRTCPNT-FILE.CIS-ALLOC-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Ia_Alloc.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOTAL-IA-ALLOC
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().equals("Y")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND EQ 'Y'
                {
                    pnd_Total_Ia_Addr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-IA-ADDR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Nonp_Sync_Ind().equals("Y")))                                                                            //Natural: IF CIS-PRTCPNT-FILE.CIS-NONP-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Ia_Nonp.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-IA-NONP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id().equals("TPA")))                                                                                    //Natural: IF CIS-PRTCPNT-FILE.CIS-RQST-ID EQ 'TPA'
            {
                pnd_Total_Tpa.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOTAL-TPA
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().equals("Y")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Tpa_Cor.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-TPA-COR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().equals("S")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND EQ 'S'
                {
                    pnd_Total_Tpa_Cor_Omni.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-TPA-COR-OMNI
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind().equals("Y")))                                                                    //Natural: IF CIS-PRTCPNT-FILE.CIS-RQST-SYS-MIT-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Tpa_Mit.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-TPA-MIT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Alloc_Sync_Ind().equals("Y")))                                                                           //Natural: IF CIS-PRTCPNT-FILE.CIS-ALLOC-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Tpa_Alloc.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-TPA-ALLOC
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().equals("Y")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND EQ 'Y'
                {
                    pnd_Total_Tpa_Addr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOTAL-TPA-ADDR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().equals("S")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND EQ 'S'
                {
                    pnd_Total_Tpa_Addr_Omni.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TOTAL-TPA-ADDR-OMNI
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Nonp_Sync_Ind().equals("Y")))                                                                            //Natural: IF CIS-PRTCPNT-FILE.CIS-NONP-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Tpa_Nonp.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOTAL-TPA-NONP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id().equals("TPI")))                                                                                    //Natural: IF CIS-PRTCPNT-FILE.CIS-RQST-ID EQ 'TPI'
            {
                pnd_Total_Tpa.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOTAL-TPA
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().equals("S")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND EQ 'S'
                {
                    pnd_Total_Tpi_Cor_Omni.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-TPI-COR-OMNI
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().equals("S")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND EQ 'S'
                {
                    pnd_Total_Tpi_Addr_Omni.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TOTAL-TPI-ADDR-OMNI
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id().equals("IPRO")))                                                                                   //Natural: IF CIS-PRTCPNT-FILE.CIS-RQST-ID EQ 'IPRO'
            {
                pnd_Total_Ipro.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TOTAL-IPRO
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().equals("Y")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Ipro_Cor.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOTAL-IPRO-COR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().equals("S")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND EQ 'S'
                {
                    pnd_Total_Ipro_Cor_Omni.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TOTAL-IPRO-COR-OMNI
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind().equals("Y")))                                                                    //Natural: IF CIS-PRTCPNT-FILE.CIS-RQST-SYS-MIT-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Ipro_Mit.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOTAL-IPRO-MIT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Alloc_Sync_Ind().equals("Y")))                                                                           //Natural: IF CIS-PRTCPNT-FILE.CIS-ALLOC-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Ipro_Alloc.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-IPRO-ALLOC
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().equals("Y")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND EQ 'Y'
                {
                    pnd_Total_Ipro_Addr.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-IPRO-ADDR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().equals("S")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND EQ 'S'
                {
                    pnd_Total_Ipro_Addr_Omni.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOTAL-IPRO-ADDR-OMNI
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Nonp_Sync_Ind().equals("Y")))                                                                            //Natural: IF CIS-PRTCPNT-FILE.CIS-NONP-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Ipro_Nonp.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-IPRO-NONP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id().equals("IPI")))                                                                                    //Natural: IF CIS-PRTCPNT-FILE.CIS-RQST-ID EQ 'IPI'
            {
                pnd_Total_Ipro.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #TOTAL-IPRO
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().equals("S")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND EQ 'S'
                {
                    pnd_Total_Ipi_Cor_Omni.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-IPI-COR-OMNI
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().equals("S")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND EQ 'S'
                {
                    pnd_Total_Ipi_Addr_Omni.nadd(1);                                                                                                                      //Natural: ADD 1 TO #TOTAL-IPI-ADDR-OMNI
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  090606
            if (condition(DbsUtil.maskMatches(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Id(),"'MDO'")))                                                                     //Natural: IF CIS-PRTCPNT-FILE.CIS-RQST-ID EQ MASK ( 'MDO' )
            {
                pnd_Total_Mdo.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #TOTAL-MDO
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Cor_Sync_Ind().equals("Y")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-COR-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Mdo_Cor.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-MDO-COR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Rqst_Sys_Mit_Sync_Ind().equals("Y")))                                                                    //Natural: IF CIS-PRTCPNT-FILE.CIS-RQST-SYS-MIT-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Mdo_Mit.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-MDO-MIT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Alloc_Sync_Ind().equals("Y")))                                                                           //Natural: IF CIS-PRTCPNT-FILE.CIS-ALLOC-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Mdo_Alloc.nadd(1);                                                                                                                          //Natural: ADD 1 TO #TOTAL-MDO-ALLOC
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Addr_Syn_Ind().equals("Y")))                                                                             //Natural: IF CIS-PRTCPNT-FILE.CIS-ADDR-SYN-IND EQ 'Y'
                {
                    pnd_Total_Mdo_Addr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOTAL-MDO-ADDR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCisl200.getCis_Prtcpnt_File_Cis_Nonp_Sync_Ind().equals("Y")))                                                                            //Natural: IF CIS-PRTCPNT-FILE.CIS-NONP-SYNC-IND EQ 'Y'
                {
                    pnd_Total_Mdo_Nonp.nadd(1);                                                                                                                           //Natural: ADD 1 TO #TOTAL-MDO-NONP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Grand_Total() throws Exception                                                                                                                       //Natural: GRAND-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        pnd_Total_Reject.reset();                                                                                                                                         //Natural: RESET #TOTAL-REJECT
        pnd_Total_Reject.compute(new ComputeParameters(false, pnd_Total_Reject), pnd_Total_Read.subtract(pnd_Total));                                                     //Natural: ASSIGN #TOTAL-REJECT := #TOTAL-READ - #TOTAL
        pnd_Total_Ia_Corp.compute(new ComputeParameters(false, pnd_Total_Ia_Corp), pnd_Total_Ia_Cor.add(pnd_Total_Ia_Mit).add(pnd_Total_Ia_Addr));                        //Natural: ASSIGN #TOTAL-IA-CORP := #TOTAL-IA-COR + #TOTAL-IA-MIT + #TOTAL-IA-ADDR
        pnd_Total_Tpa_Corp.compute(new ComputeParameters(false, pnd_Total_Tpa_Corp), pnd_Total_Tpa_Cor.add(pnd_Total_Tpa_Mit).add(pnd_Total_Tpa_Addr).add(pnd_Total_Tpa_Cor_Omni).add(pnd_Total_Tpa_Addr_Omni).add(pnd_Total_Tpi_Cor_Omni).add(pnd_Total_Tpi_Addr_Omni)); //Natural: ASSIGN #TOTAL-TPA-CORP := #TOTAL-TPA-COR + #TOTAL-TPA-MIT + #TOTAL-TPA-ADDR + #TOTAL-TPA-COR-OMNI + #TOTAL-TPA-ADDR-OMNI + #TOTAL-TPI-COR-OMNI + #TOTAL-TPI-ADDR-OMNI
        pnd_Total_Ipro_Corp.compute(new ComputeParameters(false, pnd_Total_Ipro_Corp), pnd_Total_Ipro_Cor.add(pnd_Total_Ipro_Mit).add(pnd_Total_Ipro_Addr).add(pnd_Total_Ipro_Cor_Omni).add(pnd_Total_Ipro_Addr_Omni).add(pnd_Total_Ipi_Cor_Omni).add(pnd_Total_Ipi_Addr_Omni)); //Natural: ASSIGN #TOTAL-IPRO-CORP := #TOTAL-IPRO-COR + #TOTAL-IPRO-MIT + #TOTAL-IPRO-ADDR + #TOTAL-IPRO-COR-OMNI + #TOTAL-IPRO-ADDR-OMNI + #TOTAL-IPI-COR-OMNI + #TOTAL-IPI-ADDR-OMNI
        pnd_Total_Mdo_Corp.compute(new ComputeParameters(false, pnd_Total_Mdo_Corp), pnd_Total_Mdo_Cor.add(pnd_Total_Mdo_Mit).add(pnd_Total_Mdo_Addr).add(pnd_Total_Mdo_Alloc).add(pnd_Total_Mdo_Nonp)); //Natural: ASSIGN #TOTAL-MDO-CORP := #TOTAL-MDO-COR + #TOTAL-MDO-MIT + #TOTAL-MDO-ADDR + #TOTAL-MDO-ALLOC + #TOTAL-MDO-NONP
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 1 ) ' '
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(16),"TOTAL RECORDS EXTRACTED FROM CIS PARTICIPANT FILE              : ",pnd_Total,NEWLINE,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 16T 'TOTAL RECORDS EXTRACTED FROM CIS PARTICIPANT FILE              : ' #TOTAL /// 30T 'IA - TOTAL RECORDS EXTRACTED                     : ' #TOTAL-IA // 40T 'COR       : ' #TOTAL-IA-COR / 40T 'MIT       : ' #TOTAL-IA-MIT / 40T 'ADDR      : ' #TOTAL-IA-ADDR / 40T '              ______________' / 26T 'TOTAL CORPORATE UPDATE  : ' #TOTAL-IA-CORP // 30T 'TPA - TOTAL RECORDS EXTRACTED                     : ' #TOTAL-TPA // 40T 'COR           : ' #TOTAL-TPA-COR / 40T 'COR OMNI      : ' #TOTAL-TPA-COR-OMNI / 40T 'COR ICAP OMNI : ' #TOTAL-TPI-COR-OMNI / 40T 'MIT           : ' #TOTAL-TPA-MIT / 40T 'ADDR          : ' #TOTAL-TPA-ADDR / 40T 'ADDR OMNI     : ' #TOTAL-TPA-ADDR-OMNI / 40T 'ADDR ICAP OMNI: ' #TOTAL-TPI-ADDR-OMNI / 40T '              ______________' / 26T 'TOTAL CORPORATE UPDATE  : ' #TOTAL-TPA-CORP // 30T 'IPRO - TOTAL RECORDS EXTRACTED                     : ' #TOTAL-IPRO // 40T 'COR            : ' #TOTAL-IPRO-COR / 40T 'COR OMNI       : ' #TOTAL-IPRO-COR-OMNI / 40T 'COR ICAP OMNI  : ' #TOTAL-IPI-COR-OMNI / 40T 'MIT            : ' #TOTAL-IPRO-MIT / 40T 'ADDR           : ' #TOTAL-IPRO-ADDR / 40T 'ADDR OMNI      : ' #TOTAL-IPRO-ADDR-OMNI / 40T 'ADDR ICAP OMNI : ' #TOTAL-IPI-ADDR-OMNI / 40T '              ______________' / 26T 'TOTAL CORPORATE UPDATE  : ' #TOTAL-IPRO-CORP // 30T 'MDO - TOTAL RECORDS EXTRACTED                    : ' #TOTAL-MDO // 40T 'COR       : ' #TOTAL-MDO-COR / 40T 'MIT       : ' #TOTAL-MDO-MIT / 40T 'ALLOC     : ' #TOTAL-MDO-ALLOC / 40T 'ADDR      : ' #TOTAL-MDO-ADDR / 40T 'NONP      : ' #TOTAL-MDO-NONP / 40T '              ______________' / 26T 'TOTAL CORPORATE UPDATE  : ' #TOTAL-MDO-CORP //
            TabSetting(30),"IA - TOTAL RECORDS EXTRACTED                     : ",pnd_Total_Ia,NEWLINE,NEWLINE,new TabSetting(40),"COR       : ",pnd_Total_Ia_Cor,NEWLINE,new 
            TabSetting(40),"MIT       : ",pnd_Total_Ia_Mit,NEWLINE,new TabSetting(40),"ADDR      : ",pnd_Total_Ia_Addr,NEWLINE,new TabSetting(40),"              ______________",NEWLINE,new 
            TabSetting(26),"TOTAL CORPORATE UPDATE  : ",pnd_Total_Ia_Corp,NEWLINE,NEWLINE,new TabSetting(30),"TPA - TOTAL RECORDS EXTRACTED                     : ",pnd_Total_Tpa,NEWLINE,NEWLINE,new 
            TabSetting(40),"COR           : ",pnd_Total_Tpa_Cor,NEWLINE,new TabSetting(40),"COR OMNI      : ",pnd_Total_Tpa_Cor_Omni,NEWLINE,new TabSetting(40),"COR ICAP OMNI : ",pnd_Total_Tpi_Cor_Omni,NEWLINE,new 
            TabSetting(40),"MIT           : ",pnd_Total_Tpa_Mit,NEWLINE,new TabSetting(40),"ADDR          : ",pnd_Total_Tpa_Addr,NEWLINE,new TabSetting(40),"ADDR OMNI     : ",pnd_Total_Tpa_Addr_Omni,NEWLINE,new 
            TabSetting(40),"ADDR ICAP OMNI: ",pnd_Total_Tpi_Addr_Omni,NEWLINE,new TabSetting(40),"              ______________",NEWLINE,new TabSetting(26),"TOTAL CORPORATE UPDATE  : ",pnd_Total_Tpa_Corp,NEWLINE,NEWLINE,new 
            TabSetting(30),"IPRO - TOTAL RECORDS EXTRACTED                     : ",pnd_Total_Ipro,NEWLINE,NEWLINE,new TabSetting(40),"COR            : ",pnd_Total_Ipro_Cor,NEWLINE,new 
            TabSetting(40),"COR OMNI       : ",pnd_Total_Ipro_Cor_Omni,NEWLINE,new TabSetting(40),"COR ICAP OMNI  : ",pnd_Total_Ipi_Cor_Omni,NEWLINE,new 
            TabSetting(40),"MIT            : ",pnd_Total_Ipro_Mit,NEWLINE,new TabSetting(40),"ADDR           : ",pnd_Total_Ipro_Addr,NEWLINE,new TabSetting(40),"ADDR OMNI      : ",pnd_Total_Ipro_Addr_Omni,NEWLINE,new 
            TabSetting(40),"ADDR ICAP OMNI : ",pnd_Total_Ipi_Addr_Omni,NEWLINE,new TabSetting(40),"              ______________",NEWLINE,new TabSetting(26),"TOTAL CORPORATE UPDATE  : ",pnd_Total_Ipro_Corp,NEWLINE,NEWLINE,new 
            TabSetting(30),"MDO - TOTAL RECORDS EXTRACTED                    : ",pnd_Total_Mdo,NEWLINE,NEWLINE,new TabSetting(40),"COR       : ",pnd_Total_Mdo_Cor,NEWLINE,new 
            TabSetting(40),"MIT       : ",pnd_Total_Mdo_Mit,NEWLINE,new TabSetting(40),"ALLOC     : ",pnd_Total_Mdo_Alloc,NEWLINE,new TabSetting(40),"ADDR      : ",pnd_Total_Mdo_Addr,NEWLINE,new 
            TabSetting(40),"NONP      : ",pnd_Total_Mdo_Nonp,NEWLINE,new TabSetting(40),"              ______________",NEWLINE,new TabSetting(26),"TOTAL CORPORATE UPDATE  : ",
            pnd_Total_Mdo_Corp,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),NEWLINE,new TabSetting(53),"CONSOLIDATED ISSUE SYSTEM",NEWLINE,new                     //Natural: WRITE ( 1 ) NOTITLE *PROGRAM / 53T 'CONSOLIDATED ISSUE SYSTEM' / 40T 'DAILY CORPORATE FILES UPDATE EXTRACT REPORT' 110T 'RUN DATE:' *DATU /
                        TabSetting(40),"DAILY CORPORATE FILES UPDATE EXTRACT REPORT",new TabSetting(110),"RUN DATE:",Global.getDATU(),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");
    }
}
