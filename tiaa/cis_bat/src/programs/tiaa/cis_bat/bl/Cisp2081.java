/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:23:15 PM
**        * FROM NATURAL PROGRAM : Cisp2081
************************************************************
**        * FILE NAME            : Cisp2081.java
**        * CLASS NAME           : Cisp2081
**        * INSTANCE NAME        : Cisp2081
************************************************************
************************************************************************
* HISTORY
* 10/06/2006 O SOTTO COMMENTED-OUT CHECK FOR CIS-BENE-TRUST WHEN
*                    LOADING BENE INFORMATION. SC 100606.
* 08/06/2013 B NEWSOM - SETTLEMENT RESTRICTION = 'R' IS SET FOR IA, TPA,
*                       AND IPRO CONTRACTS WHICH GETS FED INTO
*                       BENEFICIARY SYSTEM.  WHEN THIS HAPPENS,
*                       BENEFICIARIES DO NOT DISPLAY IN UD.   WE
*                       UNDERSTAND WHEN THIS RESTRICTION IS SET, USERS
*                       CANNOT CHANGE THE BENE AND IT'S NOT AVAILABLE TO
*                       CHANGE ON THE WEB.  THE BUSINESS REQUESTED THAT
*                       SETTLEMENT-RESTRICTION-IND SHOULD BE 'N' ON ALL
*                       RQST IDS COMING TO THE BENE SYSTEM.  (CHG290458)
* 08/26/2015 - BUDDY NEWSOM - INSTITUTIONAL PREMIUM FILE SUNSET (IPFS) *
* REMOVE VIEW INSIDE THE PROGRAM AS IT IS NOT BEING USED OR REFERENCED.*
* REPLACE IT WITH A CALL TO NECN4000 TO RETREVE THE BUSINESS DATE.     *
* 05/11/17  (BABRE)   PIN EXPANSION CHANGES. (C420007)         PINE    *
* 04/04/19  B.NEWSOM  BREAKING ACIS DEPENDENCIES OF THE BENE LEGACY    *
*                     SYSTEM                             (BADOTBLS)    *
************************************************************************

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisp2081 extends BLNatBase
{
    // Data Areas
    private LdaCisl2081 ldaCisl2081;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup bene_File;

    private DbsGroup bene_File_Bene_File_Data;
    private DbsField bene_File_Bene_Intrfcng_Systm;
    private DbsField bene_File_Bene_Rqstng_System;
    private DbsField bene_File_Bene_Intrfce_Bsnss_Dte;
    private DbsField bene_File_Bene_Intrfce_Mgrtn_Ind;
    private DbsField bene_File_Bene_Nmbr_Of_Benes;
    private DbsField bene_File_Bene_Tiaa_Nbr;
    private DbsField bene_File_Bene_Cref_Nbr;
    private DbsField bene_File_Bene_Pin_Nbr;
    private DbsField bene_File_Bene_Lob;
    private DbsField bene_File_Bene_Lob_Type;
    private DbsField bene_File_Bene_Part_Prfx;
    private DbsField bene_File_Bene_Part_Sffx;
    private DbsField bene_File_Bene_Part_First_Nme;
    private DbsField bene_File_Bene_Part_Middle_Nme;
    private DbsField bene_File_Bene_Part_Last_Nme;
    private DbsField bene_File_Bene_Part_Ssn;
    private DbsField bene_File_Bene_Part_Dob;
    private DbsField bene_File_Bene_Estate;
    private DbsField bene_File_Bene_Trust;
    private DbsField bene_File_Bene_Category;
    private DbsField bene_File_Bene_Effective_Dt;
    private DbsField bene_File_Bene_Mos_Ind;
    private DbsField bene_File_Bene_Mos_Irrvcble_Ind;
    private DbsField bene_File_Bene_Pymnt_Chld_Dcsd_Ind;
    private DbsField bene_File_Bene_Update_Dt;
    private DbsField bene_File_Bene_Update_Time;
    private DbsField bene_File_Bene_Update_By;
    private DbsField bene_File_Bene_Record_Status;
    private DbsField bene_File_Bene_Same_As_Ind;
    private DbsField bene_File_Bene_New_Issuefslash_Chng_Ind;
    private DbsField bene_File_Bene_Contract_Type;
    private DbsField bene_File_Bene_Tiaa_Cref_Ind;
    private DbsField bene_File_Bene_Stat;
    private DbsField bene_File_Bene_Dflt_To_Estate;
    private DbsField bene_File_Bene_More_Than_Five_Benes_Ind;
    private DbsField bene_File_Bene_Illgble_Ind;
    private DbsField bene_File_Bene_Exempt_Spouse_Rights;
    private DbsField bene_File_Bene_Spouse_Waived_Bnfts;
    private DbsField bene_File_Bene_Trust_Data_Fldr;
    private DbsField bene_File_Bene_Addr_Fldr;
    private DbsField bene_File_Bene_Co_Owner_Data_Fldr;
    private DbsField bene_File_Bene_Fldr_Log_Dte_Tme;
    private DbsField bene_File_Bene_Fldr_Min;
    private DbsField bene_File_Bene_Fldr_Srce_Id;
    private DbsField bene_File_Bene_Rqst_Timestamp;

    private DbsGroup bene_File__R_Field_1;
    private DbsField bene_File_Bene_Rqst_Date;
    private DbsField bene_File_Bene_Rqst_Time;
    private DbsField bene_File_Bene_Last_Vrfy_Dte;
    private DbsField bene_File_Bene_Last_Vrfy_Tme;
    private DbsField bene_File_Bene_Last_Vrfy_Userid;
    private DbsField bene_File_Bene_Last_Dsgntn_Srce;
    private DbsField bene_File_Bene_Last_Dsgntn_System;

    private DbsGroup bene_File_Bene_Data;
    private DbsField bene_File_Bene_Type;
    private DbsField bene_File_Bene_Name;
    private DbsField bene_File_Bene_Extended_Name;
    private DbsField bene_File_Bene_Ssn_Cd;
    private DbsField bene_File_Bene_Ssn_Nbr;
    private DbsField bene_File_Bene_Dob;
    private DbsField bene_File_Bene_Dte_Birth_Trust;
    private DbsField bene_File_Bene_Relationship_Free_Txt;
    private DbsField bene_File_Bene_Relationship_Cde;
    private DbsField bene_File_Bene_Prctge_Frctn_Ind;
    private DbsField bene_File_Bene_Irrvcbl_Ind;
    private DbsField bene_File_Bene_Alloc_Pct;
    private DbsField bene_File_Bene_Nmrtr_Nbr;
    private DbsField bene_File_Bene_Dnmntr_Nbr;
    private DbsField bene_File_Bene_Std_Txt_Ind;
    private DbsField bene_File_Bene_Sttlmnt_Rstrctn;

    private DbsGroup bene_File_Bene_Addtl_Info;
    private DbsField bene_File_Bene_Addr1;
    private DbsField bene_File_Bene_Addr2;
    private DbsField bene_File_Bene_Addr3_City;
    private DbsField bene_File_Bene_State;
    private DbsField bene_File_Bene_Zip;
    private DbsField bene_File_Bene_Country;
    private DbsField bene_File_Bene_Phone;
    private DbsField bene_File_Bene_Gender;
    private DbsField bene_File_Bene_Spcl_Txt;
    private DbsField bene_File_Bene_Mdo_Calc_Bene;
    private DbsField bene_File_Bene_Spcl_Dsgn_Txt;
    private DbsField bene_File_Bene_Hold_Cde;
    private DbsField bene_File_Pnd_Table_Rltn;

    private DbsGroup bene_File__R_Field_2;
    private DbsField bene_File_Pnd_Rltn_Cd;
    private DbsField bene_File_Pnd_Rltn_Text;
    private DbsField bene_File_Bene_Total_Contract_Written;
    private DbsField bene_File_Bene_Total_Mos_Written;
    private DbsField bene_File_Bene_Total_Dest_Written;
    private DbsField bene_File_Bene_Return_Code;
    private DbsField pnd_Cis_Tiaa_Nbr;
    private DbsField pnd_Mit_Log_Dte_Time;
    private DbsField pnd_Rqst_Id_Key;
    private DbsField pnd_I;
    private DbsField pnd_M;
    private DbsField pnd_N;
    private DbsField pnd_Total_Bene;
    private DbsField pnd_Total;
    private DbsField pnd_Total_Read;
    private DbsField pnd_Total_Error;
    private DbsField pnd_Isn;
    private DbsField pnd_Part_Name;
    private DbsField pnd_Today;

    private DbsGroup pnd_Today__R_Field_3;
    private DbsField pnd_Today_Pnd_Today_Cc;
    private DbsField pnd_Today_Pnd_Today_Yy;
    private DbsField pnd_Today_Pnd_Today_Mm;
    private DbsField pnd_Today_Pnd_Today_Dd;

    private DbsGroup pnd_Today__R_Field_4;
    private DbsField pnd_Today_Pnd_Today_A;
    private DbsField pnd_Today_Date;
    private DbsField pnd_Prmry_Ssn;

    private DbsGroup pnd_Prmry_Ssn__R_Field_5;
    private DbsField pnd_Prmry_Ssn_Pnd_Prmry_Ssn_N;
    private DbsField pnd_Cntgnt_Ssn;

    private DbsGroup pnd_Cntgnt_Ssn__R_Field_6;
    private DbsField pnd_Cntgnt_Ssn_Pnd_Cntgnt_Ssn_N;
    private DbsField pnd_Total_Ia;
    private DbsField pnd_Total_Mdo;
    private DbsField pnd_Total_Tpa;
    private DbsField pnd_Total_Ipro;
    private DbsField pnd_Mos;
    private DbsField pnd_Mos1;
    private DbsField pnd_Mos2;
    private DbsField pnd_Rqst_Id;
    private DbsField pnd_Found;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCisl2081 = new LdaCisl2081();
        registerRecord(ldaCisl2081);
        registerRecord(ldaCisl2081.getVw_cis_Bene_File_01());

        // Local Variables
        localVariables = new DbsRecord();

        bene_File = localVariables.newGroupInRecord("bene_File", "BENE-FILE");

        bene_File_Bene_File_Data = bene_File.newGroupInGroup("bene_File_Bene_File_Data", "BENE-FILE-DATA");
        bene_File_Bene_Intrfcng_Systm = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfcng_Systm", "BENE-INTRFCNG-SYSTM", FieldType.STRING, 
            8);
        bene_File_Bene_Rqstng_System = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Rqstng_System", "BENE-RQSTNG-SYSTEM", FieldType.STRING, 
            10);
        bene_File_Bene_Intrfce_Bsnss_Dte = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfce_Bsnss_Dte", "BENE-INTRFCE-BSNSS-DTE", FieldType.STRING, 
            8);
        bene_File_Bene_Intrfce_Mgrtn_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Intrfce_Mgrtn_Ind", "BENE-INTRFCE-MGRTN-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Nmbr_Of_Benes = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Nmbr_Of_Benes", "BENE-NMBR-OF-BENES", FieldType.NUMERIC, 
            3);
        bene_File_Bene_Tiaa_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Tiaa_Nbr", "BENE-TIAA-NBR", FieldType.STRING, 10);
        bene_File_Bene_Cref_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Cref_Nbr", "BENE-CREF-NBR", FieldType.STRING, 10);
        bene_File_Bene_Pin_Nbr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Pin_Nbr", "BENE-PIN-NBR", FieldType.NUMERIC, 12);
        bene_File_Bene_Lob = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Lob", "BENE-LOB", FieldType.STRING, 1);
        bene_File_Bene_Lob_Type = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Lob_Type", "BENE-LOB-TYPE", FieldType.STRING, 1);
        bene_File_Bene_Part_Prfx = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Prfx", "BENE-PART-PRFX", FieldType.STRING, 10);
        bene_File_Bene_Part_Sffx = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Sffx", "BENE-PART-SFFX", FieldType.STRING, 10);
        bene_File_Bene_Part_First_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_First_Nme", "BENE-PART-FIRST-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Middle_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Middle_Nme", "BENE-PART-MIDDLE-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Last_Nme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Last_Nme", "BENE-PART-LAST-NME", FieldType.STRING, 
            30);
        bene_File_Bene_Part_Ssn = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Ssn", "BENE-PART-SSN", FieldType.NUMERIC, 9);
        bene_File_Bene_Part_Dob = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Part_Dob", "BENE-PART-DOB", FieldType.NUMERIC, 8);
        bene_File_Bene_Estate = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Estate", "BENE-ESTATE", FieldType.STRING, 1);
        bene_File_Bene_Trust = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Trust", "BENE-TRUST", FieldType.STRING, 1);
        bene_File_Bene_Category = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Category", "BENE-CATEGORY", FieldType.STRING, 1);
        bene_File_Bene_Effective_Dt = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Effective_Dt", "BENE-EFFECTIVE-DT", FieldType.STRING, 8);
        bene_File_Bene_Mos_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Mos_Ind", "BENE-MOS-IND", FieldType.STRING, 1);
        bene_File_Bene_Mos_Irrvcble_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Mos_Irrvcble_Ind", "BENE-MOS-IRRVCBLE-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Pymnt_Chld_Dcsd_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Pymnt_Chld_Dcsd_Ind", "BENE-PYMNT-CHLD-DCSD-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Update_Dt = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_Dt", "BENE-UPDATE-DT", FieldType.STRING, 8);
        bene_File_Bene_Update_Time = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_Time", "BENE-UPDATE-TIME", FieldType.STRING, 7);
        bene_File_Bene_Update_By = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Update_By", "BENE-UPDATE-BY", FieldType.STRING, 8);
        bene_File_Bene_Record_Status = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Record_Status", "BENE-RECORD-STATUS", FieldType.STRING, 
            1);
        bene_File_Bene_Same_As_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Same_As_Ind", "BENE-SAME-AS-IND", FieldType.STRING, 1);
        bene_File_Bene_New_Issuefslash_Chng_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_New_Issuefslash_Chng_Ind", "BENE-NEW-ISSUE/CHNG-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Contract_Type = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Contract_Type", "BENE-CONTRACT-TYPE", FieldType.STRING, 
            1);
        bene_File_Bene_Tiaa_Cref_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Tiaa_Cref_Ind", "BENE-TIAA-CREF-IND", FieldType.STRING, 
            1);
        bene_File_Bene_Stat = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Stat", "BENE-STAT", FieldType.STRING, 1);
        bene_File_Bene_Dflt_To_Estate = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Dflt_To_Estate", "BENE-DFLT-TO-ESTATE", FieldType.STRING, 
            1);
        bene_File_Bene_More_Than_Five_Benes_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_More_Than_Five_Benes_Ind", "BENE-MORE-THAN-FIVE-BENES-IND", 
            FieldType.STRING, 1);
        bene_File_Bene_Illgble_Ind = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Illgble_Ind", "BENE-ILLGBLE-IND", FieldType.STRING, 1);
        bene_File_Bene_Exempt_Spouse_Rights = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Exempt_Spouse_Rights", "BENE-EXEMPT-SPOUSE-RIGHTS", 
            FieldType.STRING, 1);
        bene_File_Bene_Spouse_Waived_Bnfts = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Spouse_Waived_Bnfts", "BENE-SPOUSE-WAIVED-BNFTS", 
            FieldType.STRING, 1);
        bene_File_Bene_Trust_Data_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Trust_Data_Fldr", "BENE-TRUST-DATA-FLDR", FieldType.STRING, 
            1);
        bene_File_Bene_Addr_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Addr_Fldr", "BENE-ADDR-FLDR", FieldType.STRING, 1);
        bene_File_Bene_Co_Owner_Data_Fldr = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Co_Owner_Data_Fldr", "BENE-CO-OWNER-DATA-FLDR", FieldType.STRING, 
            1);
        bene_File_Bene_Fldr_Log_Dte_Tme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Log_Dte_Tme", "BENE-FLDR-LOG-DTE-TME", FieldType.STRING, 
            15);
        bene_File_Bene_Fldr_Min = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Min", "BENE-FLDR-MIN", FieldType.STRING, 11);
        bene_File_Bene_Fldr_Srce_Id = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Fldr_Srce_Id", "BENE-FLDR-SRCE-ID", FieldType.STRING, 6);
        bene_File_Bene_Rqst_Timestamp = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Rqst_Timestamp", "BENE-RQST-TIMESTAMP", FieldType.STRING, 
            15);

        bene_File__R_Field_1 = bene_File_Bene_File_Data.newGroupInGroup("bene_File__R_Field_1", "REDEFINE", bene_File_Bene_Rqst_Timestamp);
        bene_File_Bene_Rqst_Date = bene_File__R_Field_1.newFieldInGroup("bene_File_Bene_Rqst_Date", "BENE-RQST-DATE", FieldType.STRING, 8);
        bene_File_Bene_Rqst_Time = bene_File__R_Field_1.newFieldInGroup("bene_File_Bene_Rqst_Time", "BENE-RQST-TIME", FieldType.STRING, 7);
        bene_File_Bene_Last_Vrfy_Dte = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Dte", "BENE-LAST-VRFY-DTE", FieldType.STRING, 
            8);
        bene_File_Bene_Last_Vrfy_Tme = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Tme", "BENE-LAST-VRFY-TME", FieldType.STRING, 
            7);
        bene_File_Bene_Last_Vrfy_Userid = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Vrfy_Userid", "BENE-LAST-VRFY-USERID", FieldType.STRING, 
            8);
        bene_File_Bene_Last_Dsgntn_Srce = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Dsgntn_Srce", "BENE-LAST-DSGNTN-SRCE", FieldType.STRING, 
            1);
        bene_File_Bene_Last_Dsgntn_System = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Last_Dsgntn_System", "BENE-LAST-DSGNTN-SYSTEM", FieldType.STRING, 
            1);

        bene_File_Bene_Data = bene_File_Bene_File_Data.newGroupInGroup("bene_File_Bene_Data", "BENE-DATA");
        bene_File_Bene_Type = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Type", "BENE-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Name = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Name", "BENE-NAME", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Extended_Name = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Extended_Name", "BENE-EXTENDED-NAME", FieldType.STRING, 
            35, new DbsArrayController(1, 40));
        bene_File_Bene_Ssn_Cd = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Ssn_Cd", "BENE-SSN-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Ssn_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Ssn_Nbr", "BENE-SSN-NBR", FieldType.STRING, 9, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dob = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dob", "BENE-DOB", FieldType.NUMERIC, 8, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dte_Birth_Trust = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dte_Birth_Trust", "BENE-DTE-BIRTH-TRUST", FieldType.STRING, 
            8, new DbsArrayController(1, 40));
        bene_File_Bene_Relationship_Free_Txt = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Relationship_Free_Txt", "BENE-RELATIONSHIP-FREE-TXT", 
            FieldType.STRING, 15, new DbsArrayController(1, 40));
        bene_File_Bene_Relationship_Cde = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Relationship_Cde", "BENE-RELATIONSHIP-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 40));
        bene_File_Bene_Prctge_Frctn_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Prctge_Frctn_Ind", "BENE-PRCTGE-FRCTN-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        bene_File_Bene_Irrvcbl_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Irrvcbl_Ind", "BENE-IRRVCBL-IND", FieldType.STRING, 1, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Alloc_Pct = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Alloc_Pct", "BENE-ALLOC-PCT", FieldType.NUMERIC, 5, 2, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Nmrtr_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Nmrtr_Nbr", "BENE-NMRTR-NBR", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            40));
        bene_File_Bene_Dnmntr_Nbr = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Dnmntr_Nbr", "BENE-DNMNTR-NBR", FieldType.NUMERIC, 3, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Std_Txt_Ind = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Std_Txt_Ind", "BENE-STD-TXT-IND", FieldType.STRING, 1, new 
            DbsArrayController(1, 40));
        bene_File_Bene_Sttlmnt_Rstrctn = bene_File_Bene_Data.newFieldArrayInGroup("bene_File_Bene_Sttlmnt_Rstrctn", "BENE-STTLMNT-RSTRCTN", FieldType.STRING, 
            1, new DbsArrayController(1, 40));

        bene_File_Bene_Addtl_Info = bene_File_Bene_File_Data.newGroupInGroup("bene_File_Bene_Addtl_Info", "BENE-ADDTL-INFO");
        bene_File_Bene_Addr1 = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr1", "BENE-ADDR1", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Addr2 = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr2", "BENE-ADDR2", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Addr3_City = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Addr3_City", "BENE-ADDR3-CITY", FieldType.STRING, 35, 
            new DbsArrayController(1, 40));
        bene_File_Bene_State = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_State", "BENE-STATE", FieldType.STRING, 2, new DbsArrayController(1, 
            40));
        bene_File_Bene_Zip = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Zip", "BENE-ZIP", FieldType.STRING, 10, new DbsArrayController(1, 
            40));
        bene_File_Bene_Country = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Country", "BENE-COUNTRY", FieldType.STRING, 35, new DbsArrayController(1, 
            40));
        bene_File_Bene_Phone = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Phone", "BENE-PHONE", FieldType.STRING, 20, new DbsArrayController(1, 
            40));
        bene_File_Bene_Gender = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Gender", "BENE-GENDER", FieldType.STRING, 1, new DbsArrayController(1, 
            40));
        bene_File_Bene_Spcl_Txt = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Spcl_Txt", "BENE-SPCL-TXT", FieldType.STRING, 72, new 
            DbsArrayController(1, 40, 1, 3));
        bene_File_Bene_Mdo_Calc_Bene = bene_File_Bene_Addtl_Info.newFieldArrayInGroup("bene_File_Bene_Mdo_Calc_Bene", "BENE-MDO-CALC-BENE", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        bene_File_Bene_Spcl_Dsgn_Txt = bene_File_Bene_File_Data.newFieldArrayInGroup("bene_File_Bene_Spcl_Dsgn_Txt", "BENE-SPCL-DSGN-TXT", FieldType.STRING, 
            72, new DbsArrayController(1, 60));
        bene_File_Bene_Hold_Cde = bene_File_Bene_File_Data.newFieldInGroup("bene_File_Bene_Hold_Cde", "BENE-HOLD-CDE", FieldType.STRING, 8);
        bene_File_Pnd_Table_Rltn = bene_File.newFieldInGroup("bene_File_Pnd_Table_Rltn", "#TABLE-RLTN", FieldType.STRING, 1683);

        bene_File__R_Field_2 = bene_File.newGroupInGroup("bene_File__R_Field_2", "REDEFINE", bene_File_Pnd_Table_Rltn);
        bene_File_Pnd_Rltn_Cd = bene_File__R_Field_2.newFieldArrayInGroup("bene_File_Pnd_Rltn_Cd", "#RLTN-CD", FieldType.STRING, 2, new DbsArrayController(1, 
            99));
        bene_File_Pnd_Rltn_Text = bene_File__R_Field_2.newFieldArrayInGroup("bene_File_Pnd_Rltn_Text", "#RLTN-TEXT", FieldType.STRING, 15, new DbsArrayController(1, 
            99));
        bene_File_Bene_Total_Contract_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Contract_Written", "BENE-TOTAL-CONTRACT-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Total_Mos_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Mos_Written", "BENE-TOTAL-MOS-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Total_Dest_Written = bene_File.newFieldInGroup("bene_File_Bene_Total_Dest_Written", "BENE-TOTAL-DEST-WRITTEN", FieldType.NUMERIC, 
            10);
        bene_File_Bene_Return_Code = bene_File.newFieldInGroup("bene_File_Bene_Return_Code", "BENE-RETURN-CODE", FieldType.STRING, 1);
        pnd_Cis_Tiaa_Nbr = localVariables.newFieldInRecord("pnd_Cis_Tiaa_Nbr", "#CIS-TIAA-NBR", FieldType.STRING, 10);
        pnd_Mit_Log_Dte_Time = localVariables.newFieldInRecord("pnd_Mit_Log_Dte_Time", "#MIT-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_Rqst_Id_Key = localVariables.newFieldInRecord("pnd_Rqst_Id_Key", "#RQST-ID-KEY", FieldType.STRING, 35);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 9);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 9);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.NUMERIC, 9);
        pnd_Total_Bene = localVariables.newFieldInRecord("pnd_Total_Bene", "#TOTAL-BENE", FieldType.NUMERIC, 9);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.NUMERIC, 9);
        pnd_Total_Read = localVariables.newFieldInRecord("pnd_Total_Read", "#TOTAL-READ", FieldType.NUMERIC, 9);
        pnd_Total_Error = localVariables.newFieldInRecord("pnd_Total_Error", "#TOTAL-ERROR", FieldType.NUMERIC, 9);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.NUMERIC, 10);
        pnd_Part_Name = localVariables.newFieldInRecord("pnd_Part_Name", "#PART-NAME", FieldType.STRING, 50);
        pnd_Today = localVariables.newFieldInRecord("pnd_Today", "#TODAY", FieldType.NUMERIC, 8);

        pnd_Today__R_Field_3 = localVariables.newGroupInRecord("pnd_Today__R_Field_3", "REDEFINE", pnd_Today);
        pnd_Today_Pnd_Today_Cc = pnd_Today__R_Field_3.newFieldInGroup("pnd_Today_Pnd_Today_Cc", "#TODAY-CC", FieldType.NUMERIC, 2);
        pnd_Today_Pnd_Today_Yy = pnd_Today__R_Field_3.newFieldInGroup("pnd_Today_Pnd_Today_Yy", "#TODAY-YY", FieldType.NUMERIC, 2);
        pnd_Today_Pnd_Today_Mm = pnd_Today__R_Field_3.newFieldInGroup("pnd_Today_Pnd_Today_Mm", "#TODAY-MM", FieldType.NUMERIC, 2);
        pnd_Today_Pnd_Today_Dd = pnd_Today__R_Field_3.newFieldInGroup("pnd_Today_Pnd_Today_Dd", "#TODAY-DD", FieldType.NUMERIC, 2);

        pnd_Today__R_Field_4 = localVariables.newGroupInRecord("pnd_Today__R_Field_4", "REDEFINE", pnd_Today);
        pnd_Today_Pnd_Today_A = pnd_Today__R_Field_4.newFieldInGroup("pnd_Today_Pnd_Today_A", "#TODAY-A", FieldType.STRING, 8);
        pnd_Today_Date = localVariables.newFieldInRecord("pnd_Today_Date", "#TODAY-DATE", FieldType.DATE);
        pnd_Prmry_Ssn = localVariables.newFieldInRecord("pnd_Prmry_Ssn", "#PRMRY-SSN", FieldType.STRING, 9);

        pnd_Prmry_Ssn__R_Field_5 = localVariables.newGroupInRecord("pnd_Prmry_Ssn__R_Field_5", "REDEFINE", pnd_Prmry_Ssn);
        pnd_Prmry_Ssn_Pnd_Prmry_Ssn_N = pnd_Prmry_Ssn__R_Field_5.newFieldInGroup("pnd_Prmry_Ssn_Pnd_Prmry_Ssn_N", "#PRMRY-SSN-N", FieldType.NUMERIC, 9);
        pnd_Cntgnt_Ssn = localVariables.newFieldInRecord("pnd_Cntgnt_Ssn", "#CNTGNT-SSN", FieldType.STRING, 9);

        pnd_Cntgnt_Ssn__R_Field_6 = localVariables.newGroupInRecord("pnd_Cntgnt_Ssn__R_Field_6", "REDEFINE", pnd_Cntgnt_Ssn);
        pnd_Cntgnt_Ssn_Pnd_Cntgnt_Ssn_N = pnd_Cntgnt_Ssn__R_Field_6.newFieldInGroup("pnd_Cntgnt_Ssn_Pnd_Cntgnt_Ssn_N", "#CNTGNT-SSN-N", FieldType.NUMERIC, 
            9);
        pnd_Total_Ia = localVariables.newFieldInRecord("pnd_Total_Ia", "#TOTAL-IA", FieldType.NUMERIC, 9);
        pnd_Total_Mdo = localVariables.newFieldInRecord("pnd_Total_Mdo", "#TOTAL-MDO", FieldType.NUMERIC, 9);
        pnd_Total_Tpa = localVariables.newFieldInRecord("pnd_Total_Tpa", "#TOTAL-TPA", FieldType.NUMERIC, 9);
        pnd_Total_Ipro = localVariables.newFieldInRecord("pnd_Total_Ipro", "#TOTAL-IPRO", FieldType.NUMERIC, 9);
        pnd_Mos = localVariables.newFieldInRecord("pnd_Mos", "#MOS", FieldType.NUMERIC, 9);
        pnd_Mos1 = localVariables.newFieldInRecord("pnd_Mos1", "#MOS1", FieldType.NUMERIC, 9);
        pnd_Mos2 = localVariables.newFieldInRecord("pnd_Mos2", "#MOS2", FieldType.NUMERIC, 9);
        pnd_Rqst_Id = localVariables.newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 8);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCisl2081.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisp2081() throws Exception
    {
        super("Cisp2081");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //* PINE<<                                                                                                                                                        //Natural: FORMAT ( 1 ) LS = 133 PS = 60;//Natural: FORMAT ( 2 ) LS = 133 PS = 60;//Natural: FORMAT ( 3 ) LS = 133 PS = 60
                                                                                                                                                                          //Natural: PERFORM GET-TODAYS-BDATE
        sub_Get_Todays_Bdate();
        if (condition(Global.isEscape())) {return;}
        ldaCisl2081.getVw_cis_Bene_File_01().startDatabaseRead                                                                                                            //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 );//Natural: AT TOP OF PAGE ( 3 );//Natural: READ CIS-BENE-FILE-01 BY CIS-BENE-SYNC-IND EQ 'Y'
        (
        "READ1",
        new Wc[] { new Wc("CIS_BENE_SYNC_IND", ">=", "Y", WcType.BY) },
        new Oc[] { new Oc("CIS_BENE_SYNC_IND", "ASC") }
        );
        READ1:
        while (condition(ldaCisl2081.getVw_cis_Bene_File_01().readNextRow("READ1")))
        {
            if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Sync_Ind().notEquals("Y")))                                                                            //Natural: IF CIS-BENE-SYNC-IND NE 'Y'
            {
                if (true) break READ1;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  BADOTBLS
            pnd_Isn.reset();                                                                                                                                              //Natural: RESET #ISN #TOTAL-BENE BENE-FILE-DATA #N
            pnd_Total_Bene.reset();
            bene_File_Bene_File_Data.reset();
            pnd_N.reset();
            pnd_Isn.setValue(ldaCisl2081.getVw_cis_Bene_File_01().getAstISN("READ1"));                                                                                    //Natural: ASSIGN #ISN := *ISN
            //*  BADOTBLS
            pnd_Cis_Tiaa_Nbr.reset();                                                                                                                                     //Natural: RESET #CIS-TIAA-NBR #MIT-LOG-DTE-TIME BENE-TIAA-CREF-IND #FOUND
            pnd_Mit_Log_Dte_Time.reset();
            bene_File_Bene_Tiaa_Cref_Ind.reset();
            pnd_Found.reset();
            pnd_Cis_Tiaa_Nbr.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr());                                                                               //Natural: ASSIGN #CIS-TIAA-NBR := CIS-BENE-TIAA-NBR
                                                                                                                                                                          //Natural: PERFORM GET-MIT-LOG-DTE-TIME
            sub_Get_Mit_Log_Dte_Time();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Found.getBoolean()))                                                                                                                        //Natural: IF #FOUND
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_N.reset();                                                                                                                                                //Natural: RESET #N
            //*  BADOTBLS
            if (condition(bene_File_Bene_Tiaa_Cref_Ind.equals("B") && ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA")))                                   //Natural: IF BENE-TIAA-CREF-IND = 'B' AND CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IA'
            {
                pnd_N.setValue(2);                                                                                                                                        //Natural: ASSIGN #N := 2
                pnd_Total.nadd(pnd_N);                                                                                                                                    //Natural: ADD #N TO #TOTAL
            }                                                                                                                                                             //Natural: END-IF
            //*  BADOTBLS
            if (condition(bene_File_Bene_Tiaa_Cref_Ind.equals("T") && ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA")))                                   //Natural: IF BENE-TIAA-CREF-IND = 'T' AND CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IA'
            {
                pnd_N.setValue(1);                                                                                                                                        //Natural: ASSIGN #N := 1
                pnd_Total.nadd(pnd_N);                                                                                                                                    //Natural: ADD #N TO #TOTAL
            }                                                                                                                                                             //Natural: END-IF
            //*  BADOTBLS
            if (condition(bene_File_Bene_Tiaa_Cref_Ind.equals("C") && ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA")))                                   //Natural: IF BENE-TIAA-CREF-IND = 'C' AND CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IA'
            {
                pnd_N.setValue(1);                                                                                                                                        //Natural: ASSIGN #N := 1
                pnd_Total.nadd(pnd_N);                                                                                                                                    //Natural: ADD #N TO #TOTAL
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("MDO")))                                                                              //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'MDO'
            {
                pnd_N.setValue(1);                                                                                                                                        //Natural: ASSIGN #N := 1
                pnd_Total.nadd(pnd_N);                                                                                                                                    //Natural: ADD #N TO #TOTAL
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("TPA")))                                                                              //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'TPA'
            {
                pnd_N.setValue(1);                                                                                                                                        //Natural: ASSIGN #N := 1
                pnd_Total.nadd(pnd_N);                                                                                                                                    //Natural: ADD #N TO #TOTAL
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IPRO")))                                                                             //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IPRO'
            {
                pnd_N.setValue(1);                                                                                                                                        //Natural: ASSIGN #N := 1
                pnd_Total.nadd(pnd_N);                                                                                                                                    //Natural: ADD #N TO #TOTAL
            }                                                                                                                                                             //Natural: END-IF
            pnd_Total_Read.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #TOTAL-READ
            FOR01:                                                                                                                                                        //Natural: FOR #M = 1 TO #N
            for (pnd_M.setValue(1); condition(pnd_M.lessOrEqual(pnd_N)); pnd_M.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM LOAD-BENEA970-PDA
                sub_Load_Benea970_Pda();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CALL-BENE
                sub_Call_Bene();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM UPDATE-CIS-BENE
            sub_Update_Cis_Bene();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
        sub_Write_Summary_Report();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "=",bene_File_Bene_Total_Contract_Written);                                                                                                 //Natural: WRITE '=' BENE-TOTAL-CONTRACT-WRITTEN
        if (Global.isEscape()) return;
        getReports().write(0, "=",bene_File_Bene_Total_Mos_Written);                                                                                                      //Natural: WRITE '=' BENE-TOTAL-MOS-WRITTEN
        if (Global.isEscape()) return;
        getReports().write(0, "=",bene_File_Bene_Total_Dest_Written);                                                                                                     //Natural: WRITE '=' BENE-TOTAL-DEST-WRITTEN
        if (Global.isEscape()) return;
        if (condition(bene_File_Bene_Total_Contract_Written.equals(getZero())))                                                                                           //Natural: IF BENE-TOTAL-CONTRACT-WRITTEN = 0
        {
            getWorkFiles().write(10, false, "                                       ");                                                                                   //Natural: WRITE WORK FILE 10 '                                       '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(bene_File_Bene_Total_Mos_Written.equals(getZero())))                                                                                                //Natural: IF BENE-TOTAL-MOS-WRITTEN = 0
        {
            getWorkFiles().write(11, false, "                                       ");                                                                                   //Natural: WRITE WORK FILE 11 '                                       '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(bene_File_Bene_Total_Dest_Written.equals(getZero())))                                                                                               //Natural: IF BENE-TOTAL-DEST-WRITTEN = 0
        {
            getWorkFiles().write(12, false, "                                       ");                                                                                   //Natural: WRITE WORK FILE 12 '                                       '
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(52),"*** E N D  O F  R E P O R T ***");                                                 //Natural: WRITE ( 1 ) // 52T '*** E N D  O F  R E P O R T ***'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(52),"*** E N D  O F  R E P O R T ***");                                                 //Natural: WRITE ( 2 ) // 52T '*** E N D  O F  R E P O R T ***'
        if (Global.isEscape()) return;
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-BENEA970-PDA
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-BENE
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CIS-BENE
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-REPORT
        //* *---------------------------------
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXCEPTION-REPORT
        //* *---------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RECORD-SENT-TO-BENE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TODAYS-BDATE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-CALC-BENE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MIT-LOG-DTE-TIME
    }
    private void sub_Load_Benea970_Pda() throws Exception                                                                                                                 //Natural: LOAD-BENEA970-PDA
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        bene_File_Bene_File_Data.reset();                                                                                                                                 //Natural: RESET BENE-FILE-DATA
        bene_File_Bene_Intrfce_Bsnss_Dte.setValue(pnd_Today);                                                                                                             //Natural: ASSIGN BENE-INTRFCE-BSNSS-DTE := #TODAY
        bene_File_Bene_Intrfcng_Systm.setValue("CIS");                                                                                                                    //Natural: ASSIGN BENE-INTRFCNG-SYSTM := 'CIS'
        bene_File_Bene_Last_Dsgntn_Srce.setValue("I");                                                                                                                    //Natural: ASSIGN BENE-LAST-DSGNTN-SRCE := 'I'
        bene_File_Bene_Last_Dsgntn_System.setValue("C");                                                                                                                  //Natural: ASSIGN BENE-LAST-DSGNTN-SYSTEM := 'C'
        //*  BADOTBLS
        bene_File_Bene_Effective_Dt.setValueEdited(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Sync_Upd_Date(),new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED CIS-BENE-FILE-01.CIS-BENE-SYNC-UPD-DATE ( EM = YYYYMMDD ) TO BENE-EFFECTIVE-DT
        //*  BADOTBLS
        if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA") || ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("TPA")                 //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IA' OR CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'TPA' OR CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IPRO'
            || ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IPRO")))
        {
            bene_File_Bene_Rqstng_System.setValue("ADAM");                                                                                                                //Natural: ASSIGN BENE-RQSTNG-SYSTEM := 'ADAM'
            //*  BADOTBLS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            bene_File_Bene_Rqstng_System.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id());                                                                    //Natural: ASSIGN BENE-RQSTNG-SYSTEM := CIS-BENE-FILE-01.CIS-BENE-RQST-ID
            //*  BADOTBLS
            //*  BADOTBLS
            //*  BADOTBLS
        }                                                                                                                                                                 //Natural: END-IF
        bene_File_Bene_New_Issuefslash_Chng_Ind.setValue("N");                                                                                                            //Natural: ASSIGN BENE-NEW-ISSUE/CHNG-IND := 'N'
        bene_File_Bene_Intrfce_Mgrtn_Ind.setValue("I");                                                                                                                   //Natural: ASSIGN BENE-INTRFCE-MGRTN-IND := 'I'
        bene_File_Bene_Pin_Nbr.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Unique_Id_Nbr());                                                                        //Natural: ASSIGN BENE-PIN-NBR := CIS-BENE-FILE-01.CIS-BENE-UNIQUE-ID-NBR
        if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA")))                                                                                   //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IA'
        {
            //*  BADOTBLS
            if (condition(pnd_M.equals(1)))                                                                                                                               //Natural: IF #M EQ 1
            {
                bene_File_Bene_Tiaa_Nbr.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr());                                                                    //Natural: ASSIGN BENE-TIAA-NBR := CIS-BENE-FILE-01.CIS-BENE-TIAA-NBR
            }                                                                                                                                                             //Natural: END-IF
            //*  BADOTBLS
            if (condition(pnd_M.equals(2)))                                                                                                                               //Natural: IF #M EQ 2
            {
                bene_File_Bene_Tiaa_Nbr.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Cref_Nbr());                                                                    //Natural: ASSIGN BENE-TIAA-NBR := CIS-BENE-FILE-01.CIS-BENE-CREF-NBR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("TPA")))                                                                                  //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'TPA'
        {
            //*  BADOTBLS
            if (condition(pnd_M.equals(1)))                                                                                                                               //Natural: IF #M EQ 1
            {
                bene_File_Bene_Tiaa_Nbr.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr());                                                                    //Natural: ASSIGN BENE-TIAA-NBR := CIS-BENE-FILE-01.CIS-BENE-TIAA-NBR
            }                                                                                                                                                             //Natural: END-IF
            //*  BADOTBLS
            if (condition(pnd_M.equals(2)))                                                                                                                               //Natural: IF #M EQ 2
            {
                bene_File_Bene_Tiaa_Nbr.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Cref_Nbr());                                                                    //Natural: ASSIGN BENE-TIAA-NBR := CIS-BENE-FILE-01.CIS-BENE-CREF-NBR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IPRO")))                                                                                 //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IPRO'
        {
            //*  BADOTBLS
            if (condition(pnd_M.equals(1)))                                                                                                                               //Natural: IF #M EQ 1
            {
                bene_File_Bene_Tiaa_Nbr.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr());                                                                    //Natural: ASSIGN BENE-TIAA-NBR := CIS-BENE-FILE-01.CIS-BENE-TIAA-NBR
            }                                                                                                                                                             //Natural: END-IF
            //*  BADOTBLS
            if (condition(pnd_M.equals(2)))                                                                                                                               //Natural: IF #M EQ 2
            {
                bene_File_Bene_Tiaa_Nbr.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Cref_Nbr());                                                                    //Natural: ASSIGN BENE-TIAA-NBR := CIS-BENE-FILE-01.CIS-BENE-CREF-NBR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  BADOTBLS
        //*  BADOTBLS
        if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("MDO")))                                                                                  //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'MDO'
        {
            bene_File_Bene_Tiaa_Nbr.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr());                                                                        //Natural: ASSIGN BENE-TIAA-NBR := CIS-BENE-FILE-01.CIS-BENE-TIAA-NBR
            bene_File_Bene_Cref_Nbr.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Cref_Nbr());                                                                        //Natural: ASSIGN BENE-CREF-NBR := CIS-BENE-FILE-01.CIS-BENE-CREF-NBR
            //*  BADOTBLS
        }                                                                                                                                                                 //Natural: END-IF
        bene_File_Bene_Tiaa_Cref_Ind.setValue(" ");                                                                                                                       //Natural: ASSIGN BENE-TIAA-CREF-IND := ' '
        //*  BADOTBLS
        if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA")))                                                                                   //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IA'
        {
            bene_File_Bene_Contract_Type.setValue("I");                                                                                                                   //Natural: ASSIGN BENE-CONTRACT-TYPE := 'I'
        }                                                                                                                                                                 //Natural: END-IF
        //*  BADOTBLS
        if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("MDO")))                                                                                  //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'MDO'
        {
            bene_File_Bene_Contract_Type.setValue("D");                                                                                                                   //Natural: ASSIGN BENE-CONTRACT-TYPE := 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //*  BADOTBLS
        if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("TPA")))                                                                                  //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'TPA'
        {
            bene_File_Bene_Contract_Type.setValue("I");                                                                                                                   //Natural: ASSIGN BENE-CONTRACT-TYPE := 'I'
        }                                                                                                                                                                 //Natural: END-IF
        //*  BADOTBLS
        if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IPRO")))                                                                                 //Natural: IF CIS-BENE-FILE-01.CIS-BENE-RQST-ID EQ 'IPRO'
        {
            bene_File_Bene_Contract_Type.setValue("I");                                                                                                                   //Natural: ASSIGN BENE-CONTRACT-TYPE := 'I'
            //*  BADOTBLS
            //*  BADOTBLS
        }                                                                                                                                                                 //Natural: END-IF
        bene_File_Bene_Mos_Ind.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Std_Free());                                                                             //Natural: ASSIGN BENE-MOS-IND := CIS-BENE-FILE-01.CIS-BENE-STD-FREE
        bene_File_Bene_Fldr_Log_Dte_Tme.setValue(pnd_Mit_Log_Dte_Time);                                                                                                   //Natural: ASSIGN BENE-FLDR-LOG-DTE-TME := #MIT-LOG-DTE-TIME
        //*  BADOTBLS
        if (condition(bene_File_Bene_Mos_Ind.equals("Y") && ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("MDO")))                                            //Natural: IF BENE-MOS-IND EQ 'Y' AND CIS-BENE-RQST-ID EQ 'MDO'
        {
            pnd_Rqst_Id_Key.reset();                                                                                                                                      //Natural: RESET #RQST-ID-KEY
            pnd_Rqst_Id_Key.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id_Key());                                                                             //Natural: ASSIGN #RQST-ID-KEY := CIS-BENE-FILE-01.CIS-BENE-RQST-ID-KEY
            pnd_Rqst_Id.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id());                                                                                     //Natural: ASSIGN #RQST-ID := CIS-BENE-FILE-01.CIS-BENE-RQST-ID
            //*  BADOTBLS
            DbsUtil.callnat(Cisn2082.class , getCurrentProcessState(), pnd_Rqst_Id_Key, pnd_Rqst_Id, bene_File);                                                          //Natural: CALLNAT 'CISN2082' #RQST-ID-KEY #RQST-ID BENE-FILE
            if (condition(Global.isEscape())) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Mos.reset();                                                                                                                                                  //Natural: RESET #MOS
        //*  BADOTBLS
        if (condition(bene_File_Bene_Mos_Ind.equals("Y") && (ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IA") || ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("TPA")  //Natural: IF BENE-MOS-IND EQ 'Y' AND ( CIS-BENE-RQST-ID EQ 'IA' OR CIS-BENE-RQST-ID EQ 'TPA' OR CIS-BENE-RQST-ID EQ 'IPRO' )
            || ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Rqst_Id().equals("IPRO"))))
        {
            FOR02:                                                                                                                                                        //Natural: FOR #MOS1 1 TO 20
            for (pnd_Mos1.setValue(1); condition(pnd_Mos1.lessOrEqual(20)); pnd_Mos1.nadd(1))
            {
                FOR03:                                                                                                                                                    //Natural: FOR #MOS2 1 TO 3
                for (pnd_Mos2.setValue(1); condition(pnd_Mos2.lessOrEqual(3)); pnd_Mos2.nadd(1))
                {
                    pnd_Mos.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #MOS
                    bene_File_Bene_Spcl_Txt.getValue(pnd_Mos1,pnd_Mos2).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_Mos1,             //Natural: ASSIGN BENE-SPCL-TXT ( #MOS1,#MOS2 ) := CIS-PRMRY-BENE-SPCL-TXT ( #MOS1,#MOS2 )
                        pnd_Mos2));
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Total_Ia.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #TOTAL-IA
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Estate().notEquals("Y") || ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Estate().equals(" ")))                 //Natural: IF CIS-BENE-FILE-01.CIS-BENE-ESTATE NE 'Y' OR CIS-BENE-FILE-01.CIS-BENE-ESTATE EQ ' '
        {
            FOR1:                                                                                                                                                         //Natural: FOR #I 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Name().getValue(pnd_I).equals(" ") && ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_I,1).equals(" ")  //Natural: IF CIS-BENE-FILE-01.CIS-PRMRY-BENE-NAME ( #I ) EQ ' ' AND CIS-BENE-FILE-01.CIS-PRMRY-BENE-SPCL-TXT ( #I,1 ) EQ ' ' AND CIS-BENE-FILE-01.CIS-PRMRY-BENE-SPCL-TXT ( #I,2 ) EQ ' ' AND CIS-BENE-FILE-01.CIS-PRMRY-BENE-SPCL-TXT ( #I,3 ) EQ ' '
                    && ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_I,2).equals(" ") && ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_I,
                    3).equals(" ")))
                {
                    if (true) break FOR1;                                                                                                                                 //Natural: ESCAPE BOTTOM ( FOR1. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  BADOTBLS
                    //*  BADOTBLS
                    pnd_Total_Bene.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-BENE
                    bene_File_Bene_Name.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Name().getValue(pnd_I));                         //Natural: ASSIGN BENE-NAME ( #TOTAL-BENE ) := CIS-PRMRY-BENE-NAME ( #I )
                    bene_File_Bene_Extended_Name.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Extndd_Nme().getValue(pnd_I));          //Natural: ASSIGN BENE-EXTENDED-NAME ( #TOTAL-BENE ) := CIS-PRMRY-BENE-EXTNDD-NME ( #I )
                    bene_File_Bene_Type.getValue(pnd_Total_Bene).setValue("P");                                                                                           //Natural: ASSIGN BENE-TYPE ( #TOTAL-BENE ) := 'P'
                    //*  BADOTBLS
                    bene_File_Bene_Dte_Birth_Trust.getValue(pnd_Total_Bene).setValueEdited(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Dob().getValue(pnd_I),new       //Natural: MOVE EDITED CIS-PRMRY-BENE-DOB ( #I ) ( EM = YYYYMMDD ) TO BENE-DTE-BIRTH-TRUST ( #TOTAL-BENE )
                        ReportEditMask("YYYYMMDD"));
                    bene_File_Bene_Relationship_Cde.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn_Cde().getValue(pnd_I));         //Natural: ASSIGN BENE-RELATIONSHIP-CDE ( #TOTAL-BENE ) := CIS-PRMRY-BENE-RLTN-CDE ( #I )
                    bene_File_Bene_Relationship_Free_Txt.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Rltn().getValue(pnd_I));        //Natural: ASSIGN BENE-RELATIONSHIP-FREE-TXT ( #TOTAL-BENE ) := CIS-PRMRY-BENE-RLTN ( #I )
                    //*  BADOTBLS
                    //*  BADOTBLS
                    if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr().getValue(pnd_I).notEquals(getZero())))                                         //Natural: IF CIS-PRMRY-BENE-SSN-NBR ( #I ) NE 0
                    {
                        bene_File_Bene_Ssn_Cd.getValue(pnd_Total_Bene).setValue("S");                                                                                     //Natural: ASSIGN BENE-SSN-CD ( #TOTAL-BENE ) := 'S'
                        pnd_Prmry_Ssn_Pnd_Prmry_Ssn_N.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr().getValue(pnd_I));                                 //Natural: ASSIGN #PRMRY-SSN-N := CIS-PRMRY-BENE-SSN-NBR ( #I )
                        bene_File_Bene_Ssn_Nbr.getValue(pnd_Total_Bene).setValue(pnd_Prmry_Ssn);                                                                          //Natural: ASSIGN BENE-SSN-NBR ( #TOTAL-BENE ) := #PRMRY-SSN
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BADOTBLS
                    if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(pnd_I).notEquals(getZero()) && ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_I).notEquals(getZero()))) //Natural: IF CIS-PRMRY-BENE-NMRTR-NBR ( #I ) NE 0 AND CIS-PRMRY-BENE-DNMNTR-NBR ( #I ) NE 0
                    {
                        bene_File_Bene_Prctge_Frctn_Ind.getValue(pnd_Total_Bene).setValue("S");                                                                           //Natural: ASSIGN BENE-PRCTGE-FRCTN-IND ( #TOTAL-BENE ) := 'S'
                        bene_File_Bene_Nmrtr_Nbr.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(pnd_I));           //Natural: ASSIGN BENE-NMRTR-NBR ( #TOTAL-BENE ) := CIS-PRMRY-BENE-NMRTR-NBR ( #I )
                        bene_File_Bene_Dnmntr_Nbr.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_I));         //Natural: ASSIGN BENE-DNMNTR-NBR ( #TOTAL-BENE ) := CIS-PRMRY-BENE-DNMNTR-NBR ( #I )
                        //*  BADOTBLS
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        bene_File_Bene_Prctge_Frctn_Ind.getValue(pnd_Total_Bene).setValue("P");                                                                           //Natural: ASSIGN BENE-PRCTGE-FRCTN-IND ( #TOTAL-BENE ) := 'P'
                        bene_File_Bene_Alloc_Pct.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Allctn_Pct().getValue(pnd_I));          //Natural: ASSIGN BENE-ALLOC-PCT ( #TOTAL-BENE ) := CIS-PRMRY-BENE-ALLCTN-PCT ( #I )
                        //*  BADOTBLS
                    }                                                                                                                                                     //Natural: END-IF
                    bene_File_Bene_Spcl_Txt.getValue(pnd_Total_Bene,"*").setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_I,               //Natural: ASSIGN BENE-SPCL-TXT ( #TOTAL-BENE,* ) := CIS-PRMRY-BENE-SPCL-TXT ( #I,* )
                        "*"));
                    bene_File_Bene_Sttlmnt_Rstrctn.getValue(pnd_Total_Bene).setValue("N");                                                                                //Natural: ASSIGN BENE-STTLMNT-RSTRCTN ( #TOTAL-BENE ) := 'N'
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-CALC-BENE
                sub_Check_Calc_Bene();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FOR1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FOR1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            FOR2:                                                                                                                                                         //Natural: FOR #I 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Name().getValue(pnd_I).equals(" ") && ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt().getValue(pnd_I,1).equals(" ")  //Natural: IF CIS-CNTGNT-BENE-NAME ( #I ) EQ ' ' AND CIS-BENE-FILE-01.CIS-CNTGNT-BENE-SPCL-TXT ( #I,1 ) EQ ' ' AND CIS-BENE-FILE-01.CIS-CNTGNT-BENE-SPCL-TXT ( #I,2 ) EQ ' ' AND CIS-BENE-FILE-01.CIS-CNTGNT-BENE-SPCL-TXT ( #I,3 ) EQ ' '
                    && ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt().getValue(pnd_I,2).equals(" ") && ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt().getValue(pnd_I,
                    3).equals(" ")))
                {
                    //*        AND (CIS-BENE-FILE-01.CIS-BENE-TRUST EQ ' '    /* 100606
                    //*        OR  CIS-BENE-FILE-01.CIS-BENE-TRUST EQ 'N')    /* 100606
                    if (true) break FOR2;                                                                                                                                 //Natural: ESCAPE BOTTOM ( FOR2. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Total_Bene.nadd(1);                                                                                                                               //Natural: ADD 1 TO #TOTAL-BENE
                    if (condition(pnd_Total_Bene.greater(30)))                                                                                                            //Natural: IF #TOTAL-BENE GT 30
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  BADOTBLS
                        //*  BADOTBLS
                    }                                                                                                                                                     //Natural: END-IF
                    bene_File_Bene_Name.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Name().getValue(pnd_I));                        //Natural: ASSIGN BENE-NAME ( #TOTAL-BENE ) := CIS-CNTGNT-BENE-NAME ( #I )
                    bene_File_Bene_Extended_Name.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Extndd_Nme().getValue(pnd_I));         //Natural: ASSIGN BENE-EXTENDED-NAME ( #TOTAL-BENE ) := CIS-CNTGNT-BENE-EXTNDD-NME ( #I )
                    bene_File_Bene_Type.getValue(pnd_Total_Bene).setValue("C");                                                                                           //Natural: ASSIGN BENE-TYPE ( #TOTAL-BENE ) := 'C'
                    //*  BADOTBLS
                    bene_File_Bene_Dte_Birth_Trust.getValue(pnd_Total_Bene).setValueEdited(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dob().getValue(pnd_I),new      //Natural: MOVE EDITED CIS-CNTGNT-BENE-DOB ( #I ) ( EM = YYYYMMDD ) TO BENE-DTE-BIRTH-TRUST ( #TOTAL-BENE )
                        ReportEditMask("YYYYMMDD"));
                    bene_File_Bene_Relationship_Cde.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn_Cde().getValue(pnd_I));        //Natural: ASSIGN BENE-RELATIONSHIP-CDE ( #TOTAL-BENE ) := CIS-CNTGNT-BENE-RLTN-CDE ( #I )
                    bene_File_Bene_Relationship_Free_Txt.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Rltn().getValue(pnd_I));       //Natural: ASSIGN BENE-RELATIONSHIP-FREE-TXT ( #TOTAL-BENE ) := CIS-CNTGNT-BENE-RLTN ( #I )
                    //*  BADOTBLS
                    //*  BADOTBLS
                    if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr().getValue(pnd_I).notEquals(getZero())))                                        //Natural: IF CIS-CNTGNT-BENE-SSN-NBR ( #I ) NE 0
                    {
                        bene_File_Bene_Ssn_Cd.getValue(pnd_Total_Bene).setValue("S");                                                                                     //Natural: ASSIGN BENE-SSN-CD ( #TOTAL-BENE ) := 'S'
                        pnd_Cntgnt_Ssn_Pnd_Cntgnt_Ssn_N.setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Ssn_Nbr().getValue(pnd_I));                              //Natural: ASSIGN #CNTGNT-SSN-N := CIS-CNTGNT-BENE-SSN-NBR ( #I )
                        bene_File_Bene_Ssn_Nbr.getValue(pnd_Total_Bene).setValue(pnd_Cntgnt_Ssn);                                                                         //Natural: ASSIGN BENE-SSN-NBR ( #TOTAL-BENE ) := #CNTGNT-SSN
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BADOTBLS
                    if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(pnd_I).notEquals(getZero()) && ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_I).notEquals(getZero()))) //Natural: IF CIS-CNTGNT-BENE-NMRTR-NBR ( #I ) NE 0 AND CIS-CNTGNT-BENE-DNMNTR-NBR ( #I ) NE 0
                    {
                        bene_File_Bene_Prctge_Frctn_Ind.getValue(pnd_Total_Bene).setValue("S");                                                                           //Natural: ASSIGN BENE-PRCTGE-FRCTN-IND ( #TOTAL-BENE ) := 'S'
                        bene_File_Bene_Nmrtr_Nbr.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(pnd_I));          //Natural: ASSIGN BENE-NMRTR-NBR ( #TOTAL-BENE ) := CIS-CNTGNT-BENE-NMRTR-NBR ( #I )
                        bene_File_Bene_Dnmntr_Nbr.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_I));        //Natural: ASSIGN BENE-DNMNTR-NBR ( #TOTAL-BENE ) := CIS-CNTGNT-BENE-DNMNTR-NBR ( #I )
                        //*  BADOTBLS
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        bene_File_Bene_Prctge_Frctn_Ind.getValue(pnd_Total_Bene).setValue("P");                                                                           //Natural: ASSIGN BENE-PRCTGE-FRCTN-IND ( #TOTAL-BENE ) := 'P'
                        bene_File_Bene_Alloc_Pct.getValue(pnd_Total_Bene).setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Allctn_Pct().getValue(pnd_I));         //Natural: ASSIGN BENE-ALLOC-PCT ( #TOTAL-BENE ) := CIS-CNTGNT-BENE-ALLCTN-PCT ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  BADOTBLS
                }                                                                                                                                                         //Natural: END-IF
                bene_File_Bene_Spcl_Txt.getValue(pnd_Total_Bene,"*").setValue(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Spcl_Txt().getValue(pnd_I,                  //Natural: ASSIGN BENE-SPCL-TXT ( #TOTAL-BENE,* ) := CIS-CNTGNT-BENE-SPCL-TXT ( #I,* )
                    "*"));
                bene_File_Bene_Sttlmnt_Rstrctn.getValue(pnd_Total_Bene).setValue("N");                                                                                    //Natural: ASSIGN BENE-STTLMNT-RSTRCTN ( #TOTAL-BENE ) := 'N'
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  BADOTBLS
            if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn().equals("Y") && ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn().equals("Y"))) //Natural: IF CIS-PRMRY-BENE-CHILD-PRVSN EQ 'Y' AND CIS-CNTGNT-BENE-CHILD-PRVSN EQ 'Y'
            {
                bene_File_Bene_Pymnt_Chld_Dcsd_Ind.setValue("A");                                                                                                         //Natural: ASSIGN BENE-PYMNT-CHLD-DCSD-IND := 'A'
            }                                                                                                                                                             //Natural: END-IF
            //*  BADOTBLS
            if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn().equals("Y") && (ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn().equals("N")  //Natural: IF CIS-PRMRY-BENE-CHILD-PRVSN EQ 'Y' AND ( CIS-CNTGNT-BENE-CHILD-PRVSN EQ 'N' OR CIS-CNTGNT-BENE-CHILD-PRVSN EQ ' ' )
                || ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn().equals(" "))))
            {
                bene_File_Bene_Pymnt_Chld_Dcsd_Ind.setValue("B");                                                                                                         //Natural: ASSIGN BENE-PYMNT-CHLD-DCSD-IND := 'B'
            }                                                                                                                                                             //Natural: END-IF
            //*  BADOTBLS
            if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Cntgnt_Bene_Child_Prvsn().equals("Y") && (ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn().equals("N")  //Natural: IF CIS-CNTGNT-BENE-CHILD-PRVSN EQ 'Y' AND ( CIS-PRMRY-BENE-CHILD-PRVSN EQ 'N' OR CIS-PRMRY-BENE-CHILD-PRVSN EQ ' ' )
                || ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Child_Prvsn().equals(" "))))
            {
                bene_File_Bene_Pymnt_Chld_Dcsd_Ind.setValue("C");                                                                                                         //Natural: ASSIGN BENE-PYMNT-CHLD-DCSD-IND := 'C'
                //*  BADOTBLS
            }                                                                                                                                                             //Natural: END-IF
            bene_File_Bene_Nmbr_Of_Benes.setValue(pnd_Total_Bene);                                                                                                        //Natural: ASSIGN BENE-NMBR-OF-BENES := #TOTAL-BENE
            pnd_Total_Bene.reset();                                                                                                                                       //Natural: RESET #TOTAL-BENE
            //*  BADOTBLS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            bene_File_Bene_Dflt_To_Estate.setValue("Y");                                                                                                                  //Natural: ASSIGN BENE-DFLT-TO-ESTATE := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Bene() throws Exception                                                                                                                         //Natural: CALL-BENE
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        //*  BADOTBLS
        DbsUtil.callnat(Filtomdm.class , getCurrentProcessState(), bene_File);                                                                                            //Natural: CALLNAT 'FILTOMDM' BENE-FILE
        if (condition(Global.isEscape())) return;
        if (condition(bene_File_Bene_Return_Code.equals("E")))                                                                                                            //Natural: IF BENE-RETURN-CODE EQ 'E'
        {
            //*  BADOTBLS
                                                                                                                                                                          //Natural: PERFORM WRITE-EXCEPTION-REPORT
            sub_Write_Exception_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Total_Error.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOTAL-ERROR
        }                                                                                                                                                                 //Natural: END-IF
        //*  BADOTBLS
        if (condition(bene_File_Bene_Return_Code.equals("W")))                                                                                                            //Natural: IF BENE-RETURN-CODE EQ 'W'
        {
                                                                                                                                                                          //Natural: PERFORM RECORD-SENT-TO-BENE
            sub_Record_Sent_To_Bene();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Cis_Bene() throws Exception                                                                                                                   //Natural: UPDATE-CIS-BENE
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        GET1:                                                                                                                                                             //Natural: GET CIS-BENE-FILE-01 #ISN
        ldaCisl2081.getVw_cis_Bene_File_01().readByID(pnd_Isn.getLong(), "GET1");
        ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Sync_Ind().setValue("M");                                                                                                //Natural: ASSIGN CIS-BENE-SYNC-IND := 'M'
        ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Sync_Upd_Date().setValue(Global.getDATX());                                                                              //Natural: ASSIGN CIS-BENE-SYNC-UPD-DATE := *DATX
        ldaCisl2081.getVw_cis_Bene_File_01().updateDBRow("GET1");                                                                                                         //Natural: UPDATE ( GET1. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Write_Summary_Report() throws Exception                                                                                                              //Natural: WRITE-SUMMARY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Total.nsubtract(pnd_Total_Error);                                                                                                                             //Natural: ASSIGN #TOTAL := #TOTAL - #TOTAL-ERROR
        getReports().write(3, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 3 ) ' '
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,new TabSetting(35),pnd_Total_Read,new TabSetting(65),pnd_Total,new TabSetting(95),pnd_Total_Error,NEWLINE,new          //Natural: WRITE ( 3 ) 35T #TOTAL-READ 65T #TOTAL 95T #TOTAL-ERROR / 1T '-' ( 125 ) /
            TabSetting(1),"-",new RepeatItem(125),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE," ",NEWLINE,new TabSetting(1),"G r a n d   T o t a l",new TabSetting(35),pnd_Total_Read,new TabSetting(65),pnd_Total,new  //Natural: WRITE ( 3 ) ' ' / 1T 'G r a n d   T o t a l' 35T #TOTAL-READ 65T #TOTAL 95T #TOTAL-ERROR /// 52T '*** E N D  O F  R E P O R T ***'
            TabSetting(95),pnd_Total_Error,NEWLINE,NEWLINE,NEWLINE,new TabSetting(52),"*** E N D  O F  R E P O R T ***");
        if (Global.isEscape()) return;
    }
    private void sub_Write_Exception_Report() throws Exception                                                                                                            //Natural: WRITE-EXCEPTION-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        getReports().write(1, ReportOption.NOTITLE,bene_File_Bene_Pin_Nbr,"/",bene_File_Bene_Tiaa_Nbr,new ColumnSpacing(4),bene_File_Bene_Rqstng_System);                 //Natural: WRITE ( 1 ) BENE-PIN-NBR '/' BENE-TIAA-NBR 4X BENE-RQSTNG-SYSTEM
        if (Global.isEscape()) return;
    }
    private void sub_Record_Sent_To_Bene() throws Exception                                                                                                               //Natural: RECORD-SENT-TO-BENE
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pnd_Part_Name.reset();                                                                                                                                            //Natural: RESET #PART-NAME
        pnd_Part_Name.setValue(DbsUtil.compress(ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Annt_Prfx(), " ", ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Annt_Frst_Nme(),      //Natural: COMPRESS CIS-BENE-FILE-01.CIS-BENE-ANNT-PRFX ' ' CIS-BENE-FILE-01.CIS-BENE-ANNT-FRST-NME ' ' CIS-BENE-FILE-01.CIS-BENE-ANNT-MID-NME ' ' CIS-BENE-FILE-01.CIS-BENE-ANNT-LST-NME ' ' CIS-BENE-FILE-01.CIS-BENE-ANNT-SFFX INTO #PART-NAME
            " ", ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Annt_Mid_Nme(), " ", ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Annt_Lst_Nme(), " ", ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Annt_Sffx()));
        //*  BADOTBLS
        //*  BADOTBLS
        //*  BADOTBLS
        getReports().write(2, ReportOption.NOTITLE,bene_File_Bene_Pin_Nbr,"/",bene_File_Bene_Tiaa_Nbr,new ColumnSpacing(4),bene_File_Bene_Rqstng_System,new               //Natural: WRITE ( 2 ) BENE-PIN-NBR '/' BENE-TIAA-NBR 4X BENE-RQSTNG-SYSTEM 4X #PART-NAME
            ColumnSpacing(4),pnd_Part_Name);
        if (Global.isEscape()) return;
    }
    private void sub_Get_Todays_Bdate() throws Exception                                                                                                                  //Natural: GET-TODAYS-BDATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Today.setValue(Global.getDATN());                                                                                                                             //Natural: ASSIGN #TODAY := *DATN
        DbsUtil.callnat(Scin8888.class , getCurrentProcessState(), pnd_Today);                                                                                            //Natural: CALLNAT 'SCIN8888' #TODAY
        if (condition(Global.isEscape())) return;
        if (condition(DbsUtil.maskMatches(pnd_Today,"YYYYMMDD")))                                                                                                         //Natural: IF #TODAY EQ MASK ( YYYYMMDD )
        {
            pnd_Today_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Today_Pnd_Today_A);                                                                          //Natural: MOVE EDITED #TODAY-A TO #TODAY-DATE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Calc_Bene() throws Exception                                                                                                                   //Natural: CHECK-CALC-BENE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Name().getValue(pnd_I).notEquals(" ")))                                                              //Natural: IF CIS-BENE-FILE-01.CIS-PRMRY-BENE-NAME ( #I ) NE ' '
        {
            //*  BADOTBLS
            if (condition(ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Name().getValue(pnd_I).equals(ldaCisl2081.getCis_Bene_File_01_Cis_Clcltn_Bene_Name())            //Natural: IF CIS-BENE-FILE-01.CIS-PRMRY-BENE-NAME ( #I ) EQ CIS-BENE-FILE-01.CIS-CLCLTN-BENE-NAME AND CIS-BENE-FILE-01.CIS-PRMRY-BENE-SSN-NBR ( #I ) EQ CIS-BENE-FILE-01.CIS-CLCLTN-BENE-SSN-NBR AND CIS-BENE-FILE-01.CIS-PRMRY-BENE-DOB ( #I ) EQ CIS-BENE-FILE-01.CIS-CLCLTN-BENE-DOB
                && ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Ssn_Nbr().getValue(pnd_I).equals(ldaCisl2081.getCis_Bene_File_01_Cis_Clcltn_Bene_Ssn_Nbr()) 
                && ldaCisl2081.getCis_Bene_File_01_Cis_Prmry_Bene_Dob().getValue(pnd_I).equals(ldaCisl2081.getCis_Bene_File_01_Cis_Clcltn_Bene_Dob())))
            {
                bene_File_Bene_Mdo_Calc_Bene.getValue(pnd_I).setValue("Y");                                                                                               //Natural: ASSIGN BENE-MDO-CALC-BENE ( #I ) := 'Y'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Mit_Log_Dte_Time() throws Exception                                                                                                              //Natural: GET-MIT-LOG-DTE-TIME
    {
        if (BLNatReinput.isReinput()) return;

        //*  BADOTBLS
        DbsUtil.callnat(Cisn2084.class , getCurrentProcessState(), pnd_Cis_Tiaa_Nbr, pnd_Mit_Log_Dte_Time, bene_File_Bene_Tiaa_Cref_Ind, pnd_Found);                      //Natural: CALLNAT 'CISN2084' #CIS-TIAA-NBR #MIT-LOG-DTE-TIME BENE-TIAA-CREF-IND #FOUND
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(120),pnd_Today_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 120T #TODAY-DATE ( EM = MM/DD/YYYY ) // 1T 'Page :' *PAGE-NUMBER ( 1 ) 52T 'CIS' 120T *TIME / 40T 'BENEFICIARY INTERFACE SYSTEM' / 40T 'Exceptions Detail Report' / 40T 'Business Date: ' #TODAY-DATE ( EM = MM/DD/YYYY ) /// 'Sent Business Date:' #TODAY-DATE ( EM = MM/DD/YYYY ) // 'Pin' '/' 'Contract' 30T 'SYSTEM' 5X 'Error Message'/ 1T '------------------------' 30T '-----------' 1X '-' ( 60 )
                        TabSetting(1),"Page :",getReports().getPageNumberDbs(1),new TabSetting(52),"CIS",new TabSetting(120),Global.getTIME(),NEWLINE,new 
                        TabSetting(40),"BENEFICIARY INTERFACE SYSTEM",NEWLINE,new TabSetting(40),"Exceptions Detail Report",NEWLINE,new TabSetting(40),"Business Date: ",pnd_Today_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE,"Sent Business Date:",pnd_Today_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,"Pin","/","Contract",new 
                        TabSetting(30),"SYSTEM",new ColumnSpacing(5),"Error Message",NEWLINE,new TabSetting(1),"------------------------",new TabSetting(30),"-----------",new 
                        ColumnSpacing(1),"-",new RepeatItem(60));
                    //*    25T 'SYSTEM'
                    //*    1T '---------------------'
                    //*    25T '-----------'
                    //* PINE<<
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(120),pnd_Today_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new  //Natural: WRITE ( 2 ) NOTITLE *PROGRAM 120T #TODAY-DATE ( EM = MM/DD/YYYY ) // 1T 'Page :' *PAGE-NUMBER ( 2 ) 52T 'CIS' 120T *TIME / 40T 'BENEFICIARY INTERFACE SYSTEM' / 40T 'Successful Interface Detail Report' / 40T 'Business Date: ' #TODAY-DATE ( EM = MM/DD/YYYY ) /// 'Sent Business Date:' #TODAY-DATE ( EM = MM/DD/YYYY ) // 'Pin' '/' 'Contract' 30T 'SYSTEM' 42T 'NAME ASSOCIATED WITH CONTRACT' / 1T '------------------------' 30T '-----------' 1X '-' ( 60 )
                        TabSetting(1),"Page :",getReports().getPageNumberDbs(2),new TabSetting(52),"CIS",new TabSetting(120),Global.getTIME(),NEWLINE,new 
                        TabSetting(40),"BENEFICIARY INTERFACE SYSTEM",NEWLINE,new TabSetting(40),"Successful Interface Detail Report",NEWLINE,new TabSetting(40),"Business Date: ",pnd_Today_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE,"Sent Business Date:",pnd_Today_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,"Pin","/","Contract",new 
                        TabSetting(30),"SYSTEM",new TabSetting(42),"NAME ASSOCIATED WITH CONTRACT",NEWLINE,new TabSetting(1),"------------------------",new 
                        TabSetting(30),"-----------",new ColumnSpacing(1),"-",new RepeatItem(60));
                    //*    25T 'SYSTEM' 37T 'NAME ASSOCIATED WITH CONTRACT' /
                    //*    1T '---------------------'
                    //*    25T '-----------'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(120),pnd_Today_Date, new ReportEditMask               //Natural: WRITE ( 3 ) NOTITLE 1T *PROGRAM 120T #TODAY-DATE ( EM = MM/DD/YYYY ) // 1T 'Page :' *PAGE-NUMBER ( 3 ) 52T 'CIS' 120T *TIME / 40T 'BENEFICIARY INTERFACE SYSTEM' / 40T 'Interface Summary Report' / 40T 'Business Date: ' #TODAY-DATE ( EM = MM/DD/YYYY ) /// 32T 'Total Designations' 62T 'Total Designations' 92T 'Total' / 32T 'Read from CIS' 62T 'Written to Interface' 92T 'Errors' 1X '_' ( 132 )
                        ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(1),"Page :",getReports().getPageNumberDbs(3),new TabSetting(52),"CIS",new TabSetting(120),Global.getTIME(),NEWLINE,new 
                        TabSetting(40),"BENEFICIARY INTERFACE SYSTEM",NEWLINE,new TabSetting(40),"Interface Summary Report",NEWLINE,new TabSetting(40),"Business Date: ",pnd_Today_Date, 
                        new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,NEWLINE,new TabSetting(32),"Total Designations",new TabSetting(62),"Total Designations",new 
                        TabSetting(92),"Total",NEWLINE,new TabSetting(32),"Read from CIS",new TabSetting(62),"Written to Interface",new TabSetting(92),"Errors",new 
                        ColumnSpacing(1),"_",new RepeatItem(132));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60");
        Global.format(2, "LS=133 PS=60");
        Global.format(3, "LS=133 PS=60");
    }
}
