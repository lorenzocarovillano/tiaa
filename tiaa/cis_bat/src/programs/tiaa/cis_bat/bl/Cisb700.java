/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:21:44 PM
**        * FROM NATURAL PROGRAM : Cisb700
************************************************************
**        * FILE NAME            : Cisb700.java
**        * CLASS NAME           : Cisb700
**        * INSTANCE NAME        : Cisb700
************************************************************
**======================================================================
** SYSTEM         : CIS
** DESCRIPTION    : THIS PROGRAM WILL UPDATE DATA FROM A WORK FILE TO AN
**                : ADABAS FILE.
** PRIME KEY      : SEQ READ
** INPUT FILE     : WORK FILE OR TAPE
** OUTPUT FILE    : THIS WILL BE THE ADABAS FILE (KDO)
** PROGRAM        : CISB700
** GENERATED      :
** FUNCTION       : THIS PROGRAM WILL UPDATE RECORDS THAT WERE WRITTEN
**                : TO THE EXTRACT FILE BY CISPR200.
** MODIFICATIONS  :  DATE     MOD BY      DESC OF CHANGE
**                :  2-20-09  K. GATES    RESTOW FOR CISLPART
**                :  6-04-09  K. GATES    RESTOW FOR CISLPART
**                :  3-24-10  C. MASON    RESTOW FOR CISLPART & CISPARTV
**                : 10-25-10  G. GUERRERO RESTOW FOR CISLPART & CISPARTV
**                : 12-13-14  W. BAUER    RESTOW FOR CISLPART & CISPARTV
**                : 09-01-15  B. ELLO     RESTOW FOR CISPARTV FOR BENE
**                                        IN THE PLAN PROJECT (BIP)
**                :  5-09-17  GHOSABE     PIN EXPANSION CHANGES(C420007)
**                :                       RESTOW ONLY- PINE.
**                : 11-16-19  L. SHU      RESTOW FOR CISLPART UPDATES
**======================================================================

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb700 extends BLNatBase
{
    // Data Areas
    private LdaCispartv ldaCispartv;
    private LdaCislpart ldaCislpart;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Read_Counter;
    private DbsField pnd_Records_Written;
    private DbsField pnd_Update_Cnt;
    private DbsField pnd_Work_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCispartv = new LdaCispartv();
        registerRecord(ldaCispartv);
        registerRecord(ldaCispartv.getVw_cis_Part_View());
        ldaCislpart = new LdaCislpart();
        registerRecord(ldaCislpart);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Read_Counter = localVariables.newFieldInRecord("pnd_Read_Counter", "#READ-COUNTER", FieldType.NUMERIC, 7);
        pnd_Records_Written = localVariables.newFieldInRecord("pnd_Records_Written", "#RECORDS-WRITTEN", FieldType.NUMERIC, 7);
        pnd_Update_Cnt = localVariables.newFieldInRecord("pnd_Update_Cnt", "#UPDATE-CNT", FieldType.NUMERIC, 7);
        pnd_Work_Cnt = localVariables.newFieldInRecord("pnd_Work_Cnt", "#WORK-CNT", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCispartv.initializeValues();
        ldaCislpart.initializeValues();

        localVariables.reset();
        pnd_Update_Cnt.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisb700() throws Exception
    {
        super("Cisb700");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *---------------------------MAIN LOGIC---------------------------------
        //*  ZP=ON
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 62 LS = 130;//Natural: FORMAT ( 2 ) PS = 62 LS = 75 ZP = ON;//Natural: FORMAT ( 3 ) PS = 62 LS = 75 ZP = ON
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT JUSTIFIED 1T *DATU '-' *TIMX ( EM = HH:IIAP ) 29T 'UPDATE OF THE KDO  ' 60T 'PG:  ' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / 1T *INIT-USER '-' *PROGRAM 60T 'UPDATE: RPT ' /
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #PARTICIPANT-WORK-FILE
        while (condition(getWorkFiles().read(1, ldaCislpart.getPnd_Participant_Work_File())))
        {
            pnd_Read_Counter.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #READ-COUNTER
            ldaCispartv.getVw_cis_Part_View().startDatabaseRead                                                                                                           //Natural: READ CIS-PART-VIEW BY CIS-RQST-ID-KEY = #RQST-ID-KEY
            (
            "PND_PND_L0490",
            new Wc[] { new Wc("CIS_RQST_ID_KEY", ">=", ldaCislpart.getPnd_Participant_Work_File_Pnd_Rqst_Id_Key(), WcType.BY) },
            new Oc[] { new Oc("CIS_RQST_ID_KEY", "ASC") }
            );
            PND_PND_L0490:
            while (condition(ldaCispartv.getVw_cis_Part_View().readNextRow("PND_PND_L0490")))
            {
                if (condition(ldaCispartv.getCis_Part_View_Cis_Rqst_Id_Key().equals(ldaCislpart.getPnd_Participant_Work_File_Pnd_Rqst_Id_Key())))                         //Natural: IF CIS-RQST-ID-KEY = #RQST-ID-KEY
                {
                    ldaCispartv.getCis_Part_View_Cis_Status_Cd().setValue("P");                                                                                           //Natural: MOVE 'P' TO CIS-STATUS-CD
                    ldaCispartv.getCis_Part_View_Cis_Opn_Clsd_Ind().setValue("C");                                                                                        //Natural: MOVE 'C' TO CIS-OPN-CLSD-IND
                    ldaCispartv.getVw_cis_Part_View().updateDBRow("PND_PND_L0490");                                                                                       //Natural: UPDATE ( ##L0490. )
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    pnd_Update_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO #UPDATE-CNT
                    getReports().write(1, "TIAA CNTRCT NUM           = ",new TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Tiaa_Nbr(),NEWLINE,"CREF CNTRCT NUM           = ",new  //Natural: WRITE ( 1 ) 'TIAA CNTRCT NUM           = ' 30T CIS-TIAA-NBR / 'CREF CNTRCT NUM           = ' 30T CIS-CERT-NBR / 'KEY FOR CIS               = ' 30T CIS-RQST-ID-KEY / 'CIS STATUS IND            = ' 30T CIS-STATUS-CD / 'CIS OPN CLSD IND          = ' 30T CIS-OPN-CLSD-IND ///
                        TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Cert_Nbr(),NEWLINE,"KEY FOR CIS               = ",new TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Rqst_Id_Key(),NEWLINE,"CIS STATUS IND            = ",new 
                        TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Status_Cd(),NEWLINE,"CIS OPN CLSD IND          = ",new TabSetting(30),ldaCispartv.getCis_Part_View_Cis_Opn_Clsd_Ind(),
                        NEWLINE,NEWLINE,NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L0490"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0490"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* *
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, "TOTAL NUMBER OF RECORDS READ   ",pnd_Read_Counter);                                                                                        //Natural: WRITE ( 1 ) 'TOTAL NUMBER OF RECORDS READ   ' #READ-COUNTER
        if (Global.isEscape()) return;
        getReports().write(1, "TOTAL NUMBER OF RECORDS UPDATED",pnd_Update_Cnt);                                                                                          //Natural: WRITE ( 1 ) 'TOTAL NUMBER OF RECORDS UPDATED' #UPDATE-CNT
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=62 LS=130");
        Global.format(2, "PS=62 LS=75 ZP=ON");
        Global.format(3, "PS=62 LS=75 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(29),"UPDATE OF THE KDO  ",new TabSetting(60),"PG:  ",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new 
            TabSetting(1),Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(60),"UPDATE: RPT ",NEWLINE);
    }
}
