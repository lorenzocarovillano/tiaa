/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:23:07 PM
**        * FROM NATURAL PROGRAM : Cisp0051
************************************************************
**        * FILE NAME            : Cisp0051.java
**        * CLASS NAME           : Cisp0051
**        * INSTANCE NAME        : Cisp0051
************************************************************
************************************************************************
* PROGRAM  : CISP0051
* SYSTEM   : CIS
* TITLE    : CIS SYNCHRONOUS PROXY
* GENERATED: 04/12/2018
* FUNCTION : THIS MODULE IS CALLED FROM A NATURAL 'wrapper' THAT IS
*            CALLED FROM A LEGACY APPLICATION. INPUT KEY IS PASSED TO
*            IT AND FIXED LENGTH MESSAGE IS CREATED AND 'PUT' TO THE
*            REQUEST QUEUE AND THEN A GET IS ISSUED TO GET DATA FROM
*            THE REPLY QUEUE. THE DATA IS THEN MOVED TO THE PDA AND
*            CONTROL IS PASSED BACK TO THE CALLING WRAPPER AND ON TO
*            THE APPLICATION .
************************************************************************
* DATE      USERID     DESCRIPTION
*
************************************************************************
* 04/12/18  NEWSOM     INITIAL IMPLEMENTATION
************************************************************************
*

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisp0051 extends BLNatBase
{
    // Data Areas
    private GdaCisg0051 gdaCisg0051;
    private PdaErla1000 pdaErla1000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_curr_Environment;
    private DbsField curr_Environment_Env_Id;
    private DbsField curr_Environment_Env_Descp;
    private DbsField pnd_Qmgrname_For_Curr_Env;

    private DbsGroup pnd_Qmgrname_For_Curr_Env__R_Field_1;

    private DbsGroup pnd_Qmgrname_For_Curr_Env_Pnd_Table;
    private DbsField pnd_Qmgrname_For_Curr_Env_Pnd_Curr_Env;
    private DbsField pnd_Qmgrname_For_Curr_Env_Pnd_Queuemgr;
    private DbsField pnd_Indx;

    private DataAccessProgramView vw_error_Trigger;
    private DbsField error_Trigger_Err_Trigger;
    private DbsField error_Trigger_Err_Trigger_Term;
    private DbsField error_Trigger_Err_Trigger_Tran;
    private DbsField error_Trigger_Err_Trigger_User;
    private DbsField error_Trigger_Err_Trigger_Region;
    private DbsField error_Trigger_Err_Trigger_Timestamp;
    private DbsField error_Trigger_Err_Mqmd_Msgid;
    private DbsField error_Trigger_Err_Error_Queue;
    private DbsField pnd_Data_In;

    private DbsGroup pnd_Data_In__R_Field_2;
    private DbsField pnd_Data_In_Pnd_In_Msg_Guid;
    private DbsField pnd_Data_In_Pnd_In_Msg_Ping_Ind;
    private DbsField pnd_Data_In_Pnd_In_Sender_Appl_Id;
    private DbsField pnd_Data_In_Pnd_In_Tgt_Appl_Id;
    private DbsField pnd_Data_In_Pnd_In_Tgt_Module_Type;
    private DbsField pnd_Data_In_Pnd_In_Tgt_Module_Name;
    private DbsField pnd_Data_In_Pnd_In_Pda_Ctr;
    private DbsField pnd_Data_In_Pnd_In_Pda_Length;
    private DbsField pnd_Data_In_Pnd_In_Msg_Date_Sent;
    private DbsField pnd_Data_In_Pnd_In_Msg_Time_Sent;
    private DbsField pnd_Data_In_Pnd_In_Msg_Filler;
    private DbsField pnd_Data_In_Pnd_In_Request_Text;
    private DbsField pnd_Appl_Data_Out;

    private DbsGroup pnd_Appl_Data_Out__R_Field_3;

    private DbsGroup pnd_Appl_Data_Out_Pnd_Header;
    private DbsField pnd_Appl_Data_Out_Pnd_Msg_Guid;
    private DbsField pnd_Appl_Data_Out_Pnd_Msg_Ping_Ind;
    private DbsField pnd_Appl_Data_Out_Pnd_Sender_Appl_Id;
    private DbsField pnd_Appl_Data_Out_Pnd_Tgt_Appl_Id;
    private DbsField pnd_Appl_Data_Out_Pnd_Tgt_Module_Code;
    private DbsField pnd_Appl_Data_Out_Pnd_Tgt_Module_Name;
    private DbsField pnd_Appl_Data_Out_Pnd_Pda_Ctr;
    private DbsField pnd_Appl_Data_Out_Pnd_Pda_Length;
    private DbsField pnd_Appl_Data_Out_Pnd_Msg_Date_Sent;
    private DbsField pnd_Appl_Data_Out_Pnd_Msg_Time_Sent;
    private DbsField pnd_Appl_Data_Out_Pnd_Header_Filler;

    private DbsGroup pnd_Appl_Data_Out_Pnd_Response;
    private DbsField pnd_Appl_Data_Out_Pnd_Response_Code;
    private DbsField pnd_Appl_Data_Out_Pnd_Response_Text;
    private DbsField pnd_Appl_Data_Out_Pnd_Response_Date;
    private DbsField pnd_Appl_Data_Out_Pnd_Response_Time;
    private DbsField pnd_Appl_Data_Out_Pnd_Response_Filler;
    private DbsField pnd_Appl_Data_Out_Pnd_Response_Mdm;
    private DbsField pnd_Unique_Id;
    private DbsField pnd_Prefix;
    private DbsField pnd_Xml_Length;
    private DbsField pnd_Format_Date_Sent;
    private DbsField pnd_Format_Time_Sent;
    private DbsField pnd_Short_Text;

    private DbsGroup pnd_Msgbuffer;

    private DbsGroup pnd_Msgbuffer_Pnd_Msgstring0;
    private DbsField pnd_Msgbuffer_Pnd_Msgstring1;
    private DbsField pnd_Msgbuffer_Pnd_Msgstring;
    private DbsField pnd_Error_Message;

    private DbsGroup pnd_Error_Message__R_Field_4;
    private DbsField pnd_Error_Message_Pnd_Mq_Message_Id;
    private DbsField pnd_Error_Message_Pnd_Reason_Code_Error;
    private DbsField pnd_Error_Message_Pnd_Reason_Code_Level;
    private DbsField pnd_Error_Message_Pnd_Short_Text_Error;
    private DbsField pnd_Error_Message_Pnd_Mq_Function_Error;
    private DbsField pnd_Error_Message_Pnd_Completion_Code_Error;

    private DbsGroup pnd_Error_Handler_Area;
    private DbsField pnd_Error_Handler_Area_Pnd_Error_Number;
    private DbsField pnd_Error_Handler_Area_Pnd_Error_Line;
    private DbsField pnd_Error_Handler_Area_Pnd_Error_Status;
    private DbsField pnd_Error_Handler_Area_Pnd_Program_Name;
    private DbsField pnd_Error_Handler_Area_Pnd_Program_Level;
    private DbsField pnd_Error_Handler_Area_Pnd_Mq_Function;
    private DbsField pnd_Error_Handler_Area_Pnd_Completion_Code;
    private DbsField pnd_Error_Handler_Area_Pnd_Error_Text;
    private DbsField pnd_Reason_Code;
    private DbsField pnd_Alternate_Userid;
    private DbsField pnd_Dbl_Quote;
    private DbsField pnd_Exclaim;
    private DbsField pnd_Lbracket;
    private DbsField pnd_Rbracket;
    private DbsField pnd_Carriage_Rtn;
    private DbsField pnd_Xml_Rec_Length;
    private DbsField pnd_Msg_Length_Out1;
    private DbsField pnd_Msg_Length_Out2;
    private DbsField pnd_Guid_Post;

    private DbsGroup pnd_Xml_File_Dflts;
    private DbsField pnd_Xml_File_Dflts_Pnd_Ln_Strt;
    private DbsField pnd_Xml_File_Dflts_Pnd_Ln_End;
    private DbsField pnd_Xml_Record;

    private DbsGroup pnd_Xml_Record__R_Field_5;
    private DbsField pnd_Xml_Record_Pnd_Rcrd_Prefix;
    private DbsField pnd_Xml_Record_Pnd_Rcrd_Bdy;
    private DbsField pnd_Xml_Record_Pnd_Suffix;

    private DbsGroup pnd_Xml_Record__R_Field_6;
    private DbsField pnd_Xml_Record_Pnd_Xml_Record_Array;
    private DbsField pnd_Qmgrname;
    private DbsField pnd_Inputqname;
    private DbsField pnd_Bufferlength;
    private DbsField pnd_Charattrlength;
    private DbsField pnd_Charattrs;
    private DbsField pnd_Closeoptions;
    private DbsField pnd_Compcode;
    private DbsField pnd_Datalength;
    private DbsField pnd_Datalength_Out;
    private DbsField pnd_Getmsgopt;
    private DbsField pnd_Hconn;
    private DbsField pnd_Hobj1;
    private DbsField pnd_Hobj2;
    private DbsField pnd_Intattrcount;
    private DbsField pnd_Intattrs;
    private DbsField pnd_Messagelength;
    private DbsField pnd_Msgdesc;
    private DbsField pnd_Objdesc;
    private DbsField pnd_Openoptions;
    private DbsField pnd_Putmsgopt;
    private DbsField pnd_Reason;
    private DbsField pnd_Selectorcount;
    private DbsField pnd_Selectors;
    private DbsField pnd_Expiry_Interval;
    private DbsField pnd_Save_Correlid;
    private DbsField pnd_Wait_Interval;
    private DbsField mqci_None;
    private DbsField mqco_None;
    private DbsField mqfmt_Md_Extension;
    private DbsField mqfmt_String;
    private DbsField mqmi_None;
    private DbsField mqoo_Browse;
    private DbsField mqoo_Input_Shared;
    private DbsField mqoo_Inquire;
    private DbsField mqoo_Output;
    private DbsField mqoo_Alternate_User_Authority;
    private DbsField mqgmo_Accept_Truncated_Msg;
    private DbsField mqgmo_No_Syncpoint;
    private DbsField mqgmo_Syncpoint;
    private DbsField mqpmo_No_Syncpoint;
    private DbsField mqgmo_Wait;
    private DbsField mqia_Current_Q_Depth;
    private DbsField mqpmo_Alternate_User_Authority;
    private DbsField mqper_Persistent;
    private DbsField mqper_Not_Persistent;
    private DbsField mqwi_Unlimited;
    private DbsField mqgmo_Fail_If_Quiescing;
    private DbsField mqgmo_Convert;

    private DbsGroup mqh_Object_Mqrfh2;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2_Strucid;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2_Version;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2_Struclength;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2_Encoding;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2_Codedcharsetid;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2_Format;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2_Flags;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2_Namevalueccsid;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2_Namevaluelength;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2_Namevaluedata;

    private DbsGroup mqh_Object_Mqrfh2__R_Field_7;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2;

    private DbsGroup mqh_Object_Mqrfh2__R_Field_8;
    private DbsField mqh_Object_Mqrfh2_Mqrfh2_String;

    private DbsGroup mqm_Object_Descriptor;
    private DbsField mqm_Object_Descriptor_Mqod_Strucid;
    private DbsField mqm_Object_Descriptor_Mqod_Version;
    private DbsField mqm_Object_Descriptor_Mqod_Objecttype;
    private DbsField mqm_Object_Descriptor_Mqod_Objectname;
    private DbsField mqm_Object_Descriptor_Mqod_Objectqmgrname;
    private DbsField mqm_Object_Descriptor_Mqod_Dynamicqname;
    private DbsField mqm_Object_Descriptor_Mqod_Alternateuserid;
    private DbsField mqm_Object_Descriptor_Mqod_Recspresent;
    private DbsField mqm_Object_Descriptor_Mqod_Knowndestcount;
    private DbsField mqm_Object_Descriptor_Mqod_Unknowndestcount;
    private DbsField mqm_Object_Descriptor_Mqod_Invaliddestcount;
    private DbsField mqm_Object_Descriptor_Mqod_Objectrecoffset;
    private DbsField mqm_Object_Descriptor_Mqod_Responserecoffset;
    private DbsField mqm_Object_Descriptor_Mqod_Objectrecptr;
    private DbsField mqm_Object_Descriptor_Mqod_Responserecptr;
    private DbsField mqm_Object_Descriptor_Mqod_Alternatesecurityid;
    private DbsField mqm_Object_Descriptor_Mqod_Resolvedqname;
    private DbsField mqm_Object_Descriptor_Mqod_Resolvedqmgrname;

    private DbsGroup mqm_Object_Descriptor__R_Field_9;
    private DbsField mqm_Object_Descriptor_Mqod;

    private DbsGroup mqm_Message_Descriptor;
    private DbsField mqm_Message_Descriptor_Mqmd_Strucid;
    private DbsField mqm_Message_Descriptor_Mqmd_Version;
    private DbsField mqm_Message_Descriptor_Mqmd_Report;
    private DbsField mqm_Message_Descriptor_Mqmd_Msgtype;
    private DbsField mqm_Message_Descriptor_Mqmd_Expiry;
    private DbsField mqm_Message_Descriptor_Mqmd_Feedback;
    private DbsField mqm_Message_Descriptor_Mqmd_Encoding;
    private DbsField mqm_Message_Descriptor_Mqmd_Codedcharsetid;
    private DbsField mqm_Message_Descriptor_Mqmd_Format;
    private DbsField mqm_Message_Descriptor_Mqmd_Priority;
    private DbsField mqm_Message_Descriptor_Mqmd_Persistence;
    private DbsField mqm_Message_Descriptor_Mqmd_Msgid;
    private DbsField mqm_Message_Descriptor_Mqmd_Correlid;
    private DbsField mqm_Message_Descriptor_Mqmd_Backoutcount;
    private DbsField mqm_Message_Descriptor_Mqmd_Replytoq;
    private DbsField mqm_Message_Descriptor_Mqmd_Replytoqmgr;
    private DbsField mqm_Message_Descriptor_Mqmd_Useridentifier;
    private DbsField mqm_Message_Descriptor_Mqmd_Accountingtoken;
    private DbsField mqm_Message_Descriptor_Mqmd_Applidentitydata;
    private DbsField mqm_Message_Descriptor_Mqmd_Putappltype;
    private DbsField mqm_Message_Descriptor_Mqmd_Putapplname;
    private DbsField mqm_Message_Descriptor_Mqmd_Putdate;
    private DbsField mqm_Message_Descriptor_Mqmd_Puttime;
    private DbsField mqm_Message_Descriptor_Mqmd_Applorigindata;

    private DbsGroup mqm_Message_Descriptor__R_Field_10;
    private DbsField mqm_Message_Descriptor_Mqmd;

    private DbsGroup mqm_Get_Message_Options;
    private DbsField mqm_Get_Message_Options_Mqgmo_Strucid;
    private DbsField mqm_Get_Message_Options_Mqgmo_Version;
    private DbsField mqm_Get_Message_Options_Mqgmo_Options;
    private DbsField mqm_Get_Message_Options_Mqgmo_Waitinterval;
    private DbsField mqm_Get_Message_Options_Mqgmo_Signal1;
    private DbsField mqm_Get_Message_Options_Mqgmo_Signal2;
    private DbsField mqm_Get_Message_Options_Mqgmo_Resolvedqname;
    private DbsField mqm_Get_Message_Options_Mqgmo_Matchoptions;
    private DbsField mqm_Get_Message_Options_Mqgmo_Groupstatus;
    private DbsField mqm_Get_Message_Options_Mqgmo_Segmentstatus;
    private DbsField mqm_Get_Message_Options_Mqgmo_Segmentation;
    private DbsField mqm_Get_Message_Options_Mqgmo_Reserved1;
    private DbsField mqm_Get_Message_Options_Mqgmo_Msgtoken;
    private DbsField mqm_Get_Message_Options_Mqgmo_Returnedlength;

    private DbsGroup mqm_Get_Message_Options__R_Field_11;
    private DbsField mqm_Get_Message_Options_Mqgmo;

    private DbsGroup mqm_Put_Message_Options;
    private DbsField mqm_Put_Message_Options_Mqpmo_Strucid;
    private DbsField mqm_Put_Message_Options_Mqpmo_Version;
    private DbsField mqm_Put_Message_Options_Mqpmo_Options;
    private DbsField mqm_Put_Message_Options_Mqpmo_Timeout;
    private DbsField mqm_Put_Message_Options_Mqpmo_Context;
    private DbsField mqm_Put_Message_Options_Mqpmo_Knowndestcount;
    private DbsField mqm_Put_Message_Options_Mqpmo_Unknowndestcount;
    private DbsField mqm_Put_Message_Options_Mqpmo_Invaliddestcount;
    private DbsField mqm_Put_Message_Options_Mqpmo_Resolvedqname;
    private DbsField mqm_Put_Message_Options_Mqpmo_Resolvedqmgrname;

    private DbsGroup mqm_Put_Message_Options__R_Field_12;
    private DbsField mqm_Put_Message_Options_Mqpmo;

    private DbsGroup mq_Error_Data;
    private DbsField mq_Error_Data_Mq_Error_Prog_Id;
    private DbsField mq_Error_Data_Mq_Error_Filler1;
    private DbsField mq_Error_Data_Mq_Error_Command;
    private DbsField mq_Error_Data_Mq_Error_Filler2;
    private DbsField mq_Error_Data_Mq_Error_Code;
    private DbsField mq_Error_Data_Mq_Error_Filler3;
    private DbsField mq_Error_Data_Mq_Error_Reason;
    private DbsField mq_Error_Data_Mq_Error_Filler4;
    private DbsField mq_Error_Data_Mq_Error_Resource;

    private DbsGroup cmqtml;
    private DbsField cmqtml_Mqtm_Strucid;
    private DbsField cmqtml_Mqtm_Version;
    private DbsField cmqtml_Mqtm_Qname;
    private DbsField cmqtml_Mqtm_Processname;
    private DbsField cmqtml_Mqtm_Triggerdata;
    private DbsField cmqtml_Mqtm_Appltype;
    private DbsField cmqtml_Mqtm_Applid;
    private DbsField cmqtml_Mqtm_Envdata;
    private DbsField cmqtml_Mqtm_Userdata;
    private DbsField pls_Trace;
    private DbsField pls_Qmgrname;
    private DbsField pls_Hconn;
    private DbsField pls_Mqod;
    private DbsField pls_Hobj1;
    private DbsField pls_Hobj2;
    private int mqconnReturnCode;
    private int mqopenReturnCode;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaCisg0051 = GdaCisg0051.getInstance(getCallnatLevel());
        registerRecord(gdaCisg0051);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaErla1000 = new PdaErla1000(localVariables);

        // Local Variables

        vw_curr_Environment = new DataAccessProgramView(new NameInfo("vw_curr_Environment", "CURR-ENVIRONMENT"), "ENV_CURR", "ENV_CURR");
        curr_Environment_Env_Id = vw_curr_Environment.getRecord().newFieldInGroup("curr_Environment_Env_Id", "ENV-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ENV_ID");
        curr_Environment_Env_Descp = vw_curr_Environment.getRecord().newFieldInGroup("curr_Environment_Env_Descp", "ENV-DESCP", FieldType.STRING, 16, 
            RepeatingFieldStrategy.None, "ENV_DESCP");
        registerRecord(vw_curr_Environment);

        pnd_Qmgrname_For_Curr_Env = localVariables.newFieldArrayInRecord("pnd_Qmgrname_For_Curr_Env", "#QMGRNAME-FOR-CURR-ENV", FieldType.STRING, 6, new 
            DbsArrayController(1, 12));

        pnd_Qmgrname_For_Curr_Env__R_Field_1 = localVariables.newGroupInRecord("pnd_Qmgrname_For_Curr_Env__R_Field_1", "REDEFINE", pnd_Qmgrname_For_Curr_Env);

        pnd_Qmgrname_For_Curr_Env_Pnd_Table = pnd_Qmgrname_For_Curr_Env__R_Field_1.newGroupArrayInGroup("pnd_Qmgrname_For_Curr_Env_Pnd_Table", "#TABLE", 
            new DbsArrayController(1, 11));
        pnd_Qmgrname_For_Curr_Env_Pnd_Curr_Env = pnd_Qmgrname_For_Curr_Env_Pnd_Table.newFieldInGroup("pnd_Qmgrname_For_Curr_Env_Pnd_Curr_Env", "#CURR-ENV", 
            FieldType.STRING, 2);
        pnd_Qmgrname_For_Curr_Env_Pnd_Queuemgr = pnd_Qmgrname_For_Curr_Env_Pnd_Table.newFieldInGroup("pnd_Qmgrname_For_Curr_Env_Pnd_Queuemgr", "#QUEUEMGR", 
            FieldType.STRING, 4);
        pnd_Indx = localVariables.newFieldInRecord("pnd_Indx", "#INDX", FieldType.NUMERIC, 2);

        vw_error_Trigger = new DataAccessProgramView(new NameInfo("vw_error_Trigger", "ERROR-TRIGGER"), "ERROR_TRIGGER", "ERROR_HANDLER");
        error_Trigger_Err_Trigger = vw_error_Trigger.getRecord().newFieldInGroup("error_Trigger_Err_Trigger", "ERR-TRIGGER", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ERR_TRIGGER");
        error_Trigger_Err_Trigger_Term = vw_error_Trigger.getRecord().newFieldInGroup("error_Trigger_Err_Trigger_Term", "ERR-TRIGGER-TERM", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ERR_TRIGGER_TERM");
        error_Trigger_Err_Trigger_Tran = vw_error_Trigger.getRecord().newFieldInGroup("error_Trigger_Err_Trigger_Tran", "ERR-TRIGGER-TRAN", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ERR_TRIGGER_TRAN");
        error_Trigger_Err_Trigger_User = vw_error_Trigger.getRecord().newFieldInGroup("error_Trigger_Err_Trigger_User", "ERR-TRIGGER-USER", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ERR_TRIGGER_USER");
        error_Trigger_Err_Trigger_Region = vw_error_Trigger.getRecord().newFieldInGroup("error_Trigger_Err_Trigger_Region", "ERR-TRIGGER-REGION", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ERR_TRIGGER_REGION");
        error_Trigger_Err_Trigger_Timestamp = vw_error_Trigger.getRecord().newFieldInGroup("error_Trigger_Err_Trigger_Timestamp", "ERR-TRIGGER-TIMESTAMP", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ERR_TRIGGER_TIMESTAMP");
        error_Trigger_Err_Mqmd_Msgid = vw_error_Trigger.getRecord().newFieldInGroup("error_Trigger_Err_Mqmd_Msgid", "ERR-MQMD-MSGID", FieldType.STRING, 
            24, RepeatingFieldStrategy.None, "ERR_MQMD_MSGID");
        error_Trigger_Err_Error_Queue = vw_error_Trigger.getRecord().newFieldInGroup("error_Trigger_Err_Error_Queue", "ERR-ERROR-QUEUE", FieldType.STRING, 
            48, RepeatingFieldStrategy.None, "ERR_ERROR_QUEUE");
        registerRecord(vw_error_Trigger);

        pnd_Data_In = localVariables.newFieldArrayInRecord("pnd_Data_In", "#DATA-IN", FieldType.STRING, 1, new DbsArrayController(1, 32760));

        pnd_Data_In__R_Field_2 = localVariables.newGroupInRecord("pnd_Data_In__R_Field_2", "REDEFINE", pnd_Data_In);
        pnd_Data_In_Pnd_In_Msg_Guid = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_In_Msg_Guid", "#IN-MSG-GUID", FieldType.STRING, 60);
        pnd_Data_In_Pnd_In_Msg_Ping_Ind = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_In_Msg_Ping_Ind", "#IN-MSG-PING-IND", FieldType.STRING, 
            1);
        pnd_Data_In_Pnd_In_Sender_Appl_Id = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_In_Sender_Appl_Id", "#IN-SENDER-APPL-ID", FieldType.STRING, 
            8);
        pnd_Data_In_Pnd_In_Tgt_Appl_Id = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_In_Tgt_Appl_Id", "#IN-TGT-APPL-ID", FieldType.STRING, 
            8);
        pnd_Data_In_Pnd_In_Tgt_Module_Type = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_In_Tgt_Module_Type", "#IN-TGT-MODULE-TYPE", FieldType.STRING, 
            1);
        pnd_Data_In_Pnd_In_Tgt_Module_Name = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_In_Tgt_Module_Name", "#IN-TGT-MODULE-NAME", FieldType.STRING, 
            8);
        pnd_Data_In_Pnd_In_Pda_Ctr = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_In_Pda_Ctr", "#IN-PDA-CTR", FieldType.NUMERIC, 2);
        pnd_Data_In_Pnd_In_Pda_Length = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_In_Pda_Length", "#IN-PDA-LENGTH", FieldType.NUMERIC, 5);
        pnd_Data_In_Pnd_In_Msg_Date_Sent = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_In_Msg_Date_Sent", "#IN-MSG-DATE-SENT", FieldType.STRING, 
            10);
        pnd_Data_In_Pnd_In_Msg_Time_Sent = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_In_Msg_Time_Sent", "#IN-MSG-TIME-SENT", FieldType.STRING, 
            8);
        pnd_Data_In_Pnd_In_Msg_Filler = pnd_Data_In__R_Field_2.newFieldInGroup("pnd_Data_In_Pnd_In_Msg_Filler", "#IN-MSG-FILLER", FieldType.STRING, 89);
        pnd_Data_In_Pnd_In_Request_Text = pnd_Data_In__R_Field_2.newFieldArrayInGroup("pnd_Data_In_Pnd_In_Request_Text", "#IN-REQUEST-TEXT", FieldType.STRING, 
            1, new DbsArrayController(1, 32560));
        pnd_Appl_Data_Out = localVariables.newFieldArrayInRecord("pnd_Appl_Data_Out", "#APPL-DATA-OUT", FieldType.STRING, 1, new DbsArrayController(1, 
            32760));

        pnd_Appl_Data_Out__R_Field_3 = localVariables.newGroupInRecord("pnd_Appl_Data_Out__R_Field_3", "REDEFINE", pnd_Appl_Data_Out);

        pnd_Appl_Data_Out_Pnd_Header = pnd_Appl_Data_Out__R_Field_3.newGroupInGroup("pnd_Appl_Data_Out_Pnd_Header", "#HEADER");
        pnd_Appl_Data_Out_Pnd_Msg_Guid = pnd_Appl_Data_Out_Pnd_Header.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Msg_Guid", "#MSG-GUID", FieldType.STRING, 
            60);
        pnd_Appl_Data_Out_Pnd_Msg_Ping_Ind = pnd_Appl_Data_Out_Pnd_Header.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Msg_Ping_Ind", "#MSG-PING-IND", FieldType.STRING, 
            1);
        pnd_Appl_Data_Out_Pnd_Sender_Appl_Id = pnd_Appl_Data_Out_Pnd_Header.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Sender_Appl_Id", "#SENDER-APPL-ID", 
            FieldType.STRING, 8);
        pnd_Appl_Data_Out_Pnd_Tgt_Appl_Id = pnd_Appl_Data_Out_Pnd_Header.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Tgt_Appl_Id", "#TGT-APPL-ID", FieldType.STRING, 
            8);
        pnd_Appl_Data_Out_Pnd_Tgt_Module_Code = pnd_Appl_Data_Out_Pnd_Header.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Tgt_Module_Code", "#TGT-MODULE-CODE", 
            FieldType.STRING, 1);
        pnd_Appl_Data_Out_Pnd_Tgt_Module_Name = pnd_Appl_Data_Out_Pnd_Header.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Tgt_Module_Name", "#TGT-MODULE-NAME", 
            FieldType.STRING, 8);
        pnd_Appl_Data_Out_Pnd_Pda_Ctr = pnd_Appl_Data_Out_Pnd_Header.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Pda_Ctr", "#PDA-CTR", FieldType.NUMERIC, 2);
        pnd_Appl_Data_Out_Pnd_Pda_Length = pnd_Appl_Data_Out_Pnd_Header.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Pda_Length", "#PDA-LENGTH", FieldType.NUMERIC, 
            5);
        pnd_Appl_Data_Out_Pnd_Msg_Date_Sent = pnd_Appl_Data_Out_Pnd_Header.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Msg_Date_Sent", "#MSG-DATE-SENT", FieldType.STRING, 
            10);
        pnd_Appl_Data_Out_Pnd_Msg_Time_Sent = pnd_Appl_Data_Out_Pnd_Header.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Msg_Time_Sent", "#MSG-TIME-SENT", FieldType.STRING, 
            8);
        pnd_Appl_Data_Out_Pnd_Header_Filler = pnd_Appl_Data_Out_Pnd_Header.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Header_Filler", "#HEADER-FILLER", FieldType.STRING, 
            89);

        pnd_Appl_Data_Out_Pnd_Response = pnd_Appl_Data_Out__R_Field_3.newGroupInGroup("pnd_Appl_Data_Out_Pnd_Response", "#RESPONSE");
        pnd_Appl_Data_Out_Pnd_Response_Code = pnd_Appl_Data_Out_Pnd_Response.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Response_Code", "#RESPONSE-CODE", 
            FieldType.STRING, 4);
        pnd_Appl_Data_Out_Pnd_Response_Text = pnd_Appl_Data_Out_Pnd_Response.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Response_Text", "#RESPONSE-TEXT", 
            FieldType.STRING, 100);
        pnd_Appl_Data_Out_Pnd_Response_Date = pnd_Appl_Data_Out_Pnd_Response.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Response_Date", "#RESPONSE-DATE", 
            FieldType.STRING, 10);
        pnd_Appl_Data_Out_Pnd_Response_Time = pnd_Appl_Data_Out_Pnd_Response.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Response_Time", "#RESPONSE-TIME", 
            FieldType.STRING, 8);
        pnd_Appl_Data_Out_Pnd_Response_Filler = pnd_Appl_Data_Out_Pnd_Response.newFieldInGroup("pnd_Appl_Data_Out_Pnd_Response_Filler", "#RESPONSE-FILLER", 
            FieldType.STRING, 78);
        pnd_Appl_Data_Out_Pnd_Response_Mdm = pnd_Appl_Data_Out__R_Field_3.newFieldArrayInGroup("pnd_Appl_Data_Out_Pnd_Response_Mdm", "#RESPONSE-MDM", 
            FieldType.STRING, 1, new DbsArrayController(1, 32360));
        pnd_Unique_Id = localVariables.newFieldInRecord("pnd_Unique_Id", "#UNIQUE-ID", FieldType.STRING, 45);
        pnd_Prefix = localVariables.newFieldInRecord("pnd_Prefix", "#PREFIX", FieldType.STRING, 15);
        pnd_Xml_Length = localVariables.newFieldInRecord("pnd_Xml_Length", "#XML-LENGTH", FieldType.NUMERIC, 5);
        pnd_Format_Date_Sent = localVariables.newFieldInRecord("pnd_Format_Date_Sent", "#FORMAT-DATE-SENT", FieldType.STRING, 8);
        pnd_Format_Time_Sent = localVariables.newFieldInRecord("pnd_Format_Time_Sent", "#FORMAT-TIME-SENT", FieldType.STRING, 6);
        pnd_Short_Text = localVariables.newFieldInRecord("pnd_Short_Text", "#SHORT-TEXT", FieldType.STRING, 36);

        pnd_Msgbuffer = localVariables.newGroupInRecord("pnd_Msgbuffer", "#MSGBUFFER");

        pnd_Msgbuffer_Pnd_Msgstring0 = pnd_Msgbuffer.newGroupInGroup("pnd_Msgbuffer_Pnd_Msgstring0", "#MSGSTRING0");
        pnd_Msgbuffer_Pnd_Msgstring1 = pnd_Msgbuffer_Pnd_Msgstring0.newFieldInGroup("pnd_Msgbuffer_Pnd_Msgstring1", "#MSGSTRING1", FieldType.STRING, 250);
        pnd_Msgbuffer_Pnd_Msgstring = pnd_Msgbuffer.newFieldArrayInGroup("pnd_Msgbuffer_Pnd_Msgstring", "#MSGSTRING", FieldType.STRING, 1, new DbsArrayController(1, 
            32510));
        pnd_Error_Message = localVariables.newFieldInRecord("pnd_Error_Message", "#ERROR-MESSAGE", FieldType.STRING, 72);

        pnd_Error_Message__R_Field_4 = localVariables.newGroupInRecord("pnd_Error_Message__R_Field_4", "REDEFINE", pnd_Error_Message);
        pnd_Error_Message_Pnd_Mq_Message_Id = pnd_Error_Message__R_Field_4.newFieldInGroup("pnd_Error_Message_Pnd_Mq_Message_Id", "#MQ-MESSAGE-ID", FieldType.STRING, 
            5);
        pnd_Error_Message_Pnd_Reason_Code_Error = pnd_Error_Message__R_Field_4.newFieldInGroup("pnd_Error_Message_Pnd_Reason_Code_Error", "#REASON-CODE-ERROR", 
            FieldType.STRING, 4);
        pnd_Error_Message_Pnd_Reason_Code_Level = pnd_Error_Message__R_Field_4.newFieldInGroup("pnd_Error_Message_Pnd_Reason_Code_Level", "#REASON-CODE-LEVEL", 
            FieldType.STRING, 1);
        pnd_Error_Message_Pnd_Short_Text_Error = pnd_Error_Message__R_Field_4.newFieldInGroup("pnd_Error_Message_Pnd_Short_Text_Error", "#SHORT-TEXT-ERROR", 
            FieldType.STRING, 36);
        pnd_Error_Message_Pnd_Mq_Function_Error = pnd_Error_Message__R_Field_4.newFieldInGroup("pnd_Error_Message_Pnd_Mq_Function_Error", "#MQ-FUNCTION-ERROR", 
            FieldType.STRING, 8);
        pnd_Error_Message_Pnd_Completion_Code_Error = pnd_Error_Message__R_Field_4.newFieldInGroup("pnd_Error_Message_Pnd_Completion_Code_Error", "#COMPLETION-CODE-ERROR", 
            FieldType.STRING, 2);

        pnd_Error_Handler_Area = localVariables.newGroupInRecord("pnd_Error_Handler_Area", "#ERROR-HANDLER-AREA");
        pnd_Error_Handler_Area_Pnd_Error_Number = pnd_Error_Handler_Area.newFieldInGroup("pnd_Error_Handler_Area_Pnd_Error_Number", "#ERROR-NUMBER", FieldType.NUMERIC, 
            4);
        pnd_Error_Handler_Area_Pnd_Error_Line = pnd_Error_Handler_Area.newFieldInGroup("pnd_Error_Handler_Area_Pnd_Error_Line", "#ERROR-LINE", FieldType.STRING, 
            6);
        pnd_Error_Handler_Area_Pnd_Error_Status = pnd_Error_Handler_Area.newFieldInGroup("pnd_Error_Handler_Area_Pnd_Error_Status", "#ERROR-STATUS", FieldType.STRING, 
            1);
        pnd_Error_Handler_Area_Pnd_Program_Name = pnd_Error_Handler_Area.newFieldInGroup("pnd_Error_Handler_Area_Pnd_Program_Name", "#PROGRAM-NAME", FieldType.STRING, 
            8);
        pnd_Error_Handler_Area_Pnd_Program_Level = pnd_Error_Handler_Area.newFieldInGroup("pnd_Error_Handler_Area_Pnd_Program_Level", "#PROGRAM-LEVEL", 
            FieldType.NUMERIC, 2);
        pnd_Error_Handler_Area_Pnd_Mq_Function = pnd_Error_Handler_Area.newFieldInGroup("pnd_Error_Handler_Area_Pnd_Mq_Function", "#MQ-FUNCTION", FieldType.STRING, 
            8);
        pnd_Error_Handler_Area_Pnd_Completion_Code = pnd_Error_Handler_Area.newFieldInGroup("pnd_Error_Handler_Area_Pnd_Completion_Code", "#COMPLETION-CODE", 
            FieldType.STRING, 2);
        pnd_Error_Handler_Area_Pnd_Error_Text = pnd_Error_Handler_Area.newFieldInGroup("pnd_Error_Handler_Area_Pnd_Error_Text", "#ERROR-TEXT", FieldType.STRING, 
            60);
        pnd_Reason_Code = localVariables.newFieldInRecord("pnd_Reason_Code", "#REASON-CODE", FieldType.INTEGER, 4);
        pnd_Alternate_Userid = localVariables.newFieldInRecord("pnd_Alternate_Userid", "#ALTERNATE-USERID", FieldType.STRING, 12);
        pnd_Dbl_Quote = localVariables.newFieldInRecord("pnd_Dbl_Quote", "#DBL-QUOTE", FieldType.STRING, 1);
        pnd_Exclaim = localVariables.newFieldInRecord("pnd_Exclaim", "#EXCLAIM", FieldType.STRING, 1);
        pnd_Lbracket = localVariables.newFieldInRecord("pnd_Lbracket", "#LBRACKET", FieldType.STRING, 1);
        pnd_Rbracket = localVariables.newFieldInRecord("pnd_Rbracket", "#RBRACKET", FieldType.STRING, 1);
        pnd_Carriage_Rtn = localVariables.newFieldInRecord("pnd_Carriage_Rtn", "#CARRIAGE-RTN", FieldType.STRING, 1);
        pnd_Xml_Rec_Length = localVariables.newFieldInRecord("pnd_Xml_Rec_Length", "#XML-REC-LENGTH", FieldType.PACKED_DECIMAL, 3);
        pnd_Msg_Length_Out1 = localVariables.newFieldInRecord("pnd_Msg_Length_Out1", "#MSG-LENGTH-OUT1", FieldType.INTEGER, 4);
        pnd_Msg_Length_Out2 = localVariables.newFieldInRecord("pnd_Msg_Length_Out2", "#MSG-LENGTH-OUT2", FieldType.INTEGER, 4);
        pnd_Guid_Post = localVariables.newFieldInRecord("pnd_Guid_Post", "#GUID-POST", FieldType.STRING, 4);

        pnd_Xml_File_Dflts = localVariables.newGroupInRecord("pnd_Xml_File_Dflts", "#XML-FILE-DFLTS");
        pnd_Xml_File_Dflts_Pnd_Ln_Strt = pnd_Xml_File_Dflts.newFieldInGroup("pnd_Xml_File_Dflts_Pnd_Ln_Strt", "#LN-STRT", FieldType.STRING, 1);
        pnd_Xml_File_Dflts_Pnd_Ln_End = pnd_Xml_File_Dflts.newFieldInGroup("pnd_Xml_File_Dflts_Pnd_Ln_End", "#LN-END", FieldType.STRING, 1);
        pnd_Xml_Record = localVariables.newFieldInRecord("pnd_Xml_Record", "#XML-RECORD", FieldType.STRING, 80);

        pnd_Xml_Record__R_Field_5 = localVariables.newGroupInRecord("pnd_Xml_Record__R_Field_5", "REDEFINE", pnd_Xml_Record);
        pnd_Xml_Record_Pnd_Rcrd_Prefix = pnd_Xml_Record__R_Field_5.newFieldInGroup("pnd_Xml_Record_Pnd_Rcrd_Prefix", "#RCRD-PREFIX", FieldType.STRING, 
            1);
        pnd_Xml_Record_Pnd_Rcrd_Bdy = pnd_Xml_Record__R_Field_5.newFieldInGroup("pnd_Xml_Record_Pnd_Rcrd_Bdy", "#RCRD-BDY", FieldType.STRING, 78);
        pnd_Xml_Record_Pnd_Suffix = pnd_Xml_Record__R_Field_5.newFieldInGroup("pnd_Xml_Record_Pnd_Suffix", "#SUFFIX", FieldType.STRING, 1);

        pnd_Xml_Record__R_Field_6 = localVariables.newGroupInRecord("pnd_Xml_Record__R_Field_6", "REDEFINE", pnd_Xml_Record);
        pnd_Xml_Record_Pnd_Xml_Record_Array = pnd_Xml_Record__R_Field_6.newFieldArrayInGroup("pnd_Xml_Record_Pnd_Xml_Record_Array", "#XML-RECORD-ARRAY", 
            FieldType.STRING, 1, new DbsArrayController(1, 80));
        pnd_Qmgrname = localVariables.newFieldInRecord("pnd_Qmgrname", "#QMGRNAME", FieldType.STRING, 48);
        pnd_Inputqname = localVariables.newFieldInRecord("pnd_Inputqname", "#INPUTQNAME", FieldType.STRING, 48);
        pnd_Bufferlength = localVariables.newFieldInRecord("pnd_Bufferlength", "#BUFFERLENGTH", FieldType.INTEGER, 4);
        pnd_Charattrlength = localVariables.newFieldInRecord("pnd_Charattrlength", "#CHARATTRLENGTH", FieldType.INTEGER, 4);
        pnd_Charattrs = localVariables.newFieldInRecord("pnd_Charattrs", "#CHARATTRS", FieldType.STRING, 1);
        pnd_Closeoptions = localVariables.newFieldInRecord("pnd_Closeoptions", "#CLOSEOPTIONS", FieldType.INTEGER, 4);
        pnd_Compcode = localVariables.newFieldInRecord("pnd_Compcode", "#COMPCODE", FieldType.INTEGER, 4);
        pnd_Datalength = localVariables.newFieldInRecord("pnd_Datalength", "#DATALENGTH", FieldType.INTEGER, 4);
        pnd_Datalength_Out = localVariables.newFieldInRecord("pnd_Datalength_Out", "#DATALENGTH-OUT", FieldType.INTEGER, 4);
        pnd_Getmsgopt = localVariables.newFieldInRecord("pnd_Getmsgopt", "#GETMSGOPT", FieldType.STRING, 128);
        pnd_Hconn = localVariables.newFieldInRecord("pnd_Hconn", "#HCONN", FieldType.INTEGER, 4);
        pnd_Hobj1 = localVariables.newFieldInRecord("pnd_Hobj1", "#HOBJ1", FieldType.INTEGER, 4);
        pnd_Hobj2 = localVariables.newFieldInRecord("pnd_Hobj2", "#HOBJ2", FieldType.INTEGER, 4);
        pnd_Intattrcount = localVariables.newFieldInRecord("pnd_Intattrcount", "#INTATTRCOUNT", FieldType.INTEGER, 4);
        pnd_Intattrs = localVariables.newFieldInRecord("pnd_Intattrs", "#INTATTRS", FieldType.INTEGER, 4);
        pnd_Messagelength = localVariables.newFieldInRecord("pnd_Messagelength", "#MESSAGELENGTH", FieldType.INTEGER, 4);
        pnd_Msgdesc = localVariables.newFieldInRecord("pnd_Msgdesc", "#MSGDESC", FieldType.STRING, 72);
        pnd_Objdesc = localVariables.newFieldInRecord("pnd_Objdesc", "#OBJDESC", FieldType.STRING, 168);
        pnd_Openoptions = localVariables.newFieldInRecord("pnd_Openoptions", "#OPENOPTIONS", FieldType.INTEGER, 4);
        pnd_Putmsgopt = localVariables.newFieldInRecord("pnd_Putmsgopt", "#PUTMSGOPT", FieldType.STRING, 128);
        pnd_Reason = localVariables.newFieldInRecord("pnd_Reason", "#REASON", FieldType.INTEGER, 4);
        pnd_Selectorcount = localVariables.newFieldInRecord("pnd_Selectorcount", "#SELECTORCOUNT", FieldType.INTEGER, 4);
        pnd_Selectors = localVariables.newFieldInRecord("pnd_Selectors", "#SELECTORS", FieldType.INTEGER, 4);
        pnd_Expiry_Interval = localVariables.newFieldInRecord("pnd_Expiry_Interval", "#EXPIRY-INTERVAL", FieldType.INTEGER, 4);
        pnd_Save_Correlid = localVariables.newFieldInRecord("pnd_Save_Correlid", "#SAVE-CORRELID", FieldType.STRING, 24);
        pnd_Wait_Interval = localVariables.newFieldInRecord("pnd_Wait_Interval", "#WAIT-INTERVAL", FieldType.INTEGER, 4);
        mqci_None = localVariables.newFieldInRecord("mqci_None", "MQCI-NONE", FieldType.STRING, 24);
        mqco_None = localVariables.newFieldInRecord("mqco_None", "MQCO-NONE", FieldType.INTEGER, 4);
        mqfmt_Md_Extension = localVariables.newFieldInRecord("mqfmt_Md_Extension", "MQFMT-MD-EXTENSION", FieldType.STRING, 8);
        mqfmt_String = localVariables.newFieldInRecord("mqfmt_String", "MQFMT-STRING", FieldType.STRING, 8);
        mqmi_None = localVariables.newFieldInRecord("mqmi_None", "MQMI-NONE", FieldType.STRING, 24);
        mqoo_Browse = localVariables.newFieldInRecord("mqoo_Browse", "MQOO-BROWSE", FieldType.INTEGER, 4);
        mqoo_Input_Shared = localVariables.newFieldInRecord("mqoo_Input_Shared", "MQOO-INPUT-SHARED", FieldType.INTEGER, 4);
        mqoo_Inquire = localVariables.newFieldInRecord("mqoo_Inquire", "MQOO_INQUIRE", FieldType.INTEGER, 4);
        mqoo_Output = localVariables.newFieldInRecord("mqoo_Output", "MQOO-OUTPUT", FieldType.INTEGER, 4);
        mqoo_Alternate_User_Authority = localVariables.newFieldInRecord("mqoo_Alternate_User_Authority", "MQOO-ALTERNATE-USER-AUTHORITY", FieldType.INTEGER, 
            4);
        mqgmo_Accept_Truncated_Msg = localVariables.newFieldInRecord("mqgmo_Accept_Truncated_Msg", "MQGMO-ACCEPT-TRUNCATED-MSG", FieldType.INTEGER, 4);
        mqgmo_No_Syncpoint = localVariables.newFieldInRecord("mqgmo_No_Syncpoint", "MQGMO-NO-SYNCPOINT", FieldType.INTEGER, 4);
        mqgmo_Syncpoint = localVariables.newFieldInRecord("mqgmo_Syncpoint", "MQGMO-SYNCPOINT", FieldType.INTEGER, 4);
        mqpmo_No_Syncpoint = localVariables.newFieldInRecord("mqpmo_No_Syncpoint", "MQPMO-NO-SYNCPOINT", FieldType.INTEGER, 4);
        mqgmo_Wait = localVariables.newFieldInRecord("mqgmo_Wait", "MQGMO-WAIT", FieldType.INTEGER, 4);
        mqia_Current_Q_Depth = localVariables.newFieldInRecord("mqia_Current_Q_Depth", "MQIA-CURRENT-Q-DEPTH", FieldType.INTEGER, 4);
        mqpmo_Alternate_User_Authority = localVariables.newFieldInRecord("mqpmo_Alternate_User_Authority", "MQPMO-ALTERNATE-USER-AUTHORITY", FieldType.INTEGER, 
            4);
        mqper_Persistent = localVariables.newFieldInRecord("mqper_Persistent", "MQPER-PERSISTENT", FieldType.INTEGER, 4);
        mqper_Not_Persistent = localVariables.newFieldInRecord("mqper_Not_Persistent", "MQPER-NOT-PERSISTENT", FieldType.INTEGER, 4);
        mqwi_Unlimited = localVariables.newFieldInRecord("mqwi_Unlimited", "MQWI-UNLIMITED", FieldType.INTEGER, 4);
        mqgmo_Fail_If_Quiescing = localVariables.newFieldInRecord("mqgmo_Fail_If_Quiescing", "MQGMO-FAIL-IF-QUIESCING", FieldType.INTEGER, 4);
        mqgmo_Convert = localVariables.newFieldInRecord("mqgmo_Convert", "MQGMO-CONVERT", FieldType.INTEGER, 4);

        mqh_Object_Mqrfh2 = localVariables.newGroupInRecord("mqh_Object_Mqrfh2", "MQH-OBJECT-MQRFH2");
        mqh_Object_Mqrfh2_Mqrfh2_Strucid = mqh_Object_Mqrfh2.newFieldInGroup("mqh_Object_Mqrfh2_Mqrfh2_Strucid", "MQRFH2-STRUCID", FieldType.STRING, 4);
        mqh_Object_Mqrfh2_Mqrfh2_Version = mqh_Object_Mqrfh2.newFieldInGroup("mqh_Object_Mqrfh2_Mqrfh2_Version", "MQRFH2-VERSION", FieldType.INTEGER, 
            4);
        mqh_Object_Mqrfh2_Mqrfh2_Struclength = mqh_Object_Mqrfh2.newFieldInGroup("mqh_Object_Mqrfh2_Mqrfh2_Struclength", "MQRFH2-STRUCLENGTH", FieldType.INTEGER, 
            4);
        mqh_Object_Mqrfh2_Mqrfh2_Encoding = mqh_Object_Mqrfh2.newFieldInGroup("mqh_Object_Mqrfh2_Mqrfh2_Encoding", "MQRFH2-ENCODING", FieldType.INTEGER, 
            4);
        mqh_Object_Mqrfh2_Mqrfh2_Codedcharsetid = mqh_Object_Mqrfh2.newFieldInGroup("mqh_Object_Mqrfh2_Mqrfh2_Codedcharsetid", "MQRFH2-CODEDCHARSETID", 
            FieldType.INTEGER, 4);
        mqh_Object_Mqrfh2_Mqrfh2_Format = mqh_Object_Mqrfh2.newFieldInGroup("mqh_Object_Mqrfh2_Mqrfh2_Format", "MQRFH2-FORMAT", FieldType.STRING, 8);
        mqh_Object_Mqrfh2_Mqrfh2_Flags = mqh_Object_Mqrfh2.newFieldInGroup("mqh_Object_Mqrfh2_Mqrfh2_Flags", "MQRFH2-FLAGS", FieldType.INTEGER, 4);
        mqh_Object_Mqrfh2_Mqrfh2_Namevalueccsid = mqh_Object_Mqrfh2.newFieldInGroup("mqh_Object_Mqrfh2_Mqrfh2_Namevalueccsid", "MQRFH2-NAMEVALUECCSID", 
            FieldType.INTEGER, 4);
        mqh_Object_Mqrfh2_Mqrfh2_Namevaluelength = mqh_Object_Mqrfh2.newFieldInGroup("mqh_Object_Mqrfh2_Mqrfh2_Namevaluelength", "MQRFH2-NAMEVALUELENGTH", 
            FieldType.INTEGER, 4);
        mqh_Object_Mqrfh2_Mqrfh2_Namevaluedata = mqh_Object_Mqrfh2.newFieldInGroup("mqh_Object_Mqrfh2_Mqrfh2_Namevaluedata", "MQRFH2-NAMEVALUEDATA", FieldType.STRING, 
            96);

        mqh_Object_Mqrfh2__R_Field_7 = localVariables.newGroupInRecord("mqh_Object_Mqrfh2__R_Field_7", "REDEFINE", mqh_Object_Mqrfh2);
        mqh_Object_Mqrfh2_Mqrfh2 = mqh_Object_Mqrfh2__R_Field_7.newFieldArrayInGroup("mqh_Object_Mqrfh2_Mqrfh2", "MQRFH2", FieldType.STRING, 1, new DbsArrayController(1, 
            136));

        mqh_Object_Mqrfh2__R_Field_8 = localVariables.newGroupInRecord("mqh_Object_Mqrfh2__R_Field_8", "REDEFINE", mqh_Object_Mqrfh2);
        mqh_Object_Mqrfh2_Mqrfh2_String = mqh_Object_Mqrfh2__R_Field_8.newFieldInGroup("mqh_Object_Mqrfh2_Mqrfh2_String", "MQRFH2-STRING", FieldType.STRING, 
            136);

        mqm_Object_Descriptor = localVariables.newGroupInRecord("mqm_Object_Descriptor", "MQM-OBJECT-DESCRIPTOR");
        mqm_Object_Descriptor_Mqod_Strucid = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Strucid", "MQOD-STRUCID", FieldType.STRING, 
            4);
        mqm_Object_Descriptor_Mqod_Version = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Version", "MQOD-VERSION", FieldType.INTEGER, 
            4);
        mqm_Object_Descriptor_Mqod_Objecttype = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Objecttype", "MQOD-OBJECTTYPE", FieldType.INTEGER, 
            4);
        mqm_Object_Descriptor_Mqod_Objectname = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Objectname", "MQOD-OBJECTNAME", FieldType.STRING, 
            48);
        mqm_Object_Descriptor_Mqod_Objectqmgrname = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Objectqmgrname", "MQOD-OBJECTQMGRNAME", 
            FieldType.STRING, 48);
        mqm_Object_Descriptor_Mqod_Dynamicqname = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Dynamicqname", "MQOD-DYNAMICQNAME", 
            FieldType.STRING, 48);
        mqm_Object_Descriptor_Mqod_Alternateuserid = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Alternateuserid", "MQOD-ALTERNATEUSERID", 
            FieldType.STRING, 12);
        mqm_Object_Descriptor_Mqod_Recspresent = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Recspresent", "MQOD-RECSPRESENT", FieldType.INTEGER, 
            4);
        mqm_Object_Descriptor_Mqod_Knowndestcount = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Knowndestcount", "MQOD-KNOWNDESTCOUNT", 
            FieldType.INTEGER, 4);
        mqm_Object_Descriptor_Mqod_Unknowndestcount = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Unknowndestcount", "MQOD-UNKNOWNDESTCOUNT", 
            FieldType.INTEGER, 4);
        mqm_Object_Descriptor_Mqod_Invaliddestcount = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Invaliddestcount", "MQOD-INVALIDDESTCOUNT", 
            FieldType.INTEGER, 4);
        mqm_Object_Descriptor_Mqod_Objectrecoffset = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Objectrecoffset", "MQOD-OBJECTRECOFFSET", 
            FieldType.INTEGER, 4);
        mqm_Object_Descriptor_Mqod_Responserecoffset = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Responserecoffset", "MQOD-RESPONSERECOFFSET", 
            FieldType.INTEGER, 4);
        mqm_Object_Descriptor_Mqod_Objectrecptr = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Objectrecptr", "MQOD-OBJECTRECPTR", 
            FieldType.STRING, 4);
        mqm_Object_Descriptor_Mqod_Responserecptr = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Responserecptr", "MQOD-RESPONSERECPTR", 
            FieldType.STRING, 4);
        mqm_Object_Descriptor_Mqod_Alternatesecurityid = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Alternatesecurityid", "MQOD-ALTERNATESECURITYID", 
            FieldType.STRING, 40);
        mqm_Object_Descriptor_Mqod_Resolvedqname = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Resolvedqname", "MQOD-RESOLVEDQNAME", 
            FieldType.STRING, 48);
        mqm_Object_Descriptor_Mqod_Resolvedqmgrname = mqm_Object_Descriptor.newFieldInGroup("mqm_Object_Descriptor_Mqod_Resolvedqmgrname", "MQOD-RESOLVEDQMGRNAME", 
            FieldType.STRING, 48);

        mqm_Object_Descriptor__R_Field_9 = localVariables.newGroupInRecord("mqm_Object_Descriptor__R_Field_9", "REDEFINE", mqm_Object_Descriptor);
        mqm_Object_Descriptor_Mqod = mqm_Object_Descriptor__R_Field_9.newFieldArrayInGroup("mqm_Object_Descriptor_Mqod", "MQOD", FieldType.STRING, 1, 
            new DbsArrayController(1, 336));

        mqm_Message_Descriptor = localVariables.newGroupInRecord("mqm_Message_Descriptor", "MQM-MESSAGE-DESCRIPTOR");
        mqm_Message_Descriptor_Mqmd_Strucid = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Strucid", "MQMD-STRUCID", FieldType.STRING, 
            4);
        mqm_Message_Descriptor_Mqmd_Version = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Version", "MQMD-VERSION", FieldType.INTEGER, 
            4);
        mqm_Message_Descriptor_Mqmd_Report = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Report", "MQMD-REPORT", FieldType.INTEGER, 
            4);
        mqm_Message_Descriptor_Mqmd_Msgtype = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Msgtype", "MQMD-MSGTYPE", FieldType.INTEGER, 
            4);
        mqm_Message_Descriptor_Mqmd_Expiry = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Expiry", "MQMD-EXPIRY", FieldType.INTEGER, 
            4);
        mqm_Message_Descriptor_Mqmd_Feedback = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Feedback", "MQMD-FEEDBACK", FieldType.INTEGER, 
            4);
        mqm_Message_Descriptor_Mqmd_Encoding = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Encoding", "MQMD-ENCODING", FieldType.INTEGER, 
            4);
        mqm_Message_Descriptor_Mqmd_Codedcharsetid = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Codedcharsetid", "MQMD-CODEDCHARSETID", 
            FieldType.INTEGER, 4);
        mqm_Message_Descriptor_Mqmd_Format = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Format", "MQMD-FORMAT", FieldType.STRING, 
            8);
        mqm_Message_Descriptor_Mqmd_Priority = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Priority", "MQMD-PRIORITY", FieldType.INTEGER, 
            4);
        mqm_Message_Descriptor_Mqmd_Persistence = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Persistence", "MQMD-PERSISTENCE", 
            FieldType.INTEGER, 4);
        mqm_Message_Descriptor_Mqmd_Msgid = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Msgid", "MQMD-MSGID", FieldType.STRING, 
            24);
        mqm_Message_Descriptor_Mqmd_Correlid = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Correlid", "MQMD-CORRELID", FieldType.STRING, 
            24);
        mqm_Message_Descriptor_Mqmd_Backoutcount = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Backoutcount", "MQMD-BACKOUTCOUNT", 
            FieldType.INTEGER, 4);
        mqm_Message_Descriptor_Mqmd_Replytoq = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Replytoq", "MQMD-REPLYTOQ", FieldType.STRING, 
            48);
        mqm_Message_Descriptor_Mqmd_Replytoqmgr = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Replytoqmgr", "MQMD-REPLYTOQMGR", 
            FieldType.STRING, 48);
        mqm_Message_Descriptor_Mqmd_Useridentifier = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Useridentifier", "MQMD-USERIDENTIFIER", 
            FieldType.STRING, 12);
        mqm_Message_Descriptor_Mqmd_Accountingtoken = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Accountingtoken", "MQMD-ACCOUNTINGTOKEN", 
            FieldType.STRING, 32);
        mqm_Message_Descriptor_Mqmd_Applidentitydata = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Applidentitydata", "MQMD-APPLIDENTITYDATA", 
            FieldType.STRING, 32);
        mqm_Message_Descriptor_Mqmd_Putappltype = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Putappltype", "MQMD-PUTAPPLTYPE", 
            FieldType.INTEGER, 4);
        mqm_Message_Descriptor_Mqmd_Putapplname = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Putapplname", "MQMD-PUTAPPLNAME", 
            FieldType.STRING, 28);
        mqm_Message_Descriptor_Mqmd_Putdate = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Putdate", "MQMD-PUTDATE", FieldType.STRING, 
            8);
        mqm_Message_Descriptor_Mqmd_Puttime = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Puttime", "MQMD-PUTTIME", FieldType.STRING, 
            48);
        mqm_Message_Descriptor_Mqmd_Applorigindata = mqm_Message_Descriptor.newFieldInGroup("mqm_Message_Descriptor_Mqmd_Applorigindata", "MQMD-APPLORIGINDATA", 
            FieldType.STRING, 4);

        mqm_Message_Descriptor__R_Field_10 = localVariables.newGroupInRecord("mqm_Message_Descriptor__R_Field_10", "REDEFINE", mqm_Message_Descriptor);
        mqm_Message_Descriptor_Mqmd = mqm_Message_Descriptor__R_Field_10.newFieldArrayInGroup("mqm_Message_Descriptor_Mqmd", "MQMD", FieldType.STRING, 
            1, new DbsArrayController(1, 324));

        mqm_Get_Message_Options = localVariables.newGroupInRecord("mqm_Get_Message_Options", "MQM-GET-MESSAGE-OPTIONS");
        mqm_Get_Message_Options_Mqgmo_Strucid = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Strucid", "MQGMO-STRUCID", FieldType.STRING, 
            4);
        mqm_Get_Message_Options_Mqgmo_Version = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Version", "MQGMO-VERSION", FieldType.INTEGER, 
            4);
        mqm_Get_Message_Options_Mqgmo_Options = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Options", "MQGMO-OPTIONS", FieldType.INTEGER, 
            4);
        mqm_Get_Message_Options_Mqgmo_Waitinterval = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Waitinterval", "MQGMO-WAITINTERVAL", 
            FieldType.INTEGER, 4);
        mqm_Get_Message_Options_Mqgmo_Signal1 = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Signal1", "MQGMO-SIGNAL1", FieldType.STRING, 
            4);
        mqm_Get_Message_Options_Mqgmo_Signal2 = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Signal2", "MQGMO-SIGNAL2", FieldType.INTEGER, 
            4);
        mqm_Get_Message_Options_Mqgmo_Resolvedqname = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Resolvedqname", "MQGMO-RESOLVEDQNAME", 
            FieldType.STRING, 48);
        mqm_Get_Message_Options_Mqgmo_Matchoptions = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Matchoptions", "MQGMO-MATCHOPTIONS", 
            FieldType.INTEGER, 4);
        mqm_Get_Message_Options_Mqgmo_Groupstatus = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Groupstatus", "MQGMO-GROUPSTATUS", 
            FieldType.STRING, 1);
        mqm_Get_Message_Options_Mqgmo_Segmentstatus = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Segmentstatus", "MQGMO-SEGMENTSTATUS", 
            FieldType.STRING, 1);
        mqm_Get_Message_Options_Mqgmo_Segmentation = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Segmentation", "MQGMO-SEGMENTATION", 
            FieldType.STRING, 1);
        mqm_Get_Message_Options_Mqgmo_Reserved1 = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Reserved1", "MQGMO-RESERVED1", 
            FieldType.STRING, 1);
        mqm_Get_Message_Options_Mqgmo_Msgtoken = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Msgtoken", "MQGMO-MSGTOKEN", FieldType.STRING, 
            16);
        mqm_Get_Message_Options_Mqgmo_Returnedlength = mqm_Get_Message_Options.newFieldInGroup("mqm_Get_Message_Options_Mqgmo_Returnedlength", "MQGMO-RETURNEDLENGTH", 
            FieldType.INTEGER, 4);

        mqm_Get_Message_Options__R_Field_11 = localVariables.newGroupInRecord("mqm_Get_Message_Options__R_Field_11", "REDEFINE", mqm_Get_Message_Options);
        mqm_Get_Message_Options_Mqgmo = mqm_Get_Message_Options__R_Field_11.newFieldInGroup("mqm_Get_Message_Options_Mqgmo", "MQGMO", FieldType.STRING, 
            100);

        mqm_Put_Message_Options = localVariables.newGroupInRecord("mqm_Put_Message_Options", "MQM-PUT-MESSAGE-OPTIONS");
        mqm_Put_Message_Options_Mqpmo_Strucid = mqm_Put_Message_Options.newFieldInGroup("mqm_Put_Message_Options_Mqpmo_Strucid", "MQPMO-STRUCID", FieldType.STRING, 
            4);
        mqm_Put_Message_Options_Mqpmo_Version = mqm_Put_Message_Options.newFieldInGroup("mqm_Put_Message_Options_Mqpmo_Version", "MQPMO-VERSION", FieldType.INTEGER, 
            4);
        mqm_Put_Message_Options_Mqpmo_Options = mqm_Put_Message_Options.newFieldInGroup("mqm_Put_Message_Options_Mqpmo_Options", "MQPMO-OPTIONS", FieldType.INTEGER, 
            4);
        mqm_Put_Message_Options_Mqpmo_Timeout = mqm_Put_Message_Options.newFieldInGroup("mqm_Put_Message_Options_Mqpmo_Timeout", "MQPMO-TIMEOUT", FieldType.INTEGER, 
            4);
        mqm_Put_Message_Options_Mqpmo_Context = mqm_Put_Message_Options.newFieldInGroup("mqm_Put_Message_Options_Mqpmo_Context", "MQPMO-CONTEXT", FieldType.INTEGER, 
            4);
        mqm_Put_Message_Options_Mqpmo_Knowndestcount = mqm_Put_Message_Options.newFieldInGroup("mqm_Put_Message_Options_Mqpmo_Knowndestcount", "MQPMO-KNOWNDESTCOUNT", 
            FieldType.INTEGER, 4);
        mqm_Put_Message_Options_Mqpmo_Unknowndestcount = mqm_Put_Message_Options.newFieldInGroup("mqm_Put_Message_Options_Mqpmo_Unknowndestcount", "MQPMO-UNKNOWNDESTCOUNT", 
            FieldType.INTEGER, 4);
        mqm_Put_Message_Options_Mqpmo_Invaliddestcount = mqm_Put_Message_Options.newFieldInGroup("mqm_Put_Message_Options_Mqpmo_Invaliddestcount", "MQPMO-INVALIDDESTCOUNT", 
            FieldType.INTEGER, 4);
        mqm_Put_Message_Options_Mqpmo_Resolvedqname = mqm_Put_Message_Options.newFieldInGroup("mqm_Put_Message_Options_Mqpmo_Resolvedqname", "MQPMO-RESOLVEDQNAME", 
            FieldType.STRING, 48);
        mqm_Put_Message_Options_Mqpmo_Resolvedqmgrname = mqm_Put_Message_Options.newFieldInGroup("mqm_Put_Message_Options_Mqpmo_Resolvedqmgrname", "MQPMO-RESOLVEDQMGRNAME", 
            FieldType.STRING, 48);

        mqm_Put_Message_Options__R_Field_12 = localVariables.newGroupInRecord("mqm_Put_Message_Options__R_Field_12", "REDEFINE", mqm_Put_Message_Options);
        mqm_Put_Message_Options_Mqpmo = mqm_Put_Message_Options__R_Field_12.newFieldInGroup("mqm_Put_Message_Options_Mqpmo", "MQPMO", FieldType.STRING, 
            128);

        mq_Error_Data = localVariables.newGroupInRecord("mq_Error_Data", "MQ-ERROR-DATA");
        mq_Error_Data_Mq_Error_Prog_Id = mq_Error_Data.newFieldInGroup("mq_Error_Data_Mq_Error_Prog_Id", "MQ-ERROR-PROG-ID", FieldType.STRING, 8);
        mq_Error_Data_Mq_Error_Filler1 = mq_Error_Data.newFieldInGroup("mq_Error_Data_Mq_Error_Filler1", "MQ-ERROR-FILLER1", FieldType.STRING, 1);
        mq_Error_Data_Mq_Error_Command = mq_Error_Data.newFieldInGroup("mq_Error_Data_Mq_Error_Command", "MQ-ERROR-COMMAND", FieldType.STRING, 8);
        mq_Error_Data_Mq_Error_Filler2 = mq_Error_Data.newFieldInGroup("mq_Error_Data_Mq_Error_Filler2", "MQ-ERROR-FILLER2", FieldType.STRING, 1);
        mq_Error_Data_Mq_Error_Code = mq_Error_Data.newFieldInGroup("mq_Error_Data_Mq_Error_Code", "MQ-ERROR-CODE", FieldType.STRING, 4);
        mq_Error_Data_Mq_Error_Filler3 = mq_Error_Data.newFieldInGroup("mq_Error_Data_Mq_Error_Filler3", "MQ-ERROR-FILLER3", FieldType.STRING, 1);
        mq_Error_Data_Mq_Error_Reason = mq_Error_Data.newFieldInGroup("mq_Error_Data_Mq_Error_Reason", "MQ-ERROR-REASON", FieldType.STRING, 4);
        mq_Error_Data_Mq_Error_Filler4 = mq_Error_Data.newFieldInGroup("mq_Error_Data_Mq_Error_Filler4", "MQ-ERROR-FILLER4", FieldType.STRING, 1);
        mq_Error_Data_Mq_Error_Resource = mq_Error_Data.newFieldInGroup("mq_Error_Data_Mq_Error_Resource", "MQ-ERROR-RESOURCE", FieldType.STRING, 48);

        cmqtml = localVariables.newGroupInRecord("cmqtml", "CMQTML");
        cmqtml_Mqtm_Strucid = cmqtml.newFieldInGroup("cmqtml_Mqtm_Strucid", "MQTM-STRUCID", FieldType.STRING, 4);
        cmqtml_Mqtm_Version = cmqtml.newFieldInGroup("cmqtml_Mqtm_Version", "MQTM-VERSION", FieldType.INTEGER, 4);
        cmqtml_Mqtm_Qname = cmqtml.newFieldInGroup("cmqtml_Mqtm_Qname", "MQTM-QNAME", FieldType.STRING, 48);
        cmqtml_Mqtm_Processname = cmqtml.newFieldInGroup("cmqtml_Mqtm_Processname", "MQTM-PROCESSNAME", FieldType.STRING, 48);
        cmqtml_Mqtm_Triggerdata = cmqtml.newFieldInGroup("cmqtml_Mqtm_Triggerdata", "MQTM-TRIGGERDATA", FieldType.STRING, 64);
        cmqtml_Mqtm_Appltype = cmqtml.newFieldInGroup("cmqtml_Mqtm_Appltype", "MQTM-APPLTYPE", FieldType.INTEGER, 4);
        cmqtml_Mqtm_Applid = cmqtml.newFieldArrayInGroup("cmqtml_Mqtm_Applid", "MQTM-APPLID", FieldType.STRING, 1, new DbsArrayController(1, 256));
        cmqtml_Mqtm_Envdata = cmqtml.newFieldInGroup("cmqtml_Mqtm_Envdata", "MQTM-ENVDATA", FieldType.STRING, 128);
        cmqtml_Mqtm_Userdata = cmqtml.newFieldInGroup("cmqtml_Mqtm_Userdata", "MQTM-USERDATA", FieldType.STRING, 128);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        pls_Qmgrname = WsIndependent.getInstance().newFieldInRecord("pls_Qmgrname", "+QMGRNAME", FieldType.STRING, 48);
        pls_Hconn = WsIndependent.getInstance().newFieldInRecord("pls_Hconn", "+HCONN", FieldType.INTEGER, 4);
        pls_Mqod = WsIndependent.getInstance().newFieldArrayInRecord("pls_Mqod", "+MQOD", FieldType.STRING, 1, new DbsArrayController(1, 336));
        pls_Hobj1 = WsIndependent.getInstance().newFieldInRecord("pls_Hobj1", "+HOBJ1", FieldType.INTEGER, 4);
        pls_Hobj2 = WsIndependent.getInstance().newFieldInRecord("pls_Hobj2", "+HOBJ2", FieldType.INTEGER, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_curr_Environment.reset();
        vw_error_Trigger.reset();

        localVariables.reset();
        pnd_Qmgrname_For_Curr_Env.getValue(1).setInitialValue("P MQN1");
        pnd_Qmgrname_For_Curr_Env.getValue(2).setInitialValue("P1MQN1");
        pnd_Qmgrname_For_Curr_Env.getValue(3).setInitialValue("P2MQN2");
        pnd_Qmgrname_For_Curr_Env.getValue(4).setInitialValue("DRMQR1");
        pnd_Qmgrname_For_Curr_Env.getValue(5).setInitialValue("PFMQPF");
        pnd_Qmgrname_For_Curr_Env.getValue(6).setInitialValue("U MQU1");
        pnd_Qmgrname_For_Curr_Env.getValue(7).setInitialValue("I1MQI1");
        pnd_Qmgrname_For_Curr_Env.getValue(8).setInitialValue("I2MQI2");
        pnd_Qmgrname_For_Curr_Env.getValue(9).setInitialValue("S1MQS1");
        pnd_Qmgrname_For_Curr_Env.getValue(10).setInitialValue("S2MQS2");
        pnd_Qmgrname_For_Curr_Env.getValue(11).setInitialValue("S4MQS4");
        pnd_Qmgrname_For_Curr_Env.getValue(12).setInitialValue("A MQA1");
        pnd_Xml_Length.setInitialValue(229);
        pnd_Msgbuffer_Pnd_Msgstring.getValue("*").setInitialValue("A");
        pnd_Error_Handler_Area_Pnd_Error_Status.setInitialValue("M");
        pnd_Error_Handler_Area_Pnd_Program_Name.setInitialValue("CISP0051");
        pnd_Error_Handler_Area_Pnd_Program_Level.setInitialValue(1);
        pnd_Alternate_Userid.setInitialValue("MQCISURG");
        pnd_Dbl_Quote.setInitialValue("H'7F'");
        pnd_Exclaim.setInitialValue("H'5A'");
        pnd_Lbracket.setInitialValue("H'BA'");
        pnd_Rbracket.setInitialValue("H'BB'");
        pnd_Carriage_Rtn.setInitialValue("H'0D'");
        pnd_Xml_Rec_Length.setInitialValue(120);
        pnd_Msg_Length_Out1.setInitialValue(0);
        pnd_Msg_Length_Out2.setInitialValue(0);
        pnd_Guid_Post.setInitialValue("POST");
        pnd_Xml_File_Dflts_Pnd_Ln_Strt.setInitialValue("<");
        pnd_Xml_File_Dflts_Pnd_Ln_End.setInitialValue(">");
        pnd_Inputqname.setInitialValue("APPL.CIS.CPI.INPUT.ALIAS");
        pnd_Bufferlength.setInitialValue(32760);
        pnd_Closeoptions.setInitialValue(0);
        pnd_Hconn.setInitialValue(0);
        pnd_Hobj1.setInitialValue(0);
        pnd_Hobj2.setInitialValue(0);
        pnd_Messagelength.setInitialValue(32760);
        pnd_Openoptions.setInitialValue(0);
        pnd_Expiry_Interval.setInitialValue(1200);
        pnd_Wait_Interval.setInitialValue(60000);
        mqci_None.setInitialValue("H'000000000000000000000000000000000000000000000000'");
        mqco_None.setInitialValue(0);
        mqfmt_Md_Extension.setInitialValue("MQHMDE  ");
        mqfmt_String.setInitialValue("MQSTR   ");
        mqmi_None.setInitialValue("H'000000000000000000000000000000000000000000000000'");
        mqoo_Browse.setInitialValue(8);
        mqoo_Input_Shared.setInitialValue(2);
        mqoo_Inquire.setInitialValue(32);
        mqoo_Output.setInitialValue(16);
        mqoo_Alternate_User_Authority.setInitialValue(4096);
        mqgmo_Accept_Truncated_Msg.setInitialValue(64);
        mqgmo_No_Syncpoint.setInitialValue(4);
        mqgmo_Syncpoint.setInitialValue(2);
        mqpmo_No_Syncpoint.setInitialValue(4);
        mqgmo_Wait.setInitialValue(1);
        mqia_Current_Q_Depth.setInitialValue(3);
        mqpmo_Alternate_User_Authority.setInitialValue(4096);
        mqper_Persistent.setInitialValue(1);
        mqper_Not_Persistent.setInitialValue(0);
        mqwi_Unlimited.setInitialValue(-1);
        mqgmo_Fail_If_Quiescing.setInitialValue(8192);
        mqgmo_Convert.setInitialValue(16384);
        mqh_Object_Mqrfh2_Mqrfh2_Strucid.setInitialValue("RFH ");
        mqh_Object_Mqrfh2_Mqrfh2_Version.setInitialValue(2);
        mqh_Object_Mqrfh2_Mqrfh2_Struclength.setInitialValue(136);
        mqh_Object_Mqrfh2_Mqrfh2_Encoding.setInitialValue(785);
        mqh_Object_Mqrfh2_Mqrfh2_Codedcharsetid.setInitialValue(-2);
        mqh_Object_Mqrfh2_Mqrfh2_Format.setInitialValue("MQSTR   ");
        mqh_Object_Mqrfh2_Mqrfh2_Flags.setInitialValue(0);
        mqh_Object_Mqrfh2_Mqrfh2_Namevalueccsid.setInitialValue(500);
        mqh_Object_Mqrfh2_Mqrfh2_Namevaluelength.setInitialValue(96);
        mqh_Object_Mqrfh2_Mqrfh2_Namevaluedata.setInitialValue(" ");
        mqm_Object_Descriptor_Mqod_Strucid.setInitialValue("OD  ");
        mqm_Object_Descriptor_Mqod_Version.setInitialValue(1);
        mqm_Object_Descriptor_Mqod_Objecttype.setInitialValue(1);
        mqm_Object_Descriptor_Mqod_Objectname.setInitialValue(" ");
        mqm_Object_Descriptor_Mqod_Dynamicqname.setInitialValue("CSQ.*");
        mqm_Object_Descriptor_Mqod_Alternateuserid.setInitialValue(" ");
        mqm_Object_Descriptor_Mqod_Recspresent.setInitialValue(0);
        mqm_Object_Descriptor_Mqod_Knowndestcount.setInitialValue(0);
        mqm_Object_Descriptor_Mqod_Unknowndestcount.setInitialValue(0);
        mqm_Object_Descriptor_Mqod_Invaliddestcount.setInitialValue(0);
        mqm_Object_Descriptor_Mqod_Objectrecoffset.setInitialValue(0);
        mqm_Object_Descriptor_Mqod_Responserecoffset.setInitialValue(0);
        mqm_Message_Descriptor_Mqmd_Strucid.setInitialValue("MD  ");
        mqm_Message_Descriptor_Mqmd_Version.setInitialValue(1);
        mqm_Message_Descriptor_Mqmd_Report.setInitialValue(0);
        mqm_Message_Descriptor_Mqmd_Msgtype.setInitialValue(8);
        mqm_Message_Descriptor_Mqmd_Expiry.setInitialValue(60000);
        mqm_Message_Descriptor_Mqmd_Feedback.setInitialValue(0);
        mqm_Message_Descriptor_Mqmd_Encoding.setInitialValue(785);
        mqm_Message_Descriptor_Mqmd_Codedcharsetid.setInitialValue(0);
        mqm_Message_Descriptor_Mqmd_Priority.setInitialValue(-1);
        mqm_Message_Descriptor_Mqmd_Persistence.setInitialValue(2);
        mqm_Message_Descriptor_Mqmd_Msgid.setInitialValue(" ");
        mqm_Message_Descriptor_Mqmd_Correlid.setInitialValue(" ");
        mqm_Message_Descriptor_Mqmd_Backoutcount.setInitialValue(0);
        mqm_Message_Descriptor_Mqmd_Putappltype.setInitialValue(0);
        mqm_Get_Message_Options_Mqgmo_Strucid.setInitialValue("GMO ");
        mqm_Get_Message_Options_Mqgmo_Version.setInitialValue(1);
        mqm_Get_Message_Options_Mqgmo_Options.setInitialValue(0);
        mqm_Get_Message_Options_Mqgmo_Waitinterval.setInitialValue(0);
        mqm_Get_Message_Options_Mqgmo_Signal2.setInitialValue(0);
        mqm_Get_Message_Options_Mqgmo_Matchoptions.setInitialValue(3);
        mqm_Get_Message_Options_Mqgmo_Returnedlength.setInitialValue(-1);
        mqm_Put_Message_Options_Mqpmo_Strucid.setInitialValue("PMO ");
        mqm_Put_Message_Options_Mqpmo_Version.setInitialValue(1);
        mqm_Put_Message_Options_Mqpmo_Options.setInitialValue(0);
        mqm_Put_Message_Options_Mqpmo_Timeout.setInitialValue(600);
        mqm_Put_Message_Options_Mqpmo_Context.setInitialValue(0);
        mqm_Put_Message_Options_Mqpmo_Knowndestcount.setInitialValue(0);
        mqm_Put_Message_Options_Mqpmo_Unknowndestcount.setInitialValue(0);
        mqm_Put_Message_Options_Mqpmo_Invaliddestcount.setInitialValue(0);
        mq_Error_Data_Mq_Error_Prog_Id.setInitialValue("MQNAT01");
        mq_Error_Data_Mq_Error_Filler1.setInitialValue(" ");
        mq_Error_Data_Mq_Error_Filler2.setInitialValue(" ");
        mq_Error_Data_Mq_Error_Filler3.setInitialValue(" ");
        mq_Error_Data_Mq_Error_Filler4.setInitialValue(" ");
        pls_Hconn.setInitialValue(0);
        pls_Hobj1.setInitialValue(0);
        pls_Hobj2.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisp0051() throws Exception
    {
        super("Cisp0051");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("CISP0051", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 80 KD = ON
        //* ***********************************************************************
        //*                               MAIN ROUTINES
        //* ***********************************************************************
        pls_Trace.setValue(true);                                                                                                                                         //Natural: MOVE TRUE TO +TRACE
        pnd_Data_In.getValue(1,":",32760).setValue(gdaCisg0051.getPnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_Data().getValue(1,":",32760));                                        //Natural: ASSIGN #DATA-IN ( 1:32760 ) := ##CISG0051-DATA ( 1:32760 )
        vw_curr_Environment.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) CURR-ENVIRONMENT WITH ENV-ID = ' '
        (
        "READ01",
        new Wc[] { new Wc("ENV_ID", ">=", " ", WcType.BY) },
        new Oc[] { new Oc("ENV_ID", "ASC") },
        1
        );
        READ01:
        while (condition(vw_curr_Environment.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "=",curr_Environment_Env_Id);                                                                                                               //Natural: WRITE '=' ENV-ID
        if (Global.isEscape()) return;
        pnd_Messagelength.compute(new ComputeParameters(false, pnd_Messagelength), DbsField.add(200,gdaCisg0051.getPnd_Pnd_Cisg0051_Pnd_Pnd_Pda_Length()));               //Natural: ASSIGN #MESSAGELENGTH := 200 + ##PDA-LENGTH
        pnd_Appl_Data_Out_Pnd_Response_Code.setValue("0000");                                                                                                             //Natural: ASSIGN #RESPONSE-CODE := '0000'
        pnd_Appl_Data_Out_Pnd_Response_Text.setValue(" ");                                                                                                                //Natural: ASSIGN #RESPONSE-TEXT := ' '
        FOR01:                                                                                                                                                            //Natural: FOR #INDX 1 TO 12
        for (pnd_Indx.setValue(1); condition(pnd_Indx.lessOrEqual(12)); pnd_Indx.nadd(1))
        {
            if (condition(pnd_Qmgrname_For_Curr_Env_Pnd_Curr_Env.getValue(pnd_Indx).equals(curr_Environment_Env_Id)))                                                     //Natural: IF #CURR-ENV ( #INDX ) = ENV-ID
            {
                pnd_Qmgrname.setValue(pnd_Qmgrname_For_Curr_Env_Pnd_Queuemgr.getValue(pnd_Indx));                                                                         //Natural: ASSIGN #QMGRNAME := #QUEUEMGR ( #INDX )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Indx);                                                                                                                              //Natural: WRITE '=' #INDX
        if (Global.isEscape()) return;
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "QMGR Name   :",pnd_Qmgrname);                                                                                                          //Natural: WRITE 'QMGR Name   :' #QMGRNAME
            if (Global.isEscape()) return;
            getReports().write(0, "Input QName :",pnd_Inputqname);                                                                                                        //Natural: WRITE 'Input QName :' #INPUTQNAME
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CONNECT-MQ-QUEUE-MGR
        sub_Connect_Mq_Queue_Mgr();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Appl_Data_Out_Pnd_Response_Code.equals("0000")))                                                                                                //Natural: IF #RESPONSE-CODE EQ '0000'
        {
                                                                                                                                                                          //Natural: PERFORM OPEN-INPUT-QUEUE
            sub_Open_Input_Queue();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pls_Hconn.setValue(pnd_Hconn);                                                                                                                                    //Natural: ASSIGN +HCONN := #HCONN
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "=",pls_Hconn);                                                                                                                         //Natural: WRITE '=' +HCONN
            if (Global.isEscape()) return;
            getReports().write(0, pnd_Appl_Data_Out_Pnd_Response_Code);                                                                                                   //Natural: WRITE #RESPONSE-CODE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        gdaCisg0051.getPnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_Data().getValue(201,":",400).setValue(pnd_Appl_Data_Out.getValue(201,":",400));                                  //Natural: ASSIGN ##CISG0051-DATA ( 201:400 ) := #APPL-DATA-OUT ( 201:400 )
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONNECT-MQ-QUEUE-MGR
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-INPUT-QUEUE
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Connect_Mq_Queue_Mgr() throws Exception                                                                                                              //Natural: CONNECT-MQ-QUEUE-MGR
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "MQConn Time IN :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));                                                                //Natural: WRITE 'MQConn Time IN :' *TIMX ( EM = HH:II:SS.T )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        mqconnReturnCode = DbsUtil.callExternalProgram("MQCONN",pnd_Qmgrname,pnd_Hconn,pnd_Compcode,pnd_Reason);                                                          //Natural: CALL 'MQCONN' USING #QMGRNAME #HCONN #COMPCODE #REASON
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "MQConn Time OUT:",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"),NEWLINE,"COMPCODE: ",pnd_Compcode,"REASON: ",                    //Natural: WRITE 'MQConn Time OUT:' *TIMX ( EM = HH:II:SS.T ) / 'COMPCODE: ' #COMPCODE 'REASON: ' #REASON 'HCONN: ' #HCONN
                pnd_Reason,"HCONN: ",pnd_Hconn);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Compcode.notEquals(getZero()) || pnd_Reason.notEquals(getZero())))                                                                              //Natural: IF #COMPCODE NE 0 OR #REASON NE 0
        {
            pnd_Appl_Data_Out_Pnd_Response_Code.setValue(pnd_Compcode);                                                                                                   //Natural: ASSIGN #RESPONSE-CODE := #COMPCODE
            pnd_Appl_Data_Out_Pnd_Response_Text.setValue(DbsUtil.compress(pnd_Error_Handler_Area_Pnd_Program_Name, "COMPCODE: ", pnd_Compcode, " REASON: ",               //Natural: COMPRESS #PROGRAM-NAME 'COMPCODE: ' #COMPCODE ' REASON: ' #REASON ' FAILED: CONNECT MQ ERROR TO ' #QMGRNAME INTO #RESPONSE-TEXT
                pnd_Reason, " FAILED: CONNECT MQ ERROR TO ", pnd_Qmgrname));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pls_Qmgrname.setValue(pnd_Qmgrname);                                                                                                                          //Natural: ASSIGN +QMGRNAME := #QMGRNAME
            pls_Hconn.setValue(pnd_Hconn);                                                                                                                                //Natural: ASSIGN +HCONN := #HCONN
        }                                                                                                                                                                 //Natural: END-IF
        //*  CONNECT-MQ-PUT-QUEUE
    }
    private void sub_Open_Input_Queue() throws Exception                                                                                                                  //Natural: OPEN-INPUT-QUEUE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Openoptions.reset();                                                                                                                                          //Natural: RESET #OPENOPTIONS
        mqm_Object_Descriptor_Mqod_Alternateuserid.setValue(pnd_Alternate_Userid);                                                                                        //Natural: ASSIGN MQOD-ALTERNATEUSERID := #ALTERNATE-USERID
        mqm_Object_Descriptor_Mqod_Objectqmgrname.setValue(mqm_Message_Descriptor_Mqmd_Replytoqmgr);                                                                      //Natural: ASSIGN MQOD-OBJECTQMGRNAME := MQMD-REPLYTOQMGR
        mqm_Object_Descriptor_Mqod_Objectname.setValue(pnd_Inputqname);                                                                                                   //Natural: ASSIGN MQOD-OBJECTNAME := #INPUTQNAME
        pnd_Openoptions.compute(new ComputeParameters(false, pnd_Openoptions), mqoo_Output.add(mqoo_Alternate_User_Authority));                                           //Natural: ASSIGN #OPENOPTIONS := MQOO-OUTPUT + MQOO-ALTERNATE-USER-AUTHORITY
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Calling MQOpen Input Queue Time IN :",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"),NEWLINE,"=",mqm_Object_Descriptor_Mqod_Alternateuserid, //Natural: WRITE 'Calling MQOpen Input Queue Time IN :' *TIMX ( EM = HH:II:SS.T ) /'=' MQOD-ALTERNATEUSERID '=' MQOD-OBJECTQMGRNAME '=' MQOD-OBJECTNAME '=' #OPENOPTIONS '=' #HOBJ2 '=' MQOO-OUTPUT '=' MQOO-ALTERNATE-USER-AUTHORITY
                "=",mqm_Object_Descriptor_Mqod_Objectqmgrname,"=",mqm_Object_Descriptor_Mqod_Objectname,"=",pnd_Openoptions,"=",pnd_Hobj2,"=",mqoo_Output,
                "=",mqoo_Alternate_User_Authority);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        mqopenReturnCode = DbsUtil.callExternalProgram("MQOPEN",pnd_Hconn,mqm_Object_Descriptor_Mqod.getValue(1,":",336),pnd_Openoptions,pnd_Hobj2,pnd_Compcode,          //Natural: CALL 'MQOPEN' USING #HCONN MQOD ( 1:336 ) #OPENOPTIONS #HOBJ2 #COMPCODE #REASON
            pnd_Reason);
        if (condition(pls_Trace.getBoolean()))                                                                                                                            //Natural: IF +TRACE
        {
            getReports().write(0, "Back from MQOpen Input Time OUT:",Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"),NEWLINE,"CompCode:",pnd_Compcode,                //Natural: WRITE 'Back from MQOpen Input Time OUT:' *TIMX ( EM = HH:II:SS.T ) / 'CompCode:' #COMPCODE 'Reason:' #REASON 'HConn:' #HCONN / 'HobJ2:' #HOBJ2 'MQOD:' MQOD ( 1:336 ) '=' MQOD-ALTERNATEUSERID '=' MQOD-OBJECTQMGRNAME '=' MQOD-OBJECTNAME /'=' MQOO-OUTPUT /'=' MQOO-ALTERNATE-USER-AUTHORITY
                "Reason:",pnd_Reason,"HConn:",pnd_Hconn,NEWLINE,"HobJ2:",pnd_Hobj2,"MQOD:",mqm_Object_Descriptor_Mqod.getValue(1,":",336),"=",mqm_Object_Descriptor_Mqod_Alternateuserid,
                "=",mqm_Object_Descriptor_Mqod_Objectqmgrname,"=",mqm_Object_Descriptor_Mqod_Objectname,NEWLINE,"=",mqoo_Output,NEWLINE,"=",mqoo_Alternate_User_Authority);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Compcode.notEquals(getZero()) || pnd_Reason.notEquals(getZero())))                                                                              //Natural: IF #COMPCODE NE 0 OR #REASON NE 0
        {
            pnd_Appl_Data_Out_Pnd_Response_Code.setValue("0002");                                                                                                         //Natural: ASSIGN #RESPONSE-CODE := '0002'
            pnd_Appl_Data_Out_Pnd_Response_Text.setValue(DbsUtil.compress(pnd_Error_Handler_Area_Pnd_Program_Name, "FAILED: OPEN INPUT QUEUE"));                          //Natural: COMPRESS #PROGRAM-NAME 'FAILED: OPEN INPUT QUEUE' INTO #RESPONSE-TEXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pls_Hconn.setValue(pnd_Hconn);                                                                                                                                //Natural: ASSIGN +HCONN := #HCONN
            pls_Mqod.getValue(1,":",336).setValue(mqm_Object_Descriptor_Mqod.getValue(1,":",336));                                                                        //Natural: ASSIGN +MQOD ( 1:336 ) := MQOD ( 1:336 )
            pls_Hobj2.setValue(pnd_Hobj2);                                                                                                                                //Natural: ASSIGN +HOBJ2 := #HOBJ2
        }                                                                                                                                                                 //Natural: END-IF
        //*  OPEN-INPUT-QUEUE
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Appl_Data_Out_Pnd_Response_Code.setValue("1000");                                                                                                             //Natural: ASSIGN #RESPONSE-CODE := '1000'
        pnd_Appl_Data_Out_Pnd_Response_Text.setValue(DbsUtil.compress(pnd_Error_Handler_Area_Pnd_Program_Name, "FAILED: ERROR NBR:", Global.getERROR_NR(),                //Natural: COMPRESS #PROGRAM-NAME 'FAILED: ERROR NBR:' *ERROR-NR ' ERROR LINE:' *ERROR-LINE TO #RESPONSE-TEXT
            " ERROR LINE:", Global.getERROR_LINE()));
        gdaCisg0051.getPnd_Pnd_Cisg0051_Pnd_Pnd_Cisg0051_Data().getValue(201,":",400).setValue(pnd_Appl_Data_Out.getValue(201,":",400));                                  //Natural: ASSIGN ##CISG0051-DATA ( 201:400 ) := #APPL-DATA-OUT ( 201:400 )
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=80 KD=ON");
    }
}
