/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:20:56 PM
**        * FROM NATURAL PROGRAM : Cisb100
************************************************************
**        * FILE NAME            : Cisb100.java
**        * CLASS NAME           : Cisb100
**        * INSTANCE NAME        : Cisb100
************************************************************
************************************************************************
* PROGRAM  : CISB100
* SYSTEM   : CIS
*
* MOD DATE   MOD BY   DESCRIPTION OF CHANGES
* ---------  -------- --------------------------------------------------
*
* MAY 28 02  DEDIOS   RE-ORDERED FUNDS IN REPORTS 1 AND 6.
*                     (M, C, S, W, AND B  TO  C, M, S, B AND W)
* 06/07/06 O SOTTO CHANGED AS PART OF MDO LEGAL SPLIT. SC 060706.
* 03/23/09 K GATES CHANGED AS PART OF IA TIAA ACCESS.  SEE TACC KG
* 08/26/2015 - BUDDY NEWSOM - INSTITUTIONAL PREMIUM FILE SUNSET (IPFS)
* REMOVE VIEW INSIDE THE PROGRAM AS IT IS NOT BEING USED OR REFERENCED.
* REPLACE IT WITH A CALL TO NECN4000 TO RETREVE THE BUSINESS DATE.
* 05/09/2017 -(GHOSABE)     - PIN EXPANSION CHANGES.(C420007)  PINE.
************************************************************************
*

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb100 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cis;
    private DbsField cis_Cis_Pin_Nbr;
    private DbsField cis_Cis_Rqst_Id;
    private DbsField cis_Cis_Opn_Clsd_Ind;
    private DbsField cis_Cis_Status_Cd;
    private DbsField cis_Cis_Rqst_Id_Key;
    private DbsField cis_Cis_Cntrct_Apprvl_Ind;
    private DbsField cis_Cis_Tiaa_Nbr;
    private DbsField cis_Cis_Cert_Nbr;
    private DbsField cis_Cis_Tiaa_Doi;
    private DbsField cis_Cis_Cref_Doi;
    private DbsField cis_Cis_Annty_Option;
    private DbsField cis_Cis_Cntrct_Print_Dte;
    private DbsField cis_Cis_Cntrct_Type;
    private DbsField cis_Cis_Pymnt_Mode;
    private DbsField cis_Cis_Grnted_Grd_Amt;
    private DbsField cis_Cis_Grnted_Std_Amt;

    private DbsGroup cis_Cis_Da_Tiaa_Cntrcts_Rqst;
    private DbsField cis_Cis_Da_Tiaa_Nbr;
    private DbsField cis_Cis_Da_Tiaa_Proceeds_Amt;
    private DbsField cis_Cis_Da_Rea_Proceeds_Amt;

    private DbsGroup cis_Cis_Da_Cref_Cntrcts_Rqst;
    private DbsField cis_Cis_Da_Cref_Proceeds_Amt;

    private DbsGroup cis_Cis_Cref_Annty_Pymnt;
    private DbsField cis_Cis_Cref_Acct_Cde;
    private DbsField cis_Cis_Cref_Mnthly_Nbr_Units;
    private DbsField cis_Cis_Cref_Annual_Nbr_Units;
    private DbsField cis_Cis_Rea_Mnthly_Nbr_Units;
    private DbsField cis_Cis_Rea_Annual_Nbr_Units;
    private DbsField cis_Cis_Rea_Annty_Amt;
    private DbsField cis_Cis_Mdo_Contract_Cash_Status;
    private DbsField cis_Cis_Mdo_Contract_Type;
    private DbsField cis_Cis_Mdo_Traditional_Amt;
    private DbsField cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt;
    private DbsField cis_Cis_Mdo_Cref_Int_Pymnt_Amt;
    private DbsField cis_Cis_Mdo_Tiaa_Excluded_Amt;
    private DbsField cis_Cis_Mdo_Cref_Excluded_Amt;
    private DbsField cis_Cis_Rtb_Amt;
    private DbsField cis_Cis_Trnsf_Flag;
    private DbsField cis_Cis_Tacc_Annty_Amt;

    private DbsGroup cis_Cis_Tacc_Fund_Info;
    private DbsField cis_Cis_Tacc_Ind;
    private DbsField cis_Cis_Tacc_Account;
    private DbsField cis_Cis_Tacc_Mnthly_Nbr_Units;
    private DbsField cis_Cis_Tacc_Annual_Nbr_Units;
    private DbsField pnd_P;
    private DbsField pnd_R;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_I;
    private DbsField pnd_Record_Found;
    private DbsField pnd_Grnted_Grd_Amt;
    private DbsField pnd_Grnted_Grd_Amt_Tpa;
    private DbsField pnd_Grnted_Grd_Amt_Ipro;
    private DbsField pnd_Grnted_Grd_Amt_T;
    private DbsField pnd_Grnted_Std_Amt;
    private DbsField pnd_Grnted_Std_Amt_Tpa;
    private DbsField pnd_Grnted_Std_Amt_Ipro;
    private DbsField pnd_Grnted_Std_Amt_T;
    private DbsField pnd_Grnted_Grd_Amt_Tot;
    private DbsField pnd_Grnted_Grd_Amt_Tot_Tpa;
    private DbsField pnd_Grnted_Grd_Amt_Tot_Ipro;
    private DbsField pnd_Grnted_Grd_Amt_Tot_T;
    private DbsField pnd_Grnted_Std_Amt_Tot;
    private DbsField pnd_Grnted_Std_Amt_Tot_Tpa;
    private DbsField pnd_Grnted_Std_Amt_Tot_Ipro;
    private DbsField pnd_Grnted_Std_Amt_Tot_T;
    private DbsField pnd_M_Tiaa_Rea;
    private DbsField pnd_M_Tiaa_Rea_T;
    private DbsField pnd_M_Mma;
    private DbsField pnd_M_Stock;
    private DbsField pnd_M_Social;
    private DbsField pnd_M_Global;
    private DbsField pnd_M_Bond;
    private DbsField pnd_M_Grth;
    private DbsField pnd_M_Equ;
    private DbsField pnd_M_Ilb;
    private DbsField pnd_M_Taccess;
    private DbsField pnd_M_Mma_T;
    private DbsField pnd_M_Stock_T;
    private DbsField pnd_M_Social_T;
    private DbsField pnd_M_Global_T;
    private DbsField pnd_M_Bond_T;
    private DbsField pnd_M_Grth_T;
    private DbsField pnd_M_Equ_T;
    private DbsField pnd_M_Ilb_T;
    private DbsField pnd_M_Taccess_T;
    private DbsField pnd_A_Tiaa_Rea;
    private DbsField pnd_A_Tiaa_Rea_T;
    private DbsField pnd_A_Mma;
    private DbsField pnd_A_Stock;
    private DbsField pnd_A_Social;
    private DbsField pnd_A_Global;
    private DbsField pnd_A_Bond;
    private DbsField pnd_A_Grth;
    private DbsField pnd_A_Equ;
    private DbsField pnd_A_Ilb;
    private DbsField pnd_A_Taccess;
    private DbsField pnd_A_Mma_T;
    private DbsField pnd_A_Stock_T;
    private DbsField pnd_A_Social_T;
    private DbsField pnd_A_Global_T;
    private DbsField pnd_A_Bond_T;
    private DbsField pnd_A_Grth_T;
    private DbsField pnd_A_Equ_T;
    private DbsField pnd_A_Ilb_T;
    private DbsField pnd_A_Taccess_T;
    private DbsField pnd_Trad_Annuity;
    private DbsField pnd_Trad_Annuity_Tpa;
    private DbsField pnd_Trad_Annuity_Ipro;
    private DbsField pnd_Trad_Annuity_T;
    private DbsField pnd_Trad_Annuity_T_Tpa;
    private DbsField pnd_Trad_Annuity_T_Ipro;
    private DbsField pnd_Real_Annuity;
    private DbsField pnd_Real_Annuity_T;
    private DbsField pnd_Cref_Annuity;
    private DbsField pnd_Cref_Annuity_T;
    private DbsField pnd_Tacc_Annuity;
    private DbsField pnd_Tacc_Annuity_T;
    private DbsField pnd_Rea_Tacc_Annuity;
    private DbsField pnd_Rea_Tacc_Annuity_T;
    private DbsField pnd_Da_Tiaa_Proceeds_Amt;
    private DbsField pnd_Da_Tiaa_Proceeds_Amt_Tpa;
    private DbsField pnd_Da_Tiaa_Proceeds_Amt_Ipro;
    private DbsField pnd_Da_Tiaa_Proceeds_Amt_T;
    private DbsField pnd_Da_Rea_Proceeds_Amt;
    private DbsField pnd_Da_Rea_Proceeds_Amt_T;
    private DbsField pnd_Da_Cref_Proceeds_Amt;
    private DbsField pnd_Da_Cref_Proceeds_Amt_T;
    private DbsField pnd_Contract_Issued;
    private DbsField pnd_Contract_Issued_Tpa;
    private DbsField pnd_Contract_Issued_Ipro;
    private DbsField pnd_Contract_Issued_T;
    private DbsField pnd_Contract_Issued_T_Tpa;
    private DbsField pnd_Contract_Issued_T_Ipro;
    private DbsField pnd_Certif_Issued;
    private DbsField pnd_Certif_Issued_T;
    private DbsField pnd_Certif_Issued2;
    private DbsField pnd_Certif_Issued2_T;
    private DbsField pnd_No_Rec;
    private DbsField pnd_No_Rec_Tpa;
    private DbsField pnd_No_Rec_Ipro;
    private DbsField pnd_No_Rec_T;
    private DbsField pnd_No_Rec_T_Tpa;
    private DbsField pnd_No_Rec_T_Ipro;
    private DbsField pnd_N_Rec;
    private DbsField pnd_M_Rec;
    private DbsField pnd_T_Rec;
    private DbsField pnd_Ia_Rq_Pr;
    private DbsField pnd_Ia_Rq_Pr_T;
    private DbsField pnd_Mdo_Rq_Pr;
    private DbsField pnd_Ia_Total;
    private DbsField pnd_Ia_Total_T;
    private DbsField pnd_Mdo_Total;
    private DbsField pnd_Tpa_Record;

    private DbsGroup pnd_Tpa_Fields;
    private DbsField pnd_Tpa_Fields_Pnd_Tpa_Da_Tiaa_Proceeds_Amt;
    private DbsField pnd_Tpa_Fields_Pnd_Tpa_Grnted_Grd_Amt;
    private DbsField pnd_Tpa_Fields_Pnd_Tpa_Rtb_Amt;
    private DbsField pnd_Run_Date;
    private DbsField pnd_Print_Date;
    private DbsField pnd_A;
    private DbsField pnd_Apprvl;
    private DbsField pnd_N_Apprvl;

    private DbsGroup pnd_Mdo_Fields;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Cref_Int_Pymnt;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Cref_Excl;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Mma;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Stock;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Social;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Global;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Bond;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Grth;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Equ;
    private DbsField pnd_Mdo_Fields_Pnd_Mdo_Ilb;
    private DbsField pnd_Set_Tot;
    private DbsField pnd_Todays_Business_Dt;

    private DbsGroup pnd_Todays_Business_Dt__R_Field_1;
    private DbsField pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A;

    private DbsGroup pnd_Todays_Business_Dt__R_Field_2;
    private DbsField pnd_Todays_Business_Dt_Pnd_Todays_Bus_Cc;
    private DbsField pnd_Todays_Business_Dt_Pnd_Todays_Bus_Yy;
    private DbsField pnd_Todays_Business_Dt_Pnd_Todays_Bus_Mm;
    private DbsField pnd_Todays_Business_Dt_Pnd_Todays_Bus_Dd;
    private DbsField pnd_Cis_Open_Status;
    private DbsField pnd_Aprl_Y_N;
    private DbsField pnd_Cis_Rqst_Id;
    private DbsField pnd_At_Start;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cis = new DataAccessProgramView(new NameInfo("vw_cis", "CIS"), "CIS_PRTCPNT_FILE_12", "CIS_PRTCPNT_FILE", DdmPeriodicGroups.getInstance().getGroups("CIS_PRTCPNT_FILE_12"));
        cis_Cis_Pin_Nbr = vw_cis.getRecord().newFieldInGroup("cis_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CIS_PIN_NBR");
        cis_Cis_Rqst_Id = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_RQST_ID");
        cis_Cis_Opn_Clsd_Ind = vw_cis.getRecord().newFieldInGroup("cis_Cis_Opn_Clsd_Ind", "CIS-OPN-CLSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_OPN_CLSD_IND");
        cis_Cis_Status_Cd = vw_cis.getRecord().newFieldInGroup("cis_Cis_Status_Cd", "CIS-STATUS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_STATUS_CD");
        cis_Cis_Rqst_Id_Key = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "CIS_RQST_ID_KEY");
        cis_Cis_Cntrct_Apprvl_Ind = vw_cis.getRecord().newFieldInGroup("cis_Cis_Cntrct_Apprvl_Ind", "CIS-CNTRCT-APPRVL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_CNTRCT_APPRVL_IND");
        cis_Cis_Tiaa_Nbr = vw_cis.getRecord().newFieldInGroup("cis_Cis_Tiaa_Nbr", "CIS-TIAA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_TIAA_NBR");
        cis_Cis_Cert_Nbr = vw_cis.getRecord().newFieldInGroup("cis_Cis_Cert_Nbr", "CIS-CERT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_CERT_NBR");
        cis_Cis_Tiaa_Doi = vw_cis.getRecord().newFieldInGroup("cis_Cis_Tiaa_Doi", "CIS-TIAA-DOI", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_TIAA_DOI");
        cis_Cis_Cref_Doi = vw_cis.getRecord().newFieldInGroup("cis_Cis_Cref_Doi", "CIS-CREF-DOI", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CREF_DOI");
        cis_Cis_Annty_Option = vw_cis.getRecord().newFieldInGroup("cis_Cis_Annty_Option", "CIS-ANNTY-OPTION", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CIS_ANNTY_OPTION");
        cis_Cis_Cntrct_Print_Dte = vw_cis.getRecord().newFieldInGroup("cis_Cis_Cntrct_Print_Dte", "CIS-CNTRCT-PRINT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CIS_CNTRCT_PRINT_DTE");
        cis_Cis_Cntrct_Type = vw_cis.getRecord().newFieldInGroup("cis_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_CNTRCT_TYPE");
        cis_Cis_Pymnt_Mode = vw_cis.getRecord().newFieldInGroup("cis_Cis_Pymnt_Mode", "CIS-PYMNT-MODE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_PYMNT_MODE");
        cis_Cis_Grnted_Grd_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Grnted_Grd_Amt", "CIS-GRNTED-GRD-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "CIS_GRNTED_GRD_AMT");
        cis_Cis_Grnted_Std_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Grnted_Std_Amt", "CIS-GRNTED-STD-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "CIS_GRNTED_STD_AMT");

        cis_Cis_Da_Tiaa_Cntrcts_Rqst = vw_cis.getRecord().newGroupArrayInGroup("cis_Cis_Da_Tiaa_Cntrcts_Rqst", "CIS-DA-TIAA-CNTRCTS-RQST", new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Cis_Da_Tiaa_Nbr = cis_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldInGroup("cis_Cis_Da_Tiaa_Nbr", "CIS-DA-TIAA-NBR", FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_DA_TIAA_NBR", "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Cis_Da_Tiaa_Proceeds_Amt = cis_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldInGroup("cis_Cis_Da_Tiaa_Proceeds_Amt", "CIS-DA-TIAA-PROCEEDS-AMT", FieldType.NUMERIC, 
            11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_TIAA_PROCEEDS_AMT", "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Cis_Da_Rea_Proceeds_Amt = cis_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldInGroup("cis_Cis_Da_Rea_Proceeds_Amt", "CIS-DA-REA-PROCEEDS-AMT", FieldType.NUMERIC, 
            11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_REA_PROCEEDS_AMT", "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");

        cis_Cis_Da_Cref_Cntrcts_Rqst = vw_cis.getRecord().newGroupArrayInGroup("cis_Cis_Da_Cref_Cntrcts_Rqst", "CIS-DA-CREF-CNTRCTS-RQST", new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Cis_Da_Cref_Proceeds_Amt = cis_Cis_Da_Cref_Cntrcts_Rqst.newFieldInGroup("cis_Cis_Da_Cref_Proceeds_Amt", "CIS-DA-CREF-PROCEEDS-AMT", FieldType.NUMERIC, 
            11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_CREF_PROCEEDS_AMT", "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");

        cis_Cis_Cref_Annty_Pymnt = vw_cis.getRecord().newGroupArrayInGroup("cis_Cis_Cref_Annty_Pymnt", "CIS-CREF-ANNTY-PYMNT", new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Cis_Cref_Acct_Cde = cis_Cis_Cref_Annty_Pymnt.newFieldInGroup("cis_Cis_Cref_Acct_Cde", "CIS-CREF-ACCT-CDE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_CREF_ACCT_CDE", "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Cis_Cref_Mnthly_Nbr_Units = cis_Cis_Cref_Annty_Pymnt.newFieldInGroup("cis_Cis_Cref_Mnthly_Nbr_Units", "CIS-CREF-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_MNTHLY_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Cis_Cref_Annual_Nbr_Units = cis_Cis_Cref_Annty_Pymnt.newFieldInGroup("cis_Cis_Cref_Annual_Nbr_Units", "CIS-CREF-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_ANNUAL_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Cis_Rea_Mnthly_Nbr_Units = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rea_Mnthly_Nbr_Units", "CIS-REA-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, RepeatingFieldStrategy.None, "CIS_REA_MNTHLY_NBR_UNITS");
        cis_Cis_Rea_Annual_Nbr_Units = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rea_Annual_Nbr_Units", "CIS-REA-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, RepeatingFieldStrategy.None, "CIS_REA_ANNUAL_NBR_UNITS");
        cis_Cis_Rea_Annty_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rea_Annty_Amt", "CIS-REA-ANNTY-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "CIS_REA_ANNTY_AMT");
        cis_Cis_Mdo_Contract_Cash_Status = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Contract_Cash_Status", "CIS-MDO-CONTRACT-CASH-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_MDO_CONTRACT_CASH_STATUS");
        cis_Cis_Mdo_Contract_Type = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Contract_Type", "CIS-MDO-CONTRACT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_MDO_CONTRACT_TYPE");
        cis_Cis_Mdo_Traditional_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Traditional_Amt", "CIS-MDO-TRADITIONAL-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TRADITIONAL_AMT");
        cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt", "CIS-MDO-TIAA-INT-PYMNT-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TIAA_INT_PYMNT_AMT");
        cis_Cis_Mdo_Cref_Int_Pymnt_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Cref_Int_Pymnt_Amt", "CIS-MDO-CREF-INT-PYMNT-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_MDO_CREF_INT_PYMNT_AMT");
        cis_Cis_Mdo_Tiaa_Excluded_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Tiaa_Excluded_Amt", "CIS-MDO-TIAA-EXCLUDED-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TIAA_EXCLUDED_AMT");
        cis_Cis_Mdo_Cref_Excluded_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Mdo_Cref_Excluded_Amt", "CIS-MDO-CREF-EXCLUDED-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_MDO_CREF_EXCLUDED_AMT");
        cis_Cis_Rtb_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Rtb_Amt", "CIS-RTB-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "CIS_RTB_AMT");
        cis_Cis_Trnsf_Flag = vw_cis.getRecord().newFieldInGroup("cis_Cis_Trnsf_Flag", "CIS-TRNSF-FLAG", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_TRNSF_FLAG");
        cis_Cis_Tacc_Annty_Amt = vw_cis.getRecord().newFieldInGroup("cis_Cis_Tacc_Annty_Amt", "CIS-TACC-ANNTY-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, 
            "CIS_TACC_ANNTY_AMT");

        cis_Cis_Tacc_Fund_Info = vw_cis.getRecord().newGroupArrayInGroup("cis_Cis_Tacc_Fund_Info", "CIS-TACC-FUND-INFO", new DbsArrayController(1, 100) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Cis_Tacc_Ind = cis_Cis_Tacc_Fund_Info.newFieldInGroup("cis_Cis_Tacc_Ind", "CIS-TACC-IND", FieldType.STRING, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_TACC_IND", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Cis_Tacc_Account = cis_Cis_Tacc_Fund_Info.newFieldInGroup("cis_Cis_Tacc_Account", "CIS-TACC-ACCOUNT", FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_TACC_ACCOUNT", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Cis_Tacc_Mnthly_Nbr_Units = cis_Cis_Tacc_Fund_Info.newFieldInGroup("cis_Cis_Tacc_Mnthly_Nbr_Units", "CIS-TACC-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_MNTHLY_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Cis_Tacc_Annual_Nbr_Units = cis_Cis_Tacc_Fund_Info.newFieldInGroup("cis_Cis_Tacc_Annual_Nbr_Units", "CIS-TACC-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 
            11, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_ANNUAL_NBR_UNITS", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        registerRecord(vw_cis);

        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.NUMERIC, 1);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.NUMERIC, 1);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 2);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.NUMERIC, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Record_Found = localVariables.newFieldInRecord("pnd_Record_Found", "#RECORD-FOUND", FieldType.BOOLEAN, 1);
        pnd_Grnted_Grd_Amt = localVariables.newFieldArrayInRecord("pnd_Grnted_Grd_Amt", "#GRNTED-GRD-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Grnted_Grd_Amt_Tpa = localVariables.newFieldArrayInRecord("pnd_Grnted_Grd_Amt_Tpa", "#GRNTED-GRD-AMT-TPA", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Grnted_Grd_Amt_Ipro = localVariables.newFieldArrayInRecord("pnd_Grnted_Grd_Amt_Ipro", "#GRNTED-GRD-AMT-IPRO", FieldType.NUMERIC, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Grnted_Grd_Amt_T = localVariables.newFieldArrayInRecord("pnd_Grnted_Grd_Amt_T", "#GRNTED-GRD-AMT-T", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Grnted_Std_Amt = localVariables.newFieldArrayInRecord("pnd_Grnted_Std_Amt", "#GRNTED-STD-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Grnted_Std_Amt_Tpa = localVariables.newFieldArrayInRecord("pnd_Grnted_Std_Amt_Tpa", "#GRNTED-STD-AMT-TPA", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Grnted_Std_Amt_Ipro = localVariables.newFieldArrayInRecord("pnd_Grnted_Std_Amt_Ipro", "#GRNTED-STD-AMT-IPRO", FieldType.NUMERIC, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Grnted_Std_Amt_T = localVariables.newFieldArrayInRecord("pnd_Grnted_Std_Amt_T", "#GRNTED-STD-AMT-T", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Grnted_Grd_Amt_Tot = localVariables.newFieldInRecord("pnd_Grnted_Grd_Amt_Tot", "#GRNTED-GRD-AMT-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Grnted_Grd_Amt_Tot_Tpa = localVariables.newFieldInRecord("pnd_Grnted_Grd_Amt_Tot_Tpa", "#GRNTED-GRD-AMT-TOT-TPA", FieldType.NUMERIC, 11, 2);
        pnd_Grnted_Grd_Amt_Tot_Ipro = localVariables.newFieldInRecord("pnd_Grnted_Grd_Amt_Tot_Ipro", "#GRNTED-GRD-AMT-TOT-IPRO", FieldType.NUMERIC, 11, 
            2);
        pnd_Grnted_Grd_Amt_Tot_T = localVariables.newFieldInRecord("pnd_Grnted_Grd_Amt_Tot_T", "#GRNTED-GRD-AMT-TOT-T", FieldType.NUMERIC, 11, 2);
        pnd_Grnted_Std_Amt_Tot = localVariables.newFieldInRecord("pnd_Grnted_Std_Amt_Tot", "#GRNTED-STD-AMT-TOT", FieldType.NUMERIC, 11, 2);
        pnd_Grnted_Std_Amt_Tot_Tpa = localVariables.newFieldInRecord("pnd_Grnted_Std_Amt_Tot_Tpa", "#GRNTED-STD-AMT-TOT-TPA", FieldType.NUMERIC, 11, 2);
        pnd_Grnted_Std_Amt_Tot_Ipro = localVariables.newFieldInRecord("pnd_Grnted_Std_Amt_Tot_Ipro", "#GRNTED-STD-AMT-TOT-IPRO", FieldType.NUMERIC, 11, 
            2);
        pnd_Grnted_Std_Amt_Tot_T = localVariables.newFieldInRecord("pnd_Grnted_Std_Amt_Tot_T", "#GRNTED-STD-AMT-TOT-T", FieldType.NUMERIC, 11, 2);
        pnd_M_Tiaa_Rea = localVariables.newFieldArrayInRecord("pnd_M_Tiaa_Rea", "#M-TIAA-REA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Tiaa_Rea_T = localVariables.newFieldArrayInRecord("pnd_M_Tiaa_Rea_T", "#M-TIAA-REA-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_M_Mma = localVariables.newFieldArrayInRecord("pnd_M_Mma", "#M-MMA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Stock = localVariables.newFieldArrayInRecord("pnd_M_Stock", "#M-STOCK", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Social = localVariables.newFieldArrayInRecord("pnd_M_Social", "#M-SOCIAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Global = localVariables.newFieldArrayInRecord("pnd_M_Global", "#M-GLOBAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Bond = localVariables.newFieldArrayInRecord("pnd_M_Bond", "#M-BOND", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Grth = localVariables.newFieldArrayInRecord("pnd_M_Grth", "#M-GRTH", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Equ = localVariables.newFieldArrayInRecord("pnd_M_Equ", "#M-EQU", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Ilb = localVariables.newFieldArrayInRecord("pnd_M_Ilb", "#M-ILB", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Taccess = localVariables.newFieldArrayInRecord("pnd_M_Taccess", "#M-TACCESS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Mma_T = localVariables.newFieldArrayInRecord("pnd_M_Mma_T", "#M-MMA-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Stock_T = localVariables.newFieldArrayInRecord("pnd_M_Stock_T", "#M-STOCK-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Social_T = localVariables.newFieldArrayInRecord("pnd_M_Social_T", "#M-SOCIAL-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Global_T = localVariables.newFieldArrayInRecord("pnd_M_Global_T", "#M-GLOBAL-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Bond_T = localVariables.newFieldArrayInRecord("pnd_M_Bond_T", "#M-BOND-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Grth_T = localVariables.newFieldArrayInRecord("pnd_M_Grth_T", "#M-GRTH-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Equ_T = localVariables.newFieldArrayInRecord("pnd_M_Equ_T", "#M-EQU-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Ilb_T = localVariables.newFieldArrayInRecord("pnd_M_Ilb_T", "#M-ILB-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_M_Taccess_T = localVariables.newFieldArrayInRecord("pnd_M_Taccess_T", "#M-TACCESS-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_A_Tiaa_Rea = localVariables.newFieldArrayInRecord("pnd_A_Tiaa_Rea", "#A-TIAA-REA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Tiaa_Rea_T = localVariables.newFieldArrayInRecord("pnd_A_Tiaa_Rea_T", "#A-TIAA-REA-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_A_Mma = localVariables.newFieldArrayInRecord("pnd_A_Mma", "#A-MMA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Stock = localVariables.newFieldArrayInRecord("pnd_A_Stock", "#A-STOCK", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Social = localVariables.newFieldArrayInRecord("pnd_A_Social", "#A-SOCIAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Global = localVariables.newFieldArrayInRecord("pnd_A_Global", "#A-GLOBAL", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Bond = localVariables.newFieldArrayInRecord("pnd_A_Bond", "#A-BOND", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Grth = localVariables.newFieldArrayInRecord("pnd_A_Grth", "#A-GRTH", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Equ = localVariables.newFieldArrayInRecord("pnd_A_Equ", "#A-EQU", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Ilb = localVariables.newFieldArrayInRecord("pnd_A_Ilb", "#A-ILB", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Taccess = localVariables.newFieldArrayInRecord("pnd_A_Taccess", "#A-TACCESS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Mma_T = localVariables.newFieldArrayInRecord("pnd_A_Mma_T", "#A-MMA-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Stock_T = localVariables.newFieldArrayInRecord("pnd_A_Stock_T", "#A-STOCK-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Social_T = localVariables.newFieldArrayInRecord("pnd_A_Social_T", "#A-SOCIAL-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Global_T = localVariables.newFieldArrayInRecord("pnd_A_Global_T", "#A-GLOBAL-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Bond_T = localVariables.newFieldArrayInRecord("pnd_A_Bond_T", "#A-BOND-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Grth_T = localVariables.newFieldArrayInRecord("pnd_A_Grth_T", "#A-GRTH-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Equ_T = localVariables.newFieldArrayInRecord("pnd_A_Equ_T", "#A-EQU-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Ilb_T = localVariables.newFieldArrayInRecord("pnd_A_Ilb_T", "#A-ILB-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 5));
        pnd_A_Taccess_T = localVariables.newFieldArrayInRecord("pnd_A_Taccess_T", "#A-TACCESS-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Trad_Annuity = localVariables.newFieldArrayInRecord("pnd_Trad_Annuity", "#TRAD-ANNUITY", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Trad_Annuity_Tpa = localVariables.newFieldArrayInRecord("pnd_Trad_Annuity_Tpa", "#TRAD-ANNUITY-TPA", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Trad_Annuity_Ipro = localVariables.newFieldArrayInRecord("pnd_Trad_Annuity_Ipro", "#TRAD-ANNUITY-IPRO", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Trad_Annuity_T = localVariables.newFieldArrayInRecord("pnd_Trad_Annuity_T", "#TRAD-ANNUITY-T", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Trad_Annuity_T_Tpa = localVariables.newFieldArrayInRecord("pnd_Trad_Annuity_T_Tpa", "#TRAD-ANNUITY-T-TPA", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Trad_Annuity_T_Ipro = localVariables.newFieldArrayInRecord("pnd_Trad_Annuity_T_Ipro", "#TRAD-ANNUITY-T-IPRO", FieldType.NUMERIC, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_Real_Annuity = localVariables.newFieldArrayInRecord("pnd_Real_Annuity", "#REAL-ANNUITY", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Real_Annuity_T = localVariables.newFieldArrayInRecord("pnd_Real_Annuity_T", "#REAL-ANNUITY-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Cref_Annuity = localVariables.newFieldArrayInRecord("pnd_Cref_Annuity", "#CREF-ANNUITY", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Cref_Annuity_T = localVariables.newFieldArrayInRecord("pnd_Cref_Annuity_T", "#CREF-ANNUITY-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Tacc_Annuity = localVariables.newFieldArrayInRecord("pnd_Tacc_Annuity", "#TACC-ANNUITY", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Tacc_Annuity_T = localVariables.newFieldArrayInRecord("pnd_Tacc_Annuity_T", "#TACC-ANNUITY-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Rea_Tacc_Annuity = localVariables.newFieldArrayInRecord("pnd_Rea_Tacc_Annuity", "#REA-TACC-ANNUITY", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Rea_Tacc_Annuity_T = localVariables.newFieldArrayInRecord("pnd_Rea_Tacc_Annuity_T", "#REA-TACC-ANNUITY-T", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            5));
        pnd_Da_Tiaa_Proceeds_Amt = localVariables.newFieldArrayInRecord("pnd_Da_Tiaa_Proceeds_Amt", "#DA-TIAA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, 
            new DbsArrayController(1, 5));
        pnd_Da_Tiaa_Proceeds_Amt_Tpa = localVariables.newFieldArrayInRecord("pnd_Da_Tiaa_Proceeds_Amt_Tpa", "#DA-TIAA-PROCEEDS-AMT-TPA", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 5));
        pnd_Da_Tiaa_Proceeds_Amt_Ipro = localVariables.newFieldArrayInRecord("pnd_Da_Tiaa_Proceeds_Amt_Ipro", "#DA-TIAA-PROCEEDS-AMT-IPRO", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 5));
        pnd_Da_Tiaa_Proceeds_Amt_T = localVariables.newFieldArrayInRecord("pnd_Da_Tiaa_Proceeds_Amt_T", "#DA-TIAA-PROCEEDS-AMT-T", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 5));
        pnd_Da_Rea_Proceeds_Amt = localVariables.newFieldArrayInRecord("pnd_Da_Rea_Proceeds_Amt", "#DA-REA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_Da_Rea_Proceeds_Amt_T = localVariables.newFieldArrayInRecord("pnd_Da_Rea_Proceeds_Amt_T", "#DA-REA-PROCEEDS-AMT-T", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 5));
        pnd_Da_Cref_Proceeds_Amt = localVariables.newFieldArrayInRecord("pnd_Da_Cref_Proceeds_Amt", "#DA-CREF-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, 
            new DbsArrayController(1, 5));
        pnd_Da_Cref_Proceeds_Amt_T = localVariables.newFieldArrayInRecord("pnd_Da_Cref_Proceeds_Amt_T", "#DA-CREF-PROCEEDS-AMT-T", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 5));
        pnd_Contract_Issued = localVariables.newFieldArrayInRecord("pnd_Contract_Issued", "#CONTRACT-ISSUED", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_Contract_Issued_Tpa = localVariables.newFieldArrayInRecord("pnd_Contract_Issued_Tpa", "#CONTRACT-ISSUED-TPA", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_Contract_Issued_Ipro = localVariables.newFieldArrayInRecord("pnd_Contract_Issued_Ipro", "#CONTRACT-ISSUED-IPRO", FieldType.NUMERIC, 6, new 
            DbsArrayController(1, 5));
        pnd_Contract_Issued_T = localVariables.newFieldArrayInRecord("pnd_Contract_Issued_T", "#CONTRACT-ISSUED-T", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_Contract_Issued_T_Tpa = localVariables.newFieldArrayInRecord("pnd_Contract_Issued_T_Tpa", "#CONTRACT-ISSUED-T-TPA", FieldType.NUMERIC, 6, 
            new DbsArrayController(1, 5));
        pnd_Contract_Issued_T_Ipro = localVariables.newFieldArrayInRecord("pnd_Contract_Issued_T_Ipro", "#CONTRACT-ISSUED-T-IPRO", FieldType.NUMERIC, 
            6, new DbsArrayController(1, 5));
        pnd_Certif_Issued = localVariables.newFieldArrayInRecord("pnd_Certif_Issued", "#CERTIF-ISSUED", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_Certif_Issued_T = localVariables.newFieldArrayInRecord("pnd_Certif_Issued_T", "#CERTIF-ISSUED-T", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_Certif_Issued2 = localVariables.newFieldArrayInRecord("pnd_Certif_Issued2", "#CERTIF-ISSUED2", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_Certif_Issued2_T = localVariables.newFieldArrayInRecord("pnd_Certif_Issued2_T", "#CERTIF-ISSUED2-T", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_No_Rec = localVariables.newFieldArrayInRecord("pnd_No_Rec", "#NO-REC", FieldType.NUMERIC, 6, new DbsArrayController(1, 5));
        pnd_No_Rec_Tpa = localVariables.newFieldArrayInRecord("pnd_No_Rec_Tpa", "#NO-REC-TPA", FieldType.NUMERIC, 6, new DbsArrayController(1, 5));
        pnd_No_Rec_Ipro = localVariables.newFieldArrayInRecord("pnd_No_Rec_Ipro", "#NO-REC-IPRO", FieldType.NUMERIC, 6, new DbsArrayController(1, 5));
        pnd_No_Rec_T = localVariables.newFieldArrayInRecord("pnd_No_Rec_T", "#NO-REC-T", FieldType.NUMERIC, 6, new DbsArrayController(1, 5));
        pnd_No_Rec_T_Tpa = localVariables.newFieldArrayInRecord("pnd_No_Rec_T_Tpa", "#NO-REC-T-TPA", FieldType.NUMERIC, 6, new DbsArrayController(1, 5));
        pnd_No_Rec_T_Ipro = localVariables.newFieldArrayInRecord("pnd_No_Rec_T_Ipro", "#NO-REC-T-IPRO", FieldType.NUMERIC, 6, new DbsArrayController(1, 
            5));
        pnd_N_Rec = localVariables.newFieldArrayInRecord("pnd_N_Rec", "#N-REC", FieldType.NUMERIC, 9, new DbsArrayController(1, 9));
        pnd_M_Rec = localVariables.newFieldArrayInRecord("pnd_M_Rec", "#M-REC", FieldType.NUMERIC, 9, new DbsArrayController(1, 4));
        pnd_T_Rec = localVariables.newFieldArrayInRecord("pnd_T_Rec", "#T-REC", FieldType.NUMERIC, 9, new DbsArrayController(1, 9));
        pnd_Ia_Rq_Pr = localVariables.newFieldInRecord("pnd_Ia_Rq_Pr", "#IA-RQ-PR", FieldType.NUMERIC, 9);
        pnd_Ia_Rq_Pr_T = localVariables.newFieldInRecord("pnd_Ia_Rq_Pr_T", "#IA-RQ-PR-T", FieldType.NUMERIC, 9);
        pnd_Mdo_Rq_Pr = localVariables.newFieldInRecord("pnd_Mdo_Rq_Pr", "#MDO-RQ-PR", FieldType.NUMERIC, 9);
        pnd_Ia_Total = localVariables.newFieldInRecord("pnd_Ia_Total", "#IA-TOTAL", FieldType.NUMERIC, 9);
        pnd_Ia_Total_T = localVariables.newFieldInRecord("pnd_Ia_Total_T", "#IA-TOTAL-T", FieldType.NUMERIC, 9);
        pnd_Mdo_Total = localVariables.newFieldInRecord("pnd_Mdo_Total", "#MDO-TOTAL", FieldType.NUMERIC, 9);
        pnd_Tpa_Record = localVariables.newFieldInRecord("pnd_Tpa_Record", "#TPA-RECORD", FieldType.NUMERIC, 9);

        pnd_Tpa_Fields = localVariables.newGroupInRecord("pnd_Tpa_Fields", "#TPA-FIELDS");
        pnd_Tpa_Fields_Pnd_Tpa_Da_Tiaa_Proceeds_Amt = pnd_Tpa_Fields.newFieldInGroup("pnd_Tpa_Fields_Pnd_Tpa_Da_Tiaa_Proceeds_Amt", "#TPA-DA-TIAA-PROCEEDS-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Tpa_Fields_Pnd_Tpa_Grnted_Grd_Amt = pnd_Tpa_Fields.newFieldInGroup("pnd_Tpa_Fields_Pnd_Tpa_Grnted_Grd_Amt", "#TPA-GRNTED-GRD-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Tpa_Fields_Pnd_Tpa_Rtb_Amt = pnd_Tpa_Fields.newFieldInGroup("pnd_Tpa_Fields_Pnd_Tpa_Rtb_Amt", "#TPA-RTB-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Run_Date = localVariables.newFieldInRecord("pnd_Run_Date", "#RUN-DATE", FieldType.STRING, 8);
        pnd_Print_Date = localVariables.newFieldInRecord("pnd_Print_Date", "#PRINT-DATE", FieldType.STRING, 8);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 1);
        pnd_Apprvl = localVariables.newFieldArrayInRecord("pnd_Apprvl", "#APPRVL", FieldType.NUMERIC, 6, new DbsArrayController(1, 2));
        pnd_N_Apprvl = localVariables.newFieldInRecord("pnd_N_Apprvl", "#N-APPRVL", FieldType.NUMERIC, 6);

        pnd_Mdo_Fields = localVariables.newGroupInRecord("pnd_Mdo_Fields", "#MDO-FIELDS");
        pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units", "#MDO-REA-ANNTY-UNITS", 
            FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt", "#MDO-REA-ANNTY-AMT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt", "#MDO-TRADITIONAL-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt", "#MDO-TIAA-INT-PYMNT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Cref_Int_Pymnt = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Cref_Int_Pymnt", "#MDO-CREF-INT-PYMNT", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl", "#MDO-TIAA-EXCL", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Cref_Excl = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Cref_Excl", "#MDO-CREF-EXCL", FieldType.NUMERIC, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Mma = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Mma", "#MDO-MMA", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            2));
        pnd_Mdo_Fields_Pnd_Mdo_Stock = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Stock", "#MDO-STOCK", FieldType.NUMERIC, 11, 3, new 
            DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Social = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Social", "#MDO-SOCIAL", FieldType.NUMERIC, 11, 3, 
            new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Global = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Global", "#MDO-GLOBAL", FieldType.NUMERIC, 11, 3, 
            new DbsArrayController(1, 2));
        pnd_Mdo_Fields_Pnd_Mdo_Bond = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Bond", "#MDO-BOND", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            2));
        pnd_Mdo_Fields_Pnd_Mdo_Grth = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Grth", "#MDO-GRTH", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            2));
        pnd_Mdo_Fields_Pnd_Mdo_Equ = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Equ", "#MDO-EQU", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            2));
        pnd_Mdo_Fields_Pnd_Mdo_Ilb = pnd_Mdo_Fields.newFieldArrayInGroup("pnd_Mdo_Fields_Pnd_Mdo_Ilb", "#MDO-ILB", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 
            2));
        pnd_Set_Tot = localVariables.newFieldInRecord("pnd_Set_Tot", "#SET-TOT", FieldType.NUMERIC, 6);
        pnd_Todays_Business_Dt = localVariables.newFieldInRecord("pnd_Todays_Business_Dt", "#TODAYS-BUSINESS-DT", FieldType.NUMERIC, 8);

        pnd_Todays_Business_Dt__R_Field_1 = localVariables.newGroupInRecord("pnd_Todays_Business_Dt__R_Field_1", "REDEFINE", pnd_Todays_Business_Dt);
        pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A = pnd_Todays_Business_Dt__R_Field_1.newFieldInGroup("pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A", 
            "#TODAYS-BUSINESS-DT-A", FieldType.STRING, 8);

        pnd_Todays_Business_Dt__R_Field_2 = localVariables.newGroupInRecord("pnd_Todays_Business_Dt__R_Field_2", "REDEFINE", pnd_Todays_Business_Dt);
        pnd_Todays_Business_Dt_Pnd_Todays_Bus_Cc = pnd_Todays_Business_Dt__R_Field_2.newFieldInGroup("pnd_Todays_Business_Dt_Pnd_Todays_Bus_Cc", "#TODAYS-BUS-CC", 
            FieldType.NUMERIC, 2);
        pnd_Todays_Business_Dt_Pnd_Todays_Bus_Yy = pnd_Todays_Business_Dt__R_Field_2.newFieldInGroup("pnd_Todays_Business_Dt_Pnd_Todays_Bus_Yy", "#TODAYS-BUS-YY", 
            FieldType.NUMERIC, 2);
        pnd_Todays_Business_Dt_Pnd_Todays_Bus_Mm = pnd_Todays_Business_Dt__R_Field_2.newFieldInGroup("pnd_Todays_Business_Dt_Pnd_Todays_Bus_Mm", "#TODAYS-BUS-MM", 
            FieldType.NUMERIC, 2);
        pnd_Todays_Business_Dt_Pnd_Todays_Bus_Dd = pnd_Todays_Business_Dt__R_Field_2.newFieldInGroup("pnd_Todays_Business_Dt_Pnd_Todays_Bus_Dd", "#TODAYS-BUS-DD", 
            FieldType.NUMERIC, 2);
        pnd_Cis_Open_Status = localVariables.newFieldInRecord("pnd_Cis_Open_Status", "#CIS-OPEN-STATUS", FieldType.STRING, 2);
        pnd_Aprl_Y_N = localVariables.newFieldArrayInRecord("pnd_Aprl_Y_N", "#APRL-Y-N", FieldType.STRING, 20, new DbsArrayController(1, 2));
        pnd_Cis_Rqst_Id = localVariables.newFieldInRecord("pnd_Cis_Rqst_Id", "#CIS-RQST-ID", FieldType.STRING, 8);
        pnd_At_Start = localVariables.newFieldInRecord("pnd_At_Start", "#AT-START", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cis.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Cisb100() throws Exception
    {
        super("Cisb100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        getReports().atTopOfPage(atTopEventRpt6, 6);
        getReports().atTopOfPage(atTopEventRpt7, 7);
        setupReports();
        getReports().write(0, "START OF CISB100");                                                                                                                        //Natural: WRITE 'START OF CISB100'
        if (Global.isEscape()) return;
        //*  DEFINE PRINTERS AND FORMATS
        //*  DEFINE PRINTER(1)
        //*  SG=OFF                                                                                                                                                       //Natural: FORMAT ( 1 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 2 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 3 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 4 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 5 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 6 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 7 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF;//Natural: FORMAT ( 8 ) LS = 132 PS = 60 ZP = ON IS = OFF ES = OFF
        //*                                                                                                                                                               //Natural: FORMAT LS = 80 PS = 60 ZP = ON IS = OFF ES = OFF
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 4 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 5 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 6 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 7 )
        //* ***************************************************************
        pnd_Todays_Business_Dt.setValue(Global.getDATN());                                                                                                                //Natural: ASSIGN #TODAYS-BUSINESS-DT := *DATN
        //*  IPFS
        DbsUtil.callnat(Scin8888.class , getCurrentProcessState(), pnd_Todays_Business_Dt);                                                                               //Natural: CALLNAT 'SCIN8888' #TODAYS-BUSINESS-DT
        if (condition(Global.isEscape())) return;
        //*  BWN
        getReports().write(0, "=",pnd_Todays_Business_Dt);                                                                                                                //Natural: WRITE '=' #TODAYS-BUSINESS-DT
        if (Global.isEscape()) return;
        //*  IPFS
        if (condition(pnd_Todays_Business_Dt.equals(getZero())))                                                                                                          //Natural: IF #TODAYS-BUSINESS-DT = 0
        {
            //*  IPFS
            //*  IPFS
            //*  IPFS
            //*  IPFS
            getReports().write(0, "* * * * * * * * * * * * * * * * * * *",NEWLINE,"   TODAYS BUSINESS DATE IS INVALID",NEWLINE,"         FOR DATE OF",                    //Natural: WRITE '* * * * * * * * * * * * * * * * * * *' / '   TODAYS BUSINESS DATE IS INVALID' / '         FOR DATE OF' *DATN / '* * * * * * * * * * * * * * * * * * *'
                Global.getDATN(),NEWLINE,"* * * * * * * * * * * * * * * * * * *");
            if (Global.isEscape()) return;
            //*  IPFS
            DbsUtil.terminate(12);  if (true) return;                                                                                                                     //Natural: TERMINATE 12
            //*  IPFS
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ(1) CTRL BY CNTRL-SUPER-DE-1 = 'C000000'
        //*    IF CTRL.CNTRL-TODAYS-BUSINESS-DT = 0
        //*      WRITE '* * * * * * * * * * * * * * * * * * *' /
        //*        '   TODAYS BUSINESS DATE IS INVALID' /
        //*        '         FILE = 217 ' /
        //*        '         DBID = 008 ' /
        //*        '* * * * * * * * * * * * * * * * * * *'
        //*      TERMINATE 12
        //*    END-IF
        //*   IF #CNTRL-TODAYS-BUS-YY < 97
        //*     MOVE 20 TO #TODAYS-BUS-CC
        //*   ELSE
        //*     MOVE 19 TO #TODAYS-BUS-CC
        //*   END-IF
        //*   MOVE #CNTRL-TODAYS-BUS-YY TO #TODAYS-BUS-YY
        //*   MOVE #CNTRL-TODAYS-BUS-MM TO #TODAYS-BUS-MM
        //*   MOVE #CNTRL-TODAYS-BUS-DD TO #TODAYS-BUS-DD
        //*  END-READ
        //* ***************************************************************
        pnd_Cis_Open_Status.setValue("OI");                                                                                                                               //Natural: ASSIGN #CIS-OPEN-STATUS := 'OI'
        vw_cis.startDatabaseRead                                                                                                                                          //Natural: READ CIS BY CIS-OPEN-STATUS EQ #CIS-OPEN-STATUS
        (
        "READ01",
        new Wc[] { new Wc("CIS_OPEN_STATUS", ">=", pnd_Cis_Open_Status, WcType.BY) },
        new Oc[] { new Oc("CIS_OPEN_STATUS", "ASC") }
        );
        READ01:
        while (condition(vw_cis.readNextRow("READ01")))
        {
            //* *******************************************************************
            if (condition(cis_Cis_Status_Cd.notEquals("I") && cis_Cis_Opn_Clsd_Ind.notEquals("O")))                                                                       //Natural: IF CIS-STATUS-CD NE 'I' AND CIS-OPN-CLSD-IND NE 'O'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  *
            pnd_Run_Date.setValue(pnd_Todays_Business_Dt_Pnd_Todays_Business_Dt_A);                                                                                       //Natural: MOVE #TODAYS-BUSINESS-DT-A TO #RUN-DATE
            pnd_Print_Date.setValueEdited(cis_Cis_Cntrct_Print_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED CIS.CIS-CNTRCT-PRINT-DTE ( EM = YYYYMMDD ) TO #PRINT-DATE
            if (condition(pnd_Print_Date.greater(pnd_Run_Date) || pnd_Print_Date.equals(" ")))                                                                            //Natural: IF #PRINT-DATE GT #RUN-DATE OR #PRINT-DATE = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  *****************************************************888
            pnd_At_Start.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #AT-START
            if (condition(cis_Cis_Rqst_Id.equals("IA")))                                                                                                                  //Natural: IF CIS-RQST-ID = 'IA'
            {
                pnd_Record_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #RECORD-FOUND := TRUE
                if (condition(cis_Cis_Trnsf_Flag.equals("Y")))                                                                                                            //Natural: IF CIS-TRNSF-FLAG = 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM IA-TRANSFER-PROCESS
                    sub_Ia_Transfer_Process();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM IA-PROCESS
                    sub_Ia_Process();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Rqst_Id.equals("TPA")))                                                                                                                 //Natural: IF CIS-RQST-ID = 'TPA'
            {
                pnd_Record_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #RECORD-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM TPA-PROCESSING
                sub_Tpa_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Rqst_Id.equals("IPRO")))                                                                                                                //Natural: IF CIS-RQST-ID = 'IPRO'
            {
                pnd_Record_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #RECORD-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM IPRO-PROCESSING
                sub_Ipro_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  060706
            if (condition(DbsUtil.maskMatches(cis_Cis_Rqst_Id,"'MDO'")))                                                                                                  //Natural: IF CIS-RQST-ID = MASK ( 'MDO' )
            {
                pnd_Record_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #RECORD-FOUND := TRUE
                                                                                                                                                                          //Natural: PERFORM MDO-PROCESS-DAILY
                sub_Mdo_Process_Daily();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM MDO-PROCESS
                sub_Mdo_Process();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(cis_Cis_Annty_Option, cis_Cis_Tiaa_Doi, cis_Cis_Pin_Nbr, cis_Cis_Da_Tiaa_Nbr.getValue(1), cis_Cis_Da_Tiaa_Nbr.getValue(2),          //Natural: END-ALL
                cis_Cis_Da_Tiaa_Nbr.getValue(3), cis_Cis_Da_Tiaa_Nbr.getValue(4), cis_Cis_Da_Tiaa_Nbr.getValue(5), cis_Cis_Da_Tiaa_Nbr.getValue(6), cis_Cis_Da_Tiaa_Nbr.getValue(7), 
                cis_Cis_Da_Tiaa_Nbr.getValue(8), cis_Cis_Da_Tiaa_Nbr.getValue(9), cis_Cis_Da_Tiaa_Nbr.getValue(10), cis_Cis_Da_Tiaa_Nbr.getValue(11), cis_Cis_Da_Tiaa_Nbr.getValue(12), 
                cis_Cis_Da_Tiaa_Nbr.getValue(13), cis_Cis_Da_Tiaa_Nbr.getValue(14), cis_Cis_Da_Tiaa_Nbr.getValue(15), cis_Cis_Da_Tiaa_Nbr.getValue(16), 
                cis_Cis_Da_Tiaa_Nbr.getValue(17), cis_Cis_Da_Tiaa_Nbr.getValue(18), cis_Cis_Da_Tiaa_Nbr.getValue(19), cis_Cis_Da_Tiaa_Nbr.getValue(20), 
                cis_Cis_Rqst_Id, cis_Cis_Opn_Clsd_Ind, cis_Cis_Status_Cd, cis_Cis_Rqst_Id_Key, cis_Cis_Cntrct_Apprvl_Ind, cis_Cis_Tiaa_Nbr, cis_Cis_Cert_Nbr, 
                cis_Cis_Cref_Doi, cis_Cis_Cntrct_Print_Dte, cis_Cis_Cntrct_Type, cis_Cis_Pymnt_Mode, cis_Cis_Grnted_Grd_Amt, cis_Cis_Grnted_Std_Amt, cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(1), 
                cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(2), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(3), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(4), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(5), 
                cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(6), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(7), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(8), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(9), 
                cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(10), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(11), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(12), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(13), 
                cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(14), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(15), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(16), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(17), 
                cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(18), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(19), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(20), cis_Cis_Da_Rea_Proceeds_Amt.getValue(1), 
                cis_Cis_Da_Rea_Proceeds_Amt.getValue(2), cis_Cis_Da_Rea_Proceeds_Amt.getValue(3), cis_Cis_Da_Rea_Proceeds_Amt.getValue(4), cis_Cis_Da_Rea_Proceeds_Amt.getValue(5), 
                cis_Cis_Da_Rea_Proceeds_Amt.getValue(6), cis_Cis_Da_Rea_Proceeds_Amt.getValue(7), cis_Cis_Da_Rea_Proceeds_Amt.getValue(8), cis_Cis_Da_Rea_Proceeds_Amt.getValue(9), 
                cis_Cis_Da_Rea_Proceeds_Amt.getValue(10), cis_Cis_Da_Rea_Proceeds_Amt.getValue(11), cis_Cis_Da_Rea_Proceeds_Amt.getValue(12), cis_Cis_Da_Rea_Proceeds_Amt.getValue(13), 
                cis_Cis_Da_Rea_Proceeds_Amt.getValue(14), cis_Cis_Da_Rea_Proceeds_Amt.getValue(15), cis_Cis_Da_Rea_Proceeds_Amt.getValue(16), cis_Cis_Da_Rea_Proceeds_Amt.getValue(17), 
                cis_Cis_Da_Rea_Proceeds_Amt.getValue(18), cis_Cis_Da_Rea_Proceeds_Amt.getValue(19), cis_Cis_Da_Rea_Proceeds_Amt.getValue(20), cis_Cis_Da_Cref_Proceeds_Amt.getValue(1), 
                cis_Cis_Da_Cref_Proceeds_Amt.getValue(2), cis_Cis_Da_Cref_Proceeds_Amt.getValue(3), cis_Cis_Da_Cref_Proceeds_Amt.getValue(4), cis_Cis_Da_Cref_Proceeds_Amt.getValue(5), 
                cis_Cis_Da_Cref_Proceeds_Amt.getValue(6), cis_Cis_Da_Cref_Proceeds_Amt.getValue(7), cis_Cis_Da_Cref_Proceeds_Amt.getValue(8), cis_Cis_Da_Cref_Proceeds_Amt.getValue(9), 
                cis_Cis_Da_Cref_Proceeds_Amt.getValue(10), cis_Cis_Da_Cref_Proceeds_Amt.getValue(11), cis_Cis_Da_Cref_Proceeds_Amt.getValue(12), cis_Cis_Da_Cref_Proceeds_Amt.getValue(13), 
                cis_Cis_Da_Cref_Proceeds_Amt.getValue(14), cis_Cis_Da_Cref_Proceeds_Amt.getValue(15), cis_Cis_Da_Cref_Proceeds_Amt.getValue(16), cis_Cis_Da_Cref_Proceeds_Amt.getValue(17), 
                cis_Cis_Da_Cref_Proceeds_Amt.getValue(18), cis_Cis_Da_Cref_Proceeds_Amt.getValue(19), cis_Cis_Da_Cref_Proceeds_Amt.getValue(20), cis_Cis_Cref_Acct_Cde.getValue(1), 
                cis_Cis_Cref_Acct_Cde.getValue(2), cis_Cis_Cref_Acct_Cde.getValue(3), cis_Cis_Cref_Acct_Cde.getValue(4), cis_Cis_Cref_Acct_Cde.getValue(5), 
                cis_Cis_Cref_Acct_Cde.getValue(6), cis_Cis_Cref_Acct_Cde.getValue(7), cis_Cis_Cref_Acct_Cde.getValue(8), cis_Cis_Cref_Acct_Cde.getValue(9), 
                cis_Cis_Cref_Acct_Cde.getValue(10), cis_Cis_Cref_Acct_Cde.getValue(11), cis_Cis_Cref_Acct_Cde.getValue(12), cis_Cis_Cref_Acct_Cde.getValue(13), 
                cis_Cis_Cref_Acct_Cde.getValue(14), cis_Cis_Cref_Acct_Cde.getValue(15), cis_Cis_Cref_Acct_Cde.getValue(16), cis_Cis_Cref_Acct_Cde.getValue(17), 
                cis_Cis_Cref_Acct_Cde.getValue(18), cis_Cis_Cref_Acct_Cde.getValue(19), cis_Cis_Cref_Acct_Cde.getValue(20), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(1), 
                cis_Cis_Cref_Mnthly_Nbr_Units.getValue(2), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(3), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(4), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(5), 
                cis_Cis_Cref_Mnthly_Nbr_Units.getValue(6), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(7), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(8), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(9), 
                cis_Cis_Cref_Mnthly_Nbr_Units.getValue(10), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(11), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(12), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(13), 
                cis_Cis_Cref_Mnthly_Nbr_Units.getValue(14), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(15), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(16), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(17), 
                cis_Cis_Cref_Mnthly_Nbr_Units.getValue(18), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(19), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(20), cis_Cis_Cref_Annual_Nbr_Units.getValue(1), 
                cis_Cis_Cref_Annual_Nbr_Units.getValue(2), cis_Cis_Cref_Annual_Nbr_Units.getValue(3), cis_Cis_Cref_Annual_Nbr_Units.getValue(4), cis_Cis_Cref_Annual_Nbr_Units.getValue(5), 
                cis_Cis_Cref_Annual_Nbr_Units.getValue(6), cis_Cis_Cref_Annual_Nbr_Units.getValue(7), cis_Cis_Cref_Annual_Nbr_Units.getValue(8), cis_Cis_Cref_Annual_Nbr_Units.getValue(9), 
                cis_Cis_Cref_Annual_Nbr_Units.getValue(10), cis_Cis_Cref_Annual_Nbr_Units.getValue(11), cis_Cis_Cref_Annual_Nbr_Units.getValue(12), cis_Cis_Cref_Annual_Nbr_Units.getValue(13), 
                cis_Cis_Cref_Annual_Nbr_Units.getValue(14), cis_Cis_Cref_Annual_Nbr_Units.getValue(15), cis_Cis_Cref_Annual_Nbr_Units.getValue(16), cis_Cis_Cref_Annual_Nbr_Units.getValue(17), 
                cis_Cis_Cref_Annual_Nbr_Units.getValue(18), cis_Cis_Cref_Annual_Nbr_Units.getValue(19), cis_Cis_Cref_Annual_Nbr_Units.getValue(20), cis_Cis_Rea_Mnthly_Nbr_Units, 
                cis_Cis_Rea_Annual_Nbr_Units, cis_Cis_Rea_Annty_Amt, cis_Cis_Mdo_Contract_Cash_Status, cis_Cis_Mdo_Contract_Type, cis_Cis_Mdo_Traditional_Amt, 
                cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt, cis_Cis_Mdo_Cref_Int_Pymnt_Amt, cis_Cis_Mdo_Tiaa_Excluded_Amt, cis_Cis_Mdo_Cref_Excluded_Amt, cis_Cis_Rtb_Amt, 
                cis_Cis_Trnsf_Flag, cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(1), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(2), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(3), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(4), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(5), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(6), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(7), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(8), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(9), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(10), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(11), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(12), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(13), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(14), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(15), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(16), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(17), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(18), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(19), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(20), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(21), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(22), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(23), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(24), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(25), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(26), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(27), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(28), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(29), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(30), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(31), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(32), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(33), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(34), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(35), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(36), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(37), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(38), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(39), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(40), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(41), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(42), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(43), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(44), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(45), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(46), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(47), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(48), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(49), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(50), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(51), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(52), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(53), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(54), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(55), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(56), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(57), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(58), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(59), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(60), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(61), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(62), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(63), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(64), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(65), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(66), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(67), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(68), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(69), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(70), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(71), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(72), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(73), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(74), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(75), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(76), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(77), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(78), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(79), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(80), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(81), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(82), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(83), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(84), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(85), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(86), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(87), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(88), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(89), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(90), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(91), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(92), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(93), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(94), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(95), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(96), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(97), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(98), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(99), 
                cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(100), cis_Cis_Tacc_Annual_Nbr_Units.getValue(1), cis_Cis_Tacc_Annual_Nbr_Units.getValue(2), cis_Cis_Tacc_Annual_Nbr_Units.getValue(3), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(4), cis_Cis_Tacc_Annual_Nbr_Units.getValue(5), cis_Cis_Tacc_Annual_Nbr_Units.getValue(6), cis_Cis_Tacc_Annual_Nbr_Units.getValue(7), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(8), cis_Cis_Tacc_Annual_Nbr_Units.getValue(9), cis_Cis_Tacc_Annual_Nbr_Units.getValue(10), cis_Cis_Tacc_Annual_Nbr_Units.getValue(11), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(12), cis_Cis_Tacc_Annual_Nbr_Units.getValue(13), cis_Cis_Tacc_Annual_Nbr_Units.getValue(14), cis_Cis_Tacc_Annual_Nbr_Units.getValue(15), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(16), cis_Cis_Tacc_Annual_Nbr_Units.getValue(17), cis_Cis_Tacc_Annual_Nbr_Units.getValue(18), cis_Cis_Tacc_Annual_Nbr_Units.getValue(19), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(20), cis_Cis_Tacc_Annual_Nbr_Units.getValue(21), cis_Cis_Tacc_Annual_Nbr_Units.getValue(22), cis_Cis_Tacc_Annual_Nbr_Units.getValue(23), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(24), cis_Cis_Tacc_Annual_Nbr_Units.getValue(25), cis_Cis_Tacc_Annual_Nbr_Units.getValue(26), cis_Cis_Tacc_Annual_Nbr_Units.getValue(27), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(28), cis_Cis_Tacc_Annual_Nbr_Units.getValue(29), cis_Cis_Tacc_Annual_Nbr_Units.getValue(30), cis_Cis_Tacc_Annual_Nbr_Units.getValue(31), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(32), cis_Cis_Tacc_Annual_Nbr_Units.getValue(33), cis_Cis_Tacc_Annual_Nbr_Units.getValue(34), cis_Cis_Tacc_Annual_Nbr_Units.getValue(35), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(36), cis_Cis_Tacc_Annual_Nbr_Units.getValue(37), cis_Cis_Tacc_Annual_Nbr_Units.getValue(38), cis_Cis_Tacc_Annual_Nbr_Units.getValue(39), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(40), cis_Cis_Tacc_Annual_Nbr_Units.getValue(41), cis_Cis_Tacc_Annual_Nbr_Units.getValue(42), cis_Cis_Tacc_Annual_Nbr_Units.getValue(43), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(44), cis_Cis_Tacc_Annual_Nbr_Units.getValue(45), cis_Cis_Tacc_Annual_Nbr_Units.getValue(46), cis_Cis_Tacc_Annual_Nbr_Units.getValue(47), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(48), cis_Cis_Tacc_Annual_Nbr_Units.getValue(49), cis_Cis_Tacc_Annual_Nbr_Units.getValue(50), cis_Cis_Tacc_Annual_Nbr_Units.getValue(51), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(52), cis_Cis_Tacc_Annual_Nbr_Units.getValue(53), cis_Cis_Tacc_Annual_Nbr_Units.getValue(54), cis_Cis_Tacc_Annual_Nbr_Units.getValue(55), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(56), cis_Cis_Tacc_Annual_Nbr_Units.getValue(57), cis_Cis_Tacc_Annual_Nbr_Units.getValue(58), cis_Cis_Tacc_Annual_Nbr_Units.getValue(59), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(60), cis_Cis_Tacc_Annual_Nbr_Units.getValue(61), cis_Cis_Tacc_Annual_Nbr_Units.getValue(62), cis_Cis_Tacc_Annual_Nbr_Units.getValue(63), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(64), cis_Cis_Tacc_Annual_Nbr_Units.getValue(65), cis_Cis_Tacc_Annual_Nbr_Units.getValue(66), cis_Cis_Tacc_Annual_Nbr_Units.getValue(67), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(68), cis_Cis_Tacc_Annual_Nbr_Units.getValue(69), cis_Cis_Tacc_Annual_Nbr_Units.getValue(70), cis_Cis_Tacc_Annual_Nbr_Units.getValue(71), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(72), cis_Cis_Tacc_Annual_Nbr_Units.getValue(73), cis_Cis_Tacc_Annual_Nbr_Units.getValue(74), cis_Cis_Tacc_Annual_Nbr_Units.getValue(75), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(76), cis_Cis_Tacc_Annual_Nbr_Units.getValue(77), cis_Cis_Tacc_Annual_Nbr_Units.getValue(78), cis_Cis_Tacc_Annual_Nbr_Units.getValue(79), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(80), cis_Cis_Tacc_Annual_Nbr_Units.getValue(81), cis_Cis_Tacc_Annual_Nbr_Units.getValue(82), cis_Cis_Tacc_Annual_Nbr_Units.getValue(83), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(84), cis_Cis_Tacc_Annual_Nbr_Units.getValue(85), cis_Cis_Tacc_Annual_Nbr_Units.getValue(86), cis_Cis_Tacc_Annual_Nbr_Units.getValue(87), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(88), cis_Cis_Tacc_Annual_Nbr_Units.getValue(89), cis_Cis_Tacc_Annual_Nbr_Units.getValue(90), cis_Cis_Tacc_Annual_Nbr_Units.getValue(91), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(92), cis_Cis_Tacc_Annual_Nbr_Units.getValue(93), cis_Cis_Tacc_Annual_Nbr_Units.getValue(94), cis_Cis_Tacc_Annual_Nbr_Units.getValue(95), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(96), cis_Cis_Tacc_Annual_Nbr_Units.getValue(97), cis_Cis_Tacc_Annual_Nbr_Units.getValue(98), cis_Cis_Tacc_Annual_Nbr_Units.getValue(99), 
                cis_Cis_Tacc_Annual_Nbr_Units.getValue(100), cis_Cis_Tacc_Annty_Amt);
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getSort().sortData(cis_Cis_Annty_Option, cis_Cis_Tiaa_Doi);                                                                                                       //Natural: SORT CIS-ANNTY-OPTION CIS-TIAA-DOI USING CIS-PIN-NBR CIS-DA-TIAA-NBR ( 1:20 ) CIS-RQST-ID CIS-OPN-CLSD-IND CIS-STATUS-CD CIS-RQST-ID-KEY CIS-CNTRCT-APPRVL-IND CIS-TIAA-NBR CIS-CERT-NBR CIS-CREF-DOI CIS-CNTRCT-PRINT-DTE CIS-CNTRCT-TYPE CIS-PYMNT-MODE CIS-GRNTED-GRD-AMT CIS-GRNTED-STD-AMT CIS-DA-TIAA-PROCEEDS-AMT ( 1:20 ) CIS-DA-REA-PROCEEDS-AMT ( 1:20 ) CIS-DA-CREF-PROCEEDS-AMT ( 1:20 ) CIS-CREF-ACCT-CDE ( 1:20 ) CIS-CREF-MNTHLY-NBR-UNITS ( 1:20 ) CIS-CREF-ANNUAL-NBR-UNITS ( 1:20 ) CIS-REA-MNTHLY-NBR-UNITS CIS-REA-ANNUAL-NBR-UNITS CIS-REA-ANNTY-AMT CIS-MDO-CONTRACT-CASH-STATUS CIS-MDO-CONTRACT-TYPE CIS-MDO-TRADITIONAL-AMT CIS-MDO-TIAA-INT-PYMNT-AMT CIS-MDO-CREF-INT-PYMNT-AMT CIS-MDO-TIAA-EXCLUDED-AMT CIS-MDO-CREF-EXCLUDED-AMT CIS-RTB-AMT CIS-TRNSF-FLAG CIS-TACC-MNTHLY-NBR-UNITS ( 1:100 ) CIS-TACC-ANNUAL-NBR-UNITS ( 1:100 ) CIS-TACC-ANNTY-AMT
        SORT01:
        while (condition(getSort().readSortOutData(cis_Cis_Annty_Option, cis_Cis_Tiaa_Doi, cis_Cis_Pin_Nbr, cis_Cis_Da_Tiaa_Nbr.getValue(1), cis_Cis_Da_Tiaa_Nbr.getValue(2), 
            cis_Cis_Da_Tiaa_Nbr.getValue(3), cis_Cis_Da_Tiaa_Nbr.getValue(4), cis_Cis_Da_Tiaa_Nbr.getValue(5), cis_Cis_Da_Tiaa_Nbr.getValue(6), cis_Cis_Da_Tiaa_Nbr.getValue(7), 
            cis_Cis_Da_Tiaa_Nbr.getValue(8), cis_Cis_Da_Tiaa_Nbr.getValue(9), cis_Cis_Da_Tiaa_Nbr.getValue(10), cis_Cis_Da_Tiaa_Nbr.getValue(11), cis_Cis_Da_Tiaa_Nbr.getValue(12), 
            cis_Cis_Da_Tiaa_Nbr.getValue(13), cis_Cis_Da_Tiaa_Nbr.getValue(14), cis_Cis_Da_Tiaa_Nbr.getValue(15), cis_Cis_Da_Tiaa_Nbr.getValue(16), cis_Cis_Da_Tiaa_Nbr.getValue(17), 
            cis_Cis_Da_Tiaa_Nbr.getValue(18), cis_Cis_Da_Tiaa_Nbr.getValue(19), cis_Cis_Da_Tiaa_Nbr.getValue(20), cis_Cis_Rqst_Id, cis_Cis_Opn_Clsd_Ind, 
            cis_Cis_Status_Cd, cis_Cis_Rqst_Id_Key, cis_Cis_Cntrct_Apprvl_Ind, cis_Cis_Tiaa_Nbr, cis_Cis_Cert_Nbr, cis_Cis_Cref_Doi, cis_Cis_Cntrct_Print_Dte, 
            cis_Cis_Cntrct_Type, cis_Cis_Pymnt_Mode, cis_Cis_Grnted_Grd_Amt, cis_Cis_Grnted_Std_Amt, cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(1), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(2), 
            cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(3), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(4), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(5), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(6), 
            cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(7), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(8), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(9), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(10), 
            cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(11), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(12), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(13), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(14), 
            cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(15), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(16), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(17), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(18), 
            cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(19), cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(20), cis_Cis_Da_Rea_Proceeds_Amt.getValue(1), cis_Cis_Da_Rea_Proceeds_Amt.getValue(2), 
            cis_Cis_Da_Rea_Proceeds_Amt.getValue(3), cis_Cis_Da_Rea_Proceeds_Amt.getValue(4), cis_Cis_Da_Rea_Proceeds_Amt.getValue(5), cis_Cis_Da_Rea_Proceeds_Amt.getValue(6), 
            cis_Cis_Da_Rea_Proceeds_Amt.getValue(7), cis_Cis_Da_Rea_Proceeds_Amt.getValue(8), cis_Cis_Da_Rea_Proceeds_Amt.getValue(9), cis_Cis_Da_Rea_Proceeds_Amt.getValue(10), 
            cis_Cis_Da_Rea_Proceeds_Amt.getValue(11), cis_Cis_Da_Rea_Proceeds_Amt.getValue(12), cis_Cis_Da_Rea_Proceeds_Amt.getValue(13), cis_Cis_Da_Rea_Proceeds_Amt.getValue(14), 
            cis_Cis_Da_Rea_Proceeds_Amt.getValue(15), cis_Cis_Da_Rea_Proceeds_Amt.getValue(16), cis_Cis_Da_Rea_Proceeds_Amt.getValue(17), cis_Cis_Da_Rea_Proceeds_Amt.getValue(18), 
            cis_Cis_Da_Rea_Proceeds_Amt.getValue(19), cis_Cis_Da_Rea_Proceeds_Amt.getValue(20), cis_Cis_Da_Cref_Proceeds_Amt.getValue(1), cis_Cis_Da_Cref_Proceeds_Amt.getValue(2), 
            cis_Cis_Da_Cref_Proceeds_Amt.getValue(3), cis_Cis_Da_Cref_Proceeds_Amt.getValue(4), cis_Cis_Da_Cref_Proceeds_Amt.getValue(5), cis_Cis_Da_Cref_Proceeds_Amt.getValue(6), 
            cis_Cis_Da_Cref_Proceeds_Amt.getValue(7), cis_Cis_Da_Cref_Proceeds_Amt.getValue(8), cis_Cis_Da_Cref_Proceeds_Amt.getValue(9), cis_Cis_Da_Cref_Proceeds_Amt.getValue(10), 
            cis_Cis_Da_Cref_Proceeds_Amt.getValue(11), cis_Cis_Da_Cref_Proceeds_Amt.getValue(12), cis_Cis_Da_Cref_Proceeds_Amt.getValue(13), cis_Cis_Da_Cref_Proceeds_Amt.getValue(14), 
            cis_Cis_Da_Cref_Proceeds_Amt.getValue(15), cis_Cis_Da_Cref_Proceeds_Amt.getValue(16), cis_Cis_Da_Cref_Proceeds_Amt.getValue(17), cis_Cis_Da_Cref_Proceeds_Amt.getValue(18), 
            cis_Cis_Da_Cref_Proceeds_Amt.getValue(19), cis_Cis_Da_Cref_Proceeds_Amt.getValue(20), cis_Cis_Cref_Acct_Cde.getValue(1), cis_Cis_Cref_Acct_Cde.getValue(2), 
            cis_Cis_Cref_Acct_Cde.getValue(3), cis_Cis_Cref_Acct_Cde.getValue(4), cis_Cis_Cref_Acct_Cde.getValue(5), cis_Cis_Cref_Acct_Cde.getValue(6), 
            cis_Cis_Cref_Acct_Cde.getValue(7), cis_Cis_Cref_Acct_Cde.getValue(8), cis_Cis_Cref_Acct_Cde.getValue(9), cis_Cis_Cref_Acct_Cde.getValue(10), 
            cis_Cis_Cref_Acct_Cde.getValue(11), cis_Cis_Cref_Acct_Cde.getValue(12), cis_Cis_Cref_Acct_Cde.getValue(13), cis_Cis_Cref_Acct_Cde.getValue(14), 
            cis_Cis_Cref_Acct_Cde.getValue(15), cis_Cis_Cref_Acct_Cde.getValue(16), cis_Cis_Cref_Acct_Cde.getValue(17), cis_Cis_Cref_Acct_Cde.getValue(18), 
            cis_Cis_Cref_Acct_Cde.getValue(19), cis_Cis_Cref_Acct_Cde.getValue(20), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(1), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(2), 
            cis_Cis_Cref_Mnthly_Nbr_Units.getValue(3), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(4), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(5), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(6), 
            cis_Cis_Cref_Mnthly_Nbr_Units.getValue(7), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(8), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(9), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(10), 
            cis_Cis_Cref_Mnthly_Nbr_Units.getValue(11), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(12), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(13), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(14), 
            cis_Cis_Cref_Mnthly_Nbr_Units.getValue(15), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(16), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(17), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(18), 
            cis_Cis_Cref_Mnthly_Nbr_Units.getValue(19), cis_Cis_Cref_Mnthly_Nbr_Units.getValue(20), cis_Cis_Cref_Annual_Nbr_Units.getValue(1), cis_Cis_Cref_Annual_Nbr_Units.getValue(2), 
            cis_Cis_Cref_Annual_Nbr_Units.getValue(3), cis_Cis_Cref_Annual_Nbr_Units.getValue(4), cis_Cis_Cref_Annual_Nbr_Units.getValue(5), cis_Cis_Cref_Annual_Nbr_Units.getValue(6), 
            cis_Cis_Cref_Annual_Nbr_Units.getValue(7), cis_Cis_Cref_Annual_Nbr_Units.getValue(8), cis_Cis_Cref_Annual_Nbr_Units.getValue(9), cis_Cis_Cref_Annual_Nbr_Units.getValue(10), 
            cis_Cis_Cref_Annual_Nbr_Units.getValue(11), cis_Cis_Cref_Annual_Nbr_Units.getValue(12), cis_Cis_Cref_Annual_Nbr_Units.getValue(13), cis_Cis_Cref_Annual_Nbr_Units.getValue(14), 
            cis_Cis_Cref_Annual_Nbr_Units.getValue(15), cis_Cis_Cref_Annual_Nbr_Units.getValue(16), cis_Cis_Cref_Annual_Nbr_Units.getValue(17), cis_Cis_Cref_Annual_Nbr_Units.getValue(18), 
            cis_Cis_Cref_Annual_Nbr_Units.getValue(19), cis_Cis_Cref_Annual_Nbr_Units.getValue(20), cis_Cis_Rea_Mnthly_Nbr_Units, cis_Cis_Rea_Annual_Nbr_Units, 
            cis_Cis_Rea_Annty_Amt, cis_Cis_Mdo_Contract_Cash_Status, cis_Cis_Mdo_Contract_Type, cis_Cis_Mdo_Traditional_Amt, cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt, 
            cis_Cis_Mdo_Cref_Int_Pymnt_Amt, cis_Cis_Mdo_Tiaa_Excluded_Amt, cis_Cis_Mdo_Cref_Excluded_Amt, cis_Cis_Rtb_Amt, cis_Cis_Trnsf_Flag, cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(1), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(2), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(3), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(4), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(5), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(6), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(7), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(8), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(9), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(10), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(11), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(12), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(13), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(14), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(15), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(16), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(17), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(18), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(19), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(20), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(21), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(22), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(23), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(24), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(25), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(26), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(27), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(28), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(29), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(30), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(31), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(32), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(33), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(34), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(35), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(36), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(37), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(38), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(39), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(40), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(41), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(42), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(43), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(44), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(45), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(46), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(47), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(48), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(49), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(50), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(51), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(52), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(53), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(54), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(55), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(56), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(57), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(58), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(59), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(60), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(61), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(62), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(63), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(64), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(65), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(66), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(67), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(68), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(69), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(70), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(71), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(72), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(73), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(74), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(75), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(76), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(77), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(78), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(79), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(80), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(81), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(82), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(83), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(84), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(85), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(86), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(87), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(88), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(89), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(90), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(91), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(92), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(93), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(94), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(95), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(96), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(97), 
            cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(98), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(99), cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(100), cis_Cis_Tacc_Annual_Nbr_Units.getValue(1), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(2), cis_Cis_Tacc_Annual_Nbr_Units.getValue(3), cis_Cis_Tacc_Annual_Nbr_Units.getValue(4), cis_Cis_Tacc_Annual_Nbr_Units.getValue(5), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(6), cis_Cis_Tacc_Annual_Nbr_Units.getValue(7), cis_Cis_Tacc_Annual_Nbr_Units.getValue(8), cis_Cis_Tacc_Annual_Nbr_Units.getValue(9), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(10), cis_Cis_Tacc_Annual_Nbr_Units.getValue(11), cis_Cis_Tacc_Annual_Nbr_Units.getValue(12), cis_Cis_Tacc_Annual_Nbr_Units.getValue(13), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(14), cis_Cis_Tacc_Annual_Nbr_Units.getValue(15), cis_Cis_Tacc_Annual_Nbr_Units.getValue(16), cis_Cis_Tacc_Annual_Nbr_Units.getValue(17), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(18), cis_Cis_Tacc_Annual_Nbr_Units.getValue(19), cis_Cis_Tacc_Annual_Nbr_Units.getValue(20), cis_Cis_Tacc_Annual_Nbr_Units.getValue(21), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(22), cis_Cis_Tacc_Annual_Nbr_Units.getValue(23), cis_Cis_Tacc_Annual_Nbr_Units.getValue(24), cis_Cis_Tacc_Annual_Nbr_Units.getValue(25), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(26), cis_Cis_Tacc_Annual_Nbr_Units.getValue(27), cis_Cis_Tacc_Annual_Nbr_Units.getValue(28), cis_Cis_Tacc_Annual_Nbr_Units.getValue(29), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(30), cis_Cis_Tacc_Annual_Nbr_Units.getValue(31), cis_Cis_Tacc_Annual_Nbr_Units.getValue(32), cis_Cis_Tacc_Annual_Nbr_Units.getValue(33), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(34), cis_Cis_Tacc_Annual_Nbr_Units.getValue(35), cis_Cis_Tacc_Annual_Nbr_Units.getValue(36), cis_Cis_Tacc_Annual_Nbr_Units.getValue(37), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(38), cis_Cis_Tacc_Annual_Nbr_Units.getValue(39), cis_Cis_Tacc_Annual_Nbr_Units.getValue(40), cis_Cis_Tacc_Annual_Nbr_Units.getValue(41), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(42), cis_Cis_Tacc_Annual_Nbr_Units.getValue(43), cis_Cis_Tacc_Annual_Nbr_Units.getValue(44), cis_Cis_Tacc_Annual_Nbr_Units.getValue(45), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(46), cis_Cis_Tacc_Annual_Nbr_Units.getValue(47), cis_Cis_Tacc_Annual_Nbr_Units.getValue(48), cis_Cis_Tacc_Annual_Nbr_Units.getValue(49), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(50), cis_Cis_Tacc_Annual_Nbr_Units.getValue(51), cis_Cis_Tacc_Annual_Nbr_Units.getValue(52), cis_Cis_Tacc_Annual_Nbr_Units.getValue(53), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(54), cis_Cis_Tacc_Annual_Nbr_Units.getValue(55), cis_Cis_Tacc_Annual_Nbr_Units.getValue(56), cis_Cis_Tacc_Annual_Nbr_Units.getValue(57), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(58), cis_Cis_Tacc_Annual_Nbr_Units.getValue(59), cis_Cis_Tacc_Annual_Nbr_Units.getValue(60), cis_Cis_Tacc_Annual_Nbr_Units.getValue(61), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(62), cis_Cis_Tacc_Annual_Nbr_Units.getValue(63), cis_Cis_Tacc_Annual_Nbr_Units.getValue(64), cis_Cis_Tacc_Annual_Nbr_Units.getValue(65), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(66), cis_Cis_Tacc_Annual_Nbr_Units.getValue(67), cis_Cis_Tacc_Annual_Nbr_Units.getValue(68), cis_Cis_Tacc_Annual_Nbr_Units.getValue(69), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(70), cis_Cis_Tacc_Annual_Nbr_Units.getValue(71), cis_Cis_Tacc_Annual_Nbr_Units.getValue(72), cis_Cis_Tacc_Annual_Nbr_Units.getValue(73), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(74), cis_Cis_Tacc_Annual_Nbr_Units.getValue(75), cis_Cis_Tacc_Annual_Nbr_Units.getValue(76), cis_Cis_Tacc_Annual_Nbr_Units.getValue(77), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(78), cis_Cis_Tacc_Annual_Nbr_Units.getValue(79), cis_Cis_Tacc_Annual_Nbr_Units.getValue(80), cis_Cis_Tacc_Annual_Nbr_Units.getValue(81), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(82), cis_Cis_Tacc_Annual_Nbr_Units.getValue(83), cis_Cis_Tacc_Annual_Nbr_Units.getValue(84), cis_Cis_Tacc_Annual_Nbr_Units.getValue(85), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(86), cis_Cis_Tacc_Annual_Nbr_Units.getValue(87), cis_Cis_Tacc_Annual_Nbr_Units.getValue(88), cis_Cis_Tacc_Annual_Nbr_Units.getValue(89), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(90), cis_Cis_Tacc_Annual_Nbr_Units.getValue(91), cis_Cis_Tacc_Annual_Nbr_Units.getValue(92), cis_Cis_Tacc_Annual_Nbr_Units.getValue(93), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(94), cis_Cis_Tacc_Annual_Nbr_Units.getValue(95), cis_Cis_Tacc_Annual_Nbr_Units.getValue(96), cis_Cis_Tacc_Annual_Nbr_Units.getValue(97), 
            cis_Cis_Tacc_Annual_Nbr_Units.getValue(98), cis_Cis_Tacc_Annual_Nbr_Units.getValue(99), cis_Cis_Tacc_Annual_Nbr_Units.getValue(100), cis_Cis_Tacc_Annty_Amt)))
        {
                                                                                                                                                                          //Natural: PERFORM S700-PRINT-ARRAY
            sub_S700_Print_Array();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
                                                                                                                                                                          //Natural: PERFORM ALL-MODES
        sub_All_Modes();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM IA-REPORT
        sub_Ia_Report();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM IA-DAILY
        sub_Ia_Daily();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MDO-REPORT
        sub_Mdo_Report();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MDO-DAILY
        sub_Mdo_Daily();
        if (condition(Global.isEscape())) {return;}
        //*  PERFORM TPA-REPORT
                                                                                                                                                                          //Natural: PERFORM ALL-MODES-T
        sub_All_Modes_T();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM IA-REPORT-T
        sub_Ia_Report_T();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM IA-DAILY-T
        sub_Ia_Daily_T();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-PROCESS
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TPA-PROCESSING
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IPRO-PROCESSING
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-TRANSFER-PROCESS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ALL-MODES
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ALL-MODES-T
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRNTED-AMT
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRNTED-AMT-IPRO
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRNTED-AMT-TPA
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRNTED-AMT-T
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALUE-CODE
        //* *********************************************************************
        //* *
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALUE-CODE-T
        //* *********************************************************************
        //* *
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DA-PROCEED
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DA-PROCEED-TPA
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DA-PROCEED-IPRO
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DA-PROCEED-T
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ISSUED-TOTALS
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ISSUED-TOTALS-TPA
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ISSUED-TOTALS-IPRO
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ISSUED-TOTALS-T
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ISSUED-TOTALS-T-TPA
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ISSUED-TOTALS-T-IPRO
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-REPORT
        //* *SKIP(1) 1
        //*  TPA TOTALS
        //*  IPRO TOTALS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-REPORT-T
        //* **SKIP(6) 1
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-DAILY
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-DAILY-T
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDO-PROCESS-DAILY
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDO-PROCESS
        //*  IF CIS.CIS-PYMNT-MODE = 'A'
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDO-REPORT
        //* *************************************************
        //*  #SET-TOT := #Y-APPRVL + #N-APPRVL
        //*   1T 'Unapproval Contract/Certificate Sets  ' 45X #N-APPRVL /
        //*   1T '                                      ' 45X ' ----------' /
        //*   1T '       TOTAL:                         ' 45X #SET-TOT //
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MDO-DAILY
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TPA-PROCESS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TPA-REPORT
        //* *********************************************************************
        //* ***********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S700-PRINT-ARRAY
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 8 ) TITLE LEFT 30T ' DETAIL LISTING OF ALL CONTRACTS AND AMOUNTS ' // 1T 'OPTION' 12T 'TIAA NUMBER' 25T 'ID' 29T 'MONTHLY UNITS' 44T 'ANNUAL UNITS' 59T 'TIAA AMOUNT' 74T 'CREF AMOUNT' 89T 'REA AMOUNT' 104T 'ACCESS AMT'
    }
    private void sub_Ia_Process() throws Exception                                                                                                                        //Natural: IA-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        pnd_Ia_Rq_Pr.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #IA-RQ-PR
        //*   LIFE ANNUITY
        short decideConditionsMet552 = 0;                                                                                                                                 //Natural: DECIDE ON EVERY VALUE OF CIS-ANNTY-OPTION;//Natural: VALUE 'OL'
        if (condition(cis_Cis_Annty_Option.equals("OL")))
        {
            decideConditionsMet552++;
            //*   INSTALLMENT REFUND
            pnd_N_Rec.getValue(1).nadd(1);                                                                                                                                //Natural: ADD 1 TO #N-REC ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'IR'
        if (condition(cis_Cis_Annty_Option.equals("IR")))
        {
            decideConditionsMet552++;
            //*   ANNUITY CERTAIN
            pnd_N_Rec.getValue(2).nadd(1);                                                                                                                                //Natural: ADD 1 TO #N-REC ( 2 )
        }                                                                                                                                                                 //Natural: VALUE 'AC'
        if (condition(cis_Cis_Annty_Option.equals("AC")))
        {
            decideConditionsMet552++;
            //*   FULL BENEFIT
            pnd_N_Rec.getValue(3).nadd(1);                                                                                                                                //Natural: ADD 1 TO #N-REC ( 3 )
        }                                                                                                                                                                 //Natural: VALUE 'LSF'
        if (condition(cis_Cis_Annty_Option.equals("LSF")))
        {
            decideConditionsMet552++;
            //*   HALF BENEFIT
            pnd_N_Rec.getValue(4).nadd(1);                                                                                                                                //Natural: ADD 1 TO #N-REC ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 'LS'
        if (condition(cis_Cis_Annty_Option.equals("LS")))
        {
            decideConditionsMet552++;
            //*   2/3 BENEFIT
            pnd_N_Rec.getValue(5).nadd(1);                                                                                                                                //Natural: ADD 1 TO #N-REC ( 5 )
        }                                                                                                                                                                 //Natural: VALUE 'JS'
        if (condition(cis_Cis_Annty_Option.equals("JS")))
        {
            decideConditionsMet552++;
            //*   3/4 BENEFIT             /* TACC KG
            pnd_N_Rec.getValue(6).nadd(1);                                                                                                                                //Natural: ADD 1 TO #N-REC ( 6 )
        }                                                                                                                                                                 //Natural: VALUE 'LST'
        if (condition(cis_Cis_Annty_Option.equals("LST")))
        {
            decideConditionsMet552++;
            //*  TACC KG
            pnd_N_Rec.getValue(9).nadd(1);                                                                                                                                //Natural: ADD 1 TO #N-REC ( 9 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        if (condition(decideConditionsMet552 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  MONTHLY
        short decideConditionsMet579 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CIS.CIS-PYMNT-MODE;//Natural: VALUE 'M'
        if (condition((cis_Cis_Pymnt_Mode.equals("M"))))
        {
            decideConditionsMet579++;
            pnd_P.setValue(1);                                                                                                                                            //Natural: ASSIGN #P := 1
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT
            sub_Grnted_Amt();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM VALUE-CODE
            sub_Value_Code();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED
            sub_Da_Proceed();
            if (condition(Global.isEscape())) {return;}
            //*  QUARTERLY
            pnd_No_Rec.getValue(1).nadd(1);                                                                                                                               //Natural: ADD 1 TO #NO-REC ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'Q'
        else if (condition((cis_Cis_Pymnt_Mode.equals("Q"))))
        {
            decideConditionsMet579++;
            pnd_P.setValue(2);                                                                                                                                            //Natural: ASSIGN #P := 2
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT
            sub_Grnted_Amt();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM VALUE-CODE
            sub_Value_Code();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED
            sub_Da_Proceed();
            if (condition(Global.isEscape())) {return;}
            //*  SEMI-ANNUAL
            pnd_No_Rec.getValue(2).nadd(1);                                                                                                                               //Natural: ADD 1 TO #NO-REC ( 2 )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((cis_Cis_Pymnt_Mode.equals("S"))))
        {
            decideConditionsMet579++;
            pnd_P.setValue(3);                                                                                                                                            //Natural: ASSIGN #P := 3
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT
            sub_Grnted_Amt();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM VALUE-CODE
            sub_Value_Code();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED
            sub_Da_Proceed();
            if (condition(Global.isEscape())) {return;}
            //*  ANNUAL
            pnd_No_Rec.getValue(3).nadd(1);                                                                                                                               //Natural: ADD 1 TO #NO-REC ( 3 )
        }                                                                                                                                                                 //Natural: VALUE 'A'
        else if (condition((cis_Cis_Pymnt_Mode.equals("A"))))
        {
            decideConditionsMet579++;
            pnd_P.setValue(4);                                                                                                                                            //Natural: ASSIGN #P := 4
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT
            sub_Grnted_Amt();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM VALUE-CODE
            sub_Value_Code();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED
            sub_Da_Proceed();
            if (condition(Global.isEscape())) {return;}
            pnd_No_Rec.getValue(4).nadd(1);                                                                                                                               //Natural: ADD 1 TO #NO-REC ( 4 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Tpa_Processing() throws Exception                                                                                                                    //Natural: TPA-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        pnd_N_Rec.getValue(7).nadd(1);                                                                                                                                    //Natural: ADD 1 TO #N-REC ( 7 )
        //*  MONTHLY
        short decideConditionsMet618 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CIS.CIS-PYMNT-MODE;//Natural: VALUE 'M'
        if (condition((cis_Cis_Pymnt_Mode.equals("M"))))
        {
            decideConditionsMet618++;
            pnd_P.setValue(1);                                                                                                                                            //Natural: ASSIGN #P := 1
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-TPA
            sub_Grnted_Amt_Tpa();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-TPA
            sub_Da_Proceed_Tpa();
            if (condition(Global.isEscape())) {return;}
            //*  QUARTERLY
            pnd_No_Rec_Tpa.getValue(1).nadd(1);                                                                                                                           //Natural: ADD 1 TO #NO-REC-TPA ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'Q'
        else if (condition((cis_Cis_Pymnt_Mode.equals("Q"))))
        {
            decideConditionsMet618++;
            pnd_P.setValue(2);                                                                                                                                            //Natural: ASSIGN #P := 2
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-TPA
            sub_Grnted_Amt_Tpa();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-TPA
            sub_Da_Proceed_Tpa();
            if (condition(Global.isEscape())) {return;}
            //*  SEMI-ANNUAL
            pnd_No_Rec_Tpa.getValue(2).nadd(1);                                                                                                                           //Natural: ADD 1 TO #NO-REC-TPA ( 2 )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((cis_Cis_Pymnt_Mode.equals("S"))))
        {
            decideConditionsMet618++;
            pnd_P.setValue(3);                                                                                                                                            //Natural: ASSIGN #P := 3
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-TPA
            sub_Grnted_Amt_Tpa();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-TPA
            sub_Da_Proceed_Tpa();
            if (condition(Global.isEscape())) {return;}
            //*  ANNUAL
            pnd_No_Rec_Tpa.getValue(3).nadd(1);                                                                                                                           //Natural: ADD 1 TO #NO-REC-TPA ( 3 )
        }                                                                                                                                                                 //Natural: VALUE 'A'
        else if (condition((cis_Cis_Pymnt_Mode.equals("A"))))
        {
            decideConditionsMet618++;
            pnd_P.setValue(4);                                                                                                                                            //Natural: ASSIGN #P := 4
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-TPA
            sub_Grnted_Amt_Tpa();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-TPA
            sub_Da_Proceed_Tpa();
            if (condition(Global.isEscape())) {return;}
            pnd_No_Rec_Tpa.getValue(4).nadd(1);                                                                                                                           //Natural: ADD 1 TO #NO-REC-TPA ( 4 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Ipro_Processing() throws Exception                                                                                                                   //Natural: IPRO-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        pnd_N_Rec.getValue(8).nadd(1);                                                                                                                                    //Natural: ADD 1 TO #N-REC ( 8 )
        //*  MONTHLY
        short decideConditionsMet654 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CIS.CIS-PYMNT-MODE;//Natural: VALUE 'M'
        if (condition((cis_Cis_Pymnt_Mode.equals("M"))))
        {
            decideConditionsMet654++;
            pnd_P.setValue(1);                                                                                                                                            //Natural: ASSIGN #P := 1
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-IPRO
            sub_Grnted_Amt_Ipro();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-IPRO
            sub_Da_Proceed_Ipro();
            if (condition(Global.isEscape())) {return;}
            //*  QUARTERLY
            pnd_No_Rec_Ipro.getValue(1).nadd(1);                                                                                                                          //Natural: ADD 1 TO #NO-REC-IPRO ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'Q'
        else if (condition((cis_Cis_Pymnt_Mode.equals("Q"))))
        {
            decideConditionsMet654++;
            pnd_P.setValue(2);                                                                                                                                            //Natural: ASSIGN #P := 2
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-IPRO
            sub_Grnted_Amt_Ipro();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-IPRO
            sub_Da_Proceed_Ipro();
            if (condition(Global.isEscape())) {return;}
            //*  SEMI-ANNUAL
            pnd_No_Rec_Ipro.getValue(2).nadd(1);                                                                                                                          //Natural: ADD 1 TO #NO-REC-IPRO ( 2 )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((cis_Cis_Pymnt_Mode.equals("S"))))
        {
            decideConditionsMet654++;
            pnd_P.setValue(3);                                                                                                                                            //Natural: ASSIGN #P := 3
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-IPRO
            sub_Grnted_Amt_Ipro();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-IPRO
            sub_Da_Proceed_Ipro();
            if (condition(Global.isEscape())) {return;}
            //*  ANNUAL
            pnd_No_Rec_Ipro.getValue(3).nadd(1);                                                                                                                          //Natural: ADD 1 TO #NO-REC-IPRO ( 3 )
        }                                                                                                                                                                 //Natural: VALUE 'A'
        else if (condition((cis_Cis_Pymnt_Mode.equals("A"))))
        {
            decideConditionsMet654++;
            pnd_P.setValue(4);                                                                                                                                            //Natural: ASSIGN #P := 4
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-IPRO
            sub_Grnted_Amt_Ipro();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-IPRO
            sub_Da_Proceed_Ipro();
            if (condition(Global.isEscape())) {return;}
            pnd_No_Rec_Ipro.getValue(4).nadd(1);                                                                                                                          //Natural: ADD 1 TO #NO-REC-IPRO ( 4 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Ia_Transfer_Process() throws Exception                                                                                                               //Natural: IA-TRANSFER-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************
        pnd_Ia_Rq_Pr_T.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #IA-RQ-PR-T
        //*   LIFE ANNUITY
        short decideConditionsMet688 = 0;                                                                                                                                 //Natural: DECIDE ON EVERY VALUE OF CIS.CIS-ANNTY-OPTION;//Natural: VALUE 'OL'
        if (condition(cis_Cis_Annty_Option.equals("OL")))
        {
            decideConditionsMet688++;
            //*   INSTALLMENT REFUND
            pnd_T_Rec.getValue(1).nadd(1);                                                                                                                                //Natural: ADD 1 TO #T-REC ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'IR'
        if (condition(cis_Cis_Annty_Option.equals("IR")))
        {
            decideConditionsMet688++;
            //*   ANNUITY CERTAIN
            pnd_T_Rec.getValue(2).nadd(1);                                                                                                                                //Natural: ADD 1 TO #T-REC ( 2 )
        }                                                                                                                                                                 //Natural: VALUE 'AC'
        if (condition(cis_Cis_Annty_Option.equals("AC")))
        {
            decideConditionsMet688++;
            //*   FULL BENEFIT
            pnd_T_Rec.getValue(3).nadd(1);                                                                                                                                //Natural: ADD 1 TO #T-REC ( 3 )
        }                                                                                                                                                                 //Natural: VALUE 'LSF'
        if (condition(cis_Cis_Annty_Option.equals("LSF")))
        {
            decideConditionsMet688++;
            //*   HALF BENEFIT
            pnd_T_Rec.getValue(4).nadd(1);                                                                                                                                //Natural: ADD 1 TO #T-REC ( 4 )
        }                                                                                                                                                                 //Natural: VALUE 'LS'
        if (condition(cis_Cis_Annty_Option.equals("LS")))
        {
            decideConditionsMet688++;
            //*   2/3 BENEFIT
            pnd_T_Rec.getValue(5).nadd(1);                                                                                                                                //Natural: ADD 1 TO #T-REC ( 5 )
        }                                                                                                                                                                 //Natural: VALUE 'JS'
        if (condition(cis_Cis_Annty_Option.equals("JS")))
        {
            decideConditionsMet688++;
            //*   3/4 BENEFIT           /* TACC KG
            pnd_T_Rec.getValue(6).nadd(1);                                                                                                                                //Natural: ADD 1 TO #T-REC ( 6 )
        }                                                                                                                                                                 //Natural: VALUE 'LST'
        if (condition(cis_Cis_Annty_Option.equals("LST")))
        {
            decideConditionsMet688++;
            pnd_T_Rec.getValue(9).nadd(1);                                                                                                                                //Natural: ADD 1 TO #T-REC ( 9 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        if (condition(decideConditionsMet688 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  MONTHLY
        short decideConditionsMet714 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CIS.CIS-PYMNT-MODE;//Natural: VALUE 'M'
        if (condition((cis_Cis_Pymnt_Mode.equals("M"))))
        {
            decideConditionsMet714++;
            pnd_P.setValue(1);                                                                                                                                            //Natural: ASSIGN #P := 1
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-T
            sub_Grnted_Amt_T();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM VALUE-CODE-T
            sub_Value_Code_T();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-T
            sub_Da_Proceed_T();
            if (condition(Global.isEscape())) {return;}
            //*  QUARTERLY
            pnd_No_Rec_T.getValue(1).nadd(1);                                                                                                                             //Natural: ADD 1 TO #NO-REC-T ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'Q'
        else if (condition((cis_Cis_Pymnt_Mode.equals("Q"))))
        {
            decideConditionsMet714++;
            pnd_P.setValue(2);                                                                                                                                            //Natural: ASSIGN #P := 2
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-T
            sub_Grnted_Amt_T();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM VALUE-CODE-T
            sub_Value_Code_T();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-T
            sub_Da_Proceed_T();
            if (condition(Global.isEscape())) {return;}
            //*  SEMI-ANNUAL
            pnd_No_Rec_T.getValue(2).nadd(1);                                                                                                                             //Natural: ADD 1 TO #NO-REC-T ( 2 )
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((cis_Cis_Pymnt_Mode.equals("S"))))
        {
            decideConditionsMet714++;
            pnd_P.setValue(3);                                                                                                                                            //Natural: ASSIGN #P := 3
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-T
            sub_Grnted_Amt_T();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM VALUE-CODE-T
            sub_Value_Code_T();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-T
            sub_Da_Proceed_T();
            if (condition(Global.isEscape())) {return;}
            //*  ANNUAL
            pnd_No_Rec_T.getValue(3).nadd(1);                                                                                                                             //Natural: ADD 1 TO #NO-REC-T ( 3 )
        }                                                                                                                                                                 //Natural: VALUE 'A'
        else if (condition((cis_Cis_Pymnt_Mode.equals("A"))))
        {
            decideConditionsMet714++;
            pnd_P.setValue(4);                                                                                                                                            //Natural: ASSIGN #P := 4
                                                                                                                                                                          //Natural: PERFORM GRNTED-AMT-T
            sub_Grnted_Amt_T();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM VALUE-CODE-T
            sub_Value_Code_T();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DA-PROCEED-T
            sub_Da_Proceed_T();
            if (condition(Global.isEscape())) {return;}
            pnd_No_Rec_T.getValue(4).nadd(1);                                                                                                                             //Natural: ADD 1 TO #NO-REC-T ( 4 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  TACC KG
    private void sub_All_Modes() throws Exception                                                                                                                         //Natural: ALL-MODES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Grnted_Grd_Amt_Tot.compute(new ComputeParameters(false, pnd_Grnted_Grd_Amt_Tot), pnd_Grnted_Grd_Amt.getValue(1).add(pnd_Grnted_Grd_Amt.getValue(2)).add(pnd_Grnted_Grd_Amt.getValue(3)).add(pnd_Grnted_Grd_Amt.getValue(4))); //Natural: ASSIGN #GRNTED-GRD-AMT-TOT := #GRNTED-GRD-AMT ( 1 ) + #GRNTED-GRD-AMT ( 2 ) + #GRNTED-GRD-AMT ( 3 ) + #GRNTED-GRD-AMT ( 4 )
        pnd_Grnted_Std_Amt_Tot.compute(new ComputeParameters(false, pnd_Grnted_Std_Amt_Tot), pnd_Grnted_Std_Amt.getValue(1).add(pnd_Grnted_Std_Amt.getValue(2)).add(pnd_Grnted_Std_Amt.getValue(3)).add(pnd_Grnted_Std_Amt.getValue(4))); //Natural: ASSIGN #GRNTED-STD-AMT-TOT := #GRNTED-STD-AMT ( 1 ) + #GRNTED-STD-AMT ( 2 ) + #GRNTED-STD-AMT ( 3 ) + #GRNTED-STD-AMT ( 4 )
        pnd_Grnted_Grd_Amt_Tot_Tpa.compute(new ComputeParameters(false, pnd_Grnted_Grd_Amt_Tot_Tpa), pnd_Grnted_Grd_Amt_Tpa.getValue(1).add(pnd_Grnted_Grd_Amt_Tpa.getValue(2)).add(pnd_Grnted_Grd_Amt_Tpa.getValue(3)).add(pnd_Grnted_Grd_Amt_Tpa.getValue(4))); //Natural: ASSIGN #GRNTED-GRD-AMT-TOT-TPA := #GRNTED-GRD-AMT-TPA ( 1 ) + #GRNTED-GRD-AMT-TPA ( 2 ) + #GRNTED-GRD-AMT-TPA ( 3 ) + #GRNTED-GRD-AMT-TPA ( 4 )
        pnd_Grnted_Std_Amt_Tot_Tpa.compute(new ComputeParameters(false, pnd_Grnted_Std_Amt_Tot_Tpa), pnd_Grnted_Std_Amt_Tpa.getValue(1).add(pnd_Grnted_Std_Amt_Tpa.getValue(2)).add(pnd_Grnted_Std_Amt_Tpa.getValue(3)).add(pnd_Grnted_Std_Amt_Tpa.getValue(4))); //Natural: ASSIGN #GRNTED-STD-AMT-TOT-TPA := #GRNTED-STD-AMT-TPA ( 1 ) + #GRNTED-STD-AMT-TPA ( 2 ) + #GRNTED-STD-AMT-TPA ( 3 ) + #GRNTED-STD-AMT-TPA ( 4 )
        pnd_Grnted_Grd_Amt_Tot_Ipro.compute(new ComputeParameters(false, pnd_Grnted_Grd_Amt_Tot_Ipro), pnd_Grnted_Grd_Amt_Ipro.getValue(1).add(pnd_Grnted_Grd_Amt_Ipro.getValue(2)).add(pnd_Grnted_Grd_Amt_Ipro.getValue(3)).add(pnd_Grnted_Grd_Amt_Ipro.getValue(4))); //Natural: ASSIGN #GRNTED-GRD-AMT-TOT-IPRO := #GRNTED-GRD-AMT-IPRO ( 1 ) + #GRNTED-GRD-AMT-IPRO ( 2 ) + #GRNTED-GRD-AMT-IPRO ( 3 ) + #GRNTED-GRD-AMT-IPRO ( 4 )
        pnd_Grnted_Std_Amt_Tot_Ipro.compute(new ComputeParameters(false, pnd_Grnted_Std_Amt_Tot_Ipro), pnd_Grnted_Std_Amt_Ipro.getValue(1).add(pnd_Grnted_Std_Amt_Ipro.getValue(2)).add(pnd_Grnted_Std_Amt_Ipro.getValue(3)).add(pnd_Grnted_Std_Amt_Ipro.getValue(4))); //Natural: ASSIGN #GRNTED-STD-AMT-TOT-IPRO := #GRNTED-STD-AMT-IPRO ( 1 ) + #GRNTED-STD-AMT-IPRO ( 2 ) + #GRNTED-STD-AMT-IPRO ( 3 ) + #GRNTED-STD-AMT-IPRO ( 4 )
        pnd_Real_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_Real_Annuity.getValue(5)), pnd_Real_Annuity.getValue(1).add(pnd_Real_Annuity.getValue(2)).add(pnd_Real_Annuity.getValue(3)).add(pnd_Real_Annuity.getValue(4))); //Natural: ASSIGN #REAL-ANNUITY ( 5 ) := #REAL-ANNUITY ( 1 ) + #REAL-ANNUITY ( 2 ) + #REAL-ANNUITY ( 3 ) + #REAL-ANNUITY ( 4 )
        pnd_Cref_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_Cref_Annuity.getValue(5)), pnd_Cref_Annuity.getValue(1).add(pnd_Cref_Annuity.getValue(2)).add(pnd_Cref_Annuity.getValue(3)).add(pnd_Cref_Annuity.getValue(4))); //Natural: ASSIGN #CREF-ANNUITY ( 5 ) := #CREF-ANNUITY ( 1 ) + #CREF-ANNUITY ( 2 ) + #CREF-ANNUITY ( 3 ) + #CREF-ANNUITY ( 4 )
        pnd_Trad_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_Trad_Annuity.getValue(5)), pnd_Trad_Annuity.getValue(1).add(pnd_Trad_Annuity.getValue(2)).add(pnd_Trad_Annuity.getValue(3)).add(pnd_Trad_Annuity.getValue(4))); //Natural: ASSIGN #TRAD-ANNUITY ( 5 ) := #TRAD-ANNUITY ( 1 ) + #TRAD-ANNUITY ( 2 ) + #TRAD-ANNUITY ( 3 ) + #TRAD-ANNUITY ( 4 )
        pnd_Trad_Annuity_Tpa.getValue(5).compute(new ComputeParameters(false, pnd_Trad_Annuity_Tpa.getValue(5)), pnd_Trad_Annuity_Tpa.getValue(1).add(pnd_Trad_Annuity_Tpa.getValue(2)).add(pnd_Trad_Annuity_Tpa.getValue(3)).add(pnd_Trad_Annuity_Tpa.getValue(4))); //Natural: ASSIGN #TRAD-ANNUITY-TPA ( 5 ) := #TRAD-ANNUITY-TPA ( 1 ) + #TRAD-ANNUITY-TPA ( 2 ) + #TRAD-ANNUITY-TPA ( 3 ) + #TRAD-ANNUITY-TPA ( 4 )
        pnd_Trad_Annuity_Ipro.getValue(5).compute(new ComputeParameters(false, pnd_Trad_Annuity_Ipro.getValue(5)), pnd_Trad_Annuity_Ipro.getValue(1).add(pnd_Trad_Annuity_Ipro.getValue(2)).add(pnd_Trad_Annuity_Ipro.getValue(3)).add(pnd_Trad_Annuity_Ipro.getValue(4))); //Natural: ASSIGN #TRAD-ANNUITY-IPRO ( 5 ) := #TRAD-ANNUITY-IPRO ( 1 ) + #TRAD-ANNUITY-IPRO ( 2 ) + #TRAD-ANNUITY-IPRO ( 3 ) + #TRAD-ANNUITY-IPRO ( 4 )
        pnd_Tacc_Annuity.getValue(5).compute(new ComputeParameters(false, pnd_Tacc_Annuity.getValue(5)), pnd_Tacc_Annuity.getValue(1).add(pnd_Tacc_Annuity.getValue(2)).add(pnd_Tacc_Annuity.getValue(3)).add(pnd_Tacc_Annuity.getValue(4))); //Natural: ASSIGN #TACC-ANNUITY ( 5 ) := #TACC-ANNUITY ( 1 ) + #TACC-ANNUITY ( 2 ) + #TACC-ANNUITY ( 3 ) + #TACC-ANNUITY ( 4 )
        pnd_No_Rec.getValue(5).compute(new ComputeParameters(false, pnd_No_Rec.getValue(5)), pnd_No_Rec.getValue(1).add(pnd_No_Rec.getValue(2)).add(pnd_No_Rec.getValue(3)).add(pnd_No_Rec.getValue(4))); //Natural: ASSIGN #NO-REC ( 5 ) := #NO-REC ( 1 ) + #NO-REC ( 2 ) + #NO-REC ( 3 ) + #NO-REC ( 4 )
    }
    //*  TACC KG
    private void sub_All_Modes_T() throws Exception                                                                                                                       //Natural: ALL-MODES-T
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Grnted_Grd_Amt_Tot_T.compute(new ComputeParameters(false, pnd_Grnted_Grd_Amt_Tot_T), pnd_Grnted_Grd_Amt_T.getValue(1).add(pnd_Grnted_Grd_Amt_T.getValue(2)).add(pnd_Grnted_Grd_Amt_T.getValue(3)).add(pnd_Grnted_Grd_Amt_T.getValue(4))); //Natural: ASSIGN #GRNTED-GRD-AMT-TOT-T := #GRNTED-GRD-AMT-T ( 1 ) + #GRNTED-GRD-AMT-T ( 2 ) + #GRNTED-GRD-AMT-T ( 3 ) + #GRNTED-GRD-AMT-T ( 4 )
        pnd_Grnted_Std_Amt_Tot_T.compute(new ComputeParameters(false, pnd_Grnted_Std_Amt_Tot_T), pnd_Grnted_Std_Amt_T.getValue(1).add(pnd_Grnted_Std_Amt_T.getValue(2)).add(pnd_Grnted_Std_Amt_T.getValue(3)).add(pnd_Grnted_Std_Amt_T.getValue(4))); //Natural: ASSIGN #GRNTED-STD-AMT-TOT-T := #GRNTED-STD-AMT-T ( 1 ) + #GRNTED-STD-AMT-T ( 2 ) + #GRNTED-STD-AMT-T ( 3 ) + #GRNTED-STD-AMT-T ( 4 )
        pnd_Real_Annuity_T.getValue(5).compute(new ComputeParameters(false, pnd_Real_Annuity_T.getValue(5)), pnd_Real_Annuity_T.getValue(1).add(pnd_Real_Annuity_T.getValue(2)).add(pnd_Real_Annuity_T.getValue(3)).add(pnd_Real_Annuity_T.getValue(4))); //Natural: ASSIGN #REAL-ANNUITY-T ( 5 ) := #REAL-ANNUITY-T ( 1 ) + #REAL-ANNUITY-T ( 2 ) + #REAL-ANNUITY-T ( 3 ) + #REAL-ANNUITY-T ( 4 )
        pnd_Cref_Annuity_T.getValue(5).compute(new ComputeParameters(false, pnd_Cref_Annuity_T.getValue(5)), pnd_Cref_Annuity_T.getValue(1).add(pnd_Cref_Annuity_T.getValue(2)).add(pnd_Cref_Annuity_T.getValue(3)).add(pnd_Cref_Annuity_T.getValue(4))); //Natural: ASSIGN #CREF-ANNUITY-T ( 5 ) := #CREF-ANNUITY-T ( 1 ) + #CREF-ANNUITY-T ( 2 ) + #CREF-ANNUITY-T ( 3 ) + #CREF-ANNUITY-T ( 4 )
        pnd_Trad_Annuity_T.getValue(5).compute(new ComputeParameters(false, pnd_Trad_Annuity_T.getValue(5)), pnd_Trad_Annuity_T.getValue(1).add(pnd_Trad_Annuity_T.getValue(2)).add(pnd_Trad_Annuity_T.getValue(3)).add(pnd_Trad_Annuity_T.getValue(4))); //Natural: ASSIGN #TRAD-ANNUITY-T ( 5 ) := #TRAD-ANNUITY-T ( 1 ) + #TRAD-ANNUITY-T ( 2 ) + #TRAD-ANNUITY-T ( 3 ) + #TRAD-ANNUITY-T ( 4 )
        pnd_Tacc_Annuity_T.getValue(5).compute(new ComputeParameters(false, pnd_Tacc_Annuity_T.getValue(5)), pnd_Tacc_Annuity_T.getValue(1).add(pnd_Tacc_Annuity_T.getValue(2)).add(pnd_Tacc_Annuity_T.getValue(3)).add(pnd_Tacc_Annuity_T.getValue(4))); //Natural: ASSIGN #TACC-ANNUITY-T ( 5 ) := #TACC-ANNUITY-T ( 1 ) + #TACC-ANNUITY-T ( 2 ) + #TACC-ANNUITY-T ( 3 ) + #TACC-ANNUITY-T ( 4 )
        pnd_No_Rec_T.getValue(5).compute(new ComputeParameters(false, pnd_No_Rec_T.getValue(5)), pnd_No_Rec_T.getValue(1).add(pnd_No_Rec_T.getValue(2)).add(pnd_No_Rec_T.getValue(3)).add(pnd_No_Rec_T.getValue(4))); //Natural: ASSIGN #NO-REC-T ( 5 ) := #NO-REC-T ( 1 ) + #NO-REC-T ( 2 ) + #NO-REC-T ( 3 ) + #NO-REC-T ( 4 )
    }
    private void sub_Grnted_Amt() throws Exception                                                                                                                        //Natural: GRNTED-AMT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Grnted_Grd_Amt.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                                  //Natural: ASSIGN #GRNTED-GRD-AMT ( #P ) := #GRNTED-GRD-AMT ( #P ) + CIS.CIS-GRNTED-GRD-AMT
        pnd_Grnted_Std_Amt.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                                  //Natural: ASSIGN #GRNTED-STD-AMT ( #P ) := #GRNTED-STD-AMT ( #P ) + CIS.CIS-GRNTED-STD-AMT
        pnd_Trad_Annuity.getValue(pnd_P).compute(new ComputeParameters(false, pnd_Trad_Annuity.getValue(pnd_P)), pnd_Grnted_Grd_Amt.getValue(pnd_P).add(pnd_Grnted_Std_Amt.getValue(pnd_P))); //Natural: ASSIGN #TRAD-ANNUITY ( #P ) := #GRNTED-GRD-AMT ( #P ) + #GRNTED-STD-AMT ( #P )
    }
    private void sub_Grnted_Amt_Ipro() throws Exception                                                                                                                   //Natural: GRNTED-AMT-IPRO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Grnted_Grd_Amt_Ipro.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                             //Natural: ASSIGN #GRNTED-GRD-AMT-IPRO ( #P ) := #GRNTED-GRD-AMT-IPRO ( #P ) + CIS.CIS-GRNTED-GRD-AMT
        pnd_Trad_Annuity_Ipro.getValue(pnd_P).setValue(pnd_Grnted_Grd_Amt_Ipro.getValue(pnd_P));                                                                          //Natural: ASSIGN #TRAD-ANNUITY-IPRO ( #P ) := #GRNTED-GRD-AMT-IPRO ( #P )
    }
    private void sub_Grnted_Amt_Tpa() throws Exception                                                                                                                    //Natural: GRNTED-AMT-TPA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Grnted_Grd_Amt_Tpa.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                              //Natural: ASSIGN #GRNTED-GRD-AMT-TPA ( #P ) := #GRNTED-GRD-AMT-TPA ( #P ) + CIS.CIS-GRNTED-GRD-AMT
        pnd_Trad_Annuity_Tpa.getValue(pnd_P).setValue(pnd_Grnted_Grd_Amt_Tpa.getValue(pnd_P));                                                                            //Natural: ASSIGN #TRAD-ANNUITY-TPA ( #P ) := #GRNTED-GRD-AMT-TPA ( #P )
    }
    private void sub_Grnted_Amt_T() throws Exception                                                                                                                      //Natural: GRNTED-AMT-T
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Grnted_Grd_Amt_T.getValue(pnd_P).nadd(cis_Cis_Grnted_Grd_Amt);                                                                                                //Natural: ASSIGN #GRNTED-GRD-AMT-T ( #P ) := #GRNTED-GRD-AMT-T ( #P ) + CIS.CIS-GRNTED-GRD-AMT
        pnd_Grnted_Std_Amt_T.getValue(pnd_P).nadd(cis_Cis_Grnted_Std_Amt);                                                                                                //Natural: ASSIGN #GRNTED-STD-AMT-T ( #P ) := #GRNTED-STD-AMT-T ( #P ) + CIS.CIS-GRNTED-STD-AMT
        pnd_Trad_Annuity_T.getValue(pnd_P).compute(new ComputeParameters(false, pnd_Trad_Annuity_T.getValue(pnd_P)), pnd_Grnted_Grd_Amt_T.getValue(pnd_P).add(pnd_Grnted_Std_Amt_T.getValue(pnd_P))); //Natural: ASSIGN #TRAD-ANNUITY-T ( #P ) := #GRNTED-GRD-AMT-T ( #P ) + #GRNTED-STD-AMT-T ( #P )
    }
    private void sub_Value_Code() throws Exception                                                                                                                        //Natural: VALUE-CODE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_M_Tiaa_Rea.getValue(pnd_P).nadd(cis_Cis_Rea_Mnthly_Nbr_Units);                                                                                                //Natural: ASSIGN #M-TIAA-REA ( #P ) := #M-TIAA-REA ( #P ) + CIS.CIS-REA-MNTHLY-NBR-UNITS
        pnd_M_Tiaa_Rea.getValue(5).nadd(cis_Cis_Rea_Mnthly_Nbr_Units);                                                                                                    //Natural: ADD CIS.CIS-REA-MNTHLY-NBR-UNITS TO #M-TIAA-REA ( 5 )
        pnd_A_Tiaa_Rea.getValue(pnd_P).nadd(cis_Cis_Rea_Annual_Nbr_Units);                                                                                                //Natural: ASSIGN #A-TIAA-REA ( #P ) := #A-TIAA-REA ( #P ) + CIS.CIS-REA-ANNUAL-NBR-UNITS
        pnd_A_Tiaa_Rea.getValue(5).nadd(cis_Cis_Rea_Annual_Nbr_Units);                                                                                                    //Natural: ADD CIS.CIS-REA-ANNUAL-NBR-UNITS TO #A-TIAA-REA ( 5 )
        pnd_Real_Annuity.getValue(pnd_P).compute(new ComputeParameters(false, pnd_Real_Annuity.getValue(pnd_P)), pnd_M_Tiaa_Rea.getValue(pnd_P).add(pnd_A_Tiaa_Rea.getValue(pnd_P))); //Natural: ASSIGN #REAL-ANNUITY ( #P ) := #M-TIAA-REA ( #P ) + #A-TIAA-REA ( #P )
        FOR01:                                                                                                                                                            //Natural: FOR #C 1 20
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
        {
            //*  MMA
            short decideConditionsMet799 = 0;                                                                                                                             //Natural: DECIDE ON EVERY VALUE OF CIS.CIS-CREF-ACCT-CDE ( #C );//Natural: VALUE 'M'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("M")))
            {
                decideConditionsMet799++;
                pnd_M_Mma.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ASSIGN #M-MMA ( #P ) := #M-MMA ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Mma.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ASSIGN #A-MMA ( #P ) := #A-MMA ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Mma.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                                //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-MMA ( 5 )
                //*  STOCK
                pnd_A_Mma.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                                //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-MMA ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'C'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("C")))
            {
                decideConditionsMet799++;
                pnd_M_Stock.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ASSIGN #M-STOCK ( #P ) := #M-STOCK ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Stock.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ASSIGN #A-STOCK ( #P ) := #A-STOCK ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Stock.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                              //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-STOCK ( 5 )
                //*  SOCIAL CHOICE
                pnd_A_Stock.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                              //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-STOCK ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'S'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("S")))
            {
                decideConditionsMet799++;
                pnd_M_Social.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ASSIGN #M-SOCIAL ( #P ) := #M-SOCIAL ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Social.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ASSIGN #A-SOCIAL ( #P ) := #A-SOCIAL ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Social.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                             //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-SOCIAL ( 5 )
                //*  GLOBAL
                pnd_A_Social.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                             //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-SOCIAL ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'W'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("W")))
            {
                decideConditionsMet799++;
                pnd_M_Global.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ASSIGN #M-GLOBAL ( #P ) := #M-GLOBAL ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Global.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ASSIGN #A-GLOBAL ( #P ) := #A-GLOBAL ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Global.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                             //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-GLOBAL ( 5 )
                //*  BOND
                pnd_A_Global.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                             //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-GLOBAL ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'B'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("B")))
            {
                decideConditionsMet799++;
                pnd_M_Bond.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ASSIGN #M-BOND ( #P ) := #M-BOND ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Bond.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ASSIGN #A-BOND ( #P ) := #A-BOND ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Bond.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                               //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-BOND ( 5 )
                //*  GROWTH
                pnd_A_Bond.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                               //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-BOND ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'L'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("L")))
            {
                decideConditionsMet799++;
                pnd_M_Grth.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ASSIGN #M-GRTH ( #P ) := #M-GRTH ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Grth.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ASSIGN #A-GRTH ( #P ) := #A-GRTH ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Grth.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                               //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-GRTH ( 5 )
                //*  EQUITY INDEX
                pnd_A_Grth.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                               //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-GRTH ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'E'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("E")))
            {
                decideConditionsMet799++;
                pnd_M_Equ.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ASSIGN #M-EQU ( #P ) := #M-EQU ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Equ.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ASSIGN #A-EQU ( #P ) := #A-EQU ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Equ.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                                //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-EQU ( 5 )
                //*  ILB
                pnd_A_Equ.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                                //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-EQU ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'I'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("I")))
            {
                decideConditionsMet799++;
                pnd_M_Ilb.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ASSIGN #M-ILB ( #P ) := #M-ILB ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Ilb.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ASSIGN #A-ILB ( #P ) := #A-ILB ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Ilb.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                                //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-ILB ( 5 )
                pnd_A_Ilb.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                                //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-ILB ( 5 )
            }                                                                                                                                                             //Natural: NONE VALUE
            if (condition(decideConditionsMet799 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  TACC KG START
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Cref_Annuity.getValue(pnd_P).compute(new ComputeParameters(false, pnd_Cref_Annuity.getValue(pnd_P)), pnd_M_Mma.getValue(pnd_P).add(pnd_M_Stock.getValue(pnd_P)).add(pnd_M_Social.getValue(pnd_P)).add(pnd_M_Global.getValue(pnd_P)).add(pnd_M_Bond.getValue(pnd_P)).add(pnd_M_Grth.getValue(pnd_P)).add(pnd_M_Equ.getValue(pnd_P)).add(pnd_M_Ilb.getValue(pnd_P)).add(pnd_A_Mma.getValue(pnd_P)).add(pnd_A_Stock.getValue(pnd_P)).add(pnd_A_Social.getValue(pnd_P)).add(pnd_A_Global.getValue(pnd_P)).add(pnd_A_Bond.getValue(pnd_P)).add(pnd_A_Grth.getValue(pnd_P)).add(pnd_A_Equ.getValue(pnd_P)).add(pnd_A_Ilb.getValue(pnd_P))); //Natural: ASSIGN #CREF-ANNUITY ( #P ) := #M-MMA ( #P ) + #M-STOCK ( #P ) + #M-SOCIAL ( #P ) + #M-GLOBAL ( #P ) + #M-BOND ( #P ) + #M-GRTH ( #P ) + #M-EQU ( #P ) + #M-ILB ( #P ) + #A-MMA ( #P ) + #A-STOCK ( #P ) + #A-SOCIAL ( #P ) + #A-GLOBAL ( #P ) + #A-BOND ( #P ) + #A-GRTH ( #P ) + #A-EQU ( #P ) + #A-ILB ( #P )
        FOR02:                                                                                                                                                            //Natural: FOR #D 1 100
        for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(100)); pnd_D.nadd(1))
        {
            pnd_M_Taccess.getValue(pnd_P).nadd(cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D));                                                                            //Natural: ASSIGN #M-TACCESS ( #P ) := #M-TACCESS ( #P ) + CIS.CIS-TACC-MNTHLY-NBR-UNITS ( #D )
            pnd_A_Taccess.getValue(pnd_P).nadd(cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D));                                                                            //Natural: ASSIGN #A-TACCESS ( #P ) := #A-TACCESS ( #P ) + CIS.CIS-TACC-ANNUAL-NBR-UNITS ( #D )
            pnd_M_Taccess.getValue(5).nadd(cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D));                                                                                //Natural: ADD CIS.CIS-TACC-MNTHLY-NBR-UNITS ( #D ) TO #M-TACCESS ( 5 )
            pnd_A_Taccess.getValue(5).nadd(cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D));                                                                                //Natural: ADD CIS.CIS-TACC-ANNUAL-NBR-UNITS ( #D ) TO #A-TACCESS ( 5 )
            //*  TACC KG END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tacc_Annuity.getValue(pnd_P).compute(new ComputeParameters(false, pnd_Tacc_Annuity.getValue(pnd_P)), pnd_M_Taccess.getValue(pnd_P).add(pnd_A_Taccess.getValue(pnd_P))); //Natural: ASSIGN #TACC-ANNUITY ( #P ) := #M-TACCESS ( #P ) + #A-TACCESS ( #P )
    }
    private void sub_Value_Code_T() throws Exception                                                                                                                      //Natural: VALUE-CODE-T
    {
        if (BLNatReinput.isReinput()) return;

        pnd_M_Tiaa_Rea_T.getValue(pnd_P).nadd(cis_Cis_Rea_Mnthly_Nbr_Units);                                                                                              //Natural: ASSIGN #M-TIAA-REA-T ( #P ) := #M-TIAA-REA-T ( #P ) + CIS.CIS-REA-MNTHLY-NBR-UNITS
        pnd_M_Tiaa_Rea_T.getValue(5).nadd(cis_Cis_Rea_Mnthly_Nbr_Units);                                                                                                  //Natural: ADD CIS.CIS-REA-MNTHLY-NBR-UNITS TO #M-TIAA-REA-T ( 5 )
        pnd_A_Tiaa_Rea_T.getValue(pnd_P).nadd(cis_Cis_Rea_Annual_Nbr_Units);                                                                                              //Natural: ASSIGN #A-TIAA-REA-T ( #P ) := #A-TIAA-REA-T ( #P ) + CIS.CIS-REA-ANNUAL-NBR-UNITS
        pnd_A_Tiaa_Rea_T.getValue(5).nadd(cis_Cis_Rea_Annual_Nbr_Units);                                                                                                  //Natural: ADD CIS.CIS-REA-ANNUAL-NBR-UNITS TO #A-TIAA-REA-T ( 5 )
        pnd_Real_Annuity_T.getValue(pnd_P).compute(new ComputeParameters(false, pnd_Real_Annuity_T.getValue(pnd_P)), pnd_M_Tiaa_Rea_T.getValue(pnd_P).add(pnd_A_Tiaa_Rea_T.getValue(pnd_P))); //Natural: ASSIGN #REAL-ANNUITY-T ( #P ) := #M-TIAA-REA-T ( #P ) + #A-TIAA-REA-T ( #P )
        FOR03:                                                                                                                                                            //Natural: FOR #C 1 20
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
        {
            //*  MMA
            short decideConditionsMet871 = 0;                                                                                                                             //Natural: DECIDE ON EVERY VALUE OF CIS.CIS-CREF-ACCT-CDE ( #C );//Natural: VALUE 'M'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("M")))
            {
                decideConditionsMet871++;
                pnd_M_Mma_T.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ASSIGN #M-MMA-T ( #P ) := #M-MMA-T ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Mma_T.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ASSIGN #A-MMA-T ( #P ) := #A-MMA-T ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Mma_T.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                              //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-MMA-T ( 5 )
                //*  STOCK
                pnd_A_Mma_T.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                              //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-MMA-T ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'C'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("C")))
            {
                decideConditionsMet871++;
                pnd_M_Stock_T.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                        //Natural: ASSIGN #M-STOCK-T ( #P ) := #M-STOCK-T ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Stock_T.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                        //Natural: ASSIGN #A-STOCK-T ( #P ) := #A-STOCK-T ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Stock_T.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-STOCK-T ( 5 )
                //*  SOCIAL CHOICE
                pnd_A_Stock_T.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                            //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-STOCK-T ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'S'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("S")))
            {
                decideConditionsMet871++;
                pnd_M_Social_T.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ASSIGN #M-SOCIAL-T ( #P ) := #M-SOCIAL-T ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Social_T.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ASSIGN #A-SOCIAL-T ( #P ) := #A-SOCIAL-T ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Social_T.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-SOCIAL-T ( 5 )
                //*  GLOBAL
                pnd_A_Social_T.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-SOCIAL-T ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'W'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("W")))
            {
                decideConditionsMet871++;
                pnd_M_Global_T.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ASSIGN #M-GLOBAL-T ( #P ) := #M-GLOBAL-T ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Global_T.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                       //Natural: ASSIGN #A-GLOBAL-T ( #P ) := #A-GLOBAL-T ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Global_T.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-GLOBAL-T ( 5 )
                //*  BOND
                pnd_A_Global_T.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                           //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-GLOBAL-T ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'B'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("B")))
            {
                decideConditionsMet871++;
                pnd_M_Bond_T.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ASSIGN #M-BOND-T ( #P ) := #M-BOND-T ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Bond_T.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ASSIGN #A-BOND-T ( #P ) := #A-BOND-T ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Bond_T.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                             //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-BOND-T ( 5 )
                //*  GROWTH
                pnd_A_Bond_T.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                             //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-BOND-T ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'L'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("L")))
            {
                decideConditionsMet871++;
                pnd_M_Grth_T.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ASSIGN #M-GRTH-T ( #P ) := #M-GRTH-T ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Grth_T.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                         //Natural: ASSIGN #A-GRTH-T ( #P ) := #A-GRTH-T ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Grth_T.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                             //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-GRTH-T ( 5 )
                //*  EQUITY INDEX
                pnd_A_Grth_T.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                             //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-GRTH-T ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'E'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("E")))
            {
                decideConditionsMet871++;
                pnd_M_Equ_T.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ASSIGN #M-EQU-T ( #P ) := #M-EQU-T ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Equ_T.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ASSIGN #A-EQU-T ( #P ) := #A-EQU-T ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Equ_T.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                              //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-EQU-T ( 5 )
                //*  ILB
                pnd_A_Equ_T.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                              //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-EQU-T ( 5 )
            }                                                                                                                                                             //Natural: VALUE 'I'
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("I")))
            {
                decideConditionsMet871++;
                pnd_M_Ilb_T.getValue(pnd_P).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ASSIGN #M-ILB-T ( #P ) := #M-ILB-T ( #P ) + CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C )
                pnd_A_Ilb_T.getValue(pnd_P).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                          //Natural: ASSIGN #A-ILB-T ( #P ) := #A-ILB-T ( #P ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                pnd_M_Ilb_T.getValue(5).nadd(cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C));                                                                              //Natural: ADD CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) TO #M-ILB-T ( 5 )
                pnd_A_Ilb_T.getValue(5).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                                              //Natural: ADD CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) TO #A-ILB-T ( 5 )
            }                                                                                                                                                             //Natural: NONE VALUE
            if (condition(decideConditionsMet871 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  TACC KG START
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Cref_Annuity_T.getValue(pnd_P).compute(new ComputeParameters(false, pnd_Cref_Annuity_T.getValue(pnd_P)), pnd_M_Mma_T.getValue(pnd_P).add(pnd_M_Stock_T.getValue(pnd_P)).add(pnd_M_Social_T.getValue(pnd_P)).add(pnd_M_Global_T.getValue(pnd_P)).add(pnd_M_Bond_T.getValue(pnd_P)).add(pnd_M_Grth_T.getValue(pnd_P)).add(pnd_M_Equ_T.getValue(pnd_P)).add(pnd_M_Ilb_T.getValue(pnd_P)).add(pnd_A_Mma_T.getValue(pnd_P)).add(pnd_A_Stock_T.getValue(pnd_P)).add(pnd_A_Social_T.getValue(pnd_P)).add(pnd_A_Global_T.getValue(pnd_P)).add(pnd_A_Bond_T.getValue(pnd_P)).add(pnd_A_Grth_T.getValue(pnd_P)).add(pnd_A_Equ_T.getValue(pnd_P)).add(pnd_A_Ilb_T.getValue(pnd_P))); //Natural: ASSIGN #CREF-ANNUITY-T ( #P ) := #M-MMA-T ( #P ) + #M-STOCK-T ( #P ) + #M-SOCIAL-T ( #P ) + #M-GLOBAL-T ( #P ) + #M-BOND-T ( #P ) + #M-GRTH-T ( #P ) + #M-EQU-T ( #P ) + #M-ILB-T ( #P ) + #A-MMA-T ( #P ) + #A-STOCK-T ( #P ) + #A-SOCIAL-T ( #P ) + #A-GLOBAL-T ( #P ) + #A-BOND-T ( #P ) + #A-GRTH-T ( #P ) + #A-EQU-T ( #P ) + #A-ILB-T ( #P )
        FOR04:                                                                                                                                                            //Natural: FOR #D 1 100
        for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(100)); pnd_D.nadd(1))
        {
            pnd_M_Taccess_T.getValue(pnd_P).nadd(cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D));                                                                          //Natural: ASSIGN #M-TACCESS-T ( #P ) := #M-TACCESS-T ( #P ) + CIS.CIS-TACC-MNTHLY-NBR-UNITS ( #D )
            pnd_A_Taccess_T.getValue(pnd_P).nadd(cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D));                                                                          //Natural: ASSIGN #A-TACCESS-T ( #P ) := #A-TACCESS-T ( #P ) + CIS.CIS-TACC-ANNUAL-NBR-UNITS ( #D )
            pnd_M_Taccess_T.getValue(5).nadd(cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D));                                                                              //Natural: ADD CIS.CIS-TACC-MNTHLY-NBR-UNITS ( #D ) TO #M-TACCESS-T ( 5 )
            pnd_A_Taccess_T.getValue(5).nadd(cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D));                                                                              //Natural: ADD CIS.CIS-TACC-ANNUAL-NBR-UNITS ( #D ) TO #A-TACCESS-T ( 5 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tacc_Annuity_T.getValue(pnd_P).compute(new ComputeParameters(false, pnd_Tacc_Annuity_T.getValue(pnd_P)), pnd_M_Taccess_T.getValue(pnd_P).add(pnd_A_Taccess_T.getValue(pnd_P))); //Natural: ASSIGN #TACC-ANNUITY-T ( #P ) := #M-TACCESS-T ( #P ) + #A-TACCESS-T ( #P )
        //*                                                     /* TACC KG END
    }
    private void sub_Da_Proceed() throws Exception                                                                                                                        //Natural: DA-PROCEED
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(cis_Cis_Cntrct_Type.equals("T")))                                                                                                                   //Natural: IF CIS.CIS-CNTRCT-TYPE = 'T'
        {
            pnd_Contract_Issued.getValue(pnd_P).nadd(1);                                                                                                                  //Natural: ADD 1 TO #CONTRACT-ISSUED ( #P )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cis_Cis_Cntrct_Type.equals("B")))                                                                                                               //Natural: IF CIS.CIS-CNTRCT-TYPE = 'B'
            {
                pnd_Contract_Issued.getValue(pnd_P).nadd(1);                                                                                                              //Natural: ADD 1 TO #CONTRACT-ISSUED ( #P )
                pnd_Certif_Issued.getValue(pnd_P).nadd(1);                                                                                                                //Natural: ADD 1 TO #CERTIF-ISSUED ( #P )
                pnd_Certif_Issued2.getValue(pnd_P).nadd(1);                                                                                                               //Natural: ADD 1 TO #CERTIF-ISSUED2 ( #P )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR05:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                             //Natural: IF CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) NE 0
            {
                pnd_Da_Tiaa_Proceeds_Amt.getValue(pnd_P).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                              //Natural: ASSIGN #DA-TIAA-PROCEEDS-AMT ( #P ) := #DA-TIAA-PROCEEDS-AMT ( #P ) + CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I )
                pnd_Da_Tiaa_Proceeds_Amt.getValue(5).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                                  //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #DA-TIAA-PROCEEDS-AMT ( 5 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                              //Natural: IF CIS.CIS-DA-REA-PROCEEDS-AMT ( #I ) NE 0
            {
                pnd_Da_Rea_Proceeds_Amt.getValue(pnd_P).nadd(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I));                                                                //Natural: ASSIGN #DA-REA-PROCEEDS-AMT ( #P ) := #DA-REA-PROCEEDS-AMT ( #P ) + CIS.CIS-DA-REA-PROCEEDS-AMT ( #I )
                pnd_Da_Rea_Proceeds_Amt.getValue(5).nadd(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I));                                                                    //Natural: ADD CIS.CIS-DA-REA-PROCEEDS-AMT ( #I ) TO #DA-REA-PROCEEDS-AMT ( 5 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                             //Natural: IF CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I ) NE 0
            {
                pnd_Da_Cref_Proceeds_Amt.getValue(pnd_P).nadd(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I));                                                              //Natural: ASSIGN #DA-CREF-PROCEEDS-AMT ( #P ) := #DA-CREF-PROCEEDS-AMT ( #P ) + CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I )
                pnd_Da_Cref_Proceeds_Amt.getValue(5).nadd(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I));                                                                  //Natural: ADD CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I ) TO #DA-CREF-PROCEEDS-AMT ( 5 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Da_Proceed_Tpa() throws Exception                                                                                                                    //Natural: DA-PROCEED-TPA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(cis_Cis_Cntrct_Type.equals("T")))                                                                                                                   //Natural: IF CIS.CIS-CNTRCT-TYPE = 'T'
        {
            pnd_Contract_Issued_Tpa.getValue(pnd_P).nadd(1);                                                                                                              //Natural: ADD 1 TO #CONTRACT-ISSUED-TPA ( #P )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cis_Cis_Cntrct_Type.equals("B")))                                                                                                               //Natural: IF CIS.CIS-CNTRCT-TYPE = 'B'
            {
                pnd_Contract_Issued_Tpa.getValue(pnd_P).nadd(1);                                                                                                          //Natural: ADD 1 TO #CONTRACT-ISSUED-TPA ( #P )
                //*     ADD 1 TO #CERTIF-ISSUED(#P)
                //*     ADD 1 TO #CERTIF-ISSUED2(#P)
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR06:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                             //Natural: IF CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) NE 0
            {
                pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(pnd_P).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                          //Natural: ASSIGN #DA-TIAA-PROCEEDS-AMT-TPA ( #P ) := #DA-TIAA-PROCEEDS-AMT-TPA ( #P ) + CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I )
                pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(5).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                              //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #DA-TIAA-PROCEEDS-AMT-TPA ( 5 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Da_Proceed_Ipro() throws Exception                                                                                                                   //Natural: DA-PROCEED-IPRO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(cis_Cis_Cntrct_Type.equals("T")))                                                                                                                   //Natural: IF CIS.CIS-CNTRCT-TYPE = 'T'
        {
            pnd_Contract_Issued_Ipro.getValue(pnd_P).nadd(1);                                                                                                             //Natural: ADD 1 TO #CONTRACT-ISSUED-IPRO ( #P )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cis_Cis_Cntrct_Type.equals("B")))                                                                                                               //Natural: IF CIS.CIS-CNTRCT-TYPE = 'B'
            {
                pnd_Contract_Issued_Ipro.getValue(pnd_P).nadd(1);                                                                                                         //Natural: ADD 1 TO #CONTRACT-ISSUED-IPRO ( #P )
                //*     ADD 1 TO #CERTIF-ISSUED(#P)
                //*     ADD 1 TO #CERTIF-ISSUED2(#P)
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR07:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                             //Natural: IF CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) NE 0
            {
                pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(pnd_P).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                         //Natural: ASSIGN #DA-TIAA-PROCEEDS-AMT-IPRO ( #P ) := #DA-TIAA-PROCEEDS-AMT-IPRO ( #P ) + CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I )
                pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(5).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                             //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #DA-TIAA-PROCEEDS-AMT-IPRO ( 5 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Da_Proceed_T() throws Exception                                                                                                                      //Natural: DA-PROCEED-T
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(cis_Cis_Cntrct_Type.equals("T")))                                                                                                                   //Natural: IF CIS.CIS-CNTRCT-TYPE = 'T'
        {
            pnd_Contract_Issued_T.getValue(pnd_P).nadd(1);                                                                                                                //Natural: ADD 1 TO #CONTRACT-ISSUED-T ( #P )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(cis_Cis_Cntrct_Type.equals("B")))                                                                                                               //Natural: IF CIS.CIS-CNTRCT-TYPE = 'B'
            {
                pnd_Contract_Issued_T.getValue(pnd_P).nadd(1);                                                                                                            //Natural: ADD 1 TO #CONTRACT-ISSUED-T ( #P )
                pnd_Certif_Issued_T.getValue(pnd_P).nadd(1);                                                                                                              //Natural: ADD 1 TO #CERTIF-ISSUED-T ( #P )
                pnd_Certif_Issued2_T.getValue(pnd_P).nadd(1);                                                                                                             //Natural: ADD 1 TO #CERTIF-ISSUED2-T ( #P )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Certif_Issued2_T.getValue(pnd_P).nadd(1);                                                                                                             //Natural: ADD 1 TO #CERTIF-ISSUED2-T ( #P )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR08:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                             //Natural: IF CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) NE 0
            {
                pnd_Da_Tiaa_Proceeds_Amt_T.getValue(pnd_P).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                            //Natural: ASSIGN #DA-TIAA-PROCEEDS-AMT-T ( #P ) := #DA-TIAA-PROCEEDS-AMT-T ( #P ) + CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I )
                pnd_Da_Tiaa_Proceeds_Amt_T.getValue(5).nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                                //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #DA-TIAA-PROCEEDS-AMT-T ( 5 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                              //Natural: IF CIS.CIS-DA-REA-PROCEEDS-AMT ( #I ) NE 0
            {
                pnd_Da_Rea_Proceeds_Amt_T.getValue(pnd_P).nadd(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I));                                                              //Natural: ASSIGN #DA-REA-PROCEEDS-AMT-T ( #P ) := #DA-REA-PROCEEDS-AMT-T ( #P ) + CIS.CIS-DA-REA-PROCEEDS-AMT ( #I )
                pnd_Da_Rea_Proceeds_Amt_T.getValue(5).nadd(cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_I));                                                                  //Natural: ADD CIS.CIS-DA-REA-PROCEEDS-AMT ( #I ) TO #DA-REA-PROCEEDS-AMT-T ( 5 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                             //Natural: IF CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I ) NE 0
            {
                pnd_Da_Cref_Proceeds_Amt_T.getValue(pnd_P).nadd(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I));                                                            //Natural: ASSIGN #DA-CREF-PROCEEDS-AMT-T ( #P ) := #DA-CREF-PROCEEDS-AMT-T ( #P ) + CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I )
                pnd_Da_Cref_Proceeds_Amt_T.getValue(5).nadd(cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_I));                                                                //Natural: ADD CIS.CIS-DA-CREF-PROCEEDS-AMT ( #I ) TO #DA-CREF-PROCEEDS-AMT-T ( 5 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Issued_Totals() throws Exception                                                                                                                     //Natural: ISSUED-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Contract_Issued.getValue(5).compute(new ComputeParameters(false, pnd_Contract_Issued.getValue(5)), pnd_Contract_Issued.getValue(1).add(pnd_Contract_Issued.getValue(2)).add(pnd_Contract_Issued.getValue(3)).add(pnd_Contract_Issued.getValue(4))); //Natural: ASSIGN #CONTRACT-ISSUED ( 5 ) := #CONTRACT-ISSUED ( 1 ) + #CONTRACT-ISSUED ( 2 ) + #CONTRACT-ISSUED ( 3 ) + #CONTRACT-ISSUED ( 4 )
        pnd_Certif_Issued.getValue(5).compute(new ComputeParameters(false, pnd_Certif_Issued.getValue(5)), pnd_Certif_Issued.getValue(1).add(pnd_Certif_Issued.getValue(2)).add(pnd_Certif_Issued.getValue(3)).add(pnd_Certif_Issued.getValue(4))); //Natural: ASSIGN #CERTIF-ISSUED ( 5 ) := #CERTIF-ISSUED ( 1 ) + #CERTIF-ISSUED ( 2 ) + #CERTIF-ISSUED ( 3 ) + #CERTIF-ISSUED ( 4 )
        pnd_Certif_Issued2.getValue(5).compute(new ComputeParameters(false, pnd_Certif_Issued2.getValue(5)), pnd_Certif_Issued2.getValue(1).add(pnd_Certif_Issued2.getValue(2)).add(pnd_Certif_Issued2.getValue(3)).add(pnd_Certif_Issued2.getValue(4))); //Natural: ASSIGN #CERTIF-ISSUED2 ( 5 ) := #CERTIF-ISSUED2 ( 1 ) + #CERTIF-ISSUED2 ( 2 ) + #CERTIF-ISSUED2 ( 3 ) + #CERTIF-ISSUED2 ( 4 )
    }
    private void sub_Issued_Totals_Tpa() throws Exception                                                                                                                 //Natural: ISSUED-TOTALS-TPA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Contract_Issued_Tpa.getValue(5).compute(new ComputeParameters(false, pnd_Contract_Issued_Tpa.getValue(5)), pnd_Contract_Issued_Tpa.getValue(1).add(pnd_Contract_Issued_Tpa.getValue(2)).add(pnd_Contract_Issued_Tpa.getValue(3)).add(pnd_Contract_Issued_Tpa.getValue(4))); //Natural: ASSIGN #CONTRACT-ISSUED-TPA ( 5 ) := #CONTRACT-ISSUED-TPA ( 1 ) + #CONTRACT-ISSUED-TPA ( 2 ) + #CONTRACT-ISSUED-TPA ( 3 ) + #CONTRACT-ISSUED-TPA ( 4 )
    }
    private void sub_Issued_Totals_Ipro() throws Exception                                                                                                                //Natural: ISSUED-TOTALS-IPRO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Contract_Issued_Ipro.getValue(5).compute(new ComputeParameters(false, pnd_Contract_Issued_Ipro.getValue(5)), pnd_Contract_Issued_Ipro.getValue(1).add(pnd_Contract_Issued_Ipro.getValue(2)).add(pnd_Contract_Issued_Ipro.getValue(3)).add(pnd_Contract_Issued_Ipro.getValue(4))); //Natural: ASSIGN #CONTRACT-ISSUED-IPRO ( 5 ) := #CONTRACT-ISSUED-IPRO ( 1 ) + #CONTRACT-ISSUED-IPRO ( 2 ) + #CONTRACT-ISSUED-IPRO ( 3 ) + #CONTRACT-ISSUED-IPRO ( 4 )
    }
    private void sub_Issued_Totals_T() throws Exception                                                                                                                   //Natural: ISSUED-TOTALS-T
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Contract_Issued_T.getValue(5).compute(new ComputeParameters(false, pnd_Contract_Issued_T.getValue(5)), pnd_Contract_Issued_T.getValue(1).add(pnd_Contract_Issued_T.getValue(2)).add(pnd_Contract_Issued_T.getValue(3)).add(pnd_Contract_Issued_T.getValue(4))); //Natural: ASSIGN #CONTRACT-ISSUED-T ( 5 ) := #CONTRACT-ISSUED-T ( 1 ) + #CONTRACT-ISSUED-T ( 2 ) + #CONTRACT-ISSUED-T ( 3 ) + #CONTRACT-ISSUED-T ( 4 )
        pnd_Certif_Issued_T.getValue(5).compute(new ComputeParameters(false, pnd_Certif_Issued_T.getValue(5)), pnd_Certif_Issued_T.getValue(1).add(pnd_Certif_Issued_T.getValue(2)).add(pnd_Certif_Issued_T.getValue(3)).add(pnd_Certif_Issued_T.getValue(4))); //Natural: ASSIGN #CERTIF-ISSUED-T ( 5 ) := #CERTIF-ISSUED-T ( 1 ) + #CERTIF-ISSUED-T ( 2 ) + #CERTIF-ISSUED-T ( 3 ) + #CERTIF-ISSUED-T ( 4 )
        pnd_Certif_Issued2_T.getValue(5).compute(new ComputeParameters(false, pnd_Certif_Issued2_T.getValue(5)), pnd_Certif_Issued2_T.getValue(1).add(pnd_Certif_Issued2_T.getValue(2)).add(pnd_Certif_Issued2_T.getValue(3)).add(pnd_Certif_Issued2_T.getValue(4))); //Natural: ASSIGN #CERTIF-ISSUED2-T ( 5 ) := #CERTIF-ISSUED2-T ( 1 ) + #CERTIF-ISSUED2-T ( 2 ) + #CERTIF-ISSUED2-T ( 3 ) + #CERTIF-ISSUED2-T ( 4 )
    }
    private void sub_Issued_Totals_T_Tpa() throws Exception                                                                                                               //Natural: ISSUED-TOTALS-T-TPA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Contract_Issued_T_Tpa.getValue(5).compute(new ComputeParameters(false, pnd_Contract_Issued_T_Tpa.getValue(5)), pnd_Contract_Issued_T_Tpa.getValue(1).add(pnd_Contract_Issued_T_Tpa.getValue(2)).add(pnd_Contract_Issued_T_Tpa.getValue(3)).add(pnd_Contract_Issued_T_Tpa.getValue(4))); //Natural: ASSIGN #CONTRACT-ISSUED-T-TPA ( 5 ) := #CONTRACT-ISSUED-T-TPA ( 1 ) + #CONTRACT-ISSUED-T-TPA ( 2 ) + #CONTRACT-ISSUED-T-TPA ( 3 ) + #CONTRACT-ISSUED-T-TPA ( 4 )
    }
    private void sub_Issued_Totals_T_Ipro() throws Exception                                                                                                              //Natural: ISSUED-TOTALS-T-IPRO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Contract_Issued_T_Ipro.getValue(5).compute(new ComputeParameters(false, pnd_Contract_Issued_T_Ipro.getValue(5)), pnd_Contract_Issued_T_Ipro.getValue(1).add(pnd_Contract_Issued_T_Ipro.getValue(2)).add(pnd_Contract_Issued_T_Ipro.getValue(3)).add(pnd_Contract_Issued_T_Ipro.getValue(4))); //Natural: ASSIGN #CONTRACT-ISSUED-T-IPRO ( 5 ) := #CONTRACT-ISSUED-T-IPRO ( 1 ) + #CONTRACT-ISSUED-T-IPRO ( 2 ) + #CONTRACT-ISSUED-T-IPRO ( 3 ) + #CONTRACT-ISSUED-T-IPRO ( 4 )
    }
    private void sub_Ia_Report() throws Exception                                                                                                                         //Natural: IA-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
                                                                                                                                                                          //Natural: PERFORM ISSUED-TOTALS
        sub_Issued_Totals();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ISSUED-TOTALS-TPA
        sub_Issued_Totals_Tpa();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ISSUED-TOTALS-IPRO
        sub_Issued_Totals_Ipro();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,"INCOME BENEFITS", new FieldAttributes ("AD=I"),NEWLINE,"---------------");                                            //Natural: WRITE ( 1 ) 'INCOME BENEFITS' ( AD = I ) / '---------------'
        if (Global.isEscape()) return;
        getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Standard",                                                              //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 'TRADITIONAL ANNUITY - Standard' 1X #GRNTED-STD-AMT ( 1 ) ( IC = $ ) #GRNTED-STD-AMT ( 2 ) ( IC = $ ) #GRNTED-STD-AMT ( 3 ) ( IC = $ ) #GRNTED-STD-AMT ( 4 ) ( IC = $ ) 4X #GRNTED-STD-AMT-TOT ( IC = $ )
        		new ColumnSpacing(1),pnd_Grnted_Std_Amt.getValue(1), new InsertionCharacter("$"),pnd_Grnted_Std_Amt.getValue(2), new InsertionCharacter("$"),pnd_Grnted_Std_Amt.getValue(3), 
            new InsertionCharacter("$"),pnd_Grnted_Std_Amt.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Grnted_Std_Amt_Tot, new InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Graded  ",                                                              //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 'TRADITIONAL ANNUITY - Graded  ' 1X #GRNTED-GRD-AMT ( 1 ) ( IC = $ ) #GRNTED-GRD-AMT ( 2 ) ( IC = $ ) #GRNTED-GRD-AMT ( 3 ) ( IC = $ ) #GRNTED-GRD-AMT ( 4 ) ( IC = $ ) 4X #GRNTED-GRD-AMT-TOT ( IC = $ )
        		new ColumnSpacing(1),pnd_Grnted_Grd_Amt.getValue(1), new InsertionCharacter("$"),pnd_Grnted_Grd_Amt.getValue(2), new InsertionCharacter("$"),pnd_Grnted_Grd_Amt.getValue(3), 
            new InsertionCharacter("$"),pnd_Grnted_Grd_Amt.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Grnted_Grd_Amt_Tot, new InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"ANNUAL REVALUATION", new FieldAttributes ("AD=I"),NEWLINE,"------------------",NEWLINE,"TIAA REA          ",          //Natural: WRITE ( 1 ) 'ANNUAL REVALUATION' ( AD = I ) / '------------------' / 'TIAA REA          ' ( AD = I ) 15X #A-TIAA-REA ( 1 ) 2X #A-TIAA-REA ( 2 ) 2X #A-TIAA-REA ( 3 ) 2X #A-TIAA-REA ( 4 ) 5X #A-TIAA-REA ( 5 ) / 'TIAA ACCESS       ' 15X #A-TACCESS ( 1 ) 2X #A-TACCESS ( 2 ) 2X #A-TACCESS ( 3 ) 2X #A-TACCESS ( 4 ) 5X #A-TACCESS ( 5 ) // 'CREF Stock        ' 15X #A-STOCK ( 1 ) 2X #A-STOCK ( 2 ) 2X #A-STOCK ( 3 ) 2X #A-STOCK ( 4 ) 5X #A-STOCK ( 5 ) / 'CREF MMA          ' 15X #A-MMA ( 1 ) 2X #A-MMA ( 2 ) 2X #A-MMA ( 3 ) 2X #A-MMA ( 4 ) 5X #A-MMA ( 5 ) / 'CREF Social Choice' 15X #A-SOCIAL ( 1 ) 2X #A-SOCIAL ( 2 ) 2X #A-SOCIAL ( 3 ) 2X #A-SOCIAL ( 4 ) 5X #A-SOCIAL ( 5 ) / 'CREF Bond         ' 15X #A-BOND ( 1 ) 2X #A-BOND ( 2 ) 2X #A-BOND ( 3 ) 2X #A-BOND ( 4 ) 5X #A-BOND ( 5 ) / 'CREF Global       ' 15X #A-GLOBAL ( 1 ) 2X #A-GLOBAL ( 2 ) 2X #A-GLOBAL ( 3 ) 2X #A-GLOBAL ( 4 ) 5X #A-GLOBAL ( 5 ) / 'CREF Growth       ' 15X #A-GRTH ( 1 ) 2X #A-GRTH ( 2 ) 2X #A-GRTH ( 3 ) 2X #A-GRTH ( 4 ) 5X #A-GRTH ( 5 ) / 'CREF Equity Index ' 15X #A-EQU ( 1 ) 2X #A-EQU ( 2 ) 2X #A-EQU ( 3 ) 2X #A-EQU ( 4 ) 5X #A-EQU ( 5 ) / 'CREF ILB          ' 15X #A-ILB ( 1 ) 2X #A-ILB ( 2 ) 2X #A-ILB ( 3 ) 2X #A-ILB ( 4 ) 5X #A-ILB ( 5 )
            new FieldAttributes ("AD=I"),new ColumnSpacing(15),pnd_A_Tiaa_Rea.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Tiaa_Rea.getValue(2), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Tiaa_Rea.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Tiaa_Rea.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Tiaa_Rea.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA ACCESS       ",new 
            ColumnSpacing(15),pnd_A_Taccess.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Taccess.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Taccess.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Taccess.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Taccess.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"CREF Stock        ",new 
            ColumnSpacing(15),pnd_A_Stock.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Stock.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Stock.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Stock.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Stock.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF MMA          ",new 
            ColumnSpacing(15),pnd_A_Mma.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Mma.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Mma.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Mma.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Mma.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Social Choice",new 
            ColumnSpacing(15),pnd_A_Social.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Social.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Social.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Social.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Social.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Bond         ",new 
            ColumnSpacing(15),pnd_A_Bond.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Bond.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Bond.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Bond.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Bond.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Global       ",new 
            ColumnSpacing(15),pnd_A_Global.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Global.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Global.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Global.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Global.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Growth       ",new 
            ColumnSpacing(15),pnd_A_Grth.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Grth.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Grth.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Grth.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Grth.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Equity Index ",new 
            ColumnSpacing(15),pnd_A_Equ.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Equ.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Equ.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Equ.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Equ.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF ILB          ",new 
            ColumnSpacing(15),pnd_A_Ilb.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Ilb.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Ilb.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Ilb.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Ilb.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        //*  HK 12/21/00
        getReports().write(1, ReportOption.NOTITLE,"MONTHLY REVALUATION",NEWLINE,"-------------------",NEWLINE,"TIAA REA          ",new ColumnSpacing(15),pnd_M_Tiaa_Rea.getValue(1),  //Natural: WRITE ( 1 ) 'MONTHLY REVALUATION' / '-------------------' / 'TIAA REA          ' 15X #M-TIAA-REA ( 1 ) 2X #M-TIAA-REA ( 2 ) 2X #M-TIAA-REA ( 3 ) 2X #M-TIAA-REA ( 4 ) 5X #M-TIAA-REA ( 5 ) / 'TIAA ACCESS       ' 15X #M-TACCESS ( 1 ) 2X #M-TACCESS ( 2 ) 2X #M-TACCESS ( 3 ) 2X #M-TACCESS ( 4 ) 5X #M-TACCESS ( 5 ) // 'CREF Stock        ' 15X #M-STOCK ( 1 ) 2X #M-STOCK ( 2 ) 2X #M-STOCK ( 3 ) 2X #M-STOCK ( 4 ) 5X #M-STOCK ( 5 ) / 'CREF MMA          ' 15X #M-MMA ( 1 ) 2X #M-MMA ( 2 ) 2X #M-MMA ( 3 ) 2X #M-MMA ( 4 ) 5X #M-MMA ( 5 ) / 'CREF Social Choice' 15X #M-SOCIAL ( 1 ) 2X #M-SOCIAL ( 2 ) 2X #M-SOCIAL ( 3 ) 2X #M-SOCIAL ( 4 ) 5X #M-SOCIAL ( 5 ) / 'CREF Bond         ' 15X #M-BOND ( 1 ) 2X #M-BOND ( 2 ) 2X #M-BOND ( 3 ) 2X #M-BOND ( 4 ) 5X #M-BOND ( 5 ) / 'CREF Global       ' 15X #M-GLOBAL ( 1 ) 2X #M-GLOBAL ( 2 ) 2X #M-GLOBAL ( 3 ) 2X #M-GLOBAL ( 4 ) 5X #M-GLOBAL ( 5 ) / 'CREF Growth       ' 15X #M-GRTH ( 1 ) 2X #M-GRTH ( 2 ) 2X #M-GRTH ( 3 ) 2X #M-GRTH ( 4 ) 5X #M-GRTH ( 5 ) / 'CREF Equity Index ' 15X #M-EQU ( 1 ) 2X #M-EQU ( 2 ) 2X #M-EQU ( 3 ) 2X #M-EQU ( 4 ) 5X #M-EQU ( 5 ) / 'CREF ILB          ' 15X #M-ILB ( 1 ) 2X #M-ILB ( 2 ) 2X #M-ILB ( 3 ) 2X #M-ILB ( 4 ) 5X #M-ILB ( 5 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Tiaa_Rea.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Tiaa_Rea.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Tiaa_Rea.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Tiaa_Rea.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA ACCESS       ",new ColumnSpacing(15),pnd_M_Taccess.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Taccess.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Taccess.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Taccess.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Taccess.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"CREF Stock        ",new ColumnSpacing(15),pnd_M_Stock.getValue(1), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Stock.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Stock.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Stock.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Stock.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF MMA          ",new ColumnSpacing(15),pnd_M_Mma.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Mma.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Mma.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Mma.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Mma.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Social Choice",new ColumnSpacing(15),pnd_M_Social.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Social.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Social.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Social.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Social.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Bond         ",new ColumnSpacing(15),pnd_M_Bond.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Bond.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Bond.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Bond.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Bond.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Global       ",new ColumnSpacing(15),pnd_M_Global.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Global.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Global.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Global.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Global.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Growth       ",new ColumnSpacing(15),pnd_M_Grth.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Grth.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Grth.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Grth.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Grth.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF Equity Index ",new ColumnSpacing(15),pnd_M_Equ.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Equ.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Equ.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Equ.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Equ.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF ILB          ",new ColumnSpacing(15),pnd_M_Ilb.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Ilb.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Ilb.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Ilb.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Ilb.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,"SUMMARY TOTALS",new TabSetting(103),"GRAND TOTALS",NEWLINE,"--------------",new TabSetting(103),"------------",NEWLINE,"TRADITIONAL ANNUITY",NEWLINE,"-------------------",NEWLINE,new  //Natural: WRITE ( 1 ) 'SUMMARY TOTALS' 103T 'GRAND TOTALS' / '--------------' 103T '------------' / 'TRADITIONAL ANNUITY' / '-------------------' / 2X 'Contracts Issued      ' 16X #CONTRACT-ISSUED ( 1 ) 9X #CONTRACT-ISSUED ( 2 ) 9X #CONTRACT-ISSUED ( 3 ) 9X #CONTRACT-ISSUED ( 4 ) 12X #CONTRACT-ISSUED ( 5 )
            ColumnSpacing(2),"Contracts Issued      ",new ColumnSpacing(16),pnd_Contract_Issued.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued.getValue(2), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued.getValue(3), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued.getValue(4), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Contract_Issued.getValue(5), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Accumulation Applied  ",                                                 //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Accumulation Applied  ' 07X #DA-TIAA-PROCEEDS-AMT ( 1 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT ( 2 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT ( 3 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT ( 4 ) ( IC = $ ) 4X #DA-TIAA-PROCEEDS-AMT ( 5 ) ( IC = $ )
        		new ColumnSpacing(7),pnd_Da_Tiaa_Proceeds_Amt.getValue(1), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt.getValue(2), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt.getValue(3), 
            new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Da_Tiaa_Proceeds_Amt.getValue(5), 
            new InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Annuity Income ($)    ",                                                 //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Annuity Income ($)    ' 07X #TRAD-ANNUITY ( 1 ) ( IC = $ ) #TRAD-ANNUITY ( 2 ) ( IC = $ ) #TRAD-ANNUITY ( 3 ) ( IC = $ ) #TRAD-ANNUITY ( 4 ) ( IC = $ ) 4X #TRAD-ANNUITY ( 5 ) ( IC = $ )
        		new ColumnSpacing(7),pnd_Trad_Annuity.getValue(1), new InsertionCharacter("$"),pnd_Trad_Annuity.getValue(2), new InsertionCharacter("$"),pnd_Trad_Annuity.getValue(3), 
            new InsertionCharacter("$"),pnd_Trad_Annuity.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Trad_Annuity.getValue(5), new 
            InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"TPA",NEWLINE,"----",NEWLINE,new ColumnSpacing(2),"Contracts Issued      ",new ColumnSpacing(16),pnd_Contract_Issued_Tpa.getValue(1),  //Natural: WRITE ( 1 ) / 'TPA' / '----' / 2X 'Contracts Issued      ' 16X #CONTRACT-ISSUED-TPA ( 1 ) 9X #CONTRACT-ISSUED-TPA ( 2 ) 9X #CONTRACT-ISSUED-TPA ( 3 ) 9X #CONTRACT-ISSUED-TPA ( 4 ) 12X #CONTRACT-ISSUED-TPA ( 5 )
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Tpa.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Tpa.getValue(3), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Tpa.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Contract_Issued_Tpa.getValue(5), 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Accumulation Applied  ",                                                 //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Accumulation Applied  ' 07X #DA-TIAA-PROCEEDS-AMT-TPA ( 1 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-TPA ( 2 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-TPA ( 3 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-TPA ( 4 ) ( IC = $ ) 4X #DA-TIAA-PROCEEDS-AMT-TPA ( 5 ) ( IC = $ )
        		new ColumnSpacing(7),pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(1), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(2), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(3), 
            new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Da_Tiaa_Proceeds_Amt_Tpa.getValue(5), 
            new InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Annuity Income ($)    ",                                                 //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Annuity Income ($)    ' 07X #TRAD-ANNUITY-TPA ( 1 ) ( IC = $ ) #TRAD-ANNUITY-TPA ( 2 ) ( IC = $ ) #TRAD-ANNUITY-TPA ( 3 ) ( IC = $ ) #TRAD-ANNUITY-TPA ( 4 ) ( IC = $ ) 4X #TRAD-ANNUITY-TPA ( 5 ) ( IC = $ )
        		new ColumnSpacing(7),pnd_Trad_Annuity_Tpa.getValue(1), new InsertionCharacter("$"),pnd_Trad_Annuity_Tpa.getValue(2), new InsertionCharacter("$"),pnd_Trad_Annuity_Tpa.getValue(3), 
            new InsertionCharacter("$"),pnd_Trad_Annuity_Tpa.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Trad_Annuity_Tpa.getValue(5), 
            new InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"IPRO",NEWLINE,"----",NEWLINE,new ColumnSpacing(2),"Contracts Issued      ",new ColumnSpacing(16),pnd_Contract_Issued_Ipro.getValue(1),  //Natural: WRITE ( 1 ) / 'IPRO' / '----' / 2X 'Contracts Issued      ' 16X #CONTRACT-ISSUED-IPRO ( 1 ) 9X #CONTRACT-ISSUED-IPRO ( 2 ) 9X #CONTRACT-ISSUED-IPRO ( 3 ) 9X #CONTRACT-ISSUED-IPRO ( 4 ) 12X #CONTRACT-ISSUED-IPRO ( 5 )
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Ipro.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Ipro.getValue(3), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_Ipro.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Contract_Issued_Ipro.getValue(5), 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Accumulation Applied  ",                                                 //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Accumulation Applied  ' 07X #DA-TIAA-PROCEEDS-AMT-IPRO ( 1 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-IPRO ( 2 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-IPRO ( 3 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-IPRO ( 4 ) ( IC = $ ) 4X #DA-TIAA-PROCEEDS-AMT-IPRO ( 5 ) ( IC = $ )
        		new ColumnSpacing(7),pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(1), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(2), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(3), 
            new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Da_Tiaa_Proceeds_Amt_Ipro.getValue(5), 
            new InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().display(1, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Annuity Income ($)    ",                                                 //Natural: DISPLAY ( 1 ) NOHDR ( HW = OFF ) 2X 'Annuity Income ($)    ' 07X #TRAD-ANNUITY-IPRO ( 1 ) ( IC = $ ) #TRAD-ANNUITY-IPRO ( 2 ) ( IC = $ ) #TRAD-ANNUITY-IPRO ( 3 ) ( IC = $ ) #TRAD-ANNUITY-IPRO ( 4 ) ( IC = $ ) 4X #TRAD-ANNUITY-IPRO ( 5 ) ( IC = $ )
        		new ColumnSpacing(7),pnd_Trad_Annuity_Ipro.getValue(1), new InsertionCharacter("$"),pnd_Trad_Annuity_Ipro.getValue(2), new InsertionCharacter("$"),pnd_Trad_Annuity_Ipro.getValue(3), 
            new InsertionCharacter("$"),pnd_Trad_Annuity_Ipro.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Trad_Annuity_Ipro.getValue(5), 
            new InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        //*  TACC KG
        //*  TACC KG
        //*  TACC KG
        //*  TACC KG
        getReports().write(1, ReportOption.NOTITLE,"REAL ESTATE AND ACCESS ACCOUNTS",NEWLINE,"-------------------------------",NEWLINE,new ColumnSpacing(2),"Certificate Issued    ",new  //Natural: WRITE ( 1 ) 'REAL ESTATE AND ACCESS ACCOUNTS' / '-------------------------------' / 2X 'Certificate Issued    ' 16X #CERTIF-ISSUED ( 1 ) 9X #CERTIF-ISSUED ( 2 ) 9X #CERTIF-ISSUED ( 3 ) 9X #CERTIF-ISSUED ( 4 ) 12X #CERTIF-ISSUED ( 5 ) / 2X 'Accumulation Applied  ' 09X #DA-REA-PROCEEDS-AMT ( 1 ) 2X #DA-REA-PROCEEDS-AMT ( 2 ) 2X #DA-REA-PROCEEDS-AMT ( 3 ) 2X #DA-REA-PROCEEDS-AMT ( 4 ) 5X #DA-REA-PROCEEDS-AMT ( 5 ) / 2X 'Annuity Income (units)- REA' 04X #REAL-ANNUITY ( 1 ) 2X #REAL-ANNUITY ( 2 ) 2X #REAL-ANNUITY ( 3 ) 2X #REAL-ANNUITY ( 4 ) 5X #REAL-ANNUITY ( 5 ) / 2X 'Annuity Income (units)- ACC' 04X #TACC-ANNUITY ( 1 ) 2X #TACC-ANNUITY ( 2 ) 2X #TACC-ANNUITY ( 3 ) 2X #TACC-ANNUITY ( 4 ) 5X #TACC-ANNUITY ( 5 ) / 'CREF' / '----' / 2X 'Certificate Issued    ' 16X #CERTIF-ISSUED2 ( 1 ) 9X #CERTIF-ISSUED2 ( 2 ) 9X #CERTIF-ISSUED2 ( 3 ) 9X #CERTIF-ISSUED2 ( 4 ) 12X #CERTIF-ISSUED2 ( 5 ) / 2X 'Accumulation Applied  ' 09X #DA-CREF-PROCEEDS-AMT ( 1 ) 2X #DA-CREF-PROCEEDS-AMT ( 2 ) 2X #DA-CREF-PROCEEDS-AMT ( 3 ) 2X #DA-CREF-PROCEEDS-AMT ( 4 ) 5X #DA-CREF-PROCEEDS-AMT ( 5 ) / 2X 'Annuity Income (units)' 09X #CREF-ANNUITY ( 1 ) 2X #CREF-ANNUITY ( 2 ) 2X #CREF-ANNUITY ( 3 ) 2X #CREF-ANNUITY ( 4 ) 5X #CREF-ANNUITY ( 5 )
            ColumnSpacing(16),pnd_Certif_Issued.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued.getValue(2), new ReportEditMask 
            ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued.getValue(3), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued.getValue(4), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Certif_Issued.getValue(5), new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(2),"Accumulation Applied  ",new 
            ColumnSpacing(9),pnd_Da_Rea_Proceeds_Amt.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Rea_Proceeds_Amt.getValue(2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Rea_Proceeds_Amt.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Rea_Proceeds_Amt.getValue(4), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),pnd_Da_Rea_Proceeds_Amt.getValue(5), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            ColumnSpacing(2),"Annuity Income (units)- REA",new ColumnSpacing(4),pnd_Real_Annuity.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_Real_Annuity.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Real_Annuity.getValue(3), new 
            ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Real_Annuity.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_Real_Annuity.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,new ColumnSpacing(2),"Annuity Income (units)- ACC",new ColumnSpacing(4),pnd_Tacc_Annuity.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Tacc_Annuity.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Tacc_Annuity.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Tacc_Annuity.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_Tacc_Annuity.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF",NEWLINE,"----",NEWLINE,new ColumnSpacing(2),"Certificate Issued    ",new ColumnSpacing(16),pnd_Certif_Issued2.getValue(1), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued2.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued2.getValue(3), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued2.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Certif_Issued2.getValue(5), 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(2),"Accumulation Applied  ",new ColumnSpacing(9),pnd_Da_Cref_Proceeds_Amt.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Cref_Proceeds_Amt.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Cref_Proceeds_Amt.getValue(3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Cref_Proceeds_Amt.getValue(4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),pnd_Da_Cref_Proceeds_Amt.getValue(5), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(2),"Annuity Income (units)",new ColumnSpacing(9),pnd_Cref_Annuity.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Cref_Annuity.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Cref_Annuity.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Cref_Annuity.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_Cref_Annuity.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().skip(1, 1);                                                                                                                                          //Natural: SKIP ( 1 ) 1
        getReports().write(1, ReportOption.NOTITLE,"CONTRACT SETS          ",new ColumnSpacing(17),pnd_No_Rec.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new             //Natural: WRITE ( 1 ) 'CONTRACT SETS          ' 17X #NO-REC ( 1 ) 9X #NO-REC ( 2 ) 9X #NO-REC ( 3 ) 9X #NO-REC ( 4 ) 12X #NO-REC ( 5 )
            ColumnSpacing(9),pnd_No_Rec.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_No_Rec.getValue(3), new ReportEditMask ("ZZZ,ZZ9"),new 
            ColumnSpacing(9),pnd_No_Rec.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_No_Rec.getValue(5), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Ia_Report_T() throws Exception                                                                                                                       //Natural: IA-REPORT-T
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
                                                                                                                                                                          //Natural: PERFORM ISSUED-TOTALS-T
        sub_Issued_Totals_T();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ISSUED-TOTALS-T-TPA
        sub_Issued_Totals_T_Tpa();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ISSUED-TOTALS-T-IPRO
        sub_Issued_Totals_T_Ipro();
        if (condition(Global.isEscape())) {return;}
        getReports().write(6, ReportOption.NOTITLE,"INCOME BENEFITS", new FieldAttributes ("AD=I"),NEWLINE,"---------------");                                            //Natural: WRITE ( 6 ) 'INCOME BENEFITS' ( AD = I ) / '---------------'
        if (Global.isEscape()) return;
        getReports().display(6, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Standard",                                                              //Natural: DISPLAY ( 6 ) NOHDR ( HW = OFF ) 'TRADITIONAL ANNUITY - Standard' 1X #GRNTED-STD-AMT-T ( 1 ) ( IC = $ ) #GRNTED-STD-AMT-T ( 2 ) ( IC = $ ) #GRNTED-STD-AMT-T ( 3 ) ( IC = $ ) #GRNTED-STD-AMT-T ( 4 ) ( IC = $ ) 4X #GRNTED-STD-AMT-TOT-T ( IC = $ )
        		new ColumnSpacing(1),pnd_Grnted_Std_Amt_T.getValue(1), new InsertionCharacter("$"),pnd_Grnted_Std_Amt_T.getValue(2), new InsertionCharacter("$"),pnd_Grnted_Std_Amt_T.getValue(3), 
            new InsertionCharacter("$"),pnd_Grnted_Std_Amt_T.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Grnted_Std_Amt_Tot_T, new 
            InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().display(6, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Graded  ",                                                              //Natural: DISPLAY ( 6 ) NOHDR ( HW = OFF ) 'TRADITIONAL ANNUITY - Graded  ' 1X #GRNTED-GRD-AMT-T ( 1 ) ( IC = $ ) #GRNTED-GRD-AMT-T ( 2 ) ( IC = $ ) #GRNTED-GRD-AMT-T ( 3 ) ( IC = $ ) #GRNTED-GRD-AMT-T ( 4 ) ( IC = $ ) 4X #GRNTED-GRD-AMT-TOT-T ( IC = $ )
        		new ColumnSpacing(1),pnd_Grnted_Grd_Amt_T.getValue(1), new InsertionCharacter("$"),pnd_Grnted_Grd_Amt_T.getValue(2), new InsertionCharacter("$"),pnd_Grnted_Grd_Amt_T.getValue(3), 
            new InsertionCharacter("$"),pnd_Grnted_Grd_Amt_T.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Grnted_Grd_Amt_Tot_T, new 
            InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().skip(6, 1);                                                                                                                                          //Natural: SKIP ( 6 ) 1
        getReports().write(6, ReportOption.NOTITLE,"ANNUAL REVALUATION", new FieldAttributes ("AD=I"),NEWLINE,"------------------",NEWLINE,"TIAA REA          ",          //Natural: WRITE ( 6 ) 'ANNUAL REVALUATION' ( AD = I ) / '------------------' / 'TIAA REA          ' ( AD = I ) 15X #A-TIAA-REA-T ( 1 ) 2X #A-TIAA-REA-T ( 2 ) 2X #A-TIAA-REA-T ( 3 ) 2X #A-TIAA-REA-T ( 4 ) 5X #A-TIAA-REA-T ( 5 ) / 'TIAA ACCESS       ' 15X #A-TACCESS-T ( 1 ) 2X #A-TACCESS-T ( 2 ) 2X #A-TACCESS-T ( 3 ) 2X #A-TACCESS-T ( 4 ) 5X #A-TACCESS-T ( 5 ) // 'CREF STOCK        ' 15X #A-STOCK-T ( 1 ) 2X #A-STOCK-T ( 2 ) 2X #A-STOCK-T ( 3 ) 2X #A-STOCK-T ( 4 ) 5X #A-STOCK-T ( 5 ) / 'CREF MMA          ' 15X #A-MMA-T ( 1 ) 2X #A-MMA-T ( 2 ) 2X #A-MMA-T ( 3 ) 2X #A-MMA-T ( 4 ) 5X #A-MMA-T ( 5 ) / 'CREF SOCIAL CHOICE' 15X #A-SOCIAL-T ( 1 ) 2X #A-SOCIAL-T ( 2 ) 2X #A-SOCIAL-T ( 3 ) 2X #A-SOCIAL-T ( 4 ) 5X #A-SOCIAL-T ( 5 ) / 'CREF BOND         ' 15X #A-BOND-T ( 1 ) 2X #A-BOND-T ( 2 ) 2X #A-BOND-T ( 3 ) 2X #A-BOND-T ( 4 ) 5X #A-BOND-T ( 5 ) / 'CREF GLOBAL       ' 15X #A-GLOBAL-T ( 1 ) 2X #A-GLOBAL-T ( 2 ) 2X #A-GLOBAL-T ( 3 ) 2X #A-GLOBAL-T ( 4 ) 5X #A-GLOBAL-T ( 5 ) / 'CREF GROWTH       ' 15X #A-GRTH-T ( 1 ) 2X #A-GRTH-T ( 2 ) 2X #A-GRTH-T ( 3 ) 2X #A-GRTH-T ( 4 ) 5X #A-GRTH-T ( 5 ) / 'CREF EQUITY INDEX ' 15X #A-EQU-T ( 1 ) 2X #A-EQU-T ( 2 ) 2X #A-EQU-T ( 3 ) 2X #A-EQU-T ( 4 ) 5X #A-EQU-T ( 5 ) / 'CREF ILB          ' 15X #A-ILB-T ( 1 ) 2X #A-ILB-T ( 2 ) 2X #A-ILB-T ( 3 ) 2X #A-ILB-T ( 4 ) 5X #A-ILB-T ( 5 )
            new FieldAttributes ("AD=I"),new ColumnSpacing(15),pnd_A_Tiaa_Rea_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Tiaa_Rea_T.getValue(2), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Tiaa_Rea_T.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Tiaa_Rea_T.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Tiaa_Rea_T.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA ACCESS       ",new 
            ColumnSpacing(15),pnd_A_Taccess_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Taccess_T.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Taccess_T.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Taccess_T.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Taccess_T.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"CREF STOCK        ",new 
            ColumnSpacing(15),pnd_A_Stock_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Stock_T.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Stock_T.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Stock_T.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Stock_T.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF MMA          ",new 
            ColumnSpacing(15),pnd_A_Mma_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Mma_T.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Mma_T.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Mma_T.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Mma_T.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF SOCIAL CHOICE",new 
            ColumnSpacing(15),pnd_A_Social_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Social_T.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Social_T.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Social_T.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Social_T.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF BOND         ",new 
            ColumnSpacing(15),pnd_A_Bond_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Bond_T.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Bond_T.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Bond_T.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Bond_T.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF GLOBAL       ",new 
            ColumnSpacing(15),pnd_A_Global_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Global_T.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Global_T.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Global_T.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Global_T.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF GROWTH       ",new 
            ColumnSpacing(15),pnd_A_Grth_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Grth_T.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Grth_T.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Grth_T.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Grth_T.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF EQUITY INDEX ",new 
            ColumnSpacing(15),pnd_A_Equ_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Equ_T.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Equ_T.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Equ_T.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Equ_T.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF ILB          ",new 
            ColumnSpacing(15),pnd_A_Ilb_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Ilb_T.getValue(2), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Ilb_T.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_A_Ilb_T.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_A_Ilb_T.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().skip(6, 1);                                                                                                                                          //Natural: SKIP ( 6 ) 1
        getReports().write(6, ReportOption.NOTITLE,"MONTHLY REVALUATION",NEWLINE,"-------------------",NEWLINE,"TIAA REA          ",new ColumnSpacing(15),pnd_M_Tiaa_Rea_T.getValue(1),  //Natural: WRITE ( 6 ) 'MONTHLY REVALUATION' / '-------------------' / 'TIAA REA          ' 15X #M-TIAA-REA-T ( 1 ) 2X #M-TIAA-REA-T ( 2 ) 2X #M-TIAA-REA-T ( 3 ) 2X #M-TIAA-REA-T ( 4 ) 5X #M-TIAA-REA-T ( 5 ) / 'TIAA ACCESS       ' 15X #M-TACCESS-T ( 1 ) 2X #M-TACCESS-T ( 2 ) 2X #M-TACCESS-T ( 3 ) 2X #M-TACCESS-T ( 4 ) 5X #M-TACCESS-T ( 5 ) // 'CREF STOCK        ' 15X #M-STOCK-T ( 1 ) 2X #M-STOCK-T ( 2 ) 2X #M-STOCK-T ( 3 ) 2X #M-STOCK-T ( 4 ) 5X #M-STOCK-T ( 5 ) / 'CREF MMA          ' 15X #M-MMA-T ( 1 ) 2X #M-MMA-T ( 2 ) 2X #M-MMA-T ( 3 ) 2X #M-MMA-T ( 4 ) 5X #M-MMA-T ( 5 ) / 'CREF SOCIAL CHOICE' 15X #M-SOCIAL-T ( 1 ) 2X #M-SOCIAL-T ( 2 ) 2X #M-SOCIAL-T ( 3 ) 2X #M-SOCIAL-T ( 4 ) 5X #M-SOCIAL-T ( 5 ) / 'CREF BOND         ' 15X #M-BOND-T ( 1 ) 2X #M-BOND-T ( 2 ) 2X #M-BOND-T ( 3 ) 2X #M-BOND-T ( 4 ) 5X #M-BOND-T ( 5 ) / 'CREF GLOBAL       ' 15X #M-GLOBAL-T ( 1 ) 2X #M-GLOBAL-T ( 2 ) 2X #M-GLOBAL-T ( 3 ) 2X #M-GLOBAL-T ( 4 ) 5X #M-GLOBAL-T ( 5 ) / 'CREF GROWTH       ' 15X #M-GRTH-T ( 1 ) 2X #M-GRTH-T ( 2 ) 2X #M-GRTH-T ( 3 ) 2X #M-GRTH-T ( 4 ) 5X #M-GRTH-T ( 5 ) / 'CREF EQUITY INDEX ' 15X #M-EQU-T ( 1 ) 2X #M-EQU-T ( 2 ) 2X #M-EQU-T ( 3 ) 2X #M-EQU-T ( 4 ) 5X #M-EQU-T ( 5 ) / 'CREF ILB          ' 15X #M-ILB-T ( 1 ) 2X #M-ILB-T ( 2 ) 2X #M-ILB-T ( 3 ) 2X #M-ILB-T ( 4 ) 5X #M-ILB-T ( 5 )
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Tiaa_Rea_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Tiaa_Rea_T.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Tiaa_Rea_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Tiaa_Rea_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"TIAA ACCESS       ",new ColumnSpacing(15),pnd_M_Taccess_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Taccess_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Taccess_T.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Taccess_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Taccess_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,NEWLINE,"CREF STOCK        ",new ColumnSpacing(15),pnd_M_Stock_T.getValue(1), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Stock_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Stock_T.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Stock_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Stock_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF MMA          ",new ColumnSpacing(15),pnd_M_Mma_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Mma_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Mma_T.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Mma_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Mma_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF SOCIAL CHOICE",new ColumnSpacing(15),pnd_M_Social_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Social_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Social_T.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Social_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Social_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF BOND         ",new ColumnSpacing(15),pnd_M_Bond_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Bond_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Bond_T.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Bond_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Bond_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF GLOBAL       ",new ColumnSpacing(15),pnd_M_Global_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Global_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Global_T.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Global_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Global_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF GROWTH       ",new ColumnSpacing(15),pnd_M_Grth_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Grth_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Grth_T.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Grth_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Grth_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF EQUITY INDEX ",new ColumnSpacing(15),pnd_M_Equ_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Equ_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Equ_T.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Equ_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Equ_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF ILB          ",new ColumnSpacing(15),pnd_M_Ilb_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(2),pnd_M_Ilb_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Ilb_T.getValue(3), new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_M_Ilb_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_M_Ilb_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(6));                                                                                                                 //Natural: NEWPAGE ( 6 )
        if (condition(Global.isEscape())){return;}
        getReports().write(6, ReportOption.NOTITLE,"SUMMARY TOTALS",new TabSetting(103),"GRAND TOTALS",NEWLINE,"--------------",new TabSetting(103),"------------",NEWLINE,"TRADITIONAL ANNUITY",NEWLINE,"-------------------",NEWLINE,new  //Natural: WRITE ( 6 ) 'SUMMARY TOTALS' 103T 'GRAND TOTALS' / '--------------' 103T '------------' / 'TRADITIONAL ANNUITY' / '-------------------' / 2X 'Contracts Issued      ' 16X #CONTRACT-ISSUED-T ( 1 ) 9X #CONTRACT-ISSUED-T ( 2 ) 9X #CONTRACT-ISSUED-T ( 3 ) 9X #CONTRACT-ISSUED-T ( 4 ) 12X #CONTRACT-ISSUED-T ( 5 )
            ColumnSpacing(2),"Contracts Issued      ",new ColumnSpacing(16),pnd_Contract_Issued_T.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_T.getValue(2), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_T.getValue(3), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Contract_Issued_T.getValue(4), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Contract_Issued_T.getValue(5), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().display(6, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Accumulation Applied  ",                                                 //Natural: DISPLAY ( 6 ) NOHDR ( HW = OFF ) 2X 'Accumulation Applied  ' 07X #DA-TIAA-PROCEEDS-AMT-T ( 1 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-T ( 2 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-T ( 3 ) ( IC = $ ) #DA-TIAA-PROCEEDS-AMT-T ( 4 ) ( IC = $ ) 4X #DA-TIAA-PROCEEDS-AMT-T ( 5 ) ( IC = $ )
        		new ColumnSpacing(7),pnd_Da_Tiaa_Proceeds_Amt_T.getValue(1), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_T.getValue(2), new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_T.getValue(3), 
            new InsertionCharacter("$"),pnd_Da_Tiaa_Proceeds_Amt_T.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Da_Tiaa_Proceeds_Amt_T.getValue(5), 
            new InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().display(6, new HeadingWidth(false),ReportOption.NOHDR,new ColumnSpacing(2),"Annuity Income ($)    ",                                                 //Natural: DISPLAY ( 6 ) NOHDR ( HW = OFF ) 2X 'Annuity Income ($)    ' 07X #TRAD-ANNUITY-T ( 1 ) ( IC = $ ) #TRAD-ANNUITY-T ( 2 ) ( IC = $ ) #TRAD-ANNUITY-T ( 3 ) ( IC = $ ) #TRAD-ANNUITY-T ( 4 ) ( IC = $ ) 4X #TRAD-ANNUITY-T ( 5 ) ( IC = $ )
        		new ColumnSpacing(7),pnd_Trad_Annuity_T.getValue(1), new InsertionCharacter("$"),pnd_Trad_Annuity_T.getValue(2), new InsertionCharacter("$"),pnd_Trad_Annuity_T.getValue(3), 
            new InsertionCharacter("$"),pnd_Trad_Annuity_T.getValue(4), new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Trad_Annuity_T.getValue(5), 
            new InsertionCharacter("$"));
        if (Global.isEscape()) return;
        getReports().skip(6, 1);                                                                                                                                          //Natural: SKIP ( 6 ) 1
        //*  TACC KG
        //*  TACC KG
        //*  TACC KG
        //*  TACC KG
        getReports().write(6, ReportOption.NOTITLE,"REAL ESTATE AND ACCESS ACCOUNTS",NEWLINE,"-------------------------------",NEWLINE,new ColumnSpacing(2),"Certificate Issued    ",new  //Natural: WRITE ( 6 ) 'REAL ESTATE AND ACCESS ACCOUNTS' / '-------------------------------' / 2X 'Certificate Issued    ' 16X #CERTIF-ISSUED-T ( 1 ) 9X #CERTIF-ISSUED-T ( 2 ) 9X #CERTIF-ISSUED-T ( 3 ) 9X #CERTIF-ISSUED-T ( 4 ) 12X #CERTIF-ISSUED-T ( 5 ) / 2X 'Accumulation Applied  ' 09X #DA-REA-PROCEEDS-AMT-T ( 1 ) 2X #DA-REA-PROCEEDS-AMT-T ( 2 ) 2X #DA-REA-PROCEEDS-AMT-T ( 3 ) 2X #DA-REA-PROCEEDS-AMT-T ( 4 ) 5X #DA-REA-PROCEEDS-AMT-T ( 5 ) / 2X 'Annuity Income (units)- REA' 04X #REAL-ANNUITY-T ( 1 ) 2X #REAL-ANNUITY-T ( 2 ) 2X #REAL-ANNUITY-T ( 3 ) 2X #REAL-ANNUITY-T ( 4 ) 5X #REAL-ANNUITY-T ( 5 ) / 2X 'Annuity Income (units)- ACC' 04X #TACC-ANNUITY-T ( 1 ) 2X #TACC-ANNUITY-T ( 2 ) 2X #TACC-ANNUITY-T ( 3 ) 2X #TACC-ANNUITY-T ( 4 ) 5X #TACC-ANNUITY-T ( 5 ) / 'CREF' / '----' / 2X 'Certificate Issued    ' 16X #CERTIF-ISSUED2-T ( 1 ) 9X #CERTIF-ISSUED2-T ( 2 ) 9X #CERTIF-ISSUED2-T ( 3 ) 9X #CERTIF-ISSUED2-T ( 4 ) 12X #CERTIF-ISSUED2-T ( 5 ) / 2X 'Accumulation Applied  ' 09X #DA-CREF-PROCEEDS-AMT-T ( 1 ) 2X #DA-CREF-PROCEEDS-AMT-T ( 2 ) 2X #DA-CREF-PROCEEDS-AMT-T ( 3 ) 2X #DA-CREF-PROCEEDS-AMT-T ( 4 ) 5X #DA-CREF-PROCEEDS-AMT-T ( 5 ) / 2X 'Annuity Income (units)' 09X #CREF-ANNUITY-T ( 1 ) 2X #CREF-ANNUITY-T ( 2 ) 2X #CREF-ANNUITY-T ( 3 ) 2X #CREF-ANNUITY-T ( 4 ) 5X #CREF-ANNUITY-T ( 5 )
            ColumnSpacing(16),pnd_Certif_Issued_T.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued_T.getValue(2), new 
            ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued_T.getValue(3), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued_T.getValue(4), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Certif_Issued_T.getValue(5), new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(2),"Accumulation Applied  ",new 
            ColumnSpacing(9),pnd_Da_Rea_Proceeds_Amt_T.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Rea_Proceeds_Amt_T.getValue(2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Rea_Proceeds_Amt_T.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(2),pnd_Da_Rea_Proceeds_Amt_T.getValue(4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),pnd_Da_Rea_Proceeds_Amt_T.getValue(5), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(2),"Annuity Income (units)- REA",new ColumnSpacing(4),pnd_Real_Annuity_T.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Real_Annuity_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Real_Annuity_T.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Real_Annuity_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_Real_Annuity_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,new ColumnSpacing(2),"Annuity Income (units)- ACC",new ColumnSpacing(4),pnd_Tacc_Annuity_T.getValue(1), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Tacc_Annuity_T.getValue(2), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Tacc_Annuity_T.getValue(3), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Tacc_Annuity_T.getValue(4), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_Tacc_Annuity_T.getValue(5), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"CREF",NEWLINE,"----",NEWLINE,new ColumnSpacing(2),"Certificate Issued    ",new ColumnSpacing(16),pnd_Certif_Issued2_T.getValue(1), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued2_T.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued2_T.getValue(3), 
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_Certif_Issued2_T.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_Certif_Issued2_T.getValue(5), 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(2),"Accumulation Applied  ",new ColumnSpacing(9),pnd_Da_Cref_Proceeds_Amt_T.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Cref_Proceeds_Amt_T.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(2),pnd_Da_Cref_Proceeds_Amt_T.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Da_Cref_Proceeds_Amt_T.getValue(4), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),pnd_Da_Cref_Proceeds_Amt_T.getValue(5), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            ColumnSpacing(2),"Annuity Income (units)",new ColumnSpacing(9),pnd_Cref_Annuity_T.getValue(1), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Cref_Annuity_T.getValue(2), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Cref_Annuity_T.getValue(3), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(2),pnd_Cref_Annuity_T.getValue(4), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(5),pnd_Cref_Annuity_T.getValue(5), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().skip(6, 1);                                                                                                                                          //Natural: SKIP ( 6 ) 1
        getReports().write(6, ReportOption.NOTITLE,"CONTRACT SETS          ",new ColumnSpacing(17),pnd_No_Rec_T.getValue(1), new ReportEditMask ("ZZZ,ZZ9"),new           //Natural: WRITE ( 6 ) 'CONTRACT SETS          ' 17X #NO-REC-T ( 1 ) 9X #NO-REC-T ( 2 ) 9X #NO-REC-T ( 3 ) 9X #NO-REC-T ( 4 ) 12X #NO-REC-T ( 5 )
            ColumnSpacing(9),pnd_No_Rec_T.getValue(2), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_No_Rec_T.getValue(3), new ReportEditMask 
            ("ZZZ,ZZ9"),new ColumnSpacing(9),pnd_No_Rec_T.getValue(4), new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(12),pnd_No_Rec_T.getValue(5), new 
            ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    //*  HK 12/21/00
    //*  HK 12/21/00
    //*  TACC KG
    private void sub_Ia_Daily() throws Exception                                                                                                                          //Natural: IA-DAILY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ia_Total.compute(new ComputeParameters(false, pnd_Ia_Total), pnd_N_Rec.getValue(1).add(pnd_N_Rec.getValue(2)).add(pnd_N_Rec.getValue(3)).add(pnd_N_Rec.getValue(4)).add(pnd_N_Rec.getValue(5)).add(pnd_N_Rec.getValue(6)).add(pnd_N_Rec.getValue(7)).add(pnd_N_Rec.getValue(8)).add(pnd_N_Rec.getValue(9))); //Natural: ASSIGN #IA-TOTAL := #N-REC ( 1 ) + #N-REC ( 2 ) + #N-REC ( 3 ) + #N-REC ( 4 ) + #N-REC ( 5 ) + #N-REC ( 6 ) + #N-REC ( 7 ) + #N-REC ( 8 ) + #N-REC ( 9 )
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(10),"TOTAL NUMBER OF REQUESTED PROCESSED:",pnd_Ia_Rq_Pr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new  //Natural: WRITE ( 2 ) 10T 'TOTAL NUMBER OF REQUESTED PROCESSED:' #IA-RQ-PR // 10T 'IA CONTRACTS ISSUED:' // 10T 'LIFE ANNUITY:       ' #N-REC ( 1 ) // 10T 'INSTALLMENT REFUND: ' #N-REC ( 2 ) // 10T 'ANNUITY CERTAIN:    ' #N-REC ( 3 ) // 10T 'TPA:                ' #N-REC ( 7 ) // 10T 'IPRO:               ' #N-REC ( 8 ) // 10T 'FULL BENEFIT:       ' #N-REC ( 4 ) // 10T 'HALF BENEFIT:       ' #N-REC ( 5 ) // 10T '2/3  BENEFIT:       ' #N-REC ( 6 ) // 10T '3/4  BENEFIT:       ' #N-REC ( 9 ) // 10T 'TOTAL IA CONTRACTS ISSUED:          ' #IA-TOTAL /
            TabSetting(10),"IA CONTRACTS ISSUED:",NEWLINE,NEWLINE,new TabSetting(10),"LIFE ANNUITY:       ",pnd_N_Rec.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(10),"INSTALLMENT REFUND: ",pnd_N_Rec.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"ANNUITY CERTAIN:    ",pnd_N_Rec.getValue(3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"TPA:                ",pnd_N_Rec.getValue(7), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(10),"IPRO:               ",pnd_N_Rec.getValue(8), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"FULL BENEFIT:       ",pnd_N_Rec.getValue(4), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"HALF BENEFIT:       ",pnd_N_Rec.getValue(5), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(10),"2/3  BENEFIT:       ",pnd_N_Rec.getValue(6), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"3/4  BENEFIT:       ",pnd_N_Rec.getValue(9), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"TOTAL IA CONTRACTS ISSUED:          ",pnd_Ia_Total, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
    }
    //*  HK 12/21/00
    //*  HK 12/21/00
    //*  TACC KG
    private void sub_Ia_Daily_T() throws Exception                                                                                                                        //Natural: IA-DAILY-T
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ia_Total_T.compute(new ComputeParameters(false, pnd_Ia_Total_T), pnd_T_Rec.getValue(1).add(pnd_T_Rec.getValue(2)).add(pnd_T_Rec.getValue(3)).add(pnd_T_Rec.getValue(4)).add(pnd_T_Rec.getValue(5)).add(pnd_T_Rec.getValue(6)).add(pnd_T_Rec.getValue(7)).add(pnd_T_Rec.getValue(8)).add(pnd_T_Rec.getValue(9))); //Natural: ASSIGN #IA-TOTAL-T := #T-REC ( 1 ) + #T-REC ( 2 ) + #T-REC ( 3 ) + #T-REC ( 4 ) + #T-REC ( 5 ) + #T-REC ( 6 ) + #T-REC ( 7 ) + #T-REC ( 8 ) + #T-REC ( 9 )
        getReports().write(7, ReportOption.NOTITLE,new TabSetting(10),"TOTAL NUMBER OF REQUESTED PROCESSED:",pnd_Ia_Rq_Pr_T, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new  //Natural: WRITE ( 7 ) 10T 'TOTAL NUMBER OF REQUESTED PROCESSED:' #IA-RQ-PR-T // 10T 'IA CONTRACTS ISSUED:' // 10T 'LIFE ANNUITY:       ' #T-REC ( 1 ) // 10T 'INSTALLMENT REFUND: ' #T-REC ( 2 ) // 10T 'ANNUITY CERTAIN:    ' #T-REC ( 3 ) // 10T 'TPA:                ' #T-REC ( 7 ) // 10T 'IPRO:               ' #T-REC ( 8 ) // 10T 'FULL BENEFIT:       ' #T-REC ( 4 ) // 10T 'HALF BENEFIT:       ' #T-REC ( 5 ) // 10T '2/3  BENEFIT:       ' #T-REC ( 6 ) // 10T '3/4  BENEFIT:       ' #T-REC ( 9 ) // 10T 'TOTAL IA CONTRACTS ISSUED:          ' #IA-TOTAL-T /
            TabSetting(10),"IA CONTRACTS ISSUED:",NEWLINE,NEWLINE,new TabSetting(10),"LIFE ANNUITY:       ",pnd_T_Rec.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(10),"INSTALLMENT REFUND: ",pnd_T_Rec.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"ANNUITY CERTAIN:    ",pnd_T_Rec.getValue(3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"TPA:                ",pnd_T_Rec.getValue(7), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(10),"IPRO:               ",pnd_T_Rec.getValue(8), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"FULL BENEFIT:       ",pnd_T_Rec.getValue(4), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"HALF BENEFIT:       ",pnd_T_Rec.getValue(5), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(10),"2/3  BENEFIT:       ",pnd_T_Rec.getValue(6), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"3/4  BENEFIT:       ",pnd_T_Rec.getValue(9), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"TOTAL IA CONTRACTS ISSUED:          ",pnd_Ia_Total_T, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Mdo_Process_Daily() throws Exception                                                                                                                 //Natural: MDO-PROCESS-DAILY
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Mdo_Rq_Pr.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #MDO-RQ-PR
        if (condition(cis_Cis_Mdo_Contract_Cash_Status.equals("C") && cis_Cis_Mdo_Contract_Type.equals("R")))                                                             //Natural: IF CIS.CIS-MDO-CONTRACT-CASH-STATUS = 'C' AND CIS.CIS-MDO-CONTRACT-TYPE = 'R'
        {
            pnd_M_Rec.getValue(1).nadd(1);                                                                                                                                //Natural: ADD 1 TO #M-REC ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Cis_Mdo_Contract_Cash_Status.equals("N") && cis_Cis_Mdo_Contract_Type.equals("R")))                                                             //Natural: IF CIS.CIS-MDO-CONTRACT-CASH-STATUS = 'N' AND CIS.CIS-MDO-CONTRACT-TYPE = 'R'
        {
            pnd_M_Rec.getValue(2).nadd(1);                                                                                                                                //Natural: ADD 1 TO #M-REC ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Cis_Mdo_Contract_Cash_Status.equals("C") && cis_Cis_Mdo_Contract_Type.equals("S")))                                                             //Natural: IF CIS.CIS-MDO-CONTRACT-CASH-STATUS = 'C' AND CIS.CIS-MDO-CONTRACT-TYPE = 'S'
        {
            pnd_M_Rec.getValue(3).nadd(1);                                                                                                                                //Natural: ADD 1 TO #M-REC ( 3 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Cis_Mdo_Contract_Cash_Status.equals("N") && cis_Cis_Mdo_Contract_Type.equals("S")))                                                             //Natural: IF CIS.CIS-MDO-CONTRACT-CASH-STATUS = 'N' AND CIS.CIS-MDO-CONTRACT-TYPE = 'S'
        {
            pnd_M_Rec.getValue(4).nadd(1);                                                                                                                                //Natural: ADD 1 TO #M-REC ( 4 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mdo_Process() throws Exception                                                                                                                       //Natural: MDO-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(cis_Cis_Cntrct_Type.equals("B")))                                                                                                                   //Natural: IF CIS.CIS-CNTRCT-TYPE = 'B'
        {
            if (condition(cis_Cis_Cntrct_Apprvl_Ind.equals("Y")))                                                                                                         //Natural: IF CIS.CIS-CNTRCT-APPRVL-IND = 'Y'
            {
                pnd_A.setValue(1);                                                                                                                                        //Natural: ASSIGN #A := 1
                pnd_Aprl_Y_N.getValue(pnd_A).setValue("APPROVED STATES");                                                                                                 //Natural: ASSIGN #APRL-Y-N ( #A ) := 'APPROVED STATES'
                pnd_Apprvl.getValue(pnd_A).nadd(1);                                                                                                                       //Natural: ADD 1 TO #APPRVL ( #A )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cis_Cis_Cntrct_Apprvl_Ind.equals("N")))                                                                                                         //Natural: IF CIS.CIS-CNTRCT-APPRVL-IND = 'N'
            {
                pnd_A.setValue(2);                                                                                                                                        //Natural: ASSIGN #A := 2
                pnd_Aprl_Y_N.getValue(pnd_A).setValue("UNAPPROVED STATES");                                                                                               //Natural: ASSIGN #APRL-Y-N ( #A ) := 'UNAPPROVED STATES'
                pnd_Apprvl.getValue(pnd_A).nadd(1);                                                                                                                       //Natural: ADD 1 TO #APPRVL ( #A )
            }                                                                                                                                                             //Natural: END-IF
            //*  IF CIS.CIS-CNTRCT-APPRVL-IND = 'Y'   /* KAR 8/29/97
            pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units.getValue(pnd_A).nadd(cis_Cis_Rea_Annual_Nbr_Units);                                                                    //Natural: ADD CIS.CIS-REA-ANNUAL-NBR-UNITS TO #MDO-REA-ANNTY-UNITS ( #A )
            pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt.getValue(pnd_A).nadd(cis_Cis_Rea_Annty_Amt);                                                                             //Natural: ADD CIS.CIS-REA-ANNTY-AMT TO #MDO-REA-ANNTY-AMT ( #A )
            pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt.getValue(pnd_A).nadd(cis_Cis_Mdo_Traditional_Amt);                                                                     //Natural: ADD CIS.CIS-MDO-TRADITIONAL-AMT TO #MDO-TRADITIONAL-AMT ( #A )
            FOR09:                                                                                                                                                        //Natural: FOR #C 1 20
            for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
            {
                //*  MMA
                //*  STOCK
                //*  SOCIAL CHOICE
                //*  GLOBAL
                //*  BOND
                //*  GROWTH
                //*  EQUITY INDEX
                //*  ILB
                short decideConditionsMet1167 = 0;                                                                                                                        //Natural: DECIDE ON EVERY VALUE OF CIS.CIS-CREF-ACCT-CDE ( #C );//Natural: VALUE 'M'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("M")))
                {
                    decideConditionsMet1167++;
                    pnd_Mdo_Fields_Pnd_Mdo_Mma.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                       //Natural: ASSIGN #MDO-MMA ( #A ) := #MDO-MMA ( #A ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                }                                                                                                                                                         //Natural: VALUE 'C'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("C")))
                {
                    decideConditionsMet1167++;
                    pnd_Mdo_Fields_Pnd_Mdo_Stock.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                     //Natural: ASSIGN #MDO-STOCK ( #A ) := #MDO-STOCK ( #A ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                }                                                                                                                                                         //Natural: VALUE 'S'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("S")))
                {
                    decideConditionsMet1167++;
                    pnd_Mdo_Fields_Pnd_Mdo_Social.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                    //Natural: ASSIGN #MDO-SOCIAL ( #A ) := #MDO-SOCIAL ( #A ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                }                                                                                                                                                         //Natural: VALUE 'W'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("W")))
                {
                    decideConditionsMet1167++;
                    pnd_Mdo_Fields_Pnd_Mdo_Global.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                    //Natural: ASSIGN #MDO-GLOBAL ( #A ) := #MDO-GLOBAL ( #A ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                }                                                                                                                                                         //Natural: VALUE 'B'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("B")))
                {
                    decideConditionsMet1167++;
                    pnd_Mdo_Fields_Pnd_Mdo_Bond.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                      //Natural: ASSIGN #MDO-BOND ( #A ) := #MDO-BOND ( #A ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                }                                                                                                                                                         //Natural: VALUE 'L'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("L")))
                {
                    decideConditionsMet1167++;
                    pnd_Mdo_Fields_Pnd_Mdo_Grth.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                      //Natural: ASSIGN #MDO-GRTH ( #A ) := #MDO-GRTH ( #A ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                }                                                                                                                                                         //Natural: VALUE 'E'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("E")))
                {
                    decideConditionsMet1167++;
                    pnd_Mdo_Fields_Pnd_Mdo_Equ.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                       //Natural: ASSIGN #MDO-EQU ( #A ) := #MDO-EQU ( #A ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                }                                                                                                                                                         //Natural: VALUE 'I'
                if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).equals("I")))
                {
                    decideConditionsMet1167++;
                    pnd_Mdo_Fields_Pnd_Mdo_Ilb.getValue(pnd_A).nadd(cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C));                                                       //Natural: ASSIGN #MDO-ILB ( #A ) := #MDO-ILB ( #A ) + CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C )
                }                                                                                                                                                         //Natural: NONE VALUE
                if (condition(decideConditionsMet1167 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  END-IF          /* HK
            pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt.getValue(pnd_A).nadd(cis_Cis_Mdo_Tiaa_Int_Pymnt_Amt);                                                                   //Natural: ADD CIS.CIS-MDO-TIAA-INT-PYMNT-AMT TO #MDO-TIAA-INT-PYMNT ( #A )
            pnd_Mdo_Fields_Pnd_Mdo_Cref_Int_Pymnt.getValue(pnd_A).nadd(cis_Cis_Mdo_Cref_Int_Pymnt_Amt);                                                                   //Natural: ADD CIS.CIS-MDO-CREF-INT-PYMNT-AMT TO #MDO-CREF-INT-PYMNT ( #A )
            pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl.getValue(pnd_A).nadd(cis_Cis_Mdo_Tiaa_Excluded_Amt);                                                                         //Natural: ADD CIS.CIS-MDO-TIAA-EXCLUDED-AMT TO #MDO-TIAA-EXCL ( #A )
            pnd_Mdo_Fields_Pnd_Mdo_Cref_Excl.getValue(pnd_A).nadd(cis_Cis_Mdo_Cref_Excluded_Amt);                                                                         //Natural: ADD CIS.CIS-MDO-CREF-EXCLUDED-AMT TO #MDO-CREF-EXCL ( #A )
            //*  END-IF /* KAR APPRVL-Y ONLY
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Mdo_Report() throws Exception                                                                                                                        //Natural: MDO-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        FOR10:                                                                                                                                                            //Natural: FOR #A 1 2
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(2)); pnd_A.nadd(1))
        {
            getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),"   Contract/Certificate Sets  ",new ColumnSpacing(55),pnd_Apprvl.getValue(pnd_A),               //Natural: WRITE ( 3 ) 1T '   Contract/Certificate Sets  ' 55X #APPRVL ( #A ) //
                new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,"TIAA MDO",NEWLINE,new ColumnSpacing(2),"Traditional Annuity  ",new ColumnSpacing(17),pnd_Mdo_Fields_Pnd_Mdo_Traditional_Amt.getValue(pnd_A),  //Natural: WRITE ( 3 ) 'TIAA MDO' / 2X 'Traditional Annuity  ' 17X #MDO-TRADITIONAL-AMT ( #A ) / 2X 'Real Estate Account  ' 17X #MDO-REA-ANNTY-AMT ( #A ) 7X #MDO-REA-ANNTY-UNITS ( #A ) /
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(2),"Real Estate Account  ",new ColumnSpacing(17),pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Amt.getValue(pnd_A), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Mdo_Fields_Pnd_Mdo_Rea_Annty_Units.getValue(pnd_A), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),
                NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,"CREF MDO",NEWLINE,"  Stock                ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Stock.getValue(pnd_A),  //Natural: WRITE ( 3 ) / 'CREF MDO' / '  Stock                ' 38X #MDO-STOCK ( #A ) / '  Money Market         ' 38X #MDO-MMA ( #A ) / '  Social Choice        ' 38X #MDO-SOCIAL ( #A ) / '  Bond                 ' 38X #MDO-BOND ( #A ) / '  Global Equities      ' 38X #MDO-GLOBAL ( #A ) / '  Growth               ' 38X #MDO-GRTH ( #A ) / '  Equity Index         ' 38X #MDO-EQU ( #A ) / '  Inflation Linked Bond' 38X #MDO-ILB ( #A )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Money Market         ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Mma.getValue(pnd_A), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Social Choice        ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Social.getValue(pnd_A), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Bond                 ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Bond.getValue(pnd_A), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Global Equities      ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Global.getValue(pnd_A), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Growth               ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Grth.getValue(pnd_A), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Equity Index         ",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Equ.getValue(pnd_A), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE,"  Inflation Linked Bond",new ColumnSpacing(38),pnd_Mdo_Fields_Pnd_Mdo_Ilb.getValue(pnd_A), 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(35),"'First Required Payment'",new ColumnSpacing(12),"'Initial Excluded Amount'",NEWLINE,new  //Natural: WRITE ( 3 ) // 35T '"First Required Payment"' 12X '"Initial Excluded Amount"' / 35T '------------------------' 12X '-------------------------' /
                TabSetting(35),"------------------------",new ColumnSpacing(12),"-------------------------",NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().display(3, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(1),"TIAA MDO",                                                              //Natural: DISPLAY ( 3 ) NOHDR ( HW = OFF ) 1T 'TIAA MDO' 30X #MDO-TIAA-INT-PYMNT ( #A ) ( IC = $ ) 21X #MDO-TIAA-EXCL ( #A ) ( IC = $ ) /
            		new ColumnSpacing(30),pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt.getValue(pnd_A), new InsertionCharacter("$"),new ColumnSpacing(21),pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl.getValue(pnd_A), 
                new InsertionCharacter("$"),NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().display(3, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(1),"CREF MDO",                                                              //Natural: DISPLAY ( 3 ) NOHDR ( HW = OFF ) 1T 'CREF MDO' 30X #MDO-CREF-INT-PYMNT ( #A ) ( IC = $ ) 21X #MDO-CREF-EXCL ( #A ) ( IC = $ ) /
            		new ColumnSpacing(30),pnd_Mdo_Fields_Pnd_Mdo_Cref_Int_Pymnt.getValue(pnd_A), new InsertionCharacter("$"),new ColumnSpacing(21),pnd_Mdo_Fields_Pnd_Mdo_Cref_Excl.getValue(pnd_A), 
                new InsertionCharacter("$"),NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_A.equals(1)))                                                                                                                               //Natural: IF #A = 1
            {
                getReports().newPage(new ReportSpecification(3));                                                                                                         //Natural: NEWPAGE ( 3 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Mdo_Daily() throws Exception                                                                                                                         //Natural: MDO-DAILY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Mdo_Total.compute(new ComputeParameters(false, pnd_Mdo_Total), pnd_M_Rec.getValue(1).add(pnd_M_Rec.getValue(2)).add(pnd_M_Rec.getValue(3)).add(pnd_M_Rec.getValue(4))); //Natural: ASSIGN #MDO-TOTAL := #M-REC ( 1 ) + #M-REC ( 2 ) + #M-REC ( 3 ) + #M-REC ( 4 )
        getReports().write(4, ReportOption.NOTITLE,new TabSetting(10),"TOTAL NUMBER OF REQUESTS TO BE PROCESSED:",pnd_Mdo_Rq_Pr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new  //Natural: WRITE ( 4 ) 10T 'TOTAL NUMBER OF REQUESTS TO BE PROCESSED:' #MDO-RQ-PR // 10T 'MDO CONTRACTS TO BE ISSUED:' // 10T 'RETIREMENT CASH:     ' #M-REC ( 1 ) // 10T 'RETIREMENT NON-CASH: ' #M-REC ( 2 ) // 10T 'SURVIVOR CASH:       ' #M-REC ( 3 ) // 10T 'SURVIVOR NON-CASH:   ' #M-REC ( 4 ) /// 10T 'TOTAL MDO CONTRACTS ISSUED:              ' #MDO-TOTAL /
            TabSetting(10),"MDO CONTRACTS TO BE ISSUED:",NEWLINE,NEWLINE,new TabSetting(10),"RETIREMENT CASH:     ",pnd_M_Rec.getValue(1), new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"RETIREMENT NON-CASH: ",pnd_M_Rec.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(10),"SURVIVOR CASH:       ",pnd_M_Rec.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"SURVIVOR NON-CASH:   ",pnd_M_Rec.getValue(4), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,new TabSetting(10),"TOTAL MDO CONTRACTS ISSUED:              ",pnd_Mdo_Total, new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Tpa_Process() throws Exception                                                                                                                       //Natural: TPA-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Tpa_Record.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #TPA-RECORD
        FOR11:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I).notEquals(getZero())))                                                                             //Natural: IF CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) NE 0
            {
                pnd_Tpa_Fields_Pnd_Tpa_Da_Tiaa_Proceeds_Amt.nadd(cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_I));                                                           //Natural: ADD CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #I ) TO #TPA-DA-TIAA-PROCEEDS-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Tpa_Fields_Pnd_Tpa_Grnted_Grd_Amt.nadd(cis_Cis_Grnted_Grd_Amt);                                                                                               //Natural: ADD CIS-GRNTED-GRD-AMT TO #TPA-GRNTED-GRD-AMT
        pnd_Tpa_Fields_Pnd_Tpa_Rtb_Amt.nadd(cis_Cis_Rtb_Amt);                                                                                                             //Natural: ADD CIS-RTB-AMT TO #TPA-RTB-AMT
    }
    private void sub_Tpa_Report() throws Exception                                                                                                                        //Natural: TPA-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(5, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(10),"Accumulation Applied  ",                                                   //Natural: DISPLAY ( 5 ) NOHDR ( HW = OFF ) 10T 'Accumulation Applied  ' 9X #TPA-DA-TIAA-PROCEEDS-AMT ( IC = $ ) /
        		new ColumnSpacing(9),pnd_Tpa_Fields_Pnd_Tpa_Da_Tiaa_Proceeds_Amt, new InsertionCharacter("$"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().display(5, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(10),"Annuity Payment       ",                                                   //Natural: DISPLAY ( 5 ) NOHDR ( HW = OFF ) 10T 'Annuity Payment       ' 9X #TPA-GRNTED-GRD-AMT ( IC = $ ) /
        		new ColumnSpacing(9),pnd_Tpa_Fields_Pnd_Tpa_Grnted_Grd_Amt, new InsertionCharacter("$"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().display(5, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(10),"TTB                   ",                                                   //Natural: DISPLAY ( 5 ) NOHDR ( HW = OFF ) 10T 'TTB                   ' 9X #TPA-RTB-AMT ( IC = $ ) //
        		new ColumnSpacing(9),pnd_Tpa_Fields_Pnd_Tpa_Rtb_Amt, new InsertionCharacter("$"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(5, ReportOption.NOTITLE,new TabSetting(10),"- - - - - - - - - - - - - - - - - - - - - - - -",NEWLINE);                                         //Natural: WRITE ( 5 ) 10T '- - - - - - - - - - - - - - - - - - - - - - - -' /
        if (Global.isEscape()) return;
        getReports().write(5, ReportOption.NOTITLE,new TabSetting(10),"Total TPA Contracts Issued:   ",new ColumnSpacing(3),pnd_Tpa_Record, new ReportEditMask            //Natural: WRITE ( 5 ) 10T 'Total TPA Contracts Issued:   ' 3X #TPA-RECORD /
            ("ZZZ,ZZZ,ZZ9"),NEWLINE);
        if (Global.isEscape()) return;
    }
    //*   #C IS ARRAY
    private void sub_S700_Print_Array() throws Exception                                                                                                                  //Natural: S700-PRINT-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************************8******
        if (condition(cis_Cis_Rtb_Amt.notEquals(getZero())))                                                                                                              //Natural: IF CIS-RTB-AMT NE 0
        {
            //*  TACC KG
            getReports().write(8, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Rtb_Amt,new           //Natural: WRITE ( 8 ) '-' / 1T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-RTB-AMT 120T 'RTB'
                TabSetting(120),"RTB");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Cis_Mdo_Traditional_Amt.notEquals(getZero())))                                                                                                  //Natural: IF CIS-MDO-TRADITIONAL-AMT NE 0
        {
            //*  TACC KG
            getReports().write(8, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Mdo_Traditional_Amt,new  //Natural: WRITE ( 8 ) '-' / 1T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-MDO-TRADITIONAL-AMT 120T 'MDO'
                TabSetting(120),"MDO");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Cis_Rea_Mnthly_Nbr_Units.notEquals(getZero()) || cis_Cis_Rea_Annual_Nbr_Units.notEquals(getZero()) || cis_Cis_Rea_Annty_Amt.notEquals(getZero()))) //Natural: IF CIS-REA-MNTHLY-NBR-UNITS NE 0 OR CIS-REA-ANNUAL-NBR-UNITS NE 0 OR CIS-REA-ANNTY-AMT NE 0
        {
            //*  TACC KG
            getReports().write(8, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(29),cis_Cis_Rea_Mnthly_Nbr_Units,new  //Natural: WRITE ( 8 ) '-' / 1T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 29T CIS-REA-MNTHLY-NBR-UNITS 44T CIS-REA-ANNUAL-NBR-UNITS 89T CIS-REA-ANNTY-AMT 120T 'REA'
                TabSetting(44),cis_Cis_Rea_Annual_Nbr_Units,new TabSetting(89),cis_Cis_Rea_Annty_Amt,new TabSetting(120),"REA");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Cis_Grnted_Grd_Amt.notEquals(getZero())))                                                                                                       //Natural: IF CIS-GRNTED-GRD-AMT NE 0
        {
            //*  TACC KG
            getReports().write(8, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Grnted_Grd_Amt,new    //Natural: WRITE ( 8 ) '-' / 1T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-GRNTED-GRD-AMT 120T 'GRD AMT'
                TabSetting(120),"GRD AMT");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(cis_Cis_Grnted_Std_Amt.notEquals(getZero())))                                                                                                       //Natural: IF CIS-GRNTED-STD-AMT NE 0
        {
            //*  TACC KG
            getReports().write(8, "-",NEWLINE,new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(59),cis_Cis_Grnted_Std_Amt,new    //Natural: WRITE ( 8 ) '-' / 1T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 59T CIS-GRNTED-STD-AMT 120T 'STD AMT'
                TabSetting(120),"STD AMT");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR12:                                                                                                                                                            //Natural: FOR #C FROM 1 TO 20
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
        {
            if (condition(cis_Cis_Cref_Acct_Cde.getValue(pnd_C).notEquals(" ") || cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C).notEquals(getZero()) ||                   //Natural: IF CIS.CIS-CREF-ACCT-CDE ( #C ) NE ' ' OR CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) NE 0 OR CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) NE 0 OR CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #C ) NE 0 OR CIS.CIS-DA-CREF-PROCEEDS-AMT ( #C ) NE 0 OR CIS.CIS-DA-REA-PROCEEDS-AMT ( #C ) NE 0
                cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C).notEquals(getZero()) || cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_C).notEquals(getZero()) 
                || cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_C).notEquals(getZero()) || cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_C).notEquals(getZero())))
            {
                //*  TACC KG ST
                //*  TACC KG END
                getReports().write(8, new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(25),cis_Cis_Cref_Acct_Cde.getValue(pnd_C),new  //Natural: WRITE ( 8 ) 1T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 25T CIS.CIS-CREF-ACCT-CDE ( #C ) 29T CIS.CIS-CREF-MNTHLY-NBR-UNITS ( #C ) 44T CIS.CIS-CREF-ANNUAL-NBR-UNITS ( #C ) 59T CIS.CIS-DA-TIAA-PROCEEDS-AMT ( #C ) 74T CIS.CIS-DA-CREF-PROCEEDS-AMT ( #C ) 89T CIS.CIS-DA-REA-PROCEEDS-AMT ( #C ) 120T CIS-DA-TIAA-NBR ( #C )
                    TabSetting(29),cis_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_C),new TabSetting(44),cis_Cis_Cref_Annual_Nbr_Units.getValue(pnd_C),new TabSetting(59),cis_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_C),new 
                    TabSetting(74),cis_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_C),new TabSetting(89),cis_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_C),new TabSetting(120),
                    cis_Cis_Da_Tiaa_Nbr.getValue(pnd_C));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  TACC KG ST
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR13:                                                                                                                                                            //Natural: FOR #D 1 100
        for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(100)); pnd_D.nadd(1))
        {
            if (condition(cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D).notEquals(getZero()) || cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D).notEquals(getZero())))      //Natural: IF CIS.CIS-TACC-MNTHLY-NBR-UNITS ( #D ) NE 0 OR CIS.CIS-TACC-ANNUAL-NBR-UNITS ( #D ) NE 0
            {
                getReports().write(8, new TabSetting(1),cis_Cis_Annty_Option,new TabSetting(12),cis_Cis_Tiaa_Nbr,new TabSetting(29),cis_Cis_Tacc_Mnthly_Nbr_Units.getValue(pnd_D),new  //Natural: WRITE ( 8 ) 1T CIS-ANNTY-OPTION 12T CIS-TIAA-NBR 29T CIS-TACC-MNTHLY-NBR-UNITS ( #D ) 44T CIS-TACC-ANNUAL-NBR-UNITS ( #D ) 104T CIS-TACC-ANNTY-AMT 120T 'T ACCESS'
                    TabSetting(44),cis_Cis_Tacc_Annual_Nbr_Units.getValue(pnd_D),new TabSetting(104),cis_Cis_Tacc_Annty_Amt,new TabSetting(120),"T ACCESS");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  TACC KG END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM());                                                                                      //Natural: WRITE ( 1 ) NOTITLE *PROGRAM
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(36),"CONTRACT PRODUCTION CONTROL TOTALS - PAYOUT ANNUITES",new TabSetting(105),"PROCESSING DATE:",Global.getDATU(),NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) 36T 'CONTRACT PRODUCTION CONTROL TOTALS - PAYOUT ANNUITES' 105T 'PROCESSING DATE:' *DATU // 38T '  MONTHLY       QUARTERLY       SEMI-ANNUAL       ANNUAL' 105T 'ALL MODES' / 38T '$ or Units      $ or Units      $ or Units      $ or Units' 105T '$ or Units' / 38T '----------      ----------      ----------      ----------' 105T '----------'
                        TabSetting(38),"  MONTHLY       QUARTERLY       SEMI-ANNUAL       ANNUAL",new TabSetting(105),"ALL MODES",NEWLINE,new TabSetting(38),"$ or Units      $ or Units      $ or Units      $ or Units",new 
                        TabSetting(105),"$ or Units",NEWLINE,new TabSetting(38),"----------      ----------      ----------      ----------",new TabSetting(105),
                        "----------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 2 ) ' '
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(70),"PROCESSING DATE:",Global.getDATU(),NEWLINE,NEWLINE,new             //Natural: WRITE ( 2 ) NOTITLE *PROGRAM 70T 'PROCESSING DATE:' *DATU // 26T 'IA CONTRACTS RELEASED FOR PRINTING' / 26T '    DETAILS BY ANNUITY OPTION' / 26T '          DAILY REPORT' ///
                        TabSetting(26),"IA CONTRACTS RELEASED FOR PRINTING",NEWLINE,new TabSetting(26),"    DETAILS BY ANNUITY OPTION",NEWLINE,new TabSetting(26),
                        "          DAILY REPORT",NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 3 ) ' '
                    getReports().write(3, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(70),"PROCESSING DATE:",Global.getDATU(),NEWLINE,NEWLINE);               //Natural: WRITE ( 3 ) NOTITLE *PROGRAM 70T 'PROCESSING DATE:' *DATU //
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(20),"CONTRACT PRODUCTION CONTROL TOTALS - MDO",pnd_Aprl_Y_N.getValue(pnd_A),NEWLINE,NEWLINE,new  //Natural: WRITE ( 3 ) 20T 'CONTRACT PRODUCTION CONTROL TOTALS - MDO' #APRL-Y-N ( #A ) // 85T 'Contract &' / 85T 'Certificate' / 45T '  Dollars             Units               Counts' / 45T '----------         ------------         ----------' / 1T 'Considerations Applied To' / 1T '-------------------------' /
                        TabSetting(85),"Contract &",NEWLINE,new TabSetting(85),"Certificate",NEWLINE,new TabSetting(45),"  Dollars             Units               Counts",NEWLINE,new 
                        TabSetting(45),"----------         ------------         ----------",NEWLINE,new TabSetting(1),"Considerations Applied To",NEWLINE,new 
                        TabSetting(1),"-------------------------",NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 4 ) ' '
                    getReports().write(4, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(70),"PROCESSING DATE:",Global.getDATU(),NEWLINE,NEWLINE,new             //Natural: WRITE ( 4 ) NOTITLE *PROGRAM 70T 'PROCESSING DATE:' *DATU // 26T 'MDO CONTRACTS TO BE RELEASED FOR PRINTING' / 26T '           DAILY REPORT' ///
                        TabSetting(26),"MDO CONTRACTS TO BE RELEASED FOR PRINTING",NEWLINE,new TabSetting(26),"           DAILY REPORT",NEWLINE,NEWLINE,
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(5, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 5 ) ' '
                    getReports().write(5, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(70),"PROCESSING DATE:",Global.getDATU(),NEWLINE,NEWLINE);               //Natural: WRITE ( 5 ) NOTITLE *PROGRAM 70T 'PROCESSING DATE:' *DATU //
                    getReports().write(5, ReportOption.NOTITLE,new TabSetting(15),"CONTRACT PRODUCTION CONTROL TOTALS - ","Transfer Payout Annuities",                    //Natural: WRITE ( 5 ) 15T 'CONTRACT PRODUCTION CONTROL TOTALS - ' 'Transfer Payout Annuities' ///
                        NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt6 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(6, ReportOption.NOTITLE,Global.getPROGRAM());                                                                                      //Natural: WRITE ( 6 ) NOTITLE *PROGRAM
                    getReports().write(6, ReportOption.NOTITLE,new TabSetting(30),"CONTRACT PRODUCTION CONTROL TOTALS - IA TRANSFER PAYOUT ANNUITES",new                  //Natural: WRITE ( 6 ) 30T 'CONTRACT PRODUCTION CONTROL TOTALS - IA TRANSFER PAYOUT ANNUITES' 105T 'PROCESSING DATE:' *DATU // 38T '  MONTHLY       QUARTERLY       SEMI-ANNUAL       ANNUAL' 105T 'ALL MODES' / 38T '$ OR UNITS      $ OR UNITS      $ OR UNITS      $ OR UNITS' 105T '$ OR UNITS' / 38T '----------      ----------      ----------      ----------' 105T '----------'
                        TabSetting(105),"PROCESSING DATE:",Global.getDATU(),NEWLINE,NEWLINE,new TabSetting(38),"  MONTHLY       QUARTERLY       SEMI-ANNUAL       ANNUAL",new 
                        TabSetting(105),"ALL MODES",NEWLINE,new TabSetting(38),"$ OR UNITS      $ OR UNITS      $ OR UNITS      $ OR UNITS",new TabSetting(105),"$ OR UNITS",NEWLINE,new 
                        TabSetting(38),"----------      ----------      ----------      ----------",new TabSetting(105),"----------");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt7 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(7, ReportOption.NOTITLE," ");                                                                                                      //Natural: WRITE ( 7 ) ' '
                    getReports().write(7, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(70),"PROCESSING DATE:",Global.getDATU(),NEWLINE,NEWLINE,new             //Natural: WRITE ( 7 ) NOTITLE *PROGRAM 70T 'PROCESSING DATE:' *DATU // 22T 'IA TRANSFER CONTRACTS RELEASED FOR PRINTING' / 26T '    DETAILS BY ANNUITY OPTION' / 26T '          DAILY REPORT' ///
                        TabSetting(22),"IA TRANSFER CONTRACTS RELEASED FOR PRINTING",NEWLINE,new TabSetting(26),"    DETAILS BY ANNUITY OPTION",NEWLINE,new 
                        TabSetting(26),"          DAILY REPORT",NEWLINE,NEWLINE,NEWLINE);
                    //*  IPFS
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(2, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(3, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(4, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(5, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(6, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(7, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(8, "LS=132 PS=60 ZP=ON IS=OFF ES=OFF");
        Global.format(0, "LS=80 PS=60 ZP=ON IS=OFF ES=OFF");

        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(30)," DETAIL LISTING OF ALL CONTRACTS AND AMOUNTS ",NEWLINE,NEWLINE,new 
            TabSetting(1),"OPTION",new TabSetting(12),"TIAA NUMBER",new TabSetting(25),"ID",new TabSetting(29),"MONTHLY UNITS",new TabSetting(44),"ANNUAL UNITS",new 
            TabSetting(59),"TIAA AMOUNT",new TabSetting(74),"CREF AMOUNT",new TabSetting(89),"REA AMOUNT",new TabSetting(104),"ACCESS AMT");

        getReports().setDisplayColumns(1, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Standard",
        		new ColumnSpacing(1),pnd_Grnted_Std_Amt, new InsertionCharacter("$"),pnd_Grnted_Std_Amt, new InsertionCharacter("$"),pnd_Grnted_Std_Amt, new 
            InsertionCharacter("$"),pnd_Grnted_Std_Amt, new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Grnted_Std_Amt_Tot, new InsertionCharacter("$"));
        getReports().setDisplayColumns(6, new HeadingWidth(false),ReportOption.NOHDR,"TRADITIONAL ANNUITY - Standard",
        		new ColumnSpacing(1),pnd_Grnted_Std_Amt_T, new InsertionCharacter("$"),pnd_Grnted_Std_Amt_T, new InsertionCharacter("$"),pnd_Grnted_Std_Amt_T, 
            new InsertionCharacter("$"),pnd_Grnted_Std_Amt_T, new InsertionCharacter("$"),new ColumnSpacing(4),pnd_Grnted_Std_Amt_Tot_T, new InsertionCharacter("$"));
        getReports().setDisplayColumns(3, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(1),"TIAA MDO",
        		new ColumnSpacing(30),pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Int_Pymnt, new InsertionCharacter("$"),new ColumnSpacing(21),pnd_Mdo_Fields_Pnd_Mdo_Tiaa_Excl, 
            new InsertionCharacter("$"),NEWLINE);
        getReports().setDisplayColumns(5, new HeadingWidth(false),ReportOption.NOHDR,new TabSetting(10),"Accumulation Applied  ",
        		new ColumnSpacing(9),pnd_Tpa_Fields_Pnd_Tpa_Da_Tiaa_Proceeds_Amt, new InsertionCharacter("$"),NEWLINE);
    }
}
