/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:21:39 PM
**        * FROM NATURAL PROGRAM : Cisb610
************************************************************
**        * FILE NAME            : Cisb610.java
**        * CLASS NAME           : Cisb610
**        * INSTANCE NAME        : Cisb610
************************************************************
************************************************************************
* PROGRAM  : CISB610                                                   *
* SYSTEM   : CIS                                                       *
* TITLE    : DIALOGUE FORMAT PROGRAM.                                  *
* FUNCTION : BUILDS DRIVER RECORD, BENE RECORDS, ALLOCATION RECORDS,   *
*            AND FUND RECORDS FOR DIALOG.                              *
* MOD DATE    MOD BY     DESCRIPTION OF CHANGES                        *
* ----------  ---------  --------------------------------------------  *
* 07/16/2009  DEVELBISS  INITIAL WRITE                                 *
* 11/15/2011  BERGHEISER ADD NEW MDO FIELDS FOR CONFIRMS 2B (JRB1)     *
* 02/16/2011  BERGHEISER CREATE RECORD TYPES 12 AND 13 FOR MDO (JRB2)  *
* 04/05/2011  BERGHEISER SET ZERO DATES TO ALL ZEROES (JRB3)           *
* 12/13/2014  BAUER CREF REDESIGN CHANGES (ACCRC)                      *
*             MDO CHANGES.                                             *
*             1.MOVE MULTI-PLAN-IND FROM CISLEXT TO CISL601M.          *
*             2.SINGLE PLAN CONTRACT - MOVE PLAN-NUM FROM CISLEXT TO   *
*               PLAN-NUM AND FIRST OCCURRENCE OF MULTI-PLAN-NO ON      *
*               CISL601M.                                              *
*             3.MULTI PLAN CONTRACT - MOVE MULTI-PLAN-NO(*) &          *
*               MULTI-SUB-PLAN(*) FROM CISLEXT TO MULTI-PLAN-NO(*) &   *
*               MULTI-SUB-PLAN(*) ON CISL601M.                         *
*             IA CHANGES.                                              *
*             1.MOVE MULTI-PLAN-IND FROM CISLEXT TO CISL601I.          *
*             2.MOVE IA-SHR-CLASS FROM CISLEXT TO CISL601I.            *
* 05/09/2017 - GHOSABE  - PIN EXPANSION CHANGES.  (C420007)    PINE.   *
* 11/16/2019 - L SHU    - CONTRACT STRATEGY PROJECT           CNTRSTRG *
*                         MOVE FIRST TPA/IPRO IND TO  WORK FILE        *
************************************************************************
*

************************************************************ */

package tiaa.cis_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisb610 extends BLNatBase
{
    // Data Areas
    private LdaCislext ldaCislext;
    private PdaCisa620 pdaCisa620;
    private LdaCisl601i ldaCisl601i;
    private LdaCisl601m ldaCisl601m;
    private LdaCisl602 ldaCisl602;
    private LdaCisl603 ldaCisl603;
    private LdaCisl604 ldaCisl604;
    private LdaCisl605 ldaCisl605;
    private LdaCisl607 ldaCisl607;
    private LdaCisl608 ldaCisl608;
    private LdaCisl609 ldaCisl609;
    private LdaCisl610 ldaCisl610;
    private LdaCisl611 ldaCisl611;
    private LdaCisl612 ldaCisl612;
    private LdaCisl613 ldaCisl613;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_table_Entry;
    private DbsField table_Entry_Entry_Actve_Ind;
    private DbsField table_Entry_Entry_Table_Id_Nbr;
    private DbsField table_Entry_Entry_Table_Sub_Id;
    private DbsField table_Entry_Entry_Cde;

    private DbsGroup table_Entry__R_Field_1;
    private DbsField table_Entry_Entry_Job_Name;
    private DbsField table_Entry_Entry_Dscrptn_Txt;

    private DbsGroup table_Entry__R_Field_2;
    private DbsField table_Entry_Profile;
    private DbsField pnd_Input_Parm;
    private DbsField pnd_Extr_Read;
    private DbsField pnd_Tot_Written;
    private DbsField pnd_Ia_Driver_Written;
    private DbsField pnd_Mdo_Driver_Written;
    private DbsField pnd_Pbene_Written;
    private DbsField pnd_Cbene_Written;
    private DbsField pnd_Fund_Written;
    private DbsField pnd_Comut_Written;
    private DbsField pnd_Tiaa_Written;
    private DbsField pnd_Rea_Written;
    private DbsField pnd_Cref_Written;
    private DbsField pnd_Ttrn_Written;
    private DbsField pnd_Strn_Written;
    private DbsField pnd_Finv_Written;
    private DbsField pnd_Tinv_Written;
    private DbsField pnd_Ia_Guid;
    private DbsField pnd_Mdo_Guid;
    private DbsField pnd_Contract_Type;
    private DbsField pnd_Ppg;
    private DbsField pnd_Print_Type;
    private DbsField pnd_Reprint;
    private DbsField pnd_Found;
    private DbsField pnd_Work_Addr_Line;

    private DbsGroup pnd_Work_Addr_Line__R_Field_3;
    private DbsField pnd_Work_Addr_Line_Pnd_Work_Addr_Digit;
    private DbsField pnd_Work_Zip;

    private DbsGroup pnd_Work_Zip__R_Field_4;
    private DbsField pnd_Work_Zip_Pnd_Work_Zip_Digit;
    private DbsField pnd_Work_Zip_N;

    private DbsGroup pnd_Work_Zip_N__R_Field_5;
    private DbsField pnd_Work_Zip_N_Pnd_Work_Zip_Digit_N;
    private DbsField pnd_Work_Zip_Check_Digit;

    private DbsGroup pnd_Work_Zip_Check_Digit__R_Field_6;
    private DbsField pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_10;
    private DbsField pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1;

    private DbsGroup pnd_Work_Zip_Check_Digit__R_Field_7;
    private DbsField pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_A;
    private DbsField pnd_Format_Amt;

    private DbsGroup pnd_Format_Amt__R_Field_8;
    private DbsField pnd_Format_Amt_Pnd_Dollar;
    private DbsField pnd_Format_Amt_Pnd_Amount;
    private DbsField pnd_Profile_Key;

    private DbsGroup pnd_Profile_Key__R_Field_9;
    private DbsField pnd_Profile_Key_Pnd_Pj_Entry_Actve_Ind;
    private DbsField pnd_Profile_Key_Pnd_Pj_Entry_Table_Id_Nbr;
    private DbsField pnd_Profile_Key_Pnd_Pj_Entry_Table_Sub_Id;
    private DbsField pnd_Profile_Key_Pnd_Pj_Entry_Cde;

    private DbsGroup pnd_Profile_Key__R_Field_10;
    private DbsField pnd_Profile_Key_Pnd_Pj_Job_Name;
    private DbsField pnd_Tiaa_Idx;
    private DbsField pnd_Rea_Idx;
    private DbsField pnd_Cref_Idx;
    private DbsField pnd_Sub_Na;
    private DbsField pnd_Sub_Addr;
    private DbsField pnd_Sub_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Z;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCislext = new LdaCislext();
        registerRecord(ldaCislext);
        localVariables = new DbsRecord();
        pdaCisa620 = new PdaCisa620(localVariables);
        ldaCisl601i = new LdaCisl601i();
        registerRecord(ldaCisl601i);
        ldaCisl601m = new LdaCisl601m();
        registerRecord(ldaCisl601m);
        ldaCisl602 = new LdaCisl602();
        registerRecord(ldaCisl602);
        ldaCisl603 = new LdaCisl603();
        registerRecord(ldaCisl603);
        ldaCisl604 = new LdaCisl604();
        registerRecord(ldaCisl604);
        ldaCisl605 = new LdaCisl605();
        registerRecord(ldaCisl605);
        ldaCisl607 = new LdaCisl607();
        registerRecord(ldaCisl607);
        ldaCisl608 = new LdaCisl608();
        registerRecord(ldaCisl608);
        ldaCisl609 = new LdaCisl609();
        registerRecord(ldaCisl609);
        ldaCisl610 = new LdaCisl610();
        registerRecord(ldaCisl610);
        ldaCisl611 = new LdaCisl611();
        registerRecord(ldaCisl611);
        ldaCisl612 = new LdaCisl612();
        registerRecord(ldaCisl612);
        ldaCisl613 = new LdaCisl613();
        registerRecord(ldaCisl613);

        // Local Variables

        vw_table_Entry = new DataAccessProgramView(new NameInfo("vw_table_Entry", "TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        table_Entry_Entry_Actve_Ind = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        table_Entry_Entry_Table_Id_Nbr = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        table_Entry_Entry_Table_Sub_Id = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        table_Entry_Entry_Cde = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");

        table_Entry__R_Field_1 = vw_table_Entry.getRecord().newGroupInGroup("table_Entry__R_Field_1", "REDEFINE", table_Entry_Entry_Cde);
        table_Entry_Entry_Job_Name = table_Entry__R_Field_1.newFieldInGroup("table_Entry_Entry_Job_Name", "ENTRY-JOB-NAME", FieldType.STRING, 9);
        table_Entry_Entry_Dscrptn_Txt = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");

        table_Entry__R_Field_2 = vw_table_Entry.getRecord().newGroupInGroup("table_Entry__R_Field_2", "REDEFINE", table_Entry_Entry_Dscrptn_Txt);
        table_Entry_Profile = table_Entry__R_Field_2.newFieldInGroup("table_Entry_Profile", "PROFILE", FieldType.STRING, 2);
        registerRecord(vw_table_Entry);

        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 8);
        pnd_Extr_Read = localVariables.newFieldInRecord("pnd_Extr_Read", "#EXTR-READ", FieldType.NUMERIC, 6);
        pnd_Tot_Written = localVariables.newFieldInRecord("pnd_Tot_Written", "#TOT-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Ia_Driver_Written = localVariables.newFieldInRecord("pnd_Ia_Driver_Written", "#IA-DRIVER-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Mdo_Driver_Written = localVariables.newFieldInRecord("pnd_Mdo_Driver_Written", "#MDO-DRIVER-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Pbene_Written = localVariables.newFieldInRecord("pnd_Pbene_Written", "#PBENE-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Cbene_Written = localVariables.newFieldInRecord("pnd_Cbene_Written", "#CBENE-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Fund_Written = localVariables.newFieldInRecord("pnd_Fund_Written", "#FUND-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Comut_Written = localVariables.newFieldInRecord("pnd_Comut_Written", "#COMUT-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Tiaa_Written = localVariables.newFieldInRecord("pnd_Tiaa_Written", "#TIAA-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Rea_Written = localVariables.newFieldInRecord("pnd_Rea_Written", "#REA-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Cref_Written = localVariables.newFieldInRecord("pnd_Cref_Written", "#CREF-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Ttrn_Written = localVariables.newFieldInRecord("pnd_Ttrn_Written", "#TTRN-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Strn_Written = localVariables.newFieldInRecord("pnd_Strn_Written", "#STRN-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Finv_Written = localVariables.newFieldInRecord("pnd_Finv_Written", "#FINV-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Tinv_Written = localVariables.newFieldInRecord("pnd_Tinv_Written", "#TINV-WRITTEN", FieldType.NUMERIC, 6);
        pnd_Ia_Guid = localVariables.newFieldInRecord("pnd_Ia_Guid", "#IA-GUID", FieldType.NUMERIC, 5);
        pnd_Mdo_Guid = localVariables.newFieldInRecord("pnd_Mdo_Guid", "#MDO-GUID", FieldType.NUMERIC, 5);
        pnd_Contract_Type = localVariables.newFieldInRecord("pnd_Contract_Type", "#CONTRACT-TYPE", FieldType.STRING, 6);
        pnd_Ppg = localVariables.newFieldInRecord("pnd_Ppg", "#PPG", FieldType.STRING, 6);
        pnd_Print_Type = localVariables.newFieldInRecord("pnd_Print_Type", "#PRINT-TYPE", FieldType.STRING, 1);
        pnd_Reprint = localVariables.newFieldInRecord("pnd_Reprint", "#REPRINT", FieldType.BOOLEAN, 1);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Work_Addr_Line = localVariables.newFieldInRecord("pnd_Work_Addr_Line", "#WORK-ADDR-LINE", FieldType.STRING, 35);

        pnd_Work_Addr_Line__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_Addr_Line__R_Field_3", "REDEFINE", pnd_Work_Addr_Line);
        pnd_Work_Addr_Line_Pnd_Work_Addr_Digit = pnd_Work_Addr_Line__R_Field_3.newFieldArrayInGroup("pnd_Work_Addr_Line_Pnd_Work_Addr_Digit", "#WORK-ADDR-DIGIT", 
            FieldType.STRING, 1, new DbsArrayController(1, 35));
        pnd_Work_Zip = localVariables.newFieldInRecord("pnd_Work_Zip", "#WORK-ZIP", FieldType.STRING, 5);

        pnd_Work_Zip__R_Field_4 = localVariables.newGroupInRecord("pnd_Work_Zip__R_Field_4", "REDEFINE", pnd_Work_Zip);
        pnd_Work_Zip_Pnd_Work_Zip_Digit = pnd_Work_Zip__R_Field_4.newFieldArrayInGroup("pnd_Work_Zip_Pnd_Work_Zip_Digit", "#WORK-ZIP-DIGIT", FieldType.STRING, 
            1, new DbsArrayController(1, 5));
        pnd_Work_Zip_N = localVariables.newFieldInRecord("pnd_Work_Zip_N", "#WORK-ZIP-N", FieldType.NUMERIC, 5);

        pnd_Work_Zip_N__R_Field_5 = localVariables.newGroupInRecord("pnd_Work_Zip_N__R_Field_5", "REDEFINE", pnd_Work_Zip_N);
        pnd_Work_Zip_N_Pnd_Work_Zip_Digit_N = pnd_Work_Zip_N__R_Field_5.newFieldArrayInGroup("pnd_Work_Zip_N_Pnd_Work_Zip_Digit_N", "#WORK-ZIP-DIGIT-N", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 5));
        pnd_Work_Zip_Check_Digit = localVariables.newFieldInRecord("pnd_Work_Zip_Check_Digit", "#WORK-ZIP-CHECK-DIGIT", FieldType.NUMERIC, 2);

        pnd_Work_Zip_Check_Digit__R_Field_6 = localVariables.newGroupInRecord("pnd_Work_Zip_Check_Digit__R_Field_6", "REDEFINE", pnd_Work_Zip_Check_Digit);
        pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_10 = pnd_Work_Zip_Check_Digit__R_Field_6.newFieldInGroup("pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_10", 
            "#WORK-ZIP-CHECK-DIGIT-10", FieldType.NUMERIC, 1);
        pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1 = pnd_Work_Zip_Check_Digit__R_Field_6.newFieldInGroup("pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1", 
            "#WORK-ZIP-CHECK-DIGIT-1", FieldType.NUMERIC, 1);

        pnd_Work_Zip_Check_Digit__R_Field_7 = pnd_Work_Zip_Check_Digit__R_Field_6.newGroupInGroup("pnd_Work_Zip_Check_Digit__R_Field_7", "REDEFINE", pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1);
        pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_A = pnd_Work_Zip_Check_Digit__R_Field_7.newFieldInGroup("pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_A", 
            "#WORK-ZIP-CHECK-DIGIT-A", FieldType.STRING, 1);
        pnd_Format_Amt = localVariables.newFieldInRecord("pnd_Format_Amt", "#FORMAT-AMT", FieldType.STRING, 15);

        pnd_Format_Amt__R_Field_8 = localVariables.newGroupInRecord("pnd_Format_Amt__R_Field_8", "REDEFINE", pnd_Format_Amt);
        pnd_Format_Amt_Pnd_Dollar = pnd_Format_Amt__R_Field_8.newFieldInGroup("pnd_Format_Amt_Pnd_Dollar", "#DOLLAR", FieldType.STRING, 1);
        pnd_Format_Amt_Pnd_Amount = pnd_Format_Amt__R_Field_8.newFieldInGroup("pnd_Format_Amt_Pnd_Amount", "#AMOUNT", FieldType.STRING, 14);
        pnd_Profile_Key = localVariables.newFieldInRecord("pnd_Profile_Key", "#PROFILE-KEY", FieldType.STRING, 29);

        pnd_Profile_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Profile_Key__R_Field_9", "REDEFINE", pnd_Profile_Key);
        pnd_Profile_Key_Pnd_Pj_Entry_Actve_Ind = pnd_Profile_Key__R_Field_9.newFieldInGroup("pnd_Profile_Key_Pnd_Pj_Entry_Actve_Ind", "#PJ-ENTRY-ACTVE-IND", 
            FieldType.STRING, 1);
        pnd_Profile_Key_Pnd_Pj_Entry_Table_Id_Nbr = pnd_Profile_Key__R_Field_9.newFieldInGroup("pnd_Profile_Key_Pnd_Pj_Entry_Table_Id_Nbr", "#PJ-ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6);
        pnd_Profile_Key_Pnd_Pj_Entry_Table_Sub_Id = pnd_Profile_Key__R_Field_9.newFieldInGroup("pnd_Profile_Key_Pnd_Pj_Entry_Table_Sub_Id", "#PJ-ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2);
        pnd_Profile_Key_Pnd_Pj_Entry_Cde = pnd_Profile_Key__R_Field_9.newFieldInGroup("pnd_Profile_Key_Pnd_Pj_Entry_Cde", "#PJ-ENTRY-CDE", FieldType.STRING, 
            20);

        pnd_Profile_Key__R_Field_10 = pnd_Profile_Key__R_Field_9.newGroupInGroup("pnd_Profile_Key__R_Field_10", "REDEFINE", pnd_Profile_Key_Pnd_Pj_Entry_Cde);
        pnd_Profile_Key_Pnd_Pj_Job_Name = pnd_Profile_Key__R_Field_10.newFieldInGroup("pnd_Profile_Key_Pnd_Pj_Job_Name", "#PJ-JOB-NAME", FieldType.STRING, 
            9);
        pnd_Tiaa_Idx = localVariables.newFieldInRecord("pnd_Tiaa_Idx", "#TIAA-IDX", FieldType.NUMERIC, 3);
        pnd_Rea_Idx = localVariables.newFieldInRecord("pnd_Rea_Idx", "#REA-IDX", FieldType.NUMERIC, 3);
        pnd_Cref_Idx = localVariables.newFieldInRecord("pnd_Cref_Idx", "#CREF-IDX", FieldType.NUMERIC, 3);
        pnd_Sub_Na = localVariables.newFieldInRecord("pnd_Sub_Na", "#SUB-NA", FieldType.NUMERIC, 1);
        pnd_Sub_Addr = localVariables.newFieldInRecord("pnd_Sub_Addr", "#SUB-ADDR", FieldType.NUMERIC, 2);
        pnd_Sub_Cnt = localVariables.newFieldInRecord("pnd_Sub_Cnt", "#SUB-CNT", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.NUMERIC, 3);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_table_Entry.reset();

        ldaCislext.initializeValues();
        ldaCisl601i.initializeValues();
        ldaCisl601m.initializeValues();
        ldaCisl602.initializeValues();
        ldaCisl603.initializeValues();
        ldaCisl604.initializeValues();
        ldaCisl605.initializeValues();
        ldaCisl607.initializeValues();
        ldaCisl608.initializeValues();
        ldaCisl609.initializeValues();
        ldaCisl610.initializeValues();
        ldaCisl611.initializeValues();
        ldaCisl612.initializeValues();
        ldaCisl613.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisb610() throws Exception
    {
        super("Cisb610");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cisb610|Main");
        setupReports();
        while(true)
        {
            try
            {
                //* **********************************************************************
                //*  MAIN LOGIC SECTION                                                  *
                //* **********************************************************************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                if (condition(pnd_Input_Parm.equals("REPRINT")))                                                                                                          //Natural: IF #INPUT-PARM = 'REPRINT'
                {
                    pnd_Reprint.setValue(true);                                                                                                                           //Natural: ASSIGN #REPRINT := TRUE
                }                                                                                                                                                         //Natural: END-IF
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 RECORD EX
                while (condition(getWorkFiles().read(1, ldaCislext.getEx())))
                {
                    //*                                                                                                                                                   //Natural: FORMAT LS = 133
                    pnd_Extr_Read.nadd(1);                                                                                                                                //Natural: ASSIGN #EXTR-READ := #EXTR-READ + 1
                                                                                                                                                                          //Natural: PERFORM GET-BENE-DATA
                    sub_Get_Bene_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-COMMON-DRIVER-DATA
                    sub_Move_Common_Driver_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(ldaCislext.getEx_Rqst_Id().equals("MDO")))                                                                                              //Natural: IF EX.RQST-ID = 'MDO'
                    {
                                                                                                                                                                          //Natural: PERFORM MOVE-MD-DRIVER-DATA
                        sub_Move_Md_Driver_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Mdo_Guid.nadd(+1);                                                                                                                            //Natural: ADD +1 TO #MDO-GUID
                        ldaCisl601m.getMd_Guid().setValue(pnd_Mdo_Guid);                                                                                                  //Natural: ASSIGN MD.GUID := #MDO-GUID
                        getWorkFiles().write(3, true, ldaCisl601m.getMd());                                                                                               //Natural: WRITE WORK FILE 3 VARIABLE MD
                        pnd_Mdo_Driver_Written.nadd(+1);                                                                                                                  //Natural: ADD +1 TO #MDO-DRIVER-WRITTEN
                        pnd_Tot_Written.nadd(+1);                                                                                                                         //Natural: ADD +1 TO #TOT-WRITTEN
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM MOVE-IA-DRIVER-DATA
                        sub_Move_Ia_Driver_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Ia_Guid.nadd(+1);                                                                                                                             //Natural: ADD +1 TO #IA-GUID
                        ldaCisl601i.getIa_Guid().setValue(pnd_Ia_Guid);                                                                                                   //Natural: ASSIGN IA.GUID := #IA-GUID
                        getWorkFiles().write(2, true, ldaCisl601i.getIa());                                                                                               //Natural: WRITE WORK FILE 2 VARIABLE IA
                        pnd_Ia_Driver_Written.nadd(+1);                                                                                                                   //Natural: ADD +1 TO #IA-DRIVER-WRITTEN
                        pnd_Tot_Written.nadd(+1);                                                                                                                         //Natural: ADD +1 TO #TOT-WRITTEN
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RECORD TYPE 02
                                                                                                                                                                          //Natural: PERFORM WRITE-PRIMARY-BENE-DATA
                    sub_Write_Primary_Bene_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  RECORD TYPE 03
                                                                                                                                                                          //Natural: PERFORM WRITE-CNTGNT-BENE-DATA
                    sub_Write_Cntgnt_Bene_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  RECORD TYPE 04
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-FUND-DATA
                    sub_Write_Cref_Fund_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  RECORD TYPE 05
                                                                                                                                                                          //Natural: PERFORM WRITE-COMMUT-DATA
                    sub_Write_Commut_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  RECORD TYPES 07, 08, 09
                                                                                                                                                                          //Natural: PERFORM WRITE-DA-DATA
                    sub_Write_Da_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  RECORD TYPES 10, 11
                                                                                                                                                                          //Natural: PERFORM WRITE-TRNSF-DATA
                    sub_Write_Trnsf_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  JRB2
                    //*  JRB1
                    if (condition(ldaCislext.getEx_Rqst_Id().equals("MDO") && ldaCislext.getEx_Invest_Num_Tot().greater(getZero())))                                      //Natural: IF EX.RQST-ID = 'MDO' AND EX.INVEST-NUM-TOT > 0
                    {
                        //*  RECORD TYPE 12  /* JRB1
                                                                                                                                                                          //Natural: PERFORM WRITE-FROM-INVEST-DATA
                        sub_Write_From_Invest_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  RECORD TYPE 13  /* JRB1
                                                                                                                                                                          //Natural: PERFORM WRITE-TO-INVEST-DATA
                        sub_Write_To_Invest_Data();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  JRB1
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                if (condition(pnd_Extr_Read.equals(getZero())))                                                                                                           //Natural: IF #EXTR-READ = 0
                {
                    getReports().display(0, "********  EXTRACT FILE IS EMPTY ********");                                                                                  //Natural: DISPLAY '********  EXTRACT FILE IS EMPTY ********'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(50);  if (true) return;                                                                                                             //Natural: TERMINATE 50
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  JRB1
                    //*  JRB1
                    getReports().write(0, "EXTRACT RECORDS READ:            ",pnd_Extr_Read,NEWLINE,"DRIVER RECORDS WRITTEN:          ",pnd_Tot_Written,                  //Natural: WRITE 'EXTRACT RECORDS READ:            ' #EXTR-READ / 'DRIVER RECORDS WRITTEN:          ' #TOT-WRITTEN / 'PRIMARY BENE RECORDS WRITTEN:    ' #PBENE-WRITTEN / 'CONTINGENT BENE RECORDS WRITTEN: ' #CBENE-WRITTEN / 'FUND RECORDS WRITTEN:            ' #FUND-WRITTEN / 'COMUT RECORDS WRITTEN:           ' #COMUT-WRITTEN / 'TIAA RECORDS WRITTEN:            ' #TIAA-WRITTEN / 'REA RECORDS WRITTEN:             ' #REA-WRITTEN / 'CREF RECORDS WRITTEN:            ' #CREF-WRITTEN / 'TIAA TRNS RECORDS WRITTEN:       ' #TTRN-WRITTEN / 'SEPA TRNS RECORDS WRITTEN:       ' #STRN-WRITTEN / 'FROM INVEST RECORDS WRITTEN:     ' #FINV-WRITTEN / 'TO INVEST RECORDS WRITTEN:       ' #TINV-WRITTEN
                        NEWLINE,"PRIMARY BENE RECORDS WRITTEN:    ",pnd_Pbene_Written,NEWLINE,"CONTINGENT BENE RECORDS WRITTEN: ",pnd_Cbene_Written,NEWLINE,
                        "FUND RECORDS WRITTEN:            ",pnd_Fund_Written,NEWLINE,"COMUT RECORDS WRITTEN:           ",pnd_Comut_Written,NEWLINE,"TIAA RECORDS WRITTEN:            ",
                        pnd_Tiaa_Written,NEWLINE,"REA RECORDS WRITTEN:             ",pnd_Rea_Written,NEWLINE,"CREF RECORDS WRITTEN:            ",pnd_Cref_Written,
                        NEWLINE,"TIAA TRNS RECORDS WRITTEN:       ",pnd_Ttrn_Written,NEWLINE,"SEPA TRNS RECORDS WRITTEN:       ",pnd_Strn_Written,NEWLINE,
                        "FROM INVEST RECORDS WRITTEN:     ",pnd_Finv_Written,NEWLINE,"TO INVEST RECORDS WRITTEN:       ",pnd_Tinv_Written);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-COMMON-DRIVER-DATA
                //*   WB - ACCRC START
                //*   WB - ACCRC END
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-IA-DRIVER-DATA
                //* *-------------------------------------------------------------------**
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-MD-DRIVER-DATA
                //* *-------------------------------------------------------------------**
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PRIMARY-BENE-DATA
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CNTGNT-BENE-DATA
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CREF-FUND-DATA
                //* *-------------------------------------------------------------------**
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-COMMUT-DATA
                //* *-------------------------------------------------------------------**
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DA-DATA
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TRNSF-DATA
                //* *-------------------------------------------------------------------**
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-FROM-INVEST-DATA
                //* **********************************************************************
                //*  FROM INVEST RECORD 12 - WRITES BOTH THE HEADER AND DETAIL DATA     *
                //*  IN ONE RECORD.  WHEN THE HEADER DATA IS POPULATED, THE DETAIL DATA  *
                //*  IS BLANK.  WHEN THE DETAIL DATA IS POPULATED, THE HEADER RECORD     *
                //*  IS BLANK.                                                           *
                //* **********************************************************************
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TO-INVEST-DATA
                //* **********************************************************************
                //*  TO INVEST RECORD 13 - WRITES BOTH THE HEADER AND DETAIL DATA        *
                //*  IN ONE RECORD.  WHEN THE HEADER DATA IS POPULATED, THE DETAIL DATA  *
                //*  IS BLANK.  WHEN THE DETAIL DATA IS POPULATED, THE HEADER RECORD     *
                //*  IS BLANK.                                                           *
                //* **********************************************************************
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-PROFILE
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BENE-DATA
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-ZIP-POSITION
                //* *-------------------------------------------------------------------**
                //* *-------------------------------------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-ZIP
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Move_Common_Driver_Data() throws Exception                                                                                                           //Natural: MOVE-COMMON-DRIVER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------------------------**
        ldaCisl601i.getIa().reset();                                                                                                                                      //Natural: RESET IA MD
        ldaCisl601m.getMd().reset();
                                                                                                                                                                          //Natural: PERFORM LOOKUP-PROFILE
        sub_Lookup_Profile();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(pnd_Reprint.getBoolean()))                                                                                                                          //Natural: IF #REPRINT
        {
            ldaCisl601m.getMd_Reprint_Ind().setValue("R");                                                                                                                //Natural: ASSIGN MD.REPRINT-IND := IA.REPRINT-IND := 'R'
            ldaCisl601i.getIa_Reprint_Ind().setValue("R");
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCisl601m.getMd_Reprint_Ind().setValue(" ");                                                                                                                //Natural: ASSIGN MD.REPRINT-IND := IA.REPRINT-IND := ' '
            ldaCisl601i.getIa_Reprint_Ind().setValue(" ");
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl601m.getMd_Record_Type().setValue("01");                                                                                                                   //Natural: ASSIGN MD.RECORD-TYPE := IA.RECORD-TYPE := '01'
        ldaCisl601i.getIa_Record_Type().setValue("01");
        ldaCisl601m.getMd_Contract_Type().setValue(ldaCislext.getEx_Contract_Type());                                                                                     //Natural: ASSIGN MD.CONTRACT-TYPE := IA.CONTRACT-TYPE := EX.CONTRACT-TYPE
        ldaCisl601i.getIa_Contract_Type().setValue(ldaCislext.getEx_Contract_Type());
        ldaCisl601m.getMd_Rqst_Id().setValue(ldaCislext.getEx_Rqst_Id());                                                                                                 //Natural: ASSIGN MD.RQST-ID := IA.RQST-ID := EX.RQST-ID
        ldaCisl601i.getIa_Rqst_Id().setValue(ldaCislext.getEx_Rqst_Id());
        ldaCisl601m.getMd_Tiaa_Numb().setValue(ldaCislext.getEx_Tiaa_Numb());                                                                                             //Natural: ASSIGN MD.TIAA-NUMB := IA.TIAA-NUMB := EX.TIAA-NUMB
        ldaCisl601i.getIa_Tiaa_Numb().setValue(ldaCislext.getEx_Tiaa_Numb());
        ldaCisl601m.getMd_Cref_Cert_Numb().setValue(ldaCislext.getEx_Cref_Cert_Numb());                                                                                   //Natural: ASSIGN MD.CREF-CERT-NUMB := IA.CREF-CERT-NUMB := EX.CREF-CERT-NUMB
        ldaCisl601i.getIa_Cref_Cert_Numb().setValue(ldaCislext.getEx_Cref_Cert_Numb());
        ldaCisl601m.getMd_Tiaa_Issue_Date().setValue(ldaCislext.getEx_Tiaa_Issue_Date());                                                                                 //Natural: ASSIGN MD.TIAA-ISSUE-DATE := IA.TIAA-ISSUE-DATE := EX.TIAA-ISSUE-DATE
        ldaCisl601i.getIa_Tiaa_Issue_Date().setValue(ldaCislext.getEx_Tiaa_Issue_Date());
        ldaCisl601m.getMd_Cref_Issue_Date().setValue(ldaCislext.getEx_Cref_Issue_Date());                                                                                 //Natural: ASSIGN MD.CREF-ISSUE-DATE := IA.CREF-ISSUE-DATE := EX.CREF-ISSUE-DATE
        ldaCisl601i.getIa_Cref_Issue_Date().setValue(ldaCislext.getEx_Cref_Issue_Date());
        ldaCisl601m.getMd_First_Payt_Date().setValue(ldaCislext.getEx_First_Payt_Date());                                                                                 //Natural: ASSIGN MD.FIRST-PAYT-DATE := IA.FIRST-PAYT-DATE := EX.FIRST-PAYT-DATE
        ldaCisl601i.getIa_First_Payt_Date().setValue(ldaCislext.getEx_First_Payt_Date());
        ldaCisl601m.getMd_Payt_Frequency().setValue(ldaCislext.getEx_Payt_Frequency());                                                                                   //Natural: ASSIGN MD.PAYT-FREQUENCY := IA.PAYT-FREQUENCY := EX.PAYT-FREQUENCY
        ldaCisl601i.getIa_Payt_Frequency().setValue(ldaCislext.getEx_Payt_Frequency());
        ldaCisl601m.getMd_Annt_Dob().setValue(ldaCislext.getEx_Annt_Dob());                                                                                               //Natural: ASSIGN MD.ANNT-DOB := IA.ANNT-DOB := EX.ANNT-DOB
        ldaCisl601i.getIa_Annt_Dob().setValue(ldaCislext.getEx_Annt_Dob());
        ldaCisl601m.getMd_Annt_Contract_Name().setValue(ldaCislext.getEx_Annt_Contract_Name());                                                                           //Natural: ASSIGN MD.ANNT-CONTRACT-NAME := IA.ANNT-CONTRACT-NAME := EX.ANNT-CONTRACT-NAME
        ldaCisl601i.getIa_Annt_Contract_Name().setValue(ldaCislext.getEx_Annt_Contract_Name());
        ldaCisl601m.getMd_Annt_Name_Last().setValue(ldaCislext.getEx_Annt_Name_Last());                                                                                   //Natural: ASSIGN MD.ANNT-NAME-LAST := IA.ANNT-NAME-LAST := EX.ANNT-NAME-LAST
        ldaCisl601i.getIa_Annt_Name_Last().setValue(ldaCislext.getEx_Annt_Name_Last());
        ldaCisl601m.getMd_Annt_Name_First().setValue(ldaCislext.getEx_Annt_Name_First());                                                                                 //Natural: ASSIGN MD.ANNT-NAME-FIRST := IA.ANNT-NAME-FIRST := EX.ANNT-NAME-FIRST
        ldaCisl601i.getIa_Annt_Name_First().setValue(ldaCislext.getEx_Annt_Name_First());
        ldaCisl601m.getMd_Annt_Name_Middle().setValue(ldaCislext.getEx_Annt_Name_Middle());                                                                               //Natural: ASSIGN MD.ANNT-NAME-MIDDLE := IA.ANNT-NAME-MIDDLE := EX.ANNT-NAME-MIDDLE
        ldaCisl601i.getIa_Annt_Name_Middle().setValue(ldaCislext.getEx_Annt_Name_Middle());
        ldaCisl601m.getMd_Annt_Name_Prefix().setValue(ldaCislext.getEx_Annt_Name_Prefix());                                                                               //Natural: ASSIGN MD.ANNT-NAME-PREFIX := IA.ANNT-NAME-PREFIX := EX.ANNT-NAME-PREFIX
        ldaCisl601i.getIa_Annt_Name_Prefix().setValue(ldaCislext.getEx_Annt_Name_Prefix());
        ldaCisl601m.getMd_Annt_Name_Suffix().setValue(ldaCislext.getEx_Annt_Name_Suffix());                                                                               //Natural: ASSIGN MD.ANNT-NAME-SUFFIX := IA.ANNT-NAME-SUFFIX := EX.ANNT-NAME-SUFFIX
        ldaCisl601i.getIa_Annt_Name_Suffix().setValue(ldaCislext.getEx_Annt_Name_Suffix());
        ldaCisl601m.getMd_Annt_Citizenship().setValue(ldaCislext.getEx_Annt_Citizenship());                                                                               //Natural: ASSIGN MD.ANNT-CITIZENSHIP := IA.ANNT-CITIZENSHIP := EX.ANNT-CITIZENSHIP
        ldaCisl601i.getIa_Annt_Citizenship().setValue(ldaCislext.getEx_Annt_Citizenship());
        ldaCisl601m.getMd_Annt_Soc_Numb().setValue("ON FILE");                                                                                                            //Natural: ASSIGN MD.ANNT-SOC-NUMB := IA.ANNT-SOC-NUMB := 'ON FILE'
        ldaCisl601i.getIa_Annt_Soc_Numb().setValue("ON FILE");
        ldaCisl601m.getMd_Address_Name().setValue(ldaCislext.getEx_Address_Name());                                                                                       //Natural: ASSIGN MD.ADDRESS-NAME := IA.ADDRESS-NAME := EX.ADDRESS-NAME
        ldaCisl601i.getIa_Address_Name().setValue(ldaCislext.getEx_Address_Name());
        ldaCisl601m.getMd_Welcome_Name().setValue(ldaCislext.getEx_Welcome_Name());                                                                                       //Natural: ASSIGN MD.WELCOME-NAME := IA.WELCOME-NAME := EX.WELCOME-NAME
        ldaCisl601i.getIa_Welcome_Name().setValue(ldaCislext.getEx_Welcome_Name());
        ldaCisl601m.getMd_Directory_Name().setValue(ldaCislext.getEx_Directory_Name());                                                                                   //Natural: ASSIGN MD.DIRECTORY-NAME := IA.DIRECTORY-NAME := EX.DIRECTORY-NAME
        ldaCisl601i.getIa_Directory_Name().setValue(ldaCislext.getEx_Directory_Name());
        ldaCisl601m.getMd_Zip().setValue(ldaCislext.getEx_Zip());                                                                                                         //Natural: ASSIGN MD.ZIP := IA.ZIP := EX.ZIP
        ldaCisl601i.getIa_Zip().setValue(ldaCislext.getEx_Zip());
        ldaCisl601m.getMd_Pin_Numb().setValue(ldaCislext.getEx_Pin_Numb());                                                                                               //Natural: ASSIGN MD.PIN-NUMB := IA.PIN-NUMB := EX.PIN-NUMB
        ldaCisl601i.getIa_Pin_Numb().setValue(ldaCislext.getEx_Pin_Numb());
        ldaCisl601m.getMd_Guar_Intr_Rate().setValue(ldaCislext.getEx_Guar_Intr_Rate());                                                                                   //Natural: ASSIGN MD.GUAR-INTR-RATE := IA.GUAR-INTR-RATE := EX.GUAR-INTR-RATE
        ldaCisl601i.getIa_Guar_Intr_Rate().setValue(ldaCislext.getEx_Guar_Intr_Rate());
        //*  CNTRSTRG
        ldaCisl601m.getMd_Real_Estate_Units().setValue(ldaCislext.getEx_Rea_Annualy_Units(), MoveOption.LeftJustified);                                                   //Natural: MOVE LEFT JUSTIFIED EX.REA-ANNUALY-UNITS TO MD.REAL-ESTATE-UNITS
        ldaCisl601i.getIa_Rea_Annualy_Units().setValue(ldaCisl601m.getMd_Real_Estate_Units());                                                                            //Natural: ASSIGN IA.REA-ANNUALY-UNITS := MD.REAL-ESTATE-UNITS
        ldaCisl601m.getMd_Seprate_Accnt_Chrg().setValue(ldaCislext.getEx_Seprate_Accnt_Chrg());                                                                           //Natural: ASSIGN MD.SEPRATE-ACCNT-CHRG := IA.SEPRATE-ACCNT-CHRG := EX.SEPRATE_ACCNT-CHRG
        ldaCisl601i.getIa_Seprate_Accnt_Chrg().setValue(ldaCislext.getEx_Seprate_Accnt_Chrg());
        ldaCisl601m.getMd_State_Name().setValue(ldaCislext.getEx_State_Name());                                                                                           //Natural: ASSIGN MD.STATE-NAME := IA.STATE-NAME := EX.STATE-NAME
        ldaCisl601i.getIa_State_Name().setValue(ldaCislext.getEx_State_Name());
        ldaCisl601m.getMd_Inst_Name().setValue(ldaCislext.getEx_Inst_Name());                                                                                             //Natural: ASSIGN MD.INST-NAME := IA.INST-NAME := EX.INST-NAME
        ldaCisl601i.getIa_Inst_Name().setValue(ldaCislext.getEx_Inst_Name());
        ldaCisl601m.getMd_Res_Issue_State().setValue(ldaCislext.getEx_Res_Issue_State());                                                                                 //Natural: ASSIGN MD.RES-ISSUE-STATE := IA.RES-ISSUE-STATE := EX.RES-ISSUE-STATE
        ldaCisl601i.getIa_Res_Issue_State().setValue(ldaCislext.getEx_Res_Issue_State());
        ldaCisl601m.getMd_Lob().setValue(ldaCislext.getEx_Lob());                                                                                                         //Natural: ASSIGN MD.LOB := IA.LOB := EX.LOB
        ldaCisl601i.getIa_Lob().setValue(ldaCislext.getEx_Lob());
        ldaCisl601m.getMd_Lob_Type().setValue(ldaCislext.getEx_Lob_Type());                                                                                               //Natural: ASSIGN MD.LOB-TYPE := IA.LOB-TYPE := EX.LOB-TYPE
        ldaCisl601i.getIa_Lob_Type().setValue(ldaCislext.getEx_Lob_Type());
        ldaCisl601m.getMd_Region().setValue(ldaCislext.getEx_Region());                                                                                                   //Natural: ASSIGN MD.REGION := IA.REGION := EX.REGION
        ldaCisl601i.getIa_Region().setValue(ldaCislext.getEx_Region());
        ldaCisl601m.getMd_Branch().setValue(ldaCislext.getEx_Branch());                                                                                                   //Natural: ASSIGN MD.BRANCH := IA.BRANCH := EX.BRANCH
        ldaCisl601i.getIa_Branch().setValue(ldaCislext.getEx_Branch());
        ldaCisl601m.getMd_Need().setValue(ldaCislext.getEx_Need());                                                                                                       //Natural: ASSIGN MD.NEED := IA.NEED := EX.NEED
        ldaCisl601i.getIa_Need().setValue(ldaCislext.getEx_Need());
        ldaCisl601m.getMd_Ppg_Code().setValue(ldaCislext.getEx_Ppg_Code());                                                                                               //Natural: ASSIGN MD.PPG-CODE := IA.PPG-CODE := EX.PPG-CODE
        ldaCisl601i.getIa_Ppg_Code().setValue(ldaCislext.getEx_Ppg_Code());
        ldaCisl601m.getMd_Cntrct_Apprvl().setValue(ldaCislext.getEx_Cntrct_Apprvl());                                                                                     //Natural: ASSIGN MD.CNTRCT-APPRVL := IA.CNTRCT-APPRVL := EX.CNTRCT-APPRVL
        ldaCisl601i.getIa_Cntrct_Apprvl().setValue(ldaCislext.getEx_Cntrct_Apprvl());
        ldaCisl601m.getMd_Cntrct_Option().setValue(ldaCislext.getEx_Cntrct_Option());                                                                                     //Natural: ASSIGN MD.CNTRCT-OPTION := IA.CNTRCT-OPTION := EX.CNTRCT-OPTION
        ldaCisl601i.getIa_Cntrct_Option().setValue(ldaCislext.getEx_Cntrct_Option());
        ldaCisl601m.getMd_Cntrct_Cash_Status().setValue(ldaCislext.getEx_Cntrct_Cash_Status());                                                                           //Natural: ASSIGN MD.CNTRCT-CASH-STATUS := IA.CNTRCT-CASH-STATUS := EX.CNTRCT-CASH-STATUS
        ldaCisl601i.getIa_Cntrct_Cash_Status().setValue(ldaCislext.getEx_Cntrct_Cash_Status());
        ldaCisl601m.getMd_Cntrct_Retr_Surv().setValue(ldaCislext.getEx_Cntrct_Retr_Surv());                                                                               //Natural: ASSIGN MD.CNTRCT-RETR-SURV := IA.CNTRCT-RETR-SURV := EX.CNTRCT-RETR-SURV
        ldaCisl601i.getIa_Cntrct_Retr_Surv().setValue(ldaCislext.getEx_Cntrct_Retr_Surv());
        ldaCisl601m.getMd_Guar_Period_Flag().setValue(ldaCislext.getEx_Guar_Period_Flag());                                                                               //Natural: ASSIGN MD.GUAR-PERIOD-FLAG := IA.GUAR-PERIOD-FLAG := EX.GUAR-PERIOD-FLAG
        ldaCisl601i.getIa_Guar_Period_Flag().setValue(ldaCislext.getEx_Guar_Period_Flag());
        ldaCisl601m.getMd_Multiple_Intr_Rate().setValue(ldaCislext.getEx_Multiple_Intr_Rate());                                                                           //Natural: ASSIGN MD.MULTIPLE-INTR-RATE := IA.MULTIPLE-INTR-RATE := EX.MULTIPLE-INTR-RATE
        ldaCisl601i.getIa_Multiple_Intr_Rate().setValue(ldaCislext.getEx_Multiple_Intr_Rate());
        ldaCisl601m.getMd_Tiaa_Cntr_Settled().setValue(ldaCislext.getEx_Tiaa_Cntr_Settled());                                                                             //Natural: ASSIGN MD.TIAA-CNTR-SETTLED := IA.TIAA-CNTR-SETTLED := EX.TIAA-CNTR-SETTLED
        ldaCisl601i.getIa_Tiaa_Cntr_Settled().setValue(ldaCislext.getEx_Tiaa_Cntr_Settled());
        ldaCisl601m.getMd_Rea_Cntr_Settled().setValue(ldaCislext.getEx_Rea_Cntr_Settled());                                                                               //Natural: ASSIGN MD.REA-CNTR-SETTLED := IA.REA-CNTR-SETTLED := EX.REA-CNTR-SETTLED
        ldaCisl601i.getIa_Rea_Cntr_Settled().setValue(ldaCislext.getEx_Rea_Cntr_Settled());
        ldaCisl601m.getMd_Cref_Cntr_Settled().setValue(ldaCislext.getEx_Cref_Cntr_Settled());                                                                             //Natural: ASSIGN MD.CREF-CNTR-SETTLED := IA.CREF-CNTR-SETTLED := EX.CREF-CNTR-SETTLED
        ldaCisl601i.getIa_Cref_Cntr_Settled().setValue(ldaCislext.getEx_Cref_Cntr_Settled());
        ldaCisl601m.getMd_Cref_Accnt_Number().setValue(ldaCislext.getEx_Cref_Accnt_Number());                                                                             //Natural: ASSIGN MD.CREF-ACCNT-NUMBER := IA.CREF-ACCNT-NUMBER := EX.CREF-ACCNT-NUMBER
        ldaCisl601i.getIa_Cref_Accnt_Number().setValue(ldaCislext.getEx_Cref_Accnt_Number());
        ldaCisl601m.getMd_Internal_Trnsf().setValue(ldaCislext.getEx_Internal_Trnsf());                                                                                   //Natural: ASSIGN MD.INTERNAL-TRNSF := IA.INTERNAL-TRNSF := EX.INTERNAL-TRNSF
        ldaCisl601i.getIa_Internal_Trnsf().setValue(ldaCislext.getEx_Internal_Trnsf());
        ldaCisl601m.getMd_Revaluation_Cde().setValue(ldaCislext.getEx_Revaluation_Cde());                                                                                 //Natural: ASSIGN MD.REVALUATION-CDE := IA.REVALUATION-CDE := EX.REVALUATION-CDE
        ldaCisl601i.getIa_Revaluation_Cde().setValue(ldaCislext.getEx_Revaluation_Cde());
        ldaCisl601m.getMd_Orig_Issue_State().setValue(ldaCislext.getEx_Orig_Issue_State());                                                                               //Natural: ASSIGN MD.ORIG-ISSUE-STATE := IA.ORIG-ISSUE-STATE := EX.ORIG-ISSUE-STATE
        ldaCisl601i.getIa_Orig_Issue_State().setValue(ldaCislext.getEx_Orig_Issue_State());
        ldaCisl601m.getMd_Ownership_Code().setValue(ldaCislext.getEx_Ownership_Code());                                                                                   //Natural: ASSIGN MD.OWNERSHIP-CODE := IA.OWNERSHIP-CODE := EX.OWNERSHIP-CODE
        ldaCisl601i.getIa_Ownership_Code().setValue(ldaCislext.getEx_Ownership_Code());
        ldaCisl601m.getMd_Rtb_Request().setValue(ldaCislext.getEx_Rtb_Request());                                                                                         //Natural: ASSIGN MD.RTB-REQUEST := IA.RTB-REQUEST := EX.RTB-REQUEST
        ldaCisl601i.getIa_Rtb_Request().setValue(ldaCislext.getEx_Rtb_Request());
        ldaCisl601m.getMd_Issue_Eq_Frst_Pymt().setValue(ldaCislext.getEx_Issue_Eq_Frst_Pymt());                                                                           //Natural: ASSIGN MD.ISSUE-EQ-FRST-PYMT := IA.ISSUE-EQ-FRST-PYMT := EX.ISSUE-EQ-FRST-PYMT
        ldaCisl601i.getIa_Issue_Eq_Frst_Pymt().setValue(ldaCislext.getEx_Issue_Eq_Frst_Pymt());
        ldaCisl601m.getMd_Gsra_Ira_Surr_Chrg().setValue(ldaCislext.getEx_Gsra_Ira_Surr_Chrg());                                                                           //Natural: ASSIGN MD.GSRA-IRA-SURR-CHRG := IA.GSRA-IRA-SURR-CHRG := EX.GSRA-IRA-SURR-CHRG
        ldaCisl601i.getIa_Gsra_Ira_Surr_Chrg().setValue(ldaCislext.getEx_Gsra_Ira_Surr_Chrg());
        ldaCisl601m.getMd_Da_Death_Sur_Right().setValue(ldaCislext.getEx_Da_Death_Sur_Right());                                                                           //Natural: ASSIGN MD.DA-DEATH-SUR-RIGHT := IA.DA-DEATH-SUR-RIGHT := EX.DA-DEATH-SUR-RIGHT
        ldaCisl601i.getIa_Da_Death_Sur_Right().setValue(ldaCislext.getEx_Da_Death_Sur_Right());
        ldaCisl601m.getMd_Gra_Surr_Right().setValue(ldaCislext.getEx_Gra_Surr_Right());                                                                                   //Natural: ASSIGN MD.GRA-SURR-RIGHT := IA.GRA-SURR-RIGHT := EX.GRA-SURR-RIGHT
        ldaCisl601i.getIa_Gra_Surr_Right().setValue(ldaCislext.getEx_Gra_Surr_Right());
        ldaCisl601m.getMd_Personal_Annuity().setValue(ldaCislext.getEx_Personal_Annuity());                                                                               //Natural: ASSIGN MD.PERSONAL-ANNUITY := IA.PERSONAL-ANNUITY := EX.PERSONAL-ANNUITY
        ldaCisl601i.getIa_Personal_Annuity().setValue(ldaCislext.getEx_Personal_Annuity());
        ldaCisl601m.getMd_Mit_Orgnl_Unit_Cde().setValue(ldaCislext.getEx_Mit_Orgnl_Unit_Cde());                                                                           //Natural: ASSIGN MD.MIT-ORGNL-UNIT-CDE := IA.MIT-ORGNL-UNIT-CDE := EX.MIT-ORGNL-UNIT-CDE
        ldaCisl601i.getIa_Mit_Orgnl_Unit_Cde().setValue(ldaCislext.getEx_Mit_Orgnl_Unit_Cde());
        ldaCisl601m.getMd_Graded_Ind().setValue(ldaCislext.getEx_Graded_Ind());                                                                                           //Natural: ASSIGN MD.GRADED-IND := IA.GRADED-IND := EX.GRADED-IND
        ldaCisl601i.getIa_Graded_Ind().setValue(ldaCislext.getEx_Graded_Ind());
        ldaCisl601m.getMd_Frst_Pymt_After_Iss().setValue(ldaCislext.getEx_Frst_Pymt_After_Iss());                                                                         //Natural: ASSIGN MD.FRST-PYMT-AFTER-ISS := IA.FRST-PYMT-AFTER-ISS := EX.FRST-PYMT-AFTER-ISS
        ldaCisl601i.getIa_Frst_Pymt_After_Iss().setValue(ldaCislext.getEx_Frst_Pymt_After_Iss());
        ldaCisl601m.getMd_Tiaa_Cntrct_Type().setValue(ldaCislext.getEx_Tiaa_Cntrct_Type());                                                                               //Natural: ASSIGN MD.TIAA-CNTRCT-TYPE := IA.TIAA-CNTRCT-TYPE := EX.TIAA-CNTRCT-TYPE
        ldaCisl601i.getIa_Tiaa_Cntrct_Type().setValue(ldaCislext.getEx_Tiaa_Cntrct_Type());
        ldaCisl601m.getMd_Rea_Cntrct_Type().setValue(ldaCislext.getEx_Rea_Cntrct_Type());                                                                                 //Natural: ASSIGN MD.REA-CNTRCT-TYPE := IA.REA-CNTRCT-TYPE := EX.REA-CNTRCT-TYPE
        ldaCisl601i.getIa_Rea_Cntrct_Type().setValue(ldaCislext.getEx_Rea_Cntrct_Type());
        ldaCisl601m.getMd_Cref_Cntrct_Type().setValue(ldaCislext.getEx_Cref_Cntrct_Type());                                                                               //Natural: ASSIGN MD.CREF-CNTRCT-TYPE := IA.CREF-CNTRCT-TYPE := EX.CREF-CNTRCT-TYPE
        ldaCisl601i.getIa_Cref_Cntrct_Type().setValue(ldaCislext.getEx_Cref_Cntrct_Type());
        ldaCisl601m.getMd_Comut_Intr_Num().setValue(ldaCislext.getEx_Comut_Intr_Num());                                                                                   //Natural: ASSIGN MD.COMUT-INTR-NUM := IA.COMUT-INTR-NUM := EX.COMUT-INTR-NUM
        ldaCisl601i.getIa_Comut_Intr_Num().setValue(ldaCislext.getEx_Comut_Intr_Num());
        ldaCisl601m.getMd_Trnsf_Cntr_Settled().setValue(ldaCislext.getEx_Trnsf_Cntr_Settled());                                                                           //Natural: ASSIGN MD.TRNSF-CNTR-SETTLED := IA.TRNSF-CNTR-SETTLED := EX.TRNSF-CNTR-SETTLED
        ldaCisl601i.getIa_Trnsf_Cntr_Settled().setValue(ldaCislext.getEx_Trnsf_Cntr_Settled());
        ldaCisl601m.getMd_Spec_Cntrct_Type().setValue(ldaCislext.getEx_Spec_Cntrct_Type());                                                                               //Natural: ASSIGN MD.SPEC-CNTRCT-TYPE := IA.SPEC-CNTRCT-TYPE := EX.SPEC-CNTRCT-TYPE
        ldaCisl601i.getIa_Spec_Cntrct_Type().setValue(ldaCislext.getEx_Spec_Cntrct_Type());
        ldaCisl601m.getMd_Orig_Issue_Date().setValue(ldaCislext.getEx_Orig_Issue_Date());                                                                                 //Natural: ASSIGN MD.ORIG-ISSUE-DATE := IA.ORIG-ISSUE-DATE := EX.ORIG-ISSUE-DATE
        ldaCisl601i.getIa_Orig_Issue_Date().setValue(ldaCislext.getEx_Orig_Issue_Date());
        ldaCisl601m.getMd_Access_Ind().setValue(ldaCislext.getEx_Access_Ind());                                                                                           //Natural: ASSIGN MD.ACCESS-IND := IA.ACCESS-IND := EX.ACCESS-IND
        ldaCisl601i.getIa_Access_Ind().setValue(ldaCislext.getEx_Access_Ind());
        ldaCisl601m.getMd_Roth_Ind().setValue(ldaCislext.getEx_Roth_Ind());                                                                                               //Natural: ASSIGN MD.ROTH-IND := IA.ROTH-IND := EX.ROTH-IND
        ldaCisl601i.getIa_Roth_Ind().setValue(ldaCislext.getEx_Roth_Ind());
        ldaCisl601m.getMd_Payee_Ind().setValue(ldaCislext.getEx_Payee_Ind());                                                                                             //Natural: ASSIGN MD.PAYEE-IND := IA.PAYEE-IND := EX.PAYEE-IND
        ldaCisl601i.getIa_Payee_Ind().setValue(ldaCislext.getEx_Payee_Ind());
        ldaCisl601m.getMd_Multi_Plan_Ind().setValue(ldaCislext.getEx_Multi_Plan_Ind());                                                                                   //Natural: ASSIGN MD.MULTI-PLAN-IND := IA.MULTI-PLAN-IND := EX.MULTI-PLAN-IND
        ldaCisl601i.getIa_Multi_Plan_Ind().setValue(ldaCislext.getEx_Multi_Plan_Ind());
        ldaCisl601i.getIa_First_Tpa_Or_Ipro_Ind().setValue(ldaCislext.getEx_First_Tpa_Or_Ipro_Ind());                                                                     //Natural: ASSIGN IA.FIRST-TPA-OR-IPRO-IND := EX.FIRST-TPA-OR-IPRO-IND
        pnd_Format_Amt.setValue(ldaCislext.getEx_Da_Tiaa_Total_Amt());                                                                                                    //Natural: ASSIGN #FORMAT-AMT := EX.DA-TIAA-TOTAL-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601m.getMd_Da_Tiaa_Total_Amt().setValue(pnd_Format_Amt);                                                                                                   //Natural: ASSIGN MD.DA-TIAA-TOTAL-AMT := IA.DA-TIAA-TOTAL-AMT := #FORMAT-AMT
        ldaCisl601i.getIa_Da_Tiaa_Total_Amt().setValue(pnd_Format_Amt);
        pnd_Format_Amt.setValue(ldaCislext.getEx_Real_Estate_Amt());                                                                                                      //Natural: ASSIGN #FORMAT-AMT := EX.REAL-ESTATE-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601m.getMd_Real_Estate_Amt().setValue(pnd_Format_Amt);                                                                                                     //Natural: ASSIGN MD.REAL-ESTATE-AMT := IA.REAL-ESTATE-AMT := #FORMAT-AMT
        ldaCisl601i.getIa_Real_Estate_Amt().setValue(pnd_Format_Amt);
        pnd_Format_Amt.setValue(ldaCislext.getEx_Da_Cref_Total_Amt());                                                                                                    //Natural: ASSIGN #FORMAT-AMT := EX.DA-CREF-TOTAL-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601m.getMd_Da_Cref_Total_Amt().setValue(pnd_Format_Amt);                                                                                                   //Natural: ASSIGN MD.DA-CREF-TOTAL-AMT := IA.DA-CREF-TOTAL-AMT := #FORMAT-AMT
        ldaCisl601i.getIa_Da_Cref_Total_Amt().setValue(pnd_Format_Amt);
        pnd_Format_Amt.setValue(ldaCislext.getEx_Traditional_Amt());                                                                                                      //Natural: ASSIGN #FORMAT-AMT := EX.TRADITIONAL-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601m.getMd_Traditional_Amt().setValue(pnd_Format_Amt);                                                                                                     //Natural: ASSIGN MD.TRADITIONAL-AMT := IA.LAST-PYMT-AMT := #FORMAT-AMT
        ldaCisl601i.getIa_Last_Pymt_Amt().setValue(pnd_Format_Amt);
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            ldaCisl601m.getMd_Tiaa_Headers().getValue(pnd_I).setValue(ldaCislext.getEx_Tiaa_Headers().getValue(pnd_I));                                                   //Natural: ASSIGN MD.TIAA-HEADERS ( #I ) := IA.TIAA-HEADERS ( #I ) := EX.TIAA-HEADERS ( #I )
            ldaCisl601i.getIa_Tiaa_Headers().getValue(pnd_I).setValue(ldaCislext.getEx_Tiaa_Headers().getValue(pnd_I));
            ldaCisl601m.getMd_Cref_Headers().getValue(pnd_I).setValue(ldaCislext.getEx_Cref_Headers().getValue(pnd_I));                                                   //Natural: ASSIGN MD.CREF-HEADERS ( #I ) := IA.CREF-HEADERS ( #I ) := EX.CREF-HEADERS ( #I )
            ldaCisl601i.getIa_Cref_Headers().getValue(pnd_I).setValue(ldaCislext.getEx_Cref_Headers().getValue(pnd_I));
            ldaCisl601m.getMd_Tiaa_Form_Numb().getValue(pnd_I).setValue(ldaCislext.getEx_Tiaa_Form_Numb().getValue(pnd_I));                                               //Natural: ASSIGN MD.TIAA-FORM-NUMB ( #I ) := IA.TIAA-FORM-NUMB ( #I ) := EX.TIAA-FORM-NUMB ( #I )
            ldaCisl601i.getIa_Tiaa_Form_Numb().getValue(pnd_I).setValue(ldaCislext.getEx_Tiaa_Form_Numb().getValue(pnd_I));
            ldaCisl601m.getMd_Cref_Form_Numb().getValue(pnd_I).setValue(ldaCislext.getEx_Cref_Form_Numb().getValue(pnd_I));                                               //Natural: ASSIGN MD.CREF-FORM-NUMB ( #I ) := IA.CREF-FORM-NUMB ( #I ) := EX.CREF-FORM-NUMB ( #I )
            ldaCisl601i.getIa_Cref_Form_Numb().getValue(pnd_I).setValue(ldaCislext.getEx_Cref_Form_Numb().getValue(pnd_I));
            ldaCisl601m.getMd_Tiaa_Prod_Cdes().getValue(pnd_I).setValue(ldaCislext.getEx_Tiaa_Prod_Cdes().getValue(pnd_I));                                               //Natural: ASSIGN MD.TIAA-PROD-CDES ( #I ) := IA.TIAA-PROD-CDES ( #I ) := EX.TIAA-PROD-CDES ( #I )
            ldaCisl601i.getIa_Tiaa_Prod_Cdes().getValue(pnd_I).setValue(ldaCislext.getEx_Tiaa_Prod_Cdes().getValue(pnd_I));
            ldaCisl601m.getMd_Cref_Prod_Cdes().getValue(pnd_I).setValue(ldaCislext.getEx_Cref_Prod_Cdes().getValue(pnd_I));                                               //Natural: ASSIGN MD.CREF-PROD-CDES ( #I ) := IA.CREF-PROD-CDES ( #I ) := EX.CREF-PROD-CDES ( #I )
            ldaCisl601i.getIa_Cref_Prod_Cdes().getValue(pnd_I).setValue(ldaCislext.getEx_Cref_Prod_Cdes().getValue(pnd_I));
            ldaCisl601i.getIa_Tiaa_Ed_Numb().getValue(pnd_I).setValue(ldaCislext.getEx_Tiaa_Ed_Numb().getValue(pnd_I), MoveOption.LeftJustified);                         //Natural: MOVE LEFT JUSTIFIED EX.TIAA-ED-NUMB ( #I ) TO IA.TIAA-ED-NUMB ( #I )
            ldaCisl601m.getMd_Tiaa_Ed_Numb().getValue(pnd_I).setValue(ldaCisl601i.getIa_Tiaa_Ed_Numb().getValue(pnd_I));                                                  //Natural: ASSIGN MD.TIAA-ED-NUMB ( #I ) := IA.TIAA-ED-NUMB ( #I )
            ldaCisl601i.getIa_Cref_Ed_Numb().getValue(pnd_I).setValue(ldaCislext.getEx_Cref_Ed_Numb().getValue(pnd_I), MoveOption.LeftJustified);                         //Natural: MOVE LEFT JUSTIFIED EX.CREF-ED-NUMB ( #I ) TO IA.CREF-ED-NUMB ( #I )
            ldaCisl601m.getMd_Cref_Ed_Numb().getValue(pnd_I).setValue(ldaCisl601i.getIa_Cref_Ed_Numb().getValue(pnd_I));                                                  //Natural: ASSIGN MD.CREF-ED-NUMB ( #I ) := IA.CREF-ED-NUMB ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaCisl601m.getMd_Estate_Cntgnt().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Contain_Estate());                                                               //Natural: ASSIGN MD.ESTATE-CNTGNT := #CISA-CNTGNT-CONTAIN-ESTATE
        ldaCisl601i.getIa_Estate_Cntgnt().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Contain_Estate());                                                               //Natural: ASSIGN IA.ESTATE-CNTGNT := #CISA-CNTGNT-CONTAIN-ESTATE
        ldaCisl601m.getMd_Freeform_Ben().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Bene_Std_Free());                                                                        //Natural: ASSIGN MD.FREEFORM-BEN := #CISA-BENE-STD-FREE
        ldaCisl601i.getIa_Freeform_Ben().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Bene_Std_Free());                                                                        //Natural: ASSIGN IA.FREEFORM-BEN := #CISA-BENE-STD-FREE
        ldaCisl601m.getMd_Prmry_Ben_Stnd_Txt().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Std_Or_Text_Ind());                                                          //Natural: ASSIGN MD.PRMRY-BEN-STND-TXT := #CISA-PRMRY-STD-OR-TEXT-IND
        ldaCisl601i.getIa_Prmry_Ben_Stnd_Txt().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Std_Or_Text_Ind());                                                          //Natural: ASSIGN IA.PRMRY-BEN-STND-TXT := #CISA-PRMRY-STD-OR-TEXT-IND
        ldaCisl601m.getMd_Prmry_Ben_Lump_Sum().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Lump_Sum_Ind());                                                             //Natural: ASSIGN MD.PRMRY-BEN-LUMP-SUM := #CISA-PRMRY-LUMP-SUM-IND
        ldaCisl601i.getIa_Prmry_Ben_Lump_Sum().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Lump_Sum_Ind());                                                             //Natural: ASSIGN IA.PRMRY-BEN-LUMP-SUM := #CISA-PRMRY-LUMP-SUM-IND
        ldaCisl601m.getMd_Prmry_Ben_Auto().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Auto_Cmnt_Val_Ind());                                                            //Natural: ASSIGN MD.PRMRY-BEN-AUTO := #CISA-PRMRY-AUTO-CMNT-VAL-IND
        ldaCisl601i.getIa_Prmry_Ben_Auto().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Auto_Cmnt_Val_Ind());                                                            //Natural: ASSIGN IA.PRMRY-BEN-AUTO := #CISA-PRMRY-AUTO-CMNT-VAL-IND
        ldaCisl601m.getMd_Prmry_Ben_Child_Prvsn().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Bene_Child_Prvsn());                                                      //Natural: ASSIGN MD.PRMRY-BEN-CHILD-PRVSN := #CISA-PRMRY-BENE-CHILD-PRVSN
        ldaCisl601i.getIa_Prmry_Ben_Child_Prvsn().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Bene_Child_Prvsn());                                                      //Natural: ASSIGN IA.PRMRY-BEN-CHILD-PRVSN := #CISA-PRMRY-BENE-CHILD-PRVSN
        ldaCisl601m.getMd_Prmry_Ben_Numb().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Cnt());                                                                          //Natural: ASSIGN MD.PRMRY-BEN-NUMB := #CISA-PRMRY-CNT
        ldaCisl601i.getIa_Prmry_Ben_Numb().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Cnt());                                                                          //Natural: ASSIGN IA.PRMRY-BEN-NUMB := #CISA-PRMRY-CNT
        ldaCisl601m.getMd_Cntgnt_Ben_Stnd_Txt().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Std_Or_Text_Ind());                                                        //Natural: ASSIGN MD.CNTGNT-BEN-STND-TXT := #CISA-CNTGNT-STD-OR-TEXT-IND
        ldaCisl601i.getIa_Cntgnt_Ben_Stnd_Txt().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Std_Or_Text_Ind());                                                        //Natural: ASSIGN IA.CNTGNT-BEN-STND-TXT := #CISA-CNTGNT-STD-OR-TEXT-IND
        ldaCisl601m.getMd_Cntgnt_Ben_Lump_Sum().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Lump_Sum_Ind());                                                           //Natural: ASSIGN MD.CNTGNT-BEN-LUMP-SUM := #CISA-CNTGNT-LUMP-SUM-IND
        ldaCisl601i.getIa_Cntgnt_Ben_Lump_Sum().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Lump_Sum_Ind());                                                           //Natural: ASSIGN IA.CNTGNT-BEN-LUMP-SUM := #CISA-CNTGNT-LUMP-SUM-IND
        ldaCisl601m.getMd_Cntgnt_Ben_Auto().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Auto_Cmnt_Val_Ind());                                                          //Natural: ASSIGN MD.CNTGNT-BEN-AUTO := #CISA-CNTGNT-AUTO-CMNT-VAL-IND
        ldaCisl601i.getIa_Cntgnt_Ben_Auto().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Auto_Cmnt_Val_Ind());                                                          //Natural: ASSIGN IA.CNTGNT-BEN-AUTO := #CISA-CNTGNT-AUTO-CMNT-VAL-IND
        ldaCisl601m.getMd_Cntgnt_Ben_Child_Prvsn().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Bene_Child_Prvsn());                                                    //Natural: ASSIGN MD.CNTGNT-BEN-CHILD-PRVSN := #CISA-CNTGNT-BENE-CHILD-PRVSN
        ldaCisl601i.getIa_Cntgnt_Ben_Child_Prvsn().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Bene_Child_Prvsn());                                                    //Natural: ASSIGN IA.CNTGNT-BEN-CHILD-PRVSN := #CISA-CNTGNT-BENE-CHILD-PRVSN
        ldaCisl601m.getMd_Cntgnt_Ben_Numb().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Cnt());                                                                        //Natural: ASSIGN MD.CNTGNT-BEN-NUMB := #CISA-CNTGNT-CNT
        ldaCisl601i.getIa_Cntgnt_Ben_Numb().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Cnt());                                                                        //Natural: ASSIGN IA.CNTGNT-BEN-NUMB := #CISA-CNTGNT-CNT
        FOR02:                                                                                                                                                            //Natural: FOR #SUB-NA = 5 TO 1 STEP -1
        for (pnd_Sub_Na.setValue(5); condition(pnd_Sub_Na.greaterOrEqual(1)); pnd_Sub_Na.nsubtract(1))
        {
            if (condition(ldaCislext.getEx_Address_Line().getValue(pnd_Sub_Na).notEquals(" ")))                                                                           //Natural: IF EX.ADDRESS-LINE ( #SUB-NA ) NE ' '
            {
                pnd_Work_Addr_Line.setValue(ldaCislext.getEx_Address_Line().getValue(pnd_Sub_Na));                                                                        //Natural: ASSIGN #WORK-ADDR-LINE := EX.ADDRESS-LINE ( #SUB-NA )
                                                                                                                                                                          //Natural: PERFORM FIND-ZIP-POSITION
                sub_Find_Zip_Position();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                pnd_Sub_Na.setValue(0);                                                                                                                                   //Natural: MOVE 0 TO #SUB-NA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaCisl601i.getIa_Address_Line().getValue("*").setValue(ldaCislext.getEx_Address_Line().getValue("*"));                                                           //Natural: ASSIGN IA.ADDRESS-LINE ( * ) := MD.ADDRESS-LINE ( * ) := EX.ADDRESS-LINE ( * )
        ldaCisl601m.getMd_Address_Line().getValue("*").setValue(ldaCislext.getEx_Address_Line().getValue("*"));
        if (condition(DbsUtil.maskMatches(ldaCislext.getEx_Zip_5(),"NNNNN") && ldaCislext.getEx_Zip_5().notEquals("00000")))                                              //Natural: IF EX.ZIP-5 = MASK ( NNNNN ) AND EX.ZIP-5 NE '00000'
        {
            pnd_Work_Zip_Check_Digit.setValue(0);                                                                                                                         //Natural: ASSIGN #WORK-ZIP-CHECK-DIGIT := 0
            pnd_Work_Zip_N.compute(new ComputeParameters(false, pnd_Work_Zip_N), ldaCislext.getEx_Zip_5().val());                                                         //Natural: ASSIGN #WORK-ZIP-N := VAL ( EX.ZIP-5 )
            FOR03:                                                                                                                                                        //Natural: FOR #I = 1 TO 5
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
            {
                pnd_Work_Zip_Check_Digit.nadd(pnd_Work_Zip_N_Pnd_Work_Zip_Digit_N.getValue(pnd_I));                                                                       //Natural: ADD #WORK-ZIP-DIGIT-N ( #I ) TO #WORK-ZIP-CHECK-DIGIT
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1.notEquals(getZero())))                                                                      //Natural: IF #WORK-ZIP-CHECK-DIGIT-1 NE 0
            {
                pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1.compute(new ComputeParameters(false, pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1),            //Natural: ASSIGN #WORK-ZIP-CHECK-DIGIT-1 := 10 - #WORK-ZIP-CHECK-DIGIT-1
                    DbsField.subtract(10,pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_1));
            }                                                                                                                                                             //Natural: END-IF
            ldaCisl601i.getIa_Post_Zip().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "/", ldaCislext.getEx_Zip_5(), ldaCislext.getEx_Zip_4(),                //Natural: COMPRESS '/' EX.ZIP-5 EX.ZIP-4 #WORK-ZIP-CHECK-DIGIT-A '/' INTO IA.POST-ZIP LEAVING NO SPACE
                pnd_Work_Zip_Check_Digit_Pnd_Work_Zip_Check_Digit_A, "/"));
            ldaCisl601m.getMd_Post_Zip().setValue(ldaCisl601i.getIa_Post_Zip());                                                                                          //Natural: ASSIGN MD.POST-ZIP := IA.POST-ZIP
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE-COMMON-DRIVER-DATA
    }
    private void sub_Move_Ia_Driver_Data() throws Exception                                                                                                               //Natural: MOVE-IA-DRIVER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaCisl601i.getIa_Rea_Numb().setValue(ldaCislext.getEx_Rea_Numb());                                                                                               //Natural: ASSIGN IA.REA-NUMB := EX.REA-NUMB
        ldaCisl601i.getIa_Last_Payt_Date().setValue(ldaCislext.getEx_Last_Payt_Date());                                                                                   //Natural: ASSIGN IA.LAST-PAYT-DATE := EX.LAST-PAYT-DATE
        ldaCisl601i.getIa_Guar_Period().setValue(ldaCislext.getEx_Guar_Period());                                                                                         //Natural: ASSIGN IA.GUAR-PERIOD := EX.GUAR-PERIOD
        ldaCisl601i.getIa_Guar_Prd_Beg_Date().setValue(ldaCislext.getEx_Guar_Prd_Beg_Date());                                                                             //Natural: ASSIGN IA.GUAR-PRD-BEG-DATE := EX.GUAR-PRD-BEG-DATE
        ldaCisl601i.getIa_Guar_Prd_End_Date().setValue(ldaCislext.getEx_Guar_Prd_End_Date());                                                                             //Natural: ASSIGN IA.GUAR-PRD-END-DATE := EX.GUAR-PRD-END-DATE
        ldaCisl601i.getIa_Sec_Annt_Contract_Name().setValue(ldaCislext.getEx_Sec_Annt_Contract_Name());                                                                   //Natural: ASSIGN IA.SEC-ANNT-CONTRACT-NAME := EX.SEC-ANNT-CONTRACT-NAME
        ldaCisl601i.getIa_Sec_Annt_Ssn().setValue("ON FILE");                                                                                                             //Natural: ASSIGN IA.SEC-ANNT-SSN := 'ON FILE'
        ldaCisl601i.getIa_Sec_Annt_Dob().setValue(ldaCislext.getEx_Sec_Annt_Dob());                                                                                       //Natural: ASSIGN IA.SEC-ANNT-DOB := EX.SEC-ANNT-DOB
        ldaCisl601i.getIa_Survivor_Reduction().setValue(ldaCislext.getEx_Survivor_Reduction());                                                                           //Natural: ASSIGN IA.SURVIVOR-REDUCTION := EX.SURVIVOR-REDUCTION
        pnd_Format_Amt.setValue(ldaCislext.getEx_Tiaa_Graded_Amt());                                                                                                      //Natural: ASSIGN #FORMAT-AMT := EX.TIAA-GRADED-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601i.getIa_Tiaa_Graded_Amt().setValue(pnd_Format_Amt);                                                                                                     //Natural: ASSIGN IA.TIAA-GRADED-AMT := #FORMAT-AMT
        pnd_Format_Amt.setValue(ldaCislext.getEx_Tiaa_Standard_Amt());                                                                                                    //Natural: ASSIGN #FORMAT-AMT := EX.TIAA-STANDARD-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601i.getIa_Tiaa_Standard_Amt().setValue(pnd_Format_Amt);                                                                                                   //Natural: ASSIGN IA.TIAA-STANDARD-AMT := #FORMAT-AMT
        pnd_Format_Amt.setValue(ldaCislext.getEx_Tiaa_Total_Amt());                                                                                                       //Natural: ASSIGN #FORMAT-AMT := EX.TIAA-TOTAL-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601i.getIa_Tiaa_Total_Amt().setValue(pnd_Format_Amt);                                                                                                      //Natural: ASSIGN IA.TIAA-TOTAL-AMT := #FORMAT-AMT
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            pnd_Format_Amt.setValue(ldaCislext.getEx_Guar_Payt().getValue(pnd_I));                                                                                        //Natural: ASSIGN #FORMAT-AMT := EX.GUAR-PAYT ( #I )
            pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                      //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
            ldaCisl601i.getIa_Guar_Payt().getValue(pnd_I).setValue(pnd_Format_Amt);                                                                                       //Natural: ASSIGN IA.GUAR-PAYT ( #I ) := #FORMAT-AMT
            ldaCisl601i.getIa_Guar_Intr_Rte().getValue(pnd_I).setValue(ldaCislext.getEx_Guar_Intr_Rte().getValue(pnd_I));                                                 //Natural: ASSIGN IA.GUAR-INTR-RTE ( #I ) := EX.GUAR-INTR-RTE ( #I )
            ldaCisl601i.getIa_Guar_Payt_Met().getValue(pnd_I).setValue(ldaCislext.getEx_Guar_Payt_Met().getValue(pnd_I));                                                 //Natural: ASSIGN IA.GUAR-PAYT-MET ( #I ) := EX.GUAR-PAYT-MET ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Format_Amt.setValue(ldaCislext.getEx_Da_Rea_Total_Amt());                                                                                                     //Natural: ASSIGN #FORMAT-AMT := EX.DA-REA-TOTAL-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601i.getIa_Da_Rea_Total_Amt().setValue(pnd_Format_Amt);                                                                                                    //Natural: ASSIGN IA.DA-REA-TOTAL-AMT := #FORMAT-AMT
        ldaCisl601i.getIa_Rea_Monthly_Units().setValue(ldaCislext.getEx_Rea_Monthly_Units(), MoveOption.LeftJustified);                                                   //Natural: MOVE LEFT JUSTIFIED EX.REA-MONTHLY-UNITS TO IA.REA-MONTHLY-UNITS
        ldaCisl601i.getIa_Rea_Surv_Units().setValue(ldaCislext.getEx_Rea_Surv_Units(), MoveOption.LeftJustified);                                                         //Natural: MOVE LEFT JUSTIFIED EX.REA-SURV-UNITS TO IA.REA-SURV-UNITS
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            ldaCisl601i.getIa_Rea_Ed_Numb().getValue(pnd_I).setValue(ldaCislext.getEx_Rea_Ed_Numb().getValue(pnd_I), MoveOption.LeftJustified);                           //Natural: MOVE LEFT JUSTIFIED EX.REA-ED-NUMB ( #I ) TO IA.REA-ED-NUMB ( #I )
            ldaCisl601i.getIa_Rea_Form_Numb().getValue(pnd_I).setValue(ldaCislext.getEx_Rea_Form_Numb().getValue(pnd_I));                                                 //Natural: ASSIGN IA.REA-FORM-NUMB ( #I ) := EX.REA-FORM-NUMB ( #I )
            ldaCisl601i.getIa_Rea_Prod_Cdes().getValue(pnd_I).setValue(ldaCislext.getEx_Rea_Prod_Cdes().getValue(pnd_I));                                                 //Natural: ASSIGN IA.REA-PROD-CDES ( #I ) := EX.REA-PROD-CDES ( #I )
            ldaCisl601i.getIa_Rea_Headers().getValue(pnd_I).setValue(ldaCislext.getEx_Rea_Headers().getValue(pnd_I));                                                     //Natural: ASSIGN IA.REA-HEADERS ( #I ) := EX.REA-HEADERS ( #I )
            ldaCisl601i.getIa_Income_Option().getValue(pnd_I).setValue(ldaCislext.getEx_Income_Option().getValue(pnd_I));                                                 //Natural: ASSIGN IA.INCOME-OPTION ( #I ) := EX.INCOME-OPTION ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaCisl601i.getIa_Rtb_Percent().setValue(ldaCislext.getEx_Rtb_Percent());                                                                                         //Natural: ASSIGN IA.RTB-PERCENT := EX.RTB-PERCENT
        pnd_Format_Amt.setValue(ldaCislext.getEx_Rtb_Amount(), MoveOption.LeftJustified);                                                                                 //Natural: MOVE LEFT JUSTIFIED EX.RTB-AMOUNT TO #FORMAT-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601i.getIa_Rtb_Amount().setValue(pnd_Format_Amt);                                                                                                          //Natural: ASSIGN IA.RTB-AMOUNT := #FORMAT-AMT
        ldaCisl601i.getIa_Check_Mail_Addr().getValue("*").setValue(ldaCislext.getEx_Check_Mail_Addr().getValue("*"));                                                     //Natural: ASSIGN IA.CHECK-MAIL-ADDR ( * ) := EX.CHECK-MAIL-ADDR ( * )
        ldaCisl601i.getIa_Bank_Accnt_No().setValue(ldaCislext.getEx_Bank_Accnt_No());                                                                                     //Natural: ASSIGN IA.BANK-ACCNT-NO := EX.BANK-ACCNT-NO
        ldaCisl601i.getIa_Bank_Transit_Code().setValue(ldaCislext.getEx_Bank_Transit_Code());                                                                             //Natural: ASSIGN IA.BANK-TRANSIT-CODE := EX.BANK-TRANSIT-CODE
        ldaCisl601i.getIa_Access_Amt().setValue(ldaCislext.getEx_Access_Amt());                                                                                           //Natural: ASSIGN IA.ACCESS-AMT := EX.ACCESS-AMT
        ldaCisl601i.getIa_Access_Account().setValue(ldaCislext.getEx_Access_Account());                                                                                   //Natural: ASSIGN IA.ACCESS-ACCOUNT := EX.ACCESS-ACCOUNT
        ldaCisl601i.getIa_Access_Mthly_Unit().setValue(ldaCislext.getEx_Access_Mthly_Unit());                                                                             //Natural: ASSIGN IA.ACCESS-MTHLY-UNIT := EX.ACCESS-MTHLY-UNIT
        ldaCisl601i.getIa_Access_Annual_Unit().setValue(ldaCislext.getEx_Access_Annual_Unit());                                                                           //Natural: ASSIGN IA.ACCESS-ANNUAL-UNIT := EX.ACCESS-ANNUAL-UNIT
        //*  WB - ACCRC
        //* TPAIPRO
        ldaCisl601i.getIa_Trnsf_Units().setValue(ldaCislext.getEx_Trnsf_Units(), MoveOption.LeftJustified);                                                               //Natural: MOVE LEFT JUSTIFIED EX.TRNSF-UNITS TO IA.TRNSF-UNITS
        ldaCisl601i.getIa_Trnsf_Payout_Cntr().setValue(ldaCislext.getEx_Trnsf_Payout_Cntr());                                                                             //Natural: ASSIGN IA.TRNSF-PAYOUT-CNTR := EX.TRNSF-PAYOUT-CNTR
        ldaCisl601i.getIa_Trnsf_Fund().setValue(ldaCislext.getEx_Trnsf_Fund());                                                                                           //Natural: ASSIGN IA.TRNSF-FUND := EX.TRNSF-FUND
        ldaCisl601i.getIa_Trnsf_Mode().setValue(ldaCislext.getEx_Trnsf_Mode());                                                                                           //Natural: ASSIGN IA.TRNSF-MODE := EX.TRNSF-MODE
        ldaCisl601i.getIa_Four_Fifty_Seven_Ind().setValue(ldaCislext.getEx_Four_Fifty_Seven_Ind());                                                                       //Natural: ASSIGN IA.FOUR-FIFTY-SEVEN-IND := EX.FOUR-FIFTY-SEVEN-IND
        ldaCisl601i.getIa_Trnsf_Cnt_Tiaa().setValue(ldaCislext.getEx_Trnsf_Cnt_Tiaa());                                                                                   //Natural: ASSIGN IA.TRNSF-CNT-TIAA := EX.TRNSF-CNT-TIAA
        ldaCisl601i.getIa_Trnsf_Cnt_Sepa().setValue(ldaCislext.getEx_Trnsf_Cnt_Sepa());                                                                                   //Natural: ASSIGN IA.TRNSF-CNT-SEPA := EX.TRNSF-CNT-SEPA
        ldaCisl601i.getIa_Ia_Shr_Class().setValue(ldaCislext.getEx_Ia_Shr_Class());                                                                                       //Natural: ASSIGN IA.IA-SHR-CLASS := EX.IA-SHR-CLASS
        ldaCisl601i.getIa_First_Tpa_Or_Ipro_Ind().setValue(ldaCislext.getEx_First_Tpa_Or_Ipro_Ind());                                                                     //Natural: ASSIGN IA.FIRST-TPA-OR-IPRO-IND := EX.FIRST-TPA-OR-IPRO-IND
        //*  MOVE-IA-DRIVER-DATA
    }
    private void sub_Move_Md_Driver_Data() throws Exception                                                                                                               //Natural: MOVE-MD-DRIVER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Format_Amt.setValue(ldaCislext.getEx_Tiaa_Initial_Payt_Amt());                                                                                                //Natural: ASSIGN #FORMAT-AMT := EX.TIAA-INITIAL-PAYT-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601m.getMd_Tiaa_Initial_Payt_Amt().setValue(pnd_Format_Amt);                                                                                               //Natural: ASSIGN MD.TIAA-INITIAL-PAYT-AMT := #FORMAT-AMT
        pnd_Format_Amt.setValue(ldaCislext.getEx_Cref_Initial_Payt_Amt());                                                                                                //Natural: ASSIGN #FORMAT-AMT := EX.CREF-INITIAL-PAYT-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601m.getMd_Cref_Initial_Payt_Amt().setValue(pnd_Format_Amt);                                                                                               //Natural: ASSIGN MD.CREF-INITIAL-PAYT-AMT := #FORMAT-AMT
        pnd_Format_Amt.setValue(ldaCislext.getEx_Tiaa_Excluded_Amt());                                                                                                    //Natural: ASSIGN #FORMAT-AMT := EX.TIAA-EXCLUDED-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601m.getMd_Tiaa_Excluded_Amt().setValue(pnd_Format_Amt);                                                                                                   //Natural: ASSIGN MD.TIAA-EXCLUDED-AMT := #FORMAT-AMT
        pnd_Format_Amt.setValue(ldaCislext.getEx_Cref_Excluded_Amt());                                                                                                    //Natural: ASSIGN #FORMAT-AMT := EX.CREF-EXCLUDED-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601m.getMd_Cref_Excluded_Amt().setValue(pnd_Format_Amt);                                                                                                   //Natural: ASSIGN MD.CREF-EXCLUDED-AMT := #FORMAT-AMT
        ldaCisl601m.getMd_Surrender_Chrg().setValue(ldaCislext.getEx_Surrender_Chrg());                                                                                   //Natural: ASSIGN MD.SURRENDER-CHRG := EX.SURRENDER-CHRG
        ldaCisl601m.getMd_Contigent_Chrg().setValue(ldaCislext.getEx_Contigent_Chrg());                                                                                   //Natural: ASSIGN MD.CONTIGENT-CHRG := EX.CONTIGENT-CHRG
        pnd_Format_Amt.setValue(ldaCislext.getEx_Annual_Req_Dist_Amt());                                                                                                  //Natural: ASSIGN #FORMAT-AMT := EX.ANNUAL-REQ-DIST-AMT
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601m.getMd_Annual_Req_Dist_Amt().setValue(pnd_Format_Amt);                                                                                                 //Natural: ASSIGN MD.ANNUAL-REQ-DIST-AMT := #FORMAT-AMT
        ldaCisl601m.getMd_Frst_Req_Pymt_Year().setValue(ldaCislext.getEx_Frst_Req_Pymt_Year());                                                                           //Natural: ASSIGN MD.FRST-REQ-PYMT-YEAR := EX.FRST-REQ-PYMT-YEAR
        pnd_Format_Amt.setValue(ldaCislext.getEx_Frst_Req_Pymt_Amt());                                                                                                    //Natural: ASSIGN #FORMAT-AMT := EX.FRST-REQ-PYMT-AMT
        //*  JRB1
        pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                          //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
        ldaCisl601m.getMd_Frst_Req_Pymt_Amt().setValue(pnd_Format_Amt);                                                                                                   //Natural: ASSIGN MD.FRST-REQ-PYMT-AMT := #FORMAT-AMT
        ldaCisl601m.getMd_Orgnl_Prtcpnt_Name().setValue(ldaCislext.getEx_Orgnl_Prtcpnt_Name());                                                                           //Natural: ASSIGN MD.ORGNL-PRTCPNT-NAME := EX.ORGNL-PRTCPNT-NAME
        ldaCisl601m.getMd_Orgnl_Prtcpnt_Ssn().setValue(ldaCislext.getEx_Orgnl_Prtcpnt_Ssn());                                                                             //Natural: ASSIGN MD.ORGNL-PRTCPNT-SSN := EX.ORGNL-PRTCPNT-SSN
        ldaCisl601m.getMd_Orgnl_Prtcpnt_Dob().setValue(ldaCislext.getEx_Orgnl_Prtcpnt_Dob());                                                                             //Natural: ASSIGN MD.ORGNL-PRTCPNT-DOB := EX.ORGNL-PRTCPNT-DOB
        ldaCisl601m.getMd_Orgnl_Prtcpnt_Dod().setValue(ldaCislext.getEx_Orgnl_Prtcpnt_Dod());                                                                             //Natural: ASSIGN MD.ORGNL-PRTCPNT-DOD := EX.ORGNL-PRTCPNT-DOD
        ldaCisl601m.getMd_Annt_Calc_Method().setValue(ldaCislext.getEx_Annt_Calc_Method());                                                                               //Natural: ASSIGN MD.ANNT-CALC-METHOD := EX.ANNT-CALC-METHOD
        ldaCisl601m.getMd_Annt_Calc_Met_2().setValue(ldaCislext.getEx_Annt_Calc_Met_2());                                                                                 //Natural: ASSIGN MD.ANNT-CALC-MET-2 := EX.ANNT-CALC-MET-2
        ldaCisl601m.getMd_Calc_Participant().setValue(ldaCislext.getEx_Calc_Participant());                                                                               //Natural: ASSIGN MD.CALC-PARTICIPANT := EX.CALC-PARTICIPANT
        ldaCisl601m.getMd_Invest_Num_Tot().setValue(ldaCislext.getEx_Invest_Num_Tot());                                                                                   //Natural: ASSIGN MD.INVEST-NUM-TOT := EX.INVEST-NUM-TOT
        //*  WB - ACCRC
        //*  WB - ACCRC
        if (condition(ldaCislext.getEx_Multi_Plan_Ind().equals("N")))                                                                                                     //Natural: IF EX.MULTI-PLAN-IND = 'N'
        {
            ldaCisl601m.getMd_Plan_Num().setValue(ldaCislext.getEx_Plan_Num());                                                                                           //Natural: ASSIGN MD.PLAN-NUM := MD.MULTI-PLAN-NO ( 1 ) := EX.PLAN-NUM
            ldaCisl601m.getMd_Multi_Plan_No().getValue(1).setValue(ldaCislext.getEx_Plan_Num());
            //*  WB - ACCRC
            //*  WB - ACCRC
            //*  WB - ACCRC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCisl601m.getMd_Multi_Plan_No().getValue("*").setValue(ldaCislext.getEx_Multi_Plan_No().getValue("*"));                                                     //Natural: ASSIGN MD.MULTI-PLAN-NO ( * ) := EX.MULTI-PLAN-NO ( * )
            ldaCisl601m.getMd_Multi_Sub_Plan().getValue("*").setValue(ldaCislext.getEx_Multi_Sub_Plan().getValue("*"));                                                   //Natural: ASSIGN MD.MULTI-SUB-PLAN ( * ) := EX.MULTI-SUB-PLAN ( * )
        }                                                                                                                                                                 //Natural: END-IF
        ldaCisl601m.getMd_Clcltn_Beneficiary().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Calc_Bene());                                                          //Natural: ASSIGN MD.CLCLTN-BENEFICIARY := #CISA-CLCLTN-BENE-CALC-BENE
        ldaCisl601m.getMd_Clcltn_Bene_Name().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Name());                                                                 //Natural: ASSIGN MD.CLCLTN-BENE-NAME := #CISA-CLCLTN-BENE-NAME
        ldaCisl601m.getMd_Clcltn_Bene_Method().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Calc_Method());                                                        //Natural: ASSIGN MD.CLCLTN-BENE-METHOD := #CISA-CLCLTN-BENE-CALC-METHOD
        ldaCisl601m.getMd_Clcltn_Bene_Method_2().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Calc_Desc());                                                        //Natural: ASSIGN MD.CLCLTN-BENE-METHOD-2 := #CISA-CLCLTN-BENE-CALC-DESC
        ldaCisl601m.getMd_Clcltn_Bene_Dob().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Clcltn_Bene_Dob_A());                                                                 //Natural: ASSIGN MD.CLCLTN-BENE-DOB := #CISA-CLCLTN-BENE-DOB-A
        if (condition(ldaCislext.getEx_Cntrct_Retr_Surv().equals("S")))                                                                                                   //Natural: IF EX.CNTRCT-RETR-SURV = 'S'
        {
            ldaCisl601m.getMd_Clcltn_Bene_Name().setValue("NONE ALLOWED UNDER");                                                                                          //Natural: MOVE 'NONE ALLOWED UNDER' TO MD.CLCLTN-BENE-NAME
            ldaCisl601m.getMd_Clcltn_Bene_Surv().setValue("THE FEDERAL TAX LAW.");                                                                                        //Natural: MOVE 'THE FEDERAL TAX LAW.' TO MD.CLCLTN-BENE-SURV
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE-MD-DRIVER-DATA
    }
    //*  RECORD TYPE 02
    private void sub_Write_Primary_Bene_Data() throws Exception                                                                                                           //Natural: WRITE-PRIMARY-BENE-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------------------------**
        ldaCisl602.getPb().reset();                                                                                                                                       //Natural: RESET PB
        if (condition(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Cnt().greater(getZero())))                                                                                     //Natural: IF #CISA-PRMRY-CNT > 0
        {
            FOR06:                                                                                                                                                        //Natural: FOR #I = 1 TO #CISA-PRMRY-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Cnt())); pnd_I.nadd(1))
            {
                ldaCisl602.getPb_Primary_Bene_Line().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Prmry_Print_Line().getValue(pnd_I));                                         //Natural: ASSIGN PRIMARY-BENE-LINE := #CISA-PRMRY-PRINT-LINE ( #I )
                if (condition(ldaCislext.getEx_Rqst_Id().equals("MDO")))                                                                                                  //Natural: IF EX.RQST-ID = 'MDO'
                {
                    ldaCisl602.getPb_Record_Id().setValue(ldaCisl601m.getMd_Record_Id());                                                                                 //Natural: ASSIGN PB.RECORD-ID := MD.RECORD-ID
                    ldaCisl602.getPb_Record_Type().setValue("02");                                                                                                        //Natural: ASSIGN PB.RECORD-TYPE := '02'
                    getWorkFiles().write(3, true, ldaCisl602.getPb());                                                                                                    //Natural: WRITE WORK FILE 3 VARIABLE PB
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCisl602.getPb_Record_Id().setValue(ldaCisl601i.getIa_Record_Id());                                                                                 //Natural: ASSIGN PB.RECORD-ID := IA.RECORD-ID
                    ldaCisl602.getPb_Record_Type().setValue("02");                                                                                                        //Natural: ASSIGN PB.RECORD-TYPE := '02'
                    getWorkFiles().write(2, true, ldaCisl602.getPb());                                                                                                    //Natural: WRITE WORK FILE 2 VARIABLE PB
                }                                                                                                                                                         //Natural: END-IF
                pnd_Pbene_Written.nadd(+1);                                                                                                                               //Natural: ADD +1 TO #PBENE-WRITTEN
                pnd_Tot_Written.nadd(+1);                                                                                                                                 //Natural: ADD +1 TO #TOT-WRITTEN
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-PRIMARY-BENE-DATA
    }
    //*  RECORD TYPE 03
    private void sub_Write_Cntgnt_Bene_Data() throws Exception                                                                                                            //Natural: WRITE-CNTGNT-BENE-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------------------------**
        ldaCisl603.getCb().reset();                                                                                                                                       //Natural: RESET CB
        if (condition(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Cnt().greater(getZero())))                                                                                    //Natural: IF #CISA-CNTGNT-CNT > 0
        {
            FOR07:                                                                                                                                                        //Natural: FOR #I = 1 TO #CISA-CNTGNT-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Cnt())); pnd_I.nadd(1))
            {
                ldaCisl603.getCb_Cntgnt_Bene_Line().setValue(pdaCisa620.getCisa620_Pnd_Cisa_Cntgnt_Print_Line().getValue(pnd_I));                                         //Natural: ASSIGN CNTGNT-BENE-LINE := #CISA-CNTGNT-PRINT-LINE ( #I )
                if (condition(ldaCislext.getEx_Rqst_Id().equals("MDO")))                                                                                                  //Natural: IF EX.RQST-ID = 'MDO'
                {
                    ldaCisl603.getCb_Record_Id().setValue(ldaCisl601m.getMd_Record_Id());                                                                                 //Natural: ASSIGN CB.RECORD-ID := MD.RECORD-ID
                    ldaCisl603.getCb_Record_Type().setValue("03");                                                                                                        //Natural: ASSIGN CB.RECORD-TYPE := '03'
                    getWorkFiles().write(3, true, ldaCisl603.getCb());                                                                                                    //Natural: WRITE WORK FILE 3 VARIABLE CB
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCisl603.getCb_Record_Id().setValue(ldaCisl601i.getIa_Record_Id());                                                                                 //Natural: ASSIGN CB.RECORD-ID := IA.RECORD-ID
                    ldaCisl603.getCb_Record_Type().setValue("03");                                                                                                        //Natural: ASSIGN CB.RECORD-TYPE := '03'
                    getWorkFiles().write(2, true, ldaCisl603.getCb());                                                                                                    //Natural: WRITE WORK FILE 2 VARIABLE CB
                }                                                                                                                                                         //Natural: END-IF
                pnd_Cbene_Written.nadd(+1);                                                                                                                               //Natural: ADD +1 TO #CBENE-WRITTEN
                pnd_Tot_Written.nadd(+1);                                                                                                                                 //Natural: ADD +1 TO #TOT-WRITTEN
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-CNTGNT-BENE-DATA
    }
    //*  RECORD TYPE 04
    private void sub_Write_Cref_Fund_Data() throws Exception                                                                                                              //Natural: WRITE-CREF-FUND-DATA
    {
        if (BLNatReinput.isReinput()) return;

        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            ldaCisl604.getCf().reset();                                                                                                                                   //Natural: RESET CF
            if (condition(ldaCislext.getEx_Cref_Fund().getValue(pnd_I).notEquals(" ")))                                                                                   //Natural: IF EX.CREF-FUND ( #I ) NE ' '
            {
                ldaCisl604.getCf_Cref_Fund().setValue(ldaCislext.getEx_Cref_Fund().getValue(pnd_I));                                                                      //Natural: ASSIGN CF.CREF-FUND := EX.CREF-FUND ( #I )
                ldaCisl604.getCf_Cref_Mnthly_Units().setValue(ldaCislext.getEx_Cref_Mnthly_Units().getValue(pnd_I), MoveOption.LeftJustified);                            //Natural: MOVE LEFT JUSTIFIED EX.CREF-MNTHLY-UNITS ( #I ) TO CF.CREF-MNTHLY-UNITS
                ldaCisl604.getCf_Cref_Annual_Units().setValue(ldaCislext.getEx_Cref_Annual_Units().getValue(pnd_I), MoveOption.LeftJustified);                            //Natural: MOVE LEFT JUSTIFIED EX.CREF-ANNUAL-UNITS ( #I ) TO CF.CREF-ANNUAL-UNITS
                if (condition(ldaCislext.getEx_Rqst_Id().equals("MDO")))                                                                                                  //Natural: IF EX.RQST-ID = 'MDO'
                {
                    ldaCisl604.getCf_Record_Id().setValue(ldaCisl601m.getMd_Record_Id());                                                                                 //Natural: ASSIGN CF.RECORD-ID := MD.RECORD-ID
                    ldaCisl604.getCf_Record_Type().setValue("04");                                                                                                        //Natural: ASSIGN CF.RECORD-TYPE := '04'
                    getWorkFiles().write(3, true, ldaCisl604.getCf());                                                                                                    //Natural: WRITE WORK FILE 3 VARIABLE CF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCisl604.getCf_Record_Id().setValue(ldaCisl601i.getIa_Record_Id());                                                                                 //Natural: ASSIGN CF.RECORD-ID := IA.RECORD-ID
                    ldaCisl604.getCf_Record_Type().setValue("04");                                                                                                        //Natural: ASSIGN CF.RECORD-TYPE := '04'
                    getWorkFiles().write(2, true, ldaCisl604.getCf());                                                                                                    //Natural: WRITE WORK FILE 2 VARIABLE CF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_Written.nadd(+1);                                                                                                                                    //Natural: ADD +1 TO #FUND-WRITTEN
            pnd_Tot_Written.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #TOT-WRITTEN
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-CREF-FUND-DATA
    }
    //*  RECORD TYPE 05
    private void sub_Write_Commut_Data() throws Exception                                                                                                                 //Natural: WRITE-COMMUT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        FOR09:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            ldaCisl605.getCm().reset();                                                                                                                                   //Natural: RESET CM
            ldaCisl605.getCm_Record_Id().setValue(ldaCisl601i.getIa_Record_Id());                                                                                         //Natural: ASSIGN CM.RECORD-ID := IA.RECORD-ID
            ldaCisl605.getCm_Record_Type().setValue("05");                                                                                                                //Natural: ASSIGN CM.RECORD-TYPE := '05'
            if (condition(ldaCislext.getEx_Comut_Grnted_Amt().getValue(pnd_I).notEquals(" ")))                                                                            //Natural: IF EX.COMUT-GRNTED-AMT ( #I ) NE ' '
            {
                pnd_Format_Amt.setValue(ldaCislext.getEx_Comut_Grnted_Amt().getValue(pnd_I));                                                                             //Natural: ASSIGN #FORMAT-AMT := EX.COMUT-GRNTED-AMT ( #I )
                pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                  //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
                ldaCisl605.getCm_Comut_Grnted_Amt().setValue(pnd_Format_Amt);                                                                                             //Natural: ASSIGN CM.COMUT-GRNTED-AMT := #FORMAT-AMT
                ldaCisl605.getCm_Comut_Intr_Rate().setValue(ldaCislext.getEx_Comut_Intr_Rate().getValue(pnd_I));                                                          //Natural: ASSIGN CM.COMUT-INTR-RATE := EX.COMUT-INTR-RATE ( #I )
                ldaCisl605.getCm_Comut_Pymt_Method().setValue(ldaCislext.getEx_Comut_Pymt_Method().getValue(pnd_I));                                                      //Natural: ASSIGN CM.COMUT-PYMT-METHOD := EX.COMUT-PYMT-METHOD ( #I )
                ldaCisl605.getCm_Comut_Mort_Basis().setValue(ldaCislext.getEx_Comut_Mort_Basis().getValue(pnd_I));                                                        //Natural: ASSIGN CM.COMUT-MORT-BASIS := EX.COMUT-MORT-BASIS ( #I )
                getWorkFiles().write(2, true, ldaCisl605.getCm());                                                                                                        //Natural: WRITE WORK FILE 2 VARIABLE CM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Comut_Written.nadd(+1);                                                                                                                                   //Natural: ADD +1 TO #COMUT-WRITTEN
            pnd_Tot_Written.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #TOT-WRITTEN
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-COMMUT-DATA
    }
    //*  RECORD TYPES 07, 08, 09
    private void sub_Write_Da_Data() throws Exception                                                                                                                     //Natural: WRITE-DA-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------------------------**
        pnd_Tiaa_Idx.reset();                                                                                                                                             //Natural: RESET #TIAA-IDX #REA-IDX #CREF-IDX
        pnd_Rea_Idx.reset();
        pnd_Cref_Idx.reset();
        FORTIAA:                                                                                                                                                          //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaCislext.getEx_Da_Tiaa_Numb().getValue(pnd_I).equals(" ")))                                                                                   //Natural: IF EX.DA-TIAA-NUMB ( #I ) EQ ' '
            {
                pnd_Tiaa_Idx.compute(new ComputeParameters(false, pnd_Tiaa_Idx), pnd_I.subtract(1));                                                                      //Natural: ASSIGN #TIAA-IDX := #I - 1
                if (true) break FORTIAA;                                                                                                                                  //Natural: ESCAPE BOTTOM ( FORTIAA. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FORREA:                                                                                                                                                           //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaCislext.getEx_Da_Rea_Numb().getValue(pnd_I).equals(" ")))                                                                                    //Natural: IF EX.DA-REA-NUMB ( #I ) EQ ' '
            {
                pnd_Rea_Idx.compute(new ComputeParameters(false, pnd_Rea_Idx), pnd_I.subtract(1));                                                                        //Natural: ASSIGN #REA-IDX := #I - 1
                if (true) break FORREA;                                                                                                                                   //Natural: ESCAPE BOTTOM ( FORREA. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FORCREF:                                                                                                                                                          //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaCislext.getEx_Da_Cref_Numb().getValue(pnd_I).equals(" ")))                                                                                   //Natural: IF EX.DA-CREF-NUMB ( #I ) EQ ' '
            {
                pnd_Cref_Idx.compute(new ComputeParameters(false, pnd_Cref_Idx), pnd_I.subtract(1));                                                                      //Natural: ASSIGN #CREF-IDX := #I - 1
                if (true) break FORCREF;                                                                                                                                  //Natural: ESCAPE BOTTOM ( FORCREF. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR10:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaCislext.getEx_Da_Tiaa_Numb().getValue(pnd_I).notEquals(" ")))                                                                                //Natural: IF EX.DA-TIAA-NUMB ( #I ) NE ' '
            {
                ldaCisl607.getTa().reset();                                                                                                                               //Natural: RESET TA
                if (condition(pnd_Tiaa_Idx.equals(pnd_I)))                                                                                                                //Natural: IF #TIAA-IDX = #I
                {
                    ldaCisl607.getTa_Last_Record().setValue("Y");                                                                                                         //Natural: ASSIGN TA.LAST-RECORD := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCisl607.getTa_Last_Record().setValue("N");                                                                                                         //Natural: ASSIGN TA.LAST-RECORD := 'N'
                }                                                                                                                                                         //Natural: END-IF
                ldaCisl607.getTa_Da_Tiaa_Numb().setValue(ldaCislext.getEx_Da_Tiaa_Numb().getValue(pnd_I));                                                                //Natural: ASSIGN TA.DA-TIAA-NUMB := EX.DA-TIAA-NUMB ( #I )
                if (condition(ldaCislext.getEx_Rqst_Id().notEquals("MDO")))                                                                                               //Natural: IF EX.RQST-ID NE 'MDO'
                {
                    pnd_Format_Amt.setValue(ldaCislext.getEx_Da_Tiaa_Amt().getValue(pnd_I));                                                                              //Natural: ASSIGN #FORMAT-AMT := EX.DA-TIAA-AMT ( #I )
                    pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                              //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
                    ldaCisl607.getTa_Da_Tiaa_Amt().setValue(pnd_Format_Amt);                                                                                              //Natural: ASSIGN TA.DA-TIAA-AMT := #FORMAT-AMT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCislext.getEx_Rqst_Id().equals("MDO")))                                                                                                  //Natural: IF EX.RQST-ID = 'MDO'
                {
                    ldaCisl607.getTa_Record_Id().setValue(ldaCisl601m.getMd_Record_Id());                                                                                 //Natural: ASSIGN TA.RECORD-ID := MD.RECORD-ID
                    ldaCisl607.getTa_Record_Type().setValue("07");                                                                                                        //Natural: ASSIGN TA.RECORD-TYPE := '07'
                    getWorkFiles().write(3, true, ldaCisl607.getTa());                                                                                                    //Natural: WRITE WORK FILE 3 VARIABLE TA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCisl607.getTa_Record_Id().setValue(ldaCisl601i.getIa_Record_Id());                                                                                 //Natural: ASSIGN TA.RECORD-ID := IA.RECORD-ID
                    ldaCisl607.getTa_Record_Type().setValue("07");                                                                                                        //Natural: ASSIGN TA.RECORD-TYPE := '07'
                    getWorkFiles().write(2, true, ldaCisl607.getTa());                                                                                                    //Natural: WRITE WORK FILE 2 VARIABLE TA
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Written.nadd(+1);                                                                                                                                    //Natural: ADD +1 TO #TIAA-WRITTEN
            pnd_Tot_Written.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #TOT-WRITTEN
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR11:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaCislext.getEx_Da_Rea_Numb().getValue(pnd_I).notEquals(" ") && ldaCislext.getEx_Rqst_Id().notEquals("MDO")))                                  //Natural: IF EX.DA-REA-NUMB ( #I ) NE ' ' AND EX.RQST-ID NE 'MDO'
            {
                ldaCisl608.getRa().reset();                                                                                                                               //Natural: RESET RA
                if (condition(pnd_Rea_Idx.equals(pnd_I)))                                                                                                                 //Natural: IF #REA-IDX = #I
                {
                    ldaCisl608.getRa_Last_Record().setValue("Y");                                                                                                         //Natural: ASSIGN RA.LAST-RECORD := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCisl608.getRa_Last_Record().setValue("N");                                                                                                         //Natural: ASSIGN RA.LAST-RECORD := 'N'
                }                                                                                                                                                         //Natural: END-IF
                ldaCisl608.getRa_Record_Id().setValue(ldaCisl601i.getIa_Record_Id());                                                                                     //Natural: ASSIGN RA.RECORD-ID := IA.RECORD-ID
                ldaCisl608.getRa_Record_Type().setValue("08");                                                                                                            //Natural: ASSIGN RA.RECORD-TYPE := '08'
                ldaCisl608.getRa_Da_Rea_Numb().setValue(ldaCislext.getEx_Da_Rea_Numb().getValue(pnd_I));                                                                  //Natural: ASSIGN RA.DA-REA-NUMB := EX.DA-REA-NUMB ( #I )
                pnd_Format_Amt.setValue(ldaCislext.getEx_Da_Rea_Amt().getValue(pnd_I));                                                                                   //Natural: ASSIGN #FORMAT-AMT := EX.DA-REA-AMT ( #I )
                pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                                  //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
                ldaCisl608.getRa_Da_Rea_Amt().setValue(pnd_Format_Amt);                                                                                                   //Natural: ASSIGN RA.DA-REA-AMT := #FORMAT-AMT
                getWorkFiles().write(2, true, ldaCisl608.getRa());                                                                                                        //Natural: WRITE WORK FILE 2 VARIABLE RA
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rea_Written.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #REA-WRITTEN
            pnd_Tot_Written.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #TOT-WRITTEN
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR12:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaCislext.getEx_Da_Cref_Numb().getValue(pnd_I).notEquals(" ")))                                                                                //Natural: IF EX.DA-CREF-NUMB ( #I ) NE ' '
            {
                ldaCisl609.getCa().reset();                                                                                                                               //Natural: RESET CA
                if (condition(pnd_Cref_Idx.equals(pnd_I)))                                                                                                                //Natural: IF #CREF-IDX = #I
                {
                    ldaCisl609.getCa_Last_Record().setValue("Y");                                                                                                         //Natural: ASSIGN CA.LAST-RECORD := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCisl609.getCa_Last_Record().setValue("N");                                                                                                         //Natural: ASSIGN CA.LAST-RECORD := 'N'
                }                                                                                                                                                         //Natural: END-IF
                ldaCisl609.getCa_Da_Cref_Numb().setValue(ldaCislext.getEx_Da_Cref_Numb().getValue(pnd_I));                                                                //Natural: ASSIGN CA.DA-CREF-NUMB := EX.DA-CREF-NUMB ( #I )
                if (condition(ldaCislext.getEx_Rqst_Id().notEquals("MDO")))                                                                                               //Natural: IF EX.RQST-ID NE 'MDO'
                {
                    pnd_Format_Amt.setValue(ldaCislext.getEx_Da_Cref_Amt().getValue(pnd_I));                                                                              //Natural: ASSIGN #FORMAT-AMT := EX.DA-CREF-AMT ( #I )
                    pnd_Format_Amt_Pnd_Amount.setValue(pnd_Format_Amt_Pnd_Amount, MoveOption.LeftJustified);                                                              //Natural: MOVE LEFT JUSTIFIED #AMOUNT TO #AMOUNT
                    ldaCisl609.getCa_Da_Cref_Amt().setValue(pnd_Format_Amt);                                                                                              //Natural: ASSIGN CA.DA-CREF-AMT := #FORMAT-AMT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaCislext.getEx_Rqst_Id().equals("MDO")))                                                                                                  //Natural: IF EX.RQST-ID = 'MDO'
                {
                    ldaCisl609.getCa_Record_Id().setValue(ldaCisl601m.getMd_Record_Id());                                                                                 //Natural: ASSIGN CA.RECORD-ID := MD.RECORD-ID
                    ldaCisl609.getCa_Record_Type().setValue("09");                                                                                                        //Natural: ASSIGN CA.RECORD-TYPE := '09'
                    getWorkFiles().write(3, true, ldaCisl609.getCa());                                                                                                    //Natural: WRITE WORK FILE 3 VARIABLE CA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCisl609.getCa_Record_Id().setValue(ldaCisl601i.getIa_Record_Id());                                                                                 //Natural: ASSIGN CA.RECORD-ID := IA.RECORD-ID
                    ldaCisl609.getCa_Record_Type().setValue("09");                                                                                                        //Natural: ASSIGN CA.RECORD-TYPE := '09'
                    getWorkFiles().write(2, true, ldaCisl609.getCa());                                                                                                    //Natural: WRITE WORK FILE 2 VARIABLE CA
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cref_Written.nadd(+1);                                                                                                                                    //Natural: ADD +1 TO #CREF-WRITTEN
            pnd_Tot_Written.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #TOT-WRITTEN
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-DA-DATA
    }
    //*  RECORD TYPES 10, 11
    private void sub_Write_Trnsf_Data() throws Exception                                                                                                                  //Natural: WRITE-TRNSF-DATA
    {
        if (BLNatReinput.isReinput()) return;

        FOR13:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            ldaCisl610.getTt().reset();                                                                                                                                   //Natural: RESET TT
            ldaCisl610.getTt_Record_Id().setValue(ldaCisl601i.getIa_Record_Id());                                                                                         //Natural: ASSIGN TT.RECORD-ID := IA.RECORD-ID
            ldaCisl610.getTt_Record_Type().setValue("10");                                                                                                                //Natural: ASSIGN TT.RECORD-TYPE := '10'
            if (condition(ldaCislext.getEx_Trnsf_Rece_Company().getValue(pnd_I).equals("TIAA")))                                                                          //Natural: IF EX.TRNSF-RECE-COMPANY ( #I ) = 'TIAA'
            {
                ldaCisl610.getTt_Tiaa_Cref_Units().setValue(ldaCislext.getEx_Trnsf_Cref_Units().getValue(pnd_I));                                                         //Natural: ASSIGN TT.TIAA-CREF-UNITS := EX.TRNSF-CREF-UNITS ( #I )
                ldaCisl610.getTt_Tiaa_Ticker_Name().setValue(ldaCislext.getEx_Trnsf_Ticker_Name().getValue(pnd_I));                                                       //Natural: ASSIGN TT.TIAA-TICKER-NAME := EX.TRNSF-TICKER-NAME ( #I )
                ldaCisl610.getTt_Tiaa_Reval_Type().setValue(ldaCislext.getEx_Reval_Type_Name().getValue(pnd_I));                                                          //Natural: ASSIGN TT.TIAA-REVAL-TYPE := EX.REVAL-TYPE-NAME ( #I )
                getWorkFiles().write(2, true, ldaCisl610.getTt());                                                                                                        //Natural: WRITE WORK FILE 2 VARIABLE TT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ttrn_Written.nadd(+1);                                                                                                                                    //Natural: ADD +1 TO #TTRN-WRITTEN
            pnd_Tot_Written.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #TOT-WRITTEN
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR14:                                                                                                                                                            //Natural: FOR #I = 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            ldaCisl611.getSt().reset();                                                                                                                                   //Natural: RESET ST
            ldaCisl611.getSt_Record_Id().setValue(ldaCisl601i.getIa_Record_Id());                                                                                         //Natural: ASSIGN ST.RECORD-ID := IA.RECORD-ID
            ldaCisl611.getSt_Record_Type().setValue("11");                                                                                                                //Natural: ASSIGN ST.RECORD-TYPE := '11'
            if (condition(ldaCislext.getEx_Trnsf_Rece_Company().getValue(pnd_I).equals("SEPA")))                                                                          //Natural: IF EX.TRNSF-RECE-COMPANY ( #I ) = 'SEPA'
            {
                ldaCisl611.getSt_Sepa_Cref_Units().setValue(ldaCislext.getEx_Trnsf_Cref_Units().getValue(pnd_I));                                                         //Natural: ASSIGN ST.SEPA-CREF-UNITS := EX.TRNSF-CREF-UNITS ( #I )
                ldaCisl611.getSt_Sepa_Ticker_Name().setValue(ldaCislext.getEx_Trnsf_Ticker_Name().getValue(pnd_I));                                                       //Natural: ASSIGN ST.SEPA-TICKER-NAME := EX.TRNSF-TICKER-NAME ( #I )
                ldaCisl611.getSt_Sepa_Reval_Type().setValue(ldaCislext.getEx_Reval_Type_Name().getValue(pnd_I));                                                          //Natural: ASSIGN ST.SEPA-REVAL-TYPE := EX.REVAL-TYPE-NAME ( #I )
                getWorkFiles().write(2, true, ldaCisl611.getSt());                                                                                                        //Natural: WRITE WORK FILE 2 VARIABLE ST
            }                                                                                                                                                             //Natural: END-IF
            pnd_Strn_Written.nadd(+1);                                                                                                                                    //Natural: ADD +1 TO #STRN-WRITTEN
            pnd_Tot_Written.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #TOT-WRITTEN
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-TRNSF-DATA
    }
    //*  RECORD TYPES 12    /* JRB1
    private void sub_Write_From_Invest_Data() throws Exception                                                                                                            //Natural: WRITE-FROM-INVEST-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------------------------**
        ldaCisl612.getFr().reset();                                                                                                                                       //Natural: RESET FR
        ldaCisl612.getFr_Record_Id().setValue(ldaCisl601m.getMd_Record_Id());                                                                                             //Natural: ASSIGN FR.RECORD-ID := MD.RECORD-ID
        ldaCisl612.getFr_Record_Type().setValue("12");                                                                                                                    //Natural: ASSIGN FR.RECORD-TYPE := '12'
        FOR15:                                                                                                                                                            //Natural: FOR #X = 1 TO EX.INVEST-NUM-TOT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(ldaCislext.getEx_Invest_Num_Tot())); pnd_X.nadd(1))
        {
            ldaCisl612.getFr_From_Invest().reset();                                                                                                                       //Natural: RESET FROM-INVEST
            ldaCisl612.getFr_Invest_Ind().setValue("FP");                                                                                                                 //Natural: ASSIGN FR.INVEST-IND := 'FP'
            ldaCisl612.getFr_Plan_Name().setValue(ldaCislext.getEx_Plan_Name().getValue(pnd_X));                                                                          //Natural: ASSIGN FR.PLAN-NAME := EX.PLAN-NAME ( #X )
            ldaCisl612.getFr_Invest_Tiaa_Num().setValue(ldaCislext.getEx_Invest_Tiaa_Num_1().getValue(pnd_X));                                                            //Natural: ASSIGN FR.INVEST-TIAA-NUM := EX.INVEST-TIAA-NUM-1 ( #X )
            ldaCisl612.getFr_Invest_Cref_Num().setValue(ldaCislext.getEx_Invest_Cref_Num_1().getValue(pnd_X));                                                            //Natural: ASSIGN FR.INVEST-CREF-NUM := EX.INVEST-CREF-NUM-1 ( #X )
            ldaCisl612.getFr_Invest_Orig_Amt().setValue(ldaCislext.getEx_Invest_Orig_Amt().getValue(pnd_X));                                                              //Natural: ASSIGN FR.INVEST-ORIG-AMT := EX.INVEST-ORIG-AMT ( #X )
            ldaCisl612.getFr_Invest_Process_Dt().setValue(ldaCislext.getEx_Invest_Process_Dt());                                                                          //Natural: ASSIGN FR.INVEST-PROCESS-DT := EX.INVEST-PROCESS-DT
            //*  JRB3
            if (condition(ldaCisl612.getFr_Invest_Process_Dt().equals("0")))                                                                                              //Natural: IF FR.INVEST-PROCESS-DT = '0'
            {
                //*  JRB3
                ldaCisl612.getFr_Invest_Process_Dt().setValue("00000000");                                                                                                //Natural: MOVE '00000000' TO FR.INVEST-PROCESS-DT
                //*  JRB3
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(3, true, ldaCisl612.getFr());                                                                                                            //Natural: WRITE WORK FILE 3 VARIABLE FR
            pnd_Finv_Written.nadd(+1);                                                                                                                                    //Natural: ADD +1 TO #FINV-WRITTEN
            pnd_Tot_Written.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #TOT-WRITTEN
            FOR16:                                                                                                                                                        //Natural: FOR #Y = 1 TO 25
            for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(25)); pnd_Y.nadd(1))
            {
                if (condition(ldaCislext.getEx_Invest_Fund_Name_From().getValue(pnd_X,pnd_Y).equals(" ")))                                                                //Natural: IF EX.INVEST-FUND-NAME-FROM ( #X,#Y ) = ' '
                {
                    //*  (7650)
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                ldaCisl612.getFr_From_Invest().reset();                                                                                                                   //Natural: RESET FROM-INVEST
                ldaCisl612.getFr_Invest_Ind().setValue("FI");                                                                                                             //Natural: ASSIGN FR.INVEST-IND := 'FI'
                ldaCisl612.getFr_Invest_Fund_Name_From().setValue(ldaCislext.getEx_Invest_Fund_Name_From().getValue(pnd_X,pnd_Y));                                        //Natural: ASSIGN FR.INVEST-FUND-NAME-FROM := EX.INVEST-FUND-NAME-FROM ( #X,#Y )
                ldaCisl612.getFr_Invest_Fund_Amt().setValue(ldaCislext.getEx_Invest_Fund_Amt().getValue(pnd_X,pnd_Y));                                                    //Natural: ASSIGN FR.INVEST-FUND-AMT := EX.INVEST-FUND-AMT ( #X,#Y )
                ldaCisl612.getFr_Invest_Unit_Price().setValue(ldaCislext.getEx_Invest_Unit_Price().getValue(pnd_X,pnd_Y));                                                //Natural: ASSIGN FR.INVEST-UNIT-PRICE := EX.INVEST-UNIT-PRICE ( #X,#Y )
                ldaCisl612.getFr_Invest_Units().setValue(ldaCislext.getEx_Invest_Units().getValue(pnd_X,pnd_Y));                                                          //Natural: ASSIGN FR.INVEST-UNITS := EX.INVEST-UNITS ( #X,#Y )
                getWorkFiles().write(3, true, ldaCisl612.getFr());                                                                                                        //Natural: WRITE WORK FILE 3 VARIABLE FR
                pnd_Finv_Written.nadd(+1);                                                                                                                                //Natural: ADD +1 TO #FINV-WRITTEN
                pnd_Tot_Written.nadd(+1);                                                                                                                                 //Natural: ADD +1 TO #TOT-WRITTEN
                //*  (7650)
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  (7500)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-FROM-INVEST-DATA
    }
    //*  RECORD TYPES 13      /* JRB1
    private void sub_Write_To_Invest_Data() throws Exception                                                                                                              //Natural: WRITE-TO-INVEST-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------------------------**
        ldaCisl613.getTo_View().reset();                                                                                                                                  //Natural: RESET TO-VIEW
        ldaCisl613.getTo_View_Record_Id().setValue(ldaCisl601m.getMd_Record_Id());                                                                                        //Natural: ASSIGN TO-VIEW.RECORD-ID := MD.RECORD-ID
        ldaCisl613.getTo_View_Record_Type().setValue("13");                                                                                                               //Natural: ASSIGN TO-VIEW.RECORD-TYPE := '13'
        ldaCisl613.getTo_View_Invest_Ind().setValue("TP");                                                                                                                //Natural: ASSIGN TO-VIEW.INVEST-IND := 'TP'
        ldaCisl613.getTo_View_Invest_Tiaa_Num_To().setValue(ldaCislext.getEx_Invest_Tiaa_Num_To());                                                                       //Natural: ASSIGN TO-VIEW.INVEST-TIAA-NUM-TO := EX.INVEST-TIAA-NUM-TO
        ldaCisl613.getTo_View_Invest_Cref_Num_To().setValue(ldaCislext.getEx_Invest_Cref_Num_To());                                                                       //Natural: ASSIGN TO-VIEW.INVEST-CREF-NUM-TO := EX.INVEST-CREF-NUM-TO
        ldaCisl613.getTo_View_Invest_Amt_Total().setValue(ldaCislext.getEx_Invest_Amt_Total());                                                                           //Natural: ASSIGN TO-VIEW.INVEST-AMT-TOTAL := EX.INVEST-AMT-TOTAL
        ldaCisl613.getTo_View_Invest_Process_Dt().setValue(ldaCislext.getEx_Invest_Process_Dt());                                                                         //Natural: ASSIGN TO-VIEW.INVEST-PROCESS-DT := EX.INVEST-PROCESS-DT
        //*  JRB3
        if (condition(ldaCisl613.getTo_View_Invest_Process_Dt().equals("0")))                                                                                             //Natural: IF TO-VIEW.INVEST-PROCESS-DT = '0'
        {
            //*  JRB3
            ldaCisl613.getTo_View_Invest_Process_Dt().setValue("00000000");                                                                                               //Natural: MOVE '00000000' TO TO-VIEW.INVEST-PROCESS-DT
            //*  JRB3
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(3, true, ldaCisl613.getTo_View());                                                                                                           //Natural: WRITE WORK FILE 3 VARIABLE TO-VIEW
        pnd_Tinv_Written.nadd(+1);                                                                                                                                        //Natural: ADD +1 TO #TINV-WRITTEN
        pnd_Tot_Written.nadd(+1);                                                                                                                                         //Natural: ADD +1 TO #TOT-WRITTEN
        FOR17:                                                                                                                                                            //Natural: FOR #Z = 1 TO 25
        for (pnd_Z.setValue(1); condition(pnd_Z.lessOrEqual(25)); pnd_Z.nadd(1))
        {
            if (condition(ldaCislext.getEx_Invest_Tbl_3().getValue(pnd_Z).equals(" ")))                                                                                   //Natural: IF EX.INVEST-TBL-3 ( #Z ) = ' '
            {
                //*  (8080)
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaCisl613.getTo_View_To_Invest().reset();                                                                                                                    //Natural: RESET TO-INVEST
            ldaCisl613.getTo_View_Invest_Ind().setValue("TI");                                                                                                            //Natural: ASSIGN TO-VIEW.INVEST-IND := 'TI'
            ldaCisl613.getTo_View_Invest_Fund_Name_To().setValue(ldaCislext.getEx_Invest_Fund_Name_To().getValue(pnd_Z));                                                 //Natural: ASSIGN TO-VIEW.INVEST-FUND-NAME-TO := EX.INVEST-FUND-NAME-TO ( #Z )
            ldaCisl613.getTo_View_Invest_Fund_Amt_To().setValue(ldaCislext.getEx_Invest_Fund_Amt_To().getValue(pnd_Z));                                                   //Natural: ASSIGN TO-VIEW.INVEST-FUND-AMT-TO := EX.INVEST-FUND-AMT-TO ( #Z )
            ldaCisl613.getTo_View_Invest_Unit_Price_To().setValue(ldaCislext.getEx_Invest_Unit_Price_To().getValue(pnd_Z));                                               //Natural: ASSIGN TO-VIEW.INVEST-UNIT-PRICE-TO := EX.INVEST-UNIT-PRICE-TO ( #Z )
            ldaCisl613.getTo_View_Invest_Units_To().setValue(ldaCislext.getEx_Invest_Units_To().getValue(pnd_Z));                                                         //Natural: ASSIGN TO-VIEW.INVEST-UNITS-TO := EX.INVEST-UNITS-TO ( #Z )
            getWorkFiles().write(3, true, ldaCisl613.getTo_View());                                                                                                       //Natural: WRITE WORK FILE 3 VARIABLE TO-VIEW
            pnd_Tinv_Written.nadd(+1);                                                                                                                                    //Natural: ADD +1 TO #TINV-WRITTEN
            pnd_Tot_Written.nadd(+1);                                                                                                                                     //Natural: ADD +1 TO #TOT-WRITTEN
            //*  (8080)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  WRITE-TO-INVEST-DATA
    }
    private void sub_Lookup_Profile() throws Exception                                                                                                                    //Natural: LOOKUP-PROFILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------------------------**
        pnd_Profile_Key.reset();                                                                                                                                          //Natural: RESET #PROFILE-KEY #FOUND
        pnd_Found.reset();
        pnd_Profile_Key_Pnd_Pj_Entry_Actve_Ind.setValue("Y");                                                                                                             //Natural: ASSIGN #PJ-ENTRY-ACTVE-IND := 'Y'
        pnd_Profile_Key_Pnd_Pj_Entry_Table_Id_Nbr.setValue(100);                                                                                                          //Natural: ASSIGN #PJ-ENTRY-TABLE-ID-NBR := 100
        pnd_Profile_Key_Pnd_Pj_Entry_Table_Sub_Id.setValue("PJ");                                                                                                         //Natural: ASSIGN #PJ-ENTRY-TABLE-SUB-ID := 'PJ'
        if (condition(pnd_Reprint.getBoolean()))                                                                                                                          //Natural: IF #REPRINT
        {
            pnd_Profile_Key_Pnd_Pj_Job_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaCislext.getEx_Print_Jobname(), "R"));                             //Natural: COMPRESS PRINT-JOBNAME 'R' INTO #PJ-JOB-NAME LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Profile_Key_Pnd_Pj_Job_Name.setValue(ldaCislext.getEx_Print_Jobname());                                                                                   //Natural: ASSIGN #PJ-JOB-NAME := PRINT-JOBNAME
        }                                                                                                                                                                 //Natural: END-IF
        vw_table_Entry.startDatabaseRead                                                                                                                                  //Natural: READ TABLE-ENTRY BY ACTVE-IND-TABLE-ID-ENTRY-CDE FROM #PROFILE-KEY
        (
        "READ02",
        new Wc[] { new Wc("ACTVE_IND_TABLE_ID_ENTRY_CDE", ">=", pnd_Profile_Key, WcType.BY) },
        new Oc[] { new Oc("ACTVE_IND_TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READ02:
        while (condition(vw_table_Entry.readNextRow("READ02")))
        {
            if (condition(table_Entry_Entry_Actve_Ind.notEquals(pnd_Profile_Key_Pnd_Pj_Entry_Actve_Ind) || table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Profile_Key_Pnd_Pj_Entry_Table_Id_Nbr)  //Natural: IF ENTRY-ACTVE-IND NE #PJ-ENTRY-ACTVE-IND OR ENTRY-TABLE-ID-NBR NE #PJ-ENTRY-TABLE-ID-NBR OR ENTRY-TABLE-SUB-ID NE #PJ-ENTRY-TABLE-SUB-ID OR ENTRY-JOB-NAME NE #PJ-JOB-NAME
                || table_Entry_Entry_Table_Sub_Id.notEquals(pnd_Profile_Key_Pnd_Pj_Entry_Table_Sub_Id) || table_Entry_Entry_Job_Name.notEquals(pnd_Profile_Key_Pnd_Pj_Job_Name)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Found.setValue(true);                                                                                                                                     //Natural: ASSIGN #FOUND := TRUE
            if (condition(ldaCislext.getEx_Rqst_Id().equals("MDO")))                                                                                                      //Natural: IF EX.RQST-ID = 'MDO'
            {
                ldaCisl601m.getMd_Output_Profile().setValue(table_Entry_Profile);                                                                                         //Natural: ASSIGN MD.OUTPUT-PROFILE := PROFILE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCisl601i.getIa_Output_Profile().setValue(table_Entry_Profile);                                                                                         //Natural: ASSIGN IA.OUTPUT-PROFILE := PROFILE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(! (pnd_Found.getBoolean())))                                                                                                                        //Natural: IF NOT #FOUND
        {
            getReports().write(0, ReportOption.NOHDR,"JOB NAME: ",pnd_Profile_Key_Pnd_Pj_Job_Name,"NOT FOUND ON TABLE 100 PJ");                                           //Natural: WRITE NOHDR 'JOB NAME: ' #PJ-JOB-NAME 'NOT FOUND ON TABLE 100 PJ'
            if (Global.isEscape()) return;
            DbsUtil.terminate(8);  if (true) return;                                                                                                                      //Natural: TERMINATE 8
        }                                                                                                                                                                 //Natural: END-IF
        //*  LOOKUP-PROFILE
    }
    private void sub_Get_Bene_Data() throws Exception                                                                                                                     //Natural: GET-BENE-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------------------------**
        pdaCisa620.getCisa620().reset();                                                                                                                                  //Natural: RESET CISA620
        pdaCisa620.getCisa620_Pnd_Cisa_Bene_Tiaa_Nbr().setValue(ldaCislext.getEx_Tiaa_Cntrct_Num());                                                                      //Natural: ASSIGN #CISA-BENE-TIAA-NBR := TIAA-CNTRCT-NUM
        pdaCisa620.getCisa620_Pnd_Cisa_Bene_Cref_Nbr().setValue(ldaCislext.getEx_Cref_Cert_Num());                                                                        //Natural: ASSIGN #CISA-BENE-CREF-NBR := CREF-CERT-NUM
        pdaCisa620.getCisa620_Pnd_Cisa_Bene_Rqst_Id().setValue(ldaCislext.getEx_Rqst_Id());                                                                               //Natural: ASSIGN #CISA-BENE-RQST-ID := EX.RQST-ID
        if (condition(ldaCislext.getEx_Pin_Numb().notEquals(" ")))                                                                                                        //Natural: IF EX.PIN-NUMB NE ' '
        {
            //*  PINE>>
            //*  MOVE EDITED EX.PIN-NUMB TO #CISA-BENE-UNIQUE-ID-NBR (EM=9999999)
            pdaCisa620.getCisa620_Pnd_Cisa_Bene_Unique_Id_Nbr().setValueEdited(new ReportEditMask("999999999999"),ldaCislext.getEx_Pin_Numb());                           //Natural: MOVE EDITED EX.PIN-NUMB TO #CISA-BENE-UNIQUE-ID-NBR ( EM = 999999999999 )
            //* PINE<<
        }                                                                                                                                                                 //Natural: END-IF
        pdaCisa620.getCisa620_Pnd_Cisa_Bene_Rqst_Id_Key().setValue(ldaCislext.getEx_Rqst_Id_Key());                                                                       //Natural: ASSIGN #CISA-BENE-RQST-ID-KEY := RQST-ID-KEY
        pdaCisa620.getCisa620_Pnd_Cisa_Resid_Issue_State().setValue(ldaCislext.getEx_Res_Issue_State());                                                                  //Natural: ASSIGN #CISA-RESID-ISSUE-STATE := EX.RES-ISSUE-STATE
        DbsUtil.callnat(Cisn620.class , getCurrentProcessState(), pdaCisa620.getCisa620());                                                                               //Natural: CALLNAT 'CISN620' CISA620
        if (condition(Global.isEscape())) return;
        if (condition(pdaCisa620.getCisa620_Pnd_Cisa_Return_Code().greater(getZero()) && pdaCisa620.getCisa620_Pnd_Cisa_Return_Code().less(99)))                          //Natural: IF #CISA-RETURN-CODE > 0 AND #CISA-RETURN-CODE < 99
        {
            getReports().write(0, "ERROR FROM BENE - CISN620",pdaCisa620.getCisa620_Pnd_Cisa_Return_Code());                                                              //Natural: WRITE 'ERROR FROM BENE - CISN620' #CISA-RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, pdaCisa620.getCisa620_Pnd_Cisa_Msg());                                                                                                  //Natural: WRITE #CISA-MSG
            if (Global.isEscape()) return;
            DbsUtil.terminate(pdaCisa620.getCisa620_Pnd_Cisa_Return_Code());  if (true) return;                                                                           //Natural: TERMINATE #CISA-RETURN-CODE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa620.getCisa620_Pnd_Cisa_Return_Code().equals(99)))                                                                                           //Natural: IF #CISA-RETURN-CODE = 99
        {
            getReports().write(0, pdaCisa620.getCisa620_Pnd_Cisa_Msg());                                                                                                  //Natural: WRITE #CISA-MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-BENE-DATA
    }
    private void sub_Find_Zip_Position() throws Exception                                                                                                                 //Natural: FIND-ZIP-POSITION
    {
        if (BLNatReinput.isReinput()) return;

        FOR18:                                                                                                                                                            //Natural: FOR #SUB-ADDR = 35 TO 1 STEP -1
        for (pnd_Sub_Addr.setValue(35); condition(pnd_Sub_Addr.greaterOrEqual(1)); pnd_Sub_Addr.nsubtract(1))
        {
            if (condition(pnd_Work_Addr_Line_Pnd_Work_Addr_Digit.getValue(pnd_Sub_Addr).notEquals(" ")))                                                                  //Natural: IF #WORK-ADDR-DIGIT ( #SUB-ADDR ) NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM MOVE-ZIP
                sub_Move_Zip();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                pnd_Sub_Addr.setValue(0);                                                                                                                                 //Natural: MOVE 0 TO #SUB-ADDR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  FIND-ZIP-POSITION
    }
    private void sub_Move_Zip() throws Exception                                                                                                                          //Natural: MOVE-ZIP
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------------------------------------**
        if (condition(pnd_Sub_Addr.greater(29)))                                                                                                                          //Natural: IF #SUB-ADDR > 29
        {
            pnd_Sub_Na.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #SUB-NA
            if (condition(pnd_Sub_Na.lessOrEqual(5)))                                                                                                                     //Natural: IF #SUB-NA LE 5
            {
                ldaCislext.getEx_Address_Line().getValue(pnd_Sub_Na).setValue(ldaCislext.getEx_Zip());                                                                    //Natural: MOVE EX.ZIP TO EX.ADDRESS-LINE ( #SUB-NA )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Sub_Addr.nadd(+1);                                                                                                                                        //Natural: ADD +1 TO #SUB-ADDR
            pnd_Work_Zip.setValue(ldaCislext.getEx_Zip());                                                                                                                //Natural: ASSIGN #WORK-ZIP := EX.ZIP
            FOR19:                                                                                                                                                        //Natural: FOR #SUB-CNT = 1 TO 5
            for (pnd_Sub_Cnt.setValue(1); condition(pnd_Sub_Cnt.lessOrEqual(5)); pnd_Sub_Cnt.nadd(1))
            {
                pnd_Sub_Addr.nadd(+1);                                                                                                                                    //Natural: ADD +1 TO #SUB-ADDR
                pnd_Work_Addr_Line_Pnd_Work_Addr_Digit.getValue(pnd_Sub_Addr).setValue(pnd_Work_Zip_Pnd_Work_Zip_Digit.getValue(pnd_Sub_Cnt));                            //Natural: MOVE #WORK-ZIP-DIGIT ( #SUB-CNT ) TO #WORK-ADDR-DIGIT ( #SUB-ADDR )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            ldaCislext.getEx_Address_Line().getValue(pnd_Sub_Na).setValue(pnd_Work_Addr_Line);                                                                            //Natural: MOVE #WORK-ADDR-LINE TO EX.ADDRESS-LINE ( #SUB-NA )
        }                                                                                                                                                                 //Natural: END-IF
        //*   MOVE-ZIP
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133");

        getReports().setDisplayColumns(0, "********  EXTRACT FILE IS EMPTY ********");
    }
}
