/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:11 PM
**        * FROM NATURAL LDA     : FCPL600
************************************************************
**        * FILE NAME            : LdaFcpl600.java
**        * CLASS NAME           : LdaFcpl600
**        * INSTANCE NAME        : LdaFcpl600
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl600 extends DbsRecord
{
    // Properties
    private DbsField pnd_Code;
    private DbsField pnd_Code_Desc;

    public DbsField getPnd_Code() { return pnd_Code; }

    public DbsField getPnd_Code_Desc() { return pnd_Code_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Code = newFieldArrayInRecord("pnd_Code", "#CODE", FieldType.NUMERIC, 2, new DbsArrayController(1,59));

        pnd_Code_Desc = newFieldArrayInRecord("pnd_Code_Desc", "#CODE-DESC", FieldType.STRING, 70, new DbsArrayController(1,59));

        this.setRecordName("LdaFcpl600");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Code.getValue(1).setInitialValue(1);
        pnd_Code.getValue(2).setInitialValue(2);
        pnd_Code.getValue(3).setInitialValue(3);
        pnd_Code.getValue(4).setInitialValue(4);
        pnd_Code.getValue(5).setInitialValue(5);
        pnd_Code.getValue(6).setInitialValue(6);
        pnd_Code.getValue(7).setInitialValue(7);
        pnd_Code.getValue(8).setInitialValue(8);
        pnd_Code.getValue(9).setInitialValue(9);
        pnd_Code.getValue(10).setInitialValue(10);
        pnd_Code.getValue(11).setInitialValue(11);
        pnd_Code.getValue(12).setInitialValue(12);
        pnd_Code.getValue(13).setInitialValue(13);
        pnd_Code.getValue(14).setInitialValue(14);
        pnd_Code.getValue(15).setInitialValue(15);
        pnd_Code.getValue(16).setInitialValue(16);
        pnd_Code.getValue(17).setInitialValue(17);
        pnd_Code.getValue(18).setInitialValue(18);
        pnd_Code.getValue(19).setInitialValue(19);
        pnd_Code.getValue(20).setInitialValue(20);
        pnd_Code.getValue(21).setInitialValue(21);
        pnd_Code.getValue(22).setInitialValue(22);
        pnd_Code.getValue(23).setInitialValue(23);
        pnd_Code.getValue(24).setInitialValue(24);
        pnd_Code.getValue(25).setInitialValue(25);
        pnd_Code.getValue(26).setInitialValue(26);
        pnd_Code.getValue(27).setInitialValue(27);
        pnd_Code.getValue(28).setInitialValue(28);
        pnd_Code.getValue(29).setInitialValue(29);
        pnd_Code.getValue(30).setInitialValue(30);
        pnd_Code.getValue(31).setInitialValue(31);
        pnd_Code.getValue(53).setInitialValue(53);
        pnd_Code.getValue(54).setInitialValue(54);
        pnd_Code.getValue(55).setInitialValue(55);
        pnd_Code.getValue(56).setInitialValue(56);
        pnd_Code.getValue(57).setInitialValue(57);
        pnd_Code_Desc.getValue(1).setInitialValue("Single Life Annuity");
        pnd_Code_Desc.getValue(2).setInitialValue("Installment Refund");
        pnd_Code_Desc.getValue(3).setInitialValue("Half Benefit to Second Annuitant");
        pnd_Code_Desc.getValue(4).setInitialValue("Full Benefit to Survivor");
        pnd_Code_Desc.getValue(5).setInitialValue("Single Life Annuity With 10-Year Guaranteed Period");
        pnd_Code_Desc.getValue(6).setInitialValue("Single Life Annuity With 20-Year Guaranteed Period");
        pnd_Code_Desc.getValue(7).setInitialValue("Joint and Two-Thirds Benefit to Survivor");
        pnd_Code_Desc.getValue(8).setInitialValue("Joint & 2/3 Benefit to Survivor With 10-Year Guaranteed Period");
        pnd_Code_Desc.getValue(9).setInitialValue("Single Life Annuity With 15-Year Guaranteed Period");
        pnd_Code_Desc.getValue(10).setInitialValue("Joint & 2/3 Benefit to Survivor With 20-Year Guaranteed Period");
        pnd_Code_Desc.getValue(11).setInitialValue("Full Benefit to Survivor With 20-Year Guaranteed Period");
        pnd_Code_Desc.getValue(12).setInitialValue("One-Half Benefit to Second Annuitant With 20-YearGuaranteed Period");
        pnd_Code_Desc.getValue(13).setInitialValue("Joint & 2/3 Benefit to Survivor With 15-Year Guaranteed Period");
        pnd_Code_Desc.getValue(14).setInitialValue("Full Benefit to Survivor With 15-Year Guaranteed Period");
        pnd_Code_Desc.getValue(15).setInitialValue("One-Half Benefit to Second Annuitant With 15-YearGuaranteed Period");
        pnd_Code_Desc.getValue(16).setInitialValue("Full Benefit to Survivor With 10-Year Guaranteed Period");
        pnd_Code_Desc.getValue(17).setInitialValue("One-Half Benefit to Second Annuitant With 10-YearGuaranteed Period");
        pnd_Code_Desc.getValue(18).setInitialValue("Two-Thirds Benefit to Survivor - Group Annuity");
        pnd_Code_Desc.getValue(19).setInitialValue("Single Life With Death Benefit -Group Annuity");
        pnd_Code_Desc.getValue(20).setInitialValue("Temporary Life - Group Annuity");
        pnd_Code_Desc.getValue(21).setInitialValue("Annuity Certain");
        pnd_Code_Desc.getValue(22).setInitialValue("Principal and Interest");
        pnd_Code_Desc.getValue(23).setInitialValue("Family Income");
        pnd_Code_Desc.getValue(24).setInitialValue("Modified Cash Refund Annuity");
        pnd_Code_Desc.getValue(25).setInitialValue("Interest Payment Retirement Option - From a DA Contract");
        pnd_Code_Desc.getValue(26).setInitialValue("Interest Payment Retirement Option - From an SRA Contract");
        pnd_Code_Desc.getValue(27).setInitialValue("Interest Payment Retirement Option - From a GRA Contract");
        pnd_Code_Desc.getValue(28).setInitialValue("Transfer Payout Annuity - From a DA Contract");
        pnd_Code_Desc.getValue(30).setInitialValue("Transfer Payout Annuity - From a GRA Contract");
        pnd_Code_Desc.getValue(31).setInitialValue("Minimum Distribution Option");
        pnd_Code_Desc.getValue(53).setInitialValue("Last Survivor 75% w/ 5 Year Certain.");
        pnd_Code_Desc.getValue(54).setInitialValue("Last Survivor 75% w/10 Year Certain.");
        pnd_Code_Desc.getValue(55).setInitialValue("Last Survivor 75% w/15 Year Certain.");
        pnd_Code_Desc.getValue(56).setInitialValue("Last Survivor 75% w/20 Year Certain.");
        pnd_Code_Desc.getValue(57).setInitialValue("Last Survivor 75% w/ 0 Year Certain.");
    }

    // Constructor
    public LdaFcpl600() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
