/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:11 PM
**        * FROM NATURAL LDA     : FCPL702A
************************************************************
**        * FILE NAME            : LdaFcpl702a.java
**        * CLASS NAME           : LdaFcpl702a
**        * INSTANCE NAME        : LdaFcpl702a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl702a extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_pay;
    private DbsField pay_Cntrct_Ppcn_Nbr;
    private DbsField pay_Cntrct_Payee_Cde;
    private DbsField pay_Cntrct_Invrse_Dte;
    private DbsField pay_Cntrct_Check_Crrncy_Cde;
    private DbsField pay_Cntrct_Orgn_Cde;
    private DbsField pay_Cntrct_Unq_Id_Nbr;
    private DbsField pay_Cntrct_Type_Cde;
    private DbsField pay_Cntrct_Lob_Cde;
    private DbsField pay_Cntrct_Option_Cde;
    private DbsField pay_Cntrct_Mode_Cde;
    private DbsField pay_Cntrct_Roll_Dest_Cde;
    private DbsField pay_Cntrct_Settl_Amt;
    private DbsField pay_Cntrct_Cmbn_Nbr;
    private DbsField pay_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pay_Annt_Soc_Sec_Ind;
    private DbsField pay_Annt_Soc_Sec_Nbr;
    private DbsField pay_Annt_Ctznshp_Cde;
    private DbsField pay_Inv_Acct_Cde;
    private DbsField pay_Pymnt_Stats_Cde;
    private DbsField pay_Pymnt_Pay_Type_Req_Ind;
    private DbsField pay_Pymnt_Check_Dte;
    private DbsGroup pay_Pymnt_Check_DteRedef1;
    private DbsField pay_Pymnt_Dte;
    private DbsField pay_Pymnt_Eft_Dte;
    private DbsField pay_Pymnt_Check_Nbr;
    private DbsField pay_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pay_Pymnt_Prcss_Seq_NbrRedef2;
    private DbsField pay_Pymnt_Prcss_Seq_Num;
    private DbsField pay_Pymnt_Instmt_Nbr;
    private DbsField pay_Pymnt_Check_Amt;
    private DbsField pay_Pymnt_Nbr;
    private DbsField pay_Pymnt_Reqst_Nbr;
    private DataAccessProgramView vw_addr;
    private DbsField addr_Cntrct_Orgn_Cde;
    private DbsField addr_Cntrct_Ppcn_Nbr;
    private DbsField addr_Cntrct_Payee_Cde;
    private DbsField addr_Cntrct_Invrse_Dte;
    private DbsGroup addr_Ph_Name;
    private DbsField addr_Ph_Last_Name;
    private DbsField addr_Ph_First_Name;
    private DbsField addr_Ph_Middle_Name;
    private DbsField addr_Count_Castpymnt_Nme_And_Addr_Grp;
    private DbsGroup addr_Pymnt_Nme_And_Addr_Grp;
    private DbsField addr_Pymnt_Nme;
    private DbsField addr_Pymnt_Addr_Line1_Txt;
    private DbsField addr_Pymnt_Addr_Line2_Txt;
    private DbsField addr_Pymnt_Addr_Line3_Txt;
    private DbsField addr_Pymnt_Addr_Line4_Txt;
    private DbsField addr_Pymnt_Addr_Line5_Txt;
    private DbsField addr_Pymnt_Addr_Line6_Txt;
    private DbsField addr_Pymnt_Addr_Zip_Cde;
    private DbsField addr_Pymnt_Postl_Data;
    private DbsField addr_Pymnt_Eft_Transit_Id;
    private DbsField addr_Pymnt_Eft_Acct_Nbr;
    private DbsField addr_Pymnt_Chk_Sav_Ind;
    private DbsField addr_Pymnt_Reqst_Nbr;
    private DbsField addr_Ppcn_Payee_Orgn_Invrs_Seq;

    public DataAccessProgramView getVw_pay() { return vw_pay; }

    public DbsField getPay_Cntrct_Ppcn_Nbr() { return pay_Cntrct_Ppcn_Nbr; }

    public DbsField getPay_Cntrct_Payee_Cde() { return pay_Cntrct_Payee_Cde; }

    public DbsField getPay_Cntrct_Invrse_Dte() { return pay_Cntrct_Invrse_Dte; }

    public DbsField getPay_Cntrct_Check_Crrncy_Cde() { return pay_Cntrct_Check_Crrncy_Cde; }

    public DbsField getPay_Cntrct_Orgn_Cde() { return pay_Cntrct_Orgn_Cde; }

    public DbsField getPay_Cntrct_Unq_Id_Nbr() { return pay_Cntrct_Unq_Id_Nbr; }

    public DbsField getPay_Cntrct_Type_Cde() { return pay_Cntrct_Type_Cde; }

    public DbsField getPay_Cntrct_Lob_Cde() { return pay_Cntrct_Lob_Cde; }

    public DbsField getPay_Cntrct_Option_Cde() { return pay_Cntrct_Option_Cde; }

    public DbsField getPay_Cntrct_Mode_Cde() { return pay_Cntrct_Mode_Cde; }

    public DbsField getPay_Cntrct_Roll_Dest_Cde() { return pay_Cntrct_Roll_Dest_Cde; }

    public DbsField getPay_Cntrct_Settl_Amt() { return pay_Cntrct_Settl_Amt; }

    public DbsField getPay_Cntrct_Cmbn_Nbr() { return pay_Cntrct_Cmbn_Nbr; }

    public DbsField getPay_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pay_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPay_Annt_Soc_Sec_Ind() { return pay_Annt_Soc_Sec_Ind; }

    public DbsField getPay_Annt_Soc_Sec_Nbr() { return pay_Annt_Soc_Sec_Nbr; }

    public DbsField getPay_Annt_Ctznshp_Cde() { return pay_Annt_Ctznshp_Cde; }

    public DbsField getPay_Inv_Acct_Cde() { return pay_Inv_Acct_Cde; }

    public DbsField getPay_Pymnt_Stats_Cde() { return pay_Pymnt_Stats_Cde; }

    public DbsField getPay_Pymnt_Pay_Type_Req_Ind() { return pay_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPay_Pymnt_Check_Dte() { return pay_Pymnt_Check_Dte; }

    public DbsGroup getPay_Pymnt_Check_DteRedef1() { return pay_Pymnt_Check_DteRedef1; }

    public DbsField getPay_Pymnt_Dte() { return pay_Pymnt_Dte; }

    public DbsField getPay_Pymnt_Eft_Dte() { return pay_Pymnt_Eft_Dte; }

    public DbsField getPay_Pymnt_Check_Nbr() { return pay_Pymnt_Check_Nbr; }

    public DbsField getPay_Pymnt_Prcss_Seq_Nbr() { return pay_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPay_Pymnt_Prcss_Seq_NbrRedef2() { return pay_Pymnt_Prcss_Seq_NbrRedef2; }

    public DbsField getPay_Pymnt_Prcss_Seq_Num() { return pay_Pymnt_Prcss_Seq_Num; }

    public DbsField getPay_Pymnt_Instmt_Nbr() { return pay_Pymnt_Instmt_Nbr; }

    public DbsField getPay_Pymnt_Check_Amt() { return pay_Pymnt_Check_Amt; }

    public DbsField getPay_Pymnt_Nbr() { return pay_Pymnt_Nbr; }

    public DbsField getPay_Pymnt_Reqst_Nbr() { return pay_Pymnt_Reqst_Nbr; }

    public DataAccessProgramView getVw_addr() { return vw_addr; }

    public DbsField getAddr_Cntrct_Orgn_Cde() { return addr_Cntrct_Orgn_Cde; }

    public DbsField getAddr_Cntrct_Ppcn_Nbr() { return addr_Cntrct_Ppcn_Nbr; }

    public DbsField getAddr_Cntrct_Payee_Cde() { return addr_Cntrct_Payee_Cde; }

    public DbsField getAddr_Cntrct_Invrse_Dte() { return addr_Cntrct_Invrse_Dte; }

    public DbsGroup getAddr_Ph_Name() { return addr_Ph_Name; }

    public DbsField getAddr_Ph_Last_Name() { return addr_Ph_Last_Name; }

    public DbsField getAddr_Ph_First_Name() { return addr_Ph_First_Name; }

    public DbsField getAddr_Ph_Middle_Name() { return addr_Ph_Middle_Name; }

    public DbsField getAddr_Count_Castpymnt_Nme_And_Addr_Grp() { return addr_Count_Castpymnt_Nme_And_Addr_Grp; }

    public DbsGroup getAddr_Pymnt_Nme_And_Addr_Grp() { return addr_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getAddr_Pymnt_Nme() { return addr_Pymnt_Nme; }

    public DbsField getAddr_Pymnt_Addr_Line1_Txt() { return addr_Pymnt_Addr_Line1_Txt; }

    public DbsField getAddr_Pymnt_Addr_Line2_Txt() { return addr_Pymnt_Addr_Line2_Txt; }

    public DbsField getAddr_Pymnt_Addr_Line3_Txt() { return addr_Pymnt_Addr_Line3_Txt; }

    public DbsField getAddr_Pymnt_Addr_Line4_Txt() { return addr_Pymnt_Addr_Line4_Txt; }

    public DbsField getAddr_Pymnt_Addr_Line5_Txt() { return addr_Pymnt_Addr_Line5_Txt; }

    public DbsField getAddr_Pymnt_Addr_Line6_Txt() { return addr_Pymnt_Addr_Line6_Txt; }

    public DbsField getAddr_Pymnt_Addr_Zip_Cde() { return addr_Pymnt_Addr_Zip_Cde; }

    public DbsField getAddr_Pymnt_Postl_Data() { return addr_Pymnt_Postl_Data; }

    public DbsField getAddr_Pymnt_Eft_Transit_Id() { return addr_Pymnt_Eft_Transit_Id; }

    public DbsField getAddr_Pymnt_Eft_Acct_Nbr() { return addr_Pymnt_Eft_Acct_Nbr; }

    public DbsField getAddr_Pymnt_Chk_Sav_Ind() { return addr_Pymnt_Chk_Sav_Ind; }

    public DbsField getAddr_Pymnt_Reqst_Nbr() { return addr_Pymnt_Reqst_Nbr; }

    public DbsField getAddr_Ppcn_Payee_Orgn_Invrs_Seq() { return addr_Ppcn_Payee_Orgn_Invrs_Seq; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_pay = new DataAccessProgramView(new NameInfo("vw_pay", "PAY"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PYMNT"));
        pay_Cntrct_Ppcn_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        pay_Cntrct_Payee_Cde = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        pay_Cntrct_Invrse_Dte = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_INVRSE_DTE");
        pay_Cntrct_Check_Crrncy_Cde = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNTRCT_CHECK_CRRNCY_CDE");
        pay_Cntrct_Orgn_Cde = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        pay_Cntrct_Unq_Id_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_UNQ_ID_NBR");
        pay_Cntrct_Type_Cde = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        pay_Cntrct_Lob_Cde = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_LOB_CDE");
        pay_Cntrct_Option_Cde = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTION_CDE");
        pay_Cntrct_Mode_Cde = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_CDE");
        pay_Cntrct_Roll_Dest_Cde = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_ROLL_DEST_CDE");
        pay_Cntrct_Settl_Amt = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Settl_Amt", "CNTRCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_SETTL_AMT");
        pay_Cntrct_Cmbn_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBN_NBR");
        pay_Cntrct_Cancel_Rdrw_Actvty_Cde = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_ACTVTY_CDE");
        pay_Annt_Soc_Sec_Ind = vw_pay.getRecord().newFieldInGroup("pay_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "ANNT_SOC_SEC_IND");
        pay_Annt_Soc_Sec_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "ANNT_SOC_SEC_NBR");
        pay_Annt_Ctznshp_Cde = vw_pay.getRecord().newFieldInGroup("pay_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ANNT_CTZNSHP_CDE");
        pay_Inv_Acct_Cde = vw_pay.getRecord().newFieldArrayInGroup("pay_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2, new DbsArrayController(1,40) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_CDE", "FCP_CONS_PYMT_INV_ACCT");
        pay_Pymnt_Stats_Cde = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        pay_Pymnt_Pay_Type_Req_Ind = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1, 
            RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        pay_Pymnt_Check_Dte = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        pay_Pymnt_Check_DteRedef1 = vw_pay.getRecord().newGroupInGroup("pay_Pymnt_Check_DteRedef1", "Redefines", pay_Pymnt_Check_Dte);
        pay_Pymnt_Dte = pay_Pymnt_Check_DteRedef1.newFieldInGroup("pay_Pymnt_Dte", "PYMNT-DTE", FieldType.DATE);
        pay_Pymnt_Eft_Dte = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "PYMNT_EFT_DTE");
        pay_Pymnt_Check_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_NBR");
        pay_Pymnt_Prcss_Seq_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PYMNT_PRCSS_SEQ_NBR");
        pay_Pymnt_Prcss_Seq_NbrRedef2 = vw_pay.getRecord().newGroupInGroup("pay_Pymnt_Prcss_Seq_NbrRedef2", "Redefines", pay_Pymnt_Prcss_Seq_Nbr);
        pay_Pymnt_Prcss_Seq_Num = pay_Pymnt_Prcss_Seq_NbrRedef2.newFieldInGroup("pay_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pay_Pymnt_Instmt_Nbr = pay_Pymnt_Prcss_Seq_NbrRedef2.newFieldInGroup("pay_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pay_Pymnt_Check_Amt = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_AMT");
        pay_Pymnt_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, "PYMNT_NBR");
        pay_Pymnt_Reqst_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");

        vw_addr = new DataAccessProgramView(new NameInfo("vw_addr", "ADDR"), "FCP_CONS_ADDR", "FCP_CONS_LEDGR", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ADDR"));
        addr_Cntrct_Orgn_Cde = vw_addr.getRecord().newFieldInGroup("addr_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        addr_Cntrct_Ppcn_Nbr = vw_addr.getRecord().newFieldInGroup("addr_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        addr_Cntrct_Payee_Cde = vw_addr.getRecord().newFieldInGroup("addr_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        addr_Cntrct_Invrse_Dte = vw_addr.getRecord().newFieldInGroup("addr_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_INVRSE_DTE");
        addr_Ph_Name = vw_addr.getRecord().newGroupInGroup("addr_Ph_Name", "PH-NAME");
        addr_Ph_Last_Name = addr_Ph_Name.newFieldInGroup("addr_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16, RepeatingFieldStrategy.None, "PH_LAST_NAME");
        addr_Ph_First_Name = addr_Ph_Name.newFieldInGroup("addr_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10, RepeatingFieldStrategy.None, "PH_FIRST_NAME");
        addr_Ph_Middle_Name = addr_Ph_Name.newFieldInGroup("addr_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "PH_MIDDLE_NAME");
        addr_Count_Castpymnt_Nme_And_Addr_Grp = vw_addr.getRecord().newFieldInGroup("addr_Count_Castpymnt_Nme_And_Addr_Grp", "C*PYMNT-NME-AND-ADDR-GRP", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Nme_And_Addr_Grp = vw_addr.getRecord().newGroupInGroup("addr_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Nme = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38, new DbsArrayController(1,1) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_NME", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line1_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE1_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line2_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE2_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line3_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE3_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line4_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE4_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line5_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE5_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line6_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE6_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Zip_Cde = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 
            9, new DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_ZIP_CDE", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Postl_Data = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 32, new 
            DbsArrayController(1,1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_POSTL_DATA", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Eft_Transit_Id = vw_addr.getRecord().newFieldInGroup("addr_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PYMNT_EFT_TRANSIT_ID");
        addr_Pymnt_Eft_Acct_Nbr = vw_addr.getRecord().newFieldInGroup("addr_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, 
            "PYMNT_EFT_ACCT_NBR");
        addr_Pymnt_Chk_Sav_Ind = vw_addr.getRecord().newFieldInGroup("addr_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CHK_SAV_IND");
        addr_Pymnt_Reqst_Nbr = vw_addr.getRecord().newFieldInGroup("addr_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");
        addr_Ppcn_Payee_Orgn_Invrs_Seq = vw_addr.getRecord().newFieldInGroup("addr_Ppcn_Payee_Orgn_Invrs_Seq", "PPCN-PAYEE-ORGN-INVRS-SEQ", FieldType.STRING, 
            31, RepeatingFieldStrategy.None, "PPCN_PAYEE_ORGN_INVRS_SEQ");
        vw_pay.setUniquePeList();
        vw_addr.setUniquePeList();

        this.setRecordName("LdaFcpl702a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_pay.reset();
        vw_addr.reset();
    }

    // Constructor
    public LdaFcpl702a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
