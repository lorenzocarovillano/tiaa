/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:21 PM
**        * FROM NATURAL LDA     : CPOL155M
************************************************************
**        * FILE NAME            : LdaCpol155m.java
**        * CLASS NAME           : LdaCpol155m
**        * INSTANCE NAME        : LdaCpol155m
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpol155m extends DbsRecord
{
    // Properties
    private DbsGroup cpol155m;
    private DbsField cpol155m_Micr_Bank_Cde;
    private DbsField cpol155m_Micr_Check_Nbr;
    private DbsField cpol155m_Micr_Bank_Routing;
    private DbsField cpol155m_Micr_Bank_Account;
    private DbsField cpol155m_Micr_Micr_Line;
    private DbsField cpol155m_Micr_Return_Cde;

    public DbsGroup getCpol155m() { return cpol155m; }

    public DbsField getCpol155m_Micr_Bank_Cde() { return cpol155m_Micr_Bank_Cde; }

    public DbsField getCpol155m_Micr_Check_Nbr() { return cpol155m_Micr_Check_Nbr; }

    public DbsField getCpol155m_Micr_Bank_Routing() { return cpol155m_Micr_Bank_Routing; }

    public DbsField getCpol155m_Micr_Bank_Account() { return cpol155m_Micr_Bank_Account; }

    public DbsField getCpol155m_Micr_Micr_Line() { return cpol155m_Micr_Micr_Line; }

    public DbsField getCpol155m_Micr_Return_Cde() { return cpol155m_Micr_Return_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cpol155m = newGroupInRecord("cpol155m", "CPOL155M");
        cpol155m_Micr_Bank_Cde = cpol155m.newFieldInGroup("cpol155m_Micr_Bank_Cde", "MICR-BANK-CDE", FieldType.STRING, 5);
        cpol155m_Micr_Check_Nbr = cpol155m.newFieldInGroup("cpol155m_Micr_Check_Nbr", "MICR-CHECK-NBR", FieldType.NUMERIC, 10);
        cpol155m_Micr_Bank_Routing = cpol155m.newFieldInGroup("cpol155m_Micr_Bank_Routing", "MICR-BANK-ROUTING", FieldType.STRING, 9);
        cpol155m_Micr_Bank_Account = cpol155m.newFieldInGroup("cpol155m_Micr_Bank_Account", "MICR-BANK-ACCOUNT", FieldType.STRING, 21);
        cpol155m_Micr_Micr_Line = cpol155m.newFieldInGroup("cpol155m_Micr_Micr_Line", "MICR-MICR-LINE", FieldType.STRING, 100);
        cpol155m_Micr_Return_Cde = cpol155m.newFieldInGroup("cpol155m_Micr_Return_Cde", "MICR-RETURN-CDE", FieldType.STRING, 2);

        this.setRecordName("LdaCpol155m");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCpol155m() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
