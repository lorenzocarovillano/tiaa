/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:31 PM
**        * FROM NATURAL PDA     : FCPALDGR
************************************************************
**        * FILE NAME            : PdaFcpaldgr.java
**        * CLASS NAME           : PdaFcpaldgr
**        * INSTANCE NAME        : PdaFcpaldgr
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpaldgr extends PdaBase
{
    // Properties
    private DbsGroup ledger_Work_Fields;
    private DbsField ledger_Work_Fields_Ledger_Abend_Ind;
    private DbsField ledger_Work_Fields_Ledger_Return_Cde;
    private DbsGroup ledger;
    private DbsField ledger_Cntrct_Rcrd_Typ;
    private DbsField ledger_Cntrct_Check_Crrncy_Cde;
    private DbsField ledger_Cntl_Cycle_Dte;
    private DbsField ledger_Cntl_Check_Dte;
    private DbsField ledger_Cntrct_Ppcn_Nbr;
    private DbsField ledger_Cntrct_Orgn_Cde;
    private DbsField ledger_Cntrct_Cref_Nbr;
    private DbsField ledger_Pymnt_Settlmnt_Dte;
    private DbsField ledger_Pymnt_Acctg_Dte;
    private DbsField ledger_Pymnt_Intrfce_Dte;
    private DbsField ledger_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup ledger_Pymnt_Prcss_Seq_NbrRedef1;
    private DbsField ledger_Pymnt_Prcss_Seq_Num;
    private DbsField ledger_Pymnt_Instmt_Nbr;
    private DbsField ledger_C_Inv_Ledgr;
    private DbsGroup ledger_Inv_Ledgr;
    private DbsField ledger_Inv_Acct_Cde;
    private DbsField ledger_Inv_Acct_Ledgr_Nbr;
    private DbsField ledger_Inv_Acct_Ledgr_Amt;
    private DbsField ledger_Inv_Acct_Ledgr_Ind;
    private DbsField ledger_C_Inv_Ledgr_Ovrfl;
    private DbsGroup ledger_Inv_Ledgr_Ovrfl;
    private DbsField ledger_Inv_Acct_Cde_Ovrfl;
    private DbsField ledger_Inv_Acct_Ledgr_Nbr_Ovrfl;
    private DbsField ledger_Inv_Acct_Ledgr_Amt_Ovrfl;
    private DbsField ledger_Inv_Acct_Ledgr_Ind_Ovrfl;
    private DbsField ledger_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField ledger_Cntrct_Annty_Ins_Type;

    public DbsGroup getLedger_Work_Fields() { return ledger_Work_Fields; }

    public DbsField getLedger_Work_Fields_Ledger_Abend_Ind() { return ledger_Work_Fields_Ledger_Abend_Ind; }

    public DbsField getLedger_Work_Fields_Ledger_Return_Cde() { return ledger_Work_Fields_Ledger_Return_Cde; }

    public DbsGroup getLedger() { return ledger; }

    public DbsField getLedger_Cntrct_Rcrd_Typ() { return ledger_Cntrct_Rcrd_Typ; }

    public DbsField getLedger_Cntrct_Check_Crrncy_Cde() { return ledger_Cntrct_Check_Crrncy_Cde; }

    public DbsField getLedger_Cntl_Cycle_Dte() { return ledger_Cntl_Cycle_Dte; }

    public DbsField getLedger_Cntl_Check_Dte() { return ledger_Cntl_Check_Dte; }

    public DbsField getLedger_Cntrct_Ppcn_Nbr() { return ledger_Cntrct_Ppcn_Nbr; }

    public DbsField getLedger_Cntrct_Orgn_Cde() { return ledger_Cntrct_Orgn_Cde; }

    public DbsField getLedger_Cntrct_Cref_Nbr() { return ledger_Cntrct_Cref_Nbr; }

    public DbsField getLedger_Pymnt_Settlmnt_Dte() { return ledger_Pymnt_Settlmnt_Dte; }

    public DbsField getLedger_Pymnt_Acctg_Dte() { return ledger_Pymnt_Acctg_Dte; }

    public DbsField getLedger_Pymnt_Intrfce_Dte() { return ledger_Pymnt_Intrfce_Dte; }

    public DbsField getLedger_Pymnt_Prcss_Seq_Nbr() { return ledger_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getLedger_Pymnt_Prcss_Seq_NbrRedef1() { return ledger_Pymnt_Prcss_Seq_NbrRedef1; }

    public DbsField getLedger_Pymnt_Prcss_Seq_Num() { return ledger_Pymnt_Prcss_Seq_Num; }

    public DbsField getLedger_Pymnt_Instmt_Nbr() { return ledger_Pymnt_Instmt_Nbr; }

    public DbsField getLedger_C_Inv_Ledgr() { return ledger_C_Inv_Ledgr; }

    public DbsGroup getLedger_Inv_Ledgr() { return ledger_Inv_Ledgr; }

    public DbsField getLedger_Inv_Acct_Cde() { return ledger_Inv_Acct_Cde; }

    public DbsField getLedger_Inv_Acct_Ledgr_Nbr() { return ledger_Inv_Acct_Ledgr_Nbr; }

    public DbsField getLedger_Inv_Acct_Ledgr_Amt() { return ledger_Inv_Acct_Ledgr_Amt; }

    public DbsField getLedger_Inv_Acct_Ledgr_Ind() { return ledger_Inv_Acct_Ledgr_Ind; }

    public DbsField getLedger_C_Inv_Ledgr_Ovrfl() { return ledger_C_Inv_Ledgr_Ovrfl; }

    public DbsGroup getLedger_Inv_Ledgr_Ovrfl() { return ledger_Inv_Ledgr_Ovrfl; }

    public DbsField getLedger_Inv_Acct_Cde_Ovrfl() { return ledger_Inv_Acct_Cde_Ovrfl; }

    public DbsField getLedger_Inv_Acct_Ledgr_Nbr_Ovrfl() { return ledger_Inv_Acct_Ledgr_Nbr_Ovrfl; }

    public DbsField getLedger_Inv_Acct_Ledgr_Amt_Ovrfl() { return ledger_Inv_Acct_Ledgr_Amt_Ovrfl; }

    public DbsField getLedger_Inv_Acct_Ledgr_Ind_Ovrfl() { return ledger_Inv_Acct_Ledgr_Ind_Ovrfl; }

    public DbsField getLedger_Cntrct_Cancel_Rdrw_Actvty_Cde() { return ledger_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getLedger_Cntrct_Annty_Ins_Type() { return ledger_Cntrct_Annty_Ins_Type; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ledger_Work_Fields = dbsRecord.newGroupInRecord("ledger_Work_Fields", "LEDGER-WORK-FIELDS");
        ledger_Work_Fields.setParameterOption(ParameterOption.ByReference);
        ledger_Work_Fields_Ledger_Abend_Ind = ledger_Work_Fields.newFieldInGroup("ledger_Work_Fields_Ledger_Abend_Ind", "LEDGER-ABEND-IND", FieldType.BOOLEAN);
        ledger_Work_Fields_Ledger_Return_Cde = ledger_Work_Fields.newFieldInGroup("ledger_Work_Fields_Ledger_Return_Cde", "LEDGER-RETURN-CDE", FieldType.NUMERIC, 
            2);

        ledger = dbsRecord.newGroupInRecord("ledger", "LEDGER");
        ledger.setParameterOption(ParameterOption.ByReference);
        ledger_Cntrct_Rcrd_Typ = ledger.newFieldInGroup("ledger_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1);
        ledger_Cntrct_Check_Crrncy_Cde = ledger.newFieldInGroup("ledger_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        ledger_Cntl_Cycle_Dte = ledger.newFieldInGroup("ledger_Cntl_Cycle_Dte", "CNTL-CYCLE-DTE", FieldType.DATE);
        ledger_Cntl_Check_Dte = ledger.newFieldInGroup("ledger_Cntl_Check_Dte", "CNTL-CHECK-DTE", FieldType.DATE);
        ledger_Cntrct_Ppcn_Nbr = ledger.newFieldInGroup("ledger_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        ledger_Cntrct_Orgn_Cde = ledger.newFieldInGroup("ledger_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        ledger_Cntrct_Cref_Nbr = ledger.newFieldInGroup("ledger_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10);
        ledger_Pymnt_Settlmnt_Dte = ledger.newFieldInGroup("ledger_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        ledger_Pymnt_Acctg_Dte = ledger.newFieldInGroup("ledger_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        ledger_Pymnt_Intrfce_Dte = ledger.newFieldInGroup("ledger_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        ledger_Pymnt_Prcss_Seq_Nbr = ledger.newFieldInGroup("ledger_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        ledger_Pymnt_Prcss_Seq_NbrRedef1 = ledger.newGroupInGroup("ledger_Pymnt_Prcss_Seq_NbrRedef1", "Redefines", ledger_Pymnt_Prcss_Seq_Nbr);
        ledger_Pymnt_Prcss_Seq_Num = ledger_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("ledger_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        ledger_Pymnt_Instmt_Nbr = ledger_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("ledger_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        ledger_C_Inv_Ledgr = ledger.newFieldInGroup("ledger_C_Inv_Ledgr", "C-INV-LEDGR", FieldType.PACKED_DECIMAL, 3);
        ledger_Inv_Ledgr = ledger.newGroupArrayInGroup("ledger_Inv_Ledgr", "INV-LEDGR", new DbsArrayController(1,99));
        ledger_Inv_Acct_Cde = ledger_Inv_Ledgr.newFieldInGroup("ledger_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        ledger_Inv_Acct_Ledgr_Nbr = ledger_Inv_Ledgr.newFieldInGroup("ledger_Inv_Acct_Ledgr_Nbr", "INV-ACCT-LEDGR-NBR", FieldType.STRING, 15);
        ledger_Inv_Acct_Ledgr_Amt = ledger_Inv_Ledgr.newFieldInGroup("ledger_Inv_Acct_Ledgr_Amt", "INV-ACCT-LEDGR-AMT", FieldType.PACKED_DECIMAL, 11,2);
        ledger_Inv_Acct_Ledgr_Ind = ledger_Inv_Ledgr.newFieldInGroup("ledger_Inv_Acct_Ledgr_Ind", "INV-ACCT-LEDGR-IND", FieldType.STRING, 1);
        ledger_C_Inv_Ledgr_Ovrfl = ledger.newFieldInGroup("ledger_C_Inv_Ledgr_Ovrfl", "C-INV-LEDGR-OVRFL", FieldType.PACKED_DECIMAL, 3);
        ledger_Inv_Ledgr_Ovrfl = ledger.newGroupArrayInGroup("ledger_Inv_Ledgr_Ovrfl", "INV-LEDGR-OVRFL", new DbsArrayController(1,99));
        ledger_Inv_Acct_Cde_Ovrfl = ledger_Inv_Ledgr_Ovrfl.newFieldInGroup("ledger_Inv_Acct_Cde_Ovrfl", "INV-ACCT-CDE-OVRFL", FieldType.STRING, 2);
        ledger_Inv_Acct_Ledgr_Nbr_Ovrfl = ledger_Inv_Ledgr_Ovrfl.newFieldInGroup("ledger_Inv_Acct_Ledgr_Nbr_Ovrfl", "INV-ACCT-LEDGR-NBR-OVRFL", FieldType.STRING, 
            15);
        ledger_Inv_Acct_Ledgr_Amt_Ovrfl = ledger_Inv_Ledgr_Ovrfl.newFieldInGroup("ledger_Inv_Acct_Ledgr_Amt_Ovrfl", "INV-ACCT-LEDGR-AMT-OVRFL", FieldType.PACKED_DECIMAL, 
            11,2);
        ledger_Inv_Acct_Ledgr_Ind_Ovrfl = ledger_Inv_Ledgr_Ovrfl.newFieldInGroup("ledger_Inv_Acct_Ledgr_Ind_Ovrfl", "INV-ACCT-LEDGR-IND-OVRFL", FieldType.STRING, 
            1);
        ledger_Cntrct_Cancel_Rdrw_Actvty_Cde = ledger.newFieldInGroup("ledger_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2);
        ledger_Cntrct_Annty_Ins_Type = ledger.newFieldInGroup("ledger_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpaldgr(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

