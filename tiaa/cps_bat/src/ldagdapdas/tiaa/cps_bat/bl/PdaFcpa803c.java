/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:27 PM
**        * FROM NATURAL PDA     : FCPA803C
************************************************************
**        * FILE NAME            : PdaFcpa803c.java
**        * CLASS NAME           : PdaFcpa803c
**        * INSTANCE NAME        : PdaFcpa803c
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa803c extends PdaBase
{
    // Properties
    private DbsGroup pnd_Check_Sort_Fields;
    private DbsField pnd_Check_Sort_Fields_Cntrct_Hold_Cde;
    private DbsField pnd_Check_Sort_Fields_Cntrct_Company_Cde;
    private DbsField pnd_Check_Sort_Fields_Pymnt_Total_Pages;
    private DbsField pnd_Check_Sort_Fields_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Check_Sort_Fields_Pymnt_Nbr_Alpha;
    private DbsGroup pnd_Check_Sort_Fields_Pymnt_Nbr_AlphaRedef1;
    private DbsField pnd_Check_Sort_Fields_Pymnt_Nbr;
    private DbsField filler01;

    public DbsGroup getPnd_Check_Sort_Fields() { return pnd_Check_Sort_Fields; }

    public DbsField getPnd_Check_Sort_Fields_Cntrct_Hold_Cde() { return pnd_Check_Sort_Fields_Cntrct_Hold_Cde; }

    public DbsField getPnd_Check_Sort_Fields_Cntrct_Company_Cde() { return pnd_Check_Sort_Fields_Cntrct_Company_Cde; }

    public DbsField getPnd_Check_Sort_Fields_Pymnt_Total_Pages() { return pnd_Check_Sort_Fields_Pymnt_Total_Pages; }

    public DbsField getPnd_Check_Sort_Fields_Cnr_Orgnl_Invrse_Dte() { return pnd_Check_Sort_Fields_Cnr_Orgnl_Invrse_Dte; }

    public DbsField getPnd_Check_Sort_Fields_Pymnt_Nbr_Alpha() { return pnd_Check_Sort_Fields_Pymnt_Nbr_Alpha; }

    public DbsGroup getPnd_Check_Sort_Fields_Pymnt_Nbr_AlphaRedef1() { return pnd_Check_Sort_Fields_Pymnt_Nbr_AlphaRedef1; }

    public DbsField getPnd_Check_Sort_Fields_Pymnt_Nbr() { return pnd_Check_Sort_Fields_Pymnt_Nbr; }

    public DbsField getFiller01() { return filler01; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Check_Sort_Fields = dbsRecord.newGroupInRecord("pnd_Check_Sort_Fields", "#CHECK-SORT-FIELDS");
        pnd_Check_Sort_Fields.setParameterOption(ParameterOption.ByReference);
        pnd_Check_Sort_Fields_Cntrct_Hold_Cde = pnd_Check_Sort_Fields.newFieldInGroup("pnd_Check_Sort_Fields_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            4);
        pnd_Check_Sort_Fields_Cntrct_Company_Cde = pnd_Check_Sort_Fields.newFieldInGroup("pnd_Check_Sort_Fields_Cntrct_Company_Cde", "CNTRCT-COMPANY-CDE", 
            FieldType.STRING, 1);
        pnd_Check_Sort_Fields_Pymnt_Total_Pages = pnd_Check_Sort_Fields.newFieldInGroup("pnd_Check_Sort_Fields_Pymnt_Total_Pages", "PYMNT-TOTAL-PAGES", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Check_Sort_Fields_Cnr_Orgnl_Invrse_Dte = pnd_Check_Sort_Fields.newFieldInGroup("pnd_Check_Sort_Fields_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Check_Sort_Fields_Pymnt_Nbr_Alpha = pnd_Check_Sort_Fields.newFieldInGroup("pnd_Check_Sort_Fields_Pymnt_Nbr_Alpha", "PYMNT-NBR-ALPHA", FieldType.STRING, 
            10);
        pnd_Check_Sort_Fields_Pymnt_Nbr_AlphaRedef1 = pnd_Check_Sort_Fields.newGroupInGroup("pnd_Check_Sort_Fields_Pymnt_Nbr_AlphaRedef1", "Redefines", 
            pnd_Check_Sort_Fields_Pymnt_Nbr_Alpha);
        pnd_Check_Sort_Fields_Pymnt_Nbr = pnd_Check_Sort_Fields_Pymnt_Nbr_AlphaRedef1.newFieldInGroup("pnd_Check_Sort_Fields_Pymnt_Nbr", "PYMNT-NBR", 
            FieldType.NUMERIC, 10);
        filler01 = pnd_Check_Sort_Fields.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 25);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa803c(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

