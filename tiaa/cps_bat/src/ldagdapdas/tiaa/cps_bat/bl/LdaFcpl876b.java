/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:18 PM
**        * FROM NATURAL LDA     : FCPL876B
************************************************************
**        * FILE NAME            : LdaFcpl876b.java
**        * CLASS NAME           : LdaFcpl876b
**        * INSTANCE NAME        : LdaFcpl876b
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl876b extends DbsRecord
{
    // Properties
    private DbsField pnd_Fcpl876b;
    private DbsGroup pnd_Fcpl876bRedef1;
    private DbsField pnd_Fcpl876b_Pnd_Text;
    private DbsField pnd_Fcpl876b_Pnd_Addressee;
    private DbsField pnd_Fcpl876b_Pnd_Bar_Env_Id;
    private DbsField pnd_Fcpl876b_Cntrct_Orgn_Cde;
    private DbsField pnd_Fcpl876b_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Fcpl876b_Cntrct_Payee_Cde;
    private DbsField pnd_Fcpl876b_Cntrct_Hold_Cde;
    private DbsField pnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter;
    private DbsField pnd_Fcpl876b_Pnd_Pymnt_Check_Nbr;
    private DbsGroup pnd_Fcpl876b_Pnd_Pymnt_Check_NbrRedef2;
    private DbsField pnd_Fcpl876b_Pymnt_Check_Nbr;
    private DbsField pnd_Fcpl876b_Pnd_Bar_Envelopes;
    private DbsField pnd_Fcpl876b_Pnd_Bar_Pages_In_Env;
    private DbsField pnd_Fcpl876b_Pnd_Check;

    public DbsField getPnd_Fcpl876b() { return pnd_Fcpl876b; }

    public DbsGroup getPnd_Fcpl876bRedef1() { return pnd_Fcpl876bRedef1; }

    public DbsField getPnd_Fcpl876b_Pnd_Text() { return pnd_Fcpl876b_Pnd_Text; }

    public DbsField getPnd_Fcpl876b_Pnd_Addressee() { return pnd_Fcpl876b_Pnd_Addressee; }

    public DbsField getPnd_Fcpl876b_Pnd_Bar_Env_Id() { return pnd_Fcpl876b_Pnd_Bar_Env_Id; }

    public DbsField getPnd_Fcpl876b_Cntrct_Orgn_Cde() { return pnd_Fcpl876b_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Fcpl876b_Cntrct_Ppcn_Nbr() { return pnd_Fcpl876b_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Fcpl876b_Cntrct_Payee_Cde() { return pnd_Fcpl876b_Cntrct_Payee_Cde; }

    public DbsField getPnd_Fcpl876b_Cntrct_Hold_Cde() { return pnd_Fcpl876b_Cntrct_Hold_Cde; }

    public DbsField getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter() { return pnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter; }

    public DbsField getPnd_Fcpl876b_Pnd_Pymnt_Check_Nbr() { return pnd_Fcpl876b_Pnd_Pymnt_Check_Nbr; }

    public DbsGroup getPnd_Fcpl876b_Pnd_Pymnt_Check_NbrRedef2() { return pnd_Fcpl876b_Pnd_Pymnt_Check_NbrRedef2; }

    public DbsField getPnd_Fcpl876b_Pymnt_Check_Nbr() { return pnd_Fcpl876b_Pymnt_Check_Nbr; }

    public DbsField getPnd_Fcpl876b_Pnd_Bar_Envelopes() { return pnd_Fcpl876b_Pnd_Bar_Envelopes; }

    public DbsField getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env() { return pnd_Fcpl876b_Pnd_Bar_Pages_In_Env; }

    public DbsField getPnd_Fcpl876b_Pnd_Check() { return pnd_Fcpl876b_Pnd_Check; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl876b = newFieldInRecord("pnd_Fcpl876b", "#FCPL876B", FieldType.STRING, 143);
        pnd_Fcpl876bRedef1 = newGroupInRecord("pnd_Fcpl876bRedef1", "Redefines", pnd_Fcpl876b);
        pnd_Fcpl876b_Pnd_Text = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Pnd_Text", "#TEXT", FieldType.STRING, 11);
        pnd_Fcpl876b_Pnd_Addressee = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Pnd_Addressee", "#ADDRESSEE", FieldType.STRING, 38);
        pnd_Fcpl876b_Pnd_Bar_Env_Id = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Pnd_Bar_Env_Id", "#BAR-ENV-ID", FieldType.STRING, 7);
        pnd_Fcpl876b_Cntrct_Orgn_Cde = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Fcpl876b_Cntrct_Ppcn_Nbr = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Fcpl876b_Cntrct_Payee_Cde = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Fcpl876b_Cntrct_Hold_Cde = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        pnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter", "#BAR-ENV-INTEGRITY-COUNTER", 
            FieldType.NUMERIC, 1);
        pnd_Fcpl876b_Pnd_Pymnt_Check_Nbr = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Pnd_Pymnt_Check_Nbr", "#PYMNT-CHECK-NBR", FieldType.STRING, 
            7);
        pnd_Fcpl876b_Pnd_Pymnt_Check_NbrRedef2 = pnd_Fcpl876bRedef1.newGroupInGroup("pnd_Fcpl876b_Pnd_Pymnt_Check_NbrRedef2", "Redefines", pnd_Fcpl876b_Pnd_Pymnt_Check_Nbr);
        pnd_Fcpl876b_Pymnt_Check_Nbr = pnd_Fcpl876b_Pnd_Pymnt_Check_NbrRedef2.newFieldInGroup("pnd_Fcpl876b_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Fcpl876b_Pnd_Bar_Envelopes = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Pnd_Bar_Envelopes", "#BAR-ENVELOPES", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Fcpl876b_Pnd_Bar_Pages_In_Env = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Pnd_Bar_Pages_In_Env", "#BAR-PAGES-IN-ENV", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Fcpl876b_Pnd_Check = pnd_Fcpl876bRedef1.newFieldInGroup("pnd_Fcpl876b_Pnd_Check", "#CHECK", FieldType.BOOLEAN);

        this.setRecordName("LdaFcpl876b");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl876b.setInitialValue("!@#SUP01#@!");
    }

    // Constructor
    public LdaFcpl876b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
