/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:07 PM
**        * FROM NATURAL LDA     : FCPLSSC3
************************************************************
**        * FILE NAME            : LdaFcplssc3.java
**        * CLASS NAME           : LdaFcplssc3
**        * INSTANCE NAME        : LdaFcplssc3
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplssc3 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcplssc3;
    private DbsField pnd_Fcplssc3_Pnd_Cntl_Type;
    private DbsField pnd_Fcplssc3_Pnd_Pymnt_Type_Desc;

    public DbsGroup getPnd_Fcplssc3() { return pnd_Fcplssc3; }

    public DbsField getPnd_Fcplssc3_Pnd_Cntl_Type() { return pnd_Fcplssc3_Pnd_Cntl_Type; }

    public DbsField getPnd_Fcplssc3_Pnd_Pymnt_Type_Desc() { return pnd_Fcplssc3_Pnd_Pymnt_Type_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcplssc3 = newGroupInRecord("pnd_Fcplssc3", "#FCPLSSC3");
        pnd_Fcplssc3_Pnd_Cntl_Type = pnd_Fcplssc3.newFieldArrayInGroup("pnd_Fcplssc3_Pnd_Cntl_Type", "#CNTL-TYPE", FieldType.STRING, 41, new DbsArrayController(1,
            3));
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc = pnd_Fcplssc3.newFieldArrayInGroup("pnd_Fcplssc3_Pnd_Pymnt_Type_Desc", "#PYMNT-TYPE-DESC", FieldType.STRING, 
            25, new DbsArrayController(1,50));

        this.setRecordName("LdaFcplssc3");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcplssc3_Pnd_Cntl_Type.getValue(1).setInitialValue("I n p u t   F i l e   T o t a l s");
        pnd_Fcplssc3_Pnd_Cntl_Type.getValue(2).setInitialValue("C o n t r o l   R e c o r d   T o t a l s");
        pnd_Fcplssc3_Pnd_Cntl_Type.getValue(3).setInitialValue("D i f f e r e n c e");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(1).setInitialValue("Cashout   - SS");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(2).setInitialValue("Lump Sum  - SS");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(3).setInitialValue("Repurchase- SS");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(12).setInitialValue("Cancels   - SS");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(13).setInitialValue("Stops     - SS");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(14).setInitialValue("Cashout   - SS - Redraw");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(15).setInitialValue("Lump Sum  - SS - Redraw");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(16).setInitialValue("Repurchase- SS - Redraw");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(25).setInitialValue("Cancels   - DS");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(26).setInitialValue("Stops     - DS");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(27).setInitialValue("Cashout   - DS - Redraw");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(28).setInitialValue("Lump Sum  - DS - Redraw");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(29).setInitialValue("Repurchase- DS - Redraw");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(30).setInitialValue("Initial MDO");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(31).setInitialValue("Recurring MDO");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(32).setInitialValue("MDO Cancels");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(33).setInitialValue("MDO Stops");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(34).setInitialValue("MDO Redraw");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(45).setInitialValue("Total Suspense");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(46).setInitialValue("Total Cancels");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(47).setInitialValue("Total Stops");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(48).setInitialValue("Total Checks");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(49).setInitialValue("Total EFTs");
        pnd_Fcplssc3_Pnd_Pymnt_Type_Desc.getValue(50).setInitialValue("Total Rollovers");
    }

    // Constructor
    public LdaFcplssc3() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
