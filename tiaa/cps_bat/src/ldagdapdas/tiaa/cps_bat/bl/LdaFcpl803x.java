/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:16 PM
**        * FROM NATURAL LDA     : FCPL803X
************************************************************
**        * FILE NAME            : LdaFcpl803x.java
**        * CLASS NAME           : LdaFcpl803x
**        * INSTANCE NAME        : LdaFcpl803x
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl803x extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Xerox_Fields;
    private DbsField pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top;
    private DbsField pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top_Pa;
    private DbsField pnd_Xerox_Fields_Pnd_Xerox_Stmnts;
    private DbsField pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa;

    public DbsGroup getPnd_Xerox_Fields() { return pnd_Xerox_Fields; }

    public DbsField getPnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top() { return pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top; }

    public DbsField getPnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top_Pa() { return pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top_Pa; }

    public DbsField getPnd_Xerox_Fields_Pnd_Xerox_Stmnts() { return pnd_Xerox_Fields_Pnd_Xerox_Stmnts; }

    public DbsField getPnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa() { return pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Xerox_Fields = newGroupInRecord("pnd_Xerox_Fields", "#XEROX-FIELDS");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top = pnd_Xerox_Fields.newFieldInGroup("pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top", "#XEROX-STMNTS-TOP", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top_Pa = pnd_Xerox_Fields.newFieldInGroup("pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top_Pa", "#XEROX-STMNTS-TOP-PA", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts = pnd_Xerox_Fields.newFieldArrayInGroup("pnd_Xerox_Fields_Pnd_Xerox_Stmnts", "#XEROX-STMNTS", FieldType.STRING, 
            64, new DbsArrayController(1,33));
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa = pnd_Xerox_Fields.newFieldArrayInGroup("pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa", "#XEROX-STMNTS-PA", FieldType.STRING, 
            64, new DbsArrayController(1,21));

        this.setRecordName("LdaFcpl803x");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top.setInitialValue(63);
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Top_Pa.setInitialValue(64);
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(1).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=IAPERZ,FORMS=IAZERO,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(2).setInitialValue("JDE=CHKDUP,JDL=CHECK,FORMAT=IAPERZ,FORMS=IAZERO,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(3).setInitialValue("FORMAT=IAPERZ,FORMS=IAZERO,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(4).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=IAPERX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(5).setInitialValue("JDE=CHKDUP,JDL=CHECK,FORMAT=IAPERX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(7).setInitialValue("FORMAT=IAPERX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(8).setInitialValue("FORMAT=IAPERB,FORMS=IAMULT,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(9).setInitialValue("FORMAT=IAPERB,FORMS=IAMULT,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(10).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=IAMONZ,FORMS=IAZERO,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(11).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=IAMONZ,FORMS=IAZERO,FEED=SEP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(12).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=IAMONX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(13).setInitialValue("JDE=CHKDUP,JDL=CHECK,FORMAT=IAMONX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(14).setInitialValue("FORMAT=IAMONX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(15).setInitialValue("FORMAT=IAMONB,FORMS=IAMULT,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(16).setInitialValue("FORMAT=IAMONB,FORMS=IAMULT,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(17).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=IAMONA,FORMS=PRO3A,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(18).setInitialValue("JDE=CHKDUP,JDL=CHECK,FORMAT=IAMONA,FORMS=PRO3A,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(19).setInitialValue("FORMAT=IAMONA,FORMS=PRO3A,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(20).setInitialValue("JDE=CHKDUP,JDL=CHECK,FORMAT=IAMONZ,FORMS=IAZERO,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(21).setInitialValue("FORMAT=IAMONZ,FORMS=IAZERO,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(22).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=SSLETX,FORMS=MCMLET,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(23).setInitialValue("FORMAT=SSLETX,FORMS=MCMLET,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(24).setInitialValue("JDE=STMSIM,JDL=LOANJL,FORMAT=LNCHKX,FORM=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(25).setInitialValue("JDE=STMDUP,JDL=LOANJL,FORMAT=LNCHKX,FORM=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(26).setInitialValue("FORMAT=LNCHKX,FORM=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(27).setInitialValue("FORMAT=LNCHK,FORM=LNMULT,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(28).setInitialValue("FORMAT=LNCHK,FORM=LNMULT,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(29).setInitialValue("JDE=STMSIM,JDL=LOANJL,FORMAT=LNALT,FORM=LNWOC,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(30).setInitialValue("JDE=STMDUP,JDL=LOANJL,FORMAT=LNCHK,FORM=LNWOC,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(31).setInitialValue("FORMAT=LNALT,FORM=LNWOC,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(32).setInitialValue("JDE=STMSIM,JDL=LOANJL,FORMAT=LNCHKX,FORM=LNWOC,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts.getValue(33).setInitialValue("JDE=STMSIM,JDL=LOANJL,FORMAT=LNCHK,FORM=LNWOC,FEED=SEP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(1).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=PASPEZ,FORMS=PASZER,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(2).setInitialValue("JDE=CHKDUP,JDL=CHECK,FORMAT=PASPEZ,FORMS=PASZER,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(3).setInitialValue("FORMAT=PASPEZ,FORMS=PASZER,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(4).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=IAPERX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(5).setInitialValue("JDE=CHKDUP,JDL=CHECK,FORMAT=IAPERX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(7).setInitialValue("FORMAT=IAPERX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(8).setInitialValue("FORMAT=PASPEB,FORMS=PASMUL,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(9).setInitialValue("FORMAT=PASPEB,FORMS=PASMUL,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(10).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=PASMOZ,FORMS=PASZER,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(11).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=PASMOZ,FORMS=PASZER,FEED=SEP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(12).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=IAMONX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(13).setInitialValue("JDE=CHKDUP,JDL=CHECK,FORMAT=IAMONX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(14).setInitialValue("FORMAT=IAMONX,FORMS=MCMWIC,FEED=CHECKP,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(15).setInitialValue("FORMAT=PASMOB,FORMS=PASMUL,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(16).setInitialValue("FORMAT=PASMOB,FORMS=PASMUL,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(17).setInitialValue("JDE=CHKSIM,JDL=CHECK,FORMAT=PASMOA,FORMS=PASALT,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(18).setInitialValue("JDE=CHKDUP,JDL=CHECK,FORMAT=PASMOA,FORMS=PASALT,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(19).setInitialValue("FORMAT=PASMOA,FORMS=PASALT,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(20).setInitialValue("JDE=CHKDUP,JDL=CHECK,FORMAT=PASMOZ,FORMS=PASZER,FEED=BOND,END;");
        pnd_Xerox_Fields_Pnd_Xerox_Stmnts_Pa.getValue(21).setInitialValue("FORMAT=PASMOZ,FORMS=PASZER,FEED=BOND,END;");
    }

    // Constructor
    public LdaFcpl803x() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
