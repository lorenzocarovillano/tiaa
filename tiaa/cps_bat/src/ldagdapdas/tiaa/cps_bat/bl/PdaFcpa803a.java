/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:27 PM
**        * FROM NATURAL PDA     : FCPA803A
************************************************************
**        * FILE NAME            : PdaFcpa803a.java
**        * CLASS NAME           : PdaFcpa803a
**        * INSTANCE NAME        : PdaFcpa803a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa803a extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpa803a;
    private DbsGroup pnd_Fcpa803a_Pymnt_Nme_And_Addr_Grp;
    private DbsField pnd_Fcpa803a_Pymnt_Nme;
    private DbsField pnd_Fcpa803a_Pymnt_Addr_Line_Txt;
    private DbsField pnd_Fcpa803a_Pymnt_Postl_Data;
    private DbsField pnd_Fcpa803a_Pnd_Stmnt_Pymnt_Nme;

    public DbsGroup getPnd_Fcpa803a() { return pnd_Fcpa803a; }

    public DbsGroup getPnd_Fcpa803a_Pymnt_Nme_And_Addr_Grp() { return pnd_Fcpa803a_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getPnd_Fcpa803a_Pymnt_Nme() { return pnd_Fcpa803a_Pymnt_Nme; }

    public DbsField getPnd_Fcpa803a_Pymnt_Addr_Line_Txt() { return pnd_Fcpa803a_Pymnt_Addr_Line_Txt; }

    public DbsField getPnd_Fcpa803a_Pymnt_Postl_Data() { return pnd_Fcpa803a_Pymnt_Postl_Data; }

    public DbsField getPnd_Fcpa803a_Pnd_Stmnt_Pymnt_Nme() { return pnd_Fcpa803a_Pnd_Stmnt_Pymnt_Nme; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpa803a = dbsRecord.newGroupInRecord("pnd_Fcpa803a", "#FCPA803A");
        pnd_Fcpa803a.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpa803a_Pymnt_Nme_And_Addr_Grp = pnd_Fcpa803a.newGroupInGroup("pnd_Fcpa803a_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP");
        pnd_Fcpa803a_Pymnt_Nme = pnd_Fcpa803a_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Fcpa803a_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38);
        pnd_Fcpa803a_Pymnt_Addr_Line_Txt = pnd_Fcpa803a_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("pnd_Fcpa803a_Pymnt_Addr_Line_Txt", "PYMNT-ADDR-LINE-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1,6));
        pnd_Fcpa803a_Pymnt_Postl_Data = pnd_Fcpa803a_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Fcpa803a_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 
            32);
        pnd_Fcpa803a_Pnd_Stmnt_Pymnt_Nme = pnd_Fcpa803a.newFieldInGroup("pnd_Fcpa803a_Pnd_Stmnt_Pymnt_Nme", "#STMNT-PYMNT-NME", FieldType.STRING, 38);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa803a(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

