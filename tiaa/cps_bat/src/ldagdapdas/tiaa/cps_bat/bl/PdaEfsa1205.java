/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:19:49 PM
**        * FROM NATURAL PDA     : EFSA1205
************************************************************
**        * FILE NAME            : PdaEfsa1205.java
**        * CLASS NAME           : PdaEfsa1205
**        * INSTANCE NAME        : PdaEfsa1205
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaEfsa1205 extends PdaBase
{
    // Properties
    private DbsField pnd_Appc_Parm;
    private DbsGroup pnd_Appc_ParmRedef1;
    private DbsField pnd_Appc_Parm_Header;
    private DbsGroup pnd_Appc_Parm_HeaderRedef2;
    private DbsField pnd_Appc_Parm_Length_Msg;
    private DbsField pnd_Appc_Parm_Type_Msg;
    private DbsField pnd_Appc_Parm_Version_Msg;
    private DbsField pnd_Appc_Parm_Name_Prog;
    private DbsField pnd_Appc_Parm_Failing_Component;
    private DbsField pnd_Appc_Parm_Failing_Request;
    private DbsField pnd_Appc_Parm_Failing_Date;
    private DbsField pnd_Appc_Parm_Failing_Time;
    private DbsField pnd_Appc_Parm_Loopback_Probe;
    private DbsField pnd_Appc_Parm_Version_Format;
    private DbsField pnd_Appc_Parm_Format_Inout;
    private DbsField pnd_Appc_Parm_Rc_Csappc;
    private DbsField pnd_Appc_Parm_Rc_Failing_Component;
    private DbsField pnd_Appc_Parm_Rc_Appl_Pgm;
    private DbsField pnd_Appc_Parm_Role_Msg;
    private DbsField pnd_Appc_Parm_Pnd_Pgm_Name;
    private DbsField pnd_Appc_Parm_Pnd_User_Sent_Data;
    private DbsGroup pnd_Appc_Parm_Pnd_User_Sent_DataRedef3;
    private DbsField pnd_Appc_Parm_Pnd_Rqst_Log_Dte_Tme;
    private DbsField pnd_Appc_Parm_Pnd_Msg_Entry_Dte_Tme;
    private DbsField pnd_Appc_Parm_Pnd_Msg_Tiaa_Cntrct;
    private DbsField pnd_Appc_Parm_Pnd_Msg_Doc_Type;
    private DbsField pnd_Appc_Parm_Pnd_Return_Parm1;
    private DbsGroup pnd_Appc_Parm_Pnd_Return_Parm1Redef4;
    private DbsField pnd_Appc_Parm_Cms_Rtrn_Cd;
    private DbsField pnd_Appc_Parm_Error_Type;
    private DbsField pnd_Appc_Parm_Batch_Nbr;
    private DbsField pnd_Appc_Parm_Application;
    private DbsField pnd_Appc_Parm_Program;
    private DbsField pnd_Appc_Parm_Error_Text_1;
    private DbsField pnd_Appc_Parm_Error_Text_2;
    private DbsGroup pnd_Appc_Parm_Pnd_Pdqa652;
    private DbsGroup pnd_Appc_Parm_Record;
    private DbsGroup pnd_Appc_Parm_Pnd_Record1;
    private DbsField pnd_Appc_Parm_Pnd_Pin;
    private DbsField pnd_Appc_Parm_Pnd_Tiaa_Cont;
    private DbsField pnd_Appc_Parm_Pnd_Cref_Cert;
    private DbsField pnd_Appc_Parm_Pnd_Int_Roll_Cert;
    private DbsField pnd_Appc_Parm_Pnd_Int_Roll_Cont;
    private DbsField pnd_Appc_Parm_Pnd_Effective_Date;
    private DbsField pnd_Appc_Parm_Pnd_Approval_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Rqst_Approval2_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Complete_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Auth_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Auth2_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Oia_Ind;
    private DbsGroup pnd_Appc_Parm_Pnd_Record2;
    private DbsField pnd_Appc_Parm_Pnd_Ppg_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Ppg_Cnt;
    private DbsField pnd_Appc_Parm_Pnd_Ppgs;
    private DbsField pnd_Appc_Parm_Pnd_Buckets_Cnt;
    private DbsField pnd_Appc_Parm_Pnd_Buckets;
    private DbsField pnd_Appc_Parm_Pnd_Nonprem_Complete;
    private DbsField pnd_Appc_Parm_Pnd_Npde_Ledger;
    private DbsField pnd_Appc_Parm_Pnd_Adar_Complete;
    private DbsGroup pnd_Appc_Parm_Pnd_Record3;
    private DbsGroup pnd_Appc_Parm_Pnd_Actrl_Accumulation;
    private DbsField pnd_Appc_Parm_Pnd_Fund_Wthdrl_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Fund_Payee1_Accum;
    private DbsField pnd_Appc_Parm_Pnd_Fund_Payee2_Accum;
    private DbsField pnd_Appc_Parm_Pnd_Accum_Before_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Accum_After_Amt;
    private DbsGroup pnd_Appc_Parm_Pnd_Record4;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Ssn;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Dob;
    private DbsGroup pnd_Appc_Parm_Pnd_Ph_DobRedef5;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Dob_A;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Name;
    private DbsGroup pnd_Appc_Parm_Pnd_Ph_NameRedef6;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Prefix_Name;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Last_Name;
    private DbsField pnd_Appc_Parm_Pnd_Ph_First_Name;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Middle_Name;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Suffix_Name;
    private DbsGroup pnd_Appc_Parm_Pnd_Record5;
    private DbsField pnd_Appc_Parm_Pnd_Payment_Type;
    private DbsGroup pnd_Appc_Parm_Pnd_Outst_Loan;
    private DbsField pnd_Appc_Parm_Pnd_Outst_Loan_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Min_Dist_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Loan_Num;
    private DbsField pnd_Appc_Parm_Pnd_Loan_Princ_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Loan_Intrst_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Late_Intrst_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Total_Dflt_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Funds_Dflt_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Tax_Election_Ind_Ss;
    private DbsField pnd_Appc_Parm_Pnd_Citizenship_Ss;
    private DbsField pnd_Appc_Parm_Pnd_Residency_Ss;
    private DbsField pnd_Appc_Parm_Pnd_Ra_Nra_Ss;
    private DbsField pnd_Appc_Parm_Pnd_Tax_Payee_Code;
    private DbsField pnd_Appc_Parm_Pnd_Tax_Date_W8_W9;
    private DbsGroup pnd_Appc_Parm_Pnd_Tax_Date_W8_W9Redef7;
    private DbsField pnd_Appc_Parm_Pnd_Tax_Date_W8_W9_A;
    private DbsField pnd_Appc_Parm_Pnd_Check_Tracer;
    private DbsField pnd_Appc_Parm_Pnd_Check_Date;
    private DbsGroup pnd_Appc_Parm_Pnd_Check_DateRedef8;
    private DbsField pnd_Appc_Parm_Pnd_Check_Date_A;
    private DbsField pnd_Appc_Parm_Pnd_Check_Amount;
    private DbsField pnd_Appc_Parm_Pnd_Issue_Or_Settle_Date;
    private DbsGroup pnd_Appc_Parm_Pnd_Issue_Or_Settle_DateRedef9;
    private DbsField pnd_Appc_Parm_Pnd_Issue_Or_Settle_Date_A;
    private DbsField pnd_Appc_Parm_Pnd_Transfer_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Dist_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Non_Dist_Amt;
    private DbsGroup pnd_Appc_Parm_Pnd_Record6;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Name1;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Name2;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Addr1;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Addr2;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Addr3;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Addr4;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Addr5;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Addr6;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_City;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_State;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Zip;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Aba_Num;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Acct_Num;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Acct_Type;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Rollover_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Ovrpy_Rcvry_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Hold_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Type;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Foreign_Code;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Method;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Hold_Code;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Net_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Ivc_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Installment_Date;
    private DbsGroup pnd_Appc_Parm_Pnd_Payee1_Installment_DateRedef10;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Installment_Date_A;
    private DbsGroup pnd_Appc_Parm_Pnd_Payee1_Rate_Info;
    private DbsField pnd_Appc_Parm_Pnd_Rate1_Code;
    private DbsField pnd_Appc_Parm_Pnd_Rate1_Settle_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Rate1_Tiaa_Total_Amt;
    private DbsGroup pnd_Appc_Parm_Pnd_Record7;
    private DbsField pnd_Appc_Parm_Pnd_Ledger1_Cnt;
    private DbsGroup pnd_Appc_Parm_Pnd_Ledger1_Table;
    private DbsField pnd_Appc_Parm_Pnd_Ledger1_Company;
    private DbsField pnd_Appc_Parm_Pnd_Ledger1_Acct;
    private DbsField pnd_Appc_Parm_Pnd_Ledger1_Amount;
    private DbsField pnd_Appc_Parm_Pnd_Ledger1_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Ledger1_Cont;
    private DbsField pnd_Appc_Parm_Pnd_Ledger1_Eff_Date;
    private DbsField pnd_Appc_Parm_Pnd_Ledger1_Cost_Ctr;
    private DbsGroup pnd_Appc_Parm_Pnd_Record8;
    private DbsGroup pnd_Appc_Parm_Pnd_Check1_Table;
    private DbsField pnd_Appc_Parm_Pnd_Tot1_Gross_Prcds;
    private DbsField pnd_Appc_Parm_Pnd_Check1_Ivc;
    private DbsField pnd_Appc_Parm_Pnd_Dpi1_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Int1_Ext_Ro_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Fed1_Tax_Wthhld;
    private DbsField pnd_Appc_Parm_Pnd_Us1_Nra_Tax_Wthhld;
    private DbsField pnd_Appc_Parm_Pnd_St1_Tax_Wthhld;
    private DbsField pnd_Appc_Parm_Pnd_Loc1_Tax_Wthhld;
    private DbsField pnd_Appc_Parm_Pnd_Net1_Prcds;
    private DbsField pnd_Appc_Parm_Pnd_Check1_Wthdrl_Chg;
    private DbsGroup pnd_Appc_Parm_Pnd_Record9;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Name1;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Name2;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Addr1;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Addr2;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Addr3;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Addr4;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Addr5;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Addr6;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_City;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_State;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Zip;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Aba_Num;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Acct_Num;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Acct_Type;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Rollover_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Ovrpy_Rcvry_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Hold_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Type;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Foreign_Code;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Method;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Hold_Code;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Net_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Ivc_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Installment_Date;
    private DbsGroup pnd_Appc_Parm_Pnd_Payee2_Installment_DateRedef11;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Installment_Date_A;
    private DbsGroup pnd_Appc_Parm_Pnd_Payee2_Rate_Info;
    private DbsField pnd_Appc_Parm_Pnd_Rate2_Code;
    private DbsField pnd_Appc_Parm_Pnd_Rate2_Settle_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Rate2_Tiaa_Total_Amt;
    private DbsGroup pnd_Appc_Parm_Pnd_Record10;
    private DbsField pnd_Appc_Parm_Pnd_Ledger2_Cnt;
    private DbsGroup pnd_Appc_Parm_Pnd_Ledger2_Table;
    private DbsField pnd_Appc_Parm_Pnd_Ledger2_Company;
    private DbsField pnd_Appc_Parm_Pnd_Ledger2_Acct;
    private DbsField pnd_Appc_Parm_Pnd_Ledger2_Amount;
    private DbsField pnd_Appc_Parm_Pnd_Ledger2_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Ledger2_Cont;
    private DbsField pnd_Appc_Parm_Pnd_Ledger2_Eff_Date;
    private DbsField pnd_Appc_Parm_Pnd_Ledger2_Cost_Ctr;
    private DbsGroup pnd_Appc_Parm_Pnd_Record11;
    private DbsGroup pnd_Appc_Parm_Pnd_Check2_Table;
    private DbsField pnd_Appc_Parm_Pnd_Tot2_Gross_Prcds;
    private DbsField pnd_Appc_Parm_Pnd_Check2_Ivc;
    private DbsField pnd_Appc_Parm_Pnd_Dpi2_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Int2_Ext_Ro_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Fed2_Tax_Wthhld;
    private DbsField pnd_Appc_Parm_Pnd_Us2_Nra_Tax_Wthhld;
    private DbsField pnd_Appc_Parm_Pnd_St2_Tax_Wthhld;
    private DbsField pnd_Appc_Parm_Pnd_Loc2_Tax_Wthhld;
    private DbsField pnd_Appc_Parm_Pnd_Net2_Prcds;
    private DbsField pnd_Appc_Parm_Pnd_Check2_Wthdrl_Chg;
    private DbsGroup pnd_Appc_Parm_Pnd_Record12;
    private DbsGroup pnd_Appc_Parm_Pnd_Tot_Table;
    private DbsField pnd_Appc_Parm_Pnd_Tot_Gross;
    private DbsField pnd_Appc_Parm_Pnd_Tot_Net;
    private DbsField pnd_Appc_Parm_Pnd_Tot_Fedtax;
    private DbsField pnd_Appc_Parm_Pnd_Tot_Statax;
    private DbsGroup pnd_Appc_Parm_Pnd_Record13;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Addr1;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Addr2;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Addr3;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Addr4;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Addr5;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Addr6;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Zip;
    private DbsField pnd_Appc_Parm_Pnd_Ph_State;
    private DbsGroup pnd_Appc_Parm_Pnd_Record14;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Dod;
    private DbsGroup pnd_Appc_Parm_Pnd_Ph_DodRedef12;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Dod_A;
    private DbsField pnd_Appc_Parm_Pnd_Wpid;
    private DbsField pnd_Appc_Parm_Pnd_Payee1_Manu_Check;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Manu_Check;
    private DbsField pnd_Appc_Parm_Pnd_Tax_W8_Ben;
    private DbsField pnd_Appc_Parm_Pnd_Tax_W9;
    private DbsField pnd_Appc_Parm_Pnd_Tax_Year;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Roll_Cert;
    private DbsField pnd_Appc_Parm_Pnd_Payee2_Roll_Cont;
    private DbsField pnd_Appc_Parm_Pnd_Ss_Tax_Id_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Taxable_Name;
    private DbsField pnd_Appc_Parm_Pnd_Payment_Desc;
    private DbsField pnd_Appc_Parm_Pnd_Payment_Tfslash_N;
    private DbsField pnd_Appc_Parm_Pnd_Payment_Pfslash_N;
    private DbsField pnd_Appc_Parm_Pnd_Payment_Lfslash_N;
    private DbsField pnd_Appc_Parm_Pnd_Payment_T_Code;
    private DbsField pnd_Appc_Parm_Pnd_Payment_Nt;
    private DbsField pnd_Appc_Parm_Pnd_Tax_Id_Number;
    private DbsField pnd_Appc_Parm_Pnd_Tax_Cont;
    private DbsGroup pnd_Appc_Parm_Pnd_Record15;
    private DbsField pnd_Appc_Parm_Pnd_Court_Date;
    private DbsGroup pnd_Appc_Parm_Pnd_Court_DateRedef13;
    private DbsField pnd_Appc_Parm_Pnd_Court_Date_A;
    private DbsField pnd_Appc_Parm_Pnd_Dvsn_Date;
    private DbsGroup pnd_Appc_Parm_Pnd_Dvsn_DateRedef14;
    private DbsField pnd_Appc_Parm_Pnd_Dvsn_Date_A;
    private DbsField pnd_Appc_Parm_Pnd_Cont_From_Date;
    private DbsGroup pnd_Appc_Parm_Pnd_Cont_From_DateRedef15;
    private DbsField pnd_Appc_Parm_Pnd_Cont_From_Date_A;
    private DbsField pnd_Appc_Parm_Pnd_Cont_To_Date;
    private DbsGroup pnd_Appc_Parm_Pnd_Cont_To_DateRedef16;
    private DbsField pnd_Appc_Parm_Pnd_Cont_To_Date_A;
    private DbsField pnd_Appc_Parm_Pnd_Cont_Accum_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Cont_Accum_Pct;
    private DbsField pnd_Appc_Parm_Pnd_Cert_From_Date;
    private DbsGroup pnd_Appc_Parm_Pnd_Cert_From_DateRedef17;
    private DbsField pnd_Appc_Parm_Pnd_Cert_From_Date_A;
    private DbsField pnd_Appc_Parm_Pnd_Cert_To_Date;
    private DbsGroup pnd_Appc_Parm_Pnd_Cert_To_DateRedef18;
    private DbsField pnd_Appc_Parm_Pnd_Cert_To_Date_A;
    private DbsField pnd_Appc_Parm_Pnd_Cert_Accum_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Cert_Accum_Pct;
    private DbsField pnd_Appc_Parm_Pnd_Spouse_Cont;
    private DbsField pnd_Appc_Parm_Pnd_Spouse_Cert;
    private DbsGroup pnd_Appc_Parm_Pnd_Record16;
    private DbsGroup pnd_Appc_Parm_Pnd_Prem_Info;
    private DbsField pnd_Appc_Parm_Pnd_Prem_By_Bucket;
    private DbsField pnd_Appc_Parm_Pnd_Prem_By_Fund;
    private DbsField pnd_Appc_Parm_Pnd_Prem_Totals;
    private DbsField pnd_Appc_Parm_Pnd_Prem_Ftotal;
    private DbsField pnd_Appc_Parm_Pnd_Prem_Btotal;
    private DbsGroup pnd_Appc_Parm_Pnd_Record17;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Ben_Dob;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Ben_Name;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Ben_Sex;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Ben_Spouse_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Ben_Ssn;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Calc_Method;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cert;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cont;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Mdo_First_Pmnt_Date;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Mdo_Mode;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Mode_Change;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Payment_Desc;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Payment_Option;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Qualified;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Req_Beg_Date;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Sa_Spouse_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cert;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cont;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Dob_Date;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Name;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Sex;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Ssn;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Trb_Ind;
    private DbsField pnd_Appc_Parm_Pnd_Settle_Trb_Pct;
    private DbsGroup pnd_Appc_Parm_Pnd_Record18;
    private DbsField pnd_Appc_Parm_Pnd_Lob;
    private DbsGroup pnd_Appc_Parm_Pnd_Fund_Table;
    private DbsField pnd_Appc_Parm_Pnd_Ticker_Symbol;
    private DbsField pnd_Appc_Parm_Pnd_Fund_Amt;
    private DbsField pnd_Appc_Parm_Pnd_Fund_Pct;
    private DbsField pnd_Appc_Parm_Pnd_Fund_Units;
    private DbsGroup pnd_Appc_Parm_Pnd_Record19;
    private DbsField pnd_Appc_Parm_Pnd_Roth_Contrib_Yyyy;
    private DbsField pnd_Appc_Parm_Pnd_Roth_Qualified;
    private DbsField pnd_Appc_Parm_Pnd_Dod_Dte;
    private DbsField pnd_Appc_Parm_Pnd_Dob_Dte;
    private DbsField pnd_Appc_Parm_Pnd_Disabl_Dte;
    private DbsGroup pnd_Appc_Parm_Pnd_Record20;
    private DbsField pnd_Appc_Parm_Pnd_Ph_Residence;
    private DbsField pnd_Appc_Parm_Pnd_Sucu_Plan_Nbr;
    private DbsField pnd_Appc_Parm_Pnd_Sucu_Subplan_Nbr;
    private DbsField pnd_Appc_Parm_Pnd_Sucu_Orig_Cntr_Nbr;
    private DbsField pnd_Appc_Parm_Pnd_Sucu_Orig_Subplan_Nbr;
    private DbsGroup pnd_Appc_Parm_Pnd_Record20_End;
    private DbsField pnd_Appc_Parm_Pnd_End_Of_Record;

    public DbsField getPnd_Appc_Parm() { return pnd_Appc_Parm; }

    public DbsGroup getPnd_Appc_ParmRedef1() { return pnd_Appc_ParmRedef1; }

    public DbsField getPnd_Appc_Parm_Header() { return pnd_Appc_Parm_Header; }

    public DbsGroup getPnd_Appc_Parm_HeaderRedef2() { return pnd_Appc_Parm_HeaderRedef2; }

    public DbsField getPnd_Appc_Parm_Length_Msg() { return pnd_Appc_Parm_Length_Msg; }

    public DbsField getPnd_Appc_Parm_Type_Msg() { return pnd_Appc_Parm_Type_Msg; }

    public DbsField getPnd_Appc_Parm_Version_Msg() { return pnd_Appc_Parm_Version_Msg; }

    public DbsField getPnd_Appc_Parm_Name_Prog() { return pnd_Appc_Parm_Name_Prog; }

    public DbsField getPnd_Appc_Parm_Failing_Component() { return pnd_Appc_Parm_Failing_Component; }

    public DbsField getPnd_Appc_Parm_Failing_Request() { return pnd_Appc_Parm_Failing_Request; }

    public DbsField getPnd_Appc_Parm_Failing_Date() { return pnd_Appc_Parm_Failing_Date; }

    public DbsField getPnd_Appc_Parm_Failing_Time() { return pnd_Appc_Parm_Failing_Time; }

    public DbsField getPnd_Appc_Parm_Loopback_Probe() { return pnd_Appc_Parm_Loopback_Probe; }

    public DbsField getPnd_Appc_Parm_Version_Format() { return pnd_Appc_Parm_Version_Format; }

    public DbsField getPnd_Appc_Parm_Format_Inout() { return pnd_Appc_Parm_Format_Inout; }

    public DbsField getPnd_Appc_Parm_Rc_Csappc() { return pnd_Appc_Parm_Rc_Csappc; }

    public DbsField getPnd_Appc_Parm_Rc_Failing_Component() { return pnd_Appc_Parm_Rc_Failing_Component; }

    public DbsField getPnd_Appc_Parm_Rc_Appl_Pgm() { return pnd_Appc_Parm_Rc_Appl_Pgm; }

    public DbsField getPnd_Appc_Parm_Role_Msg() { return pnd_Appc_Parm_Role_Msg; }

    public DbsField getPnd_Appc_Parm_Pnd_Pgm_Name() { return pnd_Appc_Parm_Pnd_Pgm_Name; }

    public DbsField getPnd_Appc_Parm_Pnd_User_Sent_Data() { return pnd_Appc_Parm_Pnd_User_Sent_Data; }

    public DbsGroup getPnd_Appc_Parm_Pnd_User_Sent_DataRedef3() { return pnd_Appc_Parm_Pnd_User_Sent_DataRedef3; }

    public DbsField getPnd_Appc_Parm_Pnd_Rqst_Log_Dte_Tme() { return pnd_Appc_Parm_Pnd_Rqst_Log_Dte_Tme; }

    public DbsField getPnd_Appc_Parm_Pnd_Msg_Entry_Dte_Tme() { return pnd_Appc_Parm_Pnd_Msg_Entry_Dte_Tme; }

    public DbsField getPnd_Appc_Parm_Pnd_Msg_Tiaa_Cntrct() { return pnd_Appc_Parm_Pnd_Msg_Tiaa_Cntrct; }

    public DbsField getPnd_Appc_Parm_Pnd_Msg_Doc_Type() { return pnd_Appc_Parm_Pnd_Msg_Doc_Type; }

    public DbsField getPnd_Appc_Parm_Pnd_Return_Parm1() { return pnd_Appc_Parm_Pnd_Return_Parm1; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Return_Parm1Redef4() { return pnd_Appc_Parm_Pnd_Return_Parm1Redef4; }

    public DbsField getPnd_Appc_Parm_Cms_Rtrn_Cd() { return pnd_Appc_Parm_Cms_Rtrn_Cd; }

    public DbsField getPnd_Appc_Parm_Error_Type() { return pnd_Appc_Parm_Error_Type; }

    public DbsField getPnd_Appc_Parm_Batch_Nbr() { return pnd_Appc_Parm_Batch_Nbr; }

    public DbsField getPnd_Appc_Parm_Application() { return pnd_Appc_Parm_Application; }

    public DbsField getPnd_Appc_Parm_Program() { return pnd_Appc_Parm_Program; }

    public DbsField getPnd_Appc_Parm_Error_Text_1() { return pnd_Appc_Parm_Error_Text_1; }

    public DbsField getPnd_Appc_Parm_Error_Text_2() { return pnd_Appc_Parm_Error_Text_2; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Pdqa652() { return pnd_Appc_Parm_Pnd_Pdqa652; }

    public DbsGroup getPnd_Appc_Parm_Record() { return pnd_Appc_Parm_Record; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record1() { return pnd_Appc_Parm_Pnd_Record1; }

    public DbsField getPnd_Appc_Parm_Pnd_Pin() { return pnd_Appc_Parm_Pnd_Pin; }

    public DbsField getPnd_Appc_Parm_Pnd_Tiaa_Cont() { return pnd_Appc_Parm_Pnd_Tiaa_Cont; }

    public DbsField getPnd_Appc_Parm_Pnd_Cref_Cert() { return pnd_Appc_Parm_Pnd_Cref_Cert; }

    public DbsField getPnd_Appc_Parm_Pnd_Int_Roll_Cert() { return pnd_Appc_Parm_Pnd_Int_Roll_Cert; }

    public DbsField getPnd_Appc_Parm_Pnd_Int_Roll_Cont() { return pnd_Appc_Parm_Pnd_Int_Roll_Cont; }

    public DbsField getPnd_Appc_Parm_Pnd_Effective_Date() { return pnd_Appc_Parm_Pnd_Effective_Date; }

    public DbsField getPnd_Appc_Parm_Pnd_Approval_Ind() { return pnd_Appc_Parm_Pnd_Approval_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Rqst_Approval2_Ind() { return pnd_Appc_Parm_Pnd_Rqst_Approval2_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Complete_Ind() { return pnd_Appc_Parm_Pnd_Complete_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Auth_Ind() { return pnd_Appc_Parm_Pnd_Auth_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Auth2_Ind() { return pnd_Appc_Parm_Pnd_Auth2_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Oia_Ind() { return pnd_Appc_Parm_Pnd_Oia_Ind; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record2() { return pnd_Appc_Parm_Pnd_Record2; }

    public DbsField getPnd_Appc_Parm_Pnd_Ppg_Ind() { return pnd_Appc_Parm_Pnd_Ppg_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Ppg_Cnt() { return pnd_Appc_Parm_Pnd_Ppg_Cnt; }

    public DbsField getPnd_Appc_Parm_Pnd_Ppgs() { return pnd_Appc_Parm_Pnd_Ppgs; }

    public DbsField getPnd_Appc_Parm_Pnd_Buckets_Cnt() { return pnd_Appc_Parm_Pnd_Buckets_Cnt; }

    public DbsField getPnd_Appc_Parm_Pnd_Buckets() { return pnd_Appc_Parm_Pnd_Buckets; }

    public DbsField getPnd_Appc_Parm_Pnd_Nonprem_Complete() { return pnd_Appc_Parm_Pnd_Nonprem_Complete; }

    public DbsField getPnd_Appc_Parm_Pnd_Npde_Ledger() { return pnd_Appc_Parm_Pnd_Npde_Ledger; }

    public DbsField getPnd_Appc_Parm_Pnd_Adar_Complete() { return pnd_Appc_Parm_Pnd_Adar_Complete; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record3() { return pnd_Appc_Parm_Pnd_Record3; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Actrl_Accumulation() { return pnd_Appc_Parm_Pnd_Actrl_Accumulation; }

    public DbsField getPnd_Appc_Parm_Pnd_Fund_Wthdrl_Amt() { return pnd_Appc_Parm_Pnd_Fund_Wthdrl_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Fund_Payee1_Accum() { return pnd_Appc_Parm_Pnd_Fund_Payee1_Accum; }

    public DbsField getPnd_Appc_Parm_Pnd_Fund_Payee2_Accum() { return pnd_Appc_Parm_Pnd_Fund_Payee2_Accum; }

    public DbsField getPnd_Appc_Parm_Pnd_Accum_Before_Amt() { return pnd_Appc_Parm_Pnd_Accum_Before_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Accum_After_Amt() { return pnd_Appc_Parm_Pnd_Accum_After_Amt; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record4() { return pnd_Appc_Parm_Pnd_Record4; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Ssn() { return pnd_Appc_Parm_Pnd_Ph_Ssn; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Dob() { return pnd_Appc_Parm_Pnd_Ph_Dob; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Ph_DobRedef5() { return pnd_Appc_Parm_Pnd_Ph_DobRedef5; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Dob_A() { return pnd_Appc_Parm_Pnd_Ph_Dob_A; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Name() { return pnd_Appc_Parm_Pnd_Ph_Name; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Ph_NameRedef6() { return pnd_Appc_Parm_Pnd_Ph_NameRedef6; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Prefix_Name() { return pnd_Appc_Parm_Pnd_Ph_Prefix_Name; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Last_Name() { return pnd_Appc_Parm_Pnd_Ph_Last_Name; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_First_Name() { return pnd_Appc_Parm_Pnd_Ph_First_Name; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Middle_Name() { return pnd_Appc_Parm_Pnd_Ph_Middle_Name; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Suffix_Name() { return pnd_Appc_Parm_Pnd_Ph_Suffix_Name; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record5() { return pnd_Appc_Parm_Pnd_Record5; }

    public DbsField getPnd_Appc_Parm_Pnd_Payment_Type() { return pnd_Appc_Parm_Pnd_Payment_Type; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Outst_Loan() { return pnd_Appc_Parm_Pnd_Outst_Loan; }

    public DbsField getPnd_Appc_Parm_Pnd_Outst_Loan_Amt() { return pnd_Appc_Parm_Pnd_Outst_Loan_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Min_Dist_Ind() { return pnd_Appc_Parm_Pnd_Min_Dist_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Loan_Num() { return pnd_Appc_Parm_Pnd_Loan_Num; }

    public DbsField getPnd_Appc_Parm_Pnd_Loan_Princ_Amt() { return pnd_Appc_Parm_Pnd_Loan_Princ_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Loan_Intrst_Amt() { return pnd_Appc_Parm_Pnd_Loan_Intrst_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Late_Intrst_Amt() { return pnd_Appc_Parm_Pnd_Late_Intrst_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Total_Dflt_Amt() { return pnd_Appc_Parm_Pnd_Total_Dflt_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Funds_Dflt_Ind() { return pnd_Appc_Parm_Pnd_Funds_Dflt_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Tax_Election_Ind_Ss() { return pnd_Appc_Parm_Pnd_Tax_Election_Ind_Ss; }

    public DbsField getPnd_Appc_Parm_Pnd_Citizenship_Ss() { return pnd_Appc_Parm_Pnd_Citizenship_Ss; }

    public DbsField getPnd_Appc_Parm_Pnd_Residency_Ss() { return pnd_Appc_Parm_Pnd_Residency_Ss; }

    public DbsField getPnd_Appc_Parm_Pnd_Ra_Nra_Ss() { return pnd_Appc_Parm_Pnd_Ra_Nra_Ss; }

    public DbsField getPnd_Appc_Parm_Pnd_Tax_Payee_Code() { return pnd_Appc_Parm_Pnd_Tax_Payee_Code; }

    public DbsField getPnd_Appc_Parm_Pnd_Tax_Date_W8_W9() { return pnd_Appc_Parm_Pnd_Tax_Date_W8_W9; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Tax_Date_W8_W9Redef7() { return pnd_Appc_Parm_Pnd_Tax_Date_W8_W9Redef7; }

    public DbsField getPnd_Appc_Parm_Pnd_Tax_Date_W8_W9_A() { return pnd_Appc_Parm_Pnd_Tax_Date_W8_W9_A; }

    public DbsField getPnd_Appc_Parm_Pnd_Check_Tracer() { return pnd_Appc_Parm_Pnd_Check_Tracer; }

    public DbsField getPnd_Appc_Parm_Pnd_Check_Date() { return pnd_Appc_Parm_Pnd_Check_Date; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Check_DateRedef8() { return pnd_Appc_Parm_Pnd_Check_DateRedef8; }

    public DbsField getPnd_Appc_Parm_Pnd_Check_Date_A() { return pnd_Appc_Parm_Pnd_Check_Date_A; }

    public DbsField getPnd_Appc_Parm_Pnd_Check_Amount() { return pnd_Appc_Parm_Pnd_Check_Amount; }

    public DbsField getPnd_Appc_Parm_Pnd_Issue_Or_Settle_Date() { return pnd_Appc_Parm_Pnd_Issue_Or_Settle_Date; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Issue_Or_Settle_DateRedef9() { return pnd_Appc_Parm_Pnd_Issue_Or_Settle_DateRedef9; }

    public DbsField getPnd_Appc_Parm_Pnd_Issue_Or_Settle_Date_A() { return pnd_Appc_Parm_Pnd_Issue_Or_Settle_Date_A; }

    public DbsField getPnd_Appc_Parm_Pnd_Transfer_Ind() { return pnd_Appc_Parm_Pnd_Transfer_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Dist_Amt() { return pnd_Appc_Parm_Pnd_Dist_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Non_Dist_Amt() { return pnd_Appc_Parm_Pnd_Non_Dist_Amt; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record6() { return pnd_Appc_Parm_Pnd_Record6; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Name1() { return pnd_Appc_Parm_Pnd_Payee1_Name1; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Name2() { return pnd_Appc_Parm_Pnd_Payee1_Name2; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Addr1() { return pnd_Appc_Parm_Pnd_Payee1_Addr1; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Addr2() { return pnd_Appc_Parm_Pnd_Payee1_Addr2; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Addr3() { return pnd_Appc_Parm_Pnd_Payee1_Addr3; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Addr4() { return pnd_Appc_Parm_Pnd_Payee1_Addr4; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Addr5() { return pnd_Appc_Parm_Pnd_Payee1_Addr5; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Addr6() { return pnd_Appc_Parm_Pnd_Payee1_Addr6; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_City() { return pnd_Appc_Parm_Pnd_Payee1_City; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_State() { return pnd_Appc_Parm_Pnd_Payee1_State; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Zip() { return pnd_Appc_Parm_Pnd_Payee1_Zip; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Aba_Num() { return pnd_Appc_Parm_Pnd_Payee1_Aba_Num; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Acct_Num() { return pnd_Appc_Parm_Pnd_Payee1_Acct_Num; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Acct_Type() { return pnd_Appc_Parm_Pnd_Payee1_Acct_Type; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Rollover_Ind() { return pnd_Appc_Parm_Pnd_Payee1_Rollover_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Ovrpy_Rcvry_Ind() { return pnd_Appc_Parm_Pnd_Payee1_Ovrpy_Rcvry_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Hold_Ind() { return pnd_Appc_Parm_Pnd_Payee1_Hold_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Type() { return pnd_Appc_Parm_Pnd_Payee1_Type; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Foreign_Code() { return pnd_Appc_Parm_Pnd_Payee1_Foreign_Code; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Method() { return pnd_Appc_Parm_Pnd_Payee1_Method; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Hold_Code() { return pnd_Appc_Parm_Pnd_Payee1_Hold_Code; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Net_Amt() { return pnd_Appc_Parm_Pnd_Payee1_Net_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Ivc_Ind() { return pnd_Appc_Parm_Pnd_Payee1_Ivc_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Installment_Date() { return pnd_Appc_Parm_Pnd_Payee1_Installment_Date; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Payee1_Installment_DateRedef10() { return pnd_Appc_Parm_Pnd_Payee1_Installment_DateRedef10; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Installment_Date_A() { return pnd_Appc_Parm_Pnd_Payee1_Installment_Date_A; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Payee1_Rate_Info() { return pnd_Appc_Parm_Pnd_Payee1_Rate_Info; }

    public DbsField getPnd_Appc_Parm_Pnd_Rate1_Code() { return pnd_Appc_Parm_Pnd_Rate1_Code; }

    public DbsField getPnd_Appc_Parm_Pnd_Rate1_Settle_Amt() { return pnd_Appc_Parm_Pnd_Rate1_Settle_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Rate1_Tiaa_Total_Amt() { return pnd_Appc_Parm_Pnd_Rate1_Tiaa_Total_Amt; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record7() { return pnd_Appc_Parm_Pnd_Record7; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger1_Cnt() { return pnd_Appc_Parm_Pnd_Ledger1_Cnt; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Ledger1_Table() { return pnd_Appc_Parm_Pnd_Ledger1_Table; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger1_Company() { return pnd_Appc_Parm_Pnd_Ledger1_Company; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger1_Acct() { return pnd_Appc_Parm_Pnd_Ledger1_Acct; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger1_Amount() { return pnd_Appc_Parm_Pnd_Ledger1_Amount; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger1_Ind() { return pnd_Appc_Parm_Pnd_Ledger1_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger1_Cont() { return pnd_Appc_Parm_Pnd_Ledger1_Cont; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger1_Eff_Date() { return pnd_Appc_Parm_Pnd_Ledger1_Eff_Date; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger1_Cost_Ctr() { return pnd_Appc_Parm_Pnd_Ledger1_Cost_Ctr; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record8() { return pnd_Appc_Parm_Pnd_Record8; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Check1_Table() { return pnd_Appc_Parm_Pnd_Check1_Table; }

    public DbsField getPnd_Appc_Parm_Pnd_Tot1_Gross_Prcds() { return pnd_Appc_Parm_Pnd_Tot1_Gross_Prcds; }

    public DbsField getPnd_Appc_Parm_Pnd_Check1_Ivc() { return pnd_Appc_Parm_Pnd_Check1_Ivc; }

    public DbsField getPnd_Appc_Parm_Pnd_Dpi1_Amt() { return pnd_Appc_Parm_Pnd_Dpi1_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Int1_Ext_Ro_Amt() { return pnd_Appc_Parm_Pnd_Int1_Ext_Ro_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Fed1_Tax_Wthhld() { return pnd_Appc_Parm_Pnd_Fed1_Tax_Wthhld; }

    public DbsField getPnd_Appc_Parm_Pnd_Us1_Nra_Tax_Wthhld() { return pnd_Appc_Parm_Pnd_Us1_Nra_Tax_Wthhld; }

    public DbsField getPnd_Appc_Parm_Pnd_St1_Tax_Wthhld() { return pnd_Appc_Parm_Pnd_St1_Tax_Wthhld; }

    public DbsField getPnd_Appc_Parm_Pnd_Loc1_Tax_Wthhld() { return pnd_Appc_Parm_Pnd_Loc1_Tax_Wthhld; }

    public DbsField getPnd_Appc_Parm_Pnd_Net1_Prcds() { return pnd_Appc_Parm_Pnd_Net1_Prcds; }

    public DbsField getPnd_Appc_Parm_Pnd_Check1_Wthdrl_Chg() { return pnd_Appc_Parm_Pnd_Check1_Wthdrl_Chg; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record9() { return pnd_Appc_Parm_Pnd_Record9; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Name1() { return pnd_Appc_Parm_Pnd_Payee2_Name1; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Name2() { return pnd_Appc_Parm_Pnd_Payee2_Name2; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Addr1() { return pnd_Appc_Parm_Pnd_Payee2_Addr1; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Addr2() { return pnd_Appc_Parm_Pnd_Payee2_Addr2; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Addr3() { return pnd_Appc_Parm_Pnd_Payee2_Addr3; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Addr4() { return pnd_Appc_Parm_Pnd_Payee2_Addr4; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Addr5() { return pnd_Appc_Parm_Pnd_Payee2_Addr5; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Addr6() { return pnd_Appc_Parm_Pnd_Payee2_Addr6; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_City() { return pnd_Appc_Parm_Pnd_Payee2_City; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_State() { return pnd_Appc_Parm_Pnd_Payee2_State; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Zip() { return pnd_Appc_Parm_Pnd_Payee2_Zip; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Aba_Num() { return pnd_Appc_Parm_Pnd_Payee2_Aba_Num; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Acct_Num() { return pnd_Appc_Parm_Pnd_Payee2_Acct_Num; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Acct_Type() { return pnd_Appc_Parm_Pnd_Payee2_Acct_Type; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Rollover_Ind() { return pnd_Appc_Parm_Pnd_Payee2_Rollover_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Ovrpy_Rcvry_Ind() { return pnd_Appc_Parm_Pnd_Payee2_Ovrpy_Rcvry_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Hold_Ind() { return pnd_Appc_Parm_Pnd_Payee2_Hold_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Type() { return pnd_Appc_Parm_Pnd_Payee2_Type; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Foreign_Code() { return pnd_Appc_Parm_Pnd_Payee2_Foreign_Code; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Method() { return pnd_Appc_Parm_Pnd_Payee2_Method; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Hold_Code() { return pnd_Appc_Parm_Pnd_Payee2_Hold_Code; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Net_Amt() { return pnd_Appc_Parm_Pnd_Payee2_Net_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Ivc_Ind() { return pnd_Appc_Parm_Pnd_Payee2_Ivc_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Installment_Date() { return pnd_Appc_Parm_Pnd_Payee2_Installment_Date; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Payee2_Installment_DateRedef11() { return pnd_Appc_Parm_Pnd_Payee2_Installment_DateRedef11; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Installment_Date_A() { return pnd_Appc_Parm_Pnd_Payee2_Installment_Date_A; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Payee2_Rate_Info() { return pnd_Appc_Parm_Pnd_Payee2_Rate_Info; }

    public DbsField getPnd_Appc_Parm_Pnd_Rate2_Code() { return pnd_Appc_Parm_Pnd_Rate2_Code; }

    public DbsField getPnd_Appc_Parm_Pnd_Rate2_Settle_Amt() { return pnd_Appc_Parm_Pnd_Rate2_Settle_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Rate2_Tiaa_Total_Amt() { return pnd_Appc_Parm_Pnd_Rate2_Tiaa_Total_Amt; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record10() { return pnd_Appc_Parm_Pnd_Record10; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger2_Cnt() { return pnd_Appc_Parm_Pnd_Ledger2_Cnt; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Ledger2_Table() { return pnd_Appc_Parm_Pnd_Ledger2_Table; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger2_Company() { return pnd_Appc_Parm_Pnd_Ledger2_Company; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger2_Acct() { return pnd_Appc_Parm_Pnd_Ledger2_Acct; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger2_Amount() { return pnd_Appc_Parm_Pnd_Ledger2_Amount; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger2_Ind() { return pnd_Appc_Parm_Pnd_Ledger2_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger2_Cont() { return pnd_Appc_Parm_Pnd_Ledger2_Cont; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger2_Eff_Date() { return pnd_Appc_Parm_Pnd_Ledger2_Eff_Date; }

    public DbsField getPnd_Appc_Parm_Pnd_Ledger2_Cost_Ctr() { return pnd_Appc_Parm_Pnd_Ledger2_Cost_Ctr; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record11() { return pnd_Appc_Parm_Pnd_Record11; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Check2_Table() { return pnd_Appc_Parm_Pnd_Check2_Table; }

    public DbsField getPnd_Appc_Parm_Pnd_Tot2_Gross_Prcds() { return pnd_Appc_Parm_Pnd_Tot2_Gross_Prcds; }

    public DbsField getPnd_Appc_Parm_Pnd_Check2_Ivc() { return pnd_Appc_Parm_Pnd_Check2_Ivc; }

    public DbsField getPnd_Appc_Parm_Pnd_Dpi2_Amt() { return pnd_Appc_Parm_Pnd_Dpi2_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Int2_Ext_Ro_Amt() { return pnd_Appc_Parm_Pnd_Int2_Ext_Ro_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Fed2_Tax_Wthhld() { return pnd_Appc_Parm_Pnd_Fed2_Tax_Wthhld; }

    public DbsField getPnd_Appc_Parm_Pnd_Us2_Nra_Tax_Wthhld() { return pnd_Appc_Parm_Pnd_Us2_Nra_Tax_Wthhld; }

    public DbsField getPnd_Appc_Parm_Pnd_St2_Tax_Wthhld() { return pnd_Appc_Parm_Pnd_St2_Tax_Wthhld; }

    public DbsField getPnd_Appc_Parm_Pnd_Loc2_Tax_Wthhld() { return pnd_Appc_Parm_Pnd_Loc2_Tax_Wthhld; }

    public DbsField getPnd_Appc_Parm_Pnd_Net2_Prcds() { return pnd_Appc_Parm_Pnd_Net2_Prcds; }

    public DbsField getPnd_Appc_Parm_Pnd_Check2_Wthdrl_Chg() { return pnd_Appc_Parm_Pnd_Check2_Wthdrl_Chg; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record12() { return pnd_Appc_Parm_Pnd_Record12; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Tot_Table() { return pnd_Appc_Parm_Pnd_Tot_Table; }

    public DbsField getPnd_Appc_Parm_Pnd_Tot_Gross() { return pnd_Appc_Parm_Pnd_Tot_Gross; }

    public DbsField getPnd_Appc_Parm_Pnd_Tot_Net() { return pnd_Appc_Parm_Pnd_Tot_Net; }

    public DbsField getPnd_Appc_Parm_Pnd_Tot_Fedtax() { return pnd_Appc_Parm_Pnd_Tot_Fedtax; }

    public DbsField getPnd_Appc_Parm_Pnd_Tot_Statax() { return pnd_Appc_Parm_Pnd_Tot_Statax; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record13() { return pnd_Appc_Parm_Pnd_Record13; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Addr1() { return pnd_Appc_Parm_Pnd_Ph_Addr1; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Addr2() { return pnd_Appc_Parm_Pnd_Ph_Addr2; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Addr3() { return pnd_Appc_Parm_Pnd_Ph_Addr3; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Addr4() { return pnd_Appc_Parm_Pnd_Ph_Addr4; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Addr5() { return pnd_Appc_Parm_Pnd_Ph_Addr5; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Addr6() { return pnd_Appc_Parm_Pnd_Ph_Addr6; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Zip() { return pnd_Appc_Parm_Pnd_Ph_Zip; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_State() { return pnd_Appc_Parm_Pnd_Ph_State; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record14() { return pnd_Appc_Parm_Pnd_Record14; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Dod() { return pnd_Appc_Parm_Pnd_Ph_Dod; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Ph_DodRedef12() { return pnd_Appc_Parm_Pnd_Ph_DodRedef12; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Dod_A() { return pnd_Appc_Parm_Pnd_Ph_Dod_A; }

    public DbsField getPnd_Appc_Parm_Pnd_Wpid() { return pnd_Appc_Parm_Pnd_Wpid; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee1_Manu_Check() { return pnd_Appc_Parm_Pnd_Payee1_Manu_Check; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Manu_Check() { return pnd_Appc_Parm_Pnd_Payee2_Manu_Check; }

    public DbsField getPnd_Appc_Parm_Pnd_Tax_W8_Ben() { return pnd_Appc_Parm_Pnd_Tax_W8_Ben; }

    public DbsField getPnd_Appc_Parm_Pnd_Tax_W9() { return pnd_Appc_Parm_Pnd_Tax_W9; }

    public DbsField getPnd_Appc_Parm_Pnd_Tax_Year() { return pnd_Appc_Parm_Pnd_Tax_Year; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Roll_Cert() { return pnd_Appc_Parm_Pnd_Payee2_Roll_Cert; }

    public DbsField getPnd_Appc_Parm_Pnd_Payee2_Roll_Cont() { return pnd_Appc_Parm_Pnd_Payee2_Roll_Cont; }

    public DbsField getPnd_Appc_Parm_Pnd_Ss_Tax_Id_Ind() { return pnd_Appc_Parm_Pnd_Ss_Tax_Id_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Taxable_Name() { return pnd_Appc_Parm_Pnd_Taxable_Name; }

    public DbsField getPnd_Appc_Parm_Pnd_Payment_Desc() { return pnd_Appc_Parm_Pnd_Payment_Desc; }

    public DbsField getPnd_Appc_Parm_Pnd_Payment_Tfslash_N() { return pnd_Appc_Parm_Pnd_Payment_Tfslash_N; }

    public DbsField getPnd_Appc_Parm_Pnd_Payment_Pfslash_N() { return pnd_Appc_Parm_Pnd_Payment_Pfslash_N; }

    public DbsField getPnd_Appc_Parm_Pnd_Payment_Lfslash_N() { return pnd_Appc_Parm_Pnd_Payment_Lfslash_N; }

    public DbsField getPnd_Appc_Parm_Pnd_Payment_T_Code() { return pnd_Appc_Parm_Pnd_Payment_T_Code; }

    public DbsField getPnd_Appc_Parm_Pnd_Payment_Nt() { return pnd_Appc_Parm_Pnd_Payment_Nt; }

    public DbsField getPnd_Appc_Parm_Pnd_Tax_Id_Number() { return pnd_Appc_Parm_Pnd_Tax_Id_Number; }

    public DbsField getPnd_Appc_Parm_Pnd_Tax_Cont() { return pnd_Appc_Parm_Pnd_Tax_Cont; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record15() { return pnd_Appc_Parm_Pnd_Record15; }

    public DbsField getPnd_Appc_Parm_Pnd_Court_Date() { return pnd_Appc_Parm_Pnd_Court_Date; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Court_DateRedef13() { return pnd_Appc_Parm_Pnd_Court_DateRedef13; }

    public DbsField getPnd_Appc_Parm_Pnd_Court_Date_A() { return pnd_Appc_Parm_Pnd_Court_Date_A; }

    public DbsField getPnd_Appc_Parm_Pnd_Dvsn_Date() { return pnd_Appc_Parm_Pnd_Dvsn_Date; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Dvsn_DateRedef14() { return pnd_Appc_Parm_Pnd_Dvsn_DateRedef14; }

    public DbsField getPnd_Appc_Parm_Pnd_Dvsn_Date_A() { return pnd_Appc_Parm_Pnd_Dvsn_Date_A; }

    public DbsField getPnd_Appc_Parm_Pnd_Cont_From_Date() { return pnd_Appc_Parm_Pnd_Cont_From_Date; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Cont_From_DateRedef15() { return pnd_Appc_Parm_Pnd_Cont_From_DateRedef15; }

    public DbsField getPnd_Appc_Parm_Pnd_Cont_From_Date_A() { return pnd_Appc_Parm_Pnd_Cont_From_Date_A; }

    public DbsField getPnd_Appc_Parm_Pnd_Cont_To_Date() { return pnd_Appc_Parm_Pnd_Cont_To_Date; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Cont_To_DateRedef16() { return pnd_Appc_Parm_Pnd_Cont_To_DateRedef16; }

    public DbsField getPnd_Appc_Parm_Pnd_Cont_To_Date_A() { return pnd_Appc_Parm_Pnd_Cont_To_Date_A; }

    public DbsField getPnd_Appc_Parm_Pnd_Cont_Accum_Amt() { return pnd_Appc_Parm_Pnd_Cont_Accum_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Cont_Accum_Pct() { return pnd_Appc_Parm_Pnd_Cont_Accum_Pct; }

    public DbsField getPnd_Appc_Parm_Pnd_Cert_From_Date() { return pnd_Appc_Parm_Pnd_Cert_From_Date; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Cert_From_DateRedef17() { return pnd_Appc_Parm_Pnd_Cert_From_DateRedef17; }

    public DbsField getPnd_Appc_Parm_Pnd_Cert_From_Date_A() { return pnd_Appc_Parm_Pnd_Cert_From_Date_A; }

    public DbsField getPnd_Appc_Parm_Pnd_Cert_To_Date() { return pnd_Appc_Parm_Pnd_Cert_To_Date; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Cert_To_DateRedef18() { return pnd_Appc_Parm_Pnd_Cert_To_DateRedef18; }

    public DbsField getPnd_Appc_Parm_Pnd_Cert_To_Date_A() { return pnd_Appc_Parm_Pnd_Cert_To_Date_A; }

    public DbsField getPnd_Appc_Parm_Pnd_Cert_Accum_Amt() { return pnd_Appc_Parm_Pnd_Cert_Accum_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Cert_Accum_Pct() { return pnd_Appc_Parm_Pnd_Cert_Accum_Pct; }

    public DbsField getPnd_Appc_Parm_Pnd_Spouse_Cont() { return pnd_Appc_Parm_Pnd_Spouse_Cont; }

    public DbsField getPnd_Appc_Parm_Pnd_Spouse_Cert() { return pnd_Appc_Parm_Pnd_Spouse_Cert; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record16() { return pnd_Appc_Parm_Pnd_Record16; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Prem_Info() { return pnd_Appc_Parm_Pnd_Prem_Info; }

    public DbsField getPnd_Appc_Parm_Pnd_Prem_By_Bucket() { return pnd_Appc_Parm_Pnd_Prem_By_Bucket; }

    public DbsField getPnd_Appc_Parm_Pnd_Prem_By_Fund() { return pnd_Appc_Parm_Pnd_Prem_By_Fund; }

    public DbsField getPnd_Appc_Parm_Pnd_Prem_Totals() { return pnd_Appc_Parm_Pnd_Prem_Totals; }

    public DbsField getPnd_Appc_Parm_Pnd_Prem_Ftotal() { return pnd_Appc_Parm_Pnd_Prem_Ftotal; }

    public DbsField getPnd_Appc_Parm_Pnd_Prem_Btotal() { return pnd_Appc_Parm_Pnd_Prem_Btotal; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record17() { return pnd_Appc_Parm_Pnd_Record17; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Ben_Dob() { return pnd_Appc_Parm_Pnd_Settle_Ben_Dob; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Ben_Name() { return pnd_Appc_Parm_Pnd_Settle_Ben_Name; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Ben_Sex() { return pnd_Appc_Parm_Pnd_Settle_Ben_Sex; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Ben_Spouse_Ind() { return pnd_Appc_Parm_Pnd_Settle_Ben_Spouse_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Ben_Ssn() { return pnd_Appc_Parm_Pnd_Settle_Ben_Ssn; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Calc_Method() { return pnd_Appc_Parm_Pnd_Settle_Calc_Method; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cert() { return pnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cert; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cont() { return pnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cont; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Mdo_First_Pmnt_Date() { return pnd_Appc_Parm_Pnd_Settle_Mdo_First_Pmnt_Date; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Mdo_Mode() { return pnd_Appc_Parm_Pnd_Settle_Mdo_Mode; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Mode_Change() { return pnd_Appc_Parm_Pnd_Settle_Mode_Change; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Payment_Desc() { return pnd_Appc_Parm_Pnd_Settle_Payment_Desc; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Payment_Option() { return pnd_Appc_Parm_Pnd_Settle_Payment_Option; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Qualified() { return pnd_Appc_Parm_Pnd_Settle_Qualified; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Req_Beg_Date() { return pnd_Appc_Parm_Pnd_Settle_Req_Beg_Date; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Sa_Spouse_Ind() { return pnd_Appc_Parm_Pnd_Settle_Sa_Spouse_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cert() { return pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cert; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cont() { return pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cont; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Dob_Date() { return pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Dob_Date; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Name() { return pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Name; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Sex() { return pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Sex; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Sec_Annt_Ssn() { return pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Ssn; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Trb_Ind() { return pnd_Appc_Parm_Pnd_Settle_Trb_Ind; }

    public DbsField getPnd_Appc_Parm_Pnd_Settle_Trb_Pct() { return pnd_Appc_Parm_Pnd_Settle_Trb_Pct; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record18() { return pnd_Appc_Parm_Pnd_Record18; }

    public DbsField getPnd_Appc_Parm_Pnd_Lob() { return pnd_Appc_Parm_Pnd_Lob; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Fund_Table() { return pnd_Appc_Parm_Pnd_Fund_Table; }

    public DbsField getPnd_Appc_Parm_Pnd_Ticker_Symbol() { return pnd_Appc_Parm_Pnd_Ticker_Symbol; }

    public DbsField getPnd_Appc_Parm_Pnd_Fund_Amt() { return pnd_Appc_Parm_Pnd_Fund_Amt; }

    public DbsField getPnd_Appc_Parm_Pnd_Fund_Pct() { return pnd_Appc_Parm_Pnd_Fund_Pct; }

    public DbsField getPnd_Appc_Parm_Pnd_Fund_Units() { return pnd_Appc_Parm_Pnd_Fund_Units; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record19() { return pnd_Appc_Parm_Pnd_Record19; }

    public DbsField getPnd_Appc_Parm_Pnd_Roth_Contrib_Yyyy() { return pnd_Appc_Parm_Pnd_Roth_Contrib_Yyyy; }

    public DbsField getPnd_Appc_Parm_Pnd_Roth_Qualified() { return pnd_Appc_Parm_Pnd_Roth_Qualified; }

    public DbsField getPnd_Appc_Parm_Pnd_Dod_Dte() { return pnd_Appc_Parm_Pnd_Dod_Dte; }

    public DbsField getPnd_Appc_Parm_Pnd_Dob_Dte() { return pnd_Appc_Parm_Pnd_Dob_Dte; }

    public DbsField getPnd_Appc_Parm_Pnd_Disabl_Dte() { return pnd_Appc_Parm_Pnd_Disabl_Dte; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record20() { return pnd_Appc_Parm_Pnd_Record20; }

    public DbsField getPnd_Appc_Parm_Pnd_Ph_Residence() { return pnd_Appc_Parm_Pnd_Ph_Residence; }

    public DbsField getPnd_Appc_Parm_Pnd_Sucu_Plan_Nbr() { return pnd_Appc_Parm_Pnd_Sucu_Plan_Nbr; }

    public DbsField getPnd_Appc_Parm_Pnd_Sucu_Subplan_Nbr() { return pnd_Appc_Parm_Pnd_Sucu_Subplan_Nbr; }

    public DbsField getPnd_Appc_Parm_Pnd_Sucu_Orig_Cntr_Nbr() { return pnd_Appc_Parm_Pnd_Sucu_Orig_Cntr_Nbr; }

    public DbsField getPnd_Appc_Parm_Pnd_Sucu_Orig_Subplan_Nbr() { return pnd_Appc_Parm_Pnd_Sucu_Orig_Subplan_Nbr; }

    public DbsGroup getPnd_Appc_Parm_Pnd_Record20_End() { return pnd_Appc_Parm_Pnd_Record20_End; }

    public DbsField getPnd_Appc_Parm_Pnd_End_Of_Record() { return pnd_Appc_Parm_Pnd_End_Of_Record; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Appc_Parm = dbsRecord.newFieldArrayInRecord("pnd_Appc_Parm", "#APPC-PARM", FieldType.STRING, 1, new DbsArrayController(1,31752));
        pnd_Appc_Parm.setParameterOption(ParameterOption.ByReference);
        pnd_Appc_ParmRedef1 = dbsRecord.newGroupInRecord("pnd_Appc_ParmRedef1", "Redefines", pnd_Appc_Parm);
        pnd_Appc_Parm_Header = pnd_Appc_ParmRedef1.newFieldInGroup("pnd_Appc_Parm_Header", "HEADER", FieldType.STRING, 82);
        pnd_Appc_Parm_HeaderRedef2 = pnd_Appc_ParmRedef1.newGroupInGroup("pnd_Appc_Parm_HeaderRedef2", "Redefines", pnd_Appc_Parm_Header);
        pnd_Appc_Parm_Length_Msg = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Length_Msg", "LENGTH-MSG", FieldType.BINARY, 4);
        pnd_Appc_Parm_Type_Msg = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Type_Msg", "TYPE-MSG", FieldType.STRING, 2);
        pnd_Appc_Parm_Version_Msg = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Version_Msg", "VERSION-MSG", FieldType.STRING, 2);
        pnd_Appc_Parm_Name_Prog = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Name_Prog", "NAME-PROG", FieldType.STRING, 8);
        pnd_Appc_Parm_Failing_Component = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Failing_Component", "FAILING-COMPONENT", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Failing_Request = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Failing_Request", "FAILING-REQUEST", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Failing_Date = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Failing_Date", "FAILING-DATE", FieldType.STRING, 8);
        pnd_Appc_Parm_Failing_Time = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Failing_Time", "FAILING-TIME", FieldType.STRING, 12);
        pnd_Appc_Parm_Loopback_Probe = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Loopback_Probe", "LOOPBACK-PROBE", FieldType.STRING, 
            2);
        pnd_Appc_Parm_Version_Format = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Version_Format", "VERSION-FORMAT", FieldType.STRING, 
            2);
        pnd_Appc_Parm_Format_Inout = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Format_Inout", "FORMAT-INOUT", FieldType.STRING, 8);
        pnd_Appc_Parm_Rc_Csappc = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Rc_Csappc", "RC-CSAPPC", FieldType.STRING, 8);
        pnd_Appc_Parm_Rc_Failing_Component = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Rc_Failing_Component", "RC-FAILING-COMPONENT", 
            FieldType.STRING, 8);
        pnd_Appc_Parm_Rc_Appl_Pgm = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Rc_Appl_Pgm", "RC-APPL-PGM", FieldType.STRING, 1);
        pnd_Appc_Parm_Role_Msg = pnd_Appc_Parm_HeaderRedef2.newFieldInGroup("pnd_Appc_Parm_Role_Msg", "ROLE-MSG", FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Pgm_Name = pnd_Appc_ParmRedef1.newFieldInGroup("pnd_Appc_Parm_Pnd_Pgm_Name", "#PGM-NAME", FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_User_Sent_Data = pnd_Appc_ParmRedef1.newFieldInGroup("pnd_Appc_Parm_Pnd_User_Sent_Data", "#USER-SENT-DATA", FieldType.STRING, 
            48);
        pnd_Appc_Parm_Pnd_User_Sent_DataRedef3 = pnd_Appc_ParmRedef1.newGroupInGroup("pnd_Appc_Parm_Pnd_User_Sent_DataRedef3", "Redefines", pnd_Appc_Parm_Pnd_User_Sent_Data);
        pnd_Appc_Parm_Pnd_Rqst_Log_Dte_Tme = pnd_Appc_Parm_Pnd_User_Sent_DataRedef3.newFieldInGroup("pnd_Appc_Parm_Pnd_Rqst_Log_Dte_Tme", "#RQST-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Appc_Parm_Pnd_Msg_Entry_Dte_Tme = pnd_Appc_Parm_Pnd_User_Sent_DataRedef3.newFieldInGroup("pnd_Appc_Parm_Pnd_Msg_Entry_Dte_Tme", "#MSG-ENTRY-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Appc_Parm_Pnd_Msg_Tiaa_Cntrct = pnd_Appc_Parm_Pnd_User_Sent_DataRedef3.newFieldInGroup("pnd_Appc_Parm_Pnd_Msg_Tiaa_Cntrct", "#MSG-TIAA-CNTRCT", 
            FieldType.STRING, 10);
        pnd_Appc_Parm_Pnd_Msg_Doc_Type = pnd_Appc_Parm_Pnd_User_Sent_DataRedef3.newFieldInGroup("pnd_Appc_Parm_Pnd_Msg_Doc_Type", "#MSG-DOC-TYPE", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Return_Parm1 = pnd_Appc_ParmRedef1.newFieldInGroup("pnd_Appc_Parm_Pnd_Return_Parm1", "#RETURN-PARM1", FieldType.STRING, 186);
        pnd_Appc_Parm_Pnd_Return_Parm1Redef4 = pnd_Appc_ParmRedef1.newGroupInGroup("pnd_Appc_Parm_Pnd_Return_Parm1Redef4", "Redefines", pnd_Appc_Parm_Pnd_Return_Parm1);
        pnd_Appc_Parm_Cms_Rtrn_Cd = pnd_Appc_Parm_Pnd_Return_Parm1Redef4.newFieldInGroup("pnd_Appc_Parm_Cms_Rtrn_Cd", "CMS-RTRN-CD", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Error_Type = pnd_Appc_Parm_Pnd_Return_Parm1Redef4.newFieldInGroup("pnd_Appc_Parm_Error_Type", "ERROR-TYPE", FieldType.STRING, 1);
        pnd_Appc_Parm_Batch_Nbr = pnd_Appc_Parm_Pnd_Return_Parm1Redef4.newFieldInGroup("pnd_Appc_Parm_Batch_Nbr", "BATCH-NBR", FieldType.NUMERIC, 8);
        pnd_Appc_Parm_Application = pnd_Appc_Parm_Pnd_Return_Parm1Redef4.newFieldInGroup("pnd_Appc_Parm_Application", "APPLICATION", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Program = pnd_Appc_Parm_Pnd_Return_Parm1Redef4.newFieldInGroup("pnd_Appc_Parm_Program", "PROGRAM", FieldType.STRING, 8);
        pnd_Appc_Parm_Error_Text_1 = pnd_Appc_Parm_Pnd_Return_Parm1Redef4.newFieldInGroup("pnd_Appc_Parm_Error_Text_1", "ERROR-TEXT-1", FieldType.STRING, 
            80);
        pnd_Appc_Parm_Error_Text_2 = pnd_Appc_Parm_Pnd_Return_Parm1Redef4.newFieldInGroup("pnd_Appc_Parm_Error_Text_2", "ERROR-TEXT-2", FieldType.STRING, 
            80);
        pnd_Appc_Parm_Pnd_Pdqa652 = pnd_Appc_ParmRedef1.newGroupInGroup("pnd_Appc_Parm_Pnd_Pdqa652", "#PDQA652");
        pnd_Appc_Parm_Record = pnd_Appc_Parm_Pnd_Pdqa652.newGroupInGroup("pnd_Appc_Parm_Record", "RECORD");
        pnd_Appc_Parm_Pnd_Record1 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record1", "#RECORD1");
        pnd_Appc_Parm_Pnd_Pin = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Appc_Parm_Pnd_Tiaa_Cont = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Tiaa_Cont", "#TIAA-CONT", FieldType.STRING, 10);
        pnd_Appc_Parm_Pnd_Cref_Cert = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Cref_Cert", "#CREF-CERT", FieldType.STRING, 10);
        pnd_Appc_Parm_Pnd_Int_Roll_Cert = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Int_Roll_Cert", "#INT-ROLL-CERT", FieldType.STRING, 
            10);
        pnd_Appc_Parm_Pnd_Int_Roll_Cont = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Int_Roll_Cont", "#INT-ROLL-CONT", FieldType.STRING, 
            10);
        pnd_Appc_Parm_Pnd_Effective_Date = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.NUMERIC, 
            8);
        pnd_Appc_Parm_Pnd_Approval_Ind = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Approval_Ind", "#APPROVAL-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Rqst_Approval2_Ind = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Rqst_Approval2_Ind", "#RQST-APPROVAL2-IND", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Complete_Ind = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Complete_Ind", "#COMPLETE-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Auth_Ind = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Auth_Ind", "#AUTH-IND", FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Auth2_Ind = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Auth2_Ind", "#AUTH2-IND", FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Oia_Ind = pnd_Appc_Parm_Pnd_Record1.newFieldInGroup("pnd_Appc_Parm_Pnd_Oia_Ind", "#OIA-IND", FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Record2 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record2", "#RECORD2");
        pnd_Appc_Parm_Pnd_Ppg_Ind = pnd_Appc_Parm_Pnd_Record2.newFieldInGroup("pnd_Appc_Parm_Pnd_Ppg_Ind", "#PPG-IND", FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Ppg_Cnt = pnd_Appc_Parm_Pnd_Record2.newFieldInGroup("pnd_Appc_Parm_Pnd_Ppg_Cnt", "#PPG-CNT", FieldType.NUMERIC, 2);
        pnd_Appc_Parm_Pnd_Ppgs = pnd_Appc_Parm_Pnd_Record2.newFieldArrayInGroup("pnd_Appc_Parm_Pnd_Ppgs", "#PPGS", FieldType.STRING, 5, new DbsArrayController(1,
            15));
        pnd_Appc_Parm_Pnd_Buckets_Cnt = pnd_Appc_Parm_Pnd_Record2.newFieldInGroup("pnd_Appc_Parm_Pnd_Buckets_Cnt", "#BUCKETS-CNT", FieldType.NUMERIC, 
            2);
        pnd_Appc_Parm_Pnd_Buckets = pnd_Appc_Parm_Pnd_Record2.newFieldArrayInGroup("pnd_Appc_Parm_Pnd_Buckets", "#BUCKETS", FieldType.STRING, 1, new DbsArrayController(1,
            5));
        pnd_Appc_Parm_Pnd_Nonprem_Complete = pnd_Appc_Parm_Pnd_Record2.newFieldArrayInGroup("pnd_Appc_Parm_Pnd_Nonprem_Complete", "#NONPREM-COMPLETE", 
            FieldType.STRING, 1, new DbsArrayController(1,3));
        pnd_Appc_Parm_Pnd_Npde_Ledger = pnd_Appc_Parm_Pnd_Record2.newFieldArrayInGroup("pnd_Appc_Parm_Pnd_Npde_Ledger", "#NPDE-LEDGER", FieldType.STRING, 
            1, new DbsArrayController(1,3));
        pnd_Appc_Parm_Pnd_Adar_Complete = pnd_Appc_Parm_Pnd_Record2.newFieldArrayInGroup("pnd_Appc_Parm_Pnd_Adar_Complete", "#ADAR-COMPLETE", FieldType.STRING, 
            1, new DbsArrayController(1,3));
        pnd_Appc_Parm_Pnd_Record3 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record3", "#RECORD3");
        pnd_Appc_Parm_Pnd_Actrl_Accumulation = pnd_Appc_Parm_Pnd_Record3.newGroupArrayInGroup("pnd_Appc_Parm_Pnd_Actrl_Accumulation", "#ACTRL-ACCUMULATION", 
            new DbsArrayController(1,20));
        pnd_Appc_Parm_Pnd_Fund_Wthdrl_Amt = pnd_Appc_Parm_Pnd_Actrl_Accumulation.newFieldInGroup("pnd_Appc_Parm_Pnd_Fund_Wthdrl_Amt", "#FUND-WTHDRL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Fund_Payee1_Accum = pnd_Appc_Parm_Pnd_Actrl_Accumulation.newFieldInGroup("pnd_Appc_Parm_Pnd_Fund_Payee1_Accum", "#FUND-PAYEE1-ACCUM", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Fund_Payee2_Accum = pnd_Appc_Parm_Pnd_Actrl_Accumulation.newFieldInGroup("pnd_Appc_Parm_Pnd_Fund_Payee2_Accum", "#FUND-PAYEE2-ACCUM", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Accum_Before_Amt = pnd_Appc_Parm_Pnd_Actrl_Accumulation.newFieldInGroup("pnd_Appc_Parm_Pnd_Accum_Before_Amt", "#ACCUM-BEFORE-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Accum_After_Amt = pnd_Appc_Parm_Pnd_Actrl_Accumulation.newFieldInGroup("pnd_Appc_Parm_Pnd_Accum_After_Amt", "#ACCUM-AFTER-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Record4 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record4", "#RECORD4");
        pnd_Appc_Parm_Pnd_Ph_Ssn = pnd_Appc_Parm_Pnd_Record4.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Ssn", "#PH-SSN", FieldType.NUMERIC, 9);
        pnd_Appc_Parm_Pnd_Ph_Dob = pnd_Appc_Parm_Pnd_Record4.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Dob", "#PH-DOB", FieldType.NUMERIC, 8);
        pnd_Appc_Parm_Pnd_Ph_DobRedef5 = pnd_Appc_Parm_Pnd_Record4.newGroupInGroup("pnd_Appc_Parm_Pnd_Ph_DobRedef5", "Redefines", pnd_Appc_Parm_Pnd_Ph_Dob);
        pnd_Appc_Parm_Pnd_Ph_Dob_A = pnd_Appc_Parm_Pnd_Ph_DobRedef5.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Dob_A", "#PH-DOB-A", FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Ph_Name = pnd_Appc_Parm_Pnd_Record4.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Name", "#PH-NAME", FieldType.STRING, 106);
        pnd_Appc_Parm_Pnd_Ph_NameRedef6 = pnd_Appc_Parm_Pnd_Record4.newGroupInGroup("pnd_Appc_Parm_Pnd_Ph_NameRedef6", "Redefines", pnd_Appc_Parm_Pnd_Ph_Name);
        pnd_Appc_Parm_Pnd_Ph_Prefix_Name = pnd_Appc_Parm_Pnd_Ph_NameRedef6.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Prefix_Name", "#PH-PREFIX-NAME", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Ph_Last_Name = pnd_Appc_Parm_Pnd_Ph_NameRedef6.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Last_Name", "#PH-LAST-NAME", FieldType.STRING, 
            30);
        pnd_Appc_Parm_Pnd_Ph_First_Name = pnd_Appc_Parm_Pnd_Ph_NameRedef6.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_First_Name", "#PH-FIRST-NAME", FieldType.STRING, 
            30);
        pnd_Appc_Parm_Pnd_Ph_Middle_Name = pnd_Appc_Parm_Pnd_Ph_NameRedef6.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Middle_Name", "#PH-MIDDLE-NAME", FieldType.STRING, 
            30);
        pnd_Appc_Parm_Pnd_Ph_Suffix_Name = pnd_Appc_Parm_Pnd_Ph_NameRedef6.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Suffix_Name", "#PH-SUFFIX-NAME", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Record5 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record5", "#RECORD5");
        pnd_Appc_Parm_Pnd_Payment_Type = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Payment_Type", "#PAYMENT-TYPE", FieldType.STRING, 
            2);
        pnd_Appc_Parm_Pnd_Outst_Loan = pnd_Appc_Parm_Pnd_Record5.newGroupArrayInGroup("pnd_Appc_Parm_Pnd_Outst_Loan", "#OUTST-LOAN", new DbsArrayController(1,
            2));
        pnd_Appc_Parm_Pnd_Outst_Loan_Amt = pnd_Appc_Parm_Pnd_Outst_Loan.newFieldInGroup("pnd_Appc_Parm_Pnd_Outst_Loan_Amt", "#OUTST-LOAN-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Min_Dist_Ind = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Min_Dist_Ind", "#MIN-DIST-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Loan_Num = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Loan_Num", "#LOAN-NUM", FieldType.STRING, 10);
        pnd_Appc_Parm_Pnd_Loan_Princ_Amt = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Loan_Princ_Amt", "#LOAN-PRINC-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Loan_Intrst_Amt = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Loan_Intrst_Amt", "#LOAN-INTRST-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Late_Intrst_Amt = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Late_Intrst_Amt", "#LATE-INTRST-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Total_Dflt_Amt = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Total_Dflt_Amt", "#TOTAL-DFLT-AMT", FieldType.PACKED_DECIMAL, 
            13,2);
        pnd_Appc_Parm_Pnd_Funds_Dflt_Ind = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Funds_Dflt_Ind", "#FUNDS-DFLT-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Tax_Election_Ind_Ss = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Tax_Election_Ind_Ss", "#TAX-ELECTION-IND-SS", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Citizenship_Ss = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Citizenship_Ss", "#CITIZENSHIP-SS", FieldType.STRING, 
            2);
        pnd_Appc_Parm_Pnd_Residency_Ss = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Residency_Ss", "#RESIDENCY-SS", FieldType.STRING, 
            2);
        pnd_Appc_Parm_Pnd_Ra_Nra_Ss = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Ra_Nra_Ss", "#RA-NRA-SS", FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Tax_Payee_Code = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Tax_Payee_Code", "#TAX-PAYEE-CODE", FieldType.NUMERIC, 
            2);
        pnd_Appc_Parm_Pnd_Tax_Date_W8_W9 = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Tax_Date_W8_W9", "#TAX-DATE-W8-W9", FieldType.NUMERIC, 
            8);
        pnd_Appc_Parm_Pnd_Tax_Date_W8_W9Redef7 = pnd_Appc_Parm_Pnd_Record5.newGroupInGroup("pnd_Appc_Parm_Pnd_Tax_Date_W8_W9Redef7", "Redefines", pnd_Appc_Parm_Pnd_Tax_Date_W8_W9);
        pnd_Appc_Parm_Pnd_Tax_Date_W8_W9_A = pnd_Appc_Parm_Pnd_Tax_Date_W8_W9Redef7.newFieldInGroup("pnd_Appc_Parm_Pnd_Tax_Date_W8_W9_A", "#TAX-DATE-W8-W9-A", 
            FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Check_Tracer = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Check_Tracer", "#CHECK-TRACER", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Check_Date = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 8);
        pnd_Appc_Parm_Pnd_Check_DateRedef8 = pnd_Appc_Parm_Pnd_Record5.newGroupInGroup("pnd_Appc_Parm_Pnd_Check_DateRedef8", "Redefines", pnd_Appc_Parm_Pnd_Check_Date);
        pnd_Appc_Parm_Pnd_Check_Date_A = pnd_Appc_Parm_Pnd_Check_DateRedef8.newFieldInGroup("pnd_Appc_Parm_Pnd_Check_Date_A", "#CHECK-DATE-A", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Check_Amount = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Check_Amount", "#CHECK-AMOUNT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Issue_Or_Settle_Date = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Issue_Or_Settle_Date", "#ISSUE-OR-SETTLE-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Appc_Parm_Pnd_Issue_Or_Settle_DateRedef9 = pnd_Appc_Parm_Pnd_Record5.newGroupInGroup("pnd_Appc_Parm_Pnd_Issue_Or_Settle_DateRedef9", "Redefines", 
            pnd_Appc_Parm_Pnd_Issue_Or_Settle_Date);
        pnd_Appc_Parm_Pnd_Issue_Or_Settle_Date_A = pnd_Appc_Parm_Pnd_Issue_Or_Settle_DateRedef9.newFieldInGroup("pnd_Appc_Parm_Pnd_Issue_Or_Settle_Date_A", 
            "#ISSUE-OR-SETTLE-DATE-A", FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Transfer_Ind = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Transfer_Ind", "#TRANSFER-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Dist_Amt = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Dist_Amt", "#DIST-AMT", FieldType.PACKED_DECIMAL, 11,
            2);
        pnd_Appc_Parm_Pnd_Non_Dist_Amt = pnd_Appc_Parm_Pnd_Record5.newFieldInGroup("pnd_Appc_Parm_Pnd_Non_Dist_Amt", "#NON-DIST-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Record6 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record6", "#RECORD6");
        pnd_Appc_Parm_Pnd_Payee1_Name1 = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Name1", "#PAYEE1-NAME1", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee1_Name2 = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Name2", "#PAYEE1-NAME2", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee1_Addr1 = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Addr1", "#PAYEE1-ADDR1", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee1_Addr2 = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Addr2", "#PAYEE1-ADDR2", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee1_Addr3 = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Addr3", "#PAYEE1-ADDR3", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee1_Addr4 = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Addr4", "#PAYEE1-ADDR4", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee1_Addr5 = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Addr5", "#PAYEE1-ADDR5", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee1_Addr6 = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Addr6", "#PAYEE1-ADDR6", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee1_City = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_City", "#PAYEE1-CITY", FieldType.STRING, 24);
        pnd_Appc_Parm_Pnd_Payee1_State = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_State", "#PAYEE1-STATE", FieldType.STRING, 
            2);
        pnd_Appc_Parm_Pnd_Payee1_Zip = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Zip", "#PAYEE1-ZIP", FieldType.STRING, 9);
        pnd_Appc_Parm_Pnd_Payee1_Aba_Num = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Aba_Num", "#PAYEE1-ABA-NUM", FieldType.STRING, 
            9);
        pnd_Appc_Parm_Pnd_Payee1_Acct_Num = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Acct_Num", "#PAYEE1-ACCT-NUM", FieldType.STRING, 
            17);
        pnd_Appc_Parm_Pnd_Payee1_Acct_Type = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Acct_Type", "#PAYEE1-ACCT-TYPE", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Payee1_Rollover_Ind = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Rollover_Ind", "#PAYEE1-ROLLOVER-IND", 
            FieldType.STRING, 2);
        pnd_Appc_Parm_Pnd_Payee1_Ovrpy_Rcvry_Ind = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Ovrpy_Rcvry_Ind", "#PAYEE1-OVRPY-RCVRY-IND", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Payee1_Hold_Ind = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Hold_Ind", "#PAYEE1-HOLD-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Payee1_Type = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Type", "#PAYEE1-TYPE", FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Payee1_Foreign_Code = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Foreign_Code", "#PAYEE1-FOREIGN-CODE", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Payee1_Method = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Method", "#PAYEE1-METHOD", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Payee1_Hold_Code = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Hold_Code", "#PAYEE1-HOLD-CODE", FieldType.STRING, 
            4);
        pnd_Appc_Parm_Pnd_Payee1_Net_Amt = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Net_Amt", "#PAYEE1-NET-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Payee1_Ivc_Ind = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Ivc_Ind", "#PAYEE1-IVC-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Payee1_Installment_Date = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Installment_Date", "#PAYEE1-INSTALLMENT-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Appc_Parm_Pnd_Payee1_Installment_DateRedef10 = pnd_Appc_Parm_Pnd_Record6.newGroupInGroup("pnd_Appc_Parm_Pnd_Payee1_Installment_DateRedef10", 
            "Redefines", pnd_Appc_Parm_Pnd_Payee1_Installment_Date);
        pnd_Appc_Parm_Pnd_Payee1_Installment_Date_A = pnd_Appc_Parm_Pnd_Payee1_Installment_DateRedef10.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Installment_Date_A", 
            "#PAYEE1-INSTALLMENT-DATE-A", FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Payee1_Rate_Info = pnd_Appc_Parm_Pnd_Record6.newGroupArrayInGroup("pnd_Appc_Parm_Pnd_Payee1_Rate_Info", "#PAYEE1-RATE-INFO", 
            new DbsArrayController(1,99));
        pnd_Appc_Parm_Pnd_Rate1_Code = pnd_Appc_Parm_Pnd_Payee1_Rate_Info.newFieldInGroup("pnd_Appc_Parm_Pnd_Rate1_Code", "#RATE1-CODE", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Rate1_Settle_Amt = pnd_Appc_Parm_Pnd_Payee1_Rate_Info.newFieldInGroup("pnd_Appc_Parm_Pnd_Rate1_Settle_Amt", "#RATE1-SETTLE-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Rate1_Tiaa_Total_Amt = pnd_Appc_Parm_Pnd_Record6.newFieldInGroup("pnd_Appc_Parm_Pnd_Rate1_Tiaa_Total_Amt", "#RATE1-TIAA-TOTAL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Record7 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record7", "#RECORD7");
        pnd_Appc_Parm_Pnd_Ledger1_Cnt = pnd_Appc_Parm_Pnd_Record7.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger1_Cnt", "#LEDGER1-CNT", FieldType.NUMERIC, 
            3);
        pnd_Appc_Parm_Pnd_Ledger1_Table = pnd_Appc_Parm_Pnd_Record7.newGroupArrayInGroup("pnd_Appc_Parm_Pnd_Ledger1_Table", "#LEDGER1-TABLE", new DbsArrayController(1,
            90));
        pnd_Appc_Parm_Pnd_Ledger1_Company = pnd_Appc_Parm_Pnd_Ledger1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger1_Company", "#LEDGER1-COMPANY", FieldType.STRING, 
            5);
        pnd_Appc_Parm_Pnd_Ledger1_Acct = pnd_Appc_Parm_Pnd_Ledger1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger1_Acct", "#LEDGER1-ACCT", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Ledger1_Amount = pnd_Appc_Parm_Pnd_Ledger1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger1_Amount", "#LEDGER1-AMOUNT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Ledger1_Ind = pnd_Appc_Parm_Pnd_Ledger1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger1_Ind", "#LEDGER1-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Ledger1_Cont = pnd_Appc_Parm_Pnd_Ledger1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger1_Cont", "#LEDGER1-CONT", FieldType.STRING, 
            10);
        pnd_Appc_Parm_Pnd_Ledger1_Eff_Date = pnd_Appc_Parm_Pnd_Ledger1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger1_Eff_Date", "#LEDGER1-EFF-DATE", 
            FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Ledger1_Cost_Ctr = pnd_Appc_Parm_Pnd_Ledger1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger1_Cost_Ctr", "#LEDGER1-COST-CTR", 
            FieldType.STRING, 5);
        pnd_Appc_Parm_Pnd_Record8 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record8", "#RECORD8");
        pnd_Appc_Parm_Pnd_Check1_Table = pnd_Appc_Parm_Pnd_Record8.newGroupArrayInGroup("pnd_Appc_Parm_Pnd_Check1_Table", "#CHECK1-TABLE", new DbsArrayController(1,
            20));
        pnd_Appc_Parm_Pnd_Tot1_Gross_Prcds = pnd_Appc_Parm_Pnd_Check1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Tot1_Gross_Prcds", "#TOT1-GROSS-PRCDS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Check1_Ivc = pnd_Appc_Parm_Pnd_Check1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Check1_Ivc", "#CHECK1-IVC", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Dpi1_Amt = pnd_Appc_Parm_Pnd_Check1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Dpi1_Amt", "#DPI1-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Int1_Ext_Ro_Amt = pnd_Appc_Parm_Pnd_Check1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Int1_Ext_Ro_Amt", "#INT1-EXT-RO-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Fed1_Tax_Wthhld = pnd_Appc_Parm_Pnd_Check1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Fed1_Tax_Wthhld", "#FED1-TAX-WTHHLD", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Us1_Nra_Tax_Wthhld = pnd_Appc_Parm_Pnd_Check1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Us1_Nra_Tax_Wthhld", "#US1-NRA-TAX-WTHHLD", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_St1_Tax_Wthhld = pnd_Appc_Parm_Pnd_Check1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_St1_Tax_Wthhld", "#ST1-TAX-WTHHLD", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Loc1_Tax_Wthhld = pnd_Appc_Parm_Pnd_Check1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Loc1_Tax_Wthhld", "#LOC1-TAX-WTHHLD", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Net1_Prcds = pnd_Appc_Parm_Pnd_Check1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Net1_Prcds", "#NET1-PRCDS", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Check1_Wthdrl_Chg = pnd_Appc_Parm_Pnd_Check1_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Check1_Wthdrl_Chg", "#CHECK1-WTHDRL-CHG", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Record9 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record9", "#RECORD9");
        pnd_Appc_Parm_Pnd_Payee2_Name1 = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Name1", "#PAYEE2-NAME1", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee2_Name2 = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Name2", "#PAYEE2-NAME2", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee2_Addr1 = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Addr1", "#PAYEE2-ADDR1", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee2_Addr2 = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Addr2", "#PAYEE2-ADDR2", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee2_Addr3 = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Addr3", "#PAYEE2-ADDR3", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee2_Addr4 = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Addr4", "#PAYEE2-ADDR4", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee2_Addr5 = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Addr5", "#PAYEE2-ADDR5", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee2_Addr6 = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Addr6", "#PAYEE2-ADDR6", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payee2_City = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_City", "#PAYEE2-CITY", FieldType.STRING, 24);
        pnd_Appc_Parm_Pnd_Payee2_State = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_State", "#PAYEE2-STATE", FieldType.STRING, 
            2);
        pnd_Appc_Parm_Pnd_Payee2_Zip = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Zip", "#PAYEE2-ZIP", FieldType.STRING, 9);
        pnd_Appc_Parm_Pnd_Payee2_Aba_Num = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Aba_Num", "#PAYEE2-ABA-NUM", FieldType.STRING, 
            9);
        pnd_Appc_Parm_Pnd_Payee2_Acct_Num = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Acct_Num", "#PAYEE2-ACCT-NUM", FieldType.STRING, 
            17);
        pnd_Appc_Parm_Pnd_Payee2_Acct_Type = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Acct_Type", "#PAYEE2-ACCT-TYPE", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Payee2_Rollover_Ind = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Rollover_Ind", "#PAYEE2-ROLLOVER-IND", 
            FieldType.STRING, 2);
        pnd_Appc_Parm_Pnd_Payee2_Ovrpy_Rcvry_Ind = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Ovrpy_Rcvry_Ind", "#PAYEE2-OVRPY-RCVRY-IND", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Payee2_Hold_Ind = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Hold_Ind", "#PAYEE2-HOLD-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Payee2_Type = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Type", "#PAYEE2-TYPE", FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Payee2_Foreign_Code = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Foreign_Code", "#PAYEE2-FOREIGN-CODE", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Payee2_Method = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Method", "#PAYEE2-METHOD", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Payee2_Hold_Code = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Hold_Code", "#PAYEE2-HOLD-CODE", FieldType.STRING, 
            4);
        pnd_Appc_Parm_Pnd_Payee2_Net_Amt = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Net_Amt", "#PAYEE2-NET-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Payee2_Ivc_Ind = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Ivc_Ind", "#PAYEE2-IVC-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Payee2_Installment_Date = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Installment_Date", "#PAYEE2-INSTALLMENT-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Appc_Parm_Pnd_Payee2_Installment_DateRedef11 = pnd_Appc_Parm_Pnd_Record9.newGroupInGroup("pnd_Appc_Parm_Pnd_Payee2_Installment_DateRedef11", 
            "Redefines", pnd_Appc_Parm_Pnd_Payee2_Installment_Date);
        pnd_Appc_Parm_Pnd_Payee2_Installment_Date_A = pnd_Appc_Parm_Pnd_Payee2_Installment_DateRedef11.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Installment_Date_A", 
            "#PAYEE2-INSTALLMENT-DATE-A", FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Payee2_Rate_Info = pnd_Appc_Parm_Pnd_Record9.newGroupArrayInGroup("pnd_Appc_Parm_Pnd_Payee2_Rate_Info", "#PAYEE2-RATE-INFO", 
            new DbsArrayController(1,99));
        pnd_Appc_Parm_Pnd_Rate2_Code = pnd_Appc_Parm_Pnd_Payee2_Rate_Info.newFieldInGroup("pnd_Appc_Parm_Pnd_Rate2_Code", "#RATE2-CODE", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Rate2_Settle_Amt = pnd_Appc_Parm_Pnd_Payee2_Rate_Info.newFieldInGroup("pnd_Appc_Parm_Pnd_Rate2_Settle_Amt", "#RATE2-SETTLE-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Rate2_Tiaa_Total_Amt = pnd_Appc_Parm_Pnd_Record9.newFieldInGroup("pnd_Appc_Parm_Pnd_Rate2_Tiaa_Total_Amt", "#RATE2-TIAA-TOTAL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Record10 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record10", "#RECORD10");
        pnd_Appc_Parm_Pnd_Ledger2_Cnt = pnd_Appc_Parm_Pnd_Record10.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger2_Cnt", "#LEDGER2-CNT", FieldType.NUMERIC, 
            3);
        pnd_Appc_Parm_Pnd_Ledger2_Table = pnd_Appc_Parm_Pnd_Record10.newGroupArrayInGroup("pnd_Appc_Parm_Pnd_Ledger2_Table", "#LEDGER2-TABLE", new DbsArrayController(1,
            90));
        pnd_Appc_Parm_Pnd_Ledger2_Company = pnd_Appc_Parm_Pnd_Ledger2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger2_Company", "#LEDGER2-COMPANY", FieldType.STRING, 
            5);
        pnd_Appc_Parm_Pnd_Ledger2_Acct = pnd_Appc_Parm_Pnd_Ledger2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger2_Acct", "#LEDGER2-ACCT", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Ledger2_Amount = pnd_Appc_Parm_Pnd_Ledger2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger2_Amount", "#LEDGER2-AMOUNT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Ledger2_Ind = pnd_Appc_Parm_Pnd_Ledger2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger2_Ind", "#LEDGER2-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Ledger2_Cont = pnd_Appc_Parm_Pnd_Ledger2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger2_Cont", "#LEDGER2-CONT", FieldType.STRING, 
            10);
        pnd_Appc_Parm_Pnd_Ledger2_Eff_Date = pnd_Appc_Parm_Pnd_Ledger2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger2_Eff_Date", "#LEDGER2-EFF-DATE", 
            FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Ledger2_Cost_Ctr = pnd_Appc_Parm_Pnd_Ledger2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ledger2_Cost_Ctr", "#LEDGER2-COST-CTR", 
            FieldType.STRING, 5);
        pnd_Appc_Parm_Pnd_Record11 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record11", "#RECORD11");
        pnd_Appc_Parm_Pnd_Check2_Table = pnd_Appc_Parm_Pnd_Record11.newGroupArrayInGroup("pnd_Appc_Parm_Pnd_Check2_Table", "#CHECK2-TABLE", new DbsArrayController(1,
            20));
        pnd_Appc_Parm_Pnd_Tot2_Gross_Prcds = pnd_Appc_Parm_Pnd_Check2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Tot2_Gross_Prcds", "#TOT2-GROSS-PRCDS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Check2_Ivc = pnd_Appc_Parm_Pnd_Check2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Check2_Ivc", "#CHECK2-IVC", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Dpi2_Amt = pnd_Appc_Parm_Pnd_Check2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Dpi2_Amt", "#DPI2-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Int2_Ext_Ro_Amt = pnd_Appc_Parm_Pnd_Check2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Int2_Ext_Ro_Amt", "#INT2-EXT-RO-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Fed2_Tax_Wthhld = pnd_Appc_Parm_Pnd_Check2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Fed2_Tax_Wthhld", "#FED2-TAX-WTHHLD", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Us2_Nra_Tax_Wthhld = pnd_Appc_Parm_Pnd_Check2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Us2_Nra_Tax_Wthhld", "#US2-NRA-TAX-WTHHLD", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_St2_Tax_Wthhld = pnd_Appc_Parm_Pnd_Check2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_St2_Tax_Wthhld", "#ST2-TAX-WTHHLD", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Loc2_Tax_Wthhld = pnd_Appc_Parm_Pnd_Check2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Loc2_Tax_Wthhld", "#LOC2-TAX-WTHHLD", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Net2_Prcds = pnd_Appc_Parm_Pnd_Check2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Net2_Prcds", "#NET2-PRCDS", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Check2_Wthdrl_Chg = pnd_Appc_Parm_Pnd_Check2_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Check2_Wthdrl_Chg", "#CHECK2-WTHDRL-CHG", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Record12 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record12", "#RECORD12");
        pnd_Appc_Parm_Pnd_Tot_Table = pnd_Appc_Parm_Pnd_Record12.newGroupArrayInGroup("pnd_Appc_Parm_Pnd_Tot_Table", "#TOT-TABLE", new DbsArrayController(1,
            2));
        pnd_Appc_Parm_Pnd_Tot_Gross = pnd_Appc_Parm_Pnd_Tot_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Tot_Gross", "#TOT-GROSS", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Tot_Net = pnd_Appc_Parm_Pnd_Tot_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Tot_Net", "#TOT-NET", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Appc_Parm_Pnd_Tot_Fedtax = pnd_Appc_Parm_Pnd_Tot_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Tot_Fedtax", "#TOT-FEDTAX", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Tot_Statax = pnd_Appc_Parm_Pnd_Tot_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Tot_Statax", "#TOT-STATAX", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Record13 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record13", "#RECORD13");
        pnd_Appc_Parm_Pnd_Ph_Addr1 = pnd_Appc_Parm_Pnd_Record13.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Addr1", "#PH-ADDR1", FieldType.STRING, 35);
        pnd_Appc_Parm_Pnd_Ph_Addr2 = pnd_Appc_Parm_Pnd_Record13.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Addr2", "#PH-ADDR2", FieldType.STRING, 35);
        pnd_Appc_Parm_Pnd_Ph_Addr3 = pnd_Appc_Parm_Pnd_Record13.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Addr3", "#PH-ADDR3", FieldType.STRING, 35);
        pnd_Appc_Parm_Pnd_Ph_Addr4 = pnd_Appc_Parm_Pnd_Record13.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Addr4", "#PH-ADDR4", FieldType.STRING, 35);
        pnd_Appc_Parm_Pnd_Ph_Addr5 = pnd_Appc_Parm_Pnd_Record13.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Addr5", "#PH-ADDR5", FieldType.STRING, 35);
        pnd_Appc_Parm_Pnd_Ph_Addr6 = pnd_Appc_Parm_Pnd_Record13.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Addr6", "#PH-ADDR6", FieldType.STRING, 35);
        pnd_Appc_Parm_Pnd_Ph_Zip = pnd_Appc_Parm_Pnd_Record13.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Zip", "#PH-ZIP", FieldType.STRING, 9);
        pnd_Appc_Parm_Pnd_Ph_State = pnd_Appc_Parm_Pnd_Record13.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_State", "#PH-STATE", FieldType.STRING, 2);
        pnd_Appc_Parm_Pnd_Record14 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record14", "#RECORD14");
        pnd_Appc_Parm_Pnd_Ph_Dod = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Dod", "#PH-DOD", FieldType.NUMERIC, 8);
        pnd_Appc_Parm_Pnd_Ph_DodRedef12 = pnd_Appc_Parm_Pnd_Record14.newGroupInGroup("pnd_Appc_Parm_Pnd_Ph_DodRedef12", "Redefines", pnd_Appc_Parm_Pnd_Ph_Dod);
        pnd_Appc_Parm_Pnd_Ph_Dod_A = pnd_Appc_Parm_Pnd_Ph_DodRedef12.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Dod_A", "#PH-DOD-A", FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Wpid = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Wpid", "#WPID", FieldType.STRING, 6);
        pnd_Appc_Parm_Pnd_Payee1_Manu_Check = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee1_Manu_Check", "#PAYEE1-MANU-CHECK", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Payee2_Manu_Check = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Manu_Check", "#PAYEE2-MANU-CHECK", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Tax_W8_Ben = pnd_Appc_Parm_Pnd_Record14.newFieldArrayInGroup("pnd_Appc_Parm_Pnd_Tax_W8_Ben", "#TAX-W8-BEN", FieldType.STRING, 
            1, new DbsArrayController(1,3));
        pnd_Appc_Parm_Pnd_Tax_W9 = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Tax_W9", "#TAX-W9", FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Tax_Year = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Tax_Year", "#TAX-YEAR", FieldType.NUMERIC, 4);
        pnd_Appc_Parm_Pnd_Payee2_Roll_Cert = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Roll_Cert", "#PAYEE2-ROLL-CERT", FieldType.STRING, 
            10);
        pnd_Appc_Parm_Pnd_Payee2_Roll_Cont = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Payee2_Roll_Cont", "#PAYEE2-ROLL-CONT", FieldType.STRING, 
            10);
        pnd_Appc_Parm_Pnd_Ss_Tax_Id_Ind = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Ss_Tax_Id_Ind", "#SS-TAX-ID-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Taxable_Name = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Taxable_Name", "#TAXABLE-NAME", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Payment_Desc = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Payment_Desc", "#PAYMENT-DESC", FieldType.STRING, 
            25);
        pnd_Appc_Parm_Pnd_Payment_Tfslash_N = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Payment_Tfslash_N", "#PAYMENT-T/N", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Payment_Pfslash_N = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Payment_Pfslash_N", "#PAYMENT-P/N", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Payment_Lfslash_N = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Payment_Lfslash_N", "#PAYMENT-L/N", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Payment_T_Code = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Payment_T_Code", "#PAYMENT-T-CODE", FieldType.STRING, 
            2);
        pnd_Appc_Parm_Pnd_Payment_Nt = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Payment_Nt", "#PAYMENT-NT", FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Tax_Id_Number = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Tax_Id_Number", "#TAX-ID-NUMBER", FieldType.NUMERIC, 
            9);
        pnd_Appc_Parm_Pnd_Tax_Cont = pnd_Appc_Parm_Pnd_Record14.newFieldInGroup("pnd_Appc_Parm_Pnd_Tax_Cont", "#TAX-CONT", FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Record15 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record15", "#RECORD15");
        pnd_Appc_Parm_Pnd_Court_Date = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Court_Date", "#COURT-DATE", FieldType.NUMERIC, 8);
        pnd_Appc_Parm_Pnd_Court_DateRedef13 = pnd_Appc_Parm_Pnd_Record15.newGroupInGroup("pnd_Appc_Parm_Pnd_Court_DateRedef13", "Redefines", pnd_Appc_Parm_Pnd_Court_Date);
        pnd_Appc_Parm_Pnd_Court_Date_A = pnd_Appc_Parm_Pnd_Court_DateRedef13.newFieldInGroup("pnd_Appc_Parm_Pnd_Court_Date_A", "#COURT-DATE-A", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Dvsn_Date = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Dvsn_Date", "#DVSN-DATE", FieldType.NUMERIC, 8);
        pnd_Appc_Parm_Pnd_Dvsn_DateRedef14 = pnd_Appc_Parm_Pnd_Record15.newGroupInGroup("pnd_Appc_Parm_Pnd_Dvsn_DateRedef14", "Redefines", pnd_Appc_Parm_Pnd_Dvsn_Date);
        pnd_Appc_Parm_Pnd_Dvsn_Date_A = pnd_Appc_Parm_Pnd_Dvsn_DateRedef14.newFieldInGroup("pnd_Appc_Parm_Pnd_Dvsn_Date_A", "#DVSN-DATE-A", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Cont_From_Date = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Cont_From_Date", "#CONT-FROM-DATE", FieldType.NUMERIC, 
            8);
        pnd_Appc_Parm_Pnd_Cont_From_DateRedef15 = pnd_Appc_Parm_Pnd_Record15.newGroupInGroup("pnd_Appc_Parm_Pnd_Cont_From_DateRedef15", "Redefines", pnd_Appc_Parm_Pnd_Cont_From_Date);
        pnd_Appc_Parm_Pnd_Cont_From_Date_A = pnd_Appc_Parm_Pnd_Cont_From_DateRedef15.newFieldInGroup("pnd_Appc_Parm_Pnd_Cont_From_Date_A", "#CONT-FROM-DATE-A", 
            FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Cont_To_Date = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Cont_To_Date", "#CONT-TO-DATE", FieldType.NUMERIC, 
            8);
        pnd_Appc_Parm_Pnd_Cont_To_DateRedef16 = pnd_Appc_Parm_Pnd_Record15.newGroupInGroup("pnd_Appc_Parm_Pnd_Cont_To_DateRedef16", "Redefines", pnd_Appc_Parm_Pnd_Cont_To_Date);
        pnd_Appc_Parm_Pnd_Cont_To_Date_A = pnd_Appc_Parm_Pnd_Cont_To_DateRedef16.newFieldInGroup("pnd_Appc_Parm_Pnd_Cont_To_Date_A", "#CONT-TO-DATE-A", 
            FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Cont_Accum_Amt = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Cont_Accum_Amt", "#CONT-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Cont_Accum_Pct = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Cont_Accum_Pct", "#CONT-ACCUM-PCT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Cert_From_Date = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Cert_From_Date", "#CERT-FROM-DATE", FieldType.NUMERIC, 
            8);
        pnd_Appc_Parm_Pnd_Cert_From_DateRedef17 = pnd_Appc_Parm_Pnd_Record15.newGroupInGroup("pnd_Appc_Parm_Pnd_Cert_From_DateRedef17", "Redefines", pnd_Appc_Parm_Pnd_Cert_From_Date);
        pnd_Appc_Parm_Pnd_Cert_From_Date_A = pnd_Appc_Parm_Pnd_Cert_From_DateRedef17.newFieldInGroup("pnd_Appc_Parm_Pnd_Cert_From_Date_A", "#CERT-FROM-DATE-A", 
            FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Cert_To_Date = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Cert_To_Date", "#CERT-TO-DATE", FieldType.NUMERIC, 
            8);
        pnd_Appc_Parm_Pnd_Cert_To_DateRedef18 = pnd_Appc_Parm_Pnd_Record15.newGroupInGroup("pnd_Appc_Parm_Pnd_Cert_To_DateRedef18", "Redefines", pnd_Appc_Parm_Pnd_Cert_To_Date);
        pnd_Appc_Parm_Pnd_Cert_To_Date_A = pnd_Appc_Parm_Pnd_Cert_To_DateRedef18.newFieldInGroup("pnd_Appc_Parm_Pnd_Cert_To_Date_A", "#CERT-TO-DATE-A", 
            FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Cert_Accum_Amt = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Cert_Accum_Amt", "#CERT-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Cert_Accum_Pct = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Cert_Accum_Pct", "#CERT-ACCUM-PCT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Spouse_Cont = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Spouse_Cont", "#SPOUSE-CONT", FieldType.STRING, 
            10);
        pnd_Appc_Parm_Pnd_Spouse_Cert = pnd_Appc_Parm_Pnd_Record15.newFieldInGroup("pnd_Appc_Parm_Pnd_Spouse_Cert", "#SPOUSE-CERT", FieldType.STRING, 
            10);
        pnd_Appc_Parm_Pnd_Record16 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record16", "#RECORD16");
        pnd_Appc_Parm_Pnd_Prem_Info = pnd_Appc_Parm_Pnd_Record16.newGroupArrayInGroup("pnd_Appc_Parm_Pnd_Prem_Info", "#PREM-INFO", new DbsArrayController(1,
            24));
        pnd_Appc_Parm_Pnd_Prem_By_Bucket = pnd_Appc_Parm_Pnd_Prem_Info.newFieldArrayInGroup("pnd_Appc_Parm_Pnd_Prem_By_Bucket", "#PREM-BY-BUCKET", FieldType.PACKED_DECIMAL, 
            11,2, new DbsArrayController(1,5));
        pnd_Appc_Parm_Pnd_Prem_By_Fund = pnd_Appc_Parm_Pnd_Prem_Info.newFieldArrayInGroup("pnd_Appc_Parm_Pnd_Prem_By_Fund", "#PREM-BY-FUND", FieldType.PACKED_DECIMAL, 
            11,2, new DbsArrayController(1,20));
        pnd_Appc_Parm_Pnd_Prem_Totals = pnd_Appc_Parm_Pnd_Record16.newFieldArrayInGroup("pnd_Appc_Parm_Pnd_Prem_Totals", "#PREM-TOTALS", FieldType.PACKED_DECIMAL, 
            11,2, new DbsArrayController(1,25));
        pnd_Appc_Parm_Pnd_Prem_Ftotal = pnd_Appc_Parm_Pnd_Record16.newFieldInGroup("pnd_Appc_Parm_Pnd_Prem_Ftotal", "#PREM-FTOTAL", FieldType.PACKED_DECIMAL, 
            14,2);
        pnd_Appc_Parm_Pnd_Prem_Btotal = pnd_Appc_Parm_Pnd_Record16.newFieldInGroup("pnd_Appc_Parm_Pnd_Prem_Btotal", "#PREM-BTOTAL", FieldType.PACKED_DECIMAL, 
            14,2);
        pnd_Appc_Parm_Pnd_Record17 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record17", "#RECORD17");
        pnd_Appc_Parm_Pnd_Settle_Ben_Dob = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Ben_Dob", "#SETTLE-BEN-DOB", FieldType.STRING, 
            8);
        pnd_Appc_Parm_Pnd_Settle_Ben_Name = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Ben_Name", "#SETTLE-BEN-NAME", FieldType.STRING, 
            35);
        pnd_Appc_Parm_Pnd_Settle_Ben_Sex = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Ben_Sex", "#SETTLE-BEN-SEX", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Settle_Ben_Spouse_Ind = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Ben_Spouse_Ind", "#SETTLE-BEN-SPOUSE-IND", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Settle_Ben_Ssn = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Ben_Ssn", "#SETTLE-BEN-SSN", FieldType.NUMERIC, 
            9);
        pnd_Appc_Parm_Pnd_Settle_Calc_Method = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Calc_Method", "#SETTLE-CALC-METHOD", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cert = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cert", "#SETTLE-MDO-BEN-CERT", 
            FieldType.STRING, 10);
        pnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cont = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Mdo_Ben_Cont", "#SETTLE-MDO-BEN-CONT", 
            FieldType.STRING, 10);
        pnd_Appc_Parm_Pnd_Settle_Mdo_First_Pmnt_Date = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Mdo_First_Pmnt_Date", "#SETTLE-MDO-FIRST-PMNT-DATE", 
            FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Settle_Mdo_Mode = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Mdo_Mode", "#SETTLE-MDO-MODE", FieldType.NUMERIC, 
            3);
        pnd_Appc_Parm_Pnd_Settle_Mode_Change = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Mode_Change", "#SETTLE-MODE-CHANGE", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Settle_Payment_Desc = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Payment_Desc", "#SETTLE-PAYMENT-DESC", 
            FieldType.STRING, 50);
        pnd_Appc_Parm_Pnd_Settle_Payment_Option = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Payment_Option", "#SETTLE-PAYMENT-OPTION", 
            FieldType.STRING, 4);
        pnd_Appc_Parm_Pnd_Settle_Qualified = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Qualified", "#SETTLE-QUALIFIED", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Settle_Req_Beg_Date = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Req_Beg_Date", "#SETTLE-REQ-BEG-DATE", 
            FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Settle_Sa_Spouse_Ind = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Sa_Spouse_Ind", "#SETTLE-SA-SPOUSE-IND", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cert = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cert", "#SETTLE-SEC-ANNT-CERT", 
            FieldType.STRING, 10);
        pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cont = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Cont", "#SETTLE-SEC-ANNT-CONT", 
            FieldType.STRING, 10);
        pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Dob_Date = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Dob_Date", "#SETTLE-SEC-ANNT-DOB-DATE", 
            FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Name = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Name", "#SETTLE-SEC-ANNT-NAME", 
            FieldType.STRING, 35);
        pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Sex = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Sex", "#SETTLE-SEC-ANNT-SEX", 
            FieldType.STRING, 1);
        pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Ssn = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Sec_Annt_Ssn", "#SETTLE-SEC-ANNT-SSN", 
            FieldType.NUMERIC, 9);
        pnd_Appc_Parm_Pnd_Settle_Trb_Ind = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Trb_Ind", "#SETTLE-TRB-IND", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Settle_Trb_Pct = pnd_Appc_Parm_Pnd_Record17.newFieldInGroup("pnd_Appc_Parm_Pnd_Settle_Trb_Pct", "#SETTLE-TRB-PCT", FieldType.DECIMAL, 
            4,2);
        pnd_Appc_Parm_Pnd_Record18 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record18", "#RECORD18");
        pnd_Appc_Parm_Pnd_Lob = pnd_Appc_Parm_Pnd_Record18.newFieldInGroup("pnd_Appc_Parm_Pnd_Lob", "#LOB", FieldType.STRING, 10);
        pnd_Appc_Parm_Pnd_Fund_Table = pnd_Appc_Parm_Pnd_Record18.newGroupArrayInGroup("pnd_Appc_Parm_Pnd_Fund_Table", "#FUND-TABLE", new DbsArrayController(1,
            20));
        pnd_Appc_Parm_Pnd_Ticker_Symbol = pnd_Appc_Parm_Pnd_Fund_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Ticker_Symbol", "#TICKER-SYMBOL", FieldType.STRING, 
            10);
        pnd_Appc_Parm_Pnd_Fund_Amt = pnd_Appc_Parm_Pnd_Fund_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Fund_Amt", "#FUND-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Appc_Parm_Pnd_Fund_Pct = pnd_Appc_Parm_Pnd_Fund_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Fund_Pct", "#FUND-PCT", FieldType.PACKED_DECIMAL, 
            5,2);
        pnd_Appc_Parm_Pnd_Fund_Units = pnd_Appc_Parm_Pnd_Fund_Table.newFieldInGroup("pnd_Appc_Parm_Pnd_Fund_Units", "#FUND-UNITS", FieldType.PACKED_DECIMAL, 
            11,3);
        pnd_Appc_Parm_Pnd_Record19 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record19", "#RECORD19");
        pnd_Appc_Parm_Pnd_Roth_Contrib_Yyyy = pnd_Appc_Parm_Pnd_Record19.newFieldInGroup("pnd_Appc_Parm_Pnd_Roth_Contrib_Yyyy", "#ROTH-CONTRIB-YYYY", 
            FieldType.STRING, 4);
        pnd_Appc_Parm_Pnd_Roth_Qualified = pnd_Appc_Parm_Pnd_Record19.newFieldInGroup("pnd_Appc_Parm_Pnd_Roth_Qualified", "#ROTH-QUALIFIED", FieldType.STRING, 
            1);
        pnd_Appc_Parm_Pnd_Dod_Dte = pnd_Appc_Parm_Pnd_Record19.newFieldInGroup("pnd_Appc_Parm_Pnd_Dod_Dte", "#DOD-DTE", FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Dob_Dte = pnd_Appc_Parm_Pnd_Record19.newFieldInGroup("pnd_Appc_Parm_Pnd_Dob_Dte", "#DOB-DTE", FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Disabl_Dte = pnd_Appc_Parm_Pnd_Record19.newFieldInGroup("pnd_Appc_Parm_Pnd_Disabl_Dte", "#DISABL-DTE", FieldType.STRING, 8);
        pnd_Appc_Parm_Pnd_Record20 = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record20", "#RECORD20");
        pnd_Appc_Parm_Pnd_Ph_Residence = pnd_Appc_Parm_Pnd_Record20.newFieldInGroup("pnd_Appc_Parm_Pnd_Ph_Residence", "#PH-RESIDENCE", FieldType.STRING, 
            2);
        pnd_Appc_Parm_Pnd_Sucu_Plan_Nbr = pnd_Appc_Parm_Pnd_Record20.newFieldInGroup("pnd_Appc_Parm_Pnd_Sucu_Plan_Nbr", "#SUCU-PLAN-NBR", FieldType.STRING, 
            6);
        pnd_Appc_Parm_Pnd_Sucu_Subplan_Nbr = pnd_Appc_Parm_Pnd_Record20.newFieldInGroup("pnd_Appc_Parm_Pnd_Sucu_Subplan_Nbr", "#SUCU-SUBPLAN-NBR", FieldType.STRING, 
            6);
        pnd_Appc_Parm_Pnd_Sucu_Orig_Cntr_Nbr = pnd_Appc_Parm_Pnd_Record20.newFieldInGroup("pnd_Appc_Parm_Pnd_Sucu_Orig_Cntr_Nbr", "#SUCU-ORIG-CNTR-NBR", 
            FieldType.STRING, 10);
        pnd_Appc_Parm_Pnd_Sucu_Orig_Subplan_Nbr = pnd_Appc_Parm_Pnd_Record20.newFieldInGroup("pnd_Appc_Parm_Pnd_Sucu_Orig_Subplan_Nbr", "#SUCU-ORIG-SUBPLAN-NBR", 
            FieldType.STRING, 6);
        pnd_Appc_Parm_Pnd_Record20_End = pnd_Appc_Parm_Record.newGroupInGroup("pnd_Appc_Parm_Pnd_Record20_End", "#RECORD20-END");
        pnd_Appc_Parm_Pnd_End_Of_Record = pnd_Appc_Parm_Pnd_Record20_End.newFieldInGroup("pnd_Appc_Parm_Pnd_End_Of_Record", "#END-OF-RECORD", FieldType.STRING, 
            10);

        dbsRecord.reset();
    }

    // Constructors
    public PdaEfsa1205(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

