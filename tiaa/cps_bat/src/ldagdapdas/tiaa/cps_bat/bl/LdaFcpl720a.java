/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:14 PM
**        * FROM NATURAL LDA     : FCPL720A
************************************************************
**        * FILE NAME            : LdaFcpl720a.java
**        * CLASS NAME           : LdaFcpl720a
**        * INSTANCE NAME        : LdaFcpl720a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl720a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl720a;
    private DbsField pnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Fcpl720a_Pnd_Cntl_Idx;
    private DbsField pnd_Fcpl720a_Pnd_Rec_Type_Idx;
    private DbsField pnd_Fcpl720a_Pnd_Rec_Types_Text;
    private DbsGroup pnd_Fcpl720a_Pnd_Cntl_Table;
    private DbsField pnd_Fcpl720a_Pnd_Cntl_Text;
    private DbsField pnd_Fcpl720a_Pnd_Rec_Type_Cnt;

    public DbsGroup getPnd_Fcpl720a() { return pnd_Fcpl720a; }

    public DbsField getPnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde() { return pnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Fcpl720a_Pnd_Cntl_Idx() { return pnd_Fcpl720a_Pnd_Cntl_Idx; }

    public DbsField getPnd_Fcpl720a_Pnd_Rec_Type_Idx() { return pnd_Fcpl720a_Pnd_Rec_Type_Idx; }

    public DbsField getPnd_Fcpl720a_Pnd_Rec_Types_Text() { return pnd_Fcpl720a_Pnd_Rec_Types_Text; }

    public DbsGroup getPnd_Fcpl720a_Pnd_Cntl_Table() { return pnd_Fcpl720a_Pnd_Cntl_Table; }

    public DbsField getPnd_Fcpl720a_Pnd_Cntl_Text() { return pnd_Fcpl720a_Pnd_Cntl_Text; }

    public DbsField getPnd_Fcpl720a_Pnd_Rec_Type_Cnt() { return pnd_Fcpl720a_Pnd_Rec_Type_Cnt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl720a = newGroupInRecord("pnd_Fcpl720a", "#FCPL720A");
        pnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde = pnd_Fcpl720a.newFieldInGroup("pnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Fcpl720a_Pnd_Cntl_Idx = pnd_Fcpl720a.newFieldInGroup("pnd_Fcpl720a_Pnd_Cntl_Idx", "#CNTL-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl720a_Pnd_Rec_Type_Idx = pnd_Fcpl720a.newFieldInGroup("pnd_Fcpl720a_Pnd_Rec_Type_Idx", "#REC-TYPE-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl720a_Pnd_Rec_Types_Text = pnd_Fcpl720a.newFieldArrayInGroup("pnd_Fcpl720a_Pnd_Rec_Types_Text", "#REC-TYPES-TEXT", FieldType.STRING, 16, 
            new DbsArrayController(0,3));
        pnd_Fcpl720a_Pnd_Cntl_Table = pnd_Fcpl720a.newGroupArrayInGroup("pnd_Fcpl720a_Pnd_Cntl_Table", "#CNTL-TABLE", new DbsArrayController(1,4));
        pnd_Fcpl720a_Pnd_Cntl_Text = pnd_Fcpl720a_Pnd_Cntl_Table.newFieldInGroup("pnd_Fcpl720a_Pnd_Cntl_Text", "#CNTL-TEXT", FieldType.STRING, 32);
        pnd_Fcpl720a_Pnd_Rec_Type_Cnt = pnd_Fcpl720a_Pnd_Cntl_Table.newFieldArrayInGroup("pnd_Fcpl720a_Pnd_Rec_Type_Cnt", "#REC-TYPE-CNT", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(0,3));

        this.setRecordName("LdaFcpl720a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl720a_Pnd_Rec_Types_Text.getValue(0).setInitialValue("All records....:");
        pnd_Fcpl720a_Pnd_Rec_Types_Text.getValue(1).setInitialValue("Records type 10:");
        pnd_Fcpl720a_Pnd_Rec_Types_Text.getValue(2).setInitialValue("Records type 20:");
        pnd_Fcpl720a_Pnd_Rec_Types_Text.getValue(3).setInitialValue("Records type 30:");
        pnd_Fcpl720a_Pnd_Cntl_Text.getValue(1).setInitialValue("Check extract - Held checks....:");
        pnd_Fcpl720a_Pnd_Cntl_Text.getValue(2).setInitialValue("Check extract - Non Held checks:");
        pnd_Fcpl720a_Pnd_Cntl_Text.getValue(3).setInitialValue("Check extract - All checks.....:");
        pnd_Fcpl720a_Pnd_Cntl_Text.getValue(4).setInitialValue("Eft extract - All Payments.....:");
    }

    // Constructor
    public LdaFcpl720a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
