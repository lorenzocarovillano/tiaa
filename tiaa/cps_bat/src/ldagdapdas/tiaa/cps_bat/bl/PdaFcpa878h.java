/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:28 PM
**        * FROM NATURAL PDA     : FCPA878H
************************************************************
**        * FILE NAME            : PdaFcpa878h.java
**        * CLASS NAME           : PdaFcpa878h
**        * INSTANCE NAME        : PdaFcpa878h
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa878h extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpa878h;
    private DbsField pnd_Fcpa878h_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Fcpa878h_Cntrct_Mode_Cde;
    private DbsField pnd_Fcpa878h_Cntrct_Option_Cde;
    private DbsField pnd_Fcpa878h_Cntrct_Company_Cde;
    private DbsGroup pnd_Fcpa878h_Ph_Name;
    private DbsField pnd_Fcpa878h_Ph_Last_Name;
    private DbsField pnd_Fcpa878h_Ph_First_Name;
    private DbsField pnd_Fcpa878h_Ph_Middle_Name;
    private DbsField pnd_Fcpa878h_Cntrct_Da_Nbr;
    private DbsField pnd_Fcpa878h_Pymnt_Check_Dte;
    private DbsField pnd_Fcpa878h_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Fcpa878h_Pnd_Current_Page;
    private DbsField pnd_Fcpa878h_Pnd_Check_To_Annt;
    private DbsField pnd_Fcpa878h_Pnd_Gen_Headers;
    private DbsField pnd_Fcpa878h_Pnd_Header_Array;
    private DbsField pnd_Fcpa878h_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Fcpa878h_Cnr_Orgnl_Invrse_Dte;

    public DbsGroup getPnd_Fcpa878h() { return pnd_Fcpa878h; }

    public DbsField getPnd_Fcpa878h_Cntrct_Ppcn_Nbr() { return pnd_Fcpa878h_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Fcpa878h_Cntrct_Mode_Cde() { return pnd_Fcpa878h_Cntrct_Mode_Cde; }

    public DbsField getPnd_Fcpa878h_Cntrct_Option_Cde() { return pnd_Fcpa878h_Cntrct_Option_Cde; }

    public DbsField getPnd_Fcpa878h_Cntrct_Company_Cde() { return pnd_Fcpa878h_Cntrct_Company_Cde; }

    public DbsGroup getPnd_Fcpa878h_Ph_Name() { return pnd_Fcpa878h_Ph_Name; }

    public DbsField getPnd_Fcpa878h_Ph_Last_Name() { return pnd_Fcpa878h_Ph_Last_Name; }

    public DbsField getPnd_Fcpa878h_Ph_First_Name() { return pnd_Fcpa878h_Ph_First_Name; }

    public DbsField getPnd_Fcpa878h_Ph_Middle_Name() { return pnd_Fcpa878h_Ph_Middle_Name; }

    public DbsField getPnd_Fcpa878h_Cntrct_Da_Nbr() { return pnd_Fcpa878h_Cntrct_Da_Nbr; }

    public DbsField getPnd_Fcpa878h_Pymnt_Check_Dte() { return pnd_Fcpa878h_Pymnt_Check_Dte; }

    public DbsField getPnd_Fcpa878h_Pymnt_Settlmnt_Dte() { return pnd_Fcpa878h_Pymnt_Settlmnt_Dte; }

    public DbsField getPnd_Fcpa878h_Pnd_Current_Page() { return pnd_Fcpa878h_Pnd_Current_Page; }

    public DbsField getPnd_Fcpa878h_Pnd_Check_To_Annt() { return pnd_Fcpa878h_Pnd_Check_To_Annt; }

    public DbsField getPnd_Fcpa878h_Pnd_Gen_Headers() { return pnd_Fcpa878h_Pnd_Gen_Headers; }

    public DbsField getPnd_Fcpa878h_Pnd_Header_Array() { return pnd_Fcpa878h_Pnd_Header_Array; }

    public DbsField getPnd_Fcpa878h_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pnd_Fcpa878h_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPnd_Fcpa878h_Cnr_Orgnl_Invrse_Dte() { return pnd_Fcpa878h_Cnr_Orgnl_Invrse_Dte; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpa878h = dbsRecord.newGroupInRecord("pnd_Fcpa878h", "#FCPA878H");
        pnd_Fcpa878h.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpa878h_Cntrct_Ppcn_Nbr = pnd_Fcpa878h.newFieldInGroup("pnd_Fcpa878h_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Fcpa878h_Cntrct_Mode_Cde = pnd_Fcpa878h.newFieldInGroup("pnd_Fcpa878h_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        pnd_Fcpa878h_Cntrct_Option_Cde = pnd_Fcpa878h.newFieldInGroup("pnd_Fcpa878h_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        pnd_Fcpa878h_Cntrct_Company_Cde = pnd_Fcpa878h.newFieldInGroup("pnd_Fcpa878h_Cntrct_Company_Cde", "CNTRCT-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Fcpa878h_Ph_Name = pnd_Fcpa878h.newGroupInGroup("pnd_Fcpa878h_Ph_Name", "PH-NAME");
        pnd_Fcpa878h_Ph_Last_Name = pnd_Fcpa878h_Ph_Name.newFieldInGroup("pnd_Fcpa878h_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        pnd_Fcpa878h_Ph_First_Name = pnd_Fcpa878h_Ph_Name.newFieldInGroup("pnd_Fcpa878h_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        pnd_Fcpa878h_Ph_Middle_Name = pnd_Fcpa878h_Ph_Name.newFieldInGroup("pnd_Fcpa878h_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        pnd_Fcpa878h_Cntrct_Da_Nbr = pnd_Fcpa878h.newFieldArrayInGroup("pnd_Fcpa878h_Cntrct_Da_Nbr", "CNTRCT-DA-NBR", FieldType.STRING, 8, new DbsArrayController(1,
            6));
        pnd_Fcpa878h_Pymnt_Check_Dte = pnd_Fcpa878h.newFieldInGroup("pnd_Fcpa878h_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Fcpa878h_Pymnt_Settlmnt_Dte = pnd_Fcpa878h.newFieldInGroup("pnd_Fcpa878h_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Fcpa878h_Pnd_Current_Page = pnd_Fcpa878h.newFieldInGroup("pnd_Fcpa878h_Pnd_Current_Page", "#CURRENT-PAGE", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpa878h_Pnd_Check_To_Annt = pnd_Fcpa878h.newFieldInGroup("pnd_Fcpa878h_Pnd_Check_To_Annt", "#CHECK-TO-ANNT", FieldType.BOOLEAN);
        pnd_Fcpa878h_Pnd_Gen_Headers = pnd_Fcpa878h.newFieldInGroup("pnd_Fcpa878h_Pnd_Gen_Headers", "#GEN-HEADERS", FieldType.BOOLEAN);
        pnd_Fcpa878h_Pnd_Header_Array = pnd_Fcpa878h.newFieldArrayInGroup("pnd_Fcpa878h_Pnd_Header_Array", "#HEADER-ARRAY", FieldType.STRING, 143, new 
            DbsArrayController(1,4));
        pnd_Fcpa878h_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Fcpa878h.newFieldInGroup("pnd_Fcpa878h_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Fcpa878h_Cnr_Orgnl_Invrse_Dte = pnd_Fcpa878h.newFieldInGroup("pnd_Fcpa878h_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 
            8);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa878h(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

