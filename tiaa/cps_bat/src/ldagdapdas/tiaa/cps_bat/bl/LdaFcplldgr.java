/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:32 PM
**        * FROM NATURAL LDA     : FCPLLDGR
************************************************************
**        * FILE NAME            : LdaFcplldgr.java
**        * CLASS NAME           : LdaFcplldgr
**        * INSTANCE NAME        : LdaFcplldgr
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplldgr extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_fcp_Cons_Ledger;
    private DbsField fcp_Cons_Ledger_Cntrct_Rcrd_Typ;
    private DbsField fcp_Cons_Ledger_Cntrct_Check_Crrncy_Cde;
    private DbsField fcp_Cons_Ledger_Cntl_Cycle_Dte;
    private DbsField fcp_Cons_Ledger_Cntl_Check_Dte;
    private DbsField fcp_Cons_Ledger_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Ledger_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Ledger_Cntrct_Cref_Nbr;
    private DbsField fcp_Cons_Ledger_Pymnt_Settlmnt_Dte;
    private DbsField fcp_Cons_Ledger_Pymnt_Acctg_Dte;
    private DbsField fcp_Cons_Ledger_Pymnt_Intrfce_Dte;
    private DbsField fcp_Cons_Ledger_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup fcp_Cons_Ledger_Pymnt_Prcss_Seq_NbrRedef1;
    private DbsField fcp_Cons_Ledger_Pymnt_Prcss_Seq_Num;
    private DbsField fcp_Cons_Ledger_Pymnt_Instmt_Nbr;
    private DbsField fcp_Cons_Ledger_Count_Castinv_Ledgr;
    private DbsGroup fcp_Cons_Ledger_Inv_Ledgr;
    private DbsField fcp_Cons_Ledger_Inv_Acct_Cde;
    private DbsField fcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr;
    private DbsField fcp_Cons_Ledger_Inv_Acct_Ledgr_Amt;
    private DbsField fcp_Cons_Ledger_Inv_Acct_Ledgr_Ind;
    private DbsField fcp_Cons_Ledger_Count_Castinv_Ledgr_Ovrfl;
    private DbsGroup fcp_Cons_Ledger_Inv_Ledgr_Ovrfl;
    private DbsField fcp_Cons_Ledger_Inv_Acct_Cde_Ovrfl;
    private DbsField fcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr_Ovrfl;
    private DbsField fcp_Cons_Ledger_Inv_Acct_Ledgr_Amt_Ovrfl;
    private DbsField fcp_Cons_Ledger_Inv_Acct_Ledgr_Ind_Ovrfl;
    private DbsField fcp_Cons_Ledger_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField fcp_Cons_Ledger_Cntrct_Annty_Ins_Type;
    private DbsField fcp_Cons_Ledger_Rcrd_Intrfce_Settlmnt_Orgn;
    private DbsField fcp_Cons_Ledger_Rcrd_Orgn_Ppcn_Prcss_Chkdt;

    public DataAccessProgramView getVw_fcp_Cons_Ledger() { return vw_fcp_Cons_Ledger; }

    public DbsField getFcp_Cons_Ledger_Cntrct_Rcrd_Typ() { return fcp_Cons_Ledger_Cntrct_Rcrd_Typ; }

    public DbsField getFcp_Cons_Ledger_Cntrct_Check_Crrncy_Cde() { return fcp_Cons_Ledger_Cntrct_Check_Crrncy_Cde; }

    public DbsField getFcp_Cons_Ledger_Cntl_Cycle_Dte() { return fcp_Cons_Ledger_Cntl_Cycle_Dte; }

    public DbsField getFcp_Cons_Ledger_Cntl_Check_Dte() { return fcp_Cons_Ledger_Cntl_Check_Dte; }

    public DbsField getFcp_Cons_Ledger_Cntrct_Ppcn_Nbr() { return fcp_Cons_Ledger_Cntrct_Ppcn_Nbr; }

    public DbsField getFcp_Cons_Ledger_Cntrct_Orgn_Cde() { return fcp_Cons_Ledger_Cntrct_Orgn_Cde; }

    public DbsField getFcp_Cons_Ledger_Cntrct_Cref_Nbr() { return fcp_Cons_Ledger_Cntrct_Cref_Nbr; }

    public DbsField getFcp_Cons_Ledger_Pymnt_Settlmnt_Dte() { return fcp_Cons_Ledger_Pymnt_Settlmnt_Dte; }

    public DbsField getFcp_Cons_Ledger_Pymnt_Acctg_Dte() { return fcp_Cons_Ledger_Pymnt_Acctg_Dte; }

    public DbsField getFcp_Cons_Ledger_Pymnt_Intrfce_Dte() { return fcp_Cons_Ledger_Pymnt_Intrfce_Dte; }

    public DbsField getFcp_Cons_Ledger_Pymnt_Prcss_Seq_Nbr() { return fcp_Cons_Ledger_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getFcp_Cons_Ledger_Pymnt_Prcss_Seq_NbrRedef1() { return fcp_Cons_Ledger_Pymnt_Prcss_Seq_NbrRedef1; }

    public DbsField getFcp_Cons_Ledger_Pymnt_Prcss_Seq_Num() { return fcp_Cons_Ledger_Pymnt_Prcss_Seq_Num; }

    public DbsField getFcp_Cons_Ledger_Pymnt_Instmt_Nbr() { return fcp_Cons_Ledger_Pymnt_Instmt_Nbr; }

    public DbsField getFcp_Cons_Ledger_Count_Castinv_Ledgr() { return fcp_Cons_Ledger_Count_Castinv_Ledgr; }

    public DbsGroup getFcp_Cons_Ledger_Inv_Ledgr() { return fcp_Cons_Ledger_Inv_Ledgr; }

    public DbsField getFcp_Cons_Ledger_Inv_Acct_Cde() { return fcp_Cons_Ledger_Inv_Acct_Cde; }

    public DbsField getFcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr() { return fcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr; }

    public DbsField getFcp_Cons_Ledger_Inv_Acct_Ledgr_Amt() { return fcp_Cons_Ledger_Inv_Acct_Ledgr_Amt; }

    public DbsField getFcp_Cons_Ledger_Inv_Acct_Ledgr_Ind() { return fcp_Cons_Ledger_Inv_Acct_Ledgr_Ind; }

    public DbsField getFcp_Cons_Ledger_Count_Castinv_Ledgr_Ovrfl() { return fcp_Cons_Ledger_Count_Castinv_Ledgr_Ovrfl; }

    public DbsGroup getFcp_Cons_Ledger_Inv_Ledgr_Ovrfl() { return fcp_Cons_Ledger_Inv_Ledgr_Ovrfl; }

    public DbsField getFcp_Cons_Ledger_Inv_Acct_Cde_Ovrfl() { return fcp_Cons_Ledger_Inv_Acct_Cde_Ovrfl; }

    public DbsField getFcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr_Ovrfl() { return fcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr_Ovrfl; }

    public DbsField getFcp_Cons_Ledger_Inv_Acct_Ledgr_Amt_Ovrfl() { return fcp_Cons_Ledger_Inv_Acct_Ledgr_Amt_Ovrfl; }

    public DbsField getFcp_Cons_Ledger_Inv_Acct_Ledgr_Ind_Ovrfl() { return fcp_Cons_Ledger_Inv_Acct_Ledgr_Ind_Ovrfl; }

    public DbsField getFcp_Cons_Ledger_Cntrct_Cancel_Rdrw_Actvty_Cde() { return fcp_Cons_Ledger_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getFcp_Cons_Ledger_Cntrct_Annty_Ins_Type() { return fcp_Cons_Ledger_Cntrct_Annty_Ins_Type; }

    public DbsField getFcp_Cons_Ledger_Rcrd_Intrfce_Settlmnt_Orgn() { return fcp_Cons_Ledger_Rcrd_Intrfce_Settlmnt_Orgn; }

    public DbsField getFcp_Cons_Ledger_Rcrd_Orgn_Ppcn_Prcss_Chkdt() { return fcp_Cons_Ledger_Rcrd_Orgn_Ppcn_Prcss_Chkdt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_fcp_Cons_Ledger = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Ledger", "FCP-CONS-LEDGER"), "FCP_CONS_LEDGER", "FCP_EFT_GLBL", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_LEDGER"));
        fcp_Cons_Ledger_Cntrct_Rcrd_Typ = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RCRD_TYP");
        fcp_Cons_Ledger_Cntrct_Check_Crrncy_Cde = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CHECK_CRRNCY_CDE");
        fcp_Cons_Ledger_Cntl_Cycle_Dte = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Cntl_Cycle_Dte", "CNTL-CYCLE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTL_CYCLE_DTE");
        fcp_Cons_Ledger_Cntl_Check_Dte = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Cntl_Check_Dte", "CNTL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTL_CHECK_DTE");
        fcp_Cons_Ledger_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Ledger_Cntrct_Orgn_Cde = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Ledger_Cntrct_Cref_Nbr = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_CREF_NBR");
        fcp_Cons_Ledger_Pymnt_Settlmnt_Dte = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "PYMNT_SETTLMNT_DTE");
        fcp_Cons_Ledger_Pymnt_Acctg_Dte = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_ACCTG_DTE");
        fcp_Cons_Ledger_Pymnt_Intrfce_Dte = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_INTRFCE_DTE");
        fcp_Cons_Ledger_Pymnt_Prcss_Seq_Nbr = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        fcp_Cons_Ledger_Pymnt_Prcss_Seq_NbrRedef1 = vw_fcp_Cons_Ledger.getRecord().newGroupInGroup("fcp_Cons_Ledger_Pymnt_Prcss_Seq_NbrRedef1", "Redefines", 
            fcp_Cons_Ledger_Pymnt_Prcss_Seq_Nbr);
        fcp_Cons_Ledger_Pymnt_Prcss_Seq_Num = fcp_Cons_Ledger_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("fcp_Cons_Ledger_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        fcp_Cons_Ledger_Pymnt_Instmt_Nbr = fcp_Cons_Ledger_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("fcp_Cons_Ledger_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        fcp_Cons_Ledger_Count_Castinv_Ledgr = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Count_Castinv_Ledgr", "C*INV-LEDGR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_INV_LEDGR");
        fcp_Cons_Ledger_Inv_Ledgr = vw_fcp_Cons_Ledger.getRecord().newGroupInGroup("fcp_Cons_Ledger_Inv_Ledgr", "INV-LEDGR", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_EFT_GLBL_INV_LEDGR");
        fcp_Cons_Ledger_Inv_Acct_Cde = fcp_Cons_Ledger_Inv_Ledgr.newFieldArrayInGroup("fcp_Cons_Ledger_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_CDE", "FCP_EFT_GLBL_INV_LEDGR");
        fcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr = fcp_Cons_Ledger_Inv_Ledgr.newFieldArrayInGroup("fcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr", "INV-ACCT-LEDGR-NBR", 
            FieldType.STRING, 15, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LEDGR_NBR", "FCP_EFT_GLBL_INV_LEDGR");
        fcp_Cons_Ledger_Inv_Acct_Ledgr_Amt = fcp_Cons_Ledger_Inv_Ledgr.newFieldArrayInGroup("fcp_Cons_Ledger_Inv_Acct_Ledgr_Amt", "INV-ACCT-LEDGR-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LEDGR_AMT", "FCP_EFT_GLBL_INV_LEDGR");
        fcp_Cons_Ledger_Inv_Acct_Ledgr_Ind = fcp_Cons_Ledger_Inv_Ledgr.newFieldArrayInGroup("fcp_Cons_Ledger_Inv_Acct_Ledgr_Ind", "INV-ACCT-LEDGR-IND", 
            FieldType.STRING, 1, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LEDGR_IND", "FCP_EFT_GLBL_INV_LEDGR");
        fcp_Cons_Ledger_Count_Castinv_Ledgr_Ovrfl = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Count_Castinv_Ledgr_Ovrfl", "C*INV-LEDGR-OVRFL", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_INV_LEDGR_OVRFL");
        fcp_Cons_Ledger_Inv_Ledgr_Ovrfl = vw_fcp_Cons_Ledger.getRecord().newGroupInGroup("fcp_Cons_Ledger_Inv_Ledgr_Ovrfl", "INV-LEDGR-OVRFL", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_EFT_GLBL_INV_LEDGR_OVRFL");
        fcp_Cons_Ledger_Inv_Acct_Cde_Ovrfl = fcp_Cons_Ledger_Inv_Ledgr_Ovrfl.newFieldArrayInGroup("fcp_Cons_Ledger_Inv_Acct_Cde_Ovrfl", "INV-ACCT-CDE-OVRFL", 
            FieldType.STRING, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_CDE_OVRFL", "FCP_EFT_GLBL_INV_LEDGR_OVRFL");
        fcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr_Ovrfl = fcp_Cons_Ledger_Inv_Ledgr_Ovrfl.newFieldArrayInGroup("fcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr_Ovrfl", "INV-ACCT-LEDGR-NBR-OVRFL", 
            FieldType.STRING, 15, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LEDGR_NBR_OVRFL", "FCP_EFT_GLBL_INV_LEDGR_OVRFL");
        fcp_Cons_Ledger_Inv_Acct_Ledgr_Amt_Ovrfl = fcp_Cons_Ledger_Inv_Ledgr_Ovrfl.newFieldArrayInGroup("fcp_Cons_Ledger_Inv_Acct_Ledgr_Amt_Ovrfl", "INV-ACCT-LEDGR-AMT-OVRFL", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LEDGR_AMT_OVRFL", "FCP_EFT_GLBL_INV_LEDGR_OVRFL");
        fcp_Cons_Ledger_Inv_Acct_Ledgr_Ind_Ovrfl = fcp_Cons_Ledger_Inv_Ledgr_Ovrfl.newFieldArrayInGroup("fcp_Cons_Ledger_Inv_Acct_Ledgr_Ind_Ovrfl", "INV-ACCT-LEDGR-IND-OVRFL", 
            FieldType.STRING, 1, new DbsArrayController(1,99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LEDGR_IND_OVRFL", "FCP_EFT_GLBL_INV_LEDGR_OVRFL");
        fcp_Cons_Ledger_Cntrct_Cancel_Rdrw_Actvty_Cde = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_ACTVTY_CDE");
        fcp_Cons_Ledger_Cntrct_Annty_Ins_Type = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_ANNTY_INS_TYPE");
        fcp_Cons_Ledger_Rcrd_Intrfce_Settlmnt_Orgn = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Rcrd_Intrfce_Settlmnt_Orgn", "RCRD-INTRFCE-SETTLMNT-ORGN", 
            FieldType.BINARY, 11, RepeatingFieldStrategy.None, "RCRD_INTRFCE_SETTLMNT_ORGN");
        fcp_Cons_Ledger_Rcrd_Orgn_Ppcn_Prcss_Chkdt = vw_fcp_Cons_Ledger.getRecord().newFieldInGroup("fcp_Cons_Ledger_Rcrd_Orgn_Ppcn_Prcss_Chkdt", "RCRD-ORGN-PPCN-PRCSS-CHKDT", 
            FieldType.BINARY, 26, RepeatingFieldStrategy.None, "RCRD_ORGN_PPCN_PRCSS_CHKDT");
        vw_fcp_Cons_Ledger.setUniquePeList();

        this.setRecordName("LdaFcplldgr");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_fcp_Cons_Ledger.reset();
    }

    // Constructor
    public LdaFcplldgr() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
