/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:25 PM
**        * FROM NATURAL PDA     : FCPA800A
************************************************************
**        * FILE NAME            : PdaFcpa800a.java
**        * CLASS NAME           : PdaFcpa800a
**        * INSTANCE NAME        : PdaFcpa800a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa800a extends PdaBase
{
    // Properties
    private DbsGroup pnd_Cps_Totals;
    private DbsGroup pnd_Cps_Totals_Pnd_Group_Flds;
    private DbsField pnd_Cps_Totals_Pnd_Program_Lit;
    private DbsField pnd_Cps_Totals_Pnd_Total_Rec_Id;
    private DbsField pnd_Cps_Totals_Pnd_Pymnt_Check_Dte;
    private DbsField pnd_Cps_Totals_Pnd_Diff_In_Totals;
    private DbsField pnd_Cps_Totals_Pnd_Main_Header;
    private DbsField pnd_Cps_Totals_Pnd_Chk_Coll_Dvdnd_Amt;
    private DbsField pnd_Cps_Totals_Pnd_Eft_Coll_Dvdnd_Amt;
    private DbsField pnd_Cps_Totals_Pnd_Gp_Coll_Dvdnd_Amt;
    private DbsField pnd_Cps_Totals_Pnd_Ir_Coll_Dvdnd_Amt;
    private DbsGroup pnd_Cps_Totals_Pnd_Group_Totals;
    private DbsField pnd_Cps_Totals_Pnd_Title_Lit;
    private DbsGroup pnd_Cps_Totals_Pnd_Controls;
    private DbsField pnd_Cps_Totals_Pnd_Rec_Count;
    private DbsField pnd_Cps_Totals_Pnd_Pay_Count;
    private DbsField pnd_Cps_Totals_Pnd_Cntrct_Amt;
    private DbsField pnd_Cps_Totals_Pnd_Dvdnd_Amt;
    private DbsField pnd_Cps_Totals_Pnd_Cref_Amt;
    private DbsField pnd_Cps_Totals_Pnd_Gross_Amt;
    private DbsField pnd_Cps_Totals_Pnd_Ded_Amt;
    private DbsField pnd_Cps_Totals_Pnd_Tax_Amt;
    private DbsField pnd_Cps_Totals_Pnd_Net_Amt;

    public DbsGroup getPnd_Cps_Totals() { return pnd_Cps_Totals; }

    public DbsGroup getPnd_Cps_Totals_Pnd_Group_Flds() { return pnd_Cps_Totals_Pnd_Group_Flds; }

    public DbsField getPnd_Cps_Totals_Pnd_Program_Lit() { return pnd_Cps_Totals_Pnd_Program_Lit; }

    public DbsField getPnd_Cps_Totals_Pnd_Total_Rec_Id() { return pnd_Cps_Totals_Pnd_Total_Rec_Id; }

    public DbsField getPnd_Cps_Totals_Pnd_Pymnt_Check_Dte() { return pnd_Cps_Totals_Pnd_Pymnt_Check_Dte; }

    public DbsField getPnd_Cps_Totals_Pnd_Diff_In_Totals() { return pnd_Cps_Totals_Pnd_Diff_In_Totals; }

    public DbsField getPnd_Cps_Totals_Pnd_Main_Header() { return pnd_Cps_Totals_Pnd_Main_Header; }

    public DbsField getPnd_Cps_Totals_Pnd_Chk_Coll_Dvdnd_Amt() { return pnd_Cps_Totals_Pnd_Chk_Coll_Dvdnd_Amt; }

    public DbsField getPnd_Cps_Totals_Pnd_Eft_Coll_Dvdnd_Amt() { return pnd_Cps_Totals_Pnd_Eft_Coll_Dvdnd_Amt; }

    public DbsField getPnd_Cps_Totals_Pnd_Gp_Coll_Dvdnd_Amt() { return pnd_Cps_Totals_Pnd_Gp_Coll_Dvdnd_Amt; }

    public DbsField getPnd_Cps_Totals_Pnd_Ir_Coll_Dvdnd_Amt() { return pnd_Cps_Totals_Pnd_Ir_Coll_Dvdnd_Amt; }

    public DbsGroup getPnd_Cps_Totals_Pnd_Group_Totals() { return pnd_Cps_Totals_Pnd_Group_Totals; }

    public DbsField getPnd_Cps_Totals_Pnd_Title_Lit() { return pnd_Cps_Totals_Pnd_Title_Lit; }

    public DbsGroup getPnd_Cps_Totals_Pnd_Controls() { return pnd_Cps_Totals_Pnd_Controls; }

    public DbsField getPnd_Cps_Totals_Pnd_Rec_Count() { return pnd_Cps_Totals_Pnd_Rec_Count; }

    public DbsField getPnd_Cps_Totals_Pnd_Pay_Count() { return pnd_Cps_Totals_Pnd_Pay_Count; }

    public DbsField getPnd_Cps_Totals_Pnd_Cntrct_Amt() { return pnd_Cps_Totals_Pnd_Cntrct_Amt; }

    public DbsField getPnd_Cps_Totals_Pnd_Dvdnd_Amt() { return pnd_Cps_Totals_Pnd_Dvdnd_Amt; }

    public DbsField getPnd_Cps_Totals_Pnd_Cref_Amt() { return pnd_Cps_Totals_Pnd_Cref_Amt; }

    public DbsField getPnd_Cps_Totals_Pnd_Gross_Amt() { return pnd_Cps_Totals_Pnd_Gross_Amt; }

    public DbsField getPnd_Cps_Totals_Pnd_Ded_Amt() { return pnd_Cps_Totals_Pnd_Ded_Amt; }

    public DbsField getPnd_Cps_Totals_Pnd_Tax_Amt() { return pnd_Cps_Totals_Pnd_Tax_Amt; }

    public DbsField getPnd_Cps_Totals_Pnd_Net_Amt() { return pnd_Cps_Totals_Pnd_Net_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Cps_Totals = dbsRecord.newGroupInRecord("pnd_Cps_Totals", "#CPS-TOTALS");
        pnd_Cps_Totals.setParameterOption(ParameterOption.ByReference);
        pnd_Cps_Totals_Pnd_Group_Flds = pnd_Cps_Totals.newGroupInGroup("pnd_Cps_Totals_Pnd_Group_Flds", "#GROUP-FLDS");
        pnd_Cps_Totals_Pnd_Program_Lit = pnd_Cps_Totals_Pnd_Group_Flds.newFieldInGroup("pnd_Cps_Totals_Pnd_Program_Lit", "#PROGRAM-LIT", FieldType.STRING, 
            20);
        pnd_Cps_Totals_Pnd_Total_Rec_Id = pnd_Cps_Totals_Pnd_Group_Flds.newFieldInGroup("pnd_Cps_Totals_Pnd_Total_Rec_Id", "#TOTAL-REC-ID", FieldType.STRING, 
            4);
        pnd_Cps_Totals_Pnd_Pymnt_Check_Dte = pnd_Cps_Totals_Pnd_Group_Flds.newFieldInGroup("pnd_Cps_Totals_Pnd_Pymnt_Check_Dte", "#PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Cps_Totals_Pnd_Diff_In_Totals = pnd_Cps_Totals_Pnd_Group_Flds.newFieldInGroup("pnd_Cps_Totals_Pnd_Diff_In_Totals", "#DIFF-IN-TOTALS", FieldType.BOOLEAN);
        pnd_Cps_Totals_Pnd_Main_Header = pnd_Cps_Totals_Pnd_Group_Flds.newFieldInGroup("pnd_Cps_Totals_Pnd_Main_Header", "#MAIN-HEADER", FieldType.STRING, 
            20);
        pnd_Cps_Totals_Pnd_Chk_Coll_Dvdnd_Amt = pnd_Cps_Totals_Pnd_Group_Flds.newFieldInGroup("pnd_Cps_Totals_Pnd_Chk_Coll_Dvdnd_Amt", "#CHK-COLL-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cps_Totals_Pnd_Eft_Coll_Dvdnd_Amt = pnd_Cps_Totals_Pnd_Group_Flds.newFieldInGroup("pnd_Cps_Totals_Pnd_Eft_Coll_Dvdnd_Amt", "#EFT-COLL-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cps_Totals_Pnd_Gp_Coll_Dvdnd_Amt = pnd_Cps_Totals_Pnd_Group_Flds.newFieldInGroup("pnd_Cps_Totals_Pnd_Gp_Coll_Dvdnd_Amt", "#GP-COLL-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cps_Totals_Pnd_Ir_Coll_Dvdnd_Amt = pnd_Cps_Totals_Pnd_Group_Flds.newFieldInGroup("pnd_Cps_Totals_Pnd_Ir_Coll_Dvdnd_Amt", "#IR-COLL-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cps_Totals_Pnd_Group_Totals = pnd_Cps_Totals.newGroupArrayInGroup("pnd_Cps_Totals_Pnd_Group_Totals", "#GROUP-TOTALS", new DbsArrayController(1,
            3));
        pnd_Cps_Totals_Pnd_Title_Lit = pnd_Cps_Totals_Pnd_Group_Totals.newFieldInGroup("pnd_Cps_Totals_Pnd_Title_Lit", "#TITLE-LIT", FieldType.STRING, 
            20);
        pnd_Cps_Totals_Pnd_Controls = pnd_Cps_Totals_Pnd_Group_Totals.newGroupArrayInGroup("pnd_Cps_Totals_Pnd_Controls", "#CONTROLS", new DbsArrayController(1,
            19));
        pnd_Cps_Totals_Pnd_Rec_Count = pnd_Cps_Totals_Pnd_Controls.newFieldInGroup("pnd_Cps_Totals_Pnd_Rec_Count", "#REC-COUNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Cps_Totals_Pnd_Pay_Count = pnd_Cps_Totals_Pnd_Controls.newFieldInGroup("pnd_Cps_Totals_Pnd_Pay_Count", "#PAY-COUNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Cps_Totals_Pnd_Cntrct_Amt = pnd_Cps_Totals_Pnd_Controls.newFieldInGroup("pnd_Cps_Totals_Pnd_Cntrct_Amt", "#CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Cps_Totals_Pnd_Dvdnd_Amt = pnd_Cps_Totals_Pnd_Controls.newFieldInGroup("pnd_Cps_Totals_Pnd_Dvdnd_Amt", "#DVDND-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Cps_Totals_Pnd_Cref_Amt = pnd_Cps_Totals_Pnd_Controls.newFieldInGroup("pnd_Cps_Totals_Pnd_Cref_Amt", "#CREF-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Cps_Totals_Pnd_Gross_Amt = pnd_Cps_Totals_Pnd_Controls.newFieldInGroup("pnd_Cps_Totals_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Cps_Totals_Pnd_Ded_Amt = pnd_Cps_Totals_Pnd_Controls.newFieldInGroup("pnd_Cps_Totals_Pnd_Ded_Amt", "#DED-AMT", FieldType.PACKED_DECIMAL, 11,
            2);
        pnd_Cps_Totals_Pnd_Tax_Amt = pnd_Cps_Totals_Pnd_Controls.newFieldInGroup("pnd_Cps_Totals_Pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11,
            2);
        pnd_Cps_Totals_Pnd_Net_Amt = pnd_Cps_Totals_Pnd_Controls.newFieldInGroup("pnd_Cps_Totals_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11,
            2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa800a(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

