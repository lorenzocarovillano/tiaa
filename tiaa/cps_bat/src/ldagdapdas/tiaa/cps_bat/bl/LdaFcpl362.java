/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:07 PM
**        * FROM NATURAL LDA     : FCPL362
************************************************************
**        * FILE NAME            : LdaFcpl362.java
**        * CLASS NAME           : LdaFcpl362
**        * INSTANCE NAME        : LdaFcpl362
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl362 extends DbsRecord
{
    // Properties
    private DbsGroup ew_Trans_Type_Table;
    private DbsField ew_Trans_Type_Table_Pnd_Trans_Type_Max;
    private DbsGroup ew_Trans_Type_Table_Ew_Transaction_Type;
    private DbsField ew_Trans_Type_Table_Pnd_Ew_Trans_Code;
    private DbsField ew_Trans_Type_Table_Pnd_Summary_Index;
    private DbsField ew_Trans_Type_Table_Pnd_Ew_Trans_Category;
    private DbsField ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20;
    private DbsField ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40;

    public DbsGroup getEw_Trans_Type_Table() { return ew_Trans_Type_Table; }

    public DbsField getEw_Trans_Type_Table_Pnd_Trans_Type_Max() { return ew_Trans_Type_Table_Pnd_Trans_Type_Max; }

    public DbsGroup getEw_Trans_Type_Table_Ew_Transaction_Type() { return ew_Trans_Type_Table_Ew_Transaction_Type; }

    public DbsField getEw_Trans_Type_Table_Pnd_Ew_Trans_Code() { return ew_Trans_Type_Table_Pnd_Ew_Trans_Code; }

    public DbsField getEw_Trans_Type_Table_Pnd_Summary_Index() { return ew_Trans_Type_Table_Pnd_Summary_Index; }

    public DbsField getEw_Trans_Type_Table_Pnd_Ew_Trans_Category() { return ew_Trans_Type_Table_Pnd_Ew_Trans_Category; }

    public DbsField getEw_Trans_Type_Table_Pnd_Ew_Trans_Desc_20() { return ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20; }

    public DbsField getEw_Trans_Type_Table_Pnd_Ew_Trans_Desc_40() { return ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ew_Trans_Type_Table = newGroupInRecord("ew_Trans_Type_Table", "EW-TRANS-TYPE-TABLE");
        ew_Trans_Type_Table_Pnd_Trans_Type_Max = ew_Trans_Type_Table.newFieldInGroup("ew_Trans_Type_Table_Pnd_Trans_Type_Max", "#TRANS-TYPE-MAX", FieldType.NUMERIC, 
            2);
        ew_Trans_Type_Table_Ew_Transaction_Type = ew_Trans_Type_Table.newGroupArrayInGroup("ew_Trans_Type_Table_Ew_Transaction_Type", "EW-TRANSACTION-TYPE", 
            new DbsArrayController(1,15));
        ew_Trans_Type_Table_Pnd_Ew_Trans_Code = ew_Trans_Type_Table_Ew_Transaction_Type.newFieldInGroup("ew_Trans_Type_Table_Pnd_Ew_Trans_Code", "#EW-TRANS-CODE", 
            FieldType.STRING, 2);
        ew_Trans_Type_Table_Pnd_Summary_Index = ew_Trans_Type_Table_Ew_Transaction_Type.newFieldInGroup("ew_Trans_Type_Table_Pnd_Summary_Index", "#SUMMARY-INDEX", 
            FieldType.NUMERIC, 2);
        ew_Trans_Type_Table_Pnd_Ew_Trans_Category = ew_Trans_Type_Table_Ew_Transaction_Type.newFieldInGroup("ew_Trans_Type_Table_Pnd_Ew_Trans_Category", 
            "#EW-TRANS-CATEGORY", FieldType.STRING, 22);
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20 = ew_Trans_Type_Table_Ew_Transaction_Type.newFieldInGroup("ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20", 
            "#EW-TRANS-DESC-20", FieldType.STRING, 20);
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40 = ew_Trans_Type_Table_Ew_Transaction_Type.newFieldInGroup("ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40", 
            "#EW-TRANS-DESC-40", FieldType.STRING, 40);

        this.setRecordName("LdaFcpl362");
    }

    public void initializeValues() throws Exception
    {
        reset();
        ew_Trans_Type_Table_Pnd_Trans_Type_Max.setInitialValue(15);
        ew_Trans_Type_Table_Pnd_Ew_Trans_Code.getValue(1).setInitialValue("X0");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Code.getValue(2).setInitialValue("X1");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Code.getValue(3).setInitialValue("X4");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Code.getValue(4).setInitialValue("X5");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Code.getValue(5).setInitialValue("X6");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Code.getValue(6).setInitialValue("X7");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Code.getValue(10).setInitialValue("99");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Code.getValue(14).setInitialValue("X3");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Code.getValue(15).setInitialValue("X2");
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(1).setInitialValue(1);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(2).setInitialValue(2);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(3).setInitialValue(2);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(4).setInitialValue(2);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(5).setInitialValue(2);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(6).setInitialValue(3);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(7).setInitialValue(4);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(8).setInitialValue(5);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(9).setInitialValue(6);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(10).setInitialValue(7);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(11).setInitialValue(9);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(12).setInitialValue(10);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(13).setInitialValue(11);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(14).setInitialValue(12);
        ew_Trans_Type_Table_Pnd_Summary_Index.getValue(15).setInitialValue(13);
        ew_Trans_Type_Table_Pnd_Ew_Trans_Category.getValue(1).setInitialValue("Death");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Category.getValue(2).setInitialValue("Single Sum");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Category.getValue(3).setInitialValue("Single Sum");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Category.getValue(4).setInitialValue("Single Sum");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Category.getValue(5).setInitialValue("Single Sum");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Category.getValue(6).setInitialValue("MAC");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Category.getValue(10).setInitialValue("Other");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Category.getValue(14).setInitialValue("Repurchase to Suspense");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Category.getValue(15).setInitialValue("Rollover IRA/Rechar.");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20.getValue(1).setInitialValue("Dth Clm/Lump Sum");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20.getValue(2).setInitialValue("Dir Trans");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20.getValue(3).setInitialValue("Roll To Alt Carrier");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20.getValue(4).setInitialValue("Cash To Inst");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20.getValue(5).setInitialValue("Single Sum Cash");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20.getValue(6).setInitialValue("MAC");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20.getValue(10).setInitialValue("Other");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20.getValue(14).setInitialValue("Repurch To Susp");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_20.getValue(15).setInitialValue("Roll To IRA/Rechar");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40.getValue(1).setInitialValue("Death Claim / Lump Sum");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40.getValue(2).setInitialValue("Direct Transfer");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40.getValue(3).setInitialValue("Rollover To Alternate Carrier");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40.getValue(4).setInitialValue("Cash To Institution");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40.getValue(5).setInitialValue("Single Sum Cash");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40.getValue(6).setInitialValue("MAC (Miscellaneous)");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40.getValue(10).setInitialValue("Other");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40.getValue(14).setInitialValue("Repurchase To Suspense");
        ew_Trans_Type_Table_Pnd_Ew_Trans_Desc_40.getValue(15).setInitialValue("Rollover To IRA / Recharacterization");
    }

    // Constructor
    public LdaFcpl362() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
