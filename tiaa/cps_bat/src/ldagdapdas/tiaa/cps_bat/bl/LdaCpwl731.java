/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:54:10 PM
**        * FROM NATURAL LDA     : CPWL731
************************************************************
**        * FILE NAME            : LdaCpwl731.java
**        * CLASS NAME           : LdaCpwl731
**        * INSTANCE NAME        : LdaCpwl731
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpwl731 extends DbsRecord
{
    // Properties
    private DbsField paid_Check_Record;
    private DbsGroup paid_Check_RecordRedef1;
    private DbsGroup paid_Check_Record_Paid_Check_Hdr;
    private DbsField paid_Check_Record_Pc_Hdr_Rec_Type;
    private DbsField paid_Check_Record_Pc_Hdr_Acct_No;
    private DbsGroup paid_Check_Record_Pc_Hdr_Acct_NoRedef2;
    private DbsField paid_Check_Record_Pc_Hdr_Acct_No_Fill;
    private DbsField paid_Check_Record_Pc_Hdr_Acct_No_9;
    private DbsField paid_Check_Record_Pc_Hdr_As_Of_Date;
    private DbsGroup paid_Check_Record_Pc_Hdr_As_Of_DateRedef3;
    private DbsField paid_Check_Record_Pc_Hdr_As_Of_Date_A;
    private DbsGroup paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef4;
    private DbsField paid_Check_Record_Pc_Hdr_As_Of_Date_Yy;
    private DbsField paid_Check_Record_Pc_Hdr_As_Of_Date_Mmdd;
    private DbsField paid_Check_Record_Pc_Hdr_Filler;
    private DbsGroup paid_Check_RecordRedef5;
    private DbsGroup paid_Check_Record_Paid_Check_Data;
    private DbsField paid_Check_Record_Pc_Data_Acct_No;
    private DbsGroup paid_Check_Record_Pc_Data_Acct_NoRedef6;
    private DbsField paid_Check_Record_Pc_Data_Acct_No_Fill;
    private DbsField paid_Check_Record_Pc_Data_Acct_No_9;
    private DbsField paid_Check_Record_Pc_Data_Chk_Nbr_10;
    private DbsGroup paid_Check_Record_Pc_Data_Chk_Nbr_10Redef7;
    private DbsField paid_Check_Record_Pc_Data_Chk_Nbr_10_A;
    private DbsField paid_Check_Record_Pc_Data_Check_Amt;
    private DbsField paid_Check_Record_Pc_Data_Paid_Date;
    private DbsGroup paid_Check_Record_Pc_Data_Paid_DateRedef8;
    private DbsField paid_Check_Record_Pc_Data_Paid_Date_Yy;
    private DbsField paid_Check_Record_Pc_Data_Paid_Date_Mmdd;
    private DbsField paid_Check_Record_Pc_Data_Trans_Ind;
    private DbsField paid_Check_Record_Pc_Data_Additional;
    private DbsField paid_Check_Record_Pc_Data_Sequence_No;
    private DbsField paid_Check_Record_Pc_Data_Issue_Date;
    private DbsGroup paid_Check_Record_Pc_Data_Issue_DateRedef9;
    private DbsField paid_Check_Record_Pc_Data_Issue_Yy;
    private DbsField paid_Check_Record_Pc_Data_Issue_Mmdd;
    private DbsGroup paid_Check_RecordRedef10;
    private DbsGroup paid_Check_Record_Paid_Check_Trailer;
    private DbsField paid_Check_Record_Pc_Trl_Rec_Type;
    private DbsField paid_Check_Record_Pc_Trl_Total_Amt;
    private DbsField paid_Check_Record_Pc_Trl_Total_Count;
    private DbsField paid_Check_Record_Pc_Trl_Total_Outstanding_Amt;
    private DbsField paid_Check_Record_Pc_Trl_Total_Outstanding_Count;
    private DbsField paid_Check_Record_Pc_Trl_Filler;

    public DbsField getPaid_Check_Record() { return paid_Check_Record; }

    public DbsGroup getPaid_Check_RecordRedef1() { return paid_Check_RecordRedef1; }

    public DbsGroup getPaid_Check_Record_Paid_Check_Hdr() { return paid_Check_Record_Paid_Check_Hdr; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Rec_Type() { return paid_Check_Record_Pc_Hdr_Rec_Type; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Acct_No() { return paid_Check_Record_Pc_Hdr_Acct_No; }

    public DbsGroup getPaid_Check_Record_Pc_Hdr_Acct_NoRedef2() { return paid_Check_Record_Pc_Hdr_Acct_NoRedef2; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Acct_No_Fill() { return paid_Check_Record_Pc_Hdr_Acct_No_Fill; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Acct_No_9() { return paid_Check_Record_Pc_Hdr_Acct_No_9; }

    public DbsField getPaid_Check_Record_Pc_Hdr_As_Of_Date() { return paid_Check_Record_Pc_Hdr_As_Of_Date; }

    public DbsGroup getPaid_Check_Record_Pc_Hdr_As_Of_DateRedef3() { return paid_Check_Record_Pc_Hdr_As_Of_DateRedef3; }

    public DbsField getPaid_Check_Record_Pc_Hdr_As_Of_Date_A() { return paid_Check_Record_Pc_Hdr_As_Of_Date_A; }

    public DbsGroup getPaid_Check_Record_Pc_Hdr_As_Of_Date_ARedef4() { return paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef4; }

    public DbsField getPaid_Check_Record_Pc_Hdr_As_Of_Date_Yy() { return paid_Check_Record_Pc_Hdr_As_Of_Date_Yy; }

    public DbsField getPaid_Check_Record_Pc_Hdr_As_Of_Date_Mmdd() { return paid_Check_Record_Pc_Hdr_As_Of_Date_Mmdd; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Filler() { return paid_Check_Record_Pc_Hdr_Filler; }

    public DbsGroup getPaid_Check_RecordRedef5() { return paid_Check_RecordRedef5; }

    public DbsGroup getPaid_Check_Record_Paid_Check_Data() { return paid_Check_Record_Paid_Check_Data; }

    public DbsField getPaid_Check_Record_Pc_Data_Acct_No() { return paid_Check_Record_Pc_Data_Acct_No; }

    public DbsGroup getPaid_Check_Record_Pc_Data_Acct_NoRedef6() { return paid_Check_Record_Pc_Data_Acct_NoRedef6; }

    public DbsField getPaid_Check_Record_Pc_Data_Acct_No_Fill() { return paid_Check_Record_Pc_Data_Acct_No_Fill; }

    public DbsField getPaid_Check_Record_Pc_Data_Acct_No_9() { return paid_Check_Record_Pc_Data_Acct_No_9; }

    public DbsField getPaid_Check_Record_Pc_Data_Chk_Nbr_10() { return paid_Check_Record_Pc_Data_Chk_Nbr_10; }

    public DbsGroup getPaid_Check_Record_Pc_Data_Chk_Nbr_10Redef7() { return paid_Check_Record_Pc_Data_Chk_Nbr_10Redef7; }

    public DbsField getPaid_Check_Record_Pc_Data_Chk_Nbr_10_A() { return paid_Check_Record_Pc_Data_Chk_Nbr_10_A; }

    public DbsField getPaid_Check_Record_Pc_Data_Check_Amt() { return paid_Check_Record_Pc_Data_Check_Amt; }

    public DbsField getPaid_Check_Record_Pc_Data_Paid_Date() { return paid_Check_Record_Pc_Data_Paid_Date; }

    public DbsGroup getPaid_Check_Record_Pc_Data_Paid_DateRedef8() { return paid_Check_Record_Pc_Data_Paid_DateRedef8; }

    public DbsField getPaid_Check_Record_Pc_Data_Paid_Date_Yy() { return paid_Check_Record_Pc_Data_Paid_Date_Yy; }

    public DbsField getPaid_Check_Record_Pc_Data_Paid_Date_Mmdd() { return paid_Check_Record_Pc_Data_Paid_Date_Mmdd; }

    public DbsField getPaid_Check_Record_Pc_Data_Trans_Ind() { return paid_Check_Record_Pc_Data_Trans_Ind; }

    public DbsField getPaid_Check_Record_Pc_Data_Additional() { return paid_Check_Record_Pc_Data_Additional; }

    public DbsField getPaid_Check_Record_Pc_Data_Sequence_No() { return paid_Check_Record_Pc_Data_Sequence_No; }

    public DbsField getPaid_Check_Record_Pc_Data_Issue_Date() { return paid_Check_Record_Pc_Data_Issue_Date; }

    public DbsGroup getPaid_Check_Record_Pc_Data_Issue_DateRedef9() { return paid_Check_Record_Pc_Data_Issue_DateRedef9; }

    public DbsField getPaid_Check_Record_Pc_Data_Issue_Yy() { return paid_Check_Record_Pc_Data_Issue_Yy; }

    public DbsField getPaid_Check_Record_Pc_Data_Issue_Mmdd() { return paid_Check_Record_Pc_Data_Issue_Mmdd; }

    public DbsGroup getPaid_Check_RecordRedef10() { return paid_Check_RecordRedef10; }

    public DbsGroup getPaid_Check_Record_Paid_Check_Trailer() { return paid_Check_Record_Paid_Check_Trailer; }

    public DbsField getPaid_Check_Record_Pc_Trl_Rec_Type() { return paid_Check_Record_Pc_Trl_Rec_Type; }

    public DbsField getPaid_Check_Record_Pc_Trl_Total_Amt() { return paid_Check_Record_Pc_Trl_Total_Amt; }

    public DbsField getPaid_Check_Record_Pc_Trl_Total_Count() { return paid_Check_Record_Pc_Trl_Total_Count; }

    public DbsField getPaid_Check_Record_Pc_Trl_Total_Outstanding_Amt() { return paid_Check_Record_Pc_Trl_Total_Outstanding_Amt; }

    public DbsField getPaid_Check_Record_Pc_Trl_Total_Outstanding_Count() { return paid_Check_Record_Pc_Trl_Total_Outstanding_Count; }

    public DbsField getPaid_Check_Record_Pc_Trl_Filler() { return paid_Check_Record_Pc_Trl_Filler; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        paid_Check_Record = newFieldInRecord("paid_Check_Record", "PAID-CHECK-RECORD", FieldType.STRING, 67);
        paid_Check_RecordRedef1 = newGroupInRecord("paid_Check_RecordRedef1", "Redefines", paid_Check_Record);
        paid_Check_Record_Paid_Check_Hdr = paid_Check_RecordRedef1.newGroupInGroup("paid_Check_Record_Paid_Check_Hdr", "PAID-CHECK-HDR");
        paid_Check_Record_Pc_Hdr_Rec_Type = paid_Check_Record_Paid_Check_Hdr.newFieldInGroup("paid_Check_Record_Pc_Hdr_Rec_Type", "PC-HDR-REC-TYPE", FieldType.STRING, 
            3);
        paid_Check_Record_Pc_Hdr_Acct_No = paid_Check_Record_Paid_Check_Hdr.newFieldInGroup("paid_Check_Record_Pc_Hdr_Acct_No", "PC-HDR-ACCT-NO", FieldType.NUMERIC, 
            10);
        paid_Check_Record_Pc_Hdr_Acct_NoRedef2 = paid_Check_Record_Paid_Check_Hdr.newGroupInGroup("paid_Check_Record_Pc_Hdr_Acct_NoRedef2", "Redefines", 
            paid_Check_Record_Pc_Hdr_Acct_No);
        paid_Check_Record_Pc_Hdr_Acct_No_Fill = paid_Check_Record_Pc_Hdr_Acct_NoRedef2.newFieldInGroup("paid_Check_Record_Pc_Hdr_Acct_No_Fill", "PC-HDR-ACCT-NO-FILL", 
            FieldType.NUMERIC, 1);
        paid_Check_Record_Pc_Hdr_Acct_No_9 = paid_Check_Record_Pc_Hdr_Acct_NoRedef2.newFieldInGroup("paid_Check_Record_Pc_Hdr_Acct_No_9", "PC-HDR-ACCT-NO-9", 
            FieldType.NUMERIC, 9);
        paid_Check_Record_Pc_Hdr_As_Of_Date = paid_Check_Record_Paid_Check_Hdr.newFieldInGroup("paid_Check_Record_Pc_Hdr_As_Of_Date", "PC-HDR-AS-OF-DATE", 
            FieldType.NUMERIC, 6);
        paid_Check_Record_Pc_Hdr_As_Of_DateRedef3 = paid_Check_Record_Paid_Check_Hdr.newGroupInGroup("paid_Check_Record_Pc_Hdr_As_Of_DateRedef3", "Redefines", 
            paid_Check_Record_Pc_Hdr_As_Of_Date);
        paid_Check_Record_Pc_Hdr_As_Of_Date_A = paid_Check_Record_Pc_Hdr_As_Of_DateRedef3.newFieldInGroup("paid_Check_Record_Pc_Hdr_As_Of_Date_A", "PC-HDR-AS-OF-DATE-A", 
            FieldType.STRING, 6);
        paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef4 = paid_Check_Record_Pc_Hdr_As_Of_DateRedef3.newGroupInGroup("paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef4", 
            "Redefines", paid_Check_Record_Pc_Hdr_As_Of_Date_A);
        paid_Check_Record_Pc_Hdr_As_Of_Date_Yy = paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef4.newFieldInGroup("paid_Check_Record_Pc_Hdr_As_Of_Date_Yy", 
            "PC-HDR-AS-OF-DATE-YY", FieldType.STRING, 2);
        paid_Check_Record_Pc_Hdr_As_Of_Date_Mmdd = paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef4.newFieldInGroup("paid_Check_Record_Pc_Hdr_As_Of_Date_Mmdd", 
            "PC-HDR-AS-OF-DATE-MMDD", FieldType.STRING, 4);
        paid_Check_Record_Pc_Hdr_Filler = paid_Check_Record_Paid_Check_Hdr.newFieldInGroup("paid_Check_Record_Pc_Hdr_Filler", "PC-HDR-FILLER", FieldType.STRING, 
            48);
        paid_Check_RecordRedef5 = newGroupInRecord("paid_Check_RecordRedef5", "Redefines", paid_Check_Record);
        paid_Check_Record_Paid_Check_Data = paid_Check_RecordRedef5.newGroupInGroup("paid_Check_Record_Paid_Check_Data", "PAID-CHECK-DATA");
        paid_Check_Record_Pc_Data_Acct_No = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Acct_No", "PC-DATA-ACCT-NO", 
            FieldType.NUMERIC, 10);
        paid_Check_Record_Pc_Data_Acct_NoRedef6 = paid_Check_Record_Paid_Check_Data.newGroupInGroup("paid_Check_Record_Pc_Data_Acct_NoRedef6", "Redefines", 
            paid_Check_Record_Pc_Data_Acct_No);
        paid_Check_Record_Pc_Data_Acct_No_Fill = paid_Check_Record_Pc_Data_Acct_NoRedef6.newFieldInGroup("paid_Check_Record_Pc_Data_Acct_No_Fill", "PC-DATA-ACCT-NO-FILL", 
            FieldType.NUMERIC, 1);
        paid_Check_Record_Pc_Data_Acct_No_9 = paid_Check_Record_Pc_Data_Acct_NoRedef6.newFieldInGroup("paid_Check_Record_Pc_Data_Acct_No_9", "PC-DATA-ACCT-NO-9", 
            FieldType.NUMERIC, 9);
        paid_Check_Record_Pc_Data_Chk_Nbr_10 = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Chk_Nbr_10", "PC-DATA-CHK-NBR-10", 
            FieldType.NUMERIC, 10);
        paid_Check_Record_Pc_Data_Chk_Nbr_10Redef7 = paid_Check_Record_Paid_Check_Data.newGroupInGroup("paid_Check_Record_Pc_Data_Chk_Nbr_10Redef7", "Redefines", 
            paid_Check_Record_Pc_Data_Chk_Nbr_10);
        paid_Check_Record_Pc_Data_Chk_Nbr_10_A = paid_Check_Record_Pc_Data_Chk_Nbr_10Redef7.newFieldInGroup("paid_Check_Record_Pc_Data_Chk_Nbr_10_A", 
            "PC-DATA-CHK-NBR-10-A", FieldType.STRING, 10);
        paid_Check_Record_Pc_Data_Check_Amt = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Check_Amt", "PC-DATA-CHECK-AMT", 
            FieldType.DECIMAL, 10,2);
        paid_Check_Record_Pc_Data_Paid_Date = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Paid_Date", "PC-DATA-PAID-DATE", 
            FieldType.NUMERIC, 6);
        paid_Check_Record_Pc_Data_Paid_DateRedef8 = paid_Check_Record_Paid_Check_Data.newGroupInGroup("paid_Check_Record_Pc_Data_Paid_DateRedef8", "Redefines", 
            paid_Check_Record_Pc_Data_Paid_Date);
        paid_Check_Record_Pc_Data_Paid_Date_Yy = paid_Check_Record_Pc_Data_Paid_DateRedef8.newFieldInGroup("paid_Check_Record_Pc_Data_Paid_Date_Yy", "PC-DATA-PAID-DATE-YY", 
            FieldType.STRING, 2);
        paid_Check_Record_Pc_Data_Paid_Date_Mmdd = paid_Check_Record_Pc_Data_Paid_DateRedef8.newFieldInGroup("paid_Check_Record_Pc_Data_Paid_Date_Mmdd", 
            "PC-DATA-PAID-DATE-MMDD", FieldType.STRING, 4);
        paid_Check_Record_Pc_Data_Trans_Ind = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Trans_Ind", "PC-DATA-TRANS-IND", 
            FieldType.STRING, 1);
        paid_Check_Record_Pc_Data_Additional = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Additional", "PC-DATA-ADDITIONAL", 
            FieldType.STRING, 15);
        paid_Check_Record_Pc_Data_Sequence_No = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Sequence_No", "PC-DATA-SEQUENCE-NO", 
            FieldType.NUMERIC, 9);
        paid_Check_Record_Pc_Data_Issue_Date = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Issue_Date", "PC-DATA-ISSUE-DATE", 
            FieldType.NUMERIC, 6);
        paid_Check_Record_Pc_Data_Issue_DateRedef9 = paid_Check_Record_Paid_Check_Data.newGroupInGroup("paid_Check_Record_Pc_Data_Issue_DateRedef9", "Redefines", 
            paid_Check_Record_Pc_Data_Issue_Date);
        paid_Check_Record_Pc_Data_Issue_Yy = paid_Check_Record_Pc_Data_Issue_DateRedef9.newFieldInGroup("paid_Check_Record_Pc_Data_Issue_Yy", "PC-DATA-ISSUE-YY", 
            FieldType.STRING, 2);
        paid_Check_Record_Pc_Data_Issue_Mmdd = paid_Check_Record_Pc_Data_Issue_DateRedef9.newFieldInGroup("paid_Check_Record_Pc_Data_Issue_Mmdd", "PC-DATA-ISSUE-MMDD", 
            FieldType.STRING, 4);
        paid_Check_RecordRedef10 = newGroupInRecord("paid_Check_RecordRedef10", "Redefines", paid_Check_Record);
        paid_Check_Record_Paid_Check_Trailer = paid_Check_RecordRedef10.newGroupInGroup("paid_Check_Record_Paid_Check_Trailer", "PAID-CHECK-TRAILER");
        paid_Check_Record_Pc_Trl_Rec_Type = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Rec_Type", "PC-TRL-REC-TYPE", 
            FieldType.STRING, 3);
        paid_Check_Record_Pc_Trl_Total_Amt = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Total_Amt", "PC-TRL-TOTAL-AMT", 
            FieldType.DECIMAL, 12,2);
        paid_Check_Record_Pc_Trl_Total_Count = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Total_Count", "PC-TRL-TOTAL-COUNT", 
            FieldType.NUMERIC, 8);
        paid_Check_Record_Pc_Trl_Total_Outstanding_Amt = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Total_Outstanding_Amt", 
            "PC-TRL-TOTAL-OUTSTANDING-AMT", FieldType.DECIMAL, 12,2);
        paid_Check_Record_Pc_Trl_Total_Outstanding_Count = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Total_Outstanding_Count", 
            "PC-TRL-TOTAL-OUTSTANDING-COUNT", FieldType.NUMERIC, 8);
        paid_Check_Record_Pc_Trl_Filler = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Filler", "PC-TRL-FILLER", FieldType.STRING, 
            24);

        this.setRecordName("LdaCpwl731");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCpwl731() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
