/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:18 PM
**        * FROM NATURAL LDA     : FCPL876A
************************************************************
**        * FILE NAME            : LdaFcpl876a.java
**        * CLASS NAME           : LdaFcpl876a
**        * INSTANCE NAME        : LdaFcpl876a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl876a extends DbsRecord
{
    // Properties
    private DbsField pnd_Fcpl876a;
    private DbsGroup pnd_Fcpl876aRedef1;
    private DbsField pnd_Fcpl876a_Pnd_Text;
    private DbsField pnd_Fcpl876a_Cntrct_Orgn_Cde;
    private DbsField pnd_Fcpl876a_Pymnt_Check_Nbr;
    private DbsField pnd_Fcpl876a_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Fcpl876a_Pnd_Hold_Ind;

    public DbsField getPnd_Fcpl876a() { return pnd_Fcpl876a; }

    public DbsGroup getPnd_Fcpl876aRedef1() { return pnd_Fcpl876aRedef1; }

    public DbsField getPnd_Fcpl876a_Pnd_Text() { return pnd_Fcpl876a_Pnd_Text; }

    public DbsField getPnd_Fcpl876a_Cntrct_Orgn_Cde() { return pnd_Fcpl876a_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Fcpl876a_Pymnt_Check_Nbr() { return pnd_Fcpl876a_Pymnt_Check_Nbr; }

    public DbsField getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr() { return pnd_Fcpl876a_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Fcpl876a_Pnd_Hold_Ind() { return pnd_Fcpl876a_Pnd_Hold_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl876a = newFieldInRecord("pnd_Fcpl876a", "#FCPL876A", FieldType.STRING, 143);
        pnd_Fcpl876aRedef1 = newGroupInRecord("pnd_Fcpl876aRedef1", "Redefines", pnd_Fcpl876a);
        pnd_Fcpl876a_Pnd_Text = pnd_Fcpl876aRedef1.newFieldInGroup("pnd_Fcpl876a_Pnd_Text", "#TEXT", FieldType.STRING, 11);
        pnd_Fcpl876a_Cntrct_Orgn_Cde = pnd_Fcpl876aRedef1.newFieldInGroup("pnd_Fcpl876a_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Fcpl876a_Pymnt_Check_Nbr = pnd_Fcpl876aRedef1.newFieldInGroup("pnd_Fcpl876a_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Fcpl876a_Pymnt_Check_Seq_Nbr = pnd_Fcpl876aRedef1.newFieldInGroup("pnd_Fcpl876a_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.NUMERIC, 
            7);
        pnd_Fcpl876a_Pnd_Hold_Ind = pnd_Fcpl876aRedef1.newFieldInGroup("pnd_Fcpl876a_Pnd_Hold_Ind", "#HOLD-IND", FieldType.BOOLEAN);

        this.setRecordName("LdaFcpl876a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl876a.setInitialValue("!@#SUP00#@!");
    }

    // Constructor
    public LdaFcpl876a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
