/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:58:55 PM
**        * FROM NATURAL LDA     : FCPL110
************************************************************
**        * FILE NAME            : LdaFcpl110.java
**        * CLASS NAME           : LdaFcpl110
**        * INSTANCE NAME        : LdaFcpl110
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl110 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_ra;
    private DbsGroup ra_Rt_Record;
    private DbsField ra_Rt_A_I_Ind;
    private DbsField ra_Rt_Table_Id;
    private DbsField ra_Rt_Short_Key;
    private DbsGroup ra_Rt_Short_KeyRedef1;
    private DbsField ra_Bank_Source_Code;
    private DbsField filler01;
    private DbsField ra_Rt_Long_Key;
    private DbsGroup ra_Rt_Long_KeyRedef2;
    private DbsField ra_Bank_Routing;
    private DbsField ra_Filler1;
    private DbsField ra_Bank_Account;
    private DbsField ra_Filler2;
    private DbsField ra_Rt_Desc1;
    private DbsGroup ra_Rt_Desc1Redef3;
    private DbsField ra_Comp_Name_Label;
    private DbsField ra_Company_Name;
    private DbsField ra_Comp_Addr_Label;
    private DbsField ra_Company_Address;
    private DbsField ra_Comp_Acct_Label;
    private DbsField ra_Company_Account;
    private DbsField ra_Not_Valid_Label;
    private DbsField ra_Not_Valid_Msg;
    private DbsField ra_Filler21;
    private DbsField ra_Rt_Desc2;
    private DbsGroup ra_Rt_Desc2Redef4;
    private DbsField ra_Start_Check_Label;
    private DbsField ra_Start_Check_No;
    private DbsField ra_End_Check_Label;
    private DbsField ra_End_Check_No;
    private DbsField ra_Filler_26;
    private DbsField ra_Start_Seq_Label;
    private DbsField ra_Start_Seq_No;
    private DbsField ra_End_Seq_Label;
    private DbsField ra_End_Seq_No;
    private DbsField ra_Signature_Label;
    private DbsField ra_Bank_Signature_Cde;
    private DbsField ra_Filler_15;
    private DbsField ra_Title_Label;
    private DbsField ra_Title;
    private DbsField ra_Pos_Pay_Label;
    private DbsField ra_Bank_Internal_Pospay_Ind;
    private DbsField ra_Orgn_Label;
    private DbsField ra_Bank_Orgn_Cde;
    private DbsField ra_Filler3;
    private DbsField ra_Rt_Desc3;
    private DbsGroup ra_Rt_Desc3Redef5;
    private DbsField ra_Bank_Name_Label;
    private DbsField ra_Bank_Name;
    private DbsField ra_Bank_Address_Label;
    private DbsField ra_Bank_Address1;
    private DbsField ra_Bank_Above_Label;
    private DbsField ra_Bank_Above_Check_Amt_Nbr;
    private DbsField ra_Bank_Trans_Label;
    private DbsField ra_Bank_Transmit_Ind;
    private DbsField ra_Check_Ind_Label;
    private DbsField ra_Check_Acct_Ind;
    private DbsField ra_Eft_Ind_Label;
    private DbsField ra_Eft_Acct_Ind;
    private DbsField ra_Filler_1;
    private DbsField ra_Bank_Transmission_Label;
    private DbsField ra_Bank_Transmission_Cde;
    private DbsField ra_Filler4;
    private DbsField ra_Rt_Desc4;
    private DbsField ra_Rt_Desc5;
    private DbsField ra_Rt_Eff_From_Ccyymmdd;
    private DbsField ra_Rt_Eff_To_Ccyymmdd;
    private DbsField ra_Rt_Upd_Source;
    private DbsField ra_Rt_Upd_User;
    private DbsField ra_Rt_Upd_Ccyymmdd;
    private DbsField ra_Rt_Upd_Timn;
    private DbsField ra_Rt_Super1;
    private DataAccessProgramView vw_rau;
    private DbsGroup rau_Rt_Record;
    private DbsField rau_Rt_A_I_Ind;
    private DbsField rau_Rt_Table_Id;
    private DbsField rau_Rt_Short_Key;
    private DbsField rau_Rt_Long_Key;
    private DbsField rau_Rt_Desc1;
    private DbsField rau_Rt_Desc2;
    private DbsGroup rau_Rt_Desc2Redef6;
    private DbsField rau_Start_Check_Label;
    private DbsField rau_Start_Check_No;
    private DbsField rau_End_Check_Label;
    private DbsField rau_End_Check_No;
    private DbsField rau_Filler26;
    private DbsField rau_Start_Seq_Label;
    private DbsField rau_Start_Seq_No;
    private DbsField rau_End_Seq_Label;
    private DbsField rau_End_Seq_No;
    private DbsField rau_Signature_Label;
    private DbsField rau_Bank_Signature_Cde;
    private DbsField rau_Filler15;
    private DbsField rau_Title_Label;
    private DbsField rau_Title;
    private DbsField rau_Pos_Pay_Label;
    private DbsField rau_Bank_Internal_Pospay_Ind;
    private DbsField rau_Orgn_Label;
    private DbsField rau_Bank_Orgn_Cde;
    private DbsField rau_Filler3;
    private DbsField rau_Rt_Desc3;
    private DbsField rau_Rt_Desc4;
    private DbsField rau_Rt_Desc5;
    private DbsField rau_Rt_Eff_From_Ccyymmdd;
    private DbsField rau_Rt_Eff_To_Ccyymmdd;
    private DbsField rau_Rt_Upd_Source;
    private DbsField rau_Rt_Upd_User;
    private DbsField rau_Rt_Upd_Ccyymmdd;
    private DbsField rau_Rt_Upd_Timn;

    public DataAccessProgramView getVw_ra() { return vw_ra; }

    public DbsGroup getRa_Rt_Record() { return ra_Rt_Record; }

    public DbsField getRa_Rt_A_I_Ind() { return ra_Rt_A_I_Ind; }

    public DbsField getRa_Rt_Table_Id() { return ra_Rt_Table_Id; }

    public DbsField getRa_Rt_Short_Key() { return ra_Rt_Short_Key; }

    public DbsGroup getRa_Rt_Short_KeyRedef1() { return ra_Rt_Short_KeyRedef1; }

    public DbsField getRa_Bank_Source_Code() { return ra_Bank_Source_Code; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getRa_Rt_Long_Key() { return ra_Rt_Long_Key; }

    public DbsGroup getRa_Rt_Long_KeyRedef2() { return ra_Rt_Long_KeyRedef2; }

    public DbsField getRa_Bank_Routing() { return ra_Bank_Routing; }

    public DbsField getRa_Filler1() { return ra_Filler1; }

    public DbsField getRa_Bank_Account() { return ra_Bank_Account; }

    public DbsField getRa_Filler2() { return ra_Filler2; }

    public DbsField getRa_Rt_Desc1() { return ra_Rt_Desc1; }

    public DbsGroup getRa_Rt_Desc1Redef3() { return ra_Rt_Desc1Redef3; }

    public DbsField getRa_Comp_Name_Label() { return ra_Comp_Name_Label; }

    public DbsField getRa_Company_Name() { return ra_Company_Name; }

    public DbsField getRa_Comp_Addr_Label() { return ra_Comp_Addr_Label; }

    public DbsField getRa_Company_Address() { return ra_Company_Address; }

    public DbsField getRa_Comp_Acct_Label() { return ra_Comp_Acct_Label; }

    public DbsField getRa_Company_Account() { return ra_Company_Account; }

    public DbsField getRa_Not_Valid_Label() { return ra_Not_Valid_Label; }

    public DbsField getRa_Not_Valid_Msg() { return ra_Not_Valid_Msg; }

    public DbsField getRa_Filler21() { return ra_Filler21; }

    public DbsField getRa_Rt_Desc2() { return ra_Rt_Desc2; }

    public DbsGroup getRa_Rt_Desc2Redef4() { return ra_Rt_Desc2Redef4; }

    public DbsField getRa_Start_Check_Label() { return ra_Start_Check_Label; }

    public DbsField getRa_Start_Check_No() { return ra_Start_Check_No; }

    public DbsField getRa_End_Check_Label() { return ra_End_Check_Label; }

    public DbsField getRa_End_Check_No() { return ra_End_Check_No; }

    public DbsField getRa_Filler_26() { return ra_Filler_26; }

    public DbsField getRa_Start_Seq_Label() { return ra_Start_Seq_Label; }

    public DbsField getRa_Start_Seq_No() { return ra_Start_Seq_No; }

    public DbsField getRa_End_Seq_Label() { return ra_End_Seq_Label; }

    public DbsField getRa_End_Seq_No() { return ra_End_Seq_No; }

    public DbsField getRa_Signature_Label() { return ra_Signature_Label; }

    public DbsField getRa_Bank_Signature_Cde() { return ra_Bank_Signature_Cde; }

    public DbsField getRa_Filler_15() { return ra_Filler_15; }

    public DbsField getRa_Title_Label() { return ra_Title_Label; }

    public DbsField getRa_Title() { return ra_Title; }

    public DbsField getRa_Pos_Pay_Label() { return ra_Pos_Pay_Label; }

    public DbsField getRa_Bank_Internal_Pospay_Ind() { return ra_Bank_Internal_Pospay_Ind; }

    public DbsField getRa_Orgn_Label() { return ra_Orgn_Label; }

    public DbsField getRa_Bank_Orgn_Cde() { return ra_Bank_Orgn_Cde; }

    public DbsField getRa_Filler3() { return ra_Filler3; }

    public DbsField getRa_Rt_Desc3() { return ra_Rt_Desc3; }

    public DbsGroup getRa_Rt_Desc3Redef5() { return ra_Rt_Desc3Redef5; }

    public DbsField getRa_Bank_Name_Label() { return ra_Bank_Name_Label; }

    public DbsField getRa_Bank_Name() { return ra_Bank_Name; }

    public DbsField getRa_Bank_Address_Label() { return ra_Bank_Address_Label; }

    public DbsField getRa_Bank_Address1() { return ra_Bank_Address1; }

    public DbsField getRa_Bank_Above_Label() { return ra_Bank_Above_Label; }

    public DbsField getRa_Bank_Above_Check_Amt_Nbr() { return ra_Bank_Above_Check_Amt_Nbr; }

    public DbsField getRa_Bank_Trans_Label() { return ra_Bank_Trans_Label; }

    public DbsField getRa_Bank_Transmit_Ind() { return ra_Bank_Transmit_Ind; }

    public DbsField getRa_Check_Ind_Label() { return ra_Check_Ind_Label; }

    public DbsField getRa_Check_Acct_Ind() { return ra_Check_Acct_Ind; }

    public DbsField getRa_Eft_Ind_Label() { return ra_Eft_Ind_Label; }

    public DbsField getRa_Eft_Acct_Ind() { return ra_Eft_Acct_Ind; }

    public DbsField getRa_Filler_1() { return ra_Filler_1; }

    public DbsField getRa_Bank_Transmission_Label() { return ra_Bank_Transmission_Label; }

    public DbsField getRa_Bank_Transmission_Cde() { return ra_Bank_Transmission_Cde; }

    public DbsField getRa_Filler4() { return ra_Filler4; }

    public DbsField getRa_Rt_Desc4() { return ra_Rt_Desc4; }

    public DbsField getRa_Rt_Desc5() { return ra_Rt_Desc5; }

    public DbsField getRa_Rt_Eff_From_Ccyymmdd() { return ra_Rt_Eff_From_Ccyymmdd; }

    public DbsField getRa_Rt_Eff_To_Ccyymmdd() { return ra_Rt_Eff_To_Ccyymmdd; }

    public DbsField getRa_Rt_Upd_Source() { return ra_Rt_Upd_Source; }

    public DbsField getRa_Rt_Upd_User() { return ra_Rt_Upd_User; }

    public DbsField getRa_Rt_Upd_Ccyymmdd() { return ra_Rt_Upd_Ccyymmdd; }

    public DbsField getRa_Rt_Upd_Timn() { return ra_Rt_Upd_Timn; }

    public DbsField getRa_Rt_Super1() { return ra_Rt_Super1; }

    public DataAccessProgramView getVw_rau() { return vw_rau; }

    public DbsGroup getRau_Rt_Record() { return rau_Rt_Record; }

    public DbsField getRau_Rt_A_I_Ind() { return rau_Rt_A_I_Ind; }

    public DbsField getRau_Rt_Table_Id() { return rau_Rt_Table_Id; }

    public DbsField getRau_Rt_Short_Key() { return rau_Rt_Short_Key; }

    public DbsField getRau_Rt_Long_Key() { return rau_Rt_Long_Key; }

    public DbsField getRau_Rt_Desc1() { return rau_Rt_Desc1; }

    public DbsField getRau_Rt_Desc2() { return rau_Rt_Desc2; }

    public DbsGroup getRau_Rt_Desc2Redef6() { return rau_Rt_Desc2Redef6; }

    public DbsField getRau_Start_Check_Label() { return rau_Start_Check_Label; }

    public DbsField getRau_Start_Check_No() { return rau_Start_Check_No; }

    public DbsField getRau_End_Check_Label() { return rau_End_Check_Label; }

    public DbsField getRau_End_Check_No() { return rau_End_Check_No; }

    public DbsField getRau_Filler26() { return rau_Filler26; }

    public DbsField getRau_Start_Seq_Label() { return rau_Start_Seq_Label; }

    public DbsField getRau_Start_Seq_No() { return rau_Start_Seq_No; }

    public DbsField getRau_End_Seq_Label() { return rau_End_Seq_Label; }

    public DbsField getRau_End_Seq_No() { return rau_End_Seq_No; }

    public DbsField getRau_Signature_Label() { return rau_Signature_Label; }

    public DbsField getRau_Bank_Signature_Cde() { return rau_Bank_Signature_Cde; }

    public DbsField getRau_Filler15() { return rau_Filler15; }

    public DbsField getRau_Title_Label() { return rau_Title_Label; }

    public DbsField getRau_Title() { return rau_Title; }

    public DbsField getRau_Pos_Pay_Label() { return rau_Pos_Pay_Label; }

    public DbsField getRau_Bank_Internal_Pospay_Ind() { return rau_Bank_Internal_Pospay_Ind; }

    public DbsField getRau_Orgn_Label() { return rau_Orgn_Label; }

    public DbsField getRau_Bank_Orgn_Cde() { return rau_Bank_Orgn_Cde; }

    public DbsField getRau_Filler3() { return rau_Filler3; }

    public DbsField getRau_Rt_Desc3() { return rau_Rt_Desc3; }

    public DbsField getRau_Rt_Desc4() { return rau_Rt_Desc4; }

    public DbsField getRau_Rt_Desc5() { return rau_Rt_Desc5; }

    public DbsField getRau_Rt_Eff_From_Ccyymmdd() { return rau_Rt_Eff_From_Ccyymmdd; }

    public DbsField getRau_Rt_Eff_To_Ccyymmdd() { return rau_Rt_Eff_To_Ccyymmdd; }

    public DbsField getRau_Rt_Upd_Source() { return rau_Rt_Upd_Source; }

    public DbsField getRau_Rt_Upd_User() { return rau_Rt_Upd_User; }

    public DbsField getRau_Rt_Upd_Ccyymmdd() { return rau_Rt_Upd_Ccyymmdd; }

    public DbsField getRau_Rt_Upd_Timn() { return rau_Rt_Upd_Timn; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_ra = new DataAccessProgramView(new NameInfo("vw_ra", "RA"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        ra_Rt_Record = vw_ra.getRecord().newGroupInGroup("ra_Rt_Record", "RT-RECORD");
        ra_Rt_A_I_Ind = ra_Rt_Record.newFieldInGroup("ra_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        ra_Rt_Table_Id = ra_Rt_Record.newFieldInGroup("ra_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        ra_Rt_Short_Key = ra_Rt_Record.newFieldInGroup("ra_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        ra_Rt_Short_KeyRedef1 = vw_ra.getRecord().newGroupInGroup("ra_Rt_Short_KeyRedef1", "Redefines", ra_Rt_Short_Key);
        ra_Bank_Source_Code = ra_Rt_Short_KeyRedef1.newFieldInGroup("ra_Bank_Source_Code", "BANK-SOURCE-CODE", FieldType.STRING, 5);
        filler01 = ra_Rt_Short_KeyRedef1.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 15);
        ra_Rt_Long_Key = ra_Rt_Record.newFieldInGroup("ra_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        ra_Rt_Long_KeyRedef2 = vw_ra.getRecord().newGroupInGroup("ra_Rt_Long_KeyRedef2", "Redefines", ra_Rt_Long_Key);
        ra_Bank_Routing = ra_Rt_Long_KeyRedef2.newFieldInGroup("ra_Bank_Routing", "BANK-ROUTING", FieldType.STRING, 9);
        ra_Filler1 = ra_Rt_Long_KeyRedef2.newFieldInGroup("ra_Filler1", "FILLER1", FieldType.STRING, 1);
        ra_Bank_Account = ra_Rt_Long_KeyRedef2.newFieldInGroup("ra_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21);
        ra_Filler2 = ra_Rt_Long_KeyRedef2.newFieldInGroup("ra_Filler2", "FILLER2", FieldType.STRING, 9);
        ra_Rt_Desc1 = ra_Rt_Record.newFieldInGroup("ra_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        ra_Rt_Desc1Redef3 = vw_ra.getRecord().newGroupInGroup("ra_Rt_Desc1Redef3", "Redefines", ra_Rt_Desc1);
        ra_Comp_Name_Label = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Comp_Name_Label", "COMP-NAME-LABEL", FieldType.STRING, 13);
        ra_Company_Name = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Company_Name", "COMPANY-NAME", FieldType.STRING, 62);
        ra_Comp_Addr_Label = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Comp_Addr_Label", "COMP-ADDR-LABEL", FieldType.STRING, 16);
        ra_Company_Address = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Company_Address", "COMPANY-ADDRESS", FieldType.STRING, 59);
        ra_Comp_Acct_Label = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Comp_Acct_Label", "COMP-ACCT-LABEL", FieldType.STRING, 8);
        ra_Company_Account = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Company_Account", "COMPANY-ACCOUNT", FieldType.STRING, 6);
        ra_Not_Valid_Label = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Not_Valid_Label", "NOT-VALID-LABEL", FieldType.STRING, 11);
        ra_Not_Valid_Msg = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Not_Valid_Msg", "NOT-VALID-MSG", FieldType.STRING, 50);
        ra_Filler21 = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Filler21", "FILLER21", FieldType.STRING, 25);
        ra_Rt_Desc2 = ra_Rt_Record.newFieldInGroup("ra_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        ra_Rt_Desc2Redef4 = vw_ra.getRecord().newGroupInGroup("ra_Rt_Desc2Redef4", "Redefines", ra_Rt_Desc2);
        ra_Start_Check_Label = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Start_Check_Label", "START-CHECK-LABEL", FieldType.STRING, 15);
        ra_Start_Check_No = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Start_Check_No", "START-CHECK-NO", FieldType.NUMERIC, 10);
        ra_End_Check_Label = ra_Rt_Desc2Redef4.newFieldInGroup("ra_End_Check_Label", "END-CHECK-LABEL", FieldType.STRING, 14);
        ra_End_Check_No = ra_Rt_Desc2Redef4.newFieldInGroup("ra_End_Check_No", "END-CHECK-NO", FieldType.NUMERIC, 10);
        ra_Filler_26 = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Filler_26", "FILLER-26", FieldType.STRING, 26);
        ra_Start_Seq_Label = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Start_Seq_Label", "START-SEQ-LABEL", FieldType.STRING, 13);
        ra_Start_Seq_No = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Start_Seq_No", "START-SEQ-NO", FieldType.NUMERIC, 7);
        ra_End_Seq_Label = ra_Rt_Desc2Redef4.newFieldInGroup("ra_End_Seq_Label", "END-SEQ-LABEL", FieldType.STRING, 12);
        ra_End_Seq_No = ra_Rt_Desc2Redef4.newFieldInGroup("ra_End_Seq_No", "END-SEQ-NO", FieldType.NUMERIC, 7);
        ra_Signature_Label = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Signature_Label", "SIGNATURE-LABEL", FieldType.STRING, 16);
        ra_Bank_Signature_Cde = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Bank_Signature_Cde", "BANK-SIGNATURE-CDE", FieldType.STRING, 5);
        ra_Filler_15 = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Filler_15", "FILLER-15", FieldType.STRING, 15);
        ra_Title_Label = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Title_Label", "TITLE-LABEL", FieldType.STRING, 6);
        ra_Title = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Title", "TITLE", FieldType.STRING, 69);
        ra_Pos_Pay_Label = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Pos_Pay_Label", "POS-PAY-LABEL", FieldType.STRING, 12);
        ra_Bank_Internal_Pospay_Ind = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Bank_Internal_Pospay_Ind", "BANK-INTERNAL-POSPAY-IND", FieldType.STRING, 1);
        ra_Orgn_Label = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Orgn_Label", "ORGN-LABEL", FieldType.STRING, 5);
        ra_Bank_Orgn_Cde = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Bank_Orgn_Cde", "BANK-ORGN-CDE", FieldType.STRING, 2);
        ra_Filler3 = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Filler3", "FILLER3", FieldType.STRING, 5);
        ra_Rt_Desc3 = ra_Rt_Record.newFieldInGroup("ra_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        ra_Rt_Desc3Redef5 = vw_ra.getRecord().newGroupInGroup("ra_Rt_Desc3Redef5", "Redefines", ra_Rt_Desc3);
        ra_Bank_Name_Label = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Bank_Name_Label", "BANK-NAME-LABEL", FieldType.STRING, 10);
        ra_Bank_Name = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Bank_Name", "BANK-NAME", FieldType.STRING, 65);
        ra_Bank_Address_Label = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Bank_Address_Label", "BANK-ADDRESS-LABEL", FieldType.STRING, 13);
        ra_Bank_Address1 = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Bank_Address1", "BANK-ADDRESS1", FieldType.STRING, 62);
        ra_Bank_Above_Label = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Bank_Above_Label", "BANK-ABOVE-LABEL", FieldType.STRING, 15);
        ra_Bank_Above_Check_Amt_Nbr = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Bank_Above_Check_Amt_Nbr", "BANK-ABOVE-CHECK-AMT-NBR", FieldType.STRING, 10);
        ra_Bank_Trans_Label = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Bank_Trans_Label", "BANK-TRANS-LABEL", FieldType.STRING, 16);
        ra_Bank_Transmit_Ind = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Bank_Transmit_Ind", "BANK-TRANSMIT-IND", FieldType.STRING, 1);
        ra_Check_Ind_Label = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Check_Ind_Label", "CHECK-IND-LABEL", FieldType.STRING, 16);
        ra_Check_Acct_Ind = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Check_Acct_Ind", "CHECK-ACCT-IND", FieldType.STRING, 1);
        ra_Eft_Ind_Label = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Eft_Ind_Label", "EFT-IND-LABEL", FieldType.STRING, 14);
        ra_Eft_Acct_Ind = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Eft_Acct_Ind", "EFT-ACCT-IND", FieldType.STRING, 1);
        ra_Filler_1 = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Filler_1", "FILLER-1", FieldType.STRING, 1);
        ra_Bank_Transmission_Label = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Bank_Transmission_Label", "BANK-TRANSMISSION-LABEL", FieldType.STRING, 16);
        ra_Bank_Transmission_Cde = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Bank_Transmission_Cde", "BANK-TRANSMISSION-CDE", FieldType.STRING, 5);
        ra_Filler4 = ra_Rt_Desc3Redef5.newFieldInGroup("ra_Filler4", "FILLER4", FieldType.STRING, 4);
        ra_Rt_Desc4 = ra_Rt_Record.newFieldInGroup("ra_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        ra_Rt_Desc5 = ra_Rt_Record.newFieldInGroup("ra_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        ra_Rt_Eff_From_Ccyymmdd = ra_Rt_Record.newFieldInGroup("ra_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_FROM_CCYYMMDD");
        ra_Rt_Eff_To_Ccyymmdd = ra_Rt_Record.newFieldInGroup("ra_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        ra_Rt_Upd_Source = ra_Rt_Record.newFieldInGroup("ra_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_SOURCE");
        ra_Rt_Upd_User = ra_Rt_Record.newFieldInGroup("ra_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_USER");
        ra_Rt_Upd_Ccyymmdd = ra_Rt_Record.newFieldInGroup("ra_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_CCYYMMDD");
        ra_Rt_Upd_Timn = ra_Rt_Record.newFieldInGroup("ra_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RT_UPD_TIMN");
        ra_Rt_Super1 = vw_ra.getRecord().newFieldInGroup("ra_Rt_Super1", "RT-SUPER1", FieldType.STRING, 66, RepeatingFieldStrategy.None, "RT_SUPER1");

        vw_rau = new DataAccessProgramView(new NameInfo("vw_rau", "RAU"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        rau_Rt_Record = vw_rau.getRecord().newGroupInGroup("rau_Rt_Record", "RT-RECORD");
        rau_Rt_A_I_Ind = rau_Rt_Record.newFieldInGroup("rau_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rau_Rt_Table_Id = rau_Rt_Record.newFieldInGroup("rau_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        rau_Rt_Short_Key = rau_Rt_Record.newFieldInGroup("rau_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        rau_Rt_Long_Key = rau_Rt_Record.newFieldInGroup("rau_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        rau_Rt_Desc1 = rau_Rt_Record.newFieldInGroup("rau_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        rau_Rt_Desc2 = rau_Rt_Record.newFieldInGroup("rau_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        rau_Rt_Desc2Redef6 = vw_rau.getRecord().newGroupInGroup("rau_Rt_Desc2Redef6", "Redefines", rau_Rt_Desc2);
        rau_Start_Check_Label = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Start_Check_Label", "START-CHECK-LABEL", FieldType.STRING, 15);
        rau_Start_Check_No = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Start_Check_No", "START-CHECK-NO", FieldType.NUMERIC, 10);
        rau_End_Check_Label = rau_Rt_Desc2Redef6.newFieldInGroup("rau_End_Check_Label", "END-CHECK-LABEL", FieldType.STRING, 14);
        rau_End_Check_No = rau_Rt_Desc2Redef6.newFieldInGroup("rau_End_Check_No", "END-CHECK-NO", FieldType.NUMERIC, 10);
        rau_Filler26 = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Filler26", "FILLER26", FieldType.STRING, 26);
        rau_Start_Seq_Label = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Start_Seq_Label", "START-SEQ-LABEL", FieldType.STRING, 13);
        rau_Start_Seq_No = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Start_Seq_No", "START-SEQ-NO", FieldType.NUMERIC, 7);
        rau_End_Seq_Label = rau_Rt_Desc2Redef6.newFieldInGroup("rau_End_Seq_Label", "END-SEQ-LABEL", FieldType.STRING, 12);
        rau_End_Seq_No = rau_Rt_Desc2Redef6.newFieldInGroup("rau_End_Seq_No", "END-SEQ-NO", FieldType.NUMERIC, 7);
        rau_Signature_Label = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Signature_Label", "SIGNATURE-LABEL", FieldType.STRING, 16);
        rau_Bank_Signature_Cde = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Bank_Signature_Cde", "BANK-SIGNATURE-CDE", FieldType.STRING, 5);
        rau_Filler15 = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Filler15", "FILLER15", FieldType.STRING, 15);
        rau_Title_Label = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Title_Label", "TITLE-LABEL", FieldType.STRING, 6);
        rau_Title = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Title", "TITLE", FieldType.STRING, 69);
        rau_Pos_Pay_Label = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Pos_Pay_Label", "POS-PAY-LABEL", FieldType.STRING, 12);
        rau_Bank_Internal_Pospay_Ind = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Bank_Internal_Pospay_Ind", "BANK-INTERNAL-POSPAY-IND", FieldType.STRING, 
            1);
        rau_Orgn_Label = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Orgn_Label", "ORGN-LABEL", FieldType.STRING, 5);
        rau_Bank_Orgn_Cde = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Bank_Orgn_Cde", "BANK-ORGN-CDE", FieldType.STRING, 2);
        rau_Filler3 = rau_Rt_Desc2Redef6.newFieldInGroup("rau_Filler3", "FILLER3", FieldType.STRING, 5);
        rau_Rt_Desc3 = rau_Rt_Record.newFieldInGroup("rau_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        rau_Rt_Desc4 = rau_Rt_Record.newFieldInGroup("rau_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        rau_Rt_Desc5 = rau_Rt_Record.newFieldInGroup("rau_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        rau_Rt_Eff_From_Ccyymmdd = rau_Rt_Record.newFieldInGroup("rau_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_FROM_CCYYMMDD");
        rau_Rt_Eff_To_Ccyymmdd = rau_Rt_Record.newFieldInGroup("rau_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        rau_Rt_Upd_Source = rau_Rt_Record.newFieldInGroup("rau_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_SOURCE");
        rau_Rt_Upd_User = rau_Rt_Record.newFieldInGroup("rau_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_USER");
        rau_Rt_Upd_Ccyymmdd = rau_Rt_Record.newFieldInGroup("rau_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        rau_Rt_Upd_Timn = rau_Rt_Record.newFieldInGroup("rau_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RT_UPD_TIMN");

        this.setRecordName("LdaFcpl110");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_ra.reset();
        vw_rau.reset();
    }

    // Constructor
    public LdaFcpl110() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
