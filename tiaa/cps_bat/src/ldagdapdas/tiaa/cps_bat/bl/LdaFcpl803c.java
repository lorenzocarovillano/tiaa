/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:15 PM
**        * FROM NATURAL LDA     : FCPL803C
************************************************************
**        * FILE NAME            : LdaFcpl803c.java
**        * CLASS NAME           : LdaFcpl803c
**        * INSTANCE NAME        : LdaFcpl803c
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl803c extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl803c;
    private DbsField pnd_Fcpl803c_Pnd_Check_Amt_Text;
    private DbsField pnd_Fcpl803c_Pnd_Stmnt_Text;

    public DbsGroup getPnd_Fcpl803c() { return pnd_Fcpl803c; }

    public DbsField getPnd_Fcpl803c_Pnd_Check_Amt_Text() { return pnd_Fcpl803c_Pnd_Check_Amt_Text; }

    public DbsField getPnd_Fcpl803c_Pnd_Stmnt_Text() { return pnd_Fcpl803c_Pnd_Stmnt_Text; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl803c = newGroupInRecord("pnd_Fcpl803c", "#FCPL803C");
        pnd_Fcpl803c_Pnd_Check_Amt_Text = pnd_Fcpl803c.newFieldInGroup("pnd_Fcpl803c_Pnd_Check_Amt_Text", "#CHECK-AMT-TEXT", FieldType.STRING, 16);
        pnd_Fcpl803c_Pnd_Stmnt_Text = pnd_Fcpl803c.newFieldArrayInGroup("pnd_Fcpl803c_Pnd_Stmnt_Text", "#STMNT-TEXT", FieldType.STRING, 107, new DbsArrayController(1,
            2,1,5));

        this.setRecordName("LdaFcpl803c");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl803c_Pnd_Check_Amt_Text.setInitialValue("15Payment Amount");
        pnd_Fcpl803c_Pnd_Stmnt_Text.getValue(1,1).setInitialValue("17Since the total deductions you requested are equal to your income, there is no payment for this period.");
        pnd_Fcpl803c_Pnd_Stmnt_Text.getValue(1,2).setInitialValue("7If you have any questions, please call the Telephone Counseling Center.");
        pnd_Fcpl803c_Pnd_Stmnt_Text.getValue(1,3).setInitialValue("7Consultants are available weekdays from 8:00 a.m. to 10:00 p.m. ET.");
        pnd_Fcpl803c_Pnd_Stmnt_Text.getValue(1,4).setInitialValue("7");
        pnd_Fcpl803c_Pnd_Stmnt_Text.getValue(1,5).setInitialValue("7");
        pnd_Fcpl803c_Pnd_Stmnt_Text.getValue(2,1).setInitialValue("17This statement shows your TIAA-CREF payment in U.S. dollars.");
        pnd_Fcpl803c_Pnd_Stmnt_Text.getValue(2,2).setInitialValue("7As you requested, it will be converted into your local currency.");
        pnd_Fcpl803c_Pnd_Stmnt_Text.getValue(2,3).setInitialValue("7");
        pnd_Fcpl803c_Pnd_Stmnt_Text.getValue(2,4).setInitialValue("7");
        pnd_Fcpl803c_Pnd_Stmnt_Text.getValue(2,5).setInitialValue("7");
    }

    // Constructor
    public LdaFcpl803c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
