/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:41 PM
**        * FROM NATURAL LDA     : CPULANNT
************************************************************
**        * FILE NAME            : LdaCpulannt.java
**        * CLASS NAME           : LdaCpulannt
**        * INSTANCE NAME        : LdaCpulannt
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpulannt extends DbsRecord
{
    // Properties
    private DbsField annot1_Record;
    private DbsGroup annot1_RecordRedef1;
    private DbsField annot1_Record_Legacy_Eft_Key;
    private DbsGroup annot1_Record_Legacy_Eft_KeyRedef2;
    private DbsField annot1_Record_Key_Contract;
    private DbsField annot1_Record_Key_Payee;
    private DbsField annot1_Record_Key_Origin;
    private DbsField annot1_Record_Key_Inverse_Date;
    private DbsField annot1_Record_Key_Process_Seq;
    private DbsGroup annot1_Record_Key_Process_SeqRedef3;
    private DbsField annot1_Record_Pnd_Seq_A;
    private DbsField annot1_Record_Text_Line1;
    private DbsField annot1_Record_Text_Line2;
    private DbsGroup annot1_Record_Grp;
    private DbsField annot1_Record_Annot_Desc;
    private DbsField annot1_Record_Annot_Dte;
    private DbsField annot1_Record_Annot_Id;
    private DbsField filler01;
    private DbsField annot1_Record_Check_Number;
    private DbsGroup annot1_Record_Check_NumberRedef4;
    private DbsField annot1_Record_Check_Alpha;

    public DbsField getAnnot1_Record() { return annot1_Record; }

    public DbsGroup getAnnot1_RecordRedef1() { return annot1_RecordRedef1; }

    public DbsField getAnnot1_Record_Legacy_Eft_Key() { return annot1_Record_Legacy_Eft_Key; }

    public DbsGroup getAnnot1_Record_Legacy_Eft_KeyRedef2() { return annot1_Record_Legacy_Eft_KeyRedef2; }

    public DbsField getAnnot1_Record_Key_Contract() { return annot1_Record_Key_Contract; }

    public DbsField getAnnot1_Record_Key_Payee() { return annot1_Record_Key_Payee; }

    public DbsField getAnnot1_Record_Key_Origin() { return annot1_Record_Key_Origin; }

    public DbsField getAnnot1_Record_Key_Inverse_Date() { return annot1_Record_Key_Inverse_Date; }

    public DbsField getAnnot1_Record_Key_Process_Seq() { return annot1_Record_Key_Process_Seq; }

    public DbsGroup getAnnot1_Record_Key_Process_SeqRedef3() { return annot1_Record_Key_Process_SeqRedef3; }

    public DbsField getAnnot1_Record_Pnd_Seq_A() { return annot1_Record_Pnd_Seq_A; }

    public DbsField getAnnot1_Record_Text_Line1() { return annot1_Record_Text_Line1; }

    public DbsField getAnnot1_Record_Text_Line2() { return annot1_Record_Text_Line2; }

    public DbsGroup getAnnot1_Record_Grp() { return annot1_Record_Grp; }

    public DbsField getAnnot1_Record_Annot_Desc() { return annot1_Record_Annot_Desc; }

    public DbsField getAnnot1_Record_Annot_Dte() { return annot1_Record_Annot_Dte; }

    public DbsField getAnnot1_Record_Annot_Id() { return annot1_Record_Annot_Id; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getAnnot1_Record_Check_Number() { return annot1_Record_Check_Number; }

    public DbsGroup getAnnot1_Record_Check_NumberRedef4() { return annot1_Record_Check_NumberRedef4; }

    public DbsField getAnnot1_Record_Check_Alpha() { return annot1_Record_Check_Alpha; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        annot1_Record = newFieldInRecord("annot1_Record", "ANNOT1-RECORD", FieldType.STRING, 900);
        annot1_RecordRedef1 = newGroupInRecord("annot1_RecordRedef1", "Redefines", annot1_Record);
        annot1_Record_Legacy_Eft_Key = annot1_RecordRedef1.newFieldInGroup("annot1_Record_Legacy_Eft_Key", "LEGACY-EFT-KEY", FieldType.STRING, 33);
        annot1_Record_Legacy_Eft_KeyRedef2 = annot1_RecordRedef1.newGroupInGroup("annot1_Record_Legacy_Eft_KeyRedef2", "Redefines", annot1_Record_Legacy_Eft_Key);
        annot1_Record_Key_Contract = annot1_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("annot1_Record_Key_Contract", "KEY-CONTRACT", FieldType.STRING, 
            10);
        annot1_Record_Key_Payee = annot1_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("annot1_Record_Key_Payee", "KEY-PAYEE", FieldType.STRING, 4);
        annot1_Record_Key_Origin = annot1_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("annot1_Record_Key_Origin", "KEY-ORIGIN", FieldType.STRING, 2);
        annot1_Record_Key_Inverse_Date = annot1_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("annot1_Record_Key_Inverse_Date", "KEY-INVERSE-DATE", FieldType.NUMERIC, 
            8);
        annot1_Record_Key_Process_Seq = annot1_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("annot1_Record_Key_Process_Seq", "KEY-PROCESS-SEQ", FieldType.NUMERIC, 
            9);
        annot1_Record_Key_Process_SeqRedef3 = annot1_Record_Legacy_Eft_KeyRedef2.newGroupInGroup("annot1_Record_Key_Process_SeqRedef3", "Redefines", annot1_Record_Key_Process_Seq);
        annot1_Record_Pnd_Seq_A = annot1_Record_Key_Process_SeqRedef3.newFieldInGroup("annot1_Record_Pnd_Seq_A", "#SEQ-A", FieldType.STRING, 7);
        annot1_Record_Text_Line1 = annot1_RecordRedef1.newFieldInGroup("annot1_Record_Text_Line1", "TEXT-LINE1", FieldType.STRING, 70);
        annot1_Record_Text_Line2 = annot1_RecordRedef1.newFieldInGroup("annot1_Record_Text_Line2", "TEXT-LINE2", FieldType.STRING, 70);
        annot1_Record_Grp = annot1_RecordRedef1.newGroupArrayInGroup("annot1_Record_Grp", "GRP", new DbsArrayController(1,20));
        annot1_Record_Annot_Desc = annot1_Record_Grp.newFieldInGroup("annot1_Record_Annot_Desc", "ANNOT-DESC", FieldType.STRING, 22);
        annot1_Record_Annot_Dte = annot1_Record_Grp.newFieldInGroup("annot1_Record_Annot_Dte", "ANNOT-DTE", FieldType.STRING, 8);
        annot1_Record_Annot_Id = annot1_Record_Grp.newFieldInGroup("annot1_Record_Annot_Id", "ANNOT-ID", FieldType.STRING, 3);
        filler01 = annot1_RecordRedef1.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 57);
        annot1_Record_Check_Number = annot1_RecordRedef1.newFieldInGroup("annot1_Record_Check_Number", "CHECK-NUMBER", FieldType.NUMERIC, 10);
        annot1_Record_Check_NumberRedef4 = annot1_RecordRedef1.newGroupInGroup("annot1_Record_Check_NumberRedef4", "Redefines", annot1_Record_Check_Number);
        annot1_Record_Check_Alpha = annot1_Record_Check_NumberRedef4.newFieldInGroup("annot1_Record_Check_Alpha", "CHECK-ALPHA", FieldType.STRING, 10);

        this.setRecordName("LdaCpulannt");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCpulannt() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
