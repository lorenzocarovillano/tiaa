/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:07 PM
**        * FROM NATURAL LDA     : FCPL300
************************************************************
**        * FILE NAME            : LdaFcpl300.java
**        * CLASS NAME           : LdaFcpl300
**        * INSTANCE NAME        : LdaFcpl300
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl300 extends DbsRecord
{
    // Properties
    private DbsField pnd_Record_Input;
    private DbsGroup pnd_Record_InputRedef1;
    private DbsField pnd_Record_Input_Pnd_Record_Type_1;
    private DbsField pnd_Record_Input_Pnd_Account_Number_1;
    private DbsField pnd_Record_Input_Pnd_Pd_Dte_1;
    private DbsField pnd_Record_Input_Pnd_Transit_Number_1;
    private DbsField pnd_Record_Input_Pnd_Client_Number_1;
    private DbsField pnd_Record_Input_Pnd_Bank_Name_1;
    private DbsGroup pnd_Record_InputRedef2;
    private DbsField pnd_Record_Input_Pnd_Record_Type_2;
    private DbsField pnd_Record_Input_Pnd_Account_Number_2;
    private DbsField pnd_Record_Input_Pnd_Serial_Number_2;
    private DbsField pnd_Record_Input_Pnd_Amount_2;
    private DbsField pnd_Record_Input_Pnd_Stop_Ind_2;
    private DbsField pnd_Record_Input_Pnd_Item_Number_2;
    private DbsGroup pnd_Record_InputRedef3;
    private DbsField pnd_Record_Input_Pnd_Record_Type_3;
    private DbsField pnd_Record_Input_Pnd_Account_Number_3;
    private DbsField pnd_Record_Input_Pnd_Transmission_Dte_3;
    private DbsField pnd_Record_Input_Pnd_Batch_Cnt_3;
    private DbsField pnd_Record_Input_Pnd_Batch_Amt_3;

    public DbsField getPnd_Record_Input() { return pnd_Record_Input; }

    public DbsGroup getPnd_Record_InputRedef1() { return pnd_Record_InputRedef1; }

    public DbsField getPnd_Record_Input_Pnd_Record_Type_1() { return pnd_Record_Input_Pnd_Record_Type_1; }

    public DbsField getPnd_Record_Input_Pnd_Account_Number_1() { return pnd_Record_Input_Pnd_Account_Number_1; }

    public DbsField getPnd_Record_Input_Pnd_Pd_Dte_1() { return pnd_Record_Input_Pnd_Pd_Dte_1; }

    public DbsField getPnd_Record_Input_Pnd_Transit_Number_1() { return pnd_Record_Input_Pnd_Transit_Number_1; }

    public DbsField getPnd_Record_Input_Pnd_Client_Number_1() { return pnd_Record_Input_Pnd_Client_Number_1; }

    public DbsField getPnd_Record_Input_Pnd_Bank_Name_1() { return pnd_Record_Input_Pnd_Bank_Name_1; }

    public DbsGroup getPnd_Record_InputRedef2() { return pnd_Record_InputRedef2; }

    public DbsField getPnd_Record_Input_Pnd_Record_Type_2() { return pnd_Record_Input_Pnd_Record_Type_2; }

    public DbsField getPnd_Record_Input_Pnd_Account_Number_2() { return pnd_Record_Input_Pnd_Account_Number_2; }

    public DbsField getPnd_Record_Input_Pnd_Serial_Number_2() { return pnd_Record_Input_Pnd_Serial_Number_2; }

    public DbsField getPnd_Record_Input_Pnd_Amount_2() { return pnd_Record_Input_Pnd_Amount_2; }

    public DbsField getPnd_Record_Input_Pnd_Stop_Ind_2() { return pnd_Record_Input_Pnd_Stop_Ind_2; }

    public DbsField getPnd_Record_Input_Pnd_Item_Number_2() { return pnd_Record_Input_Pnd_Item_Number_2; }

    public DbsGroup getPnd_Record_InputRedef3() { return pnd_Record_InputRedef3; }

    public DbsField getPnd_Record_Input_Pnd_Record_Type_3() { return pnd_Record_Input_Pnd_Record_Type_3; }

    public DbsField getPnd_Record_Input_Pnd_Account_Number_3() { return pnd_Record_Input_Pnd_Account_Number_3; }

    public DbsField getPnd_Record_Input_Pnd_Transmission_Dte_3() { return pnd_Record_Input_Pnd_Transmission_Dte_3; }

    public DbsField getPnd_Record_Input_Pnd_Batch_Cnt_3() { return pnd_Record_Input_Pnd_Batch_Cnt_3; }

    public DbsField getPnd_Record_Input_Pnd_Batch_Amt_3() { return pnd_Record_Input_Pnd_Batch_Amt_3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Record_Input = newFieldInRecord("pnd_Record_Input", "#RECORD-INPUT", FieldType.STRING, 43);
        pnd_Record_InputRedef1 = newGroupInRecord("pnd_Record_InputRedef1", "Redefines", pnd_Record_Input);
        pnd_Record_Input_Pnd_Record_Type_1 = pnd_Record_InputRedef1.newFieldInGroup("pnd_Record_Input_Pnd_Record_Type_1", "#RECORD-TYPE-1", FieldType.STRING, 
            1);
        pnd_Record_Input_Pnd_Account_Number_1 = pnd_Record_InputRedef1.newFieldInGroup("pnd_Record_Input_Pnd_Account_Number_1", "#ACCOUNT-NUMBER-1", FieldType.STRING, 
            8);
        pnd_Record_Input_Pnd_Pd_Dte_1 = pnd_Record_InputRedef1.newFieldInGroup("pnd_Record_Input_Pnd_Pd_Dte_1", "#PD-DTE-1", FieldType.STRING, 6);
        pnd_Record_Input_Pnd_Transit_Number_1 = pnd_Record_InputRedef1.newFieldInGroup("pnd_Record_Input_Pnd_Transit_Number_1", "#TRANSIT-NUMBER-1", FieldType.STRING, 
            9);
        pnd_Record_Input_Pnd_Client_Number_1 = pnd_Record_InputRedef1.newFieldInGroup("pnd_Record_Input_Pnd_Client_Number_1", "#CLIENT-NUMBER-1", FieldType.STRING, 
            9);
        pnd_Record_Input_Pnd_Bank_Name_1 = pnd_Record_InputRedef1.newFieldInGroup("pnd_Record_Input_Pnd_Bank_Name_1", "#BANK-NAME-1", FieldType.STRING, 
            10);
        pnd_Record_InputRedef2 = newGroupInRecord("pnd_Record_InputRedef2", "Redefines", pnd_Record_Input);
        pnd_Record_Input_Pnd_Record_Type_2 = pnd_Record_InputRedef2.newFieldInGroup("pnd_Record_Input_Pnd_Record_Type_2", "#RECORD-TYPE-2", FieldType.STRING, 
            1);
        pnd_Record_Input_Pnd_Account_Number_2 = pnd_Record_InputRedef2.newFieldInGroup("pnd_Record_Input_Pnd_Account_Number_2", "#ACCOUNT-NUMBER-2", FieldType.STRING, 
            8);
        pnd_Record_Input_Pnd_Serial_Number_2 = pnd_Record_InputRedef2.newFieldInGroup("pnd_Record_Input_Pnd_Serial_Number_2", "#SERIAL-NUMBER-2", FieldType.STRING, 
            10);
        pnd_Record_Input_Pnd_Amount_2 = pnd_Record_InputRedef2.newFieldInGroup("pnd_Record_Input_Pnd_Amount_2", "#AMOUNT-2", FieldType.STRING, 12);
        pnd_Record_Input_Pnd_Stop_Ind_2 = pnd_Record_InputRedef2.newFieldInGroup("pnd_Record_Input_Pnd_Stop_Ind_2", "#STOP-IND-2", FieldType.STRING, 1);
        pnd_Record_Input_Pnd_Item_Number_2 = pnd_Record_InputRedef2.newFieldInGroup("pnd_Record_Input_Pnd_Item_Number_2", "#ITEM-NUMBER-2", FieldType.STRING, 
            9);
        pnd_Record_InputRedef3 = newGroupInRecord("pnd_Record_InputRedef3", "Redefines", pnd_Record_Input);
        pnd_Record_Input_Pnd_Record_Type_3 = pnd_Record_InputRedef3.newFieldInGroup("pnd_Record_Input_Pnd_Record_Type_3", "#RECORD-TYPE-3", FieldType.STRING, 
            1);
        pnd_Record_Input_Pnd_Account_Number_3 = pnd_Record_InputRedef3.newFieldInGroup("pnd_Record_Input_Pnd_Account_Number_3", "#ACCOUNT-NUMBER-3", FieldType.STRING, 
            8);
        pnd_Record_Input_Pnd_Transmission_Dte_3 = pnd_Record_InputRedef3.newFieldInGroup("pnd_Record_Input_Pnd_Transmission_Dte_3", "#TRANSMISSION-DTE-3", 
            FieldType.STRING, 6);
        pnd_Record_Input_Pnd_Batch_Cnt_3 = pnd_Record_InputRedef3.newFieldInGroup("pnd_Record_Input_Pnd_Batch_Cnt_3", "#BATCH-CNT-3", FieldType.STRING, 
            6);
        pnd_Record_Input_Pnd_Batch_Amt_3 = pnd_Record_InputRedef3.newFieldInGroup("pnd_Record_Input_Pnd_Batch_Amt_3", "#BATCH-AMT-3", FieldType.STRING, 
            12);

        this.setRecordName("LdaFcpl300");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl300() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
