/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:17 PM
**        * FROM NATURAL LDA     : FCPL871
************************************************************
**        * FILE NAME            : LdaFcpl871.java
**        * CLASS NAME           : LdaFcpl871
**        * INSTANCE NAME        : LdaFcpl871
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl871 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Gl_Rec;
    private DbsField pnd_Gl_Rec_Pnd_Gl_Rec_Ext;
    private DbsGroup pnd_Gl_Rec_Pnd_Gl_Rec_ExtRedef1;
    private DbsField filler01;
    private DbsField pnd_Gl_Rec_Filler_2;
    private DbsGroup pnd_Gl_Rec_Pnd_Gl_Detail;
    private DbsField pnd_Gl_Rec_Inv_Acct_Cde;
    private DbsField pnd_Gl_Rec_Inv_Acct_Ledgr_Nbr;
    private DbsField pnd_Gl_Rec_Inv_Acct_Ledgr_Amt;
    private DbsField pnd_Gl_Rec_Inv_Acct_Ledgr_Ind;

    public DbsGroup getPnd_Gl_Rec() { return pnd_Gl_Rec; }

    public DbsField getPnd_Gl_Rec_Pnd_Gl_Rec_Ext() { return pnd_Gl_Rec_Pnd_Gl_Rec_Ext; }

    public DbsGroup getPnd_Gl_Rec_Pnd_Gl_Rec_ExtRedef1() { return pnd_Gl_Rec_Pnd_Gl_Rec_ExtRedef1; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getPnd_Gl_Rec_Filler_2() { return pnd_Gl_Rec_Filler_2; }

    public DbsGroup getPnd_Gl_Rec_Pnd_Gl_Detail() { return pnd_Gl_Rec_Pnd_Gl_Detail; }

    public DbsField getPnd_Gl_Rec_Inv_Acct_Cde() { return pnd_Gl_Rec_Inv_Acct_Cde; }

    public DbsField getPnd_Gl_Rec_Inv_Acct_Ledgr_Nbr() { return pnd_Gl_Rec_Inv_Acct_Ledgr_Nbr; }

    public DbsField getPnd_Gl_Rec_Inv_Acct_Ledgr_Amt() { return pnd_Gl_Rec_Inv_Acct_Ledgr_Amt; }

    public DbsField getPnd_Gl_Rec_Inv_Acct_Ledgr_Ind() { return pnd_Gl_Rec_Inv_Acct_Ledgr_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Gl_Rec = newGroupInRecord("pnd_Gl_Rec", "#GL-REC");
        pnd_Gl_Rec_Pnd_Gl_Rec_Ext = pnd_Gl_Rec.newFieldArrayInGroup("pnd_Gl_Rec_Pnd_Gl_Rec_Ext", "#GL-REC-EXT", FieldType.STRING, 244, new DbsArrayController(1,
            2));
        pnd_Gl_Rec_Pnd_Gl_Rec_ExtRedef1 = pnd_Gl_Rec.newGroupInGroup("pnd_Gl_Rec_Pnd_Gl_Rec_ExtRedef1", "Redefines", pnd_Gl_Rec_Pnd_Gl_Rec_Ext);
        filler01 = pnd_Gl_Rec_Pnd_Gl_Rec_ExtRedef1.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 244);
        pnd_Gl_Rec_Filler_2 = pnd_Gl_Rec_Pnd_Gl_Rec_ExtRedef1.newFieldInGroup("pnd_Gl_Rec_Filler_2", "FILLER-2", FieldType.STRING, 210);
        pnd_Gl_Rec_Pnd_Gl_Detail = pnd_Gl_Rec_Pnd_Gl_Rec_ExtRedef1.newGroupInGroup("pnd_Gl_Rec_Pnd_Gl_Detail", "#GL-DETAIL");
        pnd_Gl_Rec_Inv_Acct_Cde = pnd_Gl_Rec_Pnd_Gl_Detail.newFieldInGroup("pnd_Gl_Rec_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        pnd_Gl_Rec_Inv_Acct_Ledgr_Nbr = pnd_Gl_Rec_Pnd_Gl_Detail.newFieldInGroup("pnd_Gl_Rec_Inv_Acct_Ledgr_Nbr", "INV-ACCT-LEDGR-NBR", FieldType.STRING, 
            15);
        pnd_Gl_Rec_Inv_Acct_Ledgr_Amt = pnd_Gl_Rec_Pnd_Gl_Detail.newFieldInGroup("pnd_Gl_Rec_Inv_Acct_Ledgr_Amt", "INV-ACCT-LEDGR-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Gl_Rec_Inv_Acct_Ledgr_Ind = pnd_Gl_Rec_Pnd_Gl_Detail.newFieldInGroup("pnd_Gl_Rec_Inv_Acct_Ledgr_Ind", "INV-ACCT-LEDGR-IND", FieldType.STRING, 
            1);

        this.setRecordName("LdaFcpl871");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl871() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
