/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:54:08 PM
**        * FROM NATURAL LDA     : CPWL730
************************************************************
**        * FILE NAME            : LdaCpwl730.java
**        * CLASS NAME           : LdaCpwl730
**        * INSTANCE NAME        : LdaCpwl730
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpwl730 extends DbsRecord
{
    // Properties
    private DbsField paid_Check_Record;
    private DbsGroup paid_Check_RecordRedef1;
    private DbsGroup paid_Check_Record_Paid_Check_Hdr;
    private DbsField paid_Check_Record_Pc_Hdr_Rec_Type;
    private DbsField paid_Check_Record_Pc_Hdr_Constant_1;
    private DbsField paid_Check_Record_Pc_Hdr_Acct_No;
    private DbsField paid_Check_Record_Pc_Hdr_Constant_2;
    private DbsField paid_Check_Record_Pc_Hdr_As_Of_Date;
    private DbsGroup paid_Check_Record_Pc_Hdr_As_Of_DateRedef2;
    private DbsField paid_Check_Record_Pc_Hdr_As_Of_Date_A;
    private DbsGroup paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef3;
    private DbsField paid_Check_Record_Pc_Hdr_Paid_Date_Yyyy;
    private DbsField paid_Check_Record_Pc_Hdr_Paid_Date_Mmdd;
    private DbsField paid_Check_Record_Pc_Hdr_Filler;
    private DbsGroup paid_Check_RecordRedef4;
    private DbsGroup paid_Check_Record_Paid_Check_Data;
    private DbsField paid_Check_Record_Pc_Data_Acct_No;
    private DbsField paid_Check_Record_Pc_Data_Chk_Nbr_10;
    private DbsGroup paid_Check_Record_Pc_Data_Chk_Nbr_10Redef5;
    private DbsField paid_Check_Record_Pc_Data_Filler;
    private DbsField paid_Check_Record_Pc_Data_Chk_No_07;
    private DbsField paid_Check_Record_Pc_Data_Check_Amt;
    private DbsField paid_Check_Record_Pc_Data_Paid_Date;
    private DbsGroup paid_Check_Record_Pc_Data_Paid_DateRedef6;
    private DbsField paid_Check_Record_Pc_Data_Paid_Date_Yyyy;
    private DbsField paid_Check_Record_Pc_Data_Paid_Date_Mmdd;
    private DbsField paid_Check_Record_Pc_Data_Trans_Ind;
    private DbsField paid_Check_Record_Pc_Data_Filler_1;
    private DbsField paid_Check_Record_Pc_Data_Filler_2;
    private DbsGroup paid_Check_Record_Pc_Data_Filler_2Redef7;
    private DbsField paid_Check_Record_Pc_Data_Loc_Nbr;
    private DbsField paid_Check_Record_Pc_Data_Filler_3;
    private DbsField paid_Check_Record_Pc_Data_Seq_No;
    private DbsField paid_Check_Record_Pc_Data_Rec_Type;
    private DbsField paid_Check_Record_Pc_Data_Constant_1;
    private DbsGroup paid_Check_RecordRedef8;
    private DbsGroup paid_Check_Record_Paid_Check_Trailer;
    private DbsField paid_Check_Record_Pc_Trl_Rec_Type;
    private DbsField paid_Check_Record_Pc_Trl_Constant;
    private DbsField paid_Check_Record_Pc_Trl_Acct_No;
    private DbsField paid_Check_Record_Pc_Trl_As_Of_Date;
    private DbsField paid_Check_Record_Pc_Trl_Total_Amt;
    private DbsField paid_Check_Record_Pc_Trl_Total_Count;
    private DbsField paid_Check_Record_Pc_Trl_Filler;

    public DbsField getPaid_Check_Record() { return paid_Check_Record; }

    public DbsGroup getPaid_Check_RecordRedef1() { return paid_Check_RecordRedef1; }

    public DbsGroup getPaid_Check_Record_Paid_Check_Hdr() { return paid_Check_Record_Paid_Check_Hdr; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Rec_Type() { return paid_Check_Record_Pc_Hdr_Rec_Type; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Constant_1() { return paid_Check_Record_Pc_Hdr_Constant_1; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Acct_No() { return paid_Check_Record_Pc_Hdr_Acct_No; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Constant_2() { return paid_Check_Record_Pc_Hdr_Constant_2; }

    public DbsField getPaid_Check_Record_Pc_Hdr_As_Of_Date() { return paid_Check_Record_Pc_Hdr_As_Of_Date; }

    public DbsGroup getPaid_Check_Record_Pc_Hdr_As_Of_DateRedef2() { return paid_Check_Record_Pc_Hdr_As_Of_DateRedef2; }

    public DbsField getPaid_Check_Record_Pc_Hdr_As_Of_Date_A() { return paid_Check_Record_Pc_Hdr_As_Of_Date_A; }

    public DbsGroup getPaid_Check_Record_Pc_Hdr_As_Of_Date_ARedef3() { return paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef3; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Paid_Date_Yyyy() { return paid_Check_Record_Pc_Hdr_Paid_Date_Yyyy; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Paid_Date_Mmdd() { return paid_Check_Record_Pc_Hdr_Paid_Date_Mmdd; }

    public DbsField getPaid_Check_Record_Pc_Hdr_Filler() { return paid_Check_Record_Pc_Hdr_Filler; }

    public DbsGroup getPaid_Check_RecordRedef4() { return paid_Check_RecordRedef4; }

    public DbsGroup getPaid_Check_Record_Paid_Check_Data() { return paid_Check_Record_Paid_Check_Data; }

    public DbsField getPaid_Check_Record_Pc_Data_Acct_No() { return paid_Check_Record_Pc_Data_Acct_No; }

    public DbsField getPaid_Check_Record_Pc_Data_Chk_Nbr_10() { return paid_Check_Record_Pc_Data_Chk_Nbr_10; }

    public DbsGroup getPaid_Check_Record_Pc_Data_Chk_Nbr_10Redef5() { return paid_Check_Record_Pc_Data_Chk_Nbr_10Redef5; }

    public DbsField getPaid_Check_Record_Pc_Data_Filler() { return paid_Check_Record_Pc_Data_Filler; }

    public DbsField getPaid_Check_Record_Pc_Data_Chk_No_07() { return paid_Check_Record_Pc_Data_Chk_No_07; }

    public DbsField getPaid_Check_Record_Pc_Data_Check_Amt() { return paid_Check_Record_Pc_Data_Check_Amt; }

    public DbsField getPaid_Check_Record_Pc_Data_Paid_Date() { return paid_Check_Record_Pc_Data_Paid_Date; }

    public DbsGroup getPaid_Check_Record_Pc_Data_Paid_DateRedef6() { return paid_Check_Record_Pc_Data_Paid_DateRedef6; }

    public DbsField getPaid_Check_Record_Pc_Data_Paid_Date_Yyyy() { return paid_Check_Record_Pc_Data_Paid_Date_Yyyy; }

    public DbsField getPaid_Check_Record_Pc_Data_Paid_Date_Mmdd() { return paid_Check_Record_Pc_Data_Paid_Date_Mmdd; }

    public DbsField getPaid_Check_Record_Pc_Data_Trans_Ind() { return paid_Check_Record_Pc_Data_Trans_Ind; }

    public DbsField getPaid_Check_Record_Pc_Data_Filler_1() { return paid_Check_Record_Pc_Data_Filler_1; }

    public DbsField getPaid_Check_Record_Pc_Data_Filler_2() { return paid_Check_Record_Pc_Data_Filler_2; }

    public DbsGroup getPaid_Check_Record_Pc_Data_Filler_2Redef7() { return paid_Check_Record_Pc_Data_Filler_2Redef7; }

    public DbsField getPaid_Check_Record_Pc_Data_Loc_Nbr() { return paid_Check_Record_Pc_Data_Loc_Nbr; }

    public DbsField getPaid_Check_Record_Pc_Data_Filler_3() { return paid_Check_Record_Pc_Data_Filler_3; }

    public DbsField getPaid_Check_Record_Pc_Data_Seq_No() { return paid_Check_Record_Pc_Data_Seq_No; }

    public DbsField getPaid_Check_Record_Pc_Data_Rec_Type() { return paid_Check_Record_Pc_Data_Rec_Type; }

    public DbsField getPaid_Check_Record_Pc_Data_Constant_1() { return paid_Check_Record_Pc_Data_Constant_1; }

    public DbsGroup getPaid_Check_RecordRedef8() { return paid_Check_RecordRedef8; }

    public DbsGroup getPaid_Check_Record_Paid_Check_Trailer() { return paid_Check_Record_Paid_Check_Trailer; }

    public DbsField getPaid_Check_Record_Pc_Trl_Rec_Type() { return paid_Check_Record_Pc_Trl_Rec_Type; }

    public DbsField getPaid_Check_Record_Pc_Trl_Constant() { return paid_Check_Record_Pc_Trl_Constant; }

    public DbsField getPaid_Check_Record_Pc_Trl_Acct_No() { return paid_Check_Record_Pc_Trl_Acct_No; }

    public DbsField getPaid_Check_Record_Pc_Trl_As_Of_Date() { return paid_Check_Record_Pc_Trl_As_Of_Date; }

    public DbsField getPaid_Check_Record_Pc_Trl_Total_Amt() { return paid_Check_Record_Pc_Trl_Total_Amt; }

    public DbsField getPaid_Check_Record_Pc_Trl_Total_Count() { return paid_Check_Record_Pc_Trl_Total_Count; }

    public DbsField getPaid_Check_Record_Pc_Trl_Filler() { return paid_Check_Record_Pc_Trl_Filler; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        paid_Check_Record = newFieldInRecord("paid_Check_Record", "PAID-CHECK-RECORD", FieldType.STRING, 80);
        paid_Check_RecordRedef1 = newGroupInRecord("paid_Check_RecordRedef1", "Redefines", paid_Check_Record);
        paid_Check_Record_Paid_Check_Hdr = paid_Check_RecordRedef1.newGroupInGroup("paid_Check_Record_Paid_Check_Hdr", "PAID-CHECK-HDR");
        paid_Check_Record_Pc_Hdr_Rec_Type = paid_Check_Record_Paid_Check_Hdr.newFieldInGroup("paid_Check_Record_Pc_Hdr_Rec_Type", "PC-HDR-REC-TYPE", FieldType.STRING, 
            2);
        paid_Check_Record_Pc_Hdr_Constant_1 = paid_Check_Record_Paid_Check_Hdr.newFieldInGroup("paid_Check_Record_Pc_Hdr_Constant_1", "PC-HDR-CONSTANT-1", 
            FieldType.STRING, 10);
        paid_Check_Record_Pc_Hdr_Acct_No = paid_Check_Record_Paid_Check_Hdr.newFieldInGroup("paid_Check_Record_Pc_Hdr_Acct_No", "PC-HDR-ACCT-NO", FieldType.NUMERIC, 
            13);
        paid_Check_Record_Pc_Hdr_Constant_2 = paid_Check_Record_Paid_Check_Hdr.newFieldInGroup("paid_Check_Record_Pc_Hdr_Constant_2", "PC-HDR-CONSTANT-2", 
            FieldType.STRING, 8);
        paid_Check_Record_Pc_Hdr_As_Of_Date = paid_Check_Record_Paid_Check_Hdr.newFieldInGroup("paid_Check_Record_Pc_Hdr_As_Of_Date", "PC-HDR-AS-OF-DATE", 
            FieldType.NUMERIC, 8);
        paid_Check_Record_Pc_Hdr_As_Of_DateRedef2 = paid_Check_Record_Paid_Check_Hdr.newGroupInGroup("paid_Check_Record_Pc_Hdr_As_Of_DateRedef2", "Redefines", 
            paid_Check_Record_Pc_Hdr_As_Of_Date);
        paid_Check_Record_Pc_Hdr_As_Of_Date_A = paid_Check_Record_Pc_Hdr_As_Of_DateRedef2.newFieldInGroup("paid_Check_Record_Pc_Hdr_As_Of_Date_A", "PC-HDR-AS-OF-DATE-A", 
            FieldType.STRING, 8);
        paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef3 = paid_Check_Record_Pc_Hdr_As_Of_DateRedef2.newGroupInGroup("paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef3", 
            "Redefines", paid_Check_Record_Pc_Hdr_As_Of_Date_A);
        paid_Check_Record_Pc_Hdr_Paid_Date_Yyyy = paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef3.newFieldInGroup("paid_Check_Record_Pc_Hdr_Paid_Date_Yyyy", 
            "PC-HDR-PAID-DATE-YYYY", FieldType.STRING, 4);
        paid_Check_Record_Pc_Hdr_Paid_Date_Mmdd = paid_Check_Record_Pc_Hdr_As_Of_Date_ARedef3.newFieldInGroup("paid_Check_Record_Pc_Hdr_Paid_Date_Mmdd", 
            "PC-HDR-PAID-DATE-MMDD", FieldType.STRING, 4);
        paid_Check_Record_Pc_Hdr_Filler = paid_Check_Record_Paid_Check_Hdr.newFieldInGroup("paid_Check_Record_Pc_Hdr_Filler", "PC-HDR-FILLER", FieldType.STRING, 
            39);
        paid_Check_RecordRedef4 = newGroupInRecord("paid_Check_RecordRedef4", "Redefines", paid_Check_Record);
        paid_Check_Record_Paid_Check_Data = paid_Check_RecordRedef4.newGroupInGroup("paid_Check_Record_Paid_Check_Data", "PAID-CHECK-DATA");
        paid_Check_Record_Pc_Data_Acct_No = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Acct_No", "PC-DATA-ACCT-NO", 
            FieldType.NUMERIC, 13);
        paid_Check_Record_Pc_Data_Chk_Nbr_10 = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Chk_Nbr_10", "PC-DATA-CHK-NBR-10", 
            FieldType.NUMERIC, 10);
        paid_Check_Record_Pc_Data_Chk_Nbr_10Redef5 = paid_Check_Record_Paid_Check_Data.newGroupInGroup("paid_Check_Record_Pc_Data_Chk_Nbr_10Redef5", "Redefines", 
            paid_Check_Record_Pc_Data_Chk_Nbr_10);
        paid_Check_Record_Pc_Data_Filler = paid_Check_Record_Pc_Data_Chk_Nbr_10Redef5.newFieldInGroup("paid_Check_Record_Pc_Data_Filler", "PC-DATA-FILLER", 
            FieldType.STRING, 3);
        paid_Check_Record_Pc_Data_Chk_No_07 = paid_Check_Record_Pc_Data_Chk_Nbr_10Redef5.newFieldInGroup("paid_Check_Record_Pc_Data_Chk_No_07", "PC-DATA-CHK-NO-07", 
            FieldType.NUMERIC, 7);
        paid_Check_Record_Pc_Data_Check_Amt = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Check_Amt", "PC-DATA-CHECK-AMT", 
            FieldType.DECIMAL, 10,2);
        paid_Check_Record_Pc_Data_Paid_Date = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Paid_Date", "PC-DATA-PAID-DATE", 
            FieldType.NUMERIC, 8);
        paid_Check_Record_Pc_Data_Paid_DateRedef6 = paid_Check_Record_Paid_Check_Data.newGroupInGroup("paid_Check_Record_Pc_Data_Paid_DateRedef6", "Redefines", 
            paid_Check_Record_Pc_Data_Paid_Date);
        paid_Check_Record_Pc_Data_Paid_Date_Yyyy = paid_Check_Record_Pc_Data_Paid_DateRedef6.newFieldInGroup("paid_Check_Record_Pc_Data_Paid_Date_Yyyy", 
            "PC-DATA-PAID-DATE-YYYY", FieldType.STRING, 4);
        paid_Check_Record_Pc_Data_Paid_Date_Mmdd = paid_Check_Record_Pc_Data_Paid_DateRedef6.newFieldInGroup("paid_Check_Record_Pc_Data_Paid_Date_Mmdd", 
            "PC-DATA-PAID-DATE-MMDD", FieldType.STRING, 4);
        paid_Check_Record_Pc_Data_Trans_Ind = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Trans_Ind", "PC-DATA-TRANS-IND", 
            FieldType.STRING, 1);
        paid_Check_Record_Pc_Data_Filler_1 = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Filler_1", "PC-DATA-FILLER-1", 
            FieldType.STRING, 8);
        paid_Check_Record_Pc_Data_Filler_2 = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Filler_2", "PC-DATA-FILLER-2", 
            FieldType.STRING, 15);
        paid_Check_Record_Pc_Data_Filler_2Redef7 = paid_Check_Record_Paid_Check_Data.newGroupInGroup("paid_Check_Record_Pc_Data_Filler_2Redef7", "Redefines", 
            paid_Check_Record_Pc_Data_Filler_2);
        paid_Check_Record_Pc_Data_Loc_Nbr = paid_Check_Record_Pc_Data_Filler_2Redef7.newFieldInGroup("paid_Check_Record_Pc_Data_Loc_Nbr", "PC-DATA-LOC-NBR", 
            FieldType.NUMERIC, 9);
        paid_Check_Record_Pc_Data_Filler_3 = paid_Check_Record_Pc_Data_Filler_2Redef7.newFieldInGroup("paid_Check_Record_Pc_Data_Filler_3", "PC-DATA-FILLER-3", 
            FieldType.STRING, 6);
        paid_Check_Record_Pc_Data_Seq_No = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Seq_No", "PC-DATA-SEQ-NO", FieldType.NUMERIC, 
            10);
        paid_Check_Record_Pc_Data_Rec_Type = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Rec_Type", "PC-DATA-REC-TYPE", 
            FieldType.STRING, 1);
        paid_Check_Record_Pc_Data_Constant_1 = paid_Check_Record_Paid_Check_Data.newFieldInGroup("paid_Check_Record_Pc_Data_Constant_1", "PC-DATA-CONSTANT-1", 
            FieldType.STRING, 4);
        paid_Check_RecordRedef8 = newGroupInRecord("paid_Check_RecordRedef8", "Redefines", paid_Check_Record);
        paid_Check_Record_Paid_Check_Trailer = paid_Check_RecordRedef8.newGroupInGroup("paid_Check_Record_Paid_Check_Trailer", "PAID-CHECK-TRAILER");
        paid_Check_Record_Pc_Trl_Rec_Type = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Rec_Type", "PC-TRL-REC-TYPE", 
            FieldType.STRING, 2);
        paid_Check_Record_Pc_Trl_Constant = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Constant", "PC-TRL-CONSTANT", 
            FieldType.STRING, 10);
        paid_Check_Record_Pc_Trl_Acct_No = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Acct_No", "PC-TRL-ACCT-NO", 
            FieldType.NUMERIC, 13);
        paid_Check_Record_Pc_Trl_As_Of_Date = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_As_Of_Date", "PC-TRL-AS-OF-DATE", 
            FieldType.NUMERIC, 8);
        paid_Check_Record_Pc_Trl_Total_Amt = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Total_Amt", "PC-TRL-TOTAL-AMT", 
            FieldType.DECIMAL, 12,2);
        paid_Check_Record_Pc_Trl_Total_Count = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Total_Count", "PC-TRL-TOTAL-COUNT", 
            FieldType.NUMERIC, 10);
        paid_Check_Record_Pc_Trl_Filler = paid_Check_Record_Paid_Check_Trailer.newFieldInGroup("paid_Check_Record_Pc_Trl_Filler", "PC-TRL-FILLER", FieldType.STRING, 
            25);

        this.setRecordName("LdaCpwl730");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCpwl730() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
