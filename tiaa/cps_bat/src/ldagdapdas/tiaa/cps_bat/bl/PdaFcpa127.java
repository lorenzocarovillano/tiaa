/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:14 PM
**        * FROM NATURAL PDA     : FCPA127
************************************************************
**        * FILE NAME            : PdaFcpa127.java
**        * CLASS NAME           : PdaFcpa127
**        * INSTANCE NAME        : PdaFcpa127
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa127 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Mit_Update_Pda;
    private DbsField pnd_Mit_Update_Pda_Unit_Cde;
    private DbsField pnd_Mit_Update_Pda_Status_Cde;
    private DbsField pnd_Mit_Update_Pda_Trans_Dte;
    private DbsGroup pnd_Mit_Update_Pda_Trans_DteRedef1;
    private DbsField pnd_Mit_Update_Pda_Check_Mld_Dte;
    private DbsField pnd_Mit_Update_Pda_Pnd_Ok_Return_Code;
    private DbsField pnd_Mit_Update_Pda_Pnd_Return_Message;

    public DbsGroup getPnd_Mit_Update_Pda() { return pnd_Mit_Update_Pda; }

    public DbsField getPnd_Mit_Update_Pda_Unit_Cde() { return pnd_Mit_Update_Pda_Unit_Cde; }

    public DbsField getPnd_Mit_Update_Pda_Status_Cde() { return pnd_Mit_Update_Pda_Status_Cde; }

    public DbsField getPnd_Mit_Update_Pda_Trans_Dte() { return pnd_Mit_Update_Pda_Trans_Dte; }

    public DbsGroup getPnd_Mit_Update_Pda_Trans_DteRedef1() { return pnd_Mit_Update_Pda_Trans_DteRedef1; }

    public DbsField getPnd_Mit_Update_Pda_Check_Mld_Dte() { return pnd_Mit_Update_Pda_Check_Mld_Dte; }

    public DbsField getPnd_Mit_Update_Pda_Pnd_Ok_Return_Code() { return pnd_Mit_Update_Pda_Pnd_Ok_Return_Code; }

    public DbsField getPnd_Mit_Update_Pda_Pnd_Return_Message() { return pnd_Mit_Update_Pda_Pnd_Return_Message; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Mit_Update_Pda = dbsRecord.newGroupInRecord("pnd_Mit_Update_Pda", "#MIT-UPDATE-PDA");
        pnd_Mit_Update_Pda.setParameterOption(ParameterOption.ByReference);
        pnd_Mit_Update_Pda_Unit_Cde = pnd_Mit_Update_Pda.newFieldInGroup("pnd_Mit_Update_Pda_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8);
        pnd_Mit_Update_Pda_Status_Cde = pnd_Mit_Update_Pda.newFieldInGroup("pnd_Mit_Update_Pda_Status_Cde", "STATUS-CDE", FieldType.STRING, 4);
        pnd_Mit_Update_Pda_Trans_Dte = pnd_Mit_Update_Pda.newFieldInGroup("pnd_Mit_Update_Pda_Trans_Dte", "TRANS-DTE", FieldType.DATE);
        pnd_Mit_Update_Pda_Trans_DteRedef1 = pnd_Mit_Update_Pda.newGroupInGroup("pnd_Mit_Update_Pda_Trans_DteRedef1", "Redefines", pnd_Mit_Update_Pda_Trans_Dte);
        pnd_Mit_Update_Pda_Check_Mld_Dte = pnd_Mit_Update_Pda_Trans_DteRedef1.newFieldInGroup("pnd_Mit_Update_Pda_Check_Mld_Dte", "CHECK-MLD-DTE", FieldType.DATE);
        pnd_Mit_Update_Pda_Pnd_Ok_Return_Code = pnd_Mit_Update_Pda.newFieldInGroup("pnd_Mit_Update_Pda_Pnd_Ok_Return_Code", "#OK-RETURN-CODE", FieldType.BOOLEAN);
        pnd_Mit_Update_Pda_Pnd_Return_Message = pnd_Mit_Update_Pda.newFieldInGroup("pnd_Mit_Update_Pda_Pnd_Return_Message", "#RETURN-MESSAGE", FieldType.STRING, 
            79);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa127(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

