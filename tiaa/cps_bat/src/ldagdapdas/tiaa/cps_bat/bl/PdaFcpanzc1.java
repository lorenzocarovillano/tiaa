/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:32 PM
**        * FROM NATURAL PDA     : FCPANZC1
************************************************************
**        * FILE NAME            : PdaFcpanzc1.java
**        * CLASS NAME           : PdaFcpanzc1
**        * INSTANCE NAME        : PdaFcpanzc1
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpanzc1 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpanzc1;
    private DbsField pnd_Fcpanzc1_Pnd_Accum_Occur_1;
    private DbsField pnd_Fcpanzc1_Pnd_Accum_Occur_2;

    public DbsGroup getPnd_Fcpanzc1() { return pnd_Fcpanzc1; }

    public DbsField getPnd_Fcpanzc1_Pnd_Accum_Occur_1() { return pnd_Fcpanzc1_Pnd_Accum_Occur_1; }

    public DbsField getPnd_Fcpanzc1_Pnd_Accum_Occur_2() { return pnd_Fcpanzc1_Pnd_Accum_Occur_2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpanzc1 = dbsRecord.newGroupInRecord("pnd_Fcpanzc1", "#FCPANZC1");
        pnd_Fcpanzc1.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpanzc1_Pnd_Accum_Occur_1 = pnd_Fcpanzc1.newFieldInGroup("pnd_Fcpanzc1_Pnd_Accum_Occur_1", "#ACCUM-OCCUR-1", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpanzc1_Pnd_Accum_Occur_2 = pnd_Fcpanzc1.newFieldInGroup("pnd_Fcpanzc1_Pnd_Accum_Occur_2", "#ACCUM-OCCUR-2", FieldType.PACKED_DECIMAL, 3);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpanzc1(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

