/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:57:02 PM
**        * FROM NATURAL LDA     : CWFLACT
************************************************************
**        * FILE NAME            : LdaCwflact.java
**        * CLASS NAME           : LdaCwflact
**        * INSTANCE NAME        : LdaCwflact
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwflact extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_cwf_Who_Act;
    private DbsField cwf_Who_Act_Rqst_Log_Dte_Tme;
    private DbsGroup cwf_Who_Act_Rqst_Log_Dte_TmeRedef1;
    private DbsField cwf_Who_Act_Rqst_Log_Index_Dte;
    private DbsField cwf_Who_Act_Rqst_Log_Index_Tme;
    private DbsField cwf_Who_Act_Strtng_Event_Dte_Tme;
    private DbsField cwf_Who_Act_Endng_Event_Dte_Tme;
    private DbsField cwf_Who_Act_Cmpltd_Actvty_Ind;
    private DbsField cwf_Who_Act_Admin_Unit_Cde;
    private DbsField cwf_Who_Act_Empl_Oprtr_Cde;
    private DbsGroup cwf_Who_Act_Empl_Oprtr_CdeRedef2;
    private DbsField cwf_Who_Act_Empl_Racf_Id;
    private DbsField cwf_Who_Act_Empl_Sffx_Cde;
    private DbsField cwf_Who_Act_Step_Id;
    private DbsField cwf_Who_Act_Admin_Status_Cde;
    private DbsField cwf_Who_Act_Unit_On_Tme_Ind;
    private DbsField cwf_Who_Act_Crtn_Run_Id;
    private DbsField cwf_Who_Act_Actvty_Elpsd_Clndr_Hours;
    private DbsField cwf_Who_Act_Actvty_Elpsd_Bsnss_Hours;
    private DbsField cwf_Who_Act_Invrt_Strtng_Event;
    private DbsField cwf_Who_Act_Dwnld_Updte_Dte_Tme;
    private DbsField cwf_Who_Act_Rt_Sqnce_Nbr;
    private DbsField cwf_Who_Act_Off_Rtng_Ind;
    private DbsField cwf_Who_Act_Msg_Rtrn_Cde;
    private DbsField cwf_Who_Act_Msg_Nbr;

    public DataAccessProgramView getVw_cwf_Who_Act() { return vw_cwf_Who_Act; }

    public DbsField getCwf_Who_Act_Rqst_Log_Dte_Tme() { return cwf_Who_Act_Rqst_Log_Dte_Tme; }

    public DbsGroup getCwf_Who_Act_Rqst_Log_Dte_TmeRedef1() { return cwf_Who_Act_Rqst_Log_Dte_TmeRedef1; }

    public DbsField getCwf_Who_Act_Rqst_Log_Index_Dte() { return cwf_Who_Act_Rqst_Log_Index_Dte; }

    public DbsField getCwf_Who_Act_Rqst_Log_Index_Tme() { return cwf_Who_Act_Rqst_Log_Index_Tme; }

    public DbsField getCwf_Who_Act_Strtng_Event_Dte_Tme() { return cwf_Who_Act_Strtng_Event_Dte_Tme; }

    public DbsField getCwf_Who_Act_Endng_Event_Dte_Tme() { return cwf_Who_Act_Endng_Event_Dte_Tme; }

    public DbsField getCwf_Who_Act_Cmpltd_Actvty_Ind() { return cwf_Who_Act_Cmpltd_Actvty_Ind; }

    public DbsField getCwf_Who_Act_Admin_Unit_Cde() { return cwf_Who_Act_Admin_Unit_Cde; }

    public DbsField getCwf_Who_Act_Empl_Oprtr_Cde() { return cwf_Who_Act_Empl_Oprtr_Cde; }

    public DbsGroup getCwf_Who_Act_Empl_Oprtr_CdeRedef2() { return cwf_Who_Act_Empl_Oprtr_CdeRedef2; }

    public DbsField getCwf_Who_Act_Empl_Racf_Id() { return cwf_Who_Act_Empl_Racf_Id; }

    public DbsField getCwf_Who_Act_Empl_Sffx_Cde() { return cwf_Who_Act_Empl_Sffx_Cde; }

    public DbsField getCwf_Who_Act_Step_Id() { return cwf_Who_Act_Step_Id; }

    public DbsField getCwf_Who_Act_Admin_Status_Cde() { return cwf_Who_Act_Admin_Status_Cde; }

    public DbsField getCwf_Who_Act_Unit_On_Tme_Ind() { return cwf_Who_Act_Unit_On_Tme_Ind; }

    public DbsField getCwf_Who_Act_Crtn_Run_Id() { return cwf_Who_Act_Crtn_Run_Id; }

    public DbsField getCwf_Who_Act_Actvty_Elpsd_Clndr_Hours() { return cwf_Who_Act_Actvty_Elpsd_Clndr_Hours; }

    public DbsField getCwf_Who_Act_Actvty_Elpsd_Bsnss_Hours() { return cwf_Who_Act_Actvty_Elpsd_Bsnss_Hours; }

    public DbsField getCwf_Who_Act_Invrt_Strtng_Event() { return cwf_Who_Act_Invrt_Strtng_Event; }

    public DbsField getCwf_Who_Act_Dwnld_Updte_Dte_Tme() { return cwf_Who_Act_Dwnld_Updte_Dte_Tme; }

    public DbsField getCwf_Who_Act_Rt_Sqnce_Nbr() { return cwf_Who_Act_Rt_Sqnce_Nbr; }

    public DbsField getCwf_Who_Act_Off_Rtng_Ind() { return cwf_Who_Act_Off_Rtng_Ind; }

    public DbsField getCwf_Who_Act_Msg_Rtrn_Cde() { return cwf_Who_Act_Msg_Rtrn_Cde; }

    public DbsField getCwf_Who_Act_Msg_Nbr() { return cwf_Who_Act_Msg_Nbr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_cwf_Who_Act = new DataAccessProgramView(new NameInfo("vw_cwf_Who_Act", "CWF-WHO-ACT"), "CWF_WHO_ACTIVITY_FILE", "CWF_WHO_ACTIVITY");
        cwf_Who_Act_Rqst_Log_Dte_Tme = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "RQST_LOG_DTE_TME");
        cwf_Who_Act_Rqst_Log_Dte_Tme.setDdmHeader("LOG/DATE/TIME");
        cwf_Who_Act_Rqst_Log_Dte_TmeRedef1 = vw_cwf_Who_Act.getRecord().newGroupInGroup("cwf_Who_Act_Rqst_Log_Dte_TmeRedef1", "Redefines", cwf_Who_Act_Rqst_Log_Dte_Tme);
        cwf_Who_Act_Rqst_Log_Index_Dte = cwf_Who_Act_Rqst_Log_Dte_TmeRedef1.newFieldInGroup("cwf_Who_Act_Rqst_Log_Index_Dte", "RQST-LOG-INDEX-DTE", FieldType.STRING, 
            8);
        cwf_Who_Act_Rqst_Log_Index_Tme = cwf_Who_Act_Rqst_Log_Dte_TmeRedef1.newFieldInGroup("cwf_Who_Act_Rqst_Log_Index_Tme", "RQST-LOG-INDEX-TME", FieldType.STRING, 
            7);
        cwf_Who_Act_Strtng_Event_Dte_Tme = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Strtng_Event_Dte_Tme", "STRTNG-EVENT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "STRTNG_EVENT_DTE_TME");
        cwf_Who_Act_Strtng_Event_Dte_Tme.setDdmHeader("STARTING/EVENT");
        cwf_Who_Act_Endng_Event_Dte_Tme = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Endng_Event_Dte_Tme", "ENDNG-EVENT-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ENDNG_EVENT_DTE_TME");
        cwf_Who_Act_Endng_Event_Dte_Tme.setDdmHeader("ENDING/EVENT");
        cwf_Who_Act_Cmpltd_Actvty_Ind = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Cmpltd_Actvty_Ind", "CMPLTD-ACTVTY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CMPLTD_ACTVTY_IND");
        cwf_Who_Act_Cmpltd_Actvty_Ind.setDdmHeader("COMPLETED/ACTIVITY/INDICATOR");
        cwf_Who_Act_Admin_Unit_Cde = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADMIN_UNIT_CDE");
        cwf_Who_Act_Admin_Unit_Cde.setDdmHeader("UNIT");
        cwf_Who_Act_Empl_Oprtr_Cde = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Empl_Oprtr_Cde", "EMPL-OPRTR-CDE", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "EMPL_OPRTR_CDE");
        cwf_Who_Act_Empl_Oprtr_Cde.setDdmHeader("EMPLOYEE/ID");
        cwf_Who_Act_Empl_Oprtr_CdeRedef2 = vw_cwf_Who_Act.getRecord().newGroupInGroup("cwf_Who_Act_Empl_Oprtr_CdeRedef2", "Redefines", cwf_Who_Act_Empl_Oprtr_Cde);
        cwf_Who_Act_Empl_Racf_Id = cwf_Who_Act_Empl_Oprtr_CdeRedef2.newFieldInGroup("cwf_Who_Act_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 8);
        cwf_Who_Act_Empl_Sffx_Cde = cwf_Who_Act_Empl_Oprtr_CdeRedef2.newFieldInGroup("cwf_Who_Act_Empl_Sffx_Cde", "EMPL-SFFX-CDE", FieldType.STRING, 2);
        cwf_Who_Act_Step_Id = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Step_Id", "STEP-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "STEP_ID");
        cwf_Who_Act_Step_Id.setDdmHeader("STEP/ID");
        cwf_Who_Act_Admin_Status_Cde = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Admin_Status_Cde", "ADMIN-STATUS-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADMIN_STATUS_CDE");
        cwf_Who_Act_Admin_Status_Cde.setDdmHeader("STATUS");
        cwf_Who_Act_Unit_On_Tme_Ind = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Unit_On_Tme_Ind", "UNIT-ON-TME-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "UNIT_ON_TME_IND");
        cwf_Who_Act_Unit_On_Tme_Ind.setDdmHeader("UNIT/ON/TIME");
        cwf_Who_Act_Crtn_Run_Id = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Crtn_Run_Id", "CRTN-RUN-ID", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "CRTN_RUN_ID");
        cwf_Who_Act_Crtn_Run_Id.setDdmHeader("CREATION/RUN ID");
        cwf_Who_Act_Actvty_Elpsd_Clndr_Hours = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Actvty_Elpsd_Clndr_Hours", "ACTVTY-ELPSD-CLNDR-HOURS", 
            FieldType.NUMERIC, 6, 2, RepeatingFieldStrategy.None, "ACTVTY_ELPSD_CLNDR_HOURS");
        cwf_Who_Act_Actvty_Elpsd_Clndr_Hours.setDdmHeader("ELAPSED/CALENDAR/TIME(HR)");
        cwf_Who_Act_Actvty_Elpsd_Bsnss_Hours = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Actvty_Elpsd_Bsnss_Hours", "ACTVTY-ELPSD-BSNSS-HOURS", 
            FieldType.NUMERIC, 6, 2, RepeatingFieldStrategy.None, "ACTVTY_ELPSD_BSNSS_HOURS");
        cwf_Who_Act_Actvty_Elpsd_Bsnss_Hours.setDdmHeader("ELAPSED/BUSINESS/TIME(HR)");
        cwf_Who_Act_Invrt_Strtng_Event = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Invrt_Strtng_Event", "INVRT-STRTNG-EVENT", FieldType.NUMERIC, 
            15, RepeatingFieldStrategy.None, "INVRT_STRTNG_EVENT");
        cwf_Who_Act_Invrt_Strtng_Event.setDdmHeader("INVERTED/STARTING/EVENT");
        cwf_Who_Act_Dwnld_Updte_Dte_Tme = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Dwnld_Updte_Dte_Tme", "DWNLD-UPDTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DWNLD_UPDTE_DTE_TME");
        cwf_Who_Act_Dwnld_Updte_Dte_Tme.setDdmHeader("UPDATE/DATETIME");
        cwf_Who_Act_Rt_Sqnce_Nbr = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "RT_SQNCE_NBR");
        cwf_Who_Act_Rt_Sqnce_Nbr.setDdmHeader("ROUTE/SEQ.");
        cwf_Who_Act_Off_Rtng_Ind = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Off_Rtng_Ind", "OFF-RTNG-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "OFF_RTNG_IND");
        cwf_Who_Act_Off_Rtng_Ind.setDdmHeader("OFF-ROUTING");
        cwf_Who_Act_Msg_Rtrn_Cde = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Msg_Rtrn_Cde", "MSG-RTRN-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "MSG_RTRN_CDE");
        cwf_Who_Act_Msg_Rtrn_Cde.setDdmHeader("MSGRET/CODE");
        cwf_Who_Act_Msg_Nbr = vw_cwf_Who_Act.getRecord().newFieldInGroup("cwf_Who_Act_Msg_Nbr", "MSG-NBR", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, 
            "MSG_NBR");
        cwf_Who_Act_Msg_Nbr.setDdmHeader("MSG/NUMBER");

        this.setRecordName("LdaCwflact");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_cwf_Who_Act.reset();
    }

    // Constructor
    public LdaCwflact() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
