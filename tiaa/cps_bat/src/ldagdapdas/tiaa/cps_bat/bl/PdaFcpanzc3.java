/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:32 PM
**        * FROM NATURAL PDA     : FCPANZC3
************************************************************
**        * FILE NAME            : PdaFcpanzc3.java
**        * CLASS NAME           : PdaFcpanzc3
**        * INSTANCE NAME        : PdaFcpanzc3
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpanzc3 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpanzc3;
    private DbsField pnd_Fcpanzc3_Pnd_Max_Fund;
    private DbsGroup pnd_Fcpanzc3_Pnd_Inv_Acct;
    private DbsField pnd_Fcpanzc3_Inv_Acct_Settl_Amt;
    private DbsField pnd_Fcpanzc3_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Fcpanzc3_Inv_Acct_Dci_Amt;
    private DbsField pnd_Fcpanzc3_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Fcpanzc3_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Fcpanzc3_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Fcpanzc3_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Fcpanzc3_Inv_Acct_Local_Tax_Amt;

    public DbsGroup getPnd_Fcpanzc3() { return pnd_Fcpanzc3; }

    public DbsField getPnd_Fcpanzc3_Pnd_Max_Fund() { return pnd_Fcpanzc3_Pnd_Max_Fund; }

    public DbsGroup getPnd_Fcpanzc3_Pnd_Inv_Acct() { return pnd_Fcpanzc3_Pnd_Inv_Acct; }

    public DbsField getPnd_Fcpanzc3_Inv_Acct_Settl_Amt() { return pnd_Fcpanzc3_Inv_Acct_Settl_Amt; }

    public DbsField getPnd_Fcpanzc3_Inv_Acct_Dvdnd_Amt() { return pnd_Fcpanzc3_Inv_Acct_Dvdnd_Amt; }

    public DbsField getPnd_Fcpanzc3_Inv_Acct_Dci_Amt() { return pnd_Fcpanzc3_Inv_Acct_Dci_Amt; }

    public DbsField getPnd_Fcpanzc3_Inv_Acct_Dpi_Amt() { return pnd_Fcpanzc3_Inv_Acct_Dpi_Amt; }

    public DbsField getPnd_Fcpanzc3_Inv_Acct_Net_Pymnt_Amt() { return pnd_Fcpanzc3_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getPnd_Fcpanzc3_Inv_Acct_Fdrl_Tax_Amt() { return pnd_Fcpanzc3_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getPnd_Fcpanzc3_Inv_Acct_State_Tax_Amt() { return pnd_Fcpanzc3_Inv_Acct_State_Tax_Amt; }

    public DbsField getPnd_Fcpanzc3_Inv_Acct_Local_Tax_Amt() { return pnd_Fcpanzc3_Inv_Acct_Local_Tax_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpanzc3 = dbsRecord.newGroupInRecord("pnd_Fcpanzc3", "#FCPANZC3");
        pnd_Fcpanzc3.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpanzc3_Pnd_Max_Fund = pnd_Fcpanzc3.newFieldInGroup("pnd_Fcpanzc3_Pnd_Max_Fund", "#MAX-FUND", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpanzc3_Pnd_Inv_Acct = pnd_Fcpanzc3.newGroupArrayInGroup("pnd_Fcpanzc3_Pnd_Inv_Acct", "#INV-ACCT", new DbsArrayController(1,25));
        pnd_Fcpanzc3_Inv_Acct_Settl_Amt = pnd_Fcpanzc3_Pnd_Inv_Acct.newFieldInGroup("pnd_Fcpanzc3_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Fcpanzc3_Inv_Acct_Dvdnd_Amt = pnd_Fcpanzc3_Pnd_Inv_Acct.newFieldInGroup("pnd_Fcpanzc3_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Fcpanzc3_Inv_Acct_Dci_Amt = pnd_Fcpanzc3_Pnd_Inv_Acct.newFieldInGroup("pnd_Fcpanzc3_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Fcpanzc3_Inv_Acct_Dpi_Amt = pnd_Fcpanzc3_Pnd_Inv_Acct.newFieldInGroup("pnd_Fcpanzc3_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Fcpanzc3_Inv_Acct_Net_Pymnt_Amt = pnd_Fcpanzc3_Pnd_Inv_Acct.newFieldInGroup("pnd_Fcpanzc3_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Fcpanzc3_Inv_Acct_Fdrl_Tax_Amt = pnd_Fcpanzc3_Pnd_Inv_Acct.newFieldInGroup("pnd_Fcpanzc3_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Fcpanzc3_Inv_Acct_State_Tax_Amt = pnd_Fcpanzc3_Pnd_Inv_Acct.newFieldInGroup("pnd_Fcpanzc3_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Fcpanzc3_Inv_Acct_Local_Tax_Amt = pnd_Fcpanzc3_Pnd_Inv_Acct.newFieldInGroup("pnd_Fcpanzc3_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpanzc3(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

