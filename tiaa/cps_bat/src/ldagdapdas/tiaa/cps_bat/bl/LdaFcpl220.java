/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:05 PM
**        * FROM NATURAL LDA     : FCPL220
************************************************************
**        * FILE NAME            : LdaFcpl220.java
**        * CLASS NAME           : LdaFcpl220
**        * INSTANCE NAME        : LdaFcpl220
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl220 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_fcp_Cons_Addr_View;
    private DbsField fcp_Cons_Addr_View_Rcrd_Typ;
    private DbsField fcp_Cons_Addr_View_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Addr_View_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Addr_View_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Addr_View_Cntrct_Invrse_Dte;
    private DbsField fcp_Cons_Addr_View_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup fcp_Cons_Addr_View_Ph_Name;
    private DbsField fcp_Cons_Addr_View_Ph_Last_Name;
    private DbsField fcp_Cons_Addr_View_Ph_First_Name;
    private DbsField fcp_Cons_Addr_View_Ph_Middle_Name;
    private DbsGroup fcp_Cons_Addr_View_Count_Castpymnt_Nme_And_Addr_Grp;
    private DbsGroup fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp;
    private DbsField fcp_Cons_Addr_View_Pymnt_Nme;
    private DbsField fcp_Cons_Addr_View_Pymnt_Addr_Line1_Txt;
    private DbsField fcp_Cons_Addr_View_Pymnt_Addr_Line2_Txt;
    private DbsField fcp_Cons_Addr_View_Pymnt_Addr_Line3_Txt;
    private DbsField fcp_Cons_Addr_View_Pymnt_Addr_Line4_Txt;
    private DbsField fcp_Cons_Addr_View_Pymnt_Addr_Line5_Txt;
    private DbsField fcp_Cons_Addr_View_Pymnt_Addr_Line6_Txt;
    private DbsField fcp_Cons_Addr_View_Pymnt_Addr_Zip_Cde;
    private DbsField fcp_Cons_Addr_View_Pymnt_Postl_Data;
    private DbsField fcp_Cons_Addr_View_Pymnt_Addr_Type_Ind;
    private DbsField fcp_Cons_Addr_View_Pymnt_Addr_Last_Chg_Dte;
    private DbsField fcp_Cons_Addr_View_Pymnt_Addr_Last_Chg_Tme;
    private DbsField fcp_Cons_Addr_View_Pymnt_Eft_Transit_Id;
    private DbsField fcp_Cons_Addr_View_Pymnt_Eft_Acct_Nbr;
    private DbsField fcp_Cons_Addr_View_Pymnt_Chk_Sav_Ind;
    private DbsField fcp_Cons_Addr_View_Pymnt_Deceased_Nme;
    private DbsField fcp_Cons_Addr_View_Cntrct_Hold_Tme;

    public DataAccessProgramView getVw_fcp_Cons_Addr_View() { return vw_fcp_Cons_Addr_View; }

    public DbsField getFcp_Cons_Addr_View_Rcrd_Typ() { return fcp_Cons_Addr_View_Rcrd_Typ; }

    public DbsField getFcp_Cons_Addr_View_Cntrct_Orgn_Cde() { return fcp_Cons_Addr_View_Cntrct_Orgn_Cde; }

    public DbsField getFcp_Cons_Addr_View_Cntrct_Ppcn_Nbr() { return fcp_Cons_Addr_View_Cntrct_Ppcn_Nbr; }

    public DbsField getFcp_Cons_Addr_View_Cntrct_Payee_Cde() { return fcp_Cons_Addr_View_Cntrct_Payee_Cde; }

    public DbsField getFcp_Cons_Addr_View_Cntrct_Invrse_Dte() { return fcp_Cons_Addr_View_Cntrct_Invrse_Dte; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Prcss_Seq_Nbr() { return fcp_Cons_Addr_View_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getFcp_Cons_Addr_View_Ph_Name() { return fcp_Cons_Addr_View_Ph_Name; }

    public DbsField getFcp_Cons_Addr_View_Ph_Last_Name() { return fcp_Cons_Addr_View_Ph_Last_Name; }

    public DbsField getFcp_Cons_Addr_View_Ph_First_Name() { return fcp_Cons_Addr_View_Ph_First_Name; }

    public DbsField getFcp_Cons_Addr_View_Ph_Middle_Name() { return fcp_Cons_Addr_View_Ph_Middle_Name; }

    public DbsGroup getFcp_Cons_Addr_View_Count_Castpymnt_Nme_And_Addr_Grp() { return fcp_Cons_Addr_View_Count_Castpymnt_Nme_And_Addr_Grp; }

    public DbsGroup getFcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp() { return fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Nme() { return fcp_Cons_Addr_View_Pymnt_Nme; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Addr_Line1_Txt() { return fcp_Cons_Addr_View_Pymnt_Addr_Line1_Txt; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Addr_Line2_Txt() { return fcp_Cons_Addr_View_Pymnt_Addr_Line2_Txt; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Addr_Line3_Txt() { return fcp_Cons_Addr_View_Pymnt_Addr_Line3_Txt; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Addr_Line4_Txt() { return fcp_Cons_Addr_View_Pymnt_Addr_Line4_Txt; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Addr_Line5_Txt() { return fcp_Cons_Addr_View_Pymnt_Addr_Line5_Txt; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Addr_Line6_Txt() { return fcp_Cons_Addr_View_Pymnt_Addr_Line6_Txt; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Addr_Zip_Cde() { return fcp_Cons_Addr_View_Pymnt_Addr_Zip_Cde; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Postl_Data() { return fcp_Cons_Addr_View_Pymnt_Postl_Data; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Addr_Type_Ind() { return fcp_Cons_Addr_View_Pymnt_Addr_Type_Ind; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Addr_Last_Chg_Dte() { return fcp_Cons_Addr_View_Pymnt_Addr_Last_Chg_Dte; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Addr_Last_Chg_Tme() { return fcp_Cons_Addr_View_Pymnt_Addr_Last_Chg_Tme; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Eft_Transit_Id() { return fcp_Cons_Addr_View_Pymnt_Eft_Transit_Id; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Eft_Acct_Nbr() { return fcp_Cons_Addr_View_Pymnt_Eft_Acct_Nbr; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Chk_Sav_Ind() { return fcp_Cons_Addr_View_Pymnt_Chk_Sav_Ind; }

    public DbsField getFcp_Cons_Addr_View_Pymnt_Deceased_Nme() { return fcp_Cons_Addr_View_Pymnt_Deceased_Nme; }

    public DbsField getFcp_Cons_Addr_View_Cntrct_Hold_Tme() { return fcp_Cons_Addr_View_Cntrct_Hold_Tme; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_fcp_Cons_Addr_View = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Addr_View", "FCP-CONS-ADDR-VIEW"), "FCP_CONS_ADDR", "FCP_CONS_LEDGR", 
            DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ADDR"));
        fcp_Cons_Addr_View_Rcrd_Typ = vw_fcp_Cons_Addr_View.getRecord().newFieldInGroup("fcp_Cons_Addr_View_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RCRD_TYP");
        fcp_Cons_Addr_View_Cntrct_Orgn_Cde = vw_fcp_Cons_Addr_View.getRecord().newFieldInGroup("fcp_Cons_Addr_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Addr_View_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Addr_View.getRecord().newFieldInGroup("fcp_Cons_Addr_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Addr_View_Cntrct_Payee_Cde = vw_fcp_Cons_Addr_View.getRecord().newFieldInGroup("fcp_Cons_Addr_View_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Addr_View_Cntrct_Invrse_Dte = vw_fcp_Cons_Addr_View.getRecord().newFieldInGroup("fcp_Cons_Addr_View_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        fcp_Cons_Addr_View_Pymnt_Prcss_Seq_Nbr = vw_fcp_Cons_Addr_View.getRecord().newFieldInGroup("fcp_Cons_Addr_View_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        fcp_Cons_Addr_View_Ph_Name = vw_fcp_Cons_Addr_View.getRecord().newGroupInGroup("fcp_Cons_Addr_View_Ph_Name", "PH-NAME");
        fcp_Cons_Addr_View_Ph_Last_Name = fcp_Cons_Addr_View_Ph_Name.newFieldInGroup("fcp_Cons_Addr_View_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 
            16, RepeatingFieldStrategy.None, "PH_LAST_NAME");
        fcp_Cons_Addr_View_Ph_First_Name = fcp_Cons_Addr_View_Ph_Name.newFieldInGroup("fcp_Cons_Addr_View_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "PH_FIRST_NAME");
        fcp_Cons_Addr_View_Ph_Middle_Name = fcp_Cons_Addr_View_Ph_Name.newFieldInGroup("fcp_Cons_Addr_View_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "PH_MIDDLE_NAME");
        fcp_Cons_Addr_View_Count_Castpymnt_Nme_And_Addr_Grp = vw_fcp_Cons_Addr_View.getRecord().newGroupInGroup("fcp_Cons_Addr_View_Count_Castpymnt_Nme_And_Addr_Grp", 
            "C*PYMNT-NME-AND-ADDR-GRP", RepeatingFieldStrategy.CAsteriskVariable, "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp = vw_fcp_Cons_Addr_View.getRecord().newGroupInGroup("fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Nme = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 
            38, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_NME", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Addr_Line1_Txt = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Addr_Line1_Txt", 
            "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 35, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE1_TXT", 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Addr_Line2_Txt = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Addr_Line2_Txt", 
            "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 35, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE2_TXT", 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Addr_Line3_Txt = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Addr_Line3_Txt", 
            "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 35, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE3_TXT", 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Addr_Line4_Txt = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Addr_Line4_Txt", 
            "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 35, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE4_TXT", 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Addr_Line5_Txt = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Addr_Line5_Txt", 
            "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 35, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE5_TXT", 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Addr_Line6_Txt = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Addr_Line6_Txt", 
            "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 35, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE6_TXT", 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Addr_Zip_Cde = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Addr_Zip_Cde", 
            "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 9, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_ZIP_CDE", 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Postl_Data = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", 
            FieldType.STRING, 32, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_POSTL_DATA", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Addr_Type_Ind = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Addr_Type_Ind", 
            "PYMNT-ADDR-TYPE-IND", FieldType.STRING, 1, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_TYPE_IND", 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Addr_Last_Chg_Dte = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Addr_Last_Chg_Dte", 
            "PYMNT-ADDR-LAST-CHG-DTE", FieldType.NUMERIC, 8, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LAST_CHG_DTE", 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Addr_Last_Chg_Tme = fcp_Cons_Addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_View_Pymnt_Addr_Last_Chg_Tme", 
            "PYMNT-ADDR-LAST-CHG-TME", FieldType.NUMERIC, 7, new DbsArrayController(1,2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LAST_CHG_TME", 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_View_Pymnt_Eft_Transit_Id = vw_fcp_Cons_Addr_View.getRecord().newFieldInGroup("fcp_Cons_Addr_View_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PYMNT_EFT_TRANSIT_ID");
        fcp_Cons_Addr_View_Pymnt_Eft_Acct_Nbr = vw_fcp_Cons_Addr_View.getRecord().newFieldInGroup("fcp_Cons_Addr_View_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", 
            FieldType.STRING, 21, RepeatingFieldStrategy.None, "PYMNT_EFT_ACCT_NBR");
        fcp_Cons_Addr_View_Pymnt_Chk_Sav_Ind = vw_fcp_Cons_Addr_View.getRecord().newFieldInGroup("fcp_Cons_Addr_View_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PYMNT_CHK_SAV_IND");
        fcp_Cons_Addr_View_Pymnt_Deceased_Nme = vw_fcp_Cons_Addr_View.getRecord().newFieldInGroup("fcp_Cons_Addr_View_Pymnt_Deceased_Nme", "PYMNT-DECEASED-NME", 
            FieldType.STRING, 38, RepeatingFieldStrategy.None, "PYMNT_DECEASED_NME");
        fcp_Cons_Addr_View_Cntrct_Hold_Tme = vw_fcp_Cons_Addr_View.getRecord().newFieldInGroup("fcp_Cons_Addr_View_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRCT_HOLD_TME");
        vw_fcp_Cons_Addr_View.setUniquePeList();

        this.setRecordName("LdaFcpl220");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_fcp_Cons_Addr_View.reset();
    }

    // Constructor
    public LdaFcpl220() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
