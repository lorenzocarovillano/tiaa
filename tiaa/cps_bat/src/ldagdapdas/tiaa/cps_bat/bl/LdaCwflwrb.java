/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:57:12 PM
**        * FROM NATURAL LDA     : CWFLWRB
************************************************************
**        * FILE NAME            : LdaCwflwrb.java
**        * CLASS NAME           : LdaCwflwrb
**        * INSTANCE NAME        : LdaCwflwrb
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwflwrb extends DbsRecord
{
    // Properties
    private DbsGroup cwf_Corp_Rpt_Wr;
    private DbsField cwf_Corp_Rpt_Wr_Rqst_Log_Dte_Tme_A;
    private DbsField cwf_Corp_Rpt_Wr_C1;
    private DbsField cwf_Corp_Rpt_Wr_Work_Prcss_Id;
    private DbsGroup cwf_Corp_Rpt_Wr_Work_Prcss_IdRedef1;
    private DbsField cwf_Corp_Rpt_Wr_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C2;
    private DbsField cwf_Corp_Rpt_Wr_Pin_Nbr;
    private DbsField cwf_Corp_Rpt_Wr_C3;
    private DbsField cwf_Corp_Rpt_Wr_Part_Name;
    private DbsField cwf_Corp_Rpt_Wr_C4;
    private DbsField cwf_Corp_Rpt_Wr_Ssn;
    private DbsField cwf_Corp_Rpt_Wr_C5;
    private DbsField cwf_Corp_Rpt_Wr_Tiaa_Rcvd_Dte_Tme_A;
    private DbsField cwf_Corp_Rpt_Wr_C6;
    private DbsField cwf_Corp_Rpt_Wr_Tiaa_Rcvd_Dte_A;
    private DbsField cwf_Corp_Rpt_Wr_C7;
    private DbsField cwf_Corp_Rpt_Wr_Orgnl_Unit_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C8;
    private DbsField cwf_Corp_Rpt_Wr_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C9;
    private DbsField cwf_Corp_Rpt_Wr_Rqst_Orgn_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C10;
    private DbsField cwf_Corp_Rpt_Wr_Crprte_Status_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C11;
    private DbsField cwf_Corp_Rpt_Wr_Crprte_Clock_End_Dte_Tme_A;
    private DbsField cwf_Corp_Rpt_Wr_C12;
    private DbsField cwf_Corp_Rpt_Wr_Final_Close_Out_Dte_Tme_A;
    private DbsField cwf_Corp_Rpt_Wr_C13;
    private DbsField cwf_Corp_Rpt_Wr_Effctve_Dte_A;
    private DbsField cwf_Corp_Rpt_Wr_C14;
    private DbsField cwf_Corp_Rpt_Wr_Trans_Dte_A;
    private DbsField cwf_Corp_Rpt_Wr_C15;
    private DbsField cwf_Corp_Rpt_Wr_Trnsctn_Dte_A;
    private DbsField cwf_Corp_Rpt_Wr_C16;
    private DbsField cwf_Corp_Rpt_Wr_Owner_Unit_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C17;
    private DbsField cwf_Corp_Rpt_Wr_Owner_Division;
    private DbsField cwf_Corp_Rpt_Wr_C18;
    private DbsField cwf_Corp_Rpt_Wr_Shphrd_Id;
    private DbsField cwf_Corp_Rpt_Wr_C19;
    private DbsField cwf_Corp_Rpt_Wr_Sec_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C20;
    private DbsField cwf_Corp_Rpt_Wr_Admin_Work_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C21;
    private DbsField cwf_Corp_Rpt_Wr_Sub_Rqst_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C22;
    private DbsField cwf_Corp_Rpt_Wr_Prnt_Rqst_Log_Dte_Tme_A;
    private DbsField cwf_Corp_Rpt_Wr_C23;
    private DbsField cwf_Corp_Rpt_Wr_Prnt_Work_Prcss_Id;
    private DbsField cwf_Corp_Rpt_Wr_C24;
    private DbsField cwf_Corp_Rpt_Wr_Multi_Rqst_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C25;
    private DbsField cwf_Corp_Rpt_Wr_Cmplnt_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C26;
    private DbsField cwf_Corp_Rpt_Wr_Elctrnc_Fldr_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C27;
    private DbsField cwf_Corp_Rpt_Wr_Mj_Pull_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C28;
    private DbsField cwf_Corp_Rpt_Wr_Check_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C29;
    private DbsField cwf_Corp_Rpt_Wr_Bsnss_Reply_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C30;
    private DbsField cwf_Corp_Rpt_Wr_Tlc_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C31;
    private DbsField cwf_Corp_Rpt_Wr_Redo_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C32;
    private DbsField cwf_Corp_Rpt_Wr_Crprte_On_Tme_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C33;
    private DbsField cwf_Corp_Rpt_Wr_Off_Rtng_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C34;
    private DbsField cwf_Corp_Rpt_Wr_Prcssng_Type_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C35;
    private DbsField cwf_Corp_Rpt_Wr_Log_Rqstr_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C36;
    private DbsField cwf_Corp_Rpt_Wr_Log_Insttn_Srce_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C37;
    private DbsField cwf_Corp_Rpt_Wr_Instn_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C38;
    private DbsField cwf_Corp_Rpt_Wr_Rqst_Instn_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C39;
    private DbsField cwf_Corp_Rpt_Wr_Rqst_Rgn_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C40;
    private DbsField cwf_Corp_Rpt_Wr_Rqst_Spcl_Dsgntn_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C41;
    private DbsField cwf_Corp_Rpt_Wr_Rqst_Brnch_Cde;
    private DbsField cwf_Corp_Rpt_Wr_C42;
    private DbsField cwf_Corp_Rpt_Wr_Extrnl_Pend_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C43;
    private DbsField cwf_Corp_Rpt_Wr_Crprte_Due_Dte_Tme_A;
    private DbsField cwf_Corp_Rpt_Wr_C44;
    private DbsField cwf_Corp_Rpt_Wr_Mis_Routed_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C45;
    private DbsField cwf_Corp_Rpt_Wr_Dte_Of_Birth;
    private DbsField cwf_Corp_Rpt_Wr_C46;
    private DbsField cwf_Corp_Rpt_Wr_Trade_Dte_Tme_A;
    private DbsField cwf_Corp_Rpt_Wr_C47;
    private DbsField cwf_Corp_Rpt_Wr_Mj_Media_Ind;
    private DbsField cwf_Corp_Rpt_Wr_C48;
    private DbsField cwf_Corp_Rpt_Wr_State_Of_Res;
    private DbsField cwf_Corp_Rpt_Wr_C49;
    private DbsField cwf_Corp_Rpt_Wr_Sec_Turnaround_Tme_A;
    private DbsField cwf_Corp_Rpt_Wr_C50;
    private DbsField cwf_Corp_Rpt_Wr_Sec_Updte_Dte_A;
    private DbsField cwf_Corp_Rpt_Wr_C51;
    private DbsField cwf_Corp_Rpt_Wr_Rqst_Indicators;

    public DbsGroup getCwf_Corp_Rpt_Wr() { return cwf_Corp_Rpt_Wr; }

    public DbsField getCwf_Corp_Rpt_Wr_Rqst_Log_Dte_Tme_A() { return cwf_Corp_Rpt_Wr_Rqst_Log_Dte_Tme_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C1() { return cwf_Corp_Rpt_Wr_C1; }

    public DbsField getCwf_Corp_Rpt_Wr_Work_Prcss_Id() { return cwf_Corp_Rpt_Wr_Work_Prcss_Id; }

    public DbsGroup getCwf_Corp_Rpt_Wr_Work_Prcss_IdRedef1() { return cwf_Corp_Rpt_Wr_Work_Prcss_IdRedef1; }

    public DbsField getCwf_Corp_Rpt_Wr_Work_Actn_Rqstd_Cde() { return cwf_Corp_Rpt_Wr_Work_Actn_Rqstd_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Work_Lob_Cmpny_Prdct_Cde() { return cwf_Corp_Rpt_Wr_Work_Lob_Cmpny_Prdct_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Work_Mjr_Bsnss_Prcss_Cde() { return cwf_Corp_Rpt_Wr_Work_Mjr_Bsnss_Prcss_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Work_Spcfc_Bsnss_Prcss_Cde() { return cwf_Corp_Rpt_Wr_Work_Spcfc_Bsnss_Prcss_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C2() { return cwf_Corp_Rpt_Wr_C2; }

    public DbsField getCwf_Corp_Rpt_Wr_Pin_Nbr() { return cwf_Corp_Rpt_Wr_Pin_Nbr; }

    public DbsField getCwf_Corp_Rpt_Wr_C3() { return cwf_Corp_Rpt_Wr_C3; }

    public DbsField getCwf_Corp_Rpt_Wr_Part_Name() { return cwf_Corp_Rpt_Wr_Part_Name; }

    public DbsField getCwf_Corp_Rpt_Wr_C4() { return cwf_Corp_Rpt_Wr_C4; }

    public DbsField getCwf_Corp_Rpt_Wr_Ssn() { return cwf_Corp_Rpt_Wr_Ssn; }

    public DbsField getCwf_Corp_Rpt_Wr_C5() { return cwf_Corp_Rpt_Wr_C5; }

    public DbsField getCwf_Corp_Rpt_Wr_Tiaa_Rcvd_Dte_Tme_A() { return cwf_Corp_Rpt_Wr_Tiaa_Rcvd_Dte_Tme_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C6() { return cwf_Corp_Rpt_Wr_C6; }

    public DbsField getCwf_Corp_Rpt_Wr_Tiaa_Rcvd_Dte_A() { return cwf_Corp_Rpt_Wr_Tiaa_Rcvd_Dte_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C7() { return cwf_Corp_Rpt_Wr_C7; }

    public DbsField getCwf_Corp_Rpt_Wr_Orgnl_Unit_Cde() { return cwf_Corp_Rpt_Wr_Orgnl_Unit_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C8() { return cwf_Corp_Rpt_Wr_C8; }

    public DbsField getCwf_Corp_Rpt_Wr_Rqst_Log_Oprtr_Cde() { return cwf_Corp_Rpt_Wr_Rqst_Log_Oprtr_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C9() { return cwf_Corp_Rpt_Wr_C9; }

    public DbsField getCwf_Corp_Rpt_Wr_Rqst_Orgn_Cde() { return cwf_Corp_Rpt_Wr_Rqst_Orgn_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C10() { return cwf_Corp_Rpt_Wr_C10; }

    public DbsField getCwf_Corp_Rpt_Wr_Crprte_Status_Ind() { return cwf_Corp_Rpt_Wr_Crprte_Status_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C11() { return cwf_Corp_Rpt_Wr_C11; }

    public DbsField getCwf_Corp_Rpt_Wr_Crprte_Clock_End_Dte_Tme_A() { return cwf_Corp_Rpt_Wr_Crprte_Clock_End_Dte_Tme_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C12() { return cwf_Corp_Rpt_Wr_C12; }

    public DbsField getCwf_Corp_Rpt_Wr_Final_Close_Out_Dte_Tme_A() { return cwf_Corp_Rpt_Wr_Final_Close_Out_Dte_Tme_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C13() { return cwf_Corp_Rpt_Wr_C13; }

    public DbsField getCwf_Corp_Rpt_Wr_Effctve_Dte_A() { return cwf_Corp_Rpt_Wr_Effctve_Dte_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C14() { return cwf_Corp_Rpt_Wr_C14; }

    public DbsField getCwf_Corp_Rpt_Wr_Trans_Dte_A() { return cwf_Corp_Rpt_Wr_Trans_Dte_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C15() { return cwf_Corp_Rpt_Wr_C15; }

    public DbsField getCwf_Corp_Rpt_Wr_Trnsctn_Dte_A() { return cwf_Corp_Rpt_Wr_Trnsctn_Dte_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C16() { return cwf_Corp_Rpt_Wr_C16; }

    public DbsField getCwf_Corp_Rpt_Wr_Owner_Unit_Cde() { return cwf_Corp_Rpt_Wr_Owner_Unit_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C17() { return cwf_Corp_Rpt_Wr_C17; }

    public DbsField getCwf_Corp_Rpt_Wr_Owner_Division() { return cwf_Corp_Rpt_Wr_Owner_Division; }

    public DbsField getCwf_Corp_Rpt_Wr_C18() { return cwf_Corp_Rpt_Wr_C18; }

    public DbsField getCwf_Corp_Rpt_Wr_Shphrd_Id() { return cwf_Corp_Rpt_Wr_Shphrd_Id; }

    public DbsField getCwf_Corp_Rpt_Wr_C19() { return cwf_Corp_Rpt_Wr_C19; }

    public DbsField getCwf_Corp_Rpt_Wr_Sec_Ind() { return cwf_Corp_Rpt_Wr_Sec_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C20() { return cwf_Corp_Rpt_Wr_C20; }

    public DbsField getCwf_Corp_Rpt_Wr_Admin_Work_Ind() { return cwf_Corp_Rpt_Wr_Admin_Work_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C21() { return cwf_Corp_Rpt_Wr_C21; }

    public DbsField getCwf_Corp_Rpt_Wr_Sub_Rqst_Ind() { return cwf_Corp_Rpt_Wr_Sub_Rqst_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C22() { return cwf_Corp_Rpt_Wr_C22; }

    public DbsField getCwf_Corp_Rpt_Wr_Prnt_Rqst_Log_Dte_Tme_A() { return cwf_Corp_Rpt_Wr_Prnt_Rqst_Log_Dte_Tme_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C23() { return cwf_Corp_Rpt_Wr_C23; }

    public DbsField getCwf_Corp_Rpt_Wr_Prnt_Work_Prcss_Id() { return cwf_Corp_Rpt_Wr_Prnt_Work_Prcss_Id; }

    public DbsField getCwf_Corp_Rpt_Wr_C24() { return cwf_Corp_Rpt_Wr_C24; }

    public DbsField getCwf_Corp_Rpt_Wr_Multi_Rqst_Ind() { return cwf_Corp_Rpt_Wr_Multi_Rqst_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C25() { return cwf_Corp_Rpt_Wr_C25; }

    public DbsField getCwf_Corp_Rpt_Wr_Cmplnt_Ind() { return cwf_Corp_Rpt_Wr_Cmplnt_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C26() { return cwf_Corp_Rpt_Wr_C26; }

    public DbsField getCwf_Corp_Rpt_Wr_Elctrnc_Fldr_Ind() { return cwf_Corp_Rpt_Wr_Elctrnc_Fldr_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C27() { return cwf_Corp_Rpt_Wr_C27; }

    public DbsField getCwf_Corp_Rpt_Wr_Mj_Pull_Ind() { return cwf_Corp_Rpt_Wr_Mj_Pull_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C28() { return cwf_Corp_Rpt_Wr_C28; }

    public DbsField getCwf_Corp_Rpt_Wr_Check_Ind() { return cwf_Corp_Rpt_Wr_Check_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C29() { return cwf_Corp_Rpt_Wr_C29; }

    public DbsField getCwf_Corp_Rpt_Wr_Bsnss_Reply_Ind() { return cwf_Corp_Rpt_Wr_Bsnss_Reply_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C30() { return cwf_Corp_Rpt_Wr_C30; }

    public DbsField getCwf_Corp_Rpt_Wr_Tlc_Ind() { return cwf_Corp_Rpt_Wr_Tlc_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C31() { return cwf_Corp_Rpt_Wr_C31; }

    public DbsField getCwf_Corp_Rpt_Wr_Redo_Ind() { return cwf_Corp_Rpt_Wr_Redo_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C32() { return cwf_Corp_Rpt_Wr_C32; }

    public DbsField getCwf_Corp_Rpt_Wr_Crprte_On_Tme_Ind() { return cwf_Corp_Rpt_Wr_Crprte_On_Tme_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C33() { return cwf_Corp_Rpt_Wr_C33; }

    public DbsField getCwf_Corp_Rpt_Wr_Off_Rtng_Ind() { return cwf_Corp_Rpt_Wr_Off_Rtng_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C34() { return cwf_Corp_Rpt_Wr_C34; }

    public DbsField getCwf_Corp_Rpt_Wr_Prcssng_Type_Cde() { return cwf_Corp_Rpt_Wr_Prcssng_Type_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C35() { return cwf_Corp_Rpt_Wr_C35; }

    public DbsField getCwf_Corp_Rpt_Wr_Log_Rqstr_Cde() { return cwf_Corp_Rpt_Wr_Log_Rqstr_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C36() { return cwf_Corp_Rpt_Wr_C36; }

    public DbsField getCwf_Corp_Rpt_Wr_Log_Insttn_Srce_Cde() { return cwf_Corp_Rpt_Wr_Log_Insttn_Srce_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C37() { return cwf_Corp_Rpt_Wr_C37; }

    public DbsField getCwf_Corp_Rpt_Wr_Instn_Cde() { return cwf_Corp_Rpt_Wr_Instn_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C38() { return cwf_Corp_Rpt_Wr_C38; }

    public DbsField getCwf_Corp_Rpt_Wr_Rqst_Instn_Cde() { return cwf_Corp_Rpt_Wr_Rqst_Instn_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C39() { return cwf_Corp_Rpt_Wr_C39; }

    public DbsField getCwf_Corp_Rpt_Wr_Rqst_Rgn_Cde() { return cwf_Corp_Rpt_Wr_Rqst_Rgn_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C40() { return cwf_Corp_Rpt_Wr_C40; }

    public DbsField getCwf_Corp_Rpt_Wr_Rqst_Spcl_Dsgntn_Cde() { return cwf_Corp_Rpt_Wr_Rqst_Spcl_Dsgntn_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C41() { return cwf_Corp_Rpt_Wr_C41; }

    public DbsField getCwf_Corp_Rpt_Wr_Rqst_Brnch_Cde() { return cwf_Corp_Rpt_Wr_Rqst_Brnch_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_C42() { return cwf_Corp_Rpt_Wr_C42; }

    public DbsField getCwf_Corp_Rpt_Wr_Extrnl_Pend_Ind() { return cwf_Corp_Rpt_Wr_Extrnl_Pend_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C43() { return cwf_Corp_Rpt_Wr_C43; }

    public DbsField getCwf_Corp_Rpt_Wr_Crprte_Due_Dte_Tme_A() { return cwf_Corp_Rpt_Wr_Crprte_Due_Dte_Tme_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C44() { return cwf_Corp_Rpt_Wr_C44; }

    public DbsField getCwf_Corp_Rpt_Wr_Mis_Routed_Ind() { return cwf_Corp_Rpt_Wr_Mis_Routed_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C45() { return cwf_Corp_Rpt_Wr_C45; }

    public DbsField getCwf_Corp_Rpt_Wr_Dte_Of_Birth() { return cwf_Corp_Rpt_Wr_Dte_Of_Birth; }

    public DbsField getCwf_Corp_Rpt_Wr_C46() { return cwf_Corp_Rpt_Wr_C46; }

    public DbsField getCwf_Corp_Rpt_Wr_Trade_Dte_Tme_A() { return cwf_Corp_Rpt_Wr_Trade_Dte_Tme_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C47() { return cwf_Corp_Rpt_Wr_C47; }

    public DbsField getCwf_Corp_Rpt_Wr_Mj_Media_Ind() { return cwf_Corp_Rpt_Wr_Mj_Media_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_C48() { return cwf_Corp_Rpt_Wr_C48; }

    public DbsField getCwf_Corp_Rpt_Wr_State_Of_Res() { return cwf_Corp_Rpt_Wr_State_Of_Res; }

    public DbsField getCwf_Corp_Rpt_Wr_C49() { return cwf_Corp_Rpt_Wr_C49; }

    public DbsField getCwf_Corp_Rpt_Wr_Sec_Turnaround_Tme_A() { return cwf_Corp_Rpt_Wr_Sec_Turnaround_Tme_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C50() { return cwf_Corp_Rpt_Wr_C50; }

    public DbsField getCwf_Corp_Rpt_Wr_Sec_Updte_Dte_A() { return cwf_Corp_Rpt_Wr_Sec_Updte_Dte_A; }

    public DbsField getCwf_Corp_Rpt_Wr_C51() { return cwf_Corp_Rpt_Wr_C51; }

    public DbsField getCwf_Corp_Rpt_Wr_Rqst_Indicators() { return cwf_Corp_Rpt_Wr_Rqst_Indicators; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cwf_Corp_Rpt_Wr = newGroupInRecord("cwf_Corp_Rpt_Wr", "CWF-CORP-RPT-WR");
        cwf_Corp_Rpt_Wr_Rqst_Log_Dte_Tme_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Rqst_Log_Dte_Tme_A", "RQST-LOG-DTE-TME-A", FieldType.STRING, 
            28);
        cwf_Corp_Rpt_Wr_C1 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C1", "C1", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Work_Prcss_Id = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 6);
        cwf_Corp_Rpt_Wr_Work_Prcss_IdRedef1 = cwf_Corp_Rpt_Wr.newGroupInGroup("cwf_Corp_Rpt_Wr_Work_Prcss_IdRedef1", "Redefines", cwf_Corp_Rpt_Wr_Work_Prcss_Id);
        cwf_Corp_Rpt_Wr_Work_Actn_Rqstd_Cde = cwf_Corp_Rpt_Wr_Work_Prcss_IdRedef1.newFieldInGroup("cwf_Corp_Rpt_Wr_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Work_Lob_Cmpny_Prdct_Cde = cwf_Corp_Rpt_Wr_Work_Prcss_IdRedef1.newFieldInGroup("cwf_Corp_Rpt_Wr_Work_Lob_Cmpny_Prdct_Cde", "WORK-LOB-CMPNY-PRDCT-CDE", 
            FieldType.STRING, 2);
        cwf_Corp_Rpt_Wr_Work_Mjr_Bsnss_Prcss_Cde = cwf_Corp_Rpt_Wr_Work_Prcss_IdRedef1.newFieldInGroup("cwf_Corp_Rpt_Wr_Work_Mjr_Bsnss_Prcss_Cde", "WORK-MJR-BSNSS-PRCSS-CDE", 
            FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Corp_Rpt_Wr_Work_Prcss_IdRedef1.newFieldInGroup("cwf_Corp_Rpt_Wr_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        cwf_Corp_Rpt_Wr_C2 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C2", "C2", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Pin_Nbr = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 7);
        cwf_Corp_Rpt_Wr_C3 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C3", "C3", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Part_Name = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Part_Name", "PART-NAME", FieldType.STRING, 40);
        cwf_Corp_Rpt_Wr_C4 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C4", "C4", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Ssn = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Ssn", "SSN", FieldType.NUMERIC, 9);
        cwf_Corp_Rpt_Wr_C5 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C5", "C5", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Tiaa_Rcvd_Dte_Tme_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Tiaa_Rcvd_Dte_Tme_A", "TIAA-RCVD-DTE-TME-A", FieldType.STRING, 
            26);
        cwf_Corp_Rpt_Wr_C6 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C6", "C6", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Tiaa_Rcvd_Dte_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Tiaa_Rcvd_Dte_A", "TIAA-RCVD-DTE-A", FieldType.STRING, 12);
        cwf_Corp_Rpt_Wr_C7 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C7", "C7", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Orgnl_Unit_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);
        cwf_Corp_Rpt_Wr_C8 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C8", "C8", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Rqst_Log_Oprtr_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8);
        cwf_Corp_Rpt_Wr_C9 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C9", "C9", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Rqst_Orgn_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C10 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C10", "C10", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Crprte_Status_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_C11 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C11", "C11", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Crprte_Clock_End_Dte_Tme_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Crprte_Clock_End_Dte_Tme_A", "CRPRTE-CLOCK-END-DTE-TME-A", 
            FieldType.STRING, 26);
        cwf_Corp_Rpt_Wr_C12 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C12", "C12", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Final_Close_Out_Dte_Tme_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Final_Close_Out_Dte_Tme_A", "FINAL-CLOSE-OUT-DTE-TME-A", 
            FieldType.STRING, 26);
        cwf_Corp_Rpt_Wr_C13 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C13", "C13", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Effctve_Dte_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Effctve_Dte_A", "EFFCTVE-DTE-A", FieldType.STRING, 12);
        cwf_Corp_Rpt_Wr_C14 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C14", "C14", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Trans_Dte_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Trans_Dte_A", "TRANS-DTE-A", FieldType.STRING, 12);
        cwf_Corp_Rpt_Wr_C15 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C15", "C15", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Trnsctn_Dte_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Trnsctn_Dte_A", "TRNSCTN-DTE-A", FieldType.STRING, 12);
        cwf_Corp_Rpt_Wr_C16 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C16", "C16", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Owner_Unit_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Owner_Unit_Cde", "OWNER-UNIT-CDE", FieldType.STRING, 8);
        cwf_Corp_Rpt_Wr_C17 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C17", "C17", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Owner_Division = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Owner_Division", "OWNER-DIVISION", FieldType.STRING, 6);
        cwf_Corp_Rpt_Wr_C18 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C18", "C18", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Shphrd_Id = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Shphrd_Id", "SHPHRD-ID", FieldType.STRING, 8);
        cwf_Corp_Rpt_Wr_C19 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C19", "C19", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Sec_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Sec_Ind", "SEC-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C20 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C20", "C20", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Admin_Work_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Admin_Work_Ind", "ADMIN-WORK-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C21 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C21", "C21", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Sub_Rqst_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Sub_Rqst_Ind", "SUB-RQST-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C22 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C22", "C22", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Prnt_Rqst_Log_Dte_Tme_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Prnt_Rqst_Log_Dte_Tme_A", "PRNT-RQST-LOG-DTE-TME-A", 
            FieldType.STRING, 26);
        cwf_Corp_Rpt_Wr_C23 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C23", "C23", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Prnt_Work_Prcss_Id = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Prnt_Work_Prcss_Id", "PRNT-WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cwf_Corp_Rpt_Wr_C24 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C24", "C24", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Multi_Rqst_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C25 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C25", "C25", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Cmplnt_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C26 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C26", "C26", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Elctrnc_Fldr_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C27 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C27", "C27", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Mj_Pull_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C28 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C28", "C28", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Check_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Check_Ind", "CHECK-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C29 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C29", "C29", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Bsnss_Reply_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C30 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C30", "C30", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Tlc_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Tlc_Ind", "TLC-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C31 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C31", "C31", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Redo_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Redo_Ind", "REDO-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C32 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C32", "C32", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Crprte_On_Tme_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_C33 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C33", "C33", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Off_Rtng_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Off_Rtng_Ind", "OFF-RTNG-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C34 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C34", "C34", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Prcssng_Type_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Prcssng_Type_Cde", "PRCSSNG-TYPE-CDE", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C35 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C35", "C35", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Log_Rqstr_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Log_Rqstr_Cde", "LOG-RQSTR-CDE", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C36 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C36", "C36", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Log_Insttn_Srce_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Log_Insttn_Srce_Cde", "LOG-INSTTN-SRCE-CDE", FieldType.STRING, 
            5);
        cwf_Corp_Rpt_Wr_C37 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C37", "C37", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Instn_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Instn_Cde", "INSTN-CDE", FieldType.STRING, 5);
        cwf_Corp_Rpt_Wr_C38 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C38", "C38", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Rqst_Instn_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Rqst_Instn_Cde", "RQST-INSTN-CDE", FieldType.STRING, 5);
        cwf_Corp_Rpt_Wr_C39 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C39", "C39", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Rqst_Rgn_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Rqst_Rgn_Cde", "RQST-RGN-CDE", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C40 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C40", "C40", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Rqst_Spcl_Dsgntn_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Rqst_Spcl_Dsgntn_Cde", "RQST-SPCL-DSGNTN-CDE", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_C41 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C41", "C41", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Rqst_Brnch_Cde = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C42 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C42", "C42", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Extrnl_Pend_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Extrnl_Pend_Ind", "EXTRNL-PEND-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C43 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C43", "C43", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Crprte_Due_Dte_Tme_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Crprte_Due_Dte_Tme_A", "CRPRTE-DUE-DTE-TME-A", FieldType.STRING, 
            26);
        cwf_Corp_Rpt_Wr_C44 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C44", "C44", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Mis_Routed_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Mis_Routed_Ind", "MIS-ROUTED-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C45 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C45", "C45", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Dte_Of_Birth = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Dte_Of_Birth", "DTE-OF-BIRTH", FieldType.STRING, 12);
        cwf_Corp_Rpt_Wr_C46 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C46", "C46", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Trade_Dte_Tme_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Trade_Dte_Tme_A", "TRADE-DTE-TME-A", FieldType.STRING, 26);
        cwf_Corp_Rpt_Wr_C47 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C47", "C47", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Mj_Media_Ind = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Mj_Media_Ind", "MJ-MEDIA-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_C48 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C48", "C48", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_State_Of_Res = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_State_Of_Res", "STATE-OF-RES", FieldType.STRING, 2);
        cwf_Corp_Rpt_Wr_C49 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C49", "C49", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Sec_Turnaround_Tme_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Sec_Turnaround_Tme_A", "SEC-TURNAROUND-TME-A", FieldType.STRING, 
            10);
        cwf_Corp_Rpt_Wr_C50 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C50", "C50", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Sec_Updte_Dte_A = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Sec_Updte_Dte_A", "SEC-UPDTE-DTE-A", FieldType.STRING, 12);
        cwf_Corp_Rpt_Wr_C51 = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_C51", "C51", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Rqst_Indicators = cwf_Corp_Rpt_Wr.newFieldInGroup("cwf_Corp_Rpt_Wr_Rqst_Indicators", "RQST-INDICATORS", FieldType.STRING, 25);

        this.setRecordName("LdaCwflwrb");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwflwrb() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
