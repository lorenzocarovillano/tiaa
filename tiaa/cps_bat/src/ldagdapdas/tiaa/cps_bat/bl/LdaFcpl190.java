/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:02 PM
**        * FROM NATURAL LDA     : FCPL190
************************************************************
**        * FILE NAME            : LdaFcpl190.java
**        * CLASS NAME           : LdaFcpl190
**        * INSTANCE NAME        : LdaFcpl190
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl190 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Extract_Record;
    private DbsField pnd_Extract_Record_Pnd_Extract1;
    private DbsGroup pnd_Extract_Record_Pnd_Extract1Redef1;
    private DbsField pnd_Extract_Record_Pnd_Record_Level_Nmbr;
    private DbsField pnd_Extract_Record_Pnd_Record_Occur_Nmbr;
    private DbsField pnd_Extract_Record_Pnd_Simplex_Duplex_Multiplex;
    private DbsField pnd_Extract_Record_Pnd_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Extract_Record_Pnd_Save_Contract_Hold_Code;
    private DbsField pnd_Extract_Record_Pnd_Pymnt_Check_Nbr;
    private DbsField pnd_Extract_Record_Pnd_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Extract_Record_Pnd_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Extract_Record_Pnd_Record_Filler_1;
    private DbsField pnd_Extract_Record_Pnd_Extract2;
    private DbsGroup pnd_Extract_RecordRedef2;
    private DbsField pnd_Extract_Record_Pnd_Hdr_Record_Level_Nmbr;
    private DbsField pnd_Extract_Record_Pnd_Hdr_Record_Occur_Nmbr;
    private DbsField pnd_Extract_Record_Pnd_Hdr_Simplex_Duplex_Multiplex;
    private DbsField pnd_Extract_Record_Pnd_Hdr_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Extract_Record_Pnd_Hdr_Save_Contract_Hold_Code;
    private DbsField pnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Nbr;
    private DbsField pnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Extract_Record_Pnd_Hdr_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Invrse_Dte;
    private DbsField pnd_Extract_Record_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Crrncy_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Qlfied_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Extract_Record_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Extract_Record_Cntrct_Sps_Cde;
    private DbsField pnd_Extract_Record_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Extract_Record_Pymnt_Stats_Cde;
    private DbsField pnd_Extract_Record_Pymnt_Annot_Ind;
    private DbsField pnd_Extract_Record_Pymnt_Cmbne_Ind;
    private DbsField pnd_Extract_Record_Pymnt_Ftre_Ind;
    private DbsField pnd_Extract_Record_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Extract_Record_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Extract_Record_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Extract_Record_Annt_Soc_Sec_Ind;
    private DbsField pnd_Extract_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Extract_Record_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Extract_Record_Cntrct_Rqst_Dte;
    private DbsField pnd_Extract_Record_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Extract_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Type_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Lob_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Cref_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Option_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Mode_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Hold_Grp;
    private DbsField pnd_Extract_Record_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Extract_Record_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Extract_Record_Annt_Ctznshp_Cde;
    private DbsField pnd_Extract_Record_Annt_Rsdncy_Cde;
    private DbsField pnd_Extract_Record_Pymnt_Split_Cde;
    private DbsField pnd_Extract_Record_Pymnt_Split_Reasn_Cde;
    private DbsGroup pnd_Extract_Record_Pymnt_Split_Reasn_CdeRedef3;
    private DbsField pnd_Extract_Record_Pymnt_Method_Cde;
    private DbsField pnd_Extract_Record_Pymnt_Method_Filler;
    private DbsField pnd_Extract_Record_Pymnt_Check_Dte;
    private DbsField pnd_Extract_Record_Pymnt_Cycle_Dte;
    private DbsField pnd_Extract_Record_Pymnt_Eft_Dte;
    private DbsField pnd_Extract_Record_Pymnt_Rqst_Pct;
    private DbsField pnd_Extract_Record_Pymnt_Rqst_Amt;
    private DbsField pnd_Extract_Record_Pymnt_Check_Nbr;
    private DbsField pnd_Extract_Record_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pnd_Extract_Record_Pymnt_Prcss_Seq_NbrRedef4;
    private DbsField pnd_Extract_Record_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Extract_Record_Pymnt_Instmt_Nbr;
    private DbsField pnd_Extract_Record_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Extract_Record_Pymnt_Check_Amt;
    private DbsField pnd_Extract_Record_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Extract_Record_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Extract_Record_Cntrct_Hold_Tme;
    private DbsField pnd_Extract_Record_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Extract_Record_Pymnt_Acctg_Dte;
    private DbsField pnd_Extract_Record_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Extract_Record_Pymnt_Intrfce_Dte;
    private DbsField pnd_Extract_Record_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField filler01;
    private DbsField pnd_Extract_Record_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Extract_Record_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Extract_Record_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Extract_Record_Cntrct_Insurance_Option;
    private DbsField pnd_Extract_Record_Cntrct_Life_Contingency;
    private DbsField pnd_Extract_Record_Pnd_Ws_Header_Filler;
    private DbsGroup pnd_Extract_RecordRedef5;
    private DbsField pnd_Extract_Record_Pnd_Inv_Record_Level_Nmbr;
    private DbsField pnd_Extract_Record_Pnd_Inv_Record_Occur_Nmbr;
    private DbsField pnd_Extract_Record_Pnd_Inv_Simplex_Duplex_Multiplex;
    private DbsField pnd_Extract_Record_Pnd_Inv_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Extract_Record_Pnd_Inv_Save_Contract_Hold_Code;
    private DbsField pnd_Extract_Record_Pnd_Inv_Pymnt_Check_Nbr;
    private DbsField pnd_Extract_Record_Pnd_Inv_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Extract_Record_Pnd_Inv_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Extract_Record_Pnd_Cntr_Inv_Acct;
    private DbsField pnd_Extract_Record_Pnd_Ws_Side;
    private DbsField pnd_Extract_Record_Inv_Acct_Cde;
    private DbsField pnd_Extract_Record_Inv_Acct_Unit_Qty;
    private DbsField pnd_Extract_Record_Inv_Acct_Unit_Value;
    private DbsField pnd_Extract_Record_Inv_Acct_Settl_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_Fed_Cde;
    private DbsField pnd_Extract_Record_Inv_Acct_State_Cde;
    private DbsField pnd_Extract_Record_Inv_Acct_Local_Cde;
    private DbsField pnd_Extract_Record_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_Dci_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_Start_Accum_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_End_Accum_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_Ivc_Ind;
    private DbsField pnd_Extract_Record_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_Valuat_Period;
    private DbsField pnd_Extract_Record_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Extract_Record_Inv_Acct_Exp_Amt;
    private DbsField pnd_Extract_Record_Inv_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Extract_Record_Inv_Cntrct_Payee_Cde;
    private DbsField pnd_Extract_Record_Inv_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Extract_Record_Inv_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Extract_Record_Inv_Cntrct_Option_Cde;
    private DbsField pnd_Extract_Record_Inv_Cntrct_Mode_Cde;
    private DbsField pnd_Extract_Record_Pnd_Cntr_Deductions;
    private DbsField pnd_Extract_Record_Pymnt_Ded_Cde;
    private DbsField pnd_Extract_Record_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Extract_Record_Pymnt_Ded_Amt;
    private DbsGroup pnd_Extract_Record_Pnd_Pyhdr_Data;
    private DbsField pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte;
    private DbsGroup pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef6;
    private DbsField pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4;
    private DbsGroup pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef7;
    private DbsField pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6;
    private DbsGroup pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef8;
    private DbsField pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start;
    private DbsField pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End;
    private DbsGroup pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef9;
    private DbsField pnd_Extract_Record_Pnd_Pyhdr_Annt_Ctznshp_Cde;
    private DbsField pnd_Extract_Record_Pnd_Pyhdr_Annt_Rsdncy_Cde;
    private DbsGroup pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef10;
    private DbsField pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_P6;
    private DbsGroup pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef11;
    private DbsField pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A4;
    private DbsField pnd_Extract_Record_Pnd_Inv_Acct_Filler;
    private DbsGroup pnd_Extract_RecordRedef12;
    private DbsField pnd_Extract_Record_Pnd_Na_Record_Level_Nmbr;
    private DbsField pnd_Extract_Record_Pnd_Na_Record_Occur_Nmbr;
    private DbsField pnd_Extract_Record_Pnd_Na_Simplex_Duplex_Multiplex;
    private DbsField pnd_Extract_Record_Pnd_Na_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Extract_Record_Pnd_Na_Save_Contract_Hold_Code;
    private DbsField pnd_Extract_Record_Pnd_Na_Pymnt_Check_Nbr;
    private DbsField pnd_Extract_Record_Pnd_Na_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Extract_Record_Pnd_Na_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Extract_Record_Rcrd_Typ;
    private DbsField pnd_Extract_Record_Na_Cntrct_Orgn_Cde;
    private DbsField pnd_Extract_Record_Na_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Extract_Record_Na_Cntrct_Payee_Cde;
    private DbsField pnd_Extract_Record_Na_Cntrct_Invrse_Dte;
    private DbsField pnd_Extract_Record_Na_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pnd_Extract_Record_Ph_Name;
    private DbsField pnd_Extract_Record_Ph_Last_Name;
    private DbsField pnd_Extract_Record_Ph_First_Name;
    private DbsField pnd_Extract_Record_Ph_Middle_Name;
    private DbsGroup pnd_Extract_Record_Nme_N_Addr;
    private DbsField pnd_Extract_Record_Pymnt_Nme;
    private DbsField pnd_Extract_Record_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Extract_Record_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Extract_Record_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Extract_Record_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Extract_Record_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Extract_Record_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Extract_Record_Pymnt_Addr_Zip_Cde;
    private DbsField pnd_Extract_Record_Pymnt_Postl_Data;
    private DbsField pnd_Extract_Record_Pymnt_Addr_Type_Ind;
    private DbsField pnd_Extract_Record_Pymnt_Addr_Last_Chg_Dte;
    private DbsField pnd_Extract_Record_Pymnt_Addr_Last_Chg_Tme;
    private DbsField pnd_Extract_Record_Pymnt_Eft_Transit_Id;
    private DbsField pnd_Extract_Record_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Extract_Record_Pymnt_Chk_Sav_Ind;
    private DbsField pnd_Extract_Record_Pymnt_Deceased_Nme;
    private DbsField pnd_Extract_Record_Na_Cntrct_Hold_Tme;
    private DbsField pnd_Extract_Record_Pnd_Ws_Name_N_Address_Filler;
    private DbsGroup pnd_Date_Control_Info;
    private DbsGroup pnd_Date_Control_Info_Pnd_Date_Info;
    private DbsField pnd_Date_Control_Info_Pnd_Payment_Date;
    private DbsGroup pnd_Date_Control_Info_Pnd_Payment_DateRedef13;
    private DbsField pnd_Date_Control_Info_Pnd_Payment_Date_1_2;
    private DbsField pnd_Date_Control_Info_Pnd_Payment_Date_3_8;
    private DbsField pnd_Date_Control_Info_Pnd_Future_Payment_Date;
    private DbsGroup pnd_Date_Control_Info_Pnd_Future_Payment_DateRedef14;
    private DbsField pnd_Date_Control_Info_Pnd_Future_Payment_Date_1_2;
    private DbsField pnd_Date_Control_Info_Pnd_Future_Payment_Date_3_8;
    private DbsField pnd_Date_Control_Info_Pnd_Accounting_Date;
    private DbsGroup pnd_Date_Control_Info_Pnd_Accounting_DateRedef15;
    private DbsField pnd_Date_Control_Info_Pnd_Accounting_Date_1_2;
    private DbsField pnd_Date_Control_Info_Pnd_Accounting_Date_3_8;
    private DbsField pnd_Date_Control_Info_Pnd_Future_Accounting_Date;
    private DbsGroup pnd_Date_Control_Info_Pnd_Future_Accounting_DateRedef16;
    private DbsField pnd_Date_Control_Info_Pnd_Future_Accounting_Date_1_2;
    private DbsField pnd_Date_Control_Info_Pnd_Future_Accounting_Date_3_8;
    private DbsGroup pnd_Date_Control_Info_Pnd_Control_Info;
    private DbsField pnd_Date_Control_Info_Pnd_Cntl_Cde;
    private DbsField pnd_Date_Control_Info_Pnd_Cntl_Can_Rdw_Ind;
    private DbsField pnd_Date_Control_Info_Pnd_Cntl_Cnt;
    private DbsField pnd_Date_Control_Info_Pnd_Cntl_Gross_Amt;
    private DbsField pnd_Date_Control_Info_Pnd_Cntl_Net_Amt;
    private DbsField pnd_Ws_Date_Alpha;
    private DbsGroup pnd_Ws_Date_AlphaRedef17;
    private DbsField pnd_Ws_Date_Alpha_Pnd_Ws_Date;
    private DbsGroup pnd_Ws_Date_Alpha_Pnd_Ws_DateRedef18;
    private DbsField pnd_Ws_Date_Alpha_Pnd_Ws_Date_Cc;
    private DbsField pnd_Ws_Date_Alpha_Pnd_Ws_Date_Yy;
    private DbsField pnd_Ws_Date_Alpha_Pnd_Ws_Date_Mm;
    private DbsField pnd_Ws_Date_Alpha_Pnd_Ws_Date_Dd;
    private DbsField pnd_Cntl_Super_Des;
    private DbsGroup pnd_Cntl_Super_DesRedef19;
    private DbsField pnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde;
    private DbsField pnd_Cntl_Super_Des_Pnd_Cntl_Invrse_Dte;

    public DbsGroup getPnd_Extract_Record() { return pnd_Extract_Record; }

    public DbsField getPnd_Extract_Record_Pnd_Extract1() { return pnd_Extract_Record_Pnd_Extract1; }

    public DbsGroup getPnd_Extract_Record_Pnd_Extract1Redef1() { return pnd_Extract_Record_Pnd_Extract1Redef1; }

    public DbsField getPnd_Extract_Record_Pnd_Record_Level_Nmbr() { return pnd_Extract_Record_Pnd_Record_Level_Nmbr; }

    public DbsField getPnd_Extract_Record_Pnd_Record_Occur_Nmbr() { return pnd_Extract_Record_Pnd_Record_Occur_Nmbr; }

    public DbsField getPnd_Extract_Record_Pnd_Simplex_Duplex_Multiplex() { return pnd_Extract_Record_Pnd_Simplex_Duplex_Multiplex; }

    public DbsField getPnd_Extract_Record_Pnd_Save_Pymnt_Prcss_Seq_Num() { return pnd_Extract_Record_Pnd_Save_Pymnt_Prcss_Seq_Num; }

    public DbsField getPnd_Extract_Record_Pnd_Save_Contract_Hold_Code() { return pnd_Extract_Record_Pnd_Save_Contract_Hold_Code; }

    public DbsField getPnd_Extract_Record_Pnd_Pymnt_Check_Nbr() { return pnd_Extract_Record_Pnd_Pymnt_Check_Nbr; }

    public DbsField getPnd_Extract_Record_Pnd_Pymnt_Check_Seq_Nbr() { return pnd_Extract_Record_Pnd_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Extract_Record_Pnd_Cntrct_Cmbn_Nbr() { return pnd_Extract_Record_Pnd_Cntrct_Cmbn_Nbr; }

    public DbsField getPnd_Extract_Record_Pnd_Record_Filler_1() { return pnd_Extract_Record_Pnd_Record_Filler_1; }

    public DbsField getPnd_Extract_Record_Pnd_Extract2() { return pnd_Extract_Record_Pnd_Extract2; }

    public DbsGroup getPnd_Extract_RecordRedef2() { return pnd_Extract_RecordRedef2; }

    public DbsField getPnd_Extract_Record_Pnd_Hdr_Record_Level_Nmbr() { return pnd_Extract_Record_Pnd_Hdr_Record_Level_Nmbr; }

    public DbsField getPnd_Extract_Record_Pnd_Hdr_Record_Occur_Nmbr() { return pnd_Extract_Record_Pnd_Hdr_Record_Occur_Nmbr; }

    public DbsField getPnd_Extract_Record_Pnd_Hdr_Simplex_Duplex_Multiplex() { return pnd_Extract_Record_Pnd_Hdr_Simplex_Duplex_Multiplex; }

    public DbsField getPnd_Extract_Record_Pnd_Hdr_Save_Pymnt_Prcss_Seq_Num() { return pnd_Extract_Record_Pnd_Hdr_Save_Pymnt_Prcss_Seq_Num; }

    public DbsField getPnd_Extract_Record_Pnd_Hdr_Save_Contract_Hold_Code() { return pnd_Extract_Record_Pnd_Hdr_Save_Contract_Hold_Code; }

    public DbsField getPnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Nbr() { return pnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Nbr; }

    public DbsField getPnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Seq_Nbr() { return pnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Extract_Record_Pnd_Hdr_Cntrct_Cmbn_Nbr() { return pnd_Extract_Record_Pnd_Hdr_Cntrct_Cmbn_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Ppcn_Nbr() { return pnd_Extract_Record_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Payee_Cde() { return pnd_Extract_Record_Cntrct_Payee_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Invrse_Dte() { return pnd_Extract_Record_Cntrct_Invrse_Dte; }

    public DbsField getPnd_Extract_Record_Cntrct_Check_Crrncy_Cde() { return pnd_Extract_Record_Cntrct_Check_Crrncy_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Crrncy_Cde() { return pnd_Extract_Record_Cntrct_Crrncy_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Orgn_Cde() { return pnd_Extract_Record_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Qlfied_Cde() { return pnd_Extract_Record_Cntrct_Qlfied_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Pymnt_Type_Ind() { return pnd_Extract_Record_Cntrct_Pymnt_Type_Ind; }

    public DbsField getPnd_Extract_Record_Cntrct_Sttlmnt_Type_Ind() { return pnd_Extract_Record_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getPnd_Extract_Record_Cntrct_Sps_Cde() { return pnd_Extract_Record_Cntrct_Sps_Cde; }

    public DbsField getPnd_Extract_Record_Pymnt_Rqst_Rmndr_Pct() { return pnd_Extract_Record_Pymnt_Rqst_Rmndr_Pct; }

    public DbsField getPnd_Extract_Record_Pymnt_Stats_Cde() { return pnd_Extract_Record_Pymnt_Stats_Cde; }

    public DbsField getPnd_Extract_Record_Pymnt_Annot_Ind() { return pnd_Extract_Record_Pymnt_Annot_Ind; }

    public DbsField getPnd_Extract_Record_Pymnt_Cmbne_Ind() { return pnd_Extract_Record_Pymnt_Cmbne_Ind; }

    public DbsField getPnd_Extract_Record_Pymnt_Ftre_Ind() { return pnd_Extract_Record_Pymnt_Ftre_Ind; }

    public DbsField getPnd_Extract_Record_Pymnt_Payee_Na_Addr_Trggr() { return pnd_Extract_Record_Pymnt_Payee_Na_Addr_Trggr; }

    public DbsField getPnd_Extract_Record_Pymnt_Payee_Tx_Elct_Trggr() { return pnd_Extract_Record_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getPnd_Extract_Record_Pymnt_Inst_Rep_Cde() { return pnd_Extract_Record_Pymnt_Inst_Rep_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Dvdnd_Payee_Ind() { return pnd_Extract_Record_Cntrct_Dvdnd_Payee_Ind; }

    public DbsField getPnd_Extract_Record_Annt_Soc_Sec_Ind() { return pnd_Extract_Record_Annt_Soc_Sec_Ind; }

    public DbsField getPnd_Extract_Record_Pymnt_Pay_Type_Req_Ind() { return pnd_Extract_Record_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPnd_Extract_Record_Cntrct_Rqst_Settl_Dte() { return pnd_Extract_Record_Cntrct_Rqst_Settl_Dte; }

    public DbsField getPnd_Extract_Record_Cntrct_Rqst_Dte() { return pnd_Extract_Record_Cntrct_Rqst_Dte; }

    public DbsField getPnd_Extract_Record_Cntrct_Rqst_Settl_Tme() { return pnd_Extract_Record_Cntrct_Rqst_Settl_Tme; }

    public DbsField getPnd_Extract_Record_Cntrct_Unq_Id_Nbr() { return pnd_Extract_Record_Cntrct_Unq_Id_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Type_Cde() { return pnd_Extract_Record_Cntrct_Type_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Lob_Cde() { return pnd_Extract_Record_Cntrct_Lob_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Sub_Lob_Cde() { return pnd_Extract_Record_Cntrct_Sub_Lob_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Ia_Lob_Cde() { return pnd_Extract_Record_Cntrct_Ia_Lob_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Cref_Nbr() { return pnd_Extract_Record_Cntrct_Cref_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Option_Cde() { return pnd_Extract_Record_Cntrct_Option_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Mode_Cde() { return pnd_Extract_Record_Cntrct_Mode_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Pymnt_Dest_Cde() { return pnd_Extract_Record_Cntrct_Pymnt_Dest_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Roll_Dest_Cde() { return pnd_Extract_Record_Cntrct_Roll_Dest_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Dvdnd_Payee_Cde() { return pnd_Extract_Record_Cntrct_Dvdnd_Payee_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Hold_Cde() { return pnd_Extract_Record_Cntrct_Hold_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Hold_Grp() { return pnd_Extract_Record_Cntrct_Hold_Grp; }

    public DbsField getPnd_Extract_Record_Cntrct_Da_Tiaa_1_Nbr() { return pnd_Extract_Record_Cntrct_Da_Tiaa_1_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Da_Tiaa_2_Nbr() { return pnd_Extract_Record_Cntrct_Da_Tiaa_2_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Da_Tiaa_3_Nbr() { return pnd_Extract_Record_Cntrct_Da_Tiaa_3_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Da_Tiaa_4_Nbr() { return pnd_Extract_Record_Cntrct_Da_Tiaa_4_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Da_Tiaa_5_Nbr() { return pnd_Extract_Record_Cntrct_Da_Tiaa_5_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Da_Cref_1_Nbr() { return pnd_Extract_Record_Cntrct_Da_Cref_1_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Da_Cref_2_Nbr() { return pnd_Extract_Record_Cntrct_Da_Cref_2_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Da_Cref_3_Nbr() { return pnd_Extract_Record_Cntrct_Da_Cref_3_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Da_Cref_4_Nbr() { return pnd_Extract_Record_Cntrct_Da_Cref_4_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Da_Cref_5_Nbr() { return pnd_Extract_Record_Cntrct_Da_Cref_5_Nbr; }

    public DbsField getPnd_Extract_Record_Annt_Soc_Sec_Nbr() { return pnd_Extract_Record_Annt_Soc_Sec_Nbr; }

    public DbsField getPnd_Extract_Record_Annt_Ctznshp_Cde() { return pnd_Extract_Record_Annt_Ctznshp_Cde; }

    public DbsField getPnd_Extract_Record_Annt_Rsdncy_Cde() { return pnd_Extract_Record_Annt_Rsdncy_Cde; }

    public DbsField getPnd_Extract_Record_Pymnt_Split_Cde() { return pnd_Extract_Record_Pymnt_Split_Cde; }

    public DbsField getPnd_Extract_Record_Pymnt_Split_Reasn_Cde() { return pnd_Extract_Record_Pymnt_Split_Reasn_Cde; }

    public DbsGroup getPnd_Extract_Record_Pymnt_Split_Reasn_CdeRedef3() { return pnd_Extract_Record_Pymnt_Split_Reasn_CdeRedef3; }

    public DbsField getPnd_Extract_Record_Pymnt_Method_Cde() { return pnd_Extract_Record_Pymnt_Method_Cde; }

    public DbsField getPnd_Extract_Record_Pymnt_Method_Filler() { return pnd_Extract_Record_Pymnt_Method_Filler; }

    public DbsField getPnd_Extract_Record_Pymnt_Check_Dte() { return pnd_Extract_Record_Pymnt_Check_Dte; }

    public DbsField getPnd_Extract_Record_Pymnt_Cycle_Dte() { return pnd_Extract_Record_Pymnt_Cycle_Dte; }

    public DbsField getPnd_Extract_Record_Pymnt_Eft_Dte() { return pnd_Extract_Record_Pymnt_Eft_Dte; }

    public DbsField getPnd_Extract_Record_Pymnt_Rqst_Pct() { return pnd_Extract_Record_Pymnt_Rqst_Pct; }

    public DbsField getPnd_Extract_Record_Pymnt_Rqst_Amt() { return pnd_Extract_Record_Pymnt_Rqst_Amt; }

    public DbsField getPnd_Extract_Record_Pymnt_Check_Nbr() { return pnd_Extract_Record_Pymnt_Check_Nbr; }

    public DbsField getPnd_Extract_Record_Pymnt_Prcss_Seq_Nbr() { return pnd_Extract_Record_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPnd_Extract_Record_Pymnt_Prcss_Seq_NbrRedef4() { return pnd_Extract_Record_Pymnt_Prcss_Seq_NbrRedef4; }

    public DbsField getPnd_Extract_Record_Pymnt_Prcss_Seq_Num() { return pnd_Extract_Record_Pymnt_Prcss_Seq_Num; }

    public DbsField getPnd_Extract_Record_Pymnt_Instmt_Nbr() { return pnd_Extract_Record_Pymnt_Instmt_Nbr; }

    public DbsField getPnd_Extract_Record_Pymnt_Check_Scrty_Nbr() { return pnd_Extract_Record_Pymnt_Check_Scrty_Nbr; }

    public DbsField getPnd_Extract_Record_Pymnt_Check_Amt() { return pnd_Extract_Record_Pymnt_Check_Amt; }

    public DbsField getPnd_Extract_Record_Pymnt_Settlmnt_Dte() { return pnd_Extract_Record_Pymnt_Settlmnt_Dte; }

    public DbsField getPnd_Extract_Record_Pymnt_Check_Seq_Nbr() { return pnd_Extract_Record_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Ec_Oprtr_Id_Nbr() { return pnd_Extract_Record_Cntrct_Ec_Oprtr_Id_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Cmbn_Nbr() { return pnd_Extract_Record_Cntrct_Cmbn_Nbr; }

    public DbsField getPnd_Extract_Record_Cntrct_Hold_Tme() { return pnd_Extract_Record_Cntrct_Hold_Tme; }

    public DbsField getPnd_Extract_Record_Cntrct_Cancel_Rdrw_Ind() { return pnd_Extract_Record_Cntrct_Cancel_Rdrw_Ind; }

    public DbsField getPnd_Extract_Record_Pymnt_Acctg_Dte() { return pnd_Extract_Record_Pymnt_Acctg_Dte; }

    public DbsField getPnd_Extract_Record_Pymnt_Reqst_Log_Dte_Time() { return pnd_Extract_Record_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getPnd_Extract_Record_Pymnt_Intrfce_Dte() { return pnd_Extract_Record_Pymnt_Intrfce_Dte; }

    public DbsField getPnd_Extract_Record_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pnd_Extract_Record_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getPnd_Extract_Record_Cnr_Orgnl_Invrse_Dte() { return pnd_Extract_Record_Cnr_Orgnl_Invrse_Dte; }

    public DbsField getPnd_Extract_Record_Cntrct_Annty_Ins_Type() { return pnd_Extract_Record_Cntrct_Annty_Ins_Type; }

    public DbsField getPnd_Extract_Record_Cntrct_Annty_Type_Cde() { return pnd_Extract_Record_Cntrct_Annty_Type_Cde; }

    public DbsField getPnd_Extract_Record_Cntrct_Insurance_Option() { return pnd_Extract_Record_Cntrct_Insurance_Option; }

    public DbsField getPnd_Extract_Record_Cntrct_Life_Contingency() { return pnd_Extract_Record_Cntrct_Life_Contingency; }

    public DbsField getPnd_Extract_Record_Pnd_Ws_Header_Filler() { return pnd_Extract_Record_Pnd_Ws_Header_Filler; }

    public DbsGroup getPnd_Extract_RecordRedef5() { return pnd_Extract_RecordRedef5; }

    public DbsField getPnd_Extract_Record_Pnd_Inv_Record_Level_Nmbr() { return pnd_Extract_Record_Pnd_Inv_Record_Level_Nmbr; }

    public DbsField getPnd_Extract_Record_Pnd_Inv_Record_Occur_Nmbr() { return pnd_Extract_Record_Pnd_Inv_Record_Occur_Nmbr; }

    public DbsField getPnd_Extract_Record_Pnd_Inv_Simplex_Duplex_Multiplex() { return pnd_Extract_Record_Pnd_Inv_Simplex_Duplex_Multiplex; }

    public DbsField getPnd_Extract_Record_Pnd_Inv_Save_Pymnt_Prcss_Seq_Num() { return pnd_Extract_Record_Pnd_Inv_Save_Pymnt_Prcss_Seq_Num; }

    public DbsField getPnd_Extract_Record_Pnd_Inv_Save_Contract_Hold_Code() { return pnd_Extract_Record_Pnd_Inv_Save_Contract_Hold_Code; }

    public DbsField getPnd_Extract_Record_Pnd_Inv_Pymnt_Check_Nbr() { return pnd_Extract_Record_Pnd_Inv_Pymnt_Check_Nbr; }

    public DbsField getPnd_Extract_Record_Pnd_Inv_Pymnt_Check_Seq_Nbr() { return pnd_Extract_Record_Pnd_Inv_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Extract_Record_Pnd_Inv_Cntrct_Cmbn_Nbr() { return pnd_Extract_Record_Pnd_Inv_Cntrct_Cmbn_Nbr; }

    public DbsField getPnd_Extract_Record_Pnd_Cntr_Inv_Acct() { return pnd_Extract_Record_Pnd_Cntr_Inv_Acct; }

    public DbsField getPnd_Extract_Record_Pnd_Ws_Side() { return pnd_Extract_Record_Pnd_Ws_Side; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Cde() { return pnd_Extract_Record_Inv_Acct_Cde; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Unit_Qty() { return pnd_Extract_Record_Inv_Acct_Unit_Qty; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Unit_Value() { return pnd_Extract_Record_Inv_Acct_Unit_Value; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Settl_Amt() { return pnd_Extract_Record_Inv_Acct_Settl_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Fed_Cde() { return pnd_Extract_Record_Inv_Acct_Fed_Cde; }

    public DbsField getPnd_Extract_Record_Inv_Acct_State_Cde() { return pnd_Extract_Record_Inv_Acct_State_Cde; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Local_Cde() { return pnd_Extract_Record_Inv_Acct_Local_Cde; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Ivc_Amt() { return pnd_Extract_Record_Inv_Acct_Ivc_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Dci_Amt() { return pnd_Extract_Record_Inv_Acct_Dci_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Dpi_Amt() { return pnd_Extract_Record_Inv_Acct_Dpi_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Start_Accum_Amt() { return pnd_Extract_Record_Inv_Acct_Start_Accum_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_End_Accum_Amt() { return pnd_Extract_Record_Inv_Acct_End_Accum_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Dvdnd_Amt() { return pnd_Extract_Record_Inv_Acct_Dvdnd_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Net_Pymnt_Amt() { return pnd_Extract_Record_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Ivc_Ind() { return pnd_Extract_Record_Inv_Acct_Ivc_Ind; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Adj_Ivc_Amt() { return pnd_Extract_Record_Inv_Acct_Adj_Ivc_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Valuat_Period() { return pnd_Extract_Record_Inv_Acct_Valuat_Period; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Fdrl_Tax_Amt() { return pnd_Extract_Record_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_State_Tax_Amt() { return pnd_Extract_Record_Inv_Acct_State_Tax_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Local_Tax_Amt() { return pnd_Extract_Record_Inv_Acct_Local_Tax_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Acct_Exp_Amt() { return pnd_Extract_Record_Inv_Acct_Exp_Amt; }

    public DbsField getPnd_Extract_Record_Inv_Cntrct_Ppcn_Nbr() { return pnd_Extract_Record_Inv_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Extract_Record_Inv_Cntrct_Payee_Cde() { return pnd_Extract_Record_Inv_Cntrct_Payee_Cde; }

    public DbsField getPnd_Extract_Record_Inv_Cntrct_Pymnt_Type_Ind() { return pnd_Extract_Record_Inv_Cntrct_Pymnt_Type_Ind; }

    public DbsField getPnd_Extract_Record_Inv_Cntrct_Sttlmnt_Type_Ind() { return pnd_Extract_Record_Inv_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getPnd_Extract_Record_Inv_Cntrct_Option_Cde() { return pnd_Extract_Record_Inv_Cntrct_Option_Cde; }

    public DbsField getPnd_Extract_Record_Inv_Cntrct_Mode_Cde() { return pnd_Extract_Record_Inv_Cntrct_Mode_Cde; }

    public DbsField getPnd_Extract_Record_Pnd_Cntr_Deductions() { return pnd_Extract_Record_Pnd_Cntr_Deductions; }

    public DbsField getPnd_Extract_Record_Pymnt_Ded_Cde() { return pnd_Extract_Record_Pymnt_Ded_Cde; }

    public DbsField getPnd_Extract_Record_Pymnt_Ded_Payee_Cde() { return pnd_Extract_Record_Pymnt_Ded_Payee_Cde; }

    public DbsField getPnd_Extract_Record_Pymnt_Ded_Amt() { return pnd_Extract_Record_Pymnt_Ded_Amt; }

    public DbsGroup getPnd_Extract_Record_Pnd_Pyhdr_Data() { return pnd_Extract_Record_Pnd_Pyhdr_Data; }

    public DbsField getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte; }

    public DbsGroup getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef6() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef6; }

    public DbsField getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4; }

    public DbsGroup getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef7() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef7; }

    public DbsField getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6; }

    public DbsGroup getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef8() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef8; }

    public DbsField getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start; }

    public DbsField getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End; }

    public DbsGroup getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef9() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef9; 
        }

    public DbsField getPnd_Extract_Record_Pnd_Pyhdr_Annt_Ctznshp_Cde() { return pnd_Extract_Record_Pnd_Pyhdr_Annt_Ctznshp_Cde; }

    public DbsField getPnd_Extract_Record_Pnd_Pyhdr_Annt_Rsdncy_Cde() { return pnd_Extract_Record_Pnd_Pyhdr_Annt_Rsdncy_Cde; }

    public DbsGroup getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef10() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef10; 
        }

    public DbsField getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_P6() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_P6; }

    public DbsGroup getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef11() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef11; 
        }

    public DbsField getPnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A4() { return pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A4; }

    public DbsField getPnd_Extract_Record_Pnd_Inv_Acct_Filler() { return pnd_Extract_Record_Pnd_Inv_Acct_Filler; }

    public DbsGroup getPnd_Extract_RecordRedef12() { return pnd_Extract_RecordRedef12; }

    public DbsField getPnd_Extract_Record_Pnd_Na_Record_Level_Nmbr() { return pnd_Extract_Record_Pnd_Na_Record_Level_Nmbr; }

    public DbsField getPnd_Extract_Record_Pnd_Na_Record_Occur_Nmbr() { return pnd_Extract_Record_Pnd_Na_Record_Occur_Nmbr; }

    public DbsField getPnd_Extract_Record_Pnd_Na_Simplex_Duplex_Multiplex() { return pnd_Extract_Record_Pnd_Na_Simplex_Duplex_Multiplex; }

    public DbsField getPnd_Extract_Record_Pnd_Na_Save_Pymnt_Prcss_Seq_Num() { return pnd_Extract_Record_Pnd_Na_Save_Pymnt_Prcss_Seq_Num; }

    public DbsField getPnd_Extract_Record_Pnd_Na_Save_Contract_Hold_Code() { return pnd_Extract_Record_Pnd_Na_Save_Contract_Hold_Code; }

    public DbsField getPnd_Extract_Record_Pnd_Na_Pymnt_Check_Nbr() { return pnd_Extract_Record_Pnd_Na_Pymnt_Check_Nbr; }

    public DbsField getPnd_Extract_Record_Pnd_Na_Pymnt_Check_Seq_Nbr() { return pnd_Extract_Record_Pnd_Na_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Extract_Record_Pnd_Na_Cntrct_Cmbn_Nbr() { return pnd_Extract_Record_Pnd_Na_Cntrct_Cmbn_Nbr; }

    public DbsField getPnd_Extract_Record_Rcrd_Typ() { return pnd_Extract_Record_Rcrd_Typ; }

    public DbsField getPnd_Extract_Record_Na_Cntrct_Orgn_Cde() { return pnd_Extract_Record_Na_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Extract_Record_Na_Cntrct_Ppcn_Nbr() { return pnd_Extract_Record_Na_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Extract_Record_Na_Cntrct_Payee_Cde() { return pnd_Extract_Record_Na_Cntrct_Payee_Cde; }

    public DbsField getPnd_Extract_Record_Na_Cntrct_Invrse_Dte() { return pnd_Extract_Record_Na_Cntrct_Invrse_Dte; }

    public DbsField getPnd_Extract_Record_Na_Pymnt_Prcss_Seq_Nbr() { return pnd_Extract_Record_Na_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPnd_Extract_Record_Ph_Name() { return pnd_Extract_Record_Ph_Name; }

    public DbsField getPnd_Extract_Record_Ph_Last_Name() { return pnd_Extract_Record_Ph_Last_Name; }

    public DbsField getPnd_Extract_Record_Ph_First_Name() { return pnd_Extract_Record_Ph_First_Name; }

    public DbsField getPnd_Extract_Record_Ph_Middle_Name() { return pnd_Extract_Record_Ph_Middle_Name; }

    public DbsGroup getPnd_Extract_Record_Nme_N_Addr() { return pnd_Extract_Record_Nme_N_Addr; }

    public DbsField getPnd_Extract_Record_Pymnt_Nme() { return pnd_Extract_Record_Pymnt_Nme; }

    public DbsField getPnd_Extract_Record_Pymnt_Addr_Line1_Txt() { return pnd_Extract_Record_Pymnt_Addr_Line1_Txt; }

    public DbsField getPnd_Extract_Record_Pymnt_Addr_Line2_Txt() { return pnd_Extract_Record_Pymnt_Addr_Line2_Txt; }

    public DbsField getPnd_Extract_Record_Pymnt_Addr_Line3_Txt() { return pnd_Extract_Record_Pymnt_Addr_Line3_Txt; }

    public DbsField getPnd_Extract_Record_Pymnt_Addr_Line4_Txt() { return pnd_Extract_Record_Pymnt_Addr_Line4_Txt; }

    public DbsField getPnd_Extract_Record_Pymnt_Addr_Line5_Txt() { return pnd_Extract_Record_Pymnt_Addr_Line5_Txt; }

    public DbsField getPnd_Extract_Record_Pymnt_Addr_Line6_Txt() { return pnd_Extract_Record_Pymnt_Addr_Line6_Txt; }

    public DbsField getPnd_Extract_Record_Pymnt_Addr_Zip_Cde() { return pnd_Extract_Record_Pymnt_Addr_Zip_Cde; }

    public DbsField getPnd_Extract_Record_Pymnt_Postl_Data() { return pnd_Extract_Record_Pymnt_Postl_Data; }

    public DbsField getPnd_Extract_Record_Pymnt_Addr_Type_Ind() { return pnd_Extract_Record_Pymnt_Addr_Type_Ind; }

    public DbsField getPnd_Extract_Record_Pymnt_Addr_Last_Chg_Dte() { return pnd_Extract_Record_Pymnt_Addr_Last_Chg_Dte; }

    public DbsField getPnd_Extract_Record_Pymnt_Addr_Last_Chg_Tme() { return pnd_Extract_Record_Pymnt_Addr_Last_Chg_Tme; }

    public DbsField getPnd_Extract_Record_Pymnt_Eft_Transit_Id() { return pnd_Extract_Record_Pymnt_Eft_Transit_Id; }

    public DbsField getPnd_Extract_Record_Pymnt_Eft_Acct_Nbr() { return pnd_Extract_Record_Pymnt_Eft_Acct_Nbr; }

    public DbsField getPnd_Extract_Record_Pymnt_Chk_Sav_Ind() { return pnd_Extract_Record_Pymnt_Chk_Sav_Ind; }

    public DbsField getPnd_Extract_Record_Pymnt_Deceased_Nme() { return pnd_Extract_Record_Pymnt_Deceased_Nme; }

    public DbsField getPnd_Extract_Record_Na_Cntrct_Hold_Tme() { return pnd_Extract_Record_Na_Cntrct_Hold_Tme; }

    public DbsField getPnd_Extract_Record_Pnd_Ws_Name_N_Address_Filler() { return pnd_Extract_Record_Pnd_Ws_Name_N_Address_Filler; }

    public DbsGroup getPnd_Date_Control_Info() { return pnd_Date_Control_Info; }

    public DbsGroup getPnd_Date_Control_Info_Pnd_Date_Info() { return pnd_Date_Control_Info_Pnd_Date_Info; }

    public DbsField getPnd_Date_Control_Info_Pnd_Payment_Date() { return pnd_Date_Control_Info_Pnd_Payment_Date; }

    public DbsGroup getPnd_Date_Control_Info_Pnd_Payment_DateRedef13() { return pnd_Date_Control_Info_Pnd_Payment_DateRedef13; }

    public DbsField getPnd_Date_Control_Info_Pnd_Payment_Date_1_2() { return pnd_Date_Control_Info_Pnd_Payment_Date_1_2; }

    public DbsField getPnd_Date_Control_Info_Pnd_Payment_Date_3_8() { return pnd_Date_Control_Info_Pnd_Payment_Date_3_8; }

    public DbsField getPnd_Date_Control_Info_Pnd_Future_Payment_Date() { return pnd_Date_Control_Info_Pnd_Future_Payment_Date; }

    public DbsGroup getPnd_Date_Control_Info_Pnd_Future_Payment_DateRedef14() { return pnd_Date_Control_Info_Pnd_Future_Payment_DateRedef14; }

    public DbsField getPnd_Date_Control_Info_Pnd_Future_Payment_Date_1_2() { return pnd_Date_Control_Info_Pnd_Future_Payment_Date_1_2; }

    public DbsField getPnd_Date_Control_Info_Pnd_Future_Payment_Date_3_8() { return pnd_Date_Control_Info_Pnd_Future_Payment_Date_3_8; }

    public DbsField getPnd_Date_Control_Info_Pnd_Accounting_Date() { return pnd_Date_Control_Info_Pnd_Accounting_Date; }

    public DbsGroup getPnd_Date_Control_Info_Pnd_Accounting_DateRedef15() { return pnd_Date_Control_Info_Pnd_Accounting_DateRedef15; }

    public DbsField getPnd_Date_Control_Info_Pnd_Accounting_Date_1_2() { return pnd_Date_Control_Info_Pnd_Accounting_Date_1_2; }

    public DbsField getPnd_Date_Control_Info_Pnd_Accounting_Date_3_8() { return pnd_Date_Control_Info_Pnd_Accounting_Date_3_8; }

    public DbsField getPnd_Date_Control_Info_Pnd_Future_Accounting_Date() { return pnd_Date_Control_Info_Pnd_Future_Accounting_Date; }

    public DbsGroup getPnd_Date_Control_Info_Pnd_Future_Accounting_DateRedef16() { return pnd_Date_Control_Info_Pnd_Future_Accounting_DateRedef16; }

    public DbsField getPnd_Date_Control_Info_Pnd_Future_Accounting_Date_1_2() { return pnd_Date_Control_Info_Pnd_Future_Accounting_Date_1_2; }

    public DbsField getPnd_Date_Control_Info_Pnd_Future_Accounting_Date_3_8() { return pnd_Date_Control_Info_Pnd_Future_Accounting_Date_3_8; }

    public DbsGroup getPnd_Date_Control_Info_Pnd_Control_Info() { return pnd_Date_Control_Info_Pnd_Control_Info; }

    public DbsField getPnd_Date_Control_Info_Pnd_Cntl_Cde() { return pnd_Date_Control_Info_Pnd_Cntl_Cde; }

    public DbsField getPnd_Date_Control_Info_Pnd_Cntl_Can_Rdw_Ind() { return pnd_Date_Control_Info_Pnd_Cntl_Can_Rdw_Ind; }

    public DbsField getPnd_Date_Control_Info_Pnd_Cntl_Cnt() { return pnd_Date_Control_Info_Pnd_Cntl_Cnt; }

    public DbsField getPnd_Date_Control_Info_Pnd_Cntl_Gross_Amt() { return pnd_Date_Control_Info_Pnd_Cntl_Gross_Amt; }

    public DbsField getPnd_Date_Control_Info_Pnd_Cntl_Net_Amt() { return pnd_Date_Control_Info_Pnd_Cntl_Net_Amt; }

    public DbsField getPnd_Ws_Date_Alpha() { return pnd_Ws_Date_Alpha; }

    public DbsGroup getPnd_Ws_Date_AlphaRedef17() { return pnd_Ws_Date_AlphaRedef17; }

    public DbsField getPnd_Ws_Date_Alpha_Pnd_Ws_Date() { return pnd_Ws_Date_Alpha_Pnd_Ws_Date; }

    public DbsGroup getPnd_Ws_Date_Alpha_Pnd_Ws_DateRedef18() { return pnd_Ws_Date_Alpha_Pnd_Ws_DateRedef18; }

    public DbsField getPnd_Ws_Date_Alpha_Pnd_Ws_Date_Cc() { return pnd_Ws_Date_Alpha_Pnd_Ws_Date_Cc; }

    public DbsField getPnd_Ws_Date_Alpha_Pnd_Ws_Date_Yy() { return pnd_Ws_Date_Alpha_Pnd_Ws_Date_Yy; }

    public DbsField getPnd_Ws_Date_Alpha_Pnd_Ws_Date_Mm() { return pnd_Ws_Date_Alpha_Pnd_Ws_Date_Mm; }

    public DbsField getPnd_Ws_Date_Alpha_Pnd_Ws_Date_Dd() { return pnd_Ws_Date_Alpha_Pnd_Ws_Date_Dd; }

    public DbsField getPnd_Cntl_Super_Des() { return pnd_Cntl_Super_Des; }

    public DbsGroup getPnd_Cntl_Super_DesRedef19() { return pnd_Cntl_Super_DesRedef19; }

    public DbsField getPnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde() { return pnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde; }

    public DbsField getPnd_Cntl_Super_Des_Pnd_Cntl_Invrse_Dte() { return pnd_Cntl_Super_Des_Pnd_Cntl_Invrse_Dte; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Extract_Record = newGroupInRecord("pnd_Extract_Record", "#EXTRACT-RECORD");
        pnd_Extract_Record_Pnd_Extract1 = pnd_Extract_Record.newFieldInGroup("pnd_Extract_Record_Pnd_Extract1", "#EXTRACT1", FieldType.STRING, 250);
        pnd_Extract_Record_Pnd_Extract1Redef1 = pnd_Extract_Record.newGroupInGroup("pnd_Extract_Record_Pnd_Extract1Redef1", "Redefines", pnd_Extract_Record_Pnd_Extract1);
        pnd_Extract_Record_Pnd_Record_Level_Nmbr = pnd_Extract_Record_Pnd_Extract1Redef1.newFieldInGroup("pnd_Extract_Record_Pnd_Record_Level_Nmbr", "#RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Extract_Record_Pnd_Record_Occur_Nmbr = pnd_Extract_Record_Pnd_Extract1Redef1.newFieldInGroup("pnd_Extract_Record_Pnd_Record_Occur_Nmbr", "#RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Extract_Record_Pnd_Simplex_Duplex_Multiplex = pnd_Extract_Record_Pnd_Extract1Redef1.newFieldInGroup("pnd_Extract_Record_Pnd_Simplex_Duplex_Multiplex", 
            "#SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Extract_Record_Pnd_Save_Pymnt_Prcss_Seq_Num = pnd_Extract_Record_Pnd_Extract1Redef1.newFieldInGroup("pnd_Extract_Record_Pnd_Save_Pymnt_Prcss_Seq_Num", 
            "#SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Extract_Record_Pnd_Save_Contract_Hold_Code = pnd_Extract_Record_Pnd_Extract1Redef1.newFieldInGroup("pnd_Extract_Record_Pnd_Save_Contract_Hold_Code", 
            "#SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Extract_Record_Pnd_Pymnt_Check_Nbr = pnd_Extract_Record_Pnd_Extract1Redef1.newFieldInGroup("pnd_Extract_Record_Pnd_Pymnt_Check_Nbr", "#PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Extract_Record_Pnd_Pymnt_Check_Seq_Nbr = pnd_Extract_Record_Pnd_Extract1Redef1.newFieldInGroup("pnd_Extract_Record_Pnd_Pymnt_Check_Seq_Nbr", 
            "#PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Extract_Record_Pnd_Cntrct_Cmbn_Nbr = pnd_Extract_Record_Pnd_Extract1Redef1.newFieldInGroup("pnd_Extract_Record_Pnd_Cntrct_Cmbn_Nbr", "#CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Extract_Record_Pnd_Record_Filler_1 = pnd_Extract_Record_Pnd_Extract1Redef1.newFieldInGroup("pnd_Extract_Record_Pnd_Record_Filler_1", "#RECORD-FILLER-1", 
            FieldType.STRING, 209);
        pnd_Extract_Record_Pnd_Extract2 = pnd_Extract_Record.newFieldInGroup("pnd_Extract_Record_Pnd_Extract2", "#EXTRACT2", FieldType.STRING, 250);
        pnd_Extract_RecordRedef2 = newGroupInRecord("pnd_Extract_RecordRedef2", "Redefines", pnd_Extract_Record);
        pnd_Extract_Record_Pnd_Hdr_Record_Level_Nmbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pnd_Hdr_Record_Level_Nmbr", "#HDR-RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Extract_Record_Pnd_Hdr_Record_Occur_Nmbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pnd_Hdr_Record_Occur_Nmbr", "#HDR-RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Extract_Record_Pnd_Hdr_Simplex_Duplex_Multiplex = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pnd_Hdr_Simplex_Duplex_Multiplex", 
            "#HDR-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Extract_Record_Pnd_Hdr_Save_Pymnt_Prcss_Seq_Num = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pnd_Hdr_Save_Pymnt_Prcss_Seq_Num", 
            "#HDR-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Extract_Record_Pnd_Hdr_Save_Contract_Hold_Code = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pnd_Hdr_Save_Contract_Hold_Code", 
            "#HDR-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Nbr", "#HDR-PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Seq_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Seq_Nbr", "#HDR-PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Extract_Record_Pnd_Hdr_Cntrct_Cmbn_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pnd_Hdr_Cntrct_Cmbn_Nbr", "#HDR-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Extract_Record_Cntrct_Ppcn_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Extract_Record_Cntrct_Payee_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Extract_Record_Cntrct_Invrse_Dte = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Extract_Record_Cntrct_Check_Crrncy_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Cntrct_Crrncy_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 
            1);
        pnd_Extract_Record_Cntrct_Orgn_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Extract_Record_Cntrct_Qlfied_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 
            1);
        pnd_Extract_Record_Cntrct_Pymnt_Type_Ind = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Cntrct_Sttlmnt_Type_Ind = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Cntrct_Sps_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", FieldType.STRING, 
            1);
        pnd_Extract_Record_Pymnt_Rqst_Rmndr_Pct = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Rqst_Rmndr_Pct", "PYMNT-RQST-RMNDR-PCT", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Pymnt_Stats_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1);
        pnd_Extract_Record_Pymnt_Annot_Ind = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 
            1);
        pnd_Extract_Record_Pymnt_Cmbne_Ind = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 
            1);
        pnd_Extract_Record_Pymnt_Ftre_Ind = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", FieldType.STRING, 
            1);
        pnd_Extract_Record_Pymnt_Payee_Na_Addr_Trggr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Payee_Na_Addr_Trggr", "PYMNT-PAYEE-NA-ADDR-TRGGR", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Pymnt_Payee_Tx_Elct_Trggr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Pymnt_Inst_Rep_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Inst_Rep_Cde", "PYMNT-INST-REP-CDE", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Cntrct_Dvdnd_Payee_Ind = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Dvdnd_Payee_Ind", "CNTRCT-DVDND-PAYEE-IND", 
            FieldType.NUMERIC, 1);
        pnd_Extract_Record_Annt_Soc_Sec_Ind = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 
            1);
        pnd_Extract_Record_Pymnt_Pay_Type_Req_Ind = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Extract_Record_Cntrct_Rqst_Settl_Dte = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Rqst_Settl_Dte", "CNTRCT-RQST-SETTL-DTE", 
            FieldType.DATE);
        pnd_Extract_Record_Cntrct_Rqst_Dte = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", FieldType.DATE);
        pnd_Extract_Record_Cntrct_Rqst_Settl_Tme = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Rqst_Settl_Tme", "CNTRCT-RQST-SETTL-TME", 
            FieldType.TIME);
        pnd_Extract_Record_Cntrct_Unq_Id_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Extract_Record_Cntrct_Type_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Extract_Record_Cntrct_Lob_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 
            4);
        pnd_Extract_Record_Cntrct_Sub_Lob_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Extract_Record_Cntrct_Ia_Lob_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 
            2);
        pnd_Extract_Record_Cntrct_Cref_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 
            10);
        pnd_Extract_Record_Cntrct_Option_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Extract_Record_Cntrct_Mode_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Extract_Record_Cntrct_Pymnt_Dest_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", 
            FieldType.STRING, 4);
        pnd_Extract_Record_Cntrct_Roll_Dest_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", 
            FieldType.STRING, 4);
        pnd_Extract_Record_Cntrct_Dvdnd_Payee_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", 
            FieldType.STRING, 5);
        pnd_Extract_Record_Cntrct_Hold_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            4);
        pnd_Extract_Record_Cntrct_Hold_Grp = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 
            3);
        pnd_Extract_Record_Cntrct_Da_Tiaa_1_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Da_Tiaa_1_Nbr", "CNTRCT-DA-TIAA-1-NBR", 
            FieldType.STRING, 8);
        pnd_Extract_Record_Cntrct_Da_Tiaa_2_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Da_Tiaa_2_Nbr", "CNTRCT-DA-TIAA-2-NBR", 
            FieldType.STRING, 8);
        pnd_Extract_Record_Cntrct_Da_Tiaa_3_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Da_Tiaa_3_Nbr", "CNTRCT-DA-TIAA-3-NBR", 
            FieldType.STRING, 8);
        pnd_Extract_Record_Cntrct_Da_Tiaa_4_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Da_Tiaa_4_Nbr", "CNTRCT-DA-TIAA-4-NBR", 
            FieldType.STRING, 8);
        pnd_Extract_Record_Cntrct_Da_Tiaa_5_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Da_Tiaa_5_Nbr", "CNTRCT-DA-TIAA-5-NBR", 
            FieldType.STRING, 8);
        pnd_Extract_Record_Cntrct_Da_Cref_1_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", 
            FieldType.STRING, 8);
        pnd_Extract_Record_Cntrct_Da_Cref_2_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", 
            FieldType.STRING, 8);
        pnd_Extract_Record_Cntrct_Da_Cref_3_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Da_Cref_3_Nbr", "CNTRCT-DA-CREF-3-NBR", 
            FieldType.STRING, 8);
        pnd_Extract_Record_Cntrct_Da_Cref_4_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Da_Cref_4_Nbr", "CNTRCT-DA-CREF-4-NBR", 
            FieldType.STRING, 8);
        pnd_Extract_Record_Cntrct_Da_Cref_5_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Da_Cref_5_Nbr", "CNTRCT-DA-CREF-5-NBR", 
            FieldType.STRING, 8);
        pnd_Extract_Record_Annt_Soc_Sec_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Extract_Record_Annt_Ctznshp_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            2);
        pnd_Extract_Record_Annt_Rsdncy_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 
            2);
        pnd_Extract_Record_Pymnt_Split_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", FieldType.STRING, 
            2);
        pnd_Extract_Record_Pymnt_Split_Reasn_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", 
            FieldType.STRING, 6);
        pnd_Extract_Record_Pymnt_Split_Reasn_CdeRedef3 = pnd_Extract_RecordRedef2.newGroupInGroup("pnd_Extract_Record_Pymnt_Split_Reasn_CdeRedef3", "Redefines", 
            pnd_Extract_Record_Pymnt_Split_Reasn_Cde);
        pnd_Extract_Record_Pymnt_Method_Cde = pnd_Extract_Record_Pymnt_Split_Reasn_CdeRedef3.newFieldInGroup("pnd_Extract_Record_Pymnt_Method_Cde", "PYMNT-METHOD-CDE", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Pymnt_Method_Filler = pnd_Extract_Record_Pymnt_Split_Reasn_CdeRedef3.newFieldInGroup("pnd_Extract_Record_Pymnt_Method_Filler", 
            "PYMNT-METHOD-FILLER", FieldType.STRING, 5);
        pnd_Extract_Record_Pymnt_Check_Dte = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Extract_Record_Pymnt_Cycle_Dte = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        pnd_Extract_Record_Pymnt_Eft_Dte = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        pnd_Extract_Record_Pymnt_Rqst_Pct = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Extract_Record_Pymnt_Rqst_Amt = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Extract_Record_Pymnt_Check_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Extract_Record_Pymnt_Prcss_Seq_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Extract_Record_Pymnt_Prcss_Seq_NbrRedef4 = pnd_Extract_RecordRedef2.newGroupInGroup("pnd_Extract_Record_Pymnt_Prcss_Seq_NbrRedef4", "Redefines", 
            pnd_Extract_Record_Pymnt_Prcss_Seq_Nbr);
        pnd_Extract_Record_Pymnt_Prcss_Seq_Num = pnd_Extract_Record_Pymnt_Prcss_Seq_NbrRedef4.newFieldInGroup("pnd_Extract_Record_Pymnt_Prcss_Seq_Num", 
            "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Extract_Record_Pymnt_Instmt_Nbr = pnd_Extract_Record_Pymnt_Prcss_Seq_NbrRedef4.newFieldInGroup("pnd_Extract_Record_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Extract_Record_Pymnt_Check_Scrty_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Extract_Record_Pymnt_Check_Amt = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Extract_Record_Pymnt_Settlmnt_Dte = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);
        pnd_Extract_Record_Pymnt_Check_Seq_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Extract_Record_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Ec_Oprtr_Id_Nbr", "CNTRCT-EC-OPRTR-ID-NBR", 
            FieldType.STRING, 7);
        pnd_Extract_Record_Cntrct_Cmbn_Nbr = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 
            14);
        pnd_Extract_Record_Cntrct_Hold_Tme = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME);
        pnd_Extract_Record_Cntrct_Cancel_Rdrw_Ind = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Pymnt_Acctg_Dte = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Extract_Record_Pymnt_Reqst_Log_Dte_Time = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Extract_Record_Pymnt_Intrfce_Dte = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Extract_Record_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        filler01 = pnd_Extract_RecordRedef2.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 4);
        pnd_Extract_Record_Cnr_Orgnl_Invrse_Dte = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Extract_Record_Cntrct_Annty_Ins_Type = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Cntrct_Annty_Type_Cde = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Cntrct_Insurance_Option = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Cntrct_Life_Contingency = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Pnd_Ws_Header_Filler = pnd_Extract_RecordRedef2.newFieldInGroup("pnd_Extract_Record_Pnd_Ws_Header_Filler", "#WS-HEADER-FILLER", 
            FieldType.STRING, 117);
        pnd_Extract_RecordRedef5 = newGroupInRecord("pnd_Extract_RecordRedef5", "Redefines", pnd_Extract_Record);
        pnd_Extract_Record_Pnd_Inv_Record_Level_Nmbr = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Inv_Record_Level_Nmbr", "#INV-RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Extract_Record_Pnd_Inv_Record_Occur_Nmbr = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Inv_Record_Occur_Nmbr", "#INV-RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Extract_Record_Pnd_Inv_Simplex_Duplex_Multiplex = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Inv_Simplex_Duplex_Multiplex", 
            "#INV-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Extract_Record_Pnd_Inv_Save_Pymnt_Prcss_Seq_Num = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Inv_Save_Pymnt_Prcss_Seq_Num", 
            "#INV-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Extract_Record_Pnd_Inv_Save_Contract_Hold_Code = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Inv_Save_Contract_Hold_Code", 
            "#INV-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Extract_Record_Pnd_Inv_Pymnt_Check_Nbr = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Inv_Pymnt_Check_Nbr", "#INV-PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Extract_Record_Pnd_Inv_Pymnt_Check_Seq_Nbr = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Inv_Pymnt_Check_Seq_Nbr", "#INV-PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Extract_Record_Pnd_Inv_Cntrct_Cmbn_Nbr = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Inv_Cntrct_Cmbn_Nbr", "#INV-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Extract_Record_Pnd_Cntr_Inv_Acct = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Cntr_Inv_Acct", "#CNTR-INV-ACCT", FieldType.NUMERIC, 
            3);
        pnd_Extract_Record_Pnd_Ws_Side = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Ws_Side", "#WS-SIDE", FieldType.STRING, 1);
        pnd_Extract_Record_Inv_Acct_Cde = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2);
        pnd_Extract_Record_Inv_Acct_Unit_Qty = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9,3);
        pnd_Extract_Record_Inv_Acct_Unit_Value = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", 
            FieldType.PACKED_DECIMAL, 9,4);
        pnd_Extract_Record_Inv_Acct_Settl_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Extract_Record_Inv_Acct_Fed_Cde = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 
            1);
        pnd_Extract_Record_Inv_Acct_State_Cde = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Inv_Acct_Local_Cde = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Inv_Acct_Ivc_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Extract_Record_Inv_Acct_Dci_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Extract_Record_Inv_Acct_Dpi_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Extract_Record_Inv_Acct_Start_Accum_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Extract_Record_Inv_Acct_End_Accum_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Extract_Record_Inv_Acct_Dvdnd_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Extract_Record_Inv_Acct_Net_Pymnt_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Extract_Record_Inv_Acct_Ivc_Ind = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        pnd_Extract_Record_Inv_Acct_Adj_Ivc_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Extract_Record_Inv_Acct_Valuat_Period = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Inv_Acct_Fdrl_Tax_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Extract_Record_Inv_Acct_State_Tax_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Extract_Record_Inv_Acct_Local_Tax_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Extract_Record_Inv_Acct_Exp_Amt = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Extract_Record_Inv_Cntrct_Ppcn_Nbr = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Cntrct_Ppcn_Nbr", "INV-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Extract_Record_Inv_Cntrct_Payee_Cde = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Cntrct_Payee_Cde", "INV-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Extract_Record_Inv_Cntrct_Pymnt_Type_Ind = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Cntrct_Pymnt_Type_Ind", "INV-CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Inv_Cntrct_Sttlmnt_Type_Ind = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Cntrct_Sttlmnt_Type_Ind", "INV-CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Inv_Cntrct_Option_Cde = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Cntrct_Option_Cde", "INV-CNTRCT-OPTION-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Extract_Record_Inv_Cntrct_Mode_Cde = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Inv_Cntrct_Mode_Cde", "INV-CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Extract_Record_Pnd_Cntr_Deductions = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Cntr_Deductions", "#CNTR-DEDUCTIONS", 
            FieldType.NUMERIC, 3);
        pnd_Extract_Record_Pymnt_Ded_Cde = pnd_Extract_RecordRedef5.newFieldArrayInGroup("pnd_Extract_Record_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3, new DbsArrayController(1,10));
        pnd_Extract_Record_Pymnt_Ded_Payee_Cde = pnd_Extract_RecordRedef5.newFieldArrayInGroup("pnd_Extract_Record_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8, new DbsArrayController(1,10));
        pnd_Extract_Record_Pymnt_Ded_Amt = pnd_Extract_RecordRedef5.newFieldArrayInGroup("pnd_Extract_Record_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,10));
        pnd_Extract_Record_Pnd_Pyhdr_Data = pnd_Extract_RecordRedef5.newGroupInGroup("pnd_Extract_Record_Pnd_Pyhdr_Data", "#PYHDR-DATA");
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte = pnd_Extract_Record_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte", 
            "#PYHDR-PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef6 = pnd_Extract_Record_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef6", 
            "Redefines", pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4 = pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef6.newFieldInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-A4", FieldType.STRING, 4);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef7 = pnd_Extract_Record_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef7", 
            "Redefines", pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6 = pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef7.newFieldInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_P6", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-P6", FieldType.PACKED_DECIMAL, 6);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef8 = pnd_Extract_Record_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef8", 
            "Redefines", pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start = pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_DteRedef8.newFieldInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-START", FieldType.DATE);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End = pnd_Extract_Record_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-END", FieldType.DATE);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef9 = pnd_Extract_Record_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef9", 
            "Redefines", pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End);
        pnd_Extract_Record_Pnd_Pyhdr_Annt_Ctznshp_Cde = pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef9.newFieldInGroup("pnd_Extract_Record_Pnd_Pyhdr_Annt_Ctznshp_Cde", 
            "#PYHDR-ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2);
        pnd_Extract_Record_Pnd_Pyhdr_Annt_Rsdncy_Cde = pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef9.newFieldInGroup("pnd_Extract_Record_Pnd_Pyhdr_Annt_Rsdncy_Cde", 
            "#PYHDR-ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef10 = pnd_Extract_Record_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef10", 
            "Redefines", pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_P6 = pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef10.newFieldInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_P6", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-END-P6", FieldType.PACKED_DECIMAL, 6);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef11 = pnd_Extract_Record_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef11", 
            "Redefines", pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End);
        pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A4 = pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_EndRedef11.newFieldInGroup("pnd_Extract_Record_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End_A4", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-END-A4", FieldType.STRING, 4);
        pnd_Extract_Record_Pnd_Inv_Acct_Filler = pnd_Extract_RecordRedef5.newFieldInGroup("pnd_Extract_Record_Pnd_Inv_Acct_Filler", "#INV-ACCT-FILLER", 
            FieldType.STRING, 180);
        pnd_Extract_RecordRedef12 = newGroupInRecord("pnd_Extract_RecordRedef12", "Redefines", pnd_Extract_Record);
        pnd_Extract_Record_Pnd_Na_Record_Level_Nmbr = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pnd_Na_Record_Level_Nmbr", "#NA-RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Extract_Record_Pnd_Na_Record_Occur_Nmbr = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pnd_Na_Record_Occur_Nmbr", "#NA-RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Extract_Record_Pnd_Na_Simplex_Duplex_Multiplex = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pnd_Na_Simplex_Duplex_Multiplex", 
            "#NA-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Extract_Record_Pnd_Na_Save_Pymnt_Prcss_Seq_Num = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pnd_Na_Save_Pymnt_Prcss_Seq_Num", 
            "#NA-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Extract_Record_Pnd_Na_Save_Contract_Hold_Code = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pnd_Na_Save_Contract_Hold_Code", 
            "#NA-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Extract_Record_Pnd_Na_Pymnt_Check_Nbr = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pnd_Na_Pymnt_Check_Nbr", "#NA-PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Extract_Record_Pnd_Na_Pymnt_Check_Seq_Nbr = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pnd_Na_Pymnt_Check_Seq_Nbr", "#NA-PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Extract_Record_Pnd_Na_Cntrct_Cmbn_Nbr = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pnd_Na_Cntrct_Cmbn_Nbr", "#NA-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Extract_Record_Rcrd_Typ = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 1);
        pnd_Extract_Record_Na_Cntrct_Orgn_Cde = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Na_Cntrct_Orgn_Cde", "NA-CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Extract_Record_Na_Cntrct_Ppcn_Nbr = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Na_Cntrct_Ppcn_Nbr", "NA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Extract_Record_Na_Cntrct_Payee_Cde = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Na_Cntrct_Payee_Cde", "NA-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Extract_Record_Na_Cntrct_Invrse_Dte = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Na_Cntrct_Invrse_Dte", "NA-CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Extract_Record_Na_Pymnt_Prcss_Seq_Nbr = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Na_Pymnt_Prcss_Seq_Nbr", "NA-PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Extract_Record_Ph_Name = pnd_Extract_RecordRedef12.newGroupInGroup("pnd_Extract_Record_Ph_Name", "PH-NAME");
        pnd_Extract_Record_Ph_Last_Name = pnd_Extract_Record_Ph_Name.newFieldInGroup("pnd_Extract_Record_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Extract_Record_Ph_First_Name = pnd_Extract_Record_Ph_Name.newFieldInGroup("pnd_Extract_Record_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 
            10);
        pnd_Extract_Record_Ph_Middle_Name = pnd_Extract_Record_Ph_Name.newFieldInGroup("pnd_Extract_Record_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 
            12);
        pnd_Extract_Record_Nme_N_Addr = pnd_Extract_RecordRedef12.newGroupInGroup("pnd_Extract_Record_Nme_N_Addr", "NME-N-ADDR");
        pnd_Extract_Record_Pymnt_Nme = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38);
        pnd_Extract_Record_Pymnt_Addr_Line1_Txt = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35);
        pnd_Extract_Record_Pymnt_Addr_Line2_Txt = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35);
        pnd_Extract_Record_Pymnt_Addr_Line3_Txt = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", 
            FieldType.STRING, 35);
        pnd_Extract_Record_Pymnt_Addr_Line4_Txt = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", 
            FieldType.STRING, 35);
        pnd_Extract_Record_Pymnt_Addr_Line5_Txt = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", 
            FieldType.STRING, 35);
        pnd_Extract_Record_Pymnt_Addr_Line6_Txt = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", 
            FieldType.STRING, 35);
        pnd_Extract_Record_Pymnt_Addr_Zip_Cde = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", 
            FieldType.STRING, 9);
        pnd_Extract_Record_Pymnt_Postl_Data = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", 
            FieldType.STRING, 32);
        pnd_Extract_Record_Pymnt_Addr_Type_Ind = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Pymnt_Addr_Last_Chg_Dte = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Addr_Last_Chg_Dte", "PYMNT-ADDR-LAST-CHG-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Extract_Record_Pymnt_Addr_Last_Chg_Tme = pnd_Extract_Record_Nme_N_Addr.newFieldInGroup("pnd_Extract_Record_Pymnt_Addr_Last_Chg_Tme", "PYMNT-ADDR-LAST-CHG-TME", 
            FieldType.NUMERIC, 7);
        pnd_Extract_Record_Pymnt_Eft_Transit_Id = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", 
            FieldType.NUMERIC, 9);
        pnd_Extract_Record_Pymnt_Eft_Acct_Nbr = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", 
            FieldType.STRING, 21);
        pnd_Extract_Record_Pymnt_Chk_Sav_Ind = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", 
            FieldType.STRING, 1);
        pnd_Extract_Record_Pymnt_Deceased_Nme = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pymnt_Deceased_Nme", "PYMNT-DECEASED-NME", 
            FieldType.STRING, 38);
        pnd_Extract_Record_Na_Cntrct_Hold_Tme = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Na_Cntrct_Hold_Tme", "NA-CNTRCT-HOLD-TME", 
            FieldType.TIME);
        pnd_Extract_Record_Pnd_Ws_Name_N_Address_Filler = pnd_Extract_RecordRedef12.newFieldInGroup("pnd_Extract_Record_Pnd_Ws_Name_N_Address_Filler", 
            "#WS-NAME-N-ADDRESS-FILLER", FieldType.STRING, 8);

        pnd_Date_Control_Info = newGroupInRecord("pnd_Date_Control_Info", "#DATE-CONTROL-INFO");
        pnd_Date_Control_Info_Pnd_Date_Info = pnd_Date_Control_Info.newGroupInGroup("pnd_Date_Control_Info_Pnd_Date_Info", "#DATE-INFO");
        pnd_Date_Control_Info_Pnd_Payment_Date = pnd_Date_Control_Info_Pnd_Date_Info.newFieldInGroup("pnd_Date_Control_Info_Pnd_Payment_Date", "#PAYMENT-DATE", 
            FieldType.STRING, 8);
        pnd_Date_Control_Info_Pnd_Payment_DateRedef13 = pnd_Date_Control_Info_Pnd_Date_Info.newGroupInGroup("pnd_Date_Control_Info_Pnd_Payment_DateRedef13", 
            "Redefines", pnd_Date_Control_Info_Pnd_Payment_Date);
        pnd_Date_Control_Info_Pnd_Payment_Date_1_2 = pnd_Date_Control_Info_Pnd_Payment_DateRedef13.newFieldInGroup("pnd_Date_Control_Info_Pnd_Payment_Date_1_2", 
            "#PAYMENT-DATE-1-2", FieldType.STRING, 2);
        pnd_Date_Control_Info_Pnd_Payment_Date_3_8 = pnd_Date_Control_Info_Pnd_Payment_DateRedef13.newFieldInGroup("pnd_Date_Control_Info_Pnd_Payment_Date_3_8", 
            "#PAYMENT-DATE-3-8", FieldType.STRING, 6);
        pnd_Date_Control_Info_Pnd_Future_Payment_Date = pnd_Date_Control_Info_Pnd_Date_Info.newFieldInGroup("pnd_Date_Control_Info_Pnd_Future_Payment_Date", 
            "#FUTURE-PAYMENT-DATE", FieldType.NUMERIC, 8);
        pnd_Date_Control_Info_Pnd_Future_Payment_DateRedef14 = pnd_Date_Control_Info_Pnd_Date_Info.newGroupInGroup("pnd_Date_Control_Info_Pnd_Future_Payment_DateRedef14", 
            "Redefines", pnd_Date_Control_Info_Pnd_Future_Payment_Date);
        pnd_Date_Control_Info_Pnd_Future_Payment_Date_1_2 = pnd_Date_Control_Info_Pnd_Future_Payment_DateRedef14.newFieldInGroup("pnd_Date_Control_Info_Pnd_Future_Payment_Date_1_2", 
            "#FUTURE-PAYMENT-DATE-1-2", FieldType.NUMERIC, 2);
        pnd_Date_Control_Info_Pnd_Future_Payment_Date_3_8 = pnd_Date_Control_Info_Pnd_Future_Payment_DateRedef14.newFieldInGroup("pnd_Date_Control_Info_Pnd_Future_Payment_Date_3_8", 
            "#FUTURE-PAYMENT-DATE-3-8", FieldType.NUMERIC, 6);
        pnd_Date_Control_Info_Pnd_Accounting_Date = pnd_Date_Control_Info_Pnd_Date_Info.newFieldInGroup("pnd_Date_Control_Info_Pnd_Accounting_Date", "#ACCOUNTING-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Date_Control_Info_Pnd_Accounting_DateRedef15 = pnd_Date_Control_Info_Pnd_Date_Info.newGroupInGroup("pnd_Date_Control_Info_Pnd_Accounting_DateRedef15", 
            "Redefines", pnd_Date_Control_Info_Pnd_Accounting_Date);
        pnd_Date_Control_Info_Pnd_Accounting_Date_1_2 = pnd_Date_Control_Info_Pnd_Accounting_DateRedef15.newFieldInGroup("pnd_Date_Control_Info_Pnd_Accounting_Date_1_2", 
            "#ACCOUNTING-DATE-1-2", FieldType.NUMERIC, 2);
        pnd_Date_Control_Info_Pnd_Accounting_Date_3_8 = pnd_Date_Control_Info_Pnd_Accounting_DateRedef15.newFieldInGroup("pnd_Date_Control_Info_Pnd_Accounting_Date_3_8", 
            "#ACCOUNTING-DATE-3-8", FieldType.NUMERIC, 6);
        pnd_Date_Control_Info_Pnd_Future_Accounting_Date = pnd_Date_Control_Info_Pnd_Date_Info.newFieldInGroup("pnd_Date_Control_Info_Pnd_Future_Accounting_Date", 
            "#FUTURE-ACCOUNTING-DATE", FieldType.NUMERIC, 8);
        pnd_Date_Control_Info_Pnd_Future_Accounting_DateRedef16 = pnd_Date_Control_Info_Pnd_Date_Info.newGroupInGroup("pnd_Date_Control_Info_Pnd_Future_Accounting_DateRedef16", 
            "Redefines", pnd_Date_Control_Info_Pnd_Future_Accounting_Date);
        pnd_Date_Control_Info_Pnd_Future_Accounting_Date_1_2 = pnd_Date_Control_Info_Pnd_Future_Accounting_DateRedef16.newFieldInGroup("pnd_Date_Control_Info_Pnd_Future_Accounting_Date_1_2", 
            "#FUTURE-ACCOUNTING-DATE-1-2", FieldType.NUMERIC, 2);
        pnd_Date_Control_Info_Pnd_Future_Accounting_Date_3_8 = pnd_Date_Control_Info_Pnd_Future_Accounting_DateRedef16.newFieldInGroup("pnd_Date_Control_Info_Pnd_Future_Accounting_Date_3_8", 
            "#FUTURE-ACCOUNTING-DATE-3-8", FieldType.NUMERIC, 6);
        pnd_Date_Control_Info_Pnd_Control_Info = pnd_Date_Control_Info.newGroupArrayInGroup("pnd_Date_Control_Info_Pnd_Control_Info", "#CONTROL-INFO", 
            new DbsArrayController(1,30));
        pnd_Date_Control_Info_Pnd_Cntl_Cde = pnd_Date_Control_Info_Pnd_Control_Info.newFieldInGroup("pnd_Date_Control_Info_Pnd_Cntl_Cde", "#CNTL-CDE", 
            FieldType.STRING, 2);
        pnd_Date_Control_Info_Pnd_Cntl_Can_Rdw_Ind = pnd_Date_Control_Info_Pnd_Control_Info.newFieldInGroup("pnd_Date_Control_Info_Pnd_Cntl_Can_Rdw_Ind", 
            "#CNTL-CAN-RDW-IND", FieldType.STRING, 1);
        pnd_Date_Control_Info_Pnd_Cntl_Cnt = pnd_Date_Control_Info_Pnd_Control_Info.newFieldInGroup("pnd_Date_Control_Info_Pnd_Cntl_Cnt", "#CNTL-CNT", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Date_Control_Info_Pnd_Cntl_Gross_Amt = pnd_Date_Control_Info_Pnd_Control_Info.newFieldInGroup("pnd_Date_Control_Info_Pnd_Cntl_Gross_Amt", 
            "#CNTL-GROSS-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Date_Control_Info_Pnd_Cntl_Net_Amt = pnd_Date_Control_Info_Pnd_Control_Info.newFieldInGroup("pnd_Date_Control_Info_Pnd_Cntl_Net_Amt", "#CNTL-NET-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_Ws_Date_Alpha = newFieldInRecord("pnd_Ws_Date_Alpha", "#WS-DATE-ALPHA", FieldType.STRING, 8);
        pnd_Ws_Date_AlphaRedef17 = newGroupInRecord("pnd_Ws_Date_AlphaRedef17", "Redefines", pnd_Ws_Date_Alpha);
        pnd_Ws_Date_Alpha_Pnd_Ws_Date = pnd_Ws_Date_AlphaRedef17.newFieldInGroup("pnd_Ws_Date_Alpha_Pnd_Ws_Date", "#WS-DATE", FieldType.NUMERIC, 8);
        pnd_Ws_Date_Alpha_Pnd_Ws_DateRedef18 = pnd_Ws_Date_AlphaRedef17.newGroupInGroup("pnd_Ws_Date_Alpha_Pnd_Ws_DateRedef18", "Redefines", pnd_Ws_Date_Alpha_Pnd_Ws_Date);
        pnd_Ws_Date_Alpha_Pnd_Ws_Date_Cc = pnd_Ws_Date_Alpha_Pnd_Ws_DateRedef18.newFieldInGroup("pnd_Ws_Date_Alpha_Pnd_Ws_Date_Cc", "#WS-DATE-CC", FieldType.NUMERIC, 
            2);
        pnd_Ws_Date_Alpha_Pnd_Ws_Date_Yy = pnd_Ws_Date_Alpha_Pnd_Ws_DateRedef18.newFieldInGroup("pnd_Ws_Date_Alpha_Pnd_Ws_Date_Yy", "#WS-DATE-YY", FieldType.NUMERIC, 
            2);
        pnd_Ws_Date_Alpha_Pnd_Ws_Date_Mm = pnd_Ws_Date_Alpha_Pnd_Ws_DateRedef18.newFieldInGroup("pnd_Ws_Date_Alpha_Pnd_Ws_Date_Mm", "#WS-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Ws_Date_Alpha_Pnd_Ws_Date_Dd = pnd_Ws_Date_Alpha_Pnd_Ws_DateRedef18.newFieldInGroup("pnd_Ws_Date_Alpha_Pnd_Ws_Date_Dd", "#WS-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Cntl_Super_Des = newFieldInRecord("pnd_Cntl_Super_Des", "#CNTL-SUPER-DES", FieldType.STRING, 10);
        pnd_Cntl_Super_DesRedef19 = newGroupInRecord("pnd_Cntl_Super_DesRedef19", "Redefines", pnd_Cntl_Super_Des);
        pnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde = pnd_Cntl_Super_DesRedef19.newFieldInGroup("pnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde", "#CNTL-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Cntl_Super_Des_Pnd_Cntl_Invrse_Dte = pnd_Cntl_Super_DesRedef19.newFieldInGroup("pnd_Cntl_Super_Des_Pnd_Cntl_Invrse_Dte", "#CNTL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);

        this.setRecordName("LdaFcpl190");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Date_Control_Info_Pnd_Payment_Date.setInitialValue(" ");
        pnd_Date_Control_Info_Pnd_Future_Payment_Date.setInitialValue(0);
        pnd_Date_Control_Info_Pnd_Accounting_Date.setInitialValue(0);
        pnd_Date_Control_Info_Pnd_Future_Accounting_Date.setInitialValue(0);
        pnd_Date_Control_Info_Pnd_Cntl_Cde.setInitialValue(" ");
        pnd_Date_Control_Info_Pnd_Cntl_Can_Rdw_Ind.setInitialValue(" ");
        pnd_Date_Control_Info_Pnd_Cntl_Cnt.setInitialValue(0);
        pnd_Date_Control_Info_Pnd_Cntl_Gross_Amt.setInitialValue(0);
        pnd_Date_Control_Info_Pnd_Cntl_Net_Amt.setInitialValue(0);
        pnd_Cntl_Super_Des.setInitialValue(" ");
    }

    // Constructor
    public LdaFcpl190() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
