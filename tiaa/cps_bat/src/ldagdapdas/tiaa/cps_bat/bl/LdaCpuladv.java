/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:41 PM
**        * FROM NATURAL LDA     : CPULADV
************************************************************
**        * FILE NAME            : LdaCpuladv.java
**        * CLASS NAME           : LdaCpuladv
**        * INSTANCE NAME        : LdaCpuladv
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpuladv extends DbsRecord
{
    // Properties
    private DbsField advisor_Record;
    private DbsGroup advisor_RecordRedef1;
    private DbsField advisor_Record_Advisor_Payment_Amt;
    private DbsField advisor_Record_Advisor_Plan;
    private DbsField advisor_Record_Advisor_Invoice;
    private DbsField advisor_Record_Advisor_Bill_From_Date;
    private DbsField advisor_Record_Advisor_Bill_To_Date;
    private DbsField advisor_Record_Legacy_Eft_Key;
    private DbsGroup advisor_Record_Legacy_Eft_KeyRedef2;
    private DbsField advisor_Record_Key_Contract;
    private DbsField advisor_Record_Key_Payee;
    private DbsField advisor_Record_Key_Origin;
    private DbsField advisor_Record_Key_Inverse_Date;
    private DbsField advisor_Record_Key_Process_Seq;
    private DbsGroup advisor_Record_Key_Process_SeqRedef3;
    private DbsField advisor_Record_Pnd_Seq_A;
    private DbsField filler01;
    private DbsField advisor_Record_Check_Number;
    private DbsGroup advisor_Record_Check_NumberRedef4;
    private DbsField advisor_Record_Check_Alpha;

    public DbsField getAdvisor_Record() { return advisor_Record; }

    public DbsGroup getAdvisor_RecordRedef1() { return advisor_RecordRedef1; }

    public DbsField getAdvisor_Record_Advisor_Payment_Amt() { return advisor_Record_Advisor_Payment_Amt; }

    public DbsField getAdvisor_Record_Advisor_Plan() { return advisor_Record_Advisor_Plan; }

    public DbsField getAdvisor_Record_Advisor_Invoice() { return advisor_Record_Advisor_Invoice; }

    public DbsField getAdvisor_Record_Advisor_Bill_From_Date() { return advisor_Record_Advisor_Bill_From_Date; }

    public DbsField getAdvisor_Record_Advisor_Bill_To_Date() { return advisor_Record_Advisor_Bill_To_Date; }

    public DbsField getAdvisor_Record_Legacy_Eft_Key() { return advisor_Record_Legacy_Eft_Key; }

    public DbsGroup getAdvisor_Record_Legacy_Eft_KeyRedef2() { return advisor_Record_Legacy_Eft_KeyRedef2; }

    public DbsField getAdvisor_Record_Key_Contract() { return advisor_Record_Key_Contract; }

    public DbsField getAdvisor_Record_Key_Payee() { return advisor_Record_Key_Payee; }

    public DbsField getAdvisor_Record_Key_Origin() { return advisor_Record_Key_Origin; }

    public DbsField getAdvisor_Record_Key_Inverse_Date() { return advisor_Record_Key_Inverse_Date; }

    public DbsField getAdvisor_Record_Key_Process_Seq() { return advisor_Record_Key_Process_Seq; }

    public DbsGroup getAdvisor_Record_Key_Process_SeqRedef3() { return advisor_Record_Key_Process_SeqRedef3; }

    public DbsField getAdvisor_Record_Pnd_Seq_A() { return advisor_Record_Pnd_Seq_A; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getAdvisor_Record_Check_Number() { return advisor_Record_Check_Number; }

    public DbsGroup getAdvisor_Record_Check_NumberRedef4() { return advisor_Record_Check_NumberRedef4; }

    public DbsField getAdvisor_Record_Check_Alpha() { return advisor_Record_Check_Alpha; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        advisor_Record = newFieldInRecord("advisor_Record", "ADVISOR-RECORD", FieldType.STRING, 200);
        advisor_RecordRedef1 = newGroupInRecord("advisor_RecordRedef1", "Redefines", advisor_Record);
        advisor_Record_Advisor_Payment_Amt = advisor_RecordRedef1.newFieldInGroup("advisor_Record_Advisor_Payment_Amt", "ADVISOR-PAYMENT-AMT", FieldType.DECIMAL, 
            11,2);
        advisor_Record_Advisor_Plan = advisor_RecordRedef1.newFieldInGroup("advisor_Record_Advisor_Plan", "ADVISOR-PLAN", FieldType.STRING, 6);
        advisor_Record_Advisor_Invoice = advisor_RecordRedef1.newFieldInGroup("advisor_Record_Advisor_Invoice", "ADVISOR-INVOICE", FieldType.STRING, 15);
        advisor_Record_Advisor_Bill_From_Date = advisor_RecordRedef1.newFieldInGroup("advisor_Record_Advisor_Bill_From_Date", "ADVISOR-BILL-FROM-DATE", 
            FieldType.STRING, 8);
        advisor_Record_Advisor_Bill_To_Date = advisor_RecordRedef1.newFieldInGroup("advisor_Record_Advisor_Bill_To_Date", "ADVISOR-BILL-TO-DATE", FieldType.STRING, 
            8);
        advisor_Record_Legacy_Eft_Key = advisor_RecordRedef1.newFieldInGroup("advisor_Record_Legacy_Eft_Key", "LEGACY-EFT-KEY", FieldType.STRING, 33);
        advisor_Record_Legacy_Eft_KeyRedef2 = advisor_RecordRedef1.newGroupInGroup("advisor_Record_Legacy_Eft_KeyRedef2", "Redefines", advisor_Record_Legacy_Eft_Key);
        advisor_Record_Key_Contract = advisor_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("advisor_Record_Key_Contract", "KEY-CONTRACT", FieldType.STRING, 
            10);
        advisor_Record_Key_Payee = advisor_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("advisor_Record_Key_Payee", "KEY-PAYEE", FieldType.STRING, 4);
        advisor_Record_Key_Origin = advisor_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("advisor_Record_Key_Origin", "KEY-ORIGIN", FieldType.STRING, 2);
        advisor_Record_Key_Inverse_Date = advisor_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("advisor_Record_Key_Inverse_Date", "KEY-INVERSE-DATE", FieldType.NUMERIC, 
            8);
        advisor_Record_Key_Process_Seq = advisor_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("advisor_Record_Key_Process_Seq", "KEY-PROCESS-SEQ", FieldType.NUMERIC, 
            9);
        advisor_Record_Key_Process_SeqRedef3 = advisor_Record_Legacy_Eft_KeyRedef2.newGroupInGroup("advisor_Record_Key_Process_SeqRedef3", "Redefines", 
            advisor_Record_Key_Process_Seq);
        advisor_Record_Pnd_Seq_A = advisor_Record_Key_Process_SeqRedef3.newFieldInGroup("advisor_Record_Pnd_Seq_A", "#SEQ-A", FieldType.STRING, 7);
        filler01 = advisor_RecordRedef1.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 109);
        advisor_Record_Check_Number = advisor_RecordRedef1.newFieldInGroup("advisor_Record_Check_Number", "CHECK-NUMBER", FieldType.NUMERIC, 10);
        advisor_Record_Check_NumberRedef4 = advisor_RecordRedef1.newGroupInGroup("advisor_Record_Check_NumberRedef4", "Redefines", advisor_Record_Check_Number);
        advisor_Record_Check_Alpha = advisor_Record_Check_NumberRedef4.newFieldInGroup("advisor_Record_Check_Alpha", "CHECK-ALPHA", FieldType.STRING, 
            10);

        this.setRecordName("LdaCpuladv");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCpuladv() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
