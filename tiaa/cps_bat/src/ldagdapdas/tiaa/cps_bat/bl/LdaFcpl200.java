/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:04 PM
**        * FROM NATURAL LDA     : FCPL200
************************************************************
**        * FILE NAME            : LdaFcpl200.java
**        * CLASS NAME           : LdaFcpl200
**        * INSTANCE NAME        : LdaFcpl200
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl200 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_fcp_Cons_Cntrl_View;
    private DbsField fcp_Cons_Cntrl_View_Cntrct_Rcrd_Typ;
    private DbsField fcp_Cons_Cntrl_View_Cntrct_Act_Ind;
    private DbsGroup fcp_Cons_Cntrl_View_Cntrl_Data_Grp;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Orgn_Cde;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Cycle_Dte;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Check_Dte;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Invrse_Dte;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Seq_Start_Cnt;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Seq_End_Cnt;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Month_End_Ind;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Month_End_Dte;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Bank_Acct_Nbr;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Rmrk_Line1_Txt;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Rmrk_Line2_Txt;
    private DbsGroup fcp_Cons_Cntrl_View_Count_Castcntl_Amt_Pe_Grp;
    private DbsGroup fcp_Cons_Cntrl_View_Cntl_Amt_Pe_Grp;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Type_Cde;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Cnt;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Gross_Amt;
    private DbsField fcp_Cons_Cntrl_View_Cntl_Net_Amt;
    private DbsField fcp_Cons_Cntrl_View_Cntrct_Hold_Tme;

    public DataAccessProgramView getVw_fcp_Cons_Cntrl_View() { return vw_fcp_Cons_Cntrl_View; }

    public DbsField getFcp_Cons_Cntrl_View_Cntrct_Rcrd_Typ() { return fcp_Cons_Cntrl_View_Cntrct_Rcrd_Typ; }

    public DbsField getFcp_Cons_Cntrl_View_Cntrct_Act_Ind() { return fcp_Cons_Cntrl_View_Cntrct_Act_Ind; }

    public DbsGroup getFcp_Cons_Cntrl_View_Cntrl_Data_Grp() { return fcp_Cons_Cntrl_View_Cntrl_Data_Grp; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Orgn_Cde() { return fcp_Cons_Cntrl_View_Cntl_Orgn_Cde; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Cycle_Dte() { return fcp_Cons_Cntrl_View_Cntl_Cycle_Dte; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Check_Dte() { return fcp_Cons_Cntrl_View_Cntl_Check_Dte; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Invrse_Dte() { return fcp_Cons_Cntrl_View_Cntl_Invrse_Dte; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Seq_Start_Cnt() { return fcp_Cons_Cntrl_View_Cntl_Seq_Start_Cnt; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Seq_End_Cnt() { return fcp_Cons_Cntrl_View_Cntl_Seq_End_Cnt; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Month_End_Ind() { return fcp_Cons_Cntrl_View_Cntl_Month_End_Ind; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Month_End_Dte() { return fcp_Cons_Cntrl_View_Cntl_Month_End_Dte; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Bank_Acct_Nbr() { return fcp_Cons_Cntrl_View_Cntl_Bank_Acct_Nbr; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Rmrk_Line1_Txt() { return fcp_Cons_Cntrl_View_Cntl_Rmrk_Line1_Txt; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Rmrk_Line2_Txt() { return fcp_Cons_Cntrl_View_Cntl_Rmrk_Line2_Txt; }

    public DbsGroup getFcp_Cons_Cntrl_View_Count_Castcntl_Amt_Pe_Grp() { return fcp_Cons_Cntrl_View_Count_Castcntl_Amt_Pe_Grp; }

    public DbsGroup getFcp_Cons_Cntrl_View_Cntl_Amt_Pe_Grp() { return fcp_Cons_Cntrl_View_Cntl_Amt_Pe_Grp; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Type_Cde() { return fcp_Cons_Cntrl_View_Cntl_Type_Cde; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Cnt() { return fcp_Cons_Cntrl_View_Cntl_Cnt; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Gross_Amt() { return fcp_Cons_Cntrl_View_Cntl_Gross_Amt; }

    public DbsField getFcp_Cons_Cntrl_View_Cntl_Net_Amt() { return fcp_Cons_Cntrl_View_Cntl_Net_Amt; }

    public DbsField getFcp_Cons_Cntrl_View_Cntrct_Hold_Tme() { return fcp_Cons_Cntrl_View_Cntrct_Hold_Tme; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_fcp_Cons_Cntrl_View = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Cntrl_View", "FCP-CONS-CNTRL-VIEW"), "FCP_CONS_CNTRL", "FCP_EFT_GLBL", 
            DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_CNTRL"));
        fcp_Cons_Cntrl_View_Cntrct_Rcrd_Typ = vw_fcp_Cons_Cntrl_View.getRecord().newFieldInGroup("fcp_Cons_Cntrl_View_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_RCRD_TYP");
        fcp_Cons_Cntrl_View_Cntrct_Act_Ind = vw_fcp_Cons_Cntrl_View.getRecord().newFieldInGroup("fcp_Cons_Cntrl_View_Cntrct_Act_Ind", "CNTRCT-ACT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_ACT_IND");
        fcp_Cons_Cntrl_View_Cntrl_Data_Grp = vw_fcp_Cons_Cntrl_View.getRecord().newGroupInGroup("fcp_Cons_Cntrl_View_Cntrl_Data_Grp", "CNTRL-DATA-GRP");
        fcp_Cons_Cntrl_View_Cntl_Orgn_Cde = fcp_Cons_Cntrl_View_Cntrl_Data_Grp.newFieldInGroup("fcp_Cons_Cntrl_View_Cntl_Orgn_Cde", "CNTL-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTL_ORGN_CDE");
        fcp_Cons_Cntrl_View_Cntl_Cycle_Dte = fcp_Cons_Cntrl_View_Cntrl_Data_Grp.newFieldInGroup("fcp_Cons_Cntrl_View_Cntl_Cycle_Dte", "CNTL-CYCLE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTL_CYCLE_DTE");
        fcp_Cons_Cntrl_View_Cntl_Check_Dte = fcp_Cons_Cntrl_View_Cntrl_Data_Grp.newFieldInGroup("fcp_Cons_Cntrl_View_Cntl_Check_Dte", "CNTL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTL_CHECK_DTE");
        fcp_Cons_Cntrl_View_Cntl_Invrse_Dte = fcp_Cons_Cntrl_View_Cntrl_Data_Grp.newFieldInGroup("fcp_Cons_Cntrl_View_Cntl_Invrse_Dte", "CNTL-INVRSE-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTL_INVRSE_DTE");
        fcp_Cons_Cntrl_View_Cntl_Seq_Start_Cnt = fcp_Cons_Cntrl_View_Cntrl_Data_Grp.newFieldInGroup("fcp_Cons_Cntrl_View_Cntl_Seq_Start_Cnt", "CNTL-SEQ-START-CNT", 
            FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, "CNTL_SEQ_START_CNT");
        fcp_Cons_Cntrl_View_Cntl_Seq_End_Cnt = fcp_Cons_Cntrl_View_Cntrl_Data_Grp.newFieldInGroup("fcp_Cons_Cntrl_View_Cntl_Seq_End_Cnt", "CNTL-SEQ-END-CNT", 
            FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, "CNTL_SEQ_END_CNT");
        fcp_Cons_Cntrl_View_Cntl_Month_End_Ind = fcp_Cons_Cntrl_View_Cntrl_Data_Grp.newFieldInGroup("fcp_Cons_Cntrl_View_Cntl_Month_End_Ind", "CNTL-MONTH-END-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTL_MONTH_END_IND");
        fcp_Cons_Cntrl_View_Cntl_Month_End_Dte = fcp_Cons_Cntrl_View_Cntrl_Data_Grp.newFieldInGroup("fcp_Cons_Cntrl_View_Cntl_Month_End_Dte", "CNTL-MONTH-END-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTL_MONTH_END_DTE");
        fcp_Cons_Cntrl_View_Cntl_Bank_Acct_Nbr = fcp_Cons_Cntrl_View_Cntrl_Data_Grp.newFieldInGroup("fcp_Cons_Cntrl_View_Cntl_Bank_Acct_Nbr", "CNTL-BANK-ACCT-NBR", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "CNTL_BANK_ACCT_NBR");
        fcp_Cons_Cntrl_View_Cntl_Rmrk_Line1_Txt = fcp_Cons_Cntrl_View_Cntrl_Data_Grp.newFieldInGroup("fcp_Cons_Cntrl_View_Cntl_Rmrk_Line1_Txt", "CNTL-RMRK-LINE1-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "CNTL_RMRK_LINE1_TXT");
        fcp_Cons_Cntrl_View_Cntl_Rmrk_Line2_Txt = fcp_Cons_Cntrl_View_Cntrl_Data_Grp.newFieldInGroup("fcp_Cons_Cntrl_View_Cntl_Rmrk_Line2_Txt", "CNTL-RMRK-LINE2-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "CNTL_RMRK_LINE2_TXT");
        fcp_Cons_Cntrl_View_Count_Castcntl_Amt_Pe_Grp = vw_fcp_Cons_Cntrl_View.getRecord().newGroupInGroup("fcp_Cons_Cntrl_View_Count_Castcntl_Amt_Pe_Grp", 
            "C*CNTL-AMT-PE-GRP", RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_CNTL_AMT_PE_GRP");
        fcp_Cons_Cntrl_View_Cntl_Amt_Pe_Grp = vw_fcp_Cons_Cntrl_View.getRecord().newGroupInGroup("fcp_Cons_Cntrl_View_Cntl_Amt_Pe_Grp", "CNTL-AMT-PE-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "FCP_EFT_GLBL_CNTL_AMT_PE_GRP");
        fcp_Cons_Cntrl_View_Cntl_Type_Cde = fcp_Cons_Cntrl_View_Cntl_Amt_Pe_Grp.newFieldArrayInGroup("fcp_Cons_Cntrl_View_Cntl_Type_Cde", "CNTL-TYPE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,50) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTL_TYPE_CDE", "FCP_EFT_GLBL_CNTL_AMT_PE_GRP");
        fcp_Cons_Cntrl_View_Cntl_Cnt = fcp_Cons_Cntrl_View_Cntl_Amt_Pe_Grp.newFieldArrayInGroup("fcp_Cons_Cntrl_View_Cntl_Cnt", "CNTL-CNT", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1,50) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTL_CNT", "FCP_EFT_GLBL_CNTL_AMT_PE_GRP");
        fcp_Cons_Cntrl_View_Cntl_Gross_Amt = fcp_Cons_Cntrl_View_Cntl_Amt_Pe_Grp.newFieldArrayInGroup("fcp_Cons_Cntrl_View_Cntl_Gross_Amt", "CNTL-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,50) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTL_GROSS_AMT", "FCP_EFT_GLBL_CNTL_AMT_PE_GRP");
        fcp_Cons_Cntrl_View_Cntl_Net_Amt = fcp_Cons_Cntrl_View_Cntl_Amt_Pe_Grp.newFieldArrayInGroup("fcp_Cons_Cntrl_View_Cntl_Net_Amt", "CNTL-NET-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,50) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTL_NET_AMT", "FCP_EFT_GLBL_CNTL_AMT_PE_GRP");
        fcp_Cons_Cntrl_View_Cntrct_Hold_Tme = vw_fcp_Cons_Cntrl_View.getRecord().newFieldInGroup("fcp_Cons_Cntrl_View_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRCT_HOLD_TME");
        vw_fcp_Cons_Cntrl_View.setUniquePeList();

        this.setRecordName("LdaFcpl200");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_fcp_Cons_Cntrl_View.reset();
    }

    // Constructor
    public LdaFcpl200() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
