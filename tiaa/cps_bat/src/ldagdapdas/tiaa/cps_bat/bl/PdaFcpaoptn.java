/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:32 PM
**        * FROM NATURAL PDA     : FCPAOPTN
************************************************************
**        * FILE NAME            : PdaFcpaoptn.java
**        * CLASS NAME           : PdaFcpaoptn
**        * INSTANCE NAME        : PdaFcpaoptn
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpaoptn extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpaoptn;
    private DbsField pnd_Fcpaoptn_Pnd_Predict_Table;
    private DbsField pnd_Fcpaoptn_Pnd_Return_Cde;
    private DbsField pnd_Fcpaoptn_Cntrct_Option_Cde;
    private DbsGroup pnd_Fcpaoptn_Cntrct_Option_CdeRedef1;
    private DbsField pnd_Fcpaoptn_Cntrct_Option_Cde_X;
    private DbsField pnd_Fcpaoptn_Pnd_Optn_Desc;

    public DbsGroup getPnd_Fcpaoptn() { return pnd_Fcpaoptn; }

    public DbsField getPnd_Fcpaoptn_Pnd_Predict_Table() { return pnd_Fcpaoptn_Pnd_Predict_Table; }

    public DbsField getPnd_Fcpaoptn_Pnd_Return_Cde() { return pnd_Fcpaoptn_Pnd_Return_Cde; }

    public DbsField getPnd_Fcpaoptn_Cntrct_Option_Cde() { return pnd_Fcpaoptn_Cntrct_Option_Cde; }

    public DbsGroup getPnd_Fcpaoptn_Cntrct_Option_CdeRedef1() { return pnd_Fcpaoptn_Cntrct_Option_CdeRedef1; }

    public DbsField getPnd_Fcpaoptn_Cntrct_Option_Cde_X() { return pnd_Fcpaoptn_Cntrct_Option_Cde_X; }

    public DbsField getPnd_Fcpaoptn_Pnd_Optn_Desc() { return pnd_Fcpaoptn_Pnd_Optn_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpaoptn = dbsRecord.newGroupInRecord("pnd_Fcpaoptn", "#FCPAOPTN");
        pnd_Fcpaoptn.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpaoptn_Pnd_Predict_Table = pnd_Fcpaoptn.newFieldInGroup("pnd_Fcpaoptn_Pnd_Predict_Table", "#PREDICT-TABLE", FieldType.BOOLEAN);
        pnd_Fcpaoptn_Pnd_Return_Cde = pnd_Fcpaoptn.newFieldInGroup("pnd_Fcpaoptn_Pnd_Return_Cde", "#RETURN-CDE", FieldType.BOOLEAN);
        pnd_Fcpaoptn_Cntrct_Option_Cde = pnd_Fcpaoptn.newFieldInGroup("pnd_Fcpaoptn_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        pnd_Fcpaoptn_Cntrct_Option_CdeRedef1 = pnd_Fcpaoptn.newGroupInGroup("pnd_Fcpaoptn_Cntrct_Option_CdeRedef1", "Redefines", pnd_Fcpaoptn_Cntrct_Option_Cde);
        pnd_Fcpaoptn_Cntrct_Option_Cde_X = pnd_Fcpaoptn_Cntrct_Option_CdeRedef1.newFieldInGroup("pnd_Fcpaoptn_Cntrct_Option_Cde_X", "CNTRCT-OPTION-CDE-X", 
            FieldType.STRING, 2);
        pnd_Fcpaoptn_Pnd_Optn_Desc = pnd_Fcpaoptn.newFieldInGroup("pnd_Fcpaoptn_Pnd_Optn_Desc", "#OPTN-DESC", FieldType.STRING, 70);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpaoptn(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

