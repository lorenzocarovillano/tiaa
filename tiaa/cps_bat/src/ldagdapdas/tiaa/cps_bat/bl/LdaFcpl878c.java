/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:18 PM
**        * FROM NATURAL LDA     : FCPL878C
************************************************************
**        * FILE NAME            : LdaFcpl878c.java
**        * CLASS NAME           : LdaFcpl878c
**        * INSTANCE NAME        : LdaFcpl878c
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl878c extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl878c;
    private DbsField pnd_Fcpl878c_Pnd_Letter_Text;

    public DbsGroup getPnd_Fcpl878c() { return pnd_Fcpl878c; }

    public DbsField getPnd_Fcpl878c_Pnd_Letter_Text() { return pnd_Fcpl878c_Pnd_Letter_Text; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl878c = newGroupInRecord("pnd_Fcpl878c", "#FCPL878C");
        pnd_Fcpl878c_Pnd_Letter_Text = pnd_Fcpl878c.newFieldArrayInGroup("pnd_Fcpl878c_Pnd_Letter_Text", "#LETTER-TEXT", FieldType.STRING, 72, new DbsArrayController(1,
            2,1,4));

        this.setRecordName("LdaFcpl878c");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl878c_Pnd_Letter_Text.getValue(1,1).setInitialValue("2This individual has requested that we mail the attached check to you.");
        pnd_Fcpl878c_Pnd_Letter_Text.getValue(1,2).setInitialValue("2It is to be credited to account no.");
        pnd_Fcpl878c_Pnd_Letter_Text.getValue(1,3).setInitialValue("2If you have any questions, please call the Telephone Counseling Center");
        pnd_Fcpl878c_Pnd_Letter_Text.getValue(1,4).setInitialValue("2toll free at 1 800 842-2776, M-F 8am - 10pm ET.");
        pnd_Fcpl878c_Pnd_Letter_Text.getValue(2,1).setInitialValue("2In accordance with your instructions, your payment has been sent to");
        pnd_Fcpl878c_Pnd_Letter_Text.getValue(2,2).setInitialValue("2this address.  A breakdown of this payment has been sent to your");
        pnd_Fcpl878c_Pnd_Letter_Text.getValue(2,3).setInitialValue("2residence address.");
    }

    // Constructor
    public LdaFcpl878c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
