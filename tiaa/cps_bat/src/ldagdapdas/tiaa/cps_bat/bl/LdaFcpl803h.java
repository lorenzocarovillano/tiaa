/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:15 PM
**        * FROM NATURAL LDA     : FCPL803H
************************************************************
**        * FILE NAME            : LdaFcpl803h.java
**        * CLASS NAME           : LdaFcpl803h
**        * INSTANCE NAME        : LdaFcpl803h
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl803h extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl803h;
    private DbsField pnd_Fcpl803h_Pnd_Header_Top;
    private DbsField pnd_Fcpl803h_Pnd_Header;

    public DbsGroup getPnd_Fcpl803h() { return pnd_Fcpl803h; }

    public DbsField getPnd_Fcpl803h_Pnd_Header_Top() { return pnd_Fcpl803h_Pnd_Header_Top; }

    public DbsField getPnd_Fcpl803h_Pnd_Header() { return pnd_Fcpl803h_Pnd_Header; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl803h = newGroupInRecord("pnd_Fcpl803h", "#FCPL803H");
        pnd_Fcpl803h_Pnd_Header_Top = pnd_Fcpl803h.newFieldInGroup("pnd_Fcpl803h_Pnd_Header_Top", "#HEADER-TOP", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl803h_Pnd_Header = pnd_Fcpl803h.newFieldInGroup("pnd_Fcpl803h_Pnd_Header", "#HEADER", FieldType.STRING, 21);

        this.setRecordName("LdaFcpl803h");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl803h_Pnd_Header_Top.setInitialValue(21);
        pnd_Fcpl803h_Pnd_Header.setInitialValue("Periodic Payment for:");
    }

    // Constructor
    public LdaFcpl803h() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
