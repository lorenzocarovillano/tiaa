/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:34 PM
**        * FROM NATURAL LDA     : FCPLMODE
************************************************************
**        * FILE NAME            : LdaFcplmode.java
**        * CLASS NAME           : LdaFcplmode
**        * INSTANCE NAME        : LdaFcplmode
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplmode extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcplmode;
    private DbsField pnd_Fcplmode_Pnd_Mode_Desc;
    private DbsField pnd_Fcplmode_Pnd_Mode_Desc_Length;
    private DbsField pnd_Fcplmode_Pnd_Stmnt_Increase;

    public DbsGroup getPnd_Fcplmode() { return pnd_Fcplmode; }

    public DbsField getPnd_Fcplmode_Pnd_Mode_Desc() { return pnd_Fcplmode_Pnd_Mode_Desc; }

    public DbsField getPnd_Fcplmode_Pnd_Mode_Desc_Length() { return pnd_Fcplmode_Pnd_Mode_Desc_Length; }

    public DbsField getPnd_Fcplmode_Pnd_Stmnt_Increase() { return pnd_Fcplmode_Pnd_Stmnt_Increase; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcplmode = newGroupInRecord("pnd_Fcplmode", "#FCPLMODE");
        pnd_Fcplmode_Pnd_Mode_Desc = pnd_Fcplmode.newFieldArrayInGroup("pnd_Fcplmode_Pnd_Mode_Desc", "#MODE-DESC", FieldType.STRING, 11, new DbsArrayController(0,
            4));
        pnd_Fcplmode_Pnd_Mode_Desc_Length = pnd_Fcplmode.newFieldArrayInGroup("pnd_Fcplmode_Pnd_Mode_Desc_Length", "#MODE-DESC-LENGTH", FieldType.PACKED_DECIMAL, 
            3, new DbsArrayController(0,4));
        pnd_Fcplmode_Pnd_Stmnt_Increase = pnd_Fcplmode.newFieldArrayInGroup("pnd_Fcplmode_Pnd_Stmnt_Increase", "#STMNT-INCREASE", FieldType.PACKED_DECIMAL, 
            3, new DbsArrayController(0,4));

        this.setRecordName("LdaFcplmode");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcplmode_Pnd_Mode_Desc.getValue(1).setInitialValue("Monthly");
        pnd_Fcplmode_Pnd_Mode_Desc.getValue(2).setInitialValue("Quarterly");
        pnd_Fcplmode_Pnd_Mode_Desc.getValue(3).setInitialValue("Semi-Annual");
        pnd_Fcplmode_Pnd_Mode_Desc.getValue(4).setInitialValue("Annual");
        pnd_Fcplmode_Pnd_Mode_Desc_Length.getValue(0).setInitialValue(0);
        pnd_Fcplmode_Pnd_Mode_Desc_Length.getValue(1).setInitialValue(7);
        pnd_Fcplmode_Pnd_Mode_Desc_Length.getValue(2).setInitialValue(9);
        pnd_Fcplmode_Pnd_Mode_Desc_Length.getValue(3).setInitialValue(11);
        pnd_Fcplmode_Pnd_Mode_Desc_Length.getValue(4).setInitialValue(6);
        pnd_Fcplmode_Pnd_Stmnt_Increase.getValue(0).setInitialValue(0);
        pnd_Fcplmode_Pnd_Stmnt_Increase.getValue(1).setInitialValue(0);
        pnd_Fcplmode_Pnd_Stmnt_Increase.getValue(2).setInitialValue(0);
        pnd_Fcplmode_Pnd_Stmnt_Increase.getValue(3).setInitialValue(3);
        pnd_Fcplmode_Pnd_Stmnt_Increase.getValue(4).setInitialValue(0);
    }

    // Constructor
    public LdaFcplmode() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
