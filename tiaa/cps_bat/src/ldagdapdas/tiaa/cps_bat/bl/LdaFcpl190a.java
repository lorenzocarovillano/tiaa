/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:03 PM
**        * FROM NATURAL LDA     : FCPL190A
************************************************************
**        * FILE NAME            : LdaFcpl190a.java
**        * CLASS NAME           : LdaFcpl190a
**        * INSTANCE NAME        : LdaFcpl190a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl190a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Rpt_Ext;
    private DbsGroup pnd_Rpt_Ext_Pnd_Pymnt_Addr;
    private DbsField pnd_Rpt_Ext_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Payee_Cde;
    private DbsField pnd_Rpt_Ext_Cntrct_Crrncy_Cde;
    private DbsField pnd_Rpt_Ext_Cntrct_Orgn_Cde;
    private DbsField pnd_Rpt_Ext_Cntrct_Qlfied_Cde;
    private DbsField pnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Rpt_Ext_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Type_Cde;
    private DbsField pnd_Rpt_Ext_Cntrct_Lob_Cde;
    private DbsField pnd_Rpt_Ext_Cntrct_Cref_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Option_Cde;
    private DbsField pnd_Rpt_Ext_Cntrct_Mode_Cde;
    private DbsField pnd_Rpt_Ext_Cntrct_Hold_Cde;
    private DbsField pnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Rpt_Ext_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Rpt_Ext_Annt_Ctznshp_Cde;
    private DbsField pnd_Rpt_Ext_Annt_Rsdncy_Cde;
    private DbsField pnd_Rpt_Ext_Pymnt_Check_Dte;
    private DbsField pnd_Rpt_Ext_Pymnt_Eft_Dte;
    private DbsField pnd_Rpt_Ext_Pymnt_Check_Nbr;
    private DbsField pnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pnd_Rpt_Ext_Pymnt_Prcss_Seq_NbrRedef1;
    private DbsField pnd_Rpt_Ext_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Rpt_Ext_Pymnt_Instmt_Nbr;
    private DbsField pnd_Rpt_Ext_Pymnt_Check_Amt;
    private DbsField pnd_Rpt_Ext_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Rpt_Ext_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Rpt_Ext_Pymnt_Ftre_Ind;
    private DbsField pnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Rpt_Ext_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Rpt_Ext_Pymnt_Ded_Cde;
    private DbsField pnd_Rpt_Ext_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Rpt_Ext_Pymnt_Ded_Amt;
    private DbsGroup pnd_Rpt_Ext_Pnd_Register_Extract_Addr;
    private DbsGroup pnd_Rpt_Ext_Ph_Name;
    private DbsField pnd_Rpt_Ext_Ph_Last_Name;
    private DbsField pnd_Rpt_Ext_Ph_First_Name;
    private DbsField pnd_Rpt_Ext_Ph_Middle_Name;
    private DbsGroup pnd_Rpt_Ext_Pymnt_Nme_And_Addr_Grp;
    private DbsField pnd_Rpt_Ext_Pymnt_Nme;
    private DbsField pnd_Rpt_Ext_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Rpt_Ext_Pymnt_Eft_Transit_Id;
    private DbsField pnd_Rpt_Ext_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Rpt_Ext_Pymnt_Chk_Sav_Ind;
    private DbsField pnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Rpt_Ext_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Rpt_Ext_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Rpt_Ext_Pymnt_Method_Cde;
    private DbsField pnd_Rpt_Ext_Annt_Name;
    private DbsField pnd_Rpt_Ext_Pnd_Cntr_Deductions;
    private DbsField pnd_Rpt_Ext_Cntrct_Hold_Grp;
    private DbsField pnd_Rpt_Ext_Cntrct_Invrse_Dte;
    private DbsField pnd_Rpt_Ext_Pymnt_Acctg_Dte;
    private DbsField pnd_Rpt_Ext_Pymnt_Intrfce_Dte;
    private DbsField pnd_Rpt_Ext_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Rpt_Ext_Annt_Soc_Sec_Ind;
    private DbsField pnd_Rpt_Ext_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Rpt_Ext_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Rpt_Ext_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Rpt_Ext_Cntrct_Insurance_Option;
    private DbsField pnd_Rpt_Ext_Cntrct_Life_Contingency;
    private DbsField pnd_Rpt_Ext_Pnd_Filler_For_Futcher;
    private DbsField pnd_Rpt_Ext_C_Inv_Acct;
    private DbsGroup pnd_Rpt_Ext_Pnd_Inv_Info;
    private DbsField pnd_Rpt_Ext_Pnd_Inv_Detail;
    private DbsGroup pnd_Rpt_Ext_Pnd_Inv_DetailRedef2;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Valuat_Period;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Cde;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Unit_Qty;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Unit_Value;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Settl_Amt;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Fed_Cde;
    private DbsField pnd_Rpt_Ext_Inv_Acct_State_Cde;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Local_Cde;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Dci_Amt;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Rpt_Ext_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Exp_Amt;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Cde;
    private DbsField pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Payee_Cde;
    private DbsField pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Amt;

    public DbsGroup getPnd_Rpt_Ext() { return pnd_Rpt_Ext; }

    public DbsGroup getPnd_Rpt_Ext_Pnd_Pymnt_Addr() { return pnd_Rpt_Ext_Pnd_Pymnt_Addr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr() { return pnd_Rpt_Ext_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Payee_Cde() { return pnd_Rpt_Ext_Cntrct_Payee_Cde; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Crrncy_Cde() { return pnd_Rpt_Ext_Cntrct_Crrncy_Cde; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Orgn_Cde() { return pnd_Rpt_Ext_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Qlfied_Cde() { return pnd_Rpt_Ext_Cntrct_Qlfied_Cde; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind() { return pnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Unq_Id_Nbr() { return pnd_Rpt_Ext_Cntrct_Unq_Id_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Type_Cde() { return pnd_Rpt_Ext_Cntrct_Type_Cde; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Lob_Cde() { return pnd_Rpt_Ext_Cntrct_Lob_Cde; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Cref_Nbr() { return pnd_Rpt_Ext_Cntrct_Cref_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Option_Cde() { return pnd_Rpt_Ext_Cntrct_Option_Cde; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Mode_Cde() { return pnd_Rpt_Ext_Cntrct_Mode_Cde; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Hold_Cde() { return pnd_Rpt_Ext_Cntrct_Hold_Cde; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr() { return pnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Da_Tiaa_2_Nbr() { return pnd_Rpt_Ext_Cntrct_Da_Tiaa_2_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Da_Tiaa_3_Nbr() { return pnd_Rpt_Ext_Cntrct_Da_Tiaa_3_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Da_Tiaa_4_Nbr() { return pnd_Rpt_Ext_Cntrct_Da_Tiaa_4_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Da_Tiaa_5_Nbr() { return pnd_Rpt_Ext_Cntrct_Da_Tiaa_5_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Da_Cref_1_Nbr() { return pnd_Rpt_Ext_Cntrct_Da_Cref_1_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Da_Cref_2_Nbr() { return pnd_Rpt_Ext_Cntrct_Da_Cref_2_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Da_Cref_3_Nbr() { return pnd_Rpt_Ext_Cntrct_Da_Cref_3_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Da_Cref_4_Nbr() { return pnd_Rpt_Ext_Cntrct_Da_Cref_4_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Da_Cref_5_Nbr() { return pnd_Rpt_Ext_Cntrct_Da_Cref_5_Nbr; }

    public DbsField getPnd_Rpt_Ext_Annt_Soc_Sec_Nbr() { return pnd_Rpt_Ext_Annt_Soc_Sec_Nbr; }

    public DbsField getPnd_Rpt_Ext_Annt_Ctznshp_Cde() { return pnd_Rpt_Ext_Annt_Ctznshp_Cde; }

    public DbsField getPnd_Rpt_Ext_Annt_Rsdncy_Cde() { return pnd_Rpt_Ext_Annt_Rsdncy_Cde; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Check_Dte() { return pnd_Rpt_Ext_Pymnt_Check_Dte; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Eft_Dte() { return pnd_Rpt_Ext_Pymnt_Eft_Dte; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Check_Nbr() { return pnd_Rpt_Ext_Pymnt_Check_Nbr; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr() { return pnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPnd_Rpt_Ext_Pymnt_Prcss_Seq_NbrRedef1() { return pnd_Rpt_Ext_Pymnt_Prcss_Seq_NbrRedef1; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Num() { return pnd_Rpt_Ext_Pymnt_Prcss_Seq_Num; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Instmt_Nbr() { return pnd_Rpt_Ext_Pymnt_Instmt_Nbr; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Check_Amt() { return pnd_Rpt_Ext_Pymnt_Check_Amt; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Settlmnt_Dte() { return pnd_Rpt_Ext_Pymnt_Settlmnt_Dte; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Check_Seq_Nbr() { return pnd_Rpt_Ext_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Ftre_Ind() { return pnd_Rpt_Ext_Pymnt_Ftre_Ind; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind() { return pnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Cmbn_Nbr() { return pnd_Rpt_Ext_Cntrct_Cmbn_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Ded_Cde() { return pnd_Rpt_Ext_Pymnt_Ded_Cde; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Ded_Payee_Cde() { return pnd_Rpt_Ext_Pymnt_Ded_Payee_Cde; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Ded_Amt() { return pnd_Rpt_Ext_Pymnt_Ded_Amt; }

    public DbsGroup getPnd_Rpt_Ext_Pnd_Register_Extract_Addr() { return pnd_Rpt_Ext_Pnd_Register_Extract_Addr; }

    public DbsGroup getPnd_Rpt_Ext_Ph_Name() { return pnd_Rpt_Ext_Ph_Name; }

    public DbsField getPnd_Rpt_Ext_Ph_Last_Name() { return pnd_Rpt_Ext_Ph_Last_Name; }

    public DbsField getPnd_Rpt_Ext_Ph_First_Name() { return pnd_Rpt_Ext_Ph_First_Name; }

    public DbsField getPnd_Rpt_Ext_Ph_Middle_Name() { return pnd_Rpt_Ext_Ph_Middle_Name; }

    public DbsGroup getPnd_Rpt_Ext_Pymnt_Nme_And_Addr_Grp() { return pnd_Rpt_Ext_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Nme() { return pnd_Rpt_Ext_Pymnt_Nme; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt() { return pnd_Rpt_Ext_Pymnt_Addr_Line1_Txt; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Eft_Transit_Id() { return pnd_Rpt_Ext_Pymnt_Eft_Transit_Id; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Eft_Acct_Nbr() { return pnd_Rpt_Ext_Pymnt_Eft_Acct_Nbr; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Chk_Sav_Ind() { return pnd_Rpt_Ext_Pymnt_Chk_Sav_Ind; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind() { return pnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Payee_Tx_Elct_Trggr() { return pnd_Rpt_Ext_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr() { return pnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Rqst_Settl_Dte() { return pnd_Rpt_Ext_Cntrct_Rqst_Settl_Dte; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Ind() { return pnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Ind; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Method_Cde() { return pnd_Rpt_Ext_Pymnt_Method_Cde; }

    public DbsField getPnd_Rpt_Ext_Annt_Name() { return pnd_Rpt_Ext_Annt_Name; }

    public DbsField getPnd_Rpt_Ext_Pnd_Cntr_Deductions() { return pnd_Rpt_Ext_Pnd_Cntr_Deductions; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Hold_Grp() { return pnd_Rpt_Ext_Cntrct_Hold_Grp; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Invrse_Dte() { return pnd_Rpt_Ext_Cntrct_Invrse_Dte; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Acctg_Dte() { return pnd_Rpt_Ext_Pymnt_Acctg_Dte; }

    public DbsField getPnd_Rpt_Ext_Pymnt_Intrfce_Dte() { return pnd_Rpt_Ext_Pymnt_Intrfce_Dte; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Roll_Dest_Cde() { return pnd_Rpt_Ext_Cntrct_Roll_Dest_Cde; }

    public DbsField getPnd_Rpt_Ext_Annt_Soc_Sec_Ind() { return pnd_Rpt_Ext_Annt_Soc_Sec_Ind; }

    public DbsField getPnd_Rpt_Ext_Cnr_Orgnl_Invrse_Dte() { return pnd_Rpt_Ext_Cnr_Orgnl_Invrse_Dte; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type() { return pnd_Rpt_Ext_Cntrct_Annty_Ins_Type; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Annty_Type_Cde() { return pnd_Rpt_Ext_Cntrct_Annty_Type_Cde; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Insurance_Option() { return pnd_Rpt_Ext_Cntrct_Insurance_Option; }

    public DbsField getPnd_Rpt_Ext_Cntrct_Life_Contingency() { return pnd_Rpt_Ext_Cntrct_Life_Contingency; }

    public DbsField getPnd_Rpt_Ext_Pnd_Filler_For_Futcher() { return pnd_Rpt_Ext_Pnd_Filler_For_Futcher; }

    public DbsField getPnd_Rpt_Ext_C_Inv_Acct() { return pnd_Rpt_Ext_C_Inv_Acct; }

    public DbsGroup getPnd_Rpt_Ext_Pnd_Inv_Info() { return pnd_Rpt_Ext_Pnd_Inv_Info; }

    public DbsField getPnd_Rpt_Ext_Pnd_Inv_Detail() { return pnd_Rpt_Ext_Pnd_Inv_Detail; }

    public DbsGroup getPnd_Rpt_Ext_Pnd_Inv_DetailRedef2() { return pnd_Rpt_Ext_Pnd_Inv_DetailRedef2; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Valuat_Period() { return pnd_Rpt_Ext_Inv_Acct_Valuat_Period; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Cde() { return pnd_Rpt_Ext_Inv_Acct_Cde; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Unit_Qty() { return pnd_Rpt_Ext_Inv_Acct_Unit_Qty; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Unit_Value() { return pnd_Rpt_Ext_Inv_Acct_Unit_Value; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Settl_Amt() { return pnd_Rpt_Ext_Inv_Acct_Settl_Amt; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Fed_Cde() { return pnd_Rpt_Ext_Inv_Acct_Fed_Cde; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_State_Cde() { return pnd_Rpt_Ext_Inv_Acct_State_Cde; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Local_Cde() { return pnd_Rpt_Ext_Inv_Acct_Local_Cde; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt() { return pnd_Rpt_Ext_Inv_Acct_Ivc_Amt; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Dci_Amt() { return pnd_Rpt_Ext_Inv_Acct_Dci_Amt; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt() { return pnd_Rpt_Ext_Inv_Acct_Dpi_Amt; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt() { return pnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt() { return pnd_Rpt_Ext_Inv_Acct_State_Tax_Amt; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt() { return pnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Exp_Amt() { return pnd_Rpt_Ext_Inv_Acct_Exp_Amt; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt() { return pnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt; }

    public DbsField getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt() { return pnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Cde() { return pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Cde; }

    public DbsField getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Payee_Cde() { return pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Payee_Cde; }

    public DbsField getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Amt() { return pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Rpt_Ext = newGroupInRecord("pnd_Rpt_Ext", "#RPT-EXT");
        pnd_Rpt_Ext_Pnd_Pymnt_Addr = pnd_Rpt_Ext.newGroupInGroup("pnd_Rpt_Ext_Pnd_Pymnt_Addr", "#PYMNT-ADDR");
        pnd_Rpt_Ext_Cntrct_Ppcn_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Rpt_Ext_Cntrct_Payee_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Rpt_Ext_Cntrct_Crrncy_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 
            2);
        pnd_Rpt_Ext_Cntrct_Orgn_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Rpt_Ext_Cntrct_Qlfied_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 
            1);
        pnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 
            1);
        pnd_Rpt_Ext_Cntrct_Unq_Id_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Rpt_Ext_Cntrct_Type_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        pnd_Rpt_Ext_Cntrct_Lob_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        pnd_Rpt_Ext_Cntrct_Cref_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10);
        pnd_Rpt_Ext_Cntrct_Option_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Rpt_Ext_Cntrct_Mode_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Rpt_Ext_Cntrct_Hold_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        pnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr", "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 
            8);
        pnd_Rpt_Ext_Cntrct_Da_Tiaa_2_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Da_Tiaa_2_Nbr", "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 
            8);
        pnd_Rpt_Ext_Cntrct_Da_Tiaa_3_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Da_Tiaa_3_Nbr", "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 
            8);
        pnd_Rpt_Ext_Cntrct_Da_Tiaa_4_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Da_Tiaa_4_Nbr", "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 
            8);
        pnd_Rpt_Ext_Cntrct_Da_Tiaa_5_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Da_Tiaa_5_Nbr", "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 
            8);
        pnd_Rpt_Ext_Cntrct_Da_Cref_1_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 
            8);
        pnd_Rpt_Ext_Cntrct_Da_Cref_2_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 
            8);
        pnd_Rpt_Ext_Cntrct_Da_Cref_3_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Da_Cref_3_Nbr", "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 
            8);
        pnd_Rpt_Ext_Cntrct_Da_Cref_4_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Da_Cref_4_Nbr", "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 
            8);
        pnd_Rpt_Ext_Cntrct_Da_Cref_5_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Da_Cref_5_Nbr", "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 
            8);
        pnd_Rpt_Ext_Annt_Soc_Sec_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Rpt_Ext_Annt_Ctznshp_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            2);
        pnd_Rpt_Ext_Annt_Rsdncy_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        pnd_Rpt_Ext_Pymnt_Check_Dte = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Rpt_Ext_Pymnt_Eft_Dte = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        pnd_Rpt_Ext_Pymnt_Check_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);
        pnd_Rpt_Ext_Pymnt_Prcss_Seq_NbrRedef1 = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newGroupInGroup("pnd_Rpt_Ext_Pymnt_Prcss_Seq_NbrRedef1", "Redefines", pnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr);
        pnd_Rpt_Ext_Pymnt_Prcss_Seq_Num = pnd_Rpt_Ext_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Rpt_Ext_Pymnt_Instmt_Nbr = pnd_Rpt_Ext_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 
            2);
        pnd_Rpt_Ext_Pymnt_Check_Amt = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Rpt_Ext_Pymnt_Settlmnt_Dte = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Rpt_Ext_Pymnt_Check_Seq_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Rpt_Ext_Pymnt_Ftre_Ind = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", FieldType.STRING, 1);
        pnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Rpt_Ext_Cntrct_Cmbn_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Rpt_Ext_Pymnt_Ded_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldArrayInGroup("pnd_Rpt_Ext_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3, 
            new DbsArrayController(1,10));
        pnd_Rpt_Ext_Pymnt_Ded_Payee_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldArrayInGroup("pnd_Rpt_Ext_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 
            8, new DbsArrayController(1,10));
        pnd_Rpt_Ext_Pymnt_Ded_Amt = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldArrayInGroup("pnd_Rpt_Ext_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,10));
        pnd_Rpt_Ext_Pnd_Register_Extract_Addr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newGroupInGroup("pnd_Rpt_Ext_Pnd_Register_Extract_Addr", "#REGISTER-EXTRACT-ADDR");
        pnd_Rpt_Ext_Ph_Name = pnd_Rpt_Ext_Pnd_Register_Extract_Addr.newGroupInGroup("pnd_Rpt_Ext_Ph_Name", "PH-NAME");
        pnd_Rpt_Ext_Ph_Last_Name = pnd_Rpt_Ext_Ph_Name.newFieldInGroup("pnd_Rpt_Ext_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        pnd_Rpt_Ext_Ph_First_Name = pnd_Rpt_Ext_Ph_Name.newFieldInGroup("pnd_Rpt_Ext_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 16);
        pnd_Rpt_Ext_Ph_Middle_Name = pnd_Rpt_Ext_Ph_Name.newFieldInGroup("pnd_Rpt_Ext_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 16);
        pnd_Rpt_Ext_Pymnt_Nme_And_Addr_Grp = pnd_Rpt_Ext_Pnd_Register_Extract_Addr.newGroupInGroup("pnd_Rpt_Ext_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP");
        pnd_Rpt_Ext_Pymnt_Nme = pnd_Rpt_Ext_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("pnd_Rpt_Ext_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38, new 
            DbsArrayController(1,2));
        pnd_Rpt_Ext_Pymnt_Addr_Line1_Txt = pnd_Rpt_Ext_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("pnd_Rpt_Ext_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1,2));
        pnd_Rpt_Ext_Pymnt_Eft_Transit_Id = pnd_Rpt_Ext_Pnd_Register_Extract_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", 
            FieldType.NUMERIC, 9);
        pnd_Rpt_Ext_Pymnt_Eft_Acct_Nbr = pnd_Rpt_Ext_Pnd_Register_Extract_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", 
            FieldType.STRING, 21);
        pnd_Rpt_Ext_Pymnt_Chk_Sav_Ind = pnd_Rpt_Ext_Pnd_Register_Extract_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 
            1);
        pnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Rpt_Ext_Pymnt_Payee_Tx_Elct_Trggr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", 
            FieldType.STRING, 1);
        pnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 
            7);
        pnd_Rpt_Ext_Cntrct_Rqst_Settl_Dte = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Rqst_Settl_Dte", "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        pnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Ind = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", 
            FieldType.STRING, 1);
        pnd_Rpt_Ext_Pymnt_Method_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Method_Cde", "PYMNT-METHOD-CDE", FieldType.STRING, 
            1);
        pnd_Rpt_Ext_Annt_Name = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Annt_Name", "ANNT-NAME", FieldType.STRING, 38);
        pnd_Rpt_Ext_Pnd_Cntr_Deductions = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pnd_Cntr_Deductions", "#CNTR-DEDUCTIONS", FieldType.NUMERIC, 
            3);
        pnd_Rpt_Ext_Cntrct_Hold_Grp = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3);
        pnd_Rpt_Ext_Cntrct_Invrse_Dte = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Rpt_Ext_Pymnt_Acctg_Dte = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Rpt_Ext_Pymnt_Intrfce_Dte = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Rpt_Ext_Cntrct_Roll_Dest_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 
            4);
        pnd_Rpt_Ext_Annt_Soc_Sec_Ind = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 
            1);
        pnd_Rpt_Ext_Cnr_Orgnl_Invrse_Dte = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Rpt_Ext_Cntrct_Annty_Ins_Type = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 
            1);
        pnd_Rpt_Ext_Cntrct_Annty_Type_Cde = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Rpt_Ext_Cntrct_Insurance_Option = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", 
            FieldType.STRING, 1);
        pnd_Rpt_Ext_Cntrct_Life_Contingency = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", 
            FieldType.STRING, 1);
        pnd_Rpt_Ext_Pnd_Filler_For_Futcher = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_Pnd_Filler_For_Futcher", "#FILLER-FOR-FUTCHER", FieldType.STRING, 
            31);
        pnd_Rpt_Ext_C_Inv_Acct = pnd_Rpt_Ext_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Rpt_Ext_C_Inv_Acct", "C-INV-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Rpt_Ext_Pnd_Inv_Info = pnd_Rpt_Ext.newGroupArrayInGroup("pnd_Rpt_Ext_Pnd_Inv_Info", "#INV-INFO", new DbsArrayController(1,40));
        pnd_Rpt_Ext_Pnd_Inv_Detail = pnd_Rpt_Ext_Pnd_Inv_Info.newFieldInGroup("pnd_Rpt_Ext_Pnd_Inv_Detail", "#INV-DETAIL", FieldType.STRING, 83);
        pnd_Rpt_Ext_Pnd_Inv_DetailRedef2 = pnd_Rpt_Ext_Pnd_Inv_Info.newGroupInGroup("pnd_Rpt_Ext_Pnd_Inv_DetailRedef2", "Redefines", pnd_Rpt_Ext_Pnd_Inv_Detail);
        pnd_Rpt_Ext_Inv_Acct_Valuat_Period = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        pnd_Rpt_Ext_Inv_Acct_Cde = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        pnd_Rpt_Ext_Inv_Acct_Unit_Qty = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9,3);
        pnd_Rpt_Ext_Inv_Acct_Unit_Value = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            9,4);
        pnd_Rpt_Ext_Inv_Acct_Settl_Amt = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Rpt_Ext_Inv_Acct_Fed_Cde = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 
            1);
        pnd_Rpt_Ext_Inv_Acct_State_Cde = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", FieldType.STRING, 
            1);
        pnd_Rpt_Ext_Inv_Acct_Local_Cde = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", FieldType.STRING, 
            1);
        pnd_Rpt_Ext_Inv_Acct_Ivc_Amt = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Rpt_Ext_Inv_Acct_Dci_Amt = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Rpt_Ext_Inv_Acct_Dpi_Amt = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rpt_Ext_Inv_Acct_State_Tax_Amt = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rpt_Ext_Inv_Acct_Exp_Amt = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Cde = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Cde", "##INV-DED-CDE", FieldType.NUMERIC, 
            3);
        pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Payee_Cde = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Payee_Cde", "##INV-DED-PAYEE-CDE", 
            FieldType.STRING, 8);
        pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Amt = pnd_Rpt_Ext_Pnd_Inv_DetailRedef2.newFieldInGroup("pnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Amt", "##INV-DED-AMT", FieldType.PACKED_DECIMAL, 
            9,2);

        this.setRecordName("LdaFcpl190a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl190a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
