/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:17 PM
**        * FROM NATURAL LDA     : FCPL830
************************************************************
**        * FILE NAME            : LdaFcpl830.java
**        * CLASS NAME           : LdaFcpl830
**        * INSTANCE NAME        : LdaFcpl830
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl830 extends DbsRecord
{
    // Properties
    private DbsField pnd_Output_Gl_Dtl;
    private DbsGroup pnd_Output_Gl_DtlRedef1;
    private DbsField pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Nbr;
    private DbsField pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Payee_Cde;
    private DbsField pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Mode_Cde;
    private DbsField pnd_Output_Gl_Dtl_Pnd_O_Inv_Pymnt_Ded_Cde;
    private DbsField pnd_Output_Gl_Dtl_Pnd_O_Pymnt_Acctg_Dte;
    private DbsField pnd_Output_Gl_Dtl_Pnd_O_Gl_Company;
    private DbsField pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Desc;
    private DbsField pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Db_Amt;
    private DbsField pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Cr_Amt;

    public DbsField getPnd_Output_Gl_Dtl() { return pnd_Output_Gl_Dtl; }

    public DbsGroup getPnd_Output_Gl_DtlRedef1() { return pnd_Output_Gl_DtlRedef1; }

    public DbsField getPnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Nbr() { return pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Nbr; }

    public DbsField getPnd_Output_Gl_Dtl_Pnd_O_Cntrct_Ppcn_Nbr() { return pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Output_Gl_Dtl_Pnd_O_Cntrct_Payee_Cde() { return pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Payee_Cde; }

    public DbsField getPnd_Output_Gl_Dtl_Pnd_O_Cntrct_Mode_Cde() { return pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Mode_Cde; }

    public DbsField getPnd_Output_Gl_Dtl_Pnd_O_Inv_Pymnt_Ded_Cde() { return pnd_Output_Gl_Dtl_Pnd_O_Inv_Pymnt_Ded_Cde; }

    public DbsField getPnd_Output_Gl_Dtl_Pnd_O_Pymnt_Acctg_Dte() { return pnd_Output_Gl_Dtl_Pnd_O_Pymnt_Acctg_Dte; }

    public DbsField getPnd_Output_Gl_Dtl_Pnd_O_Gl_Company() { return pnd_Output_Gl_Dtl_Pnd_O_Gl_Company; }

    public DbsField getPnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Desc() { return pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Desc; }

    public DbsField getPnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Db_Amt() { return pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Db_Amt; }

    public DbsField getPnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Cr_Amt() { return pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Cr_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Output_Gl_Dtl = newFieldInRecord("pnd_Output_Gl_Dtl", "#OUTPUT-GL-DTL", FieldType.STRING, 98);
        pnd_Output_Gl_DtlRedef1 = newGroupInRecord("pnd_Output_Gl_DtlRedef1", "Redefines", pnd_Output_Gl_Dtl);
        pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Nbr = pnd_Output_Gl_DtlRedef1.newFieldInGroup("pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Nbr", "#O-INV-ACCT-LEDGR-NBR", 
            FieldType.STRING, 15);
        pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Ppcn_Nbr = pnd_Output_Gl_DtlRedef1.newFieldInGroup("pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Ppcn_Nbr", "#O-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Payee_Cde = pnd_Output_Gl_DtlRedef1.newFieldInGroup("pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Payee_Cde", "#O-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Mode_Cde = pnd_Output_Gl_DtlRedef1.newFieldInGroup("pnd_Output_Gl_Dtl_Pnd_O_Cntrct_Mode_Cde", "#O-CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Output_Gl_Dtl_Pnd_O_Inv_Pymnt_Ded_Cde = pnd_Output_Gl_DtlRedef1.newFieldInGroup("pnd_Output_Gl_Dtl_Pnd_O_Inv_Pymnt_Ded_Cde", "#O-INV-PYMNT-DED-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Output_Gl_Dtl_Pnd_O_Pymnt_Acctg_Dte = pnd_Output_Gl_DtlRedef1.newFieldInGroup("pnd_Output_Gl_Dtl_Pnd_O_Pymnt_Acctg_Dte", "#O-PYMNT-ACCTG-DTE", 
            FieldType.DATE);
        pnd_Output_Gl_Dtl_Pnd_O_Gl_Company = pnd_Output_Gl_DtlRedef1.newFieldInGroup("pnd_Output_Gl_Dtl_Pnd_O_Gl_Company", "#O-GL-COMPANY", FieldType.STRING, 
            5);
        pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Desc = pnd_Output_Gl_DtlRedef1.newFieldInGroup("pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Desc", "#O-INV-ACCT-LEDGR-DESC", 
            FieldType.STRING, 40);
        pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Db_Amt = pnd_Output_Gl_DtlRedef1.newFieldInGroup("pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Db_Amt", "#O-INV-ACCT-LEDGR-DB-AMT", 
            FieldType.PACKED_DECIMAL, 13,2);
        pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Cr_Amt = pnd_Output_Gl_DtlRedef1.newFieldInGroup("pnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Cr_Amt", "#O-INV-ACCT-LEDGR-CR-AMT", 
            FieldType.PACKED_DECIMAL, 13,2);

        this.setRecordName("LdaFcpl830");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl830() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
