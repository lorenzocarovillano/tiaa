/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:26 PM
**        * FROM NATURAL PDA     : FCPA801B
************************************************************
**        * FILE NAME            : PdaFcpa801b.java
**        * CLASS NAME           : PdaFcpa801b
**        * INSTANCE NAME        : PdaFcpa801b
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa801b extends PdaBase
{
    // Properties
    private DbsGroup pnd_Check_Fields;
    private DbsField pnd_Check_Fields_Pnd_Fund_On_Page;
    private DbsField pnd_Check_Fields_Pnd_Ded_Pymnt_Table;
    private DbsField pnd_Check_Fields_Pnd_Cntrct_Pnd__Ded;
    private DbsField pnd_Check_Fields_Pnd_Pymnt_Records;
    private DbsField pnd_Check_Fields_Pnd_Pymnt_Ded_Pnd;
    private DbsField pnd_Check_Fields_Pnd_Dpi_Ind;
    private DbsField pnd_Check_Fields_Pnd_Pymnt_Ded_Ind;
    private DbsField pnd_Check_Fields_Pnd_Filler;

    public DbsGroup getPnd_Check_Fields() { return pnd_Check_Fields; }

    public DbsField getPnd_Check_Fields_Pnd_Fund_On_Page() { return pnd_Check_Fields_Pnd_Fund_On_Page; }

    public DbsField getPnd_Check_Fields_Pnd_Ded_Pymnt_Table() { return pnd_Check_Fields_Pnd_Ded_Pymnt_Table; }

    public DbsField getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded() { return pnd_Check_Fields_Pnd_Cntrct_Pnd__Ded; }

    public DbsField getPnd_Check_Fields_Pnd_Pymnt_Records() { return pnd_Check_Fields_Pnd_Pymnt_Records; }

    public DbsField getPnd_Check_Fields_Pnd_Pymnt_Ded_Pnd() { return pnd_Check_Fields_Pnd_Pymnt_Ded_Pnd; }

    public DbsField getPnd_Check_Fields_Pnd_Dpi_Ind() { return pnd_Check_Fields_Pnd_Dpi_Ind; }

    public DbsField getPnd_Check_Fields_Pnd_Pymnt_Ded_Ind() { return pnd_Check_Fields_Pnd_Pymnt_Ded_Ind; }

    public DbsField getPnd_Check_Fields_Pnd_Filler() { return pnd_Check_Fields_Pnd_Filler; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Check_Fields = dbsRecord.newGroupInRecord("pnd_Check_Fields", "#CHECK-FIELDS");
        pnd_Check_Fields.setParameterOption(ParameterOption.ByReference);
        pnd_Check_Fields_Pnd_Fund_On_Page = pnd_Check_Fields.newFieldArrayInGroup("pnd_Check_Fields_Pnd_Fund_On_Page", "#FUND-ON-PAGE", FieldType.PACKED_DECIMAL, 
            3, new DbsArrayController(1,20));
        pnd_Check_Fields_Pnd_Ded_Pymnt_Table = pnd_Check_Fields.newFieldArrayInGroup("pnd_Check_Fields_Pnd_Ded_Pymnt_Table", "#DED-PYMNT-TABLE", FieldType.STRING, 
            1, new DbsArrayController(1,15));
        pnd_Check_Fields_Pnd_Cntrct_Pnd__Ded = pnd_Check_Fields.newFieldInGroup("pnd_Check_Fields_Pnd_Cntrct_Pnd__Ded", "#CNTRCT-#-DED", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Check_Fields_Pnd_Pymnt_Records = pnd_Check_Fields.newFieldInGroup("pnd_Check_Fields_Pnd_Pymnt_Records", "#PYMNT-RECORDS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Check_Fields_Pnd_Pymnt_Ded_Pnd = pnd_Check_Fields.newFieldInGroup("pnd_Check_Fields_Pnd_Pymnt_Ded_Pnd", "#PYMNT-DED-#", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Check_Fields_Pnd_Dpi_Ind = pnd_Check_Fields.newFieldInGroup("pnd_Check_Fields_Pnd_Dpi_Ind", "#DPI-IND", FieldType.BOOLEAN);
        pnd_Check_Fields_Pnd_Pymnt_Ded_Ind = pnd_Check_Fields.newFieldInGroup("pnd_Check_Fields_Pnd_Pymnt_Ded_Ind", "#PYMNT-DED-IND", FieldType.BOOLEAN);
        pnd_Check_Fields_Pnd_Filler = pnd_Check_Fields.newFieldInGroup("pnd_Check_Fields_Pnd_Filler", "#FILLER", FieldType.STRING, 7);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa801b(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

