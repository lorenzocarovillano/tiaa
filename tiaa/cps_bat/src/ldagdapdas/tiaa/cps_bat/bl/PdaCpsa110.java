/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:26 PM
**        * FROM NATURAL PDA     : CPSA110
************************************************************
**        * FILE NAME            : PdaCpsa110.java
**        * CLASS NAME           : PdaCpsa110
**        * INSTANCE NAME        : PdaCpsa110
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCpsa110 extends PdaBase
{
    // Properties
    private DbsGroup cpsa110;
    private DbsGroup cpsa110_Cpsa110a;
    private DbsField cpsa110_Cpsa110_Source_Code;
    private DbsField cpsa110_Cpsa110_Function;
    private DbsField cpsa110_Cpsa110_New_Seq_Nbr;
    private DbsGroup cpsa110_Cpsa110b;
    private DbsField cpsa110_Cpsa110_Return_Code;
    private DbsField cpsa110_Rt_Table_Id;
    private DbsField cpsa110_Rt_A_I_Ind;
    private DbsField cpsa110_Rt_Short_Key;
    private DbsField cpsa110_Rt_Long_Key;
    private DbsGroup cpsa110_Bank_Account_Data;
    private DbsField cpsa110_Bank_Source_Code;
    private DbsField cpsa110_Bank_Routing;
    private DbsField cpsa110_Bank_Account;
    private DbsField cpsa110_Bank_Account_Name;
    private DbsField cpsa110_Bank_Account_Cde;
    private DbsField cpsa110_Bank_Account_Desc;
    private DbsField cpsa110_Bank_Business_Unit;
    private DbsField cpsa110_Bank_Format;
    private DbsField cpsa110_Bank_Transmit_Ind;
    private DbsField cpsa110_Bank_Ledger_Nbr;
    private DbsField cpsa110_Bank_Signature_Cde;
    private DbsField cpsa110_Check_Acct_Ind;
    private DbsField cpsa110_Eft_Acct_Ind;
    private DbsField cpsa110_Start_Seq;
    private DbsField cpsa110_End_Seq;
    private DbsField cpsa110_Hops_Acct_Ind;
    private DbsField cpsa110_Bank_Void_Msg;
    private DbsField cpsa110_Bank_Orgn_Cde;
    private DbsField cpsa110_Bank_Internal_Pospay_Ind;
    private DbsGroup cpsa110_Bank_Logo_Data;
    private DbsField cpsa110_Bank_Logo_Key;
    private DbsField cpsa110_Bank_Logo_Name1;
    private DbsField cpsa110_Bank_Logo_Name2;
    private DbsField cpsa110_Bank_Logo_Address1;
    private DbsField cpsa110_Bank_Logo_Address2;
    private DbsField cpsa110_Bank_Short_Logo_Name;
    private DbsField cpsa110_Bank_Graphic_Logo_Cde;
    private DbsGroup cpsa110_Bank_Name_Data;
    private DbsField cpsa110_Bank_Name;
    private DbsField cpsa110_Bank_Address1;
    private DbsField cpsa110_Bank_Address2;
    private DbsField cpsa110_Bank_Above_Check_Amt_Nbr;
    private DbsField cpsa110_Bank_Eft_Trans_Exists;
    private DbsField cpsa110_Bank_Pos_Pay_Trans_Exists;
    private DbsField cpsa110_Bank_Transmission_Cde;

    public DbsGroup getCpsa110() { return cpsa110; }

    public DbsGroup getCpsa110_Cpsa110a() { return cpsa110_Cpsa110a; }

    public DbsField getCpsa110_Cpsa110_Source_Code() { return cpsa110_Cpsa110_Source_Code; }

    public DbsField getCpsa110_Cpsa110_Function() { return cpsa110_Cpsa110_Function; }

    public DbsField getCpsa110_Cpsa110_New_Seq_Nbr() { return cpsa110_Cpsa110_New_Seq_Nbr; }

    public DbsGroup getCpsa110_Cpsa110b() { return cpsa110_Cpsa110b; }

    public DbsField getCpsa110_Cpsa110_Return_Code() { return cpsa110_Cpsa110_Return_Code; }

    public DbsField getCpsa110_Rt_Table_Id() { return cpsa110_Rt_Table_Id; }

    public DbsField getCpsa110_Rt_A_I_Ind() { return cpsa110_Rt_A_I_Ind; }

    public DbsField getCpsa110_Rt_Short_Key() { return cpsa110_Rt_Short_Key; }

    public DbsField getCpsa110_Rt_Long_Key() { return cpsa110_Rt_Long_Key; }

    public DbsGroup getCpsa110_Bank_Account_Data() { return cpsa110_Bank_Account_Data; }

    public DbsField getCpsa110_Bank_Source_Code() { return cpsa110_Bank_Source_Code; }

    public DbsField getCpsa110_Bank_Routing() { return cpsa110_Bank_Routing; }

    public DbsField getCpsa110_Bank_Account() { return cpsa110_Bank_Account; }

    public DbsField getCpsa110_Bank_Account_Name() { return cpsa110_Bank_Account_Name; }

    public DbsField getCpsa110_Bank_Account_Cde() { return cpsa110_Bank_Account_Cde; }

    public DbsField getCpsa110_Bank_Account_Desc() { return cpsa110_Bank_Account_Desc; }

    public DbsField getCpsa110_Bank_Business_Unit() { return cpsa110_Bank_Business_Unit; }

    public DbsField getCpsa110_Bank_Format() { return cpsa110_Bank_Format; }

    public DbsField getCpsa110_Bank_Transmit_Ind() { return cpsa110_Bank_Transmit_Ind; }

    public DbsField getCpsa110_Bank_Ledger_Nbr() { return cpsa110_Bank_Ledger_Nbr; }

    public DbsField getCpsa110_Bank_Signature_Cde() { return cpsa110_Bank_Signature_Cde; }

    public DbsField getCpsa110_Check_Acct_Ind() { return cpsa110_Check_Acct_Ind; }

    public DbsField getCpsa110_Eft_Acct_Ind() { return cpsa110_Eft_Acct_Ind; }

    public DbsField getCpsa110_Start_Seq() { return cpsa110_Start_Seq; }

    public DbsField getCpsa110_End_Seq() { return cpsa110_End_Seq; }

    public DbsField getCpsa110_Hops_Acct_Ind() { return cpsa110_Hops_Acct_Ind; }

    public DbsField getCpsa110_Bank_Void_Msg() { return cpsa110_Bank_Void_Msg; }

    public DbsField getCpsa110_Bank_Orgn_Cde() { return cpsa110_Bank_Orgn_Cde; }

    public DbsField getCpsa110_Bank_Internal_Pospay_Ind() { return cpsa110_Bank_Internal_Pospay_Ind; }

    public DbsGroup getCpsa110_Bank_Logo_Data() { return cpsa110_Bank_Logo_Data; }

    public DbsField getCpsa110_Bank_Logo_Key() { return cpsa110_Bank_Logo_Key; }

    public DbsField getCpsa110_Bank_Logo_Name1() { return cpsa110_Bank_Logo_Name1; }

    public DbsField getCpsa110_Bank_Logo_Name2() { return cpsa110_Bank_Logo_Name2; }

    public DbsField getCpsa110_Bank_Logo_Address1() { return cpsa110_Bank_Logo_Address1; }

    public DbsField getCpsa110_Bank_Logo_Address2() { return cpsa110_Bank_Logo_Address2; }

    public DbsField getCpsa110_Bank_Short_Logo_Name() { return cpsa110_Bank_Short_Logo_Name; }

    public DbsField getCpsa110_Bank_Graphic_Logo_Cde() { return cpsa110_Bank_Graphic_Logo_Cde; }

    public DbsGroup getCpsa110_Bank_Name_Data() { return cpsa110_Bank_Name_Data; }

    public DbsField getCpsa110_Bank_Name() { return cpsa110_Bank_Name; }

    public DbsField getCpsa110_Bank_Address1() { return cpsa110_Bank_Address1; }

    public DbsField getCpsa110_Bank_Address2() { return cpsa110_Bank_Address2; }

    public DbsField getCpsa110_Bank_Above_Check_Amt_Nbr() { return cpsa110_Bank_Above_Check_Amt_Nbr; }

    public DbsField getCpsa110_Bank_Eft_Trans_Exists() { return cpsa110_Bank_Eft_Trans_Exists; }

    public DbsField getCpsa110_Bank_Pos_Pay_Trans_Exists() { return cpsa110_Bank_Pos_Pay_Trans_Exists; }

    public DbsField getCpsa110_Bank_Transmission_Cde() { return cpsa110_Bank_Transmission_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cpsa110 = dbsRecord.newGroupInRecord("cpsa110", "CPSA110");
        cpsa110.setParameterOption(ParameterOption.ByReference);
        cpsa110_Cpsa110a = cpsa110.newGroupInGroup("cpsa110_Cpsa110a", "CPSA110A");
        cpsa110_Cpsa110_Source_Code = cpsa110_Cpsa110a.newFieldInGroup("cpsa110_Cpsa110_Source_Code", "CPSA110-SOURCE-CODE", FieldType.STRING, 5);
        cpsa110_Cpsa110_Function = cpsa110_Cpsa110a.newFieldInGroup("cpsa110_Cpsa110_Function", "CPSA110-FUNCTION", FieldType.STRING, 15);
        cpsa110_Cpsa110_New_Seq_Nbr = cpsa110_Cpsa110a.newFieldInGroup("cpsa110_Cpsa110_New_Seq_Nbr", "CPSA110-NEW-SEQ-NBR", FieldType.NUMERIC, 10);
        cpsa110_Cpsa110b = cpsa110.newGroupInGroup("cpsa110_Cpsa110b", "CPSA110B");
        cpsa110_Cpsa110_Return_Code = cpsa110_Cpsa110b.newFieldInGroup("cpsa110_Cpsa110_Return_Code", "CPSA110-RETURN-CODE", FieldType.STRING, 2);
        cpsa110_Rt_Table_Id = cpsa110_Cpsa110b.newFieldInGroup("cpsa110_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5);
        cpsa110_Rt_A_I_Ind = cpsa110_Cpsa110b.newFieldInGroup("cpsa110_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1);
        cpsa110_Rt_Short_Key = cpsa110_Cpsa110b.newFieldInGroup("cpsa110_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20);
        cpsa110_Rt_Long_Key = cpsa110_Cpsa110b.newFieldInGroup("cpsa110_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40);
        cpsa110_Bank_Account_Data = cpsa110_Cpsa110b.newGroupInGroup("cpsa110_Bank_Account_Data", "BANK-ACCOUNT-DATA");
        cpsa110_Bank_Source_Code = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Source_Code", "BANK-SOURCE-CODE", FieldType.STRING, 5);
        cpsa110_Bank_Routing = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Routing", "BANK-ROUTING", FieldType.STRING, 9);
        cpsa110_Bank_Account = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21);
        cpsa110_Bank_Account_Name = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Account_Name", "BANK-ACCOUNT-NAME", FieldType.STRING, 50);
        cpsa110_Bank_Account_Cde = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Account_Cde", "BANK-ACCOUNT-CDE", FieldType.STRING, 10);
        cpsa110_Bank_Account_Desc = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Account_Desc", "BANK-ACCOUNT-DESC", FieldType.STRING, 50);
        cpsa110_Bank_Business_Unit = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Business_Unit", "BANK-BUSINESS-UNIT", FieldType.STRING, 5);
        cpsa110_Bank_Format = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Format", "BANK-FORMAT", FieldType.STRING, 5);
        cpsa110_Bank_Transmit_Ind = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Transmit_Ind", "BANK-TRANSMIT-IND", FieldType.STRING, 1);
        cpsa110_Bank_Ledger_Nbr = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Ledger_Nbr", "BANK-LEDGER-NBR", FieldType.STRING, 8);
        cpsa110_Bank_Signature_Cde = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Signature_Cde", "BANK-SIGNATURE-CDE", FieldType.STRING, 5);
        cpsa110_Check_Acct_Ind = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Check_Acct_Ind", "CHECK-ACCT-IND", FieldType.STRING, 1);
        cpsa110_Eft_Acct_Ind = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Eft_Acct_Ind", "EFT-ACCT-IND", FieldType.STRING, 1);
        cpsa110_Start_Seq = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Start_Seq", "START-SEQ", FieldType.NUMERIC, 10);
        cpsa110_End_Seq = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_End_Seq", "END-SEQ", FieldType.NUMERIC, 10);
        cpsa110_Hops_Acct_Ind = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Hops_Acct_Ind", "HOPS-ACCT-IND", FieldType.STRING, 1);
        cpsa110_Bank_Void_Msg = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Void_Msg", "BANK-VOID-MSG", FieldType.STRING, 30);
        cpsa110_Bank_Orgn_Cde = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Orgn_Cde", "BANK-ORGN-CDE", FieldType.STRING, 2);
        cpsa110_Bank_Internal_Pospay_Ind = cpsa110_Bank_Account_Data.newFieldInGroup("cpsa110_Bank_Internal_Pospay_Ind", "BANK-INTERNAL-POSPAY-IND", FieldType.STRING, 
            1);
        cpsa110_Bank_Logo_Data = cpsa110_Cpsa110b.newGroupInGroup("cpsa110_Bank_Logo_Data", "BANK-LOGO-DATA");
        cpsa110_Bank_Logo_Key = cpsa110_Bank_Logo_Data.newFieldInGroup("cpsa110_Bank_Logo_Key", "BANK-LOGO-KEY", FieldType.STRING, 10);
        cpsa110_Bank_Logo_Name1 = cpsa110_Bank_Logo_Data.newFieldInGroup("cpsa110_Bank_Logo_Name1", "BANK-LOGO-NAME1", FieldType.STRING, 50);
        cpsa110_Bank_Logo_Name2 = cpsa110_Bank_Logo_Data.newFieldInGroup("cpsa110_Bank_Logo_Name2", "BANK-LOGO-NAME2", FieldType.STRING, 50);
        cpsa110_Bank_Logo_Address1 = cpsa110_Bank_Logo_Data.newFieldInGroup("cpsa110_Bank_Logo_Address1", "BANK-LOGO-ADDRESS1", FieldType.STRING, 50);
        cpsa110_Bank_Logo_Address2 = cpsa110_Bank_Logo_Data.newFieldInGroup("cpsa110_Bank_Logo_Address2", "BANK-LOGO-ADDRESS2", FieldType.STRING, 50);
        cpsa110_Bank_Short_Logo_Name = cpsa110_Bank_Logo_Data.newFieldInGroup("cpsa110_Bank_Short_Logo_Name", "BANK-SHORT-LOGO-NAME", FieldType.STRING, 
            15);
        cpsa110_Bank_Graphic_Logo_Cde = cpsa110_Bank_Logo_Data.newFieldInGroup("cpsa110_Bank_Graphic_Logo_Cde", "BANK-GRAPHIC-LOGO-CDE", FieldType.STRING, 
            5);
        cpsa110_Bank_Name_Data = cpsa110_Cpsa110b.newGroupInGroup("cpsa110_Bank_Name_Data", "BANK-NAME-DATA");
        cpsa110_Bank_Name = cpsa110_Bank_Name_Data.newFieldInGroup("cpsa110_Bank_Name", "BANK-NAME", FieldType.STRING, 50);
        cpsa110_Bank_Address1 = cpsa110_Bank_Name_Data.newFieldInGroup("cpsa110_Bank_Address1", "BANK-ADDRESS1", FieldType.STRING, 50);
        cpsa110_Bank_Address2 = cpsa110_Bank_Name_Data.newFieldInGroup("cpsa110_Bank_Address2", "BANK-ADDRESS2", FieldType.STRING, 50);
        cpsa110_Bank_Above_Check_Amt_Nbr = cpsa110_Bank_Name_Data.newFieldInGroup("cpsa110_Bank_Above_Check_Amt_Nbr", "BANK-ABOVE-CHECK-AMT-NBR", FieldType.STRING, 
            10);
        cpsa110_Bank_Eft_Trans_Exists = cpsa110_Bank_Name_Data.newFieldInGroup("cpsa110_Bank_Eft_Trans_Exists", "BANK-EFT-TRANS-EXISTS", FieldType.STRING, 
            1);
        cpsa110_Bank_Pos_Pay_Trans_Exists = cpsa110_Bank_Name_Data.newFieldInGroup("cpsa110_Bank_Pos_Pay_Trans_Exists", "BANK-POS-PAY-TRANS-EXISTS", FieldType.STRING, 
            1);
        cpsa110_Bank_Transmission_Cde = cpsa110_Bank_Name_Data.newFieldInGroup("cpsa110_Bank_Transmission_Cde", "BANK-TRANSMISSION-CDE", FieldType.STRING, 
            5);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCpsa110(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

