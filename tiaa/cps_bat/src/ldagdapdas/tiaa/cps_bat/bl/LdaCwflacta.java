/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:57:03 PM
**        * FROM NATURAL LDA     : CWFLACTA
************************************************************
**        * FILE NAME            : LdaCwflacta.java
**        * CLASS NAME           : LdaCwflacta
**        * INSTANCE NAME        : LdaCwflacta
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwflacta extends DbsRecord
{
    // Properties
    private DbsField cwf_Corp_Rpt_Act_1;
    private DbsGroup cwf_Corp_Rpt_Act_1Redef1;
    private DbsGroup cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act;
    private DbsField cwf_Corp_Rpt_Act_1_Rqst_Log_Dte_Tme_A;
    private DbsField cwf_Corp_Rpt_Act_1_Strtng_Event_Dte_Tme_A;
    private DbsField cwf_Corp_Rpt_Act_1_Endng_Event_Dte_Tme_A;
    private DbsField cwf_Corp_Rpt_Act_1_Cmpltd_Actvty_Ind;
    private DbsField cwf_Corp_Rpt_Act_1_Admin_Unit_Cde;
    private DbsField cwf_Corp_Rpt_Act_1_Empl_Racf_Id;
    private DbsField cwf_Corp_Rpt_Act_1_Step_Id;
    private DbsField cwf_Corp_Rpt_Act_1_Admin_Status_Cde;
    private DbsField cwf_Corp_Rpt_Act_1_Unit_On_Tme_Ind;
    private DbsField cwf_Corp_Rpt_Act_1_Rt_Sqnce_Nbr;

    public DbsField getCwf_Corp_Rpt_Act_1() { return cwf_Corp_Rpt_Act_1; }

    public DbsGroup getCwf_Corp_Rpt_Act_1Redef1() { return cwf_Corp_Rpt_Act_1Redef1; }

    public DbsGroup getCwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act() { return cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act; }

    public DbsField getCwf_Corp_Rpt_Act_1_Rqst_Log_Dte_Tme_A() { return cwf_Corp_Rpt_Act_1_Rqst_Log_Dte_Tme_A; }

    public DbsField getCwf_Corp_Rpt_Act_1_Strtng_Event_Dte_Tme_A() { return cwf_Corp_Rpt_Act_1_Strtng_Event_Dte_Tme_A; }

    public DbsField getCwf_Corp_Rpt_Act_1_Endng_Event_Dte_Tme_A() { return cwf_Corp_Rpt_Act_1_Endng_Event_Dte_Tme_A; }

    public DbsField getCwf_Corp_Rpt_Act_1_Cmpltd_Actvty_Ind() { return cwf_Corp_Rpt_Act_1_Cmpltd_Actvty_Ind; }

    public DbsField getCwf_Corp_Rpt_Act_1_Admin_Unit_Cde() { return cwf_Corp_Rpt_Act_1_Admin_Unit_Cde; }

    public DbsField getCwf_Corp_Rpt_Act_1_Empl_Racf_Id() { return cwf_Corp_Rpt_Act_1_Empl_Racf_Id; }

    public DbsField getCwf_Corp_Rpt_Act_1_Step_Id() { return cwf_Corp_Rpt_Act_1_Step_Id; }

    public DbsField getCwf_Corp_Rpt_Act_1_Admin_Status_Cde() { return cwf_Corp_Rpt_Act_1_Admin_Status_Cde; }

    public DbsField getCwf_Corp_Rpt_Act_1_Unit_On_Tme_Ind() { return cwf_Corp_Rpt_Act_1_Unit_On_Tme_Ind; }

    public DbsField getCwf_Corp_Rpt_Act_1_Rt_Sqnce_Nbr() { return cwf_Corp_Rpt_Act_1_Rt_Sqnce_Nbr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cwf_Corp_Rpt_Act_1 = newFieldInRecord("cwf_Corp_Rpt_Act_1", "CWF-CORP-RPT-ACT-1", FieldType.STRING, 109);
        cwf_Corp_Rpt_Act_1Redef1 = newGroupInRecord("cwf_Corp_Rpt_Act_1Redef1", "Redefines", cwf_Corp_Rpt_Act_1);
        cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act = cwf_Corp_Rpt_Act_1Redef1.newGroupInGroup("cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act", "CWF-CORP-RPT-ACT");
        cwf_Corp_Rpt_Act_1_Rqst_Log_Dte_Tme_A = cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act.newFieldInGroup("cwf_Corp_Rpt_Act_1_Rqst_Log_Dte_Tme_A", "RQST-LOG-DTE-TME-A", 
            FieldType.STRING, 26);
        cwf_Corp_Rpt_Act_1_Strtng_Event_Dte_Tme_A = cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act.newFieldInGroup("cwf_Corp_Rpt_Act_1_Strtng_Event_Dte_Tme_A", "STRTNG-EVENT-DTE-TME-A", 
            FieldType.STRING, 26);
        cwf_Corp_Rpt_Act_1_Endng_Event_Dte_Tme_A = cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act.newFieldInGroup("cwf_Corp_Rpt_Act_1_Endng_Event_Dte_Tme_A", "ENDNG-EVENT-DTE-TME-A", 
            FieldType.STRING, 26);
        cwf_Corp_Rpt_Act_1_Cmpltd_Actvty_Ind = cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act.newFieldInGroup("cwf_Corp_Rpt_Act_1_Cmpltd_Actvty_Ind", "CMPLTD-ACTVTY-IND", 
            FieldType.STRING, 1);
        cwf_Corp_Rpt_Act_1_Admin_Unit_Cde = cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act.newFieldInGroup("cwf_Corp_Rpt_Act_1_Admin_Unit_Cde", "ADMIN-UNIT-CDE", 
            FieldType.STRING, 8);
        cwf_Corp_Rpt_Act_1_Empl_Racf_Id = cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act.newFieldInGroup("cwf_Corp_Rpt_Act_1_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8);
        cwf_Corp_Rpt_Act_1_Step_Id = cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act.newFieldInGroup("cwf_Corp_Rpt_Act_1_Step_Id", "STEP-ID", FieldType.STRING, 6);
        cwf_Corp_Rpt_Act_1_Admin_Status_Cde = cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act.newFieldInGroup("cwf_Corp_Rpt_Act_1_Admin_Status_Cde", "ADMIN-STATUS-CDE", 
            FieldType.STRING, 4);
        cwf_Corp_Rpt_Act_1_Unit_On_Tme_Ind = cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act.newFieldInGroup("cwf_Corp_Rpt_Act_1_Unit_On_Tme_Ind", "UNIT-ON-TME-IND", 
            FieldType.STRING, 1);
        cwf_Corp_Rpt_Act_1_Rt_Sqnce_Nbr = cwf_Corp_Rpt_Act_1_Cwf_Corp_Rpt_Act.newFieldInGroup("cwf_Corp_Rpt_Act_1_Rt_Sqnce_Nbr", "RT-SQNCE-NBR", FieldType.NUMERIC, 
            3);

        this.setRecordName("LdaCwflacta");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwflacta() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
