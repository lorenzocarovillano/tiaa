/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:41 PM
**        * FROM NATURAL LDA     : CPULFND
************************************************************
**        * FILE NAME            : LdaCpulfnd.java
**        * CLASS NAME           : LdaCpulfnd
**        * INSTANCE NAME        : LdaCpulfnd
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpulfnd extends DbsRecord
{
    // Properties
    private DbsField detail_Record;
    private DbsGroup detail_RecordRedef1;
    private DbsField detail_Record_Investment_Id;
    private DbsField detail_Record_Unit_Shares;
    private DbsField detail_Record_Unit_Price;
    private DbsField detail_Record_Settlement_Amt;
    private DbsField detail_Record_Dividend_Amt;
    private DbsField detail_Record_Legacy_Eft_Key;
    private DbsGroup detail_Record_Legacy_Eft_KeyRedef2;
    private DbsField detail_Record_Key_Contract;
    private DbsField detail_Record_Key_Payee;
    private DbsField detail_Record_Key_Origin;
    private DbsField detail_Record_Key_Inverse_Date;
    private DbsField detail_Record_Key_Process_Seq;
    private DbsGroup detail_Record_Key_Process_SeqRedef3;
    private DbsField detail_Record_Pnd_Seq_A;
    private DbsField detail_Record_Interest_Amt;
    private DbsField detail_Record_Account_Code;
    private DbsField detail_Record_Insurance_Type;
    private DbsField filler01;
    private DbsField detail_Record_Check_Number;
    private DbsGroup detail_Record_Check_NumberRedef4;
    private DbsField detail_Record_Check_Alpha;

    public DbsField getDetail_Record() { return detail_Record; }

    public DbsGroup getDetail_RecordRedef1() { return detail_RecordRedef1; }

    public DbsField getDetail_Record_Investment_Id() { return detail_Record_Investment_Id; }

    public DbsField getDetail_Record_Unit_Shares() { return detail_Record_Unit_Shares; }

    public DbsField getDetail_Record_Unit_Price() { return detail_Record_Unit_Price; }

    public DbsField getDetail_Record_Settlement_Amt() { return detail_Record_Settlement_Amt; }

    public DbsField getDetail_Record_Dividend_Amt() { return detail_Record_Dividend_Amt; }

    public DbsField getDetail_Record_Legacy_Eft_Key() { return detail_Record_Legacy_Eft_Key; }

    public DbsGroup getDetail_Record_Legacy_Eft_KeyRedef2() { return detail_Record_Legacy_Eft_KeyRedef2; }

    public DbsField getDetail_Record_Key_Contract() { return detail_Record_Key_Contract; }

    public DbsField getDetail_Record_Key_Payee() { return detail_Record_Key_Payee; }

    public DbsField getDetail_Record_Key_Origin() { return detail_Record_Key_Origin; }

    public DbsField getDetail_Record_Key_Inverse_Date() { return detail_Record_Key_Inverse_Date; }

    public DbsField getDetail_Record_Key_Process_Seq() { return detail_Record_Key_Process_Seq; }

    public DbsGroup getDetail_Record_Key_Process_SeqRedef3() { return detail_Record_Key_Process_SeqRedef3; }

    public DbsField getDetail_Record_Pnd_Seq_A() { return detail_Record_Pnd_Seq_A; }

    public DbsField getDetail_Record_Interest_Amt() { return detail_Record_Interest_Amt; }

    public DbsField getDetail_Record_Account_Code() { return detail_Record_Account_Code; }

    public DbsField getDetail_Record_Insurance_Type() { return detail_Record_Insurance_Type; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getDetail_Record_Check_Number() { return detail_Record_Check_Number; }

    public DbsGroup getDetail_Record_Check_NumberRedef4() { return detail_Record_Check_NumberRedef4; }

    public DbsField getDetail_Record_Check_Alpha() { return detail_Record_Check_Alpha; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        detail_Record = newFieldInRecord("detail_Record", "DETAIL-RECORD", FieldType.STRING, 200);
        detail_RecordRedef1 = newGroupInRecord("detail_RecordRedef1", "Redefines", detail_Record);
        detail_Record_Investment_Id = detail_RecordRedef1.newFieldInGroup("detail_Record_Investment_Id", "INVESTMENT-ID", FieldType.STRING, 10);
        detail_Record_Unit_Shares = detail_RecordRedef1.newFieldInGroup("detail_Record_Unit_Shares", "UNIT-SHARES", FieldType.DECIMAL, 10,3);
        detail_Record_Unit_Price = detail_RecordRedef1.newFieldInGroup("detail_Record_Unit_Price", "UNIT-PRICE", FieldType.DECIMAL, 9,4);
        detail_Record_Settlement_Amt = detail_RecordRedef1.newFieldInGroup("detail_Record_Settlement_Amt", "SETTLEMENT-AMT", FieldType.DECIMAL, 11,2);
        detail_Record_Dividend_Amt = detail_RecordRedef1.newFieldInGroup("detail_Record_Dividend_Amt", "DIVIDEND-AMT", FieldType.DECIMAL, 9,2);
        detail_Record_Legacy_Eft_Key = detail_RecordRedef1.newFieldInGroup("detail_Record_Legacy_Eft_Key", "LEGACY-EFT-KEY", FieldType.STRING, 33);
        detail_Record_Legacy_Eft_KeyRedef2 = detail_RecordRedef1.newGroupInGroup("detail_Record_Legacy_Eft_KeyRedef2", "Redefines", detail_Record_Legacy_Eft_Key);
        detail_Record_Key_Contract = detail_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("detail_Record_Key_Contract", "KEY-CONTRACT", FieldType.STRING, 
            10);
        detail_Record_Key_Payee = detail_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("detail_Record_Key_Payee", "KEY-PAYEE", FieldType.STRING, 4);
        detail_Record_Key_Origin = detail_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("detail_Record_Key_Origin", "KEY-ORIGIN", FieldType.STRING, 2);
        detail_Record_Key_Inverse_Date = detail_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("detail_Record_Key_Inverse_Date", "KEY-INVERSE-DATE", FieldType.NUMERIC, 
            8);
        detail_Record_Key_Process_Seq = detail_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("detail_Record_Key_Process_Seq", "KEY-PROCESS-SEQ", FieldType.NUMERIC, 
            9);
        detail_Record_Key_Process_SeqRedef3 = detail_Record_Legacy_Eft_KeyRedef2.newGroupInGroup("detail_Record_Key_Process_SeqRedef3", "Redefines", detail_Record_Key_Process_Seq);
        detail_Record_Pnd_Seq_A = detail_Record_Key_Process_SeqRedef3.newFieldInGroup("detail_Record_Pnd_Seq_A", "#SEQ-A", FieldType.STRING, 7);
        detail_Record_Interest_Amt = detail_RecordRedef1.newFieldInGroup("detail_Record_Interest_Amt", "INTEREST-AMT", FieldType.DECIMAL, 9,2);
        detail_Record_Account_Code = detail_RecordRedef1.newFieldInGroup("detail_Record_Account_Code", "ACCOUNT-CODE", FieldType.STRING, 2);
        detail_Record_Insurance_Type = detail_RecordRedef1.newFieldInGroup("detail_Record_Insurance_Type", "INSURANCE-TYPE", FieldType.STRING, 1);
        filler01 = detail_RecordRedef1.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 96);
        detail_Record_Check_Number = detail_RecordRedef1.newFieldInGroup("detail_Record_Check_Number", "CHECK-NUMBER", FieldType.NUMERIC, 10);
        detail_Record_Check_NumberRedef4 = detail_RecordRedef1.newGroupInGroup("detail_Record_Check_NumberRedef4", "Redefines", detail_Record_Check_Number);
        detail_Record_Check_Alpha = detail_Record_Check_NumberRedef4.newFieldInGroup("detail_Record_Check_Alpha", "CHECK-ALPHA", FieldType.STRING, 10);

        this.setRecordName("LdaCpulfnd");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCpulfnd() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
