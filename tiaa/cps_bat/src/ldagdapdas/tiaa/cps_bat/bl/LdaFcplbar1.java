/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:25 PM
**        * FROM NATURAL LDA     : FCPLBAR1
************************************************************
**        * FILE NAME            : LdaFcplbar1.java
**        * CLASS NAME           : LdaFcplbar1
**        * INSTANCE NAME        : LdaFcplbar1
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplbar1 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Barcode_Lda;
    private DbsGroup pnd_Barcode_Lda_Pnd_Barcode_Input;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_New_Run;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Set_Last_Page;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Tiaa_Cover_Pull;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Cref_Cover_Pull;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Stitch_Ind;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Demand_Feed;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Unassigned_1;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Alert_Clear;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Divert;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Zip_Break;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Unassigned_2;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Unassigned_3;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Unassigned_4;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Unassigned_5;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Unassigned_6;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Unassigned_7;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Unassigned_8;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Barcode;
    private DbsGroup pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_End_Ind;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_1;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_End_Of_Set_Ind;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_2;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_3;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_4;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_5;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Page_Integrity_Counter;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Env_Id;
    private DbsGroup pnd_Barcode_Lda_Pnd_Bar_Env_IdRedef2;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Env_Id_Num;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Start_Ind;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Envelopes;
    private DbsField pnd_Barcode_Lda_Pnd_Bar_Pages_In_Env;

    public DbsGroup getPnd_Barcode_Lda() { return pnd_Barcode_Lda; }

    public DbsGroup getPnd_Barcode_Lda_Pnd_Barcode_Input() { return pnd_Barcode_Lda_Pnd_Barcode_Input; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_New_Run() { return pnd_Barcode_Lda_Pnd_Bar_New_Run; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Set_Last_Page() { return pnd_Barcode_Lda_Pnd_Bar_Set_Last_Page; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Tiaa_Cover_Pull() { return pnd_Barcode_Lda_Pnd_Bar_Tiaa_Cover_Pull; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Cref_Cover_Pull() { return pnd_Barcode_Lda_Pnd_Bar_Cref_Cover_Pull; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Stitch_Ind() { return pnd_Barcode_Lda_Pnd_Bar_Stitch_Ind; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Demand_Feed() { return pnd_Barcode_Lda_Pnd_Bar_Demand_Feed; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Unassigned_1() { return pnd_Barcode_Lda_Pnd_Bar_Unassigned_1; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Alert_Clear() { return pnd_Barcode_Lda_Pnd_Bar_Alert_Clear; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Divert() { return pnd_Barcode_Lda_Pnd_Bar_Divert; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Zip_Break() { return pnd_Barcode_Lda_Pnd_Bar_Zip_Break; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Unassigned_2() { return pnd_Barcode_Lda_Pnd_Bar_Unassigned_2; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Unassigned_3() { return pnd_Barcode_Lda_Pnd_Bar_Unassigned_3; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Unassigned_4() { return pnd_Barcode_Lda_Pnd_Bar_Unassigned_4; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Unassigned_5() { return pnd_Barcode_Lda_Pnd_Bar_Unassigned_5; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Unassigned_6() { return pnd_Barcode_Lda_Pnd_Bar_Unassigned_6; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Unassigned_7() { return pnd_Barcode_Lda_Pnd_Bar_Unassigned_7; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Unassigned_8() { return pnd_Barcode_Lda_Pnd_Bar_Unassigned_8; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5() { return pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Barcode() { return pnd_Barcode_Lda_Pnd_Bar_Barcode; }

    public DbsGroup getPnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1() { return pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_End_Ind() { return pnd_Barcode_Lda_Pnd_Bar_End_Ind; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_1() { return pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_1; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_End_Of_Set_Ind() { return pnd_Barcode_Lda_Pnd_Bar_End_Of_Set_Ind; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_2() { return pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_2; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_3() { return pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_3; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_4() { return pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_4; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_5() { return pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_5; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Page_Integrity_Counter() { return pnd_Barcode_Lda_Pnd_Bar_Page_Integrity_Counter; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter() { return pnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Env_Id() { return pnd_Barcode_Lda_Pnd_Bar_Env_Id; }

    public DbsGroup getPnd_Barcode_Lda_Pnd_Bar_Env_IdRedef2() { return pnd_Barcode_Lda_Pnd_Bar_Env_IdRedef2; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num() { return pnd_Barcode_Lda_Pnd_Bar_Env_Id_Num; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Start_Ind() { return pnd_Barcode_Lda_Pnd_Bar_Start_Ind; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Envelopes() { return pnd_Barcode_Lda_Pnd_Bar_Envelopes; }

    public DbsField getPnd_Barcode_Lda_Pnd_Bar_Pages_In_Env() { return pnd_Barcode_Lda_Pnd_Bar_Pages_In_Env; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Barcode_Lda = newGroupInRecord("pnd_Barcode_Lda", "#BARCODE-LDA");
        pnd_Barcode_Lda_Pnd_Barcode_Input = pnd_Barcode_Lda.newGroupInGroup("pnd_Barcode_Lda_Pnd_Barcode_Input", "#BARCODE-INPUT");
        pnd_Barcode_Lda_Pnd_Bar_New_Run = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_New_Run", "#BAR-NEW-RUN", FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Set_Last_Page = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Set_Last_Page", "#BAR-SET-LAST-PAGE", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Tiaa_Cover_Pull = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Tiaa_Cover_Pull", "#BAR-TIAA-COVER-PULL", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Cref_Cover_Pull = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Cref_Cover_Pull", "#BAR-CREF-COVER-PULL", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Stitch_Ind = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Stitch_Ind", "#BAR-STITCH-IND", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Demand_Feed = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Demand_Feed", "#BAR-DEMAND-FEED", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_1 = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Unassigned_1", "#BAR-UNASSIGNED-1", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Alert_Clear = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Alert_Clear", "#BAR-ALERT-CLEAR", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Divert = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Divert", "#BAR-DIVERT", FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Zip_Break = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Zip_Break", "#BAR-ZIP-BREAK", FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_2 = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Unassigned_2", "#BAR-UNASSIGNED-2", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_3 = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Unassigned_3", "#BAR-UNASSIGNED-3", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_4 = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Unassigned_4", "#BAR-UNASSIGNED-4", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_5 = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Unassigned_5", "#BAR-UNASSIGNED-5", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_6 = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Unassigned_6", "#BAR-UNASSIGNED-6", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_7 = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Unassigned_7", "#BAR-UNASSIGNED-7", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_8 = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Unassigned_8", "#BAR-UNASSIGNED-8", 
            FieldType.BOOLEAN);
        pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5 = pnd_Barcode_Lda_Pnd_Barcode_Input.newFieldArrayInGroup("pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5", 
            "#BAR-FUNCTION-BYTE-4-5", FieldType.BOOLEAN, new DbsArrayController(1,10));
        pnd_Barcode_Lda_Pnd_Bar_Barcode = pnd_Barcode_Lda.newFieldArrayInGroup("pnd_Barcode_Lda_Pnd_Bar_Barcode", "#BAR-BARCODE", FieldType.STRING, 1, 
            new DbsArrayController(1,17));
        pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1 = pnd_Barcode_Lda.newGroupInGroup("pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1", "Redefines", pnd_Barcode_Lda_Pnd_Bar_Barcode);
        pnd_Barcode_Lda_Pnd_Bar_End_Ind = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_End_Ind", "#BAR-END-IND", FieldType.STRING, 
            1);
        pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_1 = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_1", 
            "#BAR-MACHINE-FUNCTION-BYTE-1", FieldType.STRING, 1);
        pnd_Barcode_Lda_Pnd_Bar_End_Of_Set_Ind = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_End_Of_Set_Ind", "#BAR-END-OF-SET-IND", 
            FieldType.STRING, 1);
        pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_2 = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_2", 
            "#BAR-MACHINE-FUNCTION-BYTE-2", FieldType.STRING, 1);
        pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_3 = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_3", 
            "#BAR-MACHINE-FUNCTION-BYTE-3", FieldType.STRING, 1);
        pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_4 = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_4", 
            "#BAR-MACHINE-FUNCTION-BYTE-4", FieldType.STRING, 1);
        pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_5 = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Machine_Function_Byte_5", 
            "#BAR-MACHINE-FUNCTION-BYTE-5", FieldType.STRING, 1);
        pnd_Barcode_Lda_Pnd_Bar_Page_Integrity_Counter = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Page_Integrity_Counter", 
            "#BAR-PAGE-INTEGRITY-COUNTER", FieldType.NUMERIC, 1);
        pnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter", 
            "#BAR-ENV-INTEGRITY-COUNTER", FieldType.NUMERIC, 1);
        pnd_Barcode_Lda_Pnd_Bar_Env_Id = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Env_Id", "#BAR-ENV-ID", FieldType.STRING, 
            7);
        pnd_Barcode_Lda_Pnd_Bar_Env_IdRedef2 = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newGroupInGroup("pnd_Barcode_Lda_Pnd_Bar_Env_IdRedef2", "Redefines", 
            pnd_Barcode_Lda_Pnd_Bar_Env_Id);
        pnd_Barcode_Lda_Pnd_Bar_Env_Id_Num = pnd_Barcode_Lda_Pnd_Bar_Env_IdRedef2.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Env_Id_Num", "#BAR-ENV-ID-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Barcode_Lda_Pnd_Bar_Start_Ind = pnd_Barcode_Lda_Pnd_Bar_BarcodeRedef1.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Start_Ind", "#BAR-START-IND", 
            FieldType.STRING, 1);
        pnd_Barcode_Lda_Pnd_Bar_Envelopes = pnd_Barcode_Lda.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Envelopes", "#BAR-ENVELOPES", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Barcode_Lda_Pnd_Bar_Pages_In_Env = pnd_Barcode_Lda.newFieldInGroup("pnd_Barcode_Lda_Pnd_Bar_Pages_In_Env", "#BAR-PAGES-IN-ENV", FieldType.PACKED_DECIMAL, 
            7);

        this.setRecordName("LdaFcplbar1");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Barcode_Lda_Pnd_Bar_New_Run.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Set_Last_Page.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Tiaa_Cover_Pull.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Cref_Cover_Pull.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Stitch_Ind.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Demand_Feed.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_1.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Alert_Clear.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Divert.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Zip_Break.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_2.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_3.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_4.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_5.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_6.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_7.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Unassigned_8.setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5.getValue(1).setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5.getValue(2).setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5.getValue(3).setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5.getValue(4).setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5.getValue(5).setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5.getValue(6).setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5.getValue(7).setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5.getValue(8).setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5.getValue(9).setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Function_Byte_4_5.getValue(10).setInitialValue(false);
        pnd_Barcode_Lda_Pnd_Bar_Barcode.getValue(1).setInitialValue("*");
        pnd_Barcode_Lda_Pnd_Bar_Barcode.getValue(17).setInitialValue("*");
    }

    // Constructor
    public LdaFcplbar1() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
