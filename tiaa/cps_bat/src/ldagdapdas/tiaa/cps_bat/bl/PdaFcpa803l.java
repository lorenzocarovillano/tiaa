/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:28 PM
**        * FROM NATURAL PDA     : FCPA803L
************************************************************
**        * FILE NAME            : PdaFcpa803l.java
**        * CLASS NAME           : PdaFcpa803l
**        * INSTANCE NAME        : PdaFcpa803l
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa803l extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpa803l;
    private DbsField pnd_Fcpa803l_Pnd_Deductions_Loaded;
    private DbsField pnd_Fcpa803l_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Fcpa803l_Annt_Rsdncy_Cde;
    private DbsField pnd_Fcpa803l_Pnd_Ded_Ledgend_Table;
    private DbsField pnd_Fcpa803l_Pnd_State_Table_Top;
    private DbsField pnd_Fcpa803l_Pnd_State_Cde_Table;
    private DbsField pnd_Fcpa803l_Pnd_State_Table;

    public DbsGroup getPnd_Fcpa803l() { return pnd_Fcpa803l; }

    public DbsField getPnd_Fcpa803l_Pnd_Deductions_Loaded() { return pnd_Fcpa803l_Pnd_Deductions_Loaded; }

    public DbsField getPnd_Fcpa803l_Pymnt_Payee_Tx_Elct_Trggr() { return pnd_Fcpa803l_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getPnd_Fcpa803l_Annt_Rsdncy_Cde() { return pnd_Fcpa803l_Annt_Rsdncy_Cde; }

    public DbsField getPnd_Fcpa803l_Pnd_Ded_Ledgend_Table() { return pnd_Fcpa803l_Pnd_Ded_Ledgend_Table; }

    public DbsField getPnd_Fcpa803l_Pnd_State_Table_Top() { return pnd_Fcpa803l_Pnd_State_Table_Top; }

    public DbsField getPnd_Fcpa803l_Pnd_State_Cde_Table() { return pnd_Fcpa803l_Pnd_State_Cde_Table; }

    public DbsField getPnd_Fcpa803l_Pnd_State_Table() { return pnd_Fcpa803l_Pnd_State_Table; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpa803l = dbsRecord.newGroupInRecord("pnd_Fcpa803l", "#FCPA803L");
        pnd_Fcpa803l.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpa803l_Pnd_Deductions_Loaded = pnd_Fcpa803l.newFieldInGroup("pnd_Fcpa803l_Pnd_Deductions_Loaded", "#DEDUCTIONS-LOADED", FieldType.BOOLEAN);
        pnd_Fcpa803l_Pymnt_Payee_Tx_Elct_Trggr = pnd_Fcpa803l.newFieldInGroup("pnd_Fcpa803l_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 
            1);
        pnd_Fcpa803l_Annt_Rsdncy_Cde = pnd_Fcpa803l.newFieldInGroup("pnd_Fcpa803l_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        pnd_Fcpa803l_Pnd_Ded_Ledgend_Table = pnd_Fcpa803l.newFieldArrayInGroup("pnd_Fcpa803l_Pnd_Ded_Ledgend_Table", "#DED-LEDGEND-TABLE", FieldType.STRING, 
            34, new DbsArrayController(1,13));
        pnd_Fcpa803l_Pnd_State_Table_Top = pnd_Fcpa803l.newFieldInGroup("pnd_Fcpa803l_Pnd_State_Table_Top", "#STATE-TABLE-TOP", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Fcpa803l_Pnd_State_Cde_Table = pnd_Fcpa803l.newFieldArrayInGroup("pnd_Fcpa803l_Pnd_State_Cde_Table", "#STATE-CDE-TABLE", FieldType.STRING, 
            2, new DbsArrayController(1,57));
        pnd_Fcpa803l_Pnd_State_Table = pnd_Fcpa803l.newFieldArrayInGroup("pnd_Fcpa803l_Pnd_State_Table", "#STATE-TABLE", FieldType.STRING, 34, new DbsArrayController(1,
            57));

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa803l(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

