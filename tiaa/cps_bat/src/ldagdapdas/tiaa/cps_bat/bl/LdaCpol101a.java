/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:14 PM
**        * FROM NATURAL LDA     : CPOL101A
************************************************************
**        * FILE NAME            : LdaCpol101a.java
**        * CLASS NAME           : LdaCpol101a
**        * INSTANCE NAME        : LdaCpol101a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpol101a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Rules;
    private DbsField pnd_Rules_Rt_Long_Key;
    private DbsGroup pnd_Rules_Rt_Long_KeyRedef1;
    private DbsField pnd_Rules_Job_Name;
    private DbsField filler01;
    private DbsField pnd_Rules_Function;
    private DbsField filler02;
    private DbsField pnd_Rules_Rt_Desc1;
    private DbsGroup pnd_Rules_Rt_Desc1Redef2;
    private DbsField pnd_Rules_Record_Key;
    private DbsField filler03;
    private DbsField pnd_Rules_Prev_Function_Terminate_Code;
    private DbsField filler04;
    private DbsGroup pnd_Rules_Exp_Prev_Function_Group;
    private DbsField pnd_Rules_Expected_Previous_Function;
    private DbsField filler05;
    private DbsField pnd_Rules_Rt_Desc2;
    private DbsGroup pnd_Rules_Rt_Desc2Redef3;
    private DbsField pnd_Rules_New_Function;
    private DbsField filler06;
    private DbsField pnd_Rules_Try_Function;
    private DbsField filler07;
    private DbsField pnd_Rules_Invalid_Calling_Terminate_Code;
    private DbsField filler08;
    private DbsGroup pnd_Rules_Calling_Program_Group;
    private DbsField pnd_Rules_Calling_Program;
    private DbsField filler09;
    private DbsField filler10;
    private DbsField pnd_Rules_New_Update_Code;
    private DbsField filler11;
    private DbsField pnd_Rules_Restart_Value;
    private DbsField filler12;
    private DbsField pnd_Rules_Invalid_Previous_Terminate_Code;
    private DbsField filler13;
    private DbsGroup pnd_Rules_Invalid_Previous_Functions_Group;
    private DbsField pnd_Rules_Invalid_Previous_Functions;
    private DbsField filler14;
    private DbsField pnd_Rules_Rt_Desc3;
    private DbsGroup pnd_Rules_Rt_Desc3Redef4;
    private DbsField filler15;
    private DbsField pnd_Rules_Print_Report;
    private DbsField filler16;
    private DbsField pnd_Rules_Test;
    private DbsField filler17;

    public DbsGroup getPnd_Rules() { return pnd_Rules; }

    public DbsField getPnd_Rules_Rt_Long_Key() { return pnd_Rules_Rt_Long_Key; }

    public DbsGroup getPnd_Rules_Rt_Long_KeyRedef1() { return pnd_Rules_Rt_Long_KeyRedef1; }

    public DbsField getPnd_Rules_Job_Name() { return pnd_Rules_Job_Name; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getPnd_Rules_Function() { return pnd_Rules_Function; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getPnd_Rules_Rt_Desc1() { return pnd_Rules_Rt_Desc1; }

    public DbsGroup getPnd_Rules_Rt_Desc1Redef2() { return pnd_Rules_Rt_Desc1Redef2; }

    public DbsField getPnd_Rules_Record_Key() { return pnd_Rules_Record_Key; }

    public DbsField getFiller03() { return filler03; }

    public DbsField getPnd_Rules_Prev_Function_Terminate_Code() { return pnd_Rules_Prev_Function_Terminate_Code; }

    public DbsField getFiller04() { return filler04; }

    public DbsGroup getPnd_Rules_Exp_Prev_Function_Group() { return pnd_Rules_Exp_Prev_Function_Group; }

    public DbsField getPnd_Rules_Expected_Previous_Function() { return pnd_Rules_Expected_Previous_Function; }

    public DbsField getFiller05() { return filler05; }

    public DbsField getPnd_Rules_Rt_Desc2() { return pnd_Rules_Rt_Desc2; }

    public DbsGroup getPnd_Rules_Rt_Desc2Redef3() { return pnd_Rules_Rt_Desc2Redef3; }

    public DbsField getPnd_Rules_New_Function() { return pnd_Rules_New_Function; }

    public DbsField getFiller06() { return filler06; }

    public DbsField getPnd_Rules_Try_Function() { return pnd_Rules_Try_Function; }

    public DbsField getFiller07() { return filler07; }

    public DbsField getPnd_Rules_Invalid_Calling_Terminate_Code() { return pnd_Rules_Invalid_Calling_Terminate_Code; }

    public DbsField getFiller08() { return filler08; }

    public DbsGroup getPnd_Rules_Calling_Program_Group() { return pnd_Rules_Calling_Program_Group; }

    public DbsField getPnd_Rules_Calling_Program() { return pnd_Rules_Calling_Program; }

    public DbsField getFiller09() { return filler09; }

    public DbsField getFiller10() { return filler10; }

    public DbsField getPnd_Rules_New_Update_Code() { return pnd_Rules_New_Update_Code; }

    public DbsField getFiller11() { return filler11; }

    public DbsField getPnd_Rules_Restart_Value() { return pnd_Rules_Restart_Value; }

    public DbsField getFiller12() { return filler12; }

    public DbsField getPnd_Rules_Invalid_Previous_Terminate_Code() { return pnd_Rules_Invalid_Previous_Terminate_Code; }

    public DbsField getFiller13() { return filler13; }

    public DbsGroup getPnd_Rules_Invalid_Previous_Functions_Group() { return pnd_Rules_Invalid_Previous_Functions_Group; }

    public DbsField getPnd_Rules_Invalid_Previous_Functions() { return pnd_Rules_Invalid_Previous_Functions; }

    public DbsField getFiller14() { return filler14; }

    public DbsField getPnd_Rules_Rt_Desc3() { return pnd_Rules_Rt_Desc3; }

    public DbsGroup getPnd_Rules_Rt_Desc3Redef4() { return pnd_Rules_Rt_Desc3Redef4; }

    public DbsField getFiller15() { return filler15; }

    public DbsField getPnd_Rules_Print_Report() { return pnd_Rules_Print_Report; }

    public DbsField getFiller16() { return filler16; }

    public DbsField getPnd_Rules_Test() { return pnd_Rules_Test; }

    public DbsField getFiller17() { return filler17; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Rules = newGroupInRecord("pnd_Rules", "#RULES");
        pnd_Rules_Rt_Long_Key = pnd_Rules.newFieldInGroup("pnd_Rules_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40);
        pnd_Rules_Rt_Long_KeyRedef1 = pnd_Rules.newGroupInGroup("pnd_Rules_Rt_Long_KeyRedef1", "Redefines", pnd_Rules_Rt_Long_Key);
        pnd_Rules_Job_Name = pnd_Rules_Rt_Long_KeyRedef1.newFieldInGroup("pnd_Rules_Job_Name", "JOB-NAME", FieldType.STRING, 8);
        filler01 = pnd_Rules_Rt_Long_KeyRedef1.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 2);
        pnd_Rules_Function = pnd_Rules_Rt_Long_KeyRedef1.newFieldInGroup("pnd_Rules_Function", "FUNCTION", FieldType.STRING, 20);
        filler02 = pnd_Rules_Rt_Long_KeyRedef1.newFieldInGroup("filler02", "FILLER", FieldType.STRING, 10);
        pnd_Rules_Rt_Desc1 = pnd_Rules.newFieldInGroup("pnd_Rules_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250);
        pnd_Rules_Rt_Desc1Redef2 = pnd_Rules.newGroupInGroup("pnd_Rules_Rt_Desc1Redef2", "Redefines", pnd_Rules_Rt_Desc1);
        pnd_Rules_Record_Key = pnd_Rules_Rt_Desc1Redef2.newFieldInGroup("pnd_Rules_Record_Key", "RECORD-KEY", FieldType.STRING, 20);
        filler03 = pnd_Rules_Rt_Desc1Redef2.newFieldInGroup("filler03", "FILLER", FieldType.STRING, 1);
        pnd_Rules_Prev_Function_Terminate_Code = pnd_Rules_Rt_Desc1Redef2.newFieldInGroup("pnd_Rules_Prev_Function_Terminate_Code", "PREV-FUNCTION-TERMINATE-CODE", 
            FieldType.NUMERIC, 4);
        filler04 = pnd_Rules_Rt_Desc1Redef2.newFieldInGroup("filler04", "FILLER", FieldType.STRING, 1);
        pnd_Rules_Exp_Prev_Function_Group = pnd_Rules_Rt_Desc1Redef2.newGroupArrayInGroup("pnd_Rules_Exp_Prev_Function_Group", "EXP-PREV-FUNCTION-GROUP", 
            new DbsArrayController(1,10));
        pnd_Rules_Expected_Previous_Function = pnd_Rules_Exp_Prev_Function_Group.newFieldInGroup("pnd_Rules_Expected_Previous_Function", "EXPECTED-PREVIOUS-FUNCTION", 
            FieldType.STRING, 20);
        filler05 = pnd_Rules_Exp_Prev_Function_Group.newFieldInGroup("filler05", "FILLER", FieldType.STRING, 1);
        pnd_Rules_Rt_Desc2 = pnd_Rules.newFieldInGroup("pnd_Rules_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250);
        pnd_Rules_Rt_Desc2Redef3 = pnd_Rules.newGroupInGroup("pnd_Rules_Rt_Desc2Redef3", "Redefines", pnd_Rules_Rt_Desc2);
        pnd_Rules_New_Function = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("pnd_Rules_New_Function", "NEW-FUNCTION", FieldType.STRING, 20);
        filler06 = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("filler06", "FILLER", FieldType.STRING, 1);
        pnd_Rules_Try_Function = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("pnd_Rules_Try_Function", "TRY-FUNCTION", FieldType.STRING, 20);
        filler07 = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("filler07", "FILLER", FieldType.STRING, 1);
        pnd_Rules_Invalid_Calling_Terminate_Code = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("pnd_Rules_Invalid_Calling_Terminate_Code", "INVALID-CALLING-TERMINATE-CODE", 
            FieldType.NUMERIC, 4);
        filler08 = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("filler08", "FILLER", FieldType.STRING, 1);
        pnd_Rules_Calling_Program_Group = pnd_Rules_Rt_Desc2Redef3.newGroupArrayInGroup("pnd_Rules_Calling_Program_Group", "CALLING-PROGRAM-GROUP", new 
            DbsArrayController(1,3));
        pnd_Rules_Calling_Program = pnd_Rules_Calling_Program_Group.newFieldInGroup("pnd_Rules_Calling_Program", "CALLING-PROGRAM", FieldType.STRING, 
            8);
        filler09 = pnd_Rules_Calling_Program_Group.newFieldInGroup("filler09", "FILLER", FieldType.STRING, 1);
        filler10 = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("filler10", "FILLER", FieldType.STRING, 1);
        pnd_Rules_New_Update_Code = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("pnd_Rules_New_Update_Code", "NEW-UPDATE-CODE", FieldType.STRING, 9);
        filler11 = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("filler11", "FILLER", FieldType.STRING, 1);
        pnd_Rules_Restart_Value = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("pnd_Rules_Restart_Value", "RESTART-VALUE", FieldType.STRING, 8);
        filler12 = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("filler12", "FILLER", FieldType.STRING, 3);
        pnd_Rules_Invalid_Previous_Terminate_Code = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("pnd_Rules_Invalid_Previous_Terminate_Code", "INVALID-PREVIOUS-TERMINATE-CODE", 
            FieldType.NUMERIC, 4);
        filler13 = pnd_Rules_Rt_Desc2Redef3.newFieldInGroup("filler13", "FILLER", FieldType.STRING, 1);
        pnd_Rules_Invalid_Previous_Functions_Group = pnd_Rules_Rt_Desc2Redef3.newGroupArrayInGroup("pnd_Rules_Invalid_Previous_Functions_Group", "INVALID-PREVIOUS-FUNCTIONS-GROUP", 
            new DbsArrayController(1,3));
        pnd_Rules_Invalid_Previous_Functions = pnd_Rules_Invalid_Previous_Functions_Group.newFieldInGroup("pnd_Rules_Invalid_Previous_Functions", "INVALID-PREVIOUS-FUNCTIONS", 
            FieldType.STRING, 20);
        filler14 = pnd_Rules_Invalid_Previous_Functions_Group.newFieldInGroup("filler14", "FILLER", FieldType.STRING, 1);
        pnd_Rules_Rt_Desc3 = pnd_Rules.newFieldInGroup("pnd_Rules_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250);
        pnd_Rules_Rt_Desc3Redef4 = pnd_Rules.newGroupInGroup("pnd_Rules_Rt_Desc3Redef4", "Redefines", pnd_Rules_Rt_Desc3);
        filler15 = pnd_Rules_Rt_Desc3Redef4.newFieldInGroup("filler15", "FILLER", FieldType.STRING, 14);
        pnd_Rules_Print_Report = pnd_Rules_Rt_Desc3Redef4.newFieldInGroup("pnd_Rules_Print_Report", "PRINT-REPORT", FieldType.STRING, 3);
        filler16 = pnd_Rules_Rt_Desc3Redef4.newFieldInGroup("filler16", "FILLER", FieldType.STRING, 7);
        pnd_Rules_Test = pnd_Rules_Rt_Desc3Redef4.newFieldInGroup("pnd_Rules_Test", "TEST", FieldType.STRING, 4);
        filler17 = pnd_Rules_Rt_Desc3Redef4.newFieldInGroup("filler17", "FILLER", FieldType.STRING, 36);

        this.setRecordName("LdaCpol101a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCpol101a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
