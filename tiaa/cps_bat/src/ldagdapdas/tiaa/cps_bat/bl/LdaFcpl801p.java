/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:14 PM
**        * FROM NATURAL LDA     : FCPL801P
************************************************************
**        * FILE NAME            : LdaFcpl801p.java
**        * CLASS NAME           : LdaFcpl801p
**        * INSTANCE NAME        : LdaFcpl801p
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl801p extends DbsRecord
{
    // Properties
    private DbsField pnd_Valid_Pend_Codes;

    public DbsField getPnd_Valid_Pend_Codes() { return pnd_Valid_Pend_Codes; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Valid_Pend_Codes = newFieldArrayInRecord("pnd_Valid_Pend_Codes", "#VALID-PEND-CODES", FieldType.STRING, 1, new DbsArrayController(1,11));

        this.setRecordName("LdaFcpl801p");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Valid_Pend_Codes.getValue(1).setInitialValue("A");
        pnd_Valid_Pend_Codes.getValue(2).setInitialValue("C");
        pnd_Valid_Pend_Codes.getValue(3).setInitialValue("E");
        pnd_Valid_Pend_Codes.getValue(4).setInitialValue("G");
        pnd_Valid_Pend_Codes.getValue(5).setInitialValue("P");
        pnd_Valid_Pend_Codes.getValue(6).setInitialValue("Q");
        pnd_Valid_Pend_Codes.getValue(7).setInitialValue("S");
        pnd_Valid_Pend_Codes.getValue(8).setInitialValue("T");
        pnd_Valid_Pend_Codes.getValue(9).setInitialValue("M");
        pnd_Valid_Pend_Codes.getValue(10).setInitialValue("1");
        pnd_Valid_Pend_Codes.getValue(11).setInitialValue("2");
    }

    // Constructor
    public LdaFcpl801p() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
