/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:15 PM
**        * FROM NATURAL LDA     : FCPL803T
************************************************************
**        * FILE NAME            : LdaFcpl803t.java
**        * CLASS NAME           : LdaFcpl803t
**        * INSTANCE NAME        : LdaFcpl803t
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl803t extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl803t;
    private DbsField pnd_Fcpl803t_Pnd_Cntrct_Pa_Select;
    private DbsField pnd_Fcpl803t_Pnd_Cntrct_Spia;
    private DbsField pnd_Fcpl803t_Pnd_Cntrct_Tiaa_Hdr;
    private DbsField pnd_Fcpl803t_Pnd_Cntrct_Cref_Hdr;
    private DbsField pnd_Fcpl803t_Pnd_Pymnt_Hdr;
    private DbsField pnd_Fcpl803t_Pnd_Dpi_Hdr;
    private DbsField pnd_Fcpl803t_Pnd_Ded_Hdr;
    private DbsField pnd_Fcpl803t_Pnd_Net_Hdr;
    private DbsField pnd_Fcpl803t_Pnd_Blank_Hdr_Line;

    public DbsGroup getPnd_Fcpl803t() { return pnd_Fcpl803t; }

    public DbsField getPnd_Fcpl803t_Pnd_Cntrct_Pa_Select() { return pnd_Fcpl803t_Pnd_Cntrct_Pa_Select; }

    public DbsField getPnd_Fcpl803t_Pnd_Cntrct_Spia() { return pnd_Fcpl803t_Pnd_Cntrct_Spia; }

    public DbsField getPnd_Fcpl803t_Pnd_Cntrct_Tiaa_Hdr() { return pnd_Fcpl803t_Pnd_Cntrct_Tiaa_Hdr; }

    public DbsField getPnd_Fcpl803t_Pnd_Cntrct_Cref_Hdr() { return pnd_Fcpl803t_Pnd_Cntrct_Cref_Hdr; }

    public DbsField getPnd_Fcpl803t_Pnd_Pymnt_Hdr() { return pnd_Fcpl803t_Pnd_Pymnt_Hdr; }

    public DbsField getPnd_Fcpl803t_Pnd_Dpi_Hdr() { return pnd_Fcpl803t_Pnd_Dpi_Hdr; }

    public DbsField getPnd_Fcpl803t_Pnd_Ded_Hdr() { return pnd_Fcpl803t_Pnd_Ded_Hdr; }

    public DbsField getPnd_Fcpl803t_Pnd_Net_Hdr() { return pnd_Fcpl803t_Pnd_Net_Hdr; }

    public DbsField getPnd_Fcpl803t_Pnd_Blank_Hdr_Line() { return pnd_Fcpl803t_Pnd_Blank_Hdr_Line; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl803t = newGroupInRecord("pnd_Fcpl803t", "#FCPL803T");
        pnd_Fcpl803t_Pnd_Cntrct_Pa_Select = pnd_Fcpl803t.newFieldArrayInGroup("pnd_Fcpl803t_Pnd_Cntrct_Pa_Select", "#CNTRCT-PA-SELECT", FieldType.STRING, 
            17, new DbsArrayController(1,2));
        pnd_Fcpl803t_Pnd_Cntrct_Spia = pnd_Fcpl803t.newFieldArrayInGroup("pnd_Fcpl803t_Pnd_Cntrct_Spia", "#CNTRCT-SPIA", FieldType.STRING, 17, new DbsArrayController(1,
            2));
        pnd_Fcpl803t_Pnd_Cntrct_Tiaa_Hdr = pnd_Fcpl803t.newFieldArrayInGroup("pnd_Fcpl803t_Pnd_Cntrct_Tiaa_Hdr", "#CNTRCT-TIAA-HDR", FieldType.STRING, 
            13, new DbsArrayController(1,2));
        pnd_Fcpl803t_Pnd_Cntrct_Cref_Hdr = pnd_Fcpl803t.newFieldArrayInGroup("pnd_Fcpl803t_Pnd_Cntrct_Cref_Hdr", "#CNTRCT-CREF-HDR", FieldType.STRING, 
            13, new DbsArrayController(1,2));
        pnd_Fcpl803t_Pnd_Pymnt_Hdr = pnd_Fcpl803t.newFieldInGroup("pnd_Fcpl803t_Pnd_Pymnt_Hdr", "#PYMNT-HDR", FieldType.STRING, 18);
        pnd_Fcpl803t_Pnd_Dpi_Hdr = pnd_Fcpl803t.newFieldInGroup("pnd_Fcpl803t_Pnd_Dpi_Hdr", "#DPI-HDR", FieldType.STRING, 19);
        pnd_Fcpl803t_Pnd_Ded_Hdr = pnd_Fcpl803t.newFieldInGroup("pnd_Fcpl803t_Pnd_Ded_Hdr", "#DED-HDR", FieldType.STRING, 16);
        pnd_Fcpl803t_Pnd_Net_Hdr = pnd_Fcpl803t.newFieldArrayInGroup("pnd_Fcpl803t_Pnd_Net_Hdr", "#NET-HDR", FieldType.STRING, 17, new DbsArrayController(1,
            2));
        pnd_Fcpl803t_Pnd_Blank_Hdr_Line = pnd_Fcpl803t.newFieldArrayInGroup("pnd_Fcpl803t_Pnd_Blank_Hdr_Line", "#BLANK-HDR-LINE", FieldType.STRING, 2, 
            new DbsArrayController(1,3));

        this.setRecordName("LdaFcpl803t");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl803t_Pnd_Cntrct_Pa_Select.getValue(1).setInitialValue("-4PA SELECT");
        pnd_Fcpl803t_Pnd_Cntrct_Pa_Select.getValue(2).setInitialValue("4Number");
        pnd_Fcpl803t_Pnd_Cntrct_Spia.getValue(1).setInitialValue("-4SPIA");
        pnd_Fcpl803t_Pnd_Cntrct_Spia.getValue(2).setInitialValue("4Contract");
        pnd_Fcpl803t_Pnd_Cntrct_Tiaa_Hdr.getValue(1).setInitialValue("-4TIAA");
        pnd_Fcpl803t_Pnd_Cntrct_Tiaa_Hdr.getValue(2).setInitialValue("4Contract");
        pnd_Fcpl803t_Pnd_Cntrct_Cref_Hdr.getValue(1).setInitialValue("-4CREF");
        pnd_Fcpl803t_Pnd_Cntrct_Cref_Hdr.getValue(2).setInitialValue("4Certificate");
        pnd_Fcpl803t_Pnd_Pymnt_Hdr.setInitialValue(" 4         Payment");
        pnd_Fcpl803t_Pnd_Dpi_Hdr.setInitialValue(" 4         Interest");
        pnd_Fcpl803t_Pnd_Ded_Hdr.setInitialValue(" 4    Deductions");
        pnd_Fcpl803t_Pnd_Net_Hdr.getValue(1).setInitialValue("14        Net");
        pnd_Fcpl803t_Pnd_Net_Hdr.getValue(2).setInitialValue("4        Payment");
        pnd_Fcpl803t_Pnd_Blank_Hdr_Line.getValue(1).setInitialValue("14");
        pnd_Fcpl803t_Pnd_Blank_Hdr_Line.getValue(2).setInitialValue("4");
        pnd_Fcpl803t_Pnd_Blank_Hdr_Line.getValue(3).setInitialValue("15");
    }

    // Constructor
    public LdaFcpl803t() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
