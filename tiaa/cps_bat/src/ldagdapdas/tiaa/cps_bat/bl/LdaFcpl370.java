/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:08 PM
**        * FROM NATURAL LDA     : FCPL370
************************************************************
**        * FILE NAME            : LdaFcpl370.java
**        * CLASS NAME           : LdaFcpl370
**        * INSTANCE NAME        : LdaFcpl370
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl370 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Ws_Header_Record;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Qlfied_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sps_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Annot_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cmbne_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Ftre_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cref_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Grp;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Header_Record_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cycle_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Eft_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pnd_Ws_Header_Record_Pymnt_Prcss_Seq_NbrRedef1;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Ws_Header_Record_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Commuted_Value;
    private DbsField filler01;
    private DbsField pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Insurance_Option;
    private DbsField pnd_Ws_Header_Record_Cntrct_Life_Contingency;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Filler;
    private DbsGroup pnd_Ws_Name_N_Address;
    private DbsGroup pnd_Ws_Name_N_Address_Ph_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_Last_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_First_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_Middle_Name;
    private DbsGroup pnd_Ws_Occurs;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct;
    private DbsGroup pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Side;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fed_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dci_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Ind;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Valuat_Period;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Exp_Amt;
    private DbsField pnd_Ws_Occurs_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Occurs_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Deductions;
    private DbsGroup pnd_Ws_Occurs_Pnd_Ws_Deductions;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Amt;
    private DbsGroup pnd_Ws_Occurs_Pnd_Pyhdr_Data;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start;
    private DbsGroup pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_StartRedef2;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End;

    public DbsGroup getPnd_Ws_Header_Record() { return pnd_Ws_Header_Record; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Ppcn_Nbr() { return pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Payee_Cde() { return pnd_Ws_Header_Record_Cntrct_Payee_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Invrse_Dte() { return pnd_Ws_Header_Record_Cntrct_Invrse_Dte; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde() { return pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Crrncy_Cde() { return pnd_Ws_Header_Record_Cntrct_Crrncy_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Orgn_Cde() { return pnd_Ws_Header_Record_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Qlfied_Cde() { return pnd_Ws_Header_Record_Cntrct_Qlfied_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind() { return pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind() { return pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Sps_Cde() { return pnd_Ws_Header_Record_Cntrct_Sps_Cde; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct() { return pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Stats_Cde() { return pnd_Ws_Header_Record_Pymnt_Stats_Cde; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Annot_Ind() { return pnd_Ws_Header_Record_Pymnt_Annot_Ind; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Cmbne_Ind() { return pnd_Ws_Header_Record_Pymnt_Cmbne_Ind; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Ftre_Ind() { return pnd_Ws_Header_Record_Pymnt_Ftre_Ind; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr() { return pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr() { return pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde() { return pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind() { return pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind; }

    public DbsField getPnd_Ws_Header_Record_Annt_Soc_Sec_Ind() { return pnd_Ws_Header_Record_Annt_Soc_Sec_Ind; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind() { return pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte() { return pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Rqst_Dte() { return pnd_Ws_Header_Record_Cntrct_Rqst_Dte; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme() { return pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr() { return pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Type_Cde() { return pnd_Ws_Header_Record_Cntrct_Type_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Lob_Cde() { return pnd_Ws_Header_Record_Cntrct_Lob_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde() { return pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde() { return pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Cref_Nbr() { return pnd_Ws_Header_Record_Cntrct_Cref_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Option_Cde() { return pnd_Ws_Header_Record_Cntrct_Option_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Mode_Cde() { return pnd_Ws_Header_Record_Cntrct_Mode_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde() { return pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde() { return pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde() { return pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Hold_Cde() { return pnd_Ws_Header_Record_Cntrct_Hold_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Hold_Grp() { return pnd_Ws_Header_Record_Cntrct_Hold_Grp; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr() { return pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr() { return pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr() { return pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr() { return pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr() { return pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr() { return pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr() { return pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr() { return pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr() { return pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr() { return pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Annt_Soc_Sec_Nbr() { return pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Annt_Ctznshp_Cde() { return pnd_Ws_Header_Record_Annt_Ctznshp_Cde; }

    public DbsField getPnd_Ws_Header_Record_Annt_Rsdncy_Cde() { return pnd_Ws_Header_Record_Annt_Rsdncy_Cde; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Split_Cde() { return pnd_Ws_Header_Record_Pymnt_Split_Cde; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde() { return pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Check_Dte() { return pnd_Ws_Header_Record_Pymnt_Check_Dte; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Cycle_Dte() { return pnd_Ws_Header_Record_Pymnt_Cycle_Dte; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Eft_Dte() { return pnd_Ws_Header_Record_Pymnt_Eft_Dte; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Rqst_Pct() { return pnd_Ws_Header_Record_Pymnt_Rqst_Pct; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Rqst_Amt() { return pnd_Ws_Header_Record_Pymnt_Rqst_Amt; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Check_Nbr() { return pnd_Ws_Header_Record_Pymnt_Check_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr() { return pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPnd_Ws_Header_Record_Pymnt_Prcss_Seq_NbrRedef1() { return pnd_Ws_Header_Record_Pymnt_Prcss_Seq_NbrRedef1; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num() { return pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Instmt_Nbr() { return pnd_Ws_Header_Record_Pymnt_Instmt_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr() { return pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Check_Amt() { return pnd_Ws_Header_Record_Pymnt_Check_Amt; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Settlmnt_Dte() { return pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr() { return pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr() { return pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Cmbn_Nbr() { return pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Hold_Tme() { return pnd_Ws_Header_Record_Cntrct_Hold_Tme; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind() { return pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Acctg_Dte() { return pnd_Ws_Header_Record_Pymnt_Acctg_Dte; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time() { return pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Intrfce_Dte() { return pnd_Ws_Header_Record_Pymnt_Intrfce_Dte; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPnd_Ws_Header_Record_Pymnt_Commuted_Value() { return pnd_Ws_Header_Record_Pymnt_Commuted_Value; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getPnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte() { return pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Annty_Ins_Type() { return pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Annty_Type_Cde() { return pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Insurance_Option() { return pnd_Ws_Header_Record_Cntrct_Insurance_Option; }

    public DbsField getPnd_Ws_Header_Record_Cntrct_Life_Contingency() { return pnd_Ws_Header_Record_Cntrct_Life_Contingency; }

    public DbsField getPnd_Ws_Header_Record_Pnd_Ws_Header_Filler() { return pnd_Ws_Header_Record_Pnd_Ws_Header_Filler; }

    public DbsGroup getPnd_Ws_Name_N_Address() { return pnd_Ws_Name_N_Address; }

    public DbsGroup getPnd_Ws_Name_N_Address_Ph_Name() { return pnd_Ws_Name_N_Address_Ph_Name; }

    public DbsField getPnd_Ws_Name_N_Address_Ph_Last_Name() { return pnd_Ws_Name_N_Address_Ph_Last_Name; }

    public DbsField getPnd_Ws_Name_N_Address_Ph_First_Name() { return pnd_Ws_Name_N_Address_Ph_First_Name; }

    public DbsField getPnd_Ws_Name_N_Address_Ph_Middle_Name() { return pnd_Ws_Name_N_Address_Ph_Middle_Name; }

    public DbsGroup getPnd_Ws_Occurs() { return pnd_Ws_Occurs; }

    public DbsField getPnd_Ws_Occurs_Pnd_Cntr_Inv_Acct() { return pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct; }

    public DbsGroup getPnd_Ws_Occurs_Pnd_Ws_Occurs_Entry() { return pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry; }

    public DbsField getPnd_Ws_Occurs_Pnd_Ws_Side() { return pnd_Ws_Occurs_Pnd_Ws_Side; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Cde() { return pnd_Ws_Occurs_Inv_Acct_Cde; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Unit_Qty() { return pnd_Ws_Occurs_Inv_Acct_Unit_Qty; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Unit_Value() { return pnd_Ws_Occurs_Inv_Acct_Unit_Value; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Settl_Amt() { return pnd_Ws_Occurs_Inv_Acct_Settl_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Fed_Cde() { return pnd_Ws_Occurs_Inv_Acct_Fed_Cde; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_State_Cde() { return pnd_Ws_Occurs_Inv_Acct_State_Cde; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Local_Cde() { return pnd_Ws_Occurs_Inv_Acct_Local_Cde; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt() { return pnd_Ws_Occurs_Inv_Acct_Ivc_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Dci_Amt() { return pnd_Ws_Occurs_Inv_Acct_Dci_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt() { return pnd_Ws_Occurs_Inv_Acct_Dpi_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt() { return pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_End_Accum_Amt() { return pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt() { return pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt() { return pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Ivc_Ind() { return pnd_Ws_Occurs_Inv_Acct_Ivc_Ind; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt() { return pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Valuat_Period() { return pnd_Ws_Occurs_Inv_Acct_Valuat_Period; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt() { return pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt() { return pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt() { return pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt; }

    public DbsField getPnd_Ws_Occurs_Inv_Acct_Exp_Amt() { return pnd_Ws_Occurs_Inv_Acct_Exp_Amt; }

    public DbsField getPnd_Ws_Occurs_Cntrct_Ppcn_Nbr() { return pnd_Ws_Occurs_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Ws_Occurs_Cntrct_Payee_Cde() { return pnd_Ws_Occurs_Cntrct_Payee_Cde; }

    public DbsField getPnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind() { return pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind; }

    public DbsField getPnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind() { return pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getPnd_Ws_Occurs_Cntrct_Option_Cde() { return pnd_Ws_Occurs_Cntrct_Option_Cde; }

    public DbsField getPnd_Ws_Occurs_Cntrct_Mode_Cde() { return pnd_Ws_Occurs_Cntrct_Mode_Cde; }

    public DbsField getPnd_Ws_Occurs_Pnd_Cntr_Deductions() { return pnd_Ws_Occurs_Pnd_Cntr_Deductions; }

    public DbsGroup getPnd_Ws_Occurs_Pnd_Ws_Deductions() { return pnd_Ws_Occurs_Pnd_Ws_Deductions; }

    public DbsField getPnd_Ws_Occurs_Pymnt_Ded_Cde() { return pnd_Ws_Occurs_Pymnt_Ded_Cde; }

    public DbsField getPnd_Ws_Occurs_Pymnt_Ded_Payee_Cde() { return pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde; }

    public DbsField getPnd_Ws_Occurs_Pymnt_Ded_Amt() { return pnd_Ws_Occurs_Pymnt_Ded_Amt; }

    public DbsGroup getPnd_Ws_Occurs_Pnd_Pyhdr_Data() { return pnd_Ws_Occurs_Pnd_Pyhdr_Data; }

    public DbsField getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start() { return pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start; }

    public DbsGroup getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_StartRedef2() { return pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_StartRedef2; }

    public DbsField getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte() { return pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte; }

    public DbsField getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End() { return pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Ws_Header_Record = newGroupInRecord("pnd_Ws_Header_Record", "#WS-HEADER-RECORD");
        pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Header_Record_Cntrct_Payee_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cntrct_Invrse_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Crrncy_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Cntrct_Orgn_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Cntrct_Qlfied_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sps_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct", "PYMNT-RQST-RMNDR-PCT", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Stats_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Annot_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Cmbne_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Ftre_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr", "PYMNT-PAYEE-NA-ADDR-TRGGR", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde", "PYMNT-INST-REP-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind", "CNTRCT-DVDND-PAYEE-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 
            1);
        pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte", "CNTRCT-RQST-SETTL-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme", "CNTRCT-RQST-SETTL-TME", 
            FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Ws_Header_Record_Cntrct_Type_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Cntrct_Lob_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Cntrct_Cref_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Header_Record_Cntrct_Option_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Header_Record_Cntrct_Mode_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", 
            FieldType.STRING, 5);
        pnd_Ws_Header_Record_Cntrct_Hold_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cntrct_Hold_Grp = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 
            3);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr", "CNTRCT-DA-TIAA-1-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr", "CNTRCT-DA-TIAA-2-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr", "CNTRCT-DA-TIAA-3-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr", "CNTRCT-DA-TIAA-4-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr", "CNTRCT-DA-TIAA-5-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr", "CNTRCT-DA-CREF-3-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr", "CNTRCT-DA-CREF-4-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr", "CNTRCT-DA-CREF-5-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Ws_Header_Record_Annt_Ctznshp_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Header_Record_Annt_Rsdncy_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Pymnt_Split_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", 
            FieldType.STRING, 6);
        pnd_Ws_Header_Record_Pymnt_Check_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Cycle_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Eft_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Rqst_Pct = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Header_Record_Pymnt_Rqst_Amt = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Ws_Header_Record_Pymnt_Check_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_NbrRedef1 = pnd_Ws_Header_Record.newGroupInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_NbrRedef1", "Redefines", 
            pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num", 
            "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Instmt_Nbr = pnd_Ws_Header_Record_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Instmt_Nbr", 
            "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Check_Amt = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr", "CNTRCT-EC-OPRTR-ID-NBR", 
            FieldType.STRING, 7);
        pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 
            14);
        pnd_Ws_Header_Record_Cntrct_Hold_Tme = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Acctg_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Ws_Header_Record_Pymnt_Intrfce_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Commuted_Value = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Commuted_Value", "PYMNT-COMMUTED-VALUE", 
            FieldType.STRING, 2);
        filler01 = pnd_Ws_Header_Record.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Insurance_Option = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Life_Contingency = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Filler = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Filler", "#WS-HEADER-FILLER", 
            FieldType.STRING, 117);

        pnd_Ws_Name_N_Address = newGroupInRecord("pnd_Ws_Name_N_Address", "#WS-NAME-N-ADDRESS");
        pnd_Ws_Name_N_Address_Ph_Name = pnd_Ws_Name_N_Address.newGroupInGroup("pnd_Ws_Name_N_Address_Ph_Name", "PH-NAME");
        pnd_Ws_Name_N_Address_Ph_Last_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Ws_Name_N_Address_Ph_First_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 
            10);
        pnd_Ws_Name_N_Address_Ph_Middle_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_Middle_Name", "PH-MIDDLE-NAME", 
            FieldType.STRING, 12);

        pnd_Ws_Occurs = newGroupInRecord("pnd_Ws_Occurs", "#WS-OCCURS");
        pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct = pnd_Ws_Occurs.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct", "#CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry = pnd_Ws_Occurs.newGroupArrayInGroup("pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry", "#WS-OCCURS-ENTRY", new DbsArrayController(1,
            40));
        pnd_Ws_Occurs_Pnd_Ws_Side = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Side", "#WS-SIDE", FieldType.NUMERIC, 1);
        pnd_Ws_Occurs_Inv_Acct_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Occurs_Inv_Acct_Unit_Qty = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9,3);
        pnd_Ws_Occurs_Inv_Acct_Unit_Value = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", 
            FieldType.PACKED_DECIMAL, 9,4);
        pnd_Ws_Occurs_Inv_Acct_Settl_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Ws_Occurs_Inv_Acct_Fed_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_State_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Local_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Ws_Occurs_Inv_Acct_Dci_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Ws_Occurs_Inv_Acct_Dpi_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Ws_Occurs_Inv_Acct_Valuat_Period = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Ws_Occurs_Inv_Acct_Exp_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Ws_Occurs_Cntrct_Ppcn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Occurs_Cntrct_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Option_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Occurs_Cntrct_Mode_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Cntr_Deductions = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Deductions", "#CNTR-DEDUCTIONS", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Occurs_Pnd_Ws_Deductions = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newGroupArrayInGroup("pnd_Ws_Occurs_Pnd_Ws_Deductions", "#WS-DEDUCTIONS", 
            new DbsArrayController(1,10));
        pnd_Ws_Occurs_Pymnt_Ded_Cde = pnd_Ws_Occurs_Pnd_Ws_Deductions.newFieldInGroup("pnd_Ws_Occurs_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Deductions.newFieldInGroup("pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8);
        pnd_Ws_Occurs_Pymnt_Ded_Amt = pnd_Ws_Occurs_Pnd_Ws_Deductions.newFieldInGroup("pnd_Ws_Occurs_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Ws_Occurs_Pnd_Pyhdr_Data = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newGroupInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Data", "#PYHDR-DATA");
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-START", FieldType.DATE);
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_StartRedef2 = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_StartRedef2", 
            "Redefines", pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start);
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte = pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_StartRedef2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte", 
            "#PYHDR-PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End", 
            "#PYHDR-PYMNT-SETTLMNT-DTE-END", FieldType.DATE);

        this.setRecordName("LdaFcpl370");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl370() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
