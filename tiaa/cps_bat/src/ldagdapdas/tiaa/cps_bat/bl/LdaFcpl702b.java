/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:13 PM
**        * FROM NATURAL LDA     : FCPL702B
************************************************************
**        * FILE NAME            : LdaFcpl702b.java
**        * CLASS NAME           : LdaFcpl702b
**        * INSTANCE NAME        : LdaFcpl702b
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl702b extends DbsRecord
{
    // Properties
    private DbsGroup payout;
    private DbsField payout_Cntrct_Ppcn_Nbr;
    private DbsField payout_Cntrct_Payee_Cde;
    private DbsField payout_Cntrct_Invrse_Dte;
    private DbsField payout_Cntrct_Check_Crrncy_Cde;
    private DbsField payout_Cntrct_Orgn_Cde;
    private DbsField payout_Cntrct_Unq_Id_Nbr;
    private DbsField payout_Cntrct_Type_Cde;
    private DbsField payout_Cntrct_Lob_Cde;
    private DbsField payout_Cntrct_Option_Cde;
    private DbsField payout_Cntrct_Mode_Cde;
    private DbsField payout_Cntrct_Roll_Dest_Cde;
    private DbsField payout_Cntrct_Settl_Amt;
    private DbsField payout_Cntrct_Cmbn_Nbr;
    private DbsField payout_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField payout_Ph_Last_Name;
    private DbsField payout_Ph_First_Name;
    private DbsField payout_Ph_Middle_Name;
    private DbsField payout_Annt_Soc_Sec_Ind;
    private DbsField payout_Annt_Soc_Sec_Nbr;
    private DbsField payout_Pymnt_Stats_Cde;
    private DbsField payout_Pymnt_Pay_Type_Req_Ind;
    private DbsField payout_Pymnt_Check_Dte;
    private DbsField payout_Pymnt_Eft_Dte;
    private DbsField payout_Pymnt_Check_Nbr;
    private DbsField payout_Pymnt_Prcss_Seq_Nbr;
    private DbsField payout_Pymnt_Check_Amt;
    private DbsField payout_Pymnt_Nbr;
    private DbsField payout_Pymnt_Nme;
    private DbsField payout_Pymnt_Addr_Line1_Txt;
    private DbsField payout_Pymnt_Addr_Line2_Txt;
    private DbsField payout_Pymnt_Addr_Line3_Txt;
    private DbsField payout_Pymnt_Addr_Line4_Txt;
    private DbsField payout_Pymnt_Addr_Line5_Txt;
    private DbsField payout_Pymnt_Addr_Line6_Txt;
    private DbsField payout_Pymnt_Addr_Zip_Cde;
    private DbsField payout_Pymnt_Postl_Data;
    private DbsField payout_Pymnt_Eft_Transit_Id;
    private DbsField payout_Pymnt_Eft_Acct_Nbr;
    private DbsField payout_Pymnt_Chk_Sav_Ind;

    public DbsGroup getPayout() { return payout; }

    public DbsField getPayout_Cntrct_Ppcn_Nbr() { return payout_Cntrct_Ppcn_Nbr; }

    public DbsField getPayout_Cntrct_Payee_Cde() { return payout_Cntrct_Payee_Cde; }

    public DbsField getPayout_Cntrct_Invrse_Dte() { return payout_Cntrct_Invrse_Dte; }

    public DbsField getPayout_Cntrct_Check_Crrncy_Cde() { return payout_Cntrct_Check_Crrncy_Cde; }

    public DbsField getPayout_Cntrct_Orgn_Cde() { return payout_Cntrct_Orgn_Cde; }

    public DbsField getPayout_Cntrct_Unq_Id_Nbr() { return payout_Cntrct_Unq_Id_Nbr; }

    public DbsField getPayout_Cntrct_Type_Cde() { return payout_Cntrct_Type_Cde; }

    public DbsField getPayout_Cntrct_Lob_Cde() { return payout_Cntrct_Lob_Cde; }

    public DbsField getPayout_Cntrct_Option_Cde() { return payout_Cntrct_Option_Cde; }

    public DbsField getPayout_Cntrct_Mode_Cde() { return payout_Cntrct_Mode_Cde; }

    public DbsField getPayout_Cntrct_Roll_Dest_Cde() { return payout_Cntrct_Roll_Dest_Cde; }

    public DbsField getPayout_Cntrct_Settl_Amt() { return payout_Cntrct_Settl_Amt; }

    public DbsField getPayout_Cntrct_Cmbn_Nbr() { return payout_Cntrct_Cmbn_Nbr; }

    public DbsField getPayout_Cntrct_Cancel_Rdrw_Actvty_Cde() { return payout_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPayout_Ph_Last_Name() { return payout_Ph_Last_Name; }

    public DbsField getPayout_Ph_First_Name() { return payout_Ph_First_Name; }

    public DbsField getPayout_Ph_Middle_Name() { return payout_Ph_Middle_Name; }

    public DbsField getPayout_Annt_Soc_Sec_Ind() { return payout_Annt_Soc_Sec_Ind; }

    public DbsField getPayout_Annt_Soc_Sec_Nbr() { return payout_Annt_Soc_Sec_Nbr; }

    public DbsField getPayout_Pymnt_Stats_Cde() { return payout_Pymnt_Stats_Cde; }

    public DbsField getPayout_Pymnt_Pay_Type_Req_Ind() { return payout_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPayout_Pymnt_Check_Dte() { return payout_Pymnt_Check_Dte; }

    public DbsField getPayout_Pymnt_Eft_Dte() { return payout_Pymnt_Eft_Dte; }

    public DbsField getPayout_Pymnt_Check_Nbr() { return payout_Pymnt_Check_Nbr; }

    public DbsField getPayout_Pymnt_Prcss_Seq_Nbr() { return payout_Pymnt_Prcss_Seq_Nbr; }

    public DbsField getPayout_Pymnt_Check_Amt() { return payout_Pymnt_Check_Amt; }

    public DbsField getPayout_Pymnt_Nbr() { return payout_Pymnt_Nbr; }

    public DbsField getPayout_Pymnt_Nme() { return payout_Pymnt_Nme; }

    public DbsField getPayout_Pymnt_Addr_Line1_Txt() { return payout_Pymnt_Addr_Line1_Txt; }

    public DbsField getPayout_Pymnt_Addr_Line2_Txt() { return payout_Pymnt_Addr_Line2_Txt; }

    public DbsField getPayout_Pymnt_Addr_Line3_Txt() { return payout_Pymnt_Addr_Line3_Txt; }

    public DbsField getPayout_Pymnt_Addr_Line4_Txt() { return payout_Pymnt_Addr_Line4_Txt; }

    public DbsField getPayout_Pymnt_Addr_Line5_Txt() { return payout_Pymnt_Addr_Line5_Txt; }

    public DbsField getPayout_Pymnt_Addr_Line6_Txt() { return payout_Pymnt_Addr_Line6_Txt; }

    public DbsField getPayout_Pymnt_Addr_Zip_Cde() { return payout_Pymnt_Addr_Zip_Cde; }

    public DbsField getPayout_Pymnt_Postl_Data() { return payout_Pymnt_Postl_Data; }

    public DbsField getPayout_Pymnt_Eft_Transit_Id() { return payout_Pymnt_Eft_Transit_Id; }

    public DbsField getPayout_Pymnt_Eft_Acct_Nbr() { return payout_Pymnt_Eft_Acct_Nbr; }

    public DbsField getPayout_Pymnt_Chk_Sav_Ind() { return payout_Pymnt_Chk_Sav_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        payout = newGroupInRecord("payout", "PAYOUT");
        payout_Cntrct_Ppcn_Nbr = payout.newFieldInGroup("payout_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        payout_Cntrct_Payee_Cde = payout.newFieldInGroup("payout_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        payout_Cntrct_Invrse_Dte = payout.newFieldInGroup("payout_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        payout_Cntrct_Check_Crrncy_Cde = payout.newFieldInGroup("payout_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        payout_Cntrct_Orgn_Cde = payout.newFieldInGroup("payout_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        payout_Cntrct_Unq_Id_Nbr = payout.newFieldInGroup("payout_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        payout_Cntrct_Type_Cde = payout.newFieldInGroup("payout_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        payout_Cntrct_Lob_Cde = payout.newFieldInGroup("payout_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        payout_Cntrct_Option_Cde = payout.newFieldInGroup("payout_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        payout_Cntrct_Mode_Cde = payout.newFieldInGroup("payout_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        payout_Cntrct_Roll_Dest_Cde = payout.newFieldInGroup("payout_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        payout_Cntrct_Settl_Amt = payout.newFieldInGroup("payout_Cntrct_Settl_Amt", "CNTRCT-SETTL-AMT", FieldType.DECIMAL, 13,2);
        payout_Cntrct_Cmbn_Nbr = payout.newFieldInGroup("payout_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        payout_Cntrct_Cancel_Rdrw_Actvty_Cde = payout.newFieldInGroup("payout_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2);
        payout_Ph_Last_Name = payout.newFieldInGroup("payout_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        payout_Ph_First_Name = payout.newFieldInGroup("payout_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        payout_Ph_Middle_Name = payout.newFieldInGroup("payout_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        payout_Annt_Soc_Sec_Ind = payout.newFieldInGroup("payout_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1);
        payout_Annt_Soc_Sec_Nbr = payout.newFieldInGroup("payout_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        payout_Pymnt_Stats_Cde = payout.newFieldInGroup("payout_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1);
        payout_Pymnt_Pay_Type_Req_Ind = payout.newFieldInGroup("payout_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        payout_Pymnt_Check_Dte = payout.newFieldInGroup("payout_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.STRING, 8);
        payout_Pymnt_Eft_Dte = payout.newFieldInGroup("payout_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.STRING, 8);
        payout_Pymnt_Check_Nbr = payout.newFieldInGroup("payout_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        payout_Pymnt_Prcss_Seq_Nbr = payout.newFieldInGroup("payout_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        payout_Pymnt_Check_Amt = payout.newFieldInGroup("payout_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.DECIMAL, 9,2);
        payout_Pymnt_Nbr = payout.newFieldInGroup("payout_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10);
        payout_Pymnt_Nme = payout.newFieldInGroup("payout_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38);
        payout_Pymnt_Addr_Line1_Txt = payout.newFieldInGroup("payout_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 35);
        payout_Pymnt_Addr_Line2_Txt = payout.newFieldInGroup("payout_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 35);
        payout_Pymnt_Addr_Line3_Txt = payout.newFieldInGroup("payout_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 35);
        payout_Pymnt_Addr_Line4_Txt = payout.newFieldInGroup("payout_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 35);
        payout_Pymnt_Addr_Line5_Txt = payout.newFieldInGroup("payout_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 35);
        payout_Pymnt_Addr_Line6_Txt = payout.newFieldInGroup("payout_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 35);
        payout_Pymnt_Addr_Zip_Cde = payout.newFieldInGroup("payout_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 9);
        payout_Pymnt_Postl_Data = payout.newFieldInGroup("payout_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 32);
        payout_Pymnt_Eft_Transit_Id = payout.newFieldInGroup("payout_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9);
        payout_Pymnt_Eft_Acct_Nbr = payout.newFieldInGroup("payout_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        payout_Pymnt_Chk_Sav_Ind = payout.newFieldInGroup("payout_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 1);

        this.setRecordName("LdaFcpl702b");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl702b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
