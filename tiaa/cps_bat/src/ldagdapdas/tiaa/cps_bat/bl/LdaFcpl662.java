/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:11 PM
**        * FROM NATURAL LDA     : FCPL662
************************************************************
**        * FILE NAME            : LdaFcpl662.java
**        * CLASS NAME           : LdaFcpl662
**        * INSTANCE NAME        : LdaFcpl662
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl662 extends DbsRecord
{
    // Properties
    private DbsField cps_Origin_Cdes;

    public DbsField getCps_Origin_Cdes() { return cps_Origin_Cdes; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cps_Origin_Cdes = newFieldArrayInRecord("cps_Origin_Cdes", "CPS-ORIGIN-CDES", FieldType.STRING, 2, new DbsArrayController(1,20));

        this.setRecordName("LdaFcpl662");
    }

    public void initializeValues() throws Exception
    {
        reset();
        cps_Origin_Cdes.getValue(1).setInitialValue("AL");
        cps_Origin_Cdes.getValue(2).setInitialValue("AP");
        cps_Origin_Cdes.getValue(3).setInitialValue("DC");
        cps_Origin_Cdes.getValue(4).setInitialValue("DS");
        cps_Origin_Cdes.getValue(5).setInitialValue("EW");
        cps_Origin_Cdes.getValue(6).setInitialValue("IA");
        cps_Origin_Cdes.getValue(7).setInitialValue("MS");
        cps_Origin_Cdes.getValue(8).setInitialValue("NZ");
        cps_Origin_Cdes.getValue(9).setInitialValue("SS");
        cps_Origin_Cdes.getValue(10).setInitialValue("VT");
    }

    // Constructor
    public LdaFcpl662() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
