/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:17 PM
**        * FROM NATURAL LDA     : FCPL826B
************************************************************
**        * FILE NAME            : LdaFcpl826b.java
**        * CLASS NAME           : LdaFcpl826b
**        * INSTANCE NAME        : LdaFcpl826b
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl826b extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl826b;
    private DbsField pnd_Fcpl826b_Pnd_Orgn_Idx;
    private DbsField pnd_Fcpl826b_Pnd_Orgn_Idx_1;
    private DbsField pnd_Fcpl826b_Pnd_Orgn_Code;
    private DbsField pnd_Fcpl826b_Pnd_Orgn_Code_Desc;

    public DbsGroup getPnd_Fcpl826b() { return pnd_Fcpl826b; }

    public DbsField getPnd_Fcpl826b_Pnd_Orgn_Idx() { return pnd_Fcpl826b_Pnd_Orgn_Idx; }

    public DbsField getPnd_Fcpl826b_Pnd_Orgn_Idx_1() { return pnd_Fcpl826b_Pnd_Orgn_Idx_1; }

    public DbsField getPnd_Fcpl826b_Pnd_Orgn_Code() { return pnd_Fcpl826b_Pnd_Orgn_Code; }

    public DbsField getPnd_Fcpl826b_Pnd_Orgn_Code_Desc() { return pnd_Fcpl826b_Pnd_Orgn_Code_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl826b = newGroupInRecord("pnd_Fcpl826b", "#FCPL826B");
        pnd_Fcpl826b_Pnd_Orgn_Idx = pnd_Fcpl826b.newFieldInGroup("pnd_Fcpl826b_Pnd_Orgn_Idx", "#ORGN-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl826b_Pnd_Orgn_Idx_1 = pnd_Fcpl826b.newFieldInGroup("pnd_Fcpl826b_Pnd_Orgn_Idx_1", "#ORGN-IDX-1", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl826b_Pnd_Orgn_Code = pnd_Fcpl826b.newFieldArrayInGroup("pnd_Fcpl826b_Pnd_Orgn_Code", "#ORGN-CODE", FieldType.STRING, 2, new DbsArrayController(1,
            2));
        pnd_Fcpl826b_Pnd_Orgn_Code_Desc = pnd_Fcpl826b.newFieldArrayInGroup("pnd_Fcpl826b_Pnd_Orgn_Code_Desc", "#ORGN-CODE-DESC", FieldType.STRING, 35, 
            new DbsArrayController(1,2));

        this.setRecordName("LdaFcpl826b");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl826b_Pnd_Orgn_Idx_1.setInitialValue(2);
        pnd_Fcpl826b_Pnd_Orgn_Code.getValue(1).setInitialValue("AP");
        pnd_Fcpl826b_Pnd_Orgn_Code_Desc.getValue(1).setInitialValue("Annuity Payments");
        pnd_Fcpl826b_Pnd_Orgn_Code_Desc.getValue(2).setInitialValue("???????????????????????????????????");
    }

    // Constructor
    public LdaFcpl826b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
