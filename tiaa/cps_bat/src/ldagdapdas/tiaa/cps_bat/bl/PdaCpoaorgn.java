/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:26 PM
**        * FROM NATURAL PDA     : CPOAORGN
************************************************************
**        * FILE NAME            : PdaCpoaorgn.java
**        * CLASS NAME           : PdaCpoaorgn
**        * INSTANCE NAME        : PdaCpoaorgn
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCpoaorgn extends PdaBase
{
    // Properties
    private DbsGroup cpoaorgn;
    private DbsGroup cpoaorgn_Cpoaorgn_Misc;
    private DbsField cpoaorgn_Pnd_O;
    private DbsField cpoaorgn_Pnd_Orgn_Used;
    private DbsField cpoaorgn_Pnd_Orgn_Max;
    private DbsGroup cpoaorgn_Cpoaorgn_Desc;
    private DbsField cpoaorgn_Pnd_Rt_Long_Key;
    private DbsGroup cpoaorgn_Pnd_Rt_Long_KeyRedef1;
    private DbsField cpoaorgn_Pnd_Orgn_Cde_4;
    private DbsGroup cpoaorgn_Pnd_Orgn_Cde_4Redef2;
    private DbsField cpoaorgn_Pnd_Orgn_Cde_2;
    private DbsField filler01;
    private DbsField filler02;
    private DbsField cpoaorgn_Pnd_Orgn_Cde_Desc_Uc;
    private DbsField cpoaorgn_Pnd_Orgn_Cde_Desc_Mc;
    private DbsField cpoaorgn_Pnd_Orgn_Cde_Bus_Unit;

    public DbsGroup getCpoaorgn() { return cpoaorgn; }

    public DbsGroup getCpoaorgn_Cpoaorgn_Misc() { return cpoaorgn_Cpoaorgn_Misc; }

    public DbsField getCpoaorgn_Pnd_O() { return cpoaorgn_Pnd_O; }

    public DbsField getCpoaorgn_Pnd_Orgn_Used() { return cpoaorgn_Pnd_Orgn_Used; }

    public DbsField getCpoaorgn_Pnd_Orgn_Max() { return cpoaorgn_Pnd_Orgn_Max; }

    public DbsGroup getCpoaorgn_Cpoaorgn_Desc() { return cpoaorgn_Cpoaorgn_Desc; }

    public DbsField getCpoaorgn_Pnd_Rt_Long_Key() { return cpoaorgn_Pnd_Rt_Long_Key; }

    public DbsGroup getCpoaorgn_Pnd_Rt_Long_KeyRedef1() { return cpoaorgn_Pnd_Rt_Long_KeyRedef1; }

    public DbsField getCpoaorgn_Pnd_Orgn_Cde_4() { return cpoaorgn_Pnd_Orgn_Cde_4; }

    public DbsGroup getCpoaorgn_Pnd_Orgn_Cde_4Redef2() { return cpoaorgn_Pnd_Orgn_Cde_4Redef2; }

    public DbsField getCpoaorgn_Pnd_Orgn_Cde_2() { return cpoaorgn_Pnd_Orgn_Cde_2; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getCpoaorgn_Pnd_Orgn_Cde_Desc_Uc() { return cpoaorgn_Pnd_Orgn_Cde_Desc_Uc; }

    public DbsField getCpoaorgn_Pnd_Orgn_Cde_Desc_Mc() { return cpoaorgn_Pnd_Orgn_Cde_Desc_Mc; }

    public DbsField getCpoaorgn_Pnd_Orgn_Cde_Bus_Unit() { return cpoaorgn_Pnd_Orgn_Cde_Bus_Unit; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cpoaorgn = dbsRecord.newGroupInRecord("cpoaorgn", "CPOAORGN");
        cpoaorgn.setParameterOption(ParameterOption.ByReference);
        cpoaorgn_Cpoaorgn_Misc = cpoaorgn.newGroupInGroup("cpoaorgn_Cpoaorgn_Misc", "CPOAORGN-MISC");
        cpoaorgn_Pnd_O = cpoaorgn_Cpoaorgn_Misc.newFieldInGroup("cpoaorgn_Pnd_O", "#O", FieldType.PACKED_DECIMAL, 3);
        cpoaorgn_Pnd_Orgn_Used = cpoaorgn_Cpoaorgn_Misc.newFieldInGroup("cpoaorgn_Pnd_Orgn_Used", "#ORGN-USED", FieldType.PACKED_DECIMAL, 3);
        cpoaorgn_Pnd_Orgn_Max = cpoaorgn_Cpoaorgn_Misc.newFieldInGroup("cpoaorgn_Pnd_Orgn_Max", "#ORGN-MAX", FieldType.PACKED_DECIMAL, 3);
        cpoaorgn_Cpoaorgn_Desc = cpoaorgn.newGroupArrayInGroup("cpoaorgn_Cpoaorgn_Desc", "CPOAORGN-DESC", new DbsArrayController(1,20));
        cpoaorgn_Pnd_Rt_Long_Key = cpoaorgn_Cpoaorgn_Desc.newFieldInGroup("cpoaorgn_Pnd_Rt_Long_Key", "#RT-LONG-KEY", FieldType.STRING, 40);
        cpoaorgn_Pnd_Rt_Long_KeyRedef1 = cpoaorgn_Cpoaorgn_Desc.newGroupInGroup("cpoaorgn_Pnd_Rt_Long_KeyRedef1", "Redefines", cpoaorgn_Pnd_Rt_Long_Key);
        cpoaorgn_Pnd_Orgn_Cde_4 = cpoaorgn_Pnd_Rt_Long_KeyRedef1.newFieldInGroup("cpoaorgn_Pnd_Orgn_Cde_4", "#ORGN-CDE-4", FieldType.STRING, 4);
        cpoaorgn_Pnd_Orgn_Cde_4Redef2 = cpoaorgn_Pnd_Rt_Long_KeyRedef1.newGroupInGroup("cpoaorgn_Pnd_Orgn_Cde_4Redef2", "Redefines", cpoaorgn_Pnd_Orgn_Cde_4);
        cpoaorgn_Pnd_Orgn_Cde_2 = cpoaorgn_Pnd_Orgn_Cde_4Redef2.newFieldInGroup("cpoaorgn_Pnd_Orgn_Cde_2", "#ORGN-CDE-2", FieldType.STRING, 2);
        filler01 = cpoaorgn_Pnd_Orgn_Cde_4Redef2.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 2);
        filler02 = cpoaorgn_Pnd_Rt_Long_KeyRedef1.newFieldInGroup("filler02", "FILLER", FieldType.STRING, 1);
        cpoaorgn_Pnd_Orgn_Cde_Desc_Uc = cpoaorgn_Pnd_Rt_Long_KeyRedef1.newFieldInGroup("cpoaorgn_Pnd_Orgn_Cde_Desc_Uc", "#ORGN-CDE-DESC-UC", FieldType.STRING, 
            35);
        cpoaorgn_Pnd_Orgn_Cde_Desc_Mc = cpoaorgn_Cpoaorgn_Desc.newFieldInGroup("cpoaorgn_Pnd_Orgn_Cde_Desc_Mc", "#ORGN-CDE-DESC-MC", FieldType.STRING, 
            35);
        cpoaorgn_Pnd_Orgn_Cde_Bus_Unit = cpoaorgn_Cpoaorgn_Desc.newFieldInGroup("cpoaorgn_Pnd_Orgn_Cde_Bus_Unit", "#ORGN-CDE-BUS-UNIT", FieldType.STRING, 
            35);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCpoaorgn(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

