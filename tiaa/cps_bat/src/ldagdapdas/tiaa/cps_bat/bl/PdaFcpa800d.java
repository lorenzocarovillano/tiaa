/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:26 PM
**        * FROM NATURAL PDA     : FCPA800D
************************************************************
**        * FILE NAME            : PdaFcpa800d.java
**        * CLASS NAME           : PdaFcpa800d
**        * INSTANCE NAME        : PdaFcpa800d
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa800d extends PdaBase
{
    // Properties
    private DbsField wf_Pymnt_Addr_Grp;
    private DbsGroup wf_Pymnt_Addr_GrpRedef1;
    private DbsGroup wf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec;
    private DbsGroup wf_Pymnt_Addr_Grp_Pymnt_Addr_Info;
    private DbsField wf_Pymnt_Addr_Grp_Gtn_Ret_Code;
    private DbsField wf_Pymnt_Addr_Grp_Gtn_Rpt_Code;
    private DbsGroup wf_Pymnt_Addr_Grp_Gtn_Rpt_CodeRedef2;
    private DbsField wf_Pymnt_Addr_Grp_Gtn_Pymnt_Rpt_Code;
    private DbsField wf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr;
    private DbsField wf_Pymnt_Addr_Grp_Off_Mode_Contract;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Payee_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_NbrRedef3;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Num;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr;
    private DbsField wf_Pymnt_Addr_Grp_Rcrd_Typ;
    private DbsGroup wf_Pymnt_Addr_Grp_Ph_Name;
    private DbsField wf_Pymnt_Addr_Grp_Ph_Last_Name;
    private DbsField wf_Pymnt_Addr_Grp_Ph_First_Name;
    private DbsGroup wf_Pymnt_Addr_Grp_Ph_First_NameRedef4;
    private DbsField wf_Pymnt_Addr_Grp_Ph_First_Name_1;
    private DbsField wf_Pymnt_Addr_Grp_Ph_Middle_Name;
    private DbsGroup wf_Pymnt_Addr_Grp_Ph_Middle_NameRedef5;
    private DbsField wf_Pymnt_Addr_Grp_Ph_Middle_Name_1;
    private DbsGroup wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Nme;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Lines;
    private DbsGroup wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef6;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Line_Txt;
    private DbsGroup wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef7;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Foreign_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Postl_Data;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Type_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Last_Chg_Dte;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Last_Chg_Tme;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Addr_Chg_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_A;
    private DbsGroup wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_ARedef8;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Eft_Acct_Nbr;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Chk_Sav_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Dob;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Check_Crrncy_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Crrncy_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Qlfied_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Stats_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Annot_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Cmbne_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Ftre_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Annt_Soc_Sec_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Type_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Lob_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Sub_Lob_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Ia_Lob_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Cref_Nbr;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Option_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Mode_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Pymnt_Dest_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Hold_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Hold_Grp;
    private DbsField wf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr;
    private DbsField wf_Pymnt_Addr_Grp_Annt_Ctznshp_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Check_Dte;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Eft_Dte;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Check_Nbr;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Check_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Settlmnt_Dte;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Check_Seq_Nbr;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Corp_Wpid;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Reqst_Log_Dte_Time;
    private DbsField wf_Pymnt_Addr_Grp_Annt_Locality_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Acctg_Dte;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Intrfce_Dte;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Tiaa_Md_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Cref_Md_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Tax_Form;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Tax_Exempt_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Tax_Calc_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Method_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Settl_Ivc_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Ia_Issue_Dte;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Spouse_Pay_Stats;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Insurance_Option;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Life_Contingency;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Hold_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Hold_User_Id;
    private DbsField wf_Pymnt_Addr_Grp_C_Pymnt_Ded_Grp;
    private DbsGroup wf_Pymnt_Addr_Grp_Pymnt_Ded_Grp;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Ded_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Ded_Payee_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Ded_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Bar_Env_Integrity_Counter;
    private DbsField wf_Pymnt_Addr_Grp_Pymnt_Tax_Info;
    private DbsGroup wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Repeat_Adaread;
    private DbsField wf_Pymnt_Addr_Grp_Tax_New_Issue;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Free_Bytes;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Fed_Calc_Data;
    private DbsGroup wf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Fed_C_Resp_Code;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Fed_C_Filing_Stat;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Fed_C_Allow_Count;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Fed_C_Tax_Withhold;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Fed_Rollover_Tax_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Amount;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Percent;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Sta_Calc_Data;
    private DbsGroup wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Sta_C_Resp_Code;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Sta_C_Filing_Stat;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Sta_C_Allow_Count;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Sta_C_Tax_Withhold;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Sta_Rollover_Tax_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Amount;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Percent;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Sta_C_Fdrl_Tx_Pct;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Sta_C_Spouse_Cnt;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Sta_C_Blind_Cnt;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Loc_Calc_Data;
    private DbsGroup wf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Loc_C_Resp_Code;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Loc_A;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Loc_B;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Loc_C_Tax_Withhold;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Loc_Rollover_Tax_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Amount;
    private DbsField wf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Percent;
    private DbsField wf_Pymnt_Addr_Grp_Egtrra_Eligibility_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Originating_Irc_Cde;
    private DbsGroup wf_Pymnt_Addr_Grp_Originating_Irc_CdeRedef13;
    private DbsField wf_Pymnt_Addr_Grp_Distributing_Irc_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Receiving_Irc_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Count;
    private DbsGroup wf_Pymnt_Addr_Grp_Inv_Info;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Detail;
    private DbsGroup wf_Pymnt_Addr_Grp_Inv_DetailRedef14;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Company;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Cde_N;
    private DbsGroup wf_Pymnt_Addr_Grp_Inv_Acct_Cde_NRedef15;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Fed_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_State_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Local_Cde;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt;
    private DbsGroup wf_Pymnt_Addr_Grp_Inv_Acct_Dci_AmtRedef16;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Net_Adj_Ded;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Ind;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Adj_Ivc_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt;
    private DbsField wf_Pymnt_Addr_Grp_Inv_Acct_Exp_Amt;

    public DbsField getWf_Pymnt_Addr_Grp() { return wf_Pymnt_Addr_Grp; }

    public DbsGroup getWf_Pymnt_Addr_GrpRedef1() { return wf_Pymnt_Addr_GrpRedef1; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec() { return wf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Info; }

    public DbsField getWf_Pymnt_Addr_Grp_Gtn_Ret_Code() { return wf_Pymnt_Addr_Grp_Gtn_Ret_Code; }

    public DbsField getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code() { return wf_Pymnt_Addr_Grp_Gtn_Rpt_Code; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Gtn_Rpt_CodeRedef2() { return wf_Pymnt_Addr_Grp_Gtn_Rpt_CodeRedef2; }

    public DbsField getWf_Pymnt_Addr_Grp_Gtn_Pymnt_Rpt_Code() { return wf_Pymnt_Addr_Grp_Gtn_Pymnt_Rpt_Code; }

    public DbsField getWf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code() { return wf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr() { return wf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr; }

    public DbsField getWf_Pymnt_Addr_Grp_Off_Mode_Contract() { return wf_Pymnt_Addr_Grp_Off_Mode_Contract; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr() { return wf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Payee_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte() { return wf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Nbr() { return wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_NbrRedef3() { return wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_NbrRedef3; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Num() { return wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Num; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr() { return wf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr; }

    public DbsField getWf_Pymnt_Addr_Grp_Rcrd_Typ() { return wf_Pymnt_Addr_Grp_Rcrd_Typ; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Ph_Name() { return wf_Pymnt_Addr_Grp_Ph_Name; }

    public DbsField getWf_Pymnt_Addr_Grp_Ph_Last_Name() { return wf_Pymnt_Addr_Grp_Ph_Last_Name; }

    public DbsField getWf_Pymnt_Addr_Grp_Ph_First_Name() { return wf_Pymnt_Addr_Grp_Ph_First_Name; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Ph_First_NameRedef4() { return wf_Pymnt_Addr_Grp_Ph_First_NameRedef4; }

    public DbsField getWf_Pymnt_Addr_Grp_Ph_First_Name_1() { return wf_Pymnt_Addr_Grp_Ph_First_Name_1; }

    public DbsField getWf_Pymnt_Addr_Grp_Ph_Middle_Name() { return wf_Pymnt_Addr_Grp_Ph_Middle_Name; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Ph_Middle_NameRedef5() { return wf_Pymnt_Addr_Grp_Ph_Middle_NameRedef5; }

    public DbsField getWf_Pymnt_Addr_Grp_Ph_Middle_Name_1() { return wf_Pymnt_Addr_Grp_Ph_Middle_Name_1; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp() { return wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Nme() { return wf_Pymnt_Addr_Grp_Pymnt_Nme; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Lines() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Lines; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef6() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef6; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line_Txt() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Line_Txt; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef7() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef7; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Foreign_Cde() { return wf_Pymnt_Addr_Grp_Pymnt_Foreign_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Postl_Data() { return wf_Pymnt_Addr_Grp_Pymnt_Postl_Data; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Type_Ind() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Type_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Last_Chg_Dte() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Last_Chg_Dte; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Last_Chg_Tme() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Last_Chg_Tme; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Addr_Chg_Ind() { return wf_Pymnt_Addr_Grp_Pymnt_Addr_Chg_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_A() { return wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_A; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_ARedef8() { return wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_ARedef8; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id() { return wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Eft_Acct_Nbr() { return wf_Pymnt_Addr_Grp_Pymnt_Eft_Acct_Nbr; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Chk_Sav_Ind() { return wf_Pymnt_Addr_Grp_Pymnt_Chk_Sav_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Dob() { return wf_Pymnt_Addr_Grp_Pymnt_Dob; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Check_Crrncy_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Check_Crrncy_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Crrncy_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Crrncy_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Qlfied_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Qlfied_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind() { return wf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind() { return wf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Stats_Cde() { return wf_Pymnt_Addr_Grp_Pymnt_Stats_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Annot_Ind() { return wf_Pymnt_Addr_Grp_Pymnt_Annot_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Cmbne_Ind() { return wf_Pymnt_Addr_Grp_Pymnt_Cmbne_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Ftre_Ind() { return wf_Pymnt_Addr_Grp_Pymnt_Ftre_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Payee_Tx_Elct_Trggr() { return wf_Pymnt_Addr_Grp_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Ind() { return wf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Ind() { return wf_Pymnt_Addr_Grp_Annt_Soc_Sec_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind() { return wf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr() { return wf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Type_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Type_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Lob_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Lob_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Sub_Lob_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Sub_Lob_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Ia_Lob_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Ia_Lob_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Cref_Nbr() { return wf_Pymnt_Addr_Grp_Cntrct_Cref_Nbr; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Option_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Mode_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Pymnt_Dest_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Pymnt_Dest_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Hold_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Hold_Grp() { return wf_Pymnt_Addr_Grp_Cntrct_Hold_Grp; }

    public DbsField getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr() { return wf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr; }

    public DbsField getWf_Pymnt_Addr_Grp_Annt_Ctznshp_Cde() { return wf_Pymnt_Addr_Grp_Annt_Ctznshp_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde() { return wf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte() { return wf_Pymnt_Addr_Grp_Pymnt_Check_Dte; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte() { return wf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Eft_Dte() { return wf_Pymnt_Addr_Grp_Pymnt_Eft_Dte; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr() { return wf_Pymnt_Addr_Grp_Pymnt_Check_Nbr; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr() { return wf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt() { return wf_Pymnt_Addr_Grp_Pymnt_Check_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Settlmnt_Dte() { return wf_Pymnt_Addr_Grp_Pymnt_Settlmnt_Dte; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Check_Seq_Nbr() { return wf_Pymnt_Addr_Grp_Pymnt_Check_Seq_Nbr; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Corp_Wpid() { return wf_Pymnt_Addr_Grp_Pymnt_Corp_Wpid; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Reqst_Log_Dte_Time() { return wf_Pymnt_Addr_Grp_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getWf_Pymnt_Addr_Grp_Annt_Locality_Cde() { return wf_Pymnt_Addr_Grp_Annt_Locality_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Acctg_Dte() { return wf_Pymnt_Addr_Grp_Pymnt_Acctg_Dte; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Intrfce_Dte() { return wf_Pymnt_Addr_Grp_Pymnt_Intrfce_Dte; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Tiaa_Md_Amt() { return wf_Pymnt_Addr_Grp_Pymnt_Tiaa_Md_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Cref_Md_Amt() { return wf_Pymnt_Addr_Grp_Pymnt_Cref_Md_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Tax_Form() { return wf_Pymnt_Addr_Grp_Pymnt_Tax_Form; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Tax_Exempt_Ind() { return wf_Pymnt_Addr_Grp_Pymnt_Tax_Exempt_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Tax_Calc_Cde() { return wf_Pymnt_Addr_Grp_Pymnt_Tax_Calc_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Method_Cde() { return wf_Pymnt_Addr_Grp_Pymnt_Method_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Settl_Ivc_Ind() { return wf_Pymnt_Addr_Grp_Pymnt_Settl_Ivc_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Ia_Issue_Dte() { return wf_Pymnt_Addr_Grp_Pymnt_Ia_Issue_Dte; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Spouse_Pay_Stats() { return wf_Pymnt_Addr_Grp_Pymnt_Spouse_Pay_Stats; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type() { return wf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option() { return wf_Pymnt_Addr_Grp_Cntrct_Insurance_Option; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency() { return wf_Pymnt_Addr_Grp_Cntrct_Life_Contingency; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde() { return wf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte() { return wf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Hold_Ind() { return wf_Pymnt_Addr_Grp_Cntrct_Hold_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Hold_User_Id() { return wf_Pymnt_Addr_Grp_Cntrct_Hold_User_Id; }

    public DbsField getWf_Pymnt_Addr_Grp_C_Pymnt_Ded_Grp() { return wf_Pymnt_Addr_Grp_C_Pymnt_Ded_Grp; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Pymnt_Ded_Grp() { return wf_Pymnt_Addr_Grp_Pymnt_Ded_Grp; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde() { return wf_Pymnt_Addr_Grp_Pymnt_Ded_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Ded_Payee_Cde() { return wf_Pymnt_Addr_Grp_Pymnt_Ded_Payee_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt() { return wf_Pymnt_Addr_Grp_Pymnt_Ded_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde() { return wf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Bar_Env_Integrity_Counter() { return wf_Pymnt_Addr_Grp_Bar_Env_Integrity_Counter; }

    public DbsField getWf_Pymnt_Addr_Grp_Pymnt_Tax_Info() { return wf_Pymnt_Addr_Grp_Pymnt_Tax_Info; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9() { return wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Repeat_Adaread() { return wf_Pymnt_Addr_Grp_Tax_Repeat_Adaread; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_New_Issue() { return wf_Pymnt_Addr_Grp_Tax_New_Issue; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Free_Bytes() { return wf_Pymnt_Addr_Grp_Tax_Free_Bytes; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Fed_Calc_Data() { return wf_Pymnt_Addr_Grp_Tax_Fed_Calc_Data; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10() { return wf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Fed_C_Resp_Code() { return wf_Pymnt_Addr_Grp_Tax_Fed_C_Resp_Code; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Fed_C_Filing_Stat() { return wf_Pymnt_Addr_Grp_Tax_Fed_C_Filing_Stat; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Fed_C_Allow_Count() { return wf_Pymnt_Addr_Grp_Tax_Fed_C_Allow_Count; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Fed_C_Tax_Withhold() { return wf_Pymnt_Addr_Grp_Tax_Fed_C_Tax_Withhold; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Fed_Rollover_Tax_Ind() { return wf_Pymnt_Addr_Grp_Tax_Fed_Rollover_Tax_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Amount() { return wf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Amount; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Percent() { return wf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Percent; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Sta_Calc_Data() { return wf_Pymnt_Addr_Grp_Tax_Sta_Calc_Data; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11() { return wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Sta_C_Resp_Code() { return wf_Pymnt_Addr_Grp_Tax_Sta_C_Resp_Code; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Sta_C_Filing_Stat() { return wf_Pymnt_Addr_Grp_Tax_Sta_C_Filing_Stat; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Sta_C_Allow_Count() { return wf_Pymnt_Addr_Grp_Tax_Sta_C_Allow_Count; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Sta_C_Tax_Withhold() { return wf_Pymnt_Addr_Grp_Tax_Sta_C_Tax_Withhold; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Sta_Rollover_Tax_Ind() { return wf_Pymnt_Addr_Grp_Tax_Sta_Rollover_Tax_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Amount() { return wf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Amount; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Percent() { return wf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Percent; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Sta_C_Fdrl_Tx_Pct() { return wf_Pymnt_Addr_Grp_Tax_Sta_C_Fdrl_Tx_Pct; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Sta_C_Spouse_Cnt() { return wf_Pymnt_Addr_Grp_Tax_Sta_C_Spouse_Cnt; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Sta_C_Blind_Cnt() { return wf_Pymnt_Addr_Grp_Tax_Sta_C_Blind_Cnt; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Loc_Calc_Data() { return wf_Pymnt_Addr_Grp_Tax_Loc_Calc_Data; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12() { return wf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Loc_C_Resp_Code() { return wf_Pymnt_Addr_Grp_Tax_Loc_C_Resp_Code; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Loc_A() { return wf_Pymnt_Addr_Grp_Tax_Loc_A; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Loc_B() { return wf_Pymnt_Addr_Grp_Tax_Loc_B; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Loc_C_Tax_Withhold() { return wf_Pymnt_Addr_Grp_Tax_Loc_C_Tax_Withhold; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Loc_Rollover_Tax_Ind() { return wf_Pymnt_Addr_Grp_Tax_Loc_Rollover_Tax_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Amount() { return wf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Amount; }

    public DbsField getWf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Percent() { return wf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Percent; }

    public DbsField getWf_Pymnt_Addr_Grp_Egtrra_Eligibility_Ind() { return wf_Pymnt_Addr_Grp_Egtrra_Eligibility_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Originating_Irc_Cde() { return wf_Pymnt_Addr_Grp_Originating_Irc_Cde; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Originating_Irc_CdeRedef13() { return wf_Pymnt_Addr_Grp_Originating_Irc_CdeRedef13; }

    public DbsField getWf_Pymnt_Addr_Grp_Distributing_Irc_Cde() { return wf_Pymnt_Addr_Grp_Distributing_Irc_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Receiving_Irc_Cde() { return wf_Pymnt_Addr_Grp_Receiving_Irc_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Count() { return wf_Pymnt_Addr_Grp_Inv_Acct_Count; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Inv_Info() { return wf_Pymnt_Addr_Grp_Inv_Info; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Detail() { return wf_Pymnt_Addr_Grp_Inv_Detail; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Inv_DetailRedef14() { return wf_Pymnt_Addr_Grp_Inv_DetailRedef14; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Company() { return wf_Pymnt_Addr_Grp_Inv_Acct_Company; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N() { return wf_Pymnt_Addr_Grp_Inv_Acct_Cde_N; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_NRedef15() { return wf_Pymnt_Addr_Grp_Inv_Acct_Cde_NRedef15; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Cde() { return wf_Pymnt_Addr_Grp_Inv_Acct_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha() { return wf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty() { return wf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value() { return wf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Fed_Cde() { return wf_Pymnt_Addr_Grp_Inv_Acct_Fed_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_State_Cde() { return wf_Pymnt_Addr_Grp_Inv_Acct_State_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Cde() { return wf_Pymnt_Addr_Grp_Inv_Acct_Local_Cde; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt; }

    public DbsGroup getWf_Pymnt_Addr_Grp_Inv_Acct_Dci_AmtRedef16() { return wf_Pymnt_Addr_Grp_Inv_Acct_Dci_AmtRedef16; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Adj_Ded() { return wf_Pymnt_Addr_Grp_Inv_Acct_Net_Adj_Ded; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Ind() { return wf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Ind; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Adj_Ivc_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_Adj_Ivc_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period() { return wf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt; }

    public DbsField getWf_Pymnt_Addr_Grp_Inv_Acct_Exp_Amt() { return wf_Pymnt_Addr_Grp_Inv_Acct_Exp_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        wf_Pymnt_Addr_Grp = dbsRecord.newFieldArrayInRecord("wf_Pymnt_Addr_Grp", "WF-PYMNT-ADDR-GRP", FieldType.STRING, 250, new DbsArrayController(1,
            19));
        wf_Pymnt_Addr_Grp.setParameterOption(ParameterOption.ByReference);
        wf_Pymnt_Addr_GrpRedef1 = dbsRecord.newGroupInRecord("wf_Pymnt_Addr_GrpRedef1", "Redefines", wf_Pymnt_Addr_Grp);
        wf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec = wf_Pymnt_Addr_GrpRedef1.newGroupInGroup("wf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec", "WF-PYMNT-ADDR-REC");
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Info = wf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec.newGroupInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Info", "PYMNT-ADDR-INFO");
        wf_Pymnt_Addr_Grp_Gtn_Ret_Code = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Gtn_Ret_Code", "GTN-RET-CODE", FieldType.STRING, 
            4);
        wf_Pymnt_Addr_Grp_Gtn_Rpt_Code = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldArrayInGroup("wf_Pymnt_Addr_Grp_Gtn_Rpt_Code", "GTN-RPT-CODE", FieldType.NUMERIC, 
            2, new DbsArrayController(1,10));
        wf_Pymnt_Addr_Grp_Gtn_Rpt_CodeRedef2 = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newGroupInGroup("wf_Pymnt_Addr_Grp_Gtn_Rpt_CodeRedef2", "Redefines", 
            wf_Pymnt_Addr_Grp_Gtn_Rpt_Code);
        wf_Pymnt_Addr_Grp_Gtn_Pymnt_Rpt_Code = wf_Pymnt_Addr_Grp_Gtn_Rpt_CodeRedef2.newFieldArrayInGroup("wf_Pymnt_Addr_Grp_Gtn_Pymnt_Rpt_Code", "GTN-PYMNT-RPT-CODE", 
            FieldType.NUMERIC, 2, new DbsArrayController(1,7));
        wf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code = wf_Pymnt_Addr_Grp_Gtn_Rpt_CodeRedef2.newFieldArrayInGroup("wf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code", "GTN-CNTRCT-RPT-CODE", 
            FieldType.NUMERIC, 2, new DbsArrayController(1,3));
        wf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        wf_Pymnt_Addr_Grp_Off_Mode_Contract = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Off_Mode_Contract", "OFF-MODE-CONTRACT", 
            FieldType.BOOLEAN);
        wf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        wf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        wf_Pymnt_Addr_Grp_Cntrct_Payee_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        wf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Nbr = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_NbrRedef3 = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newGroupInGroup("wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_NbrRedef3", 
            "Redefines", wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Nbr);
        wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Num = wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_NbrRedef3.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        wf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr = wf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_NbrRedef3.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        wf_Pymnt_Addr_Grp_Rcrd_Typ = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Ph_Name = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newGroupInGroup("wf_Pymnt_Addr_Grp_Ph_Name", "PH-NAME");
        wf_Pymnt_Addr_Grp_Ph_Last_Name = wf_Pymnt_Addr_Grp_Ph_Name.newFieldInGroup("wf_Pymnt_Addr_Grp_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 
            16);
        wf_Pymnt_Addr_Grp_Ph_First_Name = wf_Pymnt_Addr_Grp_Ph_Name.newFieldInGroup("wf_Pymnt_Addr_Grp_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 
            10);
        wf_Pymnt_Addr_Grp_Ph_First_NameRedef4 = wf_Pymnt_Addr_Grp_Ph_Name.newGroupInGroup("wf_Pymnt_Addr_Grp_Ph_First_NameRedef4", "Redefines", wf_Pymnt_Addr_Grp_Ph_First_Name);
        wf_Pymnt_Addr_Grp_Ph_First_Name_1 = wf_Pymnt_Addr_Grp_Ph_First_NameRedef4.newFieldInGroup("wf_Pymnt_Addr_Grp_Ph_First_Name_1", "PH-FIRST-NAME-1", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Ph_Middle_Name = wf_Pymnt_Addr_Grp_Ph_Name.newFieldInGroup("wf_Pymnt_Addr_Grp_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 
            12);
        wf_Pymnt_Addr_Grp_Ph_Middle_NameRedef5 = wf_Pymnt_Addr_Grp_Ph_Name.newGroupInGroup("wf_Pymnt_Addr_Grp_Ph_Middle_NameRedef5", "Redefines", wf_Pymnt_Addr_Grp_Ph_Middle_Name);
        wf_Pymnt_Addr_Grp_Ph_Middle_Name_1 = wf_Pymnt_Addr_Grp_Ph_Middle_NameRedef5.newFieldInGroup("wf_Pymnt_Addr_Grp_Ph_Middle_Name_1", "PH-MIDDLE-NAME-1", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newGroupArrayInGroup("wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp", 
            "PYMNT-NME-AND-ADDR-GRP", new DbsArrayController(1,2));
        wf_Pymnt_Addr_Grp_Pymnt_Nme = wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 
            38);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Lines = wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Lines", "PYMNT-ADDR-LINES", 
            FieldType.STRING, 210);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef6 = wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef6", 
            "Redefines", wf_Pymnt_Addr_Grp_Pymnt_Addr_Lines);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Line_Txt = wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef6.newFieldArrayInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Line_Txt", 
            "PYMNT-ADDR-LINE-TXT", FieldType.STRING, 35, new DbsArrayController(1,6));
        wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef7 = wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef7", 
            "Redefines", wf_Pymnt_Addr_Grp_Pymnt_Addr_Lines);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt = wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef7.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt = wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef7.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt = wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef7.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", 
            FieldType.STRING, 35);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt = wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef7.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", 
            FieldType.STRING, 35);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt = wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef7.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", 
            FieldType.STRING, 35);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt = wf_Pymnt_Addr_Grp_Pymnt_Addr_LinesRedef7.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", 
            FieldType.STRING, 35);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde = wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", 
            FieldType.STRING, 9);
        wf_Pymnt_Addr_Grp_Pymnt_Foreign_Cde = wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Foreign_Cde", "PYMNT-FOREIGN-CDE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Postl_Data = wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", 
            FieldType.STRING, 32);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Type_Ind = wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Last_Chg_Dte = wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Last_Chg_Dte", 
            "PYMNT-ADDR-LAST-CHG-DTE", FieldType.NUMERIC, 8);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Last_Chg_Tme = wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Last_Chg_Tme", 
            "PYMNT-ADDR-LAST-CHG-TME", FieldType.NUMERIC, 7);
        wf_Pymnt_Addr_Grp_Pymnt_Addr_Chg_Ind = wf_Pymnt_Addr_Grp_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Addr_Chg_Ind", "PYMNT-ADDR-CHG-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_A = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_A", "PYMNT-EFT-TRANSIT-ID-A", 
            FieldType.STRING, 9);
        wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_ARedef8 = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newGroupInGroup("wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_ARedef8", 
            "Redefines", wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_A);
        wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id = wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id_ARedef8.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Eft_Transit_Id", 
            "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9);
        wf_Pymnt_Addr_Grp_Pymnt_Eft_Acct_Nbr = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", 
            FieldType.STRING, 21);
        wf_Pymnt_Addr_Grp_Pymnt_Chk_Sav_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Dob = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Dob", "PYMNT-DOB", FieldType.DATE);
        wf_Pymnt_Addr_Grp_Cntrct_Check_Crrncy_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Cntrct_Crrncy_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Cntrct_Qlfied_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Stats_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Annot_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Cmbne_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Ftre_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", FieldType.STRING, 
            1);
        wf_Pymnt_Addr_Grp_Pymnt_Payee_Tx_Elct_Trggr = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Payee_Tx_Elct_Trggr", 
            "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Ind", "CNTRCT-DVDND-PAYEE-IND", 
            FieldType.NUMERIC, 1);
        wf_Pymnt_Addr_Grp_Annt_Soc_Sec_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", 
            FieldType.NUMERIC, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        wf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        wf_Pymnt_Addr_Grp_Cntrct_Type_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 2);
        wf_Pymnt_Addr_Grp_Cntrct_Lob_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 
            4);
        wf_Pymnt_Addr_Grp_Cntrct_Sub_Lob_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", 
            FieldType.STRING, 4);
        wf_Pymnt_Addr_Grp_Cntrct_Ia_Lob_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", 
            FieldType.STRING, 2);
        wf_Pymnt_Addr_Grp_Cntrct_Cref_Nbr = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", 
            FieldType.STRING, 10);
        wf_Pymnt_Addr_Grp_Cntrct_Option_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", 
            FieldType.NUMERIC, 2);
        wf_Pymnt_Addr_Grp_Cntrct_Mode_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        wf_Pymnt_Addr_Grp_Cntrct_Pymnt_Dest_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", 
            FieldType.STRING, 4);
        wf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", 
            FieldType.STRING, 4);
        wf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", 
            FieldType.STRING, 5);
        wf_Pymnt_Addr_Grp_Cntrct_Hold_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 4);
        wf_Pymnt_Addr_Grp_Cntrct_Hold_Grp = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", 
            FieldType.STRING, 3);
        wf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        wf_Pymnt_Addr_Grp_Annt_Ctznshp_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        wf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        wf_Pymnt_Addr_Grp_Pymnt_Check_Dte = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", 
            FieldType.DATE);
        wf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", 
            FieldType.DATE);
        wf_Pymnt_Addr_Grp_Pymnt_Eft_Dte = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        wf_Pymnt_Addr_Grp_Pymnt_Check_Nbr = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        wf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", 
            FieldType.NUMERIC, 7);
        wf_Pymnt_Addr_Grp_Pymnt_Check_Amt = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Pymnt_Settlmnt_Dte = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);
        wf_Pymnt_Addr_Grp_Pymnt_Check_Seq_Nbr = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        wf_Pymnt_Addr_Grp_Pymnt_Corp_Wpid = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Corp_Wpid", "PYMNT-CORP-WPID", 
            FieldType.STRING, 6);
        wf_Pymnt_Addr_Grp_Pymnt_Reqst_Log_Dte_Time = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        wf_Pymnt_Addr_Grp_Annt_Locality_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", 
            FieldType.STRING, 2);
        wf_Pymnt_Addr_Grp_Pymnt_Acctg_Dte = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", 
            FieldType.DATE);
        wf_Pymnt_Addr_Grp_Pymnt_Intrfce_Dte = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", 
            FieldType.DATE);
        wf_Pymnt_Addr_Grp_Pymnt_Tiaa_Md_Amt = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Tiaa_Md_Amt", "PYMNT-TIAA-MD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        wf_Pymnt_Addr_Grp_Pymnt_Cref_Md_Amt = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Cref_Md_Amt", "PYMNT-CREF-MD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        wf_Pymnt_Addr_Grp_Pymnt_Tax_Form = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Tax_Form", "PYMNT-TAX-FORM", FieldType.STRING, 
            4);
        wf_Pymnt_Addr_Grp_Pymnt_Tax_Exempt_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Tax_Exempt_Ind", "PYMNT-TAX-EXEMPT-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Tax_Calc_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Tax_Calc_Cde", "PYMNT-TAX-CALC-CDE", 
            FieldType.STRING, 2);
        wf_Pymnt_Addr_Grp_Pymnt_Method_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Method_Cde", "PYMNT-METHOD-CDE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Settl_Ivc_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Settl_Ivc_Ind", "PYMNT-SETTL-IVC-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Ia_Issue_Dte = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Ia_Issue_Dte", "PYMNT-IA-ISSUE-DTE", 
            FieldType.DATE);
        wf_Pymnt_Addr_Grp_Pymnt_Spouse_Pay_Stats = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Cntrct_Insurance_Option = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Cntrct_Life_Contingency = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde", "PYMNT-SUSPEND-CDE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte", "PYMNT-SUSPEND-DTE", 
            FieldType.DATE);
        wf_Pymnt_Addr_Grp_Cntrct_Hold_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Hold_Ind", "CNTRCT-HOLD-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Cntrct_Hold_User_Id = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Hold_User_Id", "CNTRCT-HOLD-USER-ID", 
            FieldType.STRING, 3);
        wf_Pymnt_Addr_Grp_C_Pymnt_Ded_Grp = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_C_Pymnt_Ded_Grp", "C-PYMNT-DED-GRP", 
            FieldType.PACKED_DECIMAL, 3);
        wf_Pymnt_Addr_Grp_Pymnt_Ded_Grp = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newGroupInGroup("wf_Pymnt_Addr_Grp_Pymnt_Ded_Grp", "PYMNT-DED-GRP");
        wf_Pymnt_Addr_Grp_Pymnt_Ded_Cde = wf_Pymnt_Addr_Grp_Pymnt_Ded_Grp.newFieldArrayInGroup("wf_Pymnt_Addr_Grp_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3, new DbsArrayController(1,10));
        wf_Pymnt_Addr_Grp_Pymnt_Ded_Payee_Cde = wf_Pymnt_Addr_Grp_Pymnt_Ded_Grp.newFieldArrayInGroup("wf_Pymnt_Addr_Grp_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8, new DbsArrayController(1,10));
        wf_Pymnt_Addr_Grp_Pymnt_Ded_Amt = wf_Pymnt_Addr_Grp_Pymnt_Ded_Grp.newFieldArrayInGroup("wf_Pymnt_Addr_Grp_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,10));
        wf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        wf_Pymnt_Addr_Grp_Bar_Env_Integrity_Counter = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Bar_Env_Integrity_Counter", 
            "BAR-ENV-INTEGRITY-COUNTER", FieldType.NUMERIC, 1);
        wf_Pymnt_Addr_Grp_Pymnt_Tax_Info = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Pymnt_Tax_Info", "PYMNT-TAX-INFO", FieldType.STRING, 
            85);
        wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9 = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newGroupInGroup("wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9", "Redefines", 
            wf_Pymnt_Addr_Grp_Pymnt_Tax_Info);
        wf_Pymnt_Addr_Grp_Tax_Repeat_Adaread = wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Repeat_Adaread", "TAX-REPEAT-ADAREAD", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Tax_New_Issue = wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_New_Issue", "TAX-NEW-ISSUE", FieldType.STRING, 
            1);
        wf_Pymnt_Addr_Grp_Tax_Free_Bytes = wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Free_Bytes", "TAX-FREE-BYTES", 
            FieldType.STRING, 6);
        wf_Pymnt_Addr_Grp_Tax_Fed_Calc_Data = wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Fed_Calc_Data", "TAX-FED-CALC-DATA", 
            FieldType.STRING, 24);
        wf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10 = wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9.newGroupInGroup("wf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10", 
            "Redefines", wf_Pymnt_Addr_Grp_Tax_Fed_Calc_Data);
        wf_Pymnt_Addr_Grp_Tax_Fed_C_Resp_Code = wf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Fed_C_Resp_Code", "TAX-FED-C-RESP-CODE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Tax_Fed_C_Filing_Stat = wf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Fed_C_Filing_Stat", 
            "TAX-FED-C-FILING-STAT", FieldType.STRING, 2);
        wf_Pymnt_Addr_Grp_Tax_Fed_C_Allow_Count = wf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Fed_C_Allow_Count", 
            "TAX-FED-C-ALLOW-COUNT", FieldType.NUMERIC, 2);
        wf_Pymnt_Addr_Grp_Tax_Fed_C_Tax_Withhold = wf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Fed_C_Tax_Withhold", 
            "TAX-FED-C-TAX-WITHHOLD", FieldType.DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Tax_Fed_Rollover_Tax_Ind = wf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Fed_Rollover_Tax_Ind", 
            "TAX-FED-ROLLOVER-TAX-IND", FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Amount = wf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Amount", 
            "TAX-FED-C-FIXED-AMOUNT", FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Percent = wf_Pymnt_Addr_Grp_Tax_Fed_Calc_DataRedef10.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Percent", 
            "TAX-FED-C-FIXED-PERCENT", FieldType.PACKED_DECIMAL, 7,4);
        wf_Pymnt_Addr_Grp_Tax_Sta_Calc_Data = wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_Calc_Data", "TAX-STA-CALC-DATA", 
            FieldType.STRING, 29);
        wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11 = wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9.newGroupInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11", 
            "Redefines", wf_Pymnt_Addr_Grp_Tax_Sta_Calc_Data);
        wf_Pymnt_Addr_Grp_Tax_Sta_C_Resp_Code = wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_C_Resp_Code", "TAX-STA-C-RESP-CODE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Tax_Sta_C_Filing_Stat = wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_C_Filing_Stat", 
            "TAX-STA-C-FILING-STAT", FieldType.STRING, 2);
        wf_Pymnt_Addr_Grp_Tax_Sta_C_Allow_Count = wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_C_Allow_Count", 
            "TAX-STA-C-ALLOW-COUNT", FieldType.NUMERIC, 2);
        wf_Pymnt_Addr_Grp_Tax_Sta_C_Tax_Withhold = wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_C_Tax_Withhold", 
            "TAX-STA-C-TAX-WITHHOLD", FieldType.DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Tax_Sta_Rollover_Tax_Ind = wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_Rollover_Tax_Ind", 
            "TAX-STA-ROLLOVER-TAX-IND", FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Amount = wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Amount", 
            "TAX-STA-C-FIXED-AMOUNT", FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Percent = wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Percent", 
            "TAX-STA-C-FIXED-PERCENT", FieldType.PACKED_DECIMAL, 7,4);
        wf_Pymnt_Addr_Grp_Tax_Sta_C_Fdrl_Tx_Pct = wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_C_Fdrl_Tx_Pct", 
            "TAX-STA-C-FDRL-TX-PCT", FieldType.PACKED_DECIMAL, 5,3);
        wf_Pymnt_Addr_Grp_Tax_Sta_C_Spouse_Cnt = wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_C_Spouse_Cnt", 
            "TAX-STA-C-SPOUSE-CNT", FieldType.NUMERIC, 1);
        wf_Pymnt_Addr_Grp_Tax_Sta_C_Blind_Cnt = wf_Pymnt_Addr_Grp_Tax_Sta_Calc_DataRedef11.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Sta_C_Blind_Cnt", "TAX-STA-C-BLIND-CNT", 
            FieldType.NUMERIC, 1);
        wf_Pymnt_Addr_Grp_Tax_Loc_Calc_Data = wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Loc_Calc_Data", "TAX-LOC-CALC-DATA", 
            FieldType.STRING, 24);
        wf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12 = wf_Pymnt_Addr_Grp_Pymnt_Tax_InfoRedef9.newGroupInGroup("wf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12", 
            "Redefines", wf_Pymnt_Addr_Grp_Tax_Loc_Calc_Data);
        wf_Pymnt_Addr_Grp_Tax_Loc_C_Resp_Code = wf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Loc_C_Resp_Code", "TAX-LOC-C-RESP-CODE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Tax_Loc_A = wf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Loc_A", "TAX-LOC-A", FieldType.STRING, 
            2);
        wf_Pymnt_Addr_Grp_Tax_Loc_B = wf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Loc_B", "TAX-LOC-B", FieldType.NUMERIC, 
            2);
        wf_Pymnt_Addr_Grp_Tax_Loc_C_Tax_Withhold = wf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Loc_C_Tax_Withhold", 
            "TAX-LOC-C-TAX-WITHHOLD", FieldType.DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Tax_Loc_Rollover_Tax_Ind = wf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Loc_Rollover_Tax_Ind", 
            "TAX-LOC-ROLLOVER-TAX-IND", FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Amount = wf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Amount", 
            "TAX-LOC-C-FIXED-AMOUNT", FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Percent = wf_Pymnt_Addr_Grp_Tax_Loc_Calc_DataRedef12.newFieldInGroup("wf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Percent", 
            "TAX-LOC-C-FIXED-PERCENT", FieldType.PACKED_DECIMAL, 7,4);
        wf_Pymnt_Addr_Grp_Egtrra_Eligibility_Ind = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Egtrra_Eligibility_Ind", "EGTRRA-ELIGIBILITY-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Originating_Irc_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldArrayInGroup("wf_Pymnt_Addr_Grp_Originating_Irc_Cde", "ORIGINATING-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,8));
        wf_Pymnt_Addr_Grp_Originating_Irc_CdeRedef13 = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newGroupInGroup("wf_Pymnt_Addr_Grp_Originating_Irc_CdeRedef13", 
            "Redefines", wf_Pymnt_Addr_Grp_Originating_Irc_Cde);
        wf_Pymnt_Addr_Grp_Distributing_Irc_Cde = wf_Pymnt_Addr_Grp_Originating_Irc_CdeRedef13.newFieldInGroup("wf_Pymnt_Addr_Grp_Distributing_Irc_Cde", 
            "DISTRIBUTING-IRC-CDE", FieldType.STRING, 16);
        wf_Pymnt_Addr_Grp_Receiving_Irc_Cde = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Receiving_Irc_Cde", "RECEIVING-IRC-CDE", 
            FieldType.STRING, 2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Count = wf_Pymnt_Addr_Grp_Pymnt_Addr_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Count", "INV-ACCT-COUNT", FieldType.PACKED_DECIMAL, 
            2);
        wf_Pymnt_Addr_Grp_Inv_Info = wf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec.newGroupArrayInGroup("wf_Pymnt_Addr_Grp_Inv_Info", "INV-INFO", new DbsArrayController(1,
            40));
        wf_Pymnt_Addr_Grp_Inv_Detail = wf_Pymnt_Addr_Grp_Inv_Info.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Detail", "INV-DETAIL", FieldType.STRING, 82);
        wf_Pymnt_Addr_Grp_Inv_DetailRedef14 = wf_Pymnt_Addr_Grp_Inv_Info.newGroupInGroup("wf_Pymnt_Addr_Grp_Inv_DetailRedef14", "Redefines", wf_Pymnt_Addr_Grp_Inv_Detail);
        wf_Pymnt_Addr_Grp_Inv_Acct_Company = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Company", "INV-ACCT-COMPANY", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Inv_Acct_Cde_N = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Cde_N", "INV-ACCT-CDE-N", FieldType.NUMERIC, 
            2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Cde_NRedef15 = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newGroupInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Cde_NRedef15", "Redefines", 
            wf_Pymnt_Addr_Grp_Inv_Acct_Cde_N);
        wf_Pymnt_Addr_Grp_Inv_Acct_Cde = wf_Pymnt_Addr_Grp_Inv_Acct_Cde_NRedef15.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha", "INV-ACCT-CDE-ALPHA", 
            FieldType.STRING, 2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", 
            FieldType.PACKED_DECIMAL, 9,3);
        wf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", 
            FieldType.PACKED_DECIMAL, 9,4);
        wf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt", "INV-ACCT-CNTRCT-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Fed_Cde = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Inv_Acct_State_Cde = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Inv_Acct_Local_Cde = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Dci_AmtRedef16 = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newGroupInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Dci_AmtRedef16", "Redefines", 
            wf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt);
        wf_Pymnt_Addr_Grp_Inv_Acct_Net_Adj_Ded = wf_Pymnt_Addr_Grp_Inv_Acct_Dci_AmtRedef16.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Net_Adj_Ded", "INV-ACCT-NET-ADJ-DED", 
            FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Ind = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Inv_Acct_Adj_Ivc_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        wf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        wf_Pymnt_Addr_Grp_Inv_Acct_Exp_Amt = wf_Pymnt_Addr_Grp_Inv_DetailRedef14.newFieldInGroup("wf_Pymnt_Addr_Grp_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa800d(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

