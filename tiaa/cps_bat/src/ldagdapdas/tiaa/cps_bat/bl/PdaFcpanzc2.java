/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:32 PM
**        * FROM NATURAL PDA     : FCPANZC2
************************************************************
**        * FILE NAME            : PdaFcpanzc2.java
**        * CLASS NAME           : PdaFcpanzc2
**        * INSTANCE NAME        : PdaFcpanzc2
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpanzc2 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpanzc2;
    private DbsField pnd_Fcpanzc2_Pnd_Accum_Truth_Table;
    private DbsField pnd_Fcpanzc2_Pnd_New_Pymnt_Ind;
    private DbsGroup pnd_Fcpanzc2_Pnd_Accum_Table;
    private DbsField pnd_Fcpanzc2_Pnd_Pymnt_Cnt;
    private DbsField pnd_Fcpanzc2_Pnd_Settl_Amt;
    private DbsField pnd_Fcpanzc2_Pnd_Dvdnd_Amt;
    private DbsField pnd_Fcpanzc2_Pnd_Dci_Amt;
    private DbsField pnd_Fcpanzc2_Pnd_Dpi_Amt;
    private DbsField pnd_Fcpanzc2_Pnd_Net_Pymnt_Amt;
    private DbsField pnd_Fcpanzc2_Pnd_Fdrl_Tax_Amt;
    private DbsField pnd_Fcpanzc2_Pnd_State_Tax_Amt;
    private DbsField pnd_Fcpanzc2_Pnd_Local_Tax_Amt;
    private DbsField pnd_Fcpanzc2_Pnd_Rec_Cnt;

    public DbsGroup getPnd_Fcpanzc2() { return pnd_Fcpanzc2; }

    public DbsField getPnd_Fcpanzc2_Pnd_Accum_Truth_Table() { return pnd_Fcpanzc2_Pnd_Accum_Truth_Table; }

    public DbsField getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind() { return pnd_Fcpanzc2_Pnd_New_Pymnt_Ind; }

    public DbsGroup getPnd_Fcpanzc2_Pnd_Accum_Table() { return pnd_Fcpanzc2_Pnd_Accum_Table; }

    public DbsField getPnd_Fcpanzc2_Pnd_Pymnt_Cnt() { return pnd_Fcpanzc2_Pnd_Pymnt_Cnt; }

    public DbsField getPnd_Fcpanzc2_Pnd_Settl_Amt() { return pnd_Fcpanzc2_Pnd_Settl_Amt; }

    public DbsField getPnd_Fcpanzc2_Pnd_Dvdnd_Amt() { return pnd_Fcpanzc2_Pnd_Dvdnd_Amt; }

    public DbsField getPnd_Fcpanzc2_Pnd_Dci_Amt() { return pnd_Fcpanzc2_Pnd_Dci_Amt; }

    public DbsField getPnd_Fcpanzc2_Pnd_Dpi_Amt() { return pnd_Fcpanzc2_Pnd_Dpi_Amt; }

    public DbsField getPnd_Fcpanzc2_Pnd_Net_Pymnt_Amt() { return pnd_Fcpanzc2_Pnd_Net_Pymnt_Amt; }

    public DbsField getPnd_Fcpanzc2_Pnd_Fdrl_Tax_Amt() { return pnd_Fcpanzc2_Pnd_Fdrl_Tax_Amt; }

    public DbsField getPnd_Fcpanzc2_Pnd_State_Tax_Amt() { return pnd_Fcpanzc2_Pnd_State_Tax_Amt; }

    public DbsField getPnd_Fcpanzc2_Pnd_Local_Tax_Amt() { return pnd_Fcpanzc2_Pnd_Local_Tax_Amt; }

    public DbsField getPnd_Fcpanzc2_Pnd_Rec_Cnt() { return pnd_Fcpanzc2_Pnd_Rec_Cnt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpanzc2 = dbsRecord.newGroupInRecord("pnd_Fcpanzc2", "#FCPANZC2");
        pnd_Fcpanzc2.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpanzc2_Pnd_Accum_Truth_Table = pnd_Fcpanzc2.newFieldArrayInGroup("pnd_Fcpanzc2_Pnd_Accum_Truth_Table", "#ACCUM-TRUTH-TABLE", FieldType.BOOLEAN, 
            new DbsArrayController(1,9));
        pnd_Fcpanzc2_Pnd_New_Pymnt_Ind = pnd_Fcpanzc2.newFieldInGroup("pnd_Fcpanzc2_Pnd_New_Pymnt_Ind", "#NEW-PYMNT-IND", FieldType.BOOLEAN);
        pnd_Fcpanzc2_Pnd_Accum_Table = pnd_Fcpanzc2.newGroupInGroup("pnd_Fcpanzc2_Pnd_Accum_Table", "#ACCUM-TABLE");
        pnd_Fcpanzc2_Pnd_Pymnt_Cnt = pnd_Fcpanzc2_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpanzc2_Pnd_Pymnt_Cnt", "#PYMNT-CNT", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1,50));
        pnd_Fcpanzc2_Pnd_Settl_Amt = pnd_Fcpanzc2_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpanzc2_Pnd_Settl_Amt", "#SETTL-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpanzc2_Pnd_Dvdnd_Amt = pnd_Fcpanzc2_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpanzc2_Pnd_Dvdnd_Amt", "#DVDND-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpanzc2_Pnd_Dci_Amt = pnd_Fcpanzc2_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpanzc2_Pnd_Dci_Amt", "#DCI-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpanzc2_Pnd_Dpi_Amt = pnd_Fcpanzc2_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpanzc2_Pnd_Dpi_Amt", "#DPI-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpanzc2_Pnd_Net_Pymnt_Amt = pnd_Fcpanzc2_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpanzc2_Pnd_Net_Pymnt_Amt", "#NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpanzc2_Pnd_Fdrl_Tax_Amt = pnd_Fcpanzc2_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpanzc2_Pnd_Fdrl_Tax_Amt", "#FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpanzc2_Pnd_State_Tax_Amt = pnd_Fcpanzc2_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpanzc2_Pnd_State_Tax_Amt", "#STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpanzc2_Pnd_Local_Tax_Amt = pnd_Fcpanzc2_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpanzc2_Pnd_Local_Tax_Amt", "#LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpanzc2_Pnd_Rec_Cnt = pnd_Fcpanzc2_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpanzc2_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1,50));

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpanzc2(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

