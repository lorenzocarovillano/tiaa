/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:58:58 PM
**        * FROM NATURAL LDA     : FCPL123
************************************************************
**        * FILE NAME            : LdaFcpl123.java
**        * CLASS NAME           : LdaFcpl123
**        * INSTANCE NAME        : LdaFcpl123
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl123 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Warrant_Extract;
    private DbsGroup pnd_Warrant_Extract_Pnd_Warrant_Grp1;
    private DbsGroup pnd_Warrant_Extract_Pnd_Warrant_Key;
    private DbsField pnd_Warrant_Extract_Cntrct_Orgn_Cde;
    private DbsField pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Warrant_Extract_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Check_Dte;
    private DbsField pnd_Warrant_Extract_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pnd_Warrant_Extract_Pymnt_Prcss_Seq_NbrRedef1;
    private DbsField pnd_Warrant_Extract_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Warrant_Extract_Pymnt_Instmt_Nbr;
    private DbsField pnd_Warrant_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Warrant_Extract_Pnd_Record_Type;
    private DbsField pnd_Warrant_Extract_Pymnt_Instmt;
    private DbsField pnd_Warrant_Extract_Pnd_Warrant_Detail;
    private DbsGroup pnd_Warrant_Extract_Pnd_Warrant_DetailRedef2;
    private DbsGroup pnd_Warrant_Extract_Pnd_Pymnt_Addr;
    private DbsField filler01;
    private DbsField pnd_Warrant_Extract_Cntrct_Payee_Cde;
    private DbsField pnd_Warrant_Extract_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Warrant_Extract_Cntrct_Cref_Nbr;
    private DbsField pnd_Warrant_Extract_Cntrct_Type_Cde;
    private DbsField pnd_Warrant_Extract_Cntrct_Lob_Cde;
    private DbsField pnd_Warrant_Extract_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Warrant_Extract_Cntrct_Mode_Cde;
    private DbsField pnd_Warrant_Extract_Pymnt_Ded_Grp_Top;
    private DbsGroup pnd_Warrant_Extract_Pymnt_Ded_Grp;
    private DbsField pnd_Warrant_Extract_Pymnt_Ded_Cde;
    private DbsField pnd_Warrant_Extract_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Warrant_Extract_Pymnt_Ded_Amt;
    private DbsField pnd_Warrant_Extract_Pymnt_Check_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Warrant_Extract_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Warrant_Extract_Annt_Ctznshp_Cde;
    private DbsGroup pnd_Warrant_Extract_Annt_Ctznshp_CdeRedef3;
    private DbsField pnd_Warrant_Extract_Annt_Ctznshp_Cde_A;
    private DbsField pnd_Warrant_Extract_Annt_Rsdncy_Cde;
    private DbsField pnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Check_Amt;
    private DbsField pnd_Warrant_Extract_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Warrant_Extract_Pymnt_Acctg_Dte;
    private DbsField pnd_Warrant_Extract_Pymnt_Intrfce_Dte;
    private DbsGroup pnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp;
    private DbsField pnd_Warrant_Extract_Pymnt_Nme;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Eft_Transit_Id;
    private DbsField pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind;
    private DbsField pnd_Warrant_Extract_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Warrant_Extract_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Warrant_Extract_C_Inv_Acct;
    private DbsField pnd_Warrant_Extract_Notused2;
    private DbsGroup pnd_Warrant_Extract_Notused2Redef4;
    private DbsField pnd_Warrant_Extract_Pymnt_Wrrnt_Trans_Type;
    private DbsGroup pnd_Warrant_Extract_Notused2Redef5;
    private DbsField pnd_Warrant_Extract_Pymnt_Commuted_Value;
    private DbsGroup pnd_Warrant_Extract_Pnd_Warrant_DetailRedef6;
    private DbsGroup pnd_Warrant_Extract_Pnd_Ledger_Record;
    private DbsField pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr;
    private DbsField pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind;
    private DbsField pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Amt;
    private DbsField pnd_Warrant_Extract_Pnd_Inv_Acct_Cde;
    private DbsGroup pnd_Warrant_Extract_Pnd_Inv_Info;
    private DbsField pnd_Warrant_Extract_Pnd_Inv_Detail;
    private DbsGroup pnd_Warrant_Extract_Pnd_Inv_DetailRedef7;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Valuat_Period;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Cde;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Unit_Qty;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Unit_Value;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Settl_Amt;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Dci_Amt;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Warrant_Extract_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Warrant_Extract_Inv_Acct_Exp_Amt;
    private DbsField pnd_Warrant_Extract_Pnd_Pnd_Inv_Acct_Ovr_Pay_Ded_Amt;

    public DbsGroup getPnd_Warrant_Extract() { return pnd_Warrant_Extract; }

    public DbsGroup getPnd_Warrant_Extract_Pnd_Warrant_Grp1() { return pnd_Warrant_Extract_Pnd_Warrant_Grp1; }

    public DbsGroup getPnd_Warrant_Extract_Pnd_Warrant_Key() { return pnd_Warrant_Extract_Pnd_Warrant_Key; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Orgn_Cde() { return pnd_Warrant_Extract_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Unq_Id_Nbr() { return pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Reqst_Log_Dte_Time() { return pnd_Warrant_Extract_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Ppcn_Nbr() { return pnd_Warrant_Extract_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Check_Dte() { return pnd_Warrant_Extract_Pymnt_Check_Dte; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Prcss_Seq_Nbr() { return pnd_Warrant_Extract_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPnd_Warrant_Extract_Pymnt_Prcss_Seq_NbrRedef1() { return pnd_Warrant_Extract_Pymnt_Prcss_Seq_NbrRedef1; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Prcss_Seq_Num() { return pnd_Warrant_Extract_Pymnt_Prcss_Seq_Num; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Instmt_Nbr() { return pnd_Warrant_Extract_Pymnt_Instmt_Nbr; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pnd_Warrant_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPnd_Warrant_Extract_Pnd_Record_Type() { return pnd_Warrant_Extract_Pnd_Record_Type; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Instmt() { return pnd_Warrant_Extract_Pymnt_Instmt; }

    public DbsField getPnd_Warrant_Extract_Pnd_Warrant_Detail() { return pnd_Warrant_Extract_Pnd_Warrant_Detail; }

    public DbsGroup getPnd_Warrant_Extract_Pnd_Warrant_DetailRedef2() { return pnd_Warrant_Extract_Pnd_Warrant_DetailRedef2; }

    public DbsGroup getPnd_Warrant_Extract_Pnd_Pymnt_Addr() { return pnd_Warrant_Extract_Pnd_Pymnt_Addr; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Payee_Cde() { return pnd_Warrant_Extract_Cntrct_Payee_Cde; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Check_Crrncy_Cde() { return pnd_Warrant_Extract_Cntrct_Check_Crrncy_Cde; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Cref_Nbr() { return pnd_Warrant_Extract_Cntrct_Cref_Nbr; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Type_Cde() { return pnd_Warrant_Extract_Cntrct_Type_Cde; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Lob_Cde() { return pnd_Warrant_Extract_Cntrct_Lob_Cde; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Annty_Ins_Type() { return pnd_Warrant_Extract_Cntrct_Annty_Ins_Type; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Mode_Cde() { return pnd_Warrant_Extract_Cntrct_Mode_Cde; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Ded_Grp_Top() { return pnd_Warrant_Extract_Pymnt_Ded_Grp_Top; }

    public DbsGroup getPnd_Warrant_Extract_Pymnt_Ded_Grp() { return pnd_Warrant_Extract_Pymnt_Ded_Grp; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Ded_Cde() { return pnd_Warrant_Extract_Pymnt_Ded_Cde; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Ded_Payee_Cde() { return pnd_Warrant_Extract_Pymnt_Ded_Payee_Cde; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Ded_Amt() { return pnd_Warrant_Extract_Pymnt_Ded_Amt; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Check_Nbr() { return pnd_Warrant_Extract_Pymnt_Check_Nbr; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Check_Scrty_Nbr() { return pnd_Warrant_Extract_Pymnt_Check_Scrty_Nbr; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Payee_Tx_Elct_Trggr() { return pnd_Warrant_Extract_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getPnd_Warrant_Extract_Annt_Soc_Sec_Nbr() { return pnd_Warrant_Extract_Annt_Soc_Sec_Nbr; }

    public DbsField getPnd_Warrant_Extract_Annt_Ctznshp_Cde() { return pnd_Warrant_Extract_Annt_Ctznshp_Cde; }

    public DbsGroup getPnd_Warrant_Extract_Annt_Ctznshp_CdeRedef3() { return pnd_Warrant_Extract_Annt_Ctznshp_CdeRedef3; }

    public DbsField getPnd_Warrant_Extract_Annt_Ctznshp_Cde_A() { return pnd_Warrant_Extract_Annt_Ctznshp_Cde_A; }

    public DbsField getPnd_Warrant_Extract_Annt_Rsdncy_Cde() { return pnd_Warrant_Extract_Annt_Rsdncy_Cde; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind() { return pnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Check_Seq_Nbr() { return pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Check_Amt() { return pnd_Warrant_Extract_Pymnt_Check_Amt; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Settlmnt_Dte() { return pnd_Warrant_Extract_Pymnt_Settlmnt_Dte; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Acctg_Dte() { return pnd_Warrant_Extract_Pymnt_Acctg_Dte; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Intrfce_Dte() { return pnd_Warrant_Extract_Pymnt_Intrfce_Dte; }

    public DbsGroup getPnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp() { return pnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Nme() { return pnd_Warrant_Extract_Pymnt_Nme; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Addr_Line1_Txt() { return pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Addr_Line2_Txt() { return pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Addr_Line3_Txt() { return pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Addr_Line4_Txt() { return pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Addr_Line5_Txt() { return pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Addr_Line6_Txt() { return pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Eft_Transit_Id() { return pnd_Warrant_Extract_Pymnt_Eft_Transit_Id; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr() { return pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Chk_Sav_Ind() { return pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Pymnt_Type_Ind() { return pnd_Warrant_Extract_Cntrct_Pymnt_Type_Ind; }

    public DbsField getPnd_Warrant_Extract_Cntrct_Sttlmnt_Type_Ind() { return pnd_Warrant_Extract_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getPnd_Warrant_Extract_C_Inv_Acct() { return pnd_Warrant_Extract_C_Inv_Acct; }

    public DbsField getPnd_Warrant_Extract_Notused2() { return pnd_Warrant_Extract_Notused2; }

    public DbsGroup getPnd_Warrant_Extract_Notused2Redef4() { return pnd_Warrant_Extract_Notused2Redef4; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Wrrnt_Trans_Type() { return pnd_Warrant_Extract_Pymnt_Wrrnt_Trans_Type; }

    public DbsGroup getPnd_Warrant_Extract_Notused2Redef5() { return pnd_Warrant_Extract_Notused2Redef5; }

    public DbsField getPnd_Warrant_Extract_Pymnt_Commuted_Value() { return pnd_Warrant_Extract_Pymnt_Commuted_Value; }

    public DbsGroup getPnd_Warrant_Extract_Pnd_Warrant_DetailRedef6() { return pnd_Warrant_Extract_Pnd_Warrant_DetailRedef6; }

    public DbsGroup getPnd_Warrant_Extract_Pnd_Ledger_Record() { return pnd_Warrant_Extract_Pnd_Ledger_Record; }

    public DbsField getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr() { return pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr; }

    public DbsField getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind() { return pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind; }

    public DbsField getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Amt() { return pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Amt; }

    public DbsField getPnd_Warrant_Extract_Pnd_Inv_Acct_Cde() { return pnd_Warrant_Extract_Pnd_Inv_Acct_Cde; }

    public DbsGroup getPnd_Warrant_Extract_Pnd_Inv_Info() { return pnd_Warrant_Extract_Pnd_Inv_Info; }

    public DbsField getPnd_Warrant_Extract_Pnd_Inv_Detail() { return pnd_Warrant_Extract_Pnd_Inv_Detail; }

    public DbsGroup getPnd_Warrant_Extract_Pnd_Inv_DetailRedef7() { return pnd_Warrant_Extract_Pnd_Inv_DetailRedef7; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Valuat_Period() { return pnd_Warrant_Extract_Inv_Acct_Valuat_Period; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Cde() { return pnd_Warrant_Extract_Inv_Acct_Cde; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Unit_Qty() { return pnd_Warrant_Extract_Inv_Acct_Unit_Qty; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Unit_Value() { return pnd_Warrant_Extract_Inv_Acct_Unit_Value; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Settl_Amt() { return pnd_Warrant_Extract_Inv_Acct_Settl_Amt; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Dvdnd_Amt() { return pnd_Warrant_Extract_Inv_Acct_Dvdnd_Amt; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Ivc_Amt() { return pnd_Warrant_Extract_Inv_Acct_Ivc_Amt; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Dci_Amt() { return pnd_Warrant_Extract_Inv_Acct_Dci_Amt; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Dpi_Amt() { return pnd_Warrant_Extract_Inv_Acct_Dpi_Amt; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Net_Pymnt_Amt() { return pnd_Warrant_Extract_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Fdrl_Tax_Amt() { return pnd_Warrant_Extract_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_State_Tax_Amt() { return pnd_Warrant_Extract_Inv_Acct_State_Tax_Amt; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Local_Tax_Amt() { return pnd_Warrant_Extract_Inv_Acct_Local_Tax_Amt; }

    public DbsField getPnd_Warrant_Extract_Inv_Acct_Exp_Amt() { return pnd_Warrant_Extract_Inv_Acct_Exp_Amt; }

    public DbsField getPnd_Warrant_Extract_Pnd_Pnd_Inv_Acct_Ovr_Pay_Ded_Amt() { return pnd_Warrant_Extract_Pnd_Pnd_Inv_Acct_Ovr_Pay_Ded_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Warrant_Extract = newGroupInRecord("pnd_Warrant_Extract", "#WARRANT-EXTRACT");
        pnd_Warrant_Extract_Pnd_Warrant_Grp1 = pnd_Warrant_Extract.newGroupInGroup("pnd_Warrant_Extract_Pnd_Warrant_Grp1", "#WARRANT-GRP1");
        pnd_Warrant_Extract_Pnd_Warrant_Key = pnd_Warrant_Extract_Pnd_Warrant_Grp1.newGroupInGroup("pnd_Warrant_Extract_Pnd_Warrant_Key", "#WARRANT-KEY");
        pnd_Warrant_Extract_Cntrct_Orgn_Cde = pnd_Warrant_Extract_Pnd_Warrant_Key.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr = pnd_Warrant_Extract_Pnd_Warrant_Key.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Warrant_Extract_Pymnt_Reqst_Log_Dte_Time = pnd_Warrant_Extract_Pnd_Warrant_Key.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Reqst_Log_Dte_Time", 
            "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_Warrant_Extract_Cntrct_Ppcn_Nbr = pnd_Warrant_Extract_Pnd_Warrant_Key.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Warrant_Extract_Pymnt_Check_Dte = pnd_Warrant_Extract_Pnd_Warrant_Key.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", 
            FieldType.DATE);
        pnd_Warrant_Extract_Pymnt_Prcss_Seq_Nbr = pnd_Warrant_Extract_Pnd_Warrant_Key.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Warrant_Extract_Pymnt_Prcss_Seq_NbrRedef1 = pnd_Warrant_Extract_Pnd_Warrant_Key.newGroupInGroup("pnd_Warrant_Extract_Pymnt_Prcss_Seq_NbrRedef1", 
            "Redefines", pnd_Warrant_Extract_Pymnt_Prcss_Seq_Nbr);
        pnd_Warrant_Extract_Pymnt_Prcss_Seq_Num = pnd_Warrant_Extract_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Prcss_Seq_Num", 
            "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Warrant_Extract_Pymnt_Instmt_Nbr = pnd_Warrant_Extract_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Warrant_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Warrant_Extract_Pnd_Warrant_Key.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        pnd_Warrant_Extract_Pnd_Record_Type = pnd_Warrant_Extract_Pnd_Warrant_Key.newFieldInGroup("pnd_Warrant_Extract_Pnd_Record_Type", "#RECORD-TYPE", 
            FieldType.NUMERIC, 1);
        pnd_Warrant_Extract_Pymnt_Instmt = pnd_Warrant_Extract_Pnd_Warrant_Grp1.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Instmt", "PYMNT-INSTMT", FieldType.NUMERIC, 
            2);
        pnd_Warrant_Extract_Pnd_Warrant_Detail = pnd_Warrant_Extract.newFieldArrayInGroup("pnd_Warrant_Extract_Pnd_Warrant_Detail", "#WARRANT-DETAIL", 
            FieldType.STRING, 1, new DbsArrayController(1,792));
        pnd_Warrant_Extract_Pnd_Warrant_DetailRedef2 = pnd_Warrant_Extract.newGroupInGroup("pnd_Warrant_Extract_Pnd_Warrant_DetailRedef2", "Redefines", 
            pnd_Warrant_Extract_Pnd_Warrant_Detail);
        pnd_Warrant_Extract_Pnd_Pymnt_Addr = pnd_Warrant_Extract_Pnd_Warrant_DetailRedef2.newGroupInGroup("pnd_Warrant_Extract_Pnd_Pymnt_Addr", "#PYMNT-ADDR");
        filler01 = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 16);
        pnd_Warrant_Extract_Cntrct_Payee_Cde = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Warrant_Extract_Cntrct_Check_Crrncy_Cde = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Check_Crrncy_Cde", 
            "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Warrant_Extract_Cntrct_Cref_Nbr = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", 
            FieldType.STRING, 10);
        pnd_Warrant_Extract_Cntrct_Type_Cde = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Warrant_Extract_Cntrct_Lob_Cde = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Warrant_Extract_Cntrct_Annty_Ins_Type = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", 
            FieldType.STRING, 1);
        pnd_Warrant_Extract_Cntrct_Mode_Cde = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Warrant_Extract_Pymnt_Ded_Grp_Top = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Ded_Grp_Top", "PYMNT-DED-GRP-TOP", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Warrant_Extract_Pymnt_Ded_Grp = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newGroupInGroup("pnd_Warrant_Extract_Pymnt_Ded_Grp", "PYMNT-DED-GRP");
        pnd_Warrant_Extract_Pymnt_Ded_Cde = pnd_Warrant_Extract_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Warrant_Extract_Pymnt_Ded_Cde", "PYMNT-DED-CDE", 
            FieldType.NUMERIC, 3, new DbsArrayController(1,10));
        pnd_Warrant_Extract_Pymnt_Ded_Payee_Cde = pnd_Warrant_Extract_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Warrant_Extract_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8, new DbsArrayController(1,10));
        pnd_Warrant_Extract_Pymnt_Ded_Amt = pnd_Warrant_Extract_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Warrant_Extract_Pymnt_Ded_Amt", "PYMNT-DED-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,10));
        pnd_Warrant_Extract_Pymnt_Check_Nbr = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Warrant_Extract_Pymnt_Check_Scrty_Nbr = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Warrant_Extract_Pymnt_Payee_Tx_Elct_Trggr = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Payee_Tx_Elct_Trggr", 
            "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        pnd_Warrant_Extract_Annt_Soc_Sec_Nbr = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Warrant_Extract_Annt_Ctznshp_Cde = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Warrant_Extract_Annt_Ctznshp_CdeRedef3 = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newGroupInGroup("pnd_Warrant_Extract_Annt_Ctznshp_CdeRedef3", 
            "Redefines", pnd_Warrant_Extract_Annt_Ctznshp_Cde);
        pnd_Warrant_Extract_Annt_Ctznshp_Cde_A = pnd_Warrant_Extract_Annt_Ctznshp_CdeRedef3.newFieldInGroup("pnd_Warrant_Extract_Annt_Ctznshp_Cde_A", 
            "ANNT-CTZNSHP-CDE-A", FieldType.STRING, 2);
        pnd_Warrant_Extract_Annt_Rsdncy_Cde = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind", 
            "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Warrant_Extract_Pymnt_Check_Amt = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Warrant_Extract_Pymnt_Settlmnt_Dte = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);
        pnd_Warrant_Extract_Pymnt_Acctg_Dte = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", 
            FieldType.DATE);
        pnd_Warrant_Extract_Pymnt_Intrfce_Dte = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", 
            FieldType.DATE);
        pnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newGroupArrayInGroup("pnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp", 
            "PYMNT-NME-AND-ADDR-GRP", new DbsArrayController(1,2));
        pnd_Warrant_Extract_Pymnt_Nme = pnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 
            38);
        pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt = pnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt", 
            "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 35);
        pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt = pnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt", 
            "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 35);
        pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt = pnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt", 
            "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 35);
        pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt = pnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt", 
            "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 35);
        pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt = pnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt", 
            "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 35);
        pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt = pnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt", 
            "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 35);
        pnd_Warrant_Extract_Pymnt_Eft_Transit_Id = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", 
            FieldType.NUMERIC, 9);
        pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", 
            FieldType.STRING, 21);
        pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", 
            FieldType.STRING, 1);
        pnd_Warrant_Extract_Cntrct_Pymnt_Type_Ind = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Warrant_Extract_Cntrct_Sttlmnt_Type_Ind = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Cntrct_Sttlmnt_Type_Ind", 
            "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Warrant_Extract_C_Inv_Acct = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_C_Inv_Acct", "C-INV-ACCT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Warrant_Extract_Notused2 = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newFieldInGroup("pnd_Warrant_Extract_Notused2", "NOTUSED2", FieldType.STRING, 
            8);
        pnd_Warrant_Extract_Notused2Redef4 = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newGroupInGroup("pnd_Warrant_Extract_Notused2Redef4", "Redefines", pnd_Warrant_Extract_Notused2);
        pnd_Warrant_Extract_Pymnt_Wrrnt_Trans_Type = pnd_Warrant_Extract_Notused2Redef4.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Wrrnt_Trans_Type", 
            "PYMNT-WRRNT-TRANS-TYPE", FieldType.STRING, 2);
        pnd_Warrant_Extract_Notused2Redef5 = pnd_Warrant_Extract_Pnd_Pymnt_Addr.newGroupInGroup("pnd_Warrant_Extract_Notused2Redef5", "Redefines", pnd_Warrant_Extract_Notused2);
        pnd_Warrant_Extract_Pymnt_Commuted_Value = pnd_Warrant_Extract_Notused2Redef5.newFieldInGroup("pnd_Warrant_Extract_Pymnt_Commuted_Value", "PYMNT-COMMUTED-VALUE", 
            FieldType.STRING, 2);
        pnd_Warrant_Extract_Pnd_Warrant_DetailRedef6 = pnd_Warrant_Extract.newGroupInGroup("pnd_Warrant_Extract_Pnd_Warrant_DetailRedef6", "Redefines", 
            pnd_Warrant_Extract_Pnd_Warrant_Detail);
        pnd_Warrant_Extract_Pnd_Ledger_Record = pnd_Warrant_Extract_Pnd_Warrant_DetailRedef6.newGroupInGroup("pnd_Warrant_Extract_Pnd_Ledger_Record", 
            "#LEDGER-RECORD");
        pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr = pnd_Warrant_Extract_Pnd_Ledger_Record.newFieldInGroup("pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr", 
            "#INV-ACCT-LEDGR-NBR", FieldType.STRING, 15);
        pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind = pnd_Warrant_Extract_Pnd_Ledger_Record.newFieldInGroup("pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind", 
            "#INV-ACCT-LEDGR-IND", FieldType.STRING, 1);
        pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Amt = pnd_Warrant_Extract_Pnd_Ledger_Record.newFieldInGroup("pnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Amt", 
            "#INV-ACCT-LEDGR-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Warrant_Extract_Pnd_Inv_Acct_Cde = pnd_Warrant_Extract_Pnd_Ledger_Record.newFieldInGroup("pnd_Warrant_Extract_Pnd_Inv_Acct_Cde", "#INV-ACCT-CDE", 
            FieldType.STRING, 2);
        pnd_Warrant_Extract_Pnd_Inv_Info = pnd_Warrant_Extract.newGroupArrayInGroup("pnd_Warrant_Extract_Pnd_Inv_Info", "#INV-INFO", new DbsArrayController(1,
            40));
        pnd_Warrant_Extract_Pnd_Inv_Detail = pnd_Warrant_Extract_Pnd_Inv_Info.newFieldInGroup("pnd_Warrant_Extract_Pnd_Inv_Detail", "#INV-DETAIL", FieldType.STRING, 
            70);
        pnd_Warrant_Extract_Pnd_Inv_DetailRedef7 = pnd_Warrant_Extract_Pnd_Inv_Info.newGroupInGroup("pnd_Warrant_Extract_Pnd_Inv_DetailRedef7", "Redefines", 
            pnd_Warrant_Extract_Pnd_Inv_Detail);
        pnd_Warrant_Extract_Inv_Acct_Valuat_Period = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Valuat_Period", 
            "INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 1);
        pnd_Warrant_Extract_Inv_Acct_Cde = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Cde", "INV-ACCT-CDE", 
            FieldType.STRING, 2);
        pnd_Warrant_Extract_Inv_Acct_Unit_Qty = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", 
            FieldType.PACKED_DECIMAL, 9,3);
        pnd_Warrant_Extract_Inv_Acct_Unit_Value = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Unit_Value", 
            "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 9,4);
        pnd_Warrant_Extract_Inv_Acct_Settl_Amt = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Warrant_Extract_Inv_Acct_Dvdnd_Amt = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Warrant_Extract_Inv_Acct_Ivc_Amt = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Warrant_Extract_Inv_Acct_Dci_Amt = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Warrant_Extract_Inv_Acct_Dpi_Amt = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Warrant_Extract_Inv_Acct_Net_Pymnt_Amt = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Net_Pymnt_Amt", 
            "INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Warrant_Extract_Inv_Acct_Fdrl_Tax_Amt = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Fdrl_Tax_Amt", 
            "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Warrant_Extract_Inv_Acct_State_Tax_Amt = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_State_Tax_Amt", 
            "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Warrant_Extract_Inv_Acct_Local_Tax_Amt = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Local_Tax_Amt", 
            "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Warrant_Extract_Inv_Acct_Exp_Amt = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Warrant_Extract_Pnd_Pnd_Inv_Acct_Ovr_Pay_Ded_Amt = pnd_Warrant_Extract_Pnd_Inv_DetailRedef7.newFieldInGroup("pnd_Warrant_Extract_Pnd_Pnd_Inv_Acct_Ovr_Pay_Ded_Amt", 
            "##INV-ACCT-OVR-PAY-DED-AMT", FieldType.PACKED_DECIMAL, 9,2);

        this.setRecordName("LdaFcpl123");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl123() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
