/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:25 PM
**        * FROM NATURAL PDA     : CPOA110
************************************************************
**        * FILE NAME            : PdaCpoa110.java
**        * CLASS NAME           : PdaCpoa110
**        * INSTANCE NAME        : PdaCpoa110
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCpoa110 extends PdaBase
{
    // Properties
    private DbsGroup cpoa110;
    private DbsGroup cpoa110_Cpoa110a;
    private DbsField cpoa110_Cpoa110_Source_Code;
    private DbsField cpoa110_Cpoa110_Function;
    private DbsField cpoa110_Cpoa110_New_Seq_Nbr;
    private DbsField cpoa110_Filler1;
    private DbsGroup cpoa110_Cpoa110b;
    private DbsField cpoa110_Cpoa110_Return_Code;
    private DbsField cpoa110_Cpoa110_Return_Msg;
    private DbsField cpoa110_Rt_Table_Id;
    private DbsField cpoa110_Rt_A_I_Ind;
    private DbsField cpoa110_Rt_Short_Key;
    private DbsField cpoa110_Rt_Long_Key;
    private DbsGroup cpoa110_Bank_Account_Data;
    private DbsField cpoa110_Bank_Source_Code;
    private DbsField cpoa110_Bank_Routing;
    private DbsField cpoa110_Bank_Account;
    private DbsField cpoa110_Bank_Account_Name;
    private DbsField cpoa110_Bank_Account_Cde;
    private DbsField cpoa110_Bank_Account_Desc;
    private DbsField cpoa110_Bank_Business_Unit;
    private DbsField cpoa110_Bank_Format;
    private DbsField cpoa110_Bank_Transmit_Ind;
    private DbsField cpoa110_Check_Acct_Ind;
    private DbsField cpoa110_Eft_Acct_Ind;
    private DbsField cpoa110_Next_Seq;
    private DbsField cpoa110_Start_Seq;
    private DbsField cpoa110_End_Seq;
    private DbsField cpoa110_Seq_Maint_Ind;
    private DbsField cpoa110_Hops_Acct_Ind;
    private DbsField cpoa110_Bank_Ledger_Nbr;
    private DbsField cpoa110_Bank_Signature_Cde;
    private DbsField cpoa110_Bank_Void_Msg;
    private DbsField cpoa110_Bank_Orgn_Cde;
    private DbsField cpoa110_Bank_Internal_Pospay_Ind;
    private DbsField cpoa110_Help_Desk_Location;
    private DbsField cpoa110_Filler2;
    private DbsGroup cpoa110_Filler2Redef1;
    private DbsField cpoa110_Bank_Eft_Service_Class;
    private DbsField cpoa110_Bank_Eft_Company_Name;
    private DbsField cpoa110_Bank_Eft_Company_Discretion_Data;
    private DbsField cpoa110_Bank_Eft_Company_Entry_Desc;
    private DbsField cpoa110_Bank_Eft_Originator_Id;
    private DbsField cpoa110_Filler_2;
    private DbsGroup cpoa110_Bank_Logo_Data;
    private DbsField cpoa110_Bank_Logo_Key;
    private DbsField cpoa110_Bank_Logo_Name1;
    private DbsField cpoa110_Bank_Logo_Name2;
    private DbsField cpoa110_Bank_Logo_Address1;
    private DbsField cpoa110_Bank_Logo_Address2;
    private DbsField cpoa110_Bank_Short_Logo_Name;
    private DbsField cpoa110_Bank_Graphic_Logo_Cde;
    private DbsField cpoa110_Filler3;
    private DbsGroup cpoa110_Filler3Redef2;
    private DbsField cpoa110_Filler_3;
    private DbsGroup cpoa110_Bank_Name_Data;
    private DbsField cpoa110_Bank_Name;
    private DbsField cpoa110_Bank_Address1;
    private DbsField cpoa110_Bank_Address2;
    private DbsField cpoa110_Bank_Above_Check_Amt_Nbr;
    private DbsField cpoa110_Bank_Eft_Trans_Exists;
    private DbsField cpoa110_Bank_Pos_Pay_Trans_Exists;
    private DbsField cpoa110_Bank_Transmission_Cde;
    private DbsField cpoa110_Filler4;
    private DbsGroup cpoa110_Filler4Redef3;
    private DbsField cpoa110_Bank_Eft_Immediate_Origin;
    private DbsField cpoa110_Bank_Eft_File_Id_Modifier;
    private DbsField cpoa110_Bank_Eft_Destination;
    private DbsField cpoa110_Bank_Eft_Origin;
    private DbsField cpoa110_Filler_4;

    public DbsGroup getCpoa110() { return cpoa110; }

    public DbsGroup getCpoa110_Cpoa110a() { return cpoa110_Cpoa110a; }

    public DbsField getCpoa110_Cpoa110_Source_Code() { return cpoa110_Cpoa110_Source_Code; }

    public DbsField getCpoa110_Cpoa110_Function() { return cpoa110_Cpoa110_Function; }

    public DbsField getCpoa110_Cpoa110_New_Seq_Nbr() { return cpoa110_Cpoa110_New_Seq_Nbr; }

    public DbsField getCpoa110_Filler1() { return cpoa110_Filler1; }

    public DbsGroup getCpoa110_Cpoa110b() { return cpoa110_Cpoa110b; }

    public DbsField getCpoa110_Cpoa110_Return_Code() { return cpoa110_Cpoa110_Return_Code; }

    public DbsField getCpoa110_Cpoa110_Return_Msg() { return cpoa110_Cpoa110_Return_Msg; }

    public DbsField getCpoa110_Rt_Table_Id() { return cpoa110_Rt_Table_Id; }

    public DbsField getCpoa110_Rt_A_I_Ind() { return cpoa110_Rt_A_I_Ind; }

    public DbsField getCpoa110_Rt_Short_Key() { return cpoa110_Rt_Short_Key; }

    public DbsField getCpoa110_Rt_Long_Key() { return cpoa110_Rt_Long_Key; }

    public DbsGroup getCpoa110_Bank_Account_Data() { return cpoa110_Bank_Account_Data; }

    public DbsField getCpoa110_Bank_Source_Code() { return cpoa110_Bank_Source_Code; }

    public DbsField getCpoa110_Bank_Routing() { return cpoa110_Bank_Routing; }

    public DbsField getCpoa110_Bank_Account() { return cpoa110_Bank_Account; }

    public DbsField getCpoa110_Bank_Account_Name() { return cpoa110_Bank_Account_Name; }

    public DbsField getCpoa110_Bank_Account_Cde() { return cpoa110_Bank_Account_Cde; }

    public DbsField getCpoa110_Bank_Account_Desc() { return cpoa110_Bank_Account_Desc; }

    public DbsField getCpoa110_Bank_Business_Unit() { return cpoa110_Bank_Business_Unit; }

    public DbsField getCpoa110_Bank_Format() { return cpoa110_Bank_Format; }

    public DbsField getCpoa110_Bank_Transmit_Ind() { return cpoa110_Bank_Transmit_Ind; }

    public DbsField getCpoa110_Check_Acct_Ind() { return cpoa110_Check_Acct_Ind; }

    public DbsField getCpoa110_Eft_Acct_Ind() { return cpoa110_Eft_Acct_Ind; }

    public DbsField getCpoa110_Next_Seq() { return cpoa110_Next_Seq; }

    public DbsField getCpoa110_Start_Seq() { return cpoa110_Start_Seq; }

    public DbsField getCpoa110_End_Seq() { return cpoa110_End_Seq; }

    public DbsField getCpoa110_Seq_Maint_Ind() { return cpoa110_Seq_Maint_Ind; }

    public DbsField getCpoa110_Hops_Acct_Ind() { return cpoa110_Hops_Acct_Ind; }

    public DbsField getCpoa110_Bank_Ledger_Nbr() { return cpoa110_Bank_Ledger_Nbr; }

    public DbsField getCpoa110_Bank_Signature_Cde() { return cpoa110_Bank_Signature_Cde; }

    public DbsField getCpoa110_Bank_Void_Msg() { return cpoa110_Bank_Void_Msg; }

    public DbsField getCpoa110_Bank_Orgn_Cde() { return cpoa110_Bank_Orgn_Cde; }

    public DbsField getCpoa110_Bank_Internal_Pospay_Ind() { return cpoa110_Bank_Internal_Pospay_Ind; }

    public DbsField getCpoa110_Help_Desk_Location() { return cpoa110_Help_Desk_Location; }

    public DbsField getCpoa110_Filler2() { return cpoa110_Filler2; }

    public DbsGroup getCpoa110_Filler2Redef1() { return cpoa110_Filler2Redef1; }

    public DbsField getCpoa110_Bank_Eft_Service_Class() { return cpoa110_Bank_Eft_Service_Class; }

    public DbsField getCpoa110_Bank_Eft_Company_Name() { return cpoa110_Bank_Eft_Company_Name; }

    public DbsField getCpoa110_Bank_Eft_Company_Discretion_Data() { return cpoa110_Bank_Eft_Company_Discretion_Data; }

    public DbsField getCpoa110_Bank_Eft_Company_Entry_Desc() { return cpoa110_Bank_Eft_Company_Entry_Desc; }

    public DbsField getCpoa110_Bank_Eft_Originator_Id() { return cpoa110_Bank_Eft_Originator_Id; }

    public DbsField getCpoa110_Filler_2() { return cpoa110_Filler_2; }

    public DbsGroup getCpoa110_Bank_Logo_Data() { return cpoa110_Bank_Logo_Data; }

    public DbsField getCpoa110_Bank_Logo_Key() { return cpoa110_Bank_Logo_Key; }

    public DbsField getCpoa110_Bank_Logo_Name1() { return cpoa110_Bank_Logo_Name1; }

    public DbsField getCpoa110_Bank_Logo_Name2() { return cpoa110_Bank_Logo_Name2; }

    public DbsField getCpoa110_Bank_Logo_Address1() { return cpoa110_Bank_Logo_Address1; }

    public DbsField getCpoa110_Bank_Logo_Address2() { return cpoa110_Bank_Logo_Address2; }

    public DbsField getCpoa110_Bank_Short_Logo_Name() { return cpoa110_Bank_Short_Logo_Name; }

    public DbsField getCpoa110_Bank_Graphic_Logo_Cde() { return cpoa110_Bank_Graphic_Logo_Cde; }

    public DbsField getCpoa110_Filler3() { return cpoa110_Filler3; }

    public DbsGroup getCpoa110_Filler3Redef2() { return cpoa110_Filler3Redef2; }

    public DbsField getCpoa110_Filler_3() { return cpoa110_Filler_3; }

    public DbsGroup getCpoa110_Bank_Name_Data() { return cpoa110_Bank_Name_Data; }

    public DbsField getCpoa110_Bank_Name() { return cpoa110_Bank_Name; }

    public DbsField getCpoa110_Bank_Address1() { return cpoa110_Bank_Address1; }

    public DbsField getCpoa110_Bank_Address2() { return cpoa110_Bank_Address2; }

    public DbsField getCpoa110_Bank_Above_Check_Amt_Nbr() { return cpoa110_Bank_Above_Check_Amt_Nbr; }

    public DbsField getCpoa110_Bank_Eft_Trans_Exists() { return cpoa110_Bank_Eft_Trans_Exists; }

    public DbsField getCpoa110_Bank_Pos_Pay_Trans_Exists() { return cpoa110_Bank_Pos_Pay_Trans_Exists; }

    public DbsField getCpoa110_Bank_Transmission_Cde() { return cpoa110_Bank_Transmission_Cde; }

    public DbsField getCpoa110_Filler4() { return cpoa110_Filler4; }

    public DbsGroup getCpoa110_Filler4Redef3() { return cpoa110_Filler4Redef3; }

    public DbsField getCpoa110_Bank_Eft_Immediate_Origin() { return cpoa110_Bank_Eft_Immediate_Origin; }

    public DbsField getCpoa110_Bank_Eft_File_Id_Modifier() { return cpoa110_Bank_Eft_File_Id_Modifier; }

    public DbsField getCpoa110_Bank_Eft_Destination() { return cpoa110_Bank_Eft_Destination; }

    public DbsField getCpoa110_Bank_Eft_Origin() { return cpoa110_Bank_Eft_Origin; }

    public DbsField getCpoa110_Filler_4() { return cpoa110_Filler_4; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cpoa110 = dbsRecord.newGroupInRecord("cpoa110", "CPOA110");
        cpoa110.setParameterOption(ParameterOption.ByReference);
        cpoa110_Cpoa110a = cpoa110.newGroupInGroup("cpoa110_Cpoa110a", "CPOA110A");
        cpoa110_Cpoa110_Source_Code = cpoa110_Cpoa110a.newFieldInGroup("cpoa110_Cpoa110_Source_Code", "CPOA110-SOURCE-CODE", FieldType.STRING, 5);
        cpoa110_Cpoa110_Function = cpoa110_Cpoa110a.newFieldInGroup("cpoa110_Cpoa110_Function", "CPOA110-FUNCTION", FieldType.STRING, 15);
        cpoa110_Cpoa110_New_Seq_Nbr = cpoa110_Cpoa110a.newFieldInGroup("cpoa110_Cpoa110_New_Seq_Nbr", "CPOA110-NEW-SEQ-NBR", FieldType.NUMERIC, 10);
        cpoa110_Filler1 = cpoa110_Cpoa110a.newFieldInGroup("cpoa110_Filler1", "FILLER1", FieldType.STRING, 70);
        cpoa110_Cpoa110b = cpoa110.newGroupInGroup("cpoa110_Cpoa110b", "CPOA110B");
        cpoa110_Cpoa110_Return_Code = cpoa110_Cpoa110b.newFieldInGroup("cpoa110_Cpoa110_Return_Code", "CPOA110-RETURN-CODE", FieldType.STRING, 2);
        cpoa110_Cpoa110_Return_Msg = cpoa110_Cpoa110b.newFieldInGroup("cpoa110_Cpoa110_Return_Msg", "CPOA110-RETURN-MSG", FieldType.STRING, 100);
        cpoa110_Rt_Table_Id = cpoa110_Cpoa110b.newFieldInGroup("cpoa110_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5);
        cpoa110_Rt_A_I_Ind = cpoa110_Cpoa110b.newFieldInGroup("cpoa110_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1);
        cpoa110_Rt_Short_Key = cpoa110_Cpoa110b.newFieldInGroup("cpoa110_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20);
        cpoa110_Rt_Long_Key = cpoa110_Cpoa110b.newFieldInGroup("cpoa110_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40);
        cpoa110_Bank_Account_Data = cpoa110_Cpoa110b.newGroupInGroup("cpoa110_Bank_Account_Data", "BANK-ACCOUNT-DATA");
        cpoa110_Bank_Source_Code = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Source_Code", "BANK-SOURCE-CODE", FieldType.STRING, 5);
        cpoa110_Bank_Routing = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Routing", "BANK-ROUTING", FieldType.STRING, 9);
        cpoa110_Bank_Account = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21);
        cpoa110_Bank_Account_Name = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Account_Name", "BANK-ACCOUNT-NAME", FieldType.STRING, 50);
        cpoa110_Bank_Account_Cde = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Account_Cde", "BANK-ACCOUNT-CDE", FieldType.STRING, 10);
        cpoa110_Bank_Account_Desc = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Account_Desc", "BANK-ACCOUNT-DESC", FieldType.STRING, 50);
        cpoa110_Bank_Business_Unit = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Business_Unit", "BANK-BUSINESS-UNIT", FieldType.STRING, 5);
        cpoa110_Bank_Format = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Format", "BANK-FORMAT", FieldType.STRING, 5);
        cpoa110_Bank_Transmit_Ind = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Transmit_Ind", "BANK-TRANSMIT-IND", FieldType.STRING, 1);
        cpoa110_Check_Acct_Ind = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Check_Acct_Ind", "CHECK-ACCT-IND", FieldType.STRING, 1);
        cpoa110_Eft_Acct_Ind = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Eft_Acct_Ind", "EFT-ACCT-IND", FieldType.STRING, 1);
        cpoa110_Next_Seq = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Next_Seq", "NEXT-SEQ", FieldType.NUMERIC, 10);
        cpoa110_Start_Seq = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Start_Seq", "START-SEQ", FieldType.NUMERIC, 10);
        cpoa110_End_Seq = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_End_Seq", "END-SEQ", FieldType.NUMERIC, 10);
        cpoa110_Seq_Maint_Ind = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Seq_Maint_Ind", "SEQ-MAINT-IND", FieldType.STRING, 1);
        cpoa110_Hops_Acct_Ind = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Hops_Acct_Ind", "HOPS-ACCT-IND", FieldType.STRING, 1);
        cpoa110_Bank_Ledger_Nbr = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Ledger_Nbr", "BANK-LEDGER-NBR", FieldType.STRING, 8);
        cpoa110_Bank_Signature_Cde = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Signature_Cde", "BANK-SIGNATURE-CDE", FieldType.STRING, 5);
        cpoa110_Bank_Void_Msg = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Void_Msg", "BANK-VOID-MSG", FieldType.STRING, 30);
        cpoa110_Bank_Orgn_Cde = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Orgn_Cde", "BANK-ORGN-CDE", FieldType.STRING, 2);
        cpoa110_Bank_Internal_Pospay_Ind = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Bank_Internal_Pospay_Ind", "BANK-INTERNAL-POSPAY-IND", FieldType.STRING, 
            1);
        cpoa110_Help_Desk_Location = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Help_Desk_Location", "HELP-DESK-LOCATION", FieldType.STRING, 5);
        cpoa110_Filler2 = cpoa110_Bank_Account_Data.newFieldInGroup("cpoa110_Filler2", "FILLER2", FieldType.STRING, 59);
        cpoa110_Filler2Redef1 = cpoa110_Bank_Account_Data.newGroupInGroup("cpoa110_Filler2Redef1", "Redefines", cpoa110_Filler2);
        cpoa110_Bank_Eft_Service_Class = cpoa110_Filler2Redef1.newFieldInGroup("cpoa110_Bank_Eft_Service_Class", "BANK-EFT-SERVICE-CLASS", FieldType.STRING, 
            3);
        cpoa110_Bank_Eft_Company_Name = cpoa110_Filler2Redef1.newFieldInGroup("cpoa110_Bank_Eft_Company_Name", "BANK-EFT-COMPANY-NAME", FieldType.STRING, 
            16);
        cpoa110_Bank_Eft_Company_Discretion_Data = cpoa110_Filler2Redef1.newFieldInGroup("cpoa110_Bank_Eft_Company_Discretion_Data", "BANK-EFT-COMPANY-DISCRETION-DATA", 
            FieldType.STRING, 20);
        cpoa110_Bank_Eft_Company_Entry_Desc = cpoa110_Filler2Redef1.newFieldInGroup("cpoa110_Bank_Eft_Company_Entry_Desc", "BANK-EFT-COMPANY-ENTRY-DESC", 
            FieldType.STRING, 10);
        cpoa110_Bank_Eft_Originator_Id = cpoa110_Filler2Redef1.newFieldInGroup("cpoa110_Bank_Eft_Originator_Id", "BANK-EFT-ORIGINATOR-ID", FieldType.STRING, 
            8);
        cpoa110_Filler_2 = cpoa110_Filler2Redef1.newFieldInGroup("cpoa110_Filler_2", "FILLER-2", FieldType.STRING, 2);
        cpoa110_Bank_Logo_Data = cpoa110_Cpoa110b.newGroupInGroup("cpoa110_Bank_Logo_Data", "BANK-LOGO-DATA");
        cpoa110_Bank_Logo_Key = cpoa110_Bank_Logo_Data.newFieldInGroup("cpoa110_Bank_Logo_Key", "BANK-LOGO-KEY", FieldType.STRING, 10);
        cpoa110_Bank_Logo_Name1 = cpoa110_Bank_Logo_Data.newFieldInGroup("cpoa110_Bank_Logo_Name1", "BANK-LOGO-NAME1", FieldType.STRING, 50);
        cpoa110_Bank_Logo_Name2 = cpoa110_Bank_Logo_Data.newFieldInGroup("cpoa110_Bank_Logo_Name2", "BANK-LOGO-NAME2", FieldType.STRING, 50);
        cpoa110_Bank_Logo_Address1 = cpoa110_Bank_Logo_Data.newFieldInGroup("cpoa110_Bank_Logo_Address1", "BANK-LOGO-ADDRESS1", FieldType.STRING, 50);
        cpoa110_Bank_Logo_Address2 = cpoa110_Bank_Logo_Data.newFieldInGroup("cpoa110_Bank_Logo_Address2", "BANK-LOGO-ADDRESS2", FieldType.STRING, 50);
        cpoa110_Bank_Short_Logo_Name = cpoa110_Bank_Logo_Data.newFieldInGroup("cpoa110_Bank_Short_Logo_Name", "BANK-SHORT-LOGO-NAME", FieldType.STRING, 
            15);
        cpoa110_Bank_Graphic_Logo_Cde = cpoa110_Bank_Logo_Data.newFieldInGroup("cpoa110_Bank_Graphic_Logo_Cde", "BANK-GRAPHIC-LOGO-CDE", FieldType.STRING, 
            5);
        cpoa110_Filler3 = cpoa110_Bank_Logo_Data.newFieldInGroup("cpoa110_Filler3", "FILLER3", FieldType.STRING, 80);
        cpoa110_Filler3Redef2 = cpoa110_Bank_Logo_Data.newGroupInGroup("cpoa110_Filler3Redef2", "Redefines", cpoa110_Filler3);
        cpoa110_Filler_3 = cpoa110_Filler3Redef2.newFieldInGroup("cpoa110_Filler_3", "FILLER-3", FieldType.STRING, 80);
        cpoa110_Bank_Name_Data = cpoa110_Cpoa110b.newGroupInGroup("cpoa110_Bank_Name_Data", "BANK-NAME-DATA");
        cpoa110_Bank_Name = cpoa110_Bank_Name_Data.newFieldInGroup("cpoa110_Bank_Name", "BANK-NAME", FieldType.STRING, 50);
        cpoa110_Bank_Address1 = cpoa110_Bank_Name_Data.newFieldInGroup("cpoa110_Bank_Address1", "BANK-ADDRESS1", FieldType.STRING, 50);
        cpoa110_Bank_Address2 = cpoa110_Bank_Name_Data.newFieldInGroup("cpoa110_Bank_Address2", "BANK-ADDRESS2", FieldType.STRING, 50);
        cpoa110_Bank_Above_Check_Amt_Nbr = cpoa110_Bank_Name_Data.newFieldInGroup("cpoa110_Bank_Above_Check_Amt_Nbr", "BANK-ABOVE-CHECK-AMT-NBR", FieldType.STRING, 
            10);
        cpoa110_Bank_Eft_Trans_Exists = cpoa110_Bank_Name_Data.newFieldInGroup("cpoa110_Bank_Eft_Trans_Exists", "BANK-EFT-TRANS-EXISTS", FieldType.STRING, 
            1);
        cpoa110_Bank_Pos_Pay_Trans_Exists = cpoa110_Bank_Name_Data.newFieldInGroup("cpoa110_Bank_Pos_Pay_Trans_Exists", "BANK-POS-PAY-TRANS-EXISTS", FieldType.STRING, 
            1);
        cpoa110_Bank_Transmission_Cde = cpoa110_Bank_Name_Data.newFieldInGroup("cpoa110_Bank_Transmission_Cde", "BANK-TRANSMISSION-CDE", FieldType.STRING, 
            5);
        cpoa110_Filler4 = cpoa110_Bank_Name_Data.newFieldInGroup("cpoa110_Filler4", "FILLER4", FieldType.STRING, 133);
        cpoa110_Filler4Redef3 = cpoa110_Bank_Name_Data.newGroupInGroup("cpoa110_Filler4Redef3", "Redefines", cpoa110_Filler4);
        cpoa110_Bank_Eft_Immediate_Origin = cpoa110_Filler4Redef3.newFieldInGroup("cpoa110_Bank_Eft_Immediate_Origin", "BANK-EFT-IMMEDIATE-ORIGIN", FieldType.STRING, 
            10);
        cpoa110_Bank_Eft_File_Id_Modifier = cpoa110_Filler4Redef3.newFieldInGroup("cpoa110_Bank_Eft_File_Id_Modifier", "BANK-EFT-FILE-ID-MODIFIER", FieldType.STRING, 
            1);
        cpoa110_Bank_Eft_Destination = cpoa110_Filler4Redef3.newFieldInGroup("cpoa110_Bank_Eft_Destination", "BANK-EFT-DESTINATION", FieldType.STRING, 
            23);
        cpoa110_Bank_Eft_Origin = cpoa110_Filler4Redef3.newFieldInGroup("cpoa110_Bank_Eft_Origin", "BANK-EFT-ORIGIN", FieldType.STRING, 23);
        cpoa110_Filler_4 = cpoa110_Filler4Redef3.newFieldInGroup("cpoa110_Filler_4", "FILLER-4", FieldType.STRING, 76);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCpoa110(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

