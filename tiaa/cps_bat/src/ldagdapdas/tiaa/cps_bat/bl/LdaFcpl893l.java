/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:18 PM
**        * FROM NATURAL LDA     : FCPL893L
************************************************************
**        * FILE NAME            : LdaFcpl893l.java
**        * CLASS NAME           : LdaFcpl893l
**        * INSTANCE NAME        : LdaFcpl893l
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl893l extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl893l;
    private DbsField pnd_Fcpl893l_Pnd_Deductions_Loaded;
    private DbsField pnd_Fcpl893l_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Fcpl893l_Annt_Rsdncy_Cde;
    private DbsField pnd_Fcpl893l_Pnd_Ded_Ledgend_Table;
    private DbsField pnd_Fcpl893l_Pnd_State_Table_Top;
    private DbsField pnd_Fcpl893l_Pnd_State_Cde_Table;
    private DbsField pnd_Fcpl893l_Pnd_State_Table;

    public DbsGroup getPnd_Fcpl893l() { return pnd_Fcpl893l; }

    public DbsField getPnd_Fcpl893l_Pnd_Deductions_Loaded() { return pnd_Fcpl893l_Pnd_Deductions_Loaded; }

    public DbsField getPnd_Fcpl893l_Pymnt_Payee_Tx_Elct_Trggr() { return pnd_Fcpl893l_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getPnd_Fcpl893l_Annt_Rsdncy_Cde() { return pnd_Fcpl893l_Annt_Rsdncy_Cde; }

    public DbsField getPnd_Fcpl893l_Pnd_Ded_Ledgend_Table() { return pnd_Fcpl893l_Pnd_Ded_Ledgend_Table; }

    public DbsField getPnd_Fcpl893l_Pnd_State_Table_Top() { return pnd_Fcpl893l_Pnd_State_Table_Top; }

    public DbsField getPnd_Fcpl893l_Pnd_State_Cde_Table() { return pnd_Fcpl893l_Pnd_State_Cde_Table; }

    public DbsField getPnd_Fcpl893l_Pnd_State_Table() { return pnd_Fcpl893l_Pnd_State_Table; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl893l = newGroupInRecord("pnd_Fcpl893l", "#FCPL893L");
        pnd_Fcpl893l_Pnd_Deductions_Loaded = pnd_Fcpl893l.newFieldInGroup("pnd_Fcpl893l_Pnd_Deductions_Loaded", "#DEDUCTIONS-LOADED", FieldType.BOOLEAN);
        pnd_Fcpl893l_Pymnt_Payee_Tx_Elct_Trggr = pnd_Fcpl893l.newFieldInGroup("pnd_Fcpl893l_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 
            1);
        pnd_Fcpl893l_Annt_Rsdncy_Cde = pnd_Fcpl893l.newFieldInGroup("pnd_Fcpl893l_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        pnd_Fcpl893l_Pnd_Ded_Ledgend_Table = pnd_Fcpl893l.newFieldArrayInGroup("pnd_Fcpl893l_Pnd_Ded_Ledgend_Table", "#DED-LEDGEND-TABLE", FieldType.STRING, 
            39, new DbsArrayController(1,13));
        pnd_Fcpl893l_Pnd_State_Table_Top = pnd_Fcpl893l.newFieldInGroup("pnd_Fcpl893l_Pnd_State_Table_Top", "#STATE-TABLE-TOP", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Fcpl893l_Pnd_State_Cde_Table = pnd_Fcpl893l.newFieldArrayInGroup("pnd_Fcpl893l_Pnd_State_Cde_Table", "#STATE-CDE-TABLE", FieldType.STRING, 
            2, new DbsArrayController(1,57));
        pnd_Fcpl893l_Pnd_State_Table = pnd_Fcpl893l.newFieldArrayInGroup("pnd_Fcpl893l_Pnd_State_Table", "#STATE-TABLE", FieldType.STRING, 34, new DbsArrayController(1,
            57));

        this.setRecordName("LdaFcpl893l");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl893l() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
