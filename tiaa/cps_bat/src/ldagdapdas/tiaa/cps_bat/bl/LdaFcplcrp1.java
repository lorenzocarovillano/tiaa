/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:32 PM
**        * FROM NATURAL LDA     : FCPLCRP1
************************************************************
**        * FILE NAME            : LdaFcplcrp1.java
**        * CLASS NAME           : LdaFcplcrp1
**        * INSTANCE NAME        : LdaFcplcrp1
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplcrp1 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcplcrp1;
    private DbsGroup pnd_Fcplcrp1_Pnd_Cntl_Table;
    private DbsField pnd_Fcplcrp1_Pnd_Rec_Cnt;
    private DbsField pnd_Fcplcrp1_Pnd_Pymnt_Cnt;
    private DbsField pnd_Fcplcrp1_Pnd_Settl_Amt;
    private DbsField pnd_Fcplcrp1_Pnd_Net_Pymnt_Amt;

    public DbsGroup getPnd_Fcplcrp1() { return pnd_Fcplcrp1; }

    public DbsGroup getPnd_Fcplcrp1_Pnd_Cntl_Table() { return pnd_Fcplcrp1_Pnd_Cntl_Table; }

    public DbsField getPnd_Fcplcrp1_Pnd_Rec_Cnt() { return pnd_Fcplcrp1_Pnd_Rec_Cnt; }

    public DbsField getPnd_Fcplcrp1_Pnd_Pymnt_Cnt() { return pnd_Fcplcrp1_Pnd_Pymnt_Cnt; }

    public DbsField getPnd_Fcplcrp1_Pnd_Settl_Amt() { return pnd_Fcplcrp1_Pnd_Settl_Amt; }

    public DbsField getPnd_Fcplcrp1_Pnd_Net_Pymnt_Amt() { return pnd_Fcplcrp1_Pnd_Net_Pymnt_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcplcrp1 = newGroupInRecord("pnd_Fcplcrp1", "#FCPLCRP1");
        pnd_Fcplcrp1_Pnd_Cntl_Table = pnd_Fcplcrp1.newGroupArrayInGroup("pnd_Fcplcrp1_Pnd_Cntl_Table", "#CNTL-TABLE", new DbsArrayController(1,3));
        pnd_Fcplcrp1_Pnd_Rec_Cnt = pnd_Fcplcrp1_Pnd_Cntl_Table.newFieldArrayInGroup("pnd_Fcplcrp1_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 9, 
            new DbsArrayController(1,50));
        pnd_Fcplcrp1_Pnd_Pymnt_Cnt = pnd_Fcplcrp1_Pnd_Cntl_Table.newFieldArrayInGroup("pnd_Fcplcrp1_Pnd_Pymnt_Cnt", "#PYMNT-CNT", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1,50));
        pnd_Fcplcrp1_Pnd_Settl_Amt = pnd_Fcplcrp1_Pnd_Cntl_Table.newFieldArrayInGroup("pnd_Fcplcrp1_Pnd_Settl_Amt", "#SETTL-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcplcrp1_Pnd_Net_Pymnt_Amt = pnd_Fcplcrp1_Pnd_Cntl_Table.newFieldArrayInGroup("pnd_Fcplcrp1_Pnd_Net_Pymnt_Amt", "#NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));

        this.setRecordName("LdaFcplcrp1");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcplcrp1() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
