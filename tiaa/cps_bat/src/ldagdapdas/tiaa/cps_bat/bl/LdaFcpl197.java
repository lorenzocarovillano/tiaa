/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:04 PM
**        * FROM NATURAL LDA     : FCPL197
************************************************************
**        * FILE NAME            : LdaFcpl197.java
**        * CLASS NAME           : LdaFcpl197
**        * INSTANCE NAME        : LdaFcpl197
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl197 extends DbsRecord
{
    // Properties
    private DbsField pnd_User_Group;
    private DbsField pnd_User_Group_Desc;

    public DbsField getPnd_User_Group() { return pnd_User_Group; }

    public DbsField getPnd_User_Group_Desc() { return pnd_User_Group_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_User_Group = newFieldArrayInRecord("pnd_User_Group", "#USER-GROUP", FieldType.STRING, 2, new DbsArrayController(1,40));

        pnd_User_Group_Desc = newFieldArrayInRecord("pnd_User_Group_Desc", "#USER-GROUP-DESC", FieldType.STRING, 17, new DbsArrayController(1,40));

        this.setRecordName("LdaFcpl197");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_User_Group.getValue(1).setInitialValue("AA");
        pnd_User_Group.getValue(2).setInitialValue("AO");
        pnd_User_Group.getValue(3).setInitialValue("AP");
        pnd_User_Group.getValue(4).setInitialValue("BC");
        pnd_User_Group.getValue(5).setInitialValue("BO");
        pnd_User_Group.getValue(6).setInitialValue("BT");
        pnd_User_Group.getValue(7).setInitialValue("CO");
        pnd_User_Group.getValue(8).setInitialValue("CS");
        pnd_User_Group.getValue(9).setInitialValue("GA");
        pnd_User_Group.getValue(10).setInitialValue("PC");
        pnd_User_Group.getValue(11).setInitialValue("PD");
        pnd_User_Group.getValue(12).setInitialValue("RB");
        pnd_User_Group.getValue(13).setInitialValue("SB");
        pnd_User_Group.getValue(14).setInitialValue("SS");
        pnd_User_Group.getValue(15).setInitialValue("TX");
        pnd_User_Group.getValue(16).setInitialValue("UB");
        pnd_User_Group.getValue(17).setInitialValue("UC");
        pnd_User_Group.getValue(18).setInitialValue("UD");
        pnd_User_Group.getValue(19).setInitialValue("UE");
        pnd_User_Group.getValue(20).setInitialValue("UF");
        pnd_User_Group.getValue(21).setInitialValue("UG");
        pnd_User_Group.getValue(22).setInitialValue("UH");
        pnd_User_Group.getValue(23).setInitialValue("UI");
        pnd_User_Group.getValue(24).setInitialValue("UJ");
        pnd_User_Group.getValue(25).setInitialValue("UK");
        pnd_User_Group.getValue(26).setInitialValue("UL");
        pnd_User_Group.getValue(27).setInitialValue("UM");
        pnd_User_Group.getValue(28).setInitialValue("TS");
        pnd_User_Group.getValue(29).setInitialValue("MX");
        pnd_User_Group.getValue(30).setInitialValue("AL");
        pnd_User_Group_Desc.getValue(1).setInitialValue("Account*Analysis");
        pnd_User_Group_Desc.getValue(2).setInitialValue("Actuarial*Ops");
        pnd_User_Group_Desc.getValue(3).setInitialValue("Annuity*Premium");
        pnd_User_Group_Desc.getValue(4).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(5).setInitialValue("Bnft*Ctrl*-*Other");
        pnd_User_Group_Desc.getValue(6).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(7).setInitialValue("Corr/Contrct*Adm");
        pnd_User_Group_Desc.getValue(8).setInitialValue("Counseling*Svs");
        pnd_User_Group_Desc.getValue(9).setInitialValue("Grp*Annuity*Act");
        pnd_User_Group_Desc.getValue(10).setInitialValue("Payment*Control");
        pnd_User_Group_Desc.getValue(11).setInitialValue("Payment*Distrib");
        pnd_User_Group_Desc.getValue(12).setInitialValue("Retrmnt*Benefits");
        pnd_User_Group_Desc.getValue(13).setInitialValue("Survivr*Benefits");
        pnd_User_Group_Desc.getValue(14).setInitialValue("Single*Sum");
        pnd_User_Group_Desc.getValue(15).setInitialValue("Tax");
        pnd_User_Group_Desc.getValue(16).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(17).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(18).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(19).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(20).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(21).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(22).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(23).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(24).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(25).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(26).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(27).setInitialValue("Benefit*Control");
        pnd_User_Group_Desc.getValue(28).setInitialValue("Single*Sum");
        pnd_User_Group_Desc.getValue(29).setInitialValue("Minimum*Distrib");
        pnd_User_Group_Desc.getValue(30).setInitialValue("Loan*Unit");
    }

    // Constructor
    public LdaFcpl197() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
