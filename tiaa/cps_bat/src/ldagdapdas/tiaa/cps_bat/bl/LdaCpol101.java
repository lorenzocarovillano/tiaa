/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:11 PM
**        * FROM NATURAL LDA     : CPOL101
************************************************************
**        * FILE NAME            : LdaCpol101.java
**        * CLASS NAME           : LdaCpol101
**        * INSTANCE NAME        : LdaCpol101
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpol101 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_ref;
    private DbsField ref_Count_Castrt_Description;
    private DbsGroup ref_Rt_Record;
    private DbsField ref_Rt_A_I_Ind;
    private DbsField ref_Rt_Table_Id;
    private DbsField ref_Rt_Short_Key;
    private DbsGroup ref_Rt_Short_KeyRedef1;
    private DbsField ref_Cps_Key;
    private DbsField filler01;
    private DbsField ref_Cps_Inverse_Dte;
    private DbsField ref_Rt_Long_Key;
    private DbsGroup ref_Rt_Long_KeyRedef2;
    private DbsField ref_Sequence_Nbr;
    private DbsField ref_Rt_Desc1;
    private DbsGroup ref_Rt_Desc1Redef3;
    private DbsField ref_Cps_Ccyymmdd;
    private DbsField filler02;
    private DbsField ref_Cps_Date;
    private DbsGroup ref_Rt_DescriptionMuGroup;
    private DbsField ref_Rt_Description;
    private DbsGroup ref_Rt_DescriptionRedef4;
    private DbsGroup ref_Gr_Desc;
    private DbsField ref_Cps_Time;
    private DbsField ref_Cps_Cycle;
    private DbsField ref_Cps_Status;
    private DbsField ref_Cps_Check_Cnt;
    private DbsField ref_Cps_Check_Amt;
    private DbsField ref_Cps_Eft_Cnt;
    private DbsField ref_Cps_Eft_Amt;
    private DbsField ref_Cps_Other_Cnt;
    private DbsField ref_Cps_Other_Amt;
    private DbsField ref_Cps_Cs_Cnt;
    private DbsField ref_Cps_Cs_Amt;
    private DbsField ref_Cps_Redraw_Cnt;
    private DbsField ref_Cps_Redraw_Amt;
    private DbsField ref_Cps_Unused_Cnt;
    private DbsField ref_Cps_Unused_Amt;
    private DbsField ref_Pnd_Filler;
    private DbsField ref_Cps_Restart;
    private DbsField ref_Rt_Eff_From_Ccyymmdd;
    private DbsField ref_Rt_Eff_To_Ccyymmdd;
    private DbsField ref_Rt_Upd_Source;
    private DbsField ref_Rt_Upd_User;
    private DbsField ref_Rt_Upd_Ccyymmdd;
    private DbsField ref_Rt_Upd_Timn;
    private DataAccessProgramView vw_rtu;
    private DbsGroup rtu_Rt_Record;
    private DbsField rtu_Rt_A_I_Ind;
    private DbsField rtu_Rt_Table_Id;
    private DbsField rtu_Rt_Short_Key;
    private DbsField rtu_Rt_Long_Key;
    private DbsField rtu_Rt_Desc1;
    private DbsGroup rtu_Rt_Desc1Redef5;
    private DbsField rtu_Cps_Ccyymmdd;
    private DbsField filler03;
    private DbsField rtu_Cps_Date;
    private DbsField rtu_Rt_Desc2;
    private DbsField rtu_Rt_Desc3;
    private DbsField rtu_Rt_Desc4;
    private DbsField rtu_Rt_Desc5;
    private DbsGroup rtu_Rt_DescriptionMuGroup;
    private DbsField rtu_Rt_Description;
    private DbsField rtu_Rt_Eff_From_Ccyymmdd;
    private DbsField rtu_Rt_Eff_To_Ccyymmdd;
    private DbsField rtu_Rt_Upd_Source;
    private DbsField rtu_Rt_Upd_User;
    private DbsField rtu_Rt_Upd_Ccyymmdd;
    private DbsField rtu_Rt_Upd_Timn;
    private DbsField pnd_Rt_Description;
    private DbsGroup pnd_Rt_DescriptionRedef6;
    private DbsField pnd_Rt_Description_Pnd_Cps_Time;
    private DbsField pnd_Rt_Description_Pnd_Cps_Cycle;
    private DbsField pnd_Rt_Description_Pnd_Cps_Status;
    private DbsField pnd_Rt_Description_Pnd_Cps_Check_Cnt;
    private DbsField pnd_Rt_Description_Pnd_Cps_Check_Amt;
    private DbsField pnd_Rt_Description_Pnd_Cps_Eft_Cnt;
    private DbsField pnd_Rt_Description_Pnd_Cps_Eft_Amt;
    private DbsField pnd_Rt_Description_Pnd_Cps_Other_Cnt;
    private DbsField pnd_Rt_Description_Pnd_Cps_Other_Amt;
    private DbsField pnd_Rt_Description_Pnd_Cps_Cs_Cnt;
    private DbsField pnd_Rt_Description_Pnd_Cps_Cs_Amt;
    private DbsField pnd_Rt_Description_Pnd_Cps_Redraw_Cnt;
    private DbsField pnd_Rt_Description_Pnd_Cps_Redraw_Amt;
    private DbsField pnd_Rt_Description_Pnd_Cps_Unused_Cnt;
    private DbsField pnd_Rt_Description_Pnd_Cps_Unused_Amt;
    private DbsField filler04;
    private DbsField pnd_Rt_Description_Pnd_Cps_Restart;

    public DataAccessProgramView getVw_ref() { return vw_ref; }

    public DbsField getRef_Count_Castrt_Description() { return ref_Count_Castrt_Description; }

    public DbsGroup getRef_Rt_Record() { return ref_Rt_Record; }

    public DbsField getRef_Rt_A_I_Ind() { return ref_Rt_A_I_Ind; }

    public DbsField getRef_Rt_Table_Id() { return ref_Rt_Table_Id; }

    public DbsField getRef_Rt_Short_Key() { return ref_Rt_Short_Key; }

    public DbsGroup getRef_Rt_Short_KeyRedef1() { return ref_Rt_Short_KeyRedef1; }

    public DbsField getRef_Cps_Key() { return ref_Cps_Key; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getRef_Cps_Inverse_Dte() { return ref_Cps_Inverse_Dte; }

    public DbsField getRef_Rt_Long_Key() { return ref_Rt_Long_Key; }

    public DbsGroup getRef_Rt_Long_KeyRedef2() { return ref_Rt_Long_KeyRedef2; }

    public DbsField getRef_Sequence_Nbr() { return ref_Sequence_Nbr; }

    public DbsField getRef_Rt_Desc1() { return ref_Rt_Desc1; }

    public DbsGroup getRef_Rt_Desc1Redef3() { return ref_Rt_Desc1Redef3; }

    public DbsField getRef_Cps_Ccyymmdd() { return ref_Cps_Ccyymmdd; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getRef_Cps_Date() { return ref_Cps_Date; }

    public DbsGroup getRef_Rt_DescriptionMuGroup() { return ref_Rt_DescriptionMuGroup; }

    public DbsField getRef_Rt_Description() { return ref_Rt_Description; }

    public DbsGroup getRef_Rt_DescriptionRedef4() { return ref_Rt_DescriptionRedef4; }

    public DbsGroup getRef_Gr_Desc() { return ref_Gr_Desc; }

    public DbsField getRef_Cps_Time() { return ref_Cps_Time; }

    public DbsField getRef_Cps_Cycle() { return ref_Cps_Cycle; }

    public DbsField getRef_Cps_Status() { return ref_Cps_Status; }

    public DbsField getRef_Cps_Check_Cnt() { return ref_Cps_Check_Cnt; }

    public DbsField getRef_Cps_Check_Amt() { return ref_Cps_Check_Amt; }

    public DbsField getRef_Cps_Eft_Cnt() { return ref_Cps_Eft_Cnt; }

    public DbsField getRef_Cps_Eft_Amt() { return ref_Cps_Eft_Amt; }

    public DbsField getRef_Cps_Other_Cnt() { return ref_Cps_Other_Cnt; }

    public DbsField getRef_Cps_Other_Amt() { return ref_Cps_Other_Amt; }

    public DbsField getRef_Cps_Cs_Cnt() { return ref_Cps_Cs_Cnt; }

    public DbsField getRef_Cps_Cs_Amt() { return ref_Cps_Cs_Amt; }

    public DbsField getRef_Cps_Redraw_Cnt() { return ref_Cps_Redraw_Cnt; }

    public DbsField getRef_Cps_Redraw_Amt() { return ref_Cps_Redraw_Amt; }

    public DbsField getRef_Cps_Unused_Cnt() { return ref_Cps_Unused_Cnt; }

    public DbsField getRef_Cps_Unused_Amt() { return ref_Cps_Unused_Amt; }

    public DbsField getRef_Pnd_Filler() { return ref_Pnd_Filler; }

    public DbsField getRef_Cps_Restart() { return ref_Cps_Restart; }

    public DbsField getRef_Rt_Eff_From_Ccyymmdd() { return ref_Rt_Eff_From_Ccyymmdd; }

    public DbsField getRef_Rt_Eff_To_Ccyymmdd() { return ref_Rt_Eff_To_Ccyymmdd; }

    public DbsField getRef_Rt_Upd_Source() { return ref_Rt_Upd_Source; }

    public DbsField getRef_Rt_Upd_User() { return ref_Rt_Upd_User; }

    public DbsField getRef_Rt_Upd_Ccyymmdd() { return ref_Rt_Upd_Ccyymmdd; }

    public DbsField getRef_Rt_Upd_Timn() { return ref_Rt_Upd_Timn; }

    public DataAccessProgramView getVw_rtu() { return vw_rtu; }

    public DbsGroup getRtu_Rt_Record() { return rtu_Rt_Record; }

    public DbsField getRtu_Rt_A_I_Ind() { return rtu_Rt_A_I_Ind; }

    public DbsField getRtu_Rt_Table_Id() { return rtu_Rt_Table_Id; }

    public DbsField getRtu_Rt_Short_Key() { return rtu_Rt_Short_Key; }

    public DbsField getRtu_Rt_Long_Key() { return rtu_Rt_Long_Key; }

    public DbsField getRtu_Rt_Desc1() { return rtu_Rt_Desc1; }

    public DbsGroup getRtu_Rt_Desc1Redef5() { return rtu_Rt_Desc1Redef5; }

    public DbsField getRtu_Cps_Ccyymmdd() { return rtu_Cps_Ccyymmdd; }

    public DbsField getFiller03() { return filler03; }

    public DbsField getRtu_Cps_Date() { return rtu_Cps_Date; }

    public DbsField getRtu_Rt_Desc2() { return rtu_Rt_Desc2; }

    public DbsField getRtu_Rt_Desc3() { return rtu_Rt_Desc3; }

    public DbsField getRtu_Rt_Desc4() { return rtu_Rt_Desc4; }

    public DbsField getRtu_Rt_Desc5() { return rtu_Rt_Desc5; }

    public DbsGroup getRtu_Rt_DescriptionMuGroup() { return rtu_Rt_DescriptionMuGroup; }

    public DbsField getRtu_Rt_Description() { return rtu_Rt_Description; }

    public DbsField getRtu_Rt_Eff_From_Ccyymmdd() { return rtu_Rt_Eff_From_Ccyymmdd; }

    public DbsField getRtu_Rt_Eff_To_Ccyymmdd() { return rtu_Rt_Eff_To_Ccyymmdd; }

    public DbsField getRtu_Rt_Upd_Source() { return rtu_Rt_Upd_Source; }

    public DbsField getRtu_Rt_Upd_User() { return rtu_Rt_Upd_User; }

    public DbsField getRtu_Rt_Upd_Ccyymmdd() { return rtu_Rt_Upd_Ccyymmdd; }

    public DbsField getRtu_Rt_Upd_Timn() { return rtu_Rt_Upd_Timn; }

    public DbsField getPnd_Rt_Description() { return pnd_Rt_Description; }

    public DbsGroup getPnd_Rt_DescriptionRedef6() { return pnd_Rt_DescriptionRedef6; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Time() { return pnd_Rt_Description_Pnd_Cps_Time; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Cycle() { return pnd_Rt_Description_Pnd_Cps_Cycle; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Status() { return pnd_Rt_Description_Pnd_Cps_Status; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Check_Cnt() { return pnd_Rt_Description_Pnd_Cps_Check_Cnt; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Check_Amt() { return pnd_Rt_Description_Pnd_Cps_Check_Amt; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Eft_Cnt() { return pnd_Rt_Description_Pnd_Cps_Eft_Cnt; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Eft_Amt() { return pnd_Rt_Description_Pnd_Cps_Eft_Amt; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Other_Cnt() { return pnd_Rt_Description_Pnd_Cps_Other_Cnt; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Other_Amt() { return pnd_Rt_Description_Pnd_Cps_Other_Amt; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Cs_Cnt() { return pnd_Rt_Description_Pnd_Cps_Cs_Cnt; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Cs_Amt() { return pnd_Rt_Description_Pnd_Cps_Cs_Amt; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Redraw_Cnt() { return pnd_Rt_Description_Pnd_Cps_Redraw_Cnt; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Redraw_Amt() { return pnd_Rt_Description_Pnd_Cps_Redraw_Amt; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Unused_Cnt() { return pnd_Rt_Description_Pnd_Cps_Unused_Cnt; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Unused_Amt() { return pnd_Rt_Description_Pnd_Cps_Unused_Amt; }

    public DbsField getFiller04() { return filler04; }

    public DbsField getPnd_Rt_Description_Pnd_Cps_Restart() { return pnd_Rt_Description_Pnd_Cps_Restart; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_ref = new DataAccessProgramView(new NameInfo("vw_ref", "REF"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        ref_Count_Castrt_Description = vw_ref.getRecord().newFieldInGroup("ref_Count_Castrt_Description", "C*RT-DESCRIPTION", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Record = vw_ref.getRecord().newGroupInGroup("ref_Rt_Record", "RT-RECORD");
        ref_Rt_A_I_Ind = ref_Rt_Record.newFieldInGroup("ref_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        ref_Rt_Table_Id = ref_Rt_Record.newFieldInGroup("ref_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        ref_Rt_Short_Key = ref_Rt_Record.newFieldInGroup("ref_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        ref_Rt_Short_KeyRedef1 = vw_ref.getRecord().newGroupInGroup("ref_Rt_Short_KeyRedef1", "Redefines", ref_Rt_Short_Key);
        ref_Cps_Key = ref_Rt_Short_KeyRedef1.newFieldInGroup("ref_Cps_Key", "CPS-KEY", FieldType.STRING, 11);
        filler01 = ref_Rt_Short_KeyRedef1.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 1);
        ref_Cps_Inverse_Dte = ref_Rt_Short_KeyRedef1.newFieldInGroup("ref_Cps_Inverse_Dte", "CPS-INVERSE-DTE", FieldType.STRING, 8);
        ref_Rt_Long_Key = ref_Rt_Record.newFieldInGroup("ref_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        ref_Rt_Long_KeyRedef2 = vw_ref.getRecord().newGroupInGroup("ref_Rt_Long_KeyRedef2", "Redefines", ref_Rt_Long_Key);
        ref_Sequence_Nbr = ref_Rt_Long_KeyRedef2.newFieldInGroup("ref_Sequence_Nbr", "SEQUENCE-NBR", FieldType.NUMERIC, 7);
        ref_Rt_Desc1 = ref_Rt_Record.newFieldInGroup("ref_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        ref_Rt_Desc1Redef3 = vw_ref.getRecord().newGroupInGroup("ref_Rt_Desc1Redef3", "Redefines", ref_Rt_Desc1);
        ref_Cps_Ccyymmdd = ref_Rt_Desc1Redef3.newFieldInGroup("ref_Cps_Ccyymmdd", "CPS-CCYYMMDD", FieldType.NUMERIC, 8);
        filler02 = ref_Rt_Desc1Redef3.newFieldInGroup("filler02", "FILLER", FieldType.STRING, 1);
        ref_Cps_Date = ref_Rt_Desc1Redef3.newFieldInGroup("ref_Cps_Date", "CPS-DATE", FieldType.DATE);
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionRedef4 = vw_ref.getRecord().newGroupInGroup("ref_Rt_DescriptionRedef4", "Redefines", ref_Rt_Description);
        ref_Gr_Desc = ref_Rt_DescriptionRedef4.newGroupArrayInGroup("ref_Gr_Desc", "GR-DESC", new DbsArrayController(1,30));
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_DescriptionMuGroup = ref_Rt_Record.newGroupInGroup("ref_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        ref_Rt_Description = ref_Rt_DescriptionMuGroup.newFieldArrayInGroup("ref_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,30), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        ref_Rt_Eff_From_Ccyymmdd = ref_Rt_Record.newFieldInGroup("ref_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_FROM_CCYYMMDD");
        ref_Rt_Eff_To_Ccyymmdd = ref_Rt_Record.newFieldInGroup("ref_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        ref_Rt_Upd_Source = ref_Rt_Record.newFieldInGroup("ref_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_SOURCE");
        ref_Rt_Upd_User = ref_Rt_Record.newFieldInGroup("ref_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_USER");
        ref_Rt_Upd_Ccyymmdd = ref_Rt_Record.newFieldInGroup("ref_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        ref_Rt_Upd_Timn = ref_Rt_Record.newFieldInGroup("ref_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RT_UPD_TIMN");

        vw_rtu = new DataAccessProgramView(new NameInfo("vw_rtu", "RTU"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        rtu_Rt_Record = vw_rtu.getRecord().newGroupInGroup("rtu_Rt_Record", "RT-RECORD");
        rtu_Rt_A_I_Ind = rtu_Rt_Record.newFieldInGroup("rtu_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rtu_Rt_Table_Id = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        rtu_Rt_Short_Key = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        rtu_Rt_Long_Key = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        rtu_Rt_Desc1 = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        rtu_Rt_Desc1Redef5 = vw_rtu.getRecord().newGroupInGroup("rtu_Rt_Desc1Redef5", "Redefines", rtu_Rt_Desc1);
        rtu_Cps_Ccyymmdd = rtu_Rt_Desc1Redef5.newFieldInGroup("rtu_Cps_Ccyymmdd", "CPS-CCYYMMDD", FieldType.NUMERIC, 8);
        filler03 = rtu_Rt_Desc1Redef5.newFieldInGroup("filler03", "FILLER", FieldType.STRING, 1);
        rtu_Cps_Date = rtu_Rt_Desc1Redef5.newFieldInGroup("rtu_Cps_Date", "CPS-DATE", FieldType.DATE);
        rtu_Rt_Desc2 = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        rtu_Rt_Desc3 = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        rtu_Rt_Desc4 = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        rtu_Rt_Desc5 = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        rtu_Rt_DescriptionMuGroup = rtu_Rt_Record.newGroupInGroup("rtu_Rt_DescriptionMuGroup", "RT_DESCRIPTIONMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "REFERNCE_TABLE_RT_DESCRIPTION");
        rtu_Rt_Description = rtu_Rt_DescriptionMuGroup.newFieldArrayInGroup("rtu_Rt_Description", "RT-DESCRIPTION", FieldType.STRING, 250, new DbsArrayController(1,2), 
            RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RT_DESCRIPTION");
        rtu_Rt_Eff_From_Ccyymmdd = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_FROM_CCYYMMDD");
        rtu_Rt_Eff_To_Ccyymmdd = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        rtu_Rt_Upd_Source = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_SOURCE");
        rtu_Rt_Upd_User = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_USER");
        rtu_Rt_Upd_Ccyymmdd = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        rtu_Rt_Upd_Timn = rtu_Rt_Record.newFieldInGroup("rtu_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RT_UPD_TIMN");

        pnd_Rt_Description = newFieldInRecord("pnd_Rt_Description", "#RT-DESCRIPTION", FieldType.STRING, 250);
        pnd_Rt_DescriptionRedef6 = newGroupInRecord("pnd_Rt_DescriptionRedef6", "Redefines", pnd_Rt_Description);
        pnd_Rt_Description_Pnd_Cps_Time = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Time", "#CPS-TIME", FieldType.TIME);
        pnd_Rt_Description_Pnd_Cps_Cycle = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Cycle", "#CPS-CYCLE", FieldType.NUMERIC, 
            4);
        pnd_Rt_Description_Pnd_Cps_Status = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Status", "#CPS-STATUS", FieldType.STRING, 
            20);
        pnd_Rt_Description_Pnd_Cps_Check_Cnt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Check_Cnt", "#CPS-CHECK-CNT", FieldType.NUMERIC, 
            10);
        pnd_Rt_Description_Pnd_Cps_Check_Amt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Check_Amt", "#CPS-CHECK-AMT", FieldType.DECIMAL, 
            25,2);
        pnd_Rt_Description_Pnd_Cps_Eft_Cnt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Eft_Cnt", "#CPS-EFT-CNT", FieldType.NUMERIC, 
            10);
        pnd_Rt_Description_Pnd_Cps_Eft_Amt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Eft_Amt", "#CPS-EFT-AMT", FieldType.DECIMAL, 
            25,2);
        pnd_Rt_Description_Pnd_Cps_Other_Cnt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Other_Cnt", "#CPS-OTHER-CNT", FieldType.NUMERIC, 
            10);
        pnd_Rt_Description_Pnd_Cps_Other_Amt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Other_Amt", "#CPS-OTHER-AMT", FieldType.DECIMAL, 
            25,2);
        pnd_Rt_Description_Pnd_Cps_Cs_Cnt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Cs_Cnt", "#CPS-CS-CNT", FieldType.NUMERIC, 
            10);
        pnd_Rt_Description_Pnd_Cps_Cs_Amt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Cs_Amt", "#CPS-CS-AMT", FieldType.DECIMAL, 
            25,2);
        pnd_Rt_Description_Pnd_Cps_Redraw_Cnt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Redraw_Cnt", "#CPS-REDRAW-CNT", FieldType.NUMERIC, 
            10);
        pnd_Rt_Description_Pnd_Cps_Redraw_Amt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Redraw_Amt", "#CPS-REDRAW-AMT", FieldType.DECIMAL, 
            25,2);
        pnd_Rt_Description_Pnd_Cps_Unused_Cnt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Unused_Cnt", "#CPS-UNUSED-CNT", FieldType.NUMERIC, 
            10);
        pnd_Rt_Description_Pnd_Cps_Unused_Amt = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Unused_Amt", "#CPS-UNUSED-AMT", FieldType.DECIMAL, 
            25,2);
        filler04 = pnd_Rt_DescriptionRedef6.newFieldInGroup("filler04", "FILLER", FieldType.STRING, 1);
        pnd_Rt_Description_Pnd_Cps_Restart = pnd_Rt_DescriptionRedef6.newFieldInGroup("pnd_Rt_Description_Pnd_Cps_Restart", "#CPS-RESTART", FieldType.STRING, 
            8);

        this.setRecordName("LdaCpol101");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_ref.reset();
        vw_rtu.reset();
    }

    // Constructor
    public LdaCpol101() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
