/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:29 PM
**        * FROM NATURAL PDA     : FCPAACUM
************************************************************
**        * FILE NAME            : PdaFcpaacum.java
**        * CLASS NAME           : PdaFcpaacum
**        * INSTANCE NAME        : PdaFcpaacum
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpaacum extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpaacum;
    private DbsField pnd_Fcpaacum_Pnd_Accum_Occur;
    private DbsField pnd_Fcpaacum_Pnd_At_At_Max_Fund;
    private DbsField pnd_Fcpaacum_Pnd_Accum_Truth_Table;
    private DbsField pnd_Fcpaacum_Pnd_New_Pymnt_Ind;
    private DbsGroup pnd_Fcpaacum_Pnd_Accum_Table;
    private DbsField pnd_Fcpaacum_Pnd_Pymnt_Cnt;
    private DbsField pnd_Fcpaacum_Pnd_Settl_Amt;
    private DbsField pnd_Fcpaacum_Pnd_Cntrct_Amt;
    private DbsField pnd_Fcpaacum_Pnd_Dvdnd_Amt;
    private DbsField pnd_Fcpaacum_Pnd_Dci_Amt;
    private DbsField pnd_Fcpaacum_Pnd_Dpi_Amt;
    private DbsField pnd_Fcpaacum_Pnd_Net_Pymnt_Amt;
    private DbsField pnd_Fcpaacum_Pnd_Fdrl_Tax_Amt;
    private DbsField pnd_Fcpaacum_Pnd_State_Tax_Amt;
    private DbsField pnd_Fcpaacum_Pnd_Local_Tax_Amt;
    private DbsField pnd_Fcpaacum_Pnd_Rec_Cnt;

    public DbsGroup getPnd_Fcpaacum() { return pnd_Fcpaacum; }

    public DbsField getPnd_Fcpaacum_Pnd_Accum_Occur() { return pnd_Fcpaacum_Pnd_Accum_Occur; }

    public DbsField getPnd_Fcpaacum_Pnd_At_At_Max_Fund() { return pnd_Fcpaacum_Pnd_At_At_Max_Fund; }

    public DbsField getPnd_Fcpaacum_Pnd_Accum_Truth_Table() { return pnd_Fcpaacum_Pnd_Accum_Truth_Table; }

    public DbsField getPnd_Fcpaacum_Pnd_New_Pymnt_Ind() { return pnd_Fcpaacum_Pnd_New_Pymnt_Ind; }

    public DbsGroup getPnd_Fcpaacum_Pnd_Accum_Table() { return pnd_Fcpaacum_Pnd_Accum_Table; }

    public DbsField getPnd_Fcpaacum_Pnd_Pymnt_Cnt() { return pnd_Fcpaacum_Pnd_Pymnt_Cnt; }

    public DbsField getPnd_Fcpaacum_Pnd_Settl_Amt() { return pnd_Fcpaacum_Pnd_Settl_Amt; }

    public DbsField getPnd_Fcpaacum_Pnd_Cntrct_Amt() { return pnd_Fcpaacum_Pnd_Cntrct_Amt; }

    public DbsField getPnd_Fcpaacum_Pnd_Dvdnd_Amt() { return pnd_Fcpaacum_Pnd_Dvdnd_Amt; }

    public DbsField getPnd_Fcpaacum_Pnd_Dci_Amt() { return pnd_Fcpaacum_Pnd_Dci_Amt; }

    public DbsField getPnd_Fcpaacum_Pnd_Dpi_Amt() { return pnd_Fcpaacum_Pnd_Dpi_Amt; }

    public DbsField getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt() { return pnd_Fcpaacum_Pnd_Net_Pymnt_Amt; }

    public DbsField getPnd_Fcpaacum_Pnd_Fdrl_Tax_Amt() { return pnd_Fcpaacum_Pnd_Fdrl_Tax_Amt; }

    public DbsField getPnd_Fcpaacum_Pnd_State_Tax_Amt() { return pnd_Fcpaacum_Pnd_State_Tax_Amt; }

    public DbsField getPnd_Fcpaacum_Pnd_Local_Tax_Amt() { return pnd_Fcpaacum_Pnd_Local_Tax_Amt; }

    public DbsField getPnd_Fcpaacum_Pnd_Rec_Cnt() { return pnd_Fcpaacum_Pnd_Rec_Cnt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpaacum = dbsRecord.newGroupInRecord("pnd_Fcpaacum", "#FCPAACUM");
        pnd_Fcpaacum.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpaacum_Pnd_Accum_Occur = pnd_Fcpaacum.newFieldInGroup("pnd_Fcpaacum_Pnd_Accum_Occur", "#ACCUM-OCCUR", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpaacum_Pnd_At_At_Max_Fund = pnd_Fcpaacum.newFieldInGroup("pnd_Fcpaacum_Pnd_At_At_Max_Fund", "#@@MAX-FUND", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpaacum_Pnd_Accum_Truth_Table = pnd_Fcpaacum.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_Accum_Truth_Table", "#ACCUM-TRUTH-TABLE", FieldType.BOOLEAN, 
            new DbsArrayController(1,10));
        pnd_Fcpaacum_Pnd_New_Pymnt_Ind = pnd_Fcpaacum.newFieldInGroup("pnd_Fcpaacum_Pnd_New_Pymnt_Ind", "#NEW-PYMNT-IND", FieldType.BOOLEAN);
        pnd_Fcpaacum_Pnd_Accum_Table = pnd_Fcpaacum.newGroupInGroup("pnd_Fcpaacum_Pnd_Accum_Table", "#ACCUM-TABLE");
        pnd_Fcpaacum_Pnd_Pymnt_Cnt = pnd_Fcpaacum_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_Pymnt_Cnt", "#PYMNT-CNT", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1,50));
        pnd_Fcpaacum_Pnd_Settl_Amt = pnd_Fcpaacum_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_Settl_Amt", "#SETTL-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpaacum_Pnd_Cntrct_Amt = pnd_Fcpaacum_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_Cntrct_Amt", "#CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpaacum_Pnd_Dvdnd_Amt = pnd_Fcpaacum_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_Dvdnd_Amt", "#DVDND-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpaacum_Pnd_Dci_Amt = pnd_Fcpaacum_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_Dci_Amt", "#DCI-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpaacum_Pnd_Dpi_Amt = pnd_Fcpaacum_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_Dpi_Amt", "#DPI-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpaacum_Pnd_Net_Pymnt_Amt = pnd_Fcpaacum_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_Net_Pymnt_Amt", "#NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpaacum_Pnd_Fdrl_Tax_Amt = pnd_Fcpaacum_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_Fdrl_Tax_Amt", "#FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpaacum_Pnd_State_Tax_Amt = pnd_Fcpaacum_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_State_Tax_Amt", "#STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpaacum_Pnd_Local_Tax_Amt = pnd_Fcpaacum_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_Local_Tax_Amt", "#LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13,2, new DbsArrayController(1,50));
        pnd_Fcpaacum_Pnd_Rec_Cnt = pnd_Fcpaacum_Pnd_Accum_Table.newFieldArrayInGroup("pnd_Fcpaacum_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1,50));

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpaacum(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

