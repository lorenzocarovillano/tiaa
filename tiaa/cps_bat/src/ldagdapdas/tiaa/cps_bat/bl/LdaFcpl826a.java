/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:16 PM
**        * FROM NATURAL LDA     : FCPL826A
************************************************************
**        * FILE NAME            : LdaFcpl826a.java
**        * CLASS NAME           : LdaFcpl826a
**        * INSTANCE NAME        : LdaFcpl826a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl826a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl826a;
    private DbsField pnd_Fcpl826a_Pnd_Annty_Idx;
    private DbsField pnd_Fcpl826a_Pnd_Annty_Idx_1;
    private DbsField pnd_Fcpl826a_Pnd_Annty;
    private DbsField pnd_Fcpl826a_Pnd_Annty_Desc;
    private DbsField pnd_Fcpl826a_Pnd_Company_Idx;
    private DbsField pnd_Fcpl826a_Pnd_Company_Idx_1;
    private DbsField pnd_Fcpl826a_Pnd_Company_Cde;
    private DbsField pnd_Fcpl826a_Pnd_Company_Cde_Desc;
    private DbsField pnd_Fcpl826a_Pnd_Life_Cont_Idx;
    private DbsField pnd_Fcpl826a_Pnd_Life_Cont_Idx_1;
    private DbsField pnd_Fcpl826a_Pnd_Life_Cont;
    private DbsField pnd_Fcpl826a_Pnd_Life_Cont_Desc;
    private DbsField pnd_Fcpl826a_Pnd_Ins_Option_Idx;
    private DbsField pnd_Fcpl826a_Pnd_Ins_Option_Idx_1;
    private DbsField pnd_Fcpl826a_Pnd_Ins_Option;
    private DbsField pnd_Fcpl826a_Pnd_Ins_Option_Desc;
    private DbsField pnd_Fcpl826a_Pnd_Annty_Type_Idx;
    private DbsField pnd_Fcpl826a_Pnd_Annty_Type_Idx_1;
    private DbsField pnd_Fcpl826a_Pnd_Annty_Type;
    private DbsField pnd_Fcpl826a_Pnd_Annty_Type_Desc;

    public DbsGroup getPnd_Fcpl826a() { return pnd_Fcpl826a; }

    public DbsField getPnd_Fcpl826a_Pnd_Annty_Idx() { return pnd_Fcpl826a_Pnd_Annty_Idx; }

    public DbsField getPnd_Fcpl826a_Pnd_Annty_Idx_1() { return pnd_Fcpl826a_Pnd_Annty_Idx_1; }

    public DbsField getPnd_Fcpl826a_Pnd_Annty() { return pnd_Fcpl826a_Pnd_Annty; }

    public DbsField getPnd_Fcpl826a_Pnd_Annty_Desc() { return pnd_Fcpl826a_Pnd_Annty_Desc; }

    public DbsField getPnd_Fcpl826a_Pnd_Company_Idx() { return pnd_Fcpl826a_Pnd_Company_Idx; }

    public DbsField getPnd_Fcpl826a_Pnd_Company_Idx_1() { return pnd_Fcpl826a_Pnd_Company_Idx_1; }

    public DbsField getPnd_Fcpl826a_Pnd_Company_Cde() { return pnd_Fcpl826a_Pnd_Company_Cde; }

    public DbsField getPnd_Fcpl826a_Pnd_Company_Cde_Desc() { return pnd_Fcpl826a_Pnd_Company_Cde_Desc; }

    public DbsField getPnd_Fcpl826a_Pnd_Life_Cont_Idx() { return pnd_Fcpl826a_Pnd_Life_Cont_Idx; }

    public DbsField getPnd_Fcpl826a_Pnd_Life_Cont_Idx_1() { return pnd_Fcpl826a_Pnd_Life_Cont_Idx_1; }

    public DbsField getPnd_Fcpl826a_Pnd_Life_Cont() { return pnd_Fcpl826a_Pnd_Life_Cont; }

    public DbsField getPnd_Fcpl826a_Pnd_Life_Cont_Desc() { return pnd_Fcpl826a_Pnd_Life_Cont_Desc; }

    public DbsField getPnd_Fcpl826a_Pnd_Ins_Option_Idx() { return pnd_Fcpl826a_Pnd_Ins_Option_Idx; }

    public DbsField getPnd_Fcpl826a_Pnd_Ins_Option_Idx_1() { return pnd_Fcpl826a_Pnd_Ins_Option_Idx_1; }

    public DbsField getPnd_Fcpl826a_Pnd_Ins_Option() { return pnd_Fcpl826a_Pnd_Ins_Option; }

    public DbsField getPnd_Fcpl826a_Pnd_Ins_Option_Desc() { return pnd_Fcpl826a_Pnd_Ins_Option_Desc; }

    public DbsField getPnd_Fcpl826a_Pnd_Annty_Type_Idx() { return pnd_Fcpl826a_Pnd_Annty_Type_Idx; }

    public DbsField getPnd_Fcpl826a_Pnd_Annty_Type_Idx_1() { return pnd_Fcpl826a_Pnd_Annty_Type_Idx_1; }

    public DbsField getPnd_Fcpl826a_Pnd_Annty_Type() { return pnd_Fcpl826a_Pnd_Annty_Type; }

    public DbsField getPnd_Fcpl826a_Pnd_Annty_Type_Desc() { return pnd_Fcpl826a_Pnd_Annty_Type_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl826a = newGroupInRecord("pnd_Fcpl826a", "#FCPL826A");
        pnd_Fcpl826a_Pnd_Annty_Idx = pnd_Fcpl826a.newFieldInGroup("pnd_Fcpl826a_Pnd_Annty_Idx", "#ANNTY-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl826a_Pnd_Annty_Idx_1 = pnd_Fcpl826a.newFieldInGroup("pnd_Fcpl826a_Pnd_Annty_Idx_1", "#ANNTY-IDX-1", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl826a_Pnd_Annty = pnd_Fcpl826a.newFieldArrayInGroup("pnd_Fcpl826a_Pnd_Annty", "#ANNTY", FieldType.STRING, 1, new DbsArrayController(1,4));
        pnd_Fcpl826a_Pnd_Annty_Desc = pnd_Fcpl826a.newFieldArrayInGroup("pnd_Fcpl826a_Pnd_Annty_Desc", "#ANNTY-DESC", FieldType.STRING, 17, new DbsArrayController(1,
            5));
        pnd_Fcpl826a_Pnd_Company_Idx = pnd_Fcpl826a.newFieldInGroup("pnd_Fcpl826a_Pnd_Company_Idx", "#COMPANY-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl826a_Pnd_Company_Idx_1 = pnd_Fcpl826a.newFieldInGroup("pnd_Fcpl826a_Pnd_Company_Idx_1", "#COMPANY-IDX-1", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl826a_Pnd_Company_Cde = pnd_Fcpl826a.newFieldArrayInGroup("pnd_Fcpl826a_Pnd_Company_Cde", "#COMPANY-CDE", FieldType.STRING, 1, new DbsArrayController(1,
            2));
        pnd_Fcpl826a_Pnd_Company_Cde_Desc = pnd_Fcpl826a.newFieldArrayInGroup("pnd_Fcpl826a_Pnd_Company_Cde_Desc", "#COMPANY-CDE-DESC", FieldType.STRING, 
            20, new DbsArrayController(1,3));
        pnd_Fcpl826a_Pnd_Life_Cont_Idx = pnd_Fcpl826a.newFieldInGroup("pnd_Fcpl826a_Pnd_Life_Cont_Idx", "#LIFE-CONT-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl826a_Pnd_Life_Cont_Idx_1 = pnd_Fcpl826a.newFieldInGroup("pnd_Fcpl826a_Pnd_Life_Cont_Idx_1", "#LIFE-CONT-IDX-1", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Fcpl826a_Pnd_Life_Cont = pnd_Fcpl826a.newFieldArrayInGroup("pnd_Fcpl826a_Pnd_Life_Cont", "#LIFE-CONT", FieldType.STRING, 1, new DbsArrayController(1,
            3));
        pnd_Fcpl826a_Pnd_Life_Cont_Desc = pnd_Fcpl826a.newFieldArrayInGroup("pnd_Fcpl826a_Pnd_Life_Cont_Desc", "#LIFE-CONT-DESC", FieldType.STRING, 3, 
            new DbsArrayController(1,4));
        pnd_Fcpl826a_Pnd_Ins_Option_Idx = pnd_Fcpl826a.newFieldInGroup("pnd_Fcpl826a_Pnd_Ins_Option_Idx", "#INS-OPTION-IDX", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Fcpl826a_Pnd_Ins_Option_Idx_1 = pnd_Fcpl826a.newFieldInGroup("pnd_Fcpl826a_Pnd_Ins_Option_Idx_1", "#INS-OPTION-IDX-1", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Fcpl826a_Pnd_Ins_Option = pnd_Fcpl826a.newFieldArrayInGroup("pnd_Fcpl826a_Pnd_Ins_Option", "#INS-OPTION", FieldType.STRING, 1, new DbsArrayController(1,
            5));
        pnd_Fcpl826a_Pnd_Ins_Option_Desc = pnd_Fcpl826a.newFieldArrayInGroup("pnd_Fcpl826a_Pnd_Ins_Option_Desc", "#INS-OPTION-DESC", FieldType.STRING, 
            10, new DbsArrayController(1,6));
        pnd_Fcpl826a_Pnd_Annty_Type_Idx = pnd_Fcpl826a.newFieldInGroup("pnd_Fcpl826a_Pnd_Annty_Type_Idx", "#ANNTY-TYPE-IDX", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Fcpl826a_Pnd_Annty_Type_Idx_1 = pnd_Fcpl826a.newFieldInGroup("pnd_Fcpl826a_Pnd_Annty_Type_Idx_1", "#ANNTY-TYPE-IDX-1", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Fcpl826a_Pnd_Annty_Type = pnd_Fcpl826a.newFieldArrayInGroup("pnd_Fcpl826a_Pnd_Annty_Type", "#ANNTY-TYPE", FieldType.STRING, 1, new DbsArrayController(1,
            4));
        pnd_Fcpl826a_Pnd_Annty_Type_Desc = pnd_Fcpl826a.newFieldArrayInGroup("pnd_Fcpl826a_Pnd_Annty_Type_Desc", "#ANNTY-TYPE-DESC", FieldType.STRING, 
            22, new DbsArrayController(1,5));

        this.setRecordName("LdaFcpl826a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl826a_Pnd_Annty_Idx_1.setInitialValue(4);
        pnd_Fcpl826a_Pnd_Annty.getValue(1).setInitialValue("A");
        pnd_Fcpl826a_Pnd_Annty.getValue(2).setInitialValue("G");
        pnd_Fcpl826a_Pnd_Annty.getValue(3).setInitialValue("I");
        pnd_Fcpl826a_Pnd_Annty.getValue(4).setInitialValue("P");
        pnd_Fcpl826a_Pnd_Annty_Desc.getValue(1).setInitialValue("Annuity");
        pnd_Fcpl826a_Pnd_Annty_Desc.getValue(2).setInitialValue("Group Annuity");
        pnd_Fcpl826a_Pnd_Annty_Desc.getValue(3).setInitialValue("Insurance");
        pnd_Fcpl826a_Pnd_Annty_Desc.getValue(4).setInitialValue("Personal Annuity");
        pnd_Fcpl826a_Pnd_Annty_Desc.getValue(5).setInitialValue("Unknown Code");
        pnd_Fcpl826a_Pnd_Company_Idx_1.setInitialValue(3);
        pnd_Fcpl826a_Pnd_Company_Cde.getValue(1).setInitialValue("C");
        pnd_Fcpl826a_Pnd_Company_Cde.getValue(2).setInitialValue("T");
        pnd_Fcpl826a_Pnd_Company_Cde_Desc.getValue(1).setInitialValue("CREF");
        pnd_Fcpl826a_Pnd_Company_Cde_Desc.getValue(2).setInitialValue("TIAA and Real Estate");
        pnd_Fcpl826a_Pnd_Company_Cde_Desc.getValue(3).setInitialValue("TIAA-CREF Life");
        pnd_Fcpl826a_Pnd_Life_Cont_Idx_1.setInitialValue(4);
        pnd_Fcpl826a_Pnd_Life_Cont.getValue(1).setInitialValue("Y");
        pnd_Fcpl826a_Pnd_Life_Cont.getValue(2).setInitialValue("N");
        pnd_Fcpl826a_Pnd_Life_Cont.getValue(3).setInitialValue("A");
        pnd_Fcpl826a_Pnd_Life_Cont_Desc.getValue(1).setInitialValue("Yes");
        pnd_Fcpl826a_Pnd_Life_Cont_Desc.getValue(2).setInitialValue("No");
        pnd_Fcpl826a_Pnd_Life_Cont_Desc.getValue(3).setInitialValue("Any");
        pnd_Fcpl826a_Pnd_Life_Cont_Desc.getValue(4).setInitialValue("???");
        pnd_Fcpl826a_Pnd_Ins_Option_Idx_1.setInitialValue(6);
        pnd_Fcpl826a_Pnd_Ins_Option.getValue(2).setInitialValue("I");
        pnd_Fcpl826a_Pnd_Ins_Option.getValue(3).setInitialValue("C");
        pnd_Fcpl826a_Pnd_Ins_Option.getValue(4).setInitialValue("G");
        pnd_Fcpl826a_Pnd_Ins_Option.getValue(5).setInitialValue("P");
        pnd_Fcpl826a_Pnd_Ins_Option_Desc.getValue(2).setInitialValue("Individual");
        pnd_Fcpl826a_Pnd_Ins_Option_Desc.getValue(3).setInitialValue("Collective");
        pnd_Fcpl826a_Pnd_Ins_Option_Desc.getValue(4).setInitialValue("Group");
        pnd_Fcpl826a_Pnd_Ins_Option_Desc.getValue(5).setInitialValue("PA");
        pnd_Fcpl826a_Pnd_Ins_Option_Desc.getValue(6).setInitialValue("Unknown");
        pnd_Fcpl826a_Pnd_Annty_Type_Idx_1.setInitialValue(5);
        pnd_Fcpl826a_Pnd_Annty_Type.getValue(1).setInitialValue("M");
        pnd_Fcpl826a_Pnd_Annty_Type.getValue(2).setInitialValue("D");
        pnd_Fcpl826a_Pnd_Annty_Type.getValue(3).setInitialValue("P");
        pnd_Fcpl826a_Pnd_Annty_Type.getValue(4).setInitialValue("N");
        pnd_Fcpl826a_Pnd_Annty_Type_Desc.getValue(1).setInitialValue("Immediate Annuity");
        pnd_Fcpl826a_Pnd_Annty_Type_Desc.getValue(2).setInitialValue("Supplemental Contracts");
        pnd_Fcpl826a_Pnd_Annty_Type_Desc.getValue(3).setInitialValue("Par");
        pnd_Fcpl826a_Pnd_Annty_Type_Desc.getValue(4).setInitialValue("Non Par");
        pnd_Fcpl826a_Pnd_Annty_Type_Desc.getValue(5).setInitialValue("Unknown");
    }

    // Constructor
    public LdaFcpl826a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
