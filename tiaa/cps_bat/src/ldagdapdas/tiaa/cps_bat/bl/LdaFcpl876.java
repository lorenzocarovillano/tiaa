/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:18 PM
**        * FROM NATURAL LDA     : FCPL876
************************************************************
**        * FILE NAME            : LdaFcpl876.java
**        * CLASS NAME           : LdaFcpl876
**        * INSTANCE NAME        : LdaFcpl876
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl876 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl876;
    private DbsField pnd_Fcpl876_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Fcpl876_Pymnt_Check_Nbr;
    private DbsField pnd_Fcpl876_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Fcpl876_Pymnt_Last_Rollover_Nbr;
    private DbsField pnd_Fcpl876_Pnd_Bar_Envelopes;
    private DbsField pnd_Fcpl876_Pnd_Bar_Barcode;
    private DbsField pnd_Fcpl876_Pnd_Bar_New_Run;

    public DbsGroup getPnd_Fcpl876() { return pnd_Fcpl876; }

    public DbsField getPnd_Fcpl876_Pymnt_Check_Seq_Nbr() { return pnd_Fcpl876_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Fcpl876_Pymnt_Check_Nbr() { return pnd_Fcpl876_Pymnt_Check_Nbr; }

    public DbsField getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr() { return pnd_Fcpl876_Pymnt_Check_Scrty_Nbr; }

    public DbsField getPnd_Fcpl876_Pymnt_Last_Rollover_Nbr() { return pnd_Fcpl876_Pymnt_Last_Rollover_Nbr; }

    public DbsField getPnd_Fcpl876_Pnd_Bar_Envelopes() { return pnd_Fcpl876_Pnd_Bar_Envelopes; }

    public DbsField getPnd_Fcpl876_Pnd_Bar_Barcode() { return pnd_Fcpl876_Pnd_Bar_Barcode; }

    public DbsField getPnd_Fcpl876_Pnd_Bar_New_Run() { return pnd_Fcpl876_Pnd_Bar_New_Run; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl876 = newGroupInRecord("pnd_Fcpl876", "#FCPL876");
        pnd_Fcpl876_Pymnt_Check_Seq_Nbr = pnd_Fcpl876.newFieldInGroup("pnd_Fcpl876_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Fcpl876_Pymnt_Check_Nbr = pnd_Fcpl876.newFieldInGroup("pnd_Fcpl876_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Fcpl876_Pymnt_Check_Scrty_Nbr = pnd_Fcpl876.newFieldInGroup("pnd_Fcpl876_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Fcpl876_Pymnt_Last_Rollover_Nbr = pnd_Fcpl876.newFieldInGroup("pnd_Fcpl876_Pymnt_Last_Rollover_Nbr", "PYMNT-LAST-ROLLOVER-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Fcpl876_Pnd_Bar_Envelopes = pnd_Fcpl876.newFieldInGroup("pnd_Fcpl876_Pnd_Bar_Envelopes", "#BAR-ENVELOPES", FieldType.PACKED_DECIMAL, 7);
        pnd_Fcpl876_Pnd_Bar_Barcode = pnd_Fcpl876.newFieldArrayInGroup("pnd_Fcpl876_Pnd_Bar_Barcode", "#BAR-BARCODE", FieldType.STRING, 1, new DbsArrayController(1,
            17));
        pnd_Fcpl876_Pnd_Bar_New_Run = pnd_Fcpl876.newFieldInGroup("pnd_Fcpl876_Pnd_Bar_New_Run", "#BAR-NEW-RUN", FieldType.BOOLEAN);

        this.setRecordName("LdaFcpl876");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl876_Pnd_Bar_Barcode.getValue(1).setInitialValue("*");
        pnd_Fcpl876_Pnd_Bar_Barcode.getValue(10).setInitialValue("0");
        pnd_Fcpl876_Pnd_Bar_Barcode.getValue(11).setInitialValue("0");
        pnd_Fcpl876_Pnd_Bar_Barcode.getValue(12).setInitialValue("0");
        pnd_Fcpl876_Pnd_Bar_Barcode.getValue(13).setInitialValue("0");
        pnd_Fcpl876_Pnd_Bar_Barcode.getValue(14).setInitialValue("0");
        pnd_Fcpl876_Pnd_Bar_Barcode.getValue(15).setInitialValue("0");
        pnd_Fcpl876_Pnd_Bar_Barcode.getValue(16).setInitialValue("0");
        pnd_Fcpl876_Pnd_Bar_Barcode.getValue(17).setInitialValue("*");
    }

    // Constructor
    public LdaFcpl876() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
