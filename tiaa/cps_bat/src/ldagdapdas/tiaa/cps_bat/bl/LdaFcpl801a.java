/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:14 PM
**        * FROM NATURAL LDA     : FCPL801A
************************************************************
**        * FILE NAME            : LdaFcpl801a.java
**        * CLASS NAME           : LdaFcpl801a
**        * INSTANCE NAME        : LdaFcpl801a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl801a extends DbsRecord
{
    // Properties
    private DbsField wf_Error_Msg;

    public DbsField getWf_Error_Msg() { return wf_Error_Msg; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        wf_Error_Msg = newFieldInRecord("wf_Error_Msg", "WF-ERROR-MSG", FieldType.STRING, 60);

        this.setRecordName("LdaFcpl801a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl801a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
