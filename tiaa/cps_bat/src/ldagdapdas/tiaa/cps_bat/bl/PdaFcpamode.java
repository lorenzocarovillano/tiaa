/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:32 PM
**        * FROM NATURAL PDA     : FCPAMODE
************************************************************
**        * FILE NAME            : PdaFcpamode.java
**        * CLASS NAME           : PdaFcpamode
**        * INSTANCE NAME        : PdaFcpamode
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpamode extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpamode;
    private DbsField pnd_Fcpamode_Pnd_Predict_Table;
    private DbsField pnd_Fcpamode_Pnd_Return_Cde;
    private DbsField pnd_Fcpamode_Cntrct_Mode_Cde;
    private DbsGroup pnd_Fcpamode_Cntrct_Mode_CdeRedef1;
    private DbsField pnd_Fcpamode_Cntrct_Mode_Cde_X;
    private DbsField pnd_Fcpamode_Pnd_Mode_Desc;
    private DbsField pnd_Fcpamode_Pnd_Mode_Desc_Length;

    public DbsGroup getPnd_Fcpamode() { return pnd_Fcpamode; }

    public DbsField getPnd_Fcpamode_Pnd_Predict_Table() { return pnd_Fcpamode_Pnd_Predict_Table; }

    public DbsField getPnd_Fcpamode_Pnd_Return_Cde() { return pnd_Fcpamode_Pnd_Return_Cde; }

    public DbsField getPnd_Fcpamode_Cntrct_Mode_Cde() { return pnd_Fcpamode_Cntrct_Mode_Cde; }

    public DbsGroup getPnd_Fcpamode_Cntrct_Mode_CdeRedef1() { return pnd_Fcpamode_Cntrct_Mode_CdeRedef1; }

    public DbsField getPnd_Fcpamode_Cntrct_Mode_Cde_X() { return pnd_Fcpamode_Cntrct_Mode_Cde_X; }

    public DbsField getPnd_Fcpamode_Pnd_Mode_Desc() { return pnd_Fcpamode_Pnd_Mode_Desc; }

    public DbsField getPnd_Fcpamode_Pnd_Mode_Desc_Length() { return pnd_Fcpamode_Pnd_Mode_Desc_Length; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpamode = dbsRecord.newGroupInRecord("pnd_Fcpamode", "#FCPAMODE");
        pnd_Fcpamode.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpamode_Pnd_Predict_Table = pnd_Fcpamode.newFieldInGroup("pnd_Fcpamode_Pnd_Predict_Table", "#PREDICT-TABLE", FieldType.BOOLEAN);
        pnd_Fcpamode_Pnd_Return_Cde = pnd_Fcpamode.newFieldInGroup("pnd_Fcpamode_Pnd_Return_Cde", "#RETURN-CDE", FieldType.BOOLEAN);
        pnd_Fcpamode_Cntrct_Mode_Cde = pnd_Fcpamode.newFieldInGroup("pnd_Fcpamode_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        pnd_Fcpamode_Cntrct_Mode_CdeRedef1 = pnd_Fcpamode.newGroupInGroup("pnd_Fcpamode_Cntrct_Mode_CdeRedef1", "Redefines", pnd_Fcpamode_Cntrct_Mode_Cde);
        pnd_Fcpamode_Cntrct_Mode_Cde_X = pnd_Fcpamode_Cntrct_Mode_CdeRedef1.newFieldInGroup("pnd_Fcpamode_Cntrct_Mode_Cde_X", "CNTRCT-MODE-CDE-X", FieldType.STRING, 
            3);
        pnd_Fcpamode_Pnd_Mode_Desc = pnd_Fcpamode.newFieldInGroup("pnd_Fcpamode_Pnd_Mode_Desc", "#MODE-DESC", FieldType.STRING, 18);
        pnd_Fcpamode_Pnd_Mode_Desc_Length = pnd_Fcpamode.newFieldInGroup("pnd_Fcpamode_Pnd_Mode_Desc_Length", "#MODE-DESC-LENGTH", FieldType.PACKED_DECIMAL, 
            3);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpamode(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

