/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:31 PM
**        * FROM NATURAL PDA     : FCPAEXT2
************************************************************
**        * FILE NAME            : PdaFcpaext2.java
**        * CLASS NAME           : PdaFcpaext2
**        * INSTANCE NAME        : PdaFcpaext2
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpaext2 extends PdaBase
{
    // Properties
    private DbsField sort_Key;
    private DbsGroup sort_KeyRedef1;
    private DbsGroup sort_Key_Sort_Key_Struc;
    private DbsField sort_Key_Sort_Cntrct_Orgn_Cde;
    private DbsField sort_Key_Sort_Option_Cde;
    private DbsGroup sort_Key_Sort_Option_CdeRedef2;
    private DbsField sort_Key_Sort_Option_Cde_Pref;
    private DbsField sort_Key_Sort_Option_Cde_X;
    private DbsField sort_Key_Sort_Other;
    private DbsField ext2;
    private DbsGroup ext2Redef3;
    private DbsGroup ext2_Extr2;
    private DbsField ext2_Cntrct_Ppcn_Nbr;
    private DbsField ext2_Cntrct_Payee_Cde;
    private DbsField ext2_Cntrct_Invrse_Dte;
    private DbsField ext2_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup ext2_Pymnt_Prcss_Seq_NbrRedef4;
    private DbsField ext2_Pymnt_Prcss_Seq_Num;
    private DbsField ext2_Pymnt_Instmt_Nbr;
    private DbsField ext2_Pymnt_Total_Pages;
    private DbsField ext2_Funds_On_Page;
    private DbsField ext2_Cntrct_Unq_Id_Nbr;
    private DbsField ext2_Cntrct_Cmbn_Nbr;
    private DbsField ext2_Pymnt_Record;
    private DbsField ext2_Pymnt_Ivc_Amt;
    private DbsField filler01;
    private DbsField ext2_Annt_Ctznshp_Cde;
    private DbsGroup ext2_Annt_Ctznshp_CdeRedef5;
    private DbsField ext2_Annt_Ctznshp_Cde_X;
    private DbsField ext2_Annt_Locality_Cde;
    private DbsField ext2_Annt_Rsdncy_Cde;
    private DbsField ext2_Annt_Soc_Sec_Ind;
    private DbsField ext2_Annt_Soc_Sec_Nbr;
    private DbsField ext2_Cntrct_Check_Crrncy_Cde;
    private DbsField ext2_Cntrct_Crrncy_Cde;
    private DbsField ext2_Cntrct_Orgn_Cde;
    private DbsField ext2_Cntrct_Qlfied_Cde;
    private DbsField ext2_Cntrct_Pymnt_Type_Ind;
    private DbsField ext2_Cntrct_Sttlmnt_Type_Ind;
    private DbsField ext2_Cntrct_Sps_Cde;
    private DbsField ext2_Cntrct_Dvdnd_Payee_Ind;
    private DbsField ext2_Cntrct_Rqst_Settl_Dte;
    private DbsField ext2_Cntrct_Rqst_Dte;
    private DbsField ext2_Cntrct_Rqst_Settl_Tme;
    private DbsField ext2_Cntrct_Type_Cde;
    private DbsField ext2_Cntrct_Lob_Cde;
    private DbsField ext2_Cntrct_Sub_Lob_Cde;
    private DbsField ext2_Cntrct_Ia_Lob_Cde;
    private DbsField ext2_Cntrct_Cref_Nbr;
    private DbsField ext2_Cntrct_Option_Cde;
    private DbsField ext2_Cntrct_Mode_Cde;
    private DbsField ext2_Cntrct_Pymnt_Dest_Cde;
    private DbsField ext2_Cntrct_Roll_Dest_Cde;
    private DbsField ext2_Cntrct_Dvdnd_Payee_Cde;
    private DbsField ext2_Cntrct_Cancel_Rdrw_Ind;
    private DbsField ext2_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField ext2_Cntrct_Cancel_Rdrw_Amt;
    private DbsField ext2_Cntrct_Hold_Cde;
    private DbsGroup ext2_Cntrct_Hold_CdeRedef6;
    private DbsField ext2_Cntrct_Hold_Type;
    private DbsField ext2_Cntrct_Hold_Dept;
    private DbsField ext2_Cntrct_Hold_Grp;
    private DbsField ext2_Cntrct_Hold_Ind;
    private DbsField ext2_Cntrct_Hold_Tme;
    private DbsField ext2_Cntrct_Annty_Ins_Type;
    private DbsField ext2_Cntrct_Annty_Type_Cde;
    private DbsField ext2_Cntrct_Insurance_Option;
    private DbsField ext2_Cntrct_Life_Contingency;
    private DbsField ext2_Cntrct_Ac_Lt_10yrs;
    private DbsField ext2_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField ext2_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField ext2_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField ext2_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField ext2_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField ext2_Cntrct_Da_Cref_1_Nbr;
    private DbsField ext2_Cntrct_Da_Cref_2_Nbr;
    private DbsField ext2_Cntrct_Da_Cref_3_Nbr;
    private DbsField ext2_Cntrct_Da_Cref_4_Nbr;
    private DbsField ext2_Cntrct_Da_Cref_5_Nbr;
    private DbsField ext2_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField ext2_Pymnt_Rqst_Rmndr_Pct;
    private DbsField ext2_Pymnt_Stats_Cde;
    private DbsField ext2_Pymnt_Annot_Ind;
    private DbsField ext2_Pymnt_Cmbne_Ind;
    private DbsField ext2_Pymnt_Ftre_Ind;
    private DbsField ext2_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField ext2_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField ext2_Pymnt_Inst_Rep_Cde;
    private DbsField ext2_Pymnt_Pay_Type_Req_Ind;
    private DbsField ext2_Pymnt_Split_Cde;
    private DbsField ext2_Pymnt_Split_Reasn_Cde;
    private DbsField ext2_Pymnt_Check_Dte;
    private DbsField ext2_Pymnt_Cycle_Dte;
    private DbsField ext2_Pymnt_Eft_Dte;
    private DbsField ext2_Pymnt_Rqst_Pct;
    private DbsField ext2_Pymnt_Rqst_Amt;
    private DbsField ext2_Pymnt_Check_Nbr;
    private DbsField ext2_Pymnt_Check_Scrty_Nbr;
    private DbsField ext2_Pymnt_Check_Amt;
    private DbsField ext2_Pymnt_Settlmnt_Dte;
    private DbsField ext2_Pymnt_Check_Seq_Nbr;
    private DbsField ext2_Pymnt_Rprnt_Pndng_Ind;
    private DbsField ext2_Pymnt_Rprnt_Rqust_Dte;
    private DbsField ext2_Pymnt_Rprnt_Dte;
    private DbsField ext2_Pymnt_Corp_Wpid;
    private DbsField ext2_Pymnt_Reqst_Log_Dte_Time;
    private DbsField ext2_Pymnt_Acctg_Dte;
    private DbsField ext2_Pymnt_Intrfce_Dte;
    private DbsField ext2_Pymnt_Tiaa_Md_Amt;
    private DbsField ext2_Pymnt_Cref_Md_Amt;
    private DbsField ext2_Pymnt_Tax_Form;
    private DbsGroup ext2_Pymnt_Tax_FormRedef7;
    private DbsField ext2_F;
    private DbsField ext2_Pymnt_Tax_Form_1001;
    private DbsField ext2_Pymnt_Tax_Form_1078;
    private DbsField ext2_Pymnt_Tax_Exempt_Ind;
    private DbsField ext2_Pymnt_Tax_Calc_Cde;
    private DbsField ext2_Pymnt_Method_Cde;
    private DbsField ext2_Pymnt_Settl_Ivc_Ind;
    private DbsField ext2_Pymnt_Ia_Issue_Dte;
    private DbsField ext2_Pymnt_Spouse_Pay_Stats;
    private DbsField ext2_Pymnt_Trigger_Cde;
    private DbsField ext2_Pymnt_Spcl_Msg_Cde;
    private DbsField ext2_Pymnt_Eft_Transit_Id;
    private DbsField ext2_Pymnt_Eft_Acct_Nbr;
    private DbsField ext2_Pymnt_Chk_Sav_Ind;
    private DbsField ext2_Pymnt_Deceased_Nme;
    private DbsField ext2_Pymnt_Dob;
    private DbsField ext2_Pymnt_Death_Dte;
    private DbsField ext2_Pymnt_Proof_Dte;
    private DbsField ext2_Pymnt_Rtb_Dest_Typ;
    private DbsField ext2_Pymnt_Alt_Addr_Ind;
    private DbsField ext2_Notused1;
    private DbsField ext2_Notused2;
    private DbsGroup ext2_Notused2Redef8;
    private DbsField ext2_Pymnt_Wrrnt_Trans_Type;
    private DbsField ext2_C_Inv_Rtb_Grp;
    private DbsField ext2_C_Pymnt_Ded_Grp;
    private DbsField ext2_C_Inv_Acct;
    private DbsField ext2_C_Pymnt_Nme_And_Addr;
    private DbsField ext2_Cnr_Cs_Actvty_Cde;
    private DbsField ext2_Cnr_Cs_Pymnt_Intrfce_Dte;
    private DbsField ext2_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField ext2_Cnr_Cs_Pymnt_Check_Dte;
    private DbsField ext2_Cnr_Cs_New_Stop_Ind;
    private DbsField ext2_Cnr_Cs_Rqust_Dte;
    private DbsField ext2_Cnr_Cs_Rqust_Tme;
    private DbsField ext2_Cnr_Cs_Rqust_User_Id;
    private DbsField ext2_Cnr_Cs_Rqust_Term_Id;
    private DbsField ext2_Cnr_Rdrw_Rqust_Tme;
    private DbsField ext2_Cnr_Rdrw_Rqust_User_Id;
    private DbsField ext2_Cnr_Orgnl_Invrse_Dte;
    private DbsField ext2_Cnr_Orgnl_Prcss_Seq_Nbr;
    private DbsField ext2_Cnr_Orgnl_Pymnt_Acctg_Dte;
    private DbsField ext2_Cnr_Orgnl_Pymnt_Intrfce_Dte;
    private DbsField ext2_Cnr_Rdrw_Rqust_Dte;
    private DbsField ext2_Cnr_Rdrw_Rqust_Term_Id;
    private DbsField ext2_Cnr_Rdrw_Pymnt_Intrfce_Dte;
    private DbsField ext2_Cnr_Rdrw_Pymnt_Acctg_Dte;
    private DbsField ext2_Cnr_Rdrw_Pymnt_Check_Dte;
    private DbsField ext2_Cnr_Rplcmnt_Invrse_Dte;
    private DbsField ext2_Cnr_Rplcmnt_Prcss_Seq_Nbr;
    private DbsField ext2_Rcrd_Typ;
    private DbsField ext2_Tax_Fed_C_Resp_Cde;
    private DbsField ext2_Tax_Fed_C_Filing_Stat;
    private DbsField ext2_Tax_Fed_C_Allow_Cnt;
    private DbsField ext2_Tax_Fed_C_Fixed_Amt;
    private DbsField ext2_Tax_Fed_C_Fixed_Pct;
    private DbsField ext2_Tax_Sta_C_Resp_Cde;
    private DbsField ext2_Tax_Sta_C_Filing_Stat;
    private DbsField ext2_Tax_Sta_C_Allow_Cnt;
    private DbsField ext2_Tax_Sta_C_Fixed_Amt;
    private DbsField ext2_Tax_Sta_C_Fixed_Pct;
    private DbsField ext2_Tax_Sta_C_Spouse_Cnt;
    private DbsField ext2_Tax_Sta_C_Blind_Cnt;
    private DbsField ext2_Tax_Loc_C_Resp_Cde;
    private DbsField ext2_Tax_Loc_C_Fixed_Amt;
    private DbsField ext2_Tax_Loc_C_Fixed_Pct;
    private DbsGroup ext2_Ph_Name;
    private DbsField ext2_Ph_Last_Name;
    private DbsField ext2_Ph_First_Name;
    private DbsField ext2_Ph_Middle_Name;
    private DbsField ext2_Ph_Sex;
    private DbsGroup ext2_Pymnt_Nme_And_Addr_Grp;
    private DbsField ext2_Pymnt_Nme;
    private DbsField ext2_Pymnt_Addr_Line_Txt;
    private DbsGroup ext2_Pymnt_Addr_Line_TxtRedef9;
    private DbsField ext2_Pymnt_Addr_Line1_Txt;
    private DbsField ext2_Pymnt_Addr_Line2_Txt;
    private DbsField ext2_Pymnt_Addr_Line3_Txt;
    private DbsField ext2_Pymnt_Addr_Line4_Txt;
    private DbsField ext2_Pymnt_Addr_Line5_Txt;
    private DbsField ext2_Pymnt_Addr_Line6_Txt;
    private DbsGroup ext2_Pymnt_Addr_Line_TxtRedef10;
    private DbsField ext2_Hdr_Title;
    private DbsField ext2_Hdr_Filler;
    private DbsField ext2_Pymnt_Addr_Zip_Cde;
    private DbsField ext2_Pymnt_Postl_Data;
    private DbsField ext2_Pymnt_Addr_Type_Ind;
    private DbsField ext2_Pymnt_Addr_Last_Chg_Dte;
    private DbsField ext2_Pymnt_Addr_Last_Chg_Tme;
    private DbsField ext2_Pymnt_Addr_Chg_Ind;
    private DbsGroup ext2_Pymnt_Ded_Grp;
    private DbsField ext2_Pymnt_Ded_Cde;
    private DbsField ext2_Pymnt_Ded_Payee_Cde;
    private DbsField ext2_Pymnt_Ded_Amt;
    private DbsGroup ext2_Inv_Acct;
    private DbsField ext2_Inv_Acct_Cde;
    private DbsGroup ext2_Inv_Acct_CdeRedef11;
    private DbsField ext2_Inv_Acct_Cde_N;
    private DbsField ext2_Inv_Acct_Ded_Cde;
    private DbsField ext2_Inv_Acct_Ded_Amt;
    private DbsField ext2_Inv_Acct_Company;
    private DbsField ext2_Inv_Acct_Unit_Qty;
    private DbsField ext2_Inv_Acct_Unit_Value;
    private DbsField ext2_Inv_Acct_Settl_Amt;
    private DbsField ext2_Inv_Acct_Fed_Cde;
    private DbsField ext2_Inv_Acct_State_Cde;
    private DbsField ext2_Inv_Acct_Local_Cde;
    private DbsField ext2_Inv_Acct_Ivc_Amt;
    private DbsField ext2_Inv_Acct_Dci_Amt;
    private DbsField ext2_Inv_Acct_Dpi_Amt;
    private DbsField ext2_Inv_Acct_Start_Accum_Amt;
    private DbsField ext2_Inv_Acct_End_Accum_Amt;
    private DbsField ext2_Inv_Acct_Dvdnd_Amt;
    private DbsField ext2_Inv_Acct_Net_Pymnt_Amt;
    private DbsField ext2_Inv_Acct_Ivc_Ind;
    private DbsField ext2_Inv_Acct_Adj_Ivc_Amt;
    private DbsField ext2_Inv_Acct_Valuat_Period;
    private DbsField ext2_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField ext2_Inv_Acct_State_Tax_Amt;
    private DbsField ext2_Inv_Acct_Local_Tax_Amt;
    private DbsField ext2_Inv_Acct_Exp_Amt;
    private DbsGroup ext2_Inv_Rtb_Grp;
    private DbsField ext2_Inv_Rtb_Cde;
    private DbsField ext2_Inv_Acct_Fed_Dci_Tax_Amt;
    private DbsField ext2_Inv_Acct_Sta_Dci_Tax_Amt;
    private DbsField ext2_Inv_Acct_Loc_Dci_Tax_Amt;
    private DbsField ext2_Inv_Acct_Fed_Dpi_Tax_Amt;
    private DbsField ext2_Inv_Acct_Sta_Dpi_Tax_Amt;
    private DbsField ext2_Inv_Acct_Loc_Dpi_Tax_Amt;

    public DbsField getSort_Key() { return sort_Key; }

    public DbsGroup getSort_KeyRedef1() { return sort_KeyRedef1; }

    public DbsGroup getSort_Key_Sort_Key_Struc() { return sort_Key_Sort_Key_Struc; }

    public DbsField getSort_Key_Sort_Cntrct_Orgn_Cde() { return sort_Key_Sort_Cntrct_Orgn_Cde; }

    public DbsField getSort_Key_Sort_Option_Cde() { return sort_Key_Sort_Option_Cde; }

    public DbsGroup getSort_Key_Sort_Option_CdeRedef2() { return sort_Key_Sort_Option_CdeRedef2; }

    public DbsField getSort_Key_Sort_Option_Cde_Pref() { return sort_Key_Sort_Option_Cde_Pref; }

    public DbsField getSort_Key_Sort_Option_Cde_X() { return sort_Key_Sort_Option_Cde_X; }

    public DbsField getSort_Key_Sort_Other() { return sort_Key_Sort_Other; }

    public DbsField getExt2() { return ext2; }

    public DbsGroup getExt2Redef3() { return ext2Redef3; }

    public DbsGroup getExt2_Extr2() { return ext2_Extr2; }

    public DbsField getExt2_Cntrct_Ppcn_Nbr() { return ext2_Cntrct_Ppcn_Nbr; }

    public DbsField getExt2_Cntrct_Payee_Cde() { return ext2_Cntrct_Payee_Cde; }

    public DbsField getExt2_Cntrct_Invrse_Dte() { return ext2_Cntrct_Invrse_Dte; }

    public DbsField getExt2_Pymnt_Prcss_Seq_Nbr() { return ext2_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getExt2_Pymnt_Prcss_Seq_NbrRedef4() { return ext2_Pymnt_Prcss_Seq_NbrRedef4; }

    public DbsField getExt2_Pymnt_Prcss_Seq_Num() { return ext2_Pymnt_Prcss_Seq_Num; }

    public DbsField getExt2_Pymnt_Instmt_Nbr() { return ext2_Pymnt_Instmt_Nbr; }

    public DbsField getExt2_Pymnt_Total_Pages() { return ext2_Pymnt_Total_Pages; }

    public DbsField getExt2_Funds_On_Page() { return ext2_Funds_On_Page; }

    public DbsField getExt2_Cntrct_Unq_Id_Nbr() { return ext2_Cntrct_Unq_Id_Nbr; }

    public DbsField getExt2_Cntrct_Cmbn_Nbr() { return ext2_Cntrct_Cmbn_Nbr; }

    public DbsField getExt2_Pymnt_Record() { return ext2_Pymnt_Record; }

    public DbsField getExt2_Pymnt_Ivc_Amt() { return ext2_Pymnt_Ivc_Amt; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getExt2_Annt_Ctznshp_Cde() { return ext2_Annt_Ctznshp_Cde; }

    public DbsGroup getExt2_Annt_Ctznshp_CdeRedef5() { return ext2_Annt_Ctznshp_CdeRedef5; }

    public DbsField getExt2_Annt_Ctznshp_Cde_X() { return ext2_Annt_Ctznshp_Cde_X; }

    public DbsField getExt2_Annt_Locality_Cde() { return ext2_Annt_Locality_Cde; }

    public DbsField getExt2_Annt_Rsdncy_Cde() { return ext2_Annt_Rsdncy_Cde; }

    public DbsField getExt2_Annt_Soc_Sec_Ind() { return ext2_Annt_Soc_Sec_Ind; }

    public DbsField getExt2_Annt_Soc_Sec_Nbr() { return ext2_Annt_Soc_Sec_Nbr; }

    public DbsField getExt2_Cntrct_Check_Crrncy_Cde() { return ext2_Cntrct_Check_Crrncy_Cde; }

    public DbsField getExt2_Cntrct_Crrncy_Cde() { return ext2_Cntrct_Crrncy_Cde; }

    public DbsField getExt2_Cntrct_Orgn_Cde() { return ext2_Cntrct_Orgn_Cde; }

    public DbsField getExt2_Cntrct_Qlfied_Cde() { return ext2_Cntrct_Qlfied_Cde; }

    public DbsField getExt2_Cntrct_Pymnt_Type_Ind() { return ext2_Cntrct_Pymnt_Type_Ind; }

    public DbsField getExt2_Cntrct_Sttlmnt_Type_Ind() { return ext2_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getExt2_Cntrct_Sps_Cde() { return ext2_Cntrct_Sps_Cde; }

    public DbsField getExt2_Cntrct_Dvdnd_Payee_Ind() { return ext2_Cntrct_Dvdnd_Payee_Ind; }

    public DbsField getExt2_Cntrct_Rqst_Settl_Dte() { return ext2_Cntrct_Rqst_Settl_Dte; }

    public DbsField getExt2_Cntrct_Rqst_Dte() { return ext2_Cntrct_Rqst_Dte; }

    public DbsField getExt2_Cntrct_Rqst_Settl_Tme() { return ext2_Cntrct_Rqst_Settl_Tme; }

    public DbsField getExt2_Cntrct_Type_Cde() { return ext2_Cntrct_Type_Cde; }

    public DbsField getExt2_Cntrct_Lob_Cde() { return ext2_Cntrct_Lob_Cde; }

    public DbsField getExt2_Cntrct_Sub_Lob_Cde() { return ext2_Cntrct_Sub_Lob_Cde; }

    public DbsField getExt2_Cntrct_Ia_Lob_Cde() { return ext2_Cntrct_Ia_Lob_Cde; }

    public DbsField getExt2_Cntrct_Cref_Nbr() { return ext2_Cntrct_Cref_Nbr; }

    public DbsField getExt2_Cntrct_Option_Cde() { return ext2_Cntrct_Option_Cde; }

    public DbsField getExt2_Cntrct_Mode_Cde() { return ext2_Cntrct_Mode_Cde; }

    public DbsField getExt2_Cntrct_Pymnt_Dest_Cde() { return ext2_Cntrct_Pymnt_Dest_Cde; }

    public DbsField getExt2_Cntrct_Roll_Dest_Cde() { return ext2_Cntrct_Roll_Dest_Cde; }

    public DbsField getExt2_Cntrct_Dvdnd_Payee_Cde() { return ext2_Cntrct_Dvdnd_Payee_Cde; }

    public DbsField getExt2_Cntrct_Cancel_Rdrw_Ind() { return ext2_Cntrct_Cancel_Rdrw_Ind; }

    public DbsField getExt2_Cntrct_Cancel_Rdrw_Actvty_Cde() { return ext2_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getExt2_Cntrct_Cancel_Rdrw_Amt() { return ext2_Cntrct_Cancel_Rdrw_Amt; }

    public DbsField getExt2_Cntrct_Hold_Cde() { return ext2_Cntrct_Hold_Cde; }

    public DbsGroup getExt2_Cntrct_Hold_CdeRedef6() { return ext2_Cntrct_Hold_CdeRedef6; }

    public DbsField getExt2_Cntrct_Hold_Type() { return ext2_Cntrct_Hold_Type; }

    public DbsField getExt2_Cntrct_Hold_Dept() { return ext2_Cntrct_Hold_Dept; }

    public DbsField getExt2_Cntrct_Hold_Grp() { return ext2_Cntrct_Hold_Grp; }

    public DbsField getExt2_Cntrct_Hold_Ind() { return ext2_Cntrct_Hold_Ind; }

    public DbsField getExt2_Cntrct_Hold_Tme() { return ext2_Cntrct_Hold_Tme; }

    public DbsField getExt2_Cntrct_Annty_Ins_Type() { return ext2_Cntrct_Annty_Ins_Type; }

    public DbsField getExt2_Cntrct_Annty_Type_Cde() { return ext2_Cntrct_Annty_Type_Cde; }

    public DbsField getExt2_Cntrct_Insurance_Option() { return ext2_Cntrct_Insurance_Option; }

    public DbsField getExt2_Cntrct_Life_Contingency() { return ext2_Cntrct_Life_Contingency; }

    public DbsField getExt2_Cntrct_Ac_Lt_10yrs() { return ext2_Cntrct_Ac_Lt_10yrs; }

    public DbsField getExt2_Cntrct_Da_Tiaa_1_Nbr() { return ext2_Cntrct_Da_Tiaa_1_Nbr; }

    public DbsField getExt2_Cntrct_Da_Tiaa_2_Nbr() { return ext2_Cntrct_Da_Tiaa_2_Nbr; }

    public DbsField getExt2_Cntrct_Da_Tiaa_3_Nbr() { return ext2_Cntrct_Da_Tiaa_3_Nbr; }

    public DbsField getExt2_Cntrct_Da_Tiaa_4_Nbr() { return ext2_Cntrct_Da_Tiaa_4_Nbr; }

    public DbsField getExt2_Cntrct_Da_Tiaa_5_Nbr() { return ext2_Cntrct_Da_Tiaa_5_Nbr; }

    public DbsField getExt2_Cntrct_Da_Cref_1_Nbr() { return ext2_Cntrct_Da_Cref_1_Nbr; }

    public DbsField getExt2_Cntrct_Da_Cref_2_Nbr() { return ext2_Cntrct_Da_Cref_2_Nbr; }

    public DbsField getExt2_Cntrct_Da_Cref_3_Nbr() { return ext2_Cntrct_Da_Cref_3_Nbr; }

    public DbsField getExt2_Cntrct_Da_Cref_4_Nbr() { return ext2_Cntrct_Da_Cref_4_Nbr; }

    public DbsField getExt2_Cntrct_Da_Cref_5_Nbr() { return ext2_Cntrct_Da_Cref_5_Nbr; }

    public DbsField getExt2_Cntrct_Ec_Oprtr_Id_Nbr() { return ext2_Cntrct_Ec_Oprtr_Id_Nbr; }

    public DbsField getExt2_Pymnt_Rqst_Rmndr_Pct() { return ext2_Pymnt_Rqst_Rmndr_Pct; }

    public DbsField getExt2_Pymnt_Stats_Cde() { return ext2_Pymnt_Stats_Cde; }

    public DbsField getExt2_Pymnt_Annot_Ind() { return ext2_Pymnt_Annot_Ind; }

    public DbsField getExt2_Pymnt_Cmbne_Ind() { return ext2_Pymnt_Cmbne_Ind; }

    public DbsField getExt2_Pymnt_Ftre_Ind() { return ext2_Pymnt_Ftre_Ind; }

    public DbsField getExt2_Pymnt_Payee_Na_Addr_Trggr() { return ext2_Pymnt_Payee_Na_Addr_Trggr; }

    public DbsField getExt2_Pymnt_Payee_Tx_Elct_Trggr() { return ext2_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getExt2_Pymnt_Inst_Rep_Cde() { return ext2_Pymnt_Inst_Rep_Cde; }

    public DbsField getExt2_Pymnt_Pay_Type_Req_Ind() { return ext2_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getExt2_Pymnt_Split_Cde() { return ext2_Pymnt_Split_Cde; }

    public DbsField getExt2_Pymnt_Split_Reasn_Cde() { return ext2_Pymnt_Split_Reasn_Cde; }

    public DbsField getExt2_Pymnt_Check_Dte() { return ext2_Pymnt_Check_Dte; }

    public DbsField getExt2_Pymnt_Cycle_Dte() { return ext2_Pymnt_Cycle_Dte; }

    public DbsField getExt2_Pymnt_Eft_Dte() { return ext2_Pymnt_Eft_Dte; }

    public DbsField getExt2_Pymnt_Rqst_Pct() { return ext2_Pymnt_Rqst_Pct; }

    public DbsField getExt2_Pymnt_Rqst_Amt() { return ext2_Pymnt_Rqst_Amt; }

    public DbsField getExt2_Pymnt_Check_Nbr() { return ext2_Pymnt_Check_Nbr; }

    public DbsField getExt2_Pymnt_Check_Scrty_Nbr() { return ext2_Pymnt_Check_Scrty_Nbr; }

    public DbsField getExt2_Pymnt_Check_Amt() { return ext2_Pymnt_Check_Amt; }

    public DbsField getExt2_Pymnt_Settlmnt_Dte() { return ext2_Pymnt_Settlmnt_Dte; }

    public DbsField getExt2_Pymnt_Check_Seq_Nbr() { return ext2_Pymnt_Check_Seq_Nbr; }

    public DbsField getExt2_Pymnt_Rprnt_Pndng_Ind() { return ext2_Pymnt_Rprnt_Pndng_Ind; }

    public DbsField getExt2_Pymnt_Rprnt_Rqust_Dte() { return ext2_Pymnt_Rprnt_Rqust_Dte; }

    public DbsField getExt2_Pymnt_Rprnt_Dte() { return ext2_Pymnt_Rprnt_Dte; }

    public DbsField getExt2_Pymnt_Corp_Wpid() { return ext2_Pymnt_Corp_Wpid; }

    public DbsField getExt2_Pymnt_Reqst_Log_Dte_Time() { return ext2_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getExt2_Pymnt_Acctg_Dte() { return ext2_Pymnt_Acctg_Dte; }

    public DbsField getExt2_Pymnt_Intrfce_Dte() { return ext2_Pymnt_Intrfce_Dte; }

    public DbsField getExt2_Pymnt_Tiaa_Md_Amt() { return ext2_Pymnt_Tiaa_Md_Amt; }

    public DbsField getExt2_Pymnt_Cref_Md_Amt() { return ext2_Pymnt_Cref_Md_Amt; }

    public DbsField getExt2_Pymnt_Tax_Form() { return ext2_Pymnt_Tax_Form; }

    public DbsGroup getExt2_Pymnt_Tax_FormRedef7() { return ext2_Pymnt_Tax_FormRedef7; }

    public DbsField getExt2_F() { return ext2_F; }

    public DbsField getExt2_Pymnt_Tax_Form_1001() { return ext2_Pymnt_Tax_Form_1001; }

    public DbsField getExt2_Pymnt_Tax_Form_1078() { return ext2_Pymnt_Tax_Form_1078; }

    public DbsField getExt2_Pymnt_Tax_Exempt_Ind() { return ext2_Pymnt_Tax_Exempt_Ind; }

    public DbsField getExt2_Pymnt_Tax_Calc_Cde() { return ext2_Pymnt_Tax_Calc_Cde; }

    public DbsField getExt2_Pymnt_Method_Cde() { return ext2_Pymnt_Method_Cde; }

    public DbsField getExt2_Pymnt_Settl_Ivc_Ind() { return ext2_Pymnt_Settl_Ivc_Ind; }

    public DbsField getExt2_Pymnt_Ia_Issue_Dte() { return ext2_Pymnt_Ia_Issue_Dte; }

    public DbsField getExt2_Pymnt_Spouse_Pay_Stats() { return ext2_Pymnt_Spouse_Pay_Stats; }

    public DbsField getExt2_Pymnt_Trigger_Cde() { return ext2_Pymnt_Trigger_Cde; }

    public DbsField getExt2_Pymnt_Spcl_Msg_Cde() { return ext2_Pymnt_Spcl_Msg_Cde; }

    public DbsField getExt2_Pymnt_Eft_Transit_Id() { return ext2_Pymnt_Eft_Transit_Id; }

    public DbsField getExt2_Pymnt_Eft_Acct_Nbr() { return ext2_Pymnt_Eft_Acct_Nbr; }

    public DbsField getExt2_Pymnt_Chk_Sav_Ind() { return ext2_Pymnt_Chk_Sav_Ind; }

    public DbsField getExt2_Pymnt_Deceased_Nme() { return ext2_Pymnt_Deceased_Nme; }

    public DbsField getExt2_Pymnt_Dob() { return ext2_Pymnt_Dob; }

    public DbsField getExt2_Pymnt_Death_Dte() { return ext2_Pymnt_Death_Dte; }

    public DbsField getExt2_Pymnt_Proof_Dte() { return ext2_Pymnt_Proof_Dte; }

    public DbsField getExt2_Pymnt_Rtb_Dest_Typ() { return ext2_Pymnt_Rtb_Dest_Typ; }

    public DbsField getExt2_Pymnt_Alt_Addr_Ind() { return ext2_Pymnt_Alt_Addr_Ind; }

    public DbsField getExt2_Notused1() { return ext2_Notused1; }

    public DbsField getExt2_Notused2() { return ext2_Notused2; }

    public DbsGroup getExt2_Notused2Redef8() { return ext2_Notused2Redef8; }

    public DbsField getExt2_Pymnt_Wrrnt_Trans_Type() { return ext2_Pymnt_Wrrnt_Trans_Type; }

    public DbsField getExt2_C_Inv_Rtb_Grp() { return ext2_C_Inv_Rtb_Grp; }

    public DbsField getExt2_C_Pymnt_Ded_Grp() { return ext2_C_Pymnt_Ded_Grp; }

    public DbsField getExt2_C_Inv_Acct() { return ext2_C_Inv_Acct; }

    public DbsField getExt2_C_Pymnt_Nme_And_Addr() { return ext2_C_Pymnt_Nme_And_Addr; }

    public DbsField getExt2_Cnr_Cs_Actvty_Cde() { return ext2_Cnr_Cs_Actvty_Cde; }

    public DbsField getExt2_Cnr_Cs_Pymnt_Intrfce_Dte() { return ext2_Cnr_Cs_Pymnt_Intrfce_Dte; }

    public DbsField getExt2_Cnr_Cs_Pymnt_Acctg_Dte() { return ext2_Cnr_Cs_Pymnt_Acctg_Dte; }

    public DbsField getExt2_Cnr_Cs_Pymnt_Check_Dte() { return ext2_Cnr_Cs_Pymnt_Check_Dte; }

    public DbsField getExt2_Cnr_Cs_New_Stop_Ind() { return ext2_Cnr_Cs_New_Stop_Ind; }

    public DbsField getExt2_Cnr_Cs_Rqust_Dte() { return ext2_Cnr_Cs_Rqust_Dte; }

    public DbsField getExt2_Cnr_Cs_Rqust_Tme() { return ext2_Cnr_Cs_Rqust_Tme; }

    public DbsField getExt2_Cnr_Cs_Rqust_User_Id() { return ext2_Cnr_Cs_Rqust_User_Id; }

    public DbsField getExt2_Cnr_Cs_Rqust_Term_Id() { return ext2_Cnr_Cs_Rqust_Term_Id; }

    public DbsField getExt2_Cnr_Rdrw_Rqust_Tme() { return ext2_Cnr_Rdrw_Rqust_Tme; }

    public DbsField getExt2_Cnr_Rdrw_Rqust_User_Id() { return ext2_Cnr_Rdrw_Rqust_User_Id; }

    public DbsField getExt2_Cnr_Orgnl_Invrse_Dte() { return ext2_Cnr_Orgnl_Invrse_Dte; }

    public DbsField getExt2_Cnr_Orgnl_Prcss_Seq_Nbr() { return ext2_Cnr_Orgnl_Prcss_Seq_Nbr; }

    public DbsField getExt2_Cnr_Orgnl_Pymnt_Acctg_Dte() { return ext2_Cnr_Orgnl_Pymnt_Acctg_Dte; }

    public DbsField getExt2_Cnr_Orgnl_Pymnt_Intrfce_Dte() { return ext2_Cnr_Orgnl_Pymnt_Intrfce_Dte; }

    public DbsField getExt2_Cnr_Rdrw_Rqust_Dte() { return ext2_Cnr_Rdrw_Rqust_Dte; }

    public DbsField getExt2_Cnr_Rdrw_Rqust_Term_Id() { return ext2_Cnr_Rdrw_Rqust_Term_Id; }

    public DbsField getExt2_Cnr_Rdrw_Pymnt_Intrfce_Dte() { return ext2_Cnr_Rdrw_Pymnt_Intrfce_Dte; }

    public DbsField getExt2_Cnr_Rdrw_Pymnt_Acctg_Dte() { return ext2_Cnr_Rdrw_Pymnt_Acctg_Dte; }

    public DbsField getExt2_Cnr_Rdrw_Pymnt_Check_Dte() { return ext2_Cnr_Rdrw_Pymnt_Check_Dte; }

    public DbsField getExt2_Cnr_Rplcmnt_Invrse_Dte() { return ext2_Cnr_Rplcmnt_Invrse_Dte; }

    public DbsField getExt2_Cnr_Rplcmnt_Prcss_Seq_Nbr() { return ext2_Cnr_Rplcmnt_Prcss_Seq_Nbr; }

    public DbsField getExt2_Rcrd_Typ() { return ext2_Rcrd_Typ; }

    public DbsField getExt2_Tax_Fed_C_Resp_Cde() { return ext2_Tax_Fed_C_Resp_Cde; }

    public DbsField getExt2_Tax_Fed_C_Filing_Stat() { return ext2_Tax_Fed_C_Filing_Stat; }

    public DbsField getExt2_Tax_Fed_C_Allow_Cnt() { return ext2_Tax_Fed_C_Allow_Cnt; }

    public DbsField getExt2_Tax_Fed_C_Fixed_Amt() { return ext2_Tax_Fed_C_Fixed_Amt; }

    public DbsField getExt2_Tax_Fed_C_Fixed_Pct() { return ext2_Tax_Fed_C_Fixed_Pct; }

    public DbsField getExt2_Tax_Sta_C_Resp_Cde() { return ext2_Tax_Sta_C_Resp_Cde; }

    public DbsField getExt2_Tax_Sta_C_Filing_Stat() { return ext2_Tax_Sta_C_Filing_Stat; }

    public DbsField getExt2_Tax_Sta_C_Allow_Cnt() { return ext2_Tax_Sta_C_Allow_Cnt; }

    public DbsField getExt2_Tax_Sta_C_Fixed_Amt() { return ext2_Tax_Sta_C_Fixed_Amt; }

    public DbsField getExt2_Tax_Sta_C_Fixed_Pct() { return ext2_Tax_Sta_C_Fixed_Pct; }

    public DbsField getExt2_Tax_Sta_C_Spouse_Cnt() { return ext2_Tax_Sta_C_Spouse_Cnt; }

    public DbsField getExt2_Tax_Sta_C_Blind_Cnt() { return ext2_Tax_Sta_C_Blind_Cnt; }

    public DbsField getExt2_Tax_Loc_C_Resp_Cde() { return ext2_Tax_Loc_C_Resp_Cde; }

    public DbsField getExt2_Tax_Loc_C_Fixed_Amt() { return ext2_Tax_Loc_C_Fixed_Amt; }

    public DbsField getExt2_Tax_Loc_C_Fixed_Pct() { return ext2_Tax_Loc_C_Fixed_Pct; }

    public DbsGroup getExt2_Ph_Name() { return ext2_Ph_Name; }

    public DbsField getExt2_Ph_Last_Name() { return ext2_Ph_Last_Name; }

    public DbsField getExt2_Ph_First_Name() { return ext2_Ph_First_Name; }

    public DbsField getExt2_Ph_Middle_Name() { return ext2_Ph_Middle_Name; }

    public DbsField getExt2_Ph_Sex() { return ext2_Ph_Sex; }

    public DbsGroup getExt2_Pymnt_Nme_And_Addr_Grp() { return ext2_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getExt2_Pymnt_Nme() { return ext2_Pymnt_Nme; }

    public DbsField getExt2_Pymnt_Addr_Line_Txt() { return ext2_Pymnt_Addr_Line_Txt; }

    public DbsGroup getExt2_Pymnt_Addr_Line_TxtRedef9() { return ext2_Pymnt_Addr_Line_TxtRedef9; }

    public DbsField getExt2_Pymnt_Addr_Line1_Txt() { return ext2_Pymnt_Addr_Line1_Txt; }

    public DbsField getExt2_Pymnt_Addr_Line2_Txt() { return ext2_Pymnt_Addr_Line2_Txt; }

    public DbsField getExt2_Pymnt_Addr_Line3_Txt() { return ext2_Pymnt_Addr_Line3_Txt; }

    public DbsField getExt2_Pymnt_Addr_Line4_Txt() { return ext2_Pymnt_Addr_Line4_Txt; }

    public DbsField getExt2_Pymnt_Addr_Line5_Txt() { return ext2_Pymnt_Addr_Line5_Txt; }

    public DbsField getExt2_Pymnt_Addr_Line6_Txt() { return ext2_Pymnt_Addr_Line6_Txt; }

    public DbsGroup getExt2_Pymnt_Addr_Line_TxtRedef10() { return ext2_Pymnt_Addr_Line_TxtRedef10; }

    public DbsField getExt2_Hdr_Title() { return ext2_Hdr_Title; }

    public DbsField getExt2_Hdr_Filler() { return ext2_Hdr_Filler; }

    public DbsField getExt2_Pymnt_Addr_Zip_Cde() { return ext2_Pymnt_Addr_Zip_Cde; }

    public DbsField getExt2_Pymnt_Postl_Data() { return ext2_Pymnt_Postl_Data; }

    public DbsField getExt2_Pymnt_Addr_Type_Ind() { return ext2_Pymnt_Addr_Type_Ind; }

    public DbsField getExt2_Pymnt_Addr_Last_Chg_Dte() { return ext2_Pymnt_Addr_Last_Chg_Dte; }

    public DbsField getExt2_Pymnt_Addr_Last_Chg_Tme() { return ext2_Pymnt_Addr_Last_Chg_Tme; }

    public DbsField getExt2_Pymnt_Addr_Chg_Ind() { return ext2_Pymnt_Addr_Chg_Ind; }

    public DbsGroup getExt2_Pymnt_Ded_Grp() { return ext2_Pymnt_Ded_Grp; }

    public DbsField getExt2_Pymnt_Ded_Cde() { return ext2_Pymnt_Ded_Cde; }

    public DbsField getExt2_Pymnt_Ded_Payee_Cde() { return ext2_Pymnt_Ded_Payee_Cde; }

    public DbsField getExt2_Pymnt_Ded_Amt() { return ext2_Pymnt_Ded_Amt; }

    public DbsGroup getExt2_Inv_Acct() { return ext2_Inv_Acct; }

    public DbsField getExt2_Inv_Acct_Cde() { return ext2_Inv_Acct_Cde; }

    public DbsGroup getExt2_Inv_Acct_CdeRedef11() { return ext2_Inv_Acct_CdeRedef11; }

    public DbsField getExt2_Inv_Acct_Cde_N() { return ext2_Inv_Acct_Cde_N; }

    public DbsField getExt2_Inv_Acct_Ded_Cde() { return ext2_Inv_Acct_Ded_Cde; }

    public DbsField getExt2_Inv_Acct_Ded_Amt() { return ext2_Inv_Acct_Ded_Amt; }

    public DbsField getExt2_Inv_Acct_Company() { return ext2_Inv_Acct_Company; }

    public DbsField getExt2_Inv_Acct_Unit_Qty() { return ext2_Inv_Acct_Unit_Qty; }

    public DbsField getExt2_Inv_Acct_Unit_Value() { return ext2_Inv_Acct_Unit_Value; }

    public DbsField getExt2_Inv_Acct_Settl_Amt() { return ext2_Inv_Acct_Settl_Amt; }

    public DbsField getExt2_Inv_Acct_Fed_Cde() { return ext2_Inv_Acct_Fed_Cde; }

    public DbsField getExt2_Inv_Acct_State_Cde() { return ext2_Inv_Acct_State_Cde; }

    public DbsField getExt2_Inv_Acct_Local_Cde() { return ext2_Inv_Acct_Local_Cde; }

    public DbsField getExt2_Inv_Acct_Ivc_Amt() { return ext2_Inv_Acct_Ivc_Amt; }

    public DbsField getExt2_Inv_Acct_Dci_Amt() { return ext2_Inv_Acct_Dci_Amt; }

    public DbsField getExt2_Inv_Acct_Dpi_Amt() { return ext2_Inv_Acct_Dpi_Amt; }

    public DbsField getExt2_Inv_Acct_Start_Accum_Amt() { return ext2_Inv_Acct_Start_Accum_Amt; }

    public DbsField getExt2_Inv_Acct_End_Accum_Amt() { return ext2_Inv_Acct_End_Accum_Amt; }

    public DbsField getExt2_Inv_Acct_Dvdnd_Amt() { return ext2_Inv_Acct_Dvdnd_Amt; }

    public DbsField getExt2_Inv_Acct_Net_Pymnt_Amt() { return ext2_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getExt2_Inv_Acct_Ivc_Ind() { return ext2_Inv_Acct_Ivc_Ind; }

    public DbsField getExt2_Inv_Acct_Adj_Ivc_Amt() { return ext2_Inv_Acct_Adj_Ivc_Amt; }

    public DbsField getExt2_Inv_Acct_Valuat_Period() { return ext2_Inv_Acct_Valuat_Period; }

    public DbsField getExt2_Inv_Acct_Fdrl_Tax_Amt() { return ext2_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getExt2_Inv_Acct_State_Tax_Amt() { return ext2_Inv_Acct_State_Tax_Amt; }

    public DbsField getExt2_Inv_Acct_Local_Tax_Amt() { return ext2_Inv_Acct_Local_Tax_Amt; }

    public DbsField getExt2_Inv_Acct_Exp_Amt() { return ext2_Inv_Acct_Exp_Amt; }

    public DbsGroup getExt2_Inv_Rtb_Grp() { return ext2_Inv_Rtb_Grp; }

    public DbsField getExt2_Inv_Rtb_Cde() { return ext2_Inv_Rtb_Cde; }

    public DbsField getExt2_Inv_Acct_Fed_Dci_Tax_Amt() { return ext2_Inv_Acct_Fed_Dci_Tax_Amt; }

    public DbsField getExt2_Inv_Acct_Sta_Dci_Tax_Amt() { return ext2_Inv_Acct_Sta_Dci_Tax_Amt; }

    public DbsField getExt2_Inv_Acct_Loc_Dci_Tax_Amt() { return ext2_Inv_Acct_Loc_Dci_Tax_Amt; }

    public DbsField getExt2_Inv_Acct_Fed_Dpi_Tax_Amt() { return ext2_Inv_Acct_Fed_Dpi_Tax_Amt; }

    public DbsField getExt2_Inv_Acct_Sta_Dpi_Tax_Amt() { return ext2_Inv_Acct_Sta_Dpi_Tax_Amt; }

    public DbsField getExt2_Inv_Acct_Loc_Dpi_Tax_Amt() { return ext2_Inv_Acct_Loc_Dpi_Tax_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        sort_Key = dbsRecord.newFieldInRecord("sort_Key", "SORT-KEY", FieldType.STRING, 10);
        sort_Key.setParameterOption(ParameterOption.ByReference);
        sort_KeyRedef1 = dbsRecord.newGroupInRecord("sort_KeyRedef1", "Redefines", sort_Key);
        sort_Key_Sort_Key_Struc = sort_KeyRedef1.newGroupInGroup("sort_Key_Sort_Key_Struc", "SORT-KEY-STRUC");
        sort_Key_Sort_Cntrct_Orgn_Cde = sort_Key_Sort_Key_Struc.newFieldInGroup("sort_Key_Sort_Cntrct_Orgn_Cde", "SORT-CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        sort_Key_Sort_Option_Cde = sort_Key_Sort_Key_Struc.newFieldInGroup("sort_Key_Sort_Option_Cde", "SORT-OPTION-CDE", FieldType.STRING, 6);
        sort_Key_Sort_Option_CdeRedef2 = sort_Key_Sort_Key_Struc.newGroupInGroup("sort_Key_Sort_Option_CdeRedef2", "Redefines", sort_Key_Sort_Option_Cde);
        sort_Key_Sort_Option_Cde_Pref = sort_Key_Sort_Option_CdeRedef2.newFieldInGroup("sort_Key_Sort_Option_Cde_Pref", "SORT-OPTION-CDE-PREF", FieldType.STRING, 
            1);
        sort_Key_Sort_Option_Cde_X = sort_Key_Sort_Option_CdeRedef2.newFieldInGroup("sort_Key_Sort_Option_Cde_X", "SORT-OPTION-CDE-X", FieldType.STRING, 
            5);
        sort_Key_Sort_Other = sort_Key_Sort_Key_Struc.newFieldInGroup("sort_Key_Sort_Other", "SORT-OTHER", FieldType.STRING, 2);

        ext2 = dbsRecord.newFieldArrayInRecord("ext2", "EXT2", FieldType.STRING, 240, new DbsArrayController(1,25));
        ext2.setParameterOption(ParameterOption.ByReference);
        ext2Redef3 = dbsRecord.newGroupInRecord("ext2Redef3", "Redefines", ext2);
        ext2_Extr2 = ext2Redef3.newGroupInGroup("ext2_Extr2", "EXTR2");
        ext2_Cntrct_Ppcn_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        ext2_Cntrct_Payee_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        ext2_Cntrct_Invrse_Dte = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        ext2_Pymnt_Prcss_Seq_Nbr = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        ext2_Pymnt_Prcss_Seq_NbrRedef4 = ext2_Extr2.newGroupInGroup("ext2_Pymnt_Prcss_Seq_NbrRedef4", "Redefines", ext2_Pymnt_Prcss_Seq_Nbr);
        ext2_Pymnt_Prcss_Seq_Num = ext2_Pymnt_Prcss_Seq_NbrRedef4.newFieldInGroup("ext2_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        ext2_Pymnt_Instmt_Nbr = ext2_Pymnt_Prcss_Seq_NbrRedef4.newFieldInGroup("ext2_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        ext2_Pymnt_Total_Pages = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Total_Pages", "PYMNT-TOTAL-PAGES", FieldType.PACKED_DECIMAL, 3);
        ext2_Funds_On_Page = ext2_Extr2.newFieldArrayInGroup("ext2_Funds_On_Page", "FUNDS-ON-PAGE", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1,
            20));
        ext2_Cntrct_Unq_Id_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        ext2_Cntrct_Cmbn_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        ext2_Pymnt_Record = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Record", "PYMNT-RECORD", FieldType.PACKED_DECIMAL, 3);
        ext2_Pymnt_Ivc_Amt = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Ivc_Amt", "PYMNT-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        filler01 = ext2_Extr2.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 25);
        ext2_Annt_Ctznshp_Cde = ext2_Extr2.newFieldInGroup("ext2_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2);
        ext2_Annt_Ctznshp_CdeRedef5 = ext2_Extr2.newGroupInGroup("ext2_Annt_Ctznshp_CdeRedef5", "Redefines", ext2_Annt_Ctznshp_Cde);
        ext2_Annt_Ctznshp_Cde_X = ext2_Annt_Ctznshp_CdeRedef5.newFieldInGroup("ext2_Annt_Ctznshp_Cde_X", "ANNT-CTZNSHP-CDE-X", FieldType.STRING, 2);
        ext2_Annt_Locality_Cde = ext2_Extr2.newFieldInGroup("ext2_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", FieldType.STRING, 2);
        ext2_Annt_Rsdncy_Cde = ext2_Extr2.newFieldInGroup("ext2_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        ext2_Annt_Soc_Sec_Ind = ext2_Extr2.newFieldInGroup("ext2_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1);
        ext2_Annt_Soc_Sec_Nbr = ext2_Extr2.newFieldInGroup("ext2_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        ext2_Cntrct_Check_Crrncy_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        ext2_Cntrct_Crrncy_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 1);
        ext2_Cntrct_Orgn_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        ext2_Cntrct_Qlfied_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 1);
        ext2_Cntrct_Pymnt_Type_Ind = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        ext2_Cntrct_Sttlmnt_Type_Ind = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        ext2_Cntrct_Sps_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", FieldType.STRING, 1);
        ext2_Cntrct_Dvdnd_Payee_Ind = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Dvdnd_Payee_Ind", "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 1);
        ext2_Cntrct_Rqst_Settl_Dte = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Rqst_Settl_Dte", "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        ext2_Cntrct_Rqst_Dte = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", FieldType.DATE);
        ext2_Cntrct_Rqst_Settl_Tme = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Rqst_Settl_Tme", "CNTRCT-RQST-SETTL-TME", FieldType.TIME);
        ext2_Cntrct_Type_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        ext2_Cntrct_Lob_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        ext2_Cntrct_Sub_Lob_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        ext2_Cntrct_Ia_Lob_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 2);
        ext2_Cntrct_Cref_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10);
        ext2_Cntrct_Option_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        ext2_Cntrct_Mode_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        ext2_Cntrct_Pymnt_Dest_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        ext2_Cntrct_Roll_Dest_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        ext2_Cntrct_Dvdnd_Payee_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        ext2_Cntrct_Cancel_Rdrw_Ind = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        ext2_Cntrct_Cancel_Rdrw_Actvty_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2);
        ext2_Cntrct_Cancel_Rdrw_Amt = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Cancel_Rdrw_Amt", "CNTRCT-CANCEL-RDRW-AMT", FieldType.PACKED_DECIMAL, 11,
            2);
        ext2_Cntrct_Hold_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        ext2_Cntrct_Hold_CdeRedef6 = ext2_Extr2.newGroupInGroup("ext2_Cntrct_Hold_CdeRedef6", "Redefines", ext2_Cntrct_Hold_Cde);
        ext2_Cntrct_Hold_Type = ext2_Cntrct_Hold_CdeRedef6.newFieldInGroup("ext2_Cntrct_Hold_Type", "CNTRCT-HOLD-TYPE", FieldType.STRING, 2);
        ext2_Cntrct_Hold_Dept = ext2_Cntrct_Hold_CdeRedef6.newFieldInGroup("ext2_Cntrct_Hold_Dept", "CNTRCT-HOLD-DEPT", FieldType.STRING, 2);
        ext2_Cntrct_Hold_Grp = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3);
        ext2_Cntrct_Hold_Ind = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Hold_Ind", "CNTRCT-HOLD-IND", FieldType.STRING, 1);
        ext2_Cntrct_Hold_Tme = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME);
        ext2_Cntrct_Annty_Ins_Type = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        ext2_Cntrct_Annty_Type_Cde = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        ext2_Cntrct_Insurance_Option = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 1);
        ext2_Cntrct_Life_Contingency = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 1);
        ext2_Cntrct_Ac_Lt_10yrs = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Ac_Lt_10yrs", "CNTRCT-AC-LT-10YRS", FieldType.BOOLEAN);
        ext2_Cntrct_Da_Tiaa_1_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Da_Tiaa_1_Nbr", "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        ext2_Cntrct_Da_Tiaa_2_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Da_Tiaa_2_Nbr", "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        ext2_Cntrct_Da_Tiaa_3_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Da_Tiaa_3_Nbr", "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        ext2_Cntrct_Da_Tiaa_4_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Da_Tiaa_4_Nbr", "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        ext2_Cntrct_Da_Tiaa_5_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Da_Tiaa_5_Nbr", "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        ext2_Cntrct_Da_Cref_1_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        ext2_Cntrct_Da_Cref_2_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8);
        ext2_Cntrct_Da_Cref_3_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Da_Cref_3_Nbr", "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 8);
        ext2_Cntrct_Da_Cref_4_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Da_Cref_4_Nbr", "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 8);
        ext2_Cntrct_Da_Cref_5_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Da_Cref_5_Nbr", "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 8);
        ext2_Cntrct_Ec_Oprtr_Id_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cntrct_Ec_Oprtr_Id_Nbr", "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 7);
        ext2_Pymnt_Rqst_Rmndr_Pct = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Rqst_Rmndr_Pct", "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1);
        ext2_Pymnt_Stats_Cde = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1);
        ext2_Pymnt_Annot_Ind = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 1);
        ext2_Pymnt_Cmbne_Ind = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 1);
        ext2_Pymnt_Ftre_Ind = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", FieldType.STRING, 1);
        ext2_Pymnt_Payee_Na_Addr_Trggr = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Payee_Na_Addr_Trggr", "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 1);
        ext2_Pymnt_Payee_Tx_Elct_Trggr = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        ext2_Pymnt_Inst_Rep_Cde = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Inst_Rep_Cde", "PYMNT-INST-REP-CDE", FieldType.STRING, 1);
        ext2_Pymnt_Pay_Type_Req_Ind = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        ext2_Pymnt_Split_Cde = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", FieldType.STRING, 2);
        ext2_Pymnt_Split_Reasn_Cde = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);
        ext2_Pymnt_Check_Dte = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        ext2_Pymnt_Cycle_Dte = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        ext2_Pymnt_Eft_Dte = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        ext2_Pymnt_Rqst_Pct = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", FieldType.PACKED_DECIMAL, 3);
        ext2_Pymnt_Rqst_Amt = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", FieldType.PACKED_DECIMAL, 11,2);
        ext2_Pymnt_Check_Nbr = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        ext2_Pymnt_Check_Scrty_Nbr = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        ext2_Pymnt_Check_Amt = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Pymnt_Settlmnt_Dte = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        ext2_Pymnt_Check_Seq_Nbr = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        ext2_Pymnt_Rprnt_Pndng_Ind = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Rprnt_Pndng_Ind", "PYMNT-RPRNT-PNDNG-IND", FieldType.STRING, 1);
        ext2_Pymnt_Rprnt_Rqust_Dte = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Rprnt_Rqust_Dte", "PYMNT-RPRNT-RQUST-DTE", FieldType.DATE);
        ext2_Pymnt_Rprnt_Dte = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Rprnt_Dte", "PYMNT-RPRNT-DTE", FieldType.DATE);
        ext2_Pymnt_Corp_Wpid = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Corp_Wpid", "PYMNT-CORP-WPID", FieldType.STRING, 6);
        ext2_Pymnt_Reqst_Log_Dte_Time = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        ext2_Pymnt_Acctg_Dte = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        ext2_Pymnt_Intrfce_Dte = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        ext2_Pymnt_Tiaa_Md_Amt = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Tiaa_Md_Amt", "PYMNT-TIAA-MD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        ext2_Pymnt_Cref_Md_Amt = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Cref_Md_Amt", "PYMNT-CREF-MD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        ext2_Pymnt_Tax_Form = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Tax_Form", "PYMNT-TAX-FORM", FieldType.STRING, 4);
        ext2_Pymnt_Tax_FormRedef7 = ext2_Extr2.newGroupInGroup("ext2_Pymnt_Tax_FormRedef7", "Redefines", ext2_Pymnt_Tax_Form);
        ext2_F = ext2_Pymnt_Tax_FormRedef7.newFieldInGroup("ext2_F", "F", FieldType.STRING, 2);
        ext2_Pymnt_Tax_Form_1001 = ext2_Pymnt_Tax_FormRedef7.newFieldInGroup("ext2_Pymnt_Tax_Form_1001", "PYMNT-TAX-FORM-1001", FieldType.STRING, 1);
        ext2_Pymnt_Tax_Form_1078 = ext2_Pymnt_Tax_FormRedef7.newFieldInGroup("ext2_Pymnt_Tax_Form_1078", "PYMNT-TAX-FORM-1078", FieldType.STRING, 1);
        ext2_Pymnt_Tax_Exempt_Ind = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Tax_Exempt_Ind", "PYMNT-TAX-EXEMPT-IND", FieldType.STRING, 1);
        ext2_Pymnt_Tax_Calc_Cde = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Tax_Calc_Cde", "PYMNT-TAX-CALC-CDE", FieldType.STRING, 2);
        ext2_Pymnt_Method_Cde = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Method_Cde", "PYMNT-METHOD-CDE", FieldType.STRING, 1);
        ext2_Pymnt_Settl_Ivc_Ind = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Settl_Ivc_Ind", "PYMNT-SETTL-IVC-IND", FieldType.STRING, 1);
        ext2_Pymnt_Ia_Issue_Dte = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Ia_Issue_Dte", "PYMNT-IA-ISSUE-DTE", FieldType.DATE);
        ext2_Pymnt_Spouse_Pay_Stats = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", FieldType.STRING, 1);
        ext2_Pymnt_Trigger_Cde = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Trigger_Cde", "PYMNT-TRIGGER-CDE", FieldType.STRING, 1);
        ext2_Pymnt_Spcl_Msg_Cde = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Spcl_Msg_Cde", "PYMNT-SPCL-MSG-CDE", FieldType.STRING, 3);
        ext2_Pymnt_Eft_Transit_Id = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9);
        ext2_Pymnt_Eft_Acct_Nbr = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        ext2_Pymnt_Chk_Sav_Ind = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 1);
        ext2_Pymnt_Deceased_Nme = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Deceased_Nme", "PYMNT-DECEASED-NME", FieldType.STRING, 38);
        ext2_Pymnt_Dob = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Dob", "PYMNT-DOB", FieldType.DATE);
        ext2_Pymnt_Death_Dte = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Death_Dte", "PYMNT-DEATH-DTE", FieldType.DATE);
        ext2_Pymnt_Proof_Dte = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Proof_Dte", "PYMNT-PROOF-DTE", FieldType.DATE);
        ext2_Pymnt_Rtb_Dest_Typ = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Rtb_Dest_Typ", "PYMNT-RTB-DEST-TYP", FieldType.STRING, 4);
        ext2_Pymnt_Alt_Addr_Ind = ext2_Extr2.newFieldInGroup("ext2_Pymnt_Alt_Addr_Ind", "PYMNT-ALT-ADDR-IND", FieldType.BOOLEAN);
        ext2_Notused1 = ext2_Extr2.newFieldInGroup("ext2_Notused1", "NOTUSED1", FieldType.STRING, 8);
        ext2_Notused2 = ext2_Extr2.newFieldInGroup("ext2_Notused2", "NOTUSED2", FieldType.STRING, 8);
        ext2_Notused2Redef8 = ext2_Extr2.newGroupInGroup("ext2_Notused2Redef8", "Redefines", ext2_Notused2);
        ext2_Pymnt_Wrrnt_Trans_Type = ext2_Notused2Redef8.newFieldInGroup("ext2_Pymnt_Wrrnt_Trans_Type", "PYMNT-WRRNT-TRANS-TYPE", FieldType.STRING, 2);
        ext2_C_Inv_Rtb_Grp = ext2_Extr2.newFieldInGroup("ext2_C_Inv_Rtb_Grp", "C-INV-RTB-GRP", FieldType.NUMERIC, 2);
        ext2_C_Pymnt_Ded_Grp = ext2_Extr2.newFieldInGroup("ext2_C_Pymnt_Ded_Grp", "C-PYMNT-DED-GRP", FieldType.NUMERIC, 2);
        ext2_C_Inv_Acct = ext2_Extr2.newFieldInGroup("ext2_C_Inv_Acct", "C-INV-ACCT", FieldType.NUMERIC, 2);
        ext2_C_Pymnt_Nme_And_Addr = ext2_Extr2.newFieldInGroup("ext2_C_Pymnt_Nme_And_Addr", "C-PYMNT-NME-AND-ADDR", FieldType.NUMERIC, 1);
        ext2_Cnr_Cs_Actvty_Cde = ext2_Extr2.newFieldInGroup("ext2_Cnr_Cs_Actvty_Cde", "CNR-CS-ACTVTY-CDE", FieldType.STRING, 1);
        ext2_Cnr_Cs_Pymnt_Intrfce_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Cs_Pymnt_Intrfce_Dte", "CNR-CS-PYMNT-INTRFCE-DTE", FieldType.DATE);
        ext2_Cnr_Cs_Pymnt_Acctg_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", FieldType.DATE);
        ext2_Cnr_Cs_Pymnt_Check_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Cs_Pymnt_Check_Dte", "CNR-CS-PYMNT-CHECK-DTE", FieldType.DATE);
        ext2_Cnr_Cs_New_Stop_Ind = ext2_Extr2.newFieldInGroup("ext2_Cnr_Cs_New_Stop_Ind", "CNR-CS-NEW-STOP-IND", FieldType.STRING, 1);
        ext2_Cnr_Cs_Rqust_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Cs_Rqust_Dte", "CNR-CS-RQUST-DTE", FieldType.DATE);
        ext2_Cnr_Cs_Rqust_Tme = ext2_Extr2.newFieldInGroup("ext2_Cnr_Cs_Rqust_Tme", "CNR-CS-RQUST-TME", FieldType.NUMERIC, 7);
        ext2_Cnr_Cs_Rqust_User_Id = ext2_Extr2.newFieldInGroup("ext2_Cnr_Cs_Rqust_User_Id", "CNR-CS-RQUST-USER-ID", FieldType.STRING, 8);
        ext2_Cnr_Cs_Rqust_Term_Id = ext2_Extr2.newFieldInGroup("ext2_Cnr_Cs_Rqust_Term_Id", "CNR-CS-RQUST-TERM-ID", FieldType.STRING, 8);
        ext2_Cnr_Rdrw_Rqust_Tme = ext2_Extr2.newFieldInGroup("ext2_Cnr_Rdrw_Rqust_Tme", "CNR-RDRW-RQUST-TME", FieldType.NUMERIC, 7);
        ext2_Cnr_Rdrw_Rqust_User_Id = ext2_Extr2.newFieldInGroup("ext2_Cnr_Rdrw_Rqust_User_Id", "CNR-RDRW-RQUST-USER-ID", FieldType.STRING, 8);
        ext2_Cnr_Orgnl_Invrse_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        ext2_Cnr_Orgnl_Prcss_Seq_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cnr_Orgnl_Prcss_Seq_Nbr", "CNR-ORGNL-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        ext2_Cnr_Orgnl_Pymnt_Acctg_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Orgnl_Pymnt_Acctg_Dte", "CNR-ORGNL-PYMNT-ACCTG-DTE", FieldType.DATE);
        ext2_Cnr_Orgnl_Pymnt_Intrfce_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Orgnl_Pymnt_Intrfce_Dte", "CNR-ORGNL-PYMNT-INTRFCE-DTE", FieldType.DATE);
        ext2_Cnr_Rdrw_Rqust_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Rdrw_Rqust_Dte", "CNR-RDRW-RQUST-DTE", FieldType.DATE);
        ext2_Cnr_Rdrw_Rqust_Term_Id = ext2_Extr2.newFieldInGroup("ext2_Cnr_Rdrw_Rqust_Term_Id", "CNR-RDRW-RQUST-TERM-ID", FieldType.STRING, 8);
        ext2_Cnr_Rdrw_Pymnt_Intrfce_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Rdrw_Pymnt_Intrfce_Dte", "CNR-RDRW-PYMNT-INTRFCE-DTE", FieldType.DATE);
        ext2_Cnr_Rdrw_Pymnt_Acctg_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Rdrw_Pymnt_Acctg_Dte", "CNR-RDRW-PYMNT-ACCTG-DTE", FieldType.DATE);
        ext2_Cnr_Rdrw_Pymnt_Check_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Rdrw_Pymnt_Check_Dte", "CNR-RDRW-PYMNT-CHECK-DTE", FieldType.DATE);
        ext2_Cnr_Rplcmnt_Invrse_Dte = ext2_Extr2.newFieldInGroup("ext2_Cnr_Rplcmnt_Invrse_Dte", "CNR-RPLCMNT-INVRSE-DTE", FieldType.NUMERIC, 8);
        ext2_Cnr_Rplcmnt_Prcss_Seq_Nbr = ext2_Extr2.newFieldInGroup("ext2_Cnr_Rplcmnt_Prcss_Seq_Nbr", "CNR-RPLCMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);
        ext2_Rcrd_Typ = ext2_Extr2.newFieldInGroup("ext2_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 1);
        ext2_Tax_Fed_C_Resp_Cde = ext2_Extr2.newFieldInGroup("ext2_Tax_Fed_C_Resp_Cde", "TAX-FED-C-RESP-CDE", FieldType.STRING, 1);
        ext2_Tax_Fed_C_Filing_Stat = ext2_Extr2.newFieldInGroup("ext2_Tax_Fed_C_Filing_Stat", "TAX-FED-C-FILING-STAT", FieldType.STRING, 1);
        ext2_Tax_Fed_C_Allow_Cnt = ext2_Extr2.newFieldInGroup("ext2_Tax_Fed_C_Allow_Cnt", "TAX-FED-C-ALLOW-CNT", FieldType.PACKED_DECIMAL, 2);
        ext2_Tax_Fed_C_Fixed_Amt = ext2_Extr2.newFieldInGroup("ext2_Tax_Fed_C_Fixed_Amt", "TAX-FED-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Tax_Fed_C_Fixed_Pct = ext2_Extr2.newFieldInGroup("ext2_Tax_Fed_C_Fixed_Pct", "TAX-FED-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 7,4);
        ext2_Tax_Sta_C_Resp_Cde = ext2_Extr2.newFieldInGroup("ext2_Tax_Sta_C_Resp_Cde", "TAX-STA-C-RESP-CDE", FieldType.STRING, 1);
        ext2_Tax_Sta_C_Filing_Stat = ext2_Extr2.newFieldInGroup("ext2_Tax_Sta_C_Filing_Stat", "TAX-STA-C-FILING-STAT", FieldType.STRING, 1);
        ext2_Tax_Sta_C_Allow_Cnt = ext2_Extr2.newFieldInGroup("ext2_Tax_Sta_C_Allow_Cnt", "TAX-STA-C-ALLOW-CNT", FieldType.PACKED_DECIMAL, 2);
        ext2_Tax_Sta_C_Fixed_Amt = ext2_Extr2.newFieldInGroup("ext2_Tax_Sta_C_Fixed_Amt", "TAX-STA-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Tax_Sta_C_Fixed_Pct = ext2_Extr2.newFieldInGroup("ext2_Tax_Sta_C_Fixed_Pct", "TAX-STA-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 7,4);
        ext2_Tax_Sta_C_Spouse_Cnt = ext2_Extr2.newFieldInGroup("ext2_Tax_Sta_C_Spouse_Cnt", "TAX-STA-C-SPOUSE-CNT", FieldType.NUMERIC, 1);
        ext2_Tax_Sta_C_Blind_Cnt = ext2_Extr2.newFieldInGroup("ext2_Tax_Sta_C_Blind_Cnt", "TAX-STA-C-BLIND-CNT", FieldType.NUMERIC, 1);
        ext2_Tax_Loc_C_Resp_Cde = ext2_Extr2.newFieldInGroup("ext2_Tax_Loc_C_Resp_Cde", "TAX-LOC-C-RESP-CDE", FieldType.STRING, 1);
        ext2_Tax_Loc_C_Fixed_Amt = ext2_Extr2.newFieldInGroup("ext2_Tax_Loc_C_Fixed_Amt", "TAX-LOC-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Tax_Loc_C_Fixed_Pct = ext2_Extr2.newFieldInGroup("ext2_Tax_Loc_C_Fixed_Pct", "TAX-LOC-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 7,4);
        ext2_Ph_Name = ext2_Extr2.newGroupInGroup("ext2_Ph_Name", "PH-NAME");
        ext2_Ph_Last_Name = ext2_Ph_Name.newFieldInGroup("ext2_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        ext2_Ph_First_Name = ext2_Ph_Name.newFieldInGroup("ext2_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        ext2_Ph_Middle_Name = ext2_Ph_Name.newFieldInGroup("ext2_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        ext2_Ph_Sex = ext2_Extr2.newFieldInGroup("ext2_Ph_Sex", "PH-SEX", FieldType.STRING, 1);
        ext2_Pymnt_Nme_And_Addr_Grp = ext2_Extr2.newGroupArrayInGroup("ext2_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", new DbsArrayController(1,
            2));
        ext2_Pymnt_Nme = ext2_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext2_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38);
        ext2_Pymnt_Addr_Line_Txt = ext2_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("ext2_Pymnt_Addr_Line_Txt", "PYMNT-ADDR-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        ext2_Pymnt_Addr_Line_TxtRedef9 = ext2_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("ext2_Pymnt_Addr_Line_TxtRedef9", "Redefines", ext2_Pymnt_Addr_Line_Txt);
        ext2_Pymnt_Addr_Line1_Txt = ext2_Pymnt_Addr_Line_TxtRedef9.newFieldInGroup("ext2_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 
            35);
        ext2_Pymnt_Addr_Line2_Txt = ext2_Pymnt_Addr_Line_TxtRedef9.newFieldInGroup("ext2_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 
            35);
        ext2_Pymnt_Addr_Line3_Txt = ext2_Pymnt_Addr_Line_TxtRedef9.newFieldInGroup("ext2_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 
            35);
        ext2_Pymnt_Addr_Line4_Txt = ext2_Pymnt_Addr_Line_TxtRedef9.newFieldInGroup("ext2_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 
            35);
        ext2_Pymnt_Addr_Line5_Txt = ext2_Pymnt_Addr_Line_TxtRedef9.newFieldInGroup("ext2_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 
            35);
        ext2_Pymnt_Addr_Line6_Txt = ext2_Pymnt_Addr_Line_TxtRedef9.newFieldInGroup("ext2_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 
            35);
        ext2_Pymnt_Addr_Line_TxtRedef10 = ext2_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("ext2_Pymnt_Addr_Line_TxtRedef10", "Redefines", ext2_Pymnt_Addr_Line_Txt);
        ext2_Hdr_Title = ext2_Pymnt_Addr_Line_TxtRedef10.newFieldInGroup("ext2_Hdr_Title", "HDR-TITLE", FieldType.STRING, 100);
        ext2_Hdr_Filler = ext2_Pymnt_Addr_Line_TxtRedef10.newFieldInGroup("ext2_Hdr_Filler", "HDR-FILLER", FieldType.STRING, 110);
        ext2_Pymnt_Addr_Zip_Cde = ext2_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext2_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 9);
        ext2_Pymnt_Postl_Data = ext2_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext2_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 32);
        ext2_Pymnt_Addr_Type_Ind = ext2_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext2_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", FieldType.STRING, 1);
        ext2_Pymnt_Addr_Last_Chg_Dte = ext2_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext2_Pymnt_Addr_Last_Chg_Dte", "PYMNT-ADDR-LAST-CHG-DTE", FieldType.NUMERIC, 
            8);
        ext2_Pymnt_Addr_Last_Chg_Tme = ext2_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext2_Pymnt_Addr_Last_Chg_Tme", "PYMNT-ADDR-LAST-CHG-TME", FieldType.NUMERIC, 
            7);
        ext2_Pymnt_Addr_Chg_Ind = ext2_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext2_Pymnt_Addr_Chg_Ind", "PYMNT-ADDR-CHG-IND", FieldType.STRING, 1);
        ext2_Pymnt_Ded_Grp = ext2_Extr2.newGroupArrayInGroup("ext2_Pymnt_Ded_Grp", "PYMNT-DED-GRP", new DbsArrayController(1,10));
        ext2_Pymnt_Ded_Cde = ext2_Pymnt_Ded_Grp.newFieldInGroup("ext2_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3);
        ext2_Pymnt_Ded_Payee_Cde = ext2_Pymnt_Ded_Grp.newFieldInGroup("ext2_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 8);
        ext2_Pymnt_Ded_Amt = ext2_Pymnt_Ded_Grp.newFieldInGroup("ext2_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Inv_Acct = ext2_Extr2.newGroupArrayInGroup("ext2_Inv_Acct", "INV-ACCT", new DbsArrayController(1,40));
        ext2_Inv_Acct_Cde = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        ext2_Inv_Acct_CdeRedef11 = ext2_Inv_Acct.newGroupInGroup("ext2_Inv_Acct_CdeRedef11", "Redefines", ext2_Inv_Acct_Cde);
        ext2_Inv_Acct_Cde_N = ext2_Inv_Acct_CdeRedef11.newFieldInGroup("ext2_Inv_Acct_Cde_N", "INV-ACCT-CDE-N", FieldType.NUMERIC, 2);
        ext2_Inv_Acct_Ded_Cde = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Ded_Cde", "INV-ACCT-DED-CDE", FieldType.NUMERIC, 3);
        ext2_Inv_Acct_Ded_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Ded_Amt", "INV-ACCT-DED-AMT", FieldType.DECIMAL, 9,2);
        ext2_Inv_Acct_Company = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Company", "INV-ACCT-COMPANY", FieldType.STRING, 1);
        ext2_Inv_Acct_Unit_Qty = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 9,3);
        ext2_Inv_Acct_Unit_Value = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 9,4);
        ext2_Inv_Acct_Settl_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 11,2);
        ext2_Inv_Acct_Fed_Cde = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 1);
        ext2_Inv_Acct_State_Cde = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", FieldType.STRING, 1);
        ext2_Inv_Acct_Local_Cde = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", FieldType.STRING, 1);
        ext2_Inv_Acct_Ivc_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Inv_Acct_Dci_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Inv_Acct_Dpi_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Inv_Acct_Start_Accum_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext2_Inv_Acct_End_Accum_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext2_Inv_Acct_Dvdnd_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Inv_Acct_Net_Pymnt_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext2_Inv_Acct_Ivc_Ind = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 1);
        ext2_Inv_Acct_Adj_Ivc_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Inv_Acct_Valuat_Period = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 1);
        ext2_Inv_Acct_Fdrl_Tax_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Inv_Acct_State_Tax_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext2_Inv_Acct_Local_Tax_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext2_Inv_Acct_Exp_Amt = ext2_Inv_Acct.newFieldInGroup("ext2_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext2_Inv_Rtb_Grp = ext2_Extr2.newGroupArrayInGroup("ext2_Inv_Rtb_Grp", "INV-RTB-GRP", new DbsArrayController(1,20));
        ext2_Inv_Rtb_Cde = ext2_Inv_Rtb_Grp.newFieldInGroup("ext2_Inv_Rtb_Cde", "INV-RTB-CDE", FieldType.STRING, 1);
        ext2_Inv_Acct_Fed_Dci_Tax_Amt = ext2_Inv_Rtb_Grp.newFieldInGroup("ext2_Inv_Acct_Fed_Dci_Tax_Amt", "INV-ACCT-FED-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext2_Inv_Acct_Sta_Dci_Tax_Amt = ext2_Inv_Rtb_Grp.newFieldInGroup("ext2_Inv_Acct_Sta_Dci_Tax_Amt", "INV-ACCT-STA-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext2_Inv_Acct_Loc_Dci_Tax_Amt = ext2_Inv_Rtb_Grp.newFieldInGroup("ext2_Inv_Acct_Loc_Dci_Tax_Amt", "INV-ACCT-LOC-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext2_Inv_Acct_Fed_Dpi_Tax_Amt = ext2_Inv_Rtb_Grp.newFieldInGroup("ext2_Inv_Acct_Fed_Dpi_Tax_Amt", "INV-ACCT-FED-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext2_Inv_Acct_Sta_Dpi_Tax_Amt = ext2_Inv_Rtb_Grp.newFieldInGroup("ext2_Inv_Acct_Sta_Dpi_Tax_Amt", "INV-ACCT-STA-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext2_Inv_Acct_Loc_Dpi_Tax_Amt = ext2_Inv_Rtb_Grp.newFieldInGroup("ext2_Inv_Acct_Loc_Dpi_Tax_Amt", "INV-ACCT-LOC-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpaext2(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

