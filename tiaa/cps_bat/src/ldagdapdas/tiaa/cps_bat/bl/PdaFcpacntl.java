/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:29 PM
**        * FROM NATURAL PDA     : FCPACNTL
************************************************************
**        * FILE NAME            : PdaFcpacntl.java
**        * CLASS NAME           : PdaFcpacntl
**        * INSTANCE NAME        : PdaFcpacntl
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpacntl extends PdaBase
{
    // Properties
    private DbsGroup cntl;
    private DbsField cntl_Cntl_Orgn_Cde;
    private DbsField cntl_Cntl_Check_Dte;
    private DbsField cntl_Cntl_Invrse_Dte;
    private DbsField cntl_Cntl_Occur_Cnt;
    private DbsField cntl_Cntl_Type_Cde;
    private DbsGroup cntl_Cntl_Type_CdeRedef1;
    private DbsField cntl_Cntl_Occur_Num;
    private DbsField cntl_Cntl_Pymnt_Cnt;
    private DbsField cntl_Cntl_Rec_Cnt;
    private DbsField cntl_Cntl_Gross_Amt;
    private DbsField cntl_Cntl_Net_Amt;

    public DbsGroup getCntl() { return cntl; }

    public DbsField getCntl_Cntl_Orgn_Cde() { return cntl_Cntl_Orgn_Cde; }

    public DbsField getCntl_Cntl_Check_Dte() { return cntl_Cntl_Check_Dte; }

    public DbsField getCntl_Cntl_Invrse_Dte() { return cntl_Cntl_Invrse_Dte; }

    public DbsField getCntl_Cntl_Occur_Cnt() { return cntl_Cntl_Occur_Cnt; }

    public DbsField getCntl_Cntl_Type_Cde() { return cntl_Cntl_Type_Cde; }

    public DbsGroup getCntl_Cntl_Type_CdeRedef1() { return cntl_Cntl_Type_CdeRedef1; }

    public DbsField getCntl_Cntl_Occur_Num() { return cntl_Cntl_Occur_Num; }

    public DbsField getCntl_Cntl_Pymnt_Cnt() { return cntl_Cntl_Pymnt_Cnt; }

    public DbsField getCntl_Cntl_Rec_Cnt() { return cntl_Cntl_Rec_Cnt; }

    public DbsField getCntl_Cntl_Gross_Amt() { return cntl_Cntl_Gross_Amt; }

    public DbsField getCntl_Cntl_Net_Amt() { return cntl_Cntl_Net_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cntl = dbsRecord.newGroupInRecord("cntl", "CNTL");
        cntl.setParameterOption(ParameterOption.ByReference);
        cntl_Cntl_Orgn_Cde = cntl.newFieldInGroup("cntl_Cntl_Orgn_Cde", "CNTL-ORGN-CDE", FieldType.STRING, 2);
        cntl_Cntl_Check_Dte = cntl.newFieldInGroup("cntl_Cntl_Check_Dte", "CNTL-CHECK-DTE", FieldType.DATE);
        cntl_Cntl_Invrse_Dte = cntl.newFieldInGroup("cntl_Cntl_Invrse_Dte", "CNTL-INVRSE-DTE", FieldType.NUMERIC, 8);
        cntl_Cntl_Occur_Cnt = cntl.newFieldInGroup("cntl_Cntl_Occur_Cnt", "CNTL-OCCUR-CNT", FieldType.PACKED_DECIMAL, 3);
        cntl_Cntl_Type_Cde = cntl.newFieldArrayInGroup("cntl_Cntl_Type_Cde", "CNTL-TYPE-CDE", FieldType.STRING, 2, new DbsArrayController(1,50));
        cntl_Cntl_Type_CdeRedef1 = cntl.newGroupInGroup("cntl_Cntl_Type_CdeRedef1", "Redefines", cntl_Cntl_Type_Cde);
        cntl_Cntl_Occur_Num = cntl_Cntl_Type_CdeRedef1.newFieldArrayInGroup("cntl_Cntl_Occur_Num", "CNTL-OCCUR-NUM", FieldType.NUMERIC, 2, new DbsArrayController(1,
            50));
        cntl_Cntl_Pymnt_Cnt = cntl.newFieldArrayInGroup("cntl_Cntl_Pymnt_Cnt", "CNTL-PYMNT-CNT", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1,
            50));
        cntl_Cntl_Rec_Cnt = cntl.newFieldArrayInGroup("cntl_Cntl_Rec_Cnt", "CNTL-REC-CNT", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1,50));
        cntl_Cntl_Gross_Amt = cntl.newFieldArrayInGroup("cntl_Cntl_Gross_Amt", "CNTL-GROSS-AMT", FieldType.PACKED_DECIMAL, 13,2, new DbsArrayController(1,
            50));
        cntl_Cntl_Net_Amt = cntl.newFieldArrayInGroup("cntl_Cntl_Net_Amt", "CNTL-NET-AMT", FieldType.PACKED_DECIMAL, 13,2, new DbsArrayController(1,50));

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpacntl(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

