/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:19 PM
**        * FROM NATURAL LDA     : FCPL961
************************************************************
**        * FILE NAME            : LdaFcpl961.java
**        * CLASS NAME           : LdaFcpl961
**        * INSTANCE NAME        : LdaFcpl961
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl961 extends DbsRecord
{
    // Properties
    private DbsGroup common_Xml;
    private DbsField common_Xml_Xml_Header;
    private DbsField common_Xml_Documentrequests_Open;
    private DbsField common_Xml_Documentrequests_Close;
    private DbsField common_Xml_Documentrequest_Open;
    private DbsField common_Xml_Documentrequest_Close;
    private DbsField common_Xml_Requestdatetime_Open;
    private DbsField common_Xml_Requestdatetime_Data;
    private DbsField common_Xml_Requestdatetime_Close;
    private DbsField common_Xml_Batchind_Open;
    private DbsField common_Xml_Batchind_Close;
    private DbsField common_Xml_Batchcounter_Open;
    private DbsField common_Xml_Batchcounter_Data;
    private DbsField common_Xml_Batchcounter_Close;
    private DbsField common_Xml_Batchtotal_Open;
    private DbsField common_Xml_Batchtotal_Data;
    private DbsField common_Xml_Batchtotal_Close;
    private DbsField common_Xml_Mailiteminfo_Open;
    private DbsField common_Xml_Mailiteminfo_Close;
    private DbsField common_Xml_Applicationid_Open;
    private DbsField common_Xml_Applicationid_Close;
    private DbsField common_Xml_Universalid_Open;
    private DbsField common_Xml_Universalid_Data;
    private DbsField common_Xml_Universalid_Close;
    private DbsField common_Xml_Universaltype_Open;
    private DbsField common_Xml_Universaltype_Data;
    private DbsField common_Xml_Universaltype_Close;
    private DbsField common_Xml_Documentrequestid_Open;
    private DbsField common_Xml_Documentrequestid_Data;
    private DbsGroup common_Xml_Documentrequestid_DataRedef1;
    private DbsField common_Xml_Pnd_First_Twenty;
    private DbsField common_Xml_Pnd_Filler;
    private DbsGroup common_Xml_Documentrequestid_DataRedef2;
    private DbsField common_Xml_Documentrequestid_Sys_Id;
    private DbsField common_Xml_Documentrequestid_Sub_Sys_Id;
    private DbsField common_Xml_Documentrequestid_Yyyy;
    private DbsField common_Xml_Documentrequestid_Mm;
    private DbsField common_Xml_Documentrequestid_Dd;
    private DbsField common_Xml_Documentrequestid_Hh;
    private DbsField common_Xml_Documentrequestid_Ii;
    private DbsField common_Xml_Documentrequestid_Ss;
    private DbsField common_Xml_Documentrequestid_Uid;
    private DbsField common_Xml_Documentrequestid_Cnt;
    private DbsField common_Xml_Documentrequestid_Close;
    private DbsField common_Xml_Printerid_Open;
    private DbsField common_Xml_Printerid_Data;
    private DbsField common_Xml_Printerid_Close;
    private DbsField common_Xml_Fullname_Open;
    private DbsField common_Xml_Fullname_Data;
    private DbsField common_Xml_Fullname_Close;
    private DbsField common_Xml_Addresstypecode_Open;
    private DbsField common_Xml_Addresstypecode_Data;
    private DbsField common_Xml_Addresstypecode_Close;
    private DbsField common_Xml_Letterdate_Open;
    private DbsField common_Xml_Letterdate_Data;
    private DbsField common_Xml_Letterdate_Close;
    private DbsField common_Xml_Deliverytype_Open;
    private DbsField common_Xml_Deliverytype_Data;
    private DbsField common_Xml_Deliverytype_Close;
    private DbsField common_Xml_Expagind_Open;
    private DbsField common_Xml_Expagind_Data;
    private DbsField common_Xml_Expagind_Close;
    private DbsField common_Xml_Archivalind_Open;
    private DbsField common_Xml_Archivalind_Data;
    private DbsField common_Xml_Archivalind_Close;
    private DbsField common_Xml_Planid_Open;
    private DbsField common_Xml_Planid_Data;
    private DbsField common_Xml_Planid_Close;
    private DbsField common_Xml_Businessdate_Open;
    private DbsField common_Xml_Businessdate_Data;
    private DbsField common_Xml_Businessdate_Close;
    private DbsField common_Xml_Addresslines_Open;
    private DbsField common_Xml_Addresslines_Close;
    private DbsField common_Xml_Addressline_Open;
    private DbsField common_Xml_Addressline_Data;
    private DbsField common_Xml_Addressline_Close;
    private DbsField common_Xml_Documentinfo_Open;
    private DbsField common_Xml_Documentinfo_Close;
    private DbsField common_Xml_Prolinechecks_Open;
    private DbsField common_Xml_Prolinechecks_Close;
    private DbsField common_Xml_Headerinfo_Open;
    private DbsField common_Xml_Headerinfo_Close;
    private DbsField common_Xml_Settlementsystem_Open;
    private DbsField common_Xml_Settlementsystem_Data;
    private DbsField common_Xml_Settlementsystem_Close;
    private DbsField common_Xml_Checkeftind_Open;
    private DbsField common_Xml_Checkeftind_Data;
    private DbsField common_Xml_Checkeftind_Close;
    private DbsField common_Xml_Holdcode_Open;
    private DbsField common_Xml_Holdcode_Data;
    private DbsField common_Xml_Holdcode_Close;
    private DbsField common_Xml_Tiaaaddress_Open;
    private DbsField common_Xml_Tiaaaddress_Close;
    private DbsField common_Xml_Tiaaaddressline_Open;
    private DbsField common_Xml_Tiaaaddressline_Data;
    private DbsField common_Xml_Tiaaaddressline_Close;
    private DbsField common_Xml_Nonids_Index;
    private DbsField common_Xml_Ids_Index;
    private DbsField common_Xml_Tiaaphonenumber_Open;
    private DbsField common_Xml_Tiaaphonenumber_Data;
    private DbsField common_Xml_Tiaaphonenumber_Close;
    private DbsField common_Xml_Bankname_Open;
    private DbsField common_Xml_Bankname_Data;
    private DbsField common_Xml_Bankname_Close;
    private DbsField common_Xml_Bankaddress_Open;
    private DbsField common_Xml_Bankaddress_Close;
    private DbsField common_Xml_Bankaddressline_Open;
    private DbsField common_Xml_Bankaddressline_Data;
    private DbsField common_Xml_Bankaddressline_Close;
    private DbsField common_Xml_Bankfractional1_Open;
    private DbsField common_Xml_Bankfractional1_Data;
    private DbsField common_Xml_Bankfractional1_Close;
    private DbsField common_Xml_Bankfractional2_Open;
    private DbsField common_Xml_Bankfractional2_Data;
    private DbsField common_Xml_Bankfractional2_Close;
    private DbsField common_Xml_Bankroutingnumber_Open;
    private DbsField common_Xml_Bankroutingnumber_Data;
    private DbsField common_Xml_Bankroutingnumber_Close;
    private DbsField common_Xml_Bankaccountnumber_Open;
    private DbsField common_Xml_Bankaccountnumber_Data;
    private DbsField common_Xml_Bankaccountnumber_Close;
    private DbsField common_Xml_Checknumber_Open;
    private DbsField common_Xml_Checknumber_Data;
    private DbsField common_Xml_Checknumber_Close;
    private DbsField common_Xml_Checkdate_Open;
    private DbsField common_Xml_Checkdate_Data;
    private DbsField common_Xml_Checkdate_Close;
    private DbsField common_Xml_Checkamount_Open;
    private DbsField common_Xml_Checkamount_Data;
    private DbsField common_Xml_Checkamount_Close;
    private DbsField common_Xml_Accountname_Open;
    private DbsField common_Xml_Accountname_Data;
    private DbsField common_Xml_Accountname_Close;
    private DbsField common_Xml_Signatorytitle_Open;
    private DbsField common_Xml_Signatorytitle_Data;
    private DbsField common_Xml_Signatorytitle_Close;
    private DbsField common_Xml_Settlementsystemcodes_Open;
    private DbsField common_Xml_Settlementsystemcodes_Data;
    private DbsField common_Xml_Settlementsystemcodes_Close;
    private DbsField common_Xml_Nonidsinfo_Open;
    private DbsField common_Xml_Nonidsinfo_Close;
    private DbsField common_Xml_Participantname_Open;
    private DbsField common_Xml_Participantname_Data;
    private DbsField common_Xml_Participantname_Close;
    private DbsField common_Xml_Tiaacontract_Open;
    private DbsField common_Xml_Tiaacontract_Data;
    private DbsField common_Xml_Tiaacontract_Close;
    private DbsField common_Xml_Crefcontract_Open;
    private DbsField common_Xml_Crefcontract_Data;
    private DbsField common_Xml_Crefcontract_Close;
    private DbsField common_Xml_Grossamount_Open;
    private DbsField common_Xml_Grossamount_Data;
    private DbsField common_Xml_Grossamount_Close;
    private DbsField common_Xml_Totaltaxwitheld_Open;
    private DbsField common_Xml_Totaltaxwitheld_Data;
    private DbsField common_Xml_Totaltaxwitheld_Close;
    private DbsField common_Xml_Taxeswithheld_Open;
    private DbsField common_Xml_Taxeswithheld_Close;
    private DbsField common_Xml_Taxeswithheldinfo_Open;
    private DbsField common_Xml_Taxeswithheldinfo_Close;
    private DbsField common_Xml_Taxdescription_Open;
    private DbsField common_Xml_Taxdescription_Data;
    private DbsField common_Xml_Taxdescription_Close;
    private DbsField common_Xml_Taxamount_Open;
    private DbsField common_Xml_Taxamount_Data;
    private DbsField common_Xml_Taxamount_Close;
    private DbsField common_Xml_Totaltaxwithheld_Open;
    private DbsField common_Xml_Totaltaxwithheld_Data;
    private DbsField common_Xml_Totaltaxwithheld_Close;
    private DbsField common_Xml_Otherdeductiondesc_Open;
    private DbsField common_Xml_Otherdeductiondesc_Data;
    private DbsField common_Xml_Otherdeductiondesc_Close;
    private DbsField common_Xml_Otherdeductionamount_Open;
    private DbsField common_Xml_Otherdeductionamount_Data;
    private DbsField common_Xml_Otherdeductionamount_Close;
    private DbsField common_Xml_Netamount_Open;
    private DbsField common_Xml_Netamount_Data;
    private DbsField common_Xml_Netamount_Close;
    private DbsField common_Xml_Aftertaxdesc_Open;
    private DbsField common_Xml_Aftertaxdesc_Data;
    private DbsField common_Xml_Aftertaxdesc_Close;
    private DbsField common_Xml_Aftertaxamount_Open;
    private DbsField common_Xml_Aftertaxamount_Data;
    private DbsField common_Xml_Aftertaxamount_Close;
    private DbsField common_Xml_Memofield_Open;
    private DbsField common_Xml_Memofield_Data;
    private DbsField common_Xml_Memofield_Close;
    private DbsField common_Xml_Memofieldext_Open;
    private DbsField common_Xml_Memofieldext_Data;
    private DbsField common_Xml_Memofieldext_Close;
    private DbsField common_Xml_Idsinfo_Open;
    private DbsField common_Xml_Idsinfo_Close;
    private DbsField common_Xml_Disbursementdescription_Open;
    private DbsField common_Xml_Disbursementdescription_Data;
    private DbsField common_Xml_Disbursementdescription_Close;
    private DbsField common_Xml_Namelabel_Open;
    private DbsField common_Xml_Namelabel_Data;
    private DbsField common_Xml_Namelabel_Close;
    private DbsField common_Xml_Insuredname_Open;
    private DbsField common_Xml_Insuredname_Data;
    private DbsField common_Xml_Insuredname_Close;
    private DbsField common_Xml_Documentlabel1_Open;
    private DbsField common_Xml_Documentlabel1_Data;
    private DbsField common_Xml_Documentlabel1_Close;
    private DbsField common_Xml_Documentlabel2_Open;
    private DbsField common_Xml_Documentlabel2_Data;
    private DbsField common_Xml_Documentlabel2_Close;
    private DbsField common_Xml_Documentnumber_Open;
    private DbsField common_Xml_Documentnumber_Data;
    private DbsField common_Xml_Documentnumber_Close;
    private DbsField common_Xml_Comments_Open;
    private DbsField common_Xml_Comments_Close;
    private DbsField common_Xml_Commentline_Open;
    private DbsField common_Xml_Commentline_Data;
    private DbsField common_Xml_Commentline_Close;
    private DbsField common_Xml_Applicationid2_Open;
    private DbsField common_Xml_Applicationid2_Data;
    private DbsField common_Xml_Applicationid2_Close;
    private DbsField common_Xml_Initialpaymentfrequency_Open;
    private DbsField common_Xml_Initialpaymentfrequency_Data;
    private DbsField common_Xml_Initialpaymentfrequency_Close;
    private DbsField common_Xml_Typeofannuity_Open;
    private DbsField common_Xml_Typeofannuity_Data;
    private DbsField common_Xml_Typeofannuity_Close;
    private DbsField common_Xml_Tiaacreflist_Open;
    private DbsField common_Xml_Tiaacreflist_Close;
    private DbsField common_Xml_Settlementlist_Open;
    private DbsField common_Xml_Settlementlist_Data;
    private DbsField common_Xml_Settlementlist_Close;
    private DbsField common_Xml_Tiaacrefnumber_Open;
    private DbsField common_Xml_Tiaacrefnumber_Data;
    private DbsField common_Xml_Tiaacrefnumber_Close;
    private DbsField common_Xml_Tiaacrefind_Open;
    private DbsField common_Xml_Tiaacrefind_Data;
    private DbsField common_Xml_Tiaacrefind_Close;
    private DbsField common_Xml_Annuitystartdate_Open;
    private DbsField common_Xml_Annuitystartdate_Data;
    private DbsField common_Xml_Annuitystartdate_Close;
    private DbsField common_Xml_Paymentdetail_Open;
    private DbsField common_Xml_Paymentdetail_Close;
    private DbsField common_Xml_Paymentinfo_Open;
    private DbsField common_Xml_Paymentinfo_Close;
    private DbsField common_Xml_Duedate_Open;
    private DbsField common_Xml_Duedate_Data;
    private DbsField common_Xml_Duedate_Close;
    private DbsField common_Xml_Duedatefuture_Open;
    private DbsField common_Xml_Duedatefuture_Data;
    private DbsField common_Xml_Duedatefuture_Close;
    private DbsField common_Xml_Description_Open;
    private DbsField common_Xml_Description_Data;
    private DbsField common_Xml_Description_Close;
    private DbsField common_Xml_Deductionamountfrequency_Open;
    private DbsField common_Xml_Deductionamountfrequency_Data;
    private DbsField common_Xml_Deductionamountfrequency_Close;
    private DbsField common_Xml_Paymentamount_Open;
    private DbsField common_Xml_Paymentamount_Data;
    private DbsField common_Xml_Paymentamount_Close;
    private DbsField common_Xml_Paymentfrequency_Open;
    private DbsField common_Xml_Paymentfrequency_Data;
    private DbsField common_Xml_Paymentfrequency_Close;
    private DbsField common_Xml_Unit_Open;
    private DbsField common_Xml_Unit_Data;
    private DbsField common_Xml_Unit_Close;
    private DbsField common_Xml_Unitvalue_Open;
    private DbsField common_Xml_Unitvalue_Data;
    private DbsField common_Xml_Unitvalue_Close;
    private DbsField common_Xml_Contractual_Open;
    private DbsField common_Xml_Contractual_Data;
    private DbsField common_Xml_Contractual_Close;
    private DbsField common_Xml_Dividend_Open;
    private DbsField common_Xml_Dividend_Data;
    private DbsField common_Xml_Dividend_Close;
    private DbsField common_Xml_Tiaacontractpayment_Open;
    private DbsField common_Xml_Tiaacontractpayment_Data;
    private DbsField common_Xml_Tiaacontractpayment_Close;
    private DbsField common_Xml_Crefcertificate_Open;
    private DbsField common_Xml_Crefcertificate_Data;
    private DbsField common_Xml_Crefcertificate_Close;
    private DbsField common_Xml_Paymentamountfrom_Open;
    private DbsField common_Xml_Paymentamountfrom_Data;
    private DbsField common_Xml_Paymentamountfrom_Close;
    private DbsField common_Xml_Paymentamountfromfrequency_Open;
    private DbsField common_Xml_Paymentamountfromfrequency_Data;
    private DbsField common_Xml_Paymentamountfromfrequency_Close;
    private DbsField common_Xml_Interest_Open;
    private DbsField common_Xml_Interest_Data;
    private DbsField common_Xml_Interest_Close;
    private DbsField common_Xml_Interestpayment_Open;
    private DbsField common_Xml_Interestpayment_Data;
    private DbsField common_Xml_Interestpayment_Close;
    private DbsField common_Xml_Netpaymentamount_Open;
    private DbsField common_Xml_Netpaymentamount_Data;
    private DbsField common_Xml_Netpaymentamount_Close;
    private DbsField common_Xml_Subpaymentdetail_Open;
    private DbsField common_Xml_Subpaymentdetail_Close;
    private DbsField common_Xml_Subdate_Open;
    private DbsField common_Xml_Subdate_Data;
    private DbsField common_Xml_Subdate_Close;
    private DbsField common_Xml_Subtiaacontract_Open;
    private DbsField common_Xml_Subtiaacontract_Data;
    private DbsField common_Xml_Subtiaacontract_Close;
    private DbsField common_Xml_Subcrefcertificate_Open;
    private DbsField common_Xml_Subcrefcertificate_Data;
    private DbsField common_Xml_Subcrefcertificate_Close;
    private DbsField common_Xml_Subpaymentfrom_Open;
    private DbsField common_Xml_Subpaymentfrom_Data;
    private DbsField common_Xml_Subpaymentfrom_Close;
    private DbsField common_Xml_Subpaymentfromfrequency_Open;
    private DbsField common_Xml_Subpaymentfromfrequency_Data;
    private DbsField common_Xml_Subpaymentfromfrequency_Close;
    private DbsField common_Xml_Subpaymentamount_Open;
    private DbsField common_Xml_Subpaymentamount_Data;
    private DbsField common_Xml_Subpaymentamount_Close;
    private DbsField common_Xml_Subpaymentamountfrequency_Open;
    private DbsField common_Xml_Subpaymentamountfrequency_Data;
    private DbsField common_Xml_Subpaymentamountfrequency_Close;
    private DbsField common_Xml_Subdeductionamount_Open;
    private DbsField common_Xml_Subdeductionamount_Data;
    private DbsField common_Xml_Subdeductionamount_Close;
    private DbsField common_Xml_Subdeductionamountfrequency_Open;
    private DbsField common_Xml_Subdeductionamountfrequency_Data;
    private DbsField common_Xml_Subdeductionamountfrequency_Clos;
    private DbsField common_Xml_Totaldeductionamount_Open;
    private DbsField common_Xml_Totaldeductionamount_Data;
    private DbsField common_Xml_Totaldeductionamount_Close;
    private DbsField common_Xml_Deductions_Open;
    private DbsField common_Xml_Deductions_Close;
    private DbsField common_Xml_Deductionsinfo_Open;
    private DbsField common_Xml_Deductionsinfo_Close;
    private DbsField common_Xml_Deductiondescription_Open;
    private DbsField common_Xml_Deductiondescription_Data;
    private DbsField common_Xml_Deductiondescription_Close;
    private DbsField common_Xml_Deductionamount_Open;
    private DbsField common_Xml_Deductionamount_Data;
    private DbsField common_Xml_Deductionamount_Close;
    private DbsField common_Xml_Institutionname_Open;
    private DbsField common_Xml_Institutionname_Data;
    private DbsField common_Xml_Institutionname_Close;
    private DbsField common_Xml_Institutionaddress_Open;
    private DbsField common_Xml_Institutionaddress_Close;
    private DbsField common_Xml_Institutionaddress_Lines_Open;
    private DbsField common_Xml_Institutionaddress_Lines_Data;
    private DbsField common_Xml_Institutionaddress_Lines_Close;
    private DbsField common_Xml_Institutioninfo_Open;
    private DbsField common_Xml_Institutioninfo_Close;
    private DbsField common_Xml_Institutioninfoline_Open;
    private DbsField common_Xml_Institutioninfoline_Data;
    private DbsField common_Xml_Institutioninfoline_Close;
    private DbsField common_Xml_Participantinstitutionind_Open;
    private DbsField common_Xml_Participantinstitutionind_Data;
    private DbsField common_Xml_Participantinstitutionind_Close;
    private DbsField common_Xml_Omnipay_Open;
    private DbsField common_Xml_Omnipay_Close;
    private DbsField common_Xml_Documenttype_Open;
    private DbsField common_Xml_Documenttype_Data;
    private DbsField common_Xml_Documenttype_Close;
    private DbsField common_Xml_Participantinstitutionflag_Open;
    private DbsField common_Xml_Participantinstitutionflag_Data;
    private DbsField common_Xml_Participantinstitutionflag_Clos;
    private DbsField common_Xml_Paymentfor_Open;
    private DbsField common_Xml_Paymentfor_Close;
    private DbsField common_Xml_Tiaanumber_Open;
    private DbsField common_Xml_Tiaanumber_Data;
    private DbsField common_Xml_Tiaanumber_Close;
    private DbsField common_Xml_Crefnumber_Open;
    private DbsField common_Xml_Crefnumber_Data;
    private DbsField common_Xml_Crefnumber_Close;
    private DbsField common_Xml_Effectivedate_Open;
    private DbsField common_Xml_Effectivedate_Data;
    private DbsField common_Xml_Effectivedate_Close;
    private DbsField common_Xml_Forparticipant_Open;
    private DbsField common_Xml_Forparticipant_Close;
    private DbsField common_Xml_Foralternatecarrier_Open;
    private DbsField common_Xml_Foralternatecarrier_Close;
    private DbsField common_Xml_Paymentinformationsummary_Open;
    private DbsField common_Xml_Paymentinformationsummary_Close;
    private DbsField common_Xml_Paymentdetailinformation_Open;
    private DbsField common_Xml_Paymentdetailinformation_Close;
    private DbsField common_Xml_Accountnumber_Open;
    private DbsField common_Xml_Accountnumber_Data;
    private DbsField common_Xml_Accountnumber_Close;
    private DbsField common_Xml_Transferaccountslist_Open;
    private DbsField common_Xml_Transferaccountslist_Close;
    private DbsField common_Xml_Transferaccounts_Open;
    private DbsField common_Xml_Transferaccounts_Data;
    private DbsField common_Xml_Transferaccounts_Close;
    private DbsField common_Xml_Employer_Open;
    private DbsField common_Xml_Employer_Data;
    private DbsField common_Xml_Employer_Close;
    private DbsField common_Xml_Typeofplan_Open;
    private DbsField common_Xml_Typeofplan_Data;
    private DbsField common_Xml_Typeofplan_Close;
    private DbsField common_Xml_Cashavailable_Open;
    private DbsField common_Xml_Cashavailable_Data;
    private DbsField common_Xml_Cashavailable_Close;
    private DbsField common_Xml_Distributionrequirements_Open;
    private DbsField common_Xml_Distributionrequirements_Close;
    private DbsField common_Xml_Paymentinformation_Open;
    private DbsField common_Xml_Paymentinformation_Close;
    private DbsField common_Xml_Deductionstype_Open;
    private DbsField common_Xml_Deductionstype_Close;
    private DbsField common_Xml_Grosstotaldeductions_Open;
    private DbsField common_Xml_Grosstotaldeductions_Data;
    private DbsField common_Xml_Grosstotaldeductions_Close;
    private DbsField common_Xml_Grosstotalpayments_Open;
    private DbsField common_Xml_Grosstotalpayments_Data;
    private DbsField common_Xml_Grosstotalpayments_Close;
    private DbsField common_Xml_Nettotalpayments_Open;
    private DbsField common_Xml_Nettotalpayments_Data;
    private DbsField common_Xml_Nettotalpayments_Close;
    private DbsField common_Xml_Contractsorcertificates_Open;
    private DbsField common_Xml_Contractsorcertificates_Data;
    private DbsField common_Xml_Contractsorcertificates_Close;
    private DbsField common_Xml_Planname_Open;
    private DbsField common_Xml_Planname_Data;
    private DbsField common_Xml_Planname_Close;
    private DbsField common_Xml_Processingdate_Open;
    private DbsField common_Xml_Processingdate_Data;
    private DbsField common_Xml_Processingdate_Close;
    private DbsField common_Xml_Accountsinformation_Open;
    private DbsField common_Xml_Accountsinformation_Close;
    private DbsField common_Xml_Disclaimercodes_Open;
    private DbsField common_Xml_Disclaimercodes_Close;
    private DbsField common_Xml_Disclaimercodestype_Open;
    private DbsField common_Xml_Disclaimercodestype_Data;
    private DbsField common_Xml_Disclaimercodestype_Close;
    private DbsField common_Xml_Decedentname_Open;
    private DbsField common_Xml_Decedentname_Data;
    private DbsField common_Xml_Decedentname_Close;
    private DbsField common_Xml_Paymentlabel_Open;
    private DbsField common_Xml_Paymentlabel_Data;
    private DbsField common_Xml_Paymentlabel_Close;
    private DbsField common_Xml_Contractlabel_Open;
    private DbsField common_Xml_Contractlabel_Data;
    private DbsField common_Xml_Contractlabel_Close;
    private DbsField common_Xml_Paymentdate_Open;
    private DbsField common_Xml_Paymentdate_Data;
    private DbsField common_Xml_Paymentdate_Close;
    private DbsField common_Xml_Totalinterest_Open;
    private DbsField common_Xml_Totalinterest_Data;
    private DbsField common_Xml_Totalinterest_Close;
    private DbsField common_Xml_Totalnetpaymentamount_Open;
    private DbsField common_Xml_Totalnetpaymentamount_Data;
    private DbsField common_Xml_Totalnetpaymentamount_Close;
    private DbsField common_Xml_Totalpaymentamount_Open;
    private DbsField common_Xml_Totalpaymentamount_Data;
    private DbsField common_Xml_Totalpaymentamount_Close;
    private DbsField common_Xml_Interestind_Open;
    private DbsField common_Xml_Interestind_Data;
    private DbsField common_Xml_Interestind_Close;
    private DbsField common_Xml_Deductionamountind_Open;
    private DbsField common_Xml_Deductionamountind_Data;
    private DbsField common_Xml_Deductionamountind_Close;

    public DbsGroup getCommon_Xml() { return common_Xml; }

    public DbsField getCommon_Xml_Xml_Header() { return common_Xml_Xml_Header; }

    public DbsField getCommon_Xml_Documentrequests_Open() { return common_Xml_Documentrequests_Open; }

    public DbsField getCommon_Xml_Documentrequests_Close() { return common_Xml_Documentrequests_Close; }

    public DbsField getCommon_Xml_Documentrequest_Open() { return common_Xml_Documentrequest_Open; }

    public DbsField getCommon_Xml_Documentrequest_Close() { return common_Xml_Documentrequest_Close; }

    public DbsField getCommon_Xml_Requestdatetime_Open() { return common_Xml_Requestdatetime_Open; }

    public DbsField getCommon_Xml_Requestdatetime_Data() { return common_Xml_Requestdatetime_Data; }

    public DbsField getCommon_Xml_Requestdatetime_Close() { return common_Xml_Requestdatetime_Close; }

    public DbsField getCommon_Xml_Batchind_Open() { return common_Xml_Batchind_Open; }

    public DbsField getCommon_Xml_Batchind_Close() { return common_Xml_Batchind_Close; }

    public DbsField getCommon_Xml_Batchcounter_Open() { return common_Xml_Batchcounter_Open; }

    public DbsField getCommon_Xml_Batchcounter_Data() { return common_Xml_Batchcounter_Data; }

    public DbsField getCommon_Xml_Batchcounter_Close() { return common_Xml_Batchcounter_Close; }

    public DbsField getCommon_Xml_Batchtotal_Open() { return common_Xml_Batchtotal_Open; }

    public DbsField getCommon_Xml_Batchtotal_Data() { return common_Xml_Batchtotal_Data; }

    public DbsField getCommon_Xml_Batchtotal_Close() { return common_Xml_Batchtotal_Close; }

    public DbsField getCommon_Xml_Mailiteminfo_Open() { return common_Xml_Mailiteminfo_Open; }

    public DbsField getCommon_Xml_Mailiteminfo_Close() { return common_Xml_Mailiteminfo_Close; }

    public DbsField getCommon_Xml_Applicationid_Open() { return common_Xml_Applicationid_Open; }

    public DbsField getCommon_Xml_Applicationid_Close() { return common_Xml_Applicationid_Close; }

    public DbsField getCommon_Xml_Universalid_Open() { return common_Xml_Universalid_Open; }

    public DbsField getCommon_Xml_Universalid_Data() { return common_Xml_Universalid_Data; }

    public DbsField getCommon_Xml_Universalid_Close() { return common_Xml_Universalid_Close; }

    public DbsField getCommon_Xml_Universaltype_Open() { return common_Xml_Universaltype_Open; }

    public DbsField getCommon_Xml_Universaltype_Data() { return common_Xml_Universaltype_Data; }

    public DbsField getCommon_Xml_Universaltype_Close() { return common_Xml_Universaltype_Close; }

    public DbsField getCommon_Xml_Documentrequestid_Open() { return common_Xml_Documentrequestid_Open; }

    public DbsField getCommon_Xml_Documentrequestid_Data() { return common_Xml_Documentrequestid_Data; }

    public DbsGroup getCommon_Xml_Documentrequestid_DataRedef1() { return common_Xml_Documentrequestid_DataRedef1; }

    public DbsField getCommon_Xml_Pnd_First_Twenty() { return common_Xml_Pnd_First_Twenty; }

    public DbsField getCommon_Xml_Pnd_Filler() { return common_Xml_Pnd_Filler; }

    public DbsGroup getCommon_Xml_Documentrequestid_DataRedef2() { return common_Xml_Documentrequestid_DataRedef2; }

    public DbsField getCommon_Xml_Documentrequestid_Sys_Id() { return common_Xml_Documentrequestid_Sys_Id; }

    public DbsField getCommon_Xml_Documentrequestid_Sub_Sys_Id() { return common_Xml_Documentrequestid_Sub_Sys_Id; }

    public DbsField getCommon_Xml_Documentrequestid_Yyyy() { return common_Xml_Documentrequestid_Yyyy; }

    public DbsField getCommon_Xml_Documentrequestid_Mm() { return common_Xml_Documentrequestid_Mm; }

    public DbsField getCommon_Xml_Documentrequestid_Dd() { return common_Xml_Documentrequestid_Dd; }

    public DbsField getCommon_Xml_Documentrequestid_Hh() { return common_Xml_Documentrequestid_Hh; }

    public DbsField getCommon_Xml_Documentrequestid_Ii() { return common_Xml_Documentrequestid_Ii; }

    public DbsField getCommon_Xml_Documentrequestid_Ss() { return common_Xml_Documentrequestid_Ss; }

    public DbsField getCommon_Xml_Documentrequestid_Uid() { return common_Xml_Documentrequestid_Uid; }

    public DbsField getCommon_Xml_Documentrequestid_Cnt() { return common_Xml_Documentrequestid_Cnt; }

    public DbsField getCommon_Xml_Documentrequestid_Close() { return common_Xml_Documentrequestid_Close; }

    public DbsField getCommon_Xml_Printerid_Open() { return common_Xml_Printerid_Open; }

    public DbsField getCommon_Xml_Printerid_Data() { return common_Xml_Printerid_Data; }

    public DbsField getCommon_Xml_Printerid_Close() { return common_Xml_Printerid_Close; }

    public DbsField getCommon_Xml_Fullname_Open() { return common_Xml_Fullname_Open; }

    public DbsField getCommon_Xml_Fullname_Data() { return common_Xml_Fullname_Data; }

    public DbsField getCommon_Xml_Fullname_Close() { return common_Xml_Fullname_Close; }

    public DbsField getCommon_Xml_Addresstypecode_Open() { return common_Xml_Addresstypecode_Open; }

    public DbsField getCommon_Xml_Addresstypecode_Data() { return common_Xml_Addresstypecode_Data; }

    public DbsField getCommon_Xml_Addresstypecode_Close() { return common_Xml_Addresstypecode_Close; }

    public DbsField getCommon_Xml_Letterdate_Open() { return common_Xml_Letterdate_Open; }

    public DbsField getCommon_Xml_Letterdate_Data() { return common_Xml_Letterdate_Data; }

    public DbsField getCommon_Xml_Letterdate_Close() { return common_Xml_Letterdate_Close; }

    public DbsField getCommon_Xml_Deliverytype_Open() { return common_Xml_Deliverytype_Open; }

    public DbsField getCommon_Xml_Deliverytype_Data() { return common_Xml_Deliverytype_Data; }

    public DbsField getCommon_Xml_Deliverytype_Close() { return common_Xml_Deliverytype_Close; }

    public DbsField getCommon_Xml_Expagind_Open() { return common_Xml_Expagind_Open; }

    public DbsField getCommon_Xml_Expagind_Data() { return common_Xml_Expagind_Data; }

    public DbsField getCommon_Xml_Expagind_Close() { return common_Xml_Expagind_Close; }

    public DbsField getCommon_Xml_Archivalind_Open() { return common_Xml_Archivalind_Open; }

    public DbsField getCommon_Xml_Archivalind_Data() { return common_Xml_Archivalind_Data; }

    public DbsField getCommon_Xml_Archivalind_Close() { return common_Xml_Archivalind_Close; }

    public DbsField getCommon_Xml_Planid_Open() { return common_Xml_Planid_Open; }

    public DbsField getCommon_Xml_Planid_Data() { return common_Xml_Planid_Data; }

    public DbsField getCommon_Xml_Planid_Close() { return common_Xml_Planid_Close; }

    public DbsField getCommon_Xml_Businessdate_Open() { return common_Xml_Businessdate_Open; }

    public DbsField getCommon_Xml_Businessdate_Data() { return common_Xml_Businessdate_Data; }

    public DbsField getCommon_Xml_Businessdate_Close() { return common_Xml_Businessdate_Close; }

    public DbsField getCommon_Xml_Addresslines_Open() { return common_Xml_Addresslines_Open; }

    public DbsField getCommon_Xml_Addresslines_Close() { return common_Xml_Addresslines_Close; }

    public DbsField getCommon_Xml_Addressline_Open() { return common_Xml_Addressline_Open; }

    public DbsField getCommon_Xml_Addressline_Data() { return common_Xml_Addressline_Data; }

    public DbsField getCommon_Xml_Addressline_Close() { return common_Xml_Addressline_Close; }

    public DbsField getCommon_Xml_Documentinfo_Open() { return common_Xml_Documentinfo_Open; }

    public DbsField getCommon_Xml_Documentinfo_Close() { return common_Xml_Documentinfo_Close; }

    public DbsField getCommon_Xml_Prolinechecks_Open() { return common_Xml_Prolinechecks_Open; }

    public DbsField getCommon_Xml_Prolinechecks_Close() { return common_Xml_Prolinechecks_Close; }

    public DbsField getCommon_Xml_Headerinfo_Open() { return common_Xml_Headerinfo_Open; }

    public DbsField getCommon_Xml_Headerinfo_Close() { return common_Xml_Headerinfo_Close; }

    public DbsField getCommon_Xml_Settlementsystem_Open() { return common_Xml_Settlementsystem_Open; }

    public DbsField getCommon_Xml_Settlementsystem_Data() { return common_Xml_Settlementsystem_Data; }

    public DbsField getCommon_Xml_Settlementsystem_Close() { return common_Xml_Settlementsystem_Close; }

    public DbsField getCommon_Xml_Checkeftind_Open() { return common_Xml_Checkeftind_Open; }

    public DbsField getCommon_Xml_Checkeftind_Data() { return common_Xml_Checkeftind_Data; }

    public DbsField getCommon_Xml_Checkeftind_Close() { return common_Xml_Checkeftind_Close; }

    public DbsField getCommon_Xml_Holdcode_Open() { return common_Xml_Holdcode_Open; }

    public DbsField getCommon_Xml_Holdcode_Data() { return common_Xml_Holdcode_Data; }

    public DbsField getCommon_Xml_Holdcode_Close() { return common_Xml_Holdcode_Close; }

    public DbsField getCommon_Xml_Tiaaaddress_Open() { return common_Xml_Tiaaaddress_Open; }

    public DbsField getCommon_Xml_Tiaaaddress_Close() { return common_Xml_Tiaaaddress_Close; }

    public DbsField getCommon_Xml_Tiaaaddressline_Open() { return common_Xml_Tiaaaddressline_Open; }

    public DbsField getCommon_Xml_Tiaaaddressline_Data() { return common_Xml_Tiaaaddressline_Data; }

    public DbsField getCommon_Xml_Tiaaaddressline_Close() { return common_Xml_Tiaaaddressline_Close; }

    public DbsField getCommon_Xml_Nonids_Index() { return common_Xml_Nonids_Index; }

    public DbsField getCommon_Xml_Ids_Index() { return common_Xml_Ids_Index; }

    public DbsField getCommon_Xml_Tiaaphonenumber_Open() { return common_Xml_Tiaaphonenumber_Open; }

    public DbsField getCommon_Xml_Tiaaphonenumber_Data() { return common_Xml_Tiaaphonenumber_Data; }

    public DbsField getCommon_Xml_Tiaaphonenumber_Close() { return common_Xml_Tiaaphonenumber_Close; }

    public DbsField getCommon_Xml_Bankname_Open() { return common_Xml_Bankname_Open; }

    public DbsField getCommon_Xml_Bankname_Data() { return common_Xml_Bankname_Data; }

    public DbsField getCommon_Xml_Bankname_Close() { return common_Xml_Bankname_Close; }

    public DbsField getCommon_Xml_Bankaddress_Open() { return common_Xml_Bankaddress_Open; }

    public DbsField getCommon_Xml_Bankaddress_Close() { return common_Xml_Bankaddress_Close; }

    public DbsField getCommon_Xml_Bankaddressline_Open() { return common_Xml_Bankaddressline_Open; }

    public DbsField getCommon_Xml_Bankaddressline_Data() { return common_Xml_Bankaddressline_Data; }

    public DbsField getCommon_Xml_Bankaddressline_Close() { return common_Xml_Bankaddressline_Close; }

    public DbsField getCommon_Xml_Bankfractional1_Open() { return common_Xml_Bankfractional1_Open; }

    public DbsField getCommon_Xml_Bankfractional1_Data() { return common_Xml_Bankfractional1_Data; }

    public DbsField getCommon_Xml_Bankfractional1_Close() { return common_Xml_Bankfractional1_Close; }

    public DbsField getCommon_Xml_Bankfractional2_Open() { return common_Xml_Bankfractional2_Open; }

    public DbsField getCommon_Xml_Bankfractional2_Data() { return common_Xml_Bankfractional2_Data; }

    public DbsField getCommon_Xml_Bankfractional2_Close() { return common_Xml_Bankfractional2_Close; }

    public DbsField getCommon_Xml_Bankroutingnumber_Open() { return common_Xml_Bankroutingnumber_Open; }

    public DbsField getCommon_Xml_Bankroutingnumber_Data() { return common_Xml_Bankroutingnumber_Data; }

    public DbsField getCommon_Xml_Bankroutingnumber_Close() { return common_Xml_Bankroutingnumber_Close; }

    public DbsField getCommon_Xml_Bankaccountnumber_Open() { return common_Xml_Bankaccountnumber_Open; }

    public DbsField getCommon_Xml_Bankaccountnumber_Data() { return common_Xml_Bankaccountnumber_Data; }

    public DbsField getCommon_Xml_Bankaccountnumber_Close() { return common_Xml_Bankaccountnumber_Close; }

    public DbsField getCommon_Xml_Checknumber_Open() { return common_Xml_Checknumber_Open; }

    public DbsField getCommon_Xml_Checknumber_Data() { return common_Xml_Checknumber_Data; }

    public DbsField getCommon_Xml_Checknumber_Close() { return common_Xml_Checknumber_Close; }

    public DbsField getCommon_Xml_Checkdate_Open() { return common_Xml_Checkdate_Open; }

    public DbsField getCommon_Xml_Checkdate_Data() { return common_Xml_Checkdate_Data; }

    public DbsField getCommon_Xml_Checkdate_Close() { return common_Xml_Checkdate_Close; }

    public DbsField getCommon_Xml_Checkamount_Open() { return common_Xml_Checkamount_Open; }

    public DbsField getCommon_Xml_Checkamount_Data() { return common_Xml_Checkamount_Data; }

    public DbsField getCommon_Xml_Checkamount_Close() { return common_Xml_Checkamount_Close; }

    public DbsField getCommon_Xml_Accountname_Open() { return common_Xml_Accountname_Open; }

    public DbsField getCommon_Xml_Accountname_Data() { return common_Xml_Accountname_Data; }

    public DbsField getCommon_Xml_Accountname_Close() { return common_Xml_Accountname_Close; }

    public DbsField getCommon_Xml_Signatorytitle_Open() { return common_Xml_Signatorytitle_Open; }

    public DbsField getCommon_Xml_Signatorytitle_Data() { return common_Xml_Signatorytitle_Data; }

    public DbsField getCommon_Xml_Signatorytitle_Close() { return common_Xml_Signatorytitle_Close; }

    public DbsField getCommon_Xml_Settlementsystemcodes_Open() { return common_Xml_Settlementsystemcodes_Open; }

    public DbsField getCommon_Xml_Settlementsystemcodes_Data() { return common_Xml_Settlementsystemcodes_Data; }

    public DbsField getCommon_Xml_Settlementsystemcodes_Close() { return common_Xml_Settlementsystemcodes_Close; }

    public DbsField getCommon_Xml_Nonidsinfo_Open() { return common_Xml_Nonidsinfo_Open; }

    public DbsField getCommon_Xml_Nonidsinfo_Close() { return common_Xml_Nonidsinfo_Close; }

    public DbsField getCommon_Xml_Participantname_Open() { return common_Xml_Participantname_Open; }

    public DbsField getCommon_Xml_Participantname_Data() { return common_Xml_Participantname_Data; }

    public DbsField getCommon_Xml_Participantname_Close() { return common_Xml_Participantname_Close; }

    public DbsField getCommon_Xml_Tiaacontract_Open() { return common_Xml_Tiaacontract_Open; }

    public DbsField getCommon_Xml_Tiaacontract_Data() { return common_Xml_Tiaacontract_Data; }

    public DbsField getCommon_Xml_Tiaacontract_Close() { return common_Xml_Tiaacontract_Close; }

    public DbsField getCommon_Xml_Crefcontract_Open() { return common_Xml_Crefcontract_Open; }

    public DbsField getCommon_Xml_Crefcontract_Data() { return common_Xml_Crefcontract_Data; }

    public DbsField getCommon_Xml_Crefcontract_Close() { return common_Xml_Crefcontract_Close; }

    public DbsField getCommon_Xml_Grossamount_Open() { return common_Xml_Grossamount_Open; }

    public DbsField getCommon_Xml_Grossamount_Data() { return common_Xml_Grossamount_Data; }

    public DbsField getCommon_Xml_Grossamount_Close() { return common_Xml_Grossamount_Close; }

    public DbsField getCommon_Xml_Totaltaxwitheld_Open() { return common_Xml_Totaltaxwitheld_Open; }

    public DbsField getCommon_Xml_Totaltaxwitheld_Data() { return common_Xml_Totaltaxwitheld_Data; }

    public DbsField getCommon_Xml_Totaltaxwitheld_Close() { return common_Xml_Totaltaxwitheld_Close; }

    public DbsField getCommon_Xml_Taxeswithheld_Open() { return common_Xml_Taxeswithheld_Open; }

    public DbsField getCommon_Xml_Taxeswithheld_Close() { return common_Xml_Taxeswithheld_Close; }

    public DbsField getCommon_Xml_Taxeswithheldinfo_Open() { return common_Xml_Taxeswithheldinfo_Open; }

    public DbsField getCommon_Xml_Taxeswithheldinfo_Close() { return common_Xml_Taxeswithheldinfo_Close; }

    public DbsField getCommon_Xml_Taxdescription_Open() { return common_Xml_Taxdescription_Open; }

    public DbsField getCommon_Xml_Taxdescription_Data() { return common_Xml_Taxdescription_Data; }

    public DbsField getCommon_Xml_Taxdescription_Close() { return common_Xml_Taxdescription_Close; }

    public DbsField getCommon_Xml_Taxamount_Open() { return common_Xml_Taxamount_Open; }

    public DbsField getCommon_Xml_Taxamount_Data() { return common_Xml_Taxamount_Data; }

    public DbsField getCommon_Xml_Taxamount_Close() { return common_Xml_Taxamount_Close; }

    public DbsField getCommon_Xml_Totaltaxwithheld_Open() { return common_Xml_Totaltaxwithheld_Open; }

    public DbsField getCommon_Xml_Totaltaxwithheld_Data() { return common_Xml_Totaltaxwithheld_Data; }

    public DbsField getCommon_Xml_Totaltaxwithheld_Close() { return common_Xml_Totaltaxwithheld_Close; }

    public DbsField getCommon_Xml_Otherdeductiondesc_Open() { return common_Xml_Otherdeductiondesc_Open; }

    public DbsField getCommon_Xml_Otherdeductiondesc_Data() { return common_Xml_Otherdeductiondesc_Data; }

    public DbsField getCommon_Xml_Otherdeductiondesc_Close() { return common_Xml_Otherdeductiondesc_Close; }

    public DbsField getCommon_Xml_Otherdeductionamount_Open() { return common_Xml_Otherdeductionamount_Open; }

    public DbsField getCommon_Xml_Otherdeductionamount_Data() { return common_Xml_Otherdeductionamount_Data; }

    public DbsField getCommon_Xml_Otherdeductionamount_Close() { return common_Xml_Otherdeductionamount_Close; }

    public DbsField getCommon_Xml_Netamount_Open() { return common_Xml_Netamount_Open; }

    public DbsField getCommon_Xml_Netamount_Data() { return common_Xml_Netamount_Data; }

    public DbsField getCommon_Xml_Netamount_Close() { return common_Xml_Netamount_Close; }

    public DbsField getCommon_Xml_Aftertaxdesc_Open() { return common_Xml_Aftertaxdesc_Open; }

    public DbsField getCommon_Xml_Aftertaxdesc_Data() { return common_Xml_Aftertaxdesc_Data; }

    public DbsField getCommon_Xml_Aftertaxdesc_Close() { return common_Xml_Aftertaxdesc_Close; }

    public DbsField getCommon_Xml_Aftertaxamount_Open() { return common_Xml_Aftertaxamount_Open; }

    public DbsField getCommon_Xml_Aftertaxamount_Data() { return common_Xml_Aftertaxamount_Data; }

    public DbsField getCommon_Xml_Aftertaxamount_Close() { return common_Xml_Aftertaxamount_Close; }

    public DbsField getCommon_Xml_Memofield_Open() { return common_Xml_Memofield_Open; }

    public DbsField getCommon_Xml_Memofield_Data() { return common_Xml_Memofield_Data; }

    public DbsField getCommon_Xml_Memofield_Close() { return common_Xml_Memofield_Close; }

    public DbsField getCommon_Xml_Memofieldext_Open() { return common_Xml_Memofieldext_Open; }

    public DbsField getCommon_Xml_Memofieldext_Data() { return common_Xml_Memofieldext_Data; }

    public DbsField getCommon_Xml_Memofieldext_Close() { return common_Xml_Memofieldext_Close; }

    public DbsField getCommon_Xml_Idsinfo_Open() { return common_Xml_Idsinfo_Open; }

    public DbsField getCommon_Xml_Idsinfo_Close() { return common_Xml_Idsinfo_Close; }

    public DbsField getCommon_Xml_Disbursementdescription_Open() { return common_Xml_Disbursementdescription_Open; }

    public DbsField getCommon_Xml_Disbursementdescription_Data() { return common_Xml_Disbursementdescription_Data; }

    public DbsField getCommon_Xml_Disbursementdescription_Close() { return common_Xml_Disbursementdescription_Close; }

    public DbsField getCommon_Xml_Namelabel_Open() { return common_Xml_Namelabel_Open; }

    public DbsField getCommon_Xml_Namelabel_Data() { return common_Xml_Namelabel_Data; }

    public DbsField getCommon_Xml_Namelabel_Close() { return common_Xml_Namelabel_Close; }

    public DbsField getCommon_Xml_Insuredname_Open() { return common_Xml_Insuredname_Open; }

    public DbsField getCommon_Xml_Insuredname_Data() { return common_Xml_Insuredname_Data; }

    public DbsField getCommon_Xml_Insuredname_Close() { return common_Xml_Insuredname_Close; }

    public DbsField getCommon_Xml_Documentlabel1_Open() { return common_Xml_Documentlabel1_Open; }

    public DbsField getCommon_Xml_Documentlabel1_Data() { return common_Xml_Documentlabel1_Data; }

    public DbsField getCommon_Xml_Documentlabel1_Close() { return common_Xml_Documentlabel1_Close; }

    public DbsField getCommon_Xml_Documentlabel2_Open() { return common_Xml_Documentlabel2_Open; }

    public DbsField getCommon_Xml_Documentlabel2_Data() { return common_Xml_Documentlabel2_Data; }

    public DbsField getCommon_Xml_Documentlabel2_Close() { return common_Xml_Documentlabel2_Close; }

    public DbsField getCommon_Xml_Documentnumber_Open() { return common_Xml_Documentnumber_Open; }

    public DbsField getCommon_Xml_Documentnumber_Data() { return common_Xml_Documentnumber_Data; }

    public DbsField getCommon_Xml_Documentnumber_Close() { return common_Xml_Documentnumber_Close; }

    public DbsField getCommon_Xml_Comments_Open() { return common_Xml_Comments_Open; }

    public DbsField getCommon_Xml_Comments_Close() { return common_Xml_Comments_Close; }

    public DbsField getCommon_Xml_Commentline_Open() { return common_Xml_Commentline_Open; }

    public DbsField getCommon_Xml_Commentline_Data() { return common_Xml_Commentline_Data; }

    public DbsField getCommon_Xml_Commentline_Close() { return common_Xml_Commentline_Close; }

    public DbsField getCommon_Xml_Applicationid2_Open() { return common_Xml_Applicationid2_Open; }

    public DbsField getCommon_Xml_Applicationid2_Data() { return common_Xml_Applicationid2_Data; }

    public DbsField getCommon_Xml_Applicationid2_Close() { return common_Xml_Applicationid2_Close; }

    public DbsField getCommon_Xml_Initialpaymentfrequency_Open() { return common_Xml_Initialpaymentfrequency_Open; }

    public DbsField getCommon_Xml_Initialpaymentfrequency_Data() { return common_Xml_Initialpaymentfrequency_Data; }

    public DbsField getCommon_Xml_Initialpaymentfrequency_Close() { return common_Xml_Initialpaymentfrequency_Close; }

    public DbsField getCommon_Xml_Typeofannuity_Open() { return common_Xml_Typeofannuity_Open; }

    public DbsField getCommon_Xml_Typeofannuity_Data() { return common_Xml_Typeofannuity_Data; }

    public DbsField getCommon_Xml_Typeofannuity_Close() { return common_Xml_Typeofannuity_Close; }

    public DbsField getCommon_Xml_Tiaacreflist_Open() { return common_Xml_Tiaacreflist_Open; }

    public DbsField getCommon_Xml_Tiaacreflist_Close() { return common_Xml_Tiaacreflist_Close; }

    public DbsField getCommon_Xml_Settlementlist_Open() { return common_Xml_Settlementlist_Open; }

    public DbsField getCommon_Xml_Settlementlist_Data() { return common_Xml_Settlementlist_Data; }

    public DbsField getCommon_Xml_Settlementlist_Close() { return common_Xml_Settlementlist_Close; }

    public DbsField getCommon_Xml_Tiaacrefnumber_Open() { return common_Xml_Tiaacrefnumber_Open; }

    public DbsField getCommon_Xml_Tiaacrefnumber_Data() { return common_Xml_Tiaacrefnumber_Data; }

    public DbsField getCommon_Xml_Tiaacrefnumber_Close() { return common_Xml_Tiaacrefnumber_Close; }

    public DbsField getCommon_Xml_Tiaacrefind_Open() { return common_Xml_Tiaacrefind_Open; }

    public DbsField getCommon_Xml_Tiaacrefind_Data() { return common_Xml_Tiaacrefind_Data; }

    public DbsField getCommon_Xml_Tiaacrefind_Close() { return common_Xml_Tiaacrefind_Close; }

    public DbsField getCommon_Xml_Annuitystartdate_Open() { return common_Xml_Annuitystartdate_Open; }

    public DbsField getCommon_Xml_Annuitystartdate_Data() { return common_Xml_Annuitystartdate_Data; }

    public DbsField getCommon_Xml_Annuitystartdate_Close() { return common_Xml_Annuitystartdate_Close; }

    public DbsField getCommon_Xml_Paymentdetail_Open() { return common_Xml_Paymentdetail_Open; }

    public DbsField getCommon_Xml_Paymentdetail_Close() { return common_Xml_Paymentdetail_Close; }

    public DbsField getCommon_Xml_Paymentinfo_Open() { return common_Xml_Paymentinfo_Open; }

    public DbsField getCommon_Xml_Paymentinfo_Close() { return common_Xml_Paymentinfo_Close; }

    public DbsField getCommon_Xml_Duedate_Open() { return common_Xml_Duedate_Open; }

    public DbsField getCommon_Xml_Duedate_Data() { return common_Xml_Duedate_Data; }

    public DbsField getCommon_Xml_Duedate_Close() { return common_Xml_Duedate_Close; }

    public DbsField getCommon_Xml_Duedatefuture_Open() { return common_Xml_Duedatefuture_Open; }

    public DbsField getCommon_Xml_Duedatefuture_Data() { return common_Xml_Duedatefuture_Data; }

    public DbsField getCommon_Xml_Duedatefuture_Close() { return common_Xml_Duedatefuture_Close; }

    public DbsField getCommon_Xml_Description_Open() { return common_Xml_Description_Open; }

    public DbsField getCommon_Xml_Description_Data() { return common_Xml_Description_Data; }

    public DbsField getCommon_Xml_Description_Close() { return common_Xml_Description_Close; }

    public DbsField getCommon_Xml_Deductionamountfrequency_Open() { return common_Xml_Deductionamountfrequency_Open; }

    public DbsField getCommon_Xml_Deductionamountfrequency_Data() { return common_Xml_Deductionamountfrequency_Data; }

    public DbsField getCommon_Xml_Deductionamountfrequency_Close() { return common_Xml_Deductionamountfrequency_Close; }

    public DbsField getCommon_Xml_Paymentamount_Open() { return common_Xml_Paymentamount_Open; }

    public DbsField getCommon_Xml_Paymentamount_Data() { return common_Xml_Paymentamount_Data; }

    public DbsField getCommon_Xml_Paymentamount_Close() { return common_Xml_Paymentamount_Close; }

    public DbsField getCommon_Xml_Paymentfrequency_Open() { return common_Xml_Paymentfrequency_Open; }

    public DbsField getCommon_Xml_Paymentfrequency_Data() { return common_Xml_Paymentfrequency_Data; }

    public DbsField getCommon_Xml_Paymentfrequency_Close() { return common_Xml_Paymentfrequency_Close; }

    public DbsField getCommon_Xml_Unit_Open() { return common_Xml_Unit_Open; }

    public DbsField getCommon_Xml_Unit_Data() { return common_Xml_Unit_Data; }

    public DbsField getCommon_Xml_Unit_Close() { return common_Xml_Unit_Close; }

    public DbsField getCommon_Xml_Unitvalue_Open() { return common_Xml_Unitvalue_Open; }

    public DbsField getCommon_Xml_Unitvalue_Data() { return common_Xml_Unitvalue_Data; }

    public DbsField getCommon_Xml_Unitvalue_Close() { return common_Xml_Unitvalue_Close; }

    public DbsField getCommon_Xml_Contractual_Open() { return common_Xml_Contractual_Open; }

    public DbsField getCommon_Xml_Contractual_Data() { return common_Xml_Contractual_Data; }

    public DbsField getCommon_Xml_Contractual_Close() { return common_Xml_Contractual_Close; }

    public DbsField getCommon_Xml_Dividend_Open() { return common_Xml_Dividend_Open; }

    public DbsField getCommon_Xml_Dividend_Data() { return common_Xml_Dividend_Data; }

    public DbsField getCommon_Xml_Dividend_Close() { return common_Xml_Dividend_Close; }

    public DbsField getCommon_Xml_Tiaacontractpayment_Open() { return common_Xml_Tiaacontractpayment_Open; }

    public DbsField getCommon_Xml_Tiaacontractpayment_Data() { return common_Xml_Tiaacontractpayment_Data; }

    public DbsField getCommon_Xml_Tiaacontractpayment_Close() { return common_Xml_Tiaacontractpayment_Close; }

    public DbsField getCommon_Xml_Crefcertificate_Open() { return common_Xml_Crefcertificate_Open; }

    public DbsField getCommon_Xml_Crefcertificate_Data() { return common_Xml_Crefcertificate_Data; }

    public DbsField getCommon_Xml_Crefcertificate_Close() { return common_Xml_Crefcertificate_Close; }

    public DbsField getCommon_Xml_Paymentamountfrom_Open() { return common_Xml_Paymentamountfrom_Open; }

    public DbsField getCommon_Xml_Paymentamountfrom_Data() { return common_Xml_Paymentamountfrom_Data; }

    public DbsField getCommon_Xml_Paymentamountfrom_Close() { return common_Xml_Paymentamountfrom_Close; }

    public DbsField getCommon_Xml_Paymentamountfromfrequency_Open() { return common_Xml_Paymentamountfromfrequency_Open; }

    public DbsField getCommon_Xml_Paymentamountfromfrequency_Data() { return common_Xml_Paymentamountfromfrequency_Data; }

    public DbsField getCommon_Xml_Paymentamountfromfrequency_Close() { return common_Xml_Paymentamountfromfrequency_Close; }

    public DbsField getCommon_Xml_Interest_Open() { return common_Xml_Interest_Open; }

    public DbsField getCommon_Xml_Interest_Data() { return common_Xml_Interest_Data; }

    public DbsField getCommon_Xml_Interest_Close() { return common_Xml_Interest_Close; }

    public DbsField getCommon_Xml_Interestpayment_Open() { return common_Xml_Interestpayment_Open; }

    public DbsField getCommon_Xml_Interestpayment_Data() { return common_Xml_Interestpayment_Data; }

    public DbsField getCommon_Xml_Interestpayment_Close() { return common_Xml_Interestpayment_Close; }

    public DbsField getCommon_Xml_Netpaymentamount_Open() { return common_Xml_Netpaymentamount_Open; }

    public DbsField getCommon_Xml_Netpaymentamount_Data() { return common_Xml_Netpaymentamount_Data; }

    public DbsField getCommon_Xml_Netpaymentamount_Close() { return common_Xml_Netpaymentamount_Close; }

    public DbsField getCommon_Xml_Subpaymentdetail_Open() { return common_Xml_Subpaymentdetail_Open; }

    public DbsField getCommon_Xml_Subpaymentdetail_Close() { return common_Xml_Subpaymentdetail_Close; }

    public DbsField getCommon_Xml_Subdate_Open() { return common_Xml_Subdate_Open; }

    public DbsField getCommon_Xml_Subdate_Data() { return common_Xml_Subdate_Data; }

    public DbsField getCommon_Xml_Subdate_Close() { return common_Xml_Subdate_Close; }

    public DbsField getCommon_Xml_Subtiaacontract_Open() { return common_Xml_Subtiaacontract_Open; }

    public DbsField getCommon_Xml_Subtiaacontract_Data() { return common_Xml_Subtiaacontract_Data; }

    public DbsField getCommon_Xml_Subtiaacontract_Close() { return common_Xml_Subtiaacontract_Close; }

    public DbsField getCommon_Xml_Subcrefcertificate_Open() { return common_Xml_Subcrefcertificate_Open; }

    public DbsField getCommon_Xml_Subcrefcertificate_Data() { return common_Xml_Subcrefcertificate_Data; }

    public DbsField getCommon_Xml_Subcrefcertificate_Close() { return common_Xml_Subcrefcertificate_Close; }

    public DbsField getCommon_Xml_Subpaymentfrom_Open() { return common_Xml_Subpaymentfrom_Open; }

    public DbsField getCommon_Xml_Subpaymentfrom_Data() { return common_Xml_Subpaymentfrom_Data; }

    public DbsField getCommon_Xml_Subpaymentfrom_Close() { return common_Xml_Subpaymentfrom_Close; }

    public DbsField getCommon_Xml_Subpaymentfromfrequency_Open() { return common_Xml_Subpaymentfromfrequency_Open; }

    public DbsField getCommon_Xml_Subpaymentfromfrequency_Data() { return common_Xml_Subpaymentfromfrequency_Data; }

    public DbsField getCommon_Xml_Subpaymentfromfrequency_Close() { return common_Xml_Subpaymentfromfrequency_Close; }

    public DbsField getCommon_Xml_Subpaymentamount_Open() { return common_Xml_Subpaymentamount_Open; }

    public DbsField getCommon_Xml_Subpaymentamount_Data() { return common_Xml_Subpaymentamount_Data; }

    public DbsField getCommon_Xml_Subpaymentamount_Close() { return common_Xml_Subpaymentamount_Close; }

    public DbsField getCommon_Xml_Subpaymentamountfrequency_Open() { return common_Xml_Subpaymentamountfrequency_Open; }

    public DbsField getCommon_Xml_Subpaymentamountfrequency_Data() { return common_Xml_Subpaymentamountfrequency_Data; }

    public DbsField getCommon_Xml_Subpaymentamountfrequency_Close() { return common_Xml_Subpaymentamountfrequency_Close; }

    public DbsField getCommon_Xml_Subdeductionamount_Open() { return common_Xml_Subdeductionamount_Open; }

    public DbsField getCommon_Xml_Subdeductionamount_Data() { return common_Xml_Subdeductionamount_Data; }

    public DbsField getCommon_Xml_Subdeductionamount_Close() { return common_Xml_Subdeductionamount_Close; }

    public DbsField getCommon_Xml_Subdeductionamountfrequency_Open() { return common_Xml_Subdeductionamountfrequency_Open; }

    public DbsField getCommon_Xml_Subdeductionamountfrequency_Data() { return common_Xml_Subdeductionamountfrequency_Data; }

    public DbsField getCommon_Xml_Subdeductionamountfrequency_Clos() { return common_Xml_Subdeductionamountfrequency_Clos; }

    public DbsField getCommon_Xml_Totaldeductionamount_Open() { return common_Xml_Totaldeductionamount_Open; }

    public DbsField getCommon_Xml_Totaldeductionamount_Data() { return common_Xml_Totaldeductionamount_Data; }

    public DbsField getCommon_Xml_Totaldeductionamount_Close() { return common_Xml_Totaldeductionamount_Close; }

    public DbsField getCommon_Xml_Deductions_Open() { return common_Xml_Deductions_Open; }

    public DbsField getCommon_Xml_Deductions_Close() { return common_Xml_Deductions_Close; }

    public DbsField getCommon_Xml_Deductionsinfo_Open() { return common_Xml_Deductionsinfo_Open; }

    public DbsField getCommon_Xml_Deductionsinfo_Close() { return common_Xml_Deductionsinfo_Close; }

    public DbsField getCommon_Xml_Deductiondescription_Open() { return common_Xml_Deductiondescription_Open; }

    public DbsField getCommon_Xml_Deductiondescription_Data() { return common_Xml_Deductiondescription_Data; }

    public DbsField getCommon_Xml_Deductiondescription_Close() { return common_Xml_Deductiondescription_Close; }

    public DbsField getCommon_Xml_Deductionamount_Open() { return common_Xml_Deductionamount_Open; }

    public DbsField getCommon_Xml_Deductionamount_Data() { return common_Xml_Deductionamount_Data; }

    public DbsField getCommon_Xml_Deductionamount_Close() { return common_Xml_Deductionamount_Close; }

    public DbsField getCommon_Xml_Institutionname_Open() { return common_Xml_Institutionname_Open; }

    public DbsField getCommon_Xml_Institutionname_Data() { return common_Xml_Institutionname_Data; }

    public DbsField getCommon_Xml_Institutionname_Close() { return common_Xml_Institutionname_Close; }

    public DbsField getCommon_Xml_Institutionaddress_Open() { return common_Xml_Institutionaddress_Open; }

    public DbsField getCommon_Xml_Institutionaddress_Close() { return common_Xml_Institutionaddress_Close; }

    public DbsField getCommon_Xml_Institutionaddress_Lines_Open() { return common_Xml_Institutionaddress_Lines_Open; }

    public DbsField getCommon_Xml_Institutionaddress_Lines_Data() { return common_Xml_Institutionaddress_Lines_Data; }

    public DbsField getCommon_Xml_Institutionaddress_Lines_Close() { return common_Xml_Institutionaddress_Lines_Close; }

    public DbsField getCommon_Xml_Institutioninfo_Open() { return common_Xml_Institutioninfo_Open; }

    public DbsField getCommon_Xml_Institutioninfo_Close() { return common_Xml_Institutioninfo_Close; }

    public DbsField getCommon_Xml_Institutioninfoline_Open() { return common_Xml_Institutioninfoline_Open; }

    public DbsField getCommon_Xml_Institutioninfoline_Data() { return common_Xml_Institutioninfoline_Data; }

    public DbsField getCommon_Xml_Institutioninfoline_Close() { return common_Xml_Institutioninfoline_Close; }

    public DbsField getCommon_Xml_Participantinstitutionind_Open() { return common_Xml_Participantinstitutionind_Open; }

    public DbsField getCommon_Xml_Participantinstitutionind_Data() { return common_Xml_Participantinstitutionind_Data; }

    public DbsField getCommon_Xml_Participantinstitutionind_Close() { return common_Xml_Participantinstitutionind_Close; }

    public DbsField getCommon_Xml_Omnipay_Open() { return common_Xml_Omnipay_Open; }

    public DbsField getCommon_Xml_Omnipay_Close() { return common_Xml_Omnipay_Close; }

    public DbsField getCommon_Xml_Documenttype_Open() { return common_Xml_Documenttype_Open; }

    public DbsField getCommon_Xml_Documenttype_Data() { return common_Xml_Documenttype_Data; }

    public DbsField getCommon_Xml_Documenttype_Close() { return common_Xml_Documenttype_Close; }

    public DbsField getCommon_Xml_Participantinstitutionflag_Open() { return common_Xml_Participantinstitutionflag_Open; }

    public DbsField getCommon_Xml_Participantinstitutionflag_Data() { return common_Xml_Participantinstitutionflag_Data; }

    public DbsField getCommon_Xml_Participantinstitutionflag_Clos() { return common_Xml_Participantinstitutionflag_Clos; }

    public DbsField getCommon_Xml_Paymentfor_Open() { return common_Xml_Paymentfor_Open; }

    public DbsField getCommon_Xml_Paymentfor_Close() { return common_Xml_Paymentfor_Close; }

    public DbsField getCommon_Xml_Tiaanumber_Open() { return common_Xml_Tiaanumber_Open; }

    public DbsField getCommon_Xml_Tiaanumber_Data() { return common_Xml_Tiaanumber_Data; }

    public DbsField getCommon_Xml_Tiaanumber_Close() { return common_Xml_Tiaanumber_Close; }

    public DbsField getCommon_Xml_Crefnumber_Open() { return common_Xml_Crefnumber_Open; }

    public DbsField getCommon_Xml_Crefnumber_Data() { return common_Xml_Crefnumber_Data; }

    public DbsField getCommon_Xml_Crefnumber_Close() { return common_Xml_Crefnumber_Close; }

    public DbsField getCommon_Xml_Effectivedate_Open() { return common_Xml_Effectivedate_Open; }

    public DbsField getCommon_Xml_Effectivedate_Data() { return common_Xml_Effectivedate_Data; }

    public DbsField getCommon_Xml_Effectivedate_Close() { return common_Xml_Effectivedate_Close; }

    public DbsField getCommon_Xml_Forparticipant_Open() { return common_Xml_Forparticipant_Open; }

    public DbsField getCommon_Xml_Forparticipant_Close() { return common_Xml_Forparticipant_Close; }

    public DbsField getCommon_Xml_Foralternatecarrier_Open() { return common_Xml_Foralternatecarrier_Open; }

    public DbsField getCommon_Xml_Foralternatecarrier_Close() { return common_Xml_Foralternatecarrier_Close; }

    public DbsField getCommon_Xml_Paymentinformationsummary_Open() { return common_Xml_Paymentinformationsummary_Open; }

    public DbsField getCommon_Xml_Paymentinformationsummary_Close() { return common_Xml_Paymentinformationsummary_Close; }

    public DbsField getCommon_Xml_Paymentdetailinformation_Open() { return common_Xml_Paymentdetailinformation_Open; }

    public DbsField getCommon_Xml_Paymentdetailinformation_Close() { return common_Xml_Paymentdetailinformation_Close; }

    public DbsField getCommon_Xml_Accountnumber_Open() { return common_Xml_Accountnumber_Open; }

    public DbsField getCommon_Xml_Accountnumber_Data() { return common_Xml_Accountnumber_Data; }

    public DbsField getCommon_Xml_Accountnumber_Close() { return common_Xml_Accountnumber_Close; }

    public DbsField getCommon_Xml_Transferaccountslist_Open() { return common_Xml_Transferaccountslist_Open; }

    public DbsField getCommon_Xml_Transferaccountslist_Close() { return common_Xml_Transferaccountslist_Close; }

    public DbsField getCommon_Xml_Transferaccounts_Open() { return common_Xml_Transferaccounts_Open; }

    public DbsField getCommon_Xml_Transferaccounts_Data() { return common_Xml_Transferaccounts_Data; }

    public DbsField getCommon_Xml_Transferaccounts_Close() { return common_Xml_Transferaccounts_Close; }

    public DbsField getCommon_Xml_Employer_Open() { return common_Xml_Employer_Open; }

    public DbsField getCommon_Xml_Employer_Data() { return common_Xml_Employer_Data; }

    public DbsField getCommon_Xml_Employer_Close() { return common_Xml_Employer_Close; }

    public DbsField getCommon_Xml_Typeofplan_Open() { return common_Xml_Typeofplan_Open; }

    public DbsField getCommon_Xml_Typeofplan_Data() { return common_Xml_Typeofplan_Data; }

    public DbsField getCommon_Xml_Typeofplan_Close() { return common_Xml_Typeofplan_Close; }

    public DbsField getCommon_Xml_Cashavailable_Open() { return common_Xml_Cashavailable_Open; }

    public DbsField getCommon_Xml_Cashavailable_Data() { return common_Xml_Cashavailable_Data; }

    public DbsField getCommon_Xml_Cashavailable_Close() { return common_Xml_Cashavailable_Close; }

    public DbsField getCommon_Xml_Distributionrequirements_Open() { return common_Xml_Distributionrequirements_Open; }

    public DbsField getCommon_Xml_Distributionrequirements_Close() { return common_Xml_Distributionrequirements_Close; }

    public DbsField getCommon_Xml_Paymentinformation_Open() { return common_Xml_Paymentinformation_Open; }

    public DbsField getCommon_Xml_Paymentinformation_Close() { return common_Xml_Paymentinformation_Close; }

    public DbsField getCommon_Xml_Deductionstype_Open() { return common_Xml_Deductionstype_Open; }

    public DbsField getCommon_Xml_Deductionstype_Close() { return common_Xml_Deductionstype_Close; }

    public DbsField getCommon_Xml_Grosstotaldeductions_Open() { return common_Xml_Grosstotaldeductions_Open; }

    public DbsField getCommon_Xml_Grosstotaldeductions_Data() { return common_Xml_Grosstotaldeductions_Data; }

    public DbsField getCommon_Xml_Grosstotaldeductions_Close() { return common_Xml_Grosstotaldeductions_Close; }

    public DbsField getCommon_Xml_Grosstotalpayments_Open() { return common_Xml_Grosstotalpayments_Open; }

    public DbsField getCommon_Xml_Grosstotalpayments_Data() { return common_Xml_Grosstotalpayments_Data; }

    public DbsField getCommon_Xml_Grosstotalpayments_Close() { return common_Xml_Grosstotalpayments_Close; }

    public DbsField getCommon_Xml_Nettotalpayments_Open() { return common_Xml_Nettotalpayments_Open; }

    public DbsField getCommon_Xml_Nettotalpayments_Data() { return common_Xml_Nettotalpayments_Data; }

    public DbsField getCommon_Xml_Nettotalpayments_Close() { return common_Xml_Nettotalpayments_Close; }

    public DbsField getCommon_Xml_Contractsorcertificates_Open() { return common_Xml_Contractsorcertificates_Open; }

    public DbsField getCommon_Xml_Contractsorcertificates_Data() { return common_Xml_Contractsorcertificates_Data; }

    public DbsField getCommon_Xml_Contractsorcertificates_Close() { return common_Xml_Contractsorcertificates_Close; }

    public DbsField getCommon_Xml_Planname_Open() { return common_Xml_Planname_Open; }

    public DbsField getCommon_Xml_Planname_Data() { return common_Xml_Planname_Data; }

    public DbsField getCommon_Xml_Planname_Close() { return common_Xml_Planname_Close; }

    public DbsField getCommon_Xml_Processingdate_Open() { return common_Xml_Processingdate_Open; }

    public DbsField getCommon_Xml_Processingdate_Data() { return common_Xml_Processingdate_Data; }

    public DbsField getCommon_Xml_Processingdate_Close() { return common_Xml_Processingdate_Close; }

    public DbsField getCommon_Xml_Accountsinformation_Open() { return common_Xml_Accountsinformation_Open; }

    public DbsField getCommon_Xml_Accountsinformation_Close() { return common_Xml_Accountsinformation_Close; }

    public DbsField getCommon_Xml_Disclaimercodes_Open() { return common_Xml_Disclaimercodes_Open; }

    public DbsField getCommon_Xml_Disclaimercodes_Close() { return common_Xml_Disclaimercodes_Close; }

    public DbsField getCommon_Xml_Disclaimercodestype_Open() { return common_Xml_Disclaimercodestype_Open; }

    public DbsField getCommon_Xml_Disclaimercodestype_Data() { return common_Xml_Disclaimercodestype_Data; }

    public DbsField getCommon_Xml_Disclaimercodestype_Close() { return common_Xml_Disclaimercodestype_Close; }

    public DbsField getCommon_Xml_Decedentname_Open() { return common_Xml_Decedentname_Open; }

    public DbsField getCommon_Xml_Decedentname_Data() { return common_Xml_Decedentname_Data; }

    public DbsField getCommon_Xml_Decedentname_Close() { return common_Xml_Decedentname_Close; }

    public DbsField getCommon_Xml_Paymentlabel_Open() { return common_Xml_Paymentlabel_Open; }

    public DbsField getCommon_Xml_Paymentlabel_Data() { return common_Xml_Paymentlabel_Data; }

    public DbsField getCommon_Xml_Paymentlabel_Close() { return common_Xml_Paymentlabel_Close; }

    public DbsField getCommon_Xml_Contractlabel_Open() { return common_Xml_Contractlabel_Open; }

    public DbsField getCommon_Xml_Contractlabel_Data() { return common_Xml_Contractlabel_Data; }

    public DbsField getCommon_Xml_Contractlabel_Close() { return common_Xml_Contractlabel_Close; }

    public DbsField getCommon_Xml_Paymentdate_Open() { return common_Xml_Paymentdate_Open; }

    public DbsField getCommon_Xml_Paymentdate_Data() { return common_Xml_Paymentdate_Data; }

    public DbsField getCommon_Xml_Paymentdate_Close() { return common_Xml_Paymentdate_Close; }

    public DbsField getCommon_Xml_Totalinterest_Open() { return common_Xml_Totalinterest_Open; }

    public DbsField getCommon_Xml_Totalinterest_Data() { return common_Xml_Totalinterest_Data; }

    public DbsField getCommon_Xml_Totalinterest_Close() { return common_Xml_Totalinterest_Close; }

    public DbsField getCommon_Xml_Totalnetpaymentamount_Open() { return common_Xml_Totalnetpaymentamount_Open; }

    public DbsField getCommon_Xml_Totalnetpaymentamount_Data() { return common_Xml_Totalnetpaymentamount_Data; }

    public DbsField getCommon_Xml_Totalnetpaymentamount_Close() { return common_Xml_Totalnetpaymentamount_Close; }

    public DbsField getCommon_Xml_Totalpaymentamount_Open() { return common_Xml_Totalpaymentamount_Open; }

    public DbsField getCommon_Xml_Totalpaymentamount_Data() { return common_Xml_Totalpaymentamount_Data; }

    public DbsField getCommon_Xml_Totalpaymentamount_Close() { return common_Xml_Totalpaymentamount_Close; }

    public DbsField getCommon_Xml_Interestind_Open() { return common_Xml_Interestind_Open; }

    public DbsField getCommon_Xml_Interestind_Data() { return common_Xml_Interestind_Data; }

    public DbsField getCommon_Xml_Interestind_Close() { return common_Xml_Interestind_Close; }

    public DbsField getCommon_Xml_Deductionamountind_Open() { return common_Xml_Deductionamountind_Open; }

    public DbsField getCommon_Xml_Deductionamountind_Data() { return common_Xml_Deductionamountind_Data; }

    public DbsField getCommon_Xml_Deductionamountind_Close() { return common_Xml_Deductionamountind_Close; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        common_Xml = newGroupInRecord("common_Xml", "COMMON-XML");
        common_Xml_Xml_Header = common_Xml.newFieldInGroup("common_Xml_Xml_Header", "XML-HEADER", FieldType.STRING, 60);
        common_Xml_Documentrequests_Open = common_Xml.newFieldInGroup("common_Xml_Documentrequests_Open", "DOCUMENTREQUESTS-OPEN", FieldType.STRING, 90);
        common_Xml_Documentrequests_Close = common_Xml.newFieldInGroup("common_Xml_Documentrequests_Close", "DOCUMENTREQUESTS-CLOSE", FieldType.STRING, 
            30);
        common_Xml_Documentrequest_Open = common_Xml.newFieldInGroup("common_Xml_Documentrequest_Open", "DOCUMENTREQUEST-OPEN", FieldType.STRING, 30);
        common_Xml_Documentrequest_Close = common_Xml.newFieldInGroup("common_Xml_Documentrequest_Close", "DOCUMENTREQUEST-CLOSE", FieldType.STRING, 30);
        common_Xml_Requestdatetime_Open = common_Xml.newFieldInGroup("common_Xml_Requestdatetime_Open", "REQUESTDATETIME-OPEN", FieldType.STRING, 30);
        common_Xml_Requestdatetime_Data = common_Xml.newFieldInGroup("common_Xml_Requestdatetime_Data", "REQUESTDATETIME-DATA", FieldType.STRING, 30);
        common_Xml_Requestdatetime_Close = common_Xml.newFieldInGroup("common_Xml_Requestdatetime_Close", "REQUESTDATETIME-CLOSE", FieldType.STRING, 30);
        common_Xml_Batchind_Open = common_Xml.newFieldInGroup("common_Xml_Batchind_Open", "BATCHIND-OPEN", FieldType.STRING, 15);
        common_Xml_Batchind_Close = common_Xml.newFieldInGroup("common_Xml_Batchind_Close", "BATCHIND-CLOSE", FieldType.STRING, 15);
        common_Xml_Batchcounter_Open = common_Xml.newFieldInGroup("common_Xml_Batchcounter_Open", "BATCHCOUNTER-OPEN", FieldType.STRING, 15);
        common_Xml_Batchcounter_Data = common_Xml.newFieldInGroup("common_Xml_Batchcounter_Data", "BATCHCOUNTER-DATA", FieldType.NUMERIC, 10);
        common_Xml_Batchcounter_Close = common_Xml.newFieldInGroup("common_Xml_Batchcounter_Close", "BATCHCOUNTER-CLOSE", FieldType.STRING, 15);
        common_Xml_Batchtotal_Open = common_Xml.newFieldInGroup("common_Xml_Batchtotal_Open", "BATCHTOTAL-OPEN", FieldType.STRING, 15);
        common_Xml_Batchtotal_Data = common_Xml.newFieldInGroup("common_Xml_Batchtotal_Data", "BATCHTOTAL-DATA", FieldType.NUMERIC, 10);
        common_Xml_Batchtotal_Close = common_Xml.newFieldInGroup("common_Xml_Batchtotal_Close", "BATCHTOTAL-CLOSE", FieldType.STRING, 15);
        common_Xml_Mailiteminfo_Open = common_Xml.newFieldInGroup("common_Xml_Mailiteminfo_Open", "MAILITEMINFO-OPEN", FieldType.STRING, 15);
        common_Xml_Mailiteminfo_Close = common_Xml.newFieldInGroup("common_Xml_Mailiteminfo_Close", "MAILITEMINFO-CLOSE", FieldType.STRING, 15);
        common_Xml_Applicationid_Open = common_Xml.newFieldInGroup("common_Xml_Applicationid_Open", "APPLICATIONID-OPEN", FieldType.STRING, 30);
        common_Xml_Applicationid_Close = common_Xml.newFieldInGroup("common_Xml_Applicationid_Close", "APPLICATIONID-CLOSE", FieldType.STRING, 30);
        common_Xml_Universalid_Open = common_Xml.newFieldInGroup("common_Xml_Universalid_Open", "UNIVERSALID-OPEN", FieldType.STRING, 15);
        common_Xml_Universalid_Data = common_Xml.newFieldInGroup("common_Xml_Universalid_Data", "UNIVERSALID-DATA", FieldType.STRING, 12);
        common_Xml_Universalid_Close = common_Xml.newFieldInGroup("common_Xml_Universalid_Close", "UNIVERSALID-CLOSE", FieldType.STRING, 15);
        common_Xml_Universaltype_Open = common_Xml.newFieldInGroup("common_Xml_Universaltype_Open", "UNIVERSALTYPE-OPEN", FieldType.STRING, 20);
        common_Xml_Universaltype_Data = common_Xml.newFieldInGroup("common_Xml_Universaltype_Data", "UNIVERSALTYPE-DATA", FieldType.STRING, 1);
        common_Xml_Universaltype_Close = common_Xml.newFieldInGroup("common_Xml_Universaltype_Close", "UNIVERSALTYPE-CLOSE", FieldType.STRING, 20);
        common_Xml_Documentrequestid_Open = common_Xml.newFieldInGroup("common_Xml_Documentrequestid_Open", "DOCUMENTREQUESTID-OPEN", FieldType.STRING, 
            20);
        common_Xml_Documentrequestid_Data = common_Xml.newFieldInGroup("common_Xml_Documentrequestid_Data", "DOCUMENTREQUESTID-DATA", FieldType.STRING, 
            40);
        common_Xml_Documentrequestid_DataRedef1 = common_Xml.newGroupInGroup("common_Xml_Documentrequestid_DataRedef1", "Redefines", common_Xml_Documentrequestid_Data);
        common_Xml_Pnd_First_Twenty = common_Xml_Documentrequestid_DataRedef1.newFieldInGroup("common_Xml_Pnd_First_Twenty", "#FIRST-TWENTY", FieldType.STRING, 
            20);
        common_Xml_Pnd_Filler = common_Xml_Documentrequestid_DataRedef1.newFieldInGroup("common_Xml_Pnd_Filler", "#FILLER", FieldType.STRING, 20);
        common_Xml_Documentrequestid_DataRedef2 = common_Xml.newGroupInGroup("common_Xml_Documentrequestid_DataRedef2", "Redefines", common_Xml_Documentrequestid_Data);
        common_Xml_Documentrequestid_Sys_Id = common_Xml_Documentrequestid_DataRedef2.newFieldInGroup("common_Xml_Documentrequestid_Sys_Id", "DOCUMENTREQUESTID-SYS-ID", 
            FieldType.STRING, 3);
        common_Xml_Documentrequestid_Sub_Sys_Id = common_Xml_Documentrequestid_DataRedef2.newFieldInGroup("common_Xml_Documentrequestid_Sub_Sys_Id", "DOCUMENTREQUESTID-SUB-SYS-ID", 
            FieldType.STRING, 3);
        common_Xml_Documentrequestid_Yyyy = common_Xml_Documentrequestid_DataRedef2.newFieldInGroup("common_Xml_Documentrequestid_Yyyy", "DOCUMENTREQUESTID-YYYY", 
            FieldType.STRING, 4);
        common_Xml_Documentrequestid_Mm = common_Xml_Documentrequestid_DataRedef2.newFieldInGroup("common_Xml_Documentrequestid_Mm", "DOCUMENTREQUESTID-MM", 
            FieldType.STRING, 2);
        common_Xml_Documentrequestid_Dd = common_Xml_Documentrequestid_DataRedef2.newFieldInGroup("common_Xml_Documentrequestid_Dd", "DOCUMENTREQUESTID-DD", 
            FieldType.STRING, 2);
        common_Xml_Documentrequestid_Hh = common_Xml_Documentrequestid_DataRedef2.newFieldInGroup("common_Xml_Documentrequestid_Hh", "DOCUMENTREQUESTID-HH", 
            FieldType.STRING, 2);
        common_Xml_Documentrequestid_Ii = common_Xml_Documentrequestid_DataRedef2.newFieldInGroup("common_Xml_Documentrequestid_Ii", "DOCUMENTREQUESTID-II", 
            FieldType.STRING, 2);
        common_Xml_Documentrequestid_Ss = common_Xml_Documentrequestid_DataRedef2.newFieldInGroup("common_Xml_Documentrequestid_Ss", "DOCUMENTREQUESTID-SS", 
            FieldType.STRING, 2);
        common_Xml_Documentrequestid_Uid = common_Xml_Documentrequestid_DataRedef2.newFieldInGroup("common_Xml_Documentrequestid_Uid", "DOCUMENTREQUESTID-UID", 
            FieldType.STRING, 13);
        common_Xml_Documentrequestid_Cnt = common_Xml_Documentrequestid_DataRedef2.newFieldInGroup("common_Xml_Documentrequestid_Cnt", "DOCUMENTREQUESTID-CNT", 
            FieldType.STRING, 7);
        common_Xml_Documentrequestid_Close = common_Xml.newFieldInGroup("common_Xml_Documentrequestid_Close", "DOCUMENTREQUESTID-CLOSE", FieldType.STRING, 
            20);
        common_Xml_Printerid_Open = common_Xml.newFieldInGroup("common_Xml_Printerid_Open", "PRINTERID-OPEN", FieldType.STRING, 15);
        common_Xml_Printerid_Data = common_Xml.newFieldInGroup("common_Xml_Printerid_Data", "PRINTERID-DATA", FieldType.STRING, 8);
        common_Xml_Printerid_Close = common_Xml.newFieldInGroup("common_Xml_Printerid_Close", "PRINTERID-CLOSE", FieldType.STRING, 15);
        common_Xml_Fullname_Open = common_Xml.newFieldInGroup("common_Xml_Fullname_Open", "FULLNAME-OPEN", FieldType.STRING, 10);
        common_Xml_Fullname_Data = common_Xml.newFieldInGroup("common_Xml_Fullname_Data", "FULLNAME-DATA", FieldType.STRING, 60);
        common_Xml_Fullname_Close = common_Xml.newFieldInGroup("common_Xml_Fullname_Close", "FULLNAME-CLOSE", FieldType.STRING, 15);
        common_Xml_Addresstypecode_Open = common_Xml.newFieldInGroup("common_Xml_Addresstypecode_Open", "ADDRESSTYPECODE-OPEN", FieldType.STRING, 20);
        common_Xml_Addresstypecode_Data = common_Xml.newFieldInGroup("common_Xml_Addresstypecode_Data", "ADDRESSTYPECODE-DATA", FieldType.STRING, 1);
        common_Xml_Addresstypecode_Close = common_Xml.newFieldInGroup("common_Xml_Addresstypecode_Close", "ADDRESSTYPECODE-CLOSE", FieldType.STRING, 20);
        common_Xml_Letterdate_Open = common_Xml.newFieldInGroup("common_Xml_Letterdate_Open", "LETTERDATE-OPEN", FieldType.STRING, 20);
        common_Xml_Letterdate_Data = common_Xml.newFieldInGroup("common_Xml_Letterdate_Data", "LETTERDATE-DATA", FieldType.STRING, 10);
        common_Xml_Letterdate_Close = common_Xml.newFieldInGroup("common_Xml_Letterdate_Close", "LETTERDATE-CLOSE", FieldType.STRING, 20);
        common_Xml_Deliverytype_Open = common_Xml.newFieldInGroup("common_Xml_Deliverytype_Open", "DELIVERYTYPE-OPEN", FieldType.STRING, 20);
        common_Xml_Deliverytype_Data = common_Xml.newFieldInGroup("common_Xml_Deliverytype_Data", "DELIVERYTYPE-DATA", FieldType.STRING, 1);
        common_Xml_Deliverytype_Close = common_Xml.newFieldInGroup("common_Xml_Deliverytype_Close", "DELIVERYTYPE-CLOSE", FieldType.STRING, 20);
        common_Xml_Expagind_Open = common_Xml.newFieldInGroup("common_Xml_Expagind_Open", "EXPAGIND-OPEN", FieldType.STRING, 20);
        common_Xml_Expagind_Data = common_Xml.newFieldInGroup("common_Xml_Expagind_Data", "EXPAGIND-DATA", FieldType.STRING, 1);
        common_Xml_Expagind_Close = common_Xml.newFieldInGroup("common_Xml_Expagind_Close", "EXPAGIND-CLOSE", FieldType.STRING, 20);
        common_Xml_Archivalind_Open = common_Xml.newFieldInGroup("common_Xml_Archivalind_Open", "ARCHIVALIND-OPEN", FieldType.STRING, 20);
        common_Xml_Archivalind_Data = common_Xml.newFieldInGroup("common_Xml_Archivalind_Data", "ARCHIVALIND-DATA", FieldType.STRING, 1);
        common_Xml_Archivalind_Close = common_Xml.newFieldInGroup("common_Xml_Archivalind_Close", "ARCHIVALIND-CLOSE", FieldType.STRING, 20);
        common_Xml_Planid_Open = common_Xml.newFieldInGroup("common_Xml_Planid_Open", "PLANID-OPEN", FieldType.STRING, 20);
        common_Xml_Planid_Data = common_Xml.newFieldInGroup("common_Xml_Planid_Data", "PLANID-DATA", FieldType.STRING, 6);
        common_Xml_Planid_Close = common_Xml.newFieldInGroup("common_Xml_Planid_Close", "PLANID-CLOSE", FieldType.STRING, 20);
        common_Xml_Businessdate_Open = common_Xml.newFieldInGroup("common_Xml_Businessdate_Open", "BUSINESSDATE-OPEN", FieldType.STRING, 20);
        common_Xml_Businessdate_Data = common_Xml.newFieldInGroup("common_Xml_Businessdate_Data", "BUSINESSDATE-DATA", FieldType.STRING, 10);
        common_Xml_Businessdate_Close = common_Xml.newFieldInGroup("common_Xml_Businessdate_Close", "BUSINESSDATE-CLOSE", FieldType.STRING, 20);
        common_Xml_Addresslines_Open = common_Xml.newFieldInGroup("common_Xml_Addresslines_Open", "ADDRESSLINES-OPEN", FieldType.STRING, 20);
        common_Xml_Addresslines_Close = common_Xml.newFieldInGroup("common_Xml_Addresslines_Close", "ADDRESSLINES-CLOSE", FieldType.STRING, 20);
        common_Xml_Addressline_Open = common_Xml.newFieldInGroup("common_Xml_Addressline_Open", "ADDRESSLINE-OPEN", FieldType.STRING, 20);
        common_Xml_Addressline_Data = common_Xml.newFieldArrayInGroup("common_Xml_Addressline_Data", "ADDRESSLINE-DATA", FieldType.STRING, 35, new DbsArrayController(1,
            6));
        common_Xml_Addressline_Close = common_Xml.newFieldInGroup("common_Xml_Addressline_Close", "ADDRESSLINE-CLOSE", FieldType.STRING, 20);
        common_Xml_Documentinfo_Open = common_Xml.newFieldInGroup("common_Xml_Documentinfo_Open", "DOCUMENTINFO-OPEN", FieldType.STRING, 20);
        common_Xml_Documentinfo_Close = common_Xml.newFieldInGroup("common_Xml_Documentinfo_Close", "DOCUMENTINFO-CLOSE", FieldType.STRING, 20);
        common_Xml_Prolinechecks_Open = common_Xml.newFieldInGroup("common_Xml_Prolinechecks_Open", "PROLINECHECKS-OPEN", FieldType.STRING, 25);
        common_Xml_Prolinechecks_Close = common_Xml.newFieldInGroup("common_Xml_Prolinechecks_Close", "PROLINECHECKS-CLOSE", FieldType.STRING, 25);
        common_Xml_Headerinfo_Open = common_Xml.newFieldInGroup("common_Xml_Headerinfo_Open", "HEADERINFO-OPEN", FieldType.STRING, 25);
        common_Xml_Headerinfo_Close = common_Xml.newFieldInGroup("common_Xml_Headerinfo_Close", "HEADERINFO-CLOSE", FieldType.STRING, 25);
        common_Xml_Settlementsystem_Open = common_Xml.newFieldInGroup("common_Xml_Settlementsystem_Open", "SETTLEMENTSYSTEM-OPEN", FieldType.STRING, 25);
        common_Xml_Settlementsystem_Data = common_Xml.newFieldInGroup("common_Xml_Settlementsystem_Data", "SETTLEMENTSYSTEM-DATA", FieldType.STRING, 6);
        common_Xml_Settlementsystem_Close = common_Xml.newFieldInGroup("common_Xml_Settlementsystem_Close", "SETTLEMENTSYSTEM-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Checkeftind_Open = common_Xml.newFieldInGroup("common_Xml_Checkeftind_Open", "CHECKEFTIND-OPEN", FieldType.STRING, 20);
        common_Xml_Checkeftind_Data = common_Xml.newFieldInGroup("common_Xml_Checkeftind_Data", "CHECKEFTIND-DATA", FieldType.STRING, 10);
        common_Xml_Checkeftind_Close = common_Xml.newFieldInGroup("common_Xml_Checkeftind_Close", "CHECKEFTIND-CLOSE", FieldType.STRING, 20);
        common_Xml_Holdcode_Open = common_Xml.newFieldInGroup("common_Xml_Holdcode_Open", "HOLDCODE-OPEN", FieldType.STRING, 20);
        common_Xml_Holdcode_Data = common_Xml.newFieldInGroup("common_Xml_Holdcode_Data", "HOLDCODE-DATA", FieldType.STRING, 4);
        common_Xml_Holdcode_Close = common_Xml.newFieldInGroup("common_Xml_Holdcode_Close", "HOLDCODE-CLOSE", FieldType.STRING, 20);
        common_Xml_Tiaaaddress_Open = common_Xml.newFieldInGroup("common_Xml_Tiaaaddress_Open", "TIAAADDRESS-OPEN", FieldType.STRING, 20);
        common_Xml_Tiaaaddress_Close = common_Xml.newFieldInGroup("common_Xml_Tiaaaddress_Close", "TIAAADDRESS-CLOSE", FieldType.STRING, 20);
        common_Xml_Tiaaaddressline_Open = common_Xml.newFieldInGroup("common_Xml_Tiaaaddressline_Open", "TIAAADDRESSLINE-OPEN", FieldType.STRING, 25);
        common_Xml_Tiaaaddressline_Data = common_Xml.newFieldArrayInGroup("common_Xml_Tiaaaddressline_Data", "TIAAADDRESSLINE-DATA", FieldType.STRING, 
            35, new DbsArrayController(1,2,1,2));
        common_Xml_Tiaaaddressline_Close = common_Xml.newFieldInGroup("common_Xml_Tiaaaddressline_Close", "TIAAADDRESSLINE-CLOSE", FieldType.STRING, 25);
        common_Xml_Nonids_Index = common_Xml.newFieldInGroup("common_Xml_Nonids_Index", "NONIDS-INDEX", FieldType.NUMERIC, 1);
        common_Xml_Ids_Index = common_Xml.newFieldInGroup("common_Xml_Ids_Index", "IDS-INDEX", FieldType.NUMERIC, 1);
        common_Xml_Tiaaphonenumber_Open = common_Xml.newFieldInGroup("common_Xml_Tiaaphonenumber_Open", "TIAAPHONENUMBER-OPEN", FieldType.STRING, 25);
        common_Xml_Tiaaphonenumber_Data = common_Xml.newFieldArrayInGroup("common_Xml_Tiaaphonenumber_Data", "TIAAPHONENUMBER-DATA", FieldType.STRING, 
            15, new DbsArrayController(1,2));
        common_Xml_Tiaaphonenumber_Close = common_Xml.newFieldInGroup("common_Xml_Tiaaphonenumber_Close", "TIAAPHONENUMBER-CLOSE", FieldType.STRING, 25);
        common_Xml_Bankname_Open = common_Xml.newFieldInGroup("common_Xml_Bankname_Open", "BANKNAME-OPEN", FieldType.STRING, 20);
        common_Xml_Bankname_Data = common_Xml.newFieldInGroup("common_Xml_Bankname_Data", "BANKNAME-DATA", FieldType.STRING, 25);
        common_Xml_Bankname_Close = common_Xml.newFieldInGroup("common_Xml_Bankname_Close", "BANKNAME-CLOSE", FieldType.STRING, 20);
        common_Xml_Bankaddress_Open = common_Xml.newFieldInGroup("common_Xml_Bankaddress_Open", "BANKADDRESS-OPEN", FieldType.STRING, 20);
        common_Xml_Bankaddress_Close = common_Xml.newFieldInGroup("common_Xml_Bankaddress_Close", "BANKADDRESS-CLOSE", FieldType.STRING, 20);
        common_Xml_Bankaddressline_Open = common_Xml.newFieldInGroup("common_Xml_Bankaddressline_Open", "BANKADDRESSLINE-OPEN", FieldType.STRING, 25);
        common_Xml_Bankaddressline_Data = common_Xml.newFieldInGroup("common_Xml_Bankaddressline_Data", "BANKADDRESSLINE-DATA", FieldType.STRING, 35);
        common_Xml_Bankaddressline_Close = common_Xml.newFieldInGroup("common_Xml_Bankaddressline_Close", "BANKADDRESSLINE-CLOSE", FieldType.STRING, 25);
        common_Xml_Bankfractional1_Open = common_Xml.newFieldInGroup("common_Xml_Bankfractional1_Open", "BANKFRACTIONAL1-OPEN", FieldType.STRING, 25);
        common_Xml_Bankfractional1_Data = common_Xml.newFieldInGroup("common_Xml_Bankfractional1_Data", "BANKFRACTIONAL1-DATA", FieldType.STRING, 6);
        common_Xml_Bankfractional1_Close = common_Xml.newFieldInGroup("common_Xml_Bankfractional1_Close", "BANKFRACTIONAL1-CLOSE", FieldType.STRING, 25);
        common_Xml_Bankfractional2_Open = common_Xml.newFieldInGroup("common_Xml_Bankfractional2_Open", "BANKFRACTIONAL2-OPEN", FieldType.STRING, 25);
        common_Xml_Bankfractional2_Data = common_Xml.newFieldInGroup("common_Xml_Bankfractional2_Data", "BANKFRACTIONAL2-DATA", FieldType.STRING, 6);
        common_Xml_Bankfractional2_Close = common_Xml.newFieldInGroup("common_Xml_Bankfractional2_Close", "BANKFRACTIONAL2-CLOSE", FieldType.STRING, 25);
        common_Xml_Bankroutingnumber_Open = common_Xml.newFieldInGroup("common_Xml_Bankroutingnumber_Open", "BANKROUTINGNUMBER-OPEN", FieldType.STRING, 
            30);
        common_Xml_Bankroutingnumber_Data = common_Xml.newFieldInGroup("common_Xml_Bankroutingnumber_Data", "BANKROUTINGNUMBER-DATA", FieldType.STRING, 
            25);
        common_Xml_Bankroutingnumber_Close = common_Xml.newFieldInGroup("common_Xml_Bankroutingnumber_Close", "BANKROUTINGNUMBER-CLOSE", FieldType.STRING, 
            30);
        common_Xml_Bankaccountnumber_Open = common_Xml.newFieldInGroup("common_Xml_Bankaccountnumber_Open", "BANKACCOUNTNUMBER-OPEN", FieldType.STRING, 
            30);
        common_Xml_Bankaccountnumber_Data = common_Xml.newFieldInGroup("common_Xml_Bankaccountnumber_Data", "BANKACCOUNTNUMBER-DATA", FieldType.STRING, 
            25);
        common_Xml_Bankaccountnumber_Close = common_Xml.newFieldInGroup("common_Xml_Bankaccountnumber_Close", "BANKACCOUNTNUMBER-CLOSE", FieldType.STRING, 
            30);
        common_Xml_Checknumber_Open = common_Xml.newFieldInGroup("common_Xml_Checknumber_Open", "CHECKNUMBER-OPEN", FieldType.STRING, 20);
        common_Xml_Checknumber_Data = common_Xml.newFieldInGroup("common_Xml_Checknumber_Data", "CHECKNUMBER-DATA", FieldType.STRING, 10);
        common_Xml_Checknumber_Close = common_Xml.newFieldInGroup("common_Xml_Checknumber_Close", "CHECKNUMBER-CLOSE", FieldType.STRING, 20);
        common_Xml_Checkdate_Open = common_Xml.newFieldInGroup("common_Xml_Checkdate_Open", "CHECKDATE-OPEN", FieldType.STRING, 20);
        common_Xml_Checkdate_Data = common_Xml.newFieldInGroup("common_Xml_Checkdate_Data", "CHECKDATE-DATA", FieldType.STRING, 10);
        common_Xml_Checkdate_Close = common_Xml.newFieldInGroup("common_Xml_Checkdate_Close", "CHECKDATE-CLOSE", FieldType.STRING, 20);
        common_Xml_Checkamount_Open = common_Xml.newFieldInGroup("common_Xml_Checkamount_Open", "CHECKAMOUNT-OPEN", FieldType.STRING, 20);
        common_Xml_Checkamount_Data = common_Xml.newFieldInGroup("common_Xml_Checkamount_Data", "CHECKAMOUNT-DATA", FieldType.STRING, 15);
        common_Xml_Checkamount_Close = common_Xml.newFieldInGroup("common_Xml_Checkamount_Close", "CHECKAMOUNT-CLOSE", FieldType.STRING, 20);
        common_Xml_Accountname_Open = common_Xml.newFieldInGroup("common_Xml_Accountname_Open", "ACCOUNTNAME-OPEN", FieldType.STRING, 20);
        common_Xml_Accountname_Data = common_Xml.newFieldInGroup("common_Xml_Accountname_Data", "ACCOUNTNAME-DATA", FieldType.STRING, 10);
        common_Xml_Accountname_Close = common_Xml.newFieldInGroup("common_Xml_Accountname_Close", "ACCOUNTNAME-CLOSE", FieldType.STRING, 20);
        common_Xml_Signatorytitle_Open = common_Xml.newFieldInGroup("common_Xml_Signatorytitle_Open", "SIGNATORYTITLE-OPEN", FieldType.STRING, 30);
        common_Xml_Signatorytitle_Data = common_Xml.newFieldInGroup("common_Xml_Signatorytitle_Data", "SIGNATORYTITLE-DATA", FieldType.STRING, 30);
        common_Xml_Signatorytitle_Close = common_Xml.newFieldInGroup("common_Xml_Signatorytitle_Close", "SIGNATORYTITLE-CLOSE", FieldType.STRING, 30);
        common_Xml_Settlementsystemcodes_Open = common_Xml.newFieldInGroup("common_Xml_Settlementsystemcodes_Open", "SETTLEMENTSYSTEMCODES-OPEN", FieldType.STRING, 
            30);
        common_Xml_Settlementsystemcodes_Data = common_Xml.newFieldInGroup("common_Xml_Settlementsystemcodes_Data", "SETTLEMENTSYSTEMCODES-DATA", FieldType.STRING, 
            2);
        common_Xml_Settlementsystemcodes_Close = common_Xml.newFieldInGroup("common_Xml_Settlementsystemcodes_Close", "SETTLEMENTSYSTEMCODES-CLOSE", FieldType.STRING, 
            30);
        common_Xml_Nonidsinfo_Open = common_Xml.newFieldInGroup("common_Xml_Nonidsinfo_Open", "NONIDSINFO-OPEN", FieldType.STRING, 25);
        common_Xml_Nonidsinfo_Close = common_Xml.newFieldInGroup("common_Xml_Nonidsinfo_Close", "NONIDSINFO-CLOSE", FieldType.STRING, 25);
        common_Xml_Participantname_Open = common_Xml.newFieldInGroup("common_Xml_Participantname_Open", "PARTICIPANTNAME-OPEN", FieldType.STRING, 30);
        common_Xml_Participantname_Data = common_Xml.newFieldInGroup("common_Xml_Participantname_Data", "PARTICIPANTNAME-DATA", FieldType.STRING, 50);
        common_Xml_Participantname_Close = common_Xml.newFieldInGroup("common_Xml_Participantname_Close", "PARTICIPANTNAME-CLOSE", FieldType.STRING, 30);
        common_Xml_Tiaacontract_Open = common_Xml.newFieldInGroup("common_Xml_Tiaacontract_Open", "TIAACONTRACT-OPEN", FieldType.STRING, 25);
        common_Xml_Tiaacontract_Data = common_Xml.newFieldInGroup("common_Xml_Tiaacontract_Data", "TIAACONTRACT-DATA", FieldType.STRING, 11);
        common_Xml_Tiaacontract_Close = common_Xml.newFieldInGroup("common_Xml_Tiaacontract_Close", "TIAACONTRACT-CLOSE", FieldType.STRING, 25);
        common_Xml_Crefcontract_Open = common_Xml.newFieldInGroup("common_Xml_Crefcontract_Open", "CREFCONTRACT-OPEN", FieldType.STRING, 25);
        common_Xml_Crefcontract_Data = common_Xml.newFieldInGroup("common_Xml_Crefcontract_Data", "CREFCONTRACT-DATA", FieldType.STRING, 11);
        common_Xml_Crefcontract_Close = common_Xml.newFieldInGroup("common_Xml_Crefcontract_Close", "CREFCONTRACT-CLOSE", FieldType.STRING, 25);
        common_Xml_Grossamount_Open = common_Xml.newFieldInGroup("common_Xml_Grossamount_Open", "GROSSAMOUNT-OPEN", FieldType.STRING, 20);
        common_Xml_Grossamount_Data = common_Xml.newFieldInGroup("common_Xml_Grossamount_Data", "GROSSAMOUNT-DATA", FieldType.STRING, 15);
        common_Xml_Grossamount_Close = common_Xml.newFieldInGroup("common_Xml_Grossamount_Close", "GROSSAMOUNT-CLOSE", FieldType.STRING, 20);
        common_Xml_Totaltaxwitheld_Open = common_Xml.newFieldInGroup("common_Xml_Totaltaxwitheld_Open", "TOTALTAXWITHELD-OPEN", FieldType.STRING, 25);
        common_Xml_Totaltaxwitheld_Data = common_Xml.newFieldInGroup("common_Xml_Totaltaxwitheld_Data", "TOTALTAXWITHELD-DATA", FieldType.STRING, 15);
        common_Xml_Totaltaxwitheld_Close = common_Xml.newFieldInGroup("common_Xml_Totaltaxwitheld_Close", "TOTALTAXWITHELD-CLOSE", FieldType.STRING, 25);
        common_Xml_Taxeswithheld_Open = common_Xml.newFieldInGroup("common_Xml_Taxeswithheld_Open", "TAXESWITHHELD-OPEN", FieldType.STRING, 25);
        common_Xml_Taxeswithheld_Close = common_Xml.newFieldInGroup("common_Xml_Taxeswithheld_Close", "TAXESWITHHELD-CLOSE", FieldType.STRING, 25);
        common_Xml_Taxeswithheldinfo_Open = common_Xml.newFieldInGroup("common_Xml_Taxeswithheldinfo_Open", "TAXESWITHHELDINFO-OPEN", FieldType.STRING, 
            30);
        common_Xml_Taxeswithheldinfo_Close = common_Xml.newFieldInGroup("common_Xml_Taxeswithheldinfo_Close", "TAXESWITHHELDINFO-CLOSE", FieldType.STRING, 
            30);
        common_Xml_Taxdescription_Open = common_Xml.newFieldInGroup("common_Xml_Taxdescription_Open", "TAXDESCRIPTION-OPEN", FieldType.STRING, 30);
        common_Xml_Taxdescription_Data = common_Xml.newFieldArrayInGroup("common_Xml_Taxdescription_Data", "TAXDESCRIPTION-DATA", FieldType.STRING, 90, 
            new DbsArrayController(1,6));
        common_Xml_Taxdescription_Close = common_Xml.newFieldInGroup("common_Xml_Taxdescription_Close", "TAXDESCRIPTION-CLOSE", FieldType.STRING, 30);
        common_Xml_Taxamount_Open = common_Xml.newFieldInGroup("common_Xml_Taxamount_Open", "TAXAMOUNT-OPEN", FieldType.STRING, 25);
        common_Xml_Taxamount_Data = common_Xml.newFieldArrayInGroup("common_Xml_Taxamount_Data", "TAXAMOUNT-DATA", FieldType.STRING, 15, new DbsArrayController(1,
            6));
        common_Xml_Taxamount_Close = common_Xml.newFieldInGroup("common_Xml_Taxamount_Close", "TAXAMOUNT-CLOSE", FieldType.STRING, 25);
        common_Xml_Totaltaxwithheld_Open = common_Xml.newFieldInGroup("common_Xml_Totaltaxwithheld_Open", "TOTALTAXWITHHELD-OPEN", FieldType.STRING, 30);
        common_Xml_Totaltaxwithheld_Data = common_Xml.newFieldInGroup("common_Xml_Totaltaxwithheld_Data", "TOTALTAXWITHHELD-DATA", FieldType.STRING, 15);
        common_Xml_Totaltaxwithheld_Close = common_Xml.newFieldInGroup("common_Xml_Totaltaxwithheld_Close", "TOTALTAXWITHHELD-CLOSE", FieldType.STRING, 
            30);
        common_Xml_Otherdeductiondesc_Open = common_Xml.newFieldInGroup("common_Xml_Otherdeductiondesc_Open", "OTHERDEDUCTIONDESC-OPEN", FieldType.STRING, 
            30);
        common_Xml_Otherdeductiondesc_Data = common_Xml.newFieldInGroup("common_Xml_Otherdeductiondesc_Data", "OTHERDEDUCTIONDESC-DATA", FieldType.STRING, 
            70);
        common_Xml_Otherdeductiondesc_Close = common_Xml.newFieldInGroup("common_Xml_Otherdeductiondesc_Close", "OTHERDEDUCTIONDESC-CLOSE", FieldType.STRING, 
            30);
        common_Xml_Otherdeductionamount_Open = common_Xml.newFieldInGroup("common_Xml_Otherdeductionamount_Open", "OTHERDEDUCTIONAMOUNT-OPEN", FieldType.STRING, 
            30);
        common_Xml_Otherdeductionamount_Data = common_Xml.newFieldInGroup("common_Xml_Otherdeductionamount_Data", "OTHERDEDUCTIONAMOUNT-DATA", FieldType.STRING, 
            15);
        common_Xml_Otherdeductionamount_Close = common_Xml.newFieldInGroup("common_Xml_Otherdeductionamount_Close", "OTHERDEDUCTIONAMOUNT-CLOSE", FieldType.STRING, 
            30);
        common_Xml_Netamount_Open = common_Xml.newFieldInGroup("common_Xml_Netamount_Open", "NETAMOUNT-OPEN", FieldType.STRING, 20);
        common_Xml_Netamount_Data = common_Xml.newFieldInGroup("common_Xml_Netamount_Data", "NETAMOUNT-DATA", FieldType.STRING, 15);
        common_Xml_Netamount_Close = common_Xml.newFieldInGroup("common_Xml_Netamount_Close", "NETAMOUNT-CLOSE", FieldType.STRING, 20);
        common_Xml_Aftertaxdesc_Open = common_Xml.newFieldInGroup("common_Xml_Aftertaxdesc_Open", "AFTERTAXDESC-OPEN", FieldType.STRING, 25);
        common_Xml_Aftertaxdesc_Data = common_Xml.newFieldInGroup("common_Xml_Aftertaxdesc_Data", "AFTERTAXDESC-DATA", FieldType.STRING, 90);
        common_Xml_Aftertaxdesc_Close = common_Xml.newFieldInGroup("common_Xml_Aftertaxdesc_Close", "AFTERTAXDESC-CLOSE", FieldType.STRING, 25);
        common_Xml_Aftertaxamount_Open = common_Xml.newFieldInGroup("common_Xml_Aftertaxamount_Open", "AFTERTAXAMOUNT-OPEN", FieldType.STRING, 25);
        common_Xml_Aftertaxamount_Data = common_Xml.newFieldInGroup("common_Xml_Aftertaxamount_Data", "AFTERTAXAMOUNT-DATA", FieldType.STRING, 15);
        common_Xml_Aftertaxamount_Close = common_Xml.newFieldInGroup("common_Xml_Aftertaxamount_Close", "AFTERTAXAMOUNT-CLOSE", FieldType.STRING, 25);
        common_Xml_Memofield_Open = common_Xml.newFieldInGroup("common_Xml_Memofield_Open", "MEMOFIELD-OPEN", FieldType.STRING, 20);
        common_Xml_Memofield_Data = common_Xml.newFieldInGroup("common_Xml_Memofield_Data", "MEMOFIELD-DATA", FieldType.STRING, 200);
        common_Xml_Memofield_Close = common_Xml.newFieldInGroup("common_Xml_Memofield_Close", "MEMOFIELD-CLOSE", FieldType.STRING, 20);
        common_Xml_Memofieldext_Open = common_Xml.newFieldInGroup("common_Xml_Memofieldext_Open", "MEMOFIELDEXT-OPEN", FieldType.STRING, 20);
        common_Xml_Memofieldext_Data = common_Xml.newFieldInGroup("common_Xml_Memofieldext_Data", "MEMOFIELDEXT-DATA", FieldType.STRING, 200);
        common_Xml_Memofieldext_Close = common_Xml.newFieldInGroup("common_Xml_Memofieldext_Close", "MEMOFIELDEXT-CLOSE", FieldType.STRING, 20);
        common_Xml_Idsinfo_Open = common_Xml.newFieldInGroup("common_Xml_Idsinfo_Open", "IDSINFO-OPEN", FieldType.STRING, 25);
        common_Xml_Idsinfo_Close = common_Xml.newFieldInGroup("common_Xml_Idsinfo_Close", "IDSINFO-CLOSE", FieldType.STRING, 25);
        common_Xml_Disbursementdescription_Open = common_Xml.newFieldInGroup("common_Xml_Disbursementdescription_Open", "DISBURSEMENTDESCRIPTION-OPEN", 
            FieldType.STRING, 35);
        common_Xml_Disbursementdescription_Data = common_Xml.newFieldInGroup("common_Xml_Disbursementdescription_Data", "DISBURSEMENTDESCRIPTION-DATA", 
            FieldType.STRING, 35);
        common_Xml_Disbursementdescription_Close = common_Xml.newFieldInGroup("common_Xml_Disbursementdescription_Close", "DISBURSEMENTDESCRIPTION-CLOSE", 
            FieldType.STRING, 35);
        common_Xml_Namelabel_Open = common_Xml.newFieldInGroup("common_Xml_Namelabel_Open", "NAMELABEL-OPEN", FieldType.STRING, 20);
        common_Xml_Namelabel_Data = common_Xml.newFieldInGroup("common_Xml_Namelabel_Data", "NAMELABEL-DATA", FieldType.STRING, 35);
        common_Xml_Namelabel_Close = common_Xml.newFieldInGroup("common_Xml_Namelabel_Close", "NAMELABEL-CLOSE", FieldType.STRING, 20);
        common_Xml_Insuredname_Open = common_Xml.newFieldInGroup("common_Xml_Insuredname_Open", "INSUREDNAME-OPEN", FieldType.STRING, 20);
        common_Xml_Insuredname_Data = common_Xml.newFieldInGroup("common_Xml_Insuredname_Data", "INSUREDNAME-DATA", FieldType.STRING, 50);
        common_Xml_Insuredname_Close = common_Xml.newFieldInGroup("common_Xml_Insuredname_Close", "INSUREDNAME-CLOSE", FieldType.STRING, 20);
        common_Xml_Documentlabel1_Open = common_Xml.newFieldInGroup("common_Xml_Documentlabel1_Open", "DOCUMENTLABEL1-OPEN", FieldType.STRING, 25);
        common_Xml_Documentlabel1_Data = common_Xml.newFieldInGroup("common_Xml_Documentlabel1_Data", "DOCUMENTLABEL1-DATA", FieldType.STRING, 35);
        common_Xml_Documentlabel1_Close = common_Xml.newFieldInGroup("common_Xml_Documentlabel1_Close", "DOCUMENTLABEL1-CLOSE", FieldType.STRING, 25);
        common_Xml_Documentlabel2_Open = common_Xml.newFieldInGroup("common_Xml_Documentlabel2_Open", "DOCUMENTLABEL2-OPEN", FieldType.STRING, 25);
        common_Xml_Documentlabel2_Data = common_Xml.newFieldInGroup("common_Xml_Documentlabel2_Data", "DOCUMENTLABEL2-DATA", FieldType.STRING, 35);
        common_Xml_Documentlabel2_Close = common_Xml.newFieldInGroup("common_Xml_Documentlabel2_Close", "DOCUMENTLABEL2-CLOSE", FieldType.STRING, 25);
        common_Xml_Documentnumber_Open = common_Xml.newFieldInGroup("common_Xml_Documentnumber_Open", "DOCUMENTNUMBER-OPEN", FieldType.STRING, 25);
        common_Xml_Documentnumber_Data = common_Xml.newFieldInGroup("common_Xml_Documentnumber_Data", "DOCUMENTNUMBER-DATA", FieldType.STRING, 10);
        common_Xml_Documentnumber_Close = common_Xml.newFieldInGroup("common_Xml_Documentnumber_Close", "DOCUMENTNUMBER-CLOSE", FieldType.STRING, 25);
        common_Xml_Comments_Open = common_Xml.newFieldInGroup("common_Xml_Comments_Open", "COMMENTS-OPEN", FieldType.STRING, 20);
        common_Xml_Comments_Close = common_Xml.newFieldInGroup("common_Xml_Comments_Close", "COMMENTS-CLOSE", FieldType.STRING, 20);
        common_Xml_Commentline_Open = common_Xml.newFieldInGroup("common_Xml_Commentline_Open", "COMMENTLINE-OPEN", FieldType.STRING, 25);
        common_Xml_Commentline_Data = common_Xml.newFieldInGroup("common_Xml_Commentline_Data", "COMMENTLINE-DATA", FieldType.STRING, 200);
        common_Xml_Commentline_Close = common_Xml.newFieldInGroup("common_Xml_Commentline_Close", "COMMENTLINE-CLOSE", FieldType.STRING, 25);
        common_Xml_Applicationid2_Open = common_Xml.newFieldInGroup("common_Xml_Applicationid2_Open", "APPLICATIONID2-OPEN", FieldType.STRING, 30);
        common_Xml_Applicationid2_Data = common_Xml.newFieldInGroup("common_Xml_Applicationid2_Data", "APPLICATIONID2-DATA", FieldType.STRING, 20);
        common_Xml_Applicationid2_Close = common_Xml.newFieldInGroup("common_Xml_Applicationid2_Close", "APPLICATIONID2-CLOSE", FieldType.STRING, 30);
        common_Xml_Initialpaymentfrequency_Open = common_Xml.newFieldInGroup("common_Xml_Initialpaymentfrequency_Open", "INITIALPAYMENTFREQUENCY-OPEN", 
            FieldType.STRING, 30);
        common_Xml_Initialpaymentfrequency_Data = common_Xml.newFieldInGroup("common_Xml_Initialpaymentfrequency_Data", "INITIALPAYMENTFREQUENCY-DATA", 
            FieldType.STRING, 35);
        common_Xml_Initialpaymentfrequency_Close = common_Xml.newFieldInGroup("common_Xml_Initialpaymentfrequency_Close", "INITIALPAYMENTFREQUENCY-CLOSE", 
            FieldType.STRING, 30);
        common_Xml_Typeofannuity_Open = common_Xml.newFieldInGroup("common_Xml_Typeofannuity_Open", "TYPEOFANNUITY-OPEN", FieldType.STRING, 20);
        common_Xml_Typeofannuity_Data = common_Xml.newFieldInGroup("common_Xml_Typeofannuity_Data", "TYPEOFANNUITY-DATA", FieldType.STRING, 100);
        common_Xml_Typeofannuity_Close = common_Xml.newFieldInGroup("common_Xml_Typeofannuity_Close", "TYPEOFANNUITY-CLOSE", FieldType.STRING, 20);
        common_Xml_Tiaacreflist_Open = common_Xml.newFieldInGroup("common_Xml_Tiaacreflist_Open", "TIAACREFLIST-OPEN", FieldType.STRING, 20);
        common_Xml_Tiaacreflist_Close = common_Xml.newFieldInGroup("common_Xml_Tiaacreflist_Close", "TIAACREFLIST-CLOSE", FieldType.STRING, 20);
        common_Xml_Settlementlist_Open = common_Xml.newFieldInGroup("common_Xml_Settlementlist_Open", "SETTLEMENTLIST-OPEN", FieldType.STRING, 20);
        common_Xml_Settlementlist_Data = common_Xml.newFieldInGroup("common_Xml_Settlementlist_Data", "SETTLEMENTLIST-DATA", FieldType.STRING, 11);
        common_Xml_Settlementlist_Close = common_Xml.newFieldInGroup("common_Xml_Settlementlist_Close", "SETTLEMENTLIST-CLOSE", FieldType.STRING, 20);
        common_Xml_Tiaacrefnumber_Open = common_Xml.newFieldInGroup("common_Xml_Tiaacrefnumber_Open", "TIAACREFNUMBER-OPEN", FieldType.STRING, 20);
        common_Xml_Tiaacrefnumber_Data = common_Xml.newFieldInGroup("common_Xml_Tiaacrefnumber_Data", "TIAACREFNUMBER-DATA", FieldType.STRING, 11);
        common_Xml_Tiaacrefnumber_Close = common_Xml.newFieldInGroup("common_Xml_Tiaacrefnumber_Close", "TIAACREFNUMBER-CLOSE", FieldType.STRING, 20);
        common_Xml_Tiaacrefind_Open = common_Xml.newFieldInGroup("common_Xml_Tiaacrefind_Open", "TIAACREFIND-OPEN", FieldType.STRING, 15);
        common_Xml_Tiaacrefind_Data = common_Xml.newFieldInGroup("common_Xml_Tiaacrefind_Data", "TIAACREFIND-DATA", FieldType.STRING, 1);
        common_Xml_Tiaacrefind_Close = common_Xml.newFieldInGroup("common_Xml_Tiaacrefind_Close", "TIAACREFIND-CLOSE", FieldType.STRING, 15);
        common_Xml_Annuitystartdate_Open = common_Xml.newFieldInGroup("common_Xml_Annuitystartdate_Open", "ANNUITYSTARTDATE-OPEN", FieldType.STRING, 20);
        common_Xml_Annuitystartdate_Data = common_Xml.newFieldInGroup("common_Xml_Annuitystartdate_Data", "ANNUITYSTARTDATE-DATA", FieldType.STRING, 10);
        common_Xml_Annuitystartdate_Close = common_Xml.newFieldInGroup("common_Xml_Annuitystartdate_Close", "ANNUITYSTARTDATE-CLOSE", FieldType.STRING, 
            20);
        common_Xml_Paymentdetail_Open = common_Xml.newFieldInGroup("common_Xml_Paymentdetail_Open", "PAYMENTDETAIL-OPEN", FieldType.STRING, 20);
        common_Xml_Paymentdetail_Close = common_Xml.newFieldInGroup("common_Xml_Paymentdetail_Close", "PAYMENTDETAIL-CLOSE", FieldType.STRING, 20);
        common_Xml_Paymentinfo_Open = common_Xml.newFieldInGroup("common_Xml_Paymentinfo_Open", "PAYMENTINFO-OPEN", FieldType.STRING, 25);
        common_Xml_Paymentinfo_Close = common_Xml.newFieldInGroup("common_Xml_Paymentinfo_Close", "PAYMENTINFO-CLOSE", FieldType.STRING, 25);
        common_Xml_Duedate_Open = common_Xml.newFieldInGroup("common_Xml_Duedate_Open", "DUEDATE-OPEN", FieldType.STRING, 15);
        common_Xml_Duedate_Data = common_Xml.newFieldInGroup("common_Xml_Duedate_Data", "DUEDATE-DATA", FieldType.STRING, 10);
        common_Xml_Duedate_Close = common_Xml.newFieldInGroup("common_Xml_Duedate_Close", "DUEDATE-CLOSE", FieldType.STRING, 15);
        common_Xml_Duedatefuture_Open = common_Xml.newFieldInGroup("common_Xml_Duedatefuture_Open", "DUEDATEFUTURE-OPEN", FieldType.STRING, 20);
        common_Xml_Duedatefuture_Data = common_Xml.newFieldInGroup("common_Xml_Duedatefuture_Data", "DUEDATEFUTURE-DATA", FieldType.STRING, 10);
        common_Xml_Duedatefuture_Close = common_Xml.newFieldInGroup("common_Xml_Duedatefuture_Close", "DUEDATEFUTURE-CLOSE", FieldType.STRING, 20);
        common_Xml_Description_Open = common_Xml.newFieldInGroup("common_Xml_Description_Open", "DESCRIPTION-OPEN", FieldType.STRING, 15);
        common_Xml_Description_Data = common_Xml.newFieldInGroup("common_Xml_Description_Data", "DESCRIPTION-DATA", FieldType.STRING, 50);
        common_Xml_Description_Close = common_Xml.newFieldInGroup("common_Xml_Description_Close", "DESCRIPTION-CLOSE", FieldType.STRING, 15);
        common_Xml_Deductionamountfrequency_Open = common_Xml.newFieldInGroup("common_Xml_Deductionamountfrequency_Open", "DEDUCTIONAMOUNTFREQUENCY-OPEN", 
            FieldType.STRING, 35);
        common_Xml_Deductionamountfrequency_Data = common_Xml.newFieldInGroup("common_Xml_Deductionamountfrequency_Data", "DEDUCTIONAMOUNTFREQUENCY-DATA", 
            FieldType.STRING, 15);
        common_Xml_Deductionamountfrequency_Close = common_Xml.newFieldInGroup("common_Xml_Deductionamountfrequency_Close", "DEDUCTIONAMOUNTFREQUENCY-CLOSE", 
            FieldType.STRING, 35);
        common_Xml_Paymentamount_Open = common_Xml.newFieldInGroup("common_Xml_Paymentamount_Open", "PAYMENTAMOUNT-OPEN", FieldType.STRING, 20);
        common_Xml_Paymentamount_Data = common_Xml.newFieldInGroup("common_Xml_Paymentamount_Data", "PAYMENTAMOUNT-DATA", FieldType.STRING, 15);
        common_Xml_Paymentamount_Close = common_Xml.newFieldInGroup("common_Xml_Paymentamount_Close", "PAYMENTAMOUNT-CLOSE", FieldType.STRING, 20);
        common_Xml_Paymentfrequency_Open = common_Xml.newFieldInGroup("common_Xml_Paymentfrequency_Open", "PAYMENTFREQUENCY-OPEN", FieldType.STRING, 20);
        common_Xml_Paymentfrequency_Data = common_Xml.newFieldInGroup("common_Xml_Paymentfrequency_Data", "PAYMENTFREQUENCY-DATA", FieldType.STRING, 15);
        common_Xml_Paymentfrequency_Close = common_Xml.newFieldInGroup("common_Xml_Paymentfrequency_Close", "PAYMENTFREQUENCY-CLOSE", FieldType.STRING, 
            20);
        common_Xml_Unit_Open = common_Xml.newFieldInGroup("common_Xml_Unit_Open", "UNIT-OPEN", FieldType.STRING, 10);
        common_Xml_Unit_Data = common_Xml.newFieldInGroup("common_Xml_Unit_Data", "UNIT-DATA", FieldType.STRING, 15);
        common_Xml_Unit_Close = common_Xml.newFieldInGroup("common_Xml_Unit_Close", "UNIT-CLOSE", FieldType.STRING, 10);
        common_Xml_Unitvalue_Open = common_Xml.newFieldInGroup("common_Xml_Unitvalue_Open", "UNITVALUE-OPEN", FieldType.STRING, 15);
        common_Xml_Unitvalue_Data = common_Xml.newFieldInGroup("common_Xml_Unitvalue_Data", "UNITVALUE-DATA", FieldType.STRING, 15);
        common_Xml_Unitvalue_Close = common_Xml.newFieldInGroup("common_Xml_Unitvalue_Close", "UNITVALUE-CLOSE", FieldType.STRING, 15);
        common_Xml_Contractual_Open = common_Xml.newFieldInGroup("common_Xml_Contractual_Open", "CONTRACTUAL-OPEN", FieldType.STRING, 15);
        common_Xml_Contractual_Data = common_Xml.newFieldInGroup("common_Xml_Contractual_Data", "CONTRACTUAL-DATA", FieldType.STRING, 15);
        common_Xml_Contractual_Close = common_Xml.newFieldInGroup("common_Xml_Contractual_Close", "CONTRACTUAL-CLOSE", FieldType.STRING, 15);
        common_Xml_Dividend_Open = common_Xml.newFieldInGroup("common_Xml_Dividend_Open", "DIVIDEND-OPEN", FieldType.STRING, 15);
        common_Xml_Dividend_Data = common_Xml.newFieldInGroup("common_Xml_Dividend_Data", "DIVIDEND-DATA", FieldType.STRING, 15);
        common_Xml_Dividend_Close = common_Xml.newFieldInGroup("common_Xml_Dividend_Close", "DIVIDEND-CLOSE", FieldType.STRING, 15);
        common_Xml_Tiaacontractpayment_Open = common_Xml.newFieldInGroup("common_Xml_Tiaacontractpayment_Open", "TIAACONTRACTPAYMENT-OPEN", FieldType.STRING, 
            25);
        common_Xml_Tiaacontractpayment_Data = common_Xml.newFieldInGroup("common_Xml_Tiaacontractpayment_Data", "TIAACONTRACTPAYMENT-DATA", FieldType.STRING, 
            10);
        common_Xml_Tiaacontractpayment_Close = common_Xml.newFieldInGroup("common_Xml_Tiaacontractpayment_Close", "TIAACONTRACTPAYMENT-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Crefcertificate_Open = common_Xml.newFieldInGroup("common_Xml_Crefcertificate_Open", "CREFCERTIFICATE-OPEN", FieldType.STRING, 25);
        common_Xml_Crefcertificate_Data = common_Xml.newFieldInGroup("common_Xml_Crefcertificate_Data", "CREFCERTIFICATE-DATA", FieldType.STRING, 10);
        common_Xml_Crefcertificate_Close = common_Xml.newFieldInGroup("common_Xml_Crefcertificate_Close", "CREFCERTIFICATE-CLOSE", FieldType.STRING, 25);
        common_Xml_Paymentamountfrom_Open = common_Xml.newFieldInGroup("common_Xml_Paymentamountfrom_Open", "PAYMENTAMOUNTFROM-OPEN", FieldType.STRING, 
            25);
        common_Xml_Paymentamountfrom_Data = common_Xml.newFieldInGroup("common_Xml_Paymentamountfrom_Data", "PAYMENTAMOUNTFROM-DATA", FieldType.STRING, 
            15);
        common_Xml_Paymentamountfrom_Close = common_Xml.newFieldInGroup("common_Xml_Paymentamountfrom_Close", "PAYMENTAMOUNTFROM-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Paymentamountfromfrequency_Open = common_Xml.newFieldInGroup("common_Xml_Paymentamountfromfrequency_Open", "PAYMENTAMOUNTFROMFREQUENCY-OPEN", 
            FieldType.STRING, 35);
        common_Xml_Paymentamountfromfrequency_Data = common_Xml.newFieldInGroup("common_Xml_Paymentamountfromfrequency_Data", "PAYMENTAMOUNTFROMFREQUENCY-DATA", 
            FieldType.STRING, 15);
        common_Xml_Paymentamountfromfrequency_Close = common_Xml.newFieldInGroup("common_Xml_Paymentamountfromfrequency_Close", "PAYMENTAMOUNTFROMFREQUENCY-CLOSE", 
            FieldType.STRING, 35);
        common_Xml_Interest_Open = common_Xml.newFieldInGroup("common_Xml_Interest_Open", "INTEREST-OPEN", FieldType.STRING, 15);
        common_Xml_Interest_Data = common_Xml.newFieldInGroup("common_Xml_Interest_Data", "INTEREST-DATA", FieldType.STRING, 15);
        common_Xml_Interest_Close = common_Xml.newFieldInGroup("common_Xml_Interest_Close", "INTEREST-CLOSE", FieldType.STRING, 15);
        common_Xml_Interestpayment_Open = common_Xml.newFieldInGroup("common_Xml_Interestpayment_Open", "INTERESTPAYMENT-OPEN", FieldType.STRING, 20);
        common_Xml_Interestpayment_Data = common_Xml.newFieldInGroup("common_Xml_Interestpayment_Data", "INTERESTPAYMENT-DATA", FieldType.STRING, 15);
        common_Xml_Interestpayment_Close = common_Xml.newFieldInGroup("common_Xml_Interestpayment_Close", "INTERESTPAYMENT-CLOSE", FieldType.STRING, 20);
        common_Xml_Netpaymentamount_Open = common_Xml.newFieldInGroup("common_Xml_Netpaymentamount_Open", "NETPAYMENTAMOUNT-OPEN", FieldType.STRING, 25);
        common_Xml_Netpaymentamount_Data = common_Xml.newFieldInGroup("common_Xml_Netpaymentamount_Data", "NETPAYMENTAMOUNT-DATA", FieldType.STRING, 15);
        common_Xml_Netpaymentamount_Close = common_Xml.newFieldInGroup("common_Xml_Netpaymentamount_Close", "NETPAYMENTAMOUNT-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Subpaymentdetail_Open = common_Xml.newFieldInGroup("common_Xml_Subpaymentdetail_Open", "SUBPAYMENTDETAIL-OPEN", FieldType.STRING, 25);
        common_Xml_Subpaymentdetail_Close = common_Xml.newFieldInGroup("common_Xml_Subpaymentdetail_Close", "SUBPAYMENTDETAIL-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Subdate_Open = common_Xml.newFieldInGroup("common_Xml_Subdate_Open", "SUBDATE-OPEN", FieldType.STRING, 15);
        common_Xml_Subdate_Data = common_Xml.newFieldInGroup("common_Xml_Subdate_Data", "SUBDATE-DATA", FieldType.STRING, 10);
        common_Xml_Subdate_Close = common_Xml.newFieldInGroup("common_Xml_Subdate_Close", "SUBDATE-CLOSE", FieldType.STRING, 15);
        common_Xml_Subtiaacontract_Open = common_Xml.newFieldInGroup("common_Xml_Subtiaacontract_Open", "SUBTIAACONTRACT-OPEN", FieldType.STRING, 25);
        common_Xml_Subtiaacontract_Data = common_Xml.newFieldInGroup("common_Xml_Subtiaacontract_Data", "SUBTIAACONTRACT-DATA", FieldType.STRING, 10);
        common_Xml_Subtiaacontract_Close = common_Xml.newFieldInGroup("common_Xml_Subtiaacontract_Close", "SUBTIAACONTRACT-CLOSE", FieldType.STRING, 25);
        common_Xml_Subcrefcertificate_Open = common_Xml.newFieldInGroup("common_Xml_Subcrefcertificate_Open", "SUBCREFCERTIFICATE-OPEN", FieldType.STRING, 
            25);
        common_Xml_Subcrefcertificate_Data = common_Xml.newFieldInGroup("common_Xml_Subcrefcertificate_Data", "SUBCREFCERTIFICATE-DATA", FieldType.STRING, 
            10);
        common_Xml_Subcrefcertificate_Close = common_Xml.newFieldInGroup("common_Xml_Subcrefcertificate_Close", "SUBCREFCERTIFICATE-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Subpaymentfrom_Open = common_Xml.newFieldInGroup("common_Xml_Subpaymentfrom_Open", "SUBPAYMENTFROM-OPEN", FieldType.STRING, 25);
        common_Xml_Subpaymentfrom_Data = common_Xml.newFieldInGroup("common_Xml_Subpaymentfrom_Data", "SUBPAYMENTFROM-DATA", FieldType.STRING, 15);
        common_Xml_Subpaymentfrom_Close = common_Xml.newFieldInGroup("common_Xml_Subpaymentfrom_Close", "SUBPAYMENTFROM-CLOSE", FieldType.STRING, 25);
        common_Xml_Subpaymentfromfrequency_Open = common_Xml.newFieldInGroup("common_Xml_Subpaymentfromfrequency_Open", "SUBPAYMENTFROMFREQUENCY-OPEN", 
            FieldType.STRING, 35);
        common_Xml_Subpaymentfromfrequency_Data = common_Xml.newFieldInGroup("common_Xml_Subpaymentfromfrequency_Data", "SUBPAYMENTFROMFREQUENCY-DATA", 
            FieldType.STRING, 15);
        common_Xml_Subpaymentfromfrequency_Close = common_Xml.newFieldInGroup("common_Xml_Subpaymentfromfrequency_Close", "SUBPAYMENTFROMFREQUENCY-CLOSE", 
            FieldType.STRING, 35);
        common_Xml_Subpaymentamount_Open = common_Xml.newFieldInGroup("common_Xml_Subpaymentamount_Open", "SUBPAYMENTAMOUNT-OPEN", FieldType.STRING, 20);
        common_Xml_Subpaymentamount_Data = common_Xml.newFieldInGroup("common_Xml_Subpaymentamount_Data", "SUBPAYMENTAMOUNT-DATA", FieldType.STRING, 15);
        common_Xml_Subpaymentamount_Close = common_Xml.newFieldInGroup("common_Xml_Subpaymentamount_Close", "SUBPAYMENTAMOUNT-CLOSE", FieldType.STRING, 
            20);
        common_Xml_Subpaymentamountfrequency_Open = common_Xml.newFieldInGroup("common_Xml_Subpaymentamountfrequency_Open", "SUBPAYMENTAMOUNTFREQUENCY-OPEN", 
            FieldType.STRING, 35);
        common_Xml_Subpaymentamountfrequency_Data = common_Xml.newFieldInGroup("common_Xml_Subpaymentamountfrequency_Data", "SUBPAYMENTAMOUNTFREQUENCY-DATA", 
            FieldType.STRING, 15);
        common_Xml_Subpaymentamountfrequency_Close = common_Xml.newFieldInGroup("common_Xml_Subpaymentamountfrequency_Close", "SUBPAYMENTAMOUNTFREQUENCY-CLOSE", 
            FieldType.STRING, 35);
        common_Xml_Subdeductionamount_Open = common_Xml.newFieldInGroup("common_Xml_Subdeductionamount_Open", "SUBDEDUCTIONAMOUNT-OPEN", FieldType.STRING, 
            25);
        common_Xml_Subdeductionamount_Data = common_Xml.newFieldInGroup("common_Xml_Subdeductionamount_Data", "SUBDEDUCTIONAMOUNT-DATA", FieldType.STRING, 
            15);
        common_Xml_Subdeductionamount_Close = common_Xml.newFieldInGroup("common_Xml_Subdeductionamount_Close", "SUBDEDUCTIONAMOUNT-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Subdeductionamountfrequency_Open = common_Xml.newFieldInGroup("common_Xml_Subdeductionamountfrequency_Open", "SUBDEDUCTIONAMOUNTFREQUENCY-OPEN", 
            FieldType.STRING, 35);
        common_Xml_Subdeductionamountfrequency_Data = common_Xml.newFieldInGroup("common_Xml_Subdeductionamountfrequency_Data", "SUBDEDUCTIONAMOUNTFREQUENCY-DATA", 
            FieldType.STRING, 15);
        common_Xml_Subdeductionamountfrequency_Clos = common_Xml.newFieldInGroup("common_Xml_Subdeductionamountfrequency_Clos", "SUBDEDUCTIONAMOUNTFREQUENCY-CLOS", 
            FieldType.STRING, 35);
        common_Xml_Totaldeductionamount_Open = common_Xml.newFieldInGroup("common_Xml_Totaldeductionamount_Open", "TOTALDEDUCTIONAMOUNT-OPEN", FieldType.STRING, 
            25);
        common_Xml_Totaldeductionamount_Data = common_Xml.newFieldInGroup("common_Xml_Totaldeductionamount_Data", "TOTALDEDUCTIONAMOUNT-DATA", FieldType.STRING, 
            15);
        common_Xml_Totaldeductionamount_Close = common_Xml.newFieldInGroup("common_Xml_Totaldeductionamount_Close", "TOTALDEDUCTIONAMOUNT-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Deductions_Open = common_Xml.newFieldInGroup("common_Xml_Deductions_Open", "DEDUCTIONS-OPEN", FieldType.STRING, 15);
        common_Xml_Deductions_Close = common_Xml.newFieldInGroup("common_Xml_Deductions_Close", "DEDUCTIONS-CLOSE", FieldType.STRING, 15);
        common_Xml_Deductionsinfo_Open = common_Xml.newFieldInGroup("common_Xml_Deductionsinfo_Open", "DEDUCTIONSINFO-OPEN", FieldType.STRING, 25);
        common_Xml_Deductionsinfo_Close = common_Xml.newFieldInGroup("common_Xml_Deductionsinfo_Close", "DEDUCTIONSINFO-CLOSE", FieldType.STRING, 25);
        common_Xml_Deductiondescription_Open = common_Xml.newFieldInGroup("common_Xml_Deductiondescription_Open", "DEDUCTIONDESCRIPTION-OPEN", FieldType.STRING, 
            25);
        common_Xml_Deductiondescription_Data = common_Xml.newFieldInGroup("common_Xml_Deductiondescription_Data", "DEDUCTIONDESCRIPTION-DATA", FieldType.STRING, 
            30);
        common_Xml_Deductiondescription_Close = common_Xml.newFieldInGroup("common_Xml_Deductiondescription_Close", "DEDUCTIONDESCRIPTION-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Deductionamount_Open = common_Xml.newFieldInGroup("common_Xml_Deductionamount_Open", "DEDUCTIONAMOUNT-OPEN", FieldType.STRING, 20);
        common_Xml_Deductionamount_Data = common_Xml.newFieldInGroup("common_Xml_Deductionamount_Data", "DEDUCTIONAMOUNT-DATA", FieldType.STRING, 15);
        common_Xml_Deductionamount_Close = common_Xml.newFieldInGroup("common_Xml_Deductionamount_Close", "DEDUCTIONAMOUNT-CLOSE", FieldType.STRING, 20);
        common_Xml_Institutionname_Open = common_Xml.newFieldInGroup("common_Xml_Institutionname_Open", "INSTITUTIONNAME-OPEN", FieldType.STRING, 30);
        common_Xml_Institutionname_Data = common_Xml.newFieldInGroup("common_Xml_Institutionname_Data", "INSTITUTIONNAME-DATA", FieldType.STRING, 50);
        common_Xml_Institutionname_Close = common_Xml.newFieldInGroup("common_Xml_Institutionname_Close", "INSTITUTIONNAME-CLOSE", FieldType.STRING, 30);
        common_Xml_Institutionaddress_Open = common_Xml.newFieldInGroup("common_Xml_Institutionaddress_Open", "INSTITUTIONADDRESS-OPEN", FieldType.STRING, 
            25);
        common_Xml_Institutionaddress_Close = common_Xml.newFieldInGroup("common_Xml_Institutionaddress_Close", "INSTITUTIONADDRESS-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Institutionaddress_Lines_Open = common_Xml.newFieldInGroup("common_Xml_Institutionaddress_Lines_Open", "INSTITUTIONADDRESS-LINES-OPEN", 
            FieldType.STRING, 30);
        common_Xml_Institutionaddress_Lines_Data = common_Xml.newFieldArrayInGroup("common_Xml_Institutionaddress_Lines_Data", "INSTITUTIONADDRESS-LINES-DATA", 
            FieldType.STRING, 35, new DbsArrayController(1,6));
        common_Xml_Institutionaddress_Lines_Close = common_Xml.newFieldInGroup("common_Xml_Institutionaddress_Lines_Close", "INSTITUTIONADDRESS-LINES-CLOSE", 
            FieldType.STRING, 30);
        common_Xml_Institutioninfo_Open = common_Xml.newFieldInGroup("common_Xml_Institutioninfo_Open", "INSTITUTIONINFO-OPEN", FieldType.STRING, 25);
        common_Xml_Institutioninfo_Close = common_Xml.newFieldInGroup("common_Xml_Institutioninfo_Close", "INSTITUTIONINFO-CLOSE", FieldType.STRING, 25);
        common_Xml_Institutioninfoline_Open = common_Xml.newFieldInGroup("common_Xml_Institutioninfoline_Open", "INSTITUTIONINFOLINE-OPEN", FieldType.STRING, 
            25);
        common_Xml_Institutioninfoline_Data = common_Xml.newFieldArrayInGroup("common_Xml_Institutioninfoline_Data", "INSTITUTIONINFOLINE-DATA", FieldType.STRING, 
            70, new DbsArrayController(1,8));
        common_Xml_Institutioninfoline_Close = common_Xml.newFieldInGroup("common_Xml_Institutioninfoline_Close", "INSTITUTIONINFOLINE-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Participantinstitutionind_Open = common_Xml.newFieldInGroup("common_Xml_Participantinstitutionind_Open", "PARTICIPANTINSTITUTIONIND-OPEN", 
            FieldType.STRING, 30);
        common_Xml_Participantinstitutionind_Data = common_Xml.newFieldInGroup("common_Xml_Participantinstitutionind_Data", "PARTICIPANTINSTITUTIONIND-DATA", 
            FieldType.STRING, 5);
        common_Xml_Participantinstitutionind_Close = common_Xml.newFieldInGroup("common_Xml_Participantinstitutionind_Close", "PARTICIPANTINSTITUTIONIND-CLOSE", 
            FieldType.STRING, 30);
        common_Xml_Omnipay_Open = common_Xml.newFieldInGroup("common_Xml_Omnipay_Open", "OMNIPAY-OPEN", FieldType.STRING, 15);
        common_Xml_Omnipay_Close = common_Xml.newFieldInGroup("common_Xml_Omnipay_Close", "OMNIPAY-CLOSE", FieldType.STRING, 15);
        common_Xml_Documenttype_Open = common_Xml.newFieldInGroup("common_Xml_Documenttype_Open", "DOCUMENTTYPE-OPEN", FieldType.STRING, 15);
        common_Xml_Documenttype_Data = common_Xml.newFieldInGroup("common_Xml_Documenttype_Data", "DOCUMENTTYPE-DATA", FieldType.STRING, 10);
        common_Xml_Documenttype_Close = common_Xml.newFieldInGroup("common_Xml_Documenttype_Close", "DOCUMENTTYPE-CLOSE", FieldType.STRING, 15);
        common_Xml_Participantinstitutionflag_Open = common_Xml.newFieldInGroup("common_Xml_Participantinstitutionflag_Open", "PARTICIPANTINSTITUTIONFLAG-OPEN", 
            FieldType.STRING, 30);
        common_Xml_Participantinstitutionflag_Data = common_Xml.newFieldInGroup("common_Xml_Participantinstitutionflag_Data", "PARTICIPANTINSTITUTIONFLAG-DATA", 
            FieldType.STRING, 15);
        common_Xml_Participantinstitutionflag_Clos = common_Xml.newFieldInGroup("common_Xml_Participantinstitutionflag_Clos", "PARTICIPANTINSTITUTIONFLAG-CLOS", 
            FieldType.STRING, 30);
        common_Xml_Paymentfor_Open = common_Xml.newFieldInGroup("common_Xml_Paymentfor_Open", "PAYMENTFOR-OPEN", FieldType.STRING, 15);
        common_Xml_Paymentfor_Close = common_Xml.newFieldInGroup("common_Xml_Paymentfor_Close", "PAYMENTFOR-CLOSE", FieldType.STRING, 15);
        common_Xml_Tiaanumber_Open = common_Xml.newFieldInGroup("common_Xml_Tiaanumber_Open", "TIAANUMBER-OPEN", FieldType.STRING, 15);
        common_Xml_Tiaanumber_Data = common_Xml.newFieldInGroup("common_Xml_Tiaanumber_Data", "TIAANUMBER-DATA", FieldType.STRING, 10);
        common_Xml_Tiaanumber_Close = common_Xml.newFieldInGroup("common_Xml_Tiaanumber_Close", "TIAANUMBER-CLOSE", FieldType.STRING, 15);
        common_Xml_Crefnumber_Open = common_Xml.newFieldInGroup("common_Xml_Crefnumber_Open", "CREFNUMBER-OPEN", FieldType.STRING, 15);
        common_Xml_Crefnumber_Data = common_Xml.newFieldInGroup("common_Xml_Crefnumber_Data", "CREFNUMBER-DATA", FieldType.STRING, 10);
        common_Xml_Crefnumber_Close = common_Xml.newFieldInGroup("common_Xml_Crefnumber_Close", "CREFNUMBER-CLOSE", FieldType.STRING, 15);
        common_Xml_Effectivedate_Open = common_Xml.newFieldInGroup("common_Xml_Effectivedate_Open", "EFFECTIVEDATE-OPEN", FieldType.STRING, 20);
        common_Xml_Effectivedate_Data = common_Xml.newFieldInGroup("common_Xml_Effectivedate_Data", "EFFECTIVEDATE-DATA", FieldType.STRING, 10);
        common_Xml_Effectivedate_Close = common_Xml.newFieldInGroup("common_Xml_Effectivedate_Close", "EFFECTIVEDATE-CLOSE", FieldType.STRING, 20);
        common_Xml_Forparticipant_Open = common_Xml.newFieldInGroup("common_Xml_Forparticipant_Open", "FORPARTICIPANT-OPEN", FieldType.STRING, 20);
        common_Xml_Forparticipant_Close = common_Xml.newFieldInGroup("common_Xml_Forparticipant_Close", "FORPARTICIPANT-CLOSE", FieldType.STRING, 20);
        common_Xml_Foralternatecarrier_Open = common_Xml.newFieldInGroup("common_Xml_Foralternatecarrier_Open", "FORALTERNATECARRIER-OPEN", FieldType.STRING, 
            25);
        common_Xml_Foralternatecarrier_Close = common_Xml.newFieldInGroup("common_Xml_Foralternatecarrier_Close", "FORALTERNATECARRIER-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Paymentinformationsummary_Open = common_Xml.newFieldInGroup("common_Xml_Paymentinformationsummary_Open", "PAYMENTINFORMATIONSUMMARY-OPEN", 
            FieldType.STRING, 30);
        common_Xml_Paymentinformationsummary_Close = common_Xml.newFieldInGroup("common_Xml_Paymentinformationsummary_Close", "PAYMENTINFORMATIONSUMMARY-CLOSE", 
            FieldType.STRING, 30);
        common_Xml_Paymentdetailinformation_Open = common_Xml.newFieldInGroup("common_Xml_Paymentdetailinformation_Open", "PAYMENTDETAILINFORMATION-OPEN", 
            FieldType.STRING, 30);
        common_Xml_Paymentdetailinformation_Close = common_Xml.newFieldInGroup("common_Xml_Paymentdetailinformation_Close", "PAYMENTDETAILINFORMATION-CLOSE", 
            FieldType.STRING, 30);
        common_Xml_Accountnumber_Open = common_Xml.newFieldInGroup("common_Xml_Accountnumber_Open", "ACCOUNTNUMBER-OPEN", FieldType.STRING, 20);
        common_Xml_Accountnumber_Data = common_Xml.newFieldInGroup("common_Xml_Accountnumber_Data", "ACCOUNTNUMBER-DATA", FieldType.STRING, 10);
        common_Xml_Accountnumber_Close = common_Xml.newFieldInGroup("common_Xml_Accountnumber_Close", "ACCOUNTNUMBER-CLOSE", FieldType.STRING, 20);
        common_Xml_Transferaccountslist_Open = common_Xml.newFieldInGroup("common_Xml_Transferaccountslist_Open", "TRANSFERACCOUNTSLIST-OPEN", FieldType.STRING, 
            25);
        common_Xml_Transferaccountslist_Close = common_Xml.newFieldInGroup("common_Xml_Transferaccountslist_Close", "TRANSFERACCOUNTSLIST-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Transferaccounts_Open = common_Xml.newFieldInGroup("common_Xml_Transferaccounts_Open", "TRANSFERACCOUNTS-OPEN", FieldType.STRING, 20);
        common_Xml_Transferaccounts_Data = common_Xml.newFieldInGroup("common_Xml_Transferaccounts_Data", "TRANSFERACCOUNTS-DATA", FieldType.STRING, 10);
        common_Xml_Transferaccounts_Close = common_Xml.newFieldInGroup("common_Xml_Transferaccounts_Close", "TRANSFERACCOUNTS-CLOSE", FieldType.STRING, 
            20);
        common_Xml_Employer_Open = common_Xml.newFieldInGroup("common_Xml_Employer_Open", "EMPLOYER-OPEN", FieldType.STRING, 15);
        common_Xml_Employer_Data = common_Xml.newFieldInGroup("common_Xml_Employer_Data", "EMPLOYER-DATA", FieldType.STRING, 60);
        common_Xml_Employer_Close = common_Xml.newFieldInGroup("common_Xml_Employer_Close", "EMPLOYER-CLOSE", FieldType.STRING, 15);
        common_Xml_Typeofplan_Open = common_Xml.newFieldInGroup("common_Xml_Typeofplan_Open", "TYPEOFPLAN-OPEN", FieldType.STRING, 15);
        common_Xml_Typeofplan_Data = common_Xml.newFieldInGroup("common_Xml_Typeofplan_Data", "TYPEOFPLAN-DATA", FieldType.STRING, 100);
        common_Xml_Typeofplan_Close = common_Xml.newFieldInGroup("common_Xml_Typeofplan_Close", "TYPEOFPLAN-CLOSE", FieldType.STRING, 15);
        common_Xml_Cashavailable_Open = common_Xml.newFieldInGroup("common_Xml_Cashavailable_Open", "CASHAVAILABLE-OPEN", FieldType.STRING, 20);
        common_Xml_Cashavailable_Data = common_Xml.newFieldInGroup("common_Xml_Cashavailable_Data", "CASHAVAILABLE-DATA", FieldType.STRING, 15);
        common_Xml_Cashavailable_Close = common_Xml.newFieldInGroup("common_Xml_Cashavailable_Close", "CASHAVAILABLE-CLOSE", FieldType.STRING, 20);
        common_Xml_Distributionrequirements_Open = common_Xml.newFieldInGroup("common_Xml_Distributionrequirements_Open", "DISTRIBUTIONREQUIREMENTS-OPEN", 
            FieldType.STRING, 30);
        common_Xml_Distributionrequirements_Close = common_Xml.newFieldInGroup("common_Xml_Distributionrequirements_Close", "DISTRIBUTIONREQUIREMENTS-CLOSE", 
            FieldType.STRING, 30);
        common_Xml_Paymentinformation_Open = common_Xml.newFieldInGroup("common_Xml_Paymentinformation_Open", "PAYMENTINFORMATION-OPEN", FieldType.STRING, 
            25);
        common_Xml_Paymentinformation_Close = common_Xml.newFieldInGroup("common_Xml_Paymentinformation_Close", "PAYMENTINFORMATION-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Deductionstype_Open = common_Xml.newFieldInGroup("common_Xml_Deductionstype_Open", "DEDUCTIONSTYPE-OPEN", FieldType.STRING, 20);
        common_Xml_Deductionstype_Close = common_Xml.newFieldInGroup("common_Xml_Deductionstype_Close", "DEDUCTIONSTYPE-CLOSE", FieldType.STRING, 20);
        common_Xml_Grosstotaldeductions_Open = common_Xml.newFieldInGroup("common_Xml_Grosstotaldeductions_Open", "GROSSTOTALDEDUCTIONS-OPEN", FieldType.STRING, 
            25);
        common_Xml_Grosstotaldeductions_Data = common_Xml.newFieldInGroup("common_Xml_Grosstotaldeductions_Data", "GROSSTOTALDEDUCTIONS-DATA", FieldType.STRING, 
            15);
        common_Xml_Grosstotaldeductions_Close = common_Xml.newFieldInGroup("common_Xml_Grosstotaldeductions_Close", "GROSSTOTALDEDUCTIONS-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Grosstotalpayments_Open = common_Xml.newFieldInGroup("common_Xml_Grosstotalpayments_Open", "GROSSTOTALPAYMENTS-OPEN", FieldType.STRING, 
            25);
        common_Xml_Grosstotalpayments_Data = common_Xml.newFieldInGroup("common_Xml_Grosstotalpayments_Data", "GROSSTOTALPAYMENTS-DATA", FieldType.STRING, 
            15);
        common_Xml_Grosstotalpayments_Close = common_Xml.newFieldInGroup("common_Xml_Grosstotalpayments_Close", "GROSSTOTALPAYMENTS-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Nettotalpayments_Open = common_Xml.newFieldInGroup("common_Xml_Nettotalpayments_Open", "NETTOTALPAYMENTS-OPEN", FieldType.STRING, 20);
        common_Xml_Nettotalpayments_Data = common_Xml.newFieldInGroup("common_Xml_Nettotalpayments_Data", "NETTOTALPAYMENTS-DATA", FieldType.STRING, 15);
        common_Xml_Nettotalpayments_Close = common_Xml.newFieldInGroup("common_Xml_Nettotalpayments_Close", "NETTOTALPAYMENTS-CLOSE", FieldType.STRING, 
            20);
        common_Xml_Contractsorcertificates_Open = common_Xml.newFieldInGroup("common_Xml_Contractsorcertificates_Open", "CONTRACTSORCERTIFICATES-OPEN", 
            FieldType.STRING, 30);
        common_Xml_Contractsorcertificates_Data = common_Xml.newFieldInGroup("common_Xml_Contractsorcertificates_Data", "CONTRACTSORCERTIFICATES-DATA", 
            FieldType.STRING, 35);
        common_Xml_Contractsorcertificates_Close = common_Xml.newFieldInGroup("common_Xml_Contractsorcertificates_Close", "CONTRACTSORCERTIFICATES-CLOSE", 
            FieldType.STRING, 30);
        common_Xml_Planname_Open = common_Xml.newFieldInGroup("common_Xml_Planname_Open", "PLANNAME-OPEN", FieldType.STRING, 15);
        common_Xml_Planname_Data = common_Xml.newFieldInGroup("common_Xml_Planname_Data", "PLANNAME-DATA", FieldType.STRING, 100);
        common_Xml_Planname_Close = common_Xml.newFieldInGroup("common_Xml_Planname_Close", "PLANNAME-CLOSE", FieldType.STRING, 15);
        common_Xml_Processingdate_Open = common_Xml.newFieldInGroup("common_Xml_Processingdate_Open", "PROCESSINGDATE-OPEN", FieldType.STRING, 20);
        common_Xml_Processingdate_Data = common_Xml.newFieldInGroup("common_Xml_Processingdate_Data", "PROCESSINGDATE-DATA", FieldType.STRING, 10);
        common_Xml_Processingdate_Close = common_Xml.newFieldInGroup("common_Xml_Processingdate_Close", "PROCESSINGDATE-CLOSE", FieldType.STRING, 20);
        common_Xml_Accountsinformation_Open = common_Xml.newFieldInGroup("common_Xml_Accountsinformation_Open", "ACCOUNTSINFORMATION-OPEN", FieldType.STRING, 
            25);
        common_Xml_Accountsinformation_Close = common_Xml.newFieldInGroup("common_Xml_Accountsinformation_Close", "ACCOUNTSINFORMATION-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Disclaimercodes_Open = common_Xml.newFieldInGroup("common_Xml_Disclaimercodes_Open", "DISCLAIMERCODES-OPEN", FieldType.STRING, 20);
        common_Xml_Disclaimercodes_Close = common_Xml.newFieldInGroup("common_Xml_Disclaimercodes_Close", "DISCLAIMERCODES-CLOSE", FieldType.STRING, 20);
        common_Xml_Disclaimercodestype_Open = common_Xml.newFieldInGroup("common_Xml_Disclaimercodestype_Open", "DISCLAIMERCODESTYPE-OPEN", FieldType.STRING, 
            25);
        common_Xml_Disclaimercodestype_Data = common_Xml.newFieldInGroup("common_Xml_Disclaimercodestype_Data", "DISCLAIMERCODESTYPE-DATA", FieldType.STRING, 
            6);
        common_Xml_Disclaimercodestype_Close = common_Xml.newFieldInGroup("common_Xml_Disclaimercodestype_Close", "DISCLAIMERCODESTYPE-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Decedentname_Open = common_Xml.newFieldInGroup("common_Xml_Decedentname_Open", "DECEDENTNAME-OPEN", FieldType.STRING, 30);
        common_Xml_Decedentname_Data = common_Xml.newFieldInGroup("common_Xml_Decedentname_Data", "DECEDENTNAME-DATA", FieldType.STRING, 100);
        common_Xml_Decedentname_Close = common_Xml.newFieldInGroup("common_Xml_Decedentname_Close", "DECEDENTNAME-CLOSE", FieldType.STRING, 30);
        common_Xml_Paymentlabel_Open = common_Xml.newFieldInGroup("common_Xml_Paymentlabel_Open", "PAYMENTLABEL-OPEN", FieldType.STRING, 25);
        common_Xml_Paymentlabel_Data = common_Xml.newFieldInGroup("common_Xml_Paymentlabel_Data", "PAYMENTLABEL-DATA", FieldType.STRING, 100);
        common_Xml_Paymentlabel_Close = common_Xml.newFieldInGroup("common_Xml_Paymentlabel_Close", "PAYMENTLABEL-CLOSE", FieldType.STRING, 25);
        common_Xml_Contractlabel_Open = common_Xml.newFieldInGroup("common_Xml_Contractlabel_Open", "CONTRACTLABEL-OPEN", FieldType.STRING, 25);
        common_Xml_Contractlabel_Data = common_Xml.newFieldInGroup("common_Xml_Contractlabel_Data", "CONTRACTLABEL-DATA", FieldType.STRING, 100);
        common_Xml_Contractlabel_Close = common_Xml.newFieldInGroup("common_Xml_Contractlabel_Close", "CONTRACTLABEL-CLOSE", FieldType.STRING, 25);
        common_Xml_Paymentdate_Open = common_Xml.newFieldInGroup("common_Xml_Paymentdate_Open", "PAYMENTDATE-OPEN", FieldType.STRING, 20);
        common_Xml_Paymentdate_Data = common_Xml.newFieldInGroup("common_Xml_Paymentdate_Data", "PAYMENTDATE-DATA", FieldType.STRING, 10);
        common_Xml_Paymentdate_Close = common_Xml.newFieldInGroup("common_Xml_Paymentdate_Close", "PAYMENTDATE-CLOSE", FieldType.STRING, 20);
        common_Xml_Totalinterest_Open = common_Xml.newFieldInGroup("common_Xml_Totalinterest_Open", "TOTALINTEREST-OPEN", FieldType.STRING, 25);
        common_Xml_Totalinterest_Data = common_Xml.newFieldInGroup("common_Xml_Totalinterest_Data", "TOTALINTEREST-DATA", FieldType.STRING, 100);
        common_Xml_Totalinterest_Close = common_Xml.newFieldInGroup("common_Xml_Totalinterest_Close", "TOTALINTEREST-CLOSE", FieldType.STRING, 25);
        common_Xml_Totalnetpaymentamount_Open = common_Xml.newFieldInGroup("common_Xml_Totalnetpaymentamount_Open", "TOTALNETPAYMENTAMOUNT-OPEN", FieldType.STRING, 
            30);
        common_Xml_Totalnetpaymentamount_Data = common_Xml.newFieldInGroup("common_Xml_Totalnetpaymentamount_Data", "TOTALNETPAYMENTAMOUNT-DATA", FieldType.STRING, 
            15);
        common_Xml_Totalnetpaymentamount_Close = common_Xml.newFieldInGroup("common_Xml_Totalnetpaymentamount_Close", "TOTALNETPAYMENTAMOUNT-CLOSE", FieldType.STRING, 
            30);
        common_Xml_Totalpaymentamount_Open = common_Xml.newFieldInGroup("common_Xml_Totalpaymentamount_Open", "TOTALPAYMENTAMOUNT-OPEN", FieldType.STRING, 
            25);
        common_Xml_Totalpaymentamount_Data = common_Xml.newFieldInGroup("common_Xml_Totalpaymentamount_Data", "TOTALPAYMENTAMOUNT-DATA", FieldType.STRING, 
            15);
        common_Xml_Totalpaymentamount_Close = common_Xml.newFieldInGroup("common_Xml_Totalpaymentamount_Close", "TOTALPAYMENTAMOUNT-CLOSE", FieldType.STRING, 
            25);
        common_Xml_Interestind_Open = common_Xml.newFieldInGroup("common_Xml_Interestind_Open", "INTERESTIND-OPEN", FieldType.STRING, 15);
        common_Xml_Interestind_Data = common_Xml.newFieldInGroup("common_Xml_Interestind_Data", "INTERESTIND-DATA", FieldType.STRING, 1);
        common_Xml_Interestind_Close = common_Xml.newFieldInGroup("common_Xml_Interestind_Close", "INTERESTIND-CLOSE", FieldType.STRING, 15);
        common_Xml_Deductionamountind_Open = common_Xml.newFieldInGroup("common_Xml_Deductionamountind_Open", "DEDUCTIONAMOUNTIND-OPEN", FieldType.STRING, 
            25);
        common_Xml_Deductionamountind_Data = common_Xml.newFieldInGroup("common_Xml_Deductionamountind_Data", "DEDUCTIONAMOUNTIND-DATA", FieldType.STRING, 
            1);
        common_Xml_Deductionamountind_Close = common_Xml.newFieldInGroup("common_Xml_Deductionamountind_Close", "DEDUCTIONAMOUNTIND-CLOSE", FieldType.STRING, 
            25);

        this.setRecordName("LdaFcpl961");
    }

    public void initializeValues() throws Exception
    {
        reset();
        common_Xml_Xml_Header.setInitialValue("?xml version=\"1.0\" encoding=\"UTF-8\" ?");
        common_Xml_Documentrequests_Open.setInitialValue("DocumentRequests xmlns=\"http://dcs.tiaa.org/document-request-v1/document/types\"");
        common_Xml_Documentrequests_Close.setInitialValue("/DocumentRequests");
        common_Xml_Documentrequest_Open.setInitialValue("DocumentRequest");
        common_Xml_Documentrequest_Close.setInitialValue("/DocumentRequest");
        common_Xml_Requestdatetime_Open.setInitialValue("RequestDateTime");
        common_Xml_Requestdatetime_Close.setInitialValue("/RequestDateTime");
        common_Xml_Batchind_Open.setInitialValue("<BatchInd>Y");
        common_Xml_Batchind_Close.setInitialValue("/BatchInd");
        common_Xml_Batchcounter_Open.setInitialValue("BatchCounter");
        common_Xml_Batchcounter_Close.setInitialValue("/BatchCounter");
        common_Xml_Batchtotal_Open.setInitialValue("BatchTotal");
        common_Xml_Batchtotal_Close.setInitialValue("/BatchTotal");
        common_Xml_Mailiteminfo_Open.setInitialValue("MailItemInfo");
        common_Xml_Mailiteminfo_Close.setInitialValue("/MailItemInfo");
        common_Xml_Applicationid_Open.setInitialValue("<ApplicationId>PROLINECHECKS");
        common_Xml_Applicationid_Close.setInitialValue("/ApplicationId");
        common_Xml_Universalid_Open.setInitialValue("UniversalId");
        common_Xml_Universalid_Close.setInitialValue("/UniversalId");
        common_Xml_Universaltype_Open.setInitialValue("UniversalType");
        common_Xml_Universaltype_Close.setInitialValue("/UniversalType");
        common_Xml_Documentrequestid_Open.setInitialValue("DocumentRequestId");
        common_Xml_Documentrequestid_Close.setInitialValue("/DocumentRequestId");
        common_Xml_Printerid_Open.setInitialValue("PrinterId");
        common_Xml_Printerid_Close.setInitialValue("/PrinterId");
        common_Xml_Fullname_Open.setInitialValue("FullName");
        common_Xml_Fullname_Close.setInitialValue("/FullName");
        common_Xml_Addresstypecode_Open.setInitialValue("AddressTypeCode");
        common_Xml_Addresstypecode_Close.setInitialValue("/AddressTypeCode");
        common_Xml_Letterdate_Open.setInitialValue("LetterDate");
        common_Xml_Letterdate_Close.setInitialValue("/LetterDate");
        common_Xml_Deliverytype_Open.setInitialValue("DeliveryType");
        common_Xml_Deliverytype_Close.setInitialValue("/DeliveryType");
        common_Xml_Expagind_Open.setInitialValue("EXPAGInd");
        common_Xml_Expagind_Close.setInitialValue("/EXPAGInd");
        common_Xml_Archivalind_Open.setInitialValue("ArchivalInd");
        common_Xml_Archivalind_Data.setInitialValue("I");
        common_Xml_Archivalind_Close.setInitialValue("/ArchivalInd");
        common_Xml_Planid_Open.setInitialValue("PlanId");
        common_Xml_Planid_Close.setInitialValue("/PlanId");
        common_Xml_Businessdate_Open.setInitialValue("BusinessDate");
        common_Xml_Businessdate_Close.setInitialValue("/BusinessDate");
        common_Xml_Addresslines_Open.setInitialValue("AddressLines");
        common_Xml_Addresslines_Close.setInitialValue("/AddressLines");
        common_Xml_Addressline_Open.setInitialValue("AddressLine");
        common_Xml_Addressline_Close.setInitialValue("/AddressLine");
        common_Xml_Documentinfo_Open.setInitialValue("DocumentInfoXml");
        common_Xml_Documentinfo_Close.setInitialValue("/DocumentInfoXml");
        common_Xml_Prolinechecks_Open.setInitialValue("ProlineChecks");
        common_Xml_Prolinechecks_Close.setInitialValue("/ProlineChecks");
        common_Xml_Headerinfo_Open.setInitialValue("HeaderInfo");
        common_Xml_Headerinfo_Close.setInitialValue("/HeaderInfo");
        common_Xml_Settlementsystem_Open.setInitialValue("SettlementSystem");
        common_Xml_Settlementsystem_Close.setInitialValue("/SettlementSystem");
        common_Xml_Checkeftind_Open.setInitialValue("CheckEFTInd");
        common_Xml_Checkeftind_Close.setInitialValue("/CheckEFTInd");
        common_Xml_Holdcode_Open.setInitialValue("HoldCode");
        common_Xml_Holdcode_Close.setInitialValue("/HoldCode");
        common_Xml_Tiaaaddress_Open.setInitialValue("TiaaAddress");
        common_Xml_Tiaaaddress_Close.setInitialValue("/TiaaAddress");
        common_Xml_Tiaaaddressline_Open.setInitialValue("TiaaAddressLine");
        common_Xml_Tiaaaddressline_Data.getValue(1,1).setInitialValue("P.O. Box 1281");
        common_Xml_Tiaaaddressline_Data.getValue(1,2).setInitialValue("Charlotte, NC 28201-1281");
        common_Xml_Tiaaaddressline_Data.getValue(2,1).setInitialValue("730 Third Avenue");
        common_Xml_Tiaaaddressline_Data.getValue(2,2).setInitialValue("New York, NY 10017-3206");
        common_Xml_Tiaaaddressline_Close.setInitialValue("/TiaaAddressLine");
        common_Xml_Nonids_Index.setInitialValue(1);
        common_Xml_Ids_Index.setInitialValue(2);
        common_Xml_Tiaaphonenumber_Open.setInitialValue("TiaaPhoneNumber");
        common_Xml_Tiaaphonenumber_Data.getValue(1).setInitialValue("800-842-2252");
        common_Xml_Tiaaphonenumber_Data.getValue(2).setInitialValue("1-800-842-2733");
        common_Xml_Tiaaphonenumber_Close.setInitialValue("/TiaaPhoneNumber");
        common_Xml_Bankname_Open.setInitialValue("BankName");
        common_Xml_Bankname_Close.setInitialValue("/BankName");
        common_Xml_Bankaddress_Open.setInitialValue("BankAddress");
        common_Xml_Bankaddress_Close.setInitialValue("/BankAddress");
        common_Xml_Bankaddressline_Open.setInitialValue("BankAddressLine");
        common_Xml_Bankaddressline_Close.setInitialValue("/BankAddressLine");
        common_Xml_Bankfractional1_Open.setInitialValue("BankFractional1");
        common_Xml_Bankfractional1_Close.setInitialValue("/BankFractional1");
        common_Xml_Bankfractional2_Open.setInitialValue("BankFractional2");
        common_Xml_Bankfractional2_Close.setInitialValue("/BankFractional2");
        common_Xml_Bankroutingnumber_Open.setInitialValue("BankRoutingNumber");
        common_Xml_Bankroutingnumber_Close.setInitialValue("/BankRoutingNumber");
        common_Xml_Bankaccountnumber_Open.setInitialValue("BankAccountNumber");
        common_Xml_Bankaccountnumber_Close.setInitialValue("/BankAccountNumber");
        common_Xml_Checknumber_Open.setInitialValue("CheckNumber");
        common_Xml_Checknumber_Close.setInitialValue("/CheckNumber");
        common_Xml_Checkdate_Open.setInitialValue("CheckDate");
        common_Xml_Checkdate_Close.setInitialValue("/CheckDate");
        common_Xml_Checkamount_Open.setInitialValue("CheckAmount");
        common_Xml_Checkamount_Close.setInitialValue("/CheckAmount");
        common_Xml_Accountname_Open.setInitialValue("AccountName");
        common_Xml_Accountname_Close.setInitialValue("/AccountName");
        common_Xml_Signatorytitle_Open.setInitialValue("SignatoryTitle");
        common_Xml_Signatorytitle_Data.setInitialValue("Vice President and Treasurer");
        common_Xml_Signatorytitle_Close.setInitialValue("/SignatoryTitle");
        common_Xml_Settlementsystemcodes_Open.setInitialValue("SettlementSystemCodes");
        common_Xml_Settlementsystemcodes_Data.setInitialValue("OP");
        common_Xml_Settlementsystemcodes_Close.setInitialValue("/SettlementSystemCodes");
        common_Xml_Nonidsinfo_Open.setInitialValue("NonIDSInfo");
        common_Xml_Nonidsinfo_Close.setInitialValue("/NonIDSInfo");
        common_Xml_Participantname_Open.setInitialValue("ParticipantName");
        common_Xml_Participantname_Close.setInitialValue("/ParticipantName");
        common_Xml_Tiaacontract_Open.setInitialValue("TIAAContract");
        common_Xml_Tiaacontract_Close.setInitialValue("/TIAAContract");
        common_Xml_Crefcontract_Open.setInitialValue("CREFContract");
        common_Xml_Crefcontract_Close.setInitialValue("/CREFContract");
        common_Xml_Grossamount_Open.setInitialValue("GrossAmount");
        common_Xml_Grossamount_Close.setInitialValue("/GrossAmount");
        common_Xml_Totaltaxwitheld_Open.setInitialValue("TotalTaxWitheld");
        common_Xml_Totaltaxwitheld_Close.setInitialValue("/TotalTaxWitheld");
        common_Xml_Taxeswithheld_Open.setInitialValue("TaxesWithheld");
        common_Xml_Taxeswithheld_Close.setInitialValue("/TaxesWithheld");
        common_Xml_Taxeswithheldinfo_Open.setInitialValue("TaxesWithheldInfo");
        common_Xml_Taxeswithheldinfo_Close.setInitialValue("/TaxesWithheldInfo");
        common_Xml_Taxdescription_Open.setInitialValue("TaxDescription");
        common_Xml_Taxdescription_Close.setInitialValue("/TaxDescription");
        common_Xml_Taxamount_Open.setInitialValue("TaxAmount");
        common_Xml_Taxamount_Close.setInitialValue("/TaxAmount");
        common_Xml_Totaltaxwithheld_Open.setInitialValue("TotalTaxWithheld");
        common_Xml_Totaltaxwithheld_Close.setInitialValue("/TotalTaxWithheld");
        common_Xml_Otherdeductiondesc_Open.setInitialValue("OtherDeductionDesc");
        common_Xml_Otherdeductiondesc_Data.setInitialValue("Other Deductions");
        common_Xml_Otherdeductiondesc_Close.setInitialValue("/OtherDeductionDesc");
        common_Xml_Otherdeductionamount_Open.setInitialValue("OtherDeductionAmount");
        common_Xml_Otherdeductionamount_Close.setInitialValue("/OtherDeductionAmount");
        common_Xml_Netamount_Open.setInitialValue("NetAmount");
        common_Xml_Netamount_Close.setInitialValue("/NetAmount");
        common_Xml_Aftertaxdesc_Open.setInitialValue("AfterTaxDesc");
        common_Xml_Aftertaxdesc_Close.setInitialValue("/AfterTaxDesc");
        common_Xml_Aftertaxamount_Open.setInitialValue("AfterTaxAmount");
        common_Xml_Aftertaxamount_Close.setInitialValue("/AfterTaxAmount");
        common_Xml_Memofield_Open.setInitialValue("MemoField");
        common_Xml_Memofield_Close.setInitialValue("/MemoField");
        common_Xml_Memofieldext_Open.setInitialValue("MemoFieldExt");
        common_Xml_Memofieldext_Close.setInitialValue("/MemoFieldExt");
        common_Xml_Idsinfo_Open.setInitialValue("IDSInfo");
        common_Xml_Idsinfo_Close.setInitialValue("/IDSInfo");
        common_Xml_Disbursementdescription_Open.setInitialValue("DisbursementDescription");
        common_Xml_Disbursementdescription_Close.setInitialValue("/DisbursementDescription");
        common_Xml_Namelabel_Open.setInitialValue("NameLabel");
        common_Xml_Namelabel_Close.setInitialValue("/NameLabel");
        common_Xml_Insuredname_Open.setInitialValue("InsuredName");
        common_Xml_Insuredname_Close.setInitialValue("/InsuredName");
        common_Xml_Documentlabel1_Open.setInitialValue("DocumentLabel1");
        common_Xml_Documentlabel1_Close.setInitialValue("/DocumentLabel1");
        common_Xml_Documentlabel2_Open.setInitialValue("DocumentLabel2");
        common_Xml_Documentlabel2_Close.setInitialValue("/DocumentLabel2");
        common_Xml_Documentnumber_Open.setInitialValue("DocumentNumber");
        common_Xml_Documentnumber_Close.setInitialValue("/DocumentNumber");
        common_Xml_Comments_Open.setInitialValue("Comments");
        common_Xml_Comments_Close.setInitialValue("/Comments");
        common_Xml_Commentline_Open.setInitialValue("CommentLine");
        common_Xml_Commentline_Close.setInitialValue("/CommentLine");
        common_Xml_Applicationid2_Open.setInitialValue("ApplicationId");
        common_Xml_Applicationid2_Close.setInitialValue("/ApplicationId");
        common_Xml_Initialpaymentfrequency_Open.setInitialValue("InitialPaymentFrequency");
        common_Xml_Initialpaymentfrequency_Close.setInitialValue("/InitialPaymentFrequency");
        common_Xml_Typeofannuity_Open.setInitialValue("TypeOfAnnuity");
        common_Xml_Typeofannuity_Close.setInitialValue("/TypeOfAnnuity");
        common_Xml_Tiaacreflist_Open.setInitialValue("TIAACREFList");
        common_Xml_Tiaacreflist_Close.setInitialValue("/TIAACREFList");
        common_Xml_Settlementlist_Open.setInitialValue("SettlementList");
        common_Xml_Settlementlist_Close.setInitialValue("/SettlementList");
        common_Xml_Tiaacrefnumber_Open.setInitialValue("TIAACREFNumber");
        common_Xml_Tiaacrefnumber_Close.setInitialValue("/TIAACREFNumber");
        common_Xml_Tiaacrefind_Open.setInitialValue("TiaaCrefIND");
        common_Xml_Tiaacrefind_Close.setInitialValue("/TiaaCrefIND");
        common_Xml_Annuitystartdate_Open.setInitialValue("AnnuityStartDate");
        common_Xml_Annuitystartdate_Close.setInitialValue("/AnnuityStartDate");
        common_Xml_Paymentdetail_Open.setInitialValue("PaymentDetail");
        common_Xml_Paymentdetail_Close.setInitialValue("/PaymentDetail");
        common_Xml_Paymentinfo_Open.setInitialValue("PaymentInformation");
        common_Xml_Paymentinfo_Close.setInitialValue("/PaymentInformation");
        common_Xml_Duedate_Open.setInitialValue("DueDate");
        common_Xml_Duedate_Close.setInitialValue("/DueDate");
        common_Xml_Duedatefuture_Open.setInitialValue("DueDateFuture");
        common_Xml_Duedatefuture_Close.setInitialValue("/DueDateFuture");
        common_Xml_Description_Open.setInitialValue("Description");
        common_Xml_Description_Close.setInitialValue("/Description");
        common_Xml_Deductionamountfrequency_Open.setInitialValue("DeductionAmountFrequency");
        common_Xml_Deductionamountfrequency_Close.setInitialValue("/DeductionAmountFrequency");
        common_Xml_Paymentamount_Open.setInitialValue("PaymentAmount");
        common_Xml_Paymentamount_Close.setInitialValue("/PaymentAmount");
        common_Xml_Paymentfrequency_Open.setInitialValue("PaymentFrequency");
        common_Xml_Paymentfrequency_Close.setInitialValue("/PaymentFrequency");
        common_Xml_Unit_Open.setInitialValue("Unit");
        common_Xml_Unit_Close.setInitialValue("/Unit");
        common_Xml_Unitvalue_Open.setInitialValue("UnitValue");
        common_Xml_Unitvalue_Close.setInitialValue("/UnitValue");
        common_Xml_Contractual_Open.setInitialValue("Contractual");
        common_Xml_Contractual_Close.setInitialValue("/Contractual");
        common_Xml_Dividend_Open.setInitialValue("Dividend");
        common_Xml_Dividend_Close.setInitialValue("/Dividend");
        common_Xml_Tiaacontractpayment_Open.setInitialValue("TIAAContractPayment");
        common_Xml_Tiaacontractpayment_Close.setInitialValue("/TIAAContractPayment");
        common_Xml_Crefcertificate_Open.setInitialValue("CREFCertificate");
        common_Xml_Crefcertificate_Close.setInitialValue("/CREFCertificate");
        common_Xml_Paymentamountfrom_Open.setInitialValue("PaymentAmountFrom");
        common_Xml_Paymentamountfrom_Close.setInitialValue("/PaymentAmountFrom");
        common_Xml_Paymentamountfromfrequency_Open.setInitialValue("PaymentAmountFromFrequency");
        common_Xml_Paymentamountfromfrequency_Close.setInitialValue("/PaymentAmountFromFrequency");
        common_Xml_Interest_Open.setInitialValue("Interest");
        common_Xml_Interest_Close.setInitialValue("/Interest");
        common_Xml_Interestpayment_Open.setInitialValue("InterestPayment");
        common_Xml_Interestpayment_Close.setInitialValue("/InterestPayment");
        common_Xml_Netpaymentamount_Open.setInitialValue("NetPaymentAmount");
        common_Xml_Netpaymentamount_Close.setInitialValue("/NetPaymentAmount");
        common_Xml_Subpaymentdetail_Open.setInitialValue("SubPaymentDetail");
        common_Xml_Subpaymentdetail_Close.setInitialValue("/SubPaymentDetail");
        common_Xml_Subdate_Open.setInitialValue("SubDate");
        common_Xml_Subdate_Close.setInitialValue("/SubDate");
        common_Xml_Subtiaacontract_Open.setInitialValue("SubTIAAContract");
        common_Xml_Subtiaacontract_Close.setInitialValue("/SubTIAAContract");
        common_Xml_Subcrefcertificate_Open.setInitialValue("SubCREFCertificate");
        common_Xml_Subcrefcertificate_Close.setInitialValue("/SubCREFCertificate");
        common_Xml_Subpaymentfrom_Open.setInitialValue("SubPaymentFrom");
        common_Xml_Subpaymentfrom_Close.setInitialValue("/SubPaymentFrom");
        common_Xml_Subpaymentfromfrequency_Open.setInitialValue("SubPaymentFromFrequency");
        common_Xml_Subpaymentfromfrequency_Close.setInitialValue("/SubPaymentFromFrequency");
        common_Xml_Subpaymentamount_Open.setInitialValue("SubPaymentAmount");
        common_Xml_Subpaymentamount_Close.setInitialValue("/SubPaymentAmount");
        common_Xml_Subpaymentamountfrequency_Open.setInitialValue("SubPaymentAmountFrequency");
        common_Xml_Subpaymentamountfrequency_Close.setInitialValue("/SubPaymentAmountFrequency");
        common_Xml_Subdeductionamount_Open.setInitialValue("SubDeductionAmount");
        common_Xml_Subdeductionamount_Close.setInitialValue("/SubDeductionAmount");
        common_Xml_Subdeductionamountfrequency_Open.setInitialValue("SubDeductionAmountFrequency");
        common_Xml_Subdeductionamountfrequency_Clos.setInitialValue("/SubDeductionAmountFrequency");
        common_Xml_Totaldeductionamount_Open.setInitialValue("TotalDeductionAmount");
        common_Xml_Totaldeductionamount_Close.setInitialValue("/TotalDeductionAmount");
        common_Xml_Deductions_Open.setInitialValue("Deductions");
        common_Xml_Deductions_Close.setInitialValue("/Deductions");
        common_Xml_Deductionsinfo_Open.setInitialValue("DeductionsInformation");
        common_Xml_Deductionsinfo_Close.setInitialValue("/DeductionsInformation");
        common_Xml_Deductiondescription_Open.setInitialValue("DeductionDescription");
        common_Xml_Deductiondescription_Close.setInitialValue("/DeductionDescription");
        common_Xml_Deductionamount_Open.setInitialValue("DeductionAmount");
        common_Xml_Deductionamount_Close.setInitialValue("/DeductionAmount");
        common_Xml_Institutionname_Open.setInitialValue("InstitutionName");
        common_Xml_Institutionname_Close.setInitialValue("/InstitutionName");
        common_Xml_Institutionaddress_Open.setInitialValue("InstitutionAddress");
        common_Xml_Institutionaddress_Close.setInitialValue("/InstitutionAddress");
        common_Xml_Institutionaddress_Lines_Open.setInitialValue("InstitutionAddressLines");
        common_Xml_Institutionaddress_Lines_Close.setInitialValue("/InstitutionAddressLines");
        common_Xml_Institutioninfo_Open.setInitialValue("InstitutionInformation");
        common_Xml_Institutioninfo_Close.setInitialValue("/InstitutionInformation");
        common_Xml_Institutioninfoline_Open.setInitialValue("InstitutionInfoLine");
        common_Xml_Institutioninfoline_Data.getValue(1).setInitialValue("As instructed, we have sent the payment to theaddress below:");
        common_Xml_Institutioninfoline_Close.setInitialValue("/InstitutionInfoLine");
        common_Xml_Participantinstitutionind_Open.setInitialValue("ParticipantInstitutionInd");
        common_Xml_Participantinstitutionind_Close.setInitialValue("/ParticipantInstitutionInd");
        common_Xml_Omnipay_Open.setInitialValue("OMNIPay");
        common_Xml_Omnipay_Close.setInitialValue("/OMNIPay");
        common_Xml_Documenttype_Open.setInitialValue("DocumentType");
        common_Xml_Documenttype_Close.setInitialValue("/DocumentType");
        common_Xml_Participantinstitutionflag_Open.setInitialValue("ParticipantInstitutionFlag");
        common_Xml_Participantinstitutionflag_Clos.setInitialValue("/ParticipantInstitutionFlag");
        common_Xml_Paymentfor_Open.setInitialValue("PaymentFor");
        common_Xml_Paymentfor_Close.setInitialValue("/PaymentFor");
        common_Xml_Tiaanumber_Open.setInitialValue("TiaaNumber");
        common_Xml_Tiaanumber_Close.setInitialValue("/TiaaNumber");
        common_Xml_Crefnumber_Open.setInitialValue("CrefNumber");
        common_Xml_Crefnumber_Close.setInitialValue("/CrefNumber");
        common_Xml_Effectivedate_Open.setInitialValue("EffectiveDate");
        common_Xml_Effectivedate_Close.setInitialValue("/EffectiveDate");
        common_Xml_Forparticipant_Open.setInitialValue("ForParticipant");
        common_Xml_Forparticipant_Close.setInitialValue("/ForParticipant");
        common_Xml_Foralternatecarrier_Open.setInitialValue("ForAlternateCarrier");
        common_Xml_Foralternatecarrier_Close.setInitialValue("/ForAlternateCarrier");
        common_Xml_Paymentinformationsummary_Open.setInitialValue("PaymentInformationSummary");
        common_Xml_Paymentinformationsummary_Close.setInitialValue("/PaymentInformationSummary");
        common_Xml_Paymentdetailinformation_Open.setInitialValue("PaymentDetailInformation");
        common_Xml_Paymentdetailinformation_Close.setInitialValue("/PaymentDetailInformation");
        common_Xml_Accountnumber_Open.setInitialValue("AccountNumber");
        common_Xml_Accountnumber_Close.setInitialValue("/AccountNumber");
        common_Xml_Transferaccountslist_Open.setInitialValue("TransferAccountsList");
        common_Xml_Transferaccountslist_Close.setInitialValue("/TransferAccountsList");
        common_Xml_Transferaccounts_Open.setInitialValue("TransferAccounts");
        common_Xml_Transferaccounts_Close.setInitialValue("/TransferAccounts");
        common_Xml_Employer_Open.setInitialValue("Employer");
        common_Xml_Employer_Close.setInitialValue("/Employer");
        common_Xml_Typeofplan_Open.setInitialValue("TypeOfPlan");
        common_Xml_Typeofplan_Close.setInitialValue("/TypeOfPlan");
        common_Xml_Cashavailable_Open.setInitialValue("CashAvailable");
        common_Xml_Cashavailable_Close.setInitialValue("/CashAvailable");
        common_Xml_Distributionrequirements_Open.setInitialValue("DistributionRequirements");
        common_Xml_Distributionrequirements_Close.setInitialValue("/DistributionRequirements");
        common_Xml_Paymentinformation_Open.setInitialValue("TransferAccounts");
        common_Xml_Paymentinformation_Close.setInitialValue("/TransferAccounts");
        common_Xml_Deductionstype_Open.setInitialValue("DeductionsType");
        common_Xml_Deductionstype_Close.setInitialValue("/DeductionsType");
        common_Xml_Grosstotaldeductions_Open.setInitialValue("GrossTotalDeductions");
        common_Xml_Grosstotaldeductions_Close.setInitialValue("/GrossTotalDeductions");
        common_Xml_Grosstotalpayments_Open.setInitialValue("GrossTotalPayments");
        common_Xml_Grosstotalpayments_Close.setInitialValue("/GrossTotalPayments");
        common_Xml_Nettotalpayments_Open.setInitialValue("NetTotalPayments");
        common_Xml_Nettotalpayments_Close.setInitialValue("/NetTotalPayments");
        common_Xml_Contractsorcertificates_Open.setInitialValue("TransferAccounts");
        common_Xml_Contractsorcertificates_Close.setInitialValue("/TransferAccounts");
        common_Xml_Planname_Open.setInitialValue("PlanName");
        common_Xml_Planname_Close.setInitialValue("/PlanName");
        common_Xml_Processingdate_Open.setInitialValue("ProcessingDate");
        common_Xml_Processingdate_Close.setInitialValue("/ProcessingDate");
        common_Xml_Accountsinformation_Open.setInitialValue("AccountsInformation");
        common_Xml_Accountsinformation_Close.setInitialValue("/AccountsInformation");
        common_Xml_Disclaimercodes_Open.setInitialValue("DisclaimerCodes");
        common_Xml_Disclaimercodes_Close.setInitialValue("/DisclaimerCodes");
        common_Xml_Disclaimercodestype_Open.setInitialValue("PlanName");
        common_Xml_Disclaimercodestype_Close.setInitialValue("/PlanName");
        common_Xml_Decedentname_Open.setInitialValue("DecedentName");
        common_Xml_Decedentname_Close.setInitialValue("/DecedentName");
        common_Xml_Paymentlabel_Open.setInitialValue("PaymentLabel");
        common_Xml_Paymentlabel_Close.setInitialValue("/PaymentLabel");
        common_Xml_Contractlabel_Open.setInitialValue("ContractLabel");
        common_Xml_Contractlabel_Close.setInitialValue("/ContractLabel");
        common_Xml_Paymentdate_Open.setInitialValue("PaymentDate");
        common_Xml_Paymentdate_Close.setInitialValue("/PaymentDate");
        common_Xml_Totalinterest_Open.setInitialValue("TotalInterest");
        common_Xml_Totalinterest_Close.setInitialValue("/TotalInterest");
        common_Xml_Totalnetpaymentamount_Open.setInitialValue("TotalNetPaymentAmount");
        common_Xml_Totalnetpaymentamount_Close.setInitialValue("/TotalNetPaymentAmount");
        common_Xml_Totalpaymentamount_Open.setInitialValue("TotalPaymentAmount");
        common_Xml_Totalpaymentamount_Close.setInitialValue("/TotalPaymentAmount");
        common_Xml_Interestind_Open.setInitialValue("InterestInd");
        common_Xml_Interestind_Close.setInitialValue("/InterestInd");
        common_Xml_Deductionamountind_Open.setInitialValue("DeductionAmountInd");
        common_Xml_Deductionamountind_Close.setInitialValue("/DeductionAmountInd");
    }

    // Constructor
    public LdaFcpl961() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
