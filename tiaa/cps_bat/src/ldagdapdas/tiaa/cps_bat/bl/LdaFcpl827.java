/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:17 PM
**        * FROM NATURAL LDA     : FCPL827
************************************************************
**        * FILE NAME            : LdaFcpl827.java
**        * CLASS NAME           : LdaFcpl827
**        * INSTANCE NAME        : LdaFcpl827
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl827 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Lgr_Acct_Rpt;
    private DbsField pnd_Lgr_Acct_Rpt_Cntrct_Orgn_Cde;
    private DbsField pnd_Lgr_Acct_Rpt_Pymnt_Check_Dte;
    private DbsField pnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Lgr_Acct_Rpt_Cntrct_Insurance_Option;
    private DbsField pnd_Lgr_Acct_Rpt_Cntrct_Life_Contingency;
    private DbsField pnd_Lgr_Acct_Rpt_Cntrct_Company_Cde;
    private DbsField pnd_Lgr_Acct_Rpt_Acct_Company;
    private DbsField pnd_Lgr_Acct_Rpt_Inv_Acct_Cde;
    private DbsGroup pnd_Lgr_Acct_Rpt_Inv_Acct_CdeRedef1;
    private DbsField pnd_Lgr_Acct_Rpt_Inv_Acct_Cde_X;
    private DbsField pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Ind;
    private DbsField pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Nbr;
    private DbsField pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Amt;

    public DbsGroup getPnd_Lgr_Acct_Rpt() { return pnd_Lgr_Acct_Rpt; }

    public DbsField getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_Cde() { return pnd_Lgr_Acct_Rpt_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Lgr_Acct_Rpt_Pymnt_Check_Dte() { return pnd_Lgr_Acct_Rpt_Pymnt_Check_Dte; }

    public DbsField getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_Type() { return pnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_Type; }

    public DbsField getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_Cde() { return pnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_Cde; }

    public DbsField getPnd_Lgr_Acct_Rpt_Cntrct_Insurance_Option() { return pnd_Lgr_Acct_Rpt_Cntrct_Insurance_Option; }

    public DbsField getPnd_Lgr_Acct_Rpt_Cntrct_Life_Contingency() { return pnd_Lgr_Acct_Rpt_Cntrct_Life_Contingency; }

    public DbsField getPnd_Lgr_Acct_Rpt_Cntrct_Company_Cde() { return pnd_Lgr_Acct_Rpt_Cntrct_Company_Cde; }

    public DbsField getPnd_Lgr_Acct_Rpt_Acct_Company() { return pnd_Lgr_Acct_Rpt_Acct_Company; }

    public DbsField getPnd_Lgr_Acct_Rpt_Inv_Acct_Cde() { return pnd_Lgr_Acct_Rpt_Inv_Acct_Cde; }

    public DbsGroup getPnd_Lgr_Acct_Rpt_Inv_Acct_CdeRedef1() { return pnd_Lgr_Acct_Rpt_Inv_Acct_CdeRedef1; }

    public DbsField getPnd_Lgr_Acct_Rpt_Inv_Acct_Cde_X() { return pnd_Lgr_Acct_Rpt_Inv_Acct_Cde_X; }

    public DbsField getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Ind() { return pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Ind; }

    public DbsField getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Nbr() { return pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Nbr; }

    public DbsField getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Amt() { return pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Lgr_Acct_Rpt = newGroupInRecord("pnd_Lgr_Acct_Rpt", "#LGR-ACCT-RPT");
        pnd_Lgr_Acct_Rpt_Cntrct_Orgn_Cde = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Lgr_Acct_Rpt_Pymnt_Check_Dte = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_Type = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 
            1);
        pnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_Cde = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 
            1);
        pnd_Lgr_Acct_Rpt_Cntrct_Insurance_Option = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", 
            FieldType.STRING, 1);
        pnd_Lgr_Acct_Rpt_Cntrct_Life_Contingency = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", 
            FieldType.STRING, 1);
        pnd_Lgr_Acct_Rpt_Cntrct_Company_Cde = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Cntrct_Company_Cde", "CNTRCT-COMPANY-CDE", FieldType.STRING, 
            1);
        pnd_Lgr_Acct_Rpt_Acct_Company = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Acct_Company", "ACCT-COMPANY", FieldType.STRING, 5);
        pnd_Lgr_Acct_Rpt_Inv_Acct_Cde = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.NUMERIC, 2);
        pnd_Lgr_Acct_Rpt_Inv_Acct_CdeRedef1 = pnd_Lgr_Acct_Rpt.newGroupInGroup("pnd_Lgr_Acct_Rpt_Inv_Acct_CdeRedef1", "Redefines", pnd_Lgr_Acct_Rpt_Inv_Acct_Cde);
        pnd_Lgr_Acct_Rpt_Inv_Acct_Cde_X = pnd_Lgr_Acct_Rpt_Inv_Acct_CdeRedef1.newFieldInGroup("pnd_Lgr_Acct_Rpt_Inv_Acct_Cde_X", "INV-ACCT-CDE-X", FieldType.STRING, 
            2);
        pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Ind = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Ind", "INV-ACCT-LEDGR-IND", FieldType.STRING, 
            1);
        pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Nbr = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Nbr", "INV-ACCT-LEDGR-NBR", FieldType.STRING, 
            15);
        pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Amt = pnd_Lgr_Acct_Rpt.newFieldInGroup("pnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Amt", "INV-ACCT-LEDGR-AMT", FieldType.PACKED_DECIMAL, 
            13,2);

        this.setRecordName("LdaFcpl827");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl827() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
