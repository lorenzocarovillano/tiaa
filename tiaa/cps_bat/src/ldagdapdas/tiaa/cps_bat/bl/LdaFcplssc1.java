/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:07 PM
**        * FROM NATURAL LDA     : FCPLSSC1
************************************************************
**        * FILE NAME            : LdaFcplssc1.java
**        * CLASS NAME           : LdaFcplssc1
**        * INSTANCE NAME        : LdaFcplssc1
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplssc1 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcplssc1;
    private DbsField pnd_Fcplssc1_Pnd_Max_Fund;
    private DbsGroup pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct;
    private DbsField pnd_Fcplssc1_Inv_Acct_Settl_Amt;
    private DbsField pnd_Fcplssc1_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Fcplssc1_Inv_Acct_Dci_Amt;
    private DbsField pnd_Fcplssc1_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Fcplssc1_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Fcplssc1_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Fcplssc1_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Fcplssc1_Inv_Acct_Local_Tax_Amt;

    public DbsGroup getPnd_Fcplssc1() { return pnd_Fcplssc1; }

    public DbsField getPnd_Fcplssc1_Pnd_Max_Fund() { return pnd_Fcplssc1_Pnd_Max_Fund; }

    public DbsGroup getPnd_Fcplssc1_Pnd_Cntrl_Inv_Acct() { return pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct; }

    public DbsField getPnd_Fcplssc1_Inv_Acct_Settl_Amt() { return pnd_Fcplssc1_Inv_Acct_Settl_Amt; }

    public DbsField getPnd_Fcplssc1_Inv_Acct_Dvdnd_Amt() { return pnd_Fcplssc1_Inv_Acct_Dvdnd_Amt; }

    public DbsField getPnd_Fcplssc1_Inv_Acct_Dci_Amt() { return pnd_Fcplssc1_Inv_Acct_Dci_Amt; }

    public DbsField getPnd_Fcplssc1_Inv_Acct_Dpi_Amt() { return pnd_Fcplssc1_Inv_Acct_Dpi_Amt; }

    public DbsField getPnd_Fcplssc1_Inv_Acct_Net_Pymnt_Amt() { return pnd_Fcplssc1_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getPnd_Fcplssc1_Inv_Acct_Fdrl_Tax_Amt() { return pnd_Fcplssc1_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getPnd_Fcplssc1_Inv_Acct_State_Tax_Amt() { return pnd_Fcplssc1_Inv_Acct_State_Tax_Amt; }

    public DbsField getPnd_Fcplssc1_Inv_Acct_Local_Tax_Amt() { return pnd_Fcplssc1_Inv_Acct_Local_Tax_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcplssc1 = newGroupInRecord("pnd_Fcplssc1", "#FCPLSSC1");
        pnd_Fcplssc1_Pnd_Max_Fund = pnd_Fcplssc1.newFieldInGroup("pnd_Fcplssc1_Pnd_Max_Fund", "#MAX-FUND", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct = pnd_Fcplssc1.newGroupArrayInGroup("pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct", "#CNTRL-INV-ACCT", new DbsArrayController(1,
            1));
        pnd_Fcplssc1_Inv_Acct_Settl_Amt = pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct.newFieldInGroup("pnd_Fcplssc1_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Fcplssc1_Inv_Acct_Dvdnd_Amt = pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct.newFieldInGroup("pnd_Fcplssc1_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Fcplssc1_Inv_Acct_Dci_Amt = pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct.newFieldInGroup("pnd_Fcplssc1_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Fcplssc1_Inv_Acct_Dpi_Amt = pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct.newFieldInGroup("pnd_Fcplssc1_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Fcplssc1_Inv_Acct_Net_Pymnt_Amt = pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct.newFieldInGroup("pnd_Fcplssc1_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Fcplssc1_Inv_Acct_Fdrl_Tax_Amt = pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct.newFieldInGroup("pnd_Fcplssc1_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Fcplssc1_Inv_Acct_State_Tax_Amt = pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct.newFieldInGroup("pnd_Fcplssc1_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Fcplssc1_Inv_Acct_Local_Tax_Amt = pnd_Fcplssc1_Pnd_Cntrl_Inv_Acct.newFieldInGroup("pnd_Fcplssc1_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);

        this.setRecordName("LdaFcplssc1");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcplssc1_Pnd_Max_Fund.setInitialValue(1);
    }

    // Constructor
    public LdaFcplssc1() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
