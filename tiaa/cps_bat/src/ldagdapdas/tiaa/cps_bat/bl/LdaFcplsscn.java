/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:07 PM
**        * FROM NATURAL LDA     : FCPLSSCN
************************************************************
**        * FILE NAME            : LdaFcplsscn.java
**        * CLASS NAME           : LdaFcplsscn
**        * INSTANCE NAME        : LdaFcplsscn
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplsscn extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcplsscn;
    private DbsField pnd_Fcplsscn_Pnd_Cntl_Table_Key;
    private DbsField pnd_Fcplsscn_Pnd_Cntl_Table_Result;

    public DbsGroup getPnd_Fcplsscn() { return pnd_Fcplsscn; }

    public DbsField getPnd_Fcplsscn_Pnd_Cntl_Table_Key() { return pnd_Fcplsscn_Pnd_Cntl_Table_Key; }

    public DbsField getPnd_Fcplsscn_Pnd_Cntl_Table_Result() { return pnd_Fcplsscn_Pnd_Cntl_Table_Result; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcplsscn = newGroupInRecord("pnd_Fcplsscn", "#FCPLSSCN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key = pnd_Fcplsscn.newFieldArrayInGroup("pnd_Fcplsscn_Pnd_Cntl_Table_Key", "#CNTL-TABLE-KEY", FieldType.STRING, 6, 
            new DbsArrayController(1,45));
        pnd_Fcplsscn_Pnd_Cntl_Table_Result = pnd_Fcplsscn.newFieldArrayInGroup("pnd_Fcplsscn_Pnd_Cntl_Table_Result", "#CNTL-TABLE-RESULT", FieldType.PACKED_DECIMAL, 
            3, new DbsArrayController(1,45));

        this.setRecordName("LdaFcplsscn");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(1).setInitialValue("SSC");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(2).setInitialValue("SSL");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(3).setInitialValue("SSR");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(4).setInitialValue("SSC C");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(5).setInitialValue("SSL C");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(6).setInitialValue("SSR C");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(7).setInitialValue("DSC C");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(8).setInitialValue("DSL C");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(9).setInitialValue("DSR C");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(10).setInitialValue("SSC R");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(11).setInitialValue("SSL R");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(12).setInitialValue("SSR R");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(13).setInitialValue("DSC R");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(14).setInitialValue("DSL R");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(15).setInitialValue("DSR R");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(16).setInitialValue("SSC S");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(17).setInitialValue("SSL S");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(18).setInitialValue("SSR S");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(19).setInitialValue("DSC S");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(20).setInitialValue("DSL S");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(21).setInitialValue("DSR S");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(22).setInitialValue("SS30");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(23).setInitialValue("SS31");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(24).setInitialValue("SS30C");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(25).setInitialValue("SS31C");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(26).setInitialValue("SS30S");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(27).setInitialValue("SS31S");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(28).setInitialValue("SS30R");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(29).setInitialValue("SS31R");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(30).setInitialValue("SSC CN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(31).setInitialValue("SSL CN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(32).setInitialValue("SSR CN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(33).setInitialValue("DSC CN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(34).setInitialValue("DSL CN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(35).setInitialValue("DSR CN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(36).setInitialValue("SSC SN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(37).setInitialValue("SSL SN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(38).setInitialValue("SSR SN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(39).setInitialValue("DSC SN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(40).setInitialValue("DSL SN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(41).setInitialValue("DSR SN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(42).setInitialValue("SS30CN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(43).setInitialValue("SS31CN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(44).setInitialValue("SS30SN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Key.getValue(45).setInitialValue("SS31SN");
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(1).setInitialValue(1);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(2).setInitialValue(2);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(3).setInitialValue(3);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(4).setInitialValue(12);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(5).setInitialValue(12);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(6).setInitialValue(12);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(7).setInitialValue(25);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(8).setInitialValue(25);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(9).setInitialValue(25);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(10).setInitialValue(14);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(11).setInitialValue(15);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(12).setInitialValue(16);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(13).setInitialValue(27);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(14).setInitialValue(28);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(15).setInitialValue(29);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(16).setInitialValue(13);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(17).setInitialValue(13);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(18).setInitialValue(13);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(19).setInitialValue(26);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(20).setInitialValue(26);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(21).setInitialValue(26);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(22).setInitialValue(30);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(23).setInitialValue(31);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(24).setInitialValue(32);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(25).setInitialValue(32);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(26).setInitialValue(33);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(27).setInitialValue(33);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(28).setInitialValue(34);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(29).setInitialValue(34);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(30).setInitialValue(12);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(31).setInitialValue(12);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(32).setInitialValue(12);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(33).setInitialValue(25);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(34).setInitialValue(25);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(35).setInitialValue(25);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(36).setInitialValue(13);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(37).setInitialValue(13);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(38).setInitialValue(13);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(39).setInitialValue(26);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(40).setInitialValue(26);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(41).setInitialValue(26);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(42).setInitialValue(32);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(43).setInitialValue(32);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(44).setInitialValue(33);
        pnd_Fcplsscn_Pnd_Cntl_Table_Result.getValue(45).setInitialValue(33);
    }

    // Constructor
    public LdaFcplsscn() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
