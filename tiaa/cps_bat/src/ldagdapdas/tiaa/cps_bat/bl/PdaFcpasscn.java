/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:34 PM
**        * FROM NATURAL PDA     : FCPASSCN
************************************************************
**        * FILE NAME            : PdaFcpasscn.java
**        * CLASS NAME           : PdaFcpasscn
**        * INSTANCE NAME        : PdaFcpasscn
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpasscn extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpasscn;
    private DbsField pnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Fcpasscn_Pnd_Search_Key;
    private DbsGroup pnd_Fcpasscn_Pnd_Search_KeyRedef1;
    private DbsField pnd_Fcpasscn_Cntrct_Orgn_Cde;
    private DbsField pnd_Fcpasscn_Cntrct_Type_Cde;
    private DbsField pnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde;

    public DbsGroup getPnd_Fcpasscn() { return pnd_Fcpasscn; }

    public DbsField getPnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind() { return pnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPnd_Fcpasscn_Pnd_Search_Key() { return pnd_Fcpasscn_Pnd_Search_Key; }

    public DbsGroup getPnd_Fcpasscn_Pnd_Search_KeyRedef1() { return pnd_Fcpasscn_Pnd_Search_KeyRedef1; }

    public DbsField getPnd_Fcpasscn_Cntrct_Orgn_Cde() { return pnd_Fcpasscn_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Fcpasscn_Cntrct_Type_Cde() { return pnd_Fcpasscn_Cntrct_Type_Cde; }

    public DbsField getPnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpasscn = dbsRecord.newGroupInRecord("pnd_Fcpasscn", "#FCPASSCN");
        pnd_Fcpasscn.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind = pnd_Fcpasscn.newFieldInGroup("pnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        pnd_Fcpasscn_Pnd_Search_Key = pnd_Fcpasscn.newFieldInGroup("pnd_Fcpasscn_Pnd_Search_Key", "#SEARCH-KEY", FieldType.STRING, 6);
        pnd_Fcpasscn_Pnd_Search_KeyRedef1 = pnd_Fcpasscn.newGroupInGroup("pnd_Fcpasscn_Pnd_Search_KeyRedef1", "Redefines", pnd_Fcpasscn_Pnd_Search_Key);
        pnd_Fcpasscn_Cntrct_Orgn_Cde = pnd_Fcpasscn_Pnd_Search_KeyRedef1.newFieldInGroup("pnd_Fcpasscn_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Fcpasscn_Cntrct_Type_Cde = pnd_Fcpasscn_Pnd_Search_KeyRedef1.newFieldInGroup("pnd_Fcpasscn_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Fcpasscn_Pnd_Search_KeyRedef1.newFieldInGroup("pnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpasscn(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

