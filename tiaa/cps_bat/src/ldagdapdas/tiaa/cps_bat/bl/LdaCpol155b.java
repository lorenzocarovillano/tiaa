/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:21 PM
**        * FROM NATURAL LDA     : CPOL155B
************************************************************
**        * FILE NAME            : LdaCpol155b.java
**        * CLASS NAME           : LdaCpol155b
**        * INSTANCE NAME        : LdaCpol155b
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpol155b extends DbsRecord
{
    // Properties
    private DbsGroup cpol155b;
    private DbsField cpol155b_Process;
    private DbsField cpol155b_X01;
    private DbsField cpol155b_Today_Date;
    private DbsField cpol155b_X02;
    private DbsField cpol155b_Today_Time;
    private DbsField cpol155b_X03;
    private DbsField cpol155b_Cycle;
    private DbsField cpol155b_X04;
    private DbsField cpol155b_Status;
    private DbsField cpol155b_X05;
    private DbsField cpol155b_Check_Cnt;
    private DbsField cpol155b_X06;
    private DbsField cpol155b_Check_Amt;
    private DbsField cpol155b_X07;
    private DbsField cpol155b_Eft_Cnt;
    private DbsField cpol155b_X08;
    private DbsField cpol155b_Eft_Amt;
    private DbsField cpol155b_X09;
    private DbsField cpol155b_Other_Cnt;
    private DbsField cpol155b_X10;
    private DbsField cpol155b_Other_Amt;
    private DbsField cpol155b_X11;
    private DbsField cpol155b_Cs_Cnt;
    private DbsField cpol155b_X12;
    private DbsField cpol155b_Cs_Amt;
    private DbsField cpol155b_X13;
    private DbsField cpol155b_Redraw_Cnt;
    private DbsField cpol155b_X14;
    private DbsField cpol155b_Redraw_Amt;
    private DbsField filler01;

    public DbsGroup getCpol155b() { return cpol155b; }

    public DbsField getCpol155b_Process() { return cpol155b_Process; }

    public DbsField getCpol155b_X01() { return cpol155b_X01; }

    public DbsField getCpol155b_Today_Date() { return cpol155b_Today_Date; }

    public DbsField getCpol155b_X02() { return cpol155b_X02; }

    public DbsField getCpol155b_Today_Time() { return cpol155b_Today_Time; }

    public DbsField getCpol155b_X03() { return cpol155b_X03; }

    public DbsField getCpol155b_Cycle() { return cpol155b_Cycle; }

    public DbsField getCpol155b_X04() { return cpol155b_X04; }

    public DbsField getCpol155b_Status() { return cpol155b_Status; }

    public DbsField getCpol155b_X05() { return cpol155b_X05; }

    public DbsField getCpol155b_Check_Cnt() { return cpol155b_Check_Cnt; }

    public DbsField getCpol155b_X06() { return cpol155b_X06; }

    public DbsField getCpol155b_Check_Amt() { return cpol155b_Check_Amt; }

    public DbsField getCpol155b_X07() { return cpol155b_X07; }

    public DbsField getCpol155b_Eft_Cnt() { return cpol155b_Eft_Cnt; }

    public DbsField getCpol155b_X08() { return cpol155b_X08; }

    public DbsField getCpol155b_Eft_Amt() { return cpol155b_Eft_Amt; }

    public DbsField getCpol155b_X09() { return cpol155b_X09; }

    public DbsField getCpol155b_Other_Cnt() { return cpol155b_Other_Cnt; }

    public DbsField getCpol155b_X10() { return cpol155b_X10; }

    public DbsField getCpol155b_Other_Amt() { return cpol155b_Other_Amt; }

    public DbsField getCpol155b_X11() { return cpol155b_X11; }

    public DbsField getCpol155b_Cs_Cnt() { return cpol155b_Cs_Cnt; }

    public DbsField getCpol155b_X12() { return cpol155b_X12; }

    public DbsField getCpol155b_Cs_Amt() { return cpol155b_Cs_Amt; }

    public DbsField getCpol155b_X13() { return cpol155b_X13; }

    public DbsField getCpol155b_Redraw_Cnt() { return cpol155b_Redraw_Cnt; }

    public DbsField getCpol155b_X14() { return cpol155b_X14; }

    public DbsField getCpol155b_Redraw_Amt() { return cpol155b_Redraw_Amt; }

    public DbsField getFiller01() { return filler01; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cpol155b = newGroupInRecord("cpol155b", "CPOL155B");
        cpol155b_Process = cpol155b.newFieldInGroup("cpol155b_Process", "PROCESS", FieldType.STRING, 20);
        cpol155b_X01 = cpol155b.newFieldInGroup("cpol155b_X01", "X01", FieldType.STRING, 1);
        cpol155b_Today_Date = cpol155b.newFieldInGroup("cpol155b_Today_Date", "TODAY-DATE", FieldType.NUMERIC, 8);
        cpol155b_X02 = cpol155b.newFieldInGroup("cpol155b_X02", "X02", FieldType.STRING, 1);
        cpol155b_Today_Time = cpol155b.newFieldInGroup("cpol155b_Today_Time", "TODAY-TIME", FieldType.NUMERIC, 7);
        cpol155b_X03 = cpol155b.newFieldInGroup("cpol155b_X03", "X03", FieldType.STRING, 1);
        cpol155b_Cycle = cpol155b.newFieldInGroup("cpol155b_Cycle", "CYCLE", FieldType.NUMERIC, 4);
        cpol155b_X04 = cpol155b.newFieldInGroup("cpol155b_X04", "X04", FieldType.STRING, 1);
        cpol155b_Status = cpol155b.newFieldInGroup("cpol155b_Status", "STATUS", FieldType.STRING, 20);
        cpol155b_X05 = cpol155b.newFieldInGroup("cpol155b_X05", "X05", FieldType.STRING, 1);
        cpol155b_Check_Cnt = cpol155b.newFieldArrayInGroup("cpol155b_Check_Cnt", "CHECK-CNT", FieldType.NUMERIC, 10, new DbsArrayController(1,20));
        cpol155b_X06 = cpol155b.newFieldInGroup("cpol155b_X06", "X06", FieldType.STRING, 1);
        cpol155b_Check_Amt = cpol155b.newFieldArrayInGroup("cpol155b_Check_Amt", "CHECK-AMT", FieldType.DECIMAL, 25,2, new DbsArrayController(1,20));
        cpol155b_X07 = cpol155b.newFieldInGroup("cpol155b_X07", "X07", FieldType.STRING, 1);
        cpol155b_Eft_Cnt = cpol155b.newFieldArrayInGroup("cpol155b_Eft_Cnt", "EFT-CNT", FieldType.NUMERIC, 10, new DbsArrayController(1,20));
        cpol155b_X08 = cpol155b.newFieldInGroup("cpol155b_X08", "X08", FieldType.STRING, 1);
        cpol155b_Eft_Amt = cpol155b.newFieldArrayInGroup("cpol155b_Eft_Amt", "EFT-AMT", FieldType.DECIMAL, 25,2, new DbsArrayController(1,20));
        cpol155b_X09 = cpol155b.newFieldInGroup("cpol155b_X09", "X09", FieldType.STRING, 1);
        cpol155b_Other_Cnt = cpol155b.newFieldArrayInGroup("cpol155b_Other_Cnt", "OTHER-CNT", FieldType.NUMERIC, 10, new DbsArrayController(1,20));
        cpol155b_X10 = cpol155b.newFieldInGroup("cpol155b_X10", "X10", FieldType.STRING, 1);
        cpol155b_Other_Amt = cpol155b.newFieldArrayInGroup("cpol155b_Other_Amt", "OTHER-AMT", FieldType.DECIMAL, 25,2, new DbsArrayController(1,20));
        cpol155b_X11 = cpol155b.newFieldInGroup("cpol155b_X11", "X11", FieldType.STRING, 1);
        cpol155b_Cs_Cnt = cpol155b.newFieldArrayInGroup("cpol155b_Cs_Cnt", "CS-CNT", FieldType.NUMERIC, 10, new DbsArrayController(1,20));
        cpol155b_X12 = cpol155b.newFieldInGroup("cpol155b_X12", "X12", FieldType.STRING, 1);
        cpol155b_Cs_Amt = cpol155b.newFieldArrayInGroup("cpol155b_Cs_Amt", "CS-AMT", FieldType.DECIMAL, 25,2, new DbsArrayController(1,20));
        cpol155b_X13 = cpol155b.newFieldInGroup("cpol155b_X13", "X13", FieldType.STRING, 1);
        cpol155b_Redraw_Cnt = cpol155b.newFieldArrayInGroup("cpol155b_Redraw_Cnt", "REDRAW-CNT", FieldType.NUMERIC, 10, new DbsArrayController(1,20));
        cpol155b_X14 = cpol155b.newFieldInGroup("cpol155b_X14", "X14", FieldType.STRING, 1);
        cpol155b_Redraw_Amt = cpol155b.newFieldArrayInGroup("cpol155b_Redraw_Amt", "REDRAW-AMT", FieldType.DECIMAL, 25,2, new DbsArrayController(1,20));
        filler01 = cpol155b.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 227);

        this.setRecordName("LdaCpol155b");
    }

    public void initializeValues() throws Exception
    {
        reset();
        cpol155b_Process.setInitialValue("PROCESS");
        cpol155b_X01.setInitialValue(" ");
        cpol155b_Today_Date.setInitialValue(Global.getDATN());
        cpol155b_X02.setInitialValue(" ");
        cpol155b_Today_Time.setInitialValue(Global.getTIMN());
        cpol155b_X03.setInitialValue(" ");
        cpol155b_X04.setInitialValue(" ");
        cpol155b_Status.setInitialValue("print");
        cpol155b_X05.setInitialValue(" ");
        cpol155b_X06.setInitialValue(" ");
        cpol155b_X07.setInitialValue(" ");
        cpol155b_X08.setInitialValue(" ");
        cpol155b_X09.setInitialValue(" ");
        cpol155b_X10.setInitialValue(" ");
        cpol155b_X11.setInitialValue(" ");
        cpol155b_X12.setInitialValue(" ");
        cpol155b_X13.setInitialValue(" ");
        cpol155b_X14.setInitialValue(" ");
        filler01.setInitialValue(" ");
    }

    // Constructor
    public LdaCpol155b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
