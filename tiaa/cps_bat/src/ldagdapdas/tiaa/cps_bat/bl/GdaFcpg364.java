/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:48:46 PM
**        * FROM NATURAL GDA     : FCPG364
************************************************************
**        * FILE NAME            : GdaFcpg364.java
**        * CLASS NAME           : GdaFcpg364
**        * INSTANCE NAME        : GdaFcpg364
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import java.util.ArrayList;
import java.util.List;

import tiaa.tiaacommon.bl.*;

public final class GdaFcpg364 extends DbsRecord
{
    private static ThreadLocal<List<GdaFcpg364>> _instance = new ThreadLocal<List<GdaFcpg364>>();

    // Properties
    private DbsField ext;
    private DbsGroup extRedef1;
    private DbsGroup ext_Extr;
    private DbsField ext_Cntrct_Ppcn_Nbr;
    private DbsField ext_Cntrct_Payee_Cde;
    private DbsField ext_Cntrct_Invrse_Dte;
    private DbsField ext_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup ext_Pymnt_Prcss_Seq_NbrRedef2;
    private DbsField ext_Pymnt_Prcss_Seq_Num;
    private DbsField ext_Pymnt_Instmt_Nbr;
    private DbsField ext_Pymnt_Total_Pages;
    private DbsField ext_Funds_On_Page;
    private DbsField ext_Cntrct_Unq_Id_Nbr;
    private DbsField ext_Cntrct_Cmbn_Nbr;
    private DbsField ext_Pymnt_Record;
    private DbsField ext_Pymnt_Ivc_Amt;
    private DbsField filler01;
    private DbsField ext_Annt_Ctznshp_Cde;
    private DbsGroup ext_Annt_Ctznshp_CdeRedef3;
    private DbsField ext_Annt_Ctznshp_Cde_X;
    private DbsField ext_Annt_Locality_Cde;
    private DbsField ext_Annt_Rsdncy_Cde;
    private DbsField ext_Annt_Soc_Sec_Ind;
    private DbsField ext_Annt_Soc_Sec_Nbr;
    private DbsField ext_Cntrct_Check_Crrncy_Cde;
    private DbsField ext_Cntrct_Crrncy_Cde;
    private DbsField ext_Cntrct_Orgn_Cde;
    private DbsField ext_Cntrct_Qlfied_Cde;
    private DbsField ext_Cntrct_Pymnt_Type_Ind;
    private DbsField ext_Cntrct_Sttlmnt_Type_Ind;
    private DbsField ext_Cntrct_Sps_Cde;
    private DbsField ext_Cntrct_Dvdnd_Payee_Ind;
    private DbsField ext_Cntrct_Rqst_Settl_Dte;
    private DbsField ext_Cntrct_Rqst_Dte;
    private DbsField ext_Cntrct_Rqst_Settl_Tme;
    private DbsField ext_Cntrct_Type_Cde;
    private DbsField ext_Cntrct_Lob_Cde;
    private DbsField ext_Cntrct_Sub_Lob_Cde;
    private DbsField ext_Cntrct_Ia_Lob_Cde;
    private DbsField ext_Cntrct_Cref_Nbr;
    private DbsField ext_Cntrct_Option_Cde;
    private DbsField ext_Cntrct_Mode_Cde;
    private DbsField ext_Cntrct_Pymnt_Dest_Cde;
    private DbsField ext_Cntrct_Roll_Dest_Cde;
    private DbsField ext_Cntrct_Dvdnd_Payee_Cde;
    private DbsField ext_Cntrct_Cancel_Rdrw_Ind;
    private DbsField ext_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField ext_Cntrct_Cancel_Rdrw_Amt;
    private DbsField ext_Cntrct_Hold_Cde;
    private DbsGroup ext_Cntrct_Hold_CdeRedef4;
    private DbsField ext_Cntrct_Hold_Type;
    private DbsField ext_Cntrct_Hold_Dept;
    private DbsField ext_Cntrct_Hold_Grp;
    private DbsField ext_Cntrct_Hold_Ind;
    private DbsField ext_Cntrct_Hold_Tme;
    private DbsField ext_Cntrct_Annty_Ins_Type;
    private DbsField ext_Cntrct_Annty_Type_Cde;
    private DbsField ext_Cntrct_Insurance_Option;
    private DbsField ext_Cntrct_Life_Contingency;
    private DbsField ext_Cntrct_Ac_Lt_10yrs;
    private DbsField ext_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField ext_Cntrct_Da_Cref_1_Nbr;
    private DbsField ext_Cntrct_Da_Cref_2_Nbr;
    private DbsField ext_Cntrct_Da_Cref_3_Nbr;
    private DbsField ext_Cntrct_Da_Cref_4_Nbr;
    private DbsField ext_Cntrct_Da_Cref_5_Nbr;
    private DbsField ext_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField ext_Pymnt_Rqst_Rmndr_Pct;
    private DbsField ext_Pymnt_Stats_Cde;
    private DbsField ext_Pymnt_Annot_Ind;
    private DbsField ext_Pymnt_Cmbne_Ind;
    private DbsField ext_Pymnt_Ftre_Ind;
    private DbsField ext_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField ext_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField ext_Pymnt_Inst_Rep_Cde;
    private DbsField ext_Pymnt_Pay_Type_Req_Ind;
    private DbsField ext_Pymnt_Split_Cde;
    private DbsField ext_Pymnt_Split_Reasn_Cde;
    private DbsField ext_Pymnt_Check_Dte;
    private DbsField ext_Pymnt_Cycle_Dte;
    private DbsField ext_Pymnt_Eft_Dte;
    private DbsField ext_Pymnt_Rqst_Pct;
    private DbsField ext_Pymnt_Rqst_Amt;
    private DbsField ext_Pymnt_Check_Nbr;
    private DbsField ext_Pymnt_Check_Scrty_Nbr;
    private DbsField ext_Pymnt_Check_Amt;
    private DbsField ext_Pymnt_Settlmnt_Dte;
    private DbsField ext_Pymnt_Check_Seq_Nbr;
    private DbsField ext_Pymnt_Rprnt_Pndng_Ind;
    private DbsField ext_Pymnt_Rprnt_Rqust_Dte;
    private DbsField ext_Pymnt_Rprnt_Dte;
    private DbsField ext_Pymnt_Corp_Wpid;
    private DbsField ext_Pymnt_Reqst_Log_Dte_Time;
    private DbsField ext_Pymnt_Acctg_Dte;
    private DbsField ext_Pymnt_Intrfce_Dte;
    private DbsField ext_Pymnt_Tiaa_Md_Amt;
    private DbsField ext_Pymnt_Cref_Md_Amt;
    private DbsField ext_Pymnt_Tax_Form;
    private DbsGroup ext_Pymnt_Tax_FormRedef5;
    private DbsField ext_F;
    private DbsField ext_Pymnt_Tax_Form_1001;
    private DbsField ext_Pymnt_Tax_Form_1078;
    private DbsField ext_Pymnt_Tax_Exempt_Ind;
    private DbsField ext_Pymnt_Tax_Calc_Cde;
    private DbsField ext_Pymnt_Method_Cde;
    private DbsField ext_Pymnt_Settl_Ivc_Ind;
    private DbsField ext_Pymnt_Ia_Issue_Dte;
    private DbsField ext_Pymnt_Spouse_Pay_Stats;
    private DbsField ext_Pymnt_Trigger_Cde;
    private DbsField ext_Pymnt_Spcl_Msg_Cde;
    private DbsField ext_Pymnt_Eft_Transit_Id;
    private DbsField ext_Pymnt_Eft_Acct_Nbr;
    private DbsField ext_Pymnt_Chk_Sav_Ind;
    private DbsField ext_Pymnt_Deceased_Nme;
    private DbsField ext_Pymnt_Dob;
    private DbsField ext_Pymnt_Death_Dte;
    private DbsField ext_Pymnt_Proof_Dte;
    private DbsField ext_Pymnt_Rtb_Dest_Typ;
    private DbsField ext_Pymnt_Alt_Addr_Ind;
    private DbsField ext_Notused1;
    private DbsField ext_Notused2;
    private DbsGroup ext_Notused2Redef6;
    private DbsField ext_Pymnt_Wrrnt_Trans_Type;
    private DbsField ext_C_Inv_Rtb_Grp;
    private DbsField ext_C_Pymnt_Ded_Grp;
    private DbsField ext_C_Inv_Acct;
    private DbsField ext_C_Pymnt_Nme_And_Addr;
    private DbsField ext_Cnr_Cs_Actvty_Cde;
    private DbsField ext_Cnr_Cs_Pymnt_Intrfce_Dte;
    private DbsField ext_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField ext_Cnr_Cs_Pymnt_Check_Dte;
    private DbsField ext_Cnr_Cs_New_Stop_Ind;
    private DbsField ext_Cnr_Cs_Rqust_Dte;
    private DbsField ext_Cnr_Cs_Rqust_Tme;
    private DbsField ext_Cnr_Cs_Rqust_User_Id;
    private DbsField ext_Cnr_Cs_Rqust_Term_Id;
    private DbsField ext_Cnr_Rdrw_Rqust_Tme;
    private DbsField ext_Cnr_Rdrw_Rqust_User_Id;
    private DbsField ext_Cnr_Orgnl_Invrse_Dte;
    private DbsField ext_Cnr_Orgnl_Prcss_Seq_Nbr;
    private DbsField ext_Cnr_Orgnl_Pymnt_Acctg_Dte;
    private DbsField ext_Cnr_Orgnl_Pymnt_Intrfce_Dte;
    private DbsField ext_Cnr_Rdrw_Rqust_Dte;
    private DbsField ext_Cnr_Rdrw_Rqust_Term_Id;
    private DbsField ext_Cnr_Rdrw_Pymnt_Intrfce_Dte;
    private DbsField ext_Cnr_Rdrw_Pymnt_Acctg_Dte;
    private DbsField ext_Cnr_Rdrw_Pymnt_Check_Dte;
    private DbsField ext_Cnr_Rplcmnt_Invrse_Dte;
    private DbsField ext_Cnr_Rplcmnt_Prcss_Seq_Nbr;
    private DbsField ext_Rcrd_Typ;
    private DbsField ext_Tax_Fed_C_Resp_Cde;
    private DbsField ext_Tax_Fed_C_Filing_Stat;
    private DbsField ext_Tax_Fed_C_Allow_Cnt;
    private DbsField ext_Tax_Fed_C_Fixed_Amt;
    private DbsField ext_Tax_Fed_C_Fixed_Pct;
    private DbsField ext_Tax_Sta_C_Resp_Cde;
    private DbsField ext_Tax_Sta_C_Filing_Stat;
    private DbsField ext_Tax_Sta_C_Allow_Cnt;
    private DbsField ext_Tax_Sta_C_Fixed_Amt;
    private DbsField ext_Tax_Sta_C_Fixed_Pct;
    private DbsField ext_Tax_Sta_C_Spouse_Cnt;
    private DbsField ext_Tax_Sta_C_Blind_Cnt;
    private DbsField ext_Tax_Loc_C_Resp_Cde;
    private DbsField ext_Tax_Loc_C_Fixed_Amt;
    private DbsField ext_Tax_Loc_C_Fixed_Pct;
    private DbsGroup ext_Ph_Name;
    private DbsField ext_Ph_Last_Name;
    private DbsField ext_Ph_First_Name;
    private DbsField ext_Ph_Middle_Name;
    private DbsField ext_Ph_Sex;
    private DbsGroup ext_Pymnt_Nme_And_Addr_Grp;
    private DbsField ext_Pymnt_Nme;
    private DbsField ext_Pymnt_Addr_Line_Txt;
    private DbsGroup ext_Pymnt_Addr_Line_TxtRedef7;
    private DbsField ext_Pymnt_Addr_Line1_Txt;
    private DbsField ext_Pymnt_Addr_Line2_Txt;
    private DbsField ext_Pymnt_Addr_Line3_Txt;
    private DbsField ext_Pymnt_Addr_Line4_Txt;
    private DbsField ext_Pymnt_Addr_Line5_Txt;
    private DbsField ext_Pymnt_Addr_Line6_Txt;
    private DbsField ext_Pymnt_Addr_Zip_Cde;
    private DbsField ext_Pymnt_Postl_Data;
    private DbsField ext_Pymnt_Addr_Type_Ind;
    private DbsField ext_Pymnt_Addr_Last_Chg_Dte;
    private DbsField ext_Pymnt_Addr_Last_Chg_Tme;
    private DbsField ext_Pymnt_Addr_Chg_Ind;
    private DbsGroup ext_Pymnt_Ded_Grp;
    private DbsField ext_Pymnt_Ded_Cde;
    private DbsField ext_Pymnt_Ded_Payee_Cde;
    private DbsField ext_Pymnt_Ded_Amt;
    private DbsField ext_Egtrra_Eligibility_Ind;
    private DbsField ext_Originating_Irc_Cde;
    private DbsGroup ext_Originating_Irc_CdeRedef8;
    private DbsField ext_Distributing_Irc_Cde;
    private DbsField ext_Receiving_Irc_Cde;
    private DbsField ext_Tpa_Filler;
    private DbsGroup ext_Inv_Acct;
    private DbsField ext_Inv_Acct_Cde;
    private DbsGroup ext_Inv_Acct_CdeRedef9;
    private DbsField ext_Inv_Acct_Cde_N;
    private DbsField ext_Inv_Acct_Ded_Cde;
    private DbsField ext_Inv_Acct_Ded_Amt;
    private DbsField ext_Inv_Acct_Company;
    private DbsField ext_Inv_Acct_Unit_Qty;
    private DbsField ext_Inv_Acct_Unit_Value;
    private DbsField ext_Inv_Acct_Settl_Amt;
    private DbsField ext_Inv_Acct_Fed_Cde;
    private DbsField ext_Inv_Acct_State_Cde;
    private DbsField ext_Inv_Acct_Local_Cde;
    private DbsField ext_Inv_Acct_Ivc_Amt;
    private DbsField ext_Inv_Acct_Dci_Amt;
    private DbsField ext_Inv_Acct_Dpi_Amt;
    private DbsField ext_Inv_Acct_Start_Accum_Amt;
    private DbsField ext_Inv_Acct_End_Accum_Amt;
    private DbsField ext_Inv_Acct_Dvdnd_Amt;
    private DbsField ext_Inv_Acct_Net_Pymnt_Amt;
    private DbsField ext_Inv_Acct_Ivc_Ind;
    private DbsField ext_Inv_Acct_Adj_Ivc_Amt;
    private DbsField ext_Inv_Acct_Valuat_Period;
    private DbsField ext_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField ext_Inv_Acct_State_Tax_Amt;
    private DbsField ext_Inv_Acct_Local_Tax_Amt;
    private DbsField ext_Inv_Acct_Exp_Amt;
    private DbsGroup ext_Inv_Rtb_Grp;
    private DbsField ext_Inv_Rtb_Cde;
    private DbsField ext_Inv_Acct_Fed_Dci_Tax_Amt;
    private DbsField ext_Inv_Acct_Sta_Dci_Tax_Amt;
    private DbsField ext_Inv_Acct_Loc_Dci_Tax_Amt;
    private DbsField ext_Inv_Acct_Fed_Dpi_Tax_Amt;
    private DbsField ext_Inv_Acct_Sta_Dpi_Tax_Amt;
    private DbsField ext_Inv_Acct_Loc_Dpi_Tax_Amt;
    private DataAccessProgramView vw_fcp_Cons_Plan;
    private DbsGroup fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data;
    private DbsField fcp_Cons_Plan_Cntrct_Rcrd_Typ;
    private DbsField fcp_Cons_Plan_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Plan_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef10;
    private DbsField fcp_Cons_Plan_Pymnt_Prcss_Seq_Num;
    private DbsField fcp_Cons_Plan_Pymnt_Instmt_Nbr;
    private DbsField fcp_Cons_Plan_Cntl_Check_Dte;
    private DbsField fcp_Cons_Plan_Cntrct_Good_Contract;
    private DbsField fcp_Cons_Plan_Cntrct_Invalid_Cond_Ind;
    private DbsField fcp_Cons_Plan_Cntrct_Multi_Payee;
    private DbsField fcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons;
    private DbsGroup fcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup;
    private DbsField fcp_Cons_Plan_Cntrct_Invalid_Reasons;
    private DbsField fcp_Cons_Plan_Cntrct_Ivc_Amt;
    private DbsField fcp_Cons_Plan_Count_Castplan_Data;
    private DbsGroup fcp_Cons_Plan_Plan_Data;
    private DbsField fcp_Cons_Plan_Plan_Employer_Name;
    private DbsField fcp_Cons_Plan_Plan_Type;
    private DbsField fcp_Cons_Plan_Plan_Cash_Avail;
    private DbsField fcp_Cons_Plan_Plan_Employer_Cntrb;
    private DbsField fcp_Cons_Plan_Plan_Employer_Cntrb_Earn;
    private DbsField fcp_Cons_Plan_Plan_Employee_Rdctn;
    private DbsField fcp_Cons_Plan_Plan_Employee_Rdctn_Earn;
    private DbsField fcp_Cons_Plan_Plan_Employee_Ddctn;
    private DbsField fcp_Cons_Plan_Plan_Employee_Ddctn_Earn;
    private DbsField fcp_Cons_Plan_Plan_Acc_123186;
    private DbsField fcp_Cons_Plan_Plan_Acc_123188;
    private DbsField fcp_Cons_Plan_Plan_Acc_123188_Rdctn;
    private DbsField fcp_Cons_Plan_Plan_Acc_123188_Rdctn_Earn;
    private DbsField fcp_Cons_Plan_Rcrd_Orgn_Ppcn_Prcss_Chkdt;

    public DbsField getExt() { return ext; }

    public DbsGroup getExtRedef1() { return extRedef1; }

    public DbsGroup getExt_Extr() { return ext_Extr; }

    public DbsField getExt_Cntrct_Ppcn_Nbr() { return ext_Cntrct_Ppcn_Nbr; }

    public DbsField getExt_Cntrct_Payee_Cde() { return ext_Cntrct_Payee_Cde; }

    public DbsField getExt_Cntrct_Invrse_Dte() { return ext_Cntrct_Invrse_Dte; }

    public DbsField getExt_Pymnt_Prcss_Seq_Nbr() { return ext_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getExt_Pymnt_Prcss_Seq_NbrRedef2() { return ext_Pymnt_Prcss_Seq_NbrRedef2; }

    public DbsField getExt_Pymnt_Prcss_Seq_Num() { return ext_Pymnt_Prcss_Seq_Num; }

    public DbsField getExt_Pymnt_Instmt_Nbr() { return ext_Pymnt_Instmt_Nbr; }

    public DbsField getExt_Pymnt_Total_Pages() { return ext_Pymnt_Total_Pages; }

    public DbsField getExt_Funds_On_Page() { return ext_Funds_On_Page; }

    public DbsField getExt_Cntrct_Unq_Id_Nbr() { return ext_Cntrct_Unq_Id_Nbr; }

    public DbsField getExt_Cntrct_Cmbn_Nbr() { return ext_Cntrct_Cmbn_Nbr; }

    public DbsField getExt_Pymnt_Record() { return ext_Pymnt_Record; }

    public DbsField getExt_Pymnt_Ivc_Amt() { return ext_Pymnt_Ivc_Amt; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getExt_Annt_Ctznshp_Cde() { return ext_Annt_Ctznshp_Cde; }

    public DbsGroup getExt_Annt_Ctznshp_CdeRedef3() { return ext_Annt_Ctznshp_CdeRedef3; }

    public DbsField getExt_Annt_Ctznshp_Cde_X() { return ext_Annt_Ctznshp_Cde_X; }

    public DbsField getExt_Annt_Locality_Cde() { return ext_Annt_Locality_Cde; }

    public DbsField getExt_Annt_Rsdncy_Cde() { return ext_Annt_Rsdncy_Cde; }

    public DbsField getExt_Annt_Soc_Sec_Ind() { return ext_Annt_Soc_Sec_Ind; }

    public DbsField getExt_Annt_Soc_Sec_Nbr() { return ext_Annt_Soc_Sec_Nbr; }

    public DbsField getExt_Cntrct_Check_Crrncy_Cde() { return ext_Cntrct_Check_Crrncy_Cde; }

    public DbsField getExt_Cntrct_Crrncy_Cde() { return ext_Cntrct_Crrncy_Cde; }

    public DbsField getExt_Cntrct_Orgn_Cde() { return ext_Cntrct_Orgn_Cde; }

    public DbsField getExt_Cntrct_Qlfied_Cde() { return ext_Cntrct_Qlfied_Cde; }

    public DbsField getExt_Cntrct_Pymnt_Type_Ind() { return ext_Cntrct_Pymnt_Type_Ind; }

    public DbsField getExt_Cntrct_Sttlmnt_Type_Ind() { return ext_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getExt_Cntrct_Sps_Cde() { return ext_Cntrct_Sps_Cde; }

    public DbsField getExt_Cntrct_Dvdnd_Payee_Ind() { return ext_Cntrct_Dvdnd_Payee_Ind; }

    public DbsField getExt_Cntrct_Rqst_Settl_Dte() { return ext_Cntrct_Rqst_Settl_Dte; }

    public DbsField getExt_Cntrct_Rqst_Dte() { return ext_Cntrct_Rqst_Dte; }

    public DbsField getExt_Cntrct_Rqst_Settl_Tme() { return ext_Cntrct_Rqst_Settl_Tme; }

    public DbsField getExt_Cntrct_Type_Cde() { return ext_Cntrct_Type_Cde; }

    public DbsField getExt_Cntrct_Lob_Cde() { return ext_Cntrct_Lob_Cde; }

    public DbsField getExt_Cntrct_Sub_Lob_Cde() { return ext_Cntrct_Sub_Lob_Cde; }

    public DbsField getExt_Cntrct_Ia_Lob_Cde() { return ext_Cntrct_Ia_Lob_Cde; }

    public DbsField getExt_Cntrct_Cref_Nbr() { return ext_Cntrct_Cref_Nbr; }

    public DbsField getExt_Cntrct_Option_Cde() { return ext_Cntrct_Option_Cde; }

    public DbsField getExt_Cntrct_Mode_Cde() { return ext_Cntrct_Mode_Cde; }

    public DbsField getExt_Cntrct_Pymnt_Dest_Cde() { return ext_Cntrct_Pymnt_Dest_Cde; }

    public DbsField getExt_Cntrct_Roll_Dest_Cde() { return ext_Cntrct_Roll_Dest_Cde; }

    public DbsField getExt_Cntrct_Dvdnd_Payee_Cde() { return ext_Cntrct_Dvdnd_Payee_Cde; }

    public DbsField getExt_Cntrct_Cancel_Rdrw_Ind() { return ext_Cntrct_Cancel_Rdrw_Ind; }

    public DbsField getExt_Cntrct_Cancel_Rdrw_Actvty_Cde() { return ext_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getExt_Cntrct_Cancel_Rdrw_Amt() { return ext_Cntrct_Cancel_Rdrw_Amt; }

    public DbsField getExt_Cntrct_Hold_Cde() { return ext_Cntrct_Hold_Cde; }

    public DbsGroup getExt_Cntrct_Hold_CdeRedef4() { return ext_Cntrct_Hold_CdeRedef4; }

    public DbsField getExt_Cntrct_Hold_Type() { return ext_Cntrct_Hold_Type; }

    public DbsField getExt_Cntrct_Hold_Dept() { return ext_Cntrct_Hold_Dept; }

    public DbsField getExt_Cntrct_Hold_Grp() { return ext_Cntrct_Hold_Grp; }

    public DbsField getExt_Cntrct_Hold_Ind() { return ext_Cntrct_Hold_Ind; }

    public DbsField getExt_Cntrct_Hold_Tme() { return ext_Cntrct_Hold_Tme; }

    public DbsField getExt_Cntrct_Annty_Ins_Type() { return ext_Cntrct_Annty_Ins_Type; }

    public DbsField getExt_Cntrct_Annty_Type_Cde() { return ext_Cntrct_Annty_Type_Cde; }

    public DbsField getExt_Cntrct_Insurance_Option() { return ext_Cntrct_Insurance_Option; }

    public DbsField getExt_Cntrct_Life_Contingency() { return ext_Cntrct_Life_Contingency; }

    public DbsField getExt_Cntrct_Ac_Lt_10yrs() { return ext_Cntrct_Ac_Lt_10yrs; }

    public DbsField getExt_Cntrct_Da_Tiaa_1_Nbr() { return ext_Cntrct_Da_Tiaa_1_Nbr; }

    public DbsField getExt_Cntrct_Da_Tiaa_2_Nbr() { return ext_Cntrct_Da_Tiaa_2_Nbr; }

    public DbsField getExt_Cntrct_Da_Tiaa_3_Nbr() { return ext_Cntrct_Da_Tiaa_3_Nbr; }

    public DbsField getExt_Cntrct_Da_Tiaa_4_Nbr() { return ext_Cntrct_Da_Tiaa_4_Nbr; }

    public DbsField getExt_Cntrct_Da_Tiaa_5_Nbr() { return ext_Cntrct_Da_Tiaa_5_Nbr; }

    public DbsField getExt_Cntrct_Da_Cref_1_Nbr() { return ext_Cntrct_Da_Cref_1_Nbr; }

    public DbsField getExt_Cntrct_Da_Cref_2_Nbr() { return ext_Cntrct_Da_Cref_2_Nbr; }

    public DbsField getExt_Cntrct_Da_Cref_3_Nbr() { return ext_Cntrct_Da_Cref_3_Nbr; }

    public DbsField getExt_Cntrct_Da_Cref_4_Nbr() { return ext_Cntrct_Da_Cref_4_Nbr; }

    public DbsField getExt_Cntrct_Da_Cref_5_Nbr() { return ext_Cntrct_Da_Cref_5_Nbr; }

    public DbsField getExt_Cntrct_Ec_Oprtr_Id_Nbr() { return ext_Cntrct_Ec_Oprtr_Id_Nbr; }

    public DbsField getExt_Pymnt_Rqst_Rmndr_Pct() { return ext_Pymnt_Rqst_Rmndr_Pct; }

    public DbsField getExt_Pymnt_Stats_Cde() { return ext_Pymnt_Stats_Cde; }

    public DbsField getExt_Pymnt_Annot_Ind() { return ext_Pymnt_Annot_Ind; }

    public DbsField getExt_Pymnt_Cmbne_Ind() { return ext_Pymnt_Cmbne_Ind; }

    public DbsField getExt_Pymnt_Ftre_Ind() { return ext_Pymnt_Ftre_Ind; }

    public DbsField getExt_Pymnt_Payee_Na_Addr_Trggr() { return ext_Pymnt_Payee_Na_Addr_Trggr; }

    public DbsField getExt_Pymnt_Payee_Tx_Elct_Trggr() { return ext_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getExt_Pymnt_Inst_Rep_Cde() { return ext_Pymnt_Inst_Rep_Cde; }

    public DbsField getExt_Pymnt_Pay_Type_Req_Ind() { return ext_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getExt_Pymnt_Split_Cde() { return ext_Pymnt_Split_Cde; }

    public DbsField getExt_Pymnt_Split_Reasn_Cde() { return ext_Pymnt_Split_Reasn_Cde; }

    public DbsField getExt_Pymnt_Check_Dte() { return ext_Pymnt_Check_Dte; }

    public DbsField getExt_Pymnt_Cycle_Dte() { return ext_Pymnt_Cycle_Dte; }

    public DbsField getExt_Pymnt_Eft_Dte() { return ext_Pymnt_Eft_Dte; }

    public DbsField getExt_Pymnt_Rqst_Pct() { return ext_Pymnt_Rqst_Pct; }

    public DbsField getExt_Pymnt_Rqst_Amt() { return ext_Pymnt_Rqst_Amt; }

    public DbsField getExt_Pymnt_Check_Nbr() { return ext_Pymnt_Check_Nbr; }

    public DbsField getExt_Pymnt_Check_Scrty_Nbr() { return ext_Pymnt_Check_Scrty_Nbr; }

    public DbsField getExt_Pymnt_Check_Amt() { return ext_Pymnt_Check_Amt; }

    public DbsField getExt_Pymnt_Settlmnt_Dte() { return ext_Pymnt_Settlmnt_Dte; }

    public DbsField getExt_Pymnt_Check_Seq_Nbr() { return ext_Pymnt_Check_Seq_Nbr; }

    public DbsField getExt_Pymnt_Rprnt_Pndng_Ind() { return ext_Pymnt_Rprnt_Pndng_Ind; }

    public DbsField getExt_Pymnt_Rprnt_Rqust_Dte() { return ext_Pymnt_Rprnt_Rqust_Dte; }

    public DbsField getExt_Pymnt_Rprnt_Dte() { return ext_Pymnt_Rprnt_Dte; }

    public DbsField getExt_Pymnt_Corp_Wpid() { return ext_Pymnt_Corp_Wpid; }

    public DbsField getExt_Pymnt_Reqst_Log_Dte_Time() { return ext_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getExt_Pymnt_Acctg_Dte() { return ext_Pymnt_Acctg_Dte; }

    public DbsField getExt_Pymnt_Intrfce_Dte() { return ext_Pymnt_Intrfce_Dte; }

    public DbsField getExt_Pymnt_Tiaa_Md_Amt() { return ext_Pymnt_Tiaa_Md_Amt; }

    public DbsField getExt_Pymnt_Cref_Md_Amt() { return ext_Pymnt_Cref_Md_Amt; }

    public DbsField getExt_Pymnt_Tax_Form() { return ext_Pymnt_Tax_Form; }

    public DbsGroup getExt_Pymnt_Tax_FormRedef5() { return ext_Pymnt_Tax_FormRedef5; }

    public DbsField getExt_F() { return ext_F; }

    public DbsField getExt_Pymnt_Tax_Form_1001() { return ext_Pymnt_Tax_Form_1001; }

    public DbsField getExt_Pymnt_Tax_Form_1078() { return ext_Pymnt_Tax_Form_1078; }

    public DbsField getExt_Pymnt_Tax_Exempt_Ind() { return ext_Pymnt_Tax_Exempt_Ind; }

    public DbsField getExt_Pymnt_Tax_Calc_Cde() { return ext_Pymnt_Tax_Calc_Cde; }

    public DbsField getExt_Pymnt_Method_Cde() { return ext_Pymnt_Method_Cde; }

    public DbsField getExt_Pymnt_Settl_Ivc_Ind() { return ext_Pymnt_Settl_Ivc_Ind; }

    public DbsField getExt_Pymnt_Ia_Issue_Dte() { return ext_Pymnt_Ia_Issue_Dte; }

    public DbsField getExt_Pymnt_Spouse_Pay_Stats() { return ext_Pymnt_Spouse_Pay_Stats; }

    public DbsField getExt_Pymnt_Trigger_Cde() { return ext_Pymnt_Trigger_Cde; }

    public DbsField getExt_Pymnt_Spcl_Msg_Cde() { return ext_Pymnt_Spcl_Msg_Cde; }

    public DbsField getExt_Pymnt_Eft_Transit_Id() { return ext_Pymnt_Eft_Transit_Id; }

    public DbsField getExt_Pymnt_Eft_Acct_Nbr() { return ext_Pymnt_Eft_Acct_Nbr; }

    public DbsField getExt_Pymnt_Chk_Sav_Ind() { return ext_Pymnt_Chk_Sav_Ind; }

    public DbsField getExt_Pymnt_Deceased_Nme() { return ext_Pymnt_Deceased_Nme; }

    public DbsField getExt_Pymnt_Dob() { return ext_Pymnt_Dob; }

    public DbsField getExt_Pymnt_Death_Dte() { return ext_Pymnt_Death_Dte; }

    public DbsField getExt_Pymnt_Proof_Dte() { return ext_Pymnt_Proof_Dte; }

    public DbsField getExt_Pymnt_Rtb_Dest_Typ() { return ext_Pymnt_Rtb_Dest_Typ; }

    public DbsField getExt_Pymnt_Alt_Addr_Ind() { return ext_Pymnt_Alt_Addr_Ind; }

    public DbsField getExt_Notused1() { return ext_Notused1; }

    public DbsField getExt_Notused2() { return ext_Notused2; }

    public DbsGroup getExt_Notused2Redef6() { return ext_Notused2Redef6; }

    public DbsField getExt_Pymnt_Wrrnt_Trans_Type() { return ext_Pymnt_Wrrnt_Trans_Type; }

    public DbsField getExt_C_Inv_Rtb_Grp() { return ext_C_Inv_Rtb_Grp; }

    public DbsField getExt_C_Pymnt_Ded_Grp() { return ext_C_Pymnt_Ded_Grp; }

    public DbsField getExt_C_Inv_Acct() { return ext_C_Inv_Acct; }

    public DbsField getExt_C_Pymnt_Nme_And_Addr() { return ext_C_Pymnt_Nme_And_Addr; }

    public DbsField getExt_Cnr_Cs_Actvty_Cde() { return ext_Cnr_Cs_Actvty_Cde; }

    public DbsField getExt_Cnr_Cs_Pymnt_Intrfce_Dte() { return ext_Cnr_Cs_Pymnt_Intrfce_Dte; }

    public DbsField getExt_Cnr_Cs_Pymnt_Acctg_Dte() { return ext_Cnr_Cs_Pymnt_Acctg_Dte; }

    public DbsField getExt_Cnr_Cs_Pymnt_Check_Dte() { return ext_Cnr_Cs_Pymnt_Check_Dte; }

    public DbsField getExt_Cnr_Cs_New_Stop_Ind() { return ext_Cnr_Cs_New_Stop_Ind; }

    public DbsField getExt_Cnr_Cs_Rqust_Dte() { return ext_Cnr_Cs_Rqust_Dte; }

    public DbsField getExt_Cnr_Cs_Rqust_Tme() { return ext_Cnr_Cs_Rqust_Tme; }

    public DbsField getExt_Cnr_Cs_Rqust_User_Id() { return ext_Cnr_Cs_Rqust_User_Id; }

    public DbsField getExt_Cnr_Cs_Rqust_Term_Id() { return ext_Cnr_Cs_Rqust_Term_Id; }

    public DbsField getExt_Cnr_Rdrw_Rqust_Tme() { return ext_Cnr_Rdrw_Rqust_Tme; }

    public DbsField getExt_Cnr_Rdrw_Rqust_User_Id() { return ext_Cnr_Rdrw_Rqust_User_Id; }

    public DbsField getExt_Cnr_Orgnl_Invrse_Dte() { return ext_Cnr_Orgnl_Invrse_Dte; }

    public DbsField getExt_Cnr_Orgnl_Prcss_Seq_Nbr() { return ext_Cnr_Orgnl_Prcss_Seq_Nbr; }

    public DbsField getExt_Cnr_Orgnl_Pymnt_Acctg_Dte() { return ext_Cnr_Orgnl_Pymnt_Acctg_Dte; }

    public DbsField getExt_Cnr_Orgnl_Pymnt_Intrfce_Dte() { return ext_Cnr_Orgnl_Pymnt_Intrfce_Dte; }

    public DbsField getExt_Cnr_Rdrw_Rqust_Dte() { return ext_Cnr_Rdrw_Rqust_Dte; }

    public DbsField getExt_Cnr_Rdrw_Rqust_Term_Id() { return ext_Cnr_Rdrw_Rqust_Term_Id; }

    public DbsField getExt_Cnr_Rdrw_Pymnt_Intrfce_Dte() { return ext_Cnr_Rdrw_Pymnt_Intrfce_Dte; }

    public DbsField getExt_Cnr_Rdrw_Pymnt_Acctg_Dte() { return ext_Cnr_Rdrw_Pymnt_Acctg_Dte; }

    public DbsField getExt_Cnr_Rdrw_Pymnt_Check_Dte() { return ext_Cnr_Rdrw_Pymnt_Check_Dte; }

    public DbsField getExt_Cnr_Rplcmnt_Invrse_Dte() { return ext_Cnr_Rplcmnt_Invrse_Dte; }

    public DbsField getExt_Cnr_Rplcmnt_Prcss_Seq_Nbr() { return ext_Cnr_Rplcmnt_Prcss_Seq_Nbr; }

    public DbsField getExt_Rcrd_Typ() { return ext_Rcrd_Typ; }

    public DbsField getExt_Tax_Fed_C_Resp_Cde() { return ext_Tax_Fed_C_Resp_Cde; }

    public DbsField getExt_Tax_Fed_C_Filing_Stat() { return ext_Tax_Fed_C_Filing_Stat; }

    public DbsField getExt_Tax_Fed_C_Allow_Cnt() { return ext_Tax_Fed_C_Allow_Cnt; }

    public DbsField getExt_Tax_Fed_C_Fixed_Amt() { return ext_Tax_Fed_C_Fixed_Amt; }

    public DbsField getExt_Tax_Fed_C_Fixed_Pct() { return ext_Tax_Fed_C_Fixed_Pct; }

    public DbsField getExt_Tax_Sta_C_Resp_Cde() { return ext_Tax_Sta_C_Resp_Cde; }

    public DbsField getExt_Tax_Sta_C_Filing_Stat() { return ext_Tax_Sta_C_Filing_Stat; }

    public DbsField getExt_Tax_Sta_C_Allow_Cnt() { return ext_Tax_Sta_C_Allow_Cnt; }

    public DbsField getExt_Tax_Sta_C_Fixed_Amt() { return ext_Tax_Sta_C_Fixed_Amt; }

    public DbsField getExt_Tax_Sta_C_Fixed_Pct() { return ext_Tax_Sta_C_Fixed_Pct; }

    public DbsField getExt_Tax_Sta_C_Spouse_Cnt() { return ext_Tax_Sta_C_Spouse_Cnt; }

    public DbsField getExt_Tax_Sta_C_Blind_Cnt() { return ext_Tax_Sta_C_Blind_Cnt; }

    public DbsField getExt_Tax_Loc_C_Resp_Cde() { return ext_Tax_Loc_C_Resp_Cde; }

    public DbsField getExt_Tax_Loc_C_Fixed_Amt() { return ext_Tax_Loc_C_Fixed_Amt; }

    public DbsField getExt_Tax_Loc_C_Fixed_Pct() { return ext_Tax_Loc_C_Fixed_Pct; }

    public DbsGroup getExt_Ph_Name() { return ext_Ph_Name; }

    public DbsField getExt_Ph_Last_Name() { return ext_Ph_Last_Name; }

    public DbsField getExt_Ph_First_Name() { return ext_Ph_First_Name; }

    public DbsField getExt_Ph_Middle_Name() { return ext_Ph_Middle_Name; }

    public DbsField getExt_Ph_Sex() { return ext_Ph_Sex; }

    public DbsGroup getExt_Pymnt_Nme_And_Addr_Grp() { return ext_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getExt_Pymnt_Nme() { return ext_Pymnt_Nme; }

    public DbsField getExt_Pymnt_Addr_Line_Txt() { return ext_Pymnt_Addr_Line_Txt; }

    public DbsGroup getExt_Pymnt_Addr_Line_TxtRedef7() { return ext_Pymnt_Addr_Line_TxtRedef7; }

    public DbsField getExt_Pymnt_Addr_Line1_Txt() { return ext_Pymnt_Addr_Line1_Txt; }

    public DbsField getExt_Pymnt_Addr_Line2_Txt() { return ext_Pymnt_Addr_Line2_Txt; }

    public DbsField getExt_Pymnt_Addr_Line3_Txt() { return ext_Pymnt_Addr_Line3_Txt; }

    public DbsField getExt_Pymnt_Addr_Line4_Txt() { return ext_Pymnt_Addr_Line4_Txt; }

    public DbsField getExt_Pymnt_Addr_Line5_Txt() { return ext_Pymnt_Addr_Line5_Txt; }

    public DbsField getExt_Pymnt_Addr_Line6_Txt() { return ext_Pymnt_Addr_Line6_Txt; }

    public DbsField getExt_Pymnt_Addr_Zip_Cde() { return ext_Pymnt_Addr_Zip_Cde; }

    public DbsField getExt_Pymnt_Postl_Data() { return ext_Pymnt_Postl_Data; }

    public DbsField getExt_Pymnt_Addr_Type_Ind() { return ext_Pymnt_Addr_Type_Ind; }

    public DbsField getExt_Pymnt_Addr_Last_Chg_Dte() { return ext_Pymnt_Addr_Last_Chg_Dte; }

    public DbsField getExt_Pymnt_Addr_Last_Chg_Tme() { return ext_Pymnt_Addr_Last_Chg_Tme; }

    public DbsField getExt_Pymnt_Addr_Chg_Ind() { return ext_Pymnt_Addr_Chg_Ind; }

    public DbsGroup getExt_Pymnt_Ded_Grp() { return ext_Pymnt_Ded_Grp; }

    public DbsField getExt_Pymnt_Ded_Cde() { return ext_Pymnt_Ded_Cde; }

    public DbsField getExt_Pymnt_Ded_Payee_Cde() { return ext_Pymnt_Ded_Payee_Cde; }

    public DbsField getExt_Pymnt_Ded_Amt() { return ext_Pymnt_Ded_Amt; }

    public DbsField getExt_Egtrra_Eligibility_Ind() { return ext_Egtrra_Eligibility_Ind; }

    public DbsField getExt_Originating_Irc_Cde() { return ext_Originating_Irc_Cde; }

    public DbsGroup getExt_Originating_Irc_CdeRedef8() { return ext_Originating_Irc_CdeRedef8; }

    public DbsField getExt_Distributing_Irc_Cde() { return ext_Distributing_Irc_Cde; }

    public DbsField getExt_Receiving_Irc_Cde() { return ext_Receiving_Irc_Cde; }

    public DbsField getExt_Tpa_Filler() { return ext_Tpa_Filler; }

    public DbsGroup getExt_Inv_Acct() { return ext_Inv_Acct; }

    public DbsField getExt_Inv_Acct_Cde() { return ext_Inv_Acct_Cde; }

    public DbsGroup getExt_Inv_Acct_CdeRedef9() { return ext_Inv_Acct_CdeRedef9; }

    public DbsField getExt_Inv_Acct_Cde_N() { return ext_Inv_Acct_Cde_N; }

    public DbsField getExt_Inv_Acct_Ded_Cde() { return ext_Inv_Acct_Ded_Cde; }

    public DbsField getExt_Inv_Acct_Ded_Amt() { return ext_Inv_Acct_Ded_Amt; }

    public DbsField getExt_Inv_Acct_Company() { return ext_Inv_Acct_Company; }

    public DbsField getExt_Inv_Acct_Unit_Qty() { return ext_Inv_Acct_Unit_Qty; }

    public DbsField getExt_Inv_Acct_Unit_Value() { return ext_Inv_Acct_Unit_Value; }

    public DbsField getExt_Inv_Acct_Settl_Amt() { return ext_Inv_Acct_Settl_Amt; }

    public DbsField getExt_Inv_Acct_Fed_Cde() { return ext_Inv_Acct_Fed_Cde; }

    public DbsField getExt_Inv_Acct_State_Cde() { return ext_Inv_Acct_State_Cde; }

    public DbsField getExt_Inv_Acct_Local_Cde() { return ext_Inv_Acct_Local_Cde; }

    public DbsField getExt_Inv_Acct_Ivc_Amt() { return ext_Inv_Acct_Ivc_Amt; }

    public DbsField getExt_Inv_Acct_Dci_Amt() { return ext_Inv_Acct_Dci_Amt; }

    public DbsField getExt_Inv_Acct_Dpi_Amt() { return ext_Inv_Acct_Dpi_Amt; }

    public DbsField getExt_Inv_Acct_Start_Accum_Amt() { return ext_Inv_Acct_Start_Accum_Amt; }

    public DbsField getExt_Inv_Acct_End_Accum_Amt() { return ext_Inv_Acct_End_Accum_Amt; }

    public DbsField getExt_Inv_Acct_Dvdnd_Amt() { return ext_Inv_Acct_Dvdnd_Amt; }

    public DbsField getExt_Inv_Acct_Net_Pymnt_Amt() { return ext_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getExt_Inv_Acct_Ivc_Ind() { return ext_Inv_Acct_Ivc_Ind; }

    public DbsField getExt_Inv_Acct_Adj_Ivc_Amt() { return ext_Inv_Acct_Adj_Ivc_Amt; }

    public DbsField getExt_Inv_Acct_Valuat_Period() { return ext_Inv_Acct_Valuat_Period; }

    public DbsField getExt_Inv_Acct_Fdrl_Tax_Amt() { return ext_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getExt_Inv_Acct_State_Tax_Amt() { return ext_Inv_Acct_State_Tax_Amt; }

    public DbsField getExt_Inv_Acct_Local_Tax_Amt() { return ext_Inv_Acct_Local_Tax_Amt; }

    public DbsField getExt_Inv_Acct_Exp_Amt() { return ext_Inv_Acct_Exp_Amt; }

    public DbsGroup getExt_Inv_Rtb_Grp() { return ext_Inv_Rtb_Grp; }

    public DbsField getExt_Inv_Rtb_Cde() { return ext_Inv_Rtb_Cde; }

    public DbsField getExt_Inv_Acct_Fed_Dci_Tax_Amt() { return ext_Inv_Acct_Fed_Dci_Tax_Amt; }

    public DbsField getExt_Inv_Acct_Sta_Dci_Tax_Amt() { return ext_Inv_Acct_Sta_Dci_Tax_Amt; }

    public DbsField getExt_Inv_Acct_Loc_Dci_Tax_Amt() { return ext_Inv_Acct_Loc_Dci_Tax_Amt; }

    public DbsField getExt_Inv_Acct_Fed_Dpi_Tax_Amt() { return ext_Inv_Acct_Fed_Dpi_Tax_Amt; }

    public DbsField getExt_Inv_Acct_Sta_Dpi_Tax_Amt() { return ext_Inv_Acct_Sta_Dpi_Tax_Amt; }

    public DbsField getExt_Inv_Acct_Loc_Dpi_Tax_Amt() { return ext_Inv_Acct_Loc_Dpi_Tax_Amt; }

    public DataAccessProgramView getVw_fcp_Cons_Plan() { return vw_fcp_Cons_Plan; }

    public DbsGroup getFcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data() { return fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data; }

    public DbsField getFcp_Cons_Plan_Cntrct_Rcrd_Typ() { return fcp_Cons_Plan_Cntrct_Rcrd_Typ; }

    public DbsField getFcp_Cons_Plan_Cntrct_Orgn_Cde() { return fcp_Cons_Plan_Cntrct_Orgn_Cde; }

    public DbsField getFcp_Cons_Plan_Cntrct_Ppcn_Nbr() { return fcp_Cons_Plan_Cntrct_Ppcn_Nbr; }

    public DbsField getFcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr() { return fcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getFcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef10() { return fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef10; }

    public DbsField getFcp_Cons_Plan_Pymnt_Prcss_Seq_Num() { return fcp_Cons_Plan_Pymnt_Prcss_Seq_Num; }

    public DbsField getFcp_Cons_Plan_Pymnt_Instmt_Nbr() { return fcp_Cons_Plan_Pymnt_Instmt_Nbr; }

    public DbsField getFcp_Cons_Plan_Cntl_Check_Dte() { return fcp_Cons_Plan_Cntl_Check_Dte; }

    public DbsField getFcp_Cons_Plan_Cntrct_Good_Contract() { return fcp_Cons_Plan_Cntrct_Good_Contract; }

    public DbsField getFcp_Cons_Plan_Cntrct_Invalid_Cond_Ind() { return fcp_Cons_Plan_Cntrct_Invalid_Cond_Ind; }

    public DbsField getFcp_Cons_Plan_Cntrct_Multi_Payee() { return fcp_Cons_Plan_Cntrct_Multi_Payee; }

    public DbsField getFcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons() { return fcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons; }

    public DbsGroup getFcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup() { return fcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup; }

    public DbsField getFcp_Cons_Plan_Cntrct_Invalid_Reasons() { return fcp_Cons_Plan_Cntrct_Invalid_Reasons; }

    public DbsField getFcp_Cons_Plan_Cntrct_Ivc_Amt() { return fcp_Cons_Plan_Cntrct_Ivc_Amt; }

    public DbsField getFcp_Cons_Plan_Count_Castplan_Data() { return fcp_Cons_Plan_Count_Castplan_Data; }

    public DbsGroup getFcp_Cons_Plan_Plan_Data() { return fcp_Cons_Plan_Plan_Data; }

    public DbsField getFcp_Cons_Plan_Plan_Employer_Name() { return fcp_Cons_Plan_Plan_Employer_Name; }

    public DbsField getFcp_Cons_Plan_Plan_Type() { return fcp_Cons_Plan_Plan_Type; }

    public DbsField getFcp_Cons_Plan_Plan_Cash_Avail() { return fcp_Cons_Plan_Plan_Cash_Avail; }

    public DbsField getFcp_Cons_Plan_Plan_Employer_Cntrb() { return fcp_Cons_Plan_Plan_Employer_Cntrb; }

    public DbsField getFcp_Cons_Plan_Plan_Employer_Cntrb_Earn() { return fcp_Cons_Plan_Plan_Employer_Cntrb_Earn; }

    public DbsField getFcp_Cons_Plan_Plan_Employee_Rdctn() { return fcp_Cons_Plan_Plan_Employee_Rdctn; }

    public DbsField getFcp_Cons_Plan_Plan_Employee_Rdctn_Earn() { return fcp_Cons_Plan_Plan_Employee_Rdctn_Earn; }

    public DbsField getFcp_Cons_Plan_Plan_Employee_Ddctn() { return fcp_Cons_Plan_Plan_Employee_Ddctn; }

    public DbsField getFcp_Cons_Plan_Plan_Employee_Ddctn_Earn() { return fcp_Cons_Plan_Plan_Employee_Ddctn_Earn; }

    public DbsField getFcp_Cons_Plan_Plan_Acc_123186() { return fcp_Cons_Plan_Plan_Acc_123186; }

    public DbsField getFcp_Cons_Plan_Plan_Acc_123188() { return fcp_Cons_Plan_Plan_Acc_123188; }

    public DbsField getFcp_Cons_Plan_Plan_Acc_123188_Rdctn() { return fcp_Cons_Plan_Plan_Acc_123188_Rdctn; }

    public DbsField getFcp_Cons_Plan_Plan_Acc_123188_Rdctn_Earn() { return fcp_Cons_Plan_Plan_Acc_123188_Rdctn_Earn; }

    public DbsField getFcp_Cons_Plan_Rcrd_Orgn_Ppcn_Prcss_Chkdt() { return fcp_Cons_Plan_Rcrd_Orgn_Ppcn_Prcss_Chkdt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ext = newFieldArrayInRecord("ext", "EXT", FieldType.STRING, 244, new DbsArrayController(1,25));
        extRedef1 = newGroupInRecord("extRedef1", "Redefines", ext);
        ext_Extr = extRedef1.newGroupInGroup("ext_Extr", "EXTR");
        ext_Cntrct_Ppcn_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        ext_Cntrct_Payee_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        ext_Cntrct_Invrse_Dte = ext_Extr.newFieldInGroup("ext_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        ext_Pymnt_Prcss_Seq_Nbr = ext_Extr.newFieldInGroup("ext_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        ext_Pymnt_Prcss_Seq_NbrRedef2 = ext_Extr.newGroupInGroup("ext_Pymnt_Prcss_Seq_NbrRedef2", "Redefines", ext_Pymnt_Prcss_Seq_Nbr);
        ext_Pymnt_Prcss_Seq_Num = ext_Pymnt_Prcss_Seq_NbrRedef2.newFieldInGroup("ext_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        ext_Pymnt_Instmt_Nbr = ext_Pymnt_Prcss_Seq_NbrRedef2.newFieldInGroup("ext_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        ext_Pymnt_Total_Pages = ext_Extr.newFieldInGroup("ext_Pymnt_Total_Pages", "PYMNT-TOTAL-PAGES", FieldType.PACKED_DECIMAL, 3);
        ext_Funds_On_Page = ext_Extr.newFieldArrayInGroup("ext_Funds_On_Page", "FUNDS-ON-PAGE", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1,20));
        ext_Cntrct_Unq_Id_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        ext_Cntrct_Cmbn_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        ext_Pymnt_Record = ext_Extr.newFieldInGroup("ext_Pymnt_Record", "PYMNT-RECORD", FieldType.PACKED_DECIMAL, 3);
        ext_Pymnt_Ivc_Amt = ext_Extr.newFieldInGroup("ext_Pymnt_Ivc_Amt", "PYMNT-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        filler01 = ext_Extr.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 25);
        ext_Annt_Ctznshp_Cde = ext_Extr.newFieldInGroup("ext_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2);
        ext_Annt_Ctznshp_CdeRedef3 = ext_Extr.newGroupInGroup("ext_Annt_Ctznshp_CdeRedef3", "Redefines", ext_Annt_Ctznshp_Cde);
        ext_Annt_Ctznshp_Cde_X = ext_Annt_Ctznshp_CdeRedef3.newFieldInGroup("ext_Annt_Ctznshp_Cde_X", "ANNT-CTZNSHP-CDE-X", FieldType.STRING, 2);
        ext_Annt_Locality_Cde = ext_Extr.newFieldInGroup("ext_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", FieldType.STRING, 2);
        ext_Annt_Rsdncy_Cde = ext_Extr.newFieldInGroup("ext_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        ext_Annt_Soc_Sec_Ind = ext_Extr.newFieldInGroup("ext_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1);
        ext_Annt_Soc_Sec_Nbr = ext_Extr.newFieldInGroup("ext_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        ext_Cntrct_Check_Crrncy_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        ext_Cntrct_Crrncy_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 1);
        ext_Cntrct_Orgn_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        ext_Cntrct_Qlfied_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 1);
        ext_Cntrct_Pymnt_Type_Ind = ext_Extr.newFieldInGroup("ext_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        ext_Cntrct_Sttlmnt_Type_Ind = ext_Extr.newFieldInGroup("ext_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        ext_Cntrct_Sps_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", FieldType.STRING, 1);
        ext_Cntrct_Dvdnd_Payee_Ind = ext_Extr.newFieldInGroup("ext_Cntrct_Dvdnd_Payee_Ind", "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 1);
        ext_Cntrct_Rqst_Settl_Dte = ext_Extr.newFieldInGroup("ext_Cntrct_Rqst_Settl_Dte", "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        ext_Cntrct_Rqst_Dte = ext_Extr.newFieldInGroup("ext_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", FieldType.DATE);
        ext_Cntrct_Rqst_Settl_Tme = ext_Extr.newFieldInGroup("ext_Cntrct_Rqst_Settl_Tme", "CNTRCT-RQST-SETTL-TME", FieldType.TIME);
        ext_Cntrct_Type_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        ext_Cntrct_Lob_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        ext_Cntrct_Sub_Lob_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        ext_Cntrct_Ia_Lob_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 2);
        ext_Cntrct_Cref_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10);
        ext_Cntrct_Option_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        ext_Cntrct_Mode_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        ext_Cntrct_Pymnt_Dest_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        ext_Cntrct_Roll_Dest_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        ext_Cntrct_Dvdnd_Payee_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        ext_Cntrct_Cancel_Rdrw_Ind = ext_Extr.newFieldInGroup("ext_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        ext_Cntrct_Cancel_Rdrw_Actvty_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2);
        ext_Cntrct_Cancel_Rdrw_Amt = ext_Extr.newFieldInGroup("ext_Cntrct_Cancel_Rdrw_Amt", "CNTRCT-CANCEL-RDRW-AMT", FieldType.PACKED_DECIMAL, 11,2);
        ext_Cntrct_Hold_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        ext_Cntrct_Hold_CdeRedef4 = ext_Extr.newGroupInGroup("ext_Cntrct_Hold_CdeRedef4", "Redefines", ext_Cntrct_Hold_Cde);
        ext_Cntrct_Hold_Type = ext_Cntrct_Hold_CdeRedef4.newFieldInGroup("ext_Cntrct_Hold_Type", "CNTRCT-HOLD-TYPE", FieldType.STRING, 2);
        ext_Cntrct_Hold_Dept = ext_Cntrct_Hold_CdeRedef4.newFieldInGroup("ext_Cntrct_Hold_Dept", "CNTRCT-HOLD-DEPT", FieldType.STRING, 2);
        ext_Cntrct_Hold_Grp = ext_Extr.newFieldInGroup("ext_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3);
        ext_Cntrct_Hold_Ind = ext_Extr.newFieldInGroup("ext_Cntrct_Hold_Ind", "CNTRCT-HOLD-IND", FieldType.STRING, 1);
        ext_Cntrct_Hold_Tme = ext_Extr.newFieldInGroup("ext_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME);
        ext_Cntrct_Annty_Ins_Type = ext_Extr.newFieldInGroup("ext_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        ext_Cntrct_Annty_Type_Cde = ext_Extr.newFieldInGroup("ext_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        ext_Cntrct_Insurance_Option = ext_Extr.newFieldInGroup("ext_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 1);
        ext_Cntrct_Life_Contingency = ext_Extr.newFieldInGroup("ext_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 1);
        ext_Cntrct_Ac_Lt_10yrs = ext_Extr.newFieldInGroup("ext_Cntrct_Ac_Lt_10yrs", "CNTRCT-AC-LT-10YRS", FieldType.BOOLEAN);
        ext_Cntrct_Da_Tiaa_1_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Da_Tiaa_1_Nbr", "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_2_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Da_Tiaa_2_Nbr", "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_3_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Da_Tiaa_3_Nbr", "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_4_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Da_Tiaa_4_Nbr", "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_5_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Da_Tiaa_5_Nbr", "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Cref_1_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Cref_2_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Cref_3_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Da_Cref_3_Nbr", "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Cref_4_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Da_Cref_4_Nbr", "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Cref_5_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Da_Cref_5_Nbr", "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 8);
        ext_Cntrct_Ec_Oprtr_Id_Nbr = ext_Extr.newFieldInGroup("ext_Cntrct_Ec_Oprtr_Id_Nbr", "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 7);
        ext_Pymnt_Rqst_Rmndr_Pct = ext_Extr.newFieldInGroup("ext_Pymnt_Rqst_Rmndr_Pct", "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1);
        ext_Pymnt_Stats_Cde = ext_Extr.newFieldInGroup("ext_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1);
        ext_Pymnt_Annot_Ind = ext_Extr.newFieldInGroup("ext_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 1);
        ext_Pymnt_Cmbne_Ind = ext_Extr.newFieldInGroup("ext_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 1);
        ext_Pymnt_Ftre_Ind = ext_Extr.newFieldInGroup("ext_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", FieldType.STRING, 1);
        ext_Pymnt_Payee_Na_Addr_Trggr = ext_Extr.newFieldInGroup("ext_Pymnt_Payee_Na_Addr_Trggr", "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 1);
        ext_Pymnt_Payee_Tx_Elct_Trggr = ext_Extr.newFieldInGroup("ext_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        ext_Pymnt_Inst_Rep_Cde = ext_Extr.newFieldInGroup("ext_Pymnt_Inst_Rep_Cde", "PYMNT-INST-REP-CDE", FieldType.STRING, 1);
        ext_Pymnt_Pay_Type_Req_Ind = ext_Extr.newFieldInGroup("ext_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        ext_Pymnt_Split_Cde = ext_Extr.newFieldInGroup("ext_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", FieldType.STRING, 2);
        ext_Pymnt_Split_Reasn_Cde = ext_Extr.newFieldInGroup("ext_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);
        ext_Pymnt_Check_Dte = ext_Extr.newFieldInGroup("ext_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        ext_Pymnt_Cycle_Dte = ext_Extr.newFieldInGroup("ext_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        ext_Pymnt_Eft_Dte = ext_Extr.newFieldInGroup("ext_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        ext_Pymnt_Rqst_Pct = ext_Extr.newFieldInGroup("ext_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", FieldType.PACKED_DECIMAL, 3);
        ext_Pymnt_Rqst_Amt = ext_Extr.newFieldInGroup("ext_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", FieldType.PACKED_DECIMAL, 11,2);
        ext_Pymnt_Check_Nbr = ext_Extr.newFieldInGroup("ext_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        ext_Pymnt_Check_Scrty_Nbr = ext_Extr.newFieldInGroup("ext_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        ext_Pymnt_Check_Amt = ext_Extr.newFieldInGroup("ext_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Pymnt_Settlmnt_Dte = ext_Extr.newFieldInGroup("ext_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        ext_Pymnt_Check_Seq_Nbr = ext_Extr.newFieldInGroup("ext_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        ext_Pymnt_Rprnt_Pndng_Ind = ext_Extr.newFieldInGroup("ext_Pymnt_Rprnt_Pndng_Ind", "PYMNT-RPRNT-PNDNG-IND", FieldType.STRING, 1);
        ext_Pymnt_Rprnt_Rqust_Dte = ext_Extr.newFieldInGroup("ext_Pymnt_Rprnt_Rqust_Dte", "PYMNT-RPRNT-RQUST-DTE", FieldType.DATE);
        ext_Pymnt_Rprnt_Dte = ext_Extr.newFieldInGroup("ext_Pymnt_Rprnt_Dte", "PYMNT-RPRNT-DTE", FieldType.DATE);
        ext_Pymnt_Corp_Wpid = ext_Extr.newFieldInGroup("ext_Pymnt_Corp_Wpid", "PYMNT-CORP-WPID", FieldType.STRING, 6);
        ext_Pymnt_Reqst_Log_Dte_Time = ext_Extr.newFieldInGroup("ext_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        ext_Pymnt_Acctg_Dte = ext_Extr.newFieldInGroup("ext_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        ext_Pymnt_Intrfce_Dte = ext_Extr.newFieldInGroup("ext_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        ext_Pymnt_Tiaa_Md_Amt = ext_Extr.newFieldInGroup("ext_Pymnt_Tiaa_Md_Amt", "PYMNT-TIAA-MD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        ext_Pymnt_Cref_Md_Amt = ext_Extr.newFieldInGroup("ext_Pymnt_Cref_Md_Amt", "PYMNT-CREF-MD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        ext_Pymnt_Tax_Form = ext_Extr.newFieldInGroup("ext_Pymnt_Tax_Form", "PYMNT-TAX-FORM", FieldType.STRING, 4);
        ext_Pymnt_Tax_FormRedef5 = ext_Extr.newGroupInGroup("ext_Pymnt_Tax_FormRedef5", "Redefines", ext_Pymnt_Tax_Form);
        ext_F = ext_Pymnt_Tax_FormRedef5.newFieldInGroup("ext_F", "F", FieldType.STRING, 2);
        ext_Pymnt_Tax_Form_1001 = ext_Pymnt_Tax_FormRedef5.newFieldInGroup("ext_Pymnt_Tax_Form_1001", "PYMNT-TAX-FORM-1001", FieldType.STRING, 1);
        ext_Pymnt_Tax_Form_1078 = ext_Pymnt_Tax_FormRedef5.newFieldInGroup("ext_Pymnt_Tax_Form_1078", "PYMNT-TAX-FORM-1078", FieldType.STRING, 1);
        ext_Pymnt_Tax_Exempt_Ind = ext_Extr.newFieldInGroup("ext_Pymnt_Tax_Exempt_Ind", "PYMNT-TAX-EXEMPT-IND", FieldType.STRING, 1);
        ext_Pymnt_Tax_Calc_Cde = ext_Extr.newFieldInGroup("ext_Pymnt_Tax_Calc_Cde", "PYMNT-TAX-CALC-CDE", FieldType.STRING, 2);
        ext_Pymnt_Method_Cde = ext_Extr.newFieldInGroup("ext_Pymnt_Method_Cde", "PYMNT-METHOD-CDE", FieldType.STRING, 1);
        ext_Pymnt_Settl_Ivc_Ind = ext_Extr.newFieldInGroup("ext_Pymnt_Settl_Ivc_Ind", "PYMNT-SETTL-IVC-IND", FieldType.STRING, 1);
        ext_Pymnt_Ia_Issue_Dte = ext_Extr.newFieldInGroup("ext_Pymnt_Ia_Issue_Dte", "PYMNT-IA-ISSUE-DTE", FieldType.DATE);
        ext_Pymnt_Spouse_Pay_Stats = ext_Extr.newFieldInGroup("ext_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", FieldType.STRING, 1);
        ext_Pymnt_Trigger_Cde = ext_Extr.newFieldInGroup("ext_Pymnt_Trigger_Cde", "PYMNT-TRIGGER-CDE", FieldType.STRING, 1);
        ext_Pymnt_Spcl_Msg_Cde = ext_Extr.newFieldInGroup("ext_Pymnt_Spcl_Msg_Cde", "PYMNT-SPCL-MSG-CDE", FieldType.STRING, 3);
        ext_Pymnt_Eft_Transit_Id = ext_Extr.newFieldInGroup("ext_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9);
        ext_Pymnt_Eft_Acct_Nbr = ext_Extr.newFieldInGroup("ext_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        ext_Pymnt_Chk_Sav_Ind = ext_Extr.newFieldInGroup("ext_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 1);
        ext_Pymnt_Deceased_Nme = ext_Extr.newFieldInGroup("ext_Pymnt_Deceased_Nme", "PYMNT-DECEASED-NME", FieldType.STRING, 38);
        ext_Pymnt_Dob = ext_Extr.newFieldInGroup("ext_Pymnt_Dob", "PYMNT-DOB", FieldType.DATE);
        ext_Pymnt_Death_Dte = ext_Extr.newFieldInGroup("ext_Pymnt_Death_Dte", "PYMNT-DEATH-DTE", FieldType.DATE);
        ext_Pymnt_Proof_Dte = ext_Extr.newFieldInGroup("ext_Pymnt_Proof_Dte", "PYMNT-PROOF-DTE", FieldType.DATE);
        ext_Pymnt_Rtb_Dest_Typ = ext_Extr.newFieldInGroup("ext_Pymnt_Rtb_Dest_Typ", "PYMNT-RTB-DEST-TYP", FieldType.STRING, 4);
        ext_Pymnt_Alt_Addr_Ind = ext_Extr.newFieldInGroup("ext_Pymnt_Alt_Addr_Ind", "PYMNT-ALT-ADDR-IND", FieldType.BOOLEAN);
        ext_Notused1 = ext_Extr.newFieldInGroup("ext_Notused1", "NOTUSED1", FieldType.STRING, 8);
        ext_Notused2 = ext_Extr.newFieldInGroup("ext_Notused2", "NOTUSED2", FieldType.STRING, 8);
        ext_Notused2Redef6 = ext_Extr.newGroupInGroup("ext_Notused2Redef6", "Redefines", ext_Notused2);
        ext_Pymnt_Wrrnt_Trans_Type = ext_Notused2Redef6.newFieldInGroup("ext_Pymnt_Wrrnt_Trans_Type", "PYMNT-WRRNT-TRANS-TYPE", FieldType.STRING, 2);
        ext_C_Inv_Rtb_Grp = ext_Extr.newFieldInGroup("ext_C_Inv_Rtb_Grp", "C-INV-RTB-GRP", FieldType.NUMERIC, 2);
        ext_C_Pymnt_Ded_Grp = ext_Extr.newFieldInGroup("ext_C_Pymnt_Ded_Grp", "C-PYMNT-DED-GRP", FieldType.NUMERIC, 2);
        ext_C_Inv_Acct = ext_Extr.newFieldInGroup("ext_C_Inv_Acct", "C-INV-ACCT", FieldType.NUMERIC, 2);
        ext_C_Pymnt_Nme_And_Addr = ext_Extr.newFieldInGroup("ext_C_Pymnt_Nme_And_Addr", "C-PYMNT-NME-AND-ADDR", FieldType.NUMERIC, 1);
        ext_Cnr_Cs_Actvty_Cde = ext_Extr.newFieldInGroup("ext_Cnr_Cs_Actvty_Cde", "CNR-CS-ACTVTY-CDE", FieldType.STRING, 1);
        ext_Cnr_Cs_Pymnt_Intrfce_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Cs_Pymnt_Intrfce_Dte", "CNR-CS-PYMNT-INTRFCE-DTE", FieldType.DATE);
        ext_Cnr_Cs_Pymnt_Acctg_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", FieldType.DATE);
        ext_Cnr_Cs_Pymnt_Check_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Cs_Pymnt_Check_Dte", "CNR-CS-PYMNT-CHECK-DTE", FieldType.DATE);
        ext_Cnr_Cs_New_Stop_Ind = ext_Extr.newFieldInGroup("ext_Cnr_Cs_New_Stop_Ind", "CNR-CS-NEW-STOP-IND", FieldType.STRING, 1);
        ext_Cnr_Cs_Rqust_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Cs_Rqust_Dte", "CNR-CS-RQUST-DTE", FieldType.DATE);
        ext_Cnr_Cs_Rqust_Tme = ext_Extr.newFieldInGroup("ext_Cnr_Cs_Rqust_Tme", "CNR-CS-RQUST-TME", FieldType.NUMERIC, 7);
        ext_Cnr_Cs_Rqust_User_Id = ext_Extr.newFieldInGroup("ext_Cnr_Cs_Rqust_User_Id", "CNR-CS-RQUST-USER-ID", FieldType.STRING, 8);
        ext_Cnr_Cs_Rqust_Term_Id = ext_Extr.newFieldInGroup("ext_Cnr_Cs_Rqust_Term_Id", "CNR-CS-RQUST-TERM-ID", FieldType.STRING, 8);
        ext_Cnr_Rdrw_Rqust_Tme = ext_Extr.newFieldInGroup("ext_Cnr_Rdrw_Rqust_Tme", "CNR-RDRW-RQUST-TME", FieldType.NUMERIC, 7);
        ext_Cnr_Rdrw_Rqust_User_Id = ext_Extr.newFieldInGroup("ext_Cnr_Rdrw_Rqust_User_Id", "CNR-RDRW-RQUST-USER-ID", FieldType.STRING, 8);
        ext_Cnr_Orgnl_Invrse_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        ext_Cnr_Orgnl_Prcss_Seq_Nbr = ext_Extr.newFieldInGroup("ext_Cnr_Orgnl_Prcss_Seq_Nbr", "CNR-ORGNL-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        ext_Cnr_Orgnl_Pymnt_Acctg_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Orgnl_Pymnt_Acctg_Dte", "CNR-ORGNL-PYMNT-ACCTG-DTE", FieldType.DATE);
        ext_Cnr_Orgnl_Pymnt_Intrfce_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Orgnl_Pymnt_Intrfce_Dte", "CNR-ORGNL-PYMNT-INTRFCE-DTE", FieldType.DATE);
        ext_Cnr_Rdrw_Rqust_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Rdrw_Rqust_Dte", "CNR-RDRW-RQUST-DTE", FieldType.DATE);
        ext_Cnr_Rdrw_Rqust_Term_Id = ext_Extr.newFieldInGroup("ext_Cnr_Rdrw_Rqust_Term_Id", "CNR-RDRW-RQUST-TERM-ID", FieldType.STRING, 8);
        ext_Cnr_Rdrw_Pymnt_Intrfce_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Rdrw_Pymnt_Intrfce_Dte", "CNR-RDRW-PYMNT-INTRFCE-DTE", FieldType.DATE);
        ext_Cnr_Rdrw_Pymnt_Acctg_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Rdrw_Pymnt_Acctg_Dte", "CNR-RDRW-PYMNT-ACCTG-DTE", FieldType.DATE);
        ext_Cnr_Rdrw_Pymnt_Check_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Rdrw_Pymnt_Check_Dte", "CNR-RDRW-PYMNT-CHECK-DTE", FieldType.DATE);
        ext_Cnr_Rplcmnt_Invrse_Dte = ext_Extr.newFieldInGroup("ext_Cnr_Rplcmnt_Invrse_Dte", "CNR-RPLCMNT-INVRSE-DTE", FieldType.NUMERIC, 8);
        ext_Cnr_Rplcmnt_Prcss_Seq_Nbr = ext_Extr.newFieldInGroup("ext_Cnr_Rplcmnt_Prcss_Seq_Nbr", "CNR-RPLCMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        ext_Rcrd_Typ = ext_Extr.newFieldInGroup("ext_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 1);
        ext_Tax_Fed_C_Resp_Cde = ext_Extr.newFieldInGroup("ext_Tax_Fed_C_Resp_Cde", "TAX-FED-C-RESP-CDE", FieldType.STRING, 1);
        ext_Tax_Fed_C_Filing_Stat = ext_Extr.newFieldInGroup("ext_Tax_Fed_C_Filing_Stat", "TAX-FED-C-FILING-STAT", FieldType.STRING, 1);
        ext_Tax_Fed_C_Allow_Cnt = ext_Extr.newFieldInGroup("ext_Tax_Fed_C_Allow_Cnt", "TAX-FED-C-ALLOW-CNT", FieldType.PACKED_DECIMAL, 2);
        ext_Tax_Fed_C_Fixed_Amt = ext_Extr.newFieldInGroup("ext_Tax_Fed_C_Fixed_Amt", "TAX-FED-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Tax_Fed_C_Fixed_Pct = ext_Extr.newFieldInGroup("ext_Tax_Fed_C_Fixed_Pct", "TAX-FED-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 7,4);
        ext_Tax_Sta_C_Resp_Cde = ext_Extr.newFieldInGroup("ext_Tax_Sta_C_Resp_Cde", "TAX-STA-C-RESP-CDE", FieldType.STRING, 1);
        ext_Tax_Sta_C_Filing_Stat = ext_Extr.newFieldInGroup("ext_Tax_Sta_C_Filing_Stat", "TAX-STA-C-FILING-STAT", FieldType.STRING, 1);
        ext_Tax_Sta_C_Allow_Cnt = ext_Extr.newFieldInGroup("ext_Tax_Sta_C_Allow_Cnt", "TAX-STA-C-ALLOW-CNT", FieldType.PACKED_DECIMAL, 2);
        ext_Tax_Sta_C_Fixed_Amt = ext_Extr.newFieldInGroup("ext_Tax_Sta_C_Fixed_Amt", "TAX-STA-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Tax_Sta_C_Fixed_Pct = ext_Extr.newFieldInGroup("ext_Tax_Sta_C_Fixed_Pct", "TAX-STA-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 7,4);
        ext_Tax_Sta_C_Spouse_Cnt = ext_Extr.newFieldInGroup("ext_Tax_Sta_C_Spouse_Cnt", "TAX-STA-C-SPOUSE-CNT", FieldType.NUMERIC, 1);
        ext_Tax_Sta_C_Blind_Cnt = ext_Extr.newFieldInGroup("ext_Tax_Sta_C_Blind_Cnt", "TAX-STA-C-BLIND-CNT", FieldType.NUMERIC, 1);
        ext_Tax_Loc_C_Resp_Cde = ext_Extr.newFieldInGroup("ext_Tax_Loc_C_Resp_Cde", "TAX-LOC-C-RESP-CDE", FieldType.STRING, 1);
        ext_Tax_Loc_C_Fixed_Amt = ext_Extr.newFieldInGroup("ext_Tax_Loc_C_Fixed_Amt", "TAX-LOC-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Tax_Loc_C_Fixed_Pct = ext_Extr.newFieldInGroup("ext_Tax_Loc_C_Fixed_Pct", "TAX-LOC-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 7,4);
        ext_Ph_Name = ext_Extr.newGroupInGroup("ext_Ph_Name", "PH-NAME");
        ext_Ph_Last_Name = ext_Ph_Name.newFieldInGroup("ext_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        ext_Ph_First_Name = ext_Ph_Name.newFieldInGroup("ext_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        ext_Ph_Middle_Name = ext_Ph_Name.newFieldInGroup("ext_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        ext_Ph_Sex = ext_Extr.newFieldInGroup("ext_Ph_Sex", "PH-SEX", FieldType.STRING, 1);
        ext_Pymnt_Nme_And_Addr_Grp = ext_Extr.newGroupArrayInGroup("ext_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", new DbsArrayController(1,2));
        ext_Pymnt_Nme = ext_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38);
        ext_Pymnt_Addr_Line_Txt = ext_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("ext_Pymnt_Addr_Line_Txt", "PYMNT-ADDR-LINE-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,6));
        ext_Pymnt_Addr_Line_TxtRedef7 = ext_Pymnt_Nme_And_Addr_Grp.newGroupInGroup("ext_Pymnt_Addr_Line_TxtRedef7", "Redefines", ext_Pymnt_Addr_Line_Txt);
        ext_Pymnt_Addr_Line1_Txt = ext_Pymnt_Addr_Line_TxtRedef7.newFieldInGroup("ext_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 
            35);
        ext_Pymnt_Addr_Line2_Txt = ext_Pymnt_Addr_Line_TxtRedef7.newFieldInGroup("ext_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 
            35);
        ext_Pymnt_Addr_Line3_Txt = ext_Pymnt_Addr_Line_TxtRedef7.newFieldInGroup("ext_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 
            35);
        ext_Pymnt_Addr_Line4_Txt = ext_Pymnt_Addr_Line_TxtRedef7.newFieldInGroup("ext_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 
            35);
        ext_Pymnt_Addr_Line5_Txt = ext_Pymnt_Addr_Line_TxtRedef7.newFieldInGroup("ext_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 
            35);
        ext_Pymnt_Addr_Line6_Txt = ext_Pymnt_Addr_Line_TxtRedef7.newFieldInGroup("ext_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 
            35);
        ext_Pymnt_Addr_Zip_Cde = ext_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 9);
        ext_Pymnt_Postl_Data = ext_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 32);
        ext_Pymnt_Addr_Type_Ind = ext_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", FieldType.STRING, 1);
        ext_Pymnt_Addr_Last_Chg_Dte = ext_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext_Pymnt_Addr_Last_Chg_Dte", "PYMNT-ADDR-LAST-CHG-DTE", FieldType.NUMERIC, 
            8);
        ext_Pymnt_Addr_Last_Chg_Tme = ext_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext_Pymnt_Addr_Last_Chg_Tme", "PYMNT-ADDR-LAST-CHG-TME", FieldType.NUMERIC, 
            7);
        ext_Pymnt_Addr_Chg_Ind = ext_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("ext_Pymnt_Addr_Chg_Ind", "PYMNT-ADDR-CHG-IND", FieldType.STRING, 1);
        ext_Pymnt_Ded_Grp = ext_Extr.newGroupInGroup("ext_Pymnt_Ded_Grp", "PYMNT-DED-GRP");
        ext_Pymnt_Ded_Cde = ext_Pymnt_Ded_Grp.newFieldArrayInGroup("ext_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3, new DbsArrayController(1,
            10));
        ext_Pymnt_Ded_Payee_Cde = ext_Pymnt_Ded_Grp.newFieldArrayInGroup("ext_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 8, new DbsArrayController(1,
            10));
        ext_Pymnt_Ded_Amt = ext_Pymnt_Ded_Grp.newFieldArrayInGroup("ext_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            10));
        ext_Egtrra_Eligibility_Ind = ext_Extr.newFieldInGroup("ext_Egtrra_Eligibility_Ind", "EGTRRA-ELIGIBILITY-IND", FieldType.STRING, 1);
        ext_Originating_Irc_Cde = ext_Extr.newFieldArrayInGroup("ext_Originating_Irc_Cde", "ORIGINATING-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1,
            8));
        ext_Originating_Irc_CdeRedef8 = ext_Extr.newGroupInGroup("ext_Originating_Irc_CdeRedef8", "Redefines", ext_Originating_Irc_Cde);
        ext_Distributing_Irc_Cde = ext_Originating_Irc_CdeRedef8.newFieldInGroup("ext_Distributing_Irc_Cde", "DISTRIBUTING-IRC-CDE", FieldType.STRING, 
            16);
        ext_Receiving_Irc_Cde = ext_Extr.newFieldInGroup("ext_Receiving_Irc_Cde", "RECEIVING-IRC-CDE", FieldType.STRING, 2);
        ext_Tpa_Filler = ext_Extr.newFieldInGroup("ext_Tpa_Filler", "TPA-FILLER", FieldType.STRING, 81);
        ext_Inv_Acct = ext_Extr.newGroupArrayInGroup("ext_Inv_Acct", "INV-ACCT", new DbsArrayController(1,40));
        ext_Inv_Acct_Cde = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        ext_Inv_Acct_CdeRedef9 = ext_Inv_Acct.newGroupInGroup("ext_Inv_Acct_CdeRedef9", "Redefines", ext_Inv_Acct_Cde);
        ext_Inv_Acct_Cde_N = ext_Inv_Acct_CdeRedef9.newFieldInGroup("ext_Inv_Acct_Cde_N", "INV-ACCT-CDE-N", FieldType.NUMERIC, 2);
        ext_Inv_Acct_Ded_Cde = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Ded_Cde", "INV-ACCT-DED-CDE", FieldType.NUMERIC, 3);
        ext_Inv_Acct_Ded_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Ded_Amt", "INV-ACCT-DED-AMT", FieldType.DECIMAL, 9,2);
        ext_Inv_Acct_Company = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Company", "INV-ACCT-COMPANY", FieldType.STRING, 1);
        ext_Inv_Acct_Unit_Qty = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 9,3);
        ext_Inv_Acct_Unit_Value = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 9,4);
        ext_Inv_Acct_Settl_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 11,2);
        ext_Inv_Acct_Fed_Cde = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 1);
        ext_Inv_Acct_State_Cde = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", FieldType.STRING, 1);
        ext_Inv_Acct_Local_Cde = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", FieldType.STRING, 1);
        ext_Inv_Acct_Ivc_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Inv_Acct_Dci_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Inv_Acct_Dpi_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Inv_Acct_Start_Accum_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext_Inv_Acct_End_Accum_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Inv_Acct_Dvdnd_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Inv_Acct_Net_Pymnt_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Inv_Acct_Ivc_Ind = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 1);
        ext_Inv_Acct_Adj_Ivc_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Inv_Acct_Valuat_Period = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 1);
        ext_Inv_Acct_Fdrl_Tax_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Inv_Acct_State_Tax_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Inv_Acct_Local_Tax_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Inv_Acct_Exp_Amt = ext_Inv_Acct.newFieldInGroup("ext_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 9,2);
        ext_Inv_Rtb_Grp = ext_Extr.newGroupArrayInGroup("ext_Inv_Rtb_Grp", "INV-RTB-GRP", new DbsArrayController(1,20));
        ext_Inv_Rtb_Cde = ext_Inv_Rtb_Grp.newFieldInGroup("ext_Inv_Rtb_Cde", "INV-RTB-CDE", FieldType.STRING, 1);
        ext_Inv_Acct_Fed_Dci_Tax_Amt = ext_Inv_Rtb_Grp.newFieldInGroup("ext_Inv_Acct_Fed_Dci_Tax_Amt", "INV-ACCT-FED-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext_Inv_Acct_Sta_Dci_Tax_Amt = ext_Inv_Rtb_Grp.newFieldInGroup("ext_Inv_Acct_Sta_Dci_Tax_Amt", "INV-ACCT-STA-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext_Inv_Acct_Loc_Dci_Tax_Amt = ext_Inv_Rtb_Grp.newFieldInGroup("ext_Inv_Acct_Loc_Dci_Tax_Amt", "INV-ACCT-LOC-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext_Inv_Acct_Fed_Dpi_Tax_Amt = ext_Inv_Rtb_Grp.newFieldInGroup("ext_Inv_Acct_Fed_Dpi_Tax_Amt", "INV-ACCT-FED-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext_Inv_Acct_Sta_Dpi_Tax_Amt = ext_Inv_Rtb_Grp.newFieldInGroup("ext_Inv_Acct_Sta_Dpi_Tax_Amt", "INV-ACCT-STA-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        ext_Inv_Acct_Loc_Dpi_Tax_Amt = ext_Inv_Rtb_Grp.newFieldInGroup("ext_Inv_Acct_Loc_Dpi_Tax_Amt", "INV-ACCT-LOC-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2);

        vw_fcp_Cons_Plan = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Plan", "FCP-CONS-PLAN"), "FCP_CONS_PLAN", "FCP_EFT_GLBL", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PLAN"));
        fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data = vw_fcp_Cons_Plan.getRecord().newGroupInGroup("fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data", "ALT-CARRIER-FACT-SHEET-DATA");
        fcp_Cons_Plan_Cntrct_Rcrd_Typ = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_RCRD_TYP");
        fcp_Cons_Plan_Cntrct_Orgn_Cde = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Plan_Cntrct_Ppcn_Nbr = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef10 = vw_fcp_Cons_Plan.getRecord().newGroupInGroup("fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef10", "Redefines", 
            fcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr);
        fcp_Cons_Plan_Pymnt_Prcss_Seq_Num = fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef10.newFieldInGroup("fcp_Cons_Plan_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        fcp_Cons_Plan_Pymnt_Instmt_Nbr = fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef10.newFieldInGroup("fcp_Cons_Plan_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        fcp_Cons_Plan_Cntl_Check_Dte = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntl_Check_Dte", "CNTL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTL_CHECK_DTE");
        fcp_Cons_Plan_Cntrct_Good_Contract = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Good_Contract", "CNTRCT-GOOD-CONTRACT", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "CNTRCT_GOOD_CONTRACT");
        fcp_Cons_Plan_Cntrct_Invalid_Cond_Ind = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Invalid_Cond_Ind", "CNTRCT-INVALID-COND-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "CNTRCT_INVALID_COND_IND");
        fcp_Cons_Plan_Cntrct_Multi_Payee = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Multi_Payee", "CNTRCT-MULTI-PAYEE", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "CNTRCT_MULTI_PAYEE");
        fcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons", 
            "C*CNTRCT-INVALID-REASONS", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_CNTRCT_INVALID_REASONS");
        fcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newGroupInGroup("fcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup", 
            "CNTRCT_INVALID_REASONSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "FCP_EFT_GLBL_CNTRCT_INVALID_REASONS");
        fcp_Cons_Plan_Cntrct_Invalid_Reasons = fcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup.newFieldArrayInGroup("fcp_Cons_Plan_Cntrct_Invalid_Reasons", 
            "CNTRCT-INVALID-REASONS", FieldType.STRING, 50, new DbsArrayController(1,10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_INVALID_REASONS");
        fcp_Cons_Plan_Cntrct_Ivc_Amt = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "CNTRCT_IVC_AMT");
        fcp_Cons_Plan_Count_Castplan_Data = vw_fcp_Cons_Plan.getRecord().newFieldInGroup("fcp_Cons_Plan_Count_Castplan_Data", "C*PLAN-DATA", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Data = vw_fcp_Cons_Plan.getRecord().newGroupInGroup("fcp_Cons_Plan_Plan_Data", "PLAN-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employer_Name = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employer_Name", "PLAN-EMPLOYER-NAME", FieldType.STRING, 
            35, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYER_NAME", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Type = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Type", "PLAN-TYPE", FieldType.STRING, 10, new DbsArrayController(1,15) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_TYPE", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Cash_Avail = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Cash_Avail", "PLAN-CASH-AVAIL", FieldType.STRING, 
            50, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_CASH_AVAIL", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employer_Cntrb = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employer_Cntrb", "PLAN-EMPLOYER-CNTRB", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYER_CNTRB", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employer_Cntrb_Earn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employer_Cntrb_Earn", "PLAN-EMPLOYER-CNTRB-EARN", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYER_CNTRB_EARN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employee_Rdctn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employee_Rdctn", "PLAN-EMPLOYEE-RDCTN", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYEE_RDCTN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employee_Rdctn_Earn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employee_Rdctn_Earn", "PLAN-EMPLOYEE-RDCTN-EARN", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYEE_RDCTN_EARN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employee_Ddctn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employee_Ddctn", "PLAN-EMPLOYEE-DDCTN", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYEE_DDCTN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employee_Ddctn_Earn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employee_Ddctn_Earn", "PLAN-EMPLOYEE-DDCTN-EARN", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYEE_DDCTN_EARN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Acc_123186 = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Acc_123186", "PLAN-ACC-123186", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_ACC_123186", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Acc_123188 = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Acc_123188", "PLAN-ACC-123188", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_ACC_123188", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Acc_123188_Rdctn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Acc_123188_Rdctn", "PLAN-ACC-123188-RDCTN", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_ACC_123188_RDCTN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Acc_123188_Rdctn_Earn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Acc_123188_Rdctn_Earn", "PLAN-ACC-123188-RDCTN-EARN", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_ACC_123188_RDCTN_EARN", 
            "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Rcrd_Orgn_Ppcn_Prcss_Chkdt = vw_fcp_Cons_Plan.getRecord().newFieldInGroup("fcp_Cons_Plan_Rcrd_Orgn_Ppcn_Prcss_Chkdt", "RCRD-ORGN-PPCN-PRCSS-CHKDT", 
            FieldType.BINARY, 26, RepeatingFieldStrategy.None, "RCRD_ORGN_PPCN_PRCSS_CHKDT");
        vw_fcp_Cons_Plan.setUniquePeList();

        this.setRecordName("GdaFcpg364");
    }
    public void initializeValues() throws Exception
    {
        reset();

        vw_fcp_Cons_Plan.reset();
    }

    // Constructor
    private GdaFcpg364() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }

    // Instance Property
    public static GdaFcpg364 getInstance(int callnatLevel) throws Exception
    {
        if (_instance.get() == null)
            _instance.set(new ArrayList<GdaFcpg364>());

        if (_instance.get().size() < callnatLevel)
        {
            while (_instance.get().size() < callnatLevel)
            {
                _instance.get().add(new GdaFcpg364());
            }
        }
        else if (_instance.get().size() > callnatLevel)
        {
            while(_instance.get().size() > callnatLevel)
            _instance.get().remove(_instance.get().size() - 1);
        }

        return _instance.get().get(callnatLevel - 1);
    }
}

