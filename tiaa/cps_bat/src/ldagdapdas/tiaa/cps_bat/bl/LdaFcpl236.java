/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:07 PM
**        * FROM NATURAL LDA     : FCPL236
************************************************************
**        * FILE NAME            : LdaFcpl236.java
**        * CLASS NAME           : LdaFcpl236
**        * INSTANCE NAME        : LdaFcpl236
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl236 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl236_Lda;
    private DbsField pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde;
    private DbsField pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data;
    private DbsField pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char;
    private DbsField pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data;

    public DbsGroup getPnd_Fcpl236_Lda() { return pnd_Fcpl236_Lda; }

    public DbsField getPnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde() { return pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde; }

    public DbsField getPnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data() { return pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data; }

    public DbsField getPnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char() { return pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char; }

    public DbsField getPnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data() { return pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl236_Lda = newGroupInRecord("pnd_Fcpl236_Lda", "#FCPL236-LDA");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde = pnd_Fcpl236_Lda.newFieldArrayInGroup("pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde", "#FCPL236-LOB-CDE", FieldType.STRING, 
            4, new DbsArrayController(1,25));
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data = pnd_Fcpl236_Lda.newFieldArrayInGroup("pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data", "#STMNT-HDR-DATA", FieldType.STRING, 
            55, new DbsArrayController(1,25));
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char = pnd_Fcpl236_Lda.newFieldArrayInGroup("pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char", "#STMNT-HDR-CNTL-CHAR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1,25));
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data = pnd_Fcpl236_Lda.newFieldArrayInGroup("pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data", "#WARRANT-HDR-DATA", FieldType.STRING, 
            56, new DbsArrayController(1,23));

        this.setRecordName("LdaFcpl236");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(1).setInitialValue("RA");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(2).setInitialValue("RAD");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(3).setInitialValue("GRA");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(4).setInitialValue("GRAD");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(5).setInitialValue("SRA");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(6).setInitialValue("SRAD");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(7).setInitialValue("GSRA");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(8).setInitialValue("GSRD");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(9).setInitialValue("IRA");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(10).setInitialValue("IRAD");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(11).setInitialValue("30");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(12).setInitialValue("31");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(13).setInitialValue("SWAT");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(14).setInitialValue("STS");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(15).setInitialValue("ROTH");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(16).setInitialValue("CLA");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(17).setInitialValue("R1");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(18).setInitialValue("R2");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(19).setInitialValue("R3");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(20).setInitialValue("R4");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(21).setInitialValue("R5");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(22).setInitialValue("R6");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(23).setInitialValue("R7");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(24).setInitialValue("MDO");
        pnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde.getValue(25).setInitialValue("R");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(1).setInitialValue("Retirement Annuity Withdrawal Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(2).setInitialValue("Retirement Annuity Withdrawal Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(3).setInitialValue("Group Retirement Annuity Withdrawal Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(4).setInitialValue("Group Retirement Annuity Withdrawal Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(5).setInitialValue("Supplemental Retirement Annuity Withdrawal Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(6).setInitialValue("Supplemental Retirement Annuity Withdrawal Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(7).setInitialValue("Group Supplemental Retirement Annuity Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(8).setInitialValue("Group Supplemental Retirement Annuity Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(9).setInitialValue("Individual Retirement Annuity Withdrawal Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(10).setInitialValue("Individual Retirement Annuity Withdrawal Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(11).setInitialValue("Initial Minimum Distribution Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(12).setInitialValue("Recurring Minimum Distribution Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(13).setInitialValue("Systematic Withdrawal Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(14).setInitialValue("Special Transfer Services For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(15).setInitialValue("Roth IRA Withdrawal For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(16).setInitialValue("Classic IRA Withdrawal For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(24).setInitialValue("Savings & Investment Plan Withdrawal Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Data.getValue(25).setInitialValue("Payment For:");
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(1).setInitialValue(74);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(2).setInitialValue(74);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(3).setInitialValue(86);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(4).setInitialValue(86);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(5).setInitialValue(94);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(6).setInitialValue(94);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(7).setInitialValue(87);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(8).setInitialValue(87);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(9).setInitialValue(88);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(10).setInitialValue(88);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(11).setInitialValue(69);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(12).setInitialValue(75);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(13).setInitialValue(59);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(14).setInitialValue(49);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(15).setInitialValue(45);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(16).setInitialValue(47);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(24).setInitialValue(87);
        pnd_Fcpl236_Lda_Pnd_Stmnt_Hdr_Cntl_Char.getValue(25).setInitialValue(27);
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(1).setInitialValue("RETIREMENT ANNUITY WITHDRAWAL PAYMENT");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(2).setInitialValue("RETIREMENT ANNUITY DIRECT TRANSFER");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(3).setInitialValue("GROUP RETIREMENT ANNUITY WITHDRAWAL PAYMENT");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(4).setInitialValue("GROUP RETIREMENT ANNUITY DIRECT TRANSFER");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(5).setInitialValue("SUPPLEMENTAL RETIREMENT ANNUITY WITHDRAWAL PAYMENT");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(6).setInitialValue("SUPPLEMENTAL RETIREMENT ANNUITY DIRECT TRANSFER");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(7).setInitialValue("GROUP SUPPLEMENTAL RETIREMENT ANNUITY WITHDRAWAL PAYMENT");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(8).setInitialValue("GROUP SUPPLEMENTAL RETIREMENT ANNUITY DIRECT TRANSFER");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(9).setInitialValue("INDIVIDUAL RETIREMENT ANNUITY WITHDRAWAL PAYMENT");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(10).setInitialValue("INDIVIDUAL RETIREMENT ANNUITY DIRECT TRANSFER");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(17).setInitialValue("RA Internal Rollover To Classic IRA");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(18).setInitialValue("GRA Internal Rollover To Classic IRA");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(19).setInitialValue("GSRA Internal Rollover To Classic IRA");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(20).setInitialValue("SRA Internal Rollover To Classic IRA");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(21).setInitialValue("IRA Classic Internal Rollover To Roth IRA");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(22).setInitialValue("Classic IRA - Withdrawal Payment");
        pnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data.getValue(23).setInitialValue("Roth IRA - Withdrawal Payment");
    }

    // Constructor
    public LdaFcpl236() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
