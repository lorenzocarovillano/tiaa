/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:18 PM
**        * FROM NATURAL LDA     : FCPL893G
************************************************************
**        * FILE NAME            : LdaFcpl893g.java
**        * CLASS NAME           : LdaFcpl893g
**        * INSTANCE NAME        : LdaFcpl893g
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl893g extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl893g;
    private DbsField pnd_Fcpl893g_Pnd_Grand_Total_Text;
    private DbsField pnd_Fcpl893g_Pnd_Ap_Page_1_Text;
    private DbsField pnd_Fcpl893g_Pnd_Nz_Page_1_Text;
    private DbsField pnd_Fcpl893g_Pnd_Al_Stmnt_To_Annt_Text;
    private DbsField pnd_Fcpl893g_Pnd_Al_Check_Text;
    private DbsField pnd_Fcpl893g_Pnd_Phone_Text;
    private DbsField pnd_Fcpl893g_Pnd_Company_Text;
    private DbsField pnd_Fcpl893g_Pnd_Ivc_Text;
    private DbsField pnd_Fcpl893g_Pnd_Ivc_Zero_Text;
    private DbsField pnd_Fcpl893g_Pnd_Ivc_From_Rtb_Rollover_Text;
    private DbsField pnd_Fcpl893g_Pnd_Roth_Text;

    public DbsGroup getPnd_Fcpl893g() { return pnd_Fcpl893g; }

    public DbsField getPnd_Fcpl893g_Pnd_Grand_Total_Text() { return pnd_Fcpl893g_Pnd_Grand_Total_Text; }

    public DbsField getPnd_Fcpl893g_Pnd_Ap_Page_1_Text() { return pnd_Fcpl893g_Pnd_Ap_Page_1_Text; }

    public DbsField getPnd_Fcpl893g_Pnd_Nz_Page_1_Text() { return pnd_Fcpl893g_Pnd_Nz_Page_1_Text; }

    public DbsField getPnd_Fcpl893g_Pnd_Al_Stmnt_To_Annt_Text() { return pnd_Fcpl893g_Pnd_Al_Stmnt_To_Annt_Text; }

    public DbsField getPnd_Fcpl893g_Pnd_Al_Check_Text() { return pnd_Fcpl893g_Pnd_Al_Check_Text; }

    public DbsField getPnd_Fcpl893g_Pnd_Phone_Text() { return pnd_Fcpl893g_Pnd_Phone_Text; }

    public DbsField getPnd_Fcpl893g_Pnd_Company_Text() { return pnd_Fcpl893g_Pnd_Company_Text; }

    public DbsField getPnd_Fcpl893g_Pnd_Ivc_Text() { return pnd_Fcpl893g_Pnd_Ivc_Text; }

    public DbsField getPnd_Fcpl893g_Pnd_Ivc_Zero_Text() { return pnd_Fcpl893g_Pnd_Ivc_Zero_Text; }

    public DbsField getPnd_Fcpl893g_Pnd_Ivc_From_Rtb_Rollover_Text() { return pnd_Fcpl893g_Pnd_Ivc_From_Rtb_Rollover_Text; }

    public DbsField getPnd_Fcpl893g_Pnd_Roth_Text() { return pnd_Fcpl893g_Pnd_Roth_Text; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl893g = newGroupInRecord("pnd_Fcpl893g", "#FCPL893G");
        pnd_Fcpl893g_Pnd_Grand_Total_Text = pnd_Fcpl893g.newFieldArrayInGroup("pnd_Fcpl893g_Pnd_Grand_Total_Text", "#GRAND-TOTAL-TEXT", FieldType.STRING, 
            45, new DbsArrayController(1,2));
        pnd_Fcpl893g_Pnd_Ap_Page_1_Text = pnd_Fcpl893g.newFieldArrayInGroup("pnd_Fcpl893g_Pnd_Ap_Page_1_Text", "#AP-PAGE-1-TEXT", FieldType.STRING, 106, 
            new DbsArrayController(1,6));
        pnd_Fcpl893g_Pnd_Nz_Page_1_Text = pnd_Fcpl893g.newFieldArrayInGroup("pnd_Fcpl893g_Pnd_Nz_Page_1_Text", "#NZ-PAGE-1-TEXT", FieldType.STRING, 110, 
            new DbsArrayController(1,3));
        pnd_Fcpl893g_Pnd_Al_Stmnt_To_Annt_Text = pnd_Fcpl893g.newFieldArrayInGroup("pnd_Fcpl893g_Pnd_Al_Stmnt_To_Annt_Text", "#AL-STMNT-TO-ANNT-TEXT", 
            FieldType.STRING, 106, new DbsArrayController(1,3));
        pnd_Fcpl893g_Pnd_Al_Check_Text = pnd_Fcpl893g.newFieldArrayInGroup("pnd_Fcpl893g_Pnd_Al_Check_Text", "#AL-CHECK-TEXT", FieldType.STRING, 106, 
            new DbsArrayController(1,3));
        pnd_Fcpl893g_Pnd_Phone_Text = pnd_Fcpl893g.newFieldArrayInGroup("pnd_Fcpl893g_Pnd_Phone_Text", "#PHONE-TEXT", FieldType.STRING, 47, new DbsArrayController(1,
            3));
        pnd_Fcpl893g_Pnd_Company_Text = pnd_Fcpl893g.newFieldArrayInGroup("pnd_Fcpl893g_Pnd_Company_Text", "#COMPANY-TEXT", FieldType.STRING, 5, new DbsArrayController(1,
            2));
        pnd_Fcpl893g_Pnd_Ivc_Text = pnd_Fcpl893g.newFieldInGroup("pnd_Fcpl893g_Pnd_Ivc_Text", "#IVC-TEXT", FieldType.STRING, 51);
        pnd_Fcpl893g_Pnd_Ivc_Zero_Text = pnd_Fcpl893g.newFieldInGroup("pnd_Fcpl893g_Pnd_Ivc_Zero_Text", "#IVC-ZERO-TEXT", FieldType.STRING, 48);
        pnd_Fcpl893g_Pnd_Ivc_From_Rtb_Rollover_Text = pnd_Fcpl893g.newFieldInGroup("pnd_Fcpl893g_Pnd_Ivc_From_Rtb_Rollover_Text", "#IVC-FROM-RTB-ROLLOVER-TEXT", 
            FieldType.STRING, 61);
        pnd_Fcpl893g_Pnd_Roth_Text = pnd_Fcpl893g.newFieldInGroup("pnd_Fcpl893g_Pnd_Roth_Text", "#ROTH-TEXT", FieldType.STRING, 70);

        this.setRecordName("LdaFcpl893g");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl893g_Pnd_Grand_Total_Text.getValue(1).setInitialValue("This statement is continued on the next page");
        pnd_Fcpl893g_Pnd_Grand_Total_Text.getValue(2).setInitialValue("Grand Total");
        pnd_Fcpl893g_Pnd_Ap_Page_1_Text.getValue(1).setInitialValue("TIAA reports all taxable payments to IRS for the year in which they are made.{cr}");
        pnd_Fcpl893g_Pnd_Ap_Page_1_Text.getValue(2).setInitialValue("If you have any questions, please call us at {b}800-842-2252{/b}weekdays from 8 a.m. to 10 p.m. andSaturd");
        pnd_Fcpl893g_Pnd_Ap_Page_1_Text.getValue(3).setInitialValue("ay from 9 a.m. to 6 p.m.(ET). You can also. visit{b}TIAA.org{/b} for more information.");
        pnd_Fcpl893g_Pnd_Ap_Page_1_Text.getValue(4).setInitialValue("We report all taxable payments to the government for the year in which they are made.   If you have");
        pnd_Fcpl893g_Pnd_Ap_Page_1_Text.getValue(5).setInitialValue("any questions, please call our Planning and Servic$e Center toll free at 1 800 223-1200, M-F 8am -");
        pnd_Fcpl893g_Pnd_Ap_Page_1_Text.getValue(6).setInitialValue("8pm ET. This contract has been issued by TIAA-CREF!Life Insurance Company.");
        pnd_Fcpl893g_Pnd_Nz_Page_1_Text.getValue(1).setInitialValue("TIAA reports all taxable payments to the IRS for t$he year in which they are made.{cr}");
        pnd_Fcpl893g_Pnd_Nz_Page_1_Text.getValue(2).setInitialValue("If you have any questions, please call us at {b}80$0-842-2252{/b} weekdays from 8 a.m. to 10 p.m. a$nd");
        pnd_Fcpl893g_Pnd_Nz_Page_1_Text.getValue(3).setInitialValue("Saturday from 9 a.m. to 6 p.m.(ET). You can also v$isit {b}TIAA.org{/b} for more information.");
        pnd_Fcpl893g_Pnd_Al_Stmnt_To_Annt_Text.getValue(1).setInitialValue("Your loan payment has been sent to the institution listed below. Your loan is subject to all the terms");
        pnd_Fcpl893g_Pnd_Al_Stmnt_To_Annt_Text.getValue(2).setInitialValue("mentioned in the Loan Agreement. If you have any questions regarding the loan amount you received,");
        pnd_Fcpl893g_Pnd_Al_Stmnt_To_Annt_Text.getValue(3).setInitialValue("please call us at 800 842-2776, Monday to Friday8 a.m. to 10 p.m. ET and Saturday 9 a.m. to 6 p.mET.");
        pnd_Fcpl893g_Pnd_Al_Check_Text.getValue(1).setInitialValue("By endorsing and cashing this check you agree to all the terms mentioned in the Loan Agreement.  Ifyou");
        pnd_Fcpl893g_Pnd_Al_Check_Text.getValue(2).setInitialValue("have any questions regarding the loan amount you received, please call us at 800 842-2776, Monday to");
        pnd_Fcpl893g_Pnd_Al_Check_Text.getValue(3).setInitialValue("Friday 8 am to 10 pm ET and Saturday from 9 a.m. to 6 p.m. ET.");
        pnd_Fcpl893g_Pnd_Phone_Text.getValue(1).setInitialValue("toll free at 1 800 842-2776, M-F 8am - 11pm ET.");
        pnd_Fcpl893g_Pnd_Phone_Text.getValue(2).setInitialValue("collect at 212-490-9000.");
        pnd_Fcpl893g_Pnd_Phone_Text.getValue(3).setInitialValue("toll free at 1 800 223-1200, M-F 8am - 8pm ET.");
        pnd_Fcpl893g_Pnd_Company_Text.getValue(1).setInitialValue("TIAA:");
        pnd_Fcpl893g_Pnd_Company_Text.getValue(2).setInitialValue("CREF:");
        pnd_Fcpl893g_Pnd_Ivc_Text.setInitialValue("This payment includes after-tax contributions of ..");
        pnd_Fcpl893g_Pnd_Ivc_Zero_Text.setInitialValue("# PAYMENT DOES NOT HAVE AFTER TAX CONTRIBUTIONS");
        pnd_Fcpl893g_Pnd_Ivc_From_Rtb_Rollover_Text.setInitialValue("*This amount was previously taxed and can not be rolled over.");
        pnd_Fcpl893g_Pnd_Roth_Text.setInitialValue("This payment includes Roth 403(b)/401(k) after tax contributions of");
    }

    // Constructor
    public LdaFcpl893g() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
