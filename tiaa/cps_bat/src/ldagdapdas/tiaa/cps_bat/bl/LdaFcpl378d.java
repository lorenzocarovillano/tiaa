/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:09 PM
**        * FROM NATURAL LDA     : FCPL378D
************************************************************
**        * FILE NAME            : LdaFcpl378d.java
**        * CLASS NAME           : LdaFcpl378d
**        * INSTANCE NAME        : LdaFcpl378d
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl378d extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_fcp_Cons_Plan;
    private DbsGroup fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data;
    private DbsField fcp_Cons_Plan_Cntrct_Rcrd_Typ;
    private DbsField fcp_Cons_Plan_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Plan_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef1;
    private DbsField fcp_Cons_Plan_Pymnt_Prcss_Seq_Num;
    private DbsField fcp_Cons_Plan_Pymnt_Instmt_Nbr;
    private DbsField fcp_Cons_Plan_Cntl_Check_Dte;
    private DbsField fcp_Cons_Plan_Cntrct_Good_Contract;
    private DbsField fcp_Cons_Plan_Cntrct_Invalid_Cond_Ind;
    private DbsField fcp_Cons_Plan_Cntrct_Multi_Payee;
    private DbsField fcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons;
    private DbsGroup fcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup;
    private DbsField fcp_Cons_Plan_Cntrct_Invalid_Reasons;
    private DbsField fcp_Cons_Plan_Cntrct_Ivc_Amt;
    private DbsField fcp_Cons_Plan_Count_Castplan_Data;
    private DbsGroup fcp_Cons_Plan_Plan_Data;
    private DbsField fcp_Cons_Plan_Plan_Employer_Name;
    private DbsField fcp_Cons_Plan_Plan_Type;
    private DbsField fcp_Cons_Plan_Plan_Cash_Avail;
    private DbsField fcp_Cons_Plan_Plan_Employer_Cntrb;
    private DbsField fcp_Cons_Plan_Plan_Employer_Cntrb_Earn;
    private DbsField fcp_Cons_Plan_Plan_Employee_Rdctn;
    private DbsField fcp_Cons_Plan_Plan_Employee_Rdctn_Earn;
    private DbsField fcp_Cons_Plan_Plan_Employee_Ddctn;
    private DbsField fcp_Cons_Plan_Plan_Employee_Ddctn_Earn;
    private DbsField fcp_Cons_Plan_Plan_Acc_123186;
    private DbsField fcp_Cons_Plan_Plan_Acc_123188;
    private DbsField fcp_Cons_Plan_Plan_Acc_123188_Rdctn;
    private DbsField fcp_Cons_Plan_Plan_Acc_123188_Rdctn_Earn;
    private DbsField fcp_Cons_Plan_Rcrd_Orgn_Ppcn_Prcss_Chkdt;

    public DataAccessProgramView getVw_fcp_Cons_Plan() { return vw_fcp_Cons_Plan; }

    public DbsGroup getFcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data() { return fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data; }

    public DbsField getFcp_Cons_Plan_Cntrct_Rcrd_Typ() { return fcp_Cons_Plan_Cntrct_Rcrd_Typ; }

    public DbsField getFcp_Cons_Plan_Cntrct_Orgn_Cde() { return fcp_Cons_Plan_Cntrct_Orgn_Cde; }

    public DbsField getFcp_Cons_Plan_Cntrct_Ppcn_Nbr() { return fcp_Cons_Plan_Cntrct_Ppcn_Nbr; }

    public DbsField getFcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr() { return fcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getFcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef1() { return fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef1; }

    public DbsField getFcp_Cons_Plan_Pymnt_Prcss_Seq_Num() { return fcp_Cons_Plan_Pymnt_Prcss_Seq_Num; }

    public DbsField getFcp_Cons_Plan_Pymnt_Instmt_Nbr() { return fcp_Cons_Plan_Pymnt_Instmt_Nbr; }

    public DbsField getFcp_Cons_Plan_Cntl_Check_Dte() { return fcp_Cons_Plan_Cntl_Check_Dte; }

    public DbsField getFcp_Cons_Plan_Cntrct_Good_Contract() { return fcp_Cons_Plan_Cntrct_Good_Contract; }

    public DbsField getFcp_Cons_Plan_Cntrct_Invalid_Cond_Ind() { return fcp_Cons_Plan_Cntrct_Invalid_Cond_Ind; }

    public DbsField getFcp_Cons_Plan_Cntrct_Multi_Payee() { return fcp_Cons_Plan_Cntrct_Multi_Payee; }

    public DbsField getFcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons() { return fcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons; }

    public DbsGroup getFcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup() { return fcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup; }

    public DbsField getFcp_Cons_Plan_Cntrct_Invalid_Reasons() { return fcp_Cons_Plan_Cntrct_Invalid_Reasons; }

    public DbsField getFcp_Cons_Plan_Cntrct_Ivc_Amt() { return fcp_Cons_Plan_Cntrct_Ivc_Amt; }

    public DbsField getFcp_Cons_Plan_Count_Castplan_Data() { return fcp_Cons_Plan_Count_Castplan_Data; }

    public DbsGroup getFcp_Cons_Plan_Plan_Data() { return fcp_Cons_Plan_Plan_Data; }

    public DbsField getFcp_Cons_Plan_Plan_Employer_Name() { return fcp_Cons_Plan_Plan_Employer_Name; }

    public DbsField getFcp_Cons_Plan_Plan_Type() { return fcp_Cons_Plan_Plan_Type; }

    public DbsField getFcp_Cons_Plan_Plan_Cash_Avail() { return fcp_Cons_Plan_Plan_Cash_Avail; }

    public DbsField getFcp_Cons_Plan_Plan_Employer_Cntrb() { return fcp_Cons_Plan_Plan_Employer_Cntrb; }

    public DbsField getFcp_Cons_Plan_Plan_Employer_Cntrb_Earn() { return fcp_Cons_Plan_Plan_Employer_Cntrb_Earn; }

    public DbsField getFcp_Cons_Plan_Plan_Employee_Rdctn() { return fcp_Cons_Plan_Plan_Employee_Rdctn; }

    public DbsField getFcp_Cons_Plan_Plan_Employee_Rdctn_Earn() { return fcp_Cons_Plan_Plan_Employee_Rdctn_Earn; }

    public DbsField getFcp_Cons_Plan_Plan_Employee_Ddctn() { return fcp_Cons_Plan_Plan_Employee_Ddctn; }

    public DbsField getFcp_Cons_Plan_Plan_Employee_Ddctn_Earn() { return fcp_Cons_Plan_Plan_Employee_Ddctn_Earn; }

    public DbsField getFcp_Cons_Plan_Plan_Acc_123186() { return fcp_Cons_Plan_Plan_Acc_123186; }

    public DbsField getFcp_Cons_Plan_Plan_Acc_123188() { return fcp_Cons_Plan_Plan_Acc_123188; }

    public DbsField getFcp_Cons_Plan_Plan_Acc_123188_Rdctn() { return fcp_Cons_Plan_Plan_Acc_123188_Rdctn; }

    public DbsField getFcp_Cons_Plan_Plan_Acc_123188_Rdctn_Earn() { return fcp_Cons_Plan_Plan_Acc_123188_Rdctn_Earn; }

    public DbsField getFcp_Cons_Plan_Rcrd_Orgn_Ppcn_Prcss_Chkdt() { return fcp_Cons_Plan_Rcrd_Orgn_Ppcn_Prcss_Chkdt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_fcp_Cons_Plan = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Plan", "FCP-CONS-PLAN"), "FCP_CONS_PLAN", "FCP_EFT_GLBL", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PLAN"));
        fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data = vw_fcp_Cons_Plan.getRecord().newGroupInGroup("fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data", "ALT-CARRIER-FACT-SHEET-DATA");
        fcp_Cons_Plan_Cntrct_Rcrd_Typ = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_RCRD_TYP");
        fcp_Cons_Plan_Cntrct_Orgn_Cde = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Plan_Cntrct_Ppcn_Nbr = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef1 = vw_fcp_Cons_Plan.getRecord().newGroupInGroup("fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef1", "Redefines", 
            fcp_Cons_Plan_Pymnt_Prcss_Seq_Nbr);
        fcp_Cons_Plan_Pymnt_Prcss_Seq_Num = fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("fcp_Cons_Plan_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        fcp_Cons_Plan_Pymnt_Instmt_Nbr = fcp_Cons_Plan_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("fcp_Cons_Plan_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        fcp_Cons_Plan_Cntl_Check_Dte = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntl_Check_Dte", "CNTL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTL_CHECK_DTE");
        fcp_Cons_Plan_Cntrct_Good_Contract = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Good_Contract", "CNTRCT-GOOD-CONTRACT", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "CNTRCT_GOOD_CONTRACT");
        fcp_Cons_Plan_Cntrct_Invalid_Cond_Ind = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Invalid_Cond_Ind", "CNTRCT-INVALID-COND-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "CNTRCT_INVALID_COND_IND");
        fcp_Cons_Plan_Cntrct_Multi_Payee = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Multi_Payee", "CNTRCT-MULTI-PAYEE", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "CNTRCT_MULTI_PAYEE");
        fcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons", 
            "C*CNTRCT-INVALID-REASONS", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_CNTRCT_INVALID_REASONS");
        fcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newGroupInGroup("fcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup", 
            "CNTRCT_INVALID_REASONSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "FCP_EFT_GLBL_CNTRCT_INVALID_REASONS");
        fcp_Cons_Plan_Cntrct_Invalid_Reasons = fcp_Cons_Plan_Cntrct_Invalid_ReasonsMuGroup.newFieldArrayInGroup("fcp_Cons_Plan_Cntrct_Invalid_Reasons", 
            "CNTRCT-INVALID-REASONS", FieldType.STRING, 50, new DbsArrayController(1,10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_INVALID_REASONS");
        fcp_Cons_Plan_Cntrct_Ivc_Amt = fcp_Cons_Plan_Alt_Carrier_Fact_Sheet_Data.newFieldInGroup("fcp_Cons_Plan_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "CNTRCT_IVC_AMT");
        fcp_Cons_Plan_Count_Castplan_Data = vw_fcp_Cons_Plan.getRecord().newFieldInGroup("fcp_Cons_Plan_Count_Castplan_Data", "C*PLAN-DATA", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Data = vw_fcp_Cons_Plan.getRecord().newGroupInGroup("fcp_Cons_Plan_Plan_Data", "PLAN-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employer_Name = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employer_Name", "PLAN-EMPLOYER-NAME", FieldType.STRING, 
            35, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYER_NAME", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Type = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Type", "PLAN-TYPE", FieldType.STRING, 10, new DbsArrayController(1,15) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_TYPE", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Cash_Avail = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Cash_Avail", "PLAN-CASH-AVAIL", FieldType.STRING, 
            50, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_CASH_AVAIL", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employer_Cntrb = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employer_Cntrb", "PLAN-EMPLOYER-CNTRB", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYER_CNTRB", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employer_Cntrb_Earn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employer_Cntrb_Earn", "PLAN-EMPLOYER-CNTRB-EARN", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYER_CNTRB_EARN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employee_Rdctn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employee_Rdctn", "PLAN-EMPLOYEE-RDCTN", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYEE_RDCTN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employee_Rdctn_Earn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employee_Rdctn_Earn", "PLAN-EMPLOYEE-RDCTN-EARN", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYEE_RDCTN_EARN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employee_Ddctn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employee_Ddctn", "PLAN-EMPLOYEE-DDCTN", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYEE_DDCTN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Employee_Ddctn_Earn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Employee_Ddctn_Earn", "PLAN-EMPLOYEE-DDCTN-EARN", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYEE_DDCTN_EARN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Acc_123186 = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Acc_123186", "PLAN-ACC-123186", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_ACC_123186", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Acc_123188 = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Acc_123188", "PLAN-ACC-123188", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_ACC_123188", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Acc_123188_Rdctn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Acc_123188_Rdctn", "PLAN-ACC-123188-RDCTN", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_ACC_123188_RDCTN", "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Plan_Acc_123188_Rdctn_Earn = fcp_Cons_Plan_Plan_Data.newFieldArrayInGroup("fcp_Cons_Plan_Plan_Acc_123188_Rdctn_Earn", "PLAN-ACC-123188-RDCTN-EARN", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_ACC_123188_RDCTN_EARN", 
            "FCP_EFT_GLBL_PLAN_DATA");
        fcp_Cons_Plan_Rcrd_Orgn_Ppcn_Prcss_Chkdt = vw_fcp_Cons_Plan.getRecord().newFieldInGroup("fcp_Cons_Plan_Rcrd_Orgn_Ppcn_Prcss_Chkdt", "RCRD-ORGN-PPCN-PRCSS-CHKDT", 
            FieldType.BINARY, 26, RepeatingFieldStrategy.None, "RCRD_ORGN_PPCN_PRCSS_CHKDT");
        vw_fcp_Cons_Plan.setUniquePeList();

        this.setRecordName("LdaFcpl378d");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_fcp_Cons_Plan.reset();
    }

    // Constructor
    public LdaFcpl378d() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
