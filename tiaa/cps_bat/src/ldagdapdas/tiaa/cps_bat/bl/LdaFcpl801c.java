/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:14 PM
**        * FROM NATURAL LDA     : FCPL801C
************************************************************
**        * FILE NAME            : LdaFcpl801c.java
**        * CLASS NAME           : LdaFcpl801c
**        * INSTANCE NAME        : LdaFcpl801c
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl801c extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Ledger_Ext;
    private DbsGroup pnd_Ledger_Ext_Pnd_Ledger_Grp_1;
    private DbsField pnd_Ledger_Ext_Cntrct_Orgn_Cde;
    private DbsField pnd_Ledger_Ext_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ledger_Ext_Cntrct_Payee_Cde;
    private DbsField pnd_Ledger_Ext_Cntrct_Mode_Cde;
    private DbsField pnd_Ledger_Ext_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Ledger_Ext_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Ledger_Ext_Cntrct_Insurance_Option;
    private DbsField pnd_Ledger_Ext_Cntrct_Life_Contingency;
    private DbsField pnd_Ledger_Ext_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Ledger_Ext_Pymnt_Check_Dte;
    private DbsField pnd_Ledger_Ext_Pymnt_Cycle_Dte;
    private DbsField pnd_Ledger_Ext_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ledger_Ext_Pymnt_Acctg_Dte;
    private DbsField pnd_Ledger_Ext_Pymnt_Intrfce_Dte;
    private DbsGroup pnd_Ledger_Ext_Inv_Ledgr;
    private DbsField pnd_Ledger_Ext_Inv_Isa_Hash;
    private DbsField pnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr;
    private DbsField pnd_Ledger_Ext_Inv_Acct_Ledgr_Amt;
    private DbsField pnd_Ledger_Ext_Inv_Acct_Ledgr_Ind;
    private DbsGroup pnd_Ledger_Ext_Pnd_Misc_Fields;
    private DbsField pnd_Ledger_Ext_Cntrct_Company_Cde;
    private DbsField pnd_Ledger_Ext_Inv_Pymnt_Ded_Cde;
    private DbsGroup pnd_Ledger_Header;
    private DbsField pnd_Ledger_Header_Pnd_Header_Text;
    private DbsField pnd_Ledger_Header_Pnd_File_Type;
    private DbsField pnd_Ledger_Header_Pnd_Timestmp;
    private DbsField pnd_Ledger_Header_Pnd_Date_Time;
    private DbsField pnd_Ledger_Header_Pnd_Filler;

    public DbsGroup getPnd_Ledger_Ext() { return pnd_Ledger_Ext; }

    public DbsGroup getPnd_Ledger_Ext_Pnd_Ledger_Grp_1() { return pnd_Ledger_Ext_Pnd_Ledger_Grp_1; }

    public DbsField getPnd_Ledger_Ext_Cntrct_Orgn_Cde() { return pnd_Ledger_Ext_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Ledger_Ext_Cntrct_Ppcn_Nbr() { return pnd_Ledger_Ext_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Ledger_Ext_Cntrct_Payee_Cde() { return pnd_Ledger_Ext_Cntrct_Payee_Cde; }

    public DbsField getPnd_Ledger_Ext_Cntrct_Mode_Cde() { return pnd_Ledger_Ext_Cntrct_Mode_Cde; }

    public DbsField getPnd_Ledger_Ext_Cntrct_Annty_Ins_Type() { return pnd_Ledger_Ext_Cntrct_Annty_Ins_Type; }

    public DbsField getPnd_Ledger_Ext_Cntrct_Annty_Type_Cde() { return pnd_Ledger_Ext_Cntrct_Annty_Type_Cde; }

    public DbsField getPnd_Ledger_Ext_Cntrct_Insurance_Option() { return pnd_Ledger_Ext_Cntrct_Insurance_Option; }

    public DbsField getPnd_Ledger_Ext_Cntrct_Life_Contingency() { return pnd_Ledger_Ext_Cntrct_Life_Contingency; }

    public DbsField getPnd_Ledger_Ext_Pymnt_Prcss_Seq_Nbr() { return pnd_Ledger_Ext_Pymnt_Prcss_Seq_Nbr; }

    public DbsField getPnd_Ledger_Ext_Pymnt_Check_Dte() { return pnd_Ledger_Ext_Pymnt_Check_Dte; }

    public DbsField getPnd_Ledger_Ext_Pymnt_Cycle_Dte() { return pnd_Ledger_Ext_Pymnt_Cycle_Dte; }

    public DbsField getPnd_Ledger_Ext_Pymnt_Settlmnt_Dte() { return pnd_Ledger_Ext_Pymnt_Settlmnt_Dte; }

    public DbsField getPnd_Ledger_Ext_Pymnt_Acctg_Dte() { return pnd_Ledger_Ext_Pymnt_Acctg_Dte; }

    public DbsField getPnd_Ledger_Ext_Pymnt_Intrfce_Dte() { return pnd_Ledger_Ext_Pymnt_Intrfce_Dte; }

    public DbsGroup getPnd_Ledger_Ext_Inv_Ledgr() { return pnd_Ledger_Ext_Inv_Ledgr; }

    public DbsField getPnd_Ledger_Ext_Inv_Isa_Hash() { return pnd_Ledger_Ext_Inv_Isa_Hash; }

    public DbsField getPnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr() { return pnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr; }

    public DbsField getPnd_Ledger_Ext_Inv_Acct_Ledgr_Amt() { return pnd_Ledger_Ext_Inv_Acct_Ledgr_Amt; }

    public DbsField getPnd_Ledger_Ext_Inv_Acct_Ledgr_Ind() { return pnd_Ledger_Ext_Inv_Acct_Ledgr_Ind; }

    public DbsGroup getPnd_Ledger_Ext_Pnd_Misc_Fields() { return pnd_Ledger_Ext_Pnd_Misc_Fields; }

    public DbsField getPnd_Ledger_Ext_Cntrct_Company_Cde() { return pnd_Ledger_Ext_Cntrct_Company_Cde; }

    public DbsField getPnd_Ledger_Ext_Inv_Pymnt_Ded_Cde() { return pnd_Ledger_Ext_Inv_Pymnt_Ded_Cde; }

    public DbsGroup getPnd_Ledger_Header() { return pnd_Ledger_Header; }

    public DbsField getPnd_Ledger_Header_Pnd_Header_Text() { return pnd_Ledger_Header_Pnd_Header_Text; }

    public DbsField getPnd_Ledger_Header_Pnd_File_Type() { return pnd_Ledger_Header_Pnd_File_Type; }

    public DbsField getPnd_Ledger_Header_Pnd_Timestmp() { return pnd_Ledger_Header_Pnd_Timestmp; }

    public DbsField getPnd_Ledger_Header_Pnd_Date_Time() { return pnd_Ledger_Header_Pnd_Date_Time; }

    public DbsField getPnd_Ledger_Header_Pnd_Filler() { return pnd_Ledger_Header_Pnd_Filler; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Ledger_Ext = newGroupInRecord("pnd_Ledger_Ext", "#LEDGER-EXT");
        pnd_Ledger_Ext_Pnd_Ledger_Grp_1 = pnd_Ledger_Ext.newGroupInGroup("pnd_Ledger_Ext_Pnd_Ledger_Grp_1", "#LEDGER-GRP-1");
        pnd_Ledger_Ext_Cntrct_Orgn_Cde = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ledger_Ext_Cntrct_Ppcn_Nbr = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ledger_Ext_Cntrct_Payee_Cde = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ledger_Ext_Cntrct_Mode_Cde = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ledger_Ext_Cntrct_Annty_Ins_Type = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", 
            FieldType.STRING, 1);
        pnd_Ledger_Ext_Cntrct_Annty_Type_Cde = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Ledger_Ext_Cntrct_Insurance_Option = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", 
            FieldType.STRING, 1);
        pnd_Ledger_Ext_Cntrct_Life_Contingency = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", 
            FieldType.STRING, 1);
        pnd_Ledger_Ext_Pymnt_Prcss_Seq_Nbr = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Ledger_Ext_Pymnt_Check_Dte = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Ledger_Ext_Pymnt_Cycle_Dte = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        pnd_Ledger_Ext_Pymnt_Settlmnt_Dte = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);
        pnd_Ledger_Ext_Pymnt_Acctg_Dte = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Ledger_Ext_Pymnt_Intrfce_Dte = pnd_Ledger_Ext_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Ledger_Ext_Inv_Ledgr = pnd_Ledger_Ext.newGroupInGroup("pnd_Ledger_Ext_Inv_Ledgr", "INV-LEDGR");
        pnd_Ledger_Ext_Inv_Isa_Hash = pnd_Ledger_Ext_Inv_Ledgr.newFieldInGroup("pnd_Ledger_Ext_Inv_Isa_Hash", "INV-ISA-HASH", FieldType.STRING, 2);
        pnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr = pnd_Ledger_Ext_Inv_Ledgr.newFieldInGroup("pnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr", "INV-ACCT-LEDGR-NBR", FieldType.STRING, 
            15);
        pnd_Ledger_Ext_Inv_Acct_Ledgr_Amt = pnd_Ledger_Ext_Inv_Ledgr.newFieldInGroup("pnd_Ledger_Ext_Inv_Acct_Ledgr_Amt", "INV-ACCT-LEDGR-AMT", FieldType.PACKED_DECIMAL, 
            13,2);
        pnd_Ledger_Ext_Inv_Acct_Ledgr_Ind = pnd_Ledger_Ext_Inv_Ledgr.newFieldInGroup("pnd_Ledger_Ext_Inv_Acct_Ledgr_Ind", "INV-ACCT-LEDGR-IND", FieldType.STRING, 
            1);
        pnd_Ledger_Ext_Pnd_Misc_Fields = pnd_Ledger_Ext.newGroupInGroup("pnd_Ledger_Ext_Pnd_Misc_Fields", "#MISC-FIELDS");
        pnd_Ledger_Ext_Cntrct_Company_Cde = pnd_Ledger_Ext_Pnd_Misc_Fields.newFieldInGroup("pnd_Ledger_Ext_Cntrct_Company_Cde", "CNTRCT-COMPANY-CDE", 
            FieldType.STRING, 1);
        pnd_Ledger_Ext_Inv_Pymnt_Ded_Cde = pnd_Ledger_Ext_Pnd_Misc_Fields.newFieldInGroup("pnd_Ledger_Ext_Inv_Pymnt_Ded_Cde", "INV-PYMNT-DED-CDE", FieldType.NUMERIC, 
            3);

        pnd_Ledger_Header = newGroupInRecord("pnd_Ledger_Header", "#LEDGER-HEADER");
        pnd_Ledger_Header_Pnd_Header_Text = pnd_Ledger_Header.newFieldInGroup("pnd_Ledger_Header_Pnd_Header_Text", "#HEADER-TEXT", FieldType.STRING, 2);
        pnd_Ledger_Header_Pnd_File_Type = pnd_Ledger_Header.newFieldInGroup("pnd_Ledger_Header_Pnd_File_Type", "#FILE-TYPE", FieldType.STRING, 8);
        pnd_Ledger_Header_Pnd_Timestmp = pnd_Ledger_Header.newFieldInGroup("pnd_Ledger_Header_Pnd_Timestmp", "#TIMESTMP", FieldType.BINARY, 8);
        pnd_Ledger_Header_Pnd_Date_Time = pnd_Ledger_Header.newFieldInGroup("pnd_Ledger_Header_Pnd_Date_Time", "#DATE-TIME", FieldType.STRING, 21);
        pnd_Ledger_Header_Pnd_Filler = pnd_Ledger_Header.newFieldInGroup("pnd_Ledger_Header_Pnd_Filler", "#FILLER", FieldType.STRING, 42);

        this.setRecordName("LdaFcpl801c");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Ledger_Header_Pnd_Header_Text.setInitialValue("HD");
        pnd_Ledger_Header_Pnd_File_Type.setInitialValue("LGAPUPDT");
    }

    // Constructor
    public LdaFcpl801c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
