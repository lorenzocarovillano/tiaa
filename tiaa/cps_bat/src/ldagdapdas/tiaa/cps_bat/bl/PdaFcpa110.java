/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:13 PM
**        * FROM NATURAL PDA     : FCPA110
************************************************************
**        * FILE NAME            : PdaFcpa110.java
**        * CLASS NAME           : PdaFcpa110
**        * INSTANCE NAME        : PdaFcpa110
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa110 extends PdaBase
{
    // Properties
    private DbsGroup fcpa110;
    private DbsGroup fcpa110_Fcpa110a;
    private DbsField fcpa110_Fcpa110_Source_Code;
    private DbsField fcpa110_Fcpa110_Function;
    private DbsField fcpa110_Fcpa110_New_Check_Nbr;
    private DbsGroup fcpa110_Fcpa110_New_Check_NbrRedef1;
    private DbsField fcpa110_Fcpa110_New_Check_Prefix_A3;
    private DbsGroup fcpa110_Fcpa110_New_Check_Prefix_A3Redef2;
    private DbsField fcpa110_Fcpa110_New_Check_Prefix_N3;
    private DbsField fcpa110_Fcpa110_New_Check_Nbr_A7;
    private DbsGroup fcpa110_Fcpa110_New_Check_Nbr_A7Redef3;
    private DbsField fcpa110_Fcpa110_New_Check_Nbr_N7;
    private DbsField fcpa110_Fcpa110_Old_Check_Nbr;
    private DbsGroup fcpa110_Fcpa110_Old_Check_NbrRedef4;
    private DbsField fcpa110_Fcpa110_Old_Check_Nbr_A10;
    private DbsField fcpa110_Fcpa110_New_Sequence_Nbr;
    private DbsField fcpa110_Filler1;
    private DbsGroup fcpa110_Fcpa110b;
    private DbsField fcpa110_Fcpa110_Return_Code;
    private DbsField fcpa110_Fcpa110_Return_Msg;
    private DbsField fcpa110_Rt_Table_Id;
    private DbsField fcpa110_Rt_A_I_Ind;
    private DbsField fcpa110_Rt_Short_Key;
    private DbsField fcpa110_Rt_Long_Key;
    private DbsGroup fcpa110_Rt_Long_KeyRedef5;
    private DbsField fcpa110_Routing_No;
    private DbsField fcpa110_Filler_1;
    private DbsField fcpa110_Account_No;
    private DbsField fcpa110_Fller_9;
    private DbsGroup fcpa110_Company_Information;
    private DbsField fcpa110_Comp_Name_Label;
    private DbsField fcpa110_Company_Name;
    private DbsField fcpa110_Comp_Addr_Label;
    private DbsField fcpa110_Company_Address;
    private DbsField fcpa110_Comp_Acct_Label;
    private DbsField fcpa110_Company_Account;
    private DbsField fcpa110_Not_Valid_Label;
    private DbsField fcpa110_Not_Valid_Msg;
    private DbsField fcpa110_Filler2;
    private DbsGroup fcpa110_Bank_Account_Data;
    private DbsField fcpa110_Start_Check_Label;
    private DbsField fcpa110_Start_Check_No;
    private DbsGroup fcpa110_Start_Check_NoRedef6;
    private DbsField fcpa110_Start_Check_Prefix_A3;
    private DbsGroup fcpa110_Start_Check_Prefix_A3Redef7;
    private DbsField fcpa110_Start_Check_Prefix_N3;
    private DbsField fcpa110_Start_Check_No_A7;
    private DbsGroup fcpa110_Start_Check_No_A7Redef8;
    private DbsField fcpa110_Start_Check_No_N7;
    private DbsField fcpa110_End_Check_Label;
    private DbsField fcpa110_End_Check_No;
    private DbsGroup fcpa110_End_Check_NoRedef9;
    private DbsField fcpa110_End_Check_Prefix;
    private DbsField fcpa110_End_Check_No_N7;
    private DbsField fcpa110_Space_26;
    private DbsField fcpa110_Start_Seq_Label;
    private DbsField fcpa110_Start_Seq_No;
    private DbsField fcpa110_End_Seq_Label;
    private DbsField fcpa110_End_Seq_No;
    private DbsField fcpa110_Signature_Label;
    private DbsField fcpa110_Bank_Signature_Cde;
    private DbsField fcpa110_Space_15;
    private DbsField fcpa110_Title_Label;
    private DbsField fcpa110_Title;
    private DbsField fcpa110_Pos_Pay_Label;
    private DbsField fcpa110_Bank_Internal_Pospay_Ind;
    private DbsField fcpa110_Orgn_Label;
    private DbsField fcpa110_Bank_Orgn_Cde;
    private DbsField fcpa110_Filler3;
    private DbsGroup fcpa110_Bank_Name_Data;
    private DbsField fcpa110_Bank_Name_Label;
    private DbsField fcpa110_Bank_Name;
    private DbsField fcpa110_Bank_Address_Label;
    private DbsField fcpa110_Bank_Address1;
    private DbsField fcpa110_Bank_Above_Label;
    private DbsField fcpa110_Bank_Above_Check_Amt_Nbr;
    private DbsField fcpa110_Bank_Trans_Label;
    private DbsField fcpa110_Bank_Transmit_Ind;
    private DbsField fcpa110_Check_Ind_Label;
    private DbsField fcpa110_Check_Acct_Ind;
    private DbsField fcpa110_Eft_Ind_Label;
    private DbsField fcpa110_Eft_Acct_Ind;
    private DbsField fcpa110_Space_1;
    private DbsField fcpa110_Bank_Transmission_Label;
    private DbsField fcpa110_Bank_Transmission_Cde;
    private DbsField fcpa110_Filler4;

    public DbsGroup getFcpa110() { return fcpa110; }

    public DbsGroup getFcpa110_Fcpa110a() { return fcpa110_Fcpa110a; }

    public DbsField getFcpa110_Fcpa110_Source_Code() { return fcpa110_Fcpa110_Source_Code; }

    public DbsField getFcpa110_Fcpa110_Function() { return fcpa110_Fcpa110_Function; }

    public DbsField getFcpa110_Fcpa110_New_Check_Nbr() { return fcpa110_Fcpa110_New_Check_Nbr; }

    public DbsGroup getFcpa110_Fcpa110_New_Check_NbrRedef1() { return fcpa110_Fcpa110_New_Check_NbrRedef1; }

    public DbsField getFcpa110_Fcpa110_New_Check_Prefix_A3() { return fcpa110_Fcpa110_New_Check_Prefix_A3; }

    public DbsGroup getFcpa110_Fcpa110_New_Check_Prefix_A3Redef2() { return fcpa110_Fcpa110_New_Check_Prefix_A3Redef2; }

    public DbsField getFcpa110_Fcpa110_New_Check_Prefix_N3() { return fcpa110_Fcpa110_New_Check_Prefix_N3; }

    public DbsField getFcpa110_Fcpa110_New_Check_Nbr_A7() { return fcpa110_Fcpa110_New_Check_Nbr_A7; }

    public DbsGroup getFcpa110_Fcpa110_New_Check_Nbr_A7Redef3() { return fcpa110_Fcpa110_New_Check_Nbr_A7Redef3; }

    public DbsField getFcpa110_Fcpa110_New_Check_Nbr_N7() { return fcpa110_Fcpa110_New_Check_Nbr_N7; }

    public DbsField getFcpa110_Fcpa110_Old_Check_Nbr() { return fcpa110_Fcpa110_Old_Check_Nbr; }

    public DbsGroup getFcpa110_Fcpa110_Old_Check_NbrRedef4() { return fcpa110_Fcpa110_Old_Check_NbrRedef4; }

    public DbsField getFcpa110_Fcpa110_Old_Check_Nbr_A10() { return fcpa110_Fcpa110_Old_Check_Nbr_A10; }

    public DbsField getFcpa110_Fcpa110_New_Sequence_Nbr() { return fcpa110_Fcpa110_New_Sequence_Nbr; }

    public DbsField getFcpa110_Filler1() { return fcpa110_Filler1; }

    public DbsGroup getFcpa110_Fcpa110b() { return fcpa110_Fcpa110b; }

    public DbsField getFcpa110_Fcpa110_Return_Code() { return fcpa110_Fcpa110_Return_Code; }

    public DbsField getFcpa110_Fcpa110_Return_Msg() { return fcpa110_Fcpa110_Return_Msg; }

    public DbsField getFcpa110_Rt_Table_Id() { return fcpa110_Rt_Table_Id; }

    public DbsField getFcpa110_Rt_A_I_Ind() { return fcpa110_Rt_A_I_Ind; }

    public DbsField getFcpa110_Rt_Short_Key() { return fcpa110_Rt_Short_Key; }

    public DbsField getFcpa110_Rt_Long_Key() { return fcpa110_Rt_Long_Key; }

    public DbsGroup getFcpa110_Rt_Long_KeyRedef5() { return fcpa110_Rt_Long_KeyRedef5; }

    public DbsField getFcpa110_Routing_No() { return fcpa110_Routing_No; }

    public DbsField getFcpa110_Filler_1() { return fcpa110_Filler_1; }

    public DbsField getFcpa110_Account_No() { return fcpa110_Account_No; }

    public DbsField getFcpa110_Fller_9() { return fcpa110_Fller_9; }

    public DbsGroup getFcpa110_Company_Information() { return fcpa110_Company_Information; }

    public DbsField getFcpa110_Comp_Name_Label() { return fcpa110_Comp_Name_Label; }

    public DbsField getFcpa110_Company_Name() { return fcpa110_Company_Name; }

    public DbsField getFcpa110_Comp_Addr_Label() { return fcpa110_Comp_Addr_Label; }

    public DbsField getFcpa110_Company_Address() { return fcpa110_Company_Address; }

    public DbsField getFcpa110_Comp_Acct_Label() { return fcpa110_Comp_Acct_Label; }

    public DbsField getFcpa110_Company_Account() { return fcpa110_Company_Account; }

    public DbsField getFcpa110_Not_Valid_Label() { return fcpa110_Not_Valid_Label; }

    public DbsField getFcpa110_Not_Valid_Msg() { return fcpa110_Not_Valid_Msg; }

    public DbsField getFcpa110_Filler2() { return fcpa110_Filler2; }

    public DbsGroup getFcpa110_Bank_Account_Data() { return fcpa110_Bank_Account_Data; }

    public DbsField getFcpa110_Start_Check_Label() { return fcpa110_Start_Check_Label; }

    public DbsField getFcpa110_Start_Check_No() { return fcpa110_Start_Check_No; }

    public DbsGroup getFcpa110_Start_Check_NoRedef6() { return fcpa110_Start_Check_NoRedef6; }

    public DbsField getFcpa110_Start_Check_Prefix_A3() { return fcpa110_Start_Check_Prefix_A3; }

    public DbsGroup getFcpa110_Start_Check_Prefix_A3Redef7() { return fcpa110_Start_Check_Prefix_A3Redef7; }

    public DbsField getFcpa110_Start_Check_Prefix_N3() { return fcpa110_Start_Check_Prefix_N3; }

    public DbsField getFcpa110_Start_Check_No_A7() { return fcpa110_Start_Check_No_A7; }

    public DbsGroup getFcpa110_Start_Check_No_A7Redef8() { return fcpa110_Start_Check_No_A7Redef8; }

    public DbsField getFcpa110_Start_Check_No_N7() { return fcpa110_Start_Check_No_N7; }

    public DbsField getFcpa110_End_Check_Label() { return fcpa110_End_Check_Label; }

    public DbsField getFcpa110_End_Check_No() { return fcpa110_End_Check_No; }

    public DbsGroup getFcpa110_End_Check_NoRedef9() { return fcpa110_End_Check_NoRedef9; }

    public DbsField getFcpa110_End_Check_Prefix() { return fcpa110_End_Check_Prefix; }

    public DbsField getFcpa110_End_Check_No_N7() { return fcpa110_End_Check_No_N7; }

    public DbsField getFcpa110_Space_26() { return fcpa110_Space_26; }

    public DbsField getFcpa110_Start_Seq_Label() { return fcpa110_Start_Seq_Label; }

    public DbsField getFcpa110_Start_Seq_No() { return fcpa110_Start_Seq_No; }

    public DbsField getFcpa110_End_Seq_Label() { return fcpa110_End_Seq_Label; }

    public DbsField getFcpa110_End_Seq_No() { return fcpa110_End_Seq_No; }

    public DbsField getFcpa110_Signature_Label() { return fcpa110_Signature_Label; }

    public DbsField getFcpa110_Bank_Signature_Cde() { return fcpa110_Bank_Signature_Cde; }

    public DbsField getFcpa110_Space_15() { return fcpa110_Space_15; }

    public DbsField getFcpa110_Title_Label() { return fcpa110_Title_Label; }

    public DbsField getFcpa110_Title() { return fcpa110_Title; }

    public DbsField getFcpa110_Pos_Pay_Label() { return fcpa110_Pos_Pay_Label; }

    public DbsField getFcpa110_Bank_Internal_Pospay_Ind() { return fcpa110_Bank_Internal_Pospay_Ind; }

    public DbsField getFcpa110_Orgn_Label() { return fcpa110_Orgn_Label; }

    public DbsField getFcpa110_Bank_Orgn_Cde() { return fcpa110_Bank_Orgn_Cde; }

    public DbsField getFcpa110_Filler3() { return fcpa110_Filler3; }

    public DbsGroup getFcpa110_Bank_Name_Data() { return fcpa110_Bank_Name_Data; }

    public DbsField getFcpa110_Bank_Name_Label() { return fcpa110_Bank_Name_Label; }

    public DbsField getFcpa110_Bank_Name() { return fcpa110_Bank_Name; }

    public DbsField getFcpa110_Bank_Address_Label() { return fcpa110_Bank_Address_Label; }

    public DbsField getFcpa110_Bank_Address1() { return fcpa110_Bank_Address1; }

    public DbsField getFcpa110_Bank_Above_Label() { return fcpa110_Bank_Above_Label; }

    public DbsField getFcpa110_Bank_Above_Check_Amt_Nbr() { return fcpa110_Bank_Above_Check_Amt_Nbr; }

    public DbsField getFcpa110_Bank_Trans_Label() { return fcpa110_Bank_Trans_Label; }

    public DbsField getFcpa110_Bank_Transmit_Ind() { return fcpa110_Bank_Transmit_Ind; }

    public DbsField getFcpa110_Check_Ind_Label() { return fcpa110_Check_Ind_Label; }

    public DbsField getFcpa110_Check_Acct_Ind() { return fcpa110_Check_Acct_Ind; }

    public DbsField getFcpa110_Eft_Ind_Label() { return fcpa110_Eft_Ind_Label; }

    public DbsField getFcpa110_Eft_Acct_Ind() { return fcpa110_Eft_Acct_Ind; }

    public DbsField getFcpa110_Space_1() { return fcpa110_Space_1; }

    public DbsField getFcpa110_Bank_Transmission_Label() { return fcpa110_Bank_Transmission_Label; }

    public DbsField getFcpa110_Bank_Transmission_Cde() { return fcpa110_Bank_Transmission_Cde; }

    public DbsField getFcpa110_Filler4() { return fcpa110_Filler4; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        fcpa110 = dbsRecord.newGroupInRecord("fcpa110", "FCPA110");
        fcpa110.setParameterOption(ParameterOption.ByReference);
        fcpa110_Fcpa110a = fcpa110.newGroupInGroup("fcpa110_Fcpa110a", "FCPA110A");
        fcpa110_Fcpa110_Source_Code = fcpa110_Fcpa110a.newFieldInGroup("fcpa110_Fcpa110_Source_Code", "FCPA110-SOURCE-CODE", FieldType.STRING, 5);
        fcpa110_Fcpa110_Function = fcpa110_Fcpa110a.newFieldInGroup("fcpa110_Fcpa110_Function", "FCPA110-FUNCTION", FieldType.STRING, 15);
        fcpa110_Fcpa110_New_Check_Nbr = fcpa110_Fcpa110a.newFieldInGroup("fcpa110_Fcpa110_New_Check_Nbr", "FCPA110-NEW-CHECK-NBR", FieldType.NUMERIC, 
            10);
        fcpa110_Fcpa110_New_Check_NbrRedef1 = fcpa110_Fcpa110a.newGroupInGroup("fcpa110_Fcpa110_New_Check_NbrRedef1", "Redefines", fcpa110_Fcpa110_New_Check_Nbr);
        fcpa110_Fcpa110_New_Check_Prefix_A3 = fcpa110_Fcpa110_New_Check_NbrRedef1.newFieldInGroup("fcpa110_Fcpa110_New_Check_Prefix_A3", "FCPA110-NEW-CHECK-PREFIX-A3", 
            FieldType.STRING, 3);
        fcpa110_Fcpa110_New_Check_Prefix_A3Redef2 = fcpa110_Fcpa110_New_Check_NbrRedef1.newGroupInGroup("fcpa110_Fcpa110_New_Check_Prefix_A3Redef2", "Redefines", 
            fcpa110_Fcpa110_New_Check_Prefix_A3);
        fcpa110_Fcpa110_New_Check_Prefix_N3 = fcpa110_Fcpa110_New_Check_Prefix_A3Redef2.newFieldInGroup("fcpa110_Fcpa110_New_Check_Prefix_N3", "FCPA110-NEW-CHECK-PREFIX-N3", 
            FieldType.NUMERIC, 3);
        fcpa110_Fcpa110_New_Check_Nbr_A7 = fcpa110_Fcpa110_New_Check_NbrRedef1.newFieldInGroup("fcpa110_Fcpa110_New_Check_Nbr_A7", "FCPA110-NEW-CHECK-NBR-A7", 
            FieldType.STRING, 7);
        fcpa110_Fcpa110_New_Check_Nbr_A7Redef3 = fcpa110_Fcpa110_New_Check_NbrRedef1.newGroupInGroup("fcpa110_Fcpa110_New_Check_Nbr_A7Redef3", "Redefines", 
            fcpa110_Fcpa110_New_Check_Nbr_A7);
        fcpa110_Fcpa110_New_Check_Nbr_N7 = fcpa110_Fcpa110_New_Check_Nbr_A7Redef3.newFieldInGroup("fcpa110_Fcpa110_New_Check_Nbr_N7", "FCPA110-NEW-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        fcpa110_Fcpa110_Old_Check_Nbr = fcpa110_Fcpa110a.newFieldInGroup("fcpa110_Fcpa110_Old_Check_Nbr", "FCPA110-OLD-CHECK-NBR", FieldType.NUMERIC, 
            10);
        fcpa110_Fcpa110_Old_Check_NbrRedef4 = fcpa110_Fcpa110a.newGroupInGroup("fcpa110_Fcpa110_Old_Check_NbrRedef4", "Redefines", fcpa110_Fcpa110_Old_Check_Nbr);
        fcpa110_Fcpa110_Old_Check_Nbr_A10 = fcpa110_Fcpa110_Old_Check_NbrRedef4.newFieldInGroup("fcpa110_Fcpa110_Old_Check_Nbr_A10", "FCPA110-OLD-CHECK-NBR-A10", 
            FieldType.STRING, 10);
        fcpa110_Fcpa110_New_Sequence_Nbr = fcpa110_Fcpa110a.newFieldInGroup("fcpa110_Fcpa110_New_Sequence_Nbr", "FCPA110-NEW-SEQUENCE-NBR", FieldType.NUMERIC, 
            7);
        fcpa110_Filler1 = fcpa110_Fcpa110a.newFieldInGroup("fcpa110_Filler1", "FILLER1", FieldType.STRING, 53);
        fcpa110_Fcpa110b = fcpa110.newGroupInGroup("fcpa110_Fcpa110b", "FCPA110B");
        fcpa110_Fcpa110_Return_Code = fcpa110_Fcpa110b.newFieldInGroup("fcpa110_Fcpa110_Return_Code", "FCPA110-RETURN-CODE", FieldType.STRING, 4);
        fcpa110_Fcpa110_Return_Msg = fcpa110_Fcpa110b.newFieldInGroup("fcpa110_Fcpa110_Return_Msg", "FCPA110-RETURN-MSG", FieldType.STRING, 100);
        fcpa110_Rt_Table_Id = fcpa110_Fcpa110b.newFieldInGroup("fcpa110_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5);
        fcpa110_Rt_A_I_Ind = fcpa110_Fcpa110b.newFieldInGroup("fcpa110_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1);
        fcpa110_Rt_Short_Key = fcpa110_Fcpa110b.newFieldInGroup("fcpa110_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20);
        fcpa110_Rt_Long_Key = fcpa110_Fcpa110b.newFieldInGroup("fcpa110_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40);
        fcpa110_Rt_Long_KeyRedef5 = fcpa110_Fcpa110b.newGroupInGroup("fcpa110_Rt_Long_KeyRedef5", "Redefines", fcpa110_Rt_Long_Key);
        fcpa110_Routing_No = fcpa110_Rt_Long_KeyRedef5.newFieldInGroup("fcpa110_Routing_No", "ROUTING-NO", FieldType.STRING, 9);
        fcpa110_Filler_1 = fcpa110_Rt_Long_KeyRedef5.newFieldInGroup("fcpa110_Filler_1", "FILLER-1", FieldType.STRING, 1);
        fcpa110_Account_No = fcpa110_Rt_Long_KeyRedef5.newFieldInGroup("fcpa110_Account_No", "ACCOUNT-NO", FieldType.STRING, 21);
        fcpa110_Fller_9 = fcpa110_Rt_Long_KeyRedef5.newFieldInGroup("fcpa110_Fller_9", "FLLER-9", FieldType.STRING, 9);
        fcpa110_Company_Information = fcpa110_Fcpa110b.newGroupInGroup("fcpa110_Company_Information", "COMPANY-INFORMATION");
        fcpa110_Comp_Name_Label = fcpa110_Company_Information.newFieldInGroup("fcpa110_Comp_Name_Label", "COMP-NAME-LABEL", FieldType.STRING, 13);
        fcpa110_Company_Name = fcpa110_Company_Information.newFieldInGroup("fcpa110_Company_Name", "COMPANY-NAME", FieldType.STRING, 62);
        fcpa110_Comp_Addr_Label = fcpa110_Company_Information.newFieldInGroup("fcpa110_Comp_Addr_Label", "COMP-ADDR-LABEL", FieldType.STRING, 16);
        fcpa110_Company_Address = fcpa110_Company_Information.newFieldInGroup("fcpa110_Company_Address", "COMPANY-ADDRESS", FieldType.STRING, 59);
        fcpa110_Comp_Acct_Label = fcpa110_Company_Information.newFieldInGroup("fcpa110_Comp_Acct_Label", "COMP-ACCT-LABEL", FieldType.STRING, 8);
        fcpa110_Company_Account = fcpa110_Company_Information.newFieldInGroup("fcpa110_Company_Account", "COMPANY-ACCOUNT", FieldType.STRING, 6);
        fcpa110_Not_Valid_Label = fcpa110_Company_Information.newFieldInGroup("fcpa110_Not_Valid_Label", "NOT-VALID-LABEL", FieldType.STRING, 11);
        fcpa110_Not_Valid_Msg = fcpa110_Company_Information.newFieldInGroup("fcpa110_Not_Valid_Msg", "NOT-VALID-MSG", FieldType.STRING, 50);
        fcpa110_Filler2 = fcpa110_Company_Information.newFieldInGroup("fcpa110_Filler2", "FILLER2", FieldType.STRING, 25);
        fcpa110_Bank_Account_Data = fcpa110_Fcpa110b.newGroupInGroup("fcpa110_Bank_Account_Data", "BANK-ACCOUNT-DATA");
        fcpa110_Start_Check_Label = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Start_Check_Label", "START-CHECK-LABEL", FieldType.STRING, 15);
        fcpa110_Start_Check_No = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Start_Check_No", "START-CHECK-NO", FieldType.NUMERIC, 10);
        fcpa110_Start_Check_NoRedef6 = fcpa110_Bank_Account_Data.newGroupInGroup("fcpa110_Start_Check_NoRedef6", "Redefines", fcpa110_Start_Check_No);
        fcpa110_Start_Check_Prefix_A3 = fcpa110_Start_Check_NoRedef6.newFieldInGroup("fcpa110_Start_Check_Prefix_A3", "START-CHECK-PREFIX-A3", FieldType.STRING, 
            3);
        fcpa110_Start_Check_Prefix_A3Redef7 = fcpa110_Start_Check_NoRedef6.newGroupInGroup("fcpa110_Start_Check_Prefix_A3Redef7", "Redefines", fcpa110_Start_Check_Prefix_A3);
        fcpa110_Start_Check_Prefix_N3 = fcpa110_Start_Check_Prefix_A3Redef7.newFieldInGroup("fcpa110_Start_Check_Prefix_N3", "START-CHECK-PREFIX-N3", 
            FieldType.NUMERIC, 3);
        fcpa110_Start_Check_No_A7 = fcpa110_Start_Check_NoRedef6.newFieldInGroup("fcpa110_Start_Check_No_A7", "START-CHECK-NO-A7", FieldType.STRING, 7);
        fcpa110_Start_Check_No_A7Redef8 = fcpa110_Start_Check_NoRedef6.newGroupInGroup("fcpa110_Start_Check_No_A7Redef8", "Redefines", fcpa110_Start_Check_No_A7);
        fcpa110_Start_Check_No_N7 = fcpa110_Start_Check_No_A7Redef8.newFieldInGroup("fcpa110_Start_Check_No_N7", "START-CHECK-NO-N7", FieldType.NUMERIC, 
            7);
        fcpa110_End_Check_Label = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_End_Check_Label", "END-CHECK-LABEL", FieldType.STRING, 14);
        fcpa110_End_Check_No = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_End_Check_No", "END-CHECK-NO", FieldType.NUMERIC, 10);
        fcpa110_End_Check_NoRedef9 = fcpa110_Bank_Account_Data.newGroupInGroup("fcpa110_End_Check_NoRedef9", "Redefines", fcpa110_End_Check_No);
        fcpa110_End_Check_Prefix = fcpa110_End_Check_NoRedef9.newFieldInGroup("fcpa110_End_Check_Prefix", "END-CHECK-PREFIX", FieldType.NUMERIC, 3);
        fcpa110_End_Check_No_N7 = fcpa110_End_Check_NoRedef9.newFieldInGroup("fcpa110_End_Check_No_N7", "END-CHECK-NO-N7", FieldType.NUMERIC, 7);
        fcpa110_Space_26 = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Space_26", "SPACE-26", FieldType.STRING, 26);
        fcpa110_Start_Seq_Label = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Start_Seq_Label", "START-SEQ-LABEL", FieldType.STRING, 13);
        fcpa110_Start_Seq_No = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Start_Seq_No", "START-SEQ-NO", FieldType.NUMERIC, 7);
        fcpa110_End_Seq_Label = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_End_Seq_Label", "END-SEQ-LABEL", FieldType.STRING, 12);
        fcpa110_End_Seq_No = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_End_Seq_No", "END-SEQ-NO", FieldType.NUMERIC, 7);
        fcpa110_Signature_Label = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Signature_Label", "SIGNATURE-LABEL", FieldType.STRING, 16);
        fcpa110_Bank_Signature_Cde = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Bank_Signature_Cde", "BANK-SIGNATURE-CDE", FieldType.STRING, 5);
        fcpa110_Space_15 = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Space_15", "SPACE-15", FieldType.STRING, 15);
        fcpa110_Title_Label = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Title_Label", "TITLE-LABEL", FieldType.STRING, 6);
        fcpa110_Title = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Title", "TITLE", FieldType.STRING, 69);
        fcpa110_Pos_Pay_Label = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Pos_Pay_Label", "POS-PAY-LABEL", FieldType.STRING, 12);
        fcpa110_Bank_Internal_Pospay_Ind = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Bank_Internal_Pospay_Ind", "BANK-INTERNAL-POSPAY-IND", FieldType.STRING, 
            1);
        fcpa110_Orgn_Label = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Orgn_Label", "ORGN-LABEL", FieldType.STRING, 5);
        fcpa110_Bank_Orgn_Cde = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Bank_Orgn_Cde", "BANK-ORGN-CDE", FieldType.STRING, 2);
        fcpa110_Filler3 = fcpa110_Bank_Account_Data.newFieldInGroup("fcpa110_Filler3", "FILLER3", FieldType.STRING, 5);
        fcpa110_Bank_Name_Data = fcpa110_Fcpa110b.newGroupInGroup("fcpa110_Bank_Name_Data", "BANK-NAME-DATA");
        fcpa110_Bank_Name_Label = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Bank_Name_Label", "BANK-NAME-LABEL", FieldType.STRING, 10);
        fcpa110_Bank_Name = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Bank_Name", "BANK-NAME", FieldType.STRING, 65);
        fcpa110_Bank_Address_Label = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Bank_Address_Label", "BANK-ADDRESS-LABEL", FieldType.STRING, 13);
        fcpa110_Bank_Address1 = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Bank_Address1", "BANK-ADDRESS1", FieldType.STRING, 62);
        fcpa110_Bank_Above_Label = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Bank_Above_Label", "BANK-ABOVE-LABEL", FieldType.STRING, 15);
        fcpa110_Bank_Above_Check_Amt_Nbr = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Bank_Above_Check_Amt_Nbr", "BANK-ABOVE-CHECK-AMT-NBR", FieldType.STRING, 
            10);
        fcpa110_Bank_Trans_Label = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Bank_Trans_Label", "BANK-TRANS-LABEL", FieldType.STRING, 16);
        fcpa110_Bank_Transmit_Ind = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Bank_Transmit_Ind", "BANK-TRANSMIT-IND", FieldType.STRING, 1);
        fcpa110_Check_Ind_Label = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Check_Ind_Label", "CHECK-IND-LABEL", FieldType.STRING, 16);
        fcpa110_Check_Acct_Ind = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Check_Acct_Ind", "CHECK-ACCT-IND", FieldType.STRING, 1);
        fcpa110_Eft_Ind_Label = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Eft_Ind_Label", "EFT-IND-LABEL", FieldType.STRING, 14);
        fcpa110_Eft_Acct_Ind = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Eft_Acct_Ind", "EFT-ACCT-IND", FieldType.STRING, 1);
        fcpa110_Space_1 = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Space_1", "SPACE-1", FieldType.STRING, 1);
        fcpa110_Bank_Transmission_Label = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Bank_Transmission_Label", "BANK-TRANSMISSION-LABEL", FieldType.STRING, 
            16);
        fcpa110_Bank_Transmission_Cde = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Bank_Transmission_Cde", "BANK-TRANSMISSION-CDE", FieldType.STRING, 
            5);
        fcpa110_Filler4 = fcpa110_Bank_Name_Data.newFieldInGroup("fcpa110_Filler4", "FILLER4", FieldType.STRING, 4);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa110(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

