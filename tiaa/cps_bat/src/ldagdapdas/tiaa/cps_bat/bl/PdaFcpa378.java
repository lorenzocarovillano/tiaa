/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:20 PM
**        * FROM NATURAL PDA     : FCPA378
************************************************************
**        * FILE NAME            : PdaFcpa378.java
**        * CLASS NAME           : PdaFcpa378
**        * INSTANCE NAME        : PdaFcpa378
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa378 extends PdaBase
{
    // Properties
    private DbsField pnd_Pymnt_Ext_Key;
    private DbsGroup pnd_Pymnt_Ext_KeyRedef1;
    private DbsGroup pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num;
    private DbsGroup pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr;
    private DbsGroup pnd_Record_Type_10;
    private DbsField pnd_Record_Type_10_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Payee_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Invrse_Dte;
    private DbsField pnd_Record_Type_10_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Crrncy_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Orgn_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Qlfied_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Record_Type_10_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Record_Type_10_Cntrct_Sps_Cde;
    private DbsField pnd_Record_Type_10_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Record_Type_10_Pymnt_Stats_Cde;
    private DbsField pnd_Record_Type_10_Pymnt_Annot_Ind;
    private DbsField pnd_Record_Type_10_Pymnt_Cmbne_Ind;
    private DbsField pnd_Record_Type_10_Pymnt_Ftre_Ind;
    private DbsField pnd_Record_Type_10_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Record_Type_10_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Record_Type_10_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Record_Type_10_Annt_Soc_Sec_Ind;
    private DbsField pnd_Record_Type_10_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Record_Type_10_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Record_Type_10_Cntrct_Rqst_Dte;
    private DbsField pnd_Record_Type_10_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Record_Type_10_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Type_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Lob_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Cref_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Option_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Mode_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Hold_Cde;
    private DbsField pnd_Record_Type_10_Cntrct_Hold_Grp;
    private DbsField pnd_Record_Type_10_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Record_Type_10_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Record_Type_10_Annt_Ctznshp_Cde;
    private DbsField pnd_Record_Type_10_Annt_Rsdncy_Cde;
    private DbsField pnd_Record_Type_10_Pymnt_Split_Cde;
    private DbsField pnd_Record_Type_10_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Record_Type_10_Pymnt_Check_Dte;
    private DbsField pnd_Record_Type_10_Pymnt_Cycle_Dte;
    private DbsField pnd_Record_Type_10_Pymnt_Eft_Dte;
    private DbsField pnd_Record_Type_10_Pymnt_Rqst_Pct;
    private DbsField pnd_Record_Type_10_Pymnt_Rqst_Amt;
    private DbsField pnd_Record_Type_10_Pymnt_Check_Nbr;
    private DbsField pnd_Record_Type_10_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pnd_Record_Type_10_Pymnt_Prcss_Seq_NbrRedef3;
    private DbsField pnd_Record_Type_10_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Record_Type_10_Pymnt_Instmt_Nbr;
    private DbsField pnd_Record_Type_10_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Record_Type_10_Pymnt_Check_Amt;
    private DbsField pnd_Record_Type_10_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Record_Type_10_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Record_Type_10_Cntrct_Hold_Tme;
    private DbsField pnd_Record_Type_10_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Record_Type_10_Pymnt_Acctg_Dte;
    private DbsField pnd_Record_Type_10_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Record_Type_10_Pymnt_Intrfce_Dte;
    private DbsField pnd_Record_Type_10_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Record_Type_10_Pnd_Acfs;
    private DbsField pnd_Record_Type_10_Pnd_Funds_Per_Pymnt;
    private DbsField pnd_Record_Type_10_Pymnt_Spouse_Pay_Stats;
    private DbsField pnd_Record_Type_10_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Record_Type_10_Egtrra_Eligibility_Ind;
    private DbsGroup pnd_Record_Type_30;
    private DbsGroup pnd_Record_Type_30_Pnd_Record_Type_30_Grp1;
    private DbsField pnd_Record_Type_30_Rcrd_Typ;
    private DbsField pnd_Record_Type_30_Cntrct_Orgn_Cde;
    private DbsField pnd_Record_Type_30_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Record_Type_30_Cntrct_Payee_Cde;
    private DbsField pnd_Record_Type_30_Cntrct_Invrse_Dte;
    private DbsField pnd_Record_Type_30_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pnd_Record_Type_30_Ph_Name;
    private DbsField pnd_Record_Type_30_Ph_Last_Name;
    private DbsField pnd_Record_Type_30_Ph_First_Name;
    private DbsField pnd_Record_Type_30_Ph_Middle_Name;
    private DbsGroup pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp;
    private DbsField pnd_Record_Type_30_Pymnt_Nme;
    private DbsField pnd_Record_Type_30_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Record_Type_30_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Record_Type_30_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Record_Type_30_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Record_Type_30_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Record_Type_30_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Record_Type_30_Pymnt_Addr_Zip_Cde;
    private DbsField pnd_Record_Type_30_Pymnt_Postl_Data;
    private DbsField pnd_Record_Type_30_Pymnt_Addr_Type_Ind;
    private DbsField pnd_Record_Type_30_Pymnt_Addr_Last_Chg_Dte;
    private DbsField pnd_Record_Type_30_Pymnt_Addr_Last_Chg_Tme;
    private DbsGroup pnd_Record_Type_30_Pnd_Record_Type_30_Grp2;
    private DbsField pnd_Record_Type_30_Pymnt_Eft_Transit_Id;
    private DbsField pnd_Record_Type_30_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Record_Type_30_Pymnt_Chk_Sav_Ind;
    private DbsField pnd_Record_Type_30_Pymnt_Deceased_Nme;
    private DbsField pnd_Record_Type_30_Cntrct_Hold_Tme;
    private DbsGroup pnd_Record_Type_40;
    private DbsGroup pnd_Record_Type_40_Pnd_Record_Type_40_Grp1;
    private DbsField pnd_Record_Type_40_Cntrct_Orgn_Cde;
    private DbsField pnd_Record_Type_40_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Record_Type_40_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Record_Type_40_Pymnt_Check_Dte;
    private DbsField pnd_Record_Type_40_Cntrct_Good_Contract;
    private DbsField pnd_Record_Type_40_Cntrct_Invalid_Cond_Ind;
    private DbsField pnd_Record_Type_40_Cntrct_Multi_Payee;
    private DbsField pnd_Record_Type_40_Pnd_Good_Letter;
    private DbsField pnd_Record_Type_40_Pnd_Plan_Type_Display;
    private DbsField pnd_Record_Type_40_Cntrct_Ivc_Amt;
    private DbsField pnd_Record_Type_40_Pnd_Plan_Data_Top;
    private DbsGroup pnd_Record_Type_40_Plan_Data;
    private DbsField pnd_Record_Type_40_Plan_Employer_Name;
    private DbsField pnd_Record_Type_40_Plan_Type;
    private DbsField pnd_Record_Type_40_Plan_Cash_Avail;
    private DbsField pnd_Record_Type_40_Plan_Employer_Cntrb;
    private DbsField pnd_Record_Type_40_Plan_Employer_Cntrb_Earn;
    private DbsField pnd_Record_Type_40_Plan_Employee_Rdctn;
    private DbsField pnd_Record_Type_40_Plan_Employee_Rdctn_Earn;
    private DbsField pnd_Record_Type_40_Plan_Employee_Ddctn;
    private DbsField pnd_Record_Type_40_Plan_Employee_Ddctn_Earn;
    private DbsField pnd_Record_Type_40_Plan_Acc_123186;
    private DbsField pnd_Record_Type_40_Plan_Acc_123188;
    private DbsField pnd_Record_Type_40_Plan_Acc_123188_Rdctn;
    private DbsField pnd_Record_Type_40_Plan_Acc_123188_Rdctn_Earn;

    public DbsField getPnd_Pymnt_Ext_Key() { return pnd_Pymnt_Ext_Key; }

    public DbsGroup getPnd_Pymnt_Ext_KeyRedef1() { return pnd_Pymnt_Ext_KeyRedef1; }

    public DbsGroup getPnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail() { return pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd() { return pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd() { return pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex() { return pnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num() { return pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num; }

    public DbsGroup getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2() { return pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num() { return pnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num() { return pnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code() { return pnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr() { return pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr() { return pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr() { return pnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr; }

    public DbsGroup getPnd_Record_Type_10() { return pnd_Record_Type_10; }

    public DbsField getPnd_Record_Type_10_Cntrct_Ppcn_Nbr() { return pnd_Record_Type_10_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Payee_Cde() { return pnd_Record_Type_10_Cntrct_Payee_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Invrse_Dte() { return pnd_Record_Type_10_Cntrct_Invrse_Dte; }

    public DbsField getPnd_Record_Type_10_Cntrct_Check_Crrncy_Cde() { return pnd_Record_Type_10_Cntrct_Check_Crrncy_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Crrncy_Cde() { return pnd_Record_Type_10_Cntrct_Crrncy_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Orgn_Cde() { return pnd_Record_Type_10_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Qlfied_Cde() { return pnd_Record_Type_10_Cntrct_Qlfied_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Pymnt_Type_Ind() { return pnd_Record_Type_10_Cntrct_Pymnt_Type_Ind; }

    public DbsField getPnd_Record_Type_10_Cntrct_Sttlmnt_Type_Ind() { return pnd_Record_Type_10_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getPnd_Record_Type_10_Cntrct_Sps_Cde() { return pnd_Record_Type_10_Cntrct_Sps_Cde; }

    public DbsField getPnd_Record_Type_10_Pymnt_Rqst_Rmndr_Pct() { return pnd_Record_Type_10_Pymnt_Rqst_Rmndr_Pct; }

    public DbsField getPnd_Record_Type_10_Pymnt_Stats_Cde() { return pnd_Record_Type_10_Pymnt_Stats_Cde; }

    public DbsField getPnd_Record_Type_10_Pymnt_Annot_Ind() { return pnd_Record_Type_10_Pymnt_Annot_Ind; }

    public DbsField getPnd_Record_Type_10_Pymnt_Cmbne_Ind() { return pnd_Record_Type_10_Pymnt_Cmbne_Ind; }

    public DbsField getPnd_Record_Type_10_Pymnt_Ftre_Ind() { return pnd_Record_Type_10_Pymnt_Ftre_Ind; }

    public DbsField getPnd_Record_Type_10_Pymnt_Payee_Na_Addr_Trggr() { return pnd_Record_Type_10_Pymnt_Payee_Na_Addr_Trggr; }

    public DbsField getPnd_Record_Type_10_Pymnt_Payee_Tx_Elct_Trggr() { return pnd_Record_Type_10_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getPnd_Record_Type_10_Pymnt_Inst_Rep_Cde() { return pnd_Record_Type_10_Pymnt_Inst_Rep_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Dvdnd_Payee_Ind() { return pnd_Record_Type_10_Cntrct_Dvdnd_Payee_Ind; }

    public DbsField getPnd_Record_Type_10_Annt_Soc_Sec_Ind() { return pnd_Record_Type_10_Annt_Soc_Sec_Ind; }

    public DbsField getPnd_Record_Type_10_Pymnt_Pay_Type_Req_Ind() { return pnd_Record_Type_10_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPnd_Record_Type_10_Cntrct_Rqst_Settl_Dte() { return pnd_Record_Type_10_Cntrct_Rqst_Settl_Dte; }

    public DbsField getPnd_Record_Type_10_Cntrct_Rqst_Dte() { return pnd_Record_Type_10_Cntrct_Rqst_Dte; }

    public DbsField getPnd_Record_Type_10_Cntrct_Rqst_Settl_Tme() { return pnd_Record_Type_10_Cntrct_Rqst_Settl_Tme; }

    public DbsField getPnd_Record_Type_10_Cntrct_Unq_Id_Nbr() { return pnd_Record_Type_10_Cntrct_Unq_Id_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Type_Cde() { return pnd_Record_Type_10_Cntrct_Type_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Lob_Cde() { return pnd_Record_Type_10_Cntrct_Lob_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Sub_Lob_Cde() { return pnd_Record_Type_10_Cntrct_Sub_Lob_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Ia_Lob_Cde() { return pnd_Record_Type_10_Cntrct_Ia_Lob_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Cref_Nbr() { return pnd_Record_Type_10_Cntrct_Cref_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Option_Cde() { return pnd_Record_Type_10_Cntrct_Option_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Mode_Cde() { return pnd_Record_Type_10_Cntrct_Mode_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Pymnt_Dest_Cde() { return pnd_Record_Type_10_Cntrct_Pymnt_Dest_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Roll_Dest_Cde() { return pnd_Record_Type_10_Cntrct_Roll_Dest_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Dvdnd_Payee_Cde() { return pnd_Record_Type_10_Cntrct_Dvdnd_Payee_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Hold_Cde() { return pnd_Record_Type_10_Cntrct_Hold_Cde; }

    public DbsField getPnd_Record_Type_10_Cntrct_Hold_Grp() { return pnd_Record_Type_10_Cntrct_Hold_Grp; }

    public DbsField getPnd_Record_Type_10_Cntrct_Da_Tiaa_1_Nbr() { return pnd_Record_Type_10_Cntrct_Da_Tiaa_1_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Da_Tiaa_2_Nbr() { return pnd_Record_Type_10_Cntrct_Da_Tiaa_2_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Da_Tiaa_3_Nbr() { return pnd_Record_Type_10_Cntrct_Da_Tiaa_3_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Da_Tiaa_4_Nbr() { return pnd_Record_Type_10_Cntrct_Da_Tiaa_4_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Da_Tiaa_5_Nbr() { return pnd_Record_Type_10_Cntrct_Da_Tiaa_5_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Da_Cref_1_Nbr() { return pnd_Record_Type_10_Cntrct_Da_Cref_1_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Da_Cref_2_Nbr() { return pnd_Record_Type_10_Cntrct_Da_Cref_2_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Da_Cref_3_Nbr() { return pnd_Record_Type_10_Cntrct_Da_Cref_3_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Da_Cref_4_Nbr() { return pnd_Record_Type_10_Cntrct_Da_Cref_4_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Da_Cref_5_Nbr() { return pnd_Record_Type_10_Cntrct_Da_Cref_5_Nbr; }

    public DbsField getPnd_Record_Type_10_Annt_Soc_Sec_Nbr() { return pnd_Record_Type_10_Annt_Soc_Sec_Nbr; }

    public DbsField getPnd_Record_Type_10_Annt_Ctznshp_Cde() { return pnd_Record_Type_10_Annt_Ctznshp_Cde; }

    public DbsField getPnd_Record_Type_10_Annt_Rsdncy_Cde() { return pnd_Record_Type_10_Annt_Rsdncy_Cde; }

    public DbsField getPnd_Record_Type_10_Pymnt_Split_Cde() { return pnd_Record_Type_10_Pymnt_Split_Cde; }

    public DbsField getPnd_Record_Type_10_Pymnt_Split_Reasn_Cde() { return pnd_Record_Type_10_Pymnt_Split_Reasn_Cde; }

    public DbsField getPnd_Record_Type_10_Pymnt_Check_Dte() { return pnd_Record_Type_10_Pymnt_Check_Dte; }

    public DbsField getPnd_Record_Type_10_Pymnt_Cycle_Dte() { return pnd_Record_Type_10_Pymnt_Cycle_Dte; }

    public DbsField getPnd_Record_Type_10_Pymnt_Eft_Dte() { return pnd_Record_Type_10_Pymnt_Eft_Dte; }

    public DbsField getPnd_Record_Type_10_Pymnt_Rqst_Pct() { return pnd_Record_Type_10_Pymnt_Rqst_Pct; }

    public DbsField getPnd_Record_Type_10_Pymnt_Rqst_Amt() { return pnd_Record_Type_10_Pymnt_Rqst_Amt; }

    public DbsField getPnd_Record_Type_10_Pymnt_Check_Nbr() { return pnd_Record_Type_10_Pymnt_Check_Nbr; }

    public DbsField getPnd_Record_Type_10_Pymnt_Prcss_Seq_Nbr() { return pnd_Record_Type_10_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPnd_Record_Type_10_Pymnt_Prcss_Seq_NbrRedef3() { return pnd_Record_Type_10_Pymnt_Prcss_Seq_NbrRedef3; }

    public DbsField getPnd_Record_Type_10_Pymnt_Prcss_Seq_Num() { return pnd_Record_Type_10_Pymnt_Prcss_Seq_Num; }

    public DbsField getPnd_Record_Type_10_Pymnt_Instmt_Nbr() { return pnd_Record_Type_10_Pymnt_Instmt_Nbr; }

    public DbsField getPnd_Record_Type_10_Pymnt_Check_Scrty_Nbr() { return pnd_Record_Type_10_Pymnt_Check_Scrty_Nbr; }

    public DbsField getPnd_Record_Type_10_Pymnt_Check_Amt() { return pnd_Record_Type_10_Pymnt_Check_Amt; }

    public DbsField getPnd_Record_Type_10_Pymnt_Settlmnt_Dte() { return pnd_Record_Type_10_Pymnt_Settlmnt_Dte; }

    public DbsField getPnd_Record_Type_10_Pymnt_Check_Seq_Nbr() { return pnd_Record_Type_10_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Ec_Oprtr_Id_Nbr() { return pnd_Record_Type_10_Cntrct_Ec_Oprtr_Id_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Cmbn_Nbr() { return pnd_Record_Type_10_Cntrct_Cmbn_Nbr; }

    public DbsField getPnd_Record_Type_10_Cntrct_Hold_Tme() { return pnd_Record_Type_10_Cntrct_Hold_Tme; }

    public DbsField getPnd_Record_Type_10_Cntrct_Cancel_Rdrw_Ind() { return pnd_Record_Type_10_Cntrct_Cancel_Rdrw_Ind; }

    public DbsField getPnd_Record_Type_10_Pymnt_Acctg_Dte() { return pnd_Record_Type_10_Pymnt_Acctg_Dte; }

    public DbsField getPnd_Record_Type_10_Pymnt_Reqst_Log_Dte_Time() { return pnd_Record_Type_10_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getPnd_Record_Type_10_Pymnt_Intrfce_Dte() { return pnd_Record_Type_10_Pymnt_Intrfce_Dte; }

    public DbsField getPnd_Record_Type_10_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pnd_Record_Type_10_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPnd_Record_Type_10_Pnd_Acfs() { return pnd_Record_Type_10_Pnd_Acfs; }

    public DbsField getPnd_Record_Type_10_Pnd_Funds_Per_Pymnt() { return pnd_Record_Type_10_Pnd_Funds_Per_Pymnt; }

    public DbsField getPnd_Record_Type_10_Pymnt_Spouse_Pay_Stats() { return pnd_Record_Type_10_Pymnt_Spouse_Pay_Stats; }

    public DbsField getPnd_Record_Type_10_Cnr_Orgnl_Invrse_Dte() { return pnd_Record_Type_10_Cnr_Orgnl_Invrse_Dte; }

    public DbsField getPnd_Record_Type_10_Egtrra_Eligibility_Ind() { return pnd_Record_Type_10_Egtrra_Eligibility_Ind; }

    public DbsGroup getPnd_Record_Type_30() { return pnd_Record_Type_30; }

    public DbsGroup getPnd_Record_Type_30_Pnd_Record_Type_30_Grp1() { return pnd_Record_Type_30_Pnd_Record_Type_30_Grp1; }

    public DbsField getPnd_Record_Type_30_Rcrd_Typ() { return pnd_Record_Type_30_Rcrd_Typ; }

    public DbsField getPnd_Record_Type_30_Cntrct_Orgn_Cde() { return pnd_Record_Type_30_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Record_Type_30_Cntrct_Ppcn_Nbr() { return pnd_Record_Type_30_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Record_Type_30_Cntrct_Payee_Cde() { return pnd_Record_Type_30_Cntrct_Payee_Cde; }

    public DbsField getPnd_Record_Type_30_Cntrct_Invrse_Dte() { return pnd_Record_Type_30_Cntrct_Invrse_Dte; }

    public DbsField getPnd_Record_Type_30_Pymnt_Prcss_Seq_Nbr() { return pnd_Record_Type_30_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPnd_Record_Type_30_Ph_Name() { return pnd_Record_Type_30_Ph_Name; }

    public DbsField getPnd_Record_Type_30_Ph_Last_Name() { return pnd_Record_Type_30_Ph_Last_Name; }

    public DbsField getPnd_Record_Type_30_Ph_First_Name() { return pnd_Record_Type_30_Ph_First_Name; }

    public DbsField getPnd_Record_Type_30_Ph_Middle_Name() { return pnd_Record_Type_30_Ph_Middle_Name; }

    public DbsGroup getPnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp() { return pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getPnd_Record_Type_30_Pymnt_Nme() { return pnd_Record_Type_30_Pymnt_Nme; }

    public DbsField getPnd_Record_Type_30_Pymnt_Addr_Line1_Txt() { return pnd_Record_Type_30_Pymnt_Addr_Line1_Txt; }

    public DbsField getPnd_Record_Type_30_Pymnt_Addr_Line2_Txt() { return pnd_Record_Type_30_Pymnt_Addr_Line2_Txt; }

    public DbsField getPnd_Record_Type_30_Pymnt_Addr_Line3_Txt() { return pnd_Record_Type_30_Pymnt_Addr_Line3_Txt; }

    public DbsField getPnd_Record_Type_30_Pymnt_Addr_Line4_Txt() { return pnd_Record_Type_30_Pymnt_Addr_Line4_Txt; }

    public DbsField getPnd_Record_Type_30_Pymnt_Addr_Line5_Txt() { return pnd_Record_Type_30_Pymnt_Addr_Line5_Txt; }

    public DbsField getPnd_Record_Type_30_Pymnt_Addr_Line6_Txt() { return pnd_Record_Type_30_Pymnt_Addr_Line6_Txt; }

    public DbsField getPnd_Record_Type_30_Pymnt_Addr_Zip_Cde() { return pnd_Record_Type_30_Pymnt_Addr_Zip_Cde; }

    public DbsField getPnd_Record_Type_30_Pymnt_Postl_Data() { return pnd_Record_Type_30_Pymnt_Postl_Data; }

    public DbsField getPnd_Record_Type_30_Pymnt_Addr_Type_Ind() { return pnd_Record_Type_30_Pymnt_Addr_Type_Ind; }

    public DbsField getPnd_Record_Type_30_Pymnt_Addr_Last_Chg_Dte() { return pnd_Record_Type_30_Pymnt_Addr_Last_Chg_Dte; }

    public DbsField getPnd_Record_Type_30_Pymnt_Addr_Last_Chg_Tme() { return pnd_Record_Type_30_Pymnt_Addr_Last_Chg_Tme; }

    public DbsGroup getPnd_Record_Type_30_Pnd_Record_Type_30_Grp2() { return pnd_Record_Type_30_Pnd_Record_Type_30_Grp2; }

    public DbsField getPnd_Record_Type_30_Pymnt_Eft_Transit_Id() { return pnd_Record_Type_30_Pymnt_Eft_Transit_Id; }

    public DbsField getPnd_Record_Type_30_Pymnt_Eft_Acct_Nbr() { return pnd_Record_Type_30_Pymnt_Eft_Acct_Nbr; }

    public DbsField getPnd_Record_Type_30_Pymnt_Chk_Sav_Ind() { return pnd_Record_Type_30_Pymnt_Chk_Sav_Ind; }

    public DbsField getPnd_Record_Type_30_Pymnt_Deceased_Nme() { return pnd_Record_Type_30_Pymnt_Deceased_Nme; }

    public DbsField getPnd_Record_Type_30_Cntrct_Hold_Tme() { return pnd_Record_Type_30_Cntrct_Hold_Tme; }

    public DbsGroup getPnd_Record_Type_40() { return pnd_Record_Type_40; }

    public DbsGroup getPnd_Record_Type_40_Pnd_Record_Type_40_Grp1() { return pnd_Record_Type_40_Pnd_Record_Type_40_Grp1; }

    public DbsField getPnd_Record_Type_40_Cntrct_Orgn_Cde() { return pnd_Record_Type_40_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Record_Type_40_Cntrct_Ppcn_Nbr() { return pnd_Record_Type_40_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Record_Type_40_Pymnt_Prcss_Seq_Nbr() { return pnd_Record_Type_40_Pymnt_Prcss_Seq_Nbr; }

    public DbsField getPnd_Record_Type_40_Pymnt_Check_Dte() { return pnd_Record_Type_40_Pymnt_Check_Dte; }

    public DbsField getPnd_Record_Type_40_Cntrct_Good_Contract() { return pnd_Record_Type_40_Cntrct_Good_Contract; }

    public DbsField getPnd_Record_Type_40_Cntrct_Invalid_Cond_Ind() { return pnd_Record_Type_40_Cntrct_Invalid_Cond_Ind; }

    public DbsField getPnd_Record_Type_40_Cntrct_Multi_Payee() { return pnd_Record_Type_40_Cntrct_Multi_Payee; }

    public DbsField getPnd_Record_Type_40_Pnd_Good_Letter() { return pnd_Record_Type_40_Pnd_Good_Letter; }

    public DbsField getPnd_Record_Type_40_Pnd_Plan_Type_Display() { return pnd_Record_Type_40_Pnd_Plan_Type_Display; }

    public DbsField getPnd_Record_Type_40_Cntrct_Ivc_Amt() { return pnd_Record_Type_40_Cntrct_Ivc_Amt; }

    public DbsField getPnd_Record_Type_40_Pnd_Plan_Data_Top() { return pnd_Record_Type_40_Pnd_Plan_Data_Top; }

    public DbsGroup getPnd_Record_Type_40_Plan_Data() { return pnd_Record_Type_40_Plan_Data; }

    public DbsField getPnd_Record_Type_40_Plan_Employer_Name() { return pnd_Record_Type_40_Plan_Employer_Name; }

    public DbsField getPnd_Record_Type_40_Plan_Type() { return pnd_Record_Type_40_Plan_Type; }

    public DbsField getPnd_Record_Type_40_Plan_Cash_Avail() { return pnd_Record_Type_40_Plan_Cash_Avail; }

    public DbsField getPnd_Record_Type_40_Plan_Employer_Cntrb() { return pnd_Record_Type_40_Plan_Employer_Cntrb; }

    public DbsField getPnd_Record_Type_40_Plan_Employer_Cntrb_Earn() { return pnd_Record_Type_40_Plan_Employer_Cntrb_Earn; }

    public DbsField getPnd_Record_Type_40_Plan_Employee_Rdctn() { return pnd_Record_Type_40_Plan_Employee_Rdctn; }

    public DbsField getPnd_Record_Type_40_Plan_Employee_Rdctn_Earn() { return pnd_Record_Type_40_Plan_Employee_Rdctn_Earn; }

    public DbsField getPnd_Record_Type_40_Plan_Employee_Ddctn() { return pnd_Record_Type_40_Plan_Employee_Ddctn; }

    public DbsField getPnd_Record_Type_40_Plan_Employee_Ddctn_Earn() { return pnd_Record_Type_40_Plan_Employee_Ddctn_Earn; }

    public DbsField getPnd_Record_Type_40_Plan_Acc_123186() { return pnd_Record_Type_40_Plan_Acc_123186; }

    public DbsField getPnd_Record_Type_40_Plan_Acc_123188() { return pnd_Record_Type_40_Plan_Acc_123188; }

    public DbsField getPnd_Record_Type_40_Plan_Acc_123188_Rdctn() { return pnd_Record_Type_40_Plan_Acc_123188_Rdctn; }

    public DbsField getPnd_Record_Type_40_Plan_Acc_123188_Rdctn_Earn() { return pnd_Record_Type_40_Plan_Acc_123188_Rdctn_Earn; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Pymnt_Ext_Key = dbsRecord.newFieldInRecord("pnd_Pymnt_Ext_Key", "#PYMNT-EXT-KEY", FieldType.STRING, 41);
        pnd_Pymnt_Ext_Key.setParameterOption(ParameterOption.ByReference);
        pnd_Pymnt_Ext_KeyRedef1 = dbsRecord.newGroupInRecord("pnd_Pymnt_Ext_KeyRedef1", "Redefines", pnd_Pymnt_Ext_Key);
        pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail = pnd_Pymnt_Ext_KeyRedef1.newGroupInGroup("pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail", "#PYMNT-EXT-KEY-DETAIL");
        pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd", "#KEY-REC-LVL-#", 
            FieldType.NUMERIC, 2);
        pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd", 
            "#KEY-REC-OCCUR-#", FieldType.NUMERIC, 2);
        pnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex", 
            "#KEY-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num", 
            "#KEY-PYMNT-PRCSS-SEQ-NUM", FieldType.STRING, 7);
        pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2 = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newGroupInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2", 
            "Redefines", pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num);
        pnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num = pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num", "#KEY-SEQ-NUM", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num = pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num", 
            "#KEY-INST-NUM", FieldType.NUMERIC, 2);
        pnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code", 
            "#KEY-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr", 
            "#KEY-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr", 
            "#KEY-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr", 
            "#KEY-CNTRCT-CMBN-NBR", FieldType.STRING, 14);

        pnd_Record_Type_10 = dbsRecord.newGroupInRecord("pnd_Record_Type_10", "#RECORD-TYPE-10");
        pnd_Record_Type_10.setParameterOption(ParameterOption.ByReference);
        pnd_Record_Type_10_Cntrct_Ppcn_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Record_Type_10_Cntrct_Payee_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Record_Type_10_Cntrct_Invrse_Dte = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Record_Type_10_Cntrct_Check_Crrncy_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Record_Type_10_Cntrct_Crrncy_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 
            1);
        pnd_Record_Type_10_Cntrct_Orgn_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Record_Type_10_Cntrct_Qlfied_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 
            1);
        pnd_Record_Type_10_Cntrct_Pymnt_Type_Ind = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Record_Type_10_Cntrct_Sttlmnt_Type_Ind = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Record_Type_10_Cntrct_Sps_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", FieldType.STRING, 
            1);
        pnd_Record_Type_10_Pymnt_Rqst_Rmndr_Pct = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Rqst_Rmndr_Pct", "PYMNT-RQST-RMNDR-PCT", 
            FieldType.STRING, 1);
        pnd_Record_Type_10_Pymnt_Stats_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1);
        pnd_Record_Type_10_Pymnt_Annot_Ind = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 
            1);
        pnd_Record_Type_10_Pymnt_Cmbne_Ind = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 
            1);
        pnd_Record_Type_10_Pymnt_Ftre_Ind = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", FieldType.STRING, 
            1);
        pnd_Record_Type_10_Pymnt_Payee_Na_Addr_Trggr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Payee_Na_Addr_Trggr", "PYMNT-PAYEE-NA-ADDR-TRGGR", 
            FieldType.STRING, 1);
        pnd_Record_Type_10_Pymnt_Payee_Tx_Elct_Trggr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", 
            FieldType.STRING, 1);
        pnd_Record_Type_10_Pymnt_Inst_Rep_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Inst_Rep_Cde", "PYMNT-INST-REP-CDE", FieldType.STRING, 
            1);
        pnd_Record_Type_10_Cntrct_Dvdnd_Payee_Ind = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Dvdnd_Payee_Ind", "CNTRCT-DVDND-PAYEE-IND", 
            FieldType.NUMERIC, 1);
        pnd_Record_Type_10_Annt_Soc_Sec_Ind = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 
            1);
        pnd_Record_Type_10_Pymnt_Pay_Type_Req_Ind = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Record_Type_10_Cntrct_Rqst_Settl_Dte = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Rqst_Settl_Dte", "CNTRCT-RQST-SETTL-DTE", 
            FieldType.DATE);
        pnd_Record_Type_10_Cntrct_Rqst_Dte = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", FieldType.DATE);
        pnd_Record_Type_10_Cntrct_Rqst_Settl_Tme = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Rqst_Settl_Tme", "CNTRCT-RQST-SETTL-TME", 
            FieldType.TIME);
        pnd_Record_Type_10_Cntrct_Unq_Id_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Record_Type_10_Cntrct_Type_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Record_Type_10_Cntrct_Lob_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 
            4);
        pnd_Record_Type_10_Cntrct_Sub_Lob_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 
            4);
        pnd_Record_Type_10_Cntrct_Ia_Lob_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 
            2);
        pnd_Record_Type_10_Cntrct_Cref_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 
            10);
        pnd_Record_Type_10_Cntrct_Option_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Record_Type_10_Cntrct_Mode_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Record_Type_10_Cntrct_Pymnt_Dest_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", 
            FieldType.STRING, 4);
        pnd_Record_Type_10_Cntrct_Roll_Dest_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", 
            FieldType.STRING, 4);
        pnd_Record_Type_10_Cntrct_Dvdnd_Payee_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", 
            FieldType.STRING, 5);
        pnd_Record_Type_10_Cntrct_Hold_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            4);
        pnd_Record_Type_10_Cntrct_Hold_Grp = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 
            3);
        pnd_Record_Type_10_Cntrct_Da_Tiaa_1_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Da_Tiaa_1_Nbr", "CNTRCT-DA-TIAA-1-NBR", 
            FieldType.STRING, 8);
        pnd_Record_Type_10_Cntrct_Da_Tiaa_2_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Da_Tiaa_2_Nbr", "CNTRCT-DA-TIAA-2-NBR", 
            FieldType.STRING, 8);
        pnd_Record_Type_10_Cntrct_Da_Tiaa_3_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Da_Tiaa_3_Nbr", "CNTRCT-DA-TIAA-3-NBR", 
            FieldType.STRING, 8);
        pnd_Record_Type_10_Cntrct_Da_Tiaa_4_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Da_Tiaa_4_Nbr", "CNTRCT-DA-TIAA-4-NBR", 
            FieldType.STRING, 8);
        pnd_Record_Type_10_Cntrct_Da_Tiaa_5_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Da_Tiaa_5_Nbr", "CNTRCT-DA-TIAA-5-NBR", 
            FieldType.STRING, 8);
        pnd_Record_Type_10_Cntrct_Da_Cref_1_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", 
            FieldType.STRING, 8);
        pnd_Record_Type_10_Cntrct_Da_Cref_2_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", 
            FieldType.STRING, 8);
        pnd_Record_Type_10_Cntrct_Da_Cref_3_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Da_Cref_3_Nbr", "CNTRCT-DA-CREF-3-NBR", 
            FieldType.STRING, 8);
        pnd_Record_Type_10_Cntrct_Da_Cref_4_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Da_Cref_4_Nbr", "CNTRCT-DA-CREF-4-NBR", 
            FieldType.STRING, 8);
        pnd_Record_Type_10_Cntrct_Da_Cref_5_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Da_Cref_5_Nbr", "CNTRCT-DA-CREF-5-NBR", 
            FieldType.STRING, 8);
        pnd_Record_Type_10_Annt_Soc_Sec_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Record_Type_10_Annt_Ctznshp_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            2);
        pnd_Record_Type_10_Annt_Rsdncy_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 
            2);
        pnd_Record_Type_10_Pymnt_Split_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", FieldType.STRING, 
            2);
        pnd_Record_Type_10_Pymnt_Split_Reasn_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", 
            FieldType.STRING, 6);
        pnd_Record_Type_10_Pymnt_Check_Dte = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Record_Type_10_Pymnt_Cycle_Dte = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        pnd_Record_Type_10_Pymnt_Eft_Dte = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        pnd_Record_Type_10_Pymnt_Rqst_Pct = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Record_Type_10_Pymnt_Rqst_Amt = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Record_Type_10_Pymnt_Check_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Record_Type_10_Pymnt_Prcss_Seq_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);
        pnd_Record_Type_10_Pymnt_Prcss_Seq_NbrRedef3 = pnd_Record_Type_10.newGroupInGroup("pnd_Record_Type_10_Pymnt_Prcss_Seq_NbrRedef3", "Redefines", 
            pnd_Record_Type_10_Pymnt_Prcss_Seq_Nbr);
        pnd_Record_Type_10_Pymnt_Prcss_Seq_Num = pnd_Record_Type_10_Pymnt_Prcss_Seq_NbrRedef3.newFieldInGroup("pnd_Record_Type_10_Pymnt_Prcss_Seq_Num", 
            "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Record_Type_10_Pymnt_Instmt_Nbr = pnd_Record_Type_10_Pymnt_Prcss_Seq_NbrRedef3.newFieldInGroup("pnd_Record_Type_10_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Record_Type_10_Pymnt_Check_Scrty_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Record_Type_10_Pymnt_Check_Amt = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Record_Type_10_Pymnt_Settlmnt_Dte = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Record_Type_10_Pymnt_Check_Seq_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Record_Type_10_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Ec_Oprtr_Id_Nbr", "CNTRCT-EC-OPRTR-ID-NBR", 
            FieldType.STRING, 7);
        pnd_Record_Type_10_Cntrct_Cmbn_Nbr = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 
            14);
        pnd_Record_Type_10_Cntrct_Hold_Tme = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME);
        pnd_Record_Type_10_Cntrct_Cancel_Rdrw_Ind = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", 
            FieldType.STRING, 1);
        pnd_Record_Type_10_Pymnt_Acctg_Dte = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Record_Type_10_Pymnt_Reqst_Log_Dte_Time = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Record_Type_10_Pymnt_Intrfce_Dte = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Record_Type_10_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Record_Type_10_Pnd_Acfs = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pnd_Acfs", "#ACFS", FieldType.BOOLEAN);
        pnd_Record_Type_10_Pnd_Funds_Per_Pymnt = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pnd_Funds_Per_Pymnt", "#FUNDS-PER-PYMNT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Record_Type_10_Pymnt_Spouse_Pay_Stats = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", 
            FieldType.STRING, 1);
        pnd_Record_Type_10_Cnr_Orgnl_Invrse_Dte = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Record_Type_10_Egtrra_Eligibility_Ind = pnd_Record_Type_10.newFieldInGroup("pnd_Record_Type_10_Egtrra_Eligibility_Ind", "EGTRRA-ELIGIBILITY-IND", 
            FieldType.STRING, 1);

        pnd_Record_Type_30 = dbsRecord.newGroupInRecord("pnd_Record_Type_30", "#RECORD-TYPE-30");
        pnd_Record_Type_30.setParameterOption(ParameterOption.ByReference);
        pnd_Record_Type_30_Pnd_Record_Type_30_Grp1 = pnd_Record_Type_30.newGroupInGroup("pnd_Record_Type_30_Pnd_Record_Type_30_Grp1", "#RECORD-TYPE-30-GRP1");
        pnd_Record_Type_30_Rcrd_Typ = pnd_Record_Type_30_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Record_Type_30_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 
            1);
        pnd_Record_Type_30_Cntrct_Orgn_Cde = pnd_Record_Type_30_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Record_Type_30_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Record_Type_30_Cntrct_Ppcn_Nbr = pnd_Record_Type_30_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Record_Type_30_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Record_Type_30_Cntrct_Payee_Cde = pnd_Record_Type_30_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Record_Type_30_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Record_Type_30_Cntrct_Invrse_Dte = pnd_Record_Type_30_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Record_Type_30_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Record_Type_30_Pymnt_Prcss_Seq_Nbr = pnd_Record_Type_30_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Record_Type_30_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 7);
        pnd_Record_Type_30_Ph_Name = pnd_Record_Type_30_Pnd_Record_Type_30_Grp1.newGroupInGroup("pnd_Record_Type_30_Ph_Name", "PH-NAME");
        pnd_Record_Type_30_Ph_Last_Name = pnd_Record_Type_30_Ph_Name.newFieldInGroup("pnd_Record_Type_30_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Record_Type_30_Ph_First_Name = pnd_Record_Type_30_Ph_Name.newFieldInGroup("pnd_Record_Type_30_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 
            10);
        pnd_Record_Type_30_Ph_Middle_Name = pnd_Record_Type_30_Ph_Name.newFieldInGroup("pnd_Record_Type_30_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 
            12);
        pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp = pnd_Record_Type_30.newGroupInGroup("pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp", "#PYMNT-NME-AND-ADDR-GRP");
        pnd_Record_Type_30_Pymnt_Nme = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 
            38);
        pnd_Record_Type_30_Pymnt_Addr_Line1_Txt = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Addr_Line1_Txt", 
            "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 35);
        pnd_Record_Type_30_Pymnt_Addr_Line2_Txt = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Addr_Line2_Txt", 
            "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 35);
        pnd_Record_Type_30_Pymnt_Addr_Line3_Txt = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Addr_Line3_Txt", 
            "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 35);
        pnd_Record_Type_30_Pymnt_Addr_Line4_Txt = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Addr_Line4_Txt", 
            "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 35);
        pnd_Record_Type_30_Pymnt_Addr_Line5_Txt = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Addr_Line5_Txt", 
            "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 35);
        pnd_Record_Type_30_Pymnt_Addr_Line6_Txt = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Addr_Line6_Txt", 
            "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 35);
        pnd_Record_Type_30_Pymnt_Addr_Zip_Cde = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Addr_Zip_Cde", 
            "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 9);
        pnd_Record_Type_30_Pymnt_Postl_Data = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", 
            FieldType.STRING, 32);
        pnd_Record_Type_30_Pymnt_Addr_Type_Ind = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Addr_Type_Ind", 
            "PYMNT-ADDR-TYPE-IND", FieldType.STRING, 1);
        pnd_Record_Type_30_Pymnt_Addr_Last_Chg_Dte = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Addr_Last_Chg_Dte", 
            "PYMNT-ADDR-LAST-CHG-DTE", FieldType.NUMERIC, 8);
        pnd_Record_Type_30_Pymnt_Addr_Last_Chg_Tme = pnd_Record_Type_30_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Record_Type_30_Pymnt_Addr_Last_Chg_Tme", 
            "PYMNT-ADDR-LAST-CHG-TME", FieldType.NUMERIC, 7);
        pnd_Record_Type_30_Pnd_Record_Type_30_Grp2 = pnd_Record_Type_30.newGroupInGroup("pnd_Record_Type_30_Pnd_Record_Type_30_Grp2", "#RECORD-TYPE-30-GRP2");
        pnd_Record_Type_30_Pymnt_Eft_Transit_Id = pnd_Record_Type_30_Pnd_Record_Type_30_Grp2.newFieldInGroup("pnd_Record_Type_30_Pymnt_Eft_Transit_Id", 
            "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9);
        pnd_Record_Type_30_Pymnt_Eft_Acct_Nbr = pnd_Record_Type_30_Pnd_Record_Type_30_Grp2.newFieldInGroup("pnd_Record_Type_30_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", 
            FieldType.STRING, 21);
        pnd_Record_Type_30_Pymnt_Chk_Sav_Ind = pnd_Record_Type_30_Pnd_Record_Type_30_Grp2.newFieldInGroup("pnd_Record_Type_30_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", 
            FieldType.STRING, 1);
        pnd_Record_Type_30_Pymnt_Deceased_Nme = pnd_Record_Type_30_Pnd_Record_Type_30_Grp2.newFieldInGroup("pnd_Record_Type_30_Pymnt_Deceased_Nme", "PYMNT-DECEASED-NME", 
            FieldType.STRING, 38);
        pnd_Record_Type_30_Cntrct_Hold_Tme = pnd_Record_Type_30_Pnd_Record_Type_30_Grp2.newFieldInGroup("pnd_Record_Type_30_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", 
            FieldType.TIME);

        pnd_Record_Type_40 = dbsRecord.newGroupInRecord("pnd_Record_Type_40", "#RECORD-TYPE-40");
        pnd_Record_Type_40.setParameterOption(ParameterOption.ByReference);
        pnd_Record_Type_40_Pnd_Record_Type_40_Grp1 = pnd_Record_Type_40.newGroupInGroup("pnd_Record_Type_40_Pnd_Record_Type_40_Grp1", "#RECORD-TYPE-40-GRP1");
        pnd_Record_Type_40_Cntrct_Orgn_Cde = pnd_Record_Type_40_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Record_Type_40_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Record_Type_40_Cntrct_Ppcn_Nbr = pnd_Record_Type_40_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Record_Type_40_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Record_Type_40_Pymnt_Prcss_Seq_Nbr = pnd_Record_Type_40_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Record_Type_40_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        pnd_Record_Type_40_Pymnt_Check_Dte = pnd_Record_Type_40_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Record_Type_40_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", 
            FieldType.DATE);
        pnd_Record_Type_40_Cntrct_Good_Contract = pnd_Record_Type_40_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Record_Type_40_Cntrct_Good_Contract", 
            "CNTRCT-GOOD-CONTRACT", FieldType.BOOLEAN);
        pnd_Record_Type_40_Cntrct_Invalid_Cond_Ind = pnd_Record_Type_40_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Record_Type_40_Cntrct_Invalid_Cond_Ind", 
            "CNTRCT-INVALID-COND-IND", FieldType.BOOLEAN);
        pnd_Record_Type_40_Cntrct_Multi_Payee = pnd_Record_Type_40_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Record_Type_40_Cntrct_Multi_Payee", "CNTRCT-MULTI-PAYEE", 
            FieldType.BOOLEAN);
        pnd_Record_Type_40_Pnd_Good_Letter = pnd_Record_Type_40_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Record_Type_40_Pnd_Good_Letter", "#GOOD-LETTER", 
            FieldType.BOOLEAN);
        pnd_Record_Type_40_Pnd_Plan_Type_Display = pnd_Record_Type_40_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Record_Type_40_Pnd_Plan_Type_Display", 
            "#PLAN-TYPE-DISPLAY", FieldType.BOOLEAN);
        pnd_Record_Type_40_Cntrct_Ivc_Amt = pnd_Record_Type_40_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Record_Type_40_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Record_Type_40_Pnd_Plan_Data_Top = pnd_Record_Type_40.newFieldInGroup("pnd_Record_Type_40_Pnd_Plan_Data_Top", "#PLAN-DATA-TOP", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Record_Type_40_Plan_Data = pnd_Record_Type_40.newGroupInGroup("pnd_Record_Type_40_Plan_Data", "PLAN-DATA");
        pnd_Record_Type_40_Plan_Employer_Name = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Employer_Name", "PLAN-EMPLOYER-NAME", 
            FieldType.STRING, 35);
        pnd_Record_Type_40_Plan_Type = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Type", "PLAN-TYPE", FieldType.STRING, 10);
        pnd_Record_Type_40_Plan_Cash_Avail = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Cash_Avail", "PLAN-CASH-AVAIL", FieldType.STRING, 
            50);
        pnd_Record_Type_40_Plan_Employer_Cntrb = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Employer_Cntrb", "PLAN-EMPLOYER-CNTRB", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Record_Type_40_Plan_Employer_Cntrb_Earn = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Employer_Cntrb_Earn", "PLAN-EMPLOYER-CNTRB-EARN", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Record_Type_40_Plan_Employee_Rdctn = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Employee_Rdctn", "PLAN-EMPLOYEE-RDCTN", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Record_Type_40_Plan_Employee_Rdctn_Earn = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Employee_Rdctn_Earn", "PLAN-EMPLOYEE-RDCTN-EARN", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Record_Type_40_Plan_Employee_Ddctn = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Employee_Ddctn", "PLAN-EMPLOYEE-DDCTN", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Record_Type_40_Plan_Employee_Ddctn_Earn = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Employee_Ddctn_Earn", "PLAN-EMPLOYEE-DDCTN-EARN", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Record_Type_40_Plan_Acc_123186 = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Acc_123186", "PLAN-ACC-123186", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Record_Type_40_Plan_Acc_123188 = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Acc_123188", "PLAN-ACC-123188", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Record_Type_40_Plan_Acc_123188_Rdctn = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Acc_123188_Rdctn", "PLAN-ACC-123188-RDCTN", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Record_Type_40_Plan_Acc_123188_Rdctn_Earn = pnd_Record_Type_40_Plan_Data.newFieldInGroup("pnd_Record_Type_40_Plan_Acc_123188_Rdctn_Earn", 
            "PLAN-ACC-123188-RDCTN-EARN", FieldType.PACKED_DECIMAL, 11,2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa378(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

