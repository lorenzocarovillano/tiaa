/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:28 PM
**        * FROM NATURAL PDA     : FCPA874C
************************************************************
**        * FILE NAME            : PdaFcpa874c.java
**        * CLASS NAME           : PdaFcpa874c
**        * INSTANCE NAME        : PdaFcpa874c
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa874c extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpa874c;
    private DbsField pnd_Fcpa874c_Cntrct_Orgn_Cde;
    private DbsField pnd_Fcpa874c_Cntrct_Hold_Cde;
    private DbsField pnd_Fcpa874c_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Fcpa874c_Pymnt_Check_Nbr;
    private DbsField pnd_Fcpa874c_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Fcpa874c_Pymnt_Check_Amt;
    private DbsField pnd_Fcpa874c_Pymnt_Check_Dte;

    public DbsGroup getPnd_Fcpa874c() { return pnd_Fcpa874c; }

    public DbsField getPnd_Fcpa874c_Cntrct_Orgn_Cde() { return pnd_Fcpa874c_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Fcpa874c_Cntrct_Hold_Cde() { return pnd_Fcpa874c_Cntrct_Hold_Cde; }

    public DbsField getPnd_Fcpa874c_Pymnt_Eft_Acct_Nbr() { return pnd_Fcpa874c_Pymnt_Eft_Acct_Nbr; }

    public DbsField getPnd_Fcpa874c_Pymnt_Check_Nbr() { return pnd_Fcpa874c_Pymnt_Check_Nbr; }

    public DbsField getPnd_Fcpa874c_Pymnt_Check_Seq_Nbr() { return pnd_Fcpa874c_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Fcpa874c_Pymnt_Check_Amt() { return pnd_Fcpa874c_Pymnt_Check_Amt; }

    public DbsField getPnd_Fcpa874c_Pymnt_Check_Dte() { return pnd_Fcpa874c_Pymnt_Check_Dte; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpa874c = dbsRecord.newGroupInRecord("pnd_Fcpa874c", "#FCPA874C");
        pnd_Fcpa874c.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpa874c_Cntrct_Orgn_Cde = pnd_Fcpa874c.newFieldInGroup("pnd_Fcpa874c_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Fcpa874c_Cntrct_Hold_Cde = pnd_Fcpa874c.newFieldInGroup("pnd_Fcpa874c_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        pnd_Fcpa874c_Pymnt_Eft_Acct_Nbr = pnd_Fcpa874c.newFieldInGroup("pnd_Fcpa874c_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        pnd_Fcpa874c_Pymnt_Check_Nbr = pnd_Fcpa874c.newFieldInGroup("pnd_Fcpa874c_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Fcpa874c_Pymnt_Check_Seq_Nbr = pnd_Fcpa874c.newFieldInGroup("pnd_Fcpa874c_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Fcpa874c_Pymnt_Check_Amt = pnd_Fcpa874c.newFieldInGroup("pnd_Fcpa874c_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Fcpa874c_Pymnt_Check_Dte = pnd_Fcpa874c.newFieldInGroup("pnd_Fcpa874c_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa874c(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

