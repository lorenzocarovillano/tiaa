/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:07 PM
**        * FROM NATURAL LDA     : FCPL233A
************************************************************
**        * FILE NAME            : LdaFcpl233a.java
**        * CLASS NAME           : LdaFcpl233a
**        * INSTANCE NAME        : LdaFcpl233a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl233a extends DbsRecord
{
    // Properties
    private DbsGroup fcpl233a;
    private DbsField fcpl233a_Cntrct_Orgn_Cde;
    private DbsField fcpl233a_Pnd_Srt_Company_Cde;
    private DbsField fcpl233a_Pnd_Srt_Csr_Code;
    private DbsField fcpl233a_Annt_Soc_Sec_Nbr;
    private DbsField fcpl233a_Inv_Acct_Cde;
    private DbsField fcpl233a_Annt_Soc_Sec_Ind;
    private DbsField fcpl233a_Cntrct_Ppcn_Nbr;
    private DbsField fcpl233a_Inv_Acct_Settl_Amt;
    private DbsField fcpl233a_Inv_Acct_Ivc_Amt;
    private DbsField fcpl233a_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField fcpl233a_Inv_Acct_State_Tax_Amt;
    private DbsField fcpl233a_Inv_Acct_Local_Tax_Amt;
    private DbsField fcpl233a_Inv_Acct_Dvdnd_Amt;
    private DbsField fcpl233a_Pymnt_Check_Dte;
    private DbsField fcpl233a_Pymnt_Check_Nbr;
    private DbsField fcpl233a_Pymnt_Check_Amt;
    private DbsField fcpl233a_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField fcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField fcpl233a_Cnr_Orgnl_Invrse_Dte;
    private DbsField fcpl233a_Pnd_Annot_Desc;
    private DbsField fcpl233a_Pnd_Participant_Name;
    private DbsField fcpl233a_Pnd_Current_Address;
    private DbsField fcpl233a_Pnd_Previous_Address;

    public DbsGroup getFcpl233a() { return fcpl233a; }

    public DbsField getFcpl233a_Cntrct_Orgn_Cde() { return fcpl233a_Cntrct_Orgn_Cde; }

    public DbsField getFcpl233a_Pnd_Srt_Company_Cde() { return fcpl233a_Pnd_Srt_Company_Cde; }

    public DbsField getFcpl233a_Pnd_Srt_Csr_Code() { return fcpl233a_Pnd_Srt_Csr_Code; }

    public DbsField getFcpl233a_Annt_Soc_Sec_Nbr() { return fcpl233a_Annt_Soc_Sec_Nbr; }

    public DbsField getFcpl233a_Inv_Acct_Cde() { return fcpl233a_Inv_Acct_Cde; }

    public DbsField getFcpl233a_Annt_Soc_Sec_Ind() { return fcpl233a_Annt_Soc_Sec_Ind; }

    public DbsField getFcpl233a_Cntrct_Ppcn_Nbr() { return fcpl233a_Cntrct_Ppcn_Nbr; }

    public DbsField getFcpl233a_Inv_Acct_Settl_Amt() { return fcpl233a_Inv_Acct_Settl_Amt; }

    public DbsField getFcpl233a_Inv_Acct_Ivc_Amt() { return fcpl233a_Inv_Acct_Ivc_Amt; }

    public DbsField getFcpl233a_Inv_Acct_Fdrl_Tax_Amt() { return fcpl233a_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getFcpl233a_Inv_Acct_State_Tax_Amt() { return fcpl233a_Inv_Acct_State_Tax_Amt; }

    public DbsField getFcpl233a_Inv_Acct_Local_Tax_Amt() { return fcpl233a_Inv_Acct_Local_Tax_Amt; }

    public DbsField getFcpl233a_Inv_Acct_Dvdnd_Amt() { return fcpl233a_Inv_Acct_Dvdnd_Amt; }

    public DbsField getFcpl233a_Pymnt_Check_Dte() { return fcpl233a_Pymnt_Check_Dte; }

    public DbsField getFcpl233a_Pymnt_Check_Nbr() { return fcpl233a_Pymnt_Check_Nbr; }

    public DbsField getFcpl233a_Pymnt_Check_Amt() { return fcpl233a_Pymnt_Check_Amt; }

    public DbsField getFcpl233a_Pymnt_Payee_Tx_Elct_Trggr() { return fcpl233a_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getFcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde() { return fcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getFcpl233a_Cnr_Orgnl_Invrse_Dte() { return fcpl233a_Cnr_Orgnl_Invrse_Dte; }

    public DbsField getFcpl233a_Pnd_Annot_Desc() { return fcpl233a_Pnd_Annot_Desc; }

    public DbsField getFcpl233a_Pnd_Participant_Name() { return fcpl233a_Pnd_Participant_Name; }

    public DbsField getFcpl233a_Pnd_Current_Address() { return fcpl233a_Pnd_Current_Address; }

    public DbsField getFcpl233a_Pnd_Previous_Address() { return fcpl233a_Pnd_Previous_Address; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        fcpl233a = newGroupInRecord("fcpl233a", "FCPL233A");
        fcpl233a_Cntrct_Orgn_Cde = fcpl233a.newFieldInGroup("fcpl233a_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        fcpl233a_Pnd_Srt_Company_Cde = fcpl233a.newFieldInGroup("fcpl233a_Pnd_Srt_Company_Cde", "#SRT-COMPANY-CDE", FieldType.STRING, 4);
        fcpl233a_Pnd_Srt_Csr_Code = fcpl233a.newFieldInGroup("fcpl233a_Pnd_Srt_Csr_Code", "#SRT-CSR-CODE", FieldType.STRING, 1);
        fcpl233a_Annt_Soc_Sec_Nbr = fcpl233a.newFieldInGroup("fcpl233a_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        fcpl233a_Inv_Acct_Cde = fcpl233a.newFieldInGroup("fcpl233a_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        fcpl233a_Annt_Soc_Sec_Ind = fcpl233a.newFieldInGroup("fcpl233a_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1);
        fcpl233a_Cntrct_Ppcn_Nbr = fcpl233a.newFieldInGroup("fcpl233a_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 14);
        fcpl233a_Inv_Acct_Settl_Amt = fcpl233a.newFieldInGroup("fcpl233a_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 15,2);
        fcpl233a_Inv_Acct_Ivc_Amt = fcpl233a.newFieldInGroup("fcpl233a_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 15,2);
        fcpl233a_Inv_Acct_Fdrl_Tax_Amt = fcpl233a.newFieldInGroup("fcpl233a_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            15,2);
        fcpl233a_Inv_Acct_State_Tax_Amt = fcpl233a.newFieldInGroup("fcpl233a_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            15,2);
        fcpl233a_Inv_Acct_Local_Tax_Amt = fcpl233a.newFieldInGroup("fcpl233a_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            15,2);
        fcpl233a_Inv_Acct_Dvdnd_Amt = fcpl233a.newFieldInGroup("fcpl233a_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 15,2);
        fcpl233a_Pymnt_Check_Dte = fcpl233a.newFieldInGroup("fcpl233a_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        fcpl233a_Pymnt_Check_Nbr = fcpl233a.newFieldInGroup("fcpl233a_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        fcpl233a_Pymnt_Check_Amt = fcpl233a.newFieldInGroup("fcpl233a_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9,2);
        fcpl233a_Pymnt_Payee_Tx_Elct_Trggr = fcpl233a.newFieldInGroup("fcpl233a_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 
            1);
        fcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde = fcpl233a.newFieldInGroup("fcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2);
        fcpl233a_Cnr_Orgnl_Invrse_Dte = fcpl233a.newFieldInGroup("fcpl233a_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        fcpl233a_Pnd_Annot_Desc = fcpl233a.newFieldInGroup("fcpl233a_Pnd_Annot_Desc", "#ANNOT-DESC", FieldType.STRING, 30);
        fcpl233a_Pnd_Participant_Name = fcpl233a.newFieldInGroup("fcpl233a_Pnd_Participant_Name", "#PARTICIPANT-NAME", FieldType.STRING, 50);
        fcpl233a_Pnd_Current_Address = fcpl233a.newFieldInGroup("fcpl233a_Pnd_Current_Address", "#CURRENT-ADDRESS", FieldType.STRING, 100);
        fcpl233a_Pnd_Previous_Address = fcpl233a.newFieldInGroup("fcpl233a_Pnd_Previous_Address", "#PREVIOUS-ADDRESS", FieldType.STRING, 100);

        this.setRecordName("LdaFcpl233a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl233a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
