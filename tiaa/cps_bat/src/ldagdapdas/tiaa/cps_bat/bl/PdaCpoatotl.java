/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:26 PM
**        * FROM NATURAL PDA     : CPOATOTL
************************************************************
**        * FILE NAME            : PdaCpoatotl.java
**        * CLASS NAME           : PdaCpoatotl
**        * INSTANCE NAME        : PdaCpoatotl
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCpoatotl extends PdaBase
{
    // Properties
    private DbsGroup cpoatotl;
    private DbsGroup cpoatotl_Origin_Totals;
    private DbsField cpoatotl_Pnd_Origin_Code;
    private DbsField cpoatotl_Pnd_Check_Cnt;
    private DbsField cpoatotl_Pnd_Check_Amt;
    private DbsField cpoatotl_Pnd_Eft_Cnt;
    private DbsField cpoatotl_Pnd_Eft_Amt;
    private DbsField cpoatotl_Pnd_Other_Cnt;
    private DbsField cpoatotl_Pnd_Other_Amt;
    private DbsField cpoatotl_Pnd_Total_Cnt;
    private DbsField cpoatotl_Pnd_Total_Amt;
    private DbsField cpoatotl_Pnd_Cs_Cnt;
    private DbsField cpoatotl_Pnd_Cs_Amt;
    private DbsField cpoatotl_Pnd_Redraw_Cnt;
    private DbsField cpoatotl_Pnd_Redraw_Amt;

    public DbsGroup getCpoatotl() { return cpoatotl; }

    public DbsGroup getCpoatotl_Origin_Totals() { return cpoatotl_Origin_Totals; }

    public DbsField getCpoatotl_Pnd_Origin_Code() { return cpoatotl_Pnd_Origin_Code; }

    public DbsField getCpoatotl_Pnd_Check_Cnt() { return cpoatotl_Pnd_Check_Cnt; }

    public DbsField getCpoatotl_Pnd_Check_Amt() { return cpoatotl_Pnd_Check_Amt; }

    public DbsField getCpoatotl_Pnd_Eft_Cnt() { return cpoatotl_Pnd_Eft_Cnt; }

    public DbsField getCpoatotl_Pnd_Eft_Amt() { return cpoatotl_Pnd_Eft_Amt; }

    public DbsField getCpoatotl_Pnd_Other_Cnt() { return cpoatotl_Pnd_Other_Cnt; }

    public DbsField getCpoatotl_Pnd_Other_Amt() { return cpoatotl_Pnd_Other_Amt; }

    public DbsField getCpoatotl_Pnd_Total_Cnt() { return cpoatotl_Pnd_Total_Cnt; }

    public DbsField getCpoatotl_Pnd_Total_Amt() { return cpoatotl_Pnd_Total_Amt; }

    public DbsField getCpoatotl_Pnd_Cs_Cnt() { return cpoatotl_Pnd_Cs_Cnt; }

    public DbsField getCpoatotl_Pnd_Cs_Amt() { return cpoatotl_Pnd_Cs_Amt; }

    public DbsField getCpoatotl_Pnd_Redraw_Cnt() { return cpoatotl_Pnd_Redraw_Cnt; }

    public DbsField getCpoatotl_Pnd_Redraw_Amt() { return cpoatotl_Pnd_Redraw_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cpoatotl = dbsRecord.newGroupInRecord("cpoatotl", "CPOATOTL");
        cpoatotl.setParameterOption(ParameterOption.ByReference);
        cpoatotl_Origin_Totals = cpoatotl.newGroupArrayInGroup("cpoatotl_Origin_Totals", "ORIGIN-TOTALS", new DbsArrayController(1,20));
        cpoatotl_Pnd_Origin_Code = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Origin_Code", "#ORIGIN-CODE", FieldType.STRING, 4);
        cpoatotl_Pnd_Check_Cnt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Check_Cnt", "#CHECK-CNT", FieldType.NUMERIC, 10);
        cpoatotl_Pnd_Check_Amt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Check_Amt", "#CHECK-AMT", FieldType.DECIMAL, 25,2);
        cpoatotl_Pnd_Eft_Cnt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Eft_Cnt", "#EFT-CNT", FieldType.NUMERIC, 10);
        cpoatotl_Pnd_Eft_Amt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Eft_Amt", "#EFT-AMT", FieldType.DECIMAL, 25,2);
        cpoatotl_Pnd_Other_Cnt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Other_Cnt", "#OTHER-CNT", FieldType.NUMERIC, 10);
        cpoatotl_Pnd_Other_Amt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Other_Amt", "#OTHER-AMT", FieldType.DECIMAL, 25,2);
        cpoatotl_Pnd_Total_Cnt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Total_Cnt", "#TOTAL-CNT", FieldType.NUMERIC, 10);
        cpoatotl_Pnd_Total_Amt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Total_Amt", "#TOTAL-AMT", FieldType.DECIMAL, 25,2);
        cpoatotl_Pnd_Cs_Cnt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Cs_Cnt", "#CS-CNT", FieldType.NUMERIC, 10);
        cpoatotl_Pnd_Cs_Amt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Cs_Amt", "#CS-AMT", FieldType.DECIMAL, 25,2);
        cpoatotl_Pnd_Redraw_Cnt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Redraw_Cnt", "#REDRAW-CNT", FieldType.NUMERIC, 10);
        cpoatotl_Pnd_Redraw_Amt = cpoatotl_Origin_Totals.newFieldInGroup("cpoatotl_Pnd_Redraw_Amt", "#REDRAW-AMT", FieldType.DECIMAL, 25,2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCpoatotl(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

