/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:29 PM
**        * FROM NATURAL PDA     : FCPAADDR
************************************************************
**        * FILE NAME            : PdaFcpaaddr.java
**        * CLASS NAME           : PdaFcpaaddr
**        * INSTANCE NAME        : PdaFcpaaddr
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpaaddr extends PdaBase
{
    // Properties
    private DbsGroup addr_Work_Fields;
    private DbsField addr_Work_Fields_Cntrct_Cmbn_Nbr;
    private DbsField addr_Work_Fields_Current_Addr;
    private DbsField addr_Work_Fields_Addr_Abend_Ind;
    private DbsField addr_Work_Fields_Addr_Return_Cde;
    private DbsField addr_Work_Fields_Addr_Isn;
    private DbsGroup addr;
    private DbsField addr_Rcrd_Typ;
    private DbsField addr_Cntrct_Orgn_Cde;
    private DbsField addr_Cntrct_Ppcn_Nbr;
    private DbsField addr_Cntrct_Payee_Cde;
    private DbsField addr_Cntrct_Invrse_Dte;
    private DbsField addr_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup addr_Ph_Name;
    private DbsField addr_Ph_Last_Name;
    private DbsField addr_Ph_First_Name;
    private DbsField addr_Ph_Middle_Name;
    private DbsField addr_C_Pymnt_Nme_And_Addr_Grp;
    private DbsGroup addr_Pymnt_Nme_And_Addr_Grp;
    private DbsField addr_Pymnt_Nme;
    private DbsField addr_Pymnt_Addr_Line1_Txt;
    private DbsField addr_Pymnt_Addr_Line2_Txt;
    private DbsField addr_Pymnt_Addr_Line3_Txt;
    private DbsField addr_Pymnt_Addr_Line4_Txt;
    private DbsField addr_Pymnt_Addr_Line5_Txt;
    private DbsField addr_Pymnt_Addr_Line6_Txt;
    private DbsField addr_Pymnt_Addr_Zip_Cde;
    private DbsField addr_Pymnt_Postl_Data;
    private DbsField addr_Pymnt_Addr_Type_Ind;
    private DbsField addr_Pymnt_Addr_Last_Chg_Dte;
    private DbsField addr_Pymnt_Addr_Last_Chg_Tme;
    private DbsField addr_Pymnt_Eft_Transit_Id;
    private DbsField addr_Pymnt_Eft_Acct_Nbr;
    private DbsField addr_Pymnt_Chk_Sav_Ind;
    private DbsField addr_Pymnt_Deceased_Nme;
    private DbsField addr_Cntrct_Hold_Tme;
    private DbsField addr_Pymnt_Dob;
    private DbsField addr_Pymnt_Death_Dte;
    private DbsField addr_Pymnt_Proof_Dte;
    private DbsField addr_C_Pymnt_Addr_Chg_Grp;
    private DbsGroup addr_Pymnt_Addr_Chg_Grp;
    private DbsField addr_Pymnt_Addr_Chg_Ind;
    private DbsField addr_Ph_Sex;
    private DbsField addr_Pymnt_Alt_Addr_Ind;

    public DbsGroup getAddr_Work_Fields() { return addr_Work_Fields; }

    public DbsField getAddr_Work_Fields_Cntrct_Cmbn_Nbr() { return addr_Work_Fields_Cntrct_Cmbn_Nbr; }

    public DbsField getAddr_Work_Fields_Current_Addr() { return addr_Work_Fields_Current_Addr; }

    public DbsField getAddr_Work_Fields_Addr_Abend_Ind() { return addr_Work_Fields_Addr_Abend_Ind; }

    public DbsField getAddr_Work_Fields_Addr_Return_Cde() { return addr_Work_Fields_Addr_Return_Cde; }

    public DbsField getAddr_Work_Fields_Addr_Isn() { return addr_Work_Fields_Addr_Isn; }

    public DbsGroup getAddr() { return addr; }

    public DbsField getAddr_Rcrd_Typ() { return addr_Rcrd_Typ; }

    public DbsField getAddr_Cntrct_Orgn_Cde() { return addr_Cntrct_Orgn_Cde; }

    public DbsField getAddr_Cntrct_Ppcn_Nbr() { return addr_Cntrct_Ppcn_Nbr; }

    public DbsField getAddr_Cntrct_Payee_Cde() { return addr_Cntrct_Payee_Cde; }

    public DbsField getAddr_Cntrct_Invrse_Dte() { return addr_Cntrct_Invrse_Dte; }

    public DbsField getAddr_Pymnt_Prcss_Seq_Nbr() { return addr_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getAddr_Ph_Name() { return addr_Ph_Name; }

    public DbsField getAddr_Ph_Last_Name() { return addr_Ph_Last_Name; }

    public DbsField getAddr_Ph_First_Name() { return addr_Ph_First_Name; }

    public DbsField getAddr_Ph_Middle_Name() { return addr_Ph_Middle_Name; }

    public DbsField getAddr_C_Pymnt_Nme_And_Addr_Grp() { return addr_C_Pymnt_Nme_And_Addr_Grp; }

    public DbsGroup getAddr_Pymnt_Nme_And_Addr_Grp() { return addr_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getAddr_Pymnt_Nme() { return addr_Pymnt_Nme; }

    public DbsField getAddr_Pymnt_Addr_Line1_Txt() { return addr_Pymnt_Addr_Line1_Txt; }

    public DbsField getAddr_Pymnt_Addr_Line2_Txt() { return addr_Pymnt_Addr_Line2_Txt; }

    public DbsField getAddr_Pymnt_Addr_Line3_Txt() { return addr_Pymnt_Addr_Line3_Txt; }

    public DbsField getAddr_Pymnt_Addr_Line4_Txt() { return addr_Pymnt_Addr_Line4_Txt; }

    public DbsField getAddr_Pymnt_Addr_Line5_Txt() { return addr_Pymnt_Addr_Line5_Txt; }

    public DbsField getAddr_Pymnt_Addr_Line6_Txt() { return addr_Pymnt_Addr_Line6_Txt; }

    public DbsField getAddr_Pymnt_Addr_Zip_Cde() { return addr_Pymnt_Addr_Zip_Cde; }

    public DbsField getAddr_Pymnt_Postl_Data() { return addr_Pymnt_Postl_Data; }

    public DbsField getAddr_Pymnt_Addr_Type_Ind() { return addr_Pymnt_Addr_Type_Ind; }

    public DbsField getAddr_Pymnt_Addr_Last_Chg_Dte() { return addr_Pymnt_Addr_Last_Chg_Dte; }

    public DbsField getAddr_Pymnt_Addr_Last_Chg_Tme() { return addr_Pymnt_Addr_Last_Chg_Tme; }

    public DbsField getAddr_Pymnt_Eft_Transit_Id() { return addr_Pymnt_Eft_Transit_Id; }

    public DbsField getAddr_Pymnt_Eft_Acct_Nbr() { return addr_Pymnt_Eft_Acct_Nbr; }

    public DbsField getAddr_Pymnt_Chk_Sav_Ind() { return addr_Pymnt_Chk_Sav_Ind; }

    public DbsField getAddr_Pymnt_Deceased_Nme() { return addr_Pymnt_Deceased_Nme; }

    public DbsField getAddr_Cntrct_Hold_Tme() { return addr_Cntrct_Hold_Tme; }

    public DbsField getAddr_Pymnt_Dob() { return addr_Pymnt_Dob; }

    public DbsField getAddr_Pymnt_Death_Dte() { return addr_Pymnt_Death_Dte; }

    public DbsField getAddr_Pymnt_Proof_Dte() { return addr_Pymnt_Proof_Dte; }

    public DbsField getAddr_C_Pymnt_Addr_Chg_Grp() { return addr_C_Pymnt_Addr_Chg_Grp; }

    public DbsGroup getAddr_Pymnt_Addr_Chg_Grp() { return addr_Pymnt_Addr_Chg_Grp; }

    public DbsField getAddr_Pymnt_Addr_Chg_Ind() { return addr_Pymnt_Addr_Chg_Ind; }

    public DbsField getAddr_Ph_Sex() { return addr_Ph_Sex; }

    public DbsField getAddr_Pymnt_Alt_Addr_Ind() { return addr_Pymnt_Alt_Addr_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        addr_Work_Fields = dbsRecord.newGroupInRecord("addr_Work_Fields", "ADDR-WORK-FIELDS");
        addr_Work_Fields.setParameterOption(ParameterOption.ByReference);
        addr_Work_Fields_Cntrct_Cmbn_Nbr = addr_Work_Fields.newFieldInGroup("addr_Work_Fields_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        addr_Work_Fields_Current_Addr = addr_Work_Fields.newFieldInGroup("addr_Work_Fields_Current_Addr", "CURRENT-ADDR", FieldType.BOOLEAN);
        addr_Work_Fields_Addr_Abend_Ind = addr_Work_Fields.newFieldInGroup("addr_Work_Fields_Addr_Abend_Ind", "ADDR-ABEND-IND", FieldType.BOOLEAN);
        addr_Work_Fields_Addr_Return_Cde = addr_Work_Fields.newFieldInGroup("addr_Work_Fields_Addr_Return_Cde", "ADDR-RETURN-CDE", FieldType.NUMERIC, 
            2);
        addr_Work_Fields_Addr_Isn = addr_Work_Fields.newFieldInGroup("addr_Work_Fields_Addr_Isn", "ADDR-ISN", FieldType.PACKED_DECIMAL, 11);

        addr = dbsRecord.newGroupInRecord("addr", "ADDR");
        addr.setParameterOption(ParameterOption.ByReference);
        addr_Rcrd_Typ = addr.newFieldInGroup("addr_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 1);
        addr_Cntrct_Orgn_Cde = addr.newFieldInGroup("addr_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        addr_Cntrct_Ppcn_Nbr = addr.newFieldInGroup("addr_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        addr_Cntrct_Payee_Cde = addr.newFieldInGroup("addr_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        addr_Cntrct_Invrse_Dte = addr.newFieldInGroup("addr_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        addr_Pymnt_Prcss_Seq_Nbr = addr.newFieldInGroup("addr_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 7);
        addr_Ph_Name = addr.newGroupInGroup("addr_Ph_Name", "PH-NAME");
        addr_Ph_Last_Name = addr_Ph_Name.newFieldInGroup("addr_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        addr_Ph_First_Name = addr_Ph_Name.newFieldInGroup("addr_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        addr_Ph_Middle_Name = addr_Ph_Name.newFieldInGroup("addr_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        addr_C_Pymnt_Nme_And_Addr_Grp = addr.newFieldInGroup("addr_C_Pymnt_Nme_And_Addr_Grp", "C-PYMNT-NME-AND-ADDR-GRP", FieldType.PACKED_DECIMAL, 3);
        addr_Pymnt_Nme_And_Addr_Grp = addr.newGroupArrayInGroup("addr_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", new DbsArrayController(1,2));
        addr_Pymnt_Nme = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38);
        addr_Pymnt_Addr_Line1_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 
            35);
        addr_Pymnt_Addr_Line2_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 
            35);
        addr_Pymnt_Addr_Line3_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 
            35);
        addr_Pymnt_Addr_Line4_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 
            35);
        addr_Pymnt_Addr_Line5_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 
            35);
        addr_Pymnt_Addr_Line6_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 
            35);
        addr_Pymnt_Addr_Zip_Cde = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 9);
        addr_Pymnt_Postl_Data = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 32);
        addr_Pymnt_Addr_Type_Ind = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", FieldType.STRING, 1);
        addr_Pymnt_Addr_Last_Chg_Dte = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Addr_Last_Chg_Dte", "PYMNT-ADDR-LAST-CHG-DTE", FieldType.NUMERIC, 
            8);
        addr_Pymnt_Addr_Last_Chg_Tme = addr_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("addr_Pymnt_Addr_Last_Chg_Tme", "PYMNT-ADDR-LAST-CHG-TME", FieldType.NUMERIC, 
            7);
        addr_Pymnt_Eft_Transit_Id = addr.newFieldInGroup("addr_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9);
        addr_Pymnt_Eft_Acct_Nbr = addr.newFieldInGroup("addr_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        addr_Pymnt_Chk_Sav_Ind = addr.newFieldInGroup("addr_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 1);
        addr_Pymnt_Deceased_Nme = addr.newFieldInGroup("addr_Pymnt_Deceased_Nme", "PYMNT-DECEASED-NME", FieldType.STRING, 38);
        addr_Cntrct_Hold_Tme = addr.newFieldInGroup("addr_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME);
        addr_Pymnt_Dob = addr.newFieldInGroup("addr_Pymnt_Dob", "PYMNT-DOB", FieldType.DATE);
        addr_Pymnt_Death_Dte = addr.newFieldInGroup("addr_Pymnt_Death_Dte", "PYMNT-DEATH-DTE", FieldType.DATE);
        addr_Pymnt_Proof_Dte = addr.newFieldInGroup("addr_Pymnt_Proof_Dte", "PYMNT-PROOF-DTE", FieldType.DATE);
        addr_C_Pymnt_Addr_Chg_Grp = addr.newFieldInGroup("addr_C_Pymnt_Addr_Chg_Grp", "C-PYMNT-ADDR-CHG-GRP", FieldType.PACKED_DECIMAL, 3);
        addr_Pymnt_Addr_Chg_Grp = addr.newGroupInGroup("addr_Pymnt_Addr_Chg_Grp", "PYMNT-ADDR-CHG-GRP");
        addr_Pymnt_Addr_Chg_Ind = addr_Pymnt_Addr_Chg_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Chg_Ind", "PYMNT-ADDR-CHG-IND", FieldType.STRING, 1, new 
            DbsArrayController(1,2));
        addr_Ph_Sex = addr.newFieldInGroup("addr_Ph_Sex", "PH-SEX", FieldType.STRING, 1);
        addr_Pymnt_Alt_Addr_Ind = addr.newFieldInGroup("addr_Pymnt_Alt_Addr_Ind", "PYMNT-ALT-ADDR-IND", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpaaddr(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

