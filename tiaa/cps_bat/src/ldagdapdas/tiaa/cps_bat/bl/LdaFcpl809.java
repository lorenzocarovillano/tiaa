/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:16 PM
**        * FROM NATURAL LDA     : FCPL809
************************************************************
**        * FILE NAME            : LdaFcpl809.java
**        * CLASS NAME           : LdaFcpl809
**        * INSTANCE NAME        : LdaFcpl809
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl809 extends DbsRecord
{
    // Properties
    private DbsField pnd_Ltc_Ded;
    private DbsGroup pnd_Ltc_DedRedef1;
    private DbsField pnd_Ltc_Ded_Pnd_Pymnt_Check_Dte;
    private DbsField pnd_Ltc_Ded_Pnd_Pymnt_Name_A9;
    private DbsField pnd_Ltc_Ded_Pnd_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Ltc_Ded_Pnd_Pymnt_Ded_Seq_Nbr;
    private DbsField pnd_Ltc_Ded_Pnd_Cntrct_Ppcn_Nbr_A8;
    private DbsField pnd_Ltc_Ded_Pnd_Cntrct_Payee_Cde_A2;
    private DbsField pnd_Ltc_Ded_Pnd_Pymnt_Ded_Amt;

    public DbsField getPnd_Ltc_Ded() { return pnd_Ltc_Ded; }

    public DbsGroup getPnd_Ltc_DedRedef1() { return pnd_Ltc_DedRedef1; }

    public DbsField getPnd_Ltc_Ded_Pnd_Pymnt_Check_Dte() { return pnd_Ltc_Ded_Pnd_Pymnt_Check_Dte; }

    public DbsField getPnd_Ltc_Ded_Pnd_Pymnt_Name_A9() { return pnd_Ltc_Ded_Pnd_Pymnt_Name_A9; }

    public DbsField getPnd_Ltc_Ded_Pnd_Annt_Soc_Sec_Nbr() { return pnd_Ltc_Ded_Pnd_Annt_Soc_Sec_Nbr; }

    public DbsField getPnd_Ltc_Ded_Pnd_Pymnt_Ded_Seq_Nbr() { return pnd_Ltc_Ded_Pnd_Pymnt_Ded_Seq_Nbr; }

    public DbsField getPnd_Ltc_Ded_Pnd_Cntrct_Ppcn_Nbr_A8() { return pnd_Ltc_Ded_Pnd_Cntrct_Ppcn_Nbr_A8; }

    public DbsField getPnd_Ltc_Ded_Pnd_Cntrct_Payee_Cde_A2() { return pnd_Ltc_Ded_Pnd_Cntrct_Payee_Cde_A2; }

    public DbsField getPnd_Ltc_Ded_Pnd_Pymnt_Ded_Amt() { return pnd_Ltc_Ded_Pnd_Pymnt_Ded_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Ltc_Ded = newFieldInRecord("pnd_Ltc_Ded", "#LTC-DED", FieldType.STRING, 48);
        pnd_Ltc_DedRedef1 = newGroupInRecord("pnd_Ltc_DedRedef1", "Redefines", pnd_Ltc_Ded);
        pnd_Ltc_Ded_Pnd_Pymnt_Check_Dte = pnd_Ltc_DedRedef1.newFieldInGroup("pnd_Ltc_Ded_Pnd_Pymnt_Check_Dte", "#PYMNT-CHECK-DTE", FieldType.STRING, 8);
        pnd_Ltc_Ded_Pnd_Pymnt_Name_A9 = pnd_Ltc_DedRedef1.newFieldInGroup("pnd_Ltc_Ded_Pnd_Pymnt_Name_A9", "#PYMNT-NAME-A9", FieldType.STRING, 9);
        pnd_Ltc_Ded_Pnd_Annt_Soc_Sec_Nbr = pnd_Ltc_DedRedef1.newFieldInGroup("pnd_Ltc_Ded_Pnd_Annt_Soc_Sec_Nbr", "#ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Ltc_Ded_Pnd_Pymnt_Ded_Seq_Nbr = pnd_Ltc_DedRedef1.newFieldInGroup("pnd_Ltc_Ded_Pnd_Pymnt_Ded_Seq_Nbr", "#PYMNT-DED-SEQ-NBR", FieldType.STRING, 
            3);
        pnd_Ltc_Ded_Pnd_Cntrct_Ppcn_Nbr_A8 = pnd_Ltc_DedRedef1.newFieldInGroup("pnd_Ltc_Ded_Pnd_Cntrct_Ppcn_Nbr_A8", "#CNTRCT-PPCN-NBR-A8", FieldType.STRING, 
            8);
        pnd_Ltc_Ded_Pnd_Cntrct_Payee_Cde_A2 = pnd_Ltc_DedRedef1.newFieldInGroup("pnd_Ltc_Ded_Pnd_Cntrct_Payee_Cde_A2", "#CNTRCT-PAYEE-CDE-A2", FieldType.STRING, 
            2);
        pnd_Ltc_Ded_Pnd_Pymnt_Ded_Amt = pnd_Ltc_DedRedef1.newFieldInGroup("pnd_Ltc_Ded_Pnd_Pymnt_Ded_Amt", "#PYMNT-DED-AMT", FieldType.DECIMAL, 9,2);

        this.setRecordName("LdaFcpl809");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl809() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
