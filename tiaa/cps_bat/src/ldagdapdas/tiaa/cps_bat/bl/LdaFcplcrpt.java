/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:32 PM
**        * FROM NATURAL LDA     : FCPLCRPT
************************************************************
**        * FILE NAME            : LdaFcplcrpt.java
**        * CLASS NAME           : LdaFcplcrpt
**        * INSTANCE NAME        : LdaFcplcrpt
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplcrpt extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcplcrpt;
    private DbsField pnd_Fcplcrpt_Pnd_Max_Occur;
    private DbsField pnd_Fcplcrpt_Pnd_Cntl_Type;
    private DbsField pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc;

    public DbsGroup getPnd_Fcplcrpt() { return pnd_Fcplcrpt; }

    public DbsField getPnd_Fcplcrpt_Pnd_Max_Occur() { return pnd_Fcplcrpt_Pnd_Max_Occur; }

    public DbsField getPnd_Fcplcrpt_Pnd_Cntl_Type() { return pnd_Fcplcrpt_Pnd_Cntl_Type; }

    public DbsField getPnd_Fcplcrpt_Pnd_Pymnt_Type_Desc() { return pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcplcrpt = newGroupInRecord("pnd_Fcplcrpt", "#FCPLCRPT");
        pnd_Fcplcrpt_Pnd_Max_Occur = pnd_Fcplcrpt.newFieldInGroup("pnd_Fcplcrpt_Pnd_Max_Occur", "#MAX-OCCUR", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcplcrpt_Pnd_Cntl_Type = pnd_Fcplcrpt.newFieldArrayInGroup("pnd_Fcplcrpt_Pnd_Cntl_Type", "#CNTL-TYPE", FieldType.STRING, 41, new DbsArrayController(1,
            3));
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc = pnd_Fcplcrpt.newFieldArrayInGroup("pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc", "#PYMNT-TYPE-DESC", FieldType.STRING, 
            25, new DbsArrayController(1,50));

        this.setRecordName("LdaFcplcrpt");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcplcrpt_Pnd_Max_Occur.setInitialValue(23);
        pnd_Fcplcrpt_Pnd_Cntl_Type.getValue(1).setInitialValue("I n p u t   F i l e   T o t a l s");
        pnd_Fcplcrpt_Pnd_Cntl_Type.getValue(2).setInitialValue("C o n t r o l   R e c o r d   T o t a l s");
        pnd_Fcplcrpt_Pnd_Cntl_Type.getValue(3).setInitialValue("D i f f e r e n c e");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(1).setInitialValue("Checks");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(2).setInitialValue("Efts");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(3).setInitialValue("Global Pay");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(4).setInitialValue("Internal Rollover");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(5).setInitialValue("Dividend Pymnt to College");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(6).setInitialValue("Taxes not witheld for GTN");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(7).setInitialValue("Vol. Ded not taken in GTN");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(8).setInitialValue("Blocked Account");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(9).setInitialValue("Unclaimed Sums");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(10).setInitialValue("Missing Name/Address");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(11).setInitialValue("Invalid Combines");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(12).setInitialValue("Miscellaneous Pends");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(13).setInitialValue("Error File (Non-ledgers)");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(14).setInitialValue("PIN number missing");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(15).setInitialValue("Zero Checks");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(16).setInitialValue("Off-Mode Contracts");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(17).setInitialValue("Zero EFTs turned to Check");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(18).setInitialValue("EFTs turned to Checks");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(19).setInitialValue("Total Cancels");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(20).setInitialValue("Total Stops");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(21).setInitialValue("Total Redraws");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(22).setInitialValue("Zero GLPs turned to Check");
        pnd_Fcplcrpt_Pnd_Pymnt_Type_Desc.getValue(23).setInitialValue("Mutual Funds");
    }

    // Constructor
    public LdaFcplcrpt() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
