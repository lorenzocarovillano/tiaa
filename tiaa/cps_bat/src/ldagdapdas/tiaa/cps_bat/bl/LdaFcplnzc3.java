/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:34 PM
**        * FROM NATURAL LDA     : FCPLNZC3
************************************************************
**        * FILE NAME            : LdaFcplnzc3.java
**        * CLASS NAME           : LdaFcplnzc3
**        * INSTANCE NAME        : LdaFcplnzc3
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplnzc3 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcplnzc3;
    private DbsField pnd_Fcplnzc3_Pnd_Cntl_Type;
    private DbsField pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc;

    public DbsGroup getPnd_Fcplnzc3() { return pnd_Fcplnzc3; }

    public DbsField getPnd_Fcplnzc3_Pnd_Cntl_Type() { return pnd_Fcplnzc3_Pnd_Cntl_Type; }

    public DbsField getPnd_Fcplnzc3_Pnd_Pymnt_Type_Desc() { return pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcplnzc3 = newGroupInRecord("pnd_Fcplnzc3", "#FCPLNZC3");
        pnd_Fcplnzc3_Pnd_Cntl_Type = pnd_Fcplnzc3.newFieldArrayInGroup("pnd_Fcplnzc3_Pnd_Cntl_Type", "#CNTL-TYPE", FieldType.STRING, 41, new DbsArrayController(1,
            3));
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc = pnd_Fcplnzc3.newFieldArrayInGroup("pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc", "#PYMNT-TYPE-DESC", FieldType.STRING, 
            25, new DbsArrayController(1,50));

        this.setRecordName("LdaFcplnzc3");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcplnzc3_Pnd_Cntl_Type.getValue(1).setInitialValue("I n p u t   F i l e   T o t a l s");
        pnd_Fcplnzc3_Pnd_Cntl_Type.getValue(2).setInitialValue("C o n t r o l   R e c o r d   T o t a l s");
        pnd_Fcplnzc3_Pnd_Cntl_Type.getValue(3).setInitialValue("D i f f e r e n c e");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(1).setInitialValue("RTB/Retro/Periodic");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(2).setInitialValue("RTB/AC<10yr ext. transfer");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(3).setInitialValue("RTB/AC<10yr int. transfer");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(17).setInitialValue("RTB/Retro/Periodic redraw");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(18).setInitialValue("RTB/AC<10yr ext.tr redraw");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(19).setInitialValue("RTB/AC<10yr int.tr redraw");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(44).setInitialValue("Total Cancels");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(45).setInitialValue("Total Stops");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(47).setInitialValue("Total Zero Checks");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(48).setInitialValue("Total Checks");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(49).setInitialValue("Total EFTs");
        pnd_Fcplnzc3_Pnd_Pymnt_Type_Desc.getValue(50).setInitialValue("Total Global Pay");
    }

    // Constructor
    public LdaFcplnzc3() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
