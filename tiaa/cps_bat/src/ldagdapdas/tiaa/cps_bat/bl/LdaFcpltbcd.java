/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:00:08 PM
**        * FROM NATURAL LDA     : FCPLTBCD
************************************************************
**        * FILE NAME            : LdaFcpltbcd.java
**        * CLASS NAME           : LdaFcpltbcd
**        * INSTANCE NAME        : LdaFcpltbcd
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpltbcd extends DbsRecord
{
    // Properties
    private DbsGroup tbldcoda_Msg;
    private DbsField tbldcoda_Msg_Pnd_Pnd_Msg;
    private DbsField tbldcoda_Msg_Pnd_Pnd_Msg_Nr;
    private DbsField tbldcoda_Msg_Pnd_Pnd_Msg_Data;
    private DbsField tbldcoda_Msg_Pnd_Pnd_Return_Code;
    private DbsField tbldcoda_Msg_Pnd_Pnd_Error_Field;
    private DbsField tbldcoda_Msg_Pnd_Pnd_Error_Field_Index1;
    private DbsField tbldcoda_Msg_Pnd_Pnd_Error_Field_Index2;
    private DbsField tbldcoda_Msg_Pnd_Pnd_Error_Field_Index3;

    public DbsGroup getTbldcoda_Msg() { return tbldcoda_Msg; }

    public DbsField getTbldcoda_Msg_Pnd_Pnd_Msg() { return tbldcoda_Msg_Pnd_Pnd_Msg; }

    public DbsField getTbldcoda_Msg_Pnd_Pnd_Msg_Nr() { return tbldcoda_Msg_Pnd_Pnd_Msg_Nr; }

    public DbsField getTbldcoda_Msg_Pnd_Pnd_Msg_Data() { return tbldcoda_Msg_Pnd_Pnd_Msg_Data; }

    public DbsField getTbldcoda_Msg_Pnd_Pnd_Return_Code() { return tbldcoda_Msg_Pnd_Pnd_Return_Code; }

    public DbsField getTbldcoda_Msg_Pnd_Pnd_Error_Field() { return tbldcoda_Msg_Pnd_Pnd_Error_Field; }

    public DbsField getTbldcoda_Msg_Pnd_Pnd_Error_Field_Index1() { return tbldcoda_Msg_Pnd_Pnd_Error_Field_Index1; }

    public DbsField getTbldcoda_Msg_Pnd_Pnd_Error_Field_Index2() { return tbldcoda_Msg_Pnd_Pnd_Error_Field_Index2; }

    public DbsField getTbldcoda_Msg_Pnd_Pnd_Error_Field_Index3() { return tbldcoda_Msg_Pnd_Pnd_Error_Field_Index3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        tbldcoda_Msg = newGroupInRecord("tbldcoda_Msg", "TBLDCODA-MSG");
        tbldcoda_Msg_Pnd_Pnd_Msg = tbldcoda_Msg.newFieldInGroup("tbldcoda_Msg_Pnd_Pnd_Msg", "##MSG", FieldType.STRING, 79);
        tbldcoda_Msg_Pnd_Pnd_Msg_Nr = tbldcoda_Msg.newFieldInGroup("tbldcoda_Msg_Pnd_Pnd_Msg_Nr", "##MSG-NR", FieldType.NUMERIC, 4);
        tbldcoda_Msg_Pnd_Pnd_Msg_Data = tbldcoda_Msg.newFieldArrayInGroup("tbldcoda_Msg_Pnd_Pnd_Msg_Data", "##MSG-DATA", FieldType.STRING, 32, new DbsArrayController(1,
            3));
        tbldcoda_Msg_Pnd_Pnd_Return_Code = tbldcoda_Msg.newFieldInGroup("tbldcoda_Msg_Pnd_Pnd_Return_Code", "##RETURN-CODE", FieldType.STRING, 1);
        tbldcoda_Msg_Pnd_Pnd_Error_Field = tbldcoda_Msg.newFieldInGroup("tbldcoda_Msg_Pnd_Pnd_Error_Field", "##ERROR-FIELD", FieldType.STRING, 32);
        tbldcoda_Msg_Pnd_Pnd_Error_Field_Index1 = tbldcoda_Msg.newFieldInGroup("tbldcoda_Msg_Pnd_Pnd_Error_Field_Index1", "##ERROR-FIELD-INDEX1", FieldType.PACKED_DECIMAL, 
            3);
        tbldcoda_Msg_Pnd_Pnd_Error_Field_Index2 = tbldcoda_Msg.newFieldInGroup("tbldcoda_Msg_Pnd_Pnd_Error_Field_Index2", "##ERROR-FIELD-INDEX2", FieldType.PACKED_DECIMAL, 
            3);
        tbldcoda_Msg_Pnd_Pnd_Error_Field_Index3 = tbldcoda_Msg.newFieldInGroup("tbldcoda_Msg_Pnd_Pnd_Error_Field_Index3", "##ERROR-FIELD-INDEX3", FieldType.PACKED_DECIMAL, 
            3);

        this.setRecordName("LdaFcpltbcd");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpltbcd() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
