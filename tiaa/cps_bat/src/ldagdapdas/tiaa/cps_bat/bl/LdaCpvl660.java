/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:42 PM
**        * FROM NATURAL LDA     : CPVL660
************************************************************
**        * FILE NAME            : LdaCpvl660.java
**        * CLASS NAME           : LdaCpvl660
**        * INSTANCE NAME        : LdaCpvl660
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpvl660 extends DbsRecord
{
    // Properties
    private DbsGroup gw_Ledger;
    private DbsField gw_Ledger_Record_Type;
    private DbsField gw_Ledger_Oprid;
    private DbsField gw_Ledger_Pnd_Company;
    private DbsField gw_Ledger_Subsystem;
    private DbsField gw_Ledger_Batch_No;
    private DbsField gw_Ledger_Filler1;
    private DbsField gw_Ledger_Id_1;
    private DbsField gw_Ledger_Seq_N;
    private DbsField gw_Ledger_Pnd_Journal_Date;
    private DbsField gw_Ledger_Description;
    private DbsField gw_Ledger_Pnd_Amount;
    private DbsField gw_Ledger_Pnd_Dccr_Ind;
    private DbsField gw_Ledger_Filler2;
    private DbsField gw_Ledger_Pnd_Gl_Company;
    private DbsField gw_Ledger_Pnd_Gl_Account;
    private DbsField gw_Ledger_Pnd_Post_Date;
    private DbsField gw_Ledger_Pnd_Policy_Number;
    private DbsField gw_Ledger_Check_Number;
    private DbsField gw_Ledger_Pnd_Eff_Date;
    private DbsGroup gw_Ledger_Pnd_Eff_DateRedef1;
    private DbsField gw_Ledger_Pnd_Eff_Mm;
    private DbsField gw_Ledger_Pnd_Eff_Dd;
    private DbsField gw_Ledger_Pnd_Eff_Yr;
    private DbsField gw_Ledger_Constant;
    private DbsField gw_Ledger_Pnd_Residency;
    private DbsField gw_Ledger_Pnd_Ia_Orgn_Cde;
    private DbsField gw_Ledger_Pnd_Option_Cde;
    private DbsField gw_Ledger_Filler3;

    public DbsGroup getGw_Ledger() { return gw_Ledger; }

    public DbsField getGw_Ledger_Record_Type() { return gw_Ledger_Record_Type; }

    public DbsField getGw_Ledger_Oprid() { return gw_Ledger_Oprid; }

    public DbsField getGw_Ledger_Pnd_Company() { return gw_Ledger_Pnd_Company; }

    public DbsField getGw_Ledger_Subsystem() { return gw_Ledger_Subsystem; }

    public DbsField getGw_Ledger_Batch_No() { return gw_Ledger_Batch_No; }

    public DbsField getGw_Ledger_Filler1() { return gw_Ledger_Filler1; }

    public DbsField getGw_Ledger_Id_1() { return gw_Ledger_Id_1; }

    public DbsField getGw_Ledger_Seq_N() { return gw_Ledger_Seq_N; }

    public DbsField getGw_Ledger_Pnd_Journal_Date() { return gw_Ledger_Pnd_Journal_Date; }

    public DbsField getGw_Ledger_Description() { return gw_Ledger_Description; }

    public DbsField getGw_Ledger_Pnd_Amount() { return gw_Ledger_Pnd_Amount; }

    public DbsField getGw_Ledger_Pnd_Dccr_Ind() { return gw_Ledger_Pnd_Dccr_Ind; }

    public DbsField getGw_Ledger_Filler2() { return gw_Ledger_Filler2; }

    public DbsField getGw_Ledger_Pnd_Gl_Company() { return gw_Ledger_Pnd_Gl_Company; }

    public DbsField getGw_Ledger_Pnd_Gl_Account() { return gw_Ledger_Pnd_Gl_Account; }

    public DbsField getGw_Ledger_Pnd_Post_Date() { return gw_Ledger_Pnd_Post_Date; }

    public DbsField getGw_Ledger_Pnd_Policy_Number() { return gw_Ledger_Pnd_Policy_Number; }

    public DbsField getGw_Ledger_Check_Number() { return gw_Ledger_Check_Number; }

    public DbsField getGw_Ledger_Pnd_Eff_Date() { return gw_Ledger_Pnd_Eff_Date; }

    public DbsGroup getGw_Ledger_Pnd_Eff_DateRedef1() { return gw_Ledger_Pnd_Eff_DateRedef1; }

    public DbsField getGw_Ledger_Pnd_Eff_Mm() { return gw_Ledger_Pnd_Eff_Mm; }

    public DbsField getGw_Ledger_Pnd_Eff_Dd() { return gw_Ledger_Pnd_Eff_Dd; }

    public DbsField getGw_Ledger_Pnd_Eff_Yr() { return gw_Ledger_Pnd_Eff_Yr; }

    public DbsField getGw_Ledger_Constant() { return gw_Ledger_Constant; }

    public DbsField getGw_Ledger_Pnd_Residency() { return gw_Ledger_Pnd_Residency; }

    public DbsField getGw_Ledger_Pnd_Ia_Orgn_Cde() { return gw_Ledger_Pnd_Ia_Orgn_Cde; }

    public DbsField getGw_Ledger_Pnd_Option_Cde() { return gw_Ledger_Pnd_Option_Cde; }

    public DbsField getGw_Ledger_Filler3() { return gw_Ledger_Filler3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        gw_Ledger = newGroupInRecord("gw_Ledger", "GW-LEDGER");
        gw_Ledger_Record_Type = gw_Ledger.newFieldInGroup("gw_Ledger_Record_Type", "RECORD-TYPE", FieldType.STRING, 1);
        gw_Ledger_Oprid = gw_Ledger.newFieldInGroup("gw_Ledger_Oprid", "OPRID", FieldType.STRING, 8);
        gw_Ledger_Pnd_Company = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Company", "#COMPANY", FieldType.STRING, 5);
        gw_Ledger_Subsystem = gw_Ledger.newFieldInGroup("gw_Ledger_Subsystem", "SUBSYSTEM", FieldType.STRING, 3);
        gw_Ledger_Batch_No = gw_Ledger.newFieldInGroup("gw_Ledger_Batch_No", "BATCH-NO", FieldType.STRING, 3);
        gw_Ledger_Filler1 = gw_Ledger.newFieldInGroup("gw_Ledger_Filler1", "FILLER1", FieldType.STRING, 6);
        gw_Ledger_Id_1 = gw_Ledger.newFieldInGroup("gw_Ledger_Id_1", "ID-1", FieldType.STRING, 1);
        gw_Ledger_Seq_N = gw_Ledger.newFieldInGroup("gw_Ledger_Seq_N", "SEQ-N", FieldType.STRING, 5);
        gw_Ledger_Pnd_Journal_Date = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Journal_Date", "#JOURNAL-DATE", FieldType.STRING, 6);
        gw_Ledger_Description = gw_Ledger.newFieldInGroup("gw_Ledger_Description", "DESCRIPTION", FieldType.STRING, 20);
        gw_Ledger_Pnd_Amount = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Amount", "#AMOUNT", FieldType.DECIMAL, 12,2);
        gw_Ledger_Pnd_Dccr_Ind = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Dccr_Ind", "#DCCR-IND", FieldType.STRING, 1);
        gw_Ledger_Filler2 = gw_Ledger.newFieldInGroup("gw_Ledger_Filler2", "FILLER2", FieldType.STRING, 55);
        gw_Ledger_Pnd_Gl_Company = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Gl_Company", "#GL-COMPANY", FieldType.STRING, 5);
        gw_Ledger_Pnd_Gl_Account = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Gl_Account", "#GL-ACCOUNT", FieldType.STRING, 15);
        gw_Ledger_Pnd_Post_Date = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Post_Date", "#POST-DATE", FieldType.STRING, 4);
        gw_Ledger_Pnd_Policy_Number = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Policy_Number", "#POLICY-NUMBER", FieldType.STRING, 8);
        gw_Ledger_Check_Number = gw_Ledger.newFieldInGroup("gw_Ledger_Check_Number", "CHECK-NUMBER", FieldType.STRING, 8);
        gw_Ledger_Pnd_Eff_Date = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 6);
        gw_Ledger_Pnd_Eff_DateRedef1 = gw_Ledger.newGroupInGroup("gw_Ledger_Pnd_Eff_DateRedef1", "Redefines", gw_Ledger_Pnd_Eff_Date);
        gw_Ledger_Pnd_Eff_Mm = gw_Ledger_Pnd_Eff_DateRedef1.newFieldInGroup("gw_Ledger_Pnd_Eff_Mm", "#EFF-MM", FieldType.STRING, 2);
        gw_Ledger_Pnd_Eff_Dd = gw_Ledger_Pnd_Eff_DateRedef1.newFieldInGroup("gw_Ledger_Pnd_Eff_Dd", "#EFF-DD", FieldType.STRING, 2);
        gw_Ledger_Pnd_Eff_Yr = gw_Ledger_Pnd_Eff_DateRedef1.newFieldInGroup("gw_Ledger_Pnd_Eff_Yr", "#EFF-YR", FieldType.STRING, 2);
        gw_Ledger_Constant = gw_Ledger.newFieldInGroup("gw_Ledger_Constant", "CONSTANT", FieldType.STRING, 6);
        gw_Ledger_Pnd_Residency = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Residency", "#RESIDENCY", FieldType.STRING, 2);
        gw_Ledger_Pnd_Ia_Orgn_Cde = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Ia_Orgn_Cde", "#IA-ORGN-CDE", FieldType.NUMERIC, 2);
        gw_Ledger_Pnd_Option_Cde = gw_Ledger.newFieldInGroup("gw_Ledger_Pnd_Option_Cde", "#OPTION-CDE", FieldType.NUMERIC, 2);
        gw_Ledger_Filler3 = gw_Ledger.newFieldInGroup("gw_Ledger_Filler3", "FILLER3", FieldType.STRING, 16);

        this.setRecordName("LdaCpvl660");
    }

    public void initializeValues() throws Exception
    {
        reset();
        gw_Ledger_Record_Type.setInitialValue("1");
        gw_Ledger_Oprid.setInitialValue("BPSPYMTS");
        gw_Ledger_Subsystem.setInitialValue("BPS");
        gw_Ledger_Batch_No.setInitialValue("BPS");
        gw_Ledger_Id_1.setInitialValue("1");
        gw_Ledger_Seq_N.setInitialValue("00000");
        gw_Ledger_Description.setInitialValue("BPS WARRANTS");
        gw_Ledger_Check_Number.setInitialValue("00000000");
        gw_Ledger_Constant.setInitialValue("000000");
    }

    // Constructor
    public LdaCpvl660() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
