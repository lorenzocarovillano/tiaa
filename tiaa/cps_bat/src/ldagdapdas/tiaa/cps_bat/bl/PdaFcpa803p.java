/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:28 PM
**        * FROM NATURAL PDA     : FCPA803P
************************************************************
**        * FILE NAME            : PdaFcpa803p.java
**        * CLASS NAME           : PdaFcpa803p
**        * INSTANCE NAME        : PdaFcpa803p
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa803p extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpa803p;
    private DbsField pnd_Fcpa803p_Pymnt_Postl_Data;
    private DbsGroup pnd_Fcpa803p_Pymnt_Postl_DataRedef1;
    private DbsField pnd_Fcpa803p_Pnd_Zip_Pos;

    public DbsGroup getPnd_Fcpa803p() { return pnd_Fcpa803p; }

    public DbsField getPnd_Fcpa803p_Pymnt_Postl_Data() { return pnd_Fcpa803p_Pymnt_Postl_Data; }

    public DbsGroup getPnd_Fcpa803p_Pymnt_Postl_DataRedef1() { return pnd_Fcpa803p_Pymnt_Postl_DataRedef1; }

    public DbsField getPnd_Fcpa803p_Pnd_Zip_Pos() { return pnd_Fcpa803p_Pnd_Zip_Pos; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpa803p = dbsRecord.newGroupInRecord("pnd_Fcpa803p", "#FCPA803P");
        pnd_Fcpa803p.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpa803p_Pymnt_Postl_Data = pnd_Fcpa803p.newFieldInGroup("pnd_Fcpa803p_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 32);
        pnd_Fcpa803p_Pymnt_Postl_DataRedef1 = pnd_Fcpa803p.newGroupInGroup("pnd_Fcpa803p_Pymnt_Postl_DataRedef1", "Redefines", pnd_Fcpa803p_Pymnt_Postl_Data);
        pnd_Fcpa803p_Pnd_Zip_Pos = pnd_Fcpa803p_Pymnt_Postl_DataRedef1.newFieldArrayInGroup("pnd_Fcpa803p_Pnd_Zip_Pos", "#ZIP-POS", FieldType.NUMERIC, 
            1, new DbsArrayController(1,13));

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa803p(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

