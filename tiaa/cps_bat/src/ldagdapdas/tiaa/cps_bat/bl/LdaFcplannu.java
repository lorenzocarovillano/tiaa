/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:24 PM
**        * FROM NATURAL LDA     : FCPLANNU
************************************************************
**        * FILE NAME            : LdaFcplannu.java
**        * CLASS NAME           : LdaFcplannu
**        * INSTANCE NAME        : LdaFcplannu
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplannu extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_fcp_Cons_Annu;
    private DbsField fcp_Cons_Annu_Cntrct_Rcrd_Typ;
    private DbsField fcp_Cons_Annu_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Annu_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Annu_Cntrct_Invrse_Dte;
    private DbsField fcp_Cons_Annu_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup fcp_Cons_Annu_Pymnt_Prcss_Seq_NbrRedef1;
    private DbsField fcp_Cons_Annu_Pymnt_Prcss_Seq_Num;
    private DbsField fcp_Cons_Annu_Pymnt_Instmt_Nbr;
    private DbsField fcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt;
    private DbsField fcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt;
    private DbsField fcp_Cons_Annu_Count_Castpymnt_Annot_Grp;
    private DbsGroup fcp_Cons_Annu_Pymnt_Annot_Grp;
    private DbsField fcp_Cons_Annu_Pymnt_Annot_Dte;
    private DbsField fcp_Cons_Annu_Pymnt_Annot_Id_Nbr;
    private DbsField fcp_Cons_Annu_Pymnt_Annot_Flag_Ind;
    private DbsField fcp_Cons_Annu_Cntrct_Hold_Tme;

    public DataAccessProgramView getVw_fcp_Cons_Annu() { return vw_fcp_Cons_Annu; }

    public DbsField getFcp_Cons_Annu_Cntrct_Rcrd_Typ() { return fcp_Cons_Annu_Cntrct_Rcrd_Typ; }

    public DbsField getFcp_Cons_Annu_Cntrct_Ppcn_Nbr() { return fcp_Cons_Annu_Cntrct_Ppcn_Nbr; }

    public DbsField getFcp_Cons_Annu_Cntrct_Orgn_Cde() { return fcp_Cons_Annu_Cntrct_Orgn_Cde; }

    public DbsField getFcp_Cons_Annu_Cntrct_Invrse_Dte() { return fcp_Cons_Annu_Cntrct_Invrse_Dte; }

    public DbsField getFcp_Cons_Annu_Pymnt_Prcss_Seq_Nbr() { return fcp_Cons_Annu_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getFcp_Cons_Annu_Pymnt_Prcss_Seq_NbrRedef1() { return fcp_Cons_Annu_Pymnt_Prcss_Seq_NbrRedef1; }

    public DbsField getFcp_Cons_Annu_Pymnt_Prcss_Seq_Num() { return fcp_Cons_Annu_Pymnt_Prcss_Seq_Num; }

    public DbsField getFcp_Cons_Annu_Pymnt_Instmt_Nbr() { return fcp_Cons_Annu_Pymnt_Instmt_Nbr; }

    public DbsField getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt() { return fcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt; }

    public DbsField getFcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt() { return fcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt; }

    public DbsField getFcp_Cons_Annu_Count_Castpymnt_Annot_Grp() { return fcp_Cons_Annu_Count_Castpymnt_Annot_Grp; }

    public DbsGroup getFcp_Cons_Annu_Pymnt_Annot_Grp() { return fcp_Cons_Annu_Pymnt_Annot_Grp; }

    public DbsField getFcp_Cons_Annu_Pymnt_Annot_Dte() { return fcp_Cons_Annu_Pymnt_Annot_Dte; }

    public DbsField getFcp_Cons_Annu_Pymnt_Annot_Id_Nbr() { return fcp_Cons_Annu_Pymnt_Annot_Id_Nbr; }

    public DbsField getFcp_Cons_Annu_Pymnt_Annot_Flag_Ind() { return fcp_Cons_Annu_Pymnt_Annot_Flag_Ind; }

    public DbsField getFcp_Cons_Annu_Cntrct_Hold_Tme() { return fcp_Cons_Annu_Cntrct_Hold_Tme; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_fcp_Cons_Annu = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Annu", "FCP-CONS-ANNU"), "FCP_CONS_ANNOT", "FCP_EFT_GLBL", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ANNOT"));
        fcp_Cons_Annu_Cntrct_Rcrd_Typ = vw_fcp_Cons_Annu.getRecord().newFieldInGroup("fcp_Cons_Annu_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RCRD_TYP");
        fcp_Cons_Annu_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Annu.getRecord().newFieldInGroup("fcp_Cons_Annu_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Annu_Cntrct_Orgn_Cde = vw_fcp_Cons_Annu.getRecord().newFieldInGroup("fcp_Cons_Annu_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Annu_Cntrct_Invrse_Dte = vw_fcp_Cons_Annu.getRecord().newFieldInGroup("fcp_Cons_Annu_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        fcp_Cons_Annu_Pymnt_Prcss_Seq_Nbr = vw_fcp_Cons_Annu.getRecord().newFieldInGroup("fcp_Cons_Annu_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        fcp_Cons_Annu_Pymnt_Prcss_Seq_NbrRedef1 = vw_fcp_Cons_Annu.getRecord().newGroupInGroup("fcp_Cons_Annu_Pymnt_Prcss_Seq_NbrRedef1", "Redefines", 
            fcp_Cons_Annu_Pymnt_Prcss_Seq_Nbr);
        fcp_Cons_Annu_Pymnt_Prcss_Seq_Num = fcp_Cons_Annu_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("fcp_Cons_Annu_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        fcp_Cons_Annu_Pymnt_Instmt_Nbr = fcp_Cons_Annu_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("fcp_Cons_Annu_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        fcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt = vw_fcp_Cons_Annu.getRecord().newFieldInGroup("fcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt", "PYMNT-RMRK-LINE1-TXT", 
            FieldType.STRING, 70, RepeatingFieldStrategy.None, "PYMNT_RMRK_LINE1_TXT");
        fcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt = vw_fcp_Cons_Annu.getRecord().newFieldInGroup("fcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt", "PYMNT-RMRK-LINE2-TXT", 
            FieldType.STRING, 70, RepeatingFieldStrategy.None, "PYMNT_RMRK_LINE2_TXT");
        fcp_Cons_Annu_Count_Castpymnt_Annot_Grp = vw_fcp_Cons_Annu.getRecord().newFieldInGroup("fcp_Cons_Annu_Count_Castpymnt_Annot_Grp", "C*PYMNT-ANNOT-GRP", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        fcp_Cons_Annu_Pymnt_Annot_Grp = vw_fcp_Cons_Annu.getRecord().newGroupInGroup("fcp_Cons_Annu_Pymnt_Annot_Grp", "PYMNT-ANNOT-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        fcp_Cons_Annu_Pymnt_Annot_Dte = fcp_Cons_Annu_Pymnt_Annot_Grp.newFieldArrayInGroup("fcp_Cons_Annu_Pymnt_Annot_Dte", "PYMNT-ANNOT-DTE", FieldType.DATE, 
            new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_DTE", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        fcp_Cons_Annu_Pymnt_Annot_Id_Nbr = fcp_Cons_Annu_Pymnt_Annot_Grp.newFieldArrayInGroup("fcp_Cons_Annu_Pymnt_Annot_Id_Nbr", "PYMNT-ANNOT-ID-NBR", 
            FieldType.STRING, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_ID_NBR", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        fcp_Cons_Annu_Pymnt_Annot_Flag_Ind = fcp_Cons_Annu_Pymnt_Annot_Grp.newFieldArrayInGroup("fcp_Cons_Annu_Pymnt_Annot_Flag_Ind", "PYMNT-ANNOT-FLAG-IND", 
            FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_FLAG_IND", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        fcp_Cons_Annu_Cntrct_Hold_Tme = vw_fcp_Cons_Annu.getRecord().newFieldInGroup("fcp_Cons_Annu_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "CNTRCT_HOLD_TME");
        vw_fcp_Cons_Annu.setUniquePeList();

        this.setRecordName("LdaFcplannu");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_fcp_Cons_Annu.reset();
    }

    // Constructor
    public LdaFcplannu() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
