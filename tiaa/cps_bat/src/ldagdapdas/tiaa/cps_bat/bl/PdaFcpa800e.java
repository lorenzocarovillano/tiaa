/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:26 PM
**        * FROM NATURAL PDA     : FCPA800E
************************************************************
**        * FILE NAME            : PdaFcpa800e.java
**        * CLASS NAME           : PdaFcpa800e
**        * INSTANCE NAME        : PdaFcpa800e
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa800e extends PdaBase
{
    // Properties
    private DbsGroup pnd_Canadian_Tax;
    private DbsField pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Orgn_Cde;
    private DbsField pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Invrse_Dte;
    private DbsField pnd_Canadian_Tax_Pnd_Can_Tax_Pay_Type_Req_Ind;
    private DbsField pnd_Canadian_Tax_Pnd_Can_Tax_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmpny_Cde;
    private DbsField pnd_Canadian_Tax_Pnd_Can_Tax_Inv_Acct_Can_Tax_Amt;

    public DbsGroup getPnd_Canadian_Tax() { return pnd_Canadian_Tax; }

    public DbsField getPnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Orgn_Cde() { return pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Invrse_Dte() { return pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Invrse_Dte; }

    public DbsField getPnd_Canadian_Tax_Pnd_Can_Tax_Pay_Type_Req_Ind() { return pnd_Canadian_Tax_Pnd_Can_Tax_Pay_Type_Req_Ind; }

    public DbsField getPnd_Canadian_Tax_Pnd_Can_Tax_Pymnt_Prcss_Seq_Num() { return pnd_Canadian_Tax_Pnd_Can_Tax_Pymnt_Prcss_Seq_Num; }

    public DbsField getPnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmbn_Nbr() { return pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmbn_Nbr; }

    public DbsField getPnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmpny_Cde() { return pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmpny_Cde; }

    public DbsField getPnd_Canadian_Tax_Pnd_Can_Tax_Inv_Acct_Can_Tax_Amt() { return pnd_Canadian_Tax_Pnd_Can_Tax_Inv_Acct_Can_Tax_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Canadian_Tax = dbsRecord.newGroupInRecord("pnd_Canadian_Tax", "#CANADIAN-TAX");
        pnd_Canadian_Tax.setParameterOption(ParameterOption.ByReference);
        pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Orgn_Cde = pnd_Canadian_Tax.newFieldInGroup("pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Orgn_Cde", "#CAN-TAX-CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Invrse_Dte = pnd_Canadian_Tax.newFieldInGroup("pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Invrse_Dte", "#CAN-TAX-CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Canadian_Tax_Pnd_Can_Tax_Pay_Type_Req_Ind = pnd_Canadian_Tax.newFieldInGroup("pnd_Canadian_Tax_Pnd_Can_Tax_Pay_Type_Req_Ind", "#CAN-TAX-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Canadian_Tax_Pnd_Can_Tax_Pymnt_Prcss_Seq_Num = pnd_Canadian_Tax.newFieldInGroup("pnd_Canadian_Tax_Pnd_Can_Tax_Pymnt_Prcss_Seq_Num", "#CAN-TAX-PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 9);
        pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmbn_Nbr = pnd_Canadian_Tax.newFieldInGroup("pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmbn_Nbr", "#CAN-TAX-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmpny_Cde = pnd_Canadian_Tax.newFieldInGroup("pnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmpny_Cde", "#CAN-TAX-CNTRCT-CMPNY-CDE", 
            FieldType.STRING, 1);
        pnd_Canadian_Tax_Pnd_Can_Tax_Inv_Acct_Can_Tax_Amt = pnd_Canadian_Tax.newFieldArrayInGroup("pnd_Canadian_Tax_Pnd_Can_Tax_Inv_Acct_Can_Tax_Amt", 
            "#CAN-TAX-INV-ACCT-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,40));

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa800e(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

