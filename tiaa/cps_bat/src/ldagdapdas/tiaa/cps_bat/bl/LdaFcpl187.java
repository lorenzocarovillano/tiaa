/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:02 PM
**        * FROM NATURAL LDA     : FCPL187
************************************************************
**        * FILE NAME            : LdaFcpl187.java
**        * CLASS NAME           : LdaFcpl187
**        * INSTANCE NAME        : LdaFcpl187
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl187 extends DbsRecord
{
    // Properties
    private DbsField pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Cntrct_Orgn_Desc;
    private DbsField pnd_Cntrct_Hold_Dept_Cde;
    private DbsField pnd_Cntrct_Hold_Dept_Desc;
    private DbsField pnd_Cntrct_Hold_Type_Cde;
    private DbsField pnd_Cntrct_Hold_Type_Desc;

    public DbsField getPnd_Cntrct_Orgn_Cde() { return pnd_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Cntrct_Orgn_Desc() { return pnd_Cntrct_Orgn_Desc; }

    public DbsField getPnd_Cntrct_Hold_Dept_Cde() { return pnd_Cntrct_Hold_Dept_Cde; }

    public DbsField getPnd_Cntrct_Hold_Dept_Desc() { return pnd_Cntrct_Hold_Dept_Desc; }

    public DbsField getPnd_Cntrct_Hold_Type_Cde() { return pnd_Cntrct_Hold_Type_Cde; }

    public DbsField getPnd_Cntrct_Hold_Type_Desc() { return pnd_Cntrct_Hold_Type_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Cntrct_Orgn_Cde = newFieldArrayInRecord("pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.STRING, 2, new DbsArrayController(1,3));

        pnd_Cntrct_Orgn_Desc = newFieldArrayInRecord("pnd_Cntrct_Orgn_Desc", "#CNTRCT-ORGN-DESC", FieldType.STRING, 20, new DbsArrayController(1,3));

        pnd_Cntrct_Hold_Dept_Cde = newFieldArrayInRecord("pnd_Cntrct_Hold_Dept_Cde", "#CNTRCT-HOLD-DEPT-CDE", FieldType.STRING, 2, new DbsArrayController(1,
            3,1,2));

        pnd_Cntrct_Hold_Dept_Desc = newFieldArrayInRecord("pnd_Cntrct_Hold_Dept_Desc", "#CNTRCT-HOLD-DEPT-DESC", FieldType.STRING, 25, new DbsArrayController(1,
            3,1,2));

        pnd_Cntrct_Hold_Type_Cde = newFieldArrayInRecord("pnd_Cntrct_Hold_Type_Cde", "#CNTRCT-HOLD-TYPE-CDE", FieldType.STRING, 2, new DbsArrayController(1,
            3,1,2,1,20));

        pnd_Cntrct_Hold_Type_Desc = newFieldArrayInRecord("pnd_Cntrct_Hold_Type_Desc", "#CNTRCT-HOLD-TYPE-DESC", FieldType.STRING, 35, new DbsArrayController(1,
            3,1,2,1,20));

        this.setRecordName("LdaFcpl187");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Cntrct_Orgn_Cde.getValue(1).setInitialValue("NZ");
        pnd_Cntrct_Orgn_Cde.getValue(2).setInitialValue("AL");
        pnd_Cntrct_Orgn_Cde.getValue(3).setInitialValue("EW");
        pnd_Cntrct_Orgn_Desc.getValue(1).setInitialValue("ADAM");
        pnd_Cntrct_Orgn_Desc.getValue(2).setInitialValue("ANNUITY LOAN");
        pnd_Cntrct_Orgn_Desc.getValue(3).setInitialValue("ELECTRONIC WARRANT");
        pnd_Cntrct_Hold_Dept_Cde.getValue(1,1).setInitialValue("NZ");
        pnd_Cntrct_Hold_Dept_Cde.getValue(1,2).setInitialValue("00");
        pnd_Cntrct_Hold_Dept_Cde.getValue(2,1).setInitialValue("AL");
        pnd_Cntrct_Hold_Dept_Cde.getValue(2,2).setInitialValue("00");
        pnd_Cntrct_Hold_Dept_Cde.getValue(3,1).setInitialValue("EW");
        pnd_Cntrct_Hold_Dept_Cde.getValue(3,2).setInitialValue("00");
        pnd_Cntrct_Hold_Dept_Desc.getValue(1,1).setInitialValue("Benefit Payment Services");
        pnd_Cntrct_Hold_Dept_Desc.getValue(1,2).setInitialValue("Benefit Payment Services");
        pnd_Cntrct_Hold_Dept_Desc.getValue(2,1).setInitialValue("Loan Unit");
        pnd_Cntrct_Hold_Dept_Desc.getValue(2,2).setInitialValue("Loan Unit");
        pnd_Cntrct_Hold_Dept_Desc.getValue(3,1).setInitialValue("Benefit Payment Services");
        pnd_Cntrct_Hold_Dept_Desc.getValue(3,2).setInitialValue("Benefit Payment Services");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,1,1).setInitialValue("AC");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,1,2).setInitialValue("FT");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,1,3).setInitialValue("IR");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,1,4).setInitialValue("ZR");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,1,5).setInitialValue("CA");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,1).setInitialValue("BF");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,2).setInitialValue("CN");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,3).setInitialValue("CR");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,4).setInitialValue("EC");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,5).setInitialValue("EE");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,6).setInitialValue("EF");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,7).setInitialValue("FW");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,8).setInitialValue("OV");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,9).setInitialValue("PU");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,10).setInitialValue("RI");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,11).setInitialValue("SE");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,12).setInitialValue("SN");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,13).setInitialValue("SR");
        pnd_Cntrct_Hold_Type_Cde.getValue(1,2,14).setInitialValue("SP");
        pnd_Cntrct_Hold_Type_Cde.getValue(2,2,1).setInitialValue("CN");
        pnd_Cntrct_Hold_Type_Cde.getValue(2,2,2).setInitialValue("CR");
        pnd_Cntrct_Hold_Type_Cde.getValue(2,2,3).setInitialValue("FW");
        pnd_Cntrct_Hold_Type_Cde.getValue(2,2,4).setInitialValue("OV");
        pnd_Cntrct_Hold_Type_Cde.getValue(2,2,5).setInitialValue("PU");
        pnd_Cntrct_Hold_Type_Cde.getValue(2,2,6).setInitialValue("SN");
        pnd_Cntrct_Hold_Type_Cde.getValue(2,2,7).setInitialValue("SR");
        pnd_Cntrct_Hold_Type_Cde.getValue(2,2,8).setInitialValue("SE");
        pnd_Cntrct_Hold_Type_Cde.getValue(2,2,9).setInitialValue("BF");
        pnd_Cntrct_Hold_Type_Cde.getValue(2,2,10).setInitialValue("EC");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,1).setInitialValue("BF");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,2).setInitialValue("CN");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,3).setInitialValue("CR");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,4).setInitialValue("EC");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,5).setInitialValue("EE");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,6).setInitialValue("EF");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,7).setInitialValue("FW");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,8).setInitialValue("OV");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,9).setInitialValue("PU");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,10).setInitialValue("RI");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,11).setInitialValue("SE");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,12).setInitialValue("SN");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,13).setInitialValue("SR");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,14).setInitialValue("CA");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,15).setInitialValue("FT");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,16).setInitialValue("C0");
        pnd_Cntrct_Hold_Type_Cde.getValue(3,2,17).setInitialValue("F0");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,1,1).setInitialValue("Annuity Certain Less Than 10 Years");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,1,2).setInitialValue("Future");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,1,3).setInitialValue("Internal Rollover");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,1,4).setInitialValue("Zero Dollar Check");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,1,5).setInitialValue("Address Change within 14 Days");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,1).setInitialValue("Hold for Bank Form");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,2).setInitialValue("Cancel - No Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,3).setInitialValue("Cancel and Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,4).setInitialValue("Emergency BFT PMT-CHECK");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,5).setInitialValue("Emergency BFT PMT-EFT");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,6).setInitialValue("Emergency BFT PMT-FW");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,7).setInitialValue("Send FEDWIRE");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,8).setInitialValue("Send Overnight");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,9).setInitialValue("Participant Pickup");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,10).setInitialValue("Internal Rollover");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,11).setInitialValue("Send EFT");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,12).setInitialValue("Stop - No Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,13).setInitialValue("Stop and Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(1,2,14).setInitialValue("Special Handling");
        pnd_Cntrct_Hold_Type_Desc.getValue(2,2,1).setInitialValue("Cancel - No Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(2,2,2).setInitialValue("Cancel and Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(2,2,3).setInitialValue("Send Fed Wire");
        pnd_Cntrct_Hold_Type_Desc.getValue(2,2,4).setInitialValue("Send Overnight");
        pnd_Cntrct_Hold_Type_Desc.getValue(2,2,5).setInitialValue("Participant Pick-Up");
        pnd_Cntrct_Hold_Type_Desc.getValue(2,2,6).setInitialValue("Stop - No Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(2,2,7).setInitialValue("Stop and Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(2,2,8).setInitialValue("Send EFT");
        pnd_Cntrct_Hold_Type_Desc.getValue(2,2,9).setInitialValue("Hold For Bank Form");
        pnd_Cntrct_Hold_Type_Desc.getValue(2,2,10).setInitialValue("Emergency Benefit Pmt Check");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,1).setInitialValue("Hold for Bank Form");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,2).setInitialValue("Cancel - No Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,3).setInitialValue("Cancel and Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,4).setInitialValue("Emergency BFT PMT-CHECK");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,5).setInitialValue("Emergency BFT PMT-EFT");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,6).setInitialValue("Emergency BFT PMT-FW");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,7).setInitialValue("Send FEDWIRE");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,8).setInitialValue("Send Overnight");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,9).setInitialValue("Participant Pickup");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,10).setInitialValue("Internal Rollover");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,11).setInitialValue("Send EFT");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,12).setInitialValue("Stop - No Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,13).setInitialValue("Stop and Redraw");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,14).setInitialValue("Emergency Benefit Pmt Check");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,15).setInitialValue("Address Change");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,16).setInitialValue("Future Date");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,17).setInitialValue("Canadian Address");
        pnd_Cntrct_Hold_Type_Desc.getValue(3,2,18).setInitialValue("Foreign Address");
    }

    // Constructor
    public LdaFcpl187() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
