/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:42 PM
**        * FROM NATURAL LDA     : CPULPYM2
************************************************************
**        * FILE NAME            : LdaCpulpym2.java
**        * CLASS NAME           : LdaCpulpym2
**        * INSTANCE NAME        : LdaCpulpym2
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpulpym2 extends DbsRecord
{
    // Properties
    private DbsField payment_Record;
    private DbsGroup payment_RecordRedef1;
    private DbsField payment_Record_Ssn;
    private DbsField payment_Record_Plan;
    private DbsField payment_Record_Subplan;
    private DbsField payment_Record_Origin_Code;
    private DbsField payment_Record_Transaction_Type;
    private DbsField payment_Record_Status;
    private DbsField payment_Record_Spouse_Pay_Status;
    private DbsField payment_Record_Lob_Code;
    private DbsField payment_Record_Cancel_Redraw_Code;
    private DbsField payment_Record_Gross_Amt;
    private DbsField payment_Record_Taxable_Amt;
    private DbsField payment_Record_Federal_Tax;
    private DbsField payment_Record_State_Tax;
    private DbsField payment_Record_Canadian_Tax;
    private DbsField payment_Record_Local_Tax;
    private DbsField payment_Record_Fees;
    private DbsField payment_Record_Deductions;
    private DbsField payment_Record_Net_Amt;
    private DbsField payment_Record_Check_Date;
    private DbsField payment_Record_Vtran_Id;
    private DbsGroup payment_Record_Check_Destination;
    private DbsField payment_Record_Pymnt_Name;
    private DbsField payment_Record_Addr_1;
    private DbsField payment_Record_Addr_2;
    private DbsField payment_Record_Addr_3;
    private DbsField payment_Record_Addr_4;
    private DbsField payment_Record_Addr_5;
    private DbsField payment_Record_Addr_6;
    private DbsGroup payment_Record_Eft_Destination;
    private DbsField payment_Record_Bank_Name;
    private DbsField payment_Record_Account_Number;
    private DbsField payment_Record_Routing_Number;
    private DbsField payment_Record_Legacy_Eft_Key;
    private DbsGroup payment_Record_Legacy_Eft_KeyRedef2;
    private DbsField payment_Record_Key_Contract;
    private DbsField payment_Record_Key_Payee;
    private DbsField payment_Record_Key_Origin;
    private DbsField payment_Record_Key_Inverse_Date;
    private DbsField payment_Record_Key_Process_Seq;
    private DbsGroup payment_Record_Key_Process_SeqRedef3;
    private DbsField payment_Record_Pnd_Seq_A;
    private DbsField payment_Record_Frequency;
    private DbsField payment_Record_Contract_Number;
    private DbsField payment_Record_Payee_Code;
    private DbsField payment_Record_Settlement_Date;
    private DbsField payment_Record_Combined_Contract_Number;
    private DbsField payment_Record_Hold_Code;
    private DbsField payment_Record_Check_Num_Prefix;
    private DbsField payment_Record_Check_Num_Suffix;
    private DbsField payment_Record_Combined_Ind;
    private DbsField payment_Record_Omni_Trans_Type;
    private DbsField payment_Record_Checking_Savings_Ind;
    private DbsField payment_Record_Pin;
    private DbsField payment_Record_Status_Text;
    private DbsField payment_Record_Annotation_Ind;
    private DbsField payment_Record_Residency;
    private DbsGroup payment_Record_Deduction_Grp;
    private DbsField payment_Record_Deduction_Code;
    private DbsField payment_Record_Deduction_Amt;
    private DbsField payment_Record_Ivc_Amt;
    private DbsField payment_Record_Nra_Ind;
    private DbsField payment_Record_Roth_Ind;
    private DbsField payment_Record_Contract_Interest_Amt;
    private DbsField payment_Record_Roth_Contrib;
    private DbsField payment_Record_Roth_Earnings;
    private DbsField payment_Record_Roth_First_Dte;
    private DbsField payment_Record_Check_Cash_Dte;
    private DbsField payment_Record_Original_Check_Nbr;
    private DbsField payment_Record_Eft_Effective_Date;
    private DbsField payment_Record_Apin;
    private DbsGroup payment_Record_Statement_Address;
    private DbsField payment_Record_Sa_Pymnt_Name;
    private DbsField payment_Record_Sa_Addr_1;
    private DbsField payment_Record_Sa_Addr_2;
    private DbsField payment_Record_Sa_Addr_3;
    private DbsField payment_Record_Sa_Addr_4;
    private DbsField payment_Record_Sa_Addr_5;
    private DbsField payment_Record_Sa_Addr_6;
    private DbsField payment_Record_Contract_To;
    private DbsField payment_Record_Payment_Req_Type;
    private DbsField payment_Record_Pay_Group;
    private DbsField payment_Record_Intl_Iban;
    private DbsField payment_Record_Intl_Aba_Eft;
    private DbsField payment_Record_Reason_Code;
    private DbsField payment_Record_Reason_Description;
    private DbsField payment_Record_Country_Code;
    private DbsField payment_Record_Iban_Ind;
    private DbsField filler01;
    private DbsField payment_Record_Check_Number;
    private DbsGroup payment_Record_Check_NumberRedef4;
    private DbsField payment_Record_Check_Alpha;

    public DbsField getPayment_Record() { return payment_Record; }

    public DbsGroup getPayment_RecordRedef1() { return payment_RecordRedef1; }

    public DbsField getPayment_Record_Ssn() { return payment_Record_Ssn; }

    public DbsField getPayment_Record_Plan() { return payment_Record_Plan; }

    public DbsField getPayment_Record_Subplan() { return payment_Record_Subplan; }

    public DbsField getPayment_Record_Origin_Code() { return payment_Record_Origin_Code; }

    public DbsField getPayment_Record_Transaction_Type() { return payment_Record_Transaction_Type; }

    public DbsField getPayment_Record_Status() { return payment_Record_Status; }

    public DbsField getPayment_Record_Spouse_Pay_Status() { return payment_Record_Spouse_Pay_Status; }

    public DbsField getPayment_Record_Lob_Code() { return payment_Record_Lob_Code; }

    public DbsField getPayment_Record_Cancel_Redraw_Code() { return payment_Record_Cancel_Redraw_Code; }

    public DbsField getPayment_Record_Gross_Amt() { return payment_Record_Gross_Amt; }

    public DbsField getPayment_Record_Taxable_Amt() { return payment_Record_Taxable_Amt; }

    public DbsField getPayment_Record_Federal_Tax() { return payment_Record_Federal_Tax; }

    public DbsField getPayment_Record_State_Tax() { return payment_Record_State_Tax; }

    public DbsField getPayment_Record_Canadian_Tax() { return payment_Record_Canadian_Tax; }

    public DbsField getPayment_Record_Local_Tax() { return payment_Record_Local_Tax; }

    public DbsField getPayment_Record_Fees() { return payment_Record_Fees; }

    public DbsField getPayment_Record_Deductions() { return payment_Record_Deductions; }

    public DbsField getPayment_Record_Net_Amt() { return payment_Record_Net_Amt; }

    public DbsField getPayment_Record_Check_Date() { return payment_Record_Check_Date; }

    public DbsField getPayment_Record_Vtran_Id() { return payment_Record_Vtran_Id; }

    public DbsGroup getPayment_Record_Check_Destination() { return payment_Record_Check_Destination; }

    public DbsField getPayment_Record_Pymnt_Name() { return payment_Record_Pymnt_Name; }

    public DbsField getPayment_Record_Addr_1() { return payment_Record_Addr_1; }

    public DbsField getPayment_Record_Addr_2() { return payment_Record_Addr_2; }

    public DbsField getPayment_Record_Addr_3() { return payment_Record_Addr_3; }

    public DbsField getPayment_Record_Addr_4() { return payment_Record_Addr_4; }

    public DbsField getPayment_Record_Addr_5() { return payment_Record_Addr_5; }

    public DbsField getPayment_Record_Addr_6() { return payment_Record_Addr_6; }

    public DbsGroup getPayment_Record_Eft_Destination() { return payment_Record_Eft_Destination; }

    public DbsField getPayment_Record_Bank_Name() { return payment_Record_Bank_Name; }

    public DbsField getPayment_Record_Account_Number() { return payment_Record_Account_Number; }

    public DbsField getPayment_Record_Routing_Number() { return payment_Record_Routing_Number; }

    public DbsField getPayment_Record_Legacy_Eft_Key() { return payment_Record_Legacy_Eft_Key; }

    public DbsGroup getPayment_Record_Legacy_Eft_KeyRedef2() { return payment_Record_Legacy_Eft_KeyRedef2; }

    public DbsField getPayment_Record_Key_Contract() { return payment_Record_Key_Contract; }

    public DbsField getPayment_Record_Key_Payee() { return payment_Record_Key_Payee; }

    public DbsField getPayment_Record_Key_Origin() { return payment_Record_Key_Origin; }

    public DbsField getPayment_Record_Key_Inverse_Date() { return payment_Record_Key_Inverse_Date; }

    public DbsField getPayment_Record_Key_Process_Seq() { return payment_Record_Key_Process_Seq; }

    public DbsGroup getPayment_Record_Key_Process_SeqRedef3() { return payment_Record_Key_Process_SeqRedef3; }

    public DbsField getPayment_Record_Pnd_Seq_A() { return payment_Record_Pnd_Seq_A; }

    public DbsField getPayment_Record_Frequency() { return payment_Record_Frequency; }

    public DbsField getPayment_Record_Contract_Number() { return payment_Record_Contract_Number; }

    public DbsField getPayment_Record_Payee_Code() { return payment_Record_Payee_Code; }

    public DbsField getPayment_Record_Settlement_Date() { return payment_Record_Settlement_Date; }

    public DbsField getPayment_Record_Combined_Contract_Number() { return payment_Record_Combined_Contract_Number; }

    public DbsField getPayment_Record_Hold_Code() { return payment_Record_Hold_Code; }

    public DbsField getPayment_Record_Check_Num_Prefix() { return payment_Record_Check_Num_Prefix; }

    public DbsField getPayment_Record_Check_Num_Suffix() { return payment_Record_Check_Num_Suffix; }

    public DbsField getPayment_Record_Combined_Ind() { return payment_Record_Combined_Ind; }

    public DbsField getPayment_Record_Omni_Trans_Type() { return payment_Record_Omni_Trans_Type; }

    public DbsField getPayment_Record_Checking_Savings_Ind() { return payment_Record_Checking_Savings_Ind; }

    public DbsField getPayment_Record_Pin() { return payment_Record_Pin; }

    public DbsField getPayment_Record_Status_Text() { return payment_Record_Status_Text; }

    public DbsField getPayment_Record_Annotation_Ind() { return payment_Record_Annotation_Ind; }

    public DbsField getPayment_Record_Residency() { return payment_Record_Residency; }

    public DbsGroup getPayment_Record_Deduction_Grp() { return payment_Record_Deduction_Grp; }

    public DbsField getPayment_Record_Deduction_Code() { return payment_Record_Deduction_Code; }

    public DbsField getPayment_Record_Deduction_Amt() { return payment_Record_Deduction_Amt; }

    public DbsField getPayment_Record_Ivc_Amt() { return payment_Record_Ivc_Amt; }

    public DbsField getPayment_Record_Nra_Ind() { return payment_Record_Nra_Ind; }

    public DbsField getPayment_Record_Roth_Ind() { return payment_Record_Roth_Ind; }

    public DbsField getPayment_Record_Contract_Interest_Amt() { return payment_Record_Contract_Interest_Amt; }

    public DbsField getPayment_Record_Roth_Contrib() { return payment_Record_Roth_Contrib; }

    public DbsField getPayment_Record_Roth_Earnings() { return payment_Record_Roth_Earnings; }

    public DbsField getPayment_Record_Roth_First_Dte() { return payment_Record_Roth_First_Dte; }

    public DbsField getPayment_Record_Check_Cash_Dte() { return payment_Record_Check_Cash_Dte; }

    public DbsField getPayment_Record_Original_Check_Nbr() { return payment_Record_Original_Check_Nbr; }

    public DbsField getPayment_Record_Eft_Effective_Date() { return payment_Record_Eft_Effective_Date; }

    public DbsField getPayment_Record_Apin() { return payment_Record_Apin; }

    public DbsGroup getPayment_Record_Statement_Address() { return payment_Record_Statement_Address; }

    public DbsField getPayment_Record_Sa_Pymnt_Name() { return payment_Record_Sa_Pymnt_Name; }

    public DbsField getPayment_Record_Sa_Addr_1() { return payment_Record_Sa_Addr_1; }

    public DbsField getPayment_Record_Sa_Addr_2() { return payment_Record_Sa_Addr_2; }

    public DbsField getPayment_Record_Sa_Addr_3() { return payment_Record_Sa_Addr_3; }

    public DbsField getPayment_Record_Sa_Addr_4() { return payment_Record_Sa_Addr_4; }

    public DbsField getPayment_Record_Sa_Addr_5() { return payment_Record_Sa_Addr_5; }

    public DbsField getPayment_Record_Sa_Addr_6() { return payment_Record_Sa_Addr_6; }

    public DbsField getPayment_Record_Contract_To() { return payment_Record_Contract_To; }

    public DbsField getPayment_Record_Payment_Req_Type() { return payment_Record_Payment_Req_Type; }

    public DbsField getPayment_Record_Pay_Group() { return payment_Record_Pay_Group; }

    public DbsField getPayment_Record_Intl_Iban() { return payment_Record_Intl_Iban; }

    public DbsField getPayment_Record_Intl_Aba_Eft() { return payment_Record_Intl_Aba_Eft; }

    public DbsField getPayment_Record_Reason_Code() { return payment_Record_Reason_Code; }

    public DbsField getPayment_Record_Reason_Description() { return payment_Record_Reason_Description; }

    public DbsField getPayment_Record_Country_Code() { return payment_Record_Country_Code; }

    public DbsField getPayment_Record_Iban_Ind() { return payment_Record_Iban_Ind; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getPayment_Record_Check_Number() { return payment_Record_Check_Number; }

    public DbsGroup getPayment_Record_Check_NumberRedef4() { return payment_Record_Check_NumberRedef4; }

    public DbsField getPayment_Record_Check_Alpha() { return payment_Record_Check_Alpha; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        payment_Record = newFieldInRecord("payment_Record", "PAYMENT-RECORD", FieldType.STRING, 1700);
        payment_RecordRedef1 = newGroupInRecord("payment_RecordRedef1", "Redefines", payment_Record);
        payment_Record_Ssn = payment_RecordRedef1.newFieldInGroup("payment_Record_Ssn", "SSN", FieldType.NUMERIC, 9);
        payment_Record_Plan = payment_RecordRedef1.newFieldInGroup("payment_Record_Plan", "PLAN", FieldType.STRING, 6);
        payment_Record_Subplan = payment_RecordRedef1.newFieldInGroup("payment_Record_Subplan", "SUBPLAN", FieldType.STRING, 6);
        payment_Record_Origin_Code = payment_RecordRedef1.newFieldInGroup("payment_Record_Origin_Code", "ORIGIN-CODE", FieldType.STRING, 2);
        payment_Record_Transaction_Type = payment_RecordRedef1.newFieldInGroup("payment_Record_Transaction_Type", "TRANSACTION-TYPE", FieldType.STRING, 
            2);
        payment_Record_Status = payment_RecordRedef1.newFieldInGroup("payment_Record_Status", "STATUS", FieldType.STRING, 1);
        payment_Record_Spouse_Pay_Status = payment_RecordRedef1.newFieldInGroup("payment_Record_Spouse_Pay_Status", "SPOUSE-PAY-STATUS", FieldType.STRING, 
            1);
        payment_Record_Lob_Code = payment_RecordRedef1.newFieldInGroup("payment_Record_Lob_Code", "LOB-CODE", FieldType.STRING, 4);
        payment_Record_Cancel_Redraw_Code = payment_RecordRedef1.newFieldInGroup("payment_Record_Cancel_Redraw_Code", "CANCEL-REDRAW-CODE", FieldType.STRING, 
            1);
        payment_Record_Gross_Amt = payment_RecordRedef1.newFieldInGroup("payment_Record_Gross_Amt", "GROSS-AMT", FieldType.DECIMAL, 11,2);
        payment_Record_Taxable_Amt = payment_RecordRedef1.newFieldInGroup("payment_Record_Taxable_Amt", "TAXABLE-AMT", FieldType.DECIMAL, 11,2);
        payment_Record_Federal_Tax = payment_RecordRedef1.newFieldInGroup("payment_Record_Federal_Tax", "FEDERAL-TAX", FieldType.DECIMAL, 11,2);
        payment_Record_State_Tax = payment_RecordRedef1.newFieldInGroup("payment_Record_State_Tax", "STATE-TAX", FieldType.DECIMAL, 11,2);
        payment_Record_Canadian_Tax = payment_RecordRedef1.newFieldInGroup("payment_Record_Canadian_Tax", "CANADIAN-TAX", FieldType.DECIMAL, 11,2);
        payment_Record_Local_Tax = payment_RecordRedef1.newFieldInGroup("payment_Record_Local_Tax", "LOCAL-TAX", FieldType.DECIMAL, 11,2);
        payment_Record_Fees = payment_RecordRedef1.newFieldInGroup("payment_Record_Fees", "FEES", FieldType.DECIMAL, 11,2);
        payment_Record_Deductions = payment_RecordRedef1.newFieldInGroup("payment_Record_Deductions", "DEDUCTIONS", FieldType.DECIMAL, 11,2);
        payment_Record_Net_Amt = payment_RecordRedef1.newFieldInGroup("payment_Record_Net_Amt", "NET-AMT", FieldType.DECIMAL, 11,2);
        payment_Record_Check_Date = payment_RecordRedef1.newFieldInGroup("payment_Record_Check_Date", "CHECK-DATE", FieldType.STRING, 8);
        payment_Record_Vtran_Id = payment_RecordRedef1.newFieldInGroup("payment_Record_Vtran_Id", "VTRAN-ID", FieldType.STRING, 68);
        payment_Record_Check_Destination = payment_RecordRedef1.newGroupInGroup("payment_Record_Check_Destination", "CHECK-DESTINATION");
        payment_Record_Pymnt_Name = payment_Record_Check_Destination.newFieldInGroup("payment_Record_Pymnt_Name", "PYMNT-NAME", FieldType.STRING, 38);
        payment_Record_Addr_1 = payment_Record_Check_Destination.newFieldInGroup("payment_Record_Addr_1", "ADDR-1", FieldType.STRING, 35);
        payment_Record_Addr_2 = payment_Record_Check_Destination.newFieldInGroup("payment_Record_Addr_2", "ADDR-2", FieldType.STRING, 35);
        payment_Record_Addr_3 = payment_Record_Check_Destination.newFieldInGroup("payment_Record_Addr_3", "ADDR-3", FieldType.STRING, 35);
        payment_Record_Addr_4 = payment_Record_Check_Destination.newFieldInGroup("payment_Record_Addr_4", "ADDR-4", FieldType.STRING, 35);
        payment_Record_Addr_5 = payment_Record_Check_Destination.newFieldInGroup("payment_Record_Addr_5", "ADDR-5", FieldType.STRING, 35);
        payment_Record_Addr_6 = payment_Record_Check_Destination.newFieldInGroup("payment_Record_Addr_6", "ADDR-6", FieldType.STRING, 35);
        payment_Record_Eft_Destination = payment_RecordRedef1.newGroupInGroup("payment_Record_Eft_Destination", "EFT-DESTINATION");
        payment_Record_Bank_Name = payment_Record_Eft_Destination.newFieldInGroup("payment_Record_Bank_Name", "BANK-NAME", FieldType.STRING, 35);
        payment_Record_Account_Number = payment_Record_Eft_Destination.newFieldInGroup("payment_Record_Account_Number", "ACCOUNT-NUMBER", FieldType.STRING, 
            21);
        payment_Record_Routing_Number = payment_Record_Eft_Destination.newFieldInGroup("payment_Record_Routing_Number", "ROUTING-NUMBER", FieldType.NUMERIC, 
            9);
        payment_Record_Legacy_Eft_Key = payment_RecordRedef1.newFieldInGroup("payment_Record_Legacy_Eft_Key", "LEGACY-EFT-KEY", FieldType.STRING, 33);
        payment_Record_Legacy_Eft_KeyRedef2 = payment_RecordRedef1.newGroupInGroup("payment_Record_Legacy_Eft_KeyRedef2", "Redefines", payment_Record_Legacy_Eft_Key);
        payment_Record_Key_Contract = payment_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("payment_Record_Key_Contract", "KEY-CONTRACT", FieldType.STRING, 
            10);
        payment_Record_Key_Payee = payment_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("payment_Record_Key_Payee", "KEY-PAYEE", FieldType.STRING, 4);
        payment_Record_Key_Origin = payment_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("payment_Record_Key_Origin", "KEY-ORIGIN", FieldType.STRING, 2);
        payment_Record_Key_Inverse_Date = payment_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("payment_Record_Key_Inverse_Date", "KEY-INVERSE-DATE", FieldType.NUMERIC, 
            8);
        payment_Record_Key_Process_Seq = payment_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("payment_Record_Key_Process_Seq", "KEY-PROCESS-SEQ", FieldType.NUMERIC, 
            9);
        payment_Record_Key_Process_SeqRedef3 = payment_Record_Legacy_Eft_KeyRedef2.newGroupInGroup("payment_Record_Key_Process_SeqRedef3", "Redefines", 
            payment_Record_Key_Process_Seq);
        payment_Record_Pnd_Seq_A = payment_Record_Key_Process_SeqRedef3.newFieldInGroup("payment_Record_Pnd_Seq_A", "#SEQ-A", FieldType.STRING, 7);
        payment_Record_Frequency = payment_RecordRedef1.newFieldInGroup("payment_Record_Frequency", "FREQUENCY", FieldType.STRING, 1);
        payment_Record_Contract_Number = payment_RecordRedef1.newFieldInGroup("payment_Record_Contract_Number", "CONTRACT-NUMBER", FieldType.STRING, 10);
        payment_Record_Payee_Code = payment_RecordRedef1.newFieldInGroup("payment_Record_Payee_Code", "PAYEE-CODE", FieldType.STRING, 4);
        payment_Record_Settlement_Date = payment_RecordRedef1.newFieldInGroup("payment_Record_Settlement_Date", "SETTLEMENT-DATE", FieldType.STRING, 8);
        payment_Record_Combined_Contract_Number = payment_RecordRedef1.newFieldInGroup("payment_Record_Combined_Contract_Number", "COMBINED-CONTRACT-NUMBER", 
            FieldType.STRING, 14);
        payment_Record_Hold_Code = payment_RecordRedef1.newFieldInGroup("payment_Record_Hold_Code", "HOLD-CODE", FieldType.STRING, 4);
        payment_Record_Check_Num_Prefix = payment_RecordRedef1.newFieldInGroup("payment_Record_Check_Num_Prefix", "CHECK-NUM-PREFIX", FieldType.STRING, 
            1);
        payment_Record_Check_Num_Suffix = payment_RecordRedef1.newFieldInGroup("payment_Record_Check_Num_Suffix", "CHECK-NUM-SUFFIX", FieldType.STRING, 
            1);
        payment_Record_Combined_Ind = payment_RecordRedef1.newFieldInGroup("payment_Record_Combined_Ind", "COMBINED-IND", FieldType.STRING, 1);
        payment_Record_Omni_Trans_Type = payment_RecordRedef1.newFieldInGroup("payment_Record_Omni_Trans_Type", "OMNI-TRANS-TYPE", FieldType.STRING, 1);
        payment_Record_Checking_Savings_Ind = payment_RecordRedef1.newFieldInGroup("payment_Record_Checking_Savings_Ind", "CHECKING-SAVINGS-IND", FieldType.STRING, 
            1);
        payment_Record_Pin = payment_RecordRedef1.newFieldInGroup("payment_Record_Pin", "PIN", FieldType.STRING, 12);
        payment_Record_Status_Text = payment_RecordRedef1.newFieldInGroup("payment_Record_Status_Text", "STATUS-TEXT", FieldType.STRING, 10);
        payment_Record_Annotation_Ind = payment_RecordRedef1.newFieldInGroup("payment_Record_Annotation_Ind", "ANNOTATION-IND", FieldType.STRING, 1);
        payment_Record_Residency = payment_RecordRedef1.newFieldInGroup("payment_Record_Residency", "RESIDENCY", FieldType.STRING, 2);
        payment_Record_Deduction_Grp = payment_RecordRedef1.newGroupArrayInGroup("payment_Record_Deduction_Grp", "DEDUCTION-GRP", new DbsArrayController(1,
            10));
        payment_Record_Deduction_Code = payment_Record_Deduction_Grp.newFieldInGroup("payment_Record_Deduction_Code", "DEDUCTION-CODE", FieldType.NUMERIC, 
            3);
        payment_Record_Deduction_Amt = payment_Record_Deduction_Grp.newFieldInGroup("payment_Record_Deduction_Amt", "DEDUCTION-AMT", FieldType.DECIMAL, 
            9,2);
        payment_Record_Ivc_Amt = payment_RecordRedef1.newFieldInGroup("payment_Record_Ivc_Amt", "IVC-AMT", FieldType.DECIMAL, 11,2);
        payment_Record_Nra_Ind = payment_RecordRedef1.newFieldInGroup("payment_Record_Nra_Ind", "NRA-IND", FieldType.STRING, 1);
        payment_Record_Roth_Ind = payment_RecordRedef1.newFieldInGroup("payment_Record_Roth_Ind", "ROTH-IND", FieldType.STRING, 1);
        payment_Record_Contract_Interest_Amt = payment_RecordRedef1.newFieldInGroup("payment_Record_Contract_Interest_Amt", "CONTRACT-INTEREST-AMT", FieldType.DECIMAL, 
            13,2);
        payment_Record_Roth_Contrib = payment_RecordRedef1.newFieldInGroup("payment_Record_Roth_Contrib", "ROTH-CONTRIB", FieldType.DECIMAL, 9,2);
        payment_Record_Roth_Earnings = payment_RecordRedef1.newFieldInGroup("payment_Record_Roth_Earnings", "ROTH-EARNINGS", FieldType.DECIMAL, 9,2);
        payment_Record_Roth_First_Dte = payment_RecordRedef1.newFieldInGroup("payment_Record_Roth_First_Dte", "ROTH-FIRST-DTE", FieldType.STRING, 8);
        payment_Record_Check_Cash_Dte = payment_RecordRedef1.newFieldInGroup("payment_Record_Check_Cash_Dte", "CHECK-CASH-DTE", FieldType.STRING, 8);
        payment_Record_Original_Check_Nbr = payment_RecordRedef1.newFieldInGroup("payment_Record_Original_Check_Nbr", "ORIGINAL-CHECK-NBR", FieldType.STRING, 
            10);
        payment_Record_Eft_Effective_Date = payment_RecordRedef1.newFieldInGroup("payment_Record_Eft_Effective_Date", "EFT-EFFECTIVE-DATE", FieldType.STRING, 
            8);
        payment_Record_Apin = payment_RecordRedef1.newFieldInGroup("payment_Record_Apin", "APIN", FieldType.STRING, 7);
        payment_Record_Statement_Address = payment_RecordRedef1.newGroupInGroup("payment_Record_Statement_Address", "STATEMENT-ADDRESS");
        payment_Record_Sa_Pymnt_Name = payment_Record_Statement_Address.newFieldInGroup("payment_Record_Sa_Pymnt_Name", "SA-PYMNT-NAME", FieldType.STRING, 
            38);
        payment_Record_Sa_Addr_1 = payment_Record_Statement_Address.newFieldInGroup("payment_Record_Sa_Addr_1", "SA-ADDR-1", FieldType.STRING, 35);
        payment_Record_Sa_Addr_2 = payment_Record_Statement_Address.newFieldInGroup("payment_Record_Sa_Addr_2", "SA-ADDR-2", FieldType.STRING, 35);
        payment_Record_Sa_Addr_3 = payment_Record_Statement_Address.newFieldInGroup("payment_Record_Sa_Addr_3", "SA-ADDR-3", FieldType.STRING, 35);
        payment_Record_Sa_Addr_4 = payment_Record_Statement_Address.newFieldInGroup("payment_Record_Sa_Addr_4", "SA-ADDR-4", FieldType.STRING, 35);
        payment_Record_Sa_Addr_5 = payment_Record_Statement_Address.newFieldInGroup("payment_Record_Sa_Addr_5", "SA-ADDR-5", FieldType.STRING, 35);
        payment_Record_Sa_Addr_6 = payment_Record_Statement_Address.newFieldInGroup("payment_Record_Sa_Addr_6", "SA-ADDR-6", FieldType.STRING, 35);
        payment_Record_Contract_To = payment_RecordRedef1.newFieldInGroup("payment_Record_Contract_To", "CONTRACT-TO", FieldType.STRING, 10);
        payment_Record_Payment_Req_Type = payment_RecordRedef1.newFieldInGroup("payment_Record_Payment_Req_Type", "PAYMENT-REQ-TYPE", FieldType.NUMERIC, 
            1);
        payment_Record_Pay_Group = payment_RecordRedef1.newFieldInGroup("payment_Record_Pay_Group", "PAY-GROUP", FieldType.STRING, 6);
        payment_Record_Intl_Iban = payment_RecordRedef1.newFieldInGroup("payment_Record_Intl_Iban", "INTL-IBAN", FieldType.STRING, 35);
        payment_Record_Intl_Aba_Eft = payment_RecordRedef1.newFieldInGroup("payment_Record_Intl_Aba_Eft", "INTL-ABA-EFT", FieldType.STRING, 35);
        payment_Record_Reason_Code = payment_RecordRedef1.newFieldInGroup("payment_Record_Reason_Code", "REASON-CODE", FieldType.NUMERIC, 4);
        payment_Record_Reason_Description = payment_RecordRedef1.newFieldInGroup("payment_Record_Reason_Description", "REASON-DESCRIPTION", FieldType.STRING, 
            80);
        payment_Record_Country_Code = payment_RecordRedef1.newFieldInGroup("payment_Record_Country_Code", "COUNTRY-CODE", FieldType.STRING, 2);
        payment_Record_Iban_Ind = payment_RecordRedef1.newFieldInGroup("payment_Record_Iban_Ind", "IBAN-IND", FieldType.STRING, 1);
        filler01 = payment_RecordRedef1.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 439);
        payment_Record_Check_Number = payment_RecordRedef1.newFieldInGroup("payment_Record_Check_Number", "CHECK-NUMBER", FieldType.NUMERIC, 10);
        payment_Record_Check_NumberRedef4 = payment_RecordRedef1.newGroupInGroup("payment_Record_Check_NumberRedef4", "Redefines", payment_Record_Check_Number);
        payment_Record_Check_Alpha = payment_Record_Check_NumberRedef4.newFieldInGroup("payment_Record_Check_Alpha", "CHECK-ALPHA", FieldType.STRING, 
            10);

        this.setRecordName("LdaCpulpym2");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCpulpym2() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
