/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:17 PM
**        * FROM NATURAL LDA     : FCPL874T
************************************************************
**        * FILE NAME            : LdaFcpl874t.java
**        * CLASS NAME           : LdaFcpl874t
**        * INSTANCE NAME        : LdaFcpl874t
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl874t extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl803t;
    private DbsField pnd_Fcpl803t_Pnd_Pymnt_Hdr;
    private DbsField pnd_Fcpl803t_Pnd_Pymnt_Hdr_1;
    private DbsField pnd_Fcpl803t_Pnd_Dpi_Dci_Hdr;
    private DbsField pnd_Fcpl803t_Pnd_Ded_Hdr;
    private DbsField pnd_Fcpl803t_Pnd_Net_Hdr;
    private DbsField pnd_Fcpl803t_Pnd_Blank_Hdr_Line;

    public DbsGroup getPnd_Fcpl803t() { return pnd_Fcpl803t; }

    public DbsField getPnd_Fcpl803t_Pnd_Pymnt_Hdr() { return pnd_Fcpl803t_Pnd_Pymnt_Hdr; }

    public DbsField getPnd_Fcpl803t_Pnd_Pymnt_Hdr_1() { return pnd_Fcpl803t_Pnd_Pymnt_Hdr_1; }

    public DbsField getPnd_Fcpl803t_Pnd_Dpi_Dci_Hdr() { return pnd_Fcpl803t_Pnd_Dpi_Dci_Hdr; }

    public DbsField getPnd_Fcpl803t_Pnd_Ded_Hdr() { return pnd_Fcpl803t_Pnd_Ded_Hdr; }

    public DbsField getPnd_Fcpl803t_Pnd_Net_Hdr() { return pnd_Fcpl803t_Pnd_Net_Hdr; }

    public DbsField getPnd_Fcpl803t_Pnd_Blank_Hdr_Line() { return pnd_Fcpl803t_Pnd_Blank_Hdr_Line; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl803t = newGroupInRecord("pnd_Fcpl803t", "#FCPL803T");
        pnd_Fcpl803t_Pnd_Pymnt_Hdr = pnd_Fcpl803t.newFieldInGroup("pnd_Fcpl803t_Pnd_Pymnt_Hdr", "#PYMNT-HDR", FieldType.STRING, 7);
        pnd_Fcpl803t_Pnd_Pymnt_Hdr_1 = pnd_Fcpl803t.newFieldInGroup("pnd_Fcpl803t_Pnd_Pymnt_Hdr_1", "#PYMNT-HDR-1", FieldType.STRING, 4);
        pnd_Fcpl803t_Pnd_Dpi_Dci_Hdr = pnd_Fcpl803t.newFieldInGroup("pnd_Fcpl803t_Pnd_Dpi_Dci_Hdr", "#DPI-DCI-HDR", FieldType.STRING, 8);
        pnd_Fcpl803t_Pnd_Ded_Hdr = pnd_Fcpl803t.newFieldInGroup("pnd_Fcpl803t_Pnd_Ded_Hdr", "#DED-HDR", FieldType.STRING, 10);
        pnd_Fcpl803t_Pnd_Net_Hdr = pnd_Fcpl803t.newFieldArrayInGroup("pnd_Fcpl803t_Pnd_Net_Hdr", "#NET-HDR", FieldType.STRING, 15, new DbsArrayController(1,
            2));
        pnd_Fcpl803t_Pnd_Blank_Hdr_Line = pnd_Fcpl803t.newFieldArrayInGroup("pnd_Fcpl803t_Pnd_Blank_Hdr_Line", "#BLANK-HDR-LINE", FieldType.STRING, 2, 
            new DbsArrayController(1,3));

        this.setRecordName("LdaFcpl874t");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl803t_Pnd_Pymnt_Hdr.setInitialValue("Payment");
        pnd_Fcpl803t_Pnd_Pymnt_Hdr_1.setInitialValue("From");
        pnd_Fcpl803t_Pnd_Dpi_Dci_Hdr.setInitialValue("Interest");
        pnd_Fcpl803t_Pnd_Ded_Hdr.setInitialValue("Deductions");
        pnd_Fcpl803t_Pnd_Net_Hdr.getValue(1).setInitialValue("Net");
        pnd_Fcpl803t_Pnd_Net_Hdr.getValue(2).setInitialValue("Payment");
        pnd_Fcpl803t_Pnd_Blank_Hdr_Line.getValue(1).setInitialValue("14");
        pnd_Fcpl803t_Pnd_Blank_Hdr_Line.getValue(2).setInitialValue("4");
        pnd_Fcpl803t_Pnd_Blank_Hdr_Line.getValue(3).setInitialValue("15");
    }

    // Constructor
    public LdaFcpl874t() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
