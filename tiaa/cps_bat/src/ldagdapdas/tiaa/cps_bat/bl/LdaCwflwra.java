/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:57:12 PM
**        * FROM NATURAL LDA     : CWFLWRA
************************************************************
**        * FILE NAME            : LdaCwflwra.java
**        * CLASS NAME           : LdaCwflwra
**        * INSTANCE NAME        : LdaCwflwra
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwflwra extends DbsRecord
{
    // Properties
    private DbsGroup cwf_Corp_Rpt_Wr_Upd;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Rqst_Log_Dte_Tme_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Work_Prcss_Id;
    private DbsGroup cwf_Corp_Rpt_Wr_Upd_Work_Prcss_IdRedef1;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Work_Actn_Rqstd_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Work_Lob_Cmpny_Prdct_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Work_Mjr_Bsnss_Prcss_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Work_Spcfc_Bsnss_Prcss_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Pin_Nbr;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Part_Name;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Ssn;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Tiaa_Rcvd_Dte_Tme_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Tiaa_Rcvd_Dte_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Orgnl_Unit_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Rqst_Log_Oprtr_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Rqst_Orgn_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Crprte_Status_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Crprte_Clock_End_Dte_Tme_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Final_Close_Out_Dte_Tme_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Effctve_Dte_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Trans_Dte_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Trnsctn_Dte_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Owner_Unit_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Owner_Division;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Shphrd_Id;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Sec_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Admin_Work_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Sub_Rqst_Sqnce_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Prnt_Rqst_Log_Dte_Tme_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Parent_Wpid;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Multi_Rqst_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Cmplnt_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Elctrnc_Fldr_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Mj_Pull_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Check_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Bsnss_Reply_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Tlc_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Redo_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Crprte_On_Tme_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Off_Rtng_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Prcssng_Type_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Log_Rqstr_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Log_Insttn_Srce_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Instn_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Rqst_Instn_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Rqst_Rgn_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Rqst_Spcl_Dsgntn_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Rqst_Brnch_Cde;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Extrnl_Pend_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Crprte_Due_Dte_Tme_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Mis_Routed_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Dte_Of_Birth_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Trade_Dte_Tme_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Mj_Media_Ind;
    private DbsField cwf_Corp_Rpt_Wr_Upd_State_Of_Res;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Sec_Turnaround_Tme_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Sec_Updte_Dte_U;
    private DbsField cwf_Corp_Rpt_Wr_Upd_Rqst_Indicators;

    public DbsGroup getCwf_Corp_Rpt_Wr_Upd() { return cwf_Corp_Rpt_Wr_Upd; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Rqst_Log_Dte_Tme_U() { return cwf_Corp_Rpt_Wr_Upd_Rqst_Log_Dte_Tme_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Work_Prcss_Id() { return cwf_Corp_Rpt_Wr_Upd_Work_Prcss_Id; }

    public DbsGroup getCwf_Corp_Rpt_Wr_Upd_Work_Prcss_IdRedef1() { return cwf_Corp_Rpt_Wr_Upd_Work_Prcss_IdRedef1; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Work_Actn_Rqstd_Cde() { return cwf_Corp_Rpt_Wr_Upd_Work_Actn_Rqstd_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Work_Lob_Cmpny_Prdct_Cde() { return cwf_Corp_Rpt_Wr_Upd_Work_Lob_Cmpny_Prdct_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Work_Mjr_Bsnss_Prcss_Cde() { return cwf_Corp_Rpt_Wr_Upd_Work_Mjr_Bsnss_Prcss_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Work_Spcfc_Bsnss_Prcss_Cde() { return cwf_Corp_Rpt_Wr_Upd_Work_Spcfc_Bsnss_Prcss_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Pin_Nbr() { return cwf_Corp_Rpt_Wr_Upd_Pin_Nbr; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Part_Name() { return cwf_Corp_Rpt_Wr_Upd_Part_Name; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Ssn() { return cwf_Corp_Rpt_Wr_Upd_Ssn; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Tiaa_Rcvd_Dte_Tme_U() { return cwf_Corp_Rpt_Wr_Upd_Tiaa_Rcvd_Dte_Tme_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Tiaa_Rcvd_Dte_U() { return cwf_Corp_Rpt_Wr_Upd_Tiaa_Rcvd_Dte_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Orgnl_Unit_Cde() { return cwf_Corp_Rpt_Wr_Upd_Orgnl_Unit_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Rqst_Log_Oprtr_Cde() { return cwf_Corp_Rpt_Wr_Upd_Rqst_Log_Oprtr_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Rqst_Orgn_Cde() { return cwf_Corp_Rpt_Wr_Upd_Rqst_Orgn_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Crprte_Status_Ind() { return cwf_Corp_Rpt_Wr_Upd_Crprte_Status_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Crprte_Clock_End_Dte_Tme_U() { return cwf_Corp_Rpt_Wr_Upd_Crprte_Clock_End_Dte_Tme_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Final_Close_Out_Dte_Tme_U() { return cwf_Corp_Rpt_Wr_Upd_Final_Close_Out_Dte_Tme_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Effctve_Dte_U() { return cwf_Corp_Rpt_Wr_Upd_Effctve_Dte_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Trans_Dte_U() { return cwf_Corp_Rpt_Wr_Upd_Trans_Dte_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Trnsctn_Dte_U() { return cwf_Corp_Rpt_Wr_Upd_Trnsctn_Dte_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Owner_Unit_Cde() { return cwf_Corp_Rpt_Wr_Upd_Owner_Unit_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Owner_Division() { return cwf_Corp_Rpt_Wr_Upd_Owner_Division; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Shphrd_Id() { return cwf_Corp_Rpt_Wr_Upd_Shphrd_Id; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Sec_Ind() { return cwf_Corp_Rpt_Wr_Upd_Sec_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Admin_Work_Ind() { return cwf_Corp_Rpt_Wr_Upd_Admin_Work_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Sub_Rqst_Sqnce_Ind() { return cwf_Corp_Rpt_Wr_Upd_Sub_Rqst_Sqnce_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Prnt_Rqst_Log_Dte_Tme_U() { return cwf_Corp_Rpt_Wr_Upd_Prnt_Rqst_Log_Dte_Tme_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Parent_Wpid() { return cwf_Corp_Rpt_Wr_Upd_Parent_Wpid; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Multi_Rqst_Ind() { return cwf_Corp_Rpt_Wr_Upd_Multi_Rqst_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Cmplnt_Ind() { return cwf_Corp_Rpt_Wr_Upd_Cmplnt_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Elctrnc_Fldr_Ind() { return cwf_Corp_Rpt_Wr_Upd_Elctrnc_Fldr_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Mj_Pull_Ind() { return cwf_Corp_Rpt_Wr_Upd_Mj_Pull_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Check_Ind() { return cwf_Corp_Rpt_Wr_Upd_Check_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Bsnss_Reply_Ind() { return cwf_Corp_Rpt_Wr_Upd_Bsnss_Reply_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Tlc_Ind() { return cwf_Corp_Rpt_Wr_Upd_Tlc_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Redo_Ind() { return cwf_Corp_Rpt_Wr_Upd_Redo_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Crprte_On_Tme_Ind() { return cwf_Corp_Rpt_Wr_Upd_Crprte_On_Tme_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Off_Rtng_Ind() { return cwf_Corp_Rpt_Wr_Upd_Off_Rtng_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Prcssng_Type_Cde() { return cwf_Corp_Rpt_Wr_Upd_Prcssng_Type_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Log_Rqstr_Cde() { return cwf_Corp_Rpt_Wr_Upd_Log_Rqstr_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Log_Insttn_Srce_Cde() { return cwf_Corp_Rpt_Wr_Upd_Log_Insttn_Srce_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Instn_Cde() { return cwf_Corp_Rpt_Wr_Upd_Instn_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Rqst_Instn_Cde() { return cwf_Corp_Rpt_Wr_Upd_Rqst_Instn_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Rqst_Rgn_Cde() { return cwf_Corp_Rpt_Wr_Upd_Rqst_Rgn_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Rqst_Spcl_Dsgntn_Cde() { return cwf_Corp_Rpt_Wr_Upd_Rqst_Spcl_Dsgntn_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Rqst_Brnch_Cde() { return cwf_Corp_Rpt_Wr_Upd_Rqst_Brnch_Cde; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Extrnl_Pend_Ind() { return cwf_Corp_Rpt_Wr_Upd_Extrnl_Pend_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Crprte_Due_Dte_Tme_U() { return cwf_Corp_Rpt_Wr_Upd_Crprte_Due_Dte_Tme_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Mis_Routed_Ind() { return cwf_Corp_Rpt_Wr_Upd_Mis_Routed_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Dte_Of_Birth_U() { return cwf_Corp_Rpt_Wr_Upd_Dte_Of_Birth_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Trade_Dte_Tme_U() { return cwf_Corp_Rpt_Wr_Upd_Trade_Dte_Tme_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Mj_Media_Ind() { return cwf_Corp_Rpt_Wr_Upd_Mj_Media_Ind; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_State_Of_Res() { return cwf_Corp_Rpt_Wr_Upd_State_Of_Res; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Sec_Turnaround_Tme_U() { return cwf_Corp_Rpt_Wr_Upd_Sec_Turnaround_Tme_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Sec_Updte_Dte_U() { return cwf_Corp_Rpt_Wr_Upd_Sec_Updte_Dte_U; }

    public DbsField getCwf_Corp_Rpt_Wr_Upd_Rqst_Indicators() { return cwf_Corp_Rpt_Wr_Upd_Rqst_Indicators; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cwf_Corp_Rpt_Wr_Upd = newGroupInRecord("cwf_Corp_Rpt_Wr_Upd", "CWF-CORP-RPT-WR-UPD");
        cwf_Corp_Rpt_Wr_Upd_Rqst_Log_Dte_Tme_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Rqst_Log_Dte_Tme_U", "RQST-LOG-DTE-TME-U", FieldType.STRING, 
            26);
        cwf_Corp_Rpt_Wr_Upd_Work_Prcss_Id = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Work_Prcss_Id", "WORK-PRCSS-ID", FieldType.STRING, 
            6);
        cwf_Corp_Rpt_Wr_Upd_Work_Prcss_IdRedef1 = cwf_Corp_Rpt_Wr_Upd.newGroupInGroup("cwf_Corp_Rpt_Wr_Upd_Work_Prcss_IdRedef1", "Redefines", cwf_Corp_Rpt_Wr_Upd_Work_Prcss_Id);
        cwf_Corp_Rpt_Wr_Upd_Work_Actn_Rqstd_Cde = cwf_Corp_Rpt_Wr_Upd_Work_Prcss_IdRedef1.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Work_Actn_Rqstd_Cde", "WORK-ACTN-RQSTD-CDE", 
            FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_Work_Lob_Cmpny_Prdct_Cde = cwf_Corp_Rpt_Wr_Upd_Work_Prcss_IdRedef1.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Work_Lob_Cmpny_Prdct_Cde", 
            "WORK-LOB-CMPNY-PRDCT-CDE", FieldType.STRING, 2);
        cwf_Corp_Rpt_Wr_Upd_Work_Mjr_Bsnss_Prcss_Cde = cwf_Corp_Rpt_Wr_Upd_Work_Prcss_IdRedef1.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Work_Mjr_Bsnss_Prcss_Cde", 
            "WORK-MJR-BSNSS-PRCSS-CDE", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_Work_Spcfc_Bsnss_Prcss_Cde = cwf_Corp_Rpt_Wr_Upd_Work_Prcss_IdRedef1.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Work_Spcfc_Bsnss_Prcss_Cde", 
            "WORK-SPCFC-BSNSS-PRCSS-CDE", FieldType.STRING, 2);
        cwf_Corp_Rpt_Wr_Upd_Pin_Nbr = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 7);
        cwf_Corp_Rpt_Wr_Upd_Part_Name = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Part_Name", "PART-NAME", FieldType.STRING, 40);
        cwf_Corp_Rpt_Wr_Upd_Ssn = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Ssn", "SSN", FieldType.NUMERIC, 9);
        cwf_Corp_Rpt_Wr_Upd_Tiaa_Rcvd_Dte_Tme_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Tiaa_Rcvd_Dte_Tme_U", "TIAA-RCVD-DTE-TME-U", 
            FieldType.STRING, 24);
        cwf_Corp_Rpt_Wr_Upd_Tiaa_Rcvd_Dte_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Tiaa_Rcvd_Dte_U", "TIAA-RCVD-DTE-U", FieldType.STRING, 
            10);
        cwf_Corp_Rpt_Wr_Upd_Orgnl_Unit_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 
            8);
        cwf_Corp_Rpt_Wr_Upd_Rqst_Log_Oprtr_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Rqst_Log_Oprtr_Cde", "RQST-LOG-OPRTR-CDE", FieldType.STRING, 
            8);
        cwf_Corp_Rpt_Wr_Upd_Rqst_Orgn_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Crprte_Status_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Crprte_Status_Ind", "CRPRTE-STATUS-IND", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Crprte_Clock_End_Dte_Tme_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Crprte_Clock_End_Dte_Tme_U", "CRPRTE-CLOCK-END-DTE-TME-U", 
            FieldType.STRING, 24);
        cwf_Corp_Rpt_Wr_Upd_Final_Close_Out_Dte_Tme_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Final_Close_Out_Dte_Tme_U", "FINAL-CLOSE-OUT-DTE-TME-U", 
            FieldType.STRING, 24);
        cwf_Corp_Rpt_Wr_Upd_Effctve_Dte_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Effctve_Dte_U", "EFFCTVE-DTE-U", FieldType.STRING, 
            10);
        cwf_Corp_Rpt_Wr_Upd_Trans_Dte_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Trans_Dte_U", "TRANS-DTE-U", FieldType.STRING, 10);
        cwf_Corp_Rpt_Wr_Upd_Trnsctn_Dte_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Trnsctn_Dte_U", "TRNSCTN-DTE-U", FieldType.STRING, 
            10);
        cwf_Corp_Rpt_Wr_Upd_Owner_Unit_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Owner_Unit_Cde", "OWNER-UNIT-CDE", FieldType.STRING, 
            8);
        cwf_Corp_Rpt_Wr_Upd_Owner_Division = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Owner_Division", "OWNER-DIVISION", FieldType.STRING, 
            6);
        cwf_Corp_Rpt_Wr_Upd_Shphrd_Id = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Shphrd_Id", "SHPHRD-ID", FieldType.STRING, 8);
        cwf_Corp_Rpt_Wr_Upd_Sec_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Sec_Ind", "SEC-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_Admin_Work_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Admin_Work_Ind", "ADMIN-WORK-IND", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Sub_Rqst_Sqnce_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Sub_Rqst_Sqnce_Ind", "SUB-RQST-SQNCE-IND", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Prnt_Rqst_Log_Dte_Tme_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Prnt_Rqst_Log_Dte_Tme_U", "PRNT-RQST-LOG-DTE-TME-U", 
            FieldType.STRING, 24);
        cwf_Corp_Rpt_Wr_Upd_Parent_Wpid = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Parent_Wpid", "PARENT-WPID", FieldType.STRING, 6);
        cwf_Corp_Rpt_Wr_Upd_Multi_Rqst_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Multi_Rqst_Ind", "MULTI-RQST-IND", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Cmplnt_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Cmplnt_Ind", "CMPLNT-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_Elctrnc_Fldr_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Elctrnc_Fldr_Ind", "ELCTRNC-FLDR-IND", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Mj_Pull_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Mj_Pull_Ind", "MJ-PULL-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_Check_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Check_Ind", "CHECK-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_Bsnss_Reply_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Bsnss_Reply_Ind", "BSNSS-REPLY-IND", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Tlc_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Tlc_Ind", "TLC-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_Redo_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Redo_Ind", "REDO-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_Crprte_On_Tme_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Crprte_On_Tme_Ind", "CRPRTE-ON-TME-IND", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Off_Rtng_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Off_Rtng_Ind", "OFF-RTNG-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_Prcssng_Type_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Prcssng_Type_Cde", "PRCSSNG-TYPE-CDE", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Log_Rqstr_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Log_Rqstr_Cde", "LOG-RQSTR-CDE", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Log_Insttn_Srce_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Log_Insttn_Srce_Cde", "LOG-INSTTN-SRCE-CDE", 
            FieldType.STRING, 5);
        cwf_Corp_Rpt_Wr_Upd_Instn_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Instn_Cde", "INSTN-CDE", FieldType.STRING, 5);
        cwf_Corp_Rpt_Wr_Upd_Rqst_Instn_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Rqst_Instn_Cde", "RQST-INSTN-CDE", FieldType.STRING, 
            5);
        cwf_Corp_Rpt_Wr_Upd_Rqst_Rgn_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Rqst_Rgn_Cde", "RQST-RGN-CDE", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_Rqst_Spcl_Dsgntn_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Rqst_Spcl_Dsgntn_Cde", "RQST-SPCL-DSGNTN-CDE", 
            FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_Rqst_Brnch_Cde = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Rqst_Brnch_Cde", "RQST-BRNCH-CDE", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Extrnl_Pend_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Extrnl_Pend_Ind", "EXTRNL-PEND-IND", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Crprte_Due_Dte_Tme_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Crprte_Due_Dte_Tme_U", "CRPRTE-DUE-DTE-TME-U", 
            FieldType.STRING, 24);
        cwf_Corp_Rpt_Wr_Upd_Mis_Routed_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Mis_Routed_Ind", "MIS-ROUTED-IND", FieldType.STRING, 
            1);
        cwf_Corp_Rpt_Wr_Upd_Dte_Of_Birth_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Dte_Of_Birth_U", "DTE-OF-BIRTH-U", FieldType.STRING, 
            10);
        cwf_Corp_Rpt_Wr_Upd_Trade_Dte_Tme_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Trade_Dte_Tme_U", "TRADE-DTE-TME-U", FieldType.STRING, 
            24);
        cwf_Corp_Rpt_Wr_Upd_Mj_Media_Ind = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Mj_Media_Ind", "MJ-MEDIA-IND", FieldType.STRING, 1);
        cwf_Corp_Rpt_Wr_Upd_State_Of_Res = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_State_Of_Res", "STATE-OF-RES", FieldType.STRING, 2);
        cwf_Corp_Rpt_Wr_Upd_Sec_Turnaround_Tme_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Sec_Turnaround_Tme_U", "SEC-TURNAROUND-TME-U", 
            FieldType.STRING, 10);
        cwf_Corp_Rpt_Wr_Upd_Sec_Updte_Dte_U = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Sec_Updte_Dte_U", "SEC-UPDTE-DTE-U", FieldType.STRING, 
            10);
        cwf_Corp_Rpt_Wr_Upd_Rqst_Indicators = cwf_Corp_Rpt_Wr_Upd.newFieldInGroup("cwf_Corp_Rpt_Wr_Upd_Rqst_Indicators", "RQST-INDICATORS", FieldType.STRING, 
            25);

        this.setRecordName("LdaCwflwra");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwflwra() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
