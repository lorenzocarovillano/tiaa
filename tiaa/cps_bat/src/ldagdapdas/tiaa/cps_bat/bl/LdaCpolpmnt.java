/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:21 PM
**        * FROM NATURAL LDA     : CPOLPMNT
************************************************************
**        * FILE NAME            : LdaCpolpmnt.java
**        * CLASS NAME           : LdaCpolpmnt
**        * INSTANCE NAME        : LdaCpolpmnt
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpolpmnt extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_pymnt;
    private DbsField pymnt_Cntrct_Ppcn_Nbr;
    private DbsField pymnt_Cntrct_Payee_Cde;
    private DbsField pymnt_Cntrct_Invrse_Dte;
    private DbsField pymnt_Cntrct_Check_Crrncy_Cde;
    private DbsField pymnt_Cntrct_Crrncy_Cde;
    private DbsField pymnt_Cntrct_Orgn_Cde;
    private DbsField pymnt_Cntrct_Qlfied_Cde;
    private DbsField pymnt_Cntrct_Pymnt_Type_Ind;
    private DbsField pymnt_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pymnt_Cntrct_Sps_Cde;
    private DbsField pymnt_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pymnt_Pymnt_Stats_Cde;
    private DbsField pymnt_Pymnt_Annot_Ind;
    private DbsField pymnt_Pymnt_Cmbne_Ind;
    private DbsField pymnt_Pymnt_Ftre_Ind;
    private DbsField pymnt_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pymnt_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pymnt_Pymnt_Inst_Rep_Cde;
    private DbsField pymnt_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pymnt_Annt_Soc_Sec_Ind;
    private DbsField pymnt_Pymnt_Pay_Type_Req_Ind;
    private DbsField pymnt_Cntrct_Rqst_Settl_Dte;
    private DbsField pymnt_Cntrct_Rqst_Dte;
    private DbsField pymnt_Cntrct_Rqst_Settl_Tme;
    private DbsField pymnt_Cntrct_Unq_Id_Nbr;
    private DbsField pymnt_Cntrct_Type_Cde;
    private DbsField pymnt_Cntrct_Lob_Cde;
    private DbsField pymnt_Cntrct_Sub_Lob_Cde;
    private DbsField pymnt_Cntrct_Ia_Lob_Cde;
    private DbsField pymnt_Cntrct_Cref_Nbr;
    private DbsField pymnt_Cntrct_Option_Cde;
    private DbsField pymnt_Cntrct_Mode_Cde;
    private DbsField pymnt_Cntrct_Pymnt_Dest_Cde;
    private DbsField pymnt_Cntrct_Roll_Dest_Cde;
    private DbsField pymnt_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pymnt_Cntrct_Hold_Cde;
    private DbsField pymnt_Cntrct_Hold_Grp;
    private DbsField pymnt_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pymnt_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pymnt_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pymnt_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pymnt_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pymnt_Cntrct_Da_Cref_1_Nbr;
    private DbsField pymnt_Cntrct_Da_Cref_2_Nbr;
    private DbsField pymnt_Cntrct_Da_Cref_3_Nbr;
    private DbsField pymnt_Cntrct_Da_Cref_4_Nbr;
    private DbsField pymnt_Cntrct_Da_Cref_5_Nbr;
    private DbsField pymnt_Annt_Soc_Sec_Nbr;
    private DbsField pymnt_Annt_Ctznshp_Cde;
    private DbsField pymnt_Annt_Rsdncy_Cde;
    private DbsField pymnt_Pymnt_Split_Cde;
    private DbsField pymnt_Pymnt_Split_Reasn_Cde;
    private DbsField pymnt_Pymnt_Check_Dte;
    private DbsGroup pymnt_Pymnt_Check_DteRedef1;
    private DbsField pymnt_Pymnt_Dte;
    private DbsField pymnt_Pymnt_Cycle_Dte;
    private DbsField pymnt_Pymnt_Eft_Dte;
    private DbsField pymnt_Pymnt_Rqst_Pct;
    private DbsField pymnt_Pymnt_Rqst_Amt;
    private DbsField pymnt_Pymnt_Check_Nbr;
    private DbsField pymnt_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pymnt_Pymnt_Prcss_Seq_NbrRedef2;
    private DbsField pymnt_Pymnt_Prcss_Seq_Num;
    private DbsField pymnt_Pymnt_Instmt_Nbr;
    private DbsField pymnt_Pymnt_Check_Scrty_Nbr;
    private DbsField pymnt_Pymnt_Check_Amt;
    private DbsGroup pymnt_Pymnt_Check_AmtRedef3;
    private DbsField pymnt_Pymnt_Amt;
    private DbsField pymnt_Pymnt_Settlmnt_Dte;
    private DbsField pymnt_Pymnt_Check_Seq_Nbr;
    private DbsField pymnt_Count_Castinv_Acct;
    private DbsGroup pymnt_Inv_Acct;
    private DbsField pymnt_Inv_Acct_Cde;
    private DbsField pymnt_Inv_Acct_Unit_Qty;
    private DbsField pymnt_Inv_Acct_Unit_Value;
    private DbsField pymnt_Inv_Acct_Settl_Amt;
    private DbsField pymnt_Inv_Acct_Fed_Cde;
    private DbsField pymnt_Inv_Acct_State_Cde;
    private DbsField pymnt_Inv_Acct_Local_Cde;
    private DbsField pymnt_Inv_Acct_Ivc_Amt;
    private DbsField pymnt_Inv_Acct_Dci_Amt;
    private DbsField pymnt_Inv_Acct_Dpi_Amt;
    private DbsField pymnt_Inv_Acct_Start_Accum_Amt;
    private DbsField pymnt_Inv_Acct_End_Accum_Amt;
    private DbsField pymnt_Inv_Acct_Dvdnd_Amt;
    private DbsField pymnt_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pymnt_Inv_Acct_Ivc_Ind;
    private DbsField pymnt_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pymnt_Inv_Acct_Valuat_Period;
    private DbsField pymnt_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pymnt_Inv_Acct_State_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Local_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Exp_Amt;
    private DbsField pymnt_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pymnt_Cntrct_Cmbn_Nbr;
    private DbsField pymnt_Cntrct_Hold_Tme;
    private DbsField pymnt_Count_Castpymnt_Ded_Grp;
    private DbsGroup pymnt_Pymnt_Ded_Grp;
    private DbsField pymnt_Pymnt_Ded_Cde;
    private DbsField pymnt_Pymnt_Ded_Payee_Cde;
    private DbsField pymnt_Pymnt_Ded_Amt;
    private DbsField pymnt_Pymnt_Corp_Wpid;
    private DbsField pymnt_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pymnt_Annt_Locality_Cde;
    private DbsField pymnt_Count_Castinv_Rtb_Grp;
    private DbsGroup pymnt_Inv_Rtb_Grp;
    private DbsField pymnt_Inv_Rtb_Cde;
    private DbsField pymnt_Inv_Acct_Fed_Dci_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Sta_Dci_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Loc_Dci_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Fed_Dpi_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Sta_Dpi_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Loc_Dpi_Tax_Amt;
    private DbsField pymnt_Pymnt_Acctg_Dte;
    private DbsField pymnt_Pymnt_Intrfce_Dte;
    private DbsField pymnt_Pymnt_Tiaa_Md_Amt;
    private DbsField pymnt_Pymnt_Cref_Md_Amt;
    private DbsField pymnt_Pymnt_Tax_Form;
    private DbsField pymnt_Pymnt_Tax_Exempt_Ind;
    private DbsField pymnt_Pymnt_Tax_Calc_Cde;
    private DbsField pymnt_Pymnt_Method_Cde;
    private DbsField pymnt_Pymnt_Settl_Ivc_Ind;
    private DbsField pymnt_Pymnt_Ia_Issue_Dte;
    private DbsField pymnt_Pymnt_Spouse_Pay_Stats;
    private DbsField pymnt_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pymnt_Cntrct_Cancel_Rdrw_Amt;
    private DbsField pymnt_Cntrct_Hold_Ind;
    private DbsField pymnt_Notused2;
    private DbsField pymnt_Cnr_Cs_Rqust_Dte;
    private DbsField pymnt_Cnr_Cs_Rqust_Tme;
    private DbsField pymnt_Cnr_Cs_Rqust_User_Id;
    private DbsField pymnt_Cnr_Cs_Rqust_Term_Id;
    private DbsField pymnt_Notused1;
    private DbsField pymnt_Cnr_Rdrw_Rqust_Tme;
    private DbsField pymnt_Cnr_Rdrw_Rqust_User_Id;
    private DbsField pymnt_Cnr_Rdrw_Rqust_Term_Id;
    private DbsField pymnt_Cnr_Orgnl_Invrse_Dte;
    private DbsField pymnt_Cnr_Orgnl_Prcss_Seq_Nbr;
    private DbsField pymnt_Cnr_Rplcmnt_Invrse_Dte;
    private DbsField pymnt_Cnr_Rplcmnt_Prcss_Seq_Nbr;
    private DbsField pymnt_Cnr_Rdrw_Rqust_Dte;
    private DbsField pymnt_Cnr_Orgnl_Pymnt_Acctg_Dte;
    private DbsField pymnt_Cnr_Orgnl_Pymnt_Intrfce_Dte;
    private DbsField pymnt_Cnr_Cs_Actvty_Cde;
    private DbsField pymnt_Cnr_Cs_Pymnt_Intrfce_Dte;
    private DbsField pymnt_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField pymnt_Cnr_Cs_Pymnt_Check_Dte;
    private DbsField pymnt_Cnr_Cs_New_Stop_Ind;
    private DbsField pymnt_Pymnt_Rprnt_Pndng_Ind;
    private DbsField pymnt_Pymnt_Rprnt_Rqust_Dte;
    private DbsField pymnt_Pymnt_Rprnt_Dte;
    private DbsField pymnt_Cnr_Rdrw_Pymnt_Intrfce_Dte;
    private DbsField pymnt_Cnr_Rdrw_Pymnt_Acctg_Dte;
    private DbsField pymnt_Cnr_Rdrw_Pymnt_Check_Dte;
    private DbsField pymnt_Pymnt_Trigger_Cde;
    private DbsField pymnt_Pymnt_Spcl_Msg_Cde;
    private DbsField pymnt_Cntrct_Annty_Ins_Type;
    private DbsField pymnt_Cntrct_Annty_Type_Cde;
    private DbsField pymnt_Cntrct_Insurance_Option;
    private DbsField pymnt_Cntrct_Life_Contingency;
    private DbsField pymnt_Cntrct_Ac_Lt_10yrs;
    private DbsField pymnt_Tax_Fed_C_Resp_Cde;
    private DbsField pymnt_Tax_Fed_C_Filing_Stat;
    private DbsField pymnt_Tax_Fed_C_Allow_Cnt;
    private DbsField pymnt_Tax_Fed_C_Fixed_Amt;
    private DbsField pymnt_Tax_Fed_C_Fixed_Pct;
    private DbsField pymnt_Tax_Sta_C_Resp_Cde;
    private DbsField pymnt_Tax_Sta_C_Filing_Stat;
    private DbsField pymnt_Tax_Sta_C_Allow_Cnt;
    private DbsField pymnt_Tax_Sta_C_Fixed_Amt;
    private DbsField pymnt_Tax_Sta_C_Fixed_Pct;
    private DbsField pymnt_Tax_Sta_C_Spouse_Cnt;
    private DbsField pymnt_Tax_Sta_C_Blind_Cnt;
    private DbsField pymnt_Tax_Loc_C_Resp_Cde;
    private DbsField pymnt_Tax_Loc_C_Fixed_Amt;
    private DbsField pymnt_Tax_Loc_C_Fixed_Pct;
    private DbsField pymnt_Pymnt_Rtb_Dest_Typ;
    private DbsField pymnt_Invrse_Process_Dte;
    private DbsField pymnt_Pymnt_Nbr;
    private DbsField pymnt_Pymnt_Reqst_Nbr;
    private DbsGroup pymnt_Pymnt_Reqst_NbrRedef4;
    private DbsField pymnt_Pymnt_Orgn_Cde;
    private DbsField pymnt_Pymnt_Term_Id;
    private DbsField pymnt_Pymnt_User_Id;
    private DbsField pymnt_Pymnt_Seq_Nbr;
    private DbsField pymnt_Nbr_Of_Funds;
    private DbsField pymnt_Cntrct_Dvdnd_Amt;
    private DbsField pymnt_Cntrct_Settl_Amt;
    private DbsField pymnt_Cntrct_Net_Pymnt_Amt;
    private DbsField pymnt_Cntrct_Exp_Amt;
    private DbsField pymnt_Cntrct_Ivc_Amt;
    private DbsField pymnt_Cntrct_Dci_Amt;
    private DbsField pymnt_Cntrct_Dpi_Amt;
    private DbsField pymnt_Cntrct_Fdrl_Tax_Amt;
    private DbsField pymnt_Cntrct_State_Tax_Amt;
    private DbsField pymnt_Cntrct_Local_Tax_Amt;
    private DbsField pymnt_Cntrct_Adj_Ivc_Amt;
    private DbsField pymnt_Pymnt_Source_Cde;
    private DbsField pymnt_Pymnt_Transmission_Stats_Cde;
    private DbsField pymnt_Bank_Routing;
    private DbsField pymnt_Bank_Account;
    private DbsField pymnt_Bank_Transmission_Cde;
    private DbsField pymnt_Nbr_Of_Cmpnys;
    private DbsField pymnt_Pymnt_Instlmnt_Nbr;
    private DbsField pymnt_Plan_Cnt;
    private DbsField pymnt_Cntrct_Oia_Ind;
    private DbsField pymnt_Cnr_Orgnl_Reqst_Nbr;
    private DbsField pymnt_Cnr_Rplcmnt_Reqst_Nbr;
    private DbsField pymnt_Pymnt_Distribution;
    private DbsField pymnt_Cntrct_Asset_Chrg_Amt;
    private DbsField pymnt_Nbr_Of_Asset_Chrgs;
    private DbsField pymnt_Pymnt_Cycle_Nbr;
    private DbsField pymnt_Cnr_Orgnl_Pymnt_Nbr;
    private DbsField pymnt_Next_Pymnt_Due_Dte;
    private DbsField pymnt_Egtrra_Eligibility_Ind;
    private DbsGroup pymnt_Originating_Irc_CdeMuGroup;
    private DbsField pymnt_Originating_Irc_Cde;
    private DbsGroup pymnt_Originating_Irc_CdeRedef5;
    private DbsField pymnt_Distributing_Irc_Cde;
    private DbsField pymnt_Receiving_Irc_Cde;
    private DbsField pymnt_Cntrct_Sub_Lob;
    private DbsField pymnt_Cntrct_Ivc_Ind;
    private DbsField pymnt_Ir_Doc_Id;
    private DbsField pymnt_Advisor_Pin;
    private DbsField pymnt_Attention_Addr_Line;
    private DbsField pymnt_Ppcn_Inv_Orgn_Prcss_Inst;
    private DbsField pymnt_Unq_Inv_Orgn_Prcss_Inst;
    private DbsField pymnt_Chk_Dte_Org_Curr_Typ_Seq;
    private DbsField pymnt_Orgn_Status_Invrse_Seq;
    private DbsField pymnt_Check_Nbr_Stop_Ind;
    private DbsField pymnt_Cs_Intrfce_Pymnt_Dte_Check_Nbr;
    private DbsField pymnt_Cs_Acctg_Dte_Check_Nbr;
    private DbsField pymnt_Rdrw_Intrfce_Pymnt_Dte_Check_Nbr;
    private DbsField pymnt_Rdrw_Acctg_Dte_Check_Nbr;
    private DbsField pymnt_Org_Invrse_Seq_Rprnt_Ind;
    private DbsField pymnt_Pymnt_Reqst_Instmt;
    private DbsField pymnt_Pymnt_Stats_Type_Trans_Cde;
    private DbsField pymnt_Pymnt_Nbr_Stop_Ind;
    private DbsField pymnt_Orgn_Intrfce_Ptyp;
    private DbsField pymnt_Orgn_Acctg_Ptyp;

    public DataAccessProgramView getVw_pymnt() { return vw_pymnt; }

    public DbsField getPymnt_Cntrct_Ppcn_Nbr() { return pymnt_Cntrct_Ppcn_Nbr; }

    public DbsField getPymnt_Cntrct_Payee_Cde() { return pymnt_Cntrct_Payee_Cde; }

    public DbsField getPymnt_Cntrct_Invrse_Dte() { return pymnt_Cntrct_Invrse_Dte; }

    public DbsField getPymnt_Cntrct_Check_Crrncy_Cde() { return pymnt_Cntrct_Check_Crrncy_Cde; }

    public DbsField getPymnt_Cntrct_Crrncy_Cde() { return pymnt_Cntrct_Crrncy_Cde; }

    public DbsField getPymnt_Cntrct_Orgn_Cde() { return pymnt_Cntrct_Orgn_Cde; }

    public DbsField getPymnt_Cntrct_Qlfied_Cde() { return pymnt_Cntrct_Qlfied_Cde; }

    public DbsField getPymnt_Cntrct_Pymnt_Type_Ind() { return pymnt_Cntrct_Pymnt_Type_Ind; }

    public DbsField getPymnt_Cntrct_Sttlmnt_Type_Ind() { return pymnt_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getPymnt_Cntrct_Sps_Cde() { return pymnt_Cntrct_Sps_Cde; }

    public DbsField getPymnt_Pymnt_Rqst_Rmndr_Pct() { return pymnt_Pymnt_Rqst_Rmndr_Pct; }

    public DbsField getPymnt_Pymnt_Stats_Cde() { return pymnt_Pymnt_Stats_Cde; }

    public DbsField getPymnt_Pymnt_Annot_Ind() { return pymnt_Pymnt_Annot_Ind; }

    public DbsField getPymnt_Pymnt_Cmbne_Ind() { return pymnt_Pymnt_Cmbne_Ind; }

    public DbsField getPymnt_Pymnt_Ftre_Ind() { return pymnt_Pymnt_Ftre_Ind; }

    public DbsField getPymnt_Pymnt_Payee_Na_Addr_Trggr() { return pymnt_Pymnt_Payee_Na_Addr_Trggr; }

    public DbsField getPymnt_Pymnt_Payee_Tx_Elct_Trggr() { return pymnt_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getPymnt_Pymnt_Inst_Rep_Cde() { return pymnt_Pymnt_Inst_Rep_Cde; }

    public DbsField getPymnt_Cntrct_Dvdnd_Payee_Ind() { return pymnt_Cntrct_Dvdnd_Payee_Ind; }

    public DbsField getPymnt_Annt_Soc_Sec_Ind() { return pymnt_Annt_Soc_Sec_Ind; }

    public DbsField getPymnt_Pymnt_Pay_Type_Req_Ind() { return pymnt_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPymnt_Cntrct_Rqst_Settl_Dte() { return pymnt_Cntrct_Rqst_Settl_Dte; }

    public DbsField getPymnt_Cntrct_Rqst_Dte() { return pymnt_Cntrct_Rqst_Dte; }

    public DbsField getPymnt_Cntrct_Rqst_Settl_Tme() { return pymnt_Cntrct_Rqst_Settl_Tme; }

    public DbsField getPymnt_Cntrct_Unq_Id_Nbr() { return pymnt_Cntrct_Unq_Id_Nbr; }

    public DbsField getPymnt_Cntrct_Type_Cde() { return pymnt_Cntrct_Type_Cde; }

    public DbsField getPymnt_Cntrct_Lob_Cde() { return pymnt_Cntrct_Lob_Cde; }

    public DbsField getPymnt_Cntrct_Sub_Lob_Cde() { return pymnt_Cntrct_Sub_Lob_Cde; }

    public DbsField getPymnt_Cntrct_Ia_Lob_Cde() { return pymnt_Cntrct_Ia_Lob_Cde; }

    public DbsField getPymnt_Cntrct_Cref_Nbr() { return pymnt_Cntrct_Cref_Nbr; }

    public DbsField getPymnt_Cntrct_Option_Cde() { return pymnt_Cntrct_Option_Cde; }

    public DbsField getPymnt_Cntrct_Mode_Cde() { return pymnt_Cntrct_Mode_Cde; }

    public DbsField getPymnt_Cntrct_Pymnt_Dest_Cde() { return pymnt_Cntrct_Pymnt_Dest_Cde; }

    public DbsField getPymnt_Cntrct_Roll_Dest_Cde() { return pymnt_Cntrct_Roll_Dest_Cde; }

    public DbsField getPymnt_Cntrct_Dvdnd_Payee_Cde() { return pymnt_Cntrct_Dvdnd_Payee_Cde; }

    public DbsField getPymnt_Cntrct_Hold_Cde() { return pymnt_Cntrct_Hold_Cde; }

    public DbsField getPymnt_Cntrct_Hold_Grp() { return pymnt_Cntrct_Hold_Grp; }

    public DbsField getPymnt_Cntrct_Da_Tiaa_1_Nbr() { return pymnt_Cntrct_Da_Tiaa_1_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Tiaa_2_Nbr() { return pymnt_Cntrct_Da_Tiaa_2_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Tiaa_3_Nbr() { return pymnt_Cntrct_Da_Tiaa_3_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Tiaa_4_Nbr() { return pymnt_Cntrct_Da_Tiaa_4_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Tiaa_5_Nbr() { return pymnt_Cntrct_Da_Tiaa_5_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Cref_1_Nbr() { return pymnt_Cntrct_Da_Cref_1_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Cref_2_Nbr() { return pymnt_Cntrct_Da_Cref_2_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Cref_3_Nbr() { return pymnt_Cntrct_Da_Cref_3_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Cref_4_Nbr() { return pymnt_Cntrct_Da_Cref_4_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Cref_5_Nbr() { return pymnt_Cntrct_Da_Cref_5_Nbr; }

    public DbsField getPymnt_Annt_Soc_Sec_Nbr() { return pymnt_Annt_Soc_Sec_Nbr; }

    public DbsField getPymnt_Annt_Ctznshp_Cde() { return pymnt_Annt_Ctznshp_Cde; }

    public DbsField getPymnt_Annt_Rsdncy_Cde() { return pymnt_Annt_Rsdncy_Cde; }

    public DbsField getPymnt_Pymnt_Split_Cde() { return pymnt_Pymnt_Split_Cde; }

    public DbsField getPymnt_Pymnt_Split_Reasn_Cde() { return pymnt_Pymnt_Split_Reasn_Cde; }

    public DbsField getPymnt_Pymnt_Check_Dte() { return pymnt_Pymnt_Check_Dte; }

    public DbsGroup getPymnt_Pymnt_Check_DteRedef1() { return pymnt_Pymnt_Check_DteRedef1; }

    public DbsField getPymnt_Pymnt_Dte() { return pymnt_Pymnt_Dte; }

    public DbsField getPymnt_Pymnt_Cycle_Dte() { return pymnt_Pymnt_Cycle_Dte; }

    public DbsField getPymnt_Pymnt_Eft_Dte() { return pymnt_Pymnt_Eft_Dte; }

    public DbsField getPymnt_Pymnt_Rqst_Pct() { return pymnt_Pymnt_Rqst_Pct; }

    public DbsField getPymnt_Pymnt_Rqst_Amt() { return pymnt_Pymnt_Rqst_Amt; }

    public DbsField getPymnt_Pymnt_Check_Nbr() { return pymnt_Pymnt_Check_Nbr; }

    public DbsField getPymnt_Pymnt_Prcss_Seq_Nbr() { return pymnt_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPymnt_Pymnt_Prcss_Seq_NbrRedef2() { return pymnt_Pymnt_Prcss_Seq_NbrRedef2; }

    public DbsField getPymnt_Pymnt_Prcss_Seq_Num() { return pymnt_Pymnt_Prcss_Seq_Num; }

    public DbsField getPymnt_Pymnt_Instmt_Nbr() { return pymnt_Pymnt_Instmt_Nbr; }

    public DbsField getPymnt_Pymnt_Check_Scrty_Nbr() { return pymnt_Pymnt_Check_Scrty_Nbr; }

    public DbsField getPymnt_Pymnt_Check_Amt() { return pymnt_Pymnt_Check_Amt; }

    public DbsGroup getPymnt_Pymnt_Check_AmtRedef3() { return pymnt_Pymnt_Check_AmtRedef3; }

    public DbsField getPymnt_Pymnt_Amt() { return pymnt_Pymnt_Amt; }

    public DbsField getPymnt_Pymnt_Settlmnt_Dte() { return pymnt_Pymnt_Settlmnt_Dte; }

    public DbsField getPymnt_Pymnt_Check_Seq_Nbr() { return pymnt_Pymnt_Check_Seq_Nbr; }

    public DbsField getPymnt_Count_Castinv_Acct() { return pymnt_Count_Castinv_Acct; }

    public DbsGroup getPymnt_Inv_Acct() { return pymnt_Inv_Acct; }

    public DbsField getPymnt_Inv_Acct_Cde() { return pymnt_Inv_Acct_Cde; }

    public DbsField getPymnt_Inv_Acct_Unit_Qty() { return pymnt_Inv_Acct_Unit_Qty; }

    public DbsField getPymnt_Inv_Acct_Unit_Value() { return pymnt_Inv_Acct_Unit_Value; }

    public DbsField getPymnt_Inv_Acct_Settl_Amt() { return pymnt_Inv_Acct_Settl_Amt; }

    public DbsField getPymnt_Inv_Acct_Fed_Cde() { return pymnt_Inv_Acct_Fed_Cde; }

    public DbsField getPymnt_Inv_Acct_State_Cde() { return pymnt_Inv_Acct_State_Cde; }

    public DbsField getPymnt_Inv_Acct_Local_Cde() { return pymnt_Inv_Acct_Local_Cde; }

    public DbsField getPymnt_Inv_Acct_Ivc_Amt() { return pymnt_Inv_Acct_Ivc_Amt; }

    public DbsField getPymnt_Inv_Acct_Dci_Amt() { return pymnt_Inv_Acct_Dci_Amt; }

    public DbsField getPymnt_Inv_Acct_Dpi_Amt() { return pymnt_Inv_Acct_Dpi_Amt; }

    public DbsField getPymnt_Inv_Acct_Start_Accum_Amt() { return pymnt_Inv_Acct_Start_Accum_Amt; }

    public DbsField getPymnt_Inv_Acct_End_Accum_Amt() { return pymnt_Inv_Acct_End_Accum_Amt; }

    public DbsField getPymnt_Inv_Acct_Dvdnd_Amt() { return pymnt_Inv_Acct_Dvdnd_Amt; }

    public DbsField getPymnt_Inv_Acct_Net_Pymnt_Amt() { return pymnt_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getPymnt_Inv_Acct_Ivc_Ind() { return pymnt_Inv_Acct_Ivc_Ind; }

    public DbsField getPymnt_Inv_Acct_Adj_Ivc_Amt() { return pymnt_Inv_Acct_Adj_Ivc_Amt; }

    public DbsField getPymnt_Inv_Acct_Valuat_Period() { return pymnt_Inv_Acct_Valuat_Period; }

    public DbsField getPymnt_Inv_Acct_Fdrl_Tax_Amt() { return pymnt_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_State_Tax_Amt() { return pymnt_Inv_Acct_State_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Local_Tax_Amt() { return pymnt_Inv_Acct_Local_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Exp_Amt() { return pymnt_Inv_Acct_Exp_Amt; }

    public DbsField getPymnt_Cntrct_Ec_Oprtr_Id_Nbr() { return pymnt_Cntrct_Ec_Oprtr_Id_Nbr; }

    public DbsField getPymnt_Cntrct_Cmbn_Nbr() { return pymnt_Cntrct_Cmbn_Nbr; }

    public DbsField getPymnt_Cntrct_Hold_Tme() { return pymnt_Cntrct_Hold_Tme; }

    public DbsField getPymnt_Count_Castpymnt_Ded_Grp() { return pymnt_Count_Castpymnt_Ded_Grp; }

    public DbsGroup getPymnt_Pymnt_Ded_Grp() { return pymnt_Pymnt_Ded_Grp; }

    public DbsField getPymnt_Pymnt_Ded_Cde() { return pymnt_Pymnt_Ded_Cde; }

    public DbsField getPymnt_Pymnt_Ded_Payee_Cde() { return pymnt_Pymnt_Ded_Payee_Cde; }

    public DbsField getPymnt_Pymnt_Ded_Amt() { return pymnt_Pymnt_Ded_Amt; }

    public DbsField getPymnt_Pymnt_Corp_Wpid() { return pymnt_Pymnt_Corp_Wpid; }

    public DbsField getPymnt_Pymnt_Reqst_Log_Dte_Time() { return pymnt_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getPymnt_Annt_Locality_Cde() { return pymnt_Annt_Locality_Cde; }

    public DbsField getPymnt_Count_Castinv_Rtb_Grp() { return pymnt_Count_Castinv_Rtb_Grp; }

    public DbsGroup getPymnt_Inv_Rtb_Grp() { return pymnt_Inv_Rtb_Grp; }

    public DbsField getPymnt_Inv_Rtb_Cde() { return pymnt_Inv_Rtb_Cde; }

    public DbsField getPymnt_Inv_Acct_Fed_Dci_Tax_Amt() { return pymnt_Inv_Acct_Fed_Dci_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Sta_Dci_Tax_Amt() { return pymnt_Inv_Acct_Sta_Dci_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Loc_Dci_Tax_Amt() { return pymnt_Inv_Acct_Loc_Dci_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Fed_Dpi_Tax_Amt() { return pymnt_Inv_Acct_Fed_Dpi_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Sta_Dpi_Tax_Amt() { return pymnt_Inv_Acct_Sta_Dpi_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Loc_Dpi_Tax_Amt() { return pymnt_Inv_Acct_Loc_Dpi_Tax_Amt; }

    public DbsField getPymnt_Pymnt_Acctg_Dte() { return pymnt_Pymnt_Acctg_Dte; }

    public DbsField getPymnt_Pymnt_Intrfce_Dte() { return pymnt_Pymnt_Intrfce_Dte; }

    public DbsField getPymnt_Pymnt_Tiaa_Md_Amt() { return pymnt_Pymnt_Tiaa_Md_Amt; }

    public DbsField getPymnt_Pymnt_Cref_Md_Amt() { return pymnt_Pymnt_Cref_Md_Amt; }

    public DbsField getPymnt_Pymnt_Tax_Form() { return pymnt_Pymnt_Tax_Form; }

    public DbsField getPymnt_Pymnt_Tax_Exempt_Ind() { return pymnt_Pymnt_Tax_Exempt_Ind; }

    public DbsField getPymnt_Pymnt_Tax_Calc_Cde() { return pymnt_Pymnt_Tax_Calc_Cde; }

    public DbsField getPymnt_Pymnt_Method_Cde() { return pymnt_Pymnt_Method_Cde; }

    public DbsField getPymnt_Pymnt_Settl_Ivc_Ind() { return pymnt_Pymnt_Settl_Ivc_Ind; }

    public DbsField getPymnt_Pymnt_Ia_Issue_Dte() { return pymnt_Pymnt_Ia_Issue_Dte; }

    public DbsField getPymnt_Pymnt_Spouse_Pay_Stats() { return pymnt_Pymnt_Spouse_Pay_Stats; }

    public DbsField getPymnt_Cntrct_Cancel_Rdrw_Ind() { return pymnt_Cntrct_Cancel_Rdrw_Ind; }

    public DbsField getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPymnt_Cntrct_Cancel_Rdrw_Amt() { return pymnt_Cntrct_Cancel_Rdrw_Amt; }

    public DbsField getPymnt_Cntrct_Hold_Ind() { return pymnt_Cntrct_Hold_Ind; }

    public DbsField getPymnt_Notused2() { return pymnt_Notused2; }

    public DbsField getPymnt_Cnr_Cs_Rqust_Dte() { return pymnt_Cnr_Cs_Rqust_Dte; }

    public DbsField getPymnt_Cnr_Cs_Rqust_Tme() { return pymnt_Cnr_Cs_Rqust_Tme; }

    public DbsField getPymnt_Cnr_Cs_Rqust_User_Id() { return pymnt_Cnr_Cs_Rqust_User_Id; }

    public DbsField getPymnt_Cnr_Cs_Rqust_Term_Id() { return pymnt_Cnr_Cs_Rqust_Term_Id; }

    public DbsField getPymnt_Notused1() { return pymnt_Notused1; }

    public DbsField getPymnt_Cnr_Rdrw_Rqust_Tme() { return pymnt_Cnr_Rdrw_Rqust_Tme; }

    public DbsField getPymnt_Cnr_Rdrw_Rqust_User_Id() { return pymnt_Cnr_Rdrw_Rqust_User_Id; }

    public DbsField getPymnt_Cnr_Rdrw_Rqust_Term_Id() { return pymnt_Cnr_Rdrw_Rqust_Term_Id; }

    public DbsField getPymnt_Cnr_Orgnl_Invrse_Dte() { return pymnt_Cnr_Orgnl_Invrse_Dte; }

    public DbsField getPymnt_Cnr_Orgnl_Prcss_Seq_Nbr() { return pymnt_Cnr_Orgnl_Prcss_Seq_Nbr; }

    public DbsField getPymnt_Cnr_Rplcmnt_Invrse_Dte() { return pymnt_Cnr_Rplcmnt_Invrse_Dte; }

    public DbsField getPymnt_Cnr_Rplcmnt_Prcss_Seq_Nbr() { return pymnt_Cnr_Rplcmnt_Prcss_Seq_Nbr; }

    public DbsField getPymnt_Cnr_Rdrw_Rqust_Dte() { return pymnt_Cnr_Rdrw_Rqust_Dte; }

    public DbsField getPymnt_Cnr_Orgnl_Pymnt_Acctg_Dte() { return pymnt_Cnr_Orgnl_Pymnt_Acctg_Dte; }

    public DbsField getPymnt_Cnr_Orgnl_Pymnt_Intrfce_Dte() { return pymnt_Cnr_Orgnl_Pymnt_Intrfce_Dte; }

    public DbsField getPymnt_Cnr_Cs_Actvty_Cde() { return pymnt_Cnr_Cs_Actvty_Cde; }

    public DbsField getPymnt_Cnr_Cs_Pymnt_Intrfce_Dte() { return pymnt_Cnr_Cs_Pymnt_Intrfce_Dte; }

    public DbsField getPymnt_Cnr_Cs_Pymnt_Acctg_Dte() { return pymnt_Cnr_Cs_Pymnt_Acctg_Dte; }

    public DbsField getPymnt_Cnr_Cs_Pymnt_Check_Dte() { return pymnt_Cnr_Cs_Pymnt_Check_Dte; }

    public DbsField getPymnt_Cnr_Cs_New_Stop_Ind() { return pymnt_Cnr_Cs_New_Stop_Ind; }

    public DbsField getPymnt_Pymnt_Rprnt_Pndng_Ind() { return pymnt_Pymnt_Rprnt_Pndng_Ind; }

    public DbsField getPymnt_Pymnt_Rprnt_Rqust_Dte() { return pymnt_Pymnt_Rprnt_Rqust_Dte; }

    public DbsField getPymnt_Pymnt_Rprnt_Dte() { return pymnt_Pymnt_Rprnt_Dte; }

    public DbsField getPymnt_Cnr_Rdrw_Pymnt_Intrfce_Dte() { return pymnt_Cnr_Rdrw_Pymnt_Intrfce_Dte; }

    public DbsField getPymnt_Cnr_Rdrw_Pymnt_Acctg_Dte() { return pymnt_Cnr_Rdrw_Pymnt_Acctg_Dte; }

    public DbsField getPymnt_Cnr_Rdrw_Pymnt_Check_Dte() { return pymnt_Cnr_Rdrw_Pymnt_Check_Dte; }

    public DbsField getPymnt_Pymnt_Trigger_Cde() { return pymnt_Pymnt_Trigger_Cde; }

    public DbsField getPymnt_Pymnt_Spcl_Msg_Cde() { return pymnt_Pymnt_Spcl_Msg_Cde; }

    public DbsField getPymnt_Cntrct_Annty_Ins_Type() { return pymnt_Cntrct_Annty_Ins_Type; }

    public DbsField getPymnt_Cntrct_Annty_Type_Cde() { return pymnt_Cntrct_Annty_Type_Cde; }

    public DbsField getPymnt_Cntrct_Insurance_Option() { return pymnt_Cntrct_Insurance_Option; }

    public DbsField getPymnt_Cntrct_Life_Contingency() { return pymnt_Cntrct_Life_Contingency; }

    public DbsField getPymnt_Cntrct_Ac_Lt_10yrs() { return pymnt_Cntrct_Ac_Lt_10yrs; }

    public DbsField getPymnt_Tax_Fed_C_Resp_Cde() { return pymnt_Tax_Fed_C_Resp_Cde; }

    public DbsField getPymnt_Tax_Fed_C_Filing_Stat() { return pymnt_Tax_Fed_C_Filing_Stat; }

    public DbsField getPymnt_Tax_Fed_C_Allow_Cnt() { return pymnt_Tax_Fed_C_Allow_Cnt; }

    public DbsField getPymnt_Tax_Fed_C_Fixed_Amt() { return pymnt_Tax_Fed_C_Fixed_Amt; }

    public DbsField getPymnt_Tax_Fed_C_Fixed_Pct() { return pymnt_Tax_Fed_C_Fixed_Pct; }

    public DbsField getPymnt_Tax_Sta_C_Resp_Cde() { return pymnt_Tax_Sta_C_Resp_Cde; }

    public DbsField getPymnt_Tax_Sta_C_Filing_Stat() { return pymnt_Tax_Sta_C_Filing_Stat; }

    public DbsField getPymnt_Tax_Sta_C_Allow_Cnt() { return pymnt_Tax_Sta_C_Allow_Cnt; }

    public DbsField getPymnt_Tax_Sta_C_Fixed_Amt() { return pymnt_Tax_Sta_C_Fixed_Amt; }

    public DbsField getPymnt_Tax_Sta_C_Fixed_Pct() { return pymnt_Tax_Sta_C_Fixed_Pct; }

    public DbsField getPymnt_Tax_Sta_C_Spouse_Cnt() { return pymnt_Tax_Sta_C_Spouse_Cnt; }

    public DbsField getPymnt_Tax_Sta_C_Blind_Cnt() { return pymnt_Tax_Sta_C_Blind_Cnt; }

    public DbsField getPymnt_Tax_Loc_C_Resp_Cde() { return pymnt_Tax_Loc_C_Resp_Cde; }

    public DbsField getPymnt_Tax_Loc_C_Fixed_Amt() { return pymnt_Tax_Loc_C_Fixed_Amt; }

    public DbsField getPymnt_Tax_Loc_C_Fixed_Pct() { return pymnt_Tax_Loc_C_Fixed_Pct; }

    public DbsField getPymnt_Pymnt_Rtb_Dest_Typ() { return pymnt_Pymnt_Rtb_Dest_Typ; }

    public DbsField getPymnt_Invrse_Process_Dte() { return pymnt_Invrse_Process_Dte; }

    public DbsField getPymnt_Pymnt_Nbr() { return pymnt_Pymnt_Nbr; }

    public DbsField getPymnt_Pymnt_Reqst_Nbr() { return pymnt_Pymnt_Reqst_Nbr; }

    public DbsGroup getPymnt_Pymnt_Reqst_NbrRedef4() { return pymnt_Pymnt_Reqst_NbrRedef4; }

    public DbsField getPymnt_Pymnt_Orgn_Cde() { return pymnt_Pymnt_Orgn_Cde; }

    public DbsField getPymnt_Pymnt_Term_Id() { return pymnt_Pymnt_Term_Id; }

    public DbsField getPymnt_Pymnt_User_Id() { return pymnt_Pymnt_User_Id; }

    public DbsField getPymnt_Pymnt_Seq_Nbr() { return pymnt_Pymnt_Seq_Nbr; }

    public DbsField getPymnt_Nbr_Of_Funds() { return pymnt_Nbr_Of_Funds; }

    public DbsField getPymnt_Cntrct_Dvdnd_Amt() { return pymnt_Cntrct_Dvdnd_Amt; }

    public DbsField getPymnt_Cntrct_Settl_Amt() { return pymnt_Cntrct_Settl_Amt; }

    public DbsField getPymnt_Cntrct_Net_Pymnt_Amt() { return pymnt_Cntrct_Net_Pymnt_Amt; }

    public DbsField getPymnt_Cntrct_Exp_Amt() { return pymnt_Cntrct_Exp_Amt; }

    public DbsField getPymnt_Cntrct_Ivc_Amt() { return pymnt_Cntrct_Ivc_Amt; }

    public DbsField getPymnt_Cntrct_Dci_Amt() { return pymnt_Cntrct_Dci_Amt; }

    public DbsField getPymnt_Cntrct_Dpi_Amt() { return pymnt_Cntrct_Dpi_Amt; }

    public DbsField getPymnt_Cntrct_Fdrl_Tax_Amt() { return pymnt_Cntrct_Fdrl_Tax_Amt; }

    public DbsField getPymnt_Cntrct_State_Tax_Amt() { return pymnt_Cntrct_State_Tax_Amt; }

    public DbsField getPymnt_Cntrct_Local_Tax_Amt() { return pymnt_Cntrct_Local_Tax_Amt; }

    public DbsField getPymnt_Cntrct_Adj_Ivc_Amt() { return pymnt_Cntrct_Adj_Ivc_Amt; }

    public DbsField getPymnt_Pymnt_Source_Cde() { return pymnt_Pymnt_Source_Cde; }

    public DbsField getPymnt_Pymnt_Transmission_Stats_Cde() { return pymnt_Pymnt_Transmission_Stats_Cde; }

    public DbsField getPymnt_Bank_Routing() { return pymnt_Bank_Routing; }

    public DbsField getPymnt_Bank_Account() { return pymnt_Bank_Account; }

    public DbsField getPymnt_Bank_Transmission_Cde() { return pymnt_Bank_Transmission_Cde; }

    public DbsField getPymnt_Nbr_Of_Cmpnys() { return pymnt_Nbr_Of_Cmpnys; }

    public DbsField getPymnt_Pymnt_Instlmnt_Nbr() { return pymnt_Pymnt_Instlmnt_Nbr; }

    public DbsField getPymnt_Plan_Cnt() { return pymnt_Plan_Cnt; }

    public DbsField getPymnt_Cntrct_Oia_Ind() { return pymnt_Cntrct_Oia_Ind; }

    public DbsField getPymnt_Cnr_Orgnl_Reqst_Nbr() { return pymnt_Cnr_Orgnl_Reqst_Nbr; }

    public DbsField getPymnt_Cnr_Rplcmnt_Reqst_Nbr() { return pymnt_Cnr_Rplcmnt_Reqst_Nbr; }

    public DbsField getPymnt_Pymnt_Distribution() { return pymnt_Pymnt_Distribution; }

    public DbsField getPymnt_Cntrct_Asset_Chrg_Amt() { return pymnt_Cntrct_Asset_Chrg_Amt; }

    public DbsField getPymnt_Nbr_Of_Asset_Chrgs() { return pymnt_Nbr_Of_Asset_Chrgs; }

    public DbsField getPymnt_Pymnt_Cycle_Nbr() { return pymnt_Pymnt_Cycle_Nbr; }

    public DbsField getPymnt_Cnr_Orgnl_Pymnt_Nbr() { return pymnt_Cnr_Orgnl_Pymnt_Nbr; }

    public DbsField getPymnt_Next_Pymnt_Due_Dte() { return pymnt_Next_Pymnt_Due_Dte; }

    public DbsField getPymnt_Egtrra_Eligibility_Ind() { return pymnt_Egtrra_Eligibility_Ind; }

    public DbsGroup getPymnt_Originating_Irc_CdeMuGroup() { return pymnt_Originating_Irc_CdeMuGroup; }

    public DbsField getPymnt_Originating_Irc_Cde() { return pymnt_Originating_Irc_Cde; }

    public DbsGroup getPymnt_Originating_Irc_CdeRedef5() { return pymnt_Originating_Irc_CdeRedef5; }

    public DbsField getPymnt_Distributing_Irc_Cde() { return pymnt_Distributing_Irc_Cde; }

    public DbsField getPymnt_Receiving_Irc_Cde() { return pymnt_Receiving_Irc_Cde; }

    public DbsField getPymnt_Cntrct_Sub_Lob() { return pymnt_Cntrct_Sub_Lob; }

    public DbsField getPymnt_Cntrct_Ivc_Ind() { return pymnt_Cntrct_Ivc_Ind; }

    public DbsField getPymnt_Ir_Doc_Id() { return pymnt_Ir_Doc_Id; }

    public DbsField getPymnt_Advisor_Pin() { return pymnt_Advisor_Pin; }

    public DbsField getPymnt_Attention_Addr_Line() { return pymnt_Attention_Addr_Line; }

    public DbsField getPymnt_Ppcn_Inv_Orgn_Prcss_Inst() { return pymnt_Ppcn_Inv_Orgn_Prcss_Inst; }

    public DbsField getPymnt_Unq_Inv_Orgn_Prcss_Inst() { return pymnt_Unq_Inv_Orgn_Prcss_Inst; }

    public DbsField getPymnt_Chk_Dte_Org_Curr_Typ_Seq() { return pymnt_Chk_Dte_Org_Curr_Typ_Seq; }

    public DbsField getPymnt_Orgn_Status_Invrse_Seq() { return pymnt_Orgn_Status_Invrse_Seq; }

    public DbsField getPymnt_Check_Nbr_Stop_Ind() { return pymnt_Check_Nbr_Stop_Ind; }

    public DbsField getPymnt_Cs_Intrfce_Pymnt_Dte_Check_Nbr() { return pymnt_Cs_Intrfce_Pymnt_Dte_Check_Nbr; }

    public DbsField getPymnt_Cs_Acctg_Dte_Check_Nbr() { return pymnt_Cs_Acctg_Dte_Check_Nbr; }

    public DbsField getPymnt_Rdrw_Intrfce_Pymnt_Dte_Check_Nbr() { return pymnt_Rdrw_Intrfce_Pymnt_Dte_Check_Nbr; }

    public DbsField getPymnt_Rdrw_Acctg_Dte_Check_Nbr() { return pymnt_Rdrw_Acctg_Dte_Check_Nbr; }

    public DbsField getPymnt_Org_Invrse_Seq_Rprnt_Ind() { return pymnt_Org_Invrse_Seq_Rprnt_Ind; }

    public DbsField getPymnt_Pymnt_Reqst_Instmt() { return pymnt_Pymnt_Reqst_Instmt; }

    public DbsField getPymnt_Pymnt_Stats_Type_Trans_Cde() { return pymnt_Pymnt_Stats_Type_Trans_Cde; }

    public DbsField getPymnt_Pymnt_Nbr_Stop_Ind() { return pymnt_Pymnt_Nbr_Stop_Ind; }

    public DbsField getPymnt_Orgn_Intrfce_Ptyp() { return pymnt_Orgn_Intrfce_Ptyp; }

    public DbsField getPymnt_Orgn_Acctg_Ptyp() { return pymnt_Orgn_Acctg_Ptyp; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_pymnt = new DataAccessProgramView(new NameInfo("vw_pymnt", "PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PYMNT"));
        pymnt_Cntrct_Ppcn_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        pymnt_Cntrct_Payee_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        pymnt_Cntrct_Invrse_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_INVRSE_DTE");
        pymnt_Cntrct_Check_Crrncy_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CHECK_CRRNCY_CDE");
        pymnt_Cntrct_Crrncy_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_CRRNCY_CDE");
        pymnt_Cntrct_Orgn_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        pymnt_Cntrct_Qlfied_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_QLFIED_CDE");
        pymnt_Cntrct_Pymnt_Type_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNTRCT_PYMNT_TYPE_IND");
        pymnt_Cntrct_Sttlmnt_Type_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_STTLMNT_TYPE_IND");
        pymnt_Cntrct_Sps_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_SPS_CDE");
        pymnt_Pymnt_Rqst_Rmndr_Pct = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Rqst_Rmndr_Pct", "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_RQST_RMNDR_PCT");
        pymnt_Pymnt_Stats_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        pymnt_Pymnt_Annot_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_ANNOT_IND");
        pymnt_Pymnt_Cmbne_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CMBNE_IND");
        pymnt_Pymnt_Ftre_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_FTRE_IND");
        pymnt_Pymnt_Payee_Na_Addr_Trggr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Payee_Na_Addr_Trggr", "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_PAYEE_NA_ADDR_TRGGR");
        pymnt_Pymnt_Payee_Tx_Elct_Trggr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_PAYEE_TX_ELCT_TRGGR");
        pymnt_Pymnt_Inst_Rep_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Inst_Rep_Cde", "PYMNT-INST-REP-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_INST_REP_CDE");
        pymnt_Cntrct_Dvdnd_Payee_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Dvdnd_Payee_Ind", "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DVDND_PAYEE_IND");
        pymnt_Annt_Soc_Sec_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "ANNT_SOC_SEC_IND");
        pymnt_Pymnt_Pay_Type_Req_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        pymnt_Cntrct_Rqst_Settl_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Rqst_Settl_Dte", "CNTRCT-RQST-SETTL-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRCT_RQST_SETTL_DTE");
        pymnt_Cntrct_Rqst_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRCT_RQST_DTE");
        pymnt_Cntrct_Rqst_Settl_Tme = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Rqst_Settl_Tme", "CNTRCT-RQST-SETTL-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CNTRCT_RQST_SETTL_TME");
        pymnt_Cntrct_Unq_Id_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_UNQ_ID_NBR");
        pymnt_Cntrct_Type_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        pymnt_Cntrct_Lob_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_LOB_CDE");
        pymnt_Cntrct_Sub_Lob_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_SUB_LOB_CDE");
        pymnt_Cntrct_Ia_Lob_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_IA_LOB_CDE");
        pymnt_Cntrct_Cref_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_CREF_NBR");
        pymnt_Cntrct_Option_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTION_CDE");
        pymnt_Cntrct_Mode_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_CDE");
        pymnt_Cntrct_Pymnt_Dest_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "CNTRCT_PYMNT_DEST_CDE");
        pymnt_Cntrct_Roll_Dest_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_ROLL_DEST_CDE");
        pymnt_Cntrct_Dvdnd_Payee_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DVDND_PAYEE_CDE");
        pymnt_Cntrct_Hold_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_CDE");
        pymnt_Cntrct_Hold_Grp = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_GRP");
        pymnt_Cntrct_Da_Tiaa_1_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Tiaa_1_Nbr", "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_TIAA_1_NBR");
        pymnt_Cntrct_Da_Tiaa_2_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Tiaa_2_Nbr", "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_TIAA_2_NBR");
        pymnt_Cntrct_Da_Tiaa_3_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Tiaa_3_Nbr", "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_TIAA_3_NBR");
        pymnt_Cntrct_Da_Tiaa_4_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Tiaa_4_Nbr", "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_TIAA_4_NBR");
        pymnt_Cntrct_Da_Tiaa_5_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Tiaa_5_Nbr", "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_TIAA_5_NBR");
        pymnt_Cntrct_Da_Cref_1_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_CREF_1_NBR");
        pymnt_Cntrct_Da_Cref_2_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_CREF_2_NBR");
        pymnt_Cntrct_Da_Cref_3_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Cref_3_Nbr", "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_CREF_3_NBR");
        pymnt_Cntrct_Da_Cref_4_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Cref_4_Nbr", "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_CREF_4_NBR");
        pymnt_Cntrct_Da_Cref_5_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Cref_5_Nbr", "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_CREF_5_NBR");
        pymnt_Annt_Soc_Sec_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "ANNT_SOC_SEC_NBR");
        pymnt_Annt_Ctznshp_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ANNT_CTZNSHP_CDE");
        pymnt_Annt_Rsdncy_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ANNT_RSDNCY_CDE");
        pymnt_Pymnt_Split_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PYMNT_SPLIT_CDE");
        pymnt_Pymnt_Split_Reasn_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6, 
            RepeatingFieldStrategy.None, "PYMNT_SPLIT_REASN_CDE");
        pymnt_Pymnt_Check_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        pymnt_Pymnt_Check_DteRedef1 = vw_pymnt.getRecord().newGroupInGroup("pymnt_Pymnt_Check_DteRedef1", "Redefines", pymnt_Pymnt_Check_Dte);
        pymnt_Pymnt_Dte = pymnt_Pymnt_Check_DteRedef1.newFieldInGroup("pymnt_Pymnt_Dte", "PYMNT-DTE", FieldType.DATE);
        pymnt_Pymnt_Cycle_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CYCLE_DTE");
        pymnt_Pymnt_Eft_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_EFT_DTE");
        pymnt_Pymnt_Rqst_Pct = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", FieldType.PACKED_DECIMAL, 3, RepeatingFieldStrategy.None, 
            "PYMNT_RQST_PCT");
        pymnt_Pymnt_Rqst_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "PYMNT_RQST_AMT");
        pymnt_Pymnt_Check_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_NBR");
        pymnt_Pymnt_Prcss_Seq_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PYMNT_PRCSS_SEQ_NBR");
        pymnt_Pymnt_Prcss_Seq_NbrRedef2 = vw_pymnt.getRecord().newGroupInGroup("pymnt_Pymnt_Prcss_Seq_NbrRedef2", "Redefines", pymnt_Pymnt_Prcss_Seq_Nbr);
        pymnt_Pymnt_Prcss_Seq_Num = pymnt_Pymnt_Prcss_Seq_NbrRedef2.newFieldInGroup("pymnt_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pymnt_Pymnt_Instmt_Nbr = pymnt_Pymnt_Prcss_Seq_NbrRedef2.newFieldInGroup("pymnt_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pymnt_Pymnt_Check_Scrty_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SCRTY_NBR");
        pymnt_Pymnt_Check_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_AMT");
        pymnt_Pymnt_Check_AmtRedef3 = vw_pymnt.getRecord().newGroupInGroup("pymnt_Pymnt_Check_AmtRedef3", "Redefines", pymnt_Pymnt_Check_Amt);
        pymnt_Pymnt_Amt = pymnt_Pymnt_Check_AmtRedef3.newFieldInGroup("pymnt_Pymnt_Amt", "PYMNT-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pymnt_Pymnt_Settlmnt_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_SETTLMNT_DTE");
        pymnt_Pymnt_Check_Seq_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SEQ_NBR");
        pymnt_Count_Castinv_Acct = vw_pymnt.getRecord().newFieldInGroup("pymnt_Count_Castinv_Acct", "C*INV-ACCT", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, 
            "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct = vw_pymnt.getRecord().newGroupInGroup("pymnt_Inv_Acct", "INV-ACCT", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Cde = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2, new DbsArrayController(1,40) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_CDE", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Unit_Qty = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 9, 3, new 
            DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_UNIT_QTY", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Unit_Value = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 9, 
            4, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_UNIT_VALUE", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Settl_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_SETTL_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Fed_Cde = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 1, new DbsArrayController(1,40) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_FED_CDE", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_State_Cde = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", FieldType.STRING, 1, new DbsArrayController(1,40) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_STATE_CDE", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Local_Cde = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", FieldType.STRING, 1, new DbsArrayController(1,40) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LOCAL_CDE", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Ivc_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_IVC_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Dci_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_DCI_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Dpi_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_DPI_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Start_Accum_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_START_ACCUM_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_End_Accum_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_END_ACCUM_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Dvdnd_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_DVDND_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Net_Pymnt_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_NET_PYMNT_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Ivc_Ind = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 1, new DbsArrayController(1,40) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_IVC_IND", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Adj_Ivc_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_ADJ_IVC_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Valuat_Period = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 
            1, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_VALUAT_PERIOD", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Valuat_Period.setDdmHeader("RE-/VALUATION/PERIOD");
        pymnt_Inv_Acct_Fdrl_Tax_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_FDRL_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_State_Tax_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_STATE_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Local_Tax_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LOCAL_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Exp_Amt = pymnt_Inv_Acct.newFieldArrayInGroup("pymnt_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 9, 2, new 
            DbsArrayController(1,40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_EXP_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Cntrct_Ec_Oprtr_Id_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Ec_Oprtr_Id_Nbr", "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "CNTRCT_EC_OPRTR_ID_NBR");
        pymnt_Cntrct_Cmbn_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBN_NBR");
        pymnt_Cntrct_Hold_Tme = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_TME");
        pymnt_Count_Castpymnt_Ded_Grp = vw_pymnt.getRecord().newFieldInGroup("pymnt_Count_Castpymnt_Ded_Grp", "C*PYMNT-DED-GRP", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.CAsteriskVariable, "FCP_CONS_PYMT_PYMNT_DED_GRP");
        pymnt_Pymnt_Ded_Grp = vw_pymnt.getRecord().newGroupInGroup("pymnt_Pymnt_Ded_Grp", "PYMNT-DED-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_PYMNT_DED_GRP");
        pymnt_Pymnt_Ded_Cde = pymnt_Pymnt_Ded_Grp.newFieldArrayInGroup("pymnt_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3, new DbsArrayController(1,10) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_DED_CDE", "FCP_CONS_PYMT_PYMNT_DED_GRP");
        pymnt_Pymnt_Ded_Payee_Cde = pymnt_Pymnt_Ded_Grp.newFieldArrayInGroup("pymnt_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 8, 
            new DbsArrayController(1,10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_DED_PAYEE_CDE", "FCP_CONS_PYMT_PYMNT_DED_GRP");
        pymnt_Pymnt_Ded_Amt = pymnt_Pymnt_Ded_Grp.newFieldArrayInGroup("pymnt_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,10) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_DED_AMT", "FCP_CONS_PYMT_PYMNT_DED_GRP");
        pymnt_Pymnt_Corp_Wpid = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Corp_Wpid", "PYMNT-CORP-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PYMNT_CORP_WPID");
        pymnt_Pymnt_Reqst_Log_Dte_Time = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "PYMNT_REQST_LOG_DTE_TIME");
        pymnt_Annt_Locality_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ANNT_LOCALITY_CDE");
        pymnt_Count_Castinv_Rtb_Grp = vw_pymnt.getRecord().newFieldInGroup("pymnt_Count_Castinv_Rtb_Grp", "C*INV-RTB-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, 
            "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Rtb_Grp = vw_pymnt.getRecord().newGroupInGroup("pymnt_Inv_Rtb_Grp", "INV-RTB-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Rtb_Cde = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Rtb_Cde", "INV-RTB-CDE", FieldType.STRING, 1, new DbsArrayController(1,20) 
            , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_RTB_CDE", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Fed_Dci_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Fed_Dci_Tax_Amt", "INV-ACCT-FED-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_FED_DCI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Sta_Dci_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Sta_Dci_Tax_Amt", "INV-ACCT-STA-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_STA_DCI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Loc_Dci_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Loc_Dci_Tax_Amt", "INV-ACCT-LOC-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LOC_DCI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Fed_Dpi_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Fed_Dpi_Tax_Amt", "INV-ACCT-FED-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_FED_DPI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Sta_Dpi_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Sta_Dpi_Tax_Amt", "INV-ACCT-STA-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_STA_DPI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Loc_Dpi_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Loc_Dpi_Tax_Amt", "INV-ACCT-LOC-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LOC_DPI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Pymnt_Acctg_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_ACCTG_DTE");
        pymnt_Pymnt_Intrfce_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_INTRFCE_DTE");
        pymnt_Pymnt_Tiaa_Md_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Tiaa_Md_Amt", "PYMNT-TIAA-MD-AMT", FieldType.PACKED_DECIMAL, 11, 2, 
            RepeatingFieldStrategy.None, "PYMNT_TIAA_MD_AMT");
        pymnt_Pymnt_Cref_Md_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Cref_Md_Amt", "PYMNT-CREF-MD-AMT", FieldType.PACKED_DECIMAL, 11, 2, 
            RepeatingFieldStrategy.None, "PYMNT_CREF_MD_AMT");
        pymnt_Pymnt_Tax_Form = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Tax_Form", "PYMNT-TAX-FORM", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "PYMNT_TAX_FORM");
        pymnt_Pymnt_Tax_Exempt_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Tax_Exempt_Ind", "PYMNT-TAX-EXEMPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_TAX_EXEMPT_IND");
        pymnt_Pymnt_Tax_Calc_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Tax_Calc_Cde", "PYMNT-TAX-CALC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PYMNT_TAX_CALC_CDE");
        pymnt_Pymnt_Method_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Method_Cde", "PYMNT-METHOD-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_METHOD_CDE");
        pymnt_Pymnt_Settl_Ivc_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Settl_Ivc_Ind", "PYMNT-SETTL-IVC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_SETTL_IVC_IND");
        pymnt_Pymnt_Ia_Issue_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Ia_Issue_Dte", "PYMNT-IA-ISSUE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_IA_ISSUE_DTE");
        pymnt_Pymnt_Spouse_Pay_Stats = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_SPOUSE_PAY_STATS");
        pymnt_Cntrct_Cancel_Rdrw_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_IND");
        pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_ACTVTY_CDE");
        pymnt_Cntrct_Cancel_Rdrw_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cancel_Rdrw_Amt", "CNTRCT-CANCEL-RDRW-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_AMT");
        pymnt_Cntrct_Hold_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Hold_Ind", "CNTRCT-HOLD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_IND");
        pymnt_Notused2 = vw_pymnt.getRecord().newFieldInGroup("pymnt_Notused2", "NOTUSED2", FieldType.STRING, 8, RepeatingFieldStrategy.None, "NOTUSED2");
        pymnt_Cnr_Cs_Rqust_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_Rqust_Dte", "CNR-CS-RQUST-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNR_CS_RQUST_DTE");
        pymnt_Cnr_Cs_Rqust_Tme = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_Rqust_Tme", "CNR-CS-RQUST-TME", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "CNR_CS_RQUST_TME");
        pymnt_Cnr_Cs_Rqust_User_Id = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_Rqust_User_Id", "CNR-CS-RQUST-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNR_CS_RQUST_USER_ID");
        pymnt_Cnr_Cs_Rqust_Term_Id = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_Rqust_Term_Id", "CNR-CS-RQUST-TERM-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNR_CS_RQUST_TERM_ID");
        pymnt_Notused1 = vw_pymnt.getRecord().newFieldInGroup("pymnt_Notused1", "NOTUSED1", FieldType.STRING, 8, RepeatingFieldStrategy.None, "NOTUSED1");
        pymnt_Cnr_Rdrw_Rqust_Tme = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Rdrw_Rqust_Tme", "CNR-RDRW-RQUST-TME", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "CNR_RDRW_RQUST_TME");
        pymnt_Cnr_Rdrw_Rqust_User_Id = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Rdrw_Rqust_User_Id", "CNR-RDRW-RQUST-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNR_RDRW_RQUST_USER_ID");
        pymnt_Cnr_Rdrw_Rqust_Term_Id = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Rdrw_Rqust_Term_Id", "CNR-RDRW-RQUST-TERM-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNR_RDRW_RQUST_TERM_ID");
        pymnt_Cnr_Orgnl_Invrse_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "CNR_ORGNL_INVRSE_DTE");
        pymnt_Cnr_Orgnl_Prcss_Seq_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Orgnl_Prcss_Seq_Nbr", "CNR-ORGNL-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNR_ORGNL_PRCSS_SEQ_NBR");
        pymnt_Cnr_Rplcmnt_Invrse_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Rplcmnt_Invrse_Dte", "CNR-RPLCMNT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNR_RPLCMNT_INVRSE_DTE");
        pymnt_Cnr_Rplcmnt_Prcss_Seq_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Rplcmnt_Prcss_Seq_Nbr", "CNR-RPLCMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNR_RPLCMNT_PRCSS_SEQ_NBR");
        pymnt_Cnr_Rdrw_Rqust_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Rdrw_Rqust_Dte", "CNR-RDRW-RQUST-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNR_RDRW_RQUST_DTE");
        pymnt_Cnr_Orgnl_Pymnt_Acctg_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Orgnl_Pymnt_Acctg_Dte", "CNR-ORGNL-PYMNT-ACCTG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_ORGNL_PYMNT_ACCTG_DTE");
        pymnt_Cnr_Orgnl_Pymnt_Intrfce_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Orgnl_Pymnt_Intrfce_Dte", "CNR-ORGNL-PYMNT-INTRFCE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_ORGNL_PYMNT_INTRFCE_DTE");
        pymnt_Cnr_Cs_Actvty_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_Actvty_Cde", "CNR-CS-ACTVTY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNR_CS_ACTVTY_CDE");
        pymnt_Cnr_Cs_Pymnt_Intrfce_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_Pymnt_Intrfce_Dte", "CNR-CS-PYMNT-INTRFCE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_CS_PYMNT_INTRFCE_DTE");
        pymnt_Cnr_Cs_Pymnt_Acctg_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_CS_PYMNT_ACCTG_DTE");
        pymnt_Cnr_Cs_Pymnt_Check_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_Pymnt_Check_Dte", "CNR-CS-PYMNT-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_CS_PYMNT_CHECK_DTE");
        pymnt_Cnr_Cs_New_Stop_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_New_Stop_Ind", "CNR-CS-NEW-STOP-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNR_CS_NEW_STOP_IND");
        pymnt_Pymnt_Rprnt_Pndng_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Rprnt_Pndng_Ind", "PYMNT-RPRNT-PNDNG-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "PYMNT_RPRNT_PNDNG_IND");
        pymnt_Pymnt_Rprnt_Rqust_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Rprnt_Rqust_Dte", "PYMNT-RPRNT-RQUST-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_RPRNT_RQUST_DTE");
        pymnt_Pymnt_Rprnt_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Rprnt_Dte", "PYMNT-RPRNT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_RPRNT_DTE");
        pymnt_Cnr_Rdrw_Pymnt_Intrfce_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Rdrw_Pymnt_Intrfce_Dte", "CNR-RDRW-PYMNT-INTRFCE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_RDRW_PYMNT_INTRFCE_DTE");
        pymnt_Cnr_Rdrw_Pymnt_Acctg_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Rdrw_Pymnt_Acctg_Dte", "CNR-RDRW-PYMNT-ACCTG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_RDRW_PYMNT_ACCTG_DTE");
        pymnt_Cnr_Rdrw_Pymnt_Check_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Rdrw_Pymnt_Check_Dte", "CNR-RDRW-PYMNT-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_RDRW_PYMNT_CHECK_DTE");
        pymnt_Pymnt_Trigger_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Trigger_Cde", "PYMNT-TRIGGER-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_TRIGGER_CDE");
        pymnt_Pymnt_Spcl_Msg_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Spcl_Msg_Cde", "PYMNT-SPCL-MSG-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "PYMNT_SPCL_MSG_CDE");
        pymnt_Cntrct_Annty_Ins_Type = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_INS_TYPE");
        pymnt_Cntrct_Annty_Type_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_TYPE_CDE");
        pymnt_Cntrct_Insurance_Option = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_INSURANCE_OPTION");
        pymnt_Cntrct_Life_Contingency = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_LIFE_CONTINGENCY");
        pymnt_Cntrct_Ac_Lt_10yrs = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Ac_Lt_10yrs", "CNTRCT-AC-LT-10YRS", FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_AC_LT_10YRS");
        pymnt_Tax_Fed_C_Resp_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Fed_C_Resp_Cde", "TAX-FED-C-RESP-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TAX_FED_C_RESP_CDE");
        pymnt_Tax_Fed_C_Filing_Stat = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Fed_C_Filing_Stat", "TAX-FED-C-FILING-STAT", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TAX_FED_C_FILING_STAT");
        pymnt_Tax_Fed_C_Allow_Cnt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Fed_C_Allow_Cnt", "TAX-FED-C-ALLOW-CNT", FieldType.PACKED_DECIMAL, 
            2, RepeatingFieldStrategy.None, "TAX_FED_C_ALLOW_CNT");
        pymnt_Tax_Fed_C_Fixed_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Fed_C_Fixed_Amt", "TAX-FED-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TAX_FED_C_FIXED_AMT");
        pymnt_Tax_Fed_C_Fixed_Pct = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Fed_C_Fixed_Pct", "TAX-FED-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 
            7, 4, RepeatingFieldStrategy.None, "TAX_FED_C_FIXED_PCT");
        pymnt_Tax_Sta_C_Resp_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Sta_C_Resp_Cde", "TAX-STA-C-RESP-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TAX_STA_C_RESP_CDE");
        pymnt_Tax_Sta_C_Filing_Stat = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Sta_C_Filing_Stat", "TAX-STA-C-FILING-STAT", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "TAX_STA_C_FILING_STAT");
        pymnt_Tax_Sta_C_Allow_Cnt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Sta_C_Allow_Cnt", "TAX-STA-C-ALLOW-CNT", FieldType.PACKED_DECIMAL, 
            2, RepeatingFieldStrategy.None, "TAX_STA_C_ALLOW_CNT");
        pymnt_Tax_Sta_C_Fixed_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Sta_C_Fixed_Amt", "TAX-STA-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TAX_STA_C_FIXED_AMT");
        pymnt_Tax_Sta_C_Fixed_Pct = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Sta_C_Fixed_Pct", "TAX-STA-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 
            7, 4, RepeatingFieldStrategy.None, "TAX_STA_C_FIXED_PCT");
        pymnt_Tax_Sta_C_Spouse_Cnt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Sta_C_Spouse_Cnt", "TAX-STA-C-SPOUSE-CNT", FieldType.NUMERIC, 1, 
            RepeatingFieldStrategy.None, "TAX_STA_C_SPOUSE_CNT");
        pymnt_Tax_Sta_C_Blind_Cnt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Sta_C_Blind_Cnt", "TAX-STA-C-BLIND-CNT", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "TAX_STA_C_BLIND_CNT");
        pymnt_Tax_Loc_C_Resp_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Loc_C_Resp_Cde", "TAX-LOC-C-RESP-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TAX_LOC_C_RESP_CDE");
        pymnt_Tax_Loc_C_Fixed_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Loc_C_Fixed_Amt", "TAX-LOC-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TAX_LOC_C_FIXED_AMT");
        pymnt_Tax_Loc_C_Fixed_Pct = vw_pymnt.getRecord().newFieldInGroup("pymnt_Tax_Loc_C_Fixed_Pct", "TAX-LOC-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 
            7, 4, RepeatingFieldStrategy.None, "TAX_LOC_C_FIXED_PCT");
        pymnt_Pymnt_Rtb_Dest_Typ = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Rtb_Dest_Typ", "PYMNT-RTB-DEST-TYP", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "PYMNT_RTB_DEST_TYP");
        pymnt_Invrse_Process_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Invrse_Process_Dte", "INVRSE-PROCESS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "INVRSE_PROCESS_DTE");
        pymnt_Pymnt_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, "PYMNT_NBR");
        pymnt_Pymnt_Reqst_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");
        pymnt_Pymnt_Reqst_NbrRedef4 = vw_pymnt.getRecord().newGroupInGroup("pymnt_Pymnt_Reqst_NbrRedef4", "Redefines", pymnt_Pymnt_Reqst_Nbr);
        pymnt_Pymnt_Orgn_Cde = pymnt_Pymnt_Reqst_NbrRedef4.newFieldInGroup("pymnt_Pymnt_Orgn_Cde", "PYMNT-ORGN-CDE", FieldType.STRING, 4);
        pymnt_Pymnt_Term_Id = pymnt_Pymnt_Reqst_NbrRedef4.newFieldInGroup("pymnt_Pymnt_Term_Id", "PYMNT-TERM-ID", FieldType.STRING, 8);
        pymnt_Pymnt_User_Id = pymnt_Pymnt_Reqst_NbrRedef4.newFieldInGroup("pymnt_Pymnt_User_Id", "PYMNT-USER-ID", FieldType.STRING, 8);
        pymnt_Pymnt_Seq_Nbr = pymnt_Pymnt_Reqst_NbrRedef4.newFieldInGroup("pymnt_Pymnt_Seq_Nbr", "PYMNT-SEQ-NBR", FieldType.STRING, 15);
        pymnt_Nbr_Of_Funds = vw_pymnt.getRecord().newFieldInGroup("pymnt_Nbr_Of_Funds", "NBR-OF-FUNDS", FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, 
            "NBR_OF_FUNDS");
        pymnt_Cntrct_Dvdnd_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Dvdnd_Amt", "CNTRCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_DVDND_AMT");
        pymnt_Cntrct_Settl_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Settl_Amt", "CNTRCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_SETTL_AMT");
        pymnt_Cntrct_Net_Pymnt_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Net_Pymnt_Amt", "CNTRCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_NET_PYMNT_AMT");
        pymnt_Cntrct_Exp_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Exp_Amt", "CNTRCT-EXP-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_EXP_AMT");
        pymnt_Cntrct_Ivc_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_IVC_AMT");
        pymnt_Cntrct_Dci_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Dci_Amt", "CNTRCT-DCI-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_DCI_AMT");
        pymnt_Cntrct_Dpi_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Dpi_Amt", "CNTRCT-DPI-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_DPI_AMT");
        pymnt_Cntrct_Fdrl_Tax_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Fdrl_Tax_Amt", "CNTRCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_FDRL_TAX_AMT");
        pymnt_Cntrct_State_Tax_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        pymnt_Cntrct_Local_Tax_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        pymnt_Cntrct_Adj_Ivc_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Adj_Ivc_Amt", "CNTRCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 13, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ADJ_IVC_AMT");
        pymnt_Pymnt_Source_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Source_Cde", "PYMNT-SOURCE-CDE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "PYMNT_SOURCE_CDE");
        pymnt_Pymnt_Transmission_Stats_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Transmission_Stats_Cde", "PYMNT-TRANSMISSION-STATS-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PYMNT_TRANSMISSION_STATS_CDE");
        pymnt_Bank_Routing = vw_pymnt.getRecord().newFieldInGroup("pymnt_Bank_Routing", "BANK-ROUTING", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "BANK_ROUTING");
        pymnt_Bank_Account = vw_pymnt.getRecord().newFieldInGroup("pymnt_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21, RepeatingFieldStrategy.None, 
            "BANK_ACCOUNT");
        pymnt_Bank_Transmission_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Bank_Transmission_Cde", "BANK-TRANSMISSION-CDE", FieldType.STRING, 5, 
            RepeatingFieldStrategy.None, "BANK_TRANSMISSION_CDE");
        pymnt_Nbr_Of_Cmpnys = vw_pymnt.getRecord().newFieldInGroup("pymnt_Nbr_Of_Cmpnys", "NBR-OF-CMPNYS", FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, 
            "NBR_OF_CMPNYS");
        pymnt_Pymnt_Instlmnt_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "PYMNT_INSTLMNT_NBR");
        pymnt_Plan_Cnt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Plan_Cnt", "PLAN-CNT", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "PLAN_CNT");
        pymnt_Cntrct_Oia_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Oia_Ind", "CNTRCT-OIA-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_OIA_IND");
        pymnt_Cnr_Orgnl_Reqst_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Orgnl_Reqst_Nbr", "CNR-ORGNL-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "CNR_ORGNL_REQST_NBR");
        pymnt_Cnr_Rplcmnt_Reqst_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Rplcmnt_Reqst_Nbr", "CNR-RPLCMNT-REQST-NBR", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "CNR_RPLCMNT_REQST_NBR");
        pymnt_Pymnt_Distribution = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Distribution", "PYMNT-DISTRIBUTION", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "PYMNT_DISTRIBUTION");
        pymnt_Cntrct_Asset_Chrg_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Asset_Chrg_Amt", "CNTRCT-ASSET-CHRG-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_ASSET_CHRG_AMT");
        pymnt_Nbr_Of_Asset_Chrgs = vw_pymnt.getRecord().newFieldInGroup("pymnt_Nbr_Of_Asset_Chrgs", "NBR-OF-ASSET-CHRGS", FieldType.PACKED_DECIMAL, 5, 
            RepeatingFieldStrategy.None, "NBR_OF_ASSET_CHRGS");
        pymnt_Pymnt_Cycle_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Cycle_Nbr", "PYMNT-CYCLE-NBR", FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, 
            "PYMNT_CYCLE_NBR");
        pymnt_Cnr_Orgnl_Pymnt_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Orgnl_Pymnt_Nbr", "CNR-ORGNL-PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "CNR_ORGNL_PYMNT_NBR");
        pymnt_Next_Pymnt_Due_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Next_Pymnt_Due_Dte", "NEXT-PYMNT-DUE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "NEXT_PYMNT_DUE_DTE");
        pymnt_Egtrra_Eligibility_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Egtrra_Eligibility_Ind", "EGTRRA-ELIGIBILITY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "EGTRRA_ELIGIBILITY_IND");
        pymnt_Originating_Irc_CdeMuGroup = vw_pymnt.getRecord().newGroupInGroup("pymnt_Originating_Irc_CdeMuGroup", "ORIGINATING_IRC_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "FCP_CONS_PYMT_ORIGINATING_IRC_CDE");
        pymnt_Originating_Irc_Cde = pymnt_Originating_Irc_CdeMuGroup.newFieldArrayInGroup("pymnt_Originating_Irc_Cde", "ORIGINATING-IRC-CDE", FieldType.STRING, 
            2, new DbsArrayController(1,8), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ORIGINATING_IRC_CDE");
        pymnt_Originating_Irc_CdeRedef5 = vw_pymnt.getRecord().newGroupInGroup("pymnt_Originating_Irc_CdeRedef5", "Redefines", pymnt_Originating_Irc_Cde);
        pymnt_Distributing_Irc_Cde = pymnt_Originating_Irc_CdeRedef5.newFieldInGroup("pymnt_Distributing_Irc_Cde", "DISTRIBUTING-IRC-CDE", FieldType.STRING, 
            16);
        pymnt_Receiving_Irc_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Receiving_Irc_Cde", "RECEIVING-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "RECEIVING_IRC_CDE");
        pymnt_Cntrct_Sub_Lob = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Sub_Lob", "CNTRCT-SUB-LOB", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_SUB_LOB");
        pymnt_Cntrct_Ivc_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Ivc_Ind", "CNTRCT-IVC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_IVC_IND");
        pymnt_Ir_Doc_Id = vw_pymnt.getRecord().newFieldInGroup("pymnt_Ir_Doc_Id", "IR-DOC-ID", FieldType.STRING, 18, RepeatingFieldStrategy.None, "IR_DOC_ID");
        pymnt_Advisor_Pin = vw_pymnt.getRecord().newFieldInGroup("pymnt_Advisor_Pin", "ADVISOR-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "ADVISOR_PIN");
        pymnt_Attention_Addr_Line = vw_pymnt.getRecord().newFieldInGroup("pymnt_Attention_Addr_Line", "ATTENTION-ADDR-LINE", FieldType.STRING, 30, RepeatingFieldStrategy.None, 
            "ATTENTION_ADDR_LINE");
        pymnt_Ppcn_Inv_Orgn_Prcss_Inst = vw_pymnt.getRecord().newFieldInGroup("pymnt_Ppcn_Inv_Orgn_Prcss_Inst", "PPCN-INV-ORGN-PRCSS-INST", FieldType.STRING, 
            29, RepeatingFieldStrategy.None, "PPCN_INV_ORGN_PRCSS_INST");
        pymnt_Unq_Inv_Orgn_Prcss_Inst = vw_pymnt.getRecord().newFieldInGroup("pymnt_Unq_Inv_Orgn_Prcss_Inst", "UNQ-INV-ORGN-PRCSS-INST", FieldType.STRING, 
            31, RepeatingFieldStrategy.None, "UNQ_INV_ORGN_PRCSS_INST");
        pymnt_Chk_Dte_Org_Curr_Typ_Seq = vw_pymnt.getRecord().newFieldInGroup("pymnt_Chk_Dte_Org_Curr_Typ_Seq", "CHK-DTE-ORG-CURR-TYP-SEQ", FieldType.BINARY, 
            17, RepeatingFieldStrategy.None, "CHK_DTE_ORG_CURR_TYP_SEQ");
        pymnt_Orgn_Status_Invrse_Seq = vw_pymnt.getRecord().newFieldInGroup("pymnt_Orgn_Status_Invrse_Seq", "ORGN-STATUS-INVRSE-SEQ", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "ORGN_STATUS_INVRSE_SEQ");
        pymnt_Check_Nbr_Stop_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Check_Nbr_Stop_Ind", "CHECK-NBR-STOP-IND", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CHECK_NBR_STOP_IND");
        pymnt_Cs_Intrfce_Pymnt_Dte_Check_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cs_Intrfce_Pymnt_Dte_Check_Nbr", "CS-INTRFCE-PYMNT-DTE-CHECK-NBR", 
            FieldType.BINARY, 15, RepeatingFieldStrategy.None, "CS_INTRFCE_PYMNT_DTE_CHECK_NBR");
        pymnt_Cs_Acctg_Dte_Check_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cs_Acctg_Dte_Check_Nbr", "CS-ACCTG-DTE-CHECK-NBR", FieldType.BINARY, 
            11, RepeatingFieldStrategy.None, "CS_ACCTG_DTE_CHECK_NBR");
        pymnt_Rdrw_Intrfce_Pymnt_Dte_Check_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Rdrw_Intrfce_Pymnt_Dte_Check_Nbr", "RDRW-INTRFCE-PYMNT-DTE-CHECK-NBR", 
            FieldType.BINARY, 15, RepeatingFieldStrategy.None, "RDRW_INTRFCE_PYMNT_DTE_CHECK_NBR");
        pymnt_Rdrw_Acctg_Dte_Check_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Rdrw_Acctg_Dte_Check_Nbr", "RDRW-ACCTG-DTE-CHECK-NBR", FieldType.BINARY, 
            11, RepeatingFieldStrategy.None, "RDRW_ACCTG_DTE_CHECK_NBR");
        pymnt_Org_Invrse_Seq_Rprnt_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Org_Invrse_Seq_Rprnt_Ind", "ORG-INVRSE-SEQ-RPRNT-IND", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "ORG_INVRSE_SEQ_RPRNT_IND");
        pymnt_Pymnt_Reqst_Instmt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Reqst_Instmt", "PYMNT-REQST-INSTMT", FieldType.STRING, 37, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_INSTMT");
        pymnt_Pymnt_Stats_Type_Trans_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Stats_Type_Trans_Cde", "PYMNT-STATS-TYPE-TRANS-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PYMNT_STATS_TYPE_TRANS_CDE");
        pymnt_Pymnt_Nbr_Stop_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Nbr_Stop_Ind", "PYMNT-NBR-STOP-IND", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "PYMNT_NBR_STOP_IND");
        pymnt_Orgn_Intrfce_Ptyp = vw_pymnt.getRecord().newFieldInGroup("pymnt_Orgn_Intrfce_Ptyp", "ORGN-INTRFCE-PTYP", FieldType.BINARY, 7, RepeatingFieldStrategy.None, 
            "ORGN_INTRFCE_PTYP");
        pymnt_Orgn_Acctg_Ptyp = vw_pymnt.getRecord().newFieldInGroup("pymnt_Orgn_Acctg_Ptyp", "ORGN-ACCTG-PTYP", FieldType.BINARY, 7, RepeatingFieldStrategy.None, 
            "ORGN_ACCTG_PTYP");
        vw_pymnt.setUniquePeList();

        this.setRecordName("LdaCpolpmnt");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_pymnt.reset();
    }

    // Constructor
    public LdaCpolpmnt() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
