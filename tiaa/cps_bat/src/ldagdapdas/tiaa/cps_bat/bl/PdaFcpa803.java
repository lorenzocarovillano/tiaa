/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:27 PM
**        * FROM NATURAL PDA     : FCPA803
************************************************************
**        * FILE NAME            : PdaFcpa803.java
**        * CLASS NAME           : PdaFcpa803
**        * INSTANCE NAME        : PdaFcpa803
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa803 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpa803;
    private DbsField pnd_Fcpa803_Cntrct_Orgn_Cde;
    private DbsField pnd_Fcpa803_Pnd_Output_Rec;
    private DbsGroup pnd_Fcpa803_Pnd_Output_RecRedef1;
    private DbsField pnd_Fcpa803_Pnd_Cc;
    private DbsField pnd_Fcpa803_Pnd_Font;
    private DbsField pnd_Fcpa803_Pnd_Output_Rec_Detail;
    private DbsGroup pnd_Fcpa803_Pnd_Output_Rec_DetailRedef2;
    private DbsField pnd_Fcpa803_Pnd_Output_Col;
    private DbsGroup pnd_Fcpa803_Pnd_Grand_Totals;
    private DbsField pnd_Fcpa803_Pnd_Grand_Settl_Amt;
    private DbsField pnd_Fcpa803_Pnd_Grand_Dpi_Amt;
    private DbsField pnd_Fcpa803_Pnd_Grand_Ded_Amt;
    private DbsField pnd_Fcpa803_Pnd_Grand_Net_Amt;
    private DbsField pnd_Fcpa803_Pnd_Record_In_Pymnt;
    private DbsField pnd_Fcpa803_Pnd_Prev_Xerox_Sub;
    private DbsField pnd_Fcpa803_Pnd_Xerox_Sub;
    private DbsField pnd_Fcpa803_Pnd_Current_Page;
    private DbsField pnd_Fcpa803_Pnd_Addr_Ind;
    private DbsField pnd_Fcpa803_Pnd_Stmnt_Text_Ind;
    private DbsField pnd_Fcpa803_Pnd_Bar_Last_Page;
    private DbsField pnd_Fcpa803_Pnd_Dpi_Col;
    private DbsField pnd_Fcpa803_Pnd_Ded_Col;
    private DbsField pnd_Fcpa803_Pnd_Net_Col;
    private DbsField pnd_Fcpa803_Pnd_Simplex;
    private DbsField pnd_Fcpa803_Pnd_Full_Xerox;
    private DbsField pnd_Fcpa803_Pnd_Separator_Page;
    private DbsField pnd_Fcpa803_Pnd_Csr_Run;
    private DbsGroup pnd_Fcpa803_Pnd_Stmnt_Type;
    private DbsField pnd_Fcpa803_Pnd_Zero_Check;
    private DbsField pnd_Fcpa803_Pnd_Eft;
    private DbsField pnd_Fcpa803_Pnd_Global_Pay;
    private DbsField pnd_Fcpa803_Pnd_Check_To_Annt;
    private DbsField pnd_Fcpa803_Pnd_Stmnt_To_Annt;
    private DbsField pnd_Fcpa803_Pnd_Check;
    private DbsField pnd_Fcpa803_Pnd_Stmnt;
    private DbsField pnd_Fcpa803_Pnd_New_Pymnt;
    private DbsField pnd_Fcpa803_Pnd_Newpage_Ind;
    private DbsField pnd_Fcpa803_Pnd_Company_Break;
    private DbsField pnd_Fcpa803_Pnd_Ppcn_Break;
    private DbsField pnd_Fcpa803_Pnd_Odd_Page;
    private DbsField pnd_Fcpa803_Pnd_End_Of_Pymnt;
    private DbsField pnd_Fcpa803_Pnd_Roth_Ind;

    public DbsGroup getPnd_Fcpa803() { return pnd_Fcpa803; }

    public DbsField getPnd_Fcpa803_Cntrct_Orgn_Cde() { return pnd_Fcpa803_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Fcpa803_Pnd_Output_Rec() { return pnd_Fcpa803_Pnd_Output_Rec; }

    public DbsGroup getPnd_Fcpa803_Pnd_Output_RecRedef1() { return pnd_Fcpa803_Pnd_Output_RecRedef1; }

    public DbsField getPnd_Fcpa803_Pnd_Cc() { return pnd_Fcpa803_Pnd_Cc; }

    public DbsField getPnd_Fcpa803_Pnd_Font() { return pnd_Fcpa803_Pnd_Font; }

    public DbsField getPnd_Fcpa803_Pnd_Output_Rec_Detail() { return pnd_Fcpa803_Pnd_Output_Rec_Detail; }

    public DbsGroup getPnd_Fcpa803_Pnd_Output_Rec_DetailRedef2() { return pnd_Fcpa803_Pnd_Output_Rec_DetailRedef2; }

    public DbsField getPnd_Fcpa803_Pnd_Output_Col() { return pnd_Fcpa803_Pnd_Output_Col; }

    public DbsGroup getPnd_Fcpa803_Pnd_Grand_Totals() { return pnd_Fcpa803_Pnd_Grand_Totals; }

    public DbsField getPnd_Fcpa803_Pnd_Grand_Settl_Amt() { return pnd_Fcpa803_Pnd_Grand_Settl_Amt; }

    public DbsField getPnd_Fcpa803_Pnd_Grand_Dpi_Amt() { return pnd_Fcpa803_Pnd_Grand_Dpi_Amt; }

    public DbsField getPnd_Fcpa803_Pnd_Grand_Ded_Amt() { return pnd_Fcpa803_Pnd_Grand_Ded_Amt; }

    public DbsField getPnd_Fcpa803_Pnd_Grand_Net_Amt() { return pnd_Fcpa803_Pnd_Grand_Net_Amt; }

    public DbsField getPnd_Fcpa803_Pnd_Record_In_Pymnt() { return pnd_Fcpa803_Pnd_Record_In_Pymnt; }

    public DbsField getPnd_Fcpa803_Pnd_Prev_Xerox_Sub() { return pnd_Fcpa803_Pnd_Prev_Xerox_Sub; }

    public DbsField getPnd_Fcpa803_Pnd_Xerox_Sub() { return pnd_Fcpa803_Pnd_Xerox_Sub; }

    public DbsField getPnd_Fcpa803_Pnd_Current_Page() { return pnd_Fcpa803_Pnd_Current_Page; }

    public DbsField getPnd_Fcpa803_Pnd_Addr_Ind() { return pnd_Fcpa803_Pnd_Addr_Ind; }

    public DbsField getPnd_Fcpa803_Pnd_Stmnt_Text_Ind() { return pnd_Fcpa803_Pnd_Stmnt_Text_Ind; }

    public DbsField getPnd_Fcpa803_Pnd_Bar_Last_Page() { return pnd_Fcpa803_Pnd_Bar_Last_Page; }

    public DbsField getPnd_Fcpa803_Pnd_Dpi_Col() { return pnd_Fcpa803_Pnd_Dpi_Col; }

    public DbsField getPnd_Fcpa803_Pnd_Ded_Col() { return pnd_Fcpa803_Pnd_Ded_Col; }

    public DbsField getPnd_Fcpa803_Pnd_Net_Col() { return pnd_Fcpa803_Pnd_Net_Col; }

    public DbsField getPnd_Fcpa803_Pnd_Simplex() { return pnd_Fcpa803_Pnd_Simplex; }

    public DbsField getPnd_Fcpa803_Pnd_Full_Xerox() { return pnd_Fcpa803_Pnd_Full_Xerox; }

    public DbsField getPnd_Fcpa803_Pnd_Separator_Page() { return pnd_Fcpa803_Pnd_Separator_Page; }

    public DbsField getPnd_Fcpa803_Pnd_Csr_Run() { return pnd_Fcpa803_Pnd_Csr_Run; }

    public DbsGroup getPnd_Fcpa803_Pnd_Stmnt_Type() { return pnd_Fcpa803_Pnd_Stmnt_Type; }

    public DbsField getPnd_Fcpa803_Pnd_Zero_Check() { return pnd_Fcpa803_Pnd_Zero_Check; }

    public DbsField getPnd_Fcpa803_Pnd_Eft() { return pnd_Fcpa803_Pnd_Eft; }

    public DbsField getPnd_Fcpa803_Pnd_Global_Pay() { return pnd_Fcpa803_Pnd_Global_Pay; }

    public DbsField getPnd_Fcpa803_Pnd_Check_To_Annt() { return pnd_Fcpa803_Pnd_Check_To_Annt; }

    public DbsField getPnd_Fcpa803_Pnd_Stmnt_To_Annt() { return pnd_Fcpa803_Pnd_Stmnt_To_Annt; }

    public DbsField getPnd_Fcpa803_Pnd_Check() { return pnd_Fcpa803_Pnd_Check; }

    public DbsField getPnd_Fcpa803_Pnd_Stmnt() { return pnd_Fcpa803_Pnd_Stmnt; }

    public DbsField getPnd_Fcpa803_Pnd_New_Pymnt() { return pnd_Fcpa803_Pnd_New_Pymnt; }

    public DbsField getPnd_Fcpa803_Pnd_Newpage_Ind() { return pnd_Fcpa803_Pnd_Newpage_Ind; }

    public DbsField getPnd_Fcpa803_Pnd_Company_Break() { return pnd_Fcpa803_Pnd_Company_Break; }

    public DbsField getPnd_Fcpa803_Pnd_Ppcn_Break() { return pnd_Fcpa803_Pnd_Ppcn_Break; }

    public DbsField getPnd_Fcpa803_Pnd_Odd_Page() { return pnd_Fcpa803_Pnd_Odd_Page; }

    public DbsField getPnd_Fcpa803_Pnd_End_Of_Pymnt() { return pnd_Fcpa803_Pnd_End_Of_Pymnt; }

    public DbsField getPnd_Fcpa803_Pnd_Roth_Ind() { return pnd_Fcpa803_Pnd_Roth_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpa803 = dbsRecord.newGroupInRecord("pnd_Fcpa803", "#FCPA803");
        pnd_Fcpa803.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpa803_Cntrct_Orgn_Cde = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Fcpa803_Pnd_Output_Rec = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Output_Rec", "#OUTPUT-REC", FieldType.STRING, 143);
        pnd_Fcpa803_Pnd_Output_RecRedef1 = pnd_Fcpa803.newGroupInGroup("pnd_Fcpa803_Pnd_Output_RecRedef1", "Redefines", pnd_Fcpa803_Pnd_Output_Rec);
        pnd_Fcpa803_Pnd_Cc = pnd_Fcpa803_Pnd_Output_RecRedef1.newFieldInGroup("pnd_Fcpa803_Pnd_Cc", "#CC", FieldType.STRING, 1);
        pnd_Fcpa803_Pnd_Font = pnd_Fcpa803_Pnd_Output_RecRedef1.newFieldInGroup("pnd_Fcpa803_Pnd_Font", "#FONT", FieldType.STRING, 1);
        pnd_Fcpa803_Pnd_Output_Rec_Detail = pnd_Fcpa803_Pnd_Output_RecRedef1.newFieldInGroup("pnd_Fcpa803_Pnd_Output_Rec_Detail", "#OUTPUT-REC-DETAIL", 
            FieldType.STRING, 141);
        pnd_Fcpa803_Pnd_Output_Rec_DetailRedef2 = pnd_Fcpa803_Pnd_Output_RecRedef1.newGroupInGroup("pnd_Fcpa803_Pnd_Output_Rec_DetailRedef2", "Redefines", 
            pnd_Fcpa803_Pnd_Output_Rec_Detail);
        pnd_Fcpa803_Pnd_Output_Col = pnd_Fcpa803_Pnd_Output_Rec_DetailRedef2.newFieldArrayInGroup("pnd_Fcpa803_Pnd_Output_Col", "#OUTPUT-COL", FieldType.STRING, 
            15, new DbsArrayController(1,7));
        pnd_Fcpa803_Pnd_Grand_Totals = pnd_Fcpa803.newGroupInGroup("pnd_Fcpa803_Pnd_Grand_Totals", "#GRAND-TOTALS");
        pnd_Fcpa803_Pnd_Grand_Settl_Amt = pnd_Fcpa803_Pnd_Grand_Totals.newFieldInGroup("pnd_Fcpa803_Pnd_Grand_Settl_Amt", "#GRAND-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Fcpa803_Pnd_Grand_Dpi_Amt = pnd_Fcpa803_Pnd_Grand_Totals.newFieldInGroup("pnd_Fcpa803_Pnd_Grand_Dpi_Amt", "#GRAND-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Fcpa803_Pnd_Grand_Ded_Amt = pnd_Fcpa803_Pnd_Grand_Totals.newFieldInGroup("pnd_Fcpa803_Pnd_Grand_Ded_Amt", "#GRAND-DED-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Fcpa803_Pnd_Grand_Net_Amt = pnd_Fcpa803_Pnd_Grand_Totals.newFieldInGroup("pnd_Fcpa803_Pnd_Grand_Net_Amt", "#GRAND-NET-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Fcpa803_Pnd_Record_In_Pymnt = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Record_In_Pymnt", "#RECORD-IN-PYMNT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Fcpa803_Pnd_Prev_Xerox_Sub = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Prev_Xerox_Sub", "#PREV-XEROX-SUB", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpa803_Pnd_Xerox_Sub = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Xerox_Sub", "#XEROX-SUB", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpa803_Pnd_Current_Page = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Current_Page", "#CURRENT-PAGE", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpa803_Pnd_Addr_Ind = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Addr_Ind", "#ADDR-IND", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpa803_Pnd_Stmnt_Text_Ind = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Stmnt_Text_Ind", "#STMNT-TEXT-IND", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpa803_Pnd_Bar_Last_Page = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Bar_Last_Page", "#BAR-LAST-PAGE", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpa803_Pnd_Dpi_Col = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Dpi_Col", "#DPI-COL", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpa803_Pnd_Ded_Col = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Ded_Col", "#DED-COL", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpa803_Pnd_Net_Col = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Net_Col", "#NET-COL", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpa803_Pnd_Simplex = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Simplex", "#SIMPLEX", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Full_Xerox = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Full_Xerox", "#FULL-XEROX", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Separator_Page = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Separator_Page", "#SEPARATOR-PAGE", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Csr_Run = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Csr_Run", "#CSR-RUN", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Stmnt_Type = pnd_Fcpa803.newGroupInGroup("pnd_Fcpa803_Pnd_Stmnt_Type", "#STMNT-TYPE");
        pnd_Fcpa803_Pnd_Zero_Check = pnd_Fcpa803_Pnd_Stmnt_Type.newFieldInGroup("pnd_Fcpa803_Pnd_Zero_Check", "#ZERO-CHECK", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Eft = pnd_Fcpa803_Pnd_Stmnt_Type.newFieldInGroup("pnd_Fcpa803_Pnd_Eft", "#EFT", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Global_Pay = pnd_Fcpa803_Pnd_Stmnt_Type.newFieldInGroup("pnd_Fcpa803_Pnd_Global_Pay", "#GLOBAL-PAY", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Check_To_Annt = pnd_Fcpa803_Pnd_Stmnt_Type.newFieldInGroup("pnd_Fcpa803_Pnd_Check_To_Annt", "#CHECK-TO-ANNT", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Stmnt_To_Annt = pnd_Fcpa803_Pnd_Stmnt_Type.newFieldInGroup("pnd_Fcpa803_Pnd_Stmnt_To_Annt", "#STMNT-TO-ANNT", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Check = pnd_Fcpa803_Pnd_Stmnt_Type.newFieldInGroup("pnd_Fcpa803_Pnd_Check", "#CHECK", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Stmnt = pnd_Fcpa803_Pnd_Stmnt_Type.newFieldInGroup("pnd_Fcpa803_Pnd_Stmnt", "#STMNT", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_New_Pymnt = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_New_Pymnt", "#NEW-PYMNT", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Newpage_Ind = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Newpage_Ind", "#NEWPAGE-IND", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Company_Break = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Company_Break", "#COMPANY-BREAK", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Ppcn_Break = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Ppcn_Break", "#PPCN-BREAK", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Odd_Page = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Odd_Page", "#ODD-PAGE", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_End_Of_Pymnt = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_End_Of_Pymnt", "#END-OF-PYMNT", FieldType.BOOLEAN);
        pnd_Fcpa803_Pnd_Roth_Ind = pnd_Fcpa803.newFieldInGroup("pnd_Fcpa803_Pnd_Roth_Ind", "#ROTH-IND", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa803(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

