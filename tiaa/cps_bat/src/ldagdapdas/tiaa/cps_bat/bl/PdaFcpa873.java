/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:28 PM
**        * FROM NATURAL PDA     : FCPA873
************************************************************
**        * FILE NAME            : PdaFcpa873.java
**        * CLASS NAME           : PdaFcpa873
**        * INSTANCE NAME        : PdaFcpa873
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa873 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Nz_Check_Fields;
    private DbsField pnd_Nz_Check_Fields_Pymnt_Ivc_Amt;
    private DbsField pnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table;
    private DbsField pnd_Nz_Check_Fields_Pnd_Pymnt_Records;
    private DbsField pnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind;
    private DbsField pnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind;

    public DbsGroup getPnd_Nz_Check_Fields() { return pnd_Nz_Check_Fields; }

    public DbsField getPnd_Nz_Check_Fields_Pymnt_Ivc_Amt() { return pnd_Nz_Check_Fields_Pymnt_Ivc_Amt; }

    public DbsField getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table() { return pnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table; }

    public DbsField getPnd_Nz_Check_Fields_Pnd_Pymnt_Records() { return pnd_Nz_Check_Fields_Pnd_Pymnt_Records; }

    public DbsField getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind() { return pnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind; }

    public DbsField getPnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind() { return pnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Nz_Check_Fields = dbsRecord.newGroupInRecord("pnd_Nz_Check_Fields", "#NZ-CHECK-FIELDS");
        pnd_Nz_Check_Fields.setParameterOption(ParameterOption.ByReference);
        pnd_Nz_Check_Fields_Pymnt_Ivc_Amt = pnd_Nz_Check_Fields.newFieldInGroup("pnd_Nz_Check_Fields_Pymnt_Ivc_Amt", "PYMNT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        pnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table = pnd_Nz_Check_Fields.newFieldArrayInGroup("pnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table", "#DED-PYMNT-TABLE", 
            FieldType.STRING, 3, new DbsArrayController(1,5));
        pnd_Nz_Check_Fields_Pnd_Pymnt_Records = pnd_Nz_Check_Fields.newFieldInGroup("pnd_Nz_Check_Fields_Pnd_Pymnt_Records", "#PYMNT-RECORDS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind = pnd_Nz_Check_Fields.newFieldInGroup("pnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind", "#DPI-DCI-IND", FieldType.BOOLEAN);
        pnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind = pnd_Nz_Check_Fields.newFieldInGroup("pnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind", "#PYMNT-DED-IND", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa873(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

