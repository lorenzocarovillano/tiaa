/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:34 PM
**        * FROM NATURAL LDA     : FCPLOPTN
************************************************************
**        * FILE NAME            : LdaFcploptn.java
**        * CLASS NAME           : LdaFcploptn
**        * INSTANCE NAME        : LdaFcploptn
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcploptn extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcploptn;
    private DbsField pnd_Fcploptn_Pnd_Optn_Desc;

    public DbsGroup getPnd_Fcploptn() { return pnd_Fcploptn; }

    public DbsField getPnd_Fcploptn_Pnd_Optn_Desc() { return pnd_Fcploptn_Pnd_Optn_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcploptn = newGroupInRecord("pnd_Fcploptn", "#FCPLOPTN");
        pnd_Fcploptn_Pnd_Optn_Desc = pnd_Fcploptn.newFieldArrayInGroup("pnd_Fcploptn_Pnd_Optn_Desc", "#OPTN-DESC", FieldType.STRING, 70, new DbsArrayController(0,
            59));

        this.setRecordName("LdaFcploptn");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(1).setInitialValue("Single Life Annuity");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(2).setInitialValue("Installment Refund");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(3).setInitialValue("Half Benefit to Second Annuitant");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(4).setInitialValue("Full Benefit to Survivor");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(5).setInitialValue("Single Life Annuity With 10-Year Guaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(6).setInitialValue("Single Life Annuity With 20-Year Guaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(7).setInitialValue("Joint and Two-Thirds Benefit to Survivor");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(8).setInitialValue("Joint & 2/3 Benefit to Survivor With 10-Year Guaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(9).setInitialValue("Single Life Annuity With 15-Year Guaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(10).setInitialValue("Joint & 2/3 Benefit to Survivor With 20-Year Guaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(11).setInitialValue("Full Benefit to Survivor With 20-Year Guaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(12).setInitialValue("One-Half Benefit to Second Annuitant With 20-YearGuaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(13).setInitialValue("Joint & 2/3 Benefit to Survivor With 15-Year Guaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(14).setInitialValue("Full Benefit to Survivor With 15-Year Guaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(15).setInitialValue("One-Half Benefit to Second Annuitant With 15-YearGuaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(16).setInitialValue("Full Benefit to Survivor With 10-Year Guaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(17).setInitialValue("One-Half Benefit to Second Annuitant With 10-YearGuaranteed Period");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(18).setInitialValue("Two-Thirds Benefit to Survivor - Group Annuity");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(19).setInitialValue("Single Life With Death Benefit -Group Annuity");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(20).setInitialValue("Temporary Life - Group Annuity");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(21).setInitialValue("Annuity Certain");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(22).setInitialValue("Principal and Interest");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(23).setInitialValue("Family Income");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(24).setInitialValue("Modified Cash Refund Annuity");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(25).setInitialValue("Interest Payment Retirement Option - From a DA Contract");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(26).setInitialValue("Interest Payment Retirement Option - From an SRA Contract");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(27).setInitialValue("Interest Payment Retirement Option - From a GRA Contract");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(28).setInitialValue("Transfer Payout Annuity - From a DA Contract");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(30).setInitialValue("Transfer Payout Annuity - From a GRA Contract");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(31).setInitialValue("Minimum Distribution Option");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(53).setInitialValue("Last Survivor 75% w/ 5 year certain.");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(54).setInitialValue("Last Survivor 75% w/10 year certain.");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(55).setInitialValue("Last Survivor 75% w/15 year certain.");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(56).setInitialValue("Last Survivor 75% w/20 year certain.");
        pnd_Fcploptn_Pnd_Optn_Desc.getValue(57).setInitialValue("Last Survivor 75% w/ 0 year certain.");
    }

    // Constructor
    public LdaFcploptn() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
