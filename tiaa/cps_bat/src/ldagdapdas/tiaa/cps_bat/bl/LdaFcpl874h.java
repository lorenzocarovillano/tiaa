/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:17 PM
**        * FROM NATURAL LDA     : FCPL874H
************************************************************
**        * FILE NAME            : LdaFcpl874h.java
**        * CLASS NAME           : LdaFcpl874h
**        * INSTANCE NAME        : LdaFcpl874h
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl874h extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl874h;
    private DbsField pnd_Fcpl874h_Pnd_Hdr_Line1_1;
    private DbsField pnd_Fcpl874h_Pnd_Hdr_Line1_2;
    private DbsField pnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_1;
    private DbsField pnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_2;
    private DbsField pnd_Fcpl874h_Pnd_Hrd_Line3_Cref_1;
    private DbsField pnd_Fcpl874h_Pnd_Hrd_Line3_Cref_2;

    public DbsGroup getPnd_Fcpl874h() { return pnd_Fcpl874h; }

    public DbsField getPnd_Fcpl874h_Pnd_Hdr_Line1_1() { return pnd_Fcpl874h_Pnd_Hdr_Line1_1; }

    public DbsField getPnd_Fcpl874h_Pnd_Hdr_Line1_2() { return pnd_Fcpl874h_Pnd_Hdr_Line1_2; }

    public DbsField getPnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_1() { return pnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_1; }

    public DbsField getPnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_2() { return pnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_2; }

    public DbsField getPnd_Fcpl874h_Pnd_Hrd_Line3_Cref_1() { return pnd_Fcpl874h_Pnd_Hrd_Line3_Cref_1; }

    public DbsField getPnd_Fcpl874h_Pnd_Hrd_Line3_Cref_2() { return pnd_Fcpl874h_Pnd_Hrd_Line3_Cref_2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl874h = newGroupInRecord("pnd_Fcpl874h", "#FCPL874H");
        pnd_Fcpl874h_Pnd_Hdr_Line1_1 = pnd_Fcpl874h.newFieldInGroup("pnd_Fcpl874h_Pnd_Hdr_Line1_1", "#HDR-LINE1-1", FieldType.STRING, 10);
        pnd_Fcpl874h_Pnd_Hdr_Line1_2 = pnd_Fcpl874h.newFieldInGroup("pnd_Fcpl874h_Pnd_Hdr_Line1_2", "#HDR-LINE1-2", FieldType.STRING, 12);
        pnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_1 = pnd_Fcpl874h.newFieldInGroup("pnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_1", "#HRD-LINE3-TIAA-1", FieldType.STRING, 19);
        pnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_2 = pnd_Fcpl874h.newFieldInGroup("pnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_2", "#HRD-LINE3-TIAA-2", FieldType.STRING, 28);
        pnd_Fcpl874h_Pnd_Hrd_Line3_Cref_1 = pnd_Fcpl874h.newFieldInGroup("pnd_Fcpl874h_Pnd_Hrd_Line3_Cref_1", "#HRD-LINE3-CREF-1", FieldType.STRING, 22);
        pnd_Fcpl874h_Pnd_Hrd_Line3_Cref_2 = pnd_Fcpl874h.newFieldInGroup("pnd_Fcpl874h_Pnd_Hrd_Line3_Cref_2", "#HRD-LINE3-CREF-2", FieldType.STRING, 31);

        this.setRecordName("LdaFcpl874h");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl874h_Pnd_Hdr_Line1_1.setInitialValue("11Initial");
        pnd_Fcpl874h_Pnd_Hdr_Line1_2.setInitialValue("Payment for:");
        pnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_1.setInitialValue("7For TIAA Contract");
        pnd_Fcpl874h_Pnd_Hrd_Line3_Tiaa_2.setInitialValue("in Settlement of Contract(s)");
        pnd_Fcpl874h_Pnd_Hrd_Line3_Cref_1.setInitialValue("7For CREF Certificate");
        pnd_Fcpl874h_Pnd_Hrd_Line3_Cref_2.setInitialValue("in Settlement of Certificate(s)");
    }

    // Constructor
    public LdaFcpl874h() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
