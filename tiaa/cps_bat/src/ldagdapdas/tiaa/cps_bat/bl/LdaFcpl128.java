/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:58:58 PM
**        * FROM NATURAL LDA     : FCPL128
************************************************************
**        * FILE NAME            : LdaFcpl128.java
**        * CLASS NAME           : LdaFcpl128
**        * INSTANCE NAME        : LdaFcpl128
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl128 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Mit_Update_Record;
    private DbsField pnd_Mit_Update_Record_Pnd_Detail_Text;
    private DbsField pnd_Mit_Update_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Mit_Update_Record_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Mit_Update_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Mit_Update_Record_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Mit_Update_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Mit_Update_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Mit_Update_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Mit_Update_Record_Pymnt_Check_Dte;
    private DbsField pnd_Mit_Update_Record_Pymnt_Ind;
    private DbsField pnd_Mit_Update_Record_Pnd_Filler;
    private DbsGroup pnd_Mit_Header_Record;
    private DbsField pnd_Mit_Header_Record_Pnd_Header_Text;
    private DbsField pnd_Mit_Header_Record_Pnd_Timestmp;
    private DbsField pnd_Mit_Header_Record_Pnd_Date_Time;
    private DbsField pnd_Mit_Header_Record_Pnd_Rec_Cnt;
    private DbsField pnd_Mit_Header_Record_Pnd_Filler;

    public DbsGroup getPnd_Mit_Update_Record() { return pnd_Mit_Update_Record; }

    public DbsField getPnd_Mit_Update_Record_Pnd_Detail_Text() { return pnd_Mit_Update_Record_Pnd_Detail_Text; }

    public DbsField getPnd_Mit_Update_Record_Cntrct_Orgn_Cde() { return pnd_Mit_Update_Record_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Mit_Update_Record_Pymnt_Reqst_Log_Dte_Time() { return pnd_Mit_Update_Record_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getPnd_Mit_Update_Record_Cntrct_Unq_Id_Nbr() { return pnd_Mit_Update_Record_Cntrct_Unq_Id_Nbr; }

    public DbsField getPnd_Mit_Update_Record_Pymnt_Prcss_Seq_Nbr() { return pnd_Mit_Update_Record_Pymnt_Prcss_Seq_Nbr; }

    public DbsField getPnd_Mit_Update_Record_Pymnt_Pay_Type_Req_Ind() { return pnd_Mit_Update_Record_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPnd_Mit_Update_Record_Cntrct_Payee_Cde() { return pnd_Mit_Update_Record_Cntrct_Payee_Cde; }

    public DbsField getPnd_Mit_Update_Record_Cntrct_Hold_Cde() { return pnd_Mit_Update_Record_Cntrct_Hold_Cde; }

    public DbsField getPnd_Mit_Update_Record_Pymnt_Check_Dte() { return pnd_Mit_Update_Record_Pymnt_Check_Dte; }

    public DbsField getPnd_Mit_Update_Record_Pymnt_Ind() { return pnd_Mit_Update_Record_Pymnt_Ind; }

    public DbsField getPnd_Mit_Update_Record_Pnd_Filler() { return pnd_Mit_Update_Record_Pnd_Filler; }

    public DbsGroup getPnd_Mit_Header_Record() { return pnd_Mit_Header_Record; }

    public DbsField getPnd_Mit_Header_Record_Pnd_Header_Text() { return pnd_Mit_Header_Record_Pnd_Header_Text; }

    public DbsField getPnd_Mit_Header_Record_Pnd_Timestmp() { return pnd_Mit_Header_Record_Pnd_Timestmp; }

    public DbsField getPnd_Mit_Header_Record_Pnd_Date_Time() { return pnd_Mit_Header_Record_Pnd_Date_Time; }

    public DbsField getPnd_Mit_Header_Record_Pnd_Rec_Cnt() { return pnd_Mit_Header_Record_Pnd_Rec_Cnt; }

    public DbsField getPnd_Mit_Header_Record_Pnd_Filler() { return pnd_Mit_Header_Record_Pnd_Filler; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Mit_Update_Record = newGroupInRecord("pnd_Mit_Update_Record", "#MIT-UPDATE-RECORD");
        pnd_Mit_Update_Record_Pnd_Detail_Text = pnd_Mit_Update_Record.newFieldInGroup("pnd_Mit_Update_Record_Pnd_Detail_Text", "#DETAIL-TEXT", FieldType.STRING, 
            2);
        pnd_Mit_Update_Record_Cntrct_Orgn_Cde = pnd_Mit_Update_Record.newFieldInGroup("pnd_Mit_Update_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Mit_Update_Record_Pymnt_Reqst_Log_Dte_Time = pnd_Mit_Update_Record.newFieldInGroup("pnd_Mit_Update_Record_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Mit_Update_Record_Cntrct_Unq_Id_Nbr = pnd_Mit_Update_Record.newFieldInGroup("pnd_Mit_Update_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Mit_Update_Record_Pymnt_Prcss_Seq_Nbr = pnd_Mit_Update_Record.newFieldInGroup("pnd_Mit_Update_Record_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Mit_Update_Record_Pymnt_Pay_Type_Req_Ind = pnd_Mit_Update_Record.newFieldInGroup("pnd_Mit_Update_Record_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Mit_Update_Record_Cntrct_Payee_Cde = pnd_Mit_Update_Record.newFieldInGroup("pnd_Mit_Update_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Mit_Update_Record_Cntrct_Hold_Cde = pnd_Mit_Update_Record.newFieldInGroup("pnd_Mit_Update_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            4);
        pnd_Mit_Update_Record_Pymnt_Check_Dte = pnd_Mit_Update_Record.newFieldInGroup("pnd_Mit_Update_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.STRING, 
            8);
        pnd_Mit_Update_Record_Pymnt_Ind = pnd_Mit_Update_Record.newFieldInGroup("pnd_Mit_Update_Record_Pymnt_Ind", "PYMNT-IND", FieldType.STRING, 1);
        pnd_Mit_Update_Record_Pnd_Filler = pnd_Mit_Update_Record.newFieldInGroup("pnd_Mit_Update_Record_Pnd_Filler", "#FILLER", FieldType.STRING, 3);

        pnd_Mit_Header_Record = newGroupInRecord("pnd_Mit_Header_Record", "#MIT-HEADER-RECORD");
        pnd_Mit_Header_Record_Pnd_Header_Text = pnd_Mit_Header_Record.newFieldInGroup("pnd_Mit_Header_Record_Pnd_Header_Text", "#HEADER-TEXT", FieldType.STRING, 
            2);
        pnd_Mit_Header_Record_Pnd_Timestmp = pnd_Mit_Header_Record.newFieldInGroup("pnd_Mit_Header_Record_Pnd_Timestmp", "#TIMESTMP", FieldType.STRING, 
            16);
        pnd_Mit_Header_Record_Pnd_Date_Time = pnd_Mit_Header_Record.newFieldInGroup("pnd_Mit_Header_Record_Pnd_Date_Time", "#DATE-TIME", FieldType.STRING, 
            21);
        pnd_Mit_Header_Record_Pnd_Rec_Cnt = pnd_Mit_Header_Record.newFieldInGroup("pnd_Mit_Header_Record_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Mit_Header_Record_Pnd_Filler = pnd_Mit_Header_Record.newFieldInGroup("pnd_Mit_Header_Record_Pnd_Filler", "#FILLER", FieldType.STRING, 17);

        this.setRecordName("LdaFcpl128");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Mit_Header_Record_Pnd_Header_Text.setInitialValue("HD");
    }

    // Constructor
    public LdaFcpl128() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
