/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:25 PM
**        * FROM NATURAL PDA     : CPOA101
************************************************************
**        * FILE NAME            : PdaCpoa101.java
**        * CLASS NAME           : PdaCpoa101
**        * INSTANCE NAME        : PdaCpoa101
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCpoa101 extends PdaBase
{
    // Properties
    private DbsGroup cpoa101;
    private DbsField cpoa101_Procedure;
    private DbsField cpoa101_Module;
    private DbsField cpoa101_Function;
    private DbsField cpoa101_Report;
    private DbsGroup cpoa101_Cpon101_Data;
    private DbsField cpoa101_Sequence_Nbr;
    private DbsField cpoa101_Control_Key;
    private DbsGroup cpoa101_Control_KeyRedef1;
    private DbsField cpoa101_Key;
    private DbsField cpoa101_Fil1;
    private DbsField cpoa101_Key_Inverse;
    private DbsGroup cpoa101_Key_InverseRedef2;
    private DbsField cpoa101_Key_Inverse_N;
    private DbsField cpoa101_Update_Time;
    private DbsField cpoa101_Cycle_Nbr;
    private DbsField cpoa101_Current_Status;
    private DbsField cpoa101_Cntrct_Orgn_Cde;
    private DbsField cpoa101_Check_Cnt;
    private DbsField cpoa101_Check_Amt;
    private DbsField cpoa101_Eft_Cnt;
    private DbsField cpoa101_Eft_Amt;
    private DbsField cpoa101_Other_Cnt;
    private DbsField cpoa101_Other_Amt;
    private DbsField cpoa101_Cs_Cnt;
    private DbsField cpoa101_Cs_Amt;
    private DbsField cpoa101_Redraw_Cnt;
    private DbsField cpoa101_Redraw_Amt;
    private DbsField cpoa101_Unused_Cnt;
    private DbsField cpoa101_Unused_Amt;
    private DbsField cpoa101_Restart;

    public DbsGroup getCpoa101() { return cpoa101; }

    public DbsField getCpoa101_Procedure() { return cpoa101_Procedure; }

    public DbsField getCpoa101_Module() { return cpoa101_Module; }

    public DbsField getCpoa101_Function() { return cpoa101_Function; }

    public DbsField getCpoa101_Report() { return cpoa101_Report; }

    public DbsGroup getCpoa101_Cpon101_Data() { return cpoa101_Cpon101_Data; }

    public DbsField getCpoa101_Sequence_Nbr() { return cpoa101_Sequence_Nbr; }

    public DbsField getCpoa101_Control_Key() { return cpoa101_Control_Key; }

    public DbsGroup getCpoa101_Control_KeyRedef1() { return cpoa101_Control_KeyRedef1; }

    public DbsField getCpoa101_Key() { return cpoa101_Key; }

    public DbsField getCpoa101_Fil1() { return cpoa101_Fil1; }

    public DbsField getCpoa101_Key_Inverse() { return cpoa101_Key_Inverse; }

    public DbsGroup getCpoa101_Key_InverseRedef2() { return cpoa101_Key_InverseRedef2; }

    public DbsField getCpoa101_Key_Inverse_N() { return cpoa101_Key_Inverse_N; }

    public DbsField getCpoa101_Update_Time() { return cpoa101_Update_Time; }

    public DbsField getCpoa101_Cycle_Nbr() { return cpoa101_Cycle_Nbr; }

    public DbsField getCpoa101_Current_Status() { return cpoa101_Current_Status; }

    public DbsField getCpoa101_Cntrct_Orgn_Cde() { return cpoa101_Cntrct_Orgn_Cde; }

    public DbsField getCpoa101_Check_Cnt() { return cpoa101_Check_Cnt; }

    public DbsField getCpoa101_Check_Amt() { return cpoa101_Check_Amt; }

    public DbsField getCpoa101_Eft_Cnt() { return cpoa101_Eft_Cnt; }

    public DbsField getCpoa101_Eft_Amt() { return cpoa101_Eft_Amt; }

    public DbsField getCpoa101_Other_Cnt() { return cpoa101_Other_Cnt; }

    public DbsField getCpoa101_Other_Amt() { return cpoa101_Other_Amt; }

    public DbsField getCpoa101_Cs_Cnt() { return cpoa101_Cs_Cnt; }

    public DbsField getCpoa101_Cs_Amt() { return cpoa101_Cs_Amt; }

    public DbsField getCpoa101_Redraw_Cnt() { return cpoa101_Redraw_Cnt; }

    public DbsField getCpoa101_Redraw_Amt() { return cpoa101_Redraw_Amt; }

    public DbsField getCpoa101_Unused_Cnt() { return cpoa101_Unused_Cnt; }

    public DbsField getCpoa101_Unused_Amt() { return cpoa101_Unused_Amt; }

    public DbsField getCpoa101_Restart() { return cpoa101_Restart; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cpoa101 = dbsRecord.newGroupInRecord("cpoa101", "CPOA101");
        cpoa101.setParameterOption(ParameterOption.ByReference);
        cpoa101_Procedure = cpoa101.newFieldInGroup("cpoa101_Procedure", "PROCEDURE", FieldType.STRING, 8);
        cpoa101_Module = cpoa101.newFieldInGroup("cpoa101_Module", "MODULE", FieldType.STRING, 8);
        cpoa101_Function = cpoa101.newFieldInGroup("cpoa101_Function", "FUNCTION", FieldType.STRING, 20);
        cpoa101_Report = cpoa101.newFieldInGroup("cpoa101_Report", "REPORT", FieldType.STRING, 10);
        cpoa101_Cpon101_Data = cpoa101.newGroupInGroup("cpoa101_Cpon101_Data", "CPON101-DATA");
        cpoa101_Sequence_Nbr = cpoa101_Cpon101_Data.newFieldInGroup("cpoa101_Sequence_Nbr", "SEQUENCE-NBR", FieldType.NUMERIC, 7);
        cpoa101_Control_Key = cpoa101_Cpon101_Data.newFieldInGroup("cpoa101_Control_Key", "CONTROL-KEY", FieldType.STRING, 20);
        cpoa101_Control_KeyRedef1 = cpoa101_Cpon101_Data.newGroupInGroup("cpoa101_Control_KeyRedef1", "Redefines", cpoa101_Control_Key);
        cpoa101_Key = cpoa101_Control_KeyRedef1.newFieldInGroup("cpoa101_Key", "KEY", FieldType.STRING, 11);
        cpoa101_Fil1 = cpoa101_Control_KeyRedef1.newFieldInGroup("cpoa101_Fil1", "FIL1", FieldType.STRING, 1);
        cpoa101_Key_Inverse = cpoa101_Control_KeyRedef1.newFieldInGroup("cpoa101_Key_Inverse", "KEY-INVERSE", FieldType.STRING, 8);
        cpoa101_Key_InverseRedef2 = cpoa101_Control_KeyRedef1.newGroupInGroup("cpoa101_Key_InverseRedef2", "Redefines", cpoa101_Key_Inverse);
        cpoa101_Key_Inverse_N = cpoa101_Key_InverseRedef2.newFieldInGroup("cpoa101_Key_Inverse_N", "KEY-INVERSE-N", FieldType.NUMERIC, 8);
        cpoa101_Update_Time = cpoa101_Cpon101_Data.newFieldInGroup("cpoa101_Update_Time", "UPDATE-TIME", FieldType.TIME);
        cpoa101_Cycle_Nbr = cpoa101_Cpon101_Data.newFieldInGroup("cpoa101_Cycle_Nbr", "CYCLE-NBR", FieldType.NUMERIC, 4);
        cpoa101_Current_Status = cpoa101_Cpon101_Data.newFieldInGroup("cpoa101_Current_Status", "CURRENT-STATUS", FieldType.STRING, 20);
        cpoa101_Cntrct_Orgn_Cde = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, new DbsArrayController(1,
            20));
        cpoa101_Check_Cnt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Check_Cnt", "CHECK-CNT", FieldType.NUMERIC, 10, new DbsArrayController(1,
            20));
        cpoa101_Check_Amt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Check_Amt", "CHECK-AMT", FieldType.DECIMAL, 25,2, new DbsArrayController(1,
            20));
        cpoa101_Eft_Cnt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Eft_Cnt", "EFT-CNT", FieldType.NUMERIC, 10, new DbsArrayController(1,20));
        cpoa101_Eft_Amt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Eft_Amt", "EFT-AMT", FieldType.DECIMAL, 25,2, new DbsArrayController(1,20));
        cpoa101_Other_Cnt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Other_Cnt", "OTHER-CNT", FieldType.NUMERIC, 10, new DbsArrayController(1,
            20));
        cpoa101_Other_Amt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Other_Amt", "OTHER-AMT", FieldType.DECIMAL, 25,2, new DbsArrayController(1,
            20));
        cpoa101_Cs_Cnt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Cs_Cnt", "CS-CNT", FieldType.NUMERIC, 10, new DbsArrayController(1,20));
        cpoa101_Cs_Amt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Cs_Amt", "CS-AMT", FieldType.DECIMAL, 25,2, new DbsArrayController(1,20));
        cpoa101_Redraw_Cnt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Redraw_Cnt", "REDRAW-CNT", FieldType.NUMERIC, 10, new DbsArrayController(1,
            20));
        cpoa101_Redraw_Amt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Redraw_Amt", "REDRAW-AMT", FieldType.DECIMAL, 25,2, new DbsArrayController(1,
            20));
        cpoa101_Unused_Cnt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Unused_Cnt", "UNUSED-CNT", FieldType.NUMERIC, 10, new DbsArrayController(1,
            20));
        cpoa101_Unused_Amt = cpoa101_Cpon101_Data.newFieldArrayInGroup("cpoa101_Unused_Amt", "UNUSED-AMT", FieldType.DECIMAL, 25,2, new DbsArrayController(1,
            20));
        cpoa101_Restart = cpoa101_Cpon101_Data.newFieldInGroup("cpoa101_Restart", "RESTART", FieldType.STRING, 8);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCpoa101(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

