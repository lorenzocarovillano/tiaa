/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:16 PM
**        * FROM NATURAL LDA     : FCPL804
************************************************************
**        * FILE NAME            : LdaFcpl804.java
**        * CLASS NAME           : LdaFcpl804
**        * INSTANCE NAME        : LdaFcpl804
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl804 extends DbsRecord
{
    // Properties
    private DbsGroup deduction_Rec;
    private DbsField deduction_Rec_Cntrct_Company;
    private DbsField deduction_Rec_Cntrct_Cmbn_Nbr;
    private DbsField deduction_Rec_Cntrct_Ppcn_Nbr;
    private DbsField deduction_Rec_Cntrct_Payee_Cde;
    private DbsField deduction_Rec_Cntrct_Mode_Cde;
    private DbsField deduction_Rec_Cntrct_Hold_Cde;
    private DbsField deduction_Rec_Cntrct_Crrncy_Cde;
    private DbsField deduction_Rec_Ph_Last_Name;
    private DbsField deduction_Rec_Ph_First_Name;
    private DbsGroup deduction_Rec_Ph_First_NameRedef1;
    private DbsField deduction_Rec_Ph_First_Init;
    private DbsField deduction_Rec_Ph_Middle_Name;
    private DbsGroup deduction_Rec_Ph_Middle_NameRedef2;
    private DbsField deduction_Rec_Ph_Middle_Init;
    private DbsField deduction_Rec_Annt_Soc_Sec_Nbr;
    private DbsField deduction_Rec_Pymnt_Ded_Cde;
    private DbsField deduction_Rec_Pymnt_Ded_Amt;
    private DbsField deduction_Rec_Pymnt_Ded_Payee_Cde;
    private DbsField deduction_Rec_Pymnt_Ded_Payee_Seq;
    private DbsField deduction_Rec_Pymnt_Suspend_Cde;
    private DbsField deduction_Rec_Pymnt_Check_Dte;
    private DbsField deduction_Rec_Extra_Space;

    public DbsGroup getDeduction_Rec() { return deduction_Rec; }

    public DbsField getDeduction_Rec_Cntrct_Company() { return deduction_Rec_Cntrct_Company; }

    public DbsField getDeduction_Rec_Cntrct_Cmbn_Nbr() { return deduction_Rec_Cntrct_Cmbn_Nbr; }

    public DbsField getDeduction_Rec_Cntrct_Ppcn_Nbr() { return deduction_Rec_Cntrct_Ppcn_Nbr; }

    public DbsField getDeduction_Rec_Cntrct_Payee_Cde() { return deduction_Rec_Cntrct_Payee_Cde; }

    public DbsField getDeduction_Rec_Cntrct_Mode_Cde() { return deduction_Rec_Cntrct_Mode_Cde; }

    public DbsField getDeduction_Rec_Cntrct_Hold_Cde() { return deduction_Rec_Cntrct_Hold_Cde; }

    public DbsField getDeduction_Rec_Cntrct_Crrncy_Cde() { return deduction_Rec_Cntrct_Crrncy_Cde; }

    public DbsField getDeduction_Rec_Ph_Last_Name() { return deduction_Rec_Ph_Last_Name; }

    public DbsField getDeduction_Rec_Ph_First_Name() { return deduction_Rec_Ph_First_Name; }

    public DbsGroup getDeduction_Rec_Ph_First_NameRedef1() { return deduction_Rec_Ph_First_NameRedef1; }

    public DbsField getDeduction_Rec_Ph_First_Init() { return deduction_Rec_Ph_First_Init; }

    public DbsField getDeduction_Rec_Ph_Middle_Name() { return deduction_Rec_Ph_Middle_Name; }

    public DbsGroup getDeduction_Rec_Ph_Middle_NameRedef2() { return deduction_Rec_Ph_Middle_NameRedef2; }

    public DbsField getDeduction_Rec_Ph_Middle_Init() { return deduction_Rec_Ph_Middle_Init; }

    public DbsField getDeduction_Rec_Annt_Soc_Sec_Nbr() { return deduction_Rec_Annt_Soc_Sec_Nbr; }

    public DbsField getDeduction_Rec_Pymnt_Ded_Cde() { return deduction_Rec_Pymnt_Ded_Cde; }

    public DbsField getDeduction_Rec_Pymnt_Ded_Amt() { return deduction_Rec_Pymnt_Ded_Amt; }

    public DbsField getDeduction_Rec_Pymnt_Ded_Payee_Cde() { return deduction_Rec_Pymnt_Ded_Payee_Cde; }

    public DbsField getDeduction_Rec_Pymnt_Ded_Payee_Seq() { return deduction_Rec_Pymnt_Ded_Payee_Seq; }

    public DbsField getDeduction_Rec_Pymnt_Suspend_Cde() { return deduction_Rec_Pymnt_Suspend_Cde; }

    public DbsField getDeduction_Rec_Pymnt_Check_Dte() { return deduction_Rec_Pymnt_Check_Dte; }

    public DbsField getDeduction_Rec_Extra_Space() { return deduction_Rec_Extra_Space; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        deduction_Rec = newGroupInRecord("deduction_Rec", "DEDUCTION-REC");
        deduction_Rec_Cntrct_Company = deduction_Rec.newFieldInGroup("deduction_Rec_Cntrct_Company", "CNTRCT-COMPANY", FieldType.STRING, 1);
        deduction_Rec_Cntrct_Cmbn_Nbr = deduction_Rec.newFieldInGroup("deduction_Rec_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        deduction_Rec_Cntrct_Ppcn_Nbr = deduction_Rec.newFieldInGroup("deduction_Rec_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        deduction_Rec_Cntrct_Payee_Cde = deduction_Rec.newFieldInGroup("deduction_Rec_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 8);
        deduction_Rec_Cntrct_Mode_Cde = deduction_Rec.newFieldInGroup("deduction_Rec_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.STRING, 8);
        deduction_Rec_Cntrct_Hold_Cde = deduction_Rec.newFieldInGroup("deduction_Rec_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        deduction_Rec_Cntrct_Crrncy_Cde = deduction_Rec.newFieldInGroup("deduction_Rec_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 1);
        deduction_Rec_Ph_Last_Name = deduction_Rec.newFieldInGroup("deduction_Rec_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        deduction_Rec_Ph_First_Name = deduction_Rec.newFieldInGroup("deduction_Rec_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        deduction_Rec_Ph_First_NameRedef1 = deduction_Rec.newGroupInGroup("deduction_Rec_Ph_First_NameRedef1", "Redefines", deduction_Rec_Ph_First_Name);
        deduction_Rec_Ph_First_Init = deduction_Rec_Ph_First_NameRedef1.newFieldInGroup("deduction_Rec_Ph_First_Init", "PH-FIRST-INIT", FieldType.STRING, 
            1);
        deduction_Rec_Ph_Middle_Name = deduction_Rec.newFieldInGroup("deduction_Rec_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        deduction_Rec_Ph_Middle_NameRedef2 = deduction_Rec.newGroupInGroup("deduction_Rec_Ph_Middle_NameRedef2", "Redefines", deduction_Rec_Ph_Middle_Name);
        deduction_Rec_Ph_Middle_Init = deduction_Rec_Ph_Middle_NameRedef2.newFieldInGroup("deduction_Rec_Ph_Middle_Init", "PH-MIDDLE-INIT", FieldType.STRING, 
            1);
        deduction_Rec_Annt_Soc_Sec_Nbr = deduction_Rec.newFieldInGroup("deduction_Rec_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        deduction_Rec_Pymnt_Ded_Cde = deduction_Rec.newFieldInGroup("deduction_Rec_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3);
        deduction_Rec_Pymnt_Ded_Amt = deduction_Rec.newFieldInGroup("deduction_Rec_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        deduction_Rec_Pymnt_Ded_Payee_Cde = deduction_Rec.newFieldInGroup("deduction_Rec_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 
            8);
        deduction_Rec_Pymnt_Ded_Payee_Seq = deduction_Rec.newFieldInGroup("deduction_Rec_Pymnt_Ded_Payee_Seq", "PYMNT-DED-PAYEE-SEQ", FieldType.STRING, 
            3);
        deduction_Rec_Pymnt_Suspend_Cde = deduction_Rec.newFieldInGroup("deduction_Rec_Pymnt_Suspend_Cde", "PYMNT-SUSPEND-CDE", FieldType.STRING, 4);
        deduction_Rec_Pymnt_Check_Dte = deduction_Rec.newFieldInGroup("deduction_Rec_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        deduction_Rec_Extra_Space = deduction_Rec.newFieldInGroup("deduction_Rec_Extra_Space", "EXTRA-SPACE", FieldType.STRING, 80);

        this.setRecordName("LdaFcpl804");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl804() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
