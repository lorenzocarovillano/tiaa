/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:32 PM
**        * FROM NATURAL PDA     : FCPANZCN
************************************************************
**        * FILE NAME            : PdaFcpanzcn.java
**        * CLASS NAME           : PdaFcpanzcn
**        * INSTANCE NAME        : PdaFcpanzcn
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpanzcn extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpanzcn;
    private DbsField pnd_Fcpanzcn_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Fcpanzcn_Cntrct_Orgn_Cde;
    private DbsField pnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Fcpanzcn_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Fcpanzcn_Pymnt_Check_Amt;

    public DbsGroup getPnd_Fcpanzcn() { return pnd_Fcpanzcn; }

    public DbsField getPnd_Fcpanzcn_Pymnt_Pay_Type_Req_Ind() { return pnd_Fcpanzcn_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPnd_Fcpanzcn_Cntrct_Orgn_Cde() { return pnd_Fcpanzcn_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPnd_Fcpanzcn_Cntrct_Roll_Dest_Cde() { return pnd_Fcpanzcn_Cntrct_Roll_Dest_Cde; }

    public DbsField getPnd_Fcpanzcn_Pymnt_Check_Amt() { return pnd_Fcpanzcn_Pymnt_Check_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpanzcn = dbsRecord.newGroupInRecord("pnd_Fcpanzcn", "#FCPANZCN");
        pnd_Fcpanzcn.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpanzcn_Pymnt_Pay_Type_Req_Ind = pnd_Fcpanzcn.newFieldInGroup("pnd_Fcpanzcn_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        pnd_Fcpanzcn_Cntrct_Orgn_Cde = pnd_Fcpanzcn.newFieldInGroup("pnd_Fcpanzcn_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Fcpanzcn.newFieldInGroup("pnd_Fcpanzcn_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Fcpanzcn_Cntrct_Roll_Dest_Cde = pnd_Fcpanzcn.newFieldInGroup("pnd_Fcpanzcn_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 
            4);
        pnd_Fcpanzcn_Pymnt_Check_Amt = pnd_Fcpanzcn.newFieldInGroup("pnd_Fcpanzcn_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9,2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpanzcn(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

