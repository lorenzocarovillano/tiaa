/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:28 PM
**        * FROM NATURAL PDA     : FCPA874A
************************************************************
**        * FILE NAME            : PdaFcpa874a.java
**        * CLASS NAME           : PdaFcpa874a
**        * INSTANCE NAME        : PdaFcpa874a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa874a extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpa874a;
    private DbsGroup pnd_Fcpa874a_Pymnt_Nme_And_Addr_Grp;
    private DbsField pnd_Fcpa874a_Pymnt_Nme;
    private DbsField pnd_Fcpa874a_Pymnt_Addr_Line_Txt;
    private DbsField pnd_Fcpa874a_Pymnt_Postl_Data;
    private DbsField pnd_Fcpa874a_Pnd_Stmnt_Pymnt_Nme;

    public DbsGroup getPnd_Fcpa874a() { return pnd_Fcpa874a; }

    public DbsGroup getPnd_Fcpa874a_Pymnt_Nme_And_Addr_Grp() { return pnd_Fcpa874a_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getPnd_Fcpa874a_Pymnt_Nme() { return pnd_Fcpa874a_Pymnt_Nme; }

    public DbsField getPnd_Fcpa874a_Pymnt_Addr_Line_Txt() { return pnd_Fcpa874a_Pymnt_Addr_Line_Txt; }

    public DbsField getPnd_Fcpa874a_Pymnt_Postl_Data() { return pnd_Fcpa874a_Pymnt_Postl_Data; }

    public DbsField getPnd_Fcpa874a_Pnd_Stmnt_Pymnt_Nme() { return pnd_Fcpa874a_Pnd_Stmnt_Pymnt_Nme; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpa874a = dbsRecord.newGroupInRecord("pnd_Fcpa874a", "#FCPA874A");
        pnd_Fcpa874a.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpa874a_Pymnt_Nme_And_Addr_Grp = pnd_Fcpa874a.newGroupArrayInGroup("pnd_Fcpa874a_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", new DbsArrayController(1,
            2));
        pnd_Fcpa874a_Pymnt_Nme = pnd_Fcpa874a_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Fcpa874a_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38);
        pnd_Fcpa874a_Pymnt_Addr_Line_Txt = pnd_Fcpa874a_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("pnd_Fcpa874a_Pymnt_Addr_Line_Txt", "PYMNT-ADDR-LINE-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1,6));
        pnd_Fcpa874a_Pymnt_Postl_Data = pnd_Fcpa874a_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Fcpa874a_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", FieldType.STRING, 
            32);
        pnd_Fcpa874a_Pnd_Stmnt_Pymnt_Nme = pnd_Fcpa874a.newFieldInGroup("pnd_Fcpa874a_Pnd_Stmnt_Pymnt_Nme", "#STMNT-PYMNT-NME", FieldType.STRING, 38);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa874a(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

