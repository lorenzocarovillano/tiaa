/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:57:03 PM
**        * FROM NATURAL LDA     : CWFLCOUP
************************************************************
**        * FILE NAME            : LdaCwflcoup.java
**        * CLASS NAME           : LdaCwflcoup
**        * INSTANCE NAME        : LdaCwflcoup
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCwflcoup extends DbsRecord
{
    // Properties
    private DbsGroup contract_Upd;
    private DbsField contract_Upd_Rqst_Log_Dte_Tme;
    private DbsField contract_Upd_Cntrct_Seq_Num;
    private DbsField contract_Upd_Cntrct_Num;

    public DbsGroup getContract_Upd() { return contract_Upd; }

    public DbsField getContract_Upd_Rqst_Log_Dte_Tme() { return contract_Upd_Rqst_Log_Dte_Tme; }

    public DbsField getContract_Upd_Cntrct_Seq_Num() { return contract_Upd_Cntrct_Seq_Num; }

    public DbsField getContract_Upd_Cntrct_Num() { return contract_Upd_Cntrct_Num; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        contract_Upd = newGroupInRecord("contract_Upd", "CONTRACT-UPD");
        contract_Upd_Rqst_Log_Dte_Tme = contract_Upd.newFieldInGroup("contract_Upd_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 26);
        contract_Upd_Cntrct_Seq_Num = contract_Upd.newFieldInGroup("contract_Upd_Cntrct_Seq_Num", "CNTRCT-SEQ-NUM", FieldType.NUMERIC, 2);
        contract_Upd_Cntrct_Num = contract_Upd.newFieldInGroup("contract_Upd_Cntrct_Num", "CNTRCT-NUM", FieldType.STRING, 8);

        this.setRecordName("LdaCwflcoup");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCwflcoup() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
