/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:13 PM
**        * FROM NATURAL PDA     : FCPA121
************************************************************
**        * FILE NAME            : PdaFcpa121.java
**        * CLASS NAME           : PdaFcpa121
**        * INSTANCE NAME        : PdaFcpa121
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa121 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpa121;
    private DbsField pnd_Fcpa121_Inv_Acct_Isa;
    private DbsField pnd_Fcpa121_Inv_Acct_Ledgr_Nbr;
    private DbsField pnd_Fcpa121_Inv_Acct_Ledgr_Desc;

    public DbsGroup getPnd_Fcpa121() { return pnd_Fcpa121; }

    public DbsField getPnd_Fcpa121_Inv_Acct_Isa() { return pnd_Fcpa121_Inv_Acct_Isa; }

    public DbsField getPnd_Fcpa121_Inv_Acct_Ledgr_Nbr() { return pnd_Fcpa121_Inv_Acct_Ledgr_Nbr; }

    public DbsField getPnd_Fcpa121_Inv_Acct_Ledgr_Desc() { return pnd_Fcpa121_Inv_Acct_Ledgr_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpa121 = dbsRecord.newGroupInRecord("pnd_Fcpa121", "#FCPA121");
        pnd_Fcpa121.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpa121_Inv_Acct_Isa = pnd_Fcpa121.newFieldInGroup("pnd_Fcpa121_Inv_Acct_Isa", "INV-ACCT-ISA", FieldType.STRING, 5);
        pnd_Fcpa121_Inv_Acct_Ledgr_Nbr = pnd_Fcpa121.newFieldInGroup("pnd_Fcpa121_Inv_Acct_Ledgr_Nbr", "INV-ACCT-LEDGR-NBR", FieldType.STRING, 15);
        pnd_Fcpa121_Inv_Acct_Ledgr_Desc = pnd_Fcpa121.newFieldInGroup("pnd_Fcpa121_Inv_Acct_Ledgr_Desc", "INV-ACCT-LEDGR-DESC", FieldType.STRING, 40);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa121(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

