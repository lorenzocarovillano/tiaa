/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:07 PM
**        * FROM NATURAL LDA     : FCPL280
************************************************************
**        * FILE NAME            : LdaFcpl280.java
**        * CLASS NAME           : LdaFcpl280
**        * INSTANCE NAME        : LdaFcpl280
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl280 extends DbsRecord
{
    // Properties
    private DbsGroup ws_File_Header_Record_1;
    private DbsField ws_File_Header_Record_1_Ws_Record_Type_1;
    private DbsField ws_File_Header_Record_1_Ws_Priority_Code_1;
    private DbsField ws_File_Header_Record_1_Ws_Immediate_Destination_1;
    private DbsField ws_File_Header_Record_1_Ws_Immediate_Origin_1;
    private DbsField ws_File_Header_Record_1_Ws_File_Creation_Date_1;
    private DbsGroup ws_File_Header_Record_1_Ws_File_Creation_Date_1Redef1;
    private DbsField ws_File_Header_Record_1_Ws_File_Creation_Date_Yy_1;
    private DbsField ws_File_Header_Record_1_Ws_File_Creation_Date_Mm_1;
    private DbsField ws_File_Header_Record_1_Ws_File_Creation_Date_Dd_1;
    private DbsField ws_File_Header_Record_1_Ws_File_Creation_Time_1;
    private DbsGroup ws_File_Header_Record_1_Ws_File_Creation_Time_1Redef2;
    private DbsField ws_File_Header_Record_1_Ws_File_Creation_Time_Hh_1;
    private DbsField ws_File_Header_Record_1_Ws_File_Creation_Time_Mm_1;
    private DbsField ws_File_Header_Record_1_Ws_File_Id_Modifier_1;
    private DbsField ws_File_Header_Record_1_Ws_Record_Size_1;
    private DbsField ws_File_Header_Record_1_Ws_Blocking_Factor_1;
    private DbsField ws_File_Header_Record_1_Ws_Format_Code_1;
    private DbsField ws_File_Header_Record_1_Ws_Destination_1;
    private DbsField ws_File_Header_Record_1_Ws_Origin_1;
    private DbsField ws_File_Header_Record_1_Ws_Reference_Code_1;
    private DbsGroup ws_File_Header_Record_1Redef3;
    private DbsField ws_File_Header_Record_1_Ws_Record_Type_5;
    private DbsField ws_File_Header_Record_1_Ws_Service_Class_5;
    private DbsField ws_File_Header_Record_1_Ws_Company_Name_5;
    private DbsField ws_File_Header_Record_1_Ws_Company_Discretion_Data_5;
    private DbsField ws_File_Header_Record_1_Ws_Company_Id_5;
    private DbsField ws_File_Header_Record_1_Ws_Standard_Entry_Class_5;
    private DbsField ws_File_Header_Record_1_Ws_Company_Entry_Description_5;
    private DbsField ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5;
    private DbsGroup ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5Redef4;
    private DbsField ws_File_Header_Record_1_Ws_Company_Descriptive_Dt_Mon_5;
    private DbsField ws_File_Header_Record_1_Ws_Filler_5;
    private DbsField ws_File_Header_Record_1_Ws_Company_Descriptive_Dt_Yy_5;
    private DbsField ws_File_Header_Record_1_Ws_Effective_Entry_Date_5;
    private DbsGroup ws_File_Header_Record_1_Ws_Effective_Entry_Date_5Redef5;
    private DbsField ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Yy_5;
    private DbsField ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Mm_5;
    private DbsField ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Dd_5;
    private DbsField ws_File_Header_Record_1_Ws_Settlement_Date_5;
    private DbsField ws_File_Header_Record_1_Ws_Originator_Status_Code_5;
    private DbsField ws_File_Header_Record_1_Ws_Originator_Id_5;
    private DbsField ws_File_Header_Record_1_Ws_Batch_Number_5;
    private DbsGroup ws_File_Header_Record_1Redef6;
    private DbsField ws_File_Header_Record_1_Ws_Record_Type_6;
    private DbsField ws_File_Header_Record_1_Ws_Transaction_Code_6;
    private DbsField ws_File_Header_Record_1_Ws_Dfi_Id_N_Check_Digit;
    private DbsGroup ws_File_Header_Record_1_Ws_Dfi_Id_N_Check_DigitRedef7;
    private DbsField ws_File_Header_Record_1_Ws_Dfi_Id_6;
    private DbsField ws_File_Header_Record_1_Ws_Dfi_Id_Check_Digit_6;
    private DbsField ws_File_Header_Record_1_Ws_Dfi_Acct_Number_6;
    private DbsField ws_File_Header_Record_1_Ws_Amount_6;
    private DbsField ws_File_Header_Record_1_Ws_Individual_Id_6;
    private DbsField ws_File_Header_Record_1_Ws_Individual_Name_6;
    private DbsField ws_File_Header_Record_1_Ws_Discretionary_Data_6;
    private DbsField ws_File_Header_Record_1_Ws_Addenda_Record_Indicator_6;
    private DbsField ws_File_Header_Record_1_Ws_Trace_6;
    private DbsGroup ws_File_Header_Record_1_Ws_Trace_6Redef8;
    private DbsField ws_File_Header_Record_1_Ws_Prefix_Trace_6;
    private DbsField ws_File_Header_Record_1_Ws_Trace_Number_6;
    private DbsField ws_File_Header_Record_1_Ws_Suffix_Trace_6;
    private DbsGroup ws_File_Header_Record_1Redef9;
    private DbsField ws_File_Header_Record_1_Ws_Record_Type_7;
    private DbsField ws_File_Header_Record_1_Ws_Addenda_Type_Code_7;
    private DbsField ws_File_Header_Record_1_Ws_Free_Form_7;
    private DbsField ws_File_Header_Record_1_Ws_Specl_Addenda_Sequence_Nmbr_7;
    private DbsField ws_File_Header_Record_1_Ws_Entry_Detail_Sequence_Nmbr_7;
    private DbsGroup ws_File_Header_Record_1Redef10;
    private DbsField ws_File_Header_Record_1_Ws_Record_Type_8;
    private DbsField ws_File_Header_Record_1_Ws_Service_Class_8;
    private DbsField ws_File_Header_Record_1_Ws_Entry_Addenda_Count_8;
    private DbsField ws_File_Header_Record_1_Ws_Entry_Hash_8;
    private DbsField ws_File_Header_Record_1_Ws_Total_Debit_8;
    private DbsField ws_File_Header_Record_1_Ws_Total_Credit_8;
    private DbsField ws_File_Header_Record_1_Ws_Company_Id_8;
    private DbsField ws_File_Header_Record_1_Filler_8;
    private DbsField ws_File_Header_Record_1_Ws_Originator_Id_8;
    private DbsField ws_File_Header_Record_1_Ws_Batch_Number_8;
    private DbsGroup ws_File_Header_Record_1Redef11;
    private DbsField ws_File_Header_Record_1_Ws_Record_Type_9;
    private DbsField ws_File_Header_Record_1_Ws_Batch_Count_9;
    private DbsField ws_File_Header_Record_1_Ws_Block_Count_9;
    private DbsField ws_File_Header_Record_1_Ws_Entry_Addenda_Count_9;
    private DbsField ws_File_Header_Record_1_Ws_Entry_Hash_9;
    private DbsField ws_File_Header_Record_1_Ws_Total_Debit_9;
    private DbsField ws_File_Header_Record_1_Ws_Total_Credit_9;
    private DbsField ws_File_Header_Record_1_Filler_9;

    public DbsGroup getWs_File_Header_Record_1() { return ws_File_Header_Record_1; }

    public DbsField getWs_File_Header_Record_1_Ws_Record_Type_1() { return ws_File_Header_Record_1_Ws_Record_Type_1; }

    public DbsField getWs_File_Header_Record_1_Ws_Priority_Code_1() { return ws_File_Header_Record_1_Ws_Priority_Code_1; }

    public DbsField getWs_File_Header_Record_1_Ws_Immediate_Destination_1() { return ws_File_Header_Record_1_Ws_Immediate_Destination_1; }

    public DbsField getWs_File_Header_Record_1_Ws_Immediate_Origin_1() { return ws_File_Header_Record_1_Ws_Immediate_Origin_1; }

    public DbsField getWs_File_Header_Record_1_Ws_File_Creation_Date_1() { return ws_File_Header_Record_1_Ws_File_Creation_Date_1; }

    public DbsGroup getWs_File_Header_Record_1_Ws_File_Creation_Date_1Redef1() { return ws_File_Header_Record_1_Ws_File_Creation_Date_1Redef1; }

    public DbsField getWs_File_Header_Record_1_Ws_File_Creation_Date_Yy_1() { return ws_File_Header_Record_1_Ws_File_Creation_Date_Yy_1; }

    public DbsField getWs_File_Header_Record_1_Ws_File_Creation_Date_Mm_1() { return ws_File_Header_Record_1_Ws_File_Creation_Date_Mm_1; }

    public DbsField getWs_File_Header_Record_1_Ws_File_Creation_Date_Dd_1() { return ws_File_Header_Record_1_Ws_File_Creation_Date_Dd_1; }

    public DbsField getWs_File_Header_Record_1_Ws_File_Creation_Time_1() { return ws_File_Header_Record_1_Ws_File_Creation_Time_1; }

    public DbsGroup getWs_File_Header_Record_1_Ws_File_Creation_Time_1Redef2() { return ws_File_Header_Record_1_Ws_File_Creation_Time_1Redef2; }

    public DbsField getWs_File_Header_Record_1_Ws_File_Creation_Time_Hh_1() { return ws_File_Header_Record_1_Ws_File_Creation_Time_Hh_1; }

    public DbsField getWs_File_Header_Record_1_Ws_File_Creation_Time_Mm_1() { return ws_File_Header_Record_1_Ws_File_Creation_Time_Mm_1; }

    public DbsField getWs_File_Header_Record_1_Ws_File_Id_Modifier_1() { return ws_File_Header_Record_1_Ws_File_Id_Modifier_1; }

    public DbsField getWs_File_Header_Record_1_Ws_Record_Size_1() { return ws_File_Header_Record_1_Ws_Record_Size_1; }

    public DbsField getWs_File_Header_Record_1_Ws_Blocking_Factor_1() { return ws_File_Header_Record_1_Ws_Blocking_Factor_1; }

    public DbsField getWs_File_Header_Record_1_Ws_Format_Code_1() { return ws_File_Header_Record_1_Ws_Format_Code_1; }

    public DbsField getWs_File_Header_Record_1_Ws_Destination_1() { return ws_File_Header_Record_1_Ws_Destination_1; }

    public DbsField getWs_File_Header_Record_1_Ws_Origin_1() { return ws_File_Header_Record_1_Ws_Origin_1; }

    public DbsField getWs_File_Header_Record_1_Ws_Reference_Code_1() { return ws_File_Header_Record_1_Ws_Reference_Code_1; }

    public DbsGroup getWs_File_Header_Record_1Redef3() { return ws_File_Header_Record_1Redef3; }

    public DbsField getWs_File_Header_Record_1_Ws_Record_Type_5() { return ws_File_Header_Record_1_Ws_Record_Type_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Service_Class_5() { return ws_File_Header_Record_1_Ws_Service_Class_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Company_Name_5() { return ws_File_Header_Record_1_Ws_Company_Name_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Company_Discretion_Data_5() { return ws_File_Header_Record_1_Ws_Company_Discretion_Data_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Company_Id_5() { return ws_File_Header_Record_1_Ws_Company_Id_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Standard_Entry_Class_5() { return ws_File_Header_Record_1_Ws_Standard_Entry_Class_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Company_Entry_Description_5() { return ws_File_Header_Record_1_Ws_Company_Entry_Description_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Company_Descriptive_Date_5() { return ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5; }

    public DbsGroup getWs_File_Header_Record_1_Ws_Company_Descriptive_Date_5Redef4() { return ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5Redef4; 
        }

    public DbsField getWs_File_Header_Record_1_Ws_Company_Descriptive_Dt_Mon_5() { return ws_File_Header_Record_1_Ws_Company_Descriptive_Dt_Mon_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Filler_5() { return ws_File_Header_Record_1_Ws_Filler_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Company_Descriptive_Dt_Yy_5() { return ws_File_Header_Record_1_Ws_Company_Descriptive_Dt_Yy_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Effective_Entry_Date_5() { return ws_File_Header_Record_1_Ws_Effective_Entry_Date_5; }

    public DbsGroup getWs_File_Header_Record_1_Ws_Effective_Entry_Date_5Redef5() { return ws_File_Header_Record_1_Ws_Effective_Entry_Date_5Redef5; }

    public DbsField getWs_File_Header_Record_1_Ws_Effective_Entry_Dt_Yy_5() { return ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Yy_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Effective_Entry_Dt_Mm_5() { return ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Mm_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Effective_Entry_Dt_Dd_5() { return ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Dd_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Settlement_Date_5() { return ws_File_Header_Record_1_Ws_Settlement_Date_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Originator_Status_Code_5() { return ws_File_Header_Record_1_Ws_Originator_Status_Code_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Originator_Id_5() { return ws_File_Header_Record_1_Ws_Originator_Id_5; }

    public DbsField getWs_File_Header_Record_1_Ws_Batch_Number_5() { return ws_File_Header_Record_1_Ws_Batch_Number_5; }

    public DbsGroup getWs_File_Header_Record_1Redef6() { return ws_File_Header_Record_1Redef6; }

    public DbsField getWs_File_Header_Record_1_Ws_Record_Type_6() { return ws_File_Header_Record_1_Ws_Record_Type_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Transaction_Code_6() { return ws_File_Header_Record_1_Ws_Transaction_Code_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Dfi_Id_N_Check_Digit() { return ws_File_Header_Record_1_Ws_Dfi_Id_N_Check_Digit; }

    public DbsGroup getWs_File_Header_Record_1_Ws_Dfi_Id_N_Check_DigitRedef7() { return ws_File_Header_Record_1_Ws_Dfi_Id_N_Check_DigitRedef7; }

    public DbsField getWs_File_Header_Record_1_Ws_Dfi_Id_6() { return ws_File_Header_Record_1_Ws_Dfi_Id_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Dfi_Id_Check_Digit_6() { return ws_File_Header_Record_1_Ws_Dfi_Id_Check_Digit_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Dfi_Acct_Number_6() { return ws_File_Header_Record_1_Ws_Dfi_Acct_Number_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Amount_6() { return ws_File_Header_Record_1_Ws_Amount_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Individual_Id_6() { return ws_File_Header_Record_1_Ws_Individual_Id_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Individual_Name_6() { return ws_File_Header_Record_1_Ws_Individual_Name_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Discretionary_Data_6() { return ws_File_Header_Record_1_Ws_Discretionary_Data_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Addenda_Record_Indicator_6() { return ws_File_Header_Record_1_Ws_Addenda_Record_Indicator_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Trace_6() { return ws_File_Header_Record_1_Ws_Trace_6; }

    public DbsGroup getWs_File_Header_Record_1_Ws_Trace_6Redef8() { return ws_File_Header_Record_1_Ws_Trace_6Redef8; }

    public DbsField getWs_File_Header_Record_1_Ws_Prefix_Trace_6() { return ws_File_Header_Record_1_Ws_Prefix_Trace_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Trace_Number_6() { return ws_File_Header_Record_1_Ws_Trace_Number_6; }

    public DbsField getWs_File_Header_Record_1_Ws_Suffix_Trace_6() { return ws_File_Header_Record_1_Ws_Suffix_Trace_6; }

    public DbsGroup getWs_File_Header_Record_1Redef9() { return ws_File_Header_Record_1Redef9; }

    public DbsField getWs_File_Header_Record_1_Ws_Record_Type_7() { return ws_File_Header_Record_1_Ws_Record_Type_7; }

    public DbsField getWs_File_Header_Record_1_Ws_Addenda_Type_Code_7() { return ws_File_Header_Record_1_Ws_Addenda_Type_Code_7; }

    public DbsField getWs_File_Header_Record_1_Ws_Free_Form_7() { return ws_File_Header_Record_1_Ws_Free_Form_7; }

    public DbsField getWs_File_Header_Record_1_Ws_Specl_Addenda_Sequence_Nmbr_7() { return ws_File_Header_Record_1_Ws_Specl_Addenda_Sequence_Nmbr_7; }

    public DbsField getWs_File_Header_Record_1_Ws_Entry_Detail_Sequence_Nmbr_7() { return ws_File_Header_Record_1_Ws_Entry_Detail_Sequence_Nmbr_7; }

    public DbsGroup getWs_File_Header_Record_1Redef10() { return ws_File_Header_Record_1Redef10; }

    public DbsField getWs_File_Header_Record_1_Ws_Record_Type_8() { return ws_File_Header_Record_1_Ws_Record_Type_8; }

    public DbsField getWs_File_Header_Record_1_Ws_Service_Class_8() { return ws_File_Header_Record_1_Ws_Service_Class_8; }

    public DbsField getWs_File_Header_Record_1_Ws_Entry_Addenda_Count_8() { return ws_File_Header_Record_1_Ws_Entry_Addenda_Count_8; }

    public DbsField getWs_File_Header_Record_1_Ws_Entry_Hash_8() { return ws_File_Header_Record_1_Ws_Entry_Hash_8; }

    public DbsField getWs_File_Header_Record_1_Ws_Total_Debit_8() { return ws_File_Header_Record_1_Ws_Total_Debit_8; }

    public DbsField getWs_File_Header_Record_1_Ws_Total_Credit_8() { return ws_File_Header_Record_1_Ws_Total_Credit_8; }

    public DbsField getWs_File_Header_Record_1_Ws_Company_Id_8() { return ws_File_Header_Record_1_Ws_Company_Id_8; }

    public DbsField getWs_File_Header_Record_1_Filler_8() { return ws_File_Header_Record_1_Filler_8; }

    public DbsField getWs_File_Header_Record_1_Ws_Originator_Id_8() { return ws_File_Header_Record_1_Ws_Originator_Id_8; }

    public DbsField getWs_File_Header_Record_1_Ws_Batch_Number_8() { return ws_File_Header_Record_1_Ws_Batch_Number_8; }

    public DbsGroup getWs_File_Header_Record_1Redef11() { return ws_File_Header_Record_1Redef11; }

    public DbsField getWs_File_Header_Record_1_Ws_Record_Type_9() { return ws_File_Header_Record_1_Ws_Record_Type_9; }

    public DbsField getWs_File_Header_Record_1_Ws_Batch_Count_9() { return ws_File_Header_Record_1_Ws_Batch_Count_9; }

    public DbsField getWs_File_Header_Record_1_Ws_Block_Count_9() { return ws_File_Header_Record_1_Ws_Block_Count_9; }

    public DbsField getWs_File_Header_Record_1_Ws_Entry_Addenda_Count_9() { return ws_File_Header_Record_1_Ws_Entry_Addenda_Count_9; }

    public DbsField getWs_File_Header_Record_1_Ws_Entry_Hash_9() { return ws_File_Header_Record_1_Ws_Entry_Hash_9; }

    public DbsField getWs_File_Header_Record_1_Ws_Total_Debit_9() { return ws_File_Header_Record_1_Ws_Total_Debit_9; }

    public DbsField getWs_File_Header_Record_1_Ws_Total_Credit_9() { return ws_File_Header_Record_1_Ws_Total_Credit_9; }

    public DbsField getWs_File_Header_Record_1_Filler_9() { return ws_File_Header_Record_1_Filler_9; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ws_File_Header_Record_1 = newGroupInRecord("ws_File_Header_Record_1", "WS-FILE-HEADER-RECORD-1");
        ws_File_Header_Record_1_Ws_Record_Type_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_Record_Type_1", "WS-RECORD-TYPE-1", 
            FieldType.STRING, 1);
        ws_File_Header_Record_1_Ws_Priority_Code_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_Priority_Code_1", "WS-PRIORITY-CODE-1", 
            FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_Immediate_Destination_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_Immediate_Destination_1", 
            "WS-IMMEDIATE-DESTINATION-1", FieldType.STRING, 10);
        ws_File_Header_Record_1_Ws_Immediate_Origin_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_Immediate_Origin_1", "WS-IMMEDIATE-ORIGIN-1", 
            FieldType.STRING, 10);
        ws_File_Header_Record_1_Ws_File_Creation_Date_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_File_Creation_Date_1", "WS-FILE-CREATION-DATE-1", 
            FieldType.NUMERIC, 6);
        ws_File_Header_Record_1_Ws_File_Creation_Date_1Redef1 = ws_File_Header_Record_1.newGroupInGroup("ws_File_Header_Record_1_Ws_File_Creation_Date_1Redef1", 
            "Redefines", ws_File_Header_Record_1_Ws_File_Creation_Date_1);
        ws_File_Header_Record_1_Ws_File_Creation_Date_Yy_1 = ws_File_Header_Record_1_Ws_File_Creation_Date_1Redef1.newFieldInGroup("ws_File_Header_Record_1_Ws_File_Creation_Date_Yy_1", 
            "WS-FILE-CREATION-DATE-YY-1", FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_File_Creation_Date_Mm_1 = ws_File_Header_Record_1_Ws_File_Creation_Date_1Redef1.newFieldInGroup("ws_File_Header_Record_1_Ws_File_Creation_Date_Mm_1", 
            "WS-FILE-CREATION-DATE-MM-1", FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_File_Creation_Date_Dd_1 = ws_File_Header_Record_1_Ws_File_Creation_Date_1Redef1.newFieldInGroup("ws_File_Header_Record_1_Ws_File_Creation_Date_Dd_1", 
            "WS-FILE-CREATION-DATE-DD-1", FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_File_Creation_Time_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_File_Creation_Time_1", "WS-FILE-CREATION-TIME-1", 
            FieldType.NUMERIC, 4);
        ws_File_Header_Record_1_Ws_File_Creation_Time_1Redef2 = ws_File_Header_Record_1.newGroupInGroup("ws_File_Header_Record_1_Ws_File_Creation_Time_1Redef2", 
            "Redefines", ws_File_Header_Record_1_Ws_File_Creation_Time_1);
        ws_File_Header_Record_1_Ws_File_Creation_Time_Hh_1 = ws_File_Header_Record_1_Ws_File_Creation_Time_1Redef2.newFieldInGroup("ws_File_Header_Record_1_Ws_File_Creation_Time_Hh_1", 
            "WS-FILE-CREATION-TIME-HH-1", FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_File_Creation_Time_Mm_1 = ws_File_Header_Record_1_Ws_File_Creation_Time_1Redef2.newFieldInGroup("ws_File_Header_Record_1_Ws_File_Creation_Time_Mm_1", 
            "WS-FILE-CREATION-TIME-MM-1", FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_File_Id_Modifier_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_File_Id_Modifier_1", "WS-FILE-ID-MODIFIER-1", 
            FieldType.STRING, 1);
        ws_File_Header_Record_1_Ws_Record_Size_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_Record_Size_1", "WS-RECORD-SIZE-1", 
            FieldType.NUMERIC, 3);
        ws_File_Header_Record_1_Ws_Blocking_Factor_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_Blocking_Factor_1", "WS-BLOCKING-FACTOR-1", 
            FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_Format_Code_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_Format_Code_1", "WS-FORMAT-CODE-1", 
            FieldType.NUMERIC, 1);
        ws_File_Header_Record_1_Ws_Destination_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_Destination_1", "WS-DESTINATION-1", 
            FieldType.STRING, 23);
        ws_File_Header_Record_1_Ws_Origin_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_Origin_1", "WS-ORIGIN-1", FieldType.STRING, 
            23);
        ws_File_Header_Record_1_Ws_Reference_Code_1 = ws_File_Header_Record_1.newFieldInGroup("ws_File_Header_Record_1_Ws_Reference_Code_1", "WS-REFERENCE-CODE-1", 
            FieldType.STRING, 8);
        ws_File_Header_Record_1Redef3 = newGroupInRecord("ws_File_Header_Record_1Redef3", "Redefines", ws_File_Header_Record_1);
        ws_File_Header_Record_1_Ws_Record_Type_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Record_Type_5", "WS-RECORD-TYPE-5", 
            FieldType.STRING, 1);
        ws_File_Header_Record_1_Ws_Service_Class_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Service_Class_5", "WS-SERVICE-CLASS-5", 
            FieldType.NUMERIC, 3);
        ws_File_Header_Record_1_Ws_Company_Name_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Company_Name_5", "WS-COMPANY-NAME-5", 
            FieldType.STRING, 16);
        ws_File_Header_Record_1_Ws_Company_Discretion_Data_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Company_Discretion_Data_5", 
            "WS-COMPANY-DISCRETION-DATA-5", FieldType.STRING, 20);
        ws_File_Header_Record_1_Ws_Company_Id_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Company_Id_5", "WS-COMPANY-ID-5", 
            FieldType.STRING, 10);
        ws_File_Header_Record_1_Ws_Standard_Entry_Class_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Standard_Entry_Class_5", 
            "WS-STANDARD-ENTRY-CLASS-5", FieldType.STRING, 3);
        ws_File_Header_Record_1_Ws_Company_Entry_Description_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Company_Entry_Description_5", 
            "WS-COMPANY-ENTRY-DESCRIPTION-5", FieldType.STRING, 10);
        ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5", 
            "WS-COMPANY-DESCRIPTIVE-DATE-5", FieldType.STRING, 6);
        ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5Redef4 = ws_File_Header_Record_1Redef3.newGroupInGroup("ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5Redef4", 
            "Redefines", ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5);
        ws_File_Header_Record_1_Ws_Company_Descriptive_Dt_Mon_5 = ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5Redef4.newFieldInGroup("ws_File_Header_Record_1_Ws_Company_Descriptive_Dt_Mon_5", 
            "WS-COMPANY-DESCRIPTIVE-DT-MON-5", FieldType.STRING, 3);
        ws_File_Header_Record_1_Ws_Filler_5 = ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5Redef4.newFieldInGroup("ws_File_Header_Record_1_Ws_Filler_5", 
            "WS-FILLER-5", FieldType.STRING, 1);
        ws_File_Header_Record_1_Ws_Company_Descriptive_Dt_Yy_5 = ws_File_Header_Record_1_Ws_Company_Descriptive_Date_5Redef4.newFieldInGroup("ws_File_Header_Record_1_Ws_Company_Descriptive_Dt_Yy_5", 
            "WS-COMPANY-DESCRIPTIVE-DT-YY-5", FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_Effective_Entry_Date_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Effective_Entry_Date_5", 
            "WS-EFFECTIVE-ENTRY-DATE-5", FieldType.STRING, 6);
        ws_File_Header_Record_1_Ws_Effective_Entry_Date_5Redef5 = ws_File_Header_Record_1Redef3.newGroupInGroup("ws_File_Header_Record_1_Ws_Effective_Entry_Date_5Redef5", 
            "Redefines", ws_File_Header_Record_1_Ws_Effective_Entry_Date_5);
        ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Yy_5 = ws_File_Header_Record_1_Ws_Effective_Entry_Date_5Redef5.newFieldInGroup("ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Yy_5", 
            "WS-EFFECTIVE-ENTRY-DT-YY-5", FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Mm_5 = ws_File_Header_Record_1_Ws_Effective_Entry_Date_5Redef5.newFieldInGroup("ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Mm_5", 
            "WS-EFFECTIVE-ENTRY-DT-MM-5", FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Dd_5 = ws_File_Header_Record_1_Ws_Effective_Entry_Date_5Redef5.newFieldInGroup("ws_File_Header_Record_1_Ws_Effective_Entry_Dt_Dd_5", 
            "WS-EFFECTIVE-ENTRY-DT-DD-5", FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_Settlement_Date_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Settlement_Date_5", "WS-SETTLEMENT-DATE-5", 
            FieldType.STRING, 3);
        ws_File_Header_Record_1_Ws_Originator_Status_Code_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Originator_Status_Code_5", 
            "WS-ORIGINATOR-STATUS-CODE-5", FieldType.STRING, 1);
        ws_File_Header_Record_1_Ws_Originator_Id_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Originator_Id_5", "WS-ORIGINATOR-ID-5", 
            FieldType.STRING, 8);
        ws_File_Header_Record_1_Ws_Batch_Number_5 = ws_File_Header_Record_1Redef3.newFieldInGroup("ws_File_Header_Record_1_Ws_Batch_Number_5", "WS-BATCH-NUMBER-5", 
            FieldType.NUMERIC, 7);
        ws_File_Header_Record_1Redef6 = newGroupInRecord("ws_File_Header_Record_1Redef6", "Redefines", ws_File_Header_Record_1);
        ws_File_Header_Record_1_Ws_Record_Type_6 = ws_File_Header_Record_1Redef6.newFieldInGroup("ws_File_Header_Record_1_Ws_Record_Type_6", "WS-RECORD-TYPE-6", 
            FieldType.STRING, 1);
        ws_File_Header_Record_1_Ws_Transaction_Code_6 = ws_File_Header_Record_1Redef6.newFieldInGroup("ws_File_Header_Record_1_Ws_Transaction_Code_6", 
            "WS-TRANSACTION-CODE-6", FieldType.NUMERIC, 2);
        ws_File_Header_Record_1_Ws_Dfi_Id_N_Check_Digit = ws_File_Header_Record_1Redef6.newFieldInGroup("ws_File_Header_Record_1_Ws_Dfi_Id_N_Check_Digit", 
            "WS-DFI-ID-N-CHECK-DIGIT", FieldType.NUMERIC, 9);
        ws_File_Header_Record_1_Ws_Dfi_Id_N_Check_DigitRedef7 = ws_File_Header_Record_1Redef6.newGroupInGroup("ws_File_Header_Record_1_Ws_Dfi_Id_N_Check_DigitRedef7", 
            "Redefines", ws_File_Header_Record_1_Ws_Dfi_Id_N_Check_Digit);
        ws_File_Header_Record_1_Ws_Dfi_Id_6 = ws_File_Header_Record_1_Ws_Dfi_Id_N_Check_DigitRedef7.newFieldInGroup("ws_File_Header_Record_1_Ws_Dfi_Id_6", 
            "WS-DFI-ID-6", FieldType.NUMERIC, 8);
        ws_File_Header_Record_1_Ws_Dfi_Id_Check_Digit_6 = ws_File_Header_Record_1_Ws_Dfi_Id_N_Check_DigitRedef7.newFieldInGroup("ws_File_Header_Record_1_Ws_Dfi_Id_Check_Digit_6", 
            "WS-DFI-ID-CHECK-DIGIT-6", FieldType.NUMERIC, 1);
        ws_File_Header_Record_1_Ws_Dfi_Acct_Number_6 = ws_File_Header_Record_1Redef6.newFieldInGroup("ws_File_Header_Record_1_Ws_Dfi_Acct_Number_6", "WS-DFI-ACCT-NUMBER-6", 
            FieldType.STRING, 17);
        ws_File_Header_Record_1_Ws_Amount_6 = ws_File_Header_Record_1Redef6.newFieldInGroup("ws_File_Header_Record_1_Ws_Amount_6", "WS-AMOUNT-6", FieldType.DECIMAL, 
            10,2);
        ws_File_Header_Record_1_Ws_Individual_Id_6 = ws_File_Header_Record_1Redef6.newFieldInGroup("ws_File_Header_Record_1_Ws_Individual_Id_6", "WS-INDIVIDUAL-ID-6", 
            FieldType.STRING, 15);
        ws_File_Header_Record_1_Ws_Individual_Name_6 = ws_File_Header_Record_1Redef6.newFieldInGroup("ws_File_Header_Record_1_Ws_Individual_Name_6", "WS-INDIVIDUAL-NAME-6", 
            FieldType.STRING, 22);
        ws_File_Header_Record_1_Ws_Discretionary_Data_6 = ws_File_Header_Record_1Redef6.newFieldInGroup("ws_File_Header_Record_1_Ws_Discretionary_Data_6", 
            "WS-DISCRETIONARY-DATA-6", FieldType.STRING, 2);
        ws_File_Header_Record_1_Ws_Addenda_Record_Indicator_6 = ws_File_Header_Record_1Redef6.newFieldInGroup("ws_File_Header_Record_1_Ws_Addenda_Record_Indicator_6", 
            "WS-ADDENDA-RECORD-INDICATOR-6", FieldType.NUMERIC, 1);
        ws_File_Header_Record_1_Ws_Trace_6 = ws_File_Header_Record_1Redef6.newFieldInGroup("ws_File_Header_Record_1_Ws_Trace_6", "WS-TRACE-6", FieldType.STRING, 
            15);
        ws_File_Header_Record_1_Ws_Trace_6Redef8 = ws_File_Header_Record_1Redef6.newGroupInGroup("ws_File_Header_Record_1_Ws_Trace_6Redef8", "Redefines", 
            ws_File_Header_Record_1_Ws_Trace_6);
        ws_File_Header_Record_1_Ws_Prefix_Trace_6 = ws_File_Header_Record_1_Ws_Trace_6Redef8.newFieldInGroup("ws_File_Header_Record_1_Ws_Prefix_Trace_6", 
            "WS-PREFIX-TRACE-6", FieldType.STRING, 8);
        ws_File_Header_Record_1_Ws_Trace_Number_6 = ws_File_Header_Record_1_Ws_Trace_6Redef8.newFieldInGroup("ws_File_Header_Record_1_Ws_Trace_Number_6", 
            "WS-TRACE-NUMBER-6", FieldType.NUMERIC, 6);
        ws_File_Header_Record_1_Ws_Suffix_Trace_6 = ws_File_Header_Record_1_Ws_Trace_6Redef8.newFieldInGroup("ws_File_Header_Record_1_Ws_Suffix_Trace_6", 
            "WS-SUFFIX-TRACE-6", FieldType.STRING, 1);
        ws_File_Header_Record_1Redef9 = newGroupInRecord("ws_File_Header_Record_1Redef9", "Redefines", ws_File_Header_Record_1);
        ws_File_Header_Record_1_Ws_Record_Type_7 = ws_File_Header_Record_1Redef9.newFieldInGroup("ws_File_Header_Record_1_Ws_Record_Type_7", "WS-RECORD-TYPE-7", 
            FieldType.STRING, 1);
        ws_File_Header_Record_1_Ws_Addenda_Type_Code_7 = ws_File_Header_Record_1Redef9.newFieldInGroup("ws_File_Header_Record_1_Ws_Addenda_Type_Code_7", 
            "WS-ADDENDA-TYPE-CODE-7", FieldType.STRING, 2);
        ws_File_Header_Record_1_Ws_Free_Form_7 = ws_File_Header_Record_1Redef9.newFieldInGroup("ws_File_Header_Record_1_Ws_Free_Form_7", "WS-FREE-FORM-7", 
            FieldType.STRING, 80);
        ws_File_Header_Record_1_Ws_Specl_Addenda_Sequence_Nmbr_7 = ws_File_Header_Record_1Redef9.newFieldInGroup("ws_File_Header_Record_1_Ws_Specl_Addenda_Sequence_Nmbr_7", 
            "WS-SPECL-ADDENDA-SEQUENCE-NMBR-7", FieldType.NUMERIC, 4);
        ws_File_Header_Record_1_Ws_Entry_Detail_Sequence_Nmbr_7 = ws_File_Header_Record_1Redef9.newFieldInGroup("ws_File_Header_Record_1_Ws_Entry_Detail_Sequence_Nmbr_7", 
            "WS-ENTRY-DETAIL-SEQUENCE-NMBR-7", FieldType.NUMERIC, 7);
        ws_File_Header_Record_1Redef10 = newGroupInRecord("ws_File_Header_Record_1Redef10", "Redefines", ws_File_Header_Record_1);
        ws_File_Header_Record_1_Ws_Record_Type_8 = ws_File_Header_Record_1Redef10.newFieldInGroup("ws_File_Header_Record_1_Ws_Record_Type_8", "WS-RECORD-TYPE-8", 
            FieldType.STRING, 1);
        ws_File_Header_Record_1_Ws_Service_Class_8 = ws_File_Header_Record_1Redef10.newFieldInGroup("ws_File_Header_Record_1_Ws_Service_Class_8", "WS-SERVICE-CLASS-8", 
            FieldType.NUMERIC, 3);
        ws_File_Header_Record_1_Ws_Entry_Addenda_Count_8 = ws_File_Header_Record_1Redef10.newFieldInGroup("ws_File_Header_Record_1_Ws_Entry_Addenda_Count_8", 
            "WS-ENTRY-ADDENDA-COUNT-8", FieldType.NUMERIC, 6);
        ws_File_Header_Record_1_Ws_Entry_Hash_8 = ws_File_Header_Record_1Redef10.newFieldInGroup("ws_File_Header_Record_1_Ws_Entry_Hash_8", "WS-ENTRY-HASH-8", 
            FieldType.NUMERIC, 10);
        ws_File_Header_Record_1_Ws_Total_Debit_8 = ws_File_Header_Record_1Redef10.newFieldInGroup("ws_File_Header_Record_1_Ws_Total_Debit_8", "WS-TOTAL-DEBIT-8", 
            FieldType.DECIMAL, 12,2);
        ws_File_Header_Record_1_Ws_Total_Credit_8 = ws_File_Header_Record_1Redef10.newFieldInGroup("ws_File_Header_Record_1_Ws_Total_Credit_8", "WS-TOTAL-CREDIT-8", 
            FieldType.DECIMAL, 12,2);
        ws_File_Header_Record_1_Ws_Company_Id_8 = ws_File_Header_Record_1Redef10.newFieldInGroup("ws_File_Header_Record_1_Ws_Company_Id_8", "WS-COMPANY-ID-8", 
            FieldType.STRING, 10);
        ws_File_Header_Record_1_Filler_8 = ws_File_Header_Record_1Redef10.newFieldInGroup("ws_File_Header_Record_1_Filler_8", "FILLER-8", FieldType.STRING, 
            25);
        ws_File_Header_Record_1_Ws_Originator_Id_8 = ws_File_Header_Record_1Redef10.newFieldInGroup("ws_File_Header_Record_1_Ws_Originator_Id_8", "WS-ORIGINATOR-ID-8", 
            FieldType.STRING, 8);
        ws_File_Header_Record_1_Ws_Batch_Number_8 = ws_File_Header_Record_1Redef10.newFieldInGroup("ws_File_Header_Record_1_Ws_Batch_Number_8", "WS-BATCH-NUMBER-8", 
            FieldType.NUMERIC, 7);
        ws_File_Header_Record_1Redef11 = newGroupInRecord("ws_File_Header_Record_1Redef11", "Redefines", ws_File_Header_Record_1);
        ws_File_Header_Record_1_Ws_Record_Type_9 = ws_File_Header_Record_1Redef11.newFieldInGroup("ws_File_Header_Record_1_Ws_Record_Type_9", "WS-RECORD-TYPE-9", 
            FieldType.STRING, 1);
        ws_File_Header_Record_1_Ws_Batch_Count_9 = ws_File_Header_Record_1Redef11.newFieldInGroup("ws_File_Header_Record_1_Ws_Batch_Count_9", "WS-BATCH-COUNT-9", 
            FieldType.NUMERIC, 6);
        ws_File_Header_Record_1_Ws_Block_Count_9 = ws_File_Header_Record_1Redef11.newFieldInGroup("ws_File_Header_Record_1_Ws_Block_Count_9", "WS-BLOCK-COUNT-9", 
            FieldType.NUMERIC, 6);
        ws_File_Header_Record_1_Ws_Entry_Addenda_Count_9 = ws_File_Header_Record_1Redef11.newFieldInGroup("ws_File_Header_Record_1_Ws_Entry_Addenda_Count_9", 
            "WS-ENTRY-ADDENDA-COUNT-9", FieldType.NUMERIC, 8);
        ws_File_Header_Record_1_Ws_Entry_Hash_9 = ws_File_Header_Record_1Redef11.newFieldInGroup("ws_File_Header_Record_1_Ws_Entry_Hash_9", "WS-ENTRY-HASH-9", 
            FieldType.NUMERIC, 10);
        ws_File_Header_Record_1_Ws_Total_Debit_9 = ws_File_Header_Record_1Redef11.newFieldInGroup("ws_File_Header_Record_1_Ws_Total_Debit_9", "WS-TOTAL-DEBIT-9", 
            FieldType.DECIMAL, 12,2);
        ws_File_Header_Record_1_Ws_Total_Credit_9 = ws_File_Header_Record_1Redef11.newFieldInGroup("ws_File_Header_Record_1_Ws_Total_Credit_9", "WS-TOTAL-CREDIT-9", 
            FieldType.DECIMAL, 12,2);
        ws_File_Header_Record_1_Filler_9 = ws_File_Header_Record_1Redef11.newFieldInGroup("ws_File_Header_Record_1_Filler_9", "FILLER-9", FieldType.STRING, 
            39);

        this.setRecordName("LdaFcpl280");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl280() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
