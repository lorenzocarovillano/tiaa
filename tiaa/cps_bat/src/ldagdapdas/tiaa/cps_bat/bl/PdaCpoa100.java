/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:18:25 PM
**        * FROM NATURAL PDA     : CPOA100
************************************************************
**        * FILE NAME            : PdaCpoa100.java
**        * CLASS NAME           : PdaCpoa100
**        * INSTANCE NAME        : PdaCpoa100
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaCpoa100 extends PdaBase
{
    // Properties
    private DbsField cpoa100;
    private DbsGroup cpoa100Redef1;
    private DbsGroup cpoa100_Cpoa100a;
    private DbsField cpoa100_Cpoa100_Table_Id;
    private DbsField cpoa100_Cpoa100_A_I_Ind;
    private DbsField cpoa100_Cpoa100_Short_Key;
    private DbsField cpoa100_Cpoa100_Long_Key;
    private DbsField cpoa100_Cpoa100_Super_Ind;
    private DbsField cpoa100_Cpoa100_Function;
    private DbsField cpoa100_Cpoa100_Filler;
    private DbsGroup cpoa100_Cpoa100b;
    private DbsField cpoa100_Cpoa100_Return_Code;
    private DbsField cpoa100_Rt_Table_Id;
    private DbsField cpoa100_Rt_A_I_Ind;
    private DbsField cpoa100_Rt_Short_Key;
    private DbsField cpoa100_Rt_Long_Key;
    private DbsField cpoa100_Rt_Desc1;
    private DbsField cpoa100_Rt_Desc2;
    private DbsField cpoa100_Rt_Desc3;
    private DbsField cpoa100_Rt_Desc4;
    private DbsField cpoa100_Rt_Desc5;
    private DbsField cpoa100_Rt_Eff_From_Ccyymmdd;
    private DbsField cpoa100_Rt_Eff_To_Ccyymmdd;
    private DbsField cpoa100_Rt_Upd_Source;
    private DbsField cpoa100_Rt_Upd_User;
    private DbsField cpoa100_Rt_Upd_Ccyymmdd;
    private DbsField cpoa100_Rt_Upd_Timn;

    public DbsField getCpoa100() { return cpoa100; }

    public DbsGroup getCpoa100Redef1() { return cpoa100Redef1; }

    public DbsGroup getCpoa100_Cpoa100a() { return cpoa100_Cpoa100a; }

    public DbsField getCpoa100_Cpoa100_Table_Id() { return cpoa100_Cpoa100_Table_Id; }

    public DbsField getCpoa100_Cpoa100_A_I_Ind() { return cpoa100_Cpoa100_A_I_Ind; }

    public DbsField getCpoa100_Cpoa100_Short_Key() { return cpoa100_Cpoa100_Short_Key; }

    public DbsField getCpoa100_Cpoa100_Long_Key() { return cpoa100_Cpoa100_Long_Key; }

    public DbsField getCpoa100_Cpoa100_Super_Ind() { return cpoa100_Cpoa100_Super_Ind; }

    public DbsField getCpoa100_Cpoa100_Function() { return cpoa100_Cpoa100_Function; }

    public DbsField getCpoa100_Cpoa100_Filler() { return cpoa100_Cpoa100_Filler; }

    public DbsGroup getCpoa100_Cpoa100b() { return cpoa100_Cpoa100b; }

    public DbsField getCpoa100_Cpoa100_Return_Code() { return cpoa100_Cpoa100_Return_Code; }

    public DbsField getCpoa100_Rt_Table_Id() { return cpoa100_Rt_Table_Id; }

    public DbsField getCpoa100_Rt_A_I_Ind() { return cpoa100_Rt_A_I_Ind; }

    public DbsField getCpoa100_Rt_Short_Key() { return cpoa100_Rt_Short_Key; }

    public DbsField getCpoa100_Rt_Long_Key() { return cpoa100_Rt_Long_Key; }

    public DbsField getCpoa100_Rt_Desc1() { return cpoa100_Rt_Desc1; }

    public DbsField getCpoa100_Rt_Desc2() { return cpoa100_Rt_Desc2; }

    public DbsField getCpoa100_Rt_Desc3() { return cpoa100_Rt_Desc3; }

    public DbsField getCpoa100_Rt_Desc4() { return cpoa100_Rt_Desc4; }

    public DbsField getCpoa100_Rt_Desc5() { return cpoa100_Rt_Desc5; }

    public DbsField getCpoa100_Rt_Eff_From_Ccyymmdd() { return cpoa100_Rt_Eff_From_Ccyymmdd; }

    public DbsField getCpoa100_Rt_Eff_To_Ccyymmdd() { return cpoa100_Rt_Eff_To_Ccyymmdd; }

    public DbsField getCpoa100_Rt_Upd_Source() { return cpoa100_Rt_Upd_Source; }

    public DbsField getCpoa100_Rt_Upd_User() { return cpoa100_Rt_Upd_User; }

    public DbsField getCpoa100_Rt_Upd_Ccyymmdd() { return cpoa100_Rt_Upd_Ccyymmdd; }

    public DbsField getCpoa100_Rt_Upd_Timn() { return cpoa100_Rt_Upd_Timn; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cpoa100 = dbsRecord.newFieldArrayInRecord("cpoa100", "CPOA100", FieldType.STRING, 250, new DbsArrayController(1,7));
        cpoa100.setParameterOption(ParameterOption.ByReference);
        cpoa100Redef1 = dbsRecord.newGroupInRecord("cpoa100Redef1", "Redefines", cpoa100);
        cpoa100_Cpoa100a = cpoa100Redef1.newGroupInGroup("cpoa100_Cpoa100a", "CPOA100A");
        cpoa100_Cpoa100_Table_Id = cpoa100_Cpoa100a.newFieldInGroup("cpoa100_Cpoa100_Table_Id", "CPOA100-TABLE-ID", FieldType.STRING, 5);
        cpoa100_Cpoa100_A_I_Ind = cpoa100_Cpoa100a.newFieldInGroup("cpoa100_Cpoa100_A_I_Ind", "CPOA100-A-I-IND", FieldType.STRING, 1);
        cpoa100_Cpoa100_Short_Key = cpoa100_Cpoa100a.newFieldInGroup("cpoa100_Cpoa100_Short_Key", "CPOA100-SHORT-KEY", FieldType.STRING, 20);
        cpoa100_Cpoa100_Long_Key = cpoa100_Cpoa100a.newFieldInGroup("cpoa100_Cpoa100_Long_Key", "CPOA100-LONG-KEY", FieldType.STRING, 40);
        cpoa100_Cpoa100_Super_Ind = cpoa100_Cpoa100a.newFieldInGroup("cpoa100_Cpoa100_Super_Ind", "CPOA100-SUPER-IND", FieldType.STRING, 2);
        cpoa100_Cpoa100_Function = cpoa100_Cpoa100a.newFieldInGroup("cpoa100_Cpoa100_Function", "CPOA100-FUNCTION", FieldType.STRING, 15);
        cpoa100_Cpoa100_Filler = cpoa100_Cpoa100a.newFieldInGroup("cpoa100_Cpoa100_Filler", "CPOA100-FILLER", FieldType.STRING, 17);
        cpoa100_Cpoa100b = cpoa100Redef1.newGroupInGroup("cpoa100_Cpoa100b", "CPOA100B");
        cpoa100_Cpoa100_Return_Code = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Cpoa100_Return_Code", "CPOA100-RETURN-CODE", FieldType.STRING, 2);
        cpoa100_Rt_Table_Id = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5);
        cpoa100_Rt_A_I_Ind = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1);
        cpoa100_Rt_Short_Key = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20);
        cpoa100_Rt_Long_Key = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40);
        cpoa100_Rt_Desc1 = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250);
        cpoa100_Rt_Desc2 = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250);
        cpoa100_Rt_Desc3 = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250);
        cpoa100_Rt_Desc4 = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250);
        cpoa100_Rt_Desc5 = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250);
        cpoa100_Rt_Eff_From_Ccyymmdd = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8);
        cpoa100_Rt_Eff_To_Ccyymmdd = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8);
        cpoa100_Rt_Upd_Source = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8);
        cpoa100_Rt_Upd_User = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8);
        cpoa100_Rt_Upd_Ccyymmdd = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8);
        cpoa100_Rt_Upd_Timn = cpoa100_Cpoa100b.newFieldInGroup("cpoa100_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7);

        dbsRecord.reset();
    }

    // Constructors
    public PdaCpoa100(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

