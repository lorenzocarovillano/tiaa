/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:15 PM
**        * FROM NATURAL LDA     : FCPL802
************************************************************
**        * FILE NAME            : LdaFcpl802.java
**        * CLASS NAME           : LdaFcpl802
**        * INSTANCE NAME        : LdaFcpl802
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl802 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl802;
    private DbsField pnd_Fcpl802_Pnd_Company_Cde;
    private DbsField pnd_Fcpl802_Pnd_Lines_Per_Fund;
    private DbsField pnd_Fcpl802_Pnd_Current_Page;
    private DbsField pnd_Fcpl802_Pnd_Current_Lines;
    private DbsField pnd_Fcpl802_Pnd_Fund_Lines;
    private DbsField pnd_Fcpl802_Pnd_Deductions_Left;
    private DbsField pnd_Fcpl802_Pnd_Lines_Idx;
    private DbsField pnd_Fcpl802_Pnd_Lines_Per_Page;
    private DbsField pnd_Fcpl802_Pnd_Nz_Lines_Per_Page;
    private DbsField pnd_Fcpl802_Pnd_Stmnt;

    public DbsGroup getPnd_Fcpl802() { return pnd_Fcpl802; }

    public DbsField getPnd_Fcpl802_Pnd_Company_Cde() { return pnd_Fcpl802_Pnd_Company_Cde; }

    public DbsField getPnd_Fcpl802_Pnd_Lines_Per_Fund() { return pnd_Fcpl802_Pnd_Lines_Per_Fund; }

    public DbsField getPnd_Fcpl802_Pnd_Current_Page() { return pnd_Fcpl802_Pnd_Current_Page; }

    public DbsField getPnd_Fcpl802_Pnd_Current_Lines() { return pnd_Fcpl802_Pnd_Current_Lines; }

    public DbsField getPnd_Fcpl802_Pnd_Fund_Lines() { return pnd_Fcpl802_Pnd_Fund_Lines; }

    public DbsField getPnd_Fcpl802_Pnd_Deductions_Left() { return pnd_Fcpl802_Pnd_Deductions_Left; }

    public DbsField getPnd_Fcpl802_Pnd_Lines_Idx() { return pnd_Fcpl802_Pnd_Lines_Idx; }

    public DbsField getPnd_Fcpl802_Pnd_Lines_Per_Page() { return pnd_Fcpl802_Pnd_Lines_Per_Page; }

    public DbsField getPnd_Fcpl802_Pnd_Nz_Lines_Per_Page() { return pnd_Fcpl802_Pnd_Nz_Lines_Per_Page; }

    public DbsField getPnd_Fcpl802_Pnd_Stmnt() { return pnd_Fcpl802_Pnd_Stmnt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl802 = newGroupInRecord("pnd_Fcpl802", "#FCPL802");
        pnd_Fcpl802_Pnd_Company_Cde = pnd_Fcpl802.newFieldInGroup("pnd_Fcpl802_Pnd_Company_Cde", "#COMPANY-CDE", FieldType.STRING, 1);
        pnd_Fcpl802_Pnd_Lines_Per_Fund = pnd_Fcpl802.newFieldInGroup("pnd_Fcpl802_Pnd_Lines_Per_Fund", "#LINES-PER-FUND", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl802_Pnd_Current_Page = pnd_Fcpl802.newFieldInGroup("pnd_Fcpl802_Pnd_Current_Page", "#CURRENT-PAGE", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl802_Pnd_Current_Lines = pnd_Fcpl802.newFieldInGroup("pnd_Fcpl802_Pnd_Current_Lines", "#CURRENT-LINES", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl802_Pnd_Fund_Lines = pnd_Fcpl802.newFieldInGroup("pnd_Fcpl802_Pnd_Fund_Lines", "#FUND-LINES", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl802_Pnd_Deductions_Left = pnd_Fcpl802.newFieldInGroup("pnd_Fcpl802_Pnd_Deductions_Left", "#DEDUCTIONS-LEFT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Fcpl802_Pnd_Lines_Idx = pnd_Fcpl802.newFieldInGroup("pnd_Fcpl802_Pnd_Lines_Idx", "#LINES-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpl802_Pnd_Lines_Per_Page = pnd_Fcpl802.newFieldArrayInGroup("pnd_Fcpl802_Pnd_Lines_Per_Page", "#LINES-PER-PAGE", FieldType.PACKED_DECIMAL, 
            3, new DbsArrayController(1,3));
        pnd_Fcpl802_Pnd_Nz_Lines_Per_Page = pnd_Fcpl802.newFieldArrayInGroup("pnd_Fcpl802_Pnd_Nz_Lines_Per_Page", "#NZ-LINES-PER-PAGE", FieldType.PACKED_DECIMAL, 
            3, new DbsArrayController(1,3));
        pnd_Fcpl802_Pnd_Stmnt = pnd_Fcpl802.newFieldInGroup("pnd_Fcpl802_Pnd_Stmnt", "#STMNT", FieldType.BOOLEAN);

        this.setRecordName("LdaFcpl802");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl802_Pnd_Lines_Per_Fund.setInitialValue(4);
        pnd_Fcpl802_Pnd_Current_Page.setInitialValue(1);
        pnd_Fcpl802_Pnd_Lines_Per_Page.getValue(1).setInitialValue(15);
        pnd_Fcpl802_Pnd_Lines_Per_Page.getValue(2).setInitialValue(36);
        pnd_Fcpl802_Pnd_Lines_Per_Page.getValue(3).setInitialValue(56);
        pnd_Fcpl802_Pnd_Nz_Lines_Per_Page.getValue(1).setInitialValue(18);
        pnd_Fcpl802_Pnd_Nz_Lines_Per_Page.getValue(2).setInitialValue(36);
        pnd_Fcpl802_Pnd_Nz_Lines_Per_Page.getValue(3).setInitialValue(60);
    }

    // Constructor
    public LdaFcpl802() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
