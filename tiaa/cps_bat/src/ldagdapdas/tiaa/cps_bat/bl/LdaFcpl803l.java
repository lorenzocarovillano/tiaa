/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:15 PM
**        * FROM NATURAL LDA     : FCPL803L
************************************************************
**        * FILE NAME            : LdaFcpl803l.java
**        * CLASS NAME           : LdaFcpl803l
**        * INSTANCE NAME        : LdaFcpl803l
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl803l extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl803l;
    private DbsField pnd_Fcpl803l_Pnd_More_Than_10_Ded;
    private DbsField pnd_Fcpl803l_Pnd_Unknown_Ledgend;
    private DbsField pnd_Fcpl803l_Pnd_Local_Ledgend;
    private DbsField pnd_Fcpl803l_Pnd_Federal_Ledgend;
    private DbsField pnd_Fcpl803l_Pnd_State_Ledgend;
    private DbsField pnd_Fcpl803l_Pnd_Rtb_Ledgend;

    public DbsGroup getPnd_Fcpl803l() { return pnd_Fcpl803l; }

    public DbsField getPnd_Fcpl803l_Pnd_More_Than_10_Ded() { return pnd_Fcpl803l_Pnd_More_Than_10_Ded; }

    public DbsField getPnd_Fcpl803l_Pnd_Unknown_Ledgend() { return pnd_Fcpl803l_Pnd_Unknown_Ledgend; }

    public DbsField getPnd_Fcpl803l_Pnd_Local_Ledgend() { return pnd_Fcpl803l_Pnd_Local_Ledgend; }

    public DbsField getPnd_Fcpl803l_Pnd_Federal_Ledgend() { return pnd_Fcpl803l_Pnd_Federal_Ledgend; }

    public DbsField getPnd_Fcpl803l_Pnd_State_Ledgend() { return pnd_Fcpl803l_Pnd_State_Ledgend; }

    public DbsField getPnd_Fcpl803l_Pnd_Rtb_Ledgend() { return pnd_Fcpl803l_Pnd_Rtb_Ledgend; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl803l = newGroupInRecord("pnd_Fcpl803l", "#FCPL803L");
        pnd_Fcpl803l_Pnd_More_Than_10_Ded = pnd_Fcpl803l.newFieldInGroup("pnd_Fcpl803l_Pnd_More_Than_10_Ded", "#MORE-THAN-10-DED", FieldType.STRING, 31);
        pnd_Fcpl803l_Pnd_Unknown_Ledgend = pnd_Fcpl803l.newFieldInGroup("pnd_Fcpl803l_Pnd_Unknown_Ledgend", "#UNKNOWN-LEDGEND", FieldType.STRING, 24);
        pnd_Fcpl803l_Pnd_Local_Ledgend = pnd_Fcpl803l.newFieldInGroup("pnd_Fcpl803l_Pnd_Local_Ledgend", "#LOCAL-LEDGEND", FieldType.STRING, 20);
        pnd_Fcpl803l_Pnd_Federal_Ledgend = pnd_Fcpl803l.newFieldArrayInGroup("pnd_Fcpl803l_Pnd_Federal_Ledgend", "#FEDERAL-LEDGEND", FieldType.STRING, 
            33, new DbsArrayController(1,2));
        pnd_Fcpl803l_Pnd_State_Ledgend = pnd_Fcpl803l.newFieldArrayInGroup("pnd_Fcpl803l_Pnd_State_Ledgend", "#STATE-LEDGEND", FieldType.STRING, 24, new 
            DbsArrayController(1,3));
        pnd_Fcpl803l_Pnd_Rtb_Ledgend = pnd_Fcpl803l.newFieldInGroup("pnd_Fcpl803l_Pnd_Rtb_Ledgend", "#RTB-LEDGEND", FieldType.STRING, 31);

        this.setRecordName("LdaFcpl803l");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl803l_Pnd_More_Than_10_Ded.setInitialValue("= More than 10 Deductions Found");
        pnd_Fcpl803l_Pnd_Unknown_Ledgend.setInitialValue("= Unknown Deduction Code");
        pnd_Fcpl803l_Pnd_Local_Ledgend.setInitialValue("= Local Tax Withheld");
        pnd_Fcpl803l_Pnd_Federal_Ledgend.getValue(1).setInitialValue("= Federal Tax Withheld");
        pnd_Fcpl803l_Pnd_Federal_Ledgend.getValue(2).setInitialValue("= Non-Resident Alien Tax Withheld");
        pnd_Fcpl803l_Pnd_State_Ledgend.getValue(1).setInitialValue("= State Taxes Withheld");
        pnd_Fcpl803l_Pnd_State_Ledgend.getValue(2).setInitialValue("= Unknown Residence Code");
        pnd_Fcpl803l_Pnd_State_Ledgend.getValue(3).setInitialValue("Taxes Withheld");
        pnd_Fcpl803l_Pnd_Rtb_Ledgend.setInitialValue("= Retirement Transition Benefit");
    }

    // Constructor
    public LdaFcpl803l() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
