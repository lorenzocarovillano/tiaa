/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:08 PM
**        * FROM NATURAL LDA     : FCPL378
************************************************************
**        * FILE NAME            : LdaFcpl378.java
**        * CLASS NAME           : LdaFcpl378
**        * INSTANCE NAME        : LdaFcpl378
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl378 extends DbsRecord
{
    // Properties
    private DbsField pnd_Pymnt_Ext_Key;
    private DbsGroup pnd_Pymnt_Ext_KeyRedef1;
    private DbsGroup pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num;
    private DbsGroup pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr;
    private DbsGroup pnd_Rec_Type_10_Detail;
    private DbsField pnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1;
    private DbsField pnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2;
    private DbsGroup pnd_Rec_Type_10_DetailRedef3;
    private DbsGroup pnd_Rec_Type_10_Detail_Pnd_Record_Type_10;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Payee_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Invrse_Dte;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Crrncy_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Qlfied_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Sps_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Stats_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Annot_Ind;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Cmbne_Ind;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Ftre_Ind;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Rec_Type_10_Detail_Annt_Soc_Sec_Ind;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Rqst_Dte;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Type_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Lob_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Cref_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Option_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Mode_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Hold_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Hold_Grp;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Annt_Ctznshp_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Annt_Rsdncy_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Split_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Check_Dte;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Cycle_Dte;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Eft_Dte;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Rqst_Pct;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Rqst_Amt;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Check_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_NbrRedef4;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Instmt_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Check_Amt;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Hold_Tme;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Acctg_Dte;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Intrfce_Dte;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Pnd_Acfs;
    private DbsField pnd_Rec_Type_10_Detail_Pnd_Funds_Per_Pymnt;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Spouse_Pay_Stats;
    private DbsField pnd_Rec_Type_10_Detail_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Rec_Type_10_Detail_Egtrra_Eligibility_Ind;
    private DbsGroup pnd_Rec_Type_20_Detail;
    private DbsField pnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_1;
    private DbsField pnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_2;
    private DbsGroup pnd_Rec_Type_20_DetailRedef5;
    private DbsGroup pnd_Rec_Type_20_Detail_Pnd_Record_Type_20;
    private DbsField pnd_Rec_Type_20_Detail_Pnd_Inv_Acct_Top;
    private DbsField pnd_Rec_Type_20_Detail_Pnd_Side;
    private DbsGroup pnd_Rec_Type_20_Detail_Pnd_Inv_Acct;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Cde;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Unit_Qty;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Unit_Value;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Settl_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Fed_Cde;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_State_Cde;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Local_Cde;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Dci_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Start_Accum_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_End_Accum_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Ivc_Ind;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Valuat_Period;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Rec_Type_20_Detail_Inv_Acct_Exp_Amt;
    private DbsGroup pnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1;
    private DbsField pnd_Rec_Type_20_Detail_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Rec_Type_20_Detail_Cntrct_Payee_Cde;
    private DbsField pnd_Rec_Type_20_Detail_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Rec_Type_20_Detail_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Rec_Type_20_Detail_Cntrct_Option_Cde;
    private DbsField pnd_Rec_Type_20_Detail_Cntrct_Mode_Cde;
    private DbsField pnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top;
    private DbsGroup pnd_Rec_Type_20_Detail_Pymnt_Ded_Grp;
    private DbsField pnd_Rec_Type_20_Detail_Pymnt_Ded_Cde;
    private DbsField pnd_Rec_Type_20_Detail_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Rec_Type_20_Detail_Pymnt_Ded_Amt;
    private DbsGroup pnd_Rec_Type_30_Detail;
    private DbsField pnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_1;
    private DbsField pnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_2;
    private DbsGroup pnd_Rec_Type_30_DetailRedef6;
    private DbsGroup pnd_Rec_Type_30_Detail_Pnd_Record_Type_30;
    private DbsGroup pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1;
    private DbsField pnd_Rec_Type_30_Detail_Rcrd_Typ;
    private DbsField pnd_Rec_Type_30_Detail_Cntrct_Orgn_Cde;
    private DbsField pnd_Rec_Type_30_Detail_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Rec_Type_30_Detail_Cntrct_Payee_Cde;
    private DbsField pnd_Rec_Type_30_Detail_Cntrct_Invrse_Dte;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pnd_Rec_Type_30_Detail_Ph_Name;
    private DbsField pnd_Rec_Type_30_Detail_Ph_Last_Name;
    private DbsField pnd_Rec_Type_30_Detail_Ph_First_Name;
    private DbsField pnd_Rec_Type_30_Detail_Ph_Middle_Name;
    private DbsGroup pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Nme;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Addr_Zip_Cde;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Postl_Data;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Addr_Type_Ind;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Addr_Last_Chg_Dte;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Addr_Last_Chg_Tme;
    private DbsGroup pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Eft_Transit_Id;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Chk_Sav_Ind;
    private DbsField pnd_Rec_Type_30_Detail_Pymnt_Deceased_Nme;
    private DbsField pnd_Rec_Type_30_Detail_Cntrct_Hold_Tme;
    private DbsGroup pnd_Rec_Type_40_Detail;
    private DbsField pnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_1;
    private DbsField pnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_2;
    private DbsGroup pnd_Rec_Type_40_DetailRedef7;
    private DbsGroup pnd_Rec_Type_40_Detail_Pnd_Record_Type_40;
    private DbsGroup pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1;
    private DbsField pnd_Rec_Type_40_Detail_Cntrct_Orgn_Cde;
    private DbsField pnd_Rec_Type_40_Detail_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Rec_Type_40_Detail_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Rec_Type_40_Detail_Pymnt_Check_Dte;
    private DbsField pnd_Rec_Type_40_Detail_Cntrct_Good_Contract;
    private DbsField pnd_Rec_Type_40_Detail_Cntrct_Invalid_Cond_Ind;
    private DbsField pnd_Rec_Type_40_Detail_Cntrct_Multi_Payee;
    private DbsField pnd_Rec_Type_40_Detail_Pnd_Good_Letter;
    private DbsField pnd_Rec_Type_40_Detail_Pnd_Plan_Type_Display;
    private DbsField pnd_Rec_Type_40_Detail_Cntrct_Ivc_Amt;
    private DbsField pnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top;
    private DbsGroup pnd_Rec_Type_40_Detail_Plan_Data;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employer_Name;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Type;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Cash_Avail;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb_Earn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn_Earn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn_Earn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Acc_123186;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Acc_123188;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn_Earn;
    private DbsGroup pnd_Rec_Type_45_Detail;
    private DbsField pnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_1;
    private DbsField pnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_2;
    private DbsGroup pnd_Rec_Type_45_DetailRedef8;
    private DbsGroup pnd_Rec_Type_45_Detail_Pnd_Record_Type_45;
    private DbsGroup pnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1;
    private DbsField pnd_Rec_Type_45_Detail_Cntrct_Orgn_Cde;
    private DbsField pnd_Rec_Type_45_Detail_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Rec_Type_45_Detail_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Rec_Type_45_Detail_Pymnt_Check_Dte;
    private DbsField pnd_Rec_Type_45_Detail_Good_Contract;
    private DbsField pnd_Rec_Type_45_Detail_Invalid_Cond_Ind;
    private DbsField pnd_Rec_Type_45_Detail_Pnd_Invalid_Reasons_Top;
    private DbsField pnd_Rec_Type_45_Detail_Invalid_Reasons;

    public DbsField getPnd_Pymnt_Ext_Key() { return pnd_Pymnt_Ext_Key; }

    public DbsGroup getPnd_Pymnt_Ext_KeyRedef1() { return pnd_Pymnt_Ext_KeyRedef1; }

    public DbsGroup getPnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail() { return pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd() { return pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd() { return pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex() { return pnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num() { return pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num; }

    public DbsGroup getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2() { return pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num() { return pnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num() { return pnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code() { return pnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr() { return pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr() { return pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr() { return pnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr; }

    public DbsGroup getPnd_Rec_Type_10_Detail() { return pnd_Rec_Type_10_Detail; }

    public DbsField getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1() { return pnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1; }

    public DbsField getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2() { return pnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2; }

    public DbsGroup getPnd_Rec_Type_10_DetailRedef3() { return pnd_Rec_Type_10_DetailRedef3; }

    public DbsGroup getPnd_Rec_Type_10_Detail_Pnd_Record_Type_10() { return pnd_Rec_Type_10_Detail_Pnd_Record_Type_10; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Ppcn_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Payee_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Invrse_Dte() { return pnd_Rec_Type_10_Detail_Cntrct_Invrse_Dte; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Check_Crrncy_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Check_Crrncy_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Crrncy_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Crrncy_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Qlfied_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Qlfied_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Pymnt_Type_Ind() { return pnd_Rec_Type_10_Detail_Cntrct_Pymnt_Type_Ind; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Sttlmnt_Type_Ind() { return pnd_Rec_Type_10_Detail_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Sps_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Sps_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Rqst_Rmndr_Pct() { return pnd_Rec_Type_10_Detail_Pymnt_Rqst_Rmndr_Pct; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Stats_Cde() { return pnd_Rec_Type_10_Detail_Pymnt_Stats_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Annot_Ind() { return pnd_Rec_Type_10_Detail_Pymnt_Annot_Ind; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Cmbne_Ind() { return pnd_Rec_Type_10_Detail_Pymnt_Cmbne_Ind; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Ftre_Ind() { return pnd_Rec_Type_10_Detail_Pymnt_Ftre_Ind; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Payee_Na_Addr_Trggr() { return pnd_Rec_Type_10_Detail_Pymnt_Payee_Na_Addr_Trggr; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Payee_Tx_Elct_Trggr() { return pnd_Rec_Type_10_Detail_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Inst_Rep_Cde() { return pnd_Rec_Type_10_Detail_Pymnt_Inst_Rep_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Dvdnd_Payee_Ind() { return pnd_Rec_Type_10_Detail_Cntrct_Dvdnd_Payee_Ind; }

    public DbsField getPnd_Rec_Type_10_Detail_Annt_Soc_Sec_Ind() { return pnd_Rec_Type_10_Detail_Annt_Soc_Sec_Ind; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind() { return pnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Rqst_Settl_Dte() { return pnd_Rec_Type_10_Detail_Cntrct_Rqst_Settl_Dte; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Rqst_Dte() { return pnd_Rec_Type_10_Detail_Cntrct_Rqst_Dte; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Rqst_Settl_Tme() { return pnd_Rec_Type_10_Detail_Cntrct_Rqst_Settl_Tme; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Type_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Type_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Lob_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Lob_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Sub_Lob_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Sub_Lob_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Ia_Lob_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Ia_Lob_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Cref_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Cref_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Option_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Option_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Mode_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Mode_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Pymnt_Dest_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Pymnt_Dest_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Roll_Dest_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Roll_Dest_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Dvdnd_Payee_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Dvdnd_Payee_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Hold_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Hold_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Hold_Grp() { return pnd_Rec_Type_10_Detail_Cntrct_Hold_Grp; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_1_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_1_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_2_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_2_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_3_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_3_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_4_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_4_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_5_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_5_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Da_Cref_1_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_1_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Da_Cref_2_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_2_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Da_Cref_3_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_3_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Da_Cref_4_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_4_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Da_Cref_5_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_5_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Annt_Soc_Sec_Nbr() { return pnd_Rec_Type_10_Detail_Annt_Soc_Sec_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Annt_Ctznshp_Cde() { return pnd_Rec_Type_10_Detail_Annt_Ctznshp_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Annt_Rsdncy_Cde() { return pnd_Rec_Type_10_Detail_Annt_Rsdncy_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Split_Cde() { return pnd_Rec_Type_10_Detail_Pymnt_Split_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Split_Reasn_Cde() { return pnd_Rec_Type_10_Detail_Pymnt_Split_Reasn_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Check_Dte() { return pnd_Rec_Type_10_Detail_Pymnt_Check_Dte; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Cycle_Dte() { return pnd_Rec_Type_10_Detail_Pymnt_Cycle_Dte; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Eft_Dte() { return pnd_Rec_Type_10_Detail_Pymnt_Eft_Dte; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Rqst_Pct() { return pnd_Rec_Type_10_Detail_Pymnt_Rqst_Pct; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Rqst_Amt() { return pnd_Rec_Type_10_Detail_Pymnt_Rqst_Amt; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Check_Nbr() { return pnd_Rec_Type_10_Detail_Pymnt_Check_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Nbr() { return pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_NbrRedef4() { return pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_NbrRedef4; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Num() { return pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Num; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Instmt_Nbr() { return pnd_Rec_Type_10_Detail_Pymnt_Instmt_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr() { return pnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Check_Amt() { return pnd_Rec_Type_10_Detail_Pymnt_Check_Amt; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Settlmnt_Dte() { return pnd_Rec_Type_10_Detail_Pymnt_Settlmnt_Dte; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Check_Seq_Nbr() { return pnd_Rec_Type_10_Detail_Pymnt_Check_Seq_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Ec_Oprtr_Id_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Ec_Oprtr_Id_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Cmbn_Nbr() { return pnd_Rec_Type_10_Detail_Cntrct_Cmbn_Nbr; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Hold_Tme() { return pnd_Rec_Type_10_Detail_Cntrct_Hold_Tme; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Ind() { return pnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Ind; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Acctg_Dte() { return pnd_Rec_Type_10_Detail_Pymnt_Acctg_Dte; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time() { return pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Intrfce_Dte() { return pnd_Rec_Type_10_Detail_Pymnt_Intrfce_Dte; }

    public DbsField getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPnd_Rec_Type_10_Detail_Pnd_Acfs() { return pnd_Rec_Type_10_Detail_Pnd_Acfs; }

    public DbsField getPnd_Rec_Type_10_Detail_Pnd_Funds_Per_Pymnt() { return pnd_Rec_Type_10_Detail_Pnd_Funds_Per_Pymnt; }

    public DbsField getPnd_Rec_Type_10_Detail_Pymnt_Spouse_Pay_Stats() { return pnd_Rec_Type_10_Detail_Pymnt_Spouse_Pay_Stats; }

    public DbsField getPnd_Rec_Type_10_Detail_Cnr_Orgnl_Invrse_Dte() { return pnd_Rec_Type_10_Detail_Cnr_Orgnl_Invrse_Dte; }

    public DbsField getPnd_Rec_Type_10_Detail_Egtrra_Eligibility_Ind() { return pnd_Rec_Type_10_Detail_Egtrra_Eligibility_Ind; }

    public DbsGroup getPnd_Rec_Type_20_Detail() { return pnd_Rec_Type_20_Detail; }

    public DbsField getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_1() { return pnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_1; }

    public DbsField getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_2() { return pnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_2; }

    public DbsGroup getPnd_Rec_Type_20_DetailRedef5() { return pnd_Rec_Type_20_DetailRedef5; }

    public DbsGroup getPnd_Rec_Type_20_Detail_Pnd_Record_Type_20() { return pnd_Rec_Type_20_Detail_Pnd_Record_Type_20; }

    public DbsField getPnd_Rec_Type_20_Detail_Pnd_Inv_Acct_Top() { return pnd_Rec_Type_20_Detail_Pnd_Inv_Acct_Top; }

    public DbsField getPnd_Rec_Type_20_Detail_Pnd_Side() { return pnd_Rec_Type_20_Detail_Pnd_Side; }

    public DbsGroup getPnd_Rec_Type_20_Detail_Pnd_Inv_Acct() { return pnd_Rec_Type_20_Detail_Pnd_Inv_Acct; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Cde() { return pnd_Rec_Type_20_Detail_Inv_Acct_Cde; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Unit_Qty() { return pnd_Rec_Type_20_Detail_Inv_Acct_Unit_Qty; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Unit_Value() { return pnd_Rec_Type_20_Detail_Inv_Acct_Unit_Value; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Settl_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_Settl_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Fed_Cde() { return pnd_Rec_Type_20_Detail_Inv_Acct_Fed_Cde; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_State_Cde() { return pnd_Rec_Type_20_Detail_Inv_Acct_State_Cde; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Local_Cde() { return pnd_Rec_Type_20_Detail_Inv_Acct_Local_Cde; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Ivc_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_Ivc_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Dci_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_Dci_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Dpi_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_Dpi_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Start_Accum_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_Start_Accum_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_End_Accum_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_End_Accum_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Dvdnd_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_Dvdnd_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Net_Pymnt_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Ivc_Ind() { return pnd_Rec_Type_20_Detail_Inv_Acct_Ivc_Ind; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Adj_Ivc_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_Adj_Ivc_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Valuat_Period() { return pnd_Rec_Type_20_Detail_Inv_Acct_Valuat_Period; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Fdrl_Tax_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_State_Tax_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_State_Tax_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Local_Tax_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_Local_Tax_Amt; }

    public DbsField getPnd_Rec_Type_20_Detail_Inv_Acct_Exp_Amt() { return pnd_Rec_Type_20_Detail_Inv_Acct_Exp_Amt; }

    public DbsGroup getPnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1() { return pnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1; }

    public DbsField getPnd_Rec_Type_20_Detail_Cntrct_Ppcn_Nbr() { return pnd_Rec_Type_20_Detail_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Rec_Type_20_Detail_Cntrct_Payee_Cde() { return pnd_Rec_Type_20_Detail_Cntrct_Payee_Cde; }

    public DbsField getPnd_Rec_Type_20_Detail_Cntrct_Pymnt_Type_Ind() { return pnd_Rec_Type_20_Detail_Cntrct_Pymnt_Type_Ind; }

    public DbsField getPnd_Rec_Type_20_Detail_Cntrct_Sttlmnt_Type_Ind() { return pnd_Rec_Type_20_Detail_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getPnd_Rec_Type_20_Detail_Cntrct_Option_Cde() { return pnd_Rec_Type_20_Detail_Cntrct_Option_Cde; }

    public DbsField getPnd_Rec_Type_20_Detail_Cntrct_Mode_Cde() { return pnd_Rec_Type_20_Detail_Cntrct_Mode_Cde; }

    public DbsField getPnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top() { return pnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top; }

    public DbsGroup getPnd_Rec_Type_20_Detail_Pymnt_Ded_Grp() { return pnd_Rec_Type_20_Detail_Pymnt_Ded_Grp; }

    public DbsField getPnd_Rec_Type_20_Detail_Pymnt_Ded_Cde() { return pnd_Rec_Type_20_Detail_Pymnt_Ded_Cde; }

    public DbsField getPnd_Rec_Type_20_Detail_Pymnt_Ded_Payee_Cde() { return pnd_Rec_Type_20_Detail_Pymnt_Ded_Payee_Cde; }

    public DbsField getPnd_Rec_Type_20_Detail_Pymnt_Ded_Amt() { return pnd_Rec_Type_20_Detail_Pymnt_Ded_Amt; }

    public DbsGroup getPnd_Rec_Type_30_Detail() { return pnd_Rec_Type_30_Detail; }

    public DbsField getPnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_1() { return pnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_1; }

    public DbsField getPnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_2() { return pnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_2; }

    public DbsGroup getPnd_Rec_Type_30_DetailRedef6() { return pnd_Rec_Type_30_DetailRedef6; }

    public DbsGroup getPnd_Rec_Type_30_Detail_Pnd_Record_Type_30() { return pnd_Rec_Type_30_Detail_Pnd_Record_Type_30; }

    public DbsGroup getPnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1() { return pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1; }

    public DbsField getPnd_Rec_Type_30_Detail_Rcrd_Typ() { return pnd_Rec_Type_30_Detail_Rcrd_Typ; }

    public DbsField getPnd_Rec_Type_30_Detail_Cntrct_Orgn_Cde() { return pnd_Rec_Type_30_Detail_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Rec_Type_30_Detail_Cntrct_Ppcn_Nbr() { return pnd_Rec_Type_30_Detail_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Rec_Type_30_Detail_Cntrct_Payee_Cde() { return pnd_Rec_Type_30_Detail_Cntrct_Payee_Cde; }

    public DbsField getPnd_Rec_Type_30_Detail_Cntrct_Invrse_Dte() { return pnd_Rec_Type_30_Detail_Cntrct_Invrse_Dte; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Prcss_Seq_Nbr() { return pnd_Rec_Type_30_Detail_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPnd_Rec_Type_30_Detail_Ph_Name() { return pnd_Rec_Type_30_Detail_Ph_Name; }

    public DbsField getPnd_Rec_Type_30_Detail_Ph_Last_Name() { return pnd_Rec_Type_30_Detail_Ph_Last_Name; }

    public DbsField getPnd_Rec_Type_30_Detail_Ph_First_Name() { return pnd_Rec_Type_30_Detail_Ph_First_Name; }

    public DbsField getPnd_Rec_Type_30_Detail_Ph_Middle_Name() { return pnd_Rec_Type_30_Detail_Ph_Middle_Name; }

    public DbsGroup getPnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp() { return pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Nme() { return pnd_Rec_Type_30_Detail_Pymnt_Nme; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line1_Txt() { return pnd_Rec_Type_30_Detail_Pymnt_Addr_Line1_Txt; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line2_Txt() { return pnd_Rec_Type_30_Detail_Pymnt_Addr_Line2_Txt; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line3_Txt() { return pnd_Rec_Type_30_Detail_Pymnt_Addr_Line3_Txt; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line4_Txt() { return pnd_Rec_Type_30_Detail_Pymnt_Addr_Line4_Txt; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line5_Txt() { return pnd_Rec_Type_30_Detail_Pymnt_Addr_Line5_Txt; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line6_Txt() { return pnd_Rec_Type_30_Detail_Pymnt_Addr_Line6_Txt; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Addr_Zip_Cde() { return pnd_Rec_Type_30_Detail_Pymnt_Addr_Zip_Cde; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Postl_Data() { return pnd_Rec_Type_30_Detail_Pymnt_Postl_Data; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Addr_Type_Ind() { return pnd_Rec_Type_30_Detail_Pymnt_Addr_Type_Ind; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Addr_Last_Chg_Dte() { return pnd_Rec_Type_30_Detail_Pymnt_Addr_Last_Chg_Dte; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Addr_Last_Chg_Tme() { return pnd_Rec_Type_30_Detail_Pymnt_Addr_Last_Chg_Tme; }

    public DbsGroup getPnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2() { return pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Eft_Transit_Id() { return pnd_Rec_Type_30_Detail_Pymnt_Eft_Transit_Id; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Eft_Acct_Nbr() { return pnd_Rec_Type_30_Detail_Pymnt_Eft_Acct_Nbr; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Chk_Sav_Ind() { return pnd_Rec_Type_30_Detail_Pymnt_Chk_Sav_Ind; }

    public DbsField getPnd_Rec_Type_30_Detail_Pymnt_Deceased_Nme() { return pnd_Rec_Type_30_Detail_Pymnt_Deceased_Nme; }

    public DbsField getPnd_Rec_Type_30_Detail_Cntrct_Hold_Tme() { return pnd_Rec_Type_30_Detail_Cntrct_Hold_Tme; }

    public DbsGroup getPnd_Rec_Type_40_Detail() { return pnd_Rec_Type_40_Detail; }

    public DbsField getPnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_1() { return pnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_1; }

    public DbsField getPnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_2() { return pnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_2; }

    public DbsGroup getPnd_Rec_Type_40_DetailRedef7() { return pnd_Rec_Type_40_DetailRedef7; }

    public DbsGroup getPnd_Rec_Type_40_Detail_Pnd_Record_Type_40() { return pnd_Rec_Type_40_Detail_Pnd_Record_Type_40; }

    public DbsGroup getPnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1() { return pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1; }

    public DbsField getPnd_Rec_Type_40_Detail_Cntrct_Orgn_Cde() { return pnd_Rec_Type_40_Detail_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Rec_Type_40_Detail_Cntrct_Ppcn_Nbr() { return pnd_Rec_Type_40_Detail_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Rec_Type_40_Detail_Pymnt_Prcss_Seq_Nbr() { return pnd_Rec_Type_40_Detail_Pymnt_Prcss_Seq_Nbr; }

    public DbsField getPnd_Rec_Type_40_Detail_Pymnt_Check_Dte() { return pnd_Rec_Type_40_Detail_Pymnt_Check_Dte; }

    public DbsField getPnd_Rec_Type_40_Detail_Cntrct_Good_Contract() { return pnd_Rec_Type_40_Detail_Cntrct_Good_Contract; }

    public DbsField getPnd_Rec_Type_40_Detail_Cntrct_Invalid_Cond_Ind() { return pnd_Rec_Type_40_Detail_Cntrct_Invalid_Cond_Ind; }

    public DbsField getPnd_Rec_Type_40_Detail_Cntrct_Multi_Payee() { return pnd_Rec_Type_40_Detail_Cntrct_Multi_Payee; }

    public DbsField getPnd_Rec_Type_40_Detail_Pnd_Good_Letter() { return pnd_Rec_Type_40_Detail_Pnd_Good_Letter; }

    public DbsField getPnd_Rec_Type_40_Detail_Pnd_Plan_Type_Display() { return pnd_Rec_Type_40_Detail_Pnd_Plan_Type_Display; }

    public DbsField getPnd_Rec_Type_40_Detail_Cntrct_Ivc_Amt() { return pnd_Rec_Type_40_Detail_Cntrct_Ivc_Amt; }

    public DbsField getPnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top() { return pnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top; }

    public DbsGroup getPnd_Rec_Type_40_Detail_Plan_Data() { return pnd_Rec_Type_40_Detail_Plan_Data; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Employer_Name() { return pnd_Rec_Type_40_Detail_Plan_Employer_Name; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Type() { return pnd_Rec_Type_40_Detail_Plan_Type; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Cash_Avail() { return pnd_Rec_Type_40_Detail_Plan_Cash_Avail; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Employer_Cntrb() { return pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Employer_Cntrb_Earn() { return pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb_Earn; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Employee_Rdctn() { return pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Employee_Rdctn_Earn() { return pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn_Earn; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Employee_Ddctn() { return pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Employee_Ddctn_Earn() { return pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn_Earn; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Acc_123186() { return pnd_Rec_Type_40_Detail_Plan_Acc_123186; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Acc_123188() { return pnd_Rec_Type_40_Detail_Plan_Acc_123188; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn() { return pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn; }

    public DbsField getPnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn_Earn() { return pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn_Earn; }

    public DbsGroup getPnd_Rec_Type_45_Detail() { return pnd_Rec_Type_45_Detail; }

    public DbsField getPnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_1() { return pnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_1; }

    public DbsField getPnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_2() { return pnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_2; }

    public DbsGroup getPnd_Rec_Type_45_DetailRedef8() { return pnd_Rec_Type_45_DetailRedef8; }

    public DbsGroup getPnd_Rec_Type_45_Detail_Pnd_Record_Type_45() { return pnd_Rec_Type_45_Detail_Pnd_Record_Type_45; }

    public DbsGroup getPnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1() { return pnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1; }

    public DbsField getPnd_Rec_Type_45_Detail_Cntrct_Orgn_Cde() { return pnd_Rec_Type_45_Detail_Cntrct_Orgn_Cde; }

    public DbsField getPnd_Rec_Type_45_Detail_Cntrct_Ppcn_Nbr() { return pnd_Rec_Type_45_Detail_Cntrct_Ppcn_Nbr; }

    public DbsField getPnd_Rec_Type_45_Detail_Pymnt_Prcss_Seq_Nbr() { return pnd_Rec_Type_45_Detail_Pymnt_Prcss_Seq_Nbr; }

    public DbsField getPnd_Rec_Type_45_Detail_Pymnt_Check_Dte() { return pnd_Rec_Type_45_Detail_Pymnt_Check_Dte; }

    public DbsField getPnd_Rec_Type_45_Detail_Good_Contract() { return pnd_Rec_Type_45_Detail_Good_Contract; }

    public DbsField getPnd_Rec_Type_45_Detail_Invalid_Cond_Ind() { return pnd_Rec_Type_45_Detail_Invalid_Cond_Ind; }

    public DbsField getPnd_Rec_Type_45_Detail_Pnd_Invalid_Reasons_Top() { return pnd_Rec_Type_45_Detail_Pnd_Invalid_Reasons_Top; }

    public DbsField getPnd_Rec_Type_45_Detail_Invalid_Reasons() { return pnd_Rec_Type_45_Detail_Invalid_Reasons; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Pymnt_Ext_Key = newFieldInRecord("pnd_Pymnt_Ext_Key", "#PYMNT-EXT-KEY", FieldType.STRING, 41);
        pnd_Pymnt_Ext_KeyRedef1 = newGroupInRecord("pnd_Pymnt_Ext_KeyRedef1", "Redefines", pnd_Pymnt_Ext_Key);
        pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail = pnd_Pymnt_Ext_KeyRedef1.newGroupInGroup("pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail", "#PYMNT-EXT-KEY-DETAIL");
        pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd", "#KEY-REC-LVL-#", 
            FieldType.NUMERIC, 2);
        pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd", 
            "#KEY-REC-OCCUR-#", FieldType.NUMERIC, 2);
        pnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex", 
            "#KEY-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num", 
            "#KEY-PYMNT-PRCSS-SEQ-NUM", FieldType.STRING, 7);
        pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2 = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newGroupInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2", 
            "Redefines", pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_Num);
        pnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num = pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num", "#KEY-SEQ-NUM", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num = pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Prcss_Seq_NumRedef2.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num", 
            "#KEY-INST-NUM", FieldType.NUMERIC, 2);
        pnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code", 
            "#KEY-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr", 
            "#KEY-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr", 
            "#KEY-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr = pnd_Pymnt_Ext_Key_Pnd_Pymnt_Ext_Key_Detail.newFieldInGroup("pnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr", 
            "#KEY-CNTRCT-CMBN-NBR", FieldType.STRING, 14);

        pnd_Rec_Type_10_Detail = newGroupInRecord("pnd_Rec_Type_10_Detail", "#REC-TYPE-10-DETAIL");
        pnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1 = pnd_Rec_Type_10_Detail.newFieldInGroup("pnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1", "#REC-TYPE-10-DET-1", 
            FieldType.STRING, 250);
        pnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2 = pnd_Rec_Type_10_Detail.newFieldInGroup("pnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2", "#REC-TYPE-10-DET-2", 
            FieldType.STRING, 209);
        pnd_Rec_Type_10_DetailRedef3 = newGroupInRecord("pnd_Rec_Type_10_DetailRedef3", "Redefines", pnd_Rec_Type_10_Detail);
        pnd_Rec_Type_10_Detail_Pnd_Record_Type_10 = pnd_Rec_Type_10_DetailRedef3.newGroupInGroup("pnd_Rec_Type_10_Detail_Pnd_Record_Type_10", "#RECORD-TYPE-10");
        pnd_Rec_Type_10_Detail_Cntrct_Ppcn_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Rec_Type_10_Detail_Cntrct_Payee_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Payee_Cde", 
            "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Rec_Type_10_Detail_Cntrct_Invrse_Dte = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Invrse_Dte", 
            "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Rec_Type_10_Detail_Cntrct_Check_Crrncy_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Check_Crrncy_Cde", 
            "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Cntrct_Crrncy_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Crrncy_Cde", 
            "CNTRCT-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Rec_Type_10_Detail_Cntrct_Qlfied_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Qlfied_Cde", 
            "CNTRCT-QLFIED-CDE", FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Cntrct_Pymnt_Type_Ind = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Pymnt_Type_Ind", 
            "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Cntrct_Sttlmnt_Type_Ind = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Sttlmnt_Type_Ind", 
            "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Cntrct_Sps_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", 
            FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Pymnt_Rqst_Rmndr_Pct = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Rqst_Rmndr_Pct", 
            "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Pymnt_Stats_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", 
            FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Pymnt_Annot_Ind = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", 
            FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Pymnt_Cmbne_Ind = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", 
            FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Pymnt_Ftre_Ind = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", 
            FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Pymnt_Payee_Na_Addr_Trggr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Payee_Na_Addr_Trggr", 
            "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Pymnt_Payee_Tx_Elct_Trggr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Payee_Tx_Elct_Trggr", 
            "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Pymnt_Inst_Rep_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Inst_Rep_Cde", 
            "PYMNT-INST-REP-CDE", FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Cntrct_Dvdnd_Payee_Ind = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Dvdnd_Payee_Ind", 
            "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 1);
        pnd_Rec_Type_10_Detail_Annt_Soc_Sec_Ind = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Annt_Soc_Sec_Ind", 
            "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1);
        pnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind", 
            "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Rec_Type_10_Detail_Cntrct_Rqst_Settl_Dte = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Rqst_Settl_Dte", 
            "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        pnd_Rec_Type_10_Detail_Cntrct_Rqst_Dte = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", 
            FieldType.DATE);
        pnd_Rec_Type_10_Detail_Cntrct_Rqst_Settl_Tme = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Rqst_Settl_Tme", 
            "CNTRCT-RQST-SETTL-TME", FieldType.TIME);
        pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr", 
            "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Rec_Type_10_Detail_Cntrct_Type_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Rec_Type_10_Detail_Cntrct_Lob_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Rec_Type_10_Detail_Cntrct_Sub_Lob_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Sub_Lob_Cde", 
            "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        pnd_Rec_Type_10_Detail_Cntrct_Ia_Lob_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Ia_Lob_Cde", 
            "CNTRCT-IA-LOB-CDE", FieldType.STRING, 2);
        pnd_Rec_Type_10_Detail_Cntrct_Cref_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", 
            FieldType.STRING, 10);
        pnd_Rec_Type_10_Detail_Cntrct_Option_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Option_Cde", 
            "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        pnd_Rec_Type_10_Detail_Cntrct_Mode_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Rec_Type_10_Detail_Cntrct_Pymnt_Dest_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Pymnt_Dest_Cde", 
            "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        pnd_Rec_Type_10_Detail_Cntrct_Roll_Dest_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Roll_Dest_Cde", 
            "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        pnd_Rec_Type_10_Detail_Cntrct_Dvdnd_Payee_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Dvdnd_Payee_Cde", 
            "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        pnd_Rec_Type_10_Detail_Cntrct_Hold_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 4);
        pnd_Rec_Type_10_Detail_Cntrct_Hold_Grp = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", 
            FieldType.STRING, 3);
        pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_1_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_1_Nbr", 
            "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_2_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_2_Nbr", 
            "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_3_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_3_Nbr", 
            "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_4_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_4_Nbr", 
            "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_5_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Da_Tiaa_5_Nbr", 
            "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_1_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_1_Nbr", 
            "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_2_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_2_Nbr", 
            "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8);
        pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_3_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_3_Nbr", 
            "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 8);
        pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_4_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_4_Nbr", 
            "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 8);
        pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_5_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Da_Cref_5_Nbr", 
            "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 8);
        pnd_Rec_Type_10_Detail_Annt_Soc_Sec_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Annt_Soc_Sec_Nbr", 
            "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        pnd_Rec_Type_10_Detail_Annt_Ctznshp_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Annt_Ctznshp_Cde", 
            "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2);
        pnd_Rec_Type_10_Detail_Annt_Rsdncy_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Rec_Type_10_Detail_Pymnt_Split_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", 
            FieldType.STRING, 2);
        pnd_Rec_Type_10_Detail_Pymnt_Split_Reasn_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Split_Reasn_Cde", 
            "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);
        pnd_Rec_Type_10_Detail_Pymnt_Check_Dte = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", 
            FieldType.DATE);
        pnd_Rec_Type_10_Detail_Pymnt_Cycle_Dte = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", 
            FieldType.DATE);
        pnd_Rec_Type_10_Detail_Pymnt_Eft_Dte = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", 
            FieldType.DATE);
        pnd_Rec_Type_10_Detail_Pymnt_Rqst_Pct = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Rec_Type_10_Detail_Pymnt_Rqst_Amt = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_10_Detail_Pymnt_Check_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_NbrRedef4 = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newGroupInGroup("pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_NbrRedef4", 
            "Redefines", pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Nbr);
        pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Num = pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_NbrRedef4.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Num", 
            "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Rec_Type_10_Detail_Pymnt_Instmt_Nbr = pnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_NbrRedef4.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Instmt_Nbr", 
            "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr", 
            "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        pnd_Rec_Type_10_Detail_Pymnt_Check_Amt = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_10_Detail_Pymnt_Settlmnt_Dte = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Settlmnt_Dte", 
            "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Rec_Type_10_Detail_Pymnt_Check_Seq_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Check_Seq_Nbr", 
            "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Rec_Type_10_Detail_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Ec_Oprtr_Id_Nbr", 
            "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 7);
        pnd_Rec_Type_10_Detail_Cntrct_Cmbn_Nbr = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Rec_Type_10_Detail_Cntrct_Hold_Tme = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", 
            FieldType.TIME);
        pnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Ind = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Ind", 
            "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Pymnt_Acctg_Dte = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", 
            FieldType.DATE);
        pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time", 
            "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_Rec_Type_10_Detail_Pymnt_Intrfce_Dte = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Intrfce_Dte", 
            "PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        pnd_Rec_Type_10_Detail_Pnd_Acfs = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pnd_Acfs", "#ACFS", FieldType.BOOLEAN);
        pnd_Rec_Type_10_Detail_Pnd_Funds_Per_Pymnt = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pnd_Funds_Per_Pymnt", 
            "#FUNDS-PER-PYMNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Rec_Type_10_Detail_Pymnt_Spouse_Pay_Stats = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Pymnt_Spouse_Pay_Stats", 
            "PYMNT-SPOUSE-PAY-STATS", FieldType.STRING, 1);
        pnd_Rec_Type_10_Detail_Cnr_Orgnl_Invrse_Dte = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Cnr_Orgnl_Invrse_Dte", 
            "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Rec_Type_10_Detail_Egtrra_Eligibility_Ind = pnd_Rec_Type_10_Detail_Pnd_Record_Type_10.newFieldInGroup("pnd_Rec_Type_10_Detail_Egtrra_Eligibility_Ind", 
            "EGTRRA-ELIGIBILITY-IND", FieldType.STRING, 1);

        pnd_Rec_Type_20_Detail = newGroupInRecord("pnd_Rec_Type_20_Detail", "#REC-TYPE-20-DETAIL");
        pnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_1 = pnd_Rec_Type_20_Detail.newFieldInGroup("pnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_1", "#REC-TYPE-20-DET-1", 
            FieldType.STRING, 250);
        pnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_2 = pnd_Rec_Type_20_Detail.newFieldInGroup("pnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_2", "#REC-TYPE-20-DET-2", 
            FieldType.STRING, 209);
        pnd_Rec_Type_20_DetailRedef5 = newGroupInRecord("pnd_Rec_Type_20_DetailRedef5", "Redefines", pnd_Rec_Type_20_Detail);
        pnd_Rec_Type_20_Detail_Pnd_Record_Type_20 = pnd_Rec_Type_20_DetailRedef5.newGroupInGroup("pnd_Rec_Type_20_Detail_Pnd_Record_Type_20", "#RECORD-TYPE-20");
        pnd_Rec_Type_20_Detail_Pnd_Inv_Acct_Top = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20.newFieldInGroup("pnd_Rec_Type_20_Detail_Pnd_Inv_Acct_Top", 
            "#INV-ACCT-TOP", FieldType.NUMERIC, 3);
        pnd_Rec_Type_20_Detail_Pnd_Side = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20.newFieldInGroup("pnd_Rec_Type_20_Detail_Pnd_Side", "#SIDE", FieldType.STRING, 
            1);
        pnd_Rec_Type_20_Detail_Pnd_Inv_Acct = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20.newGroupInGroup("pnd_Rec_Type_20_Detail_Pnd_Inv_Acct", "#INV-ACCT");
        pnd_Rec_Type_20_Detail_Inv_Acct_Cde = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Cde", "INV-ACCT-CDE", 
            FieldType.STRING, 2);
        pnd_Rec_Type_20_Detail_Inv_Acct_Unit_Qty = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", 
            FieldType.PACKED_DECIMAL, 9,3);
        pnd_Rec_Type_20_Detail_Inv_Acct_Unit_Value = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Unit_Value", 
            "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 9,4);
        pnd_Rec_Type_20_Detail_Inv_Acct_Settl_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_Fed_Cde = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", 
            FieldType.STRING, 1);
        pnd_Rec_Type_20_Detail_Inv_Acct_State_Cde = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", 
            FieldType.STRING, 1);
        pnd_Rec_Type_20_Detail_Inv_Acct_Local_Cde = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", 
            FieldType.STRING, 1);
        pnd_Rec_Type_20_Detail_Inv_Acct_Ivc_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_Dci_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_Dpi_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_Start_Accum_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Start_Accum_Amt", 
            "INV-ACCT-START-ACCUM-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_End_Accum_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_End_Accum_Amt", 
            "INV-ACCT-END-ACCUM-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_Dvdnd_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_Net_Pymnt_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Net_Pymnt_Amt", 
            "INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_Ivc_Ind = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", 
            FieldType.STRING, 1);
        pnd_Rec_Type_20_Detail_Inv_Acct_Adj_Ivc_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Adj_Ivc_Amt", 
            "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_Valuat_Period = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Valuat_Period", 
            "INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 1);
        pnd_Rec_Type_20_Detail_Inv_Acct_Fdrl_Tax_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Fdrl_Tax_Amt", 
            "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_State_Tax_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_State_Tax_Amt", 
            "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_Local_Tax_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Local_Tax_Amt", 
            "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Inv_Acct_Exp_Amt = pnd_Rec_Type_20_Detail_Pnd_Inv_Acct.newFieldInGroup("pnd_Rec_Type_20_Detail_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1 = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20.newGroupInGroup("pnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1", 
            "#RECORD-TYPE-20-GRP1");
        pnd_Rec_Type_20_Detail_Cntrct_Ppcn_Nbr = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1.newFieldInGroup("pnd_Rec_Type_20_Detail_Cntrct_Ppcn_Nbr", 
            "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Rec_Type_20_Detail_Cntrct_Payee_Cde = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1.newFieldInGroup("pnd_Rec_Type_20_Detail_Cntrct_Payee_Cde", 
            "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Rec_Type_20_Detail_Cntrct_Pymnt_Type_Ind = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1.newFieldInGroup("pnd_Rec_Type_20_Detail_Cntrct_Pymnt_Type_Ind", 
            "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Rec_Type_20_Detail_Cntrct_Sttlmnt_Type_Ind = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1.newFieldInGroup("pnd_Rec_Type_20_Detail_Cntrct_Sttlmnt_Type_Ind", 
            "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Rec_Type_20_Detail_Cntrct_Option_Cde = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1.newFieldInGroup("pnd_Rec_Type_20_Detail_Cntrct_Option_Cde", 
            "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        pnd_Rec_Type_20_Detail_Cntrct_Mode_Cde = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1.newFieldInGroup("pnd_Rec_Type_20_Detail_Cntrct_Mode_Cde", 
            "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        pnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20.newFieldInGroup("pnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top", 
            "#PYMNT-DED-GRP-TOP", FieldType.NUMERIC, 3);
        pnd_Rec_Type_20_Detail_Pymnt_Ded_Grp = pnd_Rec_Type_20_Detail_Pnd_Record_Type_20.newGroupInGroup("pnd_Rec_Type_20_Detail_Pymnt_Ded_Grp", "PYMNT-DED-GRP");
        pnd_Rec_Type_20_Detail_Pymnt_Ded_Cde = pnd_Rec_Type_20_Detail_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Rec_Type_20_Detail_Pymnt_Ded_Cde", "PYMNT-DED-CDE", 
            FieldType.NUMERIC, 3, new DbsArrayController(1,10));
        pnd_Rec_Type_20_Detail_Pymnt_Ded_Payee_Cde = pnd_Rec_Type_20_Detail_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Rec_Type_20_Detail_Pymnt_Ded_Payee_Cde", 
            "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 8, new DbsArrayController(1,10));
        pnd_Rec_Type_20_Detail_Pymnt_Ded_Amt = pnd_Rec_Type_20_Detail_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Rec_Type_20_Detail_Pymnt_Ded_Amt", "PYMNT-DED-AMT", 
            FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,10));

        pnd_Rec_Type_30_Detail = newGroupInRecord("pnd_Rec_Type_30_Detail", "#REC-TYPE-30-DETAIL");
        pnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_1 = pnd_Rec_Type_30_Detail.newFieldInGroup("pnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_1", "#REC-TYPE-30-DET-1", 
            FieldType.STRING, 250);
        pnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_2 = pnd_Rec_Type_30_Detail.newFieldInGroup("pnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_2", "#REC-TYPE-30-DET-2", 
            FieldType.STRING, 209);
        pnd_Rec_Type_30_DetailRedef6 = newGroupInRecord("pnd_Rec_Type_30_DetailRedef6", "Redefines", pnd_Rec_Type_30_Detail);
        pnd_Rec_Type_30_Detail_Pnd_Record_Type_30 = pnd_Rec_Type_30_DetailRedef6.newGroupInGroup("pnd_Rec_Type_30_Detail_Pnd_Record_Type_30", "#RECORD-TYPE-30");
        pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1 = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30.newGroupInGroup("pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1", 
            "#RECORD-TYPE-30-GRP1");
        pnd_Rec_Type_30_Detail_Rcrd_Typ = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Rec_Type_30_Detail_Rcrd_Typ", "RCRD-TYP", 
            FieldType.STRING, 1);
        pnd_Rec_Type_30_Detail_Cntrct_Orgn_Cde = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Rec_Type_30_Detail_Cntrct_Orgn_Cde", 
            "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Rec_Type_30_Detail_Cntrct_Ppcn_Nbr = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Rec_Type_30_Detail_Cntrct_Ppcn_Nbr", 
            "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Rec_Type_30_Detail_Cntrct_Payee_Cde = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Rec_Type_30_Detail_Cntrct_Payee_Cde", 
            "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Rec_Type_30_Detail_Cntrct_Invrse_Dte = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Rec_Type_30_Detail_Cntrct_Invrse_Dte", 
            "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Rec_Type_30_Detail_Pymnt_Prcss_Seq_Nbr = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 7);
        pnd_Rec_Type_30_Detail_Ph_Name = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1.newGroupInGroup("pnd_Rec_Type_30_Detail_Ph_Name", "PH-NAME");
        pnd_Rec_Type_30_Detail_Ph_Last_Name = pnd_Rec_Type_30_Detail_Ph_Name.newFieldInGroup("pnd_Rec_Type_30_Detail_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Rec_Type_30_Detail_Ph_First_Name = pnd_Rec_Type_30_Detail_Ph_Name.newFieldInGroup("pnd_Rec_Type_30_Detail_Ph_First_Name", "PH-FIRST-NAME", 
            FieldType.STRING, 10);
        pnd_Rec_Type_30_Detail_Ph_Middle_Name = pnd_Rec_Type_30_Detail_Ph_Name.newFieldInGroup("pnd_Rec_Type_30_Detail_Ph_Middle_Name", "PH-MIDDLE-NAME", 
            FieldType.STRING, 12);
        pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30.newGroupInGroup("pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp", 
            "#PYMNT-NME-AND-ADDR-GRP");
        pnd_Rec_Type_30_Detail_Pymnt_Nme = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Nme", "PYMNT-NME", 
            FieldType.STRING, 38);
        pnd_Rec_Type_30_Detail_Pymnt_Addr_Line1_Txt = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Addr_Line1_Txt", 
            "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 35);
        pnd_Rec_Type_30_Detail_Pymnt_Addr_Line2_Txt = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Addr_Line2_Txt", 
            "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 35);
        pnd_Rec_Type_30_Detail_Pymnt_Addr_Line3_Txt = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Addr_Line3_Txt", 
            "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 35);
        pnd_Rec_Type_30_Detail_Pymnt_Addr_Line4_Txt = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Addr_Line4_Txt", 
            "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 35);
        pnd_Rec_Type_30_Detail_Pymnt_Addr_Line5_Txt = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Addr_Line5_Txt", 
            "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 35);
        pnd_Rec_Type_30_Detail_Pymnt_Addr_Line6_Txt = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Addr_Line6_Txt", 
            "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 35);
        pnd_Rec_Type_30_Detail_Pymnt_Addr_Zip_Cde = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Addr_Zip_Cde", 
            "PYMNT-ADDR-ZIP-CDE", FieldType.STRING, 9);
        pnd_Rec_Type_30_Detail_Pymnt_Postl_Data = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Postl_Data", 
            "PYMNT-POSTL-DATA", FieldType.STRING, 32);
        pnd_Rec_Type_30_Detail_Pymnt_Addr_Type_Ind = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Addr_Type_Ind", 
            "PYMNT-ADDR-TYPE-IND", FieldType.STRING, 1);
        pnd_Rec_Type_30_Detail_Pymnt_Addr_Last_Chg_Dte = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Addr_Last_Chg_Dte", 
            "PYMNT-ADDR-LAST-CHG-DTE", FieldType.NUMERIC, 8);
        pnd_Rec_Type_30_Detail_Pymnt_Addr_Last_Chg_Tme = pnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Addr_Last_Chg_Tme", 
            "PYMNT-ADDR-LAST-CHG-TME", FieldType.NUMERIC, 7);
        pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2 = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30.newGroupInGroup("pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2", 
            "#RECORD-TYPE-30-GRP2");
        pnd_Rec_Type_30_Detail_Pymnt_Eft_Transit_Id = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Eft_Transit_Id", 
            "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9);
        pnd_Rec_Type_30_Detail_Pymnt_Eft_Acct_Nbr = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Eft_Acct_Nbr", 
            "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        pnd_Rec_Type_30_Detail_Pymnt_Chk_Sav_Ind = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Chk_Sav_Ind", 
            "PYMNT-CHK-SAV-IND", FieldType.STRING, 1);
        pnd_Rec_Type_30_Detail_Pymnt_Deceased_Nme = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2.newFieldInGroup("pnd_Rec_Type_30_Detail_Pymnt_Deceased_Nme", 
            "PYMNT-DECEASED-NME", FieldType.STRING, 38);
        pnd_Rec_Type_30_Detail_Cntrct_Hold_Tme = pnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2.newFieldInGroup("pnd_Rec_Type_30_Detail_Cntrct_Hold_Tme", 
            "CNTRCT-HOLD-TME", FieldType.TIME);

        pnd_Rec_Type_40_Detail = newGroupInRecord("pnd_Rec_Type_40_Detail", "#REC-TYPE-40-DETAIL");
        pnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_1 = pnd_Rec_Type_40_Detail.newFieldInGroup("pnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_1", "#REC-TYPE-40-DET-1", 
            FieldType.STRING, 250);
        pnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_2 = pnd_Rec_Type_40_Detail.newFieldInGroup("pnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_2", "#REC-TYPE-40-DET-2", 
            FieldType.STRING, 209);
        pnd_Rec_Type_40_DetailRedef7 = newGroupInRecord("pnd_Rec_Type_40_DetailRedef7", "Redefines", pnd_Rec_Type_40_Detail);
        pnd_Rec_Type_40_Detail_Pnd_Record_Type_40 = pnd_Rec_Type_40_DetailRedef7.newGroupInGroup("pnd_Rec_Type_40_Detail_Pnd_Record_Type_40", "#RECORD-TYPE-40");
        pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1 = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40.newGroupInGroup("pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1", 
            "#RECORD-TYPE-40-GRP1");
        pnd_Rec_Type_40_Detail_Cntrct_Orgn_Cde = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Rec_Type_40_Detail_Cntrct_Orgn_Cde", 
            "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Rec_Type_40_Detail_Cntrct_Ppcn_Nbr = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Rec_Type_40_Detail_Cntrct_Ppcn_Nbr", 
            "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Rec_Type_40_Detail_Pymnt_Prcss_Seq_Nbr = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Rec_Type_40_Detail_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        pnd_Rec_Type_40_Detail_Pymnt_Check_Dte = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Rec_Type_40_Detail_Pymnt_Check_Dte", 
            "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Rec_Type_40_Detail_Cntrct_Good_Contract = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Rec_Type_40_Detail_Cntrct_Good_Contract", 
            "CNTRCT-GOOD-CONTRACT", FieldType.BOOLEAN);
        pnd_Rec_Type_40_Detail_Cntrct_Invalid_Cond_Ind = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Rec_Type_40_Detail_Cntrct_Invalid_Cond_Ind", 
            "CNTRCT-INVALID-COND-IND", FieldType.BOOLEAN);
        pnd_Rec_Type_40_Detail_Cntrct_Multi_Payee = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Rec_Type_40_Detail_Cntrct_Multi_Payee", 
            "CNTRCT-MULTI-PAYEE", FieldType.BOOLEAN);
        pnd_Rec_Type_40_Detail_Pnd_Good_Letter = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Rec_Type_40_Detail_Pnd_Good_Letter", 
            "#GOOD-LETTER", FieldType.BOOLEAN);
        pnd_Rec_Type_40_Detail_Pnd_Plan_Type_Display = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Rec_Type_40_Detail_Pnd_Plan_Type_Display", 
            "#PLAN-TYPE-DISPLAY", FieldType.BOOLEAN);
        pnd_Rec_Type_40_Detail_Cntrct_Ivc_Amt = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1.newFieldInGroup("pnd_Rec_Type_40_Detail_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40.newFieldInGroup("pnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top", 
            "#PLAN-DATA-TOP", FieldType.PACKED_DECIMAL, 3);
        pnd_Rec_Type_40_Detail_Plan_Data = pnd_Rec_Type_40_Detail_Pnd_Record_Type_40.newGroupInGroup("pnd_Rec_Type_40_Detail_Plan_Data", "PLAN-DATA");
        pnd_Rec_Type_40_Detail_Plan_Employer_Name = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Employer_Name", "PLAN-EMPLOYER-NAME", 
            FieldType.STRING, 35);
        pnd_Rec_Type_40_Detail_Plan_Type = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Type", "PLAN-TYPE", FieldType.STRING, 
            10);
        pnd_Rec_Type_40_Detail_Plan_Cash_Avail = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Cash_Avail", "PLAN-CASH-AVAIL", 
            FieldType.STRING, 50);
        pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb", "PLAN-EMPLOYER-CNTRB", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb_Earn = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb_Earn", 
            "PLAN-EMPLOYER-CNTRB-EARN", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn", "PLAN-EMPLOYEE-RDCTN", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn_Earn = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn_Earn", 
            "PLAN-EMPLOYEE-RDCTN-EARN", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn", "PLAN-EMPLOYEE-DDCTN", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn_Earn = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn_Earn", 
            "PLAN-EMPLOYEE-DDCTN-EARN", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_40_Detail_Plan_Acc_123186 = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Acc_123186", "PLAN-ACC-123186", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_40_Detail_Plan_Acc_123188 = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Acc_123188", "PLAN-ACC-123188", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn", 
            "PLAN-ACC-123188-RDCTN", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn_Earn = pnd_Rec_Type_40_Detail_Plan_Data.newFieldInGroup("pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn_Earn", 
            "PLAN-ACC-123188-RDCTN-EARN", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Rec_Type_45_Detail = newGroupInRecord("pnd_Rec_Type_45_Detail", "#REC-TYPE-45-DETAIL");
        pnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_1 = pnd_Rec_Type_45_Detail.newFieldInGroup("pnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_1", "#REC-TYPE-45-DET-1", 
            FieldType.STRING, 250);
        pnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_2 = pnd_Rec_Type_45_Detail.newFieldInGroup("pnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_2", "#REC-TYPE-45-DET-2", 
            FieldType.STRING, 209);
        pnd_Rec_Type_45_DetailRedef8 = newGroupInRecord("pnd_Rec_Type_45_DetailRedef8", "Redefines", pnd_Rec_Type_45_Detail);
        pnd_Rec_Type_45_Detail_Pnd_Record_Type_45 = pnd_Rec_Type_45_DetailRedef8.newGroupInGroup("pnd_Rec_Type_45_Detail_Pnd_Record_Type_45", "#RECORD-TYPE-45");
        pnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1 = pnd_Rec_Type_45_Detail_Pnd_Record_Type_45.newGroupInGroup("pnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1", 
            "#RECORD-TYPE-45-GRP1");
        pnd_Rec_Type_45_Detail_Cntrct_Orgn_Cde = pnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1.newFieldInGroup("pnd_Rec_Type_45_Detail_Cntrct_Orgn_Cde", 
            "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Rec_Type_45_Detail_Cntrct_Ppcn_Nbr = pnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1.newFieldInGroup("pnd_Rec_Type_45_Detail_Cntrct_Ppcn_Nbr", 
            "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Rec_Type_45_Detail_Pymnt_Prcss_Seq_Nbr = pnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1.newFieldInGroup("pnd_Rec_Type_45_Detail_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        pnd_Rec_Type_45_Detail_Pymnt_Check_Dte = pnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1.newFieldInGroup("pnd_Rec_Type_45_Detail_Pymnt_Check_Dte", 
            "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Rec_Type_45_Detail_Good_Contract = pnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1.newFieldInGroup("pnd_Rec_Type_45_Detail_Good_Contract", 
            "GOOD-CONTRACT", FieldType.BOOLEAN);
        pnd_Rec_Type_45_Detail_Invalid_Cond_Ind = pnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1.newFieldInGroup("pnd_Rec_Type_45_Detail_Invalid_Cond_Ind", 
            "INVALID-COND-IND", FieldType.BOOLEAN);
        pnd_Rec_Type_45_Detail_Pnd_Invalid_Reasons_Top = pnd_Rec_Type_45_Detail_Pnd_Record_Type_45.newFieldInGroup("pnd_Rec_Type_45_Detail_Pnd_Invalid_Reasons_Top", 
            "#INVALID-REASONS-TOP", FieldType.PACKED_DECIMAL, 3);
        pnd_Rec_Type_45_Detail_Invalid_Reasons = pnd_Rec_Type_45_Detail_Pnd_Record_Type_45.newFieldArrayInGroup("pnd_Rec_Type_45_Detail_Invalid_Reasons", 
            "INVALID-REASONS", FieldType.STRING, 50, new DbsArrayController(1,8));

        this.setRecordName("LdaFcpl378");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaFcpl378() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
