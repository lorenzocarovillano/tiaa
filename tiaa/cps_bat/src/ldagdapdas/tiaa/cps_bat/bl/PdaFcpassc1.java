/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:34 PM
**        * FROM NATURAL PDA     : FCPASSC1
************************************************************
**        * FILE NAME            : PdaFcpassc1.java
**        * CLASS NAME           : PdaFcpassc1
**        * INSTANCE NAME        : PdaFcpassc1
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpassc1 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpassc1;
    private DbsField pnd_Fcpassc1_Pnd_Accum_Occur_1;
    private DbsField pnd_Fcpassc1_Pnd_Accum_Occur_2;

    public DbsGroup getPnd_Fcpassc1() { return pnd_Fcpassc1; }

    public DbsField getPnd_Fcpassc1_Pnd_Accum_Occur_1() { return pnd_Fcpassc1_Pnd_Accum_Occur_1; }

    public DbsField getPnd_Fcpassc1_Pnd_Accum_Occur_2() { return pnd_Fcpassc1_Pnd_Accum_Occur_2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpassc1 = dbsRecord.newGroupInRecord("pnd_Fcpassc1", "#FCPASSC1");
        pnd_Fcpassc1.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpassc1_Pnd_Accum_Occur_1 = pnd_Fcpassc1.newFieldInGroup("pnd_Fcpassc1_Pnd_Accum_Occur_1", "#ACCUM-OCCUR-1", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpassc1_Pnd_Accum_Occur_2 = pnd_Fcpassc1.newFieldInGroup("pnd_Fcpassc1_Pnd_Accum_Occur_2", "#ACCUM-OCCUR-2", FieldType.PACKED_DECIMAL, 3);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpassc1(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

