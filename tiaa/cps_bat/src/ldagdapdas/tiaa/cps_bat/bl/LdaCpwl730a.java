/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:54:09 PM
**        * FROM NATURAL LDA     : CPWL730A
************************************************************
**        * FILE NAME            : LdaCpwl730a.java
**        * CLASS NAME           : LdaCpwl730a
**        * INSTANCE NAME        : LdaCpwl730a
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpwl730a extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_fcp_Acct_Pymnt;
    private DbsField fcp_Acct_Pymnt_Cntrct_Orgn_Cde;
    private DbsField fcp_Acct_Pymnt_Pymnt_Stats_Cde;
    private DbsField fcp_Acct_Pymnt_Pymnt_Transmission_Stats_Cde;
    private DbsField fcp_Acct_Pymnt_Pymnt_Pay_Type_Req_Ind;
    private DbsField fcp_Acct_Pymnt_Cntrct_Hold_Cde;
    private DbsField fcp_Acct_Pymnt_Pymnt_Dte;
    private DbsField fcp_Acct_Pymnt_Invrse_Pymnt_Dte;
    private DbsField fcp_Acct_Pymnt_Pymnt_Nbr;
    private DbsField fcp_Acct_Pymnt_Pymnt_Amt;
    private DbsField fcp_Acct_Pymnt_Pymnt_Seq_Nbr;
    private DbsField fcp_Acct_Pymnt_Pymnt_Id;
    private DbsField fcp_Acct_Pymnt_Pymnt_Foreign_Country;
    private DbsField fcp_Acct_Pymnt_Pymnt_Source_Cde;
    private DbsField fcp_Acct_Pymnt_Pymnt_Name1;
    private DbsField fcp_Acct_Pymnt_Pymnt_Name2;
    private DbsField fcp_Acct_Pymnt_Bank_Routing;
    private DbsField fcp_Acct_Pymnt_Bank_Account;
    private DbsField fcp_Acct_Pymnt_Bank_Account_Name;
    private DbsField fcp_Acct_Pymnt_Bank_Account_Cde;
    private DbsField fcp_Acct_Pymnt_Bank_Transmit_Ind;
    private DbsField fcp_Acct_Pymnt_Bank_Name;
    private DbsField fcp_Acct_Pymnt_Bank_Transmission_Cde;
    private DbsField fcp_Acct_Pymnt_Invrse_Process_Dte;
    private DbsField fcp_Acct_Pymnt_Annt_Soc_Sec_Nbr;
    private DbsField fcp_Acct_Pymnt_Pymnt_Refund_Type;
    private DbsField fcp_Acct_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Acct_Pymnt_Ppg_Nbr;
    private DbsField fcp_Acct_Pymnt_Bank_Internal_Pospay_Ind;

    public DataAccessProgramView getVw_fcp_Acct_Pymnt() { return vw_fcp_Acct_Pymnt; }

    public DbsField getFcp_Acct_Pymnt_Cntrct_Orgn_Cde() { return fcp_Acct_Pymnt_Cntrct_Orgn_Cde; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Stats_Cde() { return fcp_Acct_Pymnt_Pymnt_Stats_Cde; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Transmission_Stats_Cde() { return fcp_Acct_Pymnt_Pymnt_Transmission_Stats_Cde; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Pay_Type_Req_Ind() { return fcp_Acct_Pymnt_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getFcp_Acct_Pymnt_Cntrct_Hold_Cde() { return fcp_Acct_Pymnt_Cntrct_Hold_Cde; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Dte() { return fcp_Acct_Pymnt_Pymnt_Dte; }

    public DbsField getFcp_Acct_Pymnt_Invrse_Pymnt_Dte() { return fcp_Acct_Pymnt_Invrse_Pymnt_Dte; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Nbr() { return fcp_Acct_Pymnt_Pymnt_Nbr; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Amt() { return fcp_Acct_Pymnt_Pymnt_Amt; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Seq_Nbr() { return fcp_Acct_Pymnt_Pymnt_Seq_Nbr; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Id() { return fcp_Acct_Pymnt_Pymnt_Id; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Foreign_Country() { return fcp_Acct_Pymnt_Pymnt_Foreign_Country; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Source_Cde() { return fcp_Acct_Pymnt_Pymnt_Source_Cde; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Name1() { return fcp_Acct_Pymnt_Pymnt_Name1; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Name2() { return fcp_Acct_Pymnt_Pymnt_Name2; }

    public DbsField getFcp_Acct_Pymnt_Bank_Routing() { return fcp_Acct_Pymnt_Bank_Routing; }

    public DbsField getFcp_Acct_Pymnt_Bank_Account() { return fcp_Acct_Pymnt_Bank_Account; }

    public DbsField getFcp_Acct_Pymnt_Bank_Account_Name() { return fcp_Acct_Pymnt_Bank_Account_Name; }

    public DbsField getFcp_Acct_Pymnt_Bank_Account_Cde() { return fcp_Acct_Pymnt_Bank_Account_Cde; }

    public DbsField getFcp_Acct_Pymnt_Bank_Transmit_Ind() { return fcp_Acct_Pymnt_Bank_Transmit_Ind; }

    public DbsField getFcp_Acct_Pymnt_Bank_Name() { return fcp_Acct_Pymnt_Bank_Name; }

    public DbsField getFcp_Acct_Pymnt_Bank_Transmission_Cde() { return fcp_Acct_Pymnt_Bank_Transmission_Cde; }

    public DbsField getFcp_Acct_Pymnt_Invrse_Process_Dte() { return fcp_Acct_Pymnt_Invrse_Process_Dte; }

    public DbsField getFcp_Acct_Pymnt_Annt_Soc_Sec_Nbr() { return fcp_Acct_Pymnt_Annt_Soc_Sec_Nbr; }

    public DbsField getFcp_Acct_Pymnt_Pymnt_Refund_Type() { return fcp_Acct_Pymnt_Pymnt_Refund_Type; }

    public DbsField getFcp_Acct_Pymnt_Cntrct_Ppcn_Nbr() { return fcp_Acct_Pymnt_Cntrct_Ppcn_Nbr; }

    public DbsField getFcp_Acct_Pymnt_Ppg_Nbr() { return fcp_Acct_Pymnt_Ppg_Nbr; }

    public DbsField getFcp_Acct_Pymnt_Bank_Internal_Pospay_Ind() { return fcp_Acct_Pymnt_Bank_Internal_Pospay_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_fcp_Acct_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Acct_Pymnt", "FCP-ACCT-PYMNT"), "FCP_ACCT_PYMNT", "FCP_ACCT_PYMNT");
        fcp_Acct_Pymnt_Cntrct_Orgn_Cde = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Acct_Pymnt_Pymnt_Stats_Cde = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_STATS_CDE");
        fcp_Acct_Pymnt_Pymnt_Transmission_Stats_Cde = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Transmission_Stats_Cde", "PYMNT-TRANSMISSION-STATS-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PYMNT_TRANSMISSION_STATS_CDE");
        fcp_Acct_Pymnt_Pymnt_Pay_Type_Req_Ind = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        fcp_Acct_Pymnt_Cntrct_Hold_Cde = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        fcp_Acct_Pymnt_Pymnt_Dte = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Dte", "PYMNT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_DTE");
        fcp_Acct_Pymnt_Invrse_Pymnt_Dte = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Invrse_Pymnt_Dte", "INVRSE-PYMNT-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "INVRSE_PYMNT_DTE");
        fcp_Acct_Pymnt_Pymnt_Nbr = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        fcp_Acct_Pymnt_Pymnt_Amt = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Amt", "PYMNT-AMT", FieldType.PACKED_DECIMAL, 15, 
            2, RepeatingFieldStrategy.None, "PYMNT_AMT");
        fcp_Acct_Pymnt_Pymnt_Seq_Nbr = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Seq_Nbr", "PYMNT-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            13, RepeatingFieldStrategy.None, "PYMNT_SEQ_NBR");
        fcp_Acct_Pymnt_Pymnt_Id = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Id", "PYMNT-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "PYMNT_ID");
        fcp_Acct_Pymnt_Pymnt_Foreign_Country = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Foreign_Country", "PYMNT-FOREIGN-COUNTRY", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "PYMNT_FOREIGN_COUNTRY");
        fcp_Acct_Pymnt_Pymnt_Source_Cde = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Source_Cde", "PYMNT-SOURCE-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "PYMNT_SOURCE_CDE");
        fcp_Acct_Pymnt_Pymnt_Name1 = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Name1", "PYMNT-NAME1", FieldType.STRING, 40, 
            RepeatingFieldStrategy.None, "PYMNT_NAME1");
        fcp_Acct_Pymnt_Pymnt_Name2 = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Name2", "PYMNT-NAME2", FieldType.STRING, 40, 
            RepeatingFieldStrategy.None, "PYMNT_NAME2");
        fcp_Acct_Pymnt_Bank_Routing = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Bank_Routing", "BANK-ROUTING", FieldType.STRING, 9, 
            RepeatingFieldStrategy.None, "BANK_ROUTING");
        fcp_Acct_Pymnt_Bank_Account = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21, 
            RepeatingFieldStrategy.None, "BANK_ACCOUNT");
        fcp_Acct_Pymnt_Bank_Account_Name = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Bank_Account_Name", "BANK-ACCOUNT-NAME", FieldType.STRING, 
            50, RepeatingFieldStrategy.None, "BANK_ACCOUNT_NAME");
        fcp_Acct_Pymnt_Bank_Account_Cde = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Bank_Account_Cde", "BANK-ACCOUNT-CDE", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "BANK_ACCOUNT_CDE");
        fcp_Acct_Pymnt_Bank_Transmit_Ind = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Bank_Transmit_Ind", "BANK-TRANSMIT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BANK_TRANSMIT_IND");
        fcp_Acct_Pymnt_Bank_Name = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Bank_Name", "BANK-NAME", FieldType.STRING, 50, RepeatingFieldStrategy.None, 
            "BANK_NAME");
        fcp_Acct_Pymnt_Bank_Transmission_Cde = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Bank_Transmission_Cde", "BANK-TRANSMISSION-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "BANK_TRANSMISSION_CDE");
        fcp_Acct_Pymnt_Invrse_Process_Dte = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Invrse_Process_Dte", "INVRSE-PROCESS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "INVRSE_PROCESS_DTE");
        fcp_Acct_Pymnt_Annt_Soc_Sec_Nbr = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "ANNT_SOC_SEC_NBR");
        fcp_Acct_Pymnt_Pymnt_Refund_Type = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Pymnt_Refund_Type", "PYMNT-REFUND-TYPE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "PYMNT_REFUND_TYPE");
        fcp_Acct_Pymnt_Cntrct_Ppcn_Nbr = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Acct_Pymnt_Ppg_Nbr = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Ppg_Nbr", "PPG-NBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PPG_NBR");
        fcp_Acct_Pymnt_Bank_Internal_Pospay_Ind = vw_fcp_Acct_Pymnt.getRecord().newFieldInGroup("fcp_Acct_Pymnt_Bank_Internal_Pospay_Ind", "BANK-INTERNAL-POSPAY-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BANK_INTERNAL_POSPAY_IND");

        this.setRecordName("LdaCpwl730a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_fcp_Acct_Pymnt.reset();
    }

    // Constructor
    public LdaCpwl730a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
