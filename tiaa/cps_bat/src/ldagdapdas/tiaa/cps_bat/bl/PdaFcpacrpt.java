/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:30 PM
**        * FROM NATURAL PDA     : FCPACRPT
************************************************************
**        * FILE NAME            : PdaFcpacrpt.java
**        * CLASS NAME           : PdaFcpacrpt
**        * INSTANCE NAME        : PdaFcpacrpt
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpacrpt extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpacrpt;
    private DbsField pnd_Fcpacrpt_Pnd_Program;
    private DbsField pnd_Fcpacrpt_Pnd_Title;
    private DbsField pnd_Fcpacrpt_Pnd_No_Abend;
    private DbsField pnd_Fcpacrpt_Pnd_Diff;
    private DbsField pnd_Fcpacrpt_Pnd_Truth_Table;

    public DbsGroup getPnd_Fcpacrpt() { return pnd_Fcpacrpt; }

    public DbsField getPnd_Fcpacrpt_Pnd_Program() { return pnd_Fcpacrpt_Pnd_Program; }

    public DbsField getPnd_Fcpacrpt_Pnd_Title() { return pnd_Fcpacrpt_Pnd_Title; }

    public DbsField getPnd_Fcpacrpt_Pnd_No_Abend() { return pnd_Fcpacrpt_Pnd_No_Abend; }

    public DbsField getPnd_Fcpacrpt_Pnd_Diff() { return pnd_Fcpacrpt_Pnd_Diff; }

    public DbsField getPnd_Fcpacrpt_Pnd_Truth_Table() { return pnd_Fcpacrpt_Pnd_Truth_Table; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpacrpt = dbsRecord.newGroupInRecord("pnd_Fcpacrpt", "#FCPACRPT");
        pnd_Fcpacrpt.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpacrpt_Pnd_Program = pnd_Fcpacrpt.newFieldInGroup("pnd_Fcpacrpt_Pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Fcpacrpt_Pnd_Title = pnd_Fcpacrpt.newFieldInGroup("pnd_Fcpacrpt_Pnd_Title", "#TITLE", FieldType.STRING, 40);
        pnd_Fcpacrpt_Pnd_No_Abend = pnd_Fcpacrpt.newFieldInGroup("pnd_Fcpacrpt_Pnd_No_Abend", "#NO-ABEND", FieldType.BOOLEAN);
        pnd_Fcpacrpt_Pnd_Diff = pnd_Fcpacrpt.newFieldInGroup("pnd_Fcpacrpt_Pnd_Diff", "#DIFF", FieldType.BOOLEAN);
        pnd_Fcpacrpt_Pnd_Truth_Table = pnd_Fcpacrpt.newFieldArrayInGroup("pnd_Fcpacrpt_Pnd_Truth_Table", "#TRUTH-TABLE", FieldType.BOOLEAN, new DbsArrayController(1,
            50));

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpacrpt(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

