/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:32 PM
**        * FROM NATURAL PDA     : FCPAPMNR
************************************************************
**        * FILE NAME            : PdaFcpapmnr.java
**        * CLASS NAME           : PdaFcpapmnr
**        * INSTANCE NAME        : PdaFcpapmnr
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpapmnr extends PdaBase
{
    // Properties
    private DbsGroup pymnt_Work_Fields;
    private DbsField pymnt_Work_Fields_Pymnt_Abend_Ind;
    private DbsField pymnt_Work_Fields_Pymnt_Return_Cde;
    private DbsField pymnt_Work_Fields_Pymnt_Isn;
    private DbsGroup pymnt;
    private DbsField pymnt_Cntrct_Ppcn_Nbr;
    private DbsField pymnt_Cntrct_Payee_Cde;
    private DbsField pymnt_Cntrct_Invrse_Dte;
    private DbsField pymnt_Cntrct_Check_Crrncy_Cde;
    private DbsField pymnt_Cntrct_Crrncy_Cde;
    private DbsField pymnt_Cntrct_Orgn_Cde;
    private DbsField pymnt_Cntrct_Qlfied_Cde;
    private DbsField pymnt_Cntrct_Pymnt_Type_Ind;
    private DbsField pymnt_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pymnt_Cntrct_Sps_Cde;
    private DbsField pymnt_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pymnt_Pymnt_Stats_Cde;
    private DbsField pymnt_Pymnt_Annot_Ind;
    private DbsField pymnt_Pymnt_Cmbne_Ind;
    private DbsField pymnt_Pymnt_Ftre_Ind;
    private DbsField pymnt_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pymnt_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pymnt_Pymnt_Inst_Rep_Cde;
    private DbsField pymnt_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pymnt_Annt_Soc_Sec_Ind;
    private DbsField pymnt_Pymnt_Pay_Type_Req_Ind;
    private DbsField pymnt_Cntrct_Rqst_Settl_Dte;
    private DbsField pymnt_Cntrct_Rqst_Dte;
    private DbsField pymnt_Cntrct_Rqst_Settl_Tme;
    private DbsField pymnt_Cntrct_Unq_Id_Nbr;
    private DbsField pymnt_Cntrct_Type_Cde;
    private DbsField pymnt_Cntrct_Lob_Cde;
    private DbsField pymnt_Cntrct_Sub_Lob_Cde;
    private DbsField pymnt_Cntrct_Ia_Lob_Cde;
    private DbsField pymnt_Cntrct_Cref_Nbr;
    private DbsField pymnt_Cntrct_Option_Cde;
    private DbsField pymnt_Cntrct_Mode_Cde;
    private DbsField pymnt_Cntrct_Pymnt_Dest_Cde;
    private DbsField pymnt_Cntrct_Roll_Dest_Cde;
    private DbsField pymnt_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pymnt_Cntrct_Hold_Cde;
    private DbsField pymnt_Cntrct_Hold_Grp;
    private DbsField pymnt_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pymnt_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pymnt_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pymnt_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pymnt_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pymnt_Cntrct_Da_Cref_1_Nbr;
    private DbsField pymnt_Cntrct_Da_Cref_2_Nbr;
    private DbsField pymnt_Cntrct_Da_Cref_3_Nbr;
    private DbsField pymnt_Cntrct_Da_Cref_4_Nbr;
    private DbsField pymnt_Cntrct_Da_Cref_5_Nbr;
    private DbsField pymnt_Annt_Soc_Sec_Nbr;
    private DbsField pymnt_Annt_Ctznshp_Cde;
    private DbsField pymnt_Annt_Rsdncy_Cde;
    private DbsField pymnt_Pymnt_Split_Cde;
    private DbsField pymnt_Pymnt_Split_Reasn_Cde;
    private DbsField pymnt_Pymnt_Check_Dte;
    private DbsField pymnt_Pymnt_Cycle_Dte;
    private DbsField pymnt_Pymnt_Eft_Dte;
    private DbsField pymnt_Pymnt_Rqst_Pct;
    private DbsField pymnt_Pymnt_Rqst_Amt;
    private DbsField pymnt_Pymnt_Check_Nbr;
    private DbsField pymnt_Pymnt_Prcss_Seq_Nbr;
    private DbsGroup pymnt_Pymnt_Prcss_Seq_NbrRedef1;
    private DbsField pymnt_Pymnt_Prcss_Seq_Num;
    private DbsField pymnt_Pymnt_Instmt_Nbr;
    private DbsField pymnt_Pymnt_Check_Scrty_Nbr;
    private DbsField pymnt_Pymnt_Check_Amt;
    private DbsField pymnt_Pymnt_Settlmnt_Dte;
    private DbsField pymnt_Pymnt_Check_Seq_Nbr;
    private DbsField pymnt_C_Inv_Acct;
    private DbsField pymnt_Inv_Acct_Cde;
    private DbsField pymnt_Inv_Acct_Unit_Qty;
    private DbsField pymnt_Inv_Acct_Unit_Value;
    private DbsField pymnt_Inv_Acct_Settl_Amt;
    private DbsField pymnt_Inv_Acct_Fed_Cde;
    private DbsField pymnt_Inv_Acct_State_Cde;
    private DbsField pymnt_Inv_Acct_Local_Cde;
    private DbsField pymnt_Inv_Acct_Ivc_Amt;
    private DbsField pymnt_Inv_Acct_Dci_Amt;
    private DbsField pymnt_Inv_Acct_Dpi_Amt;
    private DbsField pymnt_Inv_Acct_Start_Accum_Amt;
    private DbsField pymnt_Inv_Acct_End_Accum_Amt;
    private DbsField pymnt_Inv_Acct_Dvdnd_Amt;
    private DbsField pymnt_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pymnt_Inv_Acct_Ivc_Ind;
    private DbsField pymnt_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pymnt_Inv_Acct_Valuat_Period;
    private DbsField pymnt_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pymnt_Inv_Acct_State_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Local_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Exp_Amt;
    private DbsField pymnt_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pymnt_Cntrct_Cmbn_Nbr;
    private DbsField pymnt_Cntrct_Hold_Tme;
    private DbsField pymnt_C_Pymnt_Ded_Grp;
    private DbsField pymnt_Pymnt_Ded_Cde;
    private DbsField pymnt_Pymnt_Ded_Payee_Cde;
    private DbsField pymnt_Pymnt_Ded_Amt;
    private DbsField pymnt_Pymnt_Corp_Wpid;
    private DbsField pymnt_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pymnt_Annt_Locality_Cde;
    private DbsField pymnt_C_Inv_Rtb_Grp;
    private DbsGroup pymnt_Inv_Rtb_Grp;
    private DbsField pymnt_Inv_Rtb_Cde;
    private DbsField pymnt_Inv_Acct_Fed_Dci_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Sta_Dci_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Loc_Dci_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Fed_Dpi_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Sta_Dpi_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Loc_Dpi_Tax_Amt;
    private DbsField pymnt_Pymnt_Acctg_Dte;
    private DbsField pymnt_Pymnt_Intrfce_Dte;
    private DbsField pymnt_Pymnt_Tiaa_Md_Amt;
    private DbsField pymnt_Pymnt_Cref_Md_Amt;
    private DbsField pymnt_Pymnt_Tax_Form;
    private DbsField pymnt_Pymnt_Tax_Exempt_Ind;
    private DbsField pymnt_Pymnt_Tax_Calc_Cde;
    private DbsField pymnt_Pymnt_Method_Cde;
    private DbsField pymnt_Pymnt_Settl_Ivc_Ind;
    private DbsField pymnt_Pymnt_Ia_Issue_Dte;
    private DbsField pymnt_Pymnt_Spouse_Pay_Stats;
    private DbsField pymnt_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pymnt_Cntrct_Cancel_Rdrw_Amt;
    private DbsField pymnt_Cntrct_Hold_Ind;
    private DbsField pymnt_Notused2;
    private DbsGroup pymnt_Notused2Redef2;
    private DbsField pymnt_Pymnt_Commuted_Value;
    private DbsGroup pymnt_Notused2Redef3;
    private DbsField pymnt_Pymnt_Wrrnt_Trans_Type;
    private DbsField pymnt_Cnr_Cs_Rqust_Dte;
    private DbsField pymnt_Cnr_Cs_Rqust_Tme;
    private DbsField pymnt_Cnr_Cs_Rqust_User_Id;
    private DbsField pymnt_Cnr_Cs_Rqust_Term_Id;
    private DbsField pymnt_Notused1;
    private DbsField pymnt_Cnr_Rdrw_Rqust_Tme;
    private DbsField pymnt_Cnr_Rdrw_Rqust_User_Id;
    private DbsField pymnt_Cnr_Rdrw_Rqust_Term_Id;
    private DbsField pymnt_Cnr_Orgnl_Invrse_Dte;
    private DbsField pymnt_Cnr_Orgnl_Prcss_Seq_Nbr;
    private DbsField pymnt_Cnr_Rplcmnt_Invrse_Dte;
    private DbsField pymnt_Cnr_Rplcmnt_Prcss_Seq_Nbr;
    private DbsField pymnt_Cnr_Rdrw_Rqust_Dte;
    private DbsField pymnt_Cnr_Orgnl_Pymnt_Acctg_Dte;
    private DbsField pymnt_Cnr_Orgnl_Pymnt_Intrfce_Dte;
    private DbsField pymnt_Cnr_Cs_Actvty_Cde;
    private DbsField pymnt_Cnr_Cs_Pymnt_Intrfce_Dte;
    private DbsField pymnt_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField pymnt_Cnr_Cs_Pymnt_Check_Dte;
    private DbsField pymnt_Cnr_Cs_New_Stop_Ind;
    private DbsField pymnt_Pymnt_Rprnt_Pndng_Ind;
    private DbsField pymnt_Pymnt_Rprnt_Rqust_Dte;
    private DbsField pymnt_Pymnt_Rprnt_Dte;
    private DbsField pymnt_Cnr_Rdrw_Pymnt_Intrfce_Dte;
    private DbsField pymnt_Cnr_Rdrw_Pymnt_Acctg_Dte;
    private DbsField pymnt_Cnr_Rdrw_Pymnt_Check_Dte;
    private DbsField pymnt_Pymnt_Trigger_Cde;
    private DbsField pymnt_Pymnt_Spcl_Msg_Cde;
    private DbsField pymnt_Cntrct_Annty_Ins_Type;
    private DbsField pymnt_Cntrct_Annty_Type_Cde;
    private DbsField pymnt_Cntrct_Insurance_Option;
    private DbsField pymnt_Cntrct_Life_Contingency;
    private DbsField pymnt_Cntrct_Ac_Lt_10yrs;
    private DbsField pymnt_Tax_Fed_C_Resp_Cde;
    private DbsField pymnt_Tax_Fed_C_Filing_Stat;
    private DbsField pymnt_Tax_Fed_C_Allow_Cnt;
    private DbsField pymnt_Tax_Fed_C_Fixed_Amt;
    private DbsField pymnt_Tax_Fed_C_Fixed_Pct;
    private DbsField pymnt_Tax_Sta_C_Resp_Cde;
    private DbsField pymnt_Tax_Sta_C_Filing_Stat;
    private DbsField pymnt_Tax_Sta_C_Allow_Cnt;
    private DbsField pymnt_Tax_Sta_C_Fixed_Amt;
    private DbsField pymnt_Tax_Sta_C_Fixed_Pct;
    private DbsField pymnt_Tax_Sta_C_Spouse_Cnt;
    private DbsField pymnt_Tax_Sta_C_Blind_Cnt;
    private DbsField pymnt_Tax_Loc_C_Resp_Cde;
    private DbsField pymnt_Tax_Loc_C_Fixed_Amt;
    private DbsField pymnt_Tax_Loc_C_Fixed_Pct;
    private DbsField pymnt_Pymnt_Rtb_Dest_Typ;
    private DbsField pymnt_Egtrra_Eligibility_Ind;
    private DbsField pymnt_Originating_Irc_Cde;
    private DbsGroup pymnt_Originating_Irc_CdeRedef4;
    private DbsField pymnt_Distributing_Irc_Cde;
    private DbsField pymnt_Receiving_Irc_Cde;
    private DbsField pymnt_Cntrct_Money_Source;
    private DbsField pymnt_Roth_Dob;
    private DbsField pymnt_Roth_First_Contrib_Dte;
    private DbsField pymnt_Roth_Death_Dte;
    private DbsField pymnt_Roth_Disability_Dte;
    private DbsField pymnt_Roth_Money_Source;
    private DbsField pymnt_Roth_Qual_Non_Qual_Distrib;

    public DbsGroup getPymnt_Work_Fields() { return pymnt_Work_Fields; }

    public DbsField getPymnt_Work_Fields_Pymnt_Abend_Ind() { return pymnt_Work_Fields_Pymnt_Abend_Ind; }

    public DbsField getPymnt_Work_Fields_Pymnt_Return_Cde() { return pymnt_Work_Fields_Pymnt_Return_Cde; }

    public DbsField getPymnt_Work_Fields_Pymnt_Isn() { return pymnt_Work_Fields_Pymnt_Isn; }

    public DbsGroup getPymnt() { return pymnt; }

    public DbsField getPymnt_Cntrct_Ppcn_Nbr() { return pymnt_Cntrct_Ppcn_Nbr; }

    public DbsField getPymnt_Cntrct_Payee_Cde() { return pymnt_Cntrct_Payee_Cde; }

    public DbsField getPymnt_Cntrct_Invrse_Dte() { return pymnt_Cntrct_Invrse_Dte; }

    public DbsField getPymnt_Cntrct_Check_Crrncy_Cde() { return pymnt_Cntrct_Check_Crrncy_Cde; }

    public DbsField getPymnt_Cntrct_Crrncy_Cde() { return pymnt_Cntrct_Crrncy_Cde; }

    public DbsField getPymnt_Cntrct_Orgn_Cde() { return pymnt_Cntrct_Orgn_Cde; }

    public DbsField getPymnt_Cntrct_Qlfied_Cde() { return pymnt_Cntrct_Qlfied_Cde; }

    public DbsField getPymnt_Cntrct_Pymnt_Type_Ind() { return pymnt_Cntrct_Pymnt_Type_Ind; }

    public DbsField getPymnt_Cntrct_Sttlmnt_Type_Ind() { return pymnt_Cntrct_Sttlmnt_Type_Ind; }

    public DbsField getPymnt_Cntrct_Sps_Cde() { return pymnt_Cntrct_Sps_Cde; }

    public DbsField getPymnt_Pymnt_Rqst_Rmndr_Pct() { return pymnt_Pymnt_Rqst_Rmndr_Pct; }

    public DbsField getPymnt_Pymnt_Stats_Cde() { return pymnt_Pymnt_Stats_Cde; }

    public DbsField getPymnt_Pymnt_Annot_Ind() { return pymnt_Pymnt_Annot_Ind; }

    public DbsField getPymnt_Pymnt_Cmbne_Ind() { return pymnt_Pymnt_Cmbne_Ind; }

    public DbsField getPymnt_Pymnt_Ftre_Ind() { return pymnt_Pymnt_Ftre_Ind; }

    public DbsField getPymnt_Pymnt_Payee_Na_Addr_Trggr() { return pymnt_Pymnt_Payee_Na_Addr_Trggr; }

    public DbsField getPymnt_Pymnt_Payee_Tx_Elct_Trggr() { return pymnt_Pymnt_Payee_Tx_Elct_Trggr; }

    public DbsField getPymnt_Pymnt_Inst_Rep_Cde() { return pymnt_Pymnt_Inst_Rep_Cde; }

    public DbsField getPymnt_Cntrct_Dvdnd_Payee_Ind() { return pymnt_Cntrct_Dvdnd_Payee_Ind; }

    public DbsField getPymnt_Annt_Soc_Sec_Ind() { return pymnt_Annt_Soc_Sec_Ind; }

    public DbsField getPymnt_Pymnt_Pay_Type_Req_Ind() { return pymnt_Pymnt_Pay_Type_Req_Ind; }

    public DbsField getPymnt_Cntrct_Rqst_Settl_Dte() { return pymnt_Cntrct_Rqst_Settl_Dte; }

    public DbsField getPymnt_Cntrct_Rqst_Dte() { return pymnt_Cntrct_Rqst_Dte; }

    public DbsField getPymnt_Cntrct_Rqst_Settl_Tme() { return pymnt_Cntrct_Rqst_Settl_Tme; }

    public DbsField getPymnt_Cntrct_Unq_Id_Nbr() { return pymnt_Cntrct_Unq_Id_Nbr; }

    public DbsField getPymnt_Cntrct_Type_Cde() { return pymnt_Cntrct_Type_Cde; }

    public DbsField getPymnt_Cntrct_Lob_Cde() { return pymnt_Cntrct_Lob_Cde; }

    public DbsField getPymnt_Cntrct_Sub_Lob_Cde() { return pymnt_Cntrct_Sub_Lob_Cde; }

    public DbsField getPymnt_Cntrct_Ia_Lob_Cde() { return pymnt_Cntrct_Ia_Lob_Cde; }

    public DbsField getPymnt_Cntrct_Cref_Nbr() { return pymnt_Cntrct_Cref_Nbr; }

    public DbsField getPymnt_Cntrct_Option_Cde() { return pymnt_Cntrct_Option_Cde; }

    public DbsField getPymnt_Cntrct_Mode_Cde() { return pymnt_Cntrct_Mode_Cde; }

    public DbsField getPymnt_Cntrct_Pymnt_Dest_Cde() { return pymnt_Cntrct_Pymnt_Dest_Cde; }

    public DbsField getPymnt_Cntrct_Roll_Dest_Cde() { return pymnt_Cntrct_Roll_Dest_Cde; }

    public DbsField getPymnt_Cntrct_Dvdnd_Payee_Cde() { return pymnt_Cntrct_Dvdnd_Payee_Cde; }

    public DbsField getPymnt_Cntrct_Hold_Cde() { return pymnt_Cntrct_Hold_Cde; }

    public DbsField getPymnt_Cntrct_Hold_Grp() { return pymnt_Cntrct_Hold_Grp; }

    public DbsField getPymnt_Cntrct_Da_Tiaa_1_Nbr() { return pymnt_Cntrct_Da_Tiaa_1_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Tiaa_2_Nbr() { return pymnt_Cntrct_Da_Tiaa_2_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Tiaa_3_Nbr() { return pymnt_Cntrct_Da_Tiaa_3_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Tiaa_4_Nbr() { return pymnt_Cntrct_Da_Tiaa_4_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Tiaa_5_Nbr() { return pymnt_Cntrct_Da_Tiaa_5_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Cref_1_Nbr() { return pymnt_Cntrct_Da_Cref_1_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Cref_2_Nbr() { return pymnt_Cntrct_Da_Cref_2_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Cref_3_Nbr() { return pymnt_Cntrct_Da_Cref_3_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Cref_4_Nbr() { return pymnt_Cntrct_Da_Cref_4_Nbr; }

    public DbsField getPymnt_Cntrct_Da_Cref_5_Nbr() { return pymnt_Cntrct_Da_Cref_5_Nbr; }

    public DbsField getPymnt_Annt_Soc_Sec_Nbr() { return pymnt_Annt_Soc_Sec_Nbr; }

    public DbsField getPymnt_Annt_Ctznshp_Cde() { return pymnt_Annt_Ctznshp_Cde; }

    public DbsField getPymnt_Annt_Rsdncy_Cde() { return pymnt_Annt_Rsdncy_Cde; }

    public DbsField getPymnt_Pymnt_Split_Cde() { return pymnt_Pymnt_Split_Cde; }

    public DbsField getPymnt_Pymnt_Split_Reasn_Cde() { return pymnt_Pymnt_Split_Reasn_Cde; }

    public DbsField getPymnt_Pymnt_Check_Dte() { return pymnt_Pymnt_Check_Dte; }

    public DbsField getPymnt_Pymnt_Cycle_Dte() { return pymnt_Pymnt_Cycle_Dte; }

    public DbsField getPymnt_Pymnt_Eft_Dte() { return pymnt_Pymnt_Eft_Dte; }

    public DbsField getPymnt_Pymnt_Rqst_Pct() { return pymnt_Pymnt_Rqst_Pct; }

    public DbsField getPymnt_Pymnt_Rqst_Amt() { return pymnt_Pymnt_Rqst_Amt; }

    public DbsField getPymnt_Pymnt_Check_Nbr() { return pymnt_Pymnt_Check_Nbr; }

    public DbsField getPymnt_Pymnt_Prcss_Seq_Nbr() { return pymnt_Pymnt_Prcss_Seq_Nbr; }

    public DbsGroup getPymnt_Pymnt_Prcss_Seq_NbrRedef1() { return pymnt_Pymnt_Prcss_Seq_NbrRedef1; }

    public DbsField getPymnt_Pymnt_Prcss_Seq_Num() { return pymnt_Pymnt_Prcss_Seq_Num; }

    public DbsField getPymnt_Pymnt_Instmt_Nbr() { return pymnt_Pymnt_Instmt_Nbr; }

    public DbsField getPymnt_Pymnt_Check_Scrty_Nbr() { return pymnt_Pymnt_Check_Scrty_Nbr; }

    public DbsField getPymnt_Pymnt_Check_Amt() { return pymnt_Pymnt_Check_Amt; }

    public DbsField getPymnt_Pymnt_Settlmnt_Dte() { return pymnt_Pymnt_Settlmnt_Dte; }

    public DbsField getPymnt_Pymnt_Check_Seq_Nbr() { return pymnt_Pymnt_Check_Seq_Nbr; }

    public DbsField getPymnt_C_Inv_Acct() { return pymnt_C_Inv_Acct; }

    public DbsField getPymnt_Inv_Acct_Cde() { return pymnt_Inv_Acct_Cde; }

    public DbsField getPymnt_Inv_Acct_Unit_Qty() { return pymnt_Inv_Acct_Unit_Qty; }

    public DbsField getPymnt_Inv_Acct_Unit_Value() { return pymnt_Inv_Acct_Unit_Value; }

    public DbsField getPymnt_Inv_Acct_Settl_Amt() { return pymnt_Inv_Acct_Settl_Amt; }

    public DbsField getPymnt_Inv_Acct_Fed_Cde() { return pymnt_Inv_Acct_Fed_Cde; }

    public DbsField getPymnt_Inv_Acct_State_Cde() { return pymnt_Inv_Acct_State_Cde; }

    public DbsField getPymnt_Inv_Acct_Local_Cde() { return pymnt_Inv_Acct_Local_Cde; }

    public DbsField getPymnt_Inv_Acct_Ivc_Amt() { return pymnt_Inv_Acct_Ivc_Amt; }

    public DbsField getPymnt_Inv_Acct_Dci_Amt() { return pymnt_Inv_Acct_Dci_Amt; }

    public DbsField getPymnt_Inv_Acct_Dpi_Amt() { return pymnt_Inv_Acct_Dpi_Amt; }

    public DbsField getPymnt_Inv_Acct_Start_Accum_Amt() { return pymnt_Inv_Acct_Start_Accum_Amt; }

    public DbsField getPymnt_Inv_Acct_End_Accum_Amt() { return pymnt_Inv_Acct_End_Accum_Amt; }

    public DbsField getPymnt_Inv_Acct_Dvdnd_Amt() { return pymnt_Inv_Acct_Dvdnd_Amt; }

    public DbsField getPymnt_Inv_Acct_Net_Pymnt_Amt() { return pymnt_Inv_Acct_Net_Pymnt_Amt; }

    public DbsField getPymnt_Inv_Acct_Ivc_Ind() { return pymnt_Inv_Acct_Ivc_Ind; }

    public DbsField getPymnt_Inv_Acct_Adj_Ivc_Amt() { return pymnt_Inv_Acct_Adj_Ivc_Amt; }

    public DbsField getPymnt_Inv_Acct_Valuat_Period() { return pymnt_Inv_Acct_Valuat_Period; }

    public DbsField getPymnt_Inv_Acct_Fdrl_Tax_Amt() { return pymnt_Inv_Acct_Fdrl_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_State_Tax_Amt() { return pymnt_Inv_Acct_State_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Local_Tax_Amt() { return pymnt_Inv_Acct_Local_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Exp_Amt() { return pymnt_Inv_Acct_Exp_Amt; }

    public DbsField getPymnt_Cntrct_Ec_Oprtr_Id_Nbr() { return pymnt_Cntrct_Ec_Oprtr_Id_Nbr; }

    public DbsField getPymnt_Cntrct_Cmbn_Nbr() { return pymnt_Cntrct_Cmbn_Nbr; }

    public DbsField getPymnt_Cntrct_Hold_Tme() { return pymnt_Cntrct_Hold_Tme; }

    public DbsField getPymnt_C_Pymnt_Ded_Grp() { return pymnt_C_Pymnt_Ded_Grp; }

    public DbsField getPymnt_Pymnt_Ded_Cde() { return pymnt_Pymnt_Ded_Cde; }

    public DbsField getPymnt_Pymnt_Ded_Payee_Cde() { return pymnt_Pymnt_Ded_Payee_Cde; }

    public DbsField getPymnt_Pymnt_Ded_Amt() { return pymnt_Pymnt_Ded_Amt; }

    public DbsField getPymnt_Pymnt_Corp_Wpid() { return pymnt_Pymnt_Corp_Wpid; }

    public DbsField getPymnt_Pymnt_Reqst_Log_Dte_Time() { return pymnt_Pymnt_Reqst_Log_Dte_Time; }

    public DbsField getPymnt_Annt_Locality_Cde() { return pymnt_Annt_Locality_Cde; }

    public DbsField getPymnt_C_Inv_Rtb_Grp() { return pymnt_C_Inv_Rtb_Grp; }

    public DbsGroup getPymnt_Inv_Rtb_Grp() { return pymnt_Inv_Rtb_Grp; }

    public DbsField getPymnt_Inv_Rtb_Cde() { return pymnt_Inv_Rtb_Cde; }

    public DbsField getPymnt_Inv_Acct_Fed_Dci_Tax_Amt() { return pymnt_Inv_Acct_Fed_Dci_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Sta_Dci_Tax_Amt() { return pymnt_Inv_Acct_Sta_Dci_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Loc_Dci_Tax_Amt() { return pymnt_Inv_Acct_Loc_Dci_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Fed_Dpi_Tax_Amt() { return pymnt_Inv_Acct_Fed_Dpi_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Sta_Dpi_Tax_Amt() { return pymnt_Inv_Acct_Sta_Dpi_Tax_Amt; }

    public DbsField getPymnt_Inv_Acct_Loc_Dpi_Tax_Amt() { return pymnt_Inv_Acct_Loc_Dpi_Tax_Amt; }

    public DbsField getPymnt_Pymnt_Acctg_Dte() { return pymnt_Pymnt_Acctg_Dte; }

    public DbsField getPymnt_Pymnt_Intrfce_Dte() { return pymnt_Pymnt_Intrfce_Dte; }

    public DbsField getPymnt_Pymnt_Tiaa_Md_Amt() { return pymnt_Pymnt_Tiaa_Md_Amt; }

    public DbsField getPymnt_Pymnt_Cref_Md_Amt() { return pymnt_Pymnt_Cref_Md_Amt; }

    public DbsField getPymnt_Pymnt_Tax_Form() { return pymnt_Pymnt_Tax_Form; }

    public DbsField getPymnt_Pymnt_Tax_Exempt_Ind() { return pymnt_Pymnt_Tax_Exempt_Ind; }

    public DbsField getPymnt_Pymnt_Tax_Calc_Cde() { return pymnt_Pymnt_Tax_Calc_Cde; }

    public DbsField getPymnt_Pymnt_Method_Cde() { return pymnt_Pymnt_Method_Cde; }

    public DbsField getPymnt_Pymnt_Settl_Ivc_Ind() { return pymnt_Pymnt_Settl_Ivc_Ind; }

    public DbsField getPymnt_Pymnt_Ia_Issue_Dte() { return pymnt_Pymnt_Ia_Issue_Dte; }

    public DbsField getPymnt_Pymnt_Spouse_Pay_Stats() { return pymnt_Pymnt_Spouse_Pay_Stats; }

    public DbsField getPymnt_Cntrct_Cancel_Rdrw_Ind() { return pymnt_Cntrct_Cancel_Rdrw_Ind; }

    public DbsField getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPymnt_Cntrct_Cancel_Rdrw_Amt() { return pymnt_Cntrct_Cancel_Rdrw_Amt; }

    public DbsField getPymnt_Cntrct_Hold_Ind() { return pymnt_Cntrct_Hold_Ind; }

    public DbsField getPymnt_Notused2() { return pymnt_Notused2; }

    public DbsGroup getPymnt_Notused2Redef2() { return pymnt_Notused2Redef2; }

    public DbsField getPymnt_Pymnt_Commuted_Value() { return pymnt_Pymnt_Commuted_Value; }

    public DbsGroup getPymnt_Notused2Redef3() { return pymnt_Notused2Redef3; }

    public DbsField getPymnt_Pymnt_Wrrnt_Trans_Type() { return pymnt_Pymnt_Wrrnt_Trans_Type; }

    public DbsField getPymnt_Cnr_Cs_Rqust_Dte() { return pymnt_Cnr_Cs_Rqust_Dte; }

    public DbsField getPymnt_Cnr_Cs_Rqust_Tme() { return pymnt_Cnr_Cs_Rqust_Tme; }

    public DbsField getPymnt_Cnr_Cs_Rqust_User_Id() { return pymnt_Cnr_Cs_Rqust_User_Id; }

    public DbsField getPymnt_Cnr_Cs_Rqust_Term_Id() { return pymnt_Cnr_Cs_Rqust_Term_Id; }

    public DbsField getPymnt_Notused1() { return pymnt_Notused1; }

    public DbsField getPymnt_Cnr_Rdrw_Rqust_Tme() { return pymnt_Cnr_Rdrw_Rqust_Tme; }

    public DbsField getPymnt_Cnr_Rdrw_Rqust_User_Id() { return pymnt_Cnr_Rdrw_Rqust_User_Id; }

    public DbsField getPymnt_Cnr_Rdrw_Rqust_Term_Id() { return pymnt_Cnr_Rdrw_Rqust_Term_Id; }

    public DbsField getPymnt_Cnr_Orgnl_Invrse_Dte() { return pymnt_Cnr_Orgnl_Invrse_Dte; }

    public DbsField getPymnt_Cnr_Orgnl_Prcss_Seq_Nbr() { return pymnt_Cnr_Orgnl_Prcss_Seq_Nbr; }

    public DbsField getPymnt_Cnr_Rplcmnt_Invrse_Dte() { return pymnt_Cnr_Rplcmnt_Invrse_Dte; }

    public DbsField getPymnt_Cnr_Rplcmnt_Prcss_Seq_Nbr() { return pymnt_Cnr_Rplcmnt_Prcss_Seq_Nbr; }

    public DbsField getPymnt_Cnr_Rdrw_Rqust_Dte() { return pymnt_Cnr_Rdrw_Rqust_Dte; }

    public DbsField getPymnt_Cnr_Orgnl_Pymnt_Acctg_Dte() { return pymnt_Cnr_Orgnl_Pymnt_Acctg_Dte; }

    public DbsField getPymnt_Cnr_Orgnl_Pymnt_Intrfce_Dte() { return pymnt_Cnr_Orgnl_Pymnt_Intrfce_Dte; }

    public DbsField getPymnt_Cnr_Cs_Actvty_Cde() { return pymnt_Cnr_Cs_Actvty_Cde; }

    public DbsField getPymnt_Cnr_Cs_Pymnt_Intrfce_Dte() { return pymnt_Cnr_Cs_Pymnt_Intrfce_Dte; }

    public DbsField getPymnt_Cnr_Cs_Pymnt_Acctg_Dte() { return pymnt_Cnr_Cs_Pymnt_Acctg_Dte; }

    public DbsField getPymnt_Cnr_Cs_Pymnt_Check_Dte() { return pymnt_Cnr_Cs_Pymnt_Check_Dte; }

    public DbsField getPymnt_Cnr_Cs_New_Stop_Ind() { return pymnt_Cnr_Cs_New_Stop_Ind; }

    public DbsField getPymnt_Pymnt_Rprnt_Pndng_Ind() { return pymnt_Pymnt_Rprnt_Pndng_Ind; }

    public DbsField getPymnt_Pymnt_Rprnt_Rqust_Dte() { return pymnt_Pymnt_Rprnt_Rqust_Dte; }

    public DbsField getPymnt_Pymnt_Rprnt_Dte() { return pymnt_Pymnt_Rprnt_Dte; }

    public DbsField getPymnt_Cnr_Rdrw_Pymnt_Intrfce_Dte() { return pymnt_Cnr_Rdrw_Pymnt_Intrfce_Dte; }

    public DbsField getPymnt_Cnr_Rdrw_Pymnt_Acctg_Dte() { return pymnt_Cnr_Rdrw_Pymnt_Acctg_Dte; }

    public DbsField getPymnt_Cnr_Rdrw_Pymnt_Check_Dte() { return pymnt_Cnr_Rdrw_Pymnt_Check_Dte; }

    public DbsField getPymnt_Pymnt_Trigger_Cde() { return pymnt_Pymnt_Trigger_Cde; }

    public DbsField getPymnt_Pymnt_Spcl_Msg_Cde() { return pymnt_Pymnt_Spcl_Msg_Cde; }

    public DbsField getPymnt_Cntrct_Annty_Ins_Type() { return pymnt_Cntrct_Annty_Ins_Type; }

    public DbsField getPymnt_Cntrct_Annty_Type_Cde() { return pymnt_Cntrct_Annty_Type_Cde; }

    public DbsField getPymnt_Cntrct_Insurance_Option() { return pymnt_Cntrct_Insurance_Option; }

    public DbsField getPymnt_Cntrct_Life_Contingency() { return pymnt_Cntrct_Life_Contingency; }

    public DbsField getPymnt_Cntrct_Ac_Lt_10yrs() { return pymnt_Cntrct_Ac_Lt_10yrs; }

    public DbsField getPymnt_Tax_Fed_C_Resp_Cde() { return pymnt_Tax_Fed_C_Resp_Cde; }

    public DbsField getPymnt_Tax_Fed_C_Filing_Stat() { return pymnt_Tax_Fed_C_Filing_Stat; }

    public DbsField getPymnt_Tax_Fed_C_Allow_Cnt() { return pymnt_Tax_Fed_C_Allow_Cnt; }

    public DbsField getPymnt_Tax_Fed_C_Fixed_Amt() { return pymnt_Tax_Fed_C_Fixed_Amt; }

    public DbsField getPymnt_Tax_Fed_C_Fixed_Pct() { return pymnt_Tax_Fed_C_Fixed_Pct; }

    public DbsField getPymnt_Tax_Sta_C_Resp_Cde() { return pymnt_Tax_Sta_C_Resp_Cde; }

    public DbsField getPymnt_Tax_Sta_C_Filing_Stat() { return pymnt_Tax_Sta_C_Filing_Stat; }

    public DbsField getPymnt_Tax_Sta_C_Allow_Cnt() { return pymnt_Tax_Sta_C_Allow_Cnt; }

    public DbsField getPymnt_Tax_Sta_C_Fixed_Amt() { return pymnt_Tax_Sta_C_Fixed_Amt; }

    public DbsField getPymnt_Tax_Sta_C_Fixed_Pct() { return pymnt_Tax_Sta_C_Fixed_Pct; }

    public DbsField getPymnt_Tax_Sta_C_Spouse_Cnt() { return pymnt_Tax_Sta_C_Spouse_Cnt; }

    public DbsField getPymnt_Tax_Sta_C_Blind_Cnt() { return pymnt_Tax_Sta_C_Blind_Cnt; }

    public DbsField getPymnt_Tax_Loc_C_Resp_Cde() { return pymnt_Tax_Loc_C_Resp_Cde; }

    public DbsField getPymnt_Tax_Loc_C_Fixed_Amt() { return pymnt_Tax_Loc_C_Fixed_Amt; }

    public DbsField getPymnt_Tax_Loc_C_Fixed_Pct() { return pymnt_Tax_Loc_C_Fixed_Pct; }

    public DbsField getPymnt_Pymnt_Rtb_Dest_Typ() { return pymnt_Pymnt_Rtb_Dest_Typ; }

    public DbsField getPymnt_Egtrra_Eligibility_Ind() { return pymnt_Egtrra_Eligibility_Ind; }

    public DbsField getPymnt_Originating_Irc_Cde() { return pymnt_Originating_Irc_Cde; }

    public DbsGroup getPymnt_Originating_Irc_CdeRedef4() { return pymnt_Originating_Irc_CdeRedef4; }

    public DbsField getPymnt_Distributing_Irc_Cde() { return pymnt_Distributing_Irc_Cde; }

    public DbsField getPymnt_Receiving_Irc_Cde() { return pymnt_Receiving_Irc_Cde; }

    public DbsField getPymnt_Cntrct_Money_Source() { return pymnt_Cntrct_Money_Source; }

    public DbsField getPymnt_Roth_Dob() { return pymnt_Roth_Dob; }

    public DbsField getPymnt_Roth_First_Contrib_Dte() { return pymnt_Roth_First_Contrib_Dte; }

    public DbsField getPymnt_Roth_Death_Dte() { return pymnt_Roth_Death_Dte; }

    public DbsField getPymnt_Roth_Disability_Dte() { return pymnt_Roth_Disability_Dte; }

    public DbsField getPymnt_Roth_Money_Source() { return pymnt_Roth_Money_Source; }

    public DbsField getPymnt_Roth_Qual_Non_Qual_Distrib() { return pymnt_Roth_Qual_Non_Qual_Distrib; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pymnt_Work_Fields = dbsRecord.newGroupInRecord("pymnt_Work_Fields", "PYMNT-WORK-FIELDS");
        pymnt_Work_Fields.setParameterOption(ParameterOption.ByReference);
        pymnt_Work_Fields_Pymnt_Abend_Ind = pymnt_Work_Fields.newFieldInGroup("pymnt_Work_Fields_Pymnt_Abend_Ind", "PYMNT-ABEND-IND", FieldType.BOOLEAN);
        pymnt_Work_Fields_Pymnt_Return_Cde = pymnt_Work_Fields.newFieldInGroup("pymnt_Work_Fields_Pymnt_Return_Cde", "PYMNT-RETURN-CDE", FieldType.NUMERIC, 
            2);
        pymnt_Work_Fields_Pymnt_Isn = pymnt_Work_Fields.newFieldInGroup("pymnt_Work_Fields_Pymnt_Isn", "PYMNT-ISN", FieldType.PACKED_DECIMAL, 11);

        pymnt = dbsRecord.newGroupInRecord("pymnt", "PYMNT");
        pymnt.setParameterOption(ParameterOption.ByReference);
        pymnt_Cntrct_Ppcn_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pymnt_Cntrct_Payee_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pymnt_Cntrct_Invrse_Dte = pymnt.newFieldInGroup("pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pymnt_Cntrct_Check_Crrncy_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pymnt_Cntrct_Crrncy_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 1);
        pymnt_Cntrct_Orgn_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pymnt_Cntrct_Qlfied_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 1);
        pymnt_Cntrct_Pymnt_Type_Ind = pymnt.newFieldInGroup("pymnt_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pymnt_Cntrct_Sttlmnt_Type_Ind = pymnt.newFieldInGroup("pymnt_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pymnt_Cntrct_Sps_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", FieldType.STRING, 1);
        pymnt_Pymnt_Rqst_Rmndr_Pct = pymnt.newFieldInGroup("pymnt_Pymnt_Rqst_Rmndr_Pct", "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1);
        pymnt_Pymnt_Stats_Cde = pymnt.newFieldInGroup("pymnt_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1);
        pymnt_Pymnt_Annot_Ind = pymnt.newFieldInGroup("pymnt_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 1);
        pymnt_Pymnt_Cmbne_Ind = pymnt.newFieldInGroup("pymnt_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 1);
        pymnt_Pymnt_Ftre_Ind = pymnt.newFieldInGroup("pymnt_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", FieldType.STRING, 1);
        pymnt_Pymnt_Payee_Na_Addr_Trggr = pymnt.newFieldInGroup("pymnt_Pymnt_Payee_Na_Addr_Trggr", "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 1);
        pymnt_Pymnt_Payee_Tx_Elct_Trggr = pymnt.newFieldInGroup("pymnt_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        pymnt_Pymnt_Inst_Rep_Cde = pymnt.newFieldInGroup("pymnt_Pymnt_Inst_Rep_Cde", "PYMNT-INST-REP-CDE", FieldType.STRING, 1);
        pymnt_Cntrct_Dvdnd_Payee_Ind = pymnt.newFieldInGroup("pymnt_Cntrct_Dvdnd_Payee_Ind", "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 1);
        pymnt_Annt_Soc_Sec_Ind = pymnt.newFieldInGroup("pymnt_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1);
        pymnt_Pymnt_Pay_Type_Req_Ind = pymnt.newFieldInGroup("pymnt_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pymnt_Cntrct_Rqst_Settl_Dte = pymnt.newFieldInGroup("pymnt_Cntrct_Rqst_Settl_Dte", "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        pymnt_Cntrct_Rqst_Dte = pymnt.newFieldInGroup("pymnt_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", FieldType.DATE);
        pymnt_Cntrct_Rqst_Settl_Tme = pymnt.newFieldInGroup("pymnt_Cntrct_Rqst_Settl_Tme", "CNTRCT-RQST-SETTL-TME", FieldType.TIME);
        pymnt_Cntrct_Unq_Id_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        pymnt_Cntrct_Type_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        pymnt_Cntrct_Lob_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        pymnt_Cntrct_Sub_Lob_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        pymnt_Cntrct_Ia_Lob_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 2);
        pymnt_Cntrct_Cref_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10);
        pymnt_Cntrct_Option_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        pymnt_Cntrct_Mode_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        pymnt_Cntrct_Pymnt_Dest_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        pymnt_Cntrct_Roll_Dest_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        pymnt_Cntrct_Dvdnd_Payee_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        pymnt_Cntrct_Hold_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        pymnt_Cntrct_Hold_Grp = pymnt.newFieldInGroup("pymnt_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3);
        pymnt_Cntrct_Da_Tiaa_1_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Da_Tiaa_1_Nbr", "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        pymnt_Cntrct_Da_Tiaa_2_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Da_Tiaa_2_Nbr", "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        pymnt_Cntrct_Da_Tiaa_3_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Da_Tiaa_3_Nbr", "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        pymnt_Cntrct_Da_Tiaa_4_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Da_Tiaa_4_Nbr", "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        pymnt_Cntrct_Da_Tiaa_5_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Da_Tiaa_5_Nbr", "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        pymnt_Cntrct_Da_Cref_1_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        pymnt_Cntrct_Da_Cref_2_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8);
        pymnt_Cntrct_Da_Cref_3_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Da_Cref_3_Nbr", "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 8);
        pymnt_Cntrct_Da_Cref_4_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Da_Cref_4_Nbr", "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 8);
        pymnt_Cntrct_Da_Cref_5_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Da_Cref_5_Nbr", "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 8);
        pymnt_Annt_Soc_Sec_Nbr = pymnt.newFieldInGroup("pymnt_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        pymnt_Annt_Ctznshp_Cde = pymnt.newFieldInGroup("pymnt_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2);
        pymnt_Annt_Rsdncy_Cde = pymnt.newFieldInGroup("pymnt_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        pymnt_Pymnt_Split_Cde = pymnt.newFieldInGroup("pymnt_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", FieldType.STRING, 2);
        pymnt_Pymnt_Split_Reasn_Cde = pymnt.newFieldInGroup("pymnt_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);
        pymnt_Pymnt_Check_Dte = pymnt.newFieldInGroup("pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pymnt_Pymnt_Cycle_Dte = pymnt.newFieldInGroup("pymnt_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        pymnt_Pymnt_Eft_Dte = pymnt.newFieldInGroup("pymnt_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        pymnt_Pymnt_Rqst_Pct = pymnt.newFieldInGroup("pymnt_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", FieldType.PACKED_DECIMAL, 3);
        pymnt_Pymnt_Rqst_Amt = pymnt.newFieldInGroup("pymnt_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pymnt_Pymnt_Check_Nbr = pymnt.newFieldInGroup("pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pymnt_Pymnt_Prcss_Seq_Nbr = pymnt.newFieldInGroup("pymnt_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        pymnt_Pymnt_Prcss_Seq_NbrRedef1 = pymnt.newGroupInGroup("pymnt_Pymnt_Prcss_Seq_NbrRedef1", "Redefines", pymnt_Pymnt_Prcss_Seq_Nbr);
        pymnt_Pymnt_Prcss_Seq_Num = pymnt_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("pymnt_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pymnt_Pymnt_Instmt_Nbr = pymnt_Pymnt_Prcss_Seq_NbrRedef1.newFieldInGroup("pymnt_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pymnt_Pymnt_Check_Scrty_Nbr = pymnt.newFieldInGroup("pymnt_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        pymnt_Pymnt_Check_Amt = pymnt.newFieldInGroup("pymnt_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pymnt_Pymnt_Settlmnt_Dte = pymnt.newFieldInGroup("pymnt_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pymnt_Pymnt_Check_Seq_Nbr = pymnt.newFieldInGroup("pymnt_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pymnt_C_Inv_Acct = pymnt.newFieldInGroup("pymnt_C_Inv_Acct", "C-INV-ACCT", FieldType.PACKED_DECIMAL, 3);
        pymnt_Inv_Acct_Cde = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2, new DbsArrayController(1,40));
        pymnt_Inv_Acct_Unit_Qty = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 9,3, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_Unit_Value = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 9,4, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_Settl_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_Fed_Cde = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 1, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_State_Cde = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", FieldType.STRING, 1, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_Local_Cde = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", FieldType.STRING, 1, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_Ivc_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_Dci_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_Dpi_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_Start_Accum_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,40));
        pymnt_Inv_Acct_End_Accum_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", FieldType.PACKED_DECIMAL, 9,2, 
            new DbsArrayController(1,40));
        pymnt_Inv_Acct_Dvdnd_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_Net_Pymnt_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 9,2, 
            new DbsArrayController(1,40));
        pymnt_Inv_Acct_Ivc_Ind = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 1, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_Adj_Ivc_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2, new 
            DbsArrayController(1,40));
        pymnt_Inv_Acct_Valuat_Period = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 1, new DbsArrayController(1,
            40));
        pymnt_Inv_Acct_Fdrl_Tax_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2, 
            new DbsArrayController(1,40));
        pymnt_Inv_Acct_State_Tax_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2, 
            new DbsArrayController(1,40));
        pymnt_Inv_Acct_Local_Tax_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9,2, 
            new DbsArrayController(1,40));
        pymnt_Inv_Acct_Exp_Amt = pymnt.newFieldArrayInGroup("pymnt_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            40));
        pymnt_Cntrct_Ec_Oprtr_Id_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Ec_Oprtr_Id_Nbr", "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 7);
        pymnt_Cntrct_Cmbn_Nbr = pymnt.newFieldInGroup("pymnt_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pymnt_Cntrct_Hold_Tme = pymnt.newFieldInGroup("pymnt_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME);
        pymnt_C_Pymnt_Ded_Grp = pymnt.newFieldInGroup("pymnt_C_Pymnt_Ded_Grp", "C-PYMNT-DED-GRP", FieldType.PACKED_DECIMAL, 3);
        pymnt_Pymnt_Ded_Cde = pymnt.newFieldArrayInGroup("pymnt_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3, new DbsArrayController(1,10));
        pymnt_Pymnt_Ded_Payee_Cde = pymnt.newFieldArrayInGroup("pymnt_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 8, new DbsArrayController(1,
            10));
        pymnt_Pymnt_Ded_Amt = pymnt.newFieldArrayInGroup("pymnt_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 9,2, new DbsArrayController(1,
            10));
        pymnt_Pymnt_Corp_Wpid = pymnt.newFieldInGroup("pymnt_Pymnt_Corp_Wpid", "PYMNT-CORP-WPID", FieldType.STRING, 6);
        pymnt_Pymnt_Reqst_Log_Dte_Time = pymnt.newFieldInGroup("pymnt_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        pymnt_Annt_Locality_Cde = pymnt.newFieldInGroup("pymnt_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", FieldType.STRING, 2);
        pymnt_C_Inv_Rtb_Grp = pymnt.newFieldInGroup("pymnt_C_Inv_Rtb_Grp", "C-INV-RTB-GRP", FieldType.PACKED_DECIMAL, 3);
        pymnt_Inv_Rtb_Grp = pymnt.newGroupInGroup("pymnt_Inv_Rtb_Grp", "INV-RTB-GRP");
        pymnt_Inv_Rtb_Cde = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Rtb_Cde", "INV-RTB-CDE", FieldType.STRING, 1, new DbsArrayController(1,20));
        pymnt_Inv_Acct_Fed_Dci_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Fed_Dci_Tax_Amt", "INV-ACCT-FED-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,20));
        pymnt_Inv_Acct_Sta_Dci_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Sta_Dci_Tax_Amt", "INV-ACCT-STA-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,20));
        pymnt_Inv_Acct_Loc_Dci_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Loc_Dci_Tax_Amt", "INV-ACCT-LOC-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,20));
        pymnt_Inv_Acct_Fed_Dpi_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Fed_Dpi_Tax_Amt", "INV-ACCT-FED-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,20));
        pymnt_Inv_Acct_Sta_Dpi_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Sta_Dpi_Tax_Amt", "INV-ACCT-STA-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,20));
        pymnt_Inv_Acct_Loc_Dpi_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Loc_Dpi_Tax_Amt", "INV-ACCT-LOC-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9,2, new DbsArrayController(1,20));
        pymnt_Pymnt_Acctg_Dte = pymnt.newFieldInGroup("pymnt_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        pymnt_Pymnt_Intrfce_Dte = pymnt.newFieldInGroup("pymnt_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        pymnt_Pymnt_Tiaa_Md_Amt = pymnt.newFieldInGroup("pymnt_Pymnt_Tiaa_Md_Amt", "PYMNT-TIAA-MD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pymnt_Pymnt_Cref_Md_Amt = pymnt.newFieldInGroup("pymnt_Pymnt_Cref_Md_Amt", "PYMNT-CREF-MD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pymnt_Pymnt_Tax_Form = pymnt.newFieldInGroup("pymnt_Pymnt_Tax_Form", "PYMNT-TAX-FORM", FieldType.STRING, 4);
        pymnt_Pymnt_Tax_Exempt_Ind = pymnt.newFieldInGroup("pymnt_Pymnt_Tax_Exempt_Ind", "PYMNT-TAX-EXEMPT-IND", FieldType.STRING, 1);
        pymnt_Pymnt_Tax_Calc_Cde = pymnt.newFieldInGroup("pymnt_Pymnt_Tax_Calc_Cde", "PYMNT-TAX-CALC-CDE", FieldType.STRING, 2);
        pymnt_Pymnt_Method_Cde = pymnt.newFieldInGroup("pymnt_Pymnt_Method_Cde", "PYMNT-METHOD-CDE", FieldType.STRING, 1);
        pymnt_Pymnt_Settl_Ivc_Ind = pymnt.newFieldInGroup("pymnt_Pymnt_Settl_Ivc_Ind", "PYMNT-SETTL-IVC-IND", FieldType.STRING, 1);
        pymnt_Pymnt_Ia_Issue_Dte = pymnt.newFieldInGroup("pymnt_Pymnt_Ia_Issue_Dte", "PYMNT-IA-ISSUE-DTE", FieldType.DATE);
        pymnt_Pymnt_Spouse_Pay_Stats = pymnt.newFieldInGroup("pymnt_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", FieldType.STRING, 1);
        pymnt_Cntrct_Cancel_Rdrw_Ind = pymnt.newFieldInGroup("pymnt_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2);
        pymnt_Cntrct_Cancel_Rdrw_Amt = pymnt.newFieldInGroup("pymnt_Cntrct_Cancel_Rdrw_Amt", "CNTRCT-CANCEL-RDRW-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pymnt_Cntrct_Hold_Ind = pymnt.newFieldInGroup("pymnt_Cntrct_Hold_Ind", "CNTRCT-HOLD-IND", FieldType.STRING, 1);
        pymnt_Notused2 = pymnt.newFieldInGroup("pymnt_Notused2", "NOTUSED2", FieldType.STRING, 8);
        pymnt_Notused2Redef2 = pymnt.newGroupInGroup("pymnt_Notused2Redef2", "Redefines", pymnt_Notused2);
        pymnt_Pymnt_Commuted_Value = pymnt_Notused2Redef2.newFieldInGroup("pymnt_Pymnt_Commuted_Value", "PYMNT-COMMUTED-VALUE", FieldType.STRING, 2);
        pymnt_Notused2Redef3 = pymnt.newGroupInGroup("pymnt_Notused2Redef3", "Redefines", pymnt_Notused2);
        pymnt_Pymnt_Wrrnt_Trans_Type = pymnt_Notused2Redef3.newFieldInGroup("pymnt_Pymnt_Wrrnt_Trans_Type", "PYMNT-WRRNT-TRANS-TYPE", FieldType.STRING, 
            2);
        pymnt_Cnr_Cs_Rqust_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Cs_Rqust_Dte", "CNR-CS-RQUST-DTE", FieldType.DATE);
        pymnt_Cnr_Cs_Rqust_Tme = pymnt.newFieldInGroup("pymnt_Cnr_Cs_Rqust_Tme", "CNR-CS-RQUST-TME", FieldType.NUMERIC, 7);
        pymnt_Cnr_Cs_Rqust_User_Id = pymnt.newFieldInGroup("pymnt_Cnr_Cs_Rqust_User_Id", "CNR-CS-RQUST-USER-ID", FieldType.STRING, 8);
        pymnt_Cnr_Cs_Rqust_Term_Id = pymnt.newFieldInGroup("pymnt_Cnr_Cs_Rqust_Term_Id", "CNR-CS-RQUST-TERM-ID", FieldType.STRING, 8);
        pymnt_Notused1 = pymnt.newFieldInGroup("pymnt_Notused1", "NOTUSED1", FieldType.STRING, 8);
        pymnt_Cnr_Rdrw_Rqust_Tme = pymnt.newFieldInGroup("pymnt_Cnr_Rdrw_Rqust_Tme", "CNR-RDRW-RQUST-TME", FieldType.NUMERIC, 7);
        pymnt_Cnr_Rdrw_Rqust_User_Id = pymnt.newFieldInGroup("pymnt_Cnr_Rdrw_Rqust_User_Id", "CNR-RDRW-RQUST-USER-ID", FieldType.STRING, 8);
        pymnt_Cnr_Rdrw_Rqust_Term_Id = pymnt.newFieldInGroup("pymnt_Cnr_Rdrw_Rqust_Term_Id", "CNR-RDRW-RQUST-TERM-ID", FieldType.STRING, 8);
        pymnt_Cnr_Orgnl_Invrse_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        pymnt_Cnr_Orgnl_Prcss_Seq_Nbr = pymnt.newFieldInGroup("pymnt_Cnr_Orgnl_Prcss_Seq_Nbr", "CNR-ORGNL-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        pymnt_Cnr_Rplcmnt_Invrse_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Rplcmnt_Invrse_Dte", "CNR-RPLCMNT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pymnt_Cnr_Rplcmnt_Prcss_Seq_Nbr = pymnt.newFieldInGroup("pymnt_Cnr_Rplcmnt_Prcss_Seq_Nbr", "CNR-RPLCMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        pymnt_Cnr_Rdrw_Rqust_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Rdrw_Rqust_Dte", "CNR-RDRW-RQUST-DTE", FieldType.DATE);
        pymnt_Cnr_Orgnl_Pymnt_Acctg_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Orgnl_Pymnt_Acctg_Dte", "CNR-ORGNL-PYMNT-ACCTG-DTE", FieldType.DATE);
        pymnt_Cnr_Orgnl_Pymnt_Intrfce_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Orgnl_Pymnt_Intrfce_Dte", "CNR-ORGNL-PYMNT-INTRFCE-DTE", FieldType.DATE);
        pymnt_Cnr_Cs_Actvty_Cde = pymnt.newFieldInGroup("pymnt_Cnr_Cs_Actvty_Cde", "CNR-CS-ACTVTY-CDE", FieldType.STRING, 1);
        pymnt_Cnr_Cs_Pymnt_Intrfce_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Cs_Pymnt_Intrfce_Dte", "CNR-CS-PYMNT-INTRFCE-DTE", FieldType.DATE);
        pymnt_Cnr_Cs_Pymnt_Acctg_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", FieldType.DATE);
        pymnt_Cnr_Cs_Pymnt_Check_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Cs_Pymnt_Check_Dte", "CNR-CS-PYMNT-CHECK-DTE", FieldType.DATE);
        pymnt_Cnr_Cs_New_Stop_Ind = pymnt.newFieldInGroup("pymnt_Cnr_Cs_New_Stop_Ind", "CNR-CS-NEW-STOP-IND", FieldType.STRING, 1);
        pymnt_Pymnt_Rprnt_Pndng_Ind = pymnt.newFieldInGroup("pymnt_Pymnt_Rprnt_Pndng_Ind", "PYMNT-RPRNT-PNDNG-IND", FieldType.STRING, 1);
        pymnt_Pymnt_Rprnt_Rqust_Dte = pymnt.newFieldInGroup("pymnt_Pymnt_Rprnt_Rqust_Dte", "PYMNT-RPRNT-RQUST-DTE", FieldType.DATE);
        pymnt_Pymnt_Rprnt_Dte = pymnt.newFieldInGroup("pymnt_Pymnt_Rprnt_Dte", "PYMNT-RPRNT-DTE", FieldType.DATE);
        pymnt_Cnr_Rdrw_Pymnt_Intrfce_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Rdrw_Pymnt_Intrfce_Dte", "CNR-RDRW-PYMNT-INTRFCE-DTE", FieldType.DATE);
        pymnt_Cnr_Rdrw_Pymnt_Acctg_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Rdrw_Pymnt_Acctg_Dte", "CNR-RDRW-PYMNT-ACCTG-DTE", FieldType.DATE);
        pymnt_Cnr_Rdrw_Pymnt_Check_Dte = pymnt.newFieldInGroup("pymnt_Cnr_Rdrw_Pymnt_Check_Dte", "CNR-RDRW-PYMNT-CHECK-DTE", FieldType.DATE);
        pymnt_Pymnt_Trigger_Cde = pymnt.newFieldInGroup("pymnt_Pymnt_Trigger_Cde", "PYMNT-TRIGGER-CDE", FieldType.STRING, 1);
        pymnt_Pymnt_Spcl_Msg_Cde = pymnt.newFieldInGroup("pymnt_Pymnt_Spcl_Msg_Cde", "PYMNT-SPCL-MSG-CDE", FieldType.STRING, 3);
        pymnt_Cntrct_Annty_Ins_Type = pymnt.newFieldInGroup("pymnt_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        pymnt_Cntrct_Annty_Type_Cde = pymnt.newFieldInGroup("pymnt_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        pymnt_Cntrct_Insurance_Option = pymnt.newFieldInGroup("pymnt_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 1);
        pymnt_Cntrct_Life_Contingency = pymnt.newFieldInGroup("pymnt_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 1);
        pymnt_Cntrct_Ac_Lt_10yrs = pymnt.newFieldInGroup("pymnt_Cntrct_Ac_Lt_10yrs", "CNTRCT-AC-LT-10YRS", FieldType.BOOLEAN);
        pymnt_Tax_Fed_C_Resp_Cde = pymnt.newFieldInGroup("pymnt_Tax_Fed_C_Resp_Cde", "TAX-FED-C-RESP-CDE", FieldType.STRING, 1);
        pymnt_Tax_Fed_C_Filing_Stat = pymnt.newFieldInGroup("pymnt_Tax_Fed_C_Filing_Stat", "TAX-FED-C-FILING-STAT", FieldType.STRING, 1);
        pymnt_Tax_Fed_C_Allow_Cnt = pymnt.newFieldInGroup("pymnt_Tax_Fed_C_Allow_Cnt", "TAX-FED-C-ALLOW-CNT", FieldType.PACKED_DECIMAL, 2);
        pymnt_Tax_Fed_C_Fixed_Amt = pymnt.newFieldInGroup("pymnt_Tax_Fed_C_Fixed_Amt", "TAX-FED-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pymnt_Tax_Fed_C_Fixed_Pct = pymnt.newFieldInGroup("pymnt_Tax_Fed_C_Fixed_Pct", "TAX-FED-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 7,4);
        pymnt_Tax_Sta_C_Resp_Cde = pymnt.newFieldInGroup("pymnt_Tax_Sta_C_Resp_Cde", "TAX-STA-C-RESP-CDE", FieldType.STRING, 1);
        pymnt_Tax_Sta_C_Filing_Stat = pymnt.newFieldInGroup("pymnt_Tax_Sta_C_Filing_Stat", "TAX-STA-C-FILING-STAT", FieldType.STRING, 1);
        pymnt_Tax_Sta_C_Allow_Cnt = pymnt.newFieldInGroup("pymnt_Tax_Sta_C_Allow_Cnt", "TAX-STA-C-ALLOW-CNT", FieldType.PACKED_DECIMAL, 2);
        pymnt_Tax_Sta_C_Fixed_Amt = pymnt.newFieldInGroup("pymnt_Tax_Sta_C_Fixed_Amt", "TAX-STA-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pymnt_Tax_Sta_C_Fixed_Pct = pymnt.newFieldInGroup("pymnt_Tax_Sta_C_Fixed_Pct", "TAX-STA-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 7,4);
        pymnt_Tax_Sta_C_Spouse_Cnt = pymnt.newFieldInGroup("pymnt_Tax_Sta_C_Spouse_Cnt", "TAX-STA-C-SPOUSE-CNT", FieldType.NUMERIC, 1);
        pymnt_Tax_Sta_C_Blind_Cnt = pymnt.newFieldInGroup("pymnt_Tax_Sta_C_Blind_Cnt", "TAX-STA-C-BLIND-CNT", FieldType.NUMERIC, 1);
        pymnt_Tax_Loc_C_Resp_Cde = pymnt.newFieldInGroup("pymnt_Tax_Loc_C_Resp_Cde", "TAX-LOC-C-RESP-CDE", FieldType.STRING, 1);
        pymnt_Tax_Loc_C_Fixed_Amt = pymnt.newFieldInGroup("pymnt_Tax_Loc_C_Fixed_Amt", "TAX-LOC-C-FIXED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pymnt_Tax_Loc_C_Fixed_Pct = pymnt.newFieldInGroup("pymnt_Tax_Loc_C_Fixed_Pct", "TAX-LOC-C-FIXED-PCT", FieldType.PACKED_DECIMAL, 7,4);
        pymnt_Pymnt_Rtb_Dest_Typ = pymnt.newFieldInGroup("pymnt_Pymnt_Rtb_Dest_Typ", "PYMNT-RTB-DEST-TYP", FieldType.STRING, 4);
        pymnt_Egtrra_Eligibility_Ind = pymnt.newFieldInGroup("pymnt_Egtrra_Eligibility_Ind", "EGTRRA-ELIGIBILITY-IND", FieldType.STRING, 1);
        pymnt_Originating_Irc_Cde = pymnt.newFieldArrayInGroup("pymnt_Originating_Irc_Cde", "ORIGINATING-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1,
            8));
        pymnt_Originating_Irc_CdeRedef4 = pymnt.newGroupInGroup("pymnt_Originating_Irc_CdeRedef4", "Redefines", pymnt_Originating_Irc_Cde);
        pymnt_Distributing_Irc_Cde = pymnt_Originating_Irc_CdeRedef4.newFieldInGroup("pymnt_Distributing_Irc_Cde", "DISTRIBUTING-IRC-CDE", FieldType.STRING, 
            16);
        pymnt_Receiving_Irc_Cde = pymnt.newFieldInGroup("pymnt_Receiving_Irc_Cde", "RECEIVING-IRC-CDE", FieldType.STRING, 2);
        pymnt_Cntrct_Money_Source = pymnt.newFieldInGroup("pymnt_Cntrct_Money_Source", "CNTRCT-MONEY-SOURCE", FieldType.STRING, 5);
        pymnt_Roth_Dob = pymnt.newFieldInGroup("pymnt_Roth_Dob", "ROTH-DOB", FieldType.NUMERIC, 8);
        pymnt_Roth_First_Contrib_Dte = pymnt.newFieldInGroup("pymnt_Roth_First_Contrib_Dte", "ROTH-FIRST-CONTRIB-DTE", FieldType.NUMERIC, 8);
        pymnt_Roth_Death_Dte = pymnt.newFieldInGroup("pymnt_Roth_Death_Dte", "ROTH-DEATH-DTE", FieldType.NUMERIC, 8);
        pymnt_Roth_Disability_Dte = pymnt.newFieldInGroup("pymnt_Roth_Disability_Dte", "ROTH-DISABILITY-DTE", FieldType.NUMERIC, 8);
        pymnt_Roth_Money_Source = pymnt.newFieldInGroup("pymnt_Roth_Money_Source", "ROTH-MONEY-SOURCE", FieldType.STRING, 5);
        pymnt_Roth_Qual_Non_Qual_Distrib = pymnt.newFieldInGroup("pymnt_Roth_Qual_Non_Qual_Distrib", "ROTH-QUAL-NON-QUAL-DISTRIB", FieldType.STRING, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpapmnr(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

