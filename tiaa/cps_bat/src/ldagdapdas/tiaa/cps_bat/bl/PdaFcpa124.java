/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:13 PM
**        * FROM NATURAL PDA     : FCPA124
************************************************************
**        * FILE NAME            : PdaFcpa124.java
**        * CLASS NAME           : PdaFcpa124
**        * INSTANCE NAME        : PdaFcpa124
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa124 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Mit_Read_Pda;
    private DbsField pnd_Mit_Read_Pda_Pin_Nbr;
    private DbsField pnd_Mit_Read_Pda_Rqst_Log_Dte_Tme;
    private DbsField pnd_Mit_Read_Pda_Admin_Unit_Cde;
    private DbsField pnd_Mit_Read_Pda_Rqst_Orgn_Cde;
    private DbsField pnd_Mit_Read_Pda_Orgnl_Unit_Cde;

    public DbsGroup getPnd_Mit_Read_Pda() { return pnd_Mit_Read_Pda; }

    public DbsField getPnd_Mit_Read_Pda_Pin_Nbr() { return pnd_Mit_Read_Pda_Pin_Nbr; }

    public DbsField getPnd_Mit_Read_Pda_Rqst_Log_Dte_Tme() { return pnd_Mit_Read_Pda_Rqst_Log_Dte_Tme; }

    public DbsField getPnd_Mit_Read_Pda_Admin_Unit_Cde() { return pnd_Mit_Read_Pda_Admin_Unit_Cde; }

    public DbsField getPnd_Mit_Read_Pda_Rqst_Orgn_Cde() { return pnd_Mit_Read_Pda_Rqst_Orgn_Cde; }

    public DbsField getPnd_Mit_Read_Pda_Orgnl_Unit_Cde() { return pnd_Mit_Read_Pda_Orgnl_Unit_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Mit_Read_Pda = dbsRecord.newGroupInRecord("pnd_Mit_Read_Pda", "#MIT-READ-PDA");
        pnd_Mit_Read_Pda.setParameterOption(ParameterOption.ByReference);
        pnd_Mit_Read_Pda_Pin_Nbr = pnd_Mit_Read_Pda.newFieldInGroup("pnd_Mit_Read_Pda_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Mit_Read_Pda_Rqst_Log_Dte_Tme = pnd_Mit_Read_Pda.newFieldInGroup("pnd_Mit_Read_Pda_Rqst_Log_Dte_Tme", "RQST-LOG-DTE-TME", FieldType.STRING, 
            15);
        pnd_Mit_Read_Pda_Admin_Unit_Cde = pnd_Mit_Read_Pda.newFieldInGroup("pnd_Mit_Read_Pda_Admin_Unit_Cde", "ADMIN-UNIT-CDE", FieldType.STRING, 8);
        pnd_Mit_Read_Pda_Rqst_Orgn_Cde = pnd_Mit_Read_Pda.newFieldInGroup("pnd_Mit_Read_Pda_Rqst_Orgn_Cde", "RQST-ORGN-CDE", FieldType.STRING, 1);
        pnd_Mit_Read_Pda_Orgnl_Unit_Cde = pnd_Mit_Read_Pda.newFieldInGroup("pnd_Mit_Read_Pda_Orgnl_Unit_Cde", "ORGNL-UNIT-CDE", FieldType.STRING, 8);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa124(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

