/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:29 PM
**        * FROM NATURAL PDA     : FCPACNTR
************************************************************
**        * FILE NAME            : PdaFcpacntr.java
**        * CLASS NAME           : PdaFcpacntr
**        * INSTANCE NAME        : PdaFcpacntr
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpacntr extends PdaBase
{
    // Properties
    private DbsGroup cntrl_Work_Fields;
    private DbsField cntrl_Work_Fields_Pnd_Cntrl_Isn;
    private DbsField cntrl_Work_Fields_Cntrl_Abend_Ind;
    private DbsField cntrl_Work_Fields_Cntrl_Return_Cde;
    private DbsGroup cntrl;
    private DbsField cntrl_Cntrct_Rcrd_Typ;
    private DbsField cntrl_Cntrct_Act_Ind;
    private DbsGroup cntrl_Cntrl_Data_Grp;
    private DbsField cntrl_Cntl_Orgn_Cde;
    private DbsField cntrl_Cntl_Cycle_Dte;
    private DbsField cntrl_Cntl_Check_Dte;
    private DbsField cntrl_Cntl_Invrse_Dte;
    private DbsField cntrl_Cntl_Seq_Start_Cnt;
    private DbsField cntrl_Cntl_Seq_End_Cnt;
    private DbsField cntrl_Cntl_Month_End_Ind;
    private DbsField cntrl_Cntl_Month_End_Dte;
    private DbsField cntrl_Cntl_Bank_Acct_Nbr;
    private DbsField cntrl_Cntl_Rmrk_Line1_Txt;
    private DbsField cntrl_Cntl_Rmrk_Line2_Txt;
    private DbsField cntrl_C_Cntl_Amt_Pe_Grp;
    private DbsGroup cntrl_Cntl_Amt_Pe_Grp;
    private DbsField cntrl_Cntl_Type_Cde;
    private DbsField cntrl_Cntl_Cnt;
    private DbsField cntrl_Cntl_Gross_Amt;
    private DbsField cntrl_Cntl_Net_Amt;
    private DbsField cntrl_Cntrct_Hold_Tme;

    public DbsGroup getCntrl_Work_Fields() { return cntrl_Work_Fields; }

    public DbsField getCntrl_Work_Fields_Pnd_Cntrl_Isn() { return cntrl_Work_Fields_Pnd_Cntrl_Isn; }

    public DbsField getCntrl_Work_Fields_Cntrl_Abend_Ind() { return cntrl_Work_Fields_Cntrl_Abend_Ind; }

    public DbsField getCntrl_Work_Fields_Cntrl_Return_Cde() { return cntrl_Work_Fields_Cntrl_Return_Cde; }

    public DbsGroup getCntrl() { return cntrl; }

    public DbsField getCntrl_Cntrct_Rcrd_Typ() { return cntrl_Cntrct_Rcrd_Typ; }

    public DbsField getCntrl_Cntrct_Act_Ind() { return cntrl_Cntrct_Act_Ind; }

    public DbsGroup getCntrl_Cntrl_Data_Grp() { return cntrl_Cntrl_Data_Grp; }

    public DbsField getCntrl_Cntl_Orgn_Cde() { return cntrl_Cntl_Orgn_Cde; }

    public DbsField getCntrl_Cntl_Cycle_Dte() { return cntrl_Cntl_Cycle_Dte; }

    public DbsField getCntrl_Cntl_Check_Dte() { return cntrl_Cntl_Check_Dte; }

    public DbsField getCntrl_Cntl_Invrse_Dte() { return cntrl_Cntl_Invrse_Dte; }

    public DbsField getCntrl_Cntl_Seq_Start_Cnt() { return cntrl_Cntl_Seq_Start_Cnt; }

    public DbsField getCntrl_Cntl_Seq_End_Cnt() { return cntrl_Cntl_Seq_End_Cnt; }

    public DbsField getCntrl_Cntl_Month_End_Ind() { return cntrl_Cntl_Month_End_Ind; }

    public DbsField getCntrl_Cntl_Month_End_Dte() { return cntrl_Cntl_Month_End_Dte; }

    public DbsField getCntrl_Cntl_Bank_Acct_Nbr() { return cntrl_Cntl_Bank_Acct_Nbr; }

    public DbsField getCntrl_Cntl_Rmrk_Line1_Txt() { return cntrl_Cntl_Rmrk_Line1_Txt; }

    public DbsField getCntrl_Cntl_Rmrk_Line2_Txt() { return cntrl_Cntl_Rmrk_Line2_Txt; }

    public DbsField getCntrl_C_Cntl_Amt_Pe_Grp() { return cntrl_C_Cntl_Amt_Pe_Grp; }

    public DbsGroup getCntrl_Cntl_Amt_Pe_Grp() { return cntrl_Cntl_Amt_Pe_Grp; }

    public DbsField getCntrl_Cntl_Type_Cde() { return cntrl_Cntl_Type_Cde; }

    public DbsField getCntrl_Cntl_Cnt() { return cntrl_Cntl_Cnt; }

    public DbsField getCntrl_Cntl_Gross_Amt() { return cntrl_Cntl_Gross_Amt; }

    public DbsField getCntrl_Cntl_Net_Amt() { return cntrl_Cntl_Net_Amt; }

    public DbsField getCntrl_Cntrct_Hold_Tme() { return cntrl_Cntrct_Hold_Tme; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        cntrl_Work_Fields = dbsRecord.newGroupInRecord("cntrl_Work_Fields", "CNTRL-WORK-FIELDS");
        cntrl_Work_Fields.setParameterOption(ParameterOption.ByReference);
        cntrl_Work_Fields_Pnd_Cntrl_Isn = cntrl_Work_Fields.newFieldInGroup("cntrl_Work_Fields_Pnd_Cntrl_Isn", "#CNTRL-ISN", FieldType.PACKED_DECIMAL, 
            11);
        cntrl_Work_Fields_Cntrl_Abend_Ind = cntrl_Work_Fields.newFieldInGroup("cntrl_Work_Fields_Cntrl_Abend_Ind", "CNTRL-ABEND-IND", FieldType.BOOLEAN);
        cntrl_Work_Fields_Cntrl_Return_Cde = cntrl_Work_Fields.newFieldInGroup("cntrl_Work_Fields_Cntrl_Return_Cde", "CNTRL-RETURN-CDE", FieldType.NUMERIC, 
            2);

        cntrl = dbsRecord.newGroupInRecord("cntrl", "CNTRL");
        cntrl.setParameterOption(ParameterOption.ByReference);
        cntrl_Cntrct_Rcrd_Typ = cntrl.newFieldInGroup("cntrl_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1);
        cntrl_Cntrct_Act_Ind = cntrl.newFieldInGroup("cntrl_Cntrct_Act_Ind", "CNTRCT-ACT-IND", FieldType.STRING, 1);
        cntrl_Cntrl_Data_Grp = cntrl.newGroupInGroup("cntrl_Cntrl_Data_Grp", "CNTRL-DATA-GRP");
        cntrl_Cntl_Orgn_Cde = cntrl_Cntrl_Data_Grp.newFieldInGroup("cntrl_Cntl_Orgn_Cde", "CNTL-ORGN-CDE", FieldType.STRING, 2);
        cntrl_Cntl_Cycle_Dte = cntrl_Cntrl_Data_Grp.newFieldInGroup("cntrl_Cntl_Cycle_Dte", "CNTL-CYCLE-DTE", FieldType.DATE);
        cntrl_Cntl_Check_Dte = cntrl_Cntrl_Data_Grp.newFieldInGroup("cntrl_Cntl_Check_Dte", "CNTL-CHECK-DTE", FieldType.DATE);
        cntrl_Cntl_Invrse_Dte = cntrl_Cntrl_Data_Grp.newFieldInGroup("cntrl_Cntl_Invrse_Dte", "CNTL-INVRSE-DTE", FieldType.NUMERIC, 8);
        cntrl_Cntl_Seq_Start_Cnt = cntrl_Cntrl_Data_Grp.newFieldInGroup("cntrl_Cntl_Seq_Start_Cnt", "CNTL-SEQ-START-CNT", FieldType.PACKED_DECIMAL, 7);
        cntrl_Cntl_Seq_End_Cnt = cntrl_Cntrl_Data_Grp.newFieldInGroup("cntrl_Cntl_Seq_End_Cnt", "CNTL-SEQ-END-CNT", FieldType.PACKED_DECIMAL, 7);
        cntrl_Cntl_Month_End_Ind = cntrl_Cntrl_Data_Grp.newFieldInGroup("cntrl_Cntl_Month_End_Ind", "CNTL-MONTH-END-IND", FieldType.STRING, 1);
        cntrl_Cntl_Month_End_Dte = cntrl_Cntrl_Data_Grp.newFieldInGroup("cntrl_Cntl_Month_End_Dte", "CNTL-MONTH-END-DTE", FieldType.DATE);
        cntrl_Cntl_Bank_Acct_Nbr = cntrl_Cntrl_Data_Grp.newFieldInGroup("cntrl_Cntl_Bank_Acct_Nbr", "CNTL-BANK-ACCT-NBR", FieldType.STRING, 20);
        cntrl_Cntl_Rmrk_Line1_Txt = cntrl_Cntrl_Data_Grp.newFieldInGroup("cntrl_Cntl_Rmrk_Line1_Txt", "CNTL-RMRK-LINE1-TXT", FieldType.STRING, 60);
        cntrl_Cntl_Rmrk_Line2_Txt = cntrl_Cntrl_Data_Grp.newFieldInGroup("cntrl_Cntl_Rmrk_Line2_Txt", "CNTL-RMRK-LINE2-TXT", FieldType.STRING, 60);
        cntrl_C_Cntl_Amt_Pe_Grp = cntrl.newFieldInGroup("cntrl_C_Cntl_Amt_Pe_Grp", "C-CNTL-AMT-PE-GRP", FieldType.BINARY, 1);
        cntrl_Cntl_Amt_Pe_Grp = cntrl.newGroupInGroup("cntrl_Cntl_Amt_Pe_Grp", "CNTL-AMT-PE-GRP");
        cntrl_Cntl_Type_Cde = cntrl_Cntl_Amt_Pe_Grp.newFieldArrayInGroup("cntrl_Cntl_Type_Cde", "CNTL-TYPE-CDE", FieldType.STRING, 2, new DbsArrayController(1,
            50));
        cntrl_Cntl_Cnt = cntrl_Cntl_Amt_Pe_Grp.newFieldArrayInGroup("cntrl_Cntl_Cnt", "CNTL-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1,
            50));
        cntrl_Cntl_Gross_Amt = cntrl_Cntl_Amt_Pe_Grp.newFieldArrayInGroup("cntrl_Cntl_Gross_Amt", "CNTL-GROSS-AMT", FieldType.PACKED_DECIMAL, 11,2, new 
            DbsArrayController(1,50));
        cntrl_Cntl_Net_Amt = cntrl_Cntl_Amt_Pe_Grp.newFieldArrayInGroup("cntrl_Cntl_Net_Amt", "CNTL-NET-AMT", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,
            50));
        cntrl_Cntrct_Hold_Tme = cntrl.newFieldInGroup("cntrl_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpacntr(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

