/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:34 PM
**        * FROM NATURAL LDA     : CPSL110
************************************************************
**        * FILE NAME            : LdaCpsl110.java
**        * CLASS NAME           : LdaCpsl110
**        * INSTANCE NAME        : LdaCpsl110
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpsl110 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_ra;
    private DbsGroup ra_Rt_Record;
    private DbsField ra_Rt_A_I_Ind;
    private DbsField ra_Rt_Table_Id;
    private DbsField ra_Rt_Short_Key;
    private DbsGroup ra_Rt_Short_KeyRedef1;
    private DbsField ra_Bank_Source_Code;
    private DbsField filler01;
    private DbsField ra_Rt_Long_Key;
    private DbsGroup ra_Rt_Long_KeyRedef2;
    private DbsField ra_Bank_Routing;
    private DbsField ra_Filler1;
    private DbsField ra_Bank_Account;
    private DbsField ra_Filler2;
    private DbsField ra_Rt_Desc1;
    private DbsGroup ra_Rt_Desc1Redef3;
    private DbsField ra_Bank_Account_Name;
    private DbsField ra_Bank_Account_Cde;
    private DbsField ra_Bank_Account_Desc;
    private DbsField ra_Bank_Business_Unit;
    private DbsField ra_Bank_Format;
    private DbsField ra_Bank_Transmit_Ind;
    private DbsField ra_Check_Acct_Ind;
    private DbsField ra_Eft_Acct_Ind;
    private DbsField ra_Start_Seq;
    private DbsField ra_End_Seq;
    private DbsField ra_Seq_Maint_Ind;
    private DbsField ra_Hops_Acct_Ind;
    private DbsField ra_Bank_Ledger_Nbr;
    private DbsField ra_Bank_Signature_Cde;
    private DbsField ra_Bank_Void_Msg;
    private DbsField ra_Bank_Orgn_Cde;
    private DbsField ra_Bank_Internal_Pospay_Ind;
    private DbsField ra_Rt_Desc2;
    private DbsGroup ra_Rt_Desc2Redef4;
    private DbsField ra_Bank_Logo_Key;
    private DbsField ra_Over_Logo_Name1;
    private DbsField ra_Over_Logo_Name2;
    private DbsField ra_Over_Logo_Addr1;
    private DbsField ra_Over_Logo_Addr2;
    private DbsField ra_Rt_Desc3;
    private DbsField ra_Rt_Desc4;
    private DbsField ra_Rt_Desc5;
    private DbsField ra_Rt_Eff_From_Ccyymmdd;
    private DbsField ra_Rt_Eff_To_Ccyymmdd;
    private DbsField ra_Rt_Upd_Source;
    private DbsField ra_Rt_Upd_User;
    private DbsField ra_Rt_Upd_Ccyymmdd;
    private DbsField ra_Rt_Upd_Timn;
    private DbsField ra_Rt_Super1;
    private DataAccessProgramView vw_rau;
    private DbsGroup rau_Rt_Record;
    private DbsField rau_Rt_A_I_Ind;
    private DbsField rau_Rt_Table_Id;
    private DbsField rau_Rt_Short_Key;
    private DbsField rau_Rt_Long_Key;
    private DbsField rau_Rt_Desc1;
    private DbsGroup rau_Rt_Desc1Redef5;
    private DbsField rau_Bank_Account_Name;
    private DbsField rau_Bank_Account_Cde;
    private DbsField rau_Bank_Account_Desc;
    private DbsField rau_Bank_Business_Unit;
    private DbsField rau_Bank_Format;
    private DbsField rau_Bank_Transmit_Ind;
    private DbsField rau_Check_Acct_Ind;
    private DbsField rau_Eft_Acct_Ind;
    private DbsField rau_Start_Seq;
    private DbsField rau_End_Seq;
    private DbsField rau_Seq_Maint_Ind;
    private DbsField rau_Hops_Acct_Ind;
    private DbsField rau_Bank_Ledger_Nbr;
    private DbsField rau_Bank_Signature_Cde;
    private DbsField rau_Bank_Void_Msg;
    private DbsField rau_Bank_Orgn_Cde;
    private DbsField rau_Bank_Internal_Pospay_Ind;
    private DbsField rau_Rt_Desc2;
    private DbsField rau_Rt_Desc3;
    private DbsField rau_Rt_Desc4;
    private DbsField rau_Rt_Desc5;
    private DbsField rau_Rt_Eff_From_Ccyymmdd;
    private DbsField rau_Rt_Eff_To_Ccyymmdd;
    private DbsField rau_Rt_Upd_Source;
    private DbsField rau_Rt_Upd_User;
    private DbsField rau_Rt_Upd_Ccyymmdd;
    private DbsField rau_Rt_Upd_Timn;
    private DataAccessProgramView vw_rl;
    private DbsGroup rl_Rt_Record;
    private DbsField rl_Rt_A_I_Ind;
    private DbsField rl_Rt_Table_Id;
    private DbsField rl_Rt_Short_Key;
    private DbsGroup rl_Rt_Short_KeyRedef6;
    private DbsField rl_Bank_Logo_Key;
    private DbsField filler02;
    private DbsField rl_Rt_Long_Key;
    private DbsField rl_Rt_Desc1;
    private DbsGroup rl_Rt_Desc1Redef7;
    private DbsField rl_Bank_Logo_Name1;
    private DbsField rl_Bank_Logo_Name2;
    private DbsField rl_Bank_Logo_Address1;
    private DbsField rl_Bank_Logo_Address2;
    private DbsField rl_Bank_Short_Logo_Name;
    private DbsField rl_Bank_Graphic_Logo_Cde;
    private DbsField rl_Rt_Desc2;
    private DbsField rl_Rt_Desc3;
    private DbsField rl_Rt_Desc4;
    private DbsField rl_Rt_Desc5;
    private DbsField rl_Rt_Eff_From_Ccyymmdd;
    private DbsField rl_Rt_Eff_To_Ccyymmdd;
    private DbsField rl_Rt_Upd_Source;
    private DbsField rl_Rt_Upd_User;
    private DbsField rl_Rt_Upd_Ccyymmdd;
    private DbsField rl_Rt_Upd_Timn;
    private DbsField rl_Rt_Super1;
    private DataAccessProgramView vw_rn;
    private DbsGroup rn_Rt_Record;
    private DbsField rn_Rt_A_I_Ind;
    private DbsField rn_Rt_Table_Id;
    private DbsField rn_Rt_Short_Key;
    private DbsField rn_Rt_Long_Key;
    private DbsGroup rn_Rt_Long_KeyRedef8;
    private DbsField rn_Bank_Routing;
    private DbsField filler03;
    private DbsField rn_Rt_Desc1;
    private DbsGroup rn_Rt_Desc1Redef9;
    private DbsField rn_Bank_Name;
    private DbsField rn_Bank_Address1;
    private DbsField rn_Bank_Address2;
    private DbsField rn_Bank_Above_Check_Amt_Nbr;
    private DbsField rn_Rt_Desc2;
    private DbsGroup rn_Rt_Desc2Redef10;
    private DbsField rn_Bank_Eft_Trans_Exists;
    private DbsField rn_Bank_Pos_Pay_Trans_Exists;
    private DbsField rn_Bank_Transmission_Cde;
    private DbsField rn_Rt_Desc3;
    private DbsField rn_Rt_Desc4;
    private DbsField rn_Rt_Desc5;
    private DbsField rn_Rt_Eff_From_Ccyymmdd;
    private DbsField rn_Rt_Eff_To_Ccyymmdd;
    private DbsField rn_Rt_Upd_Source;
    private DbsField rn_Rt_Upd_User;
    private DbsField rn_Rt_Upd_Ccyymmdd;
    private DbsField rn_Rt_Upd_Timn;
    private DbsField rn_Rt_Super2;

    public DataAccessProgramView getVw_ra() { return vw_ra; }

    public DbsGroup getRa_Rt_Record() { return ra_Rt_Record; }

    public DbsField getRa_Rt_A_I_Ind() { return ra_Rt_A_I_Ind; }

    public DbsField getRa_Rt_Table_Id() { return ra_Rt_Table_Id; }

    public DbsField getRa_Rt_Short_Key() { return ra_Rt_Short_Key; }

    public DbsGroup getRa_Rt_Short_KeyRedef1() { return ra_Rt_Short_KeyRedef1; }

    public DbsField getRa_Bank_Source_Code() { return ra_Bank_Source_Code; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getRa_Rt_Long_Key() { return ra_Rt_Long_Key; }

    public DbsGroup getRa_Rt_Long_KeyRedef2() { return ra_Rt_Long_KeyRedef2; }

    public DbsField getRa_Bank_Routing() { return ra_Bank_Routing; }

    public DbsField getRa_Filler1() { return ra_Filler1; }

    public DbsField getRa_Bank_Account() { return ra_Bank_Account; }

    public DbsField getRa_Filler2() { return ra_Filler2; }

    public DbsField getRa_Rt_Desc1() { return ra_Rt_Desc1; }

    public DbsGroup getRa_Rt_Desc1Redef3() { return ra_Rt_Desc1Redef3; }

    public DbsField getRa_Bank_Account_Name() { return ra_Bank_Account_Name; }

    public DbsField getRa_Bank_Account_Cde() { return ra_Bank_Account_Cde; }

    public DbsField getRa_Bank_Account_Desc() { return ra_Bank_Account_Desc; }

    public DbsField getRa_Bank_Business_Unit() { return ra_Bank_Business_Unit; }

    public DbsField getRa_Bank_Format() { return ra_Bank_Format; }

    public DbsField getRa_Bank_Transmit_Ind() { return ra_Bank_Transmit_Ind; }

    public DbsField getRa_Check_Acct_Ind() { return ra_Check_Acct_Ind; }

    public DbsField getRa_Eft_Acct_Ind() { return ra_Eft_Acct_Ind; }

    public DbsField getRa_Start_Seq() { return ra_Start_Seq; }

    public DbsField getRa_End_Seq() { return ra_End_Seq; }

    public DbsField getRa_Seq_Maint_Ind() { return ra_Seq_Maint_Ind; }

    public DbsField getRa_Hops_Acct_Ind() { return ra_Hops_Acct_Ind; }

    public DbsField getRa_Bank_Ledger_Nbr() { return ra_Bank_Ledger_Nbr; }

    public DbsField getRa_Bank_Signature_Cde() { return ra_Bank_Signature_Cde; }

    public DbsField getRa_Bank_Void_Msg() { return ra_Bank_Void_Msg; }

    public DbsField getRa_Bank_Orgn_Cde() { return ra_Bank_Orgn_Cde; }

    public DbsField getRa_Bank_Internal_Pospay_Ind() { return ra_Bank_Internal_Pospay_Ind; }

    public DbsField getRa_Rt_Desc2() { return ra_Rt_Desc2; }

    public DbsGroup getRa_Rt_Desc2Redef4() { return ra_Rt_Desc2Redef4; }

    public DbsField getRa_Bank_Logo_Key() { return ra_Bank_Logo_Key; }

    public DbsField getRa_Over_Logo_Name1() { return ra_Over_Logo_Name1; }

    public DbsField getRa_Over_Logo_Name2() { return ra_Over_Logo_Name2; }

    public DbsField getRa_Over_Logo_Addr1() { return ra_Over_Logo_Addr1; }

    public DbsField getRa_Over_Logo_Addr2() { return ra_Over_Logo_Addr2; }

    public DbsField getRa_Rt_Desc3() { return ra_Rt_Desc3; }

    public DbsField getRa_Rt_Desc4() { return ra_Rt_Desc4; }

    public DbsField getRa_Rt_Desc5() { return ra_Rt_Desc5; }

    public DbsField getRa_Rt_Eff_From_Ccyymmdd() { return ra_Rt_Eff_From_Ccyymmdd; }

    public DbsField getRa_Rt_Eff_To_Ccyymmdd() { return ra_Rt_Eff_To_Ccyymmdd; }

    public DbsField getRa_Rt_Upd_Source() { return ra_Rt_Upd_Source; }

    public DbsField getRa_Rt_Upd_User() { return ra_Rt_Upd_User; }

    public DbsField getRa_Rt_Upd_Ccyymmdd() { return ra_Rt_Upd_Ccyymmdd; }

    public DbsField getRa_Rt_Upd_Timn() { return ra_Rt_Upd_Timn; }

    public DbsField getRa_Rt_Super1() { return ra_Rt_Super1; }

    public DataAccessProgramView getVw_rau() { return vw_rau; }

    public DbsGroup getRau_Rt_Record() { return rau_Rt_Record; }

    public DbsField getRau_Rt_A_I_Ind() { return rau_Rt_A_I_Ind; }

    public DbsField getRau_Rt_Table_Id() { return rau_Rt_Table_Id; }

    public DbsField getRau_Rt_Short_Key() { return rau_Rt_Short_Key; }

    public DbsField getRau_Rt_Long_Key() { return rau_Rt_Long_Key; }

    public DbsField getRau_Rt_Desc1() { return rau_Rt_Desc1; }

    public DbsGroup getRau_Rt_Desc1Redef5() { return rau_Rt_Desc1Redef5; }

    public DbsField getRau_Bank_Account_Name() { return rau_Bank_Account_Name; }

    public DbsField getRau_Bank_Account_Cde() { return rau_Bank_Account_Cde; }

    public DbsField getRau_Bank_Account_Desc() { return rau_Bank_Account_Desc; }

    public DbsField getRau_Bank_Business_Unit() { return rau_Bank_Business_Unit; }

    public DbsField getRau_Bank_Format() { return rau_Bank_Format; }

    public DbsField getRau_Bank_Transmit_Ind() { return rau_Bank_Transmit_Ind; }

    public DbsField getRau_Check_Acct_Ind() { return rau_Check_Acct_Ind; }

    public DbsField getRau_Eft_Acct_Ind() { return rau_Eft_Acct_Ind; }

    public DbsField getRau_Start_Seq() { return rau_Start_Seq; }

    public DbsField getRau_End_Seq() { return rau_End_Seq; }

    public DbsField getRau_Seq_Maint_Ind() { return rau_Seq_Maint_Ind; }

    public DbsField getRau_Hops_Acct_Ind() { return rau_Hops_Acct_Ind; }

    public DbsField getRau_Bank_Ledger_Nbr() { return rau_Bank_Ledger_Nbr; }

    public DbsField getRau_Bank_Signature_Cde() { return rau_Bank_Signature_Cde; }

    public DbsField getRau_Bank_Void_Msg() { return rau_Bank_Void_Msg; }

    public DbsField getRau_Bank_Orgn_Cde() { return rau_Bank_Orgn_Cde; }

    public DbsField getRau_Bank_Internal_Pospay_Ind() { return rau_Bank_Internal_Pospay_Ind; }

    public DbsField getRau_Rt_Desc2() { return rau_Rt_Desc2; }

    public DbsField getRau_Rt_Desc3() { return rau_Rt_Desc3; }

    public DbsField getRau_Rt_Desc4() { return rau_Rt_Desc4; }

    public DbsField getRau_Rt_Desc5() { return rau_Rt_Desc5; }

    public DbsField getRau_Rt_Eff_From_Ccyymmdd() { return rau_Rt_Eff_From_Ccyymmdd; }

    public DbsField getRau_Rt_Eff_To_Ccyymmdd() { return rau_Rt_Eff_To_Ccyymmdd; }

    public DbsField getRau_Rt_Upd_Source() { return rau_Rt_Upd_Source; }

    public DbsField getRau_Rt_Upd_User() { return rau_Rt_Upd_User; }

    public DbsField getRau_Rt_Upd_Ccyymmdd() { return rau_Rt_Upd_Ccyymmdd; }

    public DbsField getRau_Rt_Upd_Timn() { return rau_Rt_Upd_Timn; }

    public DataAccessProgramView getVw_rl() { return vw_rl; }

    public DbsGroup getRl_Rt_Record() { return rl_Rt_Record; }

    public DbsField getRl_Rt_A_I_Ind() { return rl_Rt_A_I_Ind; }

    public DbsField getRl_Rt_Table_Id() { return rl_Rt_Table_Id; }

    public DbsField getRl_Rt_Short_Key() { return rl_Rt_Short_Key; }

    public DbsGroup getRl_Rt_Short_KeyRedef6() { return rl_Rt_Short_KeyRedef6; }

    public DbsField getRl_Bank_Logo_Key() { return rl_Bank_Logo_Key; }

    public DbsField getFiller02() { return filler02; }

    public DbsField getRl_Rt_Long_Key() { return rl_Rt_Long_Key; }

    public DbsField getRl_Rt_Desc1() { return rl_Rt_Desc1; }

    public DbsGroup getRl_Rt_Desc1Redef7() { return rl_Rt_Desc1Redef7; }

    public DbsField getRl_Bank_Logo_Name1() { return rl_Bank_Logo_Name1; }

    public DbsField getRl_Bank_Logo_Name2() { return rl_Bank_Logo_Name2; }

    public DbsField getRl_Bank_Logo_Address1() { return rl_Bank_Logo_Address1; }

    public DbsField getRl_Bank_Logo_Address2() { return rl_Bank_Logo_Address2; }

    public DbsField getRl_Bank_Short_Logo_Name() { return rl_Bank_Short_Logo_Name; }

    public DbsField getRl_Bank_Graphic_Logo_Cde() { return rl_Bank_Graphic_Logo_Cde; }

    public DbsField getRl_Rt_Desc2() { return rl_Rt_Desc2; }

    public DbsField getRl_Rt_Desc3() { return rl_Rt_Desc3; }

    public DbsField getRl_Rt_Desc4() { return rl_Rt_Desc4; }

    public DbsField getRl_Rt_Desc5() { return rl_Rt_Desc5; }

    public DbsField getRl_Rt_Eff_From_Ccyymmdd() { return rl_Rt_Eff_From_Ccyymmdd; }

    public DbsField getRl_Rt_Eff_To_Ccyymmdd() { return rl_Rt_Eff_To_Ccyymmdd; }

    public DbsField getRl_Rt_Upd_Source() { return rl_Rt_Upd_Source; }

    public DbsField getRl_Rt_Upd_User() { return rl_Rt_Upd_User; }

    public DbsField getRl_Rt_Upd_Ccyymmdd() { return rl_Rt_Upd_Ccyymmdd; }

    public DbsField getRl_Rt_Upd_Timn() { return rl_Rt_Upd_Timn; }

    public DbsField getRl_Rt_Super1() { return rl_Rt_Super1; }

    public DataAccessProgramView getVw_rn() { return vw_rn; }

    public DbsGroup getRn_Rt_Record() { return rn_Rt_Record; }

    public DbsField getRn_Rt_A_I_Ind() { return rn_Rt_A_I_Ind; }

    public DbsField getRn_Rt_Table_Id() { return rn_Rt_Table_Id; }

    public DbsField getRn_Rt_Short_Key() { return rn_Rt_Short_Key; }

    public DbsField getRn_Rt_Long_Key() { return rn_Rt_Long_Key; }

    public DbsGroup getRn_Rt_Long_KeyRedef8() { return rn_Rt_Long_KeyRedef8; }

    public DbsField getRn_Bank_Routing() { return rn_Bank_Routing; }

    public DbsField getFiller03() { return filler03; }

    public DbsField getRn_Rt_Desc1() { return rn_Rt_Desc1; }

    public DbsGroup getRn_Rt_Desc1Redef9() { return rn_Rt_Desc1Redef9; }

    public DbsField getRn_Bank_Name() { return rn_Bank_Name; }

    public DbsField getRn_Bank_Address1() { return rn_Bank_Address1; }

    public DbsField getRn_Bank_Address2() { return rn_Bank_Address2; }

    public DbsField getRn_Bank_Above_Check_Amt_Nbr() { return rn_Bank_Above_Check_Amt_Nbr; }

    public DbsField getRn_Rt_Desc2() { return rn_Rt_Desc2; }

    public DbsGroup getRn_Rt_Desc2Redef10() { return rn_Rt_Desc2Redef10; }

    public DbsField getRn_Bank_Eft_Trans_Exists() { return rn_Bank_Eft_Trans_Exists; }

    public DbsField getRn_Bank_Pos_Pay_Trans_Exists() { return rn_Bank_Pos_Pay_Trans_Exists; }

    public DbsField getRn_Bank_Transmission_Cde() { return rn_Bank_Transmission_Cde; }

    public DbsField getRn_Rt_Desc3() { return rn_Rt_Desc3; }

    public DbsField getRn_Rt_Desc4() { return rn_Rt_Desc4; }

    public DbsField getRn_Rt_Desc5() { return rn_Rt_Desc5; }

    public DbsField getRn_Rt_Eff_From_Ccyymmdd() { return rn_Rt_Eff_From_Ccyymmdd; }

    public DbsField getRn_Rt_Eff_To_Ccyymmdd() { return rn_Rt_Eff_To_Ccyymmdd; }

    public DbsField getRn_Rt_Upd_Source() { return rn_Rt_Upd_Source; }

    public DbsField getRn_Rt_Upd_User() { return rn_Rt_Upd_User; }

    public DbsField getRn_Rt_Upd_Ccyymmdd() { return rn_Rt_Upd_Ccyymmdd; }

    public DbsField getRn_Rt_Upd_Timn() { return rn_Rt_Upd_Timn; }

    public DbsField getRn_Rt_Super2() { return rn_Rt_Super2; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_ra = new DataAccessProgramView(new NameInfo("vw_ra", "RA"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        ra_Rt_Record = vw_ra.getRecord().newGroupInGroup("ra_Rt_Record", "RT-RECORD");
        ra_Rt_A_I_Ind = ra_Rt_Record.newFieldInGroup("ra_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        ra_Rt_Table_Id = ra_Rt_Record.newFieldInGroup("ra_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        ra_Rt_Short_Key = ra_Rt_Record.newFieldInGroup("ra_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        ra_Rt_Short_KeyRedef1 = vw_ra.getRecord().newGroupInGroup("ra_Rt_Short_KeyRedef1", "Redefines", ra_Rt_Short_Key);
        ra_Bank_Source_Code = ra_Rt_Short_KeyRedef1.newFieldInGroup("ra_Bank_Source_Code", "BANK-SOURCE-CODE", FieldType.STRING, 5);
        filler01 = ra_Rt_Short_KeyRedef1.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 15);
        ra_Rt_Long_Key = ra_Rt_Record.newFieldInGroup("ra_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        ra_Rt_Long_KeyRedef2 = vw_ra.getRecord().newGroupInGroup("ra_Rt_Long_KeyRedef2", "Redefines", ra_Rt_Long_Key);
        ra_Bank_Routing = ra_Rt_Long_KeyRedef2.newFieldInGroup("ra_Bank_Routing", "BANK-ROUTING", FieldType.STRING, 9);
        ra_Filler1 = ra_Rt_Long_KeyRedef2.newFieldInGroup("ra_Filler1", "FILLER1", FieldType.STRING, 1);
        ra_Bank_Account = ra_Rt_Long_KeyRedef2.newFieldInGroup("ra_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21);
        ra_Filler2 = ra_Rt_Long_KeyRedef2.newFieldInGroup("ra_Filler2", "FILLER2", FieldType.STRING, 9);
        ra_Rt_Desc1 = ra_Rt_Record.newFieldInGroup("ra_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        ra_Rt_Desc1Redef3 = vw_ra.getRecord().newGroupInGroup("ra_Rt_Desc1Redef3", "Redefines", ra_Rt_Desc1);
        ra_Bank_Account_Name = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Bank_Account_Name", "BANK-ACCOUNT-NAME", FieldType.STRING, 50);
        ra_Bank_Account_Cde = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Bank_Account_Cde", "BANK-ACCOUNT-CDE", FieldType.STRING, 10);
        ra_Bank_Account_Desc = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Bank_Account_Desc", "BANK-ACCOUNT-DESC", FieldType.STRING, 50);
        ra_Bank_Business_Unit = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Bank_Business_Unit", "BANK-BUSINESS-UNIT", FieldType.STRING, 5);
        ra_Bank_Format = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Bank_Format", "BANK-FORMAT", FieldType.STRING, 5);
        ra_Bank_Transmit_Ind = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Bank_Transmit_Ind", "BANK-TRANSMIT-IND", FieldType.STRING, 1);
        ra_Check_Acct_Ind = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Check_Acct_Ind", "CHECK-ACCT-IND", FieldType.STRING, 1);
        ra_Eft_Acct_Ind = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Eft_Acct_Ind", "EFT-ACCT-IND", FieldType.STRING, 1);
        ra_Start_Seq = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Start_Seq", "START-SEQ", FieldType.NUMERIC, 10);
        ra_End_Seq = ra_Rt_Desc1Redef3.newFieldInGroup("ra_End_Seq", "END-SEQ", FieldType.NUMERIC, 10);
        ra_Seq_Maint_Ind = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Seq_Maint_Ind", "SEQ-MAINT-IND", FieldType.STRING, 1);
        ra_Hops_Acct_Ind = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Hops_Acct_Ind", "HOPS-ACCT-IND", FieldType.STRING, 1);
        ra_Bank_Ledger_Nbr = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Bank_Ledger_Nbr", "BANK-LEDGER-NBR", FieldType.STRING, 8);
        ra_Bank_Signature_Cde = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Bank_Signature_Cde", "BANK-SIGNATURE-CDE", FieldType.STRING, 5);
        ra_Bank_Void_Msg = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Bank_Void_Msg", "BANK-VOID-MSG", FieldType.STRING, 30);
        ra_Bank_Orgn_Cde = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Bank_Orgn_Cde", "BANK-ORGN-CDE", FieldType.STRING, 2);
        ra_Bank_Internal_Pospay_Ind = ra_Rt_Desc1Redef3.newFieldInGroup("ra_Bank_Internal_Pospay_Ind", "BANK-INTERNAL-POSPAY-IND", FieldType.STRING, 1);
        ra_Rt_Desc2 = ra_Rt_Record.newFieldInGroup("ra_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        ra_Rt_Desc2Redef4 = vw_ra.getRecord().newGroupInGroup("ra_Rt_Desc2Redef4", "Redefines", ra_Rt_Desc2);
        ra_Bank_Logo_Key = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Bank_Logo_Key", "BANK-LOGO-KEY", FieldType.STRING, 10);
        ra_Over_Logo_Name1 = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Over_Logo_Name1", "OVER-LOGO-NAME1", FieldType.STRING, 50);
        ra_Over_Logo_Name2 = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Over_Logo_Name2", "OVER-LOGO-NAME2", FieldType.STRING, 50);
        ra_Over_Logo_Addr1 = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Over_Logo_Addr1", "OVER-LOGO-ADDR1", FieldType.STRING, 50);
        ra_Over_Logo_Addr2 = ra_Rt_Desc2Redef4.newFieldInGroup("ra_Over_Logo_Addr2", "OVER-LOGO-ADDR2", FieldType.STRING, 50);
        ra_Rt_Desc3 = ra_Rt_Record.newFieldInGroup("ra_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        ra_Rt_Desc4 = ra_Rt_Record.newFieldInGroup("ra_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        ra_Rt_Desc5 = ra_Rt_Record.newFieldInGroup("ra_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        ra_Rt_Eff_From_Ccyymmdd = ra_Rt_Record.newFieldInGroup("ra_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_FROM_CCYYMMDD");
        ra_Rt_Eff_To_Ccyymmdd = ra_Rt_Record.newFieldInGroup("ra_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        ra_Rt_Upd_Source = ra_Rt_Record.newFieldInGroup("ra_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_SOURCE");
        ra_Rt_Upd_User = ra_Rt_Record.newFieldInGroup("ra_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_USER");
        ra_Rt_Upd_Ccyymmdd = ra_Rt_Record.newFieldInGroup("ra_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_CCYYMMDD");
        ra_Rt_Upd_Timn = ra_Rt_Record.newFieldInGroup("ra_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RT_UPD_TIMN");
        ra_Rt_Super1 = vw_ra.getRecord().newFieldInGroup("ra_Rt_Super1", "RT-SUPER1", FieldType.STRING, 66, RepeatingFieldStrategy.None, "RT_SUPER1");

        vw_rau = new DataAccessProgramView(new NameInfo("vw_rau", "RAU"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        rau_Rt_Record = vw_rau.getRecord().newGroupInGroup("rau_Rt_Record", "RT-RECORD");
        rau_Rt_A_I_Ind = rau_Rt_Record.newFieldInGroup("rau_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rau_Rt_Table_Id = rau_Rt_Record.newFieldInGroup("rau_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        rau_Rt_Short_Key = rau_Rt_Record.newFieldInGroup("rau_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        rau_Rt_Long_Key = rau_Rt_Record.newFieldInGroup("rau_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        rau_Rt_Desc1 = rau_Rt_Record.newFieldInGroup("rau_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        rau_Rt_Desc1Redef5 = vw_rau.getRecord().newGroupInGroup("rau_Rt_Desc1Redef5", "Redefines", rau_Rt_Desc1);
        rau_Bank_Account_Name = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Bank_Account_Name", "BANK-ACCOUNT-NAME", FieldType.STRING, 50);
        rau_Bank_Account_Cde = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Bank_Account_Cde", "BANK-ACCOUNT-CDE", FieldType.STRING, 10);
        rau_Bank_Account_Desc = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Bank_Account_Desc", "BANK-ACCOUNT-DESC", FieldType.STRING, 50);
        rau_Bank_Business_Unit = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Bank_Business_Unit", "BANK-BUSINESS-UNIT", FieldType.STRING, 5);
        rau_Bank_Format = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Bank_Format", "BANK-FORMAT", FieldType.STRING, 5);
        rau_Bank_Transmit_Ind = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Bank_Transmit_Ind", "BANK-TRANSMIT-IND", FieldType.STRING, 1);
        rau_Check_Acct_Ind = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Check_Acct_Ind", "CHECK-ACCT-IND", FieldType.STRING, 1);
        rau_Eft_Acct_Ind = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Eft_Acct_Ind", "EFT-ACCT-IND", FieldType.STRING, 1);
        rau_Start_Seq = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Start_Seq", "START-SEQ", FieldType.NUMERIC, 10);
        rau_End_Seq = rau_Rt_Desc1Redef5.newFieldInGroup("rau_End_Seq", "END-SEQ", FieldType.NUMERIC, 10);
        rau_Seq_Maint_Ind = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Seq_Maint_Ind", "SEQ-MAINT-IND", FieldType.STRING, 1);
        rau_Hops_Acct_Ind = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Hops_Acct_Ind", "HOPS-ACCT-IND", FieldType.STRING, 1);
        rau_Bank_Ledger_Nbr = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Bank_Ledger_Nbr", "BANK-LEDGER-NBR", FieldType.STRING, 8);
        rau_Bank_Signature_Cde = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Bank_Signature_Cde", "BANK-SIGNATURE-CDE", FieldType.STRING, 5);
        rau_Bank_Void_Msg = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Bank_Void_Msg", "BANK-VOID-MSG", FieldType.STRING, 15);
        rau_Bank_Orgn_Cde = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Bank_Orgn_Cde", "BANK-ORGN-CDE", FieldType.STRING, 2);
        rau_Bank_Internal_Pospay_Ind = rau_Rt_Desc1Redef5.newFieldInGroup("rau_Bank_Internal_Pospay_Ind", "BANK-INTERNAL-POSPAY-IND", FieldType.STRING, 
            1);
        rau_Rt_Desc2 = rau_Rt_Record.newFieldInGroup("rau_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        rau_Rt_Desc3 = rau_Rt_Record.newFieldInGroup("rau_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        rau_Rt_Desc4 = rau_Rt_Record.newFieldInGroup("rau_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        rau_Rt_Desc5 = rau_Rt_Record.newFieldInGroup("rau_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        rau_Rt_Eff_From_Ccyymmdd = rau_Rt_Record.newFieldInGroup("rau_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_FROM_CCYYMMDD");
        rau_Rt_Eff_To_Ccyymmdd = rau_Rt_Record.newFieldInGroup("rau_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        rau_Rt_Upd_Source = rau_Rt_Record.newFieldInGroup("rau_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_SOURCE");
        rau_Rt_Upd_User = rau_Rt_Record.newFieldInGroup("rau_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_USER");
        rau_Rt_Upd_Ccyymmdd = rau_Rt_Record.newFieldInGroup("rau_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_UPD_CCYYMMDD");
        rau_Rt_Upd_Timn = rau_Rt_Record.newFieldInGroup("rau_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RT_UPD_TIMN");

        vw_rl = new DataAccessProgramView(new NameInfo("vw_rl", "RL"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        rl_Rt_Record = vw_rl.getRecord().newGroupInGroup("rl_Rt_Record", "RT-RECORD");
        rl_Rt_A_I_Ind = rl_Rt_Record.newFieldInGroup("rl_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rl_Rt_Table_Id = rl_Rt_Record.newFieldInGroup("rl_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        rl_Rt_Short_Key = rl_Rt_Record.newFieldInGroup("rl_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        rl_Rt_Short_KeyRedef6 = vw_rl.getRecord().newGroupInGroup("rl_Rt_Short_KeyRedef6", "Redefines", rl_Rt_Short_Key);
        rl_Bank_Logo_Key = rl_Rt_Short_KeyRedef6.newFieldInGroup("rl_Bank_Logo_Key", "BANK-LOGO-KEY", FieldType.STRING, 10);
        filler02 = rl_Rt_Short_KeyRedef6.newFieldInGroup("filler02", "FILLER", FieldType.STRING, 10);
        rl_Rt_Long_Key = rl_Rt_Record.newFieldInGroup("rl_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        rl_Rt_Desc1 = rl_Rt_Record.newFieldInGroup("rl_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        rl_Rt_Desc1Redef7 = vw_rl.getRecord().newGroupInGroup("rl_Rt_Desc1Redef7", "Redefines", rl_Rt_Desc1);
        rl_Bank_Logo_Name1 = rl_Rt_Desc1Redef7.newFieldInGroup("rl_Bank_Logo_Name1", "BANK-LOGO-NAME1", FieldType.STRING, 50);
        rl_Bank_Logo_Name2 = rl_Rt_Desc1Redef7.newFieldInGroup("rl_Bank_Logo_Name2", "BANK-LOGO-NAME2", FieldType.STRING, 50);
        rl_Bank_Logo_Address1 = rl_Rt_Desc1Redef7.newFieldInGroup("rl_Bank_Logo_Address1", "BANK-LOGO-ADDRESS1", FieldType.STRING, 50);
        rl_Bank_Logo_Address2 = rl_Rt_Desc1Redef7.newFieldInGroup("rl_Bank_Logo_Address2", "BANK-LOGO-ADDRESS2", FieldType.STRING, 50);
        rl_Bank_Short_Logo_Name = rl_Rt_Desc1Redef7.newFieldInGroup("rl_Bank_Short_Logo_Name", "BANK-SHORT-LOGO-NAME", FieldType.STRING, 15);
        rl_Bank_Graphic_Logo_Cde = rl_Rt_Desc1Redef7.newFieldInGroup("rl_Bank_Graphic_Logo_Cde", "BANK-GRAPHIC-LOGO-CDE", FieldType.STRING, 5);
        rl_Rt_Desc2 = rl_Rt_Record.newFieldInGroup("rl_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        rl_Rt_Desc3 = rl_Rt_Record.newFieldInGroup("rl_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        rl_Rt_Desc4 = rl_Rt_Record.newFieldInGroup("rl_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        rl_Rt_Desc5 = rl_Rt_Record.newFieldInGroup("rl_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        rl_Rt_Eff_From_Ccyymmdd = rl_Rt_Record.newFieldInGroup("rl_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_FROM_CCYYMMDD");
        rl_Rt_Eff_To_Ccyymmdd = rl_Rt_Record.newFieldInGroup("rl_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        rl_Rt_Upd_Source = rl_Rt_Record.newFieldInGroup("rl_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_SOURCE");
        rl_Rt_Upd_User = rl_Rt_Record.newFieldInGroup("rl_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_USER");
        rl_Rt_Upd_Ccyymmdd = rl_Rt_Record.newFieldInGroup("rl_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_CCYYMMDD");
        rl_Rt_Upd_Timn = rl_Rt_Record.newFieldInGroup("rl_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RT_UPD_TIMN");
        rl_Rt_Super1 = vw_rl.getRecord().newFieldInGroup("rl_Rt_Super1", "RT-SUPER1", FieldType.STRING, 66, RepeatingFieldStrategy.None, "RT_SUPER1");

        vw_rn = new DataAccessProgramView(new NameInfo("vw_rn", "RN"), "REFERENCE_TABLE", "REFERNCE_TABLE");
        rn_Rt_Record = vw_rn.getRecord().newGroupInGroup("rn_Rt_Record", "RT-RECORD");
        rn_Rt_A_I_Ind = rn_Rt_Record.newFieldInGroup("rn_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        rn_Rt_Table_Id = rn_Rt_Record.newFieldInGroup("rn_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        rn_Rt_Short_Key = rn_Rt_Record.newFieldInGroup("rn_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        rn_Rt_Long_Key = rn_Rt_Record.newFieldInGroup("rn_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        rn_Rt_Long_KeyRedef8 = vw_rn.getRecord().newGroupInGroup("rn_Rt_Long_KeyRedef8", "Redefines", rn_Rt_Long_Key);
        rn_Bank_Routing = rn_Rt_Long_KeyRedef8.newFieldInGroup("rn_Bank_Routing", "BANK-ROUTING", FieldType.STRING, 9);
        filler03 = rn_Rt_Long_KeyRedef8.newFieldInGroup("filler03", "FILLER", FieldType.STRING, 31);
        rn_Rt_Desc1 = rn_Rt_Record.newFieldInGroup("rn_Rt_Desc1", "RT-DESC1", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC1");
        rn_Rt_Desc1Redef9 = vw_rn.getRecord().newGroupInGroup("rn_Rt_Desc1Redef9", "Redefines", rn_Rt_Desc1);
        rn_Bank_Name = rn_Rt_Desc1Redef9.newFieldInGroup("rn_Bank_Name", "BANK-NAME", FieldType.STRING, 50);
        rn_Bank_Address1 = rn_Rt_Desc1Redef9.newFieldInGroup("rn_Bank_Address1", "BANK-ADDRESS1", FieldType.STRING, 50);
        rn_Bank_Address2 = rn_Rt_Desc1Redef9.newFieldInGroup("rn_Bank_Address2", "BANK-ADDRESS2", FieldType.STRING, 50);
        rn_Bank_Above_Check_Amt_Nbr = rn_Rt_Desc1Redef9.newFieldInGroup("rn_Bank_Above_Check_Amt_Nbr", "BANK-ABOVE-CHECK-AMT-NBR", FieldType.STRING, 10);
        rn_Rt_Desc2 = rn_Rt_Record.newFieldInGroup("rn_Rt_Desc2", "RT-DESC2", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC2");
        rn_Rt_Desc2Redef10 = vw_rn.getRecord().newGroupInGroup("rn_Rt_Desc2Redef10", "Redefines", rn_Rt_Desc2);
        rn_Bank_Eft_Trans_Exists = rn_Rt_Desc2Redef10.newFieldInGroup("rn_Bank_Eft_Trans_Exists", "BANK-EFT-TRANS-EXISTS", FieldType.STRING, 1);
        rn_Bank_Pos_Pay_Trans_Exists = rn_Rt_Desc2Redef10.newFieldInGroup("rn_Bank_Pos_Pay_Trans_Exists", "BANK-POS-PAY-TRANS-EXISTS", FieldType.STRING, 
            1);
        rn_Bank_Transmission_Cde = rn_Rt_Desc2Redef10.newFieldInGroup("rn_Bank_Transmission_Cde", "BANK-TRANSMISSION-CDE", FieldType.STRING, 5);
        rn_Rt_Desc3 = rn_Rt_Record.newFieldInGroup("rn_Rt_Desc3", "RT-DESC3", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC3");
        rn_Rt_Desc4 = rn_Rt_Record.newFieldInGroup("rn_Rt_Desc4", "RT-DESC4", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC4");
        rn_Rt_Desc5 = rn_Rt_Record.newFieldInGroup("rn_Rt_Desc5", "RT-DESC5", FieldType.STRING, 250, RepeatingFieldStrategy.None, "RT_DESC5");
        rn_Rt_Eff_From_Ccyymmdd = rn_Rt_Record.newFieldInGroup("rn_Rt_Eff_From_Ccyymmdd", "RT-EFF-FROM-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_FROM_CCYYMMDD");
        rn_Rt_Eff_To_Ccyymmdd = rn_Rt_Record.newFieldInGroup("rn_Rt_Eff_To_Ccyymmdd", "RT-EFF-TO-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "RT_EFF_TO_CCYYMMDD");
        rn_Rt_Upd_Source = rn_Rt_Record.newFieldInGroup("rn_Rt_Upd_Source", "RT-UPD-SOURCE", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_SOURCE");
        rn_Rt_Upd_User = rn_Rt_Record.newFieldInGroup("rn_Rt_Upd_User", "RT-UPD-USER", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_USER");
        rn_Rt_Upd_Ccyymmdd = rn_Rt_Record.newFieldInGroup("rn_Rt_Upd_Ccyymmdd", "RT-UPD-CCYYMMDD", FieldType.STRING, 8, RepeatingFieldStrategy.None, "RT_UPD_CCYYMMDD");
        rn_Rt_Upd_Timn = rn_Rt_Record.newFieldInGroup("rn_Rt_Upd_Timn", "RT-UPD-TIMN", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "RT_UPD_TIMN");
        rn_Rt_Super2 = vw_rn.getRecord().newFieldInGroup("rn_Rt_Super2", "RT-SUPER2", FieldType.STRING, 66, RepeatingFieldStrategy.None, "RT_SUPER2");

        this.setRecordName("LdaCpsl110");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_ra.reset();
        vw_rau.reset();
        vw_rl.reset();
        vw_rn.reset();
    }

    // Constructor
    public LdaCpsl110() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
