/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:25 PM
**        * FROM NATURAL LDA     : FCPLBAR
************************************************************
**        * FILE NAME            : LdaFcplbar.java
**        * CLASS NAME           : LdaFcplbar
**        * INSTANCE NAME        : LdaFcplbar
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplbar extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Bar_Table;
    private DbsField pnd_Bar_Table_Pnd_Bar_Table_Search_Key;
    private DbsField pnd_Bar_Table_Pnd_Bar_Table_Value;

    public DbsGroup getPnd_Bar_Table() { return pnd_Bar_Table; }

    public DbsField getPnd_Bar_Table_Pnd_Bar_Table_Search_Key() { return pnd_Bar_Table_Pnd_Bar_Table_Search_Key; }

    public DbsField getPnd_Bar_Table_Pnd_Bar_Table_Value() { return pnd_Bar_Table_Pnd_Bar_Table_Value; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Bar_Table = newGroupInRecord("pnd_Bar_Table", "#BAR-TABLE");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key = pnd_Bar_Table.newFieldArrayInGroup("pnd_Bar_Table_Pnd_Bar_Table_Search_Key", "#BAR-TABLE-SEARCH-KEY", 
            FieldType.STRING, 5, new DbsArrayController(1,32));
        pnd_Bar_Table_Pnd_Bar_Table_Value = pnd_Bar_Table.newFieldArrayInGroup("pnd_Bar_Table_Pnd_Bar_Table_Value", "#BAR-TABLE-VALUE", FieldType.STRING, 
            1, new DbsArrayController(1,32));

        this.setRecordName("LdaFcplbar");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(1).setInitialValue("00000");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(2).setInitialValue("00001");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(3).setInitialValue("00010");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(4).setInitialValue("00011");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(5).setInitialValue("00100");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(6).setInitialValue("00101");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(7).setInitialValue("00110");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(8).setInitialValue("00111");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(9).setInitialValue("01000");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(10).setInitialValue("01001");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(11).setInitialValue("01010");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(12).setInitialValue("01011");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(13).setInitialValue("01100");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(14).setInitialValue("01101");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(15).setInitialValue("01110");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(16).setInitialValue("01111");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(17).setInitialValue("10000");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(18).setInitialValue("10001");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(19).setInitialValue("10010");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(20).setInitialValue("10011");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(21).setInitialValue("10100");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(22).setInitialValue("10101");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(23).setInitialValue("10110");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(24).setInitialValue("10111");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(25).setInitialValue("11000");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(26).setInitialValue("11001");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(27).setInitialValue("11010");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(28).setInitialValue("11011");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(29).setInitialValue("11100");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(30).setInitialValue("11101");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(31).setInitialValue("11110");
        pnd_Bar_Table_Pnd_Bar_Table_Search_Key.getValue(32).setInitialValue("11111");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(1).setInitialValue("0");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(2).setInitialValue("1");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(3).setInitialValue("2");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(4).setInitialValue("3");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(5).setInitialValue("4");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(6).setInitialValue("5");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(7).setInitialValue("6");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(8).setInitialValue("7");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(9).setInitialValue("8");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(10).setInitialValue("9");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(11).setInitialValue("A");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(12).setInitialValue("B");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(13).setInitialValue("C");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(14).setInitialValue("D");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(15).setInitialValue("E");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(16).setInitialValue("F");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(17).setInitialValue("G");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(18).setInitialValue("H");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(19).setInitialValue("I");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(20).setInitialValue("J");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(21).setInitialValue("K");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(22).setInitialValue("L");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(23).setInitialValue("M");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(24).setInitialValue("N");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(25).setInitialValue("O");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(26).setInitialValue("P");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(27).setInitialValue("Q");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(28).setInitialValue("R");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(29).setInitialValue("S");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(30).setInitialValue("T");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(31).setInitialValue("U");
        pnd_Bar_Table_Pnd_Bar_Table_Value.getValue(32).setInitialValue("V");
    }

    // Constructor
    public LdaFcplbar() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
