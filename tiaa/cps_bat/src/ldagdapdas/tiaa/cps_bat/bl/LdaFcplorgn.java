/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:34 PM
**        * FROM NATURAL LDA     : FCPLORGN
************************************************************
**        * FILE NAME            : LdaFcplorgn.java
**        * CLASS NAME           : LdaFcplorgn
**        * INSTANCE NAME        : LdaFcplorgn
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcplorgn extends DbsRecord
{
    // Properties
    private DbsField t_Max;
    private DbsField t_Orgn_Cde;
    private DbsField t_Orgn_Desc;

    public DbsField getT_Max() { return t_Max; }

    public DbsField getT_Orgn_Cde() { return t_Orgn_Cde; }

    public DbsField getT_Orgn_Desc() { return t_Orgn_Desc; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        t_Max = newFieldInRecord("t_Max", "T-MAX", FieldType.NUMERIC, 2);

        t_Orgn_Cde = newFieldArrayInRecord("t_Orgn_Cde", "T-ORGN-CDE", FieldType.STRING, 2, new DbsArrayController(1,20));

        t_Orgn_Desc = newFieldArrayInRecord("t_Orgn_Desc", "T-ORGN-DESC", FieldType.STRING, 22, new DbsArrayController(1,20));

        this.setRecordName("LdaFcplorgn");
    }

    public void initializeValues() throws Exception
    {
        reset();
        t_Max.setInitialValue(20);
        t_Orgn_Cde.getValue(1).setInitialValue("AL");
        t_Orgn_Cde.getValue(2).setInitialValue("AP");
        t_Orgn_Cde.getValue(3).setInitialValue("DC");
        t_Orgn_Cde.getValue(4).setInitialValue("DS");
        t_Orgn_Cde.getValue(5).setInitialValue("EW");
        t_Orgn_Cde.getValue(6).setInitialValue("IA");
        t_Orgn_Cde.getValue(7).setInitialValue("MS");
        t_Orgn_Cde.getValue(8).setInitialValue("NZ");
        t_Orgn_Cde.getValue(9).setInitialValue("SS");
        t_Orgn_Cde.getValue(10).setInitialValue("VT");
        t_Orgn_Desc.getValue(1).setInitialValue("Annuity Loans");
        t_Orgn_Desc.getValue(2).setInitialValue("Annuity Payments (IAA)");
        t_Orgn_Desc.getValue(3).setInitialValue("IA Death");
        t_Orgn_Desc.getValue(4).setInitialValue("Daily Settlements");
        t_Orgn_Desc.getValue(5).setInitialValue("Electronic Warrants");
        t_Orgn_Desc.getValue(6).setInitialValue("Immediate Annuity");
        t_Orgn_Desc.getValue(7).setInitialValue("Monthly Settlements");
        t_Orgn_Desc.getValue(8).setInitialValue("New Annuitization");
        t_Orgn_Desc.getValue(9).setInitialValue("Single Sum");
        t_Orgn_Desc.getValue(10).setInitialValue("Proline");
        t_Orgn_Desc.getValue(11).setInitialValue("Origin missing");
    }

    // Constructor
    public LdaFcplorgn() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
