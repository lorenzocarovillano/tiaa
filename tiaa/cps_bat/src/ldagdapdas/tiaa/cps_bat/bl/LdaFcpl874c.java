/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:59:17 PM
**        * FROM NATURAL LDA     : FCPL874C
************************************************************
**        * FILE NAME            : LdaFcpl874c.java
**        * CLASS NAME           : LdaFcpl874c
**        * INSTANCE NAME        : LdaFcpl874c
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaFcpl874c extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Fcpl874c;
    private DbsField pnd_Fcpl874c_Pnd_Letter_Text;

    public DbsGroup getPnd_Fcpl874c() { return pnd_Fcpl874c; }

    public DbsField getPnd_Fcpl874c_Pnd_Letter_Text() { return pnd_Fcpl874c_Pnd_Letter_Text; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpl874c = newGroupInRecord("pnd_Fcpl874c", "#FCPL874C");
        pnd_Fcpl874c_Pnd_Letter_Text = pnd_Fcpl874c.newFieldArrayInGroup("pnd_Fcpl874c_Pnd_Letter_Text", "#LETTER-TEXT", FieldType.STRING, 90, new DbsArrayController(1,
            2,1,4));

        this.setRecordName("LdaFcpl874c");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Fcpl874c_Pnd_Letter_Text.getValue(1,1).setInitialValue("1!This individual has requested that we mail the attached check to you.");
        pnd_Fcpl874c_Pnd_Letter_Text.getValue(1,2).setInitialValue("!It is to be credited to Account No.");
        pnd_Fcpl874c_Pnd_Letter_Text.getValue(1,3).setInitialValue("!If you have any questions, please call us at 800 842-2776, Monday to");
        pnd_Fcpl874c_Pnd_Letter_Text.getValue(1,4).setInitialValue("!Friday from 8 a.m. to 10 p.m. ET Saturday from 9a.m. to 6 p.m. ET.");
        pnd_Fcpl874c_Pnd_Letter_Text.getValue(2,1).setInitialValue("1!In accordance with your instructions, your payment has been sent to");
        pnd_Fcpl874c_Pnd_Letter_Text.getValue(2,2).setInitialValue("!this address.  A breakdown of this payment has been sent to your");
        pnd_Fcpl874c_Pnd_Letter_Text.getValue(2,3).setInitialValue("!residence address.");
    }

    // Constructor
    public LdaFcpl874c() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
