/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:20:27 PM
**        * FROM NATURAL PDA     : FCPA803H
************************************************************
**        * FILE NAME            : PdaFcpa803h.java
**        * CLASS NAME           : PdaFcpa803h
**        * INSTANCE NAME        : PdaFcpa803h
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaFcpa803h extends PdaBase
{
    // Properties
    private DbsGroup pnd_Fcpa803h;
    private DbsGroup pnd_Fcpa803h_Ph_Name;
    private DbsField pnd_Fcpa803h_Ph_Last_Name;
    private DbsField pnd_Fcpa803h_Ph_First_Name;
    private DbsField pnd_Fcpa803h_Ph_Middle_Name;
    private DbsField pnd_Fcpa803h_Pymnt_Check_Nbr;
    private DbsField pnd_Fcpa803h_Pymnt_Check_Dte;
    private DbsField pnd_Fcpa803h_Pnd_Current_Page;
    private DbsField pnd_Fcpa803h_Pnd_Stmnt;
    private DbsField pnd_Fcpa803h_Pnd_Gen_Headers;
    private DbsField pnd_Fcpa803h_Pnd_Header_Array;
    private DbsField pnd_Fcpa803h_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Fcpa803h_Cnr_Orgnl_Invrse_Dte;

    public DbsGroup getPnd_Fcpa803h() { return pnd_Fcpa803h; }

    public DbsGroup getPnd_Fcpa803h_Ph_Name() { return pnd_Fcpa803h_Ph_Name; }

    public DbsField getPnd_Fcpa803h_Ph_Last_Name() { return pnd_Fcpa803h_Ph_Last_Name; }

    public DbsField getPnd_Fcpa803h_Ph_First_Name() { return pnd_Fcpa803h_Ph_First_Name; }

    public DbsField getPnd_Fcpa803h_Ph_Middle_Name() { return pnd_Fcpa803h_Ph_Middle_Name; }

    public DbsField getPnd_Fcpa803h_Pymnt_Check_Nbr() { return pnd_Fcpa803h_Pymnt_Check_Nbr; }

    public DbsField getPnd_Fcpa803h_Pymnt_Check_Dte() { return pnd_Fcpa803h_Pymnt_Check_Dte; }

    public DbsField getPnd_Fcpa803h_Pnd_Current_Page() { return pnd_Fcpa803h_Pnd_Current_Page; }

    public DbsField getPnd_Fcpa803h_Pnd_Stmnt() { return pnd_Fcpa803h_Pnd_Stmnt; }

    public DbsField getPnd_Fcpa803h_Pnd_Gen_Headers() { return pnd_Fcpa803h_Pnd_Gen_Headers; }

    public DbsField getPnd_Fcpa803h_Pnd_Header_Array() { return pnd_Fcpa803h_Pnd_Header_Array; }

    public DbsField getPnd_Fcpa803h_Cntrct_Cancel_Rdrw_Actvty_Cde() { return pnd_Fcpa803h_Cntrct_Cancel_Rdrw_Actvty_Cde; }

    public DbsField getPnd_Fcpa803h_Cnr_Orgnl_Invrse_Dte() { return pnd_Fcpa803h_Cnr_Orgnl_Invrse_Dte; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Fcpa803h = dbsRecord.newGroupInRecord("pnd_Fcpa803h", "#FCPA803H");
        pnd_Fcpa803h.setParameterOption(ParameterOption.ByReference);
        pnd_Fcpa803h_Ph_Name = pnd_Fcpa803h.newGroupInGroup("pnd_Fcpa803h_Ph_Name", "PH-NAME");
        pnd_Fcpa803h_Ph_Last_Name = pnd_Fcpa803h_Ph_Name.newFieldInGroup("pnd_Fcpa803h_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        pnd_Fcpa803h_Ph_First_Name = pnd_Fcpa803h_Ph_Name.newFieldInGroup("pnd_Fcpa803h_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        pnd_Fcpa803h_Ph_Middle_Name = pnd_Fcpa803h_Ph_Name.newFieldInGroup("pnd_Fcpa803h_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        pnd_Fcpa803h_Pymnt_Check_Nbr = pnd_Fcpa803h.newFieldInGroup("pnd_Fcpa803h_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 10);
        pnd_Fcpa803h_Pymnt_Check_Dte = pnd_Fcpa803h.newFieldInGroup("pnd_Fcpa803h_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Fcpa803h_Pnd_Current_Page = pnd_Fcpa803h.newFieldInGroup("pnd_Fcpa803h_Pnd_Current_Page", "#CURRENT-PAGE", FieldType.PACKED_DECIMAL, 3);
        pnd_Fcpa803h_Pnd_Stmnt = pnd_Fcpa803h.newFieldInGroup("pnd_Fcpa803h_Pnd_Stmnt", "#STMNT", FieldType.BOOLEAN);
        pnd_Fcpa803h_Pnd_Gen_Headers = pnd_Fcpa803h.newFieldInGroup("pnd_Fcpa803h_Pnd_Gen_Headers", "#GEN-HEADERS", FieldType.BOOLEAN);
        pnd_Fcpa803h_Pnd_Header_Array = pnd_Fcpa803h.newFieldArrayInGroup("pnd_Fcpa803h_Pnd_Header_Array", "#HEADER-ARRAY", FieldType.STRING, 143, new 
            DbsArrayController(1,4));
        pnd_Fcpa803h_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Fcpa803h.newFieldInGroup("pnd_Fcpa803h_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Fcpa803h_Cnr_Orgnl_Invrse_Dte = pnd_Fcpa803h.newFieldInGroup("pnd_Fcpa803h_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 
            8);

        dbsRecord.reset();
    }

    // Constructors
    public PdaFcpa803h(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

