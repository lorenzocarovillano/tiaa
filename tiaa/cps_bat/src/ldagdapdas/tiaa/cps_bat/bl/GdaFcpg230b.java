/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:48:45 PM
**        * FROM NATURAL GDA     : FCPG230B
************************************************************
**        * FILE NAME            : GdaFcpg230b.java
**        * CLASS NAME           : GdaFcpg230b
**        * INSTANCE NAME        : GdaFcpg230b
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import java.util.ArrayList;
import java.util.List;

import tiaa.tiaacommon.bl.*;

public final class GdaFcpg230b extends DbsRecord
{
    private static ThreadLocal<List<GdaFcpg230b>> _instance = new ThreadLocal<List<GdaFcpg230b>>();

    // Properties
    private DbsGroup summary_Total_Data;
    private DbsGroup summary_Total_Data_Pnd_Summary_Totals;
    private DbsField summary_Total_Data_Pnd_Inv_Acct;
    private DbsField summary_Total_Data_Pnd_Ledger_Number;
    private DbsField summary_Total_Data_Pnd_Ledger_Amt;
    private DbsGroup cs_Ledger_Table;
    private DbsField cs_Ledger_Table_Pnd_Ledger_Code;
    private DbsField cs_Ledger_Table_Pnd_Ledger_Desc;
    private DbsGroup cs_State_Tax_Ledger_Table;
    private DbsField cs_State_Tax_Ledger_Table_Pnd_State_Code_N2;
    private DbsField cs_State_Tax_Ledger_Table_Pnd_State_Code_A2;
    private DbsField cs_State_Tax_Ledger_Table_Pnd_State_Name;
    private DbsField cs_State_Tax_Ledger_Table_Pnd_State_Ledger_Code;
    private DbsField cs_State_Tax_Ledger_Table_Pnd_State_Ledger_Amt;

    public DbsGroup getSummary_Total_Data() { return summary_Total_Data; }

    public DbsGroup getSummary_Total_Data_Pnd_Summary_Totals() { return summary_Total_Data_Pnd_Summary_Totals; }

    public DbsField getSummary_Total_Data_Pnd_Inv_Acct() { return summary_Total_Data_Pnd_Inv_Acct; }

    public DbsField getSummary_Total_Data_Pnd_Ledger_Number() { return summary_Total_Data_Pnd_Ledger_Number; }

    public DbsField getSummary_Total_Data_Pnd_Ledger_Amt() { return summary_Total_Data_Pnd_Ledger_Amt; }

    public DbsGroup getCs_Ledger_Table() { return cs_Ledger_Table; }

    public DbsField getCs_Ledger_Table_Pnd_Ledger_Code() { return cs_Ledger_Table_Pnd_Ledger_Code; }

    public DbsField getCs_Ledger_Table_Pnd_Ledger_Desc() { return cs_Ledger_Table_Pnd_Ledger_Desc; }

    public DbsGroup getCs_State_Tax_Ledger_Table() { return cs_State_Tax_Ledger_Table; }

    public DbsField getCs_State_Tax_Ledger_Table_Pnd_State_Code_N2() { return cs_State_Tax_Ledger_Table_Pnd_State_Code_N2; }

    public DbsField getCs_State_Tax_Ledger_Table_Pnd_State_Code_A2() { return cs_State_Tax_Ledger_Table_Pnd_State_Code_A2; }

    public DbsField getCs_State_Tax_Ledger_Table_Pnd_State_Name() { return cs_State_Tax_Ledger_Table_Pnd_State_Name; }

    public DbsField getCs_State_Tax_Ledger_Table_Pnd_State_Ledger_Code() { return cs_State_Tax_Ledger_Table_Pnd_State_Ledger_Code; }

    public DbsField getCs_State_Tax_Ledger_Table_Pnd_State_Ledger_Amt() { return cs_State_Tax_Ledger_Table_Pnd_State_Ledger_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        summary_Total_Data = newGroupInRecord("summary_Total_Data", "SUMMARY-TOTAL-DATA");
        summary_Total_Data_Pnd_Summary_Totals = summary_Total_Data.newGroupArrayInGroup("summary_Total_Data_Pnd_Summary_Totals", "#SUMMARY-TOTALS", new 
            DbsArrayController(1,20));
        summary_Total_Data_Pnd_Inv_Acct = summary_Total_Data_Pnd_Summary_Totals.newFieldInGroup("summary_Total_Data_Pnd_Inv_Acct", "#INV-ACCT", FieldType.STRING, 
            8);
        summary_Total_Data_Pnd_Ledger_Number = summary_Total_Data_Pnd_Summary_Totals.newFieldArrayInGroup("summary_Total_Data_Pnd_Ledger_Number", "#LEDGER-NUMBER", 
            FieldType.STRING, 12, new DbsArrayController(1,50));
        summary_Total_Data_Pnd_Ledger_Amt = summary_Total_Data_Pnd_Summary_Totals.newFieldArrayInGroup("summary_Total_Data_Pnd_Ledger_Amt", "#LEDGER-AMT", 
            FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,50));

        cs_Ledger_Table = newGroupInRecord("cs_Ledger_Table", "CS-LEDGER-TABLE");
        cs_Ledger_Table_Pnd_Ledger_Code = cs_Ledger_Table.newFieldArrayInGroup("cs_Ledger_Table_Pnd_Ledger_Code", "#LEDGER-CODE", FieldType.STRING, 12, 
            new DbsArrayController(1,85));
        cs_Ledger_Table_Pnd_Ledger_Desc = cs_Ledger_Table.newFieldArrayInGroup("cs_Ledger_Table_Pnd_Ledger_Desc", "#LEDGER-DESC", FieldType.STRING, 50, 
            new DbsArrayController(1,85));

        cs_State_Tax_Ledger_Table = newGroupInRecord("cs_State_Tax_Ledger_Table", "CS-STATE-TAX-LEDGER-TABLE");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2 = cs_State_Tax_Ledger_Table.newFieldArrayInGroup("cs_State_Tax_Ledger_Table_Pnd_State_Code_N2", "#STATE-CODE-N2", 
            FieldType.NUMERIC, 2, new DbsArrayController(1,60));
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2 = cs_State_Tax_Ledger_Table.newFieldArrayInGroup("cs_State_Tax_Ledger_Table_Pnd_State_Code_A2", "#STATE-CODE-A2", 
            FieldType.STRING, 2, new DbsArrayController(1,60));
        cs_State_Tax_Ledger_Table_Pnd_State_Name = cs_State_Tax_Ledger_Table.newFieldArrayInGroup("cs_State_Tax_Ledger_Table_Pnd_State_Name", "#STATE-NAME", 
            FieldType.STRING, 20, new DbsArrayController(1,60));
        cs_State_Tax_Ledger_Table_Pnd_State_Ledger_Code = cs_State_Tax_Ledger_Table.newFieldArrayInGroup("cs_State_Tax_Ledger_Table_Pnd_State_Ledger_Code", 
            "#STATE-LEDGER-CODE", FieldType.STRING, 12, new DbsArrayController(1,60));
        cs_State_Tax_Ledger_Table_Pnd_State_Ledger_Amt = cs_State_Tax_Ledger_Table.newFieldArrayInGroup("cs_State_Tax_Ledger_Table_Pnd_State_Ledger_Amt", 
            "#STATE-LEDGER-AMT", FieldType.PACKED_DECIMAL, 11,2, new DbsArrayController(1,60));

        this.setRecordName("GdaFcpg230b");
    }
    public void initializeValues() throws Exception
    {
        reset();

        summary_Total_Data_Pnd_Ledger_Number.getValue(1,1).setInitialValue("06048010");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,2).setInitialValue("15010");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,3).setInitialValue("185010");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,4).setInitialValue("15224");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,5).setInitialValue("00000");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,6).setInitialValue("15025");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,7).setInitialValue("185025");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,8).setInitialValue("15610");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,9).setInitialValue("185610");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,10).setInitialValue("15625");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,11).setInitialValue("185625");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,12).setInitialValue("16080");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,13).setInitialValue("16079");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,14).setInitialValue("16084");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,15).setInitialValue("D-CREF-LTC");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,16).setInitialValue("D-TIAA-OTHER");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,17).setInitialValue("D-CREF-OTHER");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,18).setInitialValue("D-TIAA-EXP");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,19).setInitialValue("D-CREF-EXP");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,20).setInitialValue("61055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,21).setInitialValue("61056");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,22).setInitialValue("61057");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,23).setInitialValue("61058");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,24).setInitialValue("62555");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,25).setInitialValue("62556");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,26).setInitialValue("62055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,27).setInitialValue("62056");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,28).setInitialValue("62057");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,29).setInitialValue("62058");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,30).setInitialValue("64163");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,31).setInitialValue("64264");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,32).setInitialValue("61555");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,33).setInitialValue("61556");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,34).setInitialValue("C-OTHER-TIAA");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,35).setInitialValue("C-OTHER-DIV");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,36).setInitialValue("16081");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,37).setInitialValue("163232");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,38).setInitialValue("16077");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,39).setInitialValue("163222");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,40).setInitialValue("163242");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,41).setInitialValue("163252");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,42).setInitialValue("163262");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,43).setInitialValue("163272");
        summary_Total_Data_Pnd_Ledger_Number.getValue(1,44).setInitialValue("163112");
        summary_Total_Data_Pnd_Ledger_Number.getValue(2,1).setInitialValue("R043102");
        summary_Total_Data_Pnd_Ledger_Number.getValue(2,2).setInitialValue("R61055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(2,3).setInitialValue("C-OTHER");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,1).setInitialValue("804052");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,2).setInitialValue("862055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,3).setInitialValue("861055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,4).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,5).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,6).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,7).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,8).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,9).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,10).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,11).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,12).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,13).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,14).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,15).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,16).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,17).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,18).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,19).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,20).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,21).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,22).setInitialValue("C-INSTALL-DT");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,23).setInitialValue("862555");
        summary_Total_Data_Pnd_Ledger_Number.getValue(3,24).setInitialValue("C-OTHER");
        summary_Total_Data_Pnd_Ledger_Number.getValue(4,1).setInitialValue("904052");
        summary_Total_Data_Pnd_Ledger_Number.getValue(4,2).setInitialValue("962055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(4,3).setInitialValue("962555");
        summary_Total_Data_Pnd_Ledger_Number.getValue(4,4).setInitialValue("961055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(4,5).setInitialValue("C-OTHER");
        summary_Total_Data_Pnd_Ledger_Number.getValue(5,1).setInitialValue("A043102");
        summary_Total_Data_Pnd_Ledger_Number.getValue(5,2).setInitialValue("A61055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(5,3).setInitialValue("A62055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(5,4).setInitialValue("A62555");
        summary_Total_Data_Pnd_Ledger_Number.getValue(5,5).setInitialValue("C-OTHER");
        summary_Total_Data_Pnd_Ledger_Number.getValue(6,1).setInitialValue("B043102");
        summary_Total_Data_Pnd_Ledger_Number.getValue(6,2).setInitialValue("B62055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(6,3).setInitialValue("B62555");
        summary_Total_Data_Pnd_Ledger_Number.getValue(6,4).setInitialValue("B61055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(6,5).setInitialValue("C-OTHER");
        summary_Total_Data_Pnd_Ledger_Number.getValue(7,1).setInitialValue("C043102");
        summary_Total_Data_Pnd_Ledger_Number.getValue(7,2).setInitialValue("C61055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(7,3).setInitialValue("C62055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(7,4).setInitialValue("C62555");
        summary_Total_Data_Pnd_Ledger_Number.getValue(7,5).setInitialValue("C-OTHER");
        summary_Total_Data_Pnd_Ledger_Number.getValue(8,1).setInitialValue("D043102");
        summary_Total_Data_Pnd_Ledger_Number.getValue(8,2).setInitialValue("D62055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(8,3).setInitialValue("D61055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(8,4).setInitialValue("D62555");
        summary_Total_Data_Pnd_Ledger_Number.getValue(8,5).setInitialValue("C-OTHER");
        summary_Total_Data_Pnd_Ledger_Number.getValue(9,1).setInitialValue("E043102");
        summary_Total_Data_Pnd_Ledger_Number.getValue(9,2).setInitialValue("E61055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(9,3).setInitialValue("E62055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(9,4).setInitialValue("E62555");
        summary_Total_Data_Pnd_Ledger_Number.getValue(9,5).setInitialValue("C-OTHER");
        summary_Total_Data_Pnd_Ledger_Number.getValue(10,1).setInitialValue("F043102");
        summary_Total_Data_Pnd_Ledger_Number.getValue(10,2).setInitialValue("F62055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(10,3).setInitialValue("F62555");
        summary_Total_Data_Pnd_Ledger_Number.getValue(10,4).setInitialValue("F61055");
        summary_Total_Data_Pnd_Ledger_Number.getValue(10,5).setInitialValue("C-OTHER");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(1).setInitialValue("06048010");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(2).setInitialValue("15010");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(3).setInitialValue("185010");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(4).setInitialValue("15224");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(5).setInitialValue("00000");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(6).setInitialValue("15025");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(7).setInitialValue("185025");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(8).setInitialValue("15610");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(9).setInitialValue("185610");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(10).setInitialValue("15625");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(11).setInitialValue("185625");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(12).setInitialValue("16080");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(13).setInitialValue("16079");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(14).setInitialValue("16084");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(15).setInitialValue("61055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(16).setInitialValue("61056");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(17).setInitialValue("61057");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(18).setInitialValue("61058");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(19).setInitialValue("62555");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(20).setInitialValue("62556");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(21).setInitialValue("62055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(22).setInitialValue("62056");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(23).setInitialValue("62057");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(24).setInitialValue("62058");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(25).setInitialValue("64163");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(26).setInitialValue("64264");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(27).setInitialValue("61555");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(28).setInitialValue("61556");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(29).setInitialValue("16081");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(30).setInitialValue("163232");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(31).setInitialValue("16077");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(32).setInitialValue("163222");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(33).setInitialValue("163242");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(34).setInitialValue("163252");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(35).setInitialValue("163262");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(36).setInitialValue("163272");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(37).setInitialValue("163112");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(38).setInitialValue("D-OTHER");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(39).setInitialValue("C-OTHER");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(40).setInitialValue("D-CREF-LTC");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(41).setInitialValue("D-TIAA-OTHER");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(42).setInitialValue("D-CREF-OTHER");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(43).setInitialValue("D-TIAA-EXP");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(44).setInitialValue("D-CREF-EXP");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(45).setInitialValue("C-OTHER-TIAA");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(46).setInitialValue("C-OTHER-DIV");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(47).setInitialValue("R043102");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(48).setInitialValue("R61055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(49).setInitialValue("804052");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(50).setInitialValue("862055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(51).setInitialValue("862555");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(52).setInitialValue("861055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(53).setInitialValue("904052");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(54).setInitialValue("962055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(55).setInitialValue("962555");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(56).setInitialValue("961055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(57).setInitialValue("A043102");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(58).setInitialValue("A61055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(59).setInitialValue("A62055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(60).setInitialValue("A62555");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(61).setInitialValue("B043102");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(62).setInitialValue("B62055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(63).setInitialValue("B62555");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(64).setInitialValue("B61055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(65).setInitialValue("C043102");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(66).setInitialValue("C61055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(67).setInitialValue("C62055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(68).setInitialValue("C62555");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(69).setInitialValue("D043102");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(70).setInitialValue("D62055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(71).setInitialValue("D61055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(72).setInitialValue("D62555");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(73).setInitialValue("E043102");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(74).setInitialValue("E61055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(75).setInitialValue("E62055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(76).setInitialValue("E62555");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(77).setInitialValue("F043102");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(78).setInitialValue("F62055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(79).setInitialValue("F62555");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(80).setInitialValue("F61055");
        cs_Ledger_Table_Pnd_Ledger_Code.getValue(81).setInitialValue("C-INSTALL-DT");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(1).setInitialValue("DEBIT: Cash FIRST UNION CPS");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(2).setInitialValue("DEBIT: Taxes - Federal - TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(3).setInitialValue("DEBIT: Taxes - Federal - CREF");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(4).setInitialValue("DEBIT: State Taxes - TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(5).setInitialValue("DEBIT: State Taxes - CREF");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(6).setInitialValue("DEBIT: Taxes - U.S. NRA - TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(7).setInitialValue("DEBIT: Taxes - U.S. NRA - CREF");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(8).setInitialValue("DEBIT: Taxes - Canadian Federal - TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(9).setInitialValue("DEBIT: Taxes - Canadian Federal - CREF");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(10).setInitialValue("DEBIT: Taxes - Canadian NRA - TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(11).setInitialValue("DEBIT: Taxes - Canadian NRA - CREF");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(12).setInitialValue("DEBIT: Insurance Deductions - TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(13).setInitialValue("DEBIT: Insurance Deductions - CREF");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(14).setInitialValue("DEBIT: Long Term Care Deductions - TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(15).setInitialValue("CREDIT: IA, IB, IC, ID, IE, IF Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(16).setInitialValue("CREDIT: Div. on IA, IB, IC, ID, IE, IF Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(17).setInitialValue("CREDIT: GA Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(18).setInitialValue("CREDIT: Div. on GA Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(19).setInitialValue("CREDIT: S0 Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(20).setInitialValue("CREDIT: Div. on S0 Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(21).setInitialValue("CREDIT: W Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(22).setInitialValue("CREDIT: Div. on W Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(23).setInitialValue("CREDIT: GW Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(24).setInitialValue("CREDIT: Div. on GW Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(25).setInitialValue("CREDIT: W-GRP Ann-Par Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(26).setInitialValue("CREDIT: W-GRP Ann-Non-Par Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(27).setInitialValue("CREDIT: IPRO Periodic Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(28).setInitialValue("CREDIT: Div. on IPRO Periodic Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(29).setInitialValue("CREDIT: Liability Due STOCK");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(30).setInitialValue("CREDIT: Liability Due BOND");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(31).setInitialValue("CREDIT: Liability Due MMA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(32).setInitialValue("CREDIT: Liability Due SOCIAL CHOICE");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(33).setInitialValue("CREDIT: Liability Due GLOBAL");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(34).setInitialValue("CREDIT: Liability Due GROWTH");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(35).setInitialValue("CREDIT: Liability Due EQUITY");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(36).setInitialValue("CREDIT: Liability Due ILB");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(37).setInitialValue("CREDIT: Liability Due REAL ESTATE - Manual");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(38).setInitialValue("DEBIT: Other Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(39).setInitialValue("CREDIT: Other Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(40).setInitialValue("DEBIT: Long Term Care Deductions - CREF");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(41).setInitialValue("DEBIT: Other Deductions - TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(42).setInitialValue("DEBIT: Other Deductions - CREF");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(43).setInitialValue("DEBIT: Expense Charges - TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(44).setInitialValue("DEBIT: Expense Charges - CREF");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(45).setInitialValue("CREDIT: Other TIAA Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(46).setInitialValue("CREDIT: Other TIAA Dividends");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(47).setInitialValue("DEBIT: Rec. Due from TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(48).setInitialValue("CREDIT: 6M Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(49).setInitialValue("DEBIT: Rec. Due from TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(50).setInitialValue("CREDIT: L Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(51).setInitialValue("CREDIT: N Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(52).setInitialValue("CREDIT: M, T, U, Z Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(53).setInitialValue("DEBIT: Rec. Due from TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(54).setInitialValue("CREDIT: Ben. Pay. Suppl. Contr. Inv. (L)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(55).setInitialValue("CREDIT: Ben. Pay. Suppl. Contr. Not Inv. (N)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(56).setInitialValue("CREDIT: M Payments");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(57).setInitialValue("DEBIT: Rec. Due from TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(58).setInitialValue("CREDIT: Benefit Payments (M)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(59).setInitialValue("CREDIT: Benefit Payments SCI (L)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(60).setInitialValue("CREDIT: Benefit Payments SCNI (N)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(61).setInitialValue("DEBIT: Rec. Due from TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(62).setInitialValue("CREDIT: Benefit Payments (L)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(63).setInitialValue("CREDIT: Benefit Payments (N)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(64).setInitialValue("CREDIT: Benefit Payments (M)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(65).setInitialValue("DEBIT: Rec. Due from TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(66).setInitialValue("CREDIT: Benefit Payments (M)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(67).setInitialValue("CREDIT: Benefit Payments SCI (L)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(68).setInitialValue("CREDIT: Benefit Payments SCNI (N)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(69).setInitialValue("DEBIT: Rec. Due from TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(70).setInitialValue("CREDIT: Benefit Payments (L)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(71).setInitialValue("CREDIT: Benefit Payments (M)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(72).setInitialValue("CREDIT: Benefit Payments (N)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(73).setInitialValue("DEBIT: Rec. Due from TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(74).setInitialValue("CREDIT: Benefit Payments (M)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(75).setInitialValue("CREDIT: Benefit Payments SCI (L)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(76).setInitialValue("CREDIT: Benefit Payments SCNI (N)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(77).setInitialValue("DEBIT: Rec. Due from TIAA");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(78).setInitialValue("CREDIT: Benefit Payments (L)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(79).setInitialValue("CREDIT: Benefit Payments (N)");
        cs_Ledger_Table_Pnd_Ledger_Desc.getValue(80).setInitialValue("CREDIT: Benefit Payments (M)");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(1).setInitialValue(1);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(2).setInitialValue(2);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(3).setInitialValue(4);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(4).setInitialValue(3);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(5).setInitialValue(5);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(6).setInitialValue(7);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(7).setInitialValue(8);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(8).setInitialValue(9);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(9).setInitialValue(10);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(10).setInitialValue(11);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(11).setInitialValue(12);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(12).setInitialValue(14);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(13).setInitialValue(15);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(14).setInitialValue(16);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(15).setInitialValue(17);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(16).setInitialValue(18);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(17).setInitialValue(19);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(18).setInitialValue(20);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(19).setInitialValue(21);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(20).setInitialValue(22);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(21).setInitialValue(23);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(22).setInitialValue(24);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(23).setInitialValue(25);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(24).setInitialValue(26);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(25).setInitialValue(27);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(26).setInitialValue(28);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(27).setInitialValue(29);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(28).setInitialValue(30);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(29).setInitialValue(31);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(30).setInitialValue(32);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(31).setInitialValue(33);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(32).setInitialValue(34);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(33).setInitialValue(35);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(34).setInitialValue(36);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(35).setInitialValue(37);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(36).setInitialValue(38);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(37).setInitialValue(39);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(38).setInitialValue(40);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(39).setInitialValue(41);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(40).setInitialValue(42);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(41).setInitialValue(43);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(42).setInitialValue(45);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(43).setInitialValue(46);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(44).setInitialValue(47);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(45).setInitialValue(48);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(46).setInitialValue(49);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(47).setInitialValue(50);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(48).setInitialValue(51);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(49).setInitialValue(52);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(50).setInitialValue(54);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(51).setInitialValue(55);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(52).setInitialValue(56);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(53).setInitialValue(57);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_N2.getValue(54).setInitialValue(0);
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(1).setInitialValue("AL");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(2).setInitialValue("AK");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(3).setInitialValue("AR");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(4).setInitialValue("AZ");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(5).setInitialValue("CA");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(6).setInitialValue("CO");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(7).setInitialValue("CT");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(8).setInitialValue("DE");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(9).setInitialValue("DC");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(10).setInitialValue("FL");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(11).setInitialValue("GA");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(12).setInitialValue("HI");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(13).setInitialValue("ID");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(14).setInitialValue("IL");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(15).setInitialValue("IN");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(16).setInitialValue("IA");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(17).setInitialValue("IA");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(18).setInitialValue("KS");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(19).setInitialValue("KY");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(20).setInitialValue("LA");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(21).setInitialValue("ME");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(22).setInitialValue("MD");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(23).setInitialValue("MA");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(24).setInitialValue("MI");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(25).setInitialValue("MN");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(26).setInitialValue("MS");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(27).setInitialValue("MO");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(28).setInitialValue("MT");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(29).setInitialValue("NE");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(30).setInitialValue("NV");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(31).setInitialValue("NH");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(32).setInitialValue("NJ");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(33).setInitialValue("NM");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(34).setInitialValue("NY");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(35).setInitialValue("NC");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(36).setInitialValue("ND");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(37).setInitialValue("OH");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(38).setInitialValue("OK");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(39).setInitialValue("OR");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(40).setInitialValue("PA");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(41).setInitialValue("PR");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(42).setInitialValue("RI");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(43).setInitialValue("SC");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(44).setInitialValue("SD");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(45).setInitialValue("TN");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(46).setInitialValue("TX");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(47).setInitialValue("UT");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(48).setInitialValue("VT");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(49).setInitialValue("VA");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(50).setInitialValue("VI");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(51).setInitialValue("WA");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(52).setInitialValue("WV");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(53).setInitialValue("WI");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(54).setInitialValue("WY");
        cs_State_Tax_Ledger_Table_Pnd_State_Code_A2.getValue(55).setInitialValue("XX");
    }

    // Constructor
    private GdaFcpg230b() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }

    // Instance Property
    public static GdaFcpg230b getInstance(int callnatLevel) throws Exception
    {
        if (_instance.get() == null)
            _instance.set(new ArrayList<GdaFcpg230b>());

        if (_instance.get().size() < callnatLevel)
        {
            while (_instance.get().size() < callnatLevel)
            {
                _instance.get().add(new GdaFcpg230b());
            }
        }
        else if (_instance.get().size() > callnatLevel)
        {
            while(_instance.get().size() > callnatLevel)
            _instance.get().remove(_instance.get().size() - 1);
        }

        return _instance.get().get(callnatLevel - 1);
    }
}

