/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:53:41 PM
**        * FROM NATURAL LDA     : CPULANN2
************************************************************
**        * FILE NAME            : LdaCpulann2.java
**        * CLASS NAME           : LdaCpulann2
**        * INSTANCE NAME        : LdaCpulann2
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaCpulann2 extends DbsRecord
{
    // Properties
    private DbsField annot2_Record;
    private DbsGroup annot2_RecordRedef1;
    private DbsField annot2_Record_Legacy_Eft_Key;
    private DbsGroup annot2_Record_Legacy_Eft_KeyRedef2;
    private DbsField annot2_Record_Key_Contract;
    private DbsField annot2_Record_Key_Payee;
    private DbsField annot2_Record_Key_Origin;
    private DbsField annot2_Record_Key_Inverse_Date;
    private DbsField annot2_Record_Key_Process_Seq;
    private DbsGroup annot2_Record_Key_Process_SeqRedef3;
    private DbsField annot2_Record_Pnd_Seq_A;
    private DbsField annot2_Record_Sequence;
    private DbsField annot2_Record_Annot_Expand_Cmnt;
    private DbsField annot2_Record_Annot_Expand_Date;
    private DbsField annot2_Record_Annot_Expand_Time;
    private DbsField annot2_Record_Annot_Expand_Uid;
    private DbsField filler01;
    private DbsField annot2_Record_Check_Number;
    private DbsGroup annot2_Record_Check_NumberRedef4;
    private DbsField annot2_Record_Check_Alpha;

    public DbsField getAnnot2_Record() { return annot2_Record; }

    public DbsGroup getAnnot2_RecordRedef1() { return annot2_RecordRedef1; }

    public DbsField getAnnot2_Record_Legacy_Eft_Key() { return annot2_Record_Legacy_Eft_Key; }

    public DbsGroup getAnnot2_Record_Legacy_Eft_KeyRedef2() { return annot2_Record_Legacy_Eft_KeyRedef2; }

    public DbsField getAnnot2_Record_Key_Contract() { return annot2_Record_Key_Contract; }

    public DbsField getAnnot2_Record_Key_Payee() { return annot2_Record_Key_Payee; }

    public DbsField getAnnot2_Record_Key_Origin() { return annot2_Record_Key_Origin; }

    public DbsField getAnnot2_Record_Key_Inverse_Date() { return annot2_Record_Key_Inverse_Date; }

    public DbsField getAnnot2_Record_Key_Process_Seq() { return annot2_Record_Key_Process_Seq; }

    public DbsGroup getAnnot2_Record_Key_Process_SeqRedef3() { return annot2_Record_Key_Process_SeqRedef3; }

    public DbsField getAnnot2_Record_Pnd_Seq_A() { return annot2_Record_Pnd_Seq_A; }

    public DbsField getAnnot2_Record_Sequence() { return annot2_Record_Sequence; }

    public DbsField getAnnot2_Record_Annot_Expand_Cmnt() { return annot2_Record_Annot_Expand_Cmnt; }

    public DbsField getAnnot2_Record_Annot_Expand_Date() { return annot2_Record_Annot_Expand_Date; }

    public DbsField getAnnot2_Record_Annot_Expand_Time() { return annot2_Record_Annot_Expand_Time; }

    public DbsField getAnnot2_Record_Annot_Expand_Uid() { return annot2_Record_Annot_Expand_Uid; }

    public DbsField getFiller01() { return filler01; }

    public DbsField getAnnot2_Record_Check_Number() { return annot2_Record_Check_Number; }

    public DbsGroup getAnnot2_Record_Check_NumberRedef4() { return annot2_Record_Check_NumberRedef4; }

    public DbsField getAnnot2_Record_Check_Alpha() { return annot2_Record_Check_Alpha; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        annot2_Record = newFieldInRecord("annot2_Record", "ANNOT2-RECORD", FieldType.STRING, 200);
        annot2_RecordRedef1 = newGroupInRecord("annot2_RecordRedef1", "Redefines", annot2_Record);
        annot2_Record_Legacy_Eft_Key = annot2_RecordRedef1.newFieldInGroup("annot2_Record_Legacy_Eft_Key", "LEGACY-EFT-KEY", FieldType.STRING, 33);
        annot2_Record_Legacy_Eft_KeyRedef2 = annot2_RecordRedef1.newGroupInGroup("annot2_Record_Legacy_Eft_KeyRedef2", "Redefines", annot2_Record_Legacy_Eft_Key);
        annot2_Record_Key_Contract = annot2_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("annot2_Record_Key_Contract", "KEY-CONTRACT", FieldType.STRING, 
            10);
        annot2_Record_Key_Payee = annot2_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("annot2_Record_Key_Payee", "KEY-PAYEE", FieldType.STRING, 4);
        annot2_Record_Key_Origin = annot2_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("annot2_Record_Key_Origin", "KEY-ORIGIN", FieldType.STRING, 2);
        annot2_Record_Key_Inverse_Date = annot2_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("annot2_Record_Key_Inverse_Date", "KEY-INVERSE-DATE", FieldType.NUMERIC, 
            8);
        annot2_Record_Key_Process_Seq = annot2_Record_Legacy_Eft_KeyRedef2.newFieldInGroup("annot2_Record_Key_Process_Seq", "KEY-PROCESS-SEQ", FieldType.NUMERIC, 
            9);
        annot2_Record_Key_Process_SeqRedef3 = annot2_Record_Legacy_Eft_KeyRedef2.newGroupInGroup("annot2_Record_Key_Process_SeqRedef3", "Redefines", annot2_Record_Key_Process_Seq);
        annot2_Record_Pnd_Seq_A = annot2_Record_Key_Process_SeqRedef3.newFieldInGroup("annot2_Record_Pnd_Seq_A", "#SEQ-A", FieldType.STRING, 7);
        annot2_Record_Sequence = annot2_RecordRedef1.newFieldInGroup("annot2_Record_Sequence", "SEQUENCE", FieldType.NUMERIC, 2);
        annot2_Record_Annot_Expand_Cmnt = annot2_RecordRedef1.newFieldInGroup("annot2_Record_Annot_Expand_Cmnt", "ANNOT-EXPAND-CMNT", FieldType.STRING, 
            50);
        annot2_Record_Annot_Expand_Date = annot2_RecordRedef1.newFieldInGroup("annot2_Record_Annot_Expand_Date", "ANNOT-EXPAND-DATE", FieldType.STRING, 
            8);
        annot2_Record_Annot_Expand_Time = annot2_RecordRedef1.newFieldInGroup("annot2_Record_Annot_Expand_Time", "ANNOT-EXPAND-TIME", FieldType.STRING, 
            4);
        annot2_Record_Annot_Expand_Uid = annot2_RecordRedef1.newFieldInGroup("annot2_Record_Annot_Expand_Uid", "ANNOT-EXPAND-UID", FieldType.STRING, 8);
        filler01 = annot2_RecordRedef1.newFieldInGroup("filler01", "FILLER", FieldType.STRING, 85);
        annot2_Record_Check_Number = annot2_RecordRedef1.newFieldInGroup("annot2_Record_Check_Number", "CHECK-NUMBER", FieldType.NUMERIC, 10);
        annot2_Record_Check_NumberRedef4 = annot2_RecordRedef1.newGroupInGroup("annot2_Record_Check_NumberRedef4", "Redefines", annot2_Record_Check_Number);
        annot2_Record_Check_Alpha = annot2_Record_Check_NumberRedef4.newFieldInGroup("annot2_Record_Check_Alpha", "CHECK-ALPHA", FieldType.STRING, 10);

        this.setRecordName("LdaCpulann2");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaCpulann2() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
