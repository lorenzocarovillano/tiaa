/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:05 PM
**        *   FROM NATURAL MAP   :  Fcpm872i
************************************************************
**        * FILE NAME               : Fcpm872i.java
**        * CLASS NAME              : Fcpm872i
**        * INSTANCE NAME           : Fcpm872i
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #KEY-EFM.CNTRCT-CANCEL-RDRW-ACTVTY-CDE #KEY-EFM.CNTRCT-ORGN-CDE                                                          *     #KEY-EFM.CNTRCT-UNQ-ID-NBR 
    #KEY-EFM.PYMNT-REQST-LOG-DTE-TIME                                                             *     #PMNT-TAX.TAX-FED-FILING-STAT-DESC              
    *     #PMNT-TAX.TAX-STA-FILING-STAT-DESC #SCREEN-HEADER                                                                        *     #WS.#FUND-DISPLAY 
    #WS.#IVC-IND #WS.#PAGE-NUMBER                                                                          *     EXT.PYMNT-TAX-FORM-1001 EXT.PYMNT-TAX-FORM-1078 
    *     PYMNT-TAX.CNTRCT-NBR PYMNT-TAX.TAX-FED-ALLOW-CNT                                                                         *     PYMNT-TAX.TAX-FED-ELEC-OPTION 
    PYMNT-TAX.TAX-FED-FIXED-AMT                                                                *     PYMNT-TAX.TAX-FED-FIXED-PCT PYMNT-TAX.TAX-LOC-ALLOW-CNT 
    *     PYMNT-TAX.TAX-LOC-BLIND-CNT PYMNT-TAX.TAX-LOC-ELEC-OPTION                                                                *     PYMNT-TAX.TAX-LOC-FIXED-AMT 
    PYMNT-TAX.TAX-LOC-FIXED-PCT                                                                  *     PYMNT-TAX.TAX-LOC-SPOUSE-CNT PYMNT-TAX.TAX-STA-ALLOW-CNT 
    *     PYMNT-TAX.TAX-STA-BLIND-CNT PYMNT-TAX.TAX-STA-ELEC-OPTION                                                                *     PYMNT-TAX.TAX-STA-FIXED-AMT 
    PYMNT-TAX.TAX-STA-FIXED-PCT                                                                  *     PYMNT-TAX.TAX-STA-SPOUSE-CNT
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm872i extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Key_Efm_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Efm_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc;
    private DbsField pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc;
    private DbsField pnd_Screen_Header;
    private DbsField pnd_Ws_Pnd_Fund_Display;
    private DbsField pnd_Ws_Pnd_Ivc_Ind;
    private DbsField pnd_Ws_Pnd_Page_Number;
    private DbsField ext_Pymnt_Tax_Form_1001;
    private DbsField ext_Pymnt_Tax_Form_1078;
    private DbsField pymnt_Tax_Cntrct_Nbr;
    private DbsField pymnt_Tax_Tax_Fed_Allow_Cnt;
    private DbsField pymnt_Tax_Tax_Fed_Elec_Option;
    private DbsField pymnt_Tax_Tax_Fed_Fixed_Amt;
    private DbsField pymnt_Tax_Tax_Fed_Fixed_Pct;
    private DbsField pymnt_Tax_Tax_Loc_Allow_Cnt;
    private DbsField pymnt_Tax_Tax_Loc_Blind_Cnt;
    private DbsField pymnt_Tax_Tax_Loc_Elec_Option;
    private DbsField pymnt_Tax_Tax_Loc_Fixed_Amt;
    private DbsField pymnt_Tax_Tax_Loc_Fixed_Pct;
    private DbsField pymnt_Tax_Tax_Loc_Spouse_Cnt;
    private DbsField pymnt_Tax_Tax_Sta_Allow_Cnt;
    private DbsField pymnt_Tax_Tax_Sta_Blind_Cnt;
    private DbsField pymnt_Tax_Tax_Sta_Elec_Option;
    private DbsField pymnt_Tax_Tax_Sta_Fixed_Amt;
    private DbsField pymnt_Tax_Tax_Sta_Fixed_Pct;
    private DbsField pymnt_Tax_Tax_Sta_Spouse_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde", "#KEY-EFM.CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Key_Efm_Cntrct_Orgn_Cde = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Orgn_Cde", "#KEY-EFM.CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Efm_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Unq_Id_Nbr", "#KEY-EFM.CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time = parameters.newFieldInRecord("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time", "#KEY-EFM.PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc = parameters.newFieldInRecord("pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc", "#PMNT-TAX.TAX-FED-FILING-STAT-DESC", 
            FieldType.STRING, 3);
        pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc = parameters.newFieldInRecord("pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc", "#PMNT-TAX.TAX-STA-FILING-STAT-DESC", 
            FieldType.STRING, 3);
        pnd_Screen_Header = parameters.newFieldInRecord("pnd_Screen_Header", "#SCREEN-HEADER", FieldType.STRING, 35);
        pnd_Ws_Pnd_Fund_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Fund_Display", "#WS.#FUND-DISPLAY", FieldType.STRING, 60);
        pnd_Ws_Pnd_Ivc_Ind = parameters.newFieldInRecord("pnd_Ws_Pnd_Ivc_Ind", "#WS.#IVC-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Page_Number = parameters.newFieldInRecord("pnd_Ws_Pnd_Page_Number", "#WS.#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        ext_Pymnt_Tax_Form_1001 = parameters.newFieldInRecord("ext_Pymnt_Tax_Form_1001", "EXT.PYMNT-TAX-FORM-1001", FieldType.STRING, 1);
        ext_Pymnt_Tax_Form_1078 = parameters.newFieldInRecord("ext_Pymnt_Tax_Form_1078", "EXT.PYMNT-TAX-FORM-1078", FieldType.STRING, 1);
        pymnt_Tax_Cntrct_Nbr = parameters.newFieldInRecord("pymnt_Tax_Cntrct_Nbr", "PYMNT-TAX.CNTRCT-NBR", FieldType.STRING, 10);
        pymnt_Tax_Tax_Fed_Allow_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Fed_Allow_Cnt", "PYMNT-TAX.TAX-FED-ALLOW-CNT", FieldType.NUMERIC, 2);
        pymnt_Tax_Tax_Fed_Elec_Option = parameters.newFieldInRecord("pymnt_Tax_Tax_Fed_Elec_Option", "PYMNT-TAX.TAX-FED-ELEC-OPTION", FieldType.STRING, 
            1);
        pymnt_Tax_Tax_Fed_Fixed_Amt = parameters.newFieldInRecord("pymnt_Tax_Tax_Fed_Fixed_Amt", "PYMNT-TAX.TAX-FED-FIXED-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pymnt_Tax_Tax_Fed_Fixed_Pct = parameters.newFieldInRecord("pymnt_Tax_Tax_Fed_Fixed_Pct", "PYMNT-TAX.TAX-FED-FIXED-PCT", FieldType.PACKED_DECIMAL, 
            6, 2);
        pymnt_Tax_Tax_Loc_Allow_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Allow_Cnt", "PYMNT-TAX.TAX-LOC-ALLOW-CNT", FieldType.NUMERIC, 2);
        pymnt_Tax_Tax_Loc_Blind_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Blind_Cnt", "PYMNT-TAX.TAX-LOC-BLIND-CNT", FieldType.NUMERIC, 2);
        pymnt_Tax_Tax_Loc_Elec_Option = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Elec_Option", "PYMNT-TAX.TAX-LOC-ELEC-OPTION", FieldType.STRING, 
            1);
        pymnt_Tax_Tax_Loc_Fixed_Amt = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Fixed_Amt", "PYMNT-TAX.TAX-LOC-FIXED-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pymnt_Tax_Tax_Loc_Fixed_Pct = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Fixed_Pct", "PYMNT-TAX.TAX-LOC-FIXED-PCT", FieldType.PACKED_DECIMAL, 
            6, 2);
        pymnt_Tax_Tax_Loc_Spouse_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Spouse_Cnt", "PYMNT-TAX.TAX-LOC-SPOUSE-CNT", FieldType.NUMERIC, 
            2);
        pymnt_Tax_Tax_Sta_Allow_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Allow_Cnt", "PYMNT-TAX.TAX-STA-ALLOW-CNT", FieldType.NUMERIC, 2);
        pymnt_Tax_Tax_Sta_Blind_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Blind_Cnt", "PYMNT-TAX.TAX-STA-BLIND-CNT", FieldType.NUMERIC, 2);
        pymnt_Tax_Tax_Sta_Elec_Option = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Elec_Option", "PYMNT-TAX.TAX-STA-ELEC-OPTION", FieldType.STRING, 
            1);
        pymnt_Tax_Tax_Sta_Fixed_Amt = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Fixed_Amt", "PYMNT-TAX.TAX-STA-FIXED-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pymnt_Tax_Tax_Sta_Fixed_Pct = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Fixed_Pct", "PYMNT-TAX.TAX-STA-FIXED-PCT", FieldType.PACKED_DECIMAL, 
            6, 2);
        pymnt_Tax_Tax_Sta_Spouse_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Spouse_Cnt", "PYMNT-TAX.TAX-STA-SPOUSE-CNT", FieldType.NUMERIC, 
            2);
        parameters.reset();
    }

    public Fcpm872i() throws Exception
    {
        super("Fcpm872i");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=020 LS=124 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm872i", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm872i"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Screen_Header", pnd_Screen_Header, true, 1, 24, 35, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 1, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 1, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 1, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number", pnd_Ws_Pnd_Page_Number, true, 1, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "01", "BLUE", 1, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 1, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Fund_Display", pnd_Ws_Pnd_Fund_Display, true, 2, 11, 60, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time2", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 2, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde2", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 2, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr2", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 2, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number2", pnd_Ws_Pnd_Page_Number, true, 2, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "02", "BLUE", 2, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde2", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 2, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time3", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 3, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde3", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 3, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr3", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 3, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number3", pnd_Ws_Pnd_Page_Number, true, 3, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "03", "BLUE", 3, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde3", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 3, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "Tax Information", "BLUE", 4, 33, 15);
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time4", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 4, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde4", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 4, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr4", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 4, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number4", pnd_Ws_Pnd_Page_Number, true, 4, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "04", "BLUE", 4, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde4", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 4, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "---------------", "BLUE", 5, 33, 15);
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time5", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 5, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde5", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 5, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr5", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 5, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number5", pnd_Ws_Pnd_Page_Number, true, 5, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "05", "BLUE", 5, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde5", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 5, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "IVC Recovery Type....:", "BLUE", 6, 1, 22);
            uiForm.setUiControl("pnd_Ws_Pnd_Ivc_Ind", pnd_Ws_Pnd_Ivc_Ind, true, 6, 24, 7, "YELLOW", "UNKNOWN/KNOWN", true, false, null, null, "AD=ILOFHW' '~TG=KNOWNUNKNOWN", 
                ' ');
            uiForm.setUiLabel("label_9", "Policy Number:", "BLUE", 6, 51, 14);
            uiForm.setUiControl("pymnt_Tax_Cntrct_Nbr", pymnt_Tax_Cntrct_Nbr, true, 6, 66, 10, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time6", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 6, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde6", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 6, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr6", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 6, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number6", pnd_Ws_Pnd_Page_Number, true, 6, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "06", "BLUE", 6, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde6", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 6, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "Tax Form 1078 Receipt:", "BLUE", 7, 1, 22);
            uiForm.setUiControl("ext_Pymnt_Tax_Form_1078", ext_Pymnt_Tax_Form_1078, true, 7, 24, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time7", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 7, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde7", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 7, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr7", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 7, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number7", pnd_Ws_Pnd_Page_Number, true, 7, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "07", "BLUE", 7, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde7", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 7, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "Tax Form 1001 Receipt:", "BLUE", 8, 1, 22);
            uiForm.setUiControl("ext_Pymnt_Tax_Form_1001", ext_Pymnt_Tax_Form_1001, true, 8, 24, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time8", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 8, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde8", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 8, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr8", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 8, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number8", pnd_Ws_Pnd_Page_Number, true, 8, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "08", "BLUE", 8, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde8", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 8, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "Federal", "BLUE", 9, 32, 7);
            uiForm.setUiLabel("label_16", "State", "BLUE", 9, 50, 5);
            uiForm.setUiLabel("label_17", "Local", "BLUE", 9, 66, 5);
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time9", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 9, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde9", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 9, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr9", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 9, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number9", pnd_Ws_Pnd_Page_Number, true, 9, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "09", "BLUE", 9, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde9", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 9, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "--------------- --------------- ---------------", "BLUE", 10, 29, 47);
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time10", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 10, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde10", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 10, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr10", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 10, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number10", pnd_Ws_Pnd_Page_Number, true, 10, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "10", "BLUE", 10, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde10", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 10, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "Tax Election Response.....:", "BLUE", 11, 1, 27);
            uiForm.setUiControl("pymnt_Tax_Tax_Fed_Elec_Option", pymnt_Tax_Tax_Fed_Elec_Option, true, 11, 38, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Elec_Option", pymnt_Tax_Tax_Sta_Elec_Option, true, 11, 54, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Elec_Option", pymnt_Tax_Tax_Loc_Elec_Option, true, 11, 70, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time11", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 11, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde11", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 11, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr11", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 11, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number11", pnd_Ws_Pnd_Page_Number, true, 11, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "11", "BLUE", 11, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde11", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 11, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "Filing Status.............:", "BLUE", 12, 1, 27);
            uiForm.setUiControl("pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc", pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc, true, 12, 38, 3, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc", pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc, true, 12, 54, 3, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc2", pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc, true, 12, 70, 3, "YELLOW", true, false, 
                null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time12", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 12, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde12", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 12, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr12", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 12, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number12", pnd_Ws_Pnd_Page_Number, true, 12, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "12", "BLUE", 12, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde12", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 12, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "Allowance Count...........:", "BLUE", 13, 1, 27);
            uiForm.setUiControl("pymnt_Tax_Tax_Fed_Allow_Cnt", pymnt_Tax_Tax_Fed_Allow_Cnt, true, 13, 37, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Allow_Cnt", pymnt_Tax_Tax_Sta_Allow_Cnt, true, 13, 53, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Allow_Cnt", pymnt_Tax_Tax_Loc_Allow_Cnt, true, 13, 69, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time13", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 13, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde13", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 13, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr13", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 13, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number13", pnd_Ws_Pnd_Page_Number, true, 13, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "13", "BLUE", 13, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde13", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 13, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "Addl/Fixed Withholding Amt:", "BLUE", 14, 1, 27);
            uiForm.setUiControl("pymnt_Tax_Tax_Fed_Fixed_Amt", pymnt_Tax_Tax_Fed_Fixed_Amt, true, 14, 29, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Fixed_Amt", pymnt_Tax_Tax_Sta_Fixed_Amt, true, 14, 45, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Fixed_Amt", pymnt_Tax_Tax_Loc_Fixed_Amt, true, 14, 61, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time14", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 14, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde14", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 14, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr14", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 14, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number14", pnd_Ws_Pnd_Page_Number, true, 14, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "14", "BLUE", 14, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde14", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 14, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "Addl/Fixed Withholding Pct:", "BLUE", 15, 1, 27);
            uiForm.setUiControl("pymnt_Tax_Tax_Fed_Fixed_Pct", pymnt_Tax_Tax_Fed_Fixed_Pct, true, 15, 35, 9, "YELLOW", "-ZZ9.9999", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Fixed_Pct", pymnt_Tax_Tax_Sta_Fixed_Pct, true, 15, 51, 9, "YELLOW", "-ZZ9.9999", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Fixed_Pct", pymnt_Tax_Tax_Loc_Fixed_Pct, true, 15, 67, 9, "YELLOW", "-ZZ9.9999", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time15", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 15, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde15", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 15, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr15", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 15, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number15", pnd_Ws_Pnd_Page_Number, true, 15, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "15", "BLUE", 15, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde15", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 15, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "Allowance Spouse Count....:", "BLUE", 16, 1, 27);
            uiForm.setUiLabel("label_32", "N/A", "BLUE", 16, 36, 3);
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Spouse_Cnt", pymnt_Tax_Tax_Sta_Spouse_Cnt, true, 16, 53, 2, "YELLOW", "99", true, false, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Spouse_Cnt", pymnt_Tax_Tax_Loc_Spouse_Cnt, true, 16, 69, 2, "YELLOW", "99", true, false, null, "0123456789+-, ", 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time16", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 16, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde16", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 16, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr16", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 16, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number16", pnd_Ws_Pnd_Page_Number, true, 16, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "16", "BLUE", 16, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde16", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 16, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "Allowance Blind Count.....:", "BLUE", 17, 1, 27);
            uiForm.setUiLabel("label_35", "N/A", "BLUE", 17, 36, 3);
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Blind_Cnt", pymnt_Tax_Tax_Sta_Blind_Cnt, true, 17, 53, 2, "YELLOW", "99", true, false, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Blind_Cnt", pymnt_Tax_Tax_Loc_Blind_Cnt, true, 17, 69, 2, "YELLOW", "99", true, false, null, "0123456789+-, ", 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time17", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 17, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde17", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 17, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr17", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 17, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number17", pnd_Ws_Pnd_Page_Number, true, 17, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "17", "BLUE", 17, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde17", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 17, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time18", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 18, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde18", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 18, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr18", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 18, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number18", pnd_Ws_Pnd_Page_Number, true, 18, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "18", "BLUE", 18, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde18", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 18, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time19", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 19, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde19", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 19, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr19", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 19, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number19", pnd_Ws_Pnd_Page_Number, true, 19, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "19", "BLUE", 19, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde19", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 19, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time20", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 20, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde20", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 20, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr20", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 20, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number20", pnd_Ws_Pnd_Page_Number, true, 20, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_39", "20", "BLUE", 20, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde20", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 20, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
