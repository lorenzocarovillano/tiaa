/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:49:17 PM
**        *   FROM NATURAL MAP   :  Fcpm123d
************************************************************
**        * FILE NAME               : Fcpm123d.java
**        * CLASS NAME              : Fcpm123d
**        * INSTANCE NAME           : Fcpm123d
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #PYMNT-DED-AMT(*,*) #PYMNT-DED-AMT-CV(*,*) #PYMNT-DED-CDE(*,*)                                                           *     #PYMNT-DED-CDE-CV(*,*) 
    #PYMNT-DED-PAYEE-CDE(*,*)                                                                         *     #PYMNT-DED-PAYEE-CDE-CV(*,*)
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm123d extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Pymnt_Ded_Amt;
    private DbsField pnd_Pymnt_Ded_Amt_Cv;
    private DbsField pnd_Pymnt_Ded_Cde;
    private DbsField pnd_Pymnt_Ded_Cde_Cv;
    private DbsField pnd_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Pymnt_Ded_Payee_Cde_Cv;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Pymnt_Ded_Amt = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Amt", "#PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            2, 1, 5));
        pnd_Pymnt_Ded_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Amt_Cv", "#PYMNT-DED-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new DbsArrayController(1, 
            2, 1, 5));
        pnd_Pymnt_Ded_Cde = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Cde", "#PYMNT-DED-CDE", FieldType.STRING, 14, new DbsArrayController(1, 2, 
            1, 5));
        pnd_Pymnt_Ded_Cde_Cv = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Cde_Cv", "#PYMNT-DED-CDE-CV", FieldType.ATTRIBUTE_CONTROL, 2, new DbsArrayController(1, 
            2, 1, 5));
        pnd_Pymnt_Ded_Payee_Cde = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Payee_Cde", "#PYMNT-DED-PAYEE-CDE", FieldType.STRING, 8, new DbsArrayController(1, 
            2, 1, 5));
        pnd_Pymnt_Ded_Payee_Cde_Cv = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Payee_Cde_Cv", "#PYMNT-DED-PAYEE-CDE-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 2, 1, 5));
        parameters.reset();
    }

    public Fcpm123d() throws Exception
    {
        super("Fcpm123d");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=008 LS=080 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm123d", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm123d"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "-------------------------------------------------------------------------------", "", 1, 1, 79);
            uiForm.setUiLabel("label_2", "DEDUCTIONS", "BLUE", 2, 36, 10);
            uiForm.setUiLabel("label_3", "TYPE", "BLUE", 3, 2, 4);
            uiForm.setUiLabel("label_4", "SOURCE", "BLUE", 3, 17, 6);
            uiForm.setUiLabel("label_5", "AMOUNT", "BLUE", 3, 33, 6);
            uiForm.setUiLabel("label_6", "TYPE", "BLUE", 3, 42, 4);
            uiForm.setUiLabel("label_7", "SOURCE", "BLUE", 3, 57, 6);
            uiForm.setUiLabel("label_8", "AMOUNT", "BLUE", 3, 73, 6);
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_1_1", pnd_Pymnt_Ded_Cde.getValue(1,1), true, 4, 2, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(1,1), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_1_1", pnd_Pymnt_Ded_Payee_Cde.getValue(1,1), true, 4, 17, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(1,1), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_1_1", pnd_Pymnt_Ded_Amt.getValue(1,1), true, 4, 26, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(1,1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_2_1", pnd_Pymnt_Ded_Cde.getValue(2,1), true, 4, 42, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(2,1), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_2_1", pnd_Pymnt_Ded_Payee_Cde.getValue(2,1), true, 4, 57, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(2,1), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_2_1", pnd_Pymnt_Ded_Amt.getValue(2,1), true, 4, 66, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(2,1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_1_2", pnd_Pymnt_Ded_Cde.getValue(1,2), true, 5, 2, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(1,2), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_1_2", pnd_Pymnt_Ded_Payee_Cde.getValue(1,2), true, 5, 17, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(1,2), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_1_2", pnd_Pymnt_Ded_Amt.getValue(1,2), true, 5, 26, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(1,2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_2_2", pnd_Pymnt_Ded_Cde.getValue(2,2), true, 5, 42, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(2,2), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_2_2", pnd_Pymnt_Ded_Payee_Cde.getValue(2,2), true, 5, 57, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(2,2), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_2_2", pnd_Pymnt_Ded_Amt.getValue(2,2), true, 5, 66, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(2,2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_1_3", pnd_Pymnt_Ded_Cde.getValue(1,3), true, 6, 2, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(1,3), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_1_3", pnd_Pymnt_Ded_Payee_Cde.getValue(1,3), true, 6, 17, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(1,3), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_1_3", pnd_Pymnt_Ded_Amt.getValue(1,3), true, 6, 26, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(1,3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_2_3", pnd_Pymnt_Ded_Cde.getValue(2,3), true, 6, 42, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(2,3), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_2_3", pnd_Pymnt_Ded_Payee_Cde.getValue(2,3), true, 6, 57, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(2,3), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_2_3", pnd_Pymnt_Ded_Amt.getValue(2,3), true, 6, 66, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(2,3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_1_4", pnd_Pymnt_Ded_Cde.getValue(1,4), true, 7, 2, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(1,4), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_1_4", pnd_Pymnt_Ded_Payee_Cde.getValue(1,4), true, 7, 17, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(1,4), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_1_4", pnd_Pymnt_Ded_Amt.getValue(1,4), true, 7, 26, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(1,4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_2_4", pnd_Pymnt_Ded_Cde.getValue(2,4), true, 7, 42, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(2,4), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_2_4", pnd_Pymnt_Ded_Payee_Cde.getValue(2,4), true, 7, 57, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(2,4), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_2_4", pnd_Pymnt_Ded_Amt.getValue(2,4), true, 7, 66, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(2,4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_1_5", pnd_Pymnt_Ded_Cde.getValue(1,5), true, 8, 2, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(1,5), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_1_5", pnd_Pymnt_Ded_Payee_Cde.getValue(1,5), true, 8, 17, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(1,5), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_1_5", pnd_Pymnt_Ded_Amt.getValue(1,5), true, 8, 26, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(1,5), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_2_5", pnd_Pymnt_Ded_Cde.getValue(2,5), true, 8, 42, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(2,5), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_2_5", pnd_Pymnt_Ded_Payee_Cde.getValue(2,5), true, 8, 57, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(2,5), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_2_5", pnd_Pymnt_Ded_Amt.getValue(2,5), true, 8, 66, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(2,5), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
