/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:49:21 PM
**        *   FROM NATURAL MAP   :  Fcpm123i
************************************************************
**        * FILE NAME               : Fcpm123i.java
**        * CLASS NAME              : Fcpm123i
**        * INSTANCE NAME           : Fcpm123i
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #CNTRCT-CANCEL-RDRW-ACTVTY-CDE #CNTRCT-ORGN-CDE                                                                          *     #CNTRCT-UNQ-ID-NBR 
    #PAGE-NUMBER #PYMNT-DED-AMT(*,*)                                                                      *     #PYMNT-DED-AMT-CV(*,*) #PYMNT-DED-CDE(*,*) 
    #PYMNT-DED-CDE-CV(*,*)                                                        *     #PYMNT-DED-PAYEE-CDE(*,*) #PYMNT-DED-PAYEE-CDE-CV(*,*)          
    *     #PYMNT-REQST-LOG-DTE-TIME
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm123i extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Pymnt_Ded_Amt;
    private DbsField pnd_Pymnt_Ded_Amt_Cv;
    private DbsField pnd_Pymnt_Ded_Cde;
    private DbsField pnd_Pymnt_Ded_Cde_Cv;
    private DbsField pnd_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Pymnt_Ded_Payee_Cde_Cv;
    private DbsField pnd_Pymnt_Reqst_Log_Dte_Time;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cntrct_Cancel_Rdrw_Actvty_Cde = parameters.newFieldInRecord("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde", "#CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2);
        pnd_Cntrct_Orgn_Cde = parameters.newFieldInRecord("pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Cntrct_Unq_Id_Nbr", "#CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Pymnt_Ded_Amt = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Amt", "#PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            2, 1, 5));
        pnd_Pymnt_Ded_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Amt_Cv", "#PYMNT-DED-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new DbsArrayController(1, 
            2, 1, 5));
        pnd_Pymnt_Ded_Cde = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Cde", "#PYMNT-DED-CDE", FieldType.STRING, 14, new DbsArrayController(1, 2, 
            1, 5));
        pnd_Pymnt_Ded_Cde_Cv = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Cde_Cv", "#PYMNT-DED-CDE-CV", FieldType.ATTRIBUTE_CONTROL, 2, new DbsArrayController(1, 
            2, 1, 5));
        pnd_Pymnt_Ded_Payee_Cde = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Payee_Cde", "#PYMNT-DED-PAYEE-CDE", FieldType.STRING, 8, new DbsArrayController(1, 
            2, 1, 5));
        pnd_Pymnt_Ded_Payee_Cde_Cv = parameters.newFieldArrayInRecord("pnd_Pymnt_Ded_Payee_Cde_Cv", "#PYMNT-DED-PAYEE-CDE-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 2, 1, 5));
        pnd_Pymnt_Reqst_Log_Dte_Time = parameters.newFieldInRecord("pnd_Pymnt_Reqst_Log_Dte_Time", "#PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        parameters.reset();
    }

    public Fcpm123i() throws Exception
    {
        super("Fcpm123i");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=008 LS=124 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm123i", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm123i"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "------------------------------------------------------------------------------", "", 1, 1, 78);
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time", pnd_Pymnt_Reqst_Log_Dte_Time, true, 1, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde", pnd_Cntrct_Orgn_Cde, true, 1, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr", pnd_Cntrct_Unq_Id_Nbr, true, 1, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "01", "BLUE", 1, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 1, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "DEDUCTIONS", "BLUE", 2, 36, 10);
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time2", pnd_Pymnt_Reqst_Log_Dte_Time, true, 2, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde2", pnd_Cntrct_Orgn_Cde, true, 2, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr2", pnd_Cntrct_Unq_Id_Nbr, true, 2, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number2", pnd_Page_Number, true, 2, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "02", "BLUE", 2, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde2", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 2, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "TYPE", "BLUE", 3, 2, 4);
            uiForm.setUiLabel("label_6", "SOURCE", "BLUE", 3, 17, 6);
            uiForm.setUiLabel("label_7", "AMOUNT", "BLUE", 3, 33, 6);
            uiForm.setUiLabel("label_8", "TYPE", "BLUE", 3, 42, 4);
            uiForm.setUiLabel("label_9", "SOURCE", "BLUE", 3, 57, 6);
            uiForm.setUiLabel("label_10", "AMOUNT", "BLUE", 3, 73, 6);
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time3", pnd_Pymnt_Reqst_Log_Dte_Time, true, 3, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde3", pnd_Cntrct_Orgn_Cde, true, 3, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr3", pnd_Cntrct_Unq_Id_Nbr, true, 3, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number3", pnd_Page_Number, true, 3, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_11", "03", "BLUE", 3, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde3", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 3, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_1_1", pnd_Pymnt_Ded_Cde.getValue(1,1), true, 4, 2, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(1,1), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_1_1", pnd_Pymnt_Ded_Payee_Cde.getValue(1,1), true, 4, 17, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(1,1), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_1_1", pnd_Pymnt_Ded_Amt.getValue(1,1), true, 4, 26, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(1,1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_2_1", pnd_Pymnt_Ded_Cde.getValue(2,1), true, 4, 42, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(2,1), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_2_1", pnd_Pymnt_Ded_Payee_Cde.getValue(2,1), true, 4, 57, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(2,1), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_2_1", pnd_Pymnt_Ded_Amt.getValue(2,1), true, 4, 66, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(2,1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time4", pnd_Pymnt_Reqst_Log_Dte_Time, true, 4, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde4", pnd_Cntrct_Orgn_Cde, true, 4, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr4", pnd_Cntrct_Unq_Id_Nbr, true, 4, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number4", pnd_Page_Number, true, 4, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_12", "04", "BLUE", 4, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde4", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 4, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_1_2", pnd_Pymnt_Ded_Cde.getValue(1,2), true, 5, 2, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(1,2), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_1_2", pnd_Pymnt_Ded_Payee_Cde.getValue(1,2), true, 5, 17, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(1,2), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_1_2", pnd_Pymnt_Ded_Amt.getValue(1,2), true, 5, 26, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(1,2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_2_2", pnd_Pymnt_Ded_Cde.getValue(2,2), true, 5, 42, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(2,2), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_2_2", pnd_Pymnt_Ded_Payee_Cde.getValue(2,2), true, 5, 57, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(2,2), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_2_2", pnd_Pymnt_Ded_Amt.getValue(2,2), true, 5, 66, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(2,2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time5", pnd_Pymnt_Reqst_Log_Dte_Time, true, 5, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde5", pnd_Cntrct_Orgn_Cde, true, 5, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr5", pnd_Cntrct_Unq_Id_Nbr, true, 5, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number5", pnd_Page_Number, true, 5, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_13", "05", "BLUE", 5, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde5", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 5, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_1_3", pnd_Pymnt_Ded_Cde.getValue(1,3), true, 6, 2, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(1,3), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_1_3", pnd_Pymnt_Ded_Payee_Cde.getValue(1,3), true, 6, 17, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(1,3), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_1_3", pnd_Pymnt_Ded_Amt.getValue(1,3), true, 6, 26, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(1,3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_2_3", pnd_Pymnt_Ded_Cde.getValue(2,3), true, 6, 42, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(2,3), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_2_3", pnd_Pymnt_Ded_Payee_Cde.getValue(2,3), true, 6, 57, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(2,3), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_2_3", pnd_Pymnt_Ded_Amt.getValue(2,3), true, 6, 66, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(2,3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time6", pnd_Pymnt_Reqst_Log_Dte_Time, true, 6, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde6", pnd_Cntrct_Orgn_Cde, true, 6, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr6", pnd_Cntrct_Unq_Id_Nbr, true, 6, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number6", pnd_Page_Number, true, 6, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_14", "06", "BLUE", 6, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde6", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 6, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_1_4", pnd_Pymnt_Ded_Cde.getValue(1,4), true, 7, 2, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(1,4), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_1_4", pnd_Pymnt_Ded_Payee_Cde.getValue(1,4), true, 7, 17, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(1,4), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_1_4", pnd_Pymnt_Ded_Amt.getValue(1,4), true, 7, 26, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(1,4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_2_4", pnd_Pymnt_Ded_Cde.getValue(2,4), true, 7, 42, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(2,4), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_2_4", pnd_Pymnt_Ded_Payee_Cde.getValue(2,4), true, 7, 57, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(2,4), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_2_4", pnd_Pymnt_Ded_Amt.getValue(2,4), true, 7, 66, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(2,4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time7", pnd_Pymnt_Reqst_Log_Dte_Time, true, 7, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde7", pnd_Cntrct_Orgn_Cde, true, 7, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr7", pnd_Cntrct_Unq_Id_Nbr, true, 7, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number7", pnd_Page_Number, true, 7, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_15", "07", "BLUE", 7, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde7", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 7, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_1_5", pnd_Pymnt_Ded_Cde.getValue(1,5), true, 8, 2, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(1,5), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_1_5", pnd_Pymnt_Ded_Payee_Cde.getValue(1,5), true, 8, 17, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(1,5), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_1_5", pnd_Pymnt_Ded_Amt.getValue(1,5), true, 8, 26, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(1,5), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Cde_2_5", pnd_Pymnt_Ded_Cde.getValue(2,5), true, 8, 42, 14, "YELLOW", true, false, pnd_Pymnt_Ded_Cde_Cv.getValue(2,5), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Payee_Cde_2_5", pnd_Pymnt_Ded_Payee_Cde.getValue(2,5), true, 8, 57, 8, "YELLOW", true, false, pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(2,5), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Ded_Amt_2_5", pnd_Pymnt_Ded_Amt.getValue(2,5), true, 8, 66, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Pymnt_Ded_Amt_Cv.getValue(2,5), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time8", pnd_Pymnt_Reqst_Log_Dte_Time, true, 8, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde8", pnd_Cntrct_Orgn_Cde, true, 8, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr8", pnd_Cntrct_Unq_Id_Nbr, true, 8, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number8", pnd_Page_Number, true, 8, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_16", "08", "BLUE", 8, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde8", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 8, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
