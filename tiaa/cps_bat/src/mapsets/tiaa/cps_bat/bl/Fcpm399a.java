/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:49:23 PM
**        *   FROM NATURAL MAP   :  Fcpm399a
************************************************************
**        * FILE NAME               : Fcpm399a.java
**        * CLASS NAME              : Fcpm399a
**        * INSTANCE NAME           : Fcpm399a
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #ACFS-DATA #PAGE-NUMBER #REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE                                                              *     #REC-TYPE-10-DETAIL.CNTRCT-UNQ-ID-NBR 
    *     #REC-TYPE-10-DETAIL.PYMNT-REQST-LOG-DTE-TIME                                                                             *     #REC-TYPE-40-DETAIL.CNTRCT-IVC-AMT 
    *     #REC-TYPE-40-DETAIL.PLAN-ACC-123186                                                                                      *     #REC-TYPE-40-DETAIL.PLAN-ACC-123188 
    *     #REC-TYPE-40-DETAIL.PLAN-ACC-123188-RDCTN                                                                                *     #REC-TYPE-40-DETAIL.PLAN-ACC-123188-RDCTN-EARN 
    *     #REC-TYPE-40-DETAIL.PLAN-CASH-AVAIL                                                                                      *     #REC-TYPE-40-DETAIL.PLAN-EMPLOYEE-DDCTN 
    *     #REC-TYPE-40-DETAIL.PLAN-EMPLOYEE-DDCTN-EARN                                                                             *     #REC-TYPE-40-DETAIL.PLAN-EMPLOYEE-RDCTN 
    *     #REC-TYPE-40-DETAIL.PLAN-EMPLOYEE-RDCTN-EARN                                                                             *     #REC-TYPE-40-DETAIL.PLAN-EMPLOYER-CNTRB 
    *     #REC-TYPE-40-DETAIL.PLAN-EMPLOYER-CNTRB-EARN                                                                             *     #REC-TYPE-40-DETAIL.PLAN-EMPLOYER-NAME 
    *     #REC-TYPE-40-DETAIL.PLAN-TYPE #TYPE-DISPLAY #WS.#ACFS-DPI
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm399a extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Acfs_Data;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Rec_Type_40_Detail_Cntrct_Ivc_Amt;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Acc_123186;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Acc_123188;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn_Earn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Cash_Avail;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn_Earn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn_Earn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb_Earn;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Employer_Name;
    private DbsField pnd_Rec_Type_40_Detail_Plan_Type;
    private DbsField pnd_Type_Display;
    private DbsField pnd_Ws_Pnd_Acfs_Dpi;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Acfs_Data = parameters.newFieldInRecord("pnd_Acfs_Data", "#ACFS-DATA", FieldType.STRING, 4);
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde = parameters.newFieldInRecord("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde", "#REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr", "#REC-TYPE-10-DETAIL.CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time = parameters.newFieldInRecord("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time", "#REC-TYPE-10-DETAIL.PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Rec_Type_40_Detail_Cntrct_Ivc_Amt = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Cntrct_Ivc_Amt", "#REC-TYPE-40-DETAIL.CNTRCT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Rec_Type_40_Detail_Plan_Acc_123186 = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Acc_123186", "#REC-TYPE-40-DETAIL.PLAN-ACC-123186", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Rec_Type_40_Detail_Plan_Acc_123188 = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Acc_123188", "#REC-TYPE-40-DETAIL.PLAN-ACC-123188", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn", "#REC-TYPE-40-DETAIL.PLAN-ACC-123188-RDCTN", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn_Earn = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn_Earn", "#REC-TYPE-40-DETAIL.PLAN-ACC-123188-RDCTN-EARN", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Rec_Type_40_Detail_Plan_Cash_Avail = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Cash_Avail", "#REC-TYPE-40-DETAIL.PLAN-CASH-AVAIL", 
            FieldType.STRING, 50);
        pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn", "#REC-TYPE-40-DETAIL.PLAN-EMPLOYEE-DDCTN", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn_Earn = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn_Earn", "#REC-TYPE-40-DETAIL.PLAN-EMPLOYEE-DDCTN-EARN", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn", "#REC-TYPE-40-DETAIL.PLAN-EMPLOYEE-RDCTN", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn_Earn = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn_Earn", "#REC-TYPE-40-DETAIL.PLAN-EMPLOYEE-RDCTN-EARN", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb", "#REC-TYPE-40-DETAIL.PLAN-EMPLOYER-CNTRB", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb_Earn = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb_Earn", "#REC-TYPE-40-DETAIL.PLAN-EMPLOYER-CNTRB-EARN", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Rec_Type_40_Detail_Plan_Employer_Name = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Employer_Name", "#REC-TYPE-40-DETAIL.PLAN-EMPLOYER-NAME", 
            FieldType.STRING, 35);
        pnd_Rec_Type_40_Detail_Plan_Type = parameters.newFieldInRecord("pnd_Rec_Type_40_Detail_Plan_Type", "#REC-TYPE-40-DETAIL.PLAN-TYPE", FieldType.STRING, 
            10);
        pnd_Type_Display = parameters.newFieldInRecord("pnd_Type_Display", "#TYPE-DISPLAY", FieldType.STRING, 56);
        pnd_Ws_Pnd_Acfs_Dpi = parameters.newFieldInRecord("pnd_Ws_Pnd_Acfs_Dpi", "#WS.#ACFS-DPI", FieldType.PACKED_DECIMAL, 12, 2);
        parameters.reset();
    }

    public Fcpm399a() throws Exception
    {
        super("Fcpm399a");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=020 LS=126 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm399a", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm399a"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "CONSOLIDATED PAYMENT SYSTEM", "BLUE", 1, 26, 27);
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 1, 81, 15, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 1, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 1, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "01", "BLUE", 1, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data", pnd_Acfs_Data, true, 1, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Type_Display", pnd_Type_Display, true, 2, 11, 56, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time2", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 2, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde2", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 2, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr2", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 2, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number2", pnd_Page_Number, true, 2, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "02", "BLUE", 2, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data2", pnd_Acfs_Data, true, 2, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "Alternate Carrier Fact Sheet Audit Trail", "BLUE", 3, 19, 40);
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time3", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 3, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde3", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 3, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr3", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 3, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number3", pnd_Page_Number, true, 3, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "03", "BLUE", 3, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data3", pnd_Acfs_Data, true, 3, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time4", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 4, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde4", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 4, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr4", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 4, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number4", pnd_Page_Number, true, 4, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_6", "04", "BLUE", 4, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data4", pnd_Acfs_Data, true, 4, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "Employer..............:", "BLUE", 5, 2, 23);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Employer_Name", pnd_Rec_Type_40_Detail_Plan_Employer_Name, true, 5, 26, 35, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time5", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 5, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde5", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 5, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr5", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 5, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number5", pnd_Page_Number, true, 5, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_8", "05", "BLUE", 5, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data5", pnd_Acfs_Data, true, 5, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "Type of Plan..........:", "BLUE", 6, 2, 23);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Type", pnd_Rec_Type_40_Detail_Plan_Type, true, 6, 26, 10, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time6", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 6, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde6", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 6, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr6", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 6, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number6", pnd_Page_Number, true, 6, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_10", "06", "BLUE", 6, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data6", pnd_Acfs_Data, true, 6, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "Cash available under plan rules based on information", "BLUE", 7, 2, 52);
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time7", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 7, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde7", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 7, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr7", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 7, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number7", pnd_Page_Number, true, 7, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_12", "07", "BLUE", 7, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data7", pnd_Acfs_Data, true, 7, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "available to TIAA-CREF:", "BLUE", 8, 2, 23);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Cash_Avail", pnd_Rec_Type_40_Detail_Plan_Cash_Avail, true, 8, 26, 50, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time8", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 8, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde8", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 8, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr8", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 8, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number8", pnd_Page_Number, true, 8, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_14", "08", "BLUE", 8, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data8", pnd_Acfs_Data, true, 8, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "Investment in Contract.................................:", "BLUE", 9, 2, 56);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Cntrct_Ivc_Amt", pnd_Rec_Type_40_Detail_Cntrct_Ivc_Amt, true, 9, 59, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time9", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 9, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde9", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 9, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr9", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 9, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number9", pnd_Page_Number, true, 9, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_16", "09", "BLUE", 9, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data9", pnd_Acfs_Data, true, 9, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "Delayed Payment Interest...............................:", "BLUE", 10, 2, 56);
            uiForm.setUiControl("pnd_Ws_Pnd_Acfs_Dpi", pnd_Ws_Pnd_Acfs_Dpi, true, 10, 59, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time10", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 10, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde10", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 10, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr10", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 10, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number10", pnd_Page_Number, true, 10, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_18", "10", "BLUE", 10, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data10", pnd_Acfs_Data, true, 10, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "Employer Contributions.................................:", "BLUE", 11, 2, 56);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb", pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb, true, 11, 59, 15, "YELLOW", 
                "-ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time11", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 11, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde11", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 11, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr11", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 11, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number11", pnd_Page_Number, true, 11, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_20", "11", "BLUE", 11, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data11", pnd_Acfs_Data, true, 11, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "Earnings on Employer Contributions.....................:", "BLUE", 12, 2, 56);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb_Earn", pnd_Rec_Type_40_Detail_Plan_Employer_Cntrb_Earn, true, 12, 59, 15, 
                "YELLOW", "-ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time12", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 12, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde12", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 12, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr12", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 12, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number12", pnd_Page_Number, true, 12, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_22", "12", "BLUE", 12, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data12", pnd_Acfs_Data, true, 12, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "Employee Before-Tax", "BLUE", 13, 2, 19);
            uiForm.setUiLabel("label_24", "Contributions.....................:", "BLUE", 13, 23, 35);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn", pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn, true, 13, 59, 15, "YELLOW", 
                "-ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time13", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 13, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde13", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 13, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr13", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 13, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number13", pnd_Page_Number, true, 13, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_25", "13", "BLUE", 13, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data13", pnd_Acfs_Data, true, 13, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "Earnings on Employee Before-Tax Contributions..........:", "BLUE", 14, 2, 56);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn_Earn", pnd_Rec_Type_40_Detail_Plan_Employee_Rdctn_Earn, true, 14, 59, 15, 
                "YELLOW", "-ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time14", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 14, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde14", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 14, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr14", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 14, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number14", pnd_Page_Number, true, 14, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_27", "14", "BLUE", 14, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data14", pnd_Acfs_Data, true, 14, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "Employee After-Tax Contributions.......................:", "BLUE", 15, 2, 56);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn", pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn, true, 15, 59, 15, "YELLOW", 
                "-ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time15", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 15, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde15", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 15, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr15", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 15, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number15", pnd_Page_Number, true, 15, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_29", "15", "BLUE", 15, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data15", pnd_Acfs_Data, true, 15, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "Earnings on Employee After-Tax Contributions...........:", "BLUE", 16, 2, 56);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn_Earn", pnd_Rec_Type_40_Detail_Plan_Employee_Ddctn_Earn, true, 16, 59, 15, 
                "YELLOW", "-ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time16", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 16, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde16", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 16, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr16", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 16, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number16", pnd_Page_Number, true, 16, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_31", "16", "BLUE", 16, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data16", pnd_Acfs_Data, true, 16, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "Accumulation on 12/31/86...............................:", "BLUE", 17, 2, 56);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Acc_123186", pnd_Rec_Type_40_Detail_Plan_Acc_123186, true, 17, 59, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time17", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 17, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde17", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 17, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr17", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 17, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number17", pnd_Page_Number, true, 17, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_33", "17", "BLUE", 17, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data17", pnd_Acfs_Data, true, 17, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "Accumulation on 12/31/88...............................:", "BLUE", 18, 2, 56);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Acc_123188", pnd_Rec_Type_40_Detail_Plan_Acc_123188, true, 18, 59, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time18", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 18, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde18", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 18, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr18", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 18, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number18", pnd_Page_Number, true, 18, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_35", "18", "BLUE", 18, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data18", pnd_Acfs_Data, true, 18, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "Post-1988 Employee Before-Tax Contributions............:", "BLUE", 19, 2, 56);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn", pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn, true, 19, 59, 15, "YELLOW", 
                "-ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time19", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 19, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde19", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 19, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr19", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 19, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number19", pnd_Page_Number, true, 19, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_37", "19", "BLUE", 19, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data19", pnd_Acfs_Data, true, 19, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "Post-1988 Earnings on Employee Before-Tax Contributions:", "BLUE", 20, 2, 56);
            uiForm.setUiControl("pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn_Earn", pnd_Rec_Type_40_Detail_Plan_Acc_123188_Rdctn_Earn, true, 20, 59, 
                15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time20", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 20, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde20", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 20, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr20", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 20, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number20", pnd_Page_Number, true, 20, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_39", "20", "BLUE", 20, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data20", pnd_Acfs_Data, true, 20, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
