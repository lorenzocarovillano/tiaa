/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:49:15 PM
**        *   FROM NATURAL MAP   :  Fcpm123a
************************************************************
**        * FILE NAME               : Fcpm123a.java
**        * CLASS NAME              : Fcpm123a
**        * INSTANCE NAME           : Fcpm123a
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #CHECK-NUMBER-A11 #CSR-DISPLAY #CTZNSHP-DISPLAY #CURRENCY-DISPLAY                                                        *     #ORGN-DISPLAY #PAGE-NUMBER 
    #PROGRAM #PYMNT-CHK-SAV-IND-CV                                                                *     #PYMNT-CHK-SAV-IND-TEXT #PYMNT-CHK-SAV-IND-TEXT-CV 
    #PYMNT-DISPLAY                                                        *     #PYMNT-EFT-ACCT-NBR-CV #PYMNT-EFT-ACCT-NBR-TEXT                         
    *     #PYMNT-EFT-ACCT-NBR-TEXT-CV #PYMNT-EFT-TRANSIT-ID-CV                                                                     *     #PYMNT-EFT-TRANSIT-ID-TEXT 
    #PYMNT-EFT-TRANSIT-ID-TEXT-CV                                                                 *     #RUN-TIME #STATE-DISPLAY #TYPE-DISPLAY          
    *     #WARRANT-EXTRACT.ANNT-SOC-SEC-NBR                                                                                        *     #WARRANT-EXTRACT.CNTRCT-PAYEE-CDE 
    *     #WARRANT-EXTRACT.CNTRCT-PPCN-NBR                                                                                         *     #WARRANT-EXTRACT.CNTRCT-UNQ-ID-NBR 
    *     #WARRANT-EXTRACT.PYMNT-ACCTG-DTE                                                                                         *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE1-TXT(*) 
    *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE2-TXT(*)                                                                                 *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE3-TXT(*) 
    *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE4-TXT(*)                                                                                 *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE5-TXT(*) 
    *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE6-TXT(*)                                                                                 *     #WARRANT-EXTRACT.PYMNT-CHECK-AMT 
    #WARRANT-EXTRACT.PYMNT-CHECK-DTE                                                        *     #WARRANT-EXTRACT.PYMNT-CHECK-SEQ-NBR                  
    *     #WARRANT-EXTRACT.PYMNT-CHK-SAV-IND                                                                                       *     #WARRANT-EXTRACT.PYMNT-EFT-ACCT-NBR 
    *     #WARRANT-EXTRACT.PYMNT-EFT-TRANSIT-ID                                                                                    *     #WARRANT-EXTRACT.PYMNT-INTRFCE-DTE 
    #WARRANT-EXTRACT.PYMNT-NME(*)                                                         *     #WARRANT-EXTRACT.PYMNT-SETTLMNT-DTE
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm123a extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Check_Number_A11;
    private DbsField pnd_Csr_Display;
    private DbsField pnd_Ctznshp_Display;
    private DbsField pnd_Currency_Display;
    private DbsField pnd_Orgn_Display;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Program;
    private DbsField pnd_Pymnt_Chk_Sav_Ind_Cv;
    private DbsField pnd_Pymnt_Chk_Sav_Ind_Text;
    private DbsField pnd_Pymnt_Chk_Sav_Ind_Text_Cv;
    private DbsField pnd_Pymnt_Display;
    private DbsField pnd_Pymnt_Eft_Acct_Nbr_Cv;
    private DbsField pnd_Pymnt_Eft_Acct_Nbr_Text;
    private DbsField pnd_Pymnt_Eft_Acct_Nbr_Text_Cv;
    private DbsField pnd_Pymnt_Eft_Transit_Id_Cv;
    private DbsField pnd_Pymnt_Eft_Transit_Id_Text;
    private DbsField pnd_Pymnt_Eft_Transit_Id_Text_Cv;
    private DbsField pnd_Run_Time;
    private DbsField pnd_State_Display;
    private DbsField pnd_Type_Display;
    private DbsField pnd_Warrant_Extract_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Warrant_Extract_Cntrct_Payee_Cde;
    private DbsField pnd_Warrant_Extract_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Acctg_Dte;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Check_Amt;
    private DbsField pnd_Warrant_Extract_Pymnt_Check_Dte;
    private DbsField pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind;
    private DbsField pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Eft_Transit_Id;
    private DbsField pnd_Warrant_Extract_Pymnt_Intrfce_Dte;
    private DbsField pnd_Warrant_Extract_Pymnt_Nme;
    private DbsField pnd_Warrant_Extract_Pymnt_Settlmnt_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Check_Number_A11 = parameters.newFieldInRecord("pnd_Check_Number_A11", "#CHECK-NUMBER-A11", FieldType.STRING, 11);
        pnd_Csr_Display = parameters.newFieldInRecord("pnd_Csr_Display", "#CSR-DISPLAY", FieldType.STRING, 18);
        pnd_Ctznshp_Display = parameters.newFieldInRecord("pnd_Ctznshp_Display", "#CTZNSHP-DISPLAY", FieldType.STRING, 9);
        pnd_Currency_Display = parameters.newFieldInRecord("pnd_Currency_Display", "#CURRENCY-DISPLAY", FieldType.STRING, 4);
        pnd_Orgn_Display = parameters.newFieldInRecord("pnd_Orgn_Display", "#ORGN-DISPLAY", FieldType.STRING, 43);
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Pymnt_Chk_Sav_Ind_Cv = parameters.newFieldInRecord("pnd_Pymnt_Chk_Sav_Ind_Cv", "#PYMNT-CHK-SAV-IND-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Pymnt_Chk_Sav_Ind_Text = parameters.newFieldInRecord("pnd_Pymnt_Chk_Sav_Ind_Text", "#PYMNT-CHK-SAV-IND-TEXT", FieldType.STRING, 14);
        pnd_Pymnt_Chk_Sav_Ind_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Chk_Sav_Ind_Text_Cv", "#PYMNT-CHK-SAV-IND-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Pymnt_Display = parameters.newFieldInRecord("pnd_Pymnt_Display", "#PYMNT-DISPLAY", FieldType.STRING, 11);
        pnd_Pymnt_Eft_Acct_Nbr_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Acct_Nbr_Cv", "#PYMNT-EFT-ACCT-NBR-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Pymnt_Eft_Acct_Nbr_Text = parameters.newFieldInRecord("pnd_Pymnt_Eft_Acct_Nbr_Text", "#PYMNT-EFT-ACCT-NBR-TEXT", FieldType.STRING, 8);
        pnd_Pymnt_Eft_Acct_Nbr_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Acct_Nbr_Text_Cv", "#PYMNT-EFT-ACCT-NBR-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Pymnt_Eft_Transit_Id_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Transit_Id_Cv", "#PYMNT-EFT-TRANSIT-ID-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Pymnt_Eft_Transit_Id_Text = parameters.newFieldInRecord("pnd_Pymnt_Eft_Transit_Id_Text", "#PYMNT-EFT-TRANSIT-ID-TEXT", FieldType.STRING, 16);
        pnd_Pymnt_Eft_Transit_Id_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Transit_Id_Text_Cv", "#PYMNT-EFT-TRANSIT-ID-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Run_Time = parameters.newFieldInRecord("pnd_Run_Time", "#RUN-TIME", FieldType.TIME);
        pnd_State_Display = parameters.newFieldInRecord("pnd_State_Display", "#STATE-DISPLAY", FieldType.STRING, 2);
        pnd_Type_Display = parameters.newFieldInRecord("pnd_Type_Display", "#TYPE-DISPLAY", FieldType.STRING, 56);
        pnd_Warrant_Extract_Annt_Soc_Sec_Nbr = parameters.newFieldInRecord("pnd_Warrant_Extract_Annt_Soc_Sec_Nbr", "#WARRANT-EXTRACT.ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Warrant_Extract_Cntrct_Payee_Cde = parameters.newFieldInRecord("pnd_Warrant_Extract_Cntrct_Payee_Cde", "#WARRANT-EXTRACT.CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Warrant_Extract_Cntrct_Ppcn_Nbr = parameters.newFieldInRecord("pnd_Warrant_Extract_Cntrct_Ppcn_Nbr", "#WARRANT-EXTRACT.CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr", "#WARRANT-EXTRACT.CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Warrant_Extract_Pymnt_Acctg_Dte = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Acctg_Dte", "#WARRANT-EXTRACT.PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE3-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE4-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE5-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE6-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Check_Amt = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Check_Amt", "#WARRANT-EXTRACT.PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Warrant_Extract_Pymnt_Check_Dte = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Check_Dte", "#WARRANT-EXTRACT.PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr", "#WARRANT-EXTRACT.PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind", "#WARRANT-EXTRACT.PYMNT-CHK-SAV-IND", 
            FieldType.STRING, 1);
        pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr", "#WARRANT-EXTRACT.PYMNT-EFT-ACCT-NBR", 
            FieldType.STRING, 21);
        pnd_Warrant_Extract_Pymnt_Eft_Transit_Id = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Eft_Transit_Id", "#WARRANT-EXTRACT.PYMNT-EFT-TRANSIT-ID", 
            FieldType.NUMERIC, 9);
        pnd_Warrant_Extract_Pymnt_Intrfce_Dte = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Intrfce_Dte", "#WARRANT-EXTRACT.PYMNT-INTRFCE-DTE", 
            FieldType.DATE);
        pnd_Warrant_Extract_Pymnt_Nme = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Nme", "#WARRANT-EXTRACT.PYMNT-NME", FieldType.STRING, 
            38, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Settlmnt_Dte = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Settlmnt_Dte", "#WARRANT-EXTRACT.PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);
        parameters.reset();
    }

    public Fcpm123a() throws Exception
    {
        super("Fcpm123a");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=020 LS=081 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm123a", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm123a"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Run_Time", pnd_Run_Time, true, 1, 2, 20, "YELLOW", "MM/DD/YYYY' '-' 'HH:IIAP", true, true, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "PAGE:", "BLUE", 1, 68, 5);
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 74, 6, "YELLOW", "ZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("astINIT_USER", Global.getINIT_USER(), true, 2, 2, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "-", "BLUE", 2, 13, 1);
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 2, 15, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "INTERFACE DATE:", "BLUE", 3, 1, 15);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Intrfce_Dte", pnd_Warrant_Extract_Pymnt_Intrfce_Dte, true, 3, 17, 10, "YELLOW", "MM/DD/YYYY", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Orgn_Display", pnd_Orgn_Display, true, 4, 20, 43, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Type_Display", pnd_Type_Display, true, 5, 13, 56, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Csr_Display", pnd_Csr_Display, true, 6, 36, 18, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "PIN :", "BLUE", 7, 1, 5);
            uiForm.setUiControl("pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr", pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr, true, 7, 7, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "CONTRACT:", "BLUE", 7, 22, 9);
            uiForm.setUiControl("pnd_Warrant_Extract_Cntrct_Ppcn_Nbr", pnd_Warrant_Extract_Cntrct_Ppcn_Nbr, true, 7, 32, 11, "YELLOW", "XXXXXXX-XXX", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "PAYEE:", "BLUE", 7, 45, 6);
            uiForm.setUiControl("pnd_Warrant_Extract_Cntrct_Payee_Cde", pnd_Warrant_Extract_Cntrct_Payee_Cde, true, 7, 52, 4, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "SSN.:", "BLUE", 8, 1, 5);
            uiForm.setUiControl("pnd_Warrant_Extract_Annt_Soc_Sec_Nbr", pnd_Warrant_Extract_Annt_Soc_Sec_Nbr, true, 8, 7, 11, "YELLOW", "999-99-9999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "BENEFICIARY NAME AND ADDRESS:", "BLUE", 9, 2, 29);
            uiForm.setUiLabel("label_9", "PAYEE NAME AND ADDRESS:", "BLUE", 9, 42, 23);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Nme_2", pnd_Warrant_Extract_Pymnt_Nme.getValue(2), true, 10, 2, 38, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Nme_1", pnd_Warrant_Extract_Pymnt_Nme.getValue(1), true, 10, 42, 38, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt.getValue(2), true, 11, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt.getValue(1), true, 11, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt.getValue(2), true, 12, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt.getValue(1), true, 12, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt.getValue(2), true, 13, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt.getValue(1), true, 13, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt.getValue(2), true, 14, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt.getValue(1), true, 14, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt.getValue(2), true, 15, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt.getValue(1), true, 15, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt.getValue(2), true, 16, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt.getValue(1), true, 16, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "CITIZENSHIP:", "BLUE", 17, 1, 12);
            uiForm.setUiControl("pnd_Ctznshp_Display", pnd_Ctznshp_Display, true, 17, 14, 9, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "RESIDENCE:", "BLUE", 17, 24, 10);
            uiForm.setUiControl("pnd_State_Display", pnd_State_Display, true, 17, 35, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "SETTLEMENT DATE:", "BLUE", 17, 38, 16);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Settlmnt_Dte", pnd_Warrant_Extract_Pymnt_Settlmnt_Dte, true, 17, 55, 10, "YELLOW", "MM/DD/YYYY", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "CURRENCY:", "BLUE", 17, 66, 9);
            uiForm.setUiControl("pnd_Currency_Display", pnd_Currency_Display, true, 17, 76, 4, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_14", "ACCOUNTING DATE:", "BLUE", 18, 1, 16);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Acctg_Dte", pnd_Warrant_Extract_Pymnt_Acctg_Dte, true, 18, 18, 10, "YELLOW", "MM/DD/YYYY", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "PAYMENT DATE:", "BLUE", 18, 29, 13);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Check_Dte", pnd_Warrant_Extract_Pymnt_Check_Dte, true, 18, 43, 10, "YELLOW", "MM/DD/YYYY", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "AMOUNT:", "BLUE", 18, 54, 7);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Check_Amt", pnd_Warrant_Extract_Pymnt_Check_Amt, true, 18, 62, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "METHOD:", "BLUE", 19, 1, 7);
            uiForm.setUiControl("pnd_Pymnt_Display", pnd_Pymnt_Display, true, 19, 9, 11, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "NUMBER:", "BLUE", 19, 26, 7);
            uiForm.setUiControl("pnd_Check_Number_A11", pnd_Check_Number_A11, true, 19, 34, 11, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_19", "SEQ NO.:", "BLUE", 19, 53, 8);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr", pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr, true, 19, 62, 7, "YELLOW", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Eft_Transit_Id_Text", pnd_Pymnt_Eft_Transit_Id_Text, true, 20, 2, 16, "YELLOW", true, false, pnd_Pymnt_Eft_Transit_Id_Text_Cv, 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Eft_Transit_Id", pnd_Warrant_Extract_Pymnt_Eft_Transit_Id, true, 20, 19, 9, "YELLOW", "999999999", 
                true, true, pnd_Pymnt_Eft_Transit_Id_Cv, "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Eft_Acct_Nbr_Text", pnd_Pymnt_Eft_Acct_Nbr_Text, true, 20, 31, 8, "YELLOW", true, false, pnd_Pymnt_Eft_Acct_Nbr_Text_Cv, 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr", pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr, true, 20, 40, 21, "YELLOW", true, false, 
                pnd_Pymnt_Eft_Acct_Nbr_Cv, null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Chk_Sav_Ind_Text", pnd_Pymnt_Chk_Sav_Ind_Text, true, 20, 64, 14, "YELLOW", true, false, pnd_Pymnt_Chk_Sav_Ind_Text_Cv, 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind", pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind, true, 20, 79, 1, "YELLOW", true, false, 
                pnd_Pymnt_Chk_Sav_Ind_Cv, null, "AD=YLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
