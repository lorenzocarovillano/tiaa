/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:49:58 PM
**        *   FROM NATURAL MAP   :  Fcpm872
************************************************************
**        * FILE NAME               : Fcpm872.java
**        * CLASS NAME              : Fcpm872
**        * INSTANCE NAME           : Fcpm872
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #CHECK-NUMBER-A11 #IA-DA-CONTRACT #KEY-DETAIL.CNTRCT-PAYEE-CDE                                                           *     #KEY-DETAIL.CNTRCT-PPCN-NBR 
    #KEY-DETAIL.CNTRCT-UNQ-ID-NBR #OPTION                                                        *     #PAGE-NUMBER #PROGRAM #PYMNT-CHK-SAV-IND-TEXT-CV 
    *     #PYMNT-EFT-ACCT-NBR-TEXT-CV #PYMNT-EFT-TRANSIT-ID-TEXT-CV                                                                *     #RUN-TIME #SCREEN-HEADER 
    #SCREEN-HEADER2 #SCREEN-TRAILER                                                                 *     #WS.#CSR-DISPLAY #WS.#CTZNSHP-DISPLAY #WS.#MODE-DISPLAY 
    *     #WS.#OPTION-DISPLAY #WS.#PYMNT-CHK-SAV-IND-TEXT                                                                          *     #WS.#PYMNT-DISPLAY 
    #WS.#PYMNT-EFT-ACCT-NBR-TEXT                                                                          *     #WS.#PYMNT-EFT-TRANSIT-ID-TEXT #WS.#STATE-DISP-DESC 
    #WS-MODE                                                             *     #WS-PYMNT-GROSS-AMT EXT.ANNT-SOC-SEC-NBR EXT.CNTRCT-DA-CREF-1-NBR        
    *     EXT.CNTRCT-DA-TIAA-1-NBR EXT.CNTRCT-DA-TIAA-2-NBR                                                                        *     EXT.CNTRCT-DA-TIAA-3-NBR 
    EXT.CNTRCT-DA-TIAA-4-NBR                                                                        *     EXT.CNTRCT-DA-TIAA-5-NBR EXT.PH-SEX EXT.PYMNT-ACCTG-DTE 
    *     EXT.PYMNT-ADDR-LINE1-TXT(*) EXT.PYMNT-ADDR-LINE2-TXT(*)                                                                  *     EXT.PYMNT-ADDR-LINE3-TXT(*) 
    EXT.PYMNT-ADDR-LINE4-TXT(*)                                                                  *     EXT.PYMNT-ADDR-LINE5-TXT(*) EXT.PYMNT-ADDR-LINE6-TXT(*) 
    *     EXT.PYMNT-CHECK-AMT EXT.PYMNT-CHECK-DTE EXT.PYMNT-CHECK-SEQ-NBR                                                          *     EXT.PYMNT-CHK-SAV-IND 
    EXT.PYMNT-DOB EXT.PYMNT-EFT-ACCT-NBR                                                               *     EXT.PYMNT-EFT-TRANSIT-ID EXT.PYMNT-INTRFCE-DTE 
    EXT.PYMNT-NME(*)
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm872 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Check_Number_A11;
    private DbsField pnd_Ia_Da_Contract;
    private DbsField pnd_Key_Detail_Cntrct_Payee_Cde;
    private DbsField pnd_Key_Detail_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Key_Detail_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Option;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Program;
    private DbsField pnd_Pymnt_Chk_Sav_Ind_Text_Cv;
    private DbsField pnd_Pymnt_Eft_Acct_Nbr_Text_Cv;
    private DbsField pnd_Pymnt_Eft_Transit_Id_Text_Cv;
    private DbsField pnd_Run_Time;
    private DbsField pnd_Screen_Header;
    private DbsField pnd_Screen_Header2;
    private DbsField pnd_Screen_Trailer;
    private DbsField pnd_Ws_Pnd_Csr_Display;
    private DbsField pnd_Ws_Pnd_Ctznshp_Display;
    private DbsField pnd_Ws_Pnd_Mode_Display;
    private DbsField pnd_Ws_Pnd_Option_Display;
    private DbsField pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text;
    private DbsField pnd_Ws_Pnd_Pymnt_Display;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text;
    private DbsField pnd_Ws_Pnd_State_Disp_Desc;
    private DbsField pnd_Ws_Mode;
    private DbsField pnd_Ws_Pymnt_Gross_Amt;
    private DbsField ext_Annt_Soc_Sec_Nbr;
    private DbsField ext_Cntrct_Da_Cref_1_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField ext_Ph_Sex;
    private DbsField ext_Pymnt_Acctg_Dte;
    private DbsField ext_Pymnt_Addr_Line1_Txt;
    private DbsField ext_Pymnt_Addr_Line2_Txt;
    private DbsField ext_Pymnt_Addr_Line3_Txt;
    private DbsField ext_Pymnt_Addr_Line4_Txt;
    private DbsField ext_Pymnt_Addr_Line5_Txt;
    private DbsField ext_Pymnt_Addr_Line6_Txt;
    private DbsField ext_Pymnt_Check_Amt;
    private DbsField ext_Pymnt_Check_Dte;
    private DbsField ext_Pymnt_Check_Seq_Nbr;
    private DbsField ext_Pymnt_Chk_Sav_Ind;
    private DbsField ext_Pymnt_Dob;
    private DbsField ext_Pymnt_Eft_Acct_Nbr;
    private DbsField ext_Pymnt_Eft_Transit_Id;
    private DbsField ext_Pymnt_Intrfce_Dte;
    private DbsField ext_Pymnt_Nme;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Check_Number_A11 = parameters.newFieldInRecord("pnd_Check_Number_A11", "#CHECK-NUMBER-A11", FieldType.STRING, 11);
        pnd_Ia_Da_Contract = parameters.newFieldInRecord("pnd_Ia_Da_Contract", "#IA-DA-CONTRACT", FieldType.STRING, 2);
        pnd_Key_Detail_Cntrct_Payee_Cde = parameters.newFieldInRecord("pnd_Key_Detail_Cntrct_Payee_Cde", "#KEY-DETAIL.CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Key_Detail_Cntrct_Ppcn_Nbr = parameters.newFieldInRecord("pnd_Key_Detail_Cntrct_Ppcn_Nbr", "#KEY-DETAIL.CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Key_Detail_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Key_Detail_Cntrct_Unq_Id_Nbr", "#KEY-DETAIL.CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Option = parameters.newFieldInRecord("pnd_Option", "#OPTION", FieldType.STRING, 7);
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Pymnt_Chk_Sav_Ind_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Chk_Sav_Ind_Text_Cv", "#PYMNT-CHK-SAV-IND-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Pymnt_Eft_Acct_Nbr_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Acct_Nbr_Text_Cv", "#PYMNT-EFT-ACCT-NBR-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Pymnt_Eft_Transit_Id_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Transit_Id_Text_Cv", "#PYMNT-EFT-TRANSIT-ID-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Run_Time = parameters.newFieldInRecord("pnd_Run_Time", "#RUN-TIME", FieldType.TIME);
        pnd_Screen_Header = parameters.newFieldInRecord("pnd_Screen_Header", "#SCREEN-HEADER", FieldType.STRING, 35);
        pnd_Screen_Header2 = parameters.newFieldInRecord("pnd_Screen_Header2", "#SCREEN-HEADER2", FieldType.STRING, 60);
        pnd_Screen_Trailer = parameters.newFieldInRecord("pnd_Screen_Trailer", "#SCREEN-TRAILER", FieldType.STRING, 75);
        pnd_Ws_Pnd_Csr_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Csr_Display", "#WS.#CSR-DISPLAY", FieldType.STRING, 9);
        pnd_Ws_Pnd_Ctznshp_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Ctznshp_Display", "#WS.#CTZNSHP-DISPLAY", FieldType.STRING, 9);
        pnd_Ws_Pnd_Mode_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Mode_Display", "#WS.#MODE-DISPLAY", FieldType.STRING, 18);
        pnd_Ws_Pnd_Option_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Option_Display", "#WS.#OPTION-DISPLAY", FieldType.STRING, 35);
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text = parameters.newFieldInRecord("pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text", "#WS.#PYMNT-CHK-SAV-IND-TEXT", FieldType.STRING, 
            17);
        pnd_Ws_Pnd_Pymnt_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Pymnt_Display", "#WS.#PYMNT-DISPLAY", FieldType.STRING, 17);
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text = parameters.newFieldInRecord("pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text", "#WS.#PYMNT-EFT-ACCT-NBR-TEXT", FieldType.STRING, 
            8);
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text = parameters.newFieldInRecord("pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text", "#WS.#PYMNT-EFT-TRANSIT-ID-TEXT", FieldType.STRING, 
            16);
        pnd_Ws_Pnd_State_Disp_Desc = parameters.newFieldInRecord("pnd_Ws_Pnd_State_Disp_Desc", "#WS.#STATE-DISP-DESC", FieldType.STRING, 20);
        pnd_Ws_Mode = parameters.newFieldInRecord("pnd_Ws_Mode", "#WS-MODE", FieldType.STRING, 5);
        pnd_Ws_Pymnt_Gross_Amt = parameters.newFieldInRecord("pnd_Ws_Pymnt_Gross_Amt", "#WS-PYMNT-GROSS-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        ext_Annt_Soc_Sec_Nbr = parameters.newFieldInRecord("ext_Annt_Soc_Sec_Nbr", "EXT.ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        ext_Cntrct_Da_Cref_1_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Cref_1_Nbr", "EXT.CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_1_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Tiaa_1_Nbr", "EXT.CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_2_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Tiaa_2_Nbr", "EXT.CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_3_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Tiaa_3_Nbr", "EXT.CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_4_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Tiaa_4_Nbr", "EXT.CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_5_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Tiaa_5_Nbr", "EXT.CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        ext_Ph_Sex = parameters.newFieldInRecord("ext_Ph_Sex", "EXT.PH-SEX", FieldType.STRING, 1);
        ext_Pymnt_Acctg_Dte = parameters.newFieldInRecord("ext_Pymnt_Acctg_Dte", "EXT.PYMNT-ACCTG-DTE", FieldType.DATE);
        ext_Pymnt_Addr_Line1_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line1_Txt", "EXT.PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Addr_Line2_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line2_Txt", "EXT.PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Addr_Line3_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line3_Txt", "EXT.PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Addr_Line4_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line4_Txt", "EXT.PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Addr_Line5_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line5_Txt", "EXT.PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Addr_Line6_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line6_Txt", "EXT.PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Check_Amt = parameters.newFieldInRecord("ext_Pymnt_Check_Amt", "EXT.PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        ext_Pymnt_Check_Dte = parameters.newFieldInRecord("ext_Pymnt_Check_Dte", "EXT.PYMNT-CHECK-DTE", FieldType.DATE);
        ext_Pymnt_Check_Seq_Nbr = parameters.newFieldInRecord("ext_Pymnt_Check_Seq_Nbr", "EXT.PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        ext_Pymnt_Chk_Sav_Ind = parameters.newFieldInRecord("ext_Pymnt_Chk_Sav_Ind", "EXT.PYMNT-CHK-SAV-IND", FieldType.STRING, 1);
        ext_Pymnt_Dob = parameters.newFieldInRecord("ext_Pymnt_Dob", "EXT.PYMNT-DOB", FieldType.DATE);
        ext_Pymnt_Eft_Acct_Nbr = parameters.newFieldInRecord("ext_Pymnt_Eft_Acct_Nbr", "EXT.PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        ext_Pymnt_Eft_Transit_Id = parameters.newFieldInRecord("ext_Pymnt_Eft_Transit_Id", "EXT.PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9);
        ext_Pymnt_Intrfce_Dte = parameters.newFieldInRecord("ext_Pymnt_Intrfce_Dte", "EXT.PYMNT-INTRFCE-DTE", FieldType.DATE);
        ext_Pymnt_Nme = parameters.newFieldArrayInRecord("ext_Pymnt_Nme", "EXT.PYMNT-NME", FieldType.STRING, 38, new DbsArrayController(1, 2));
        parameters.reset();
    }

    public Fcpm872() throws Exception
    {
        super("Fcpm872");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=023 LS=081 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm872", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm872"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Run_Time", pnd_Run_Time, true, 1, 2, 20, "BLUE", "MM/DD/YYYY' '-' 'HH:IIAP", true, false, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "PAGE:", "BLUE", 1, 68, 5);
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 74, 6, "YELLOW", "ZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("astINIT_USER", Global.getINIT_USER(), true, 2, 2, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "-", "BLUE", 2, 13, 1);
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 2, 15, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Screen_Header", pnd_Screen_Header, true, 3, 24, 35, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Screen_Header2", pnd_Screen_Header2, true, 4, 11, 60, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "INTERFACE DATE:", "BLUE", 5, 2, 15);
            uiForm.setUiControl("ext_Pymnt_Intrfce_Dte", ext_Pymnt_Intrfce_Dte, true, 5, 18, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Csr_Display", pnd_Ws_Pnd_Csr_Display, true, 5, 36, 9, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "PIN :", "BLUE", 6, 2, 5);
            uiForm.setUiControl("pnd_Key_Detail_Cntrct_Unq_Id_Nbr", pnd_Key_Detail_Cntrct_Unq_Id_Nbr, true, 6, 8, 12, "YELLOW", "999999999999", true, 
                true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ia_Da_Contract", pnd_Ia_Da_Contract, true, 6, 32, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "Contract/Certificate:", "BLUE", 6, 35, 21);
            uiForm.setUiControl("pnd_Key_Detail_Cntrct_Ppcn_Nbr", pnd_Key_Detail_Cntrct_Ppcn_Nbr, true, 6, 57, 9, "YELLOW", "XXXXXXX-X", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Detail_Cntrct_Payee_Cde", pnd_Key_Detail_Cntrct_Payee_Cde, true, 6, 67, 4, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "SSN.:", "BLUE", 7, 2, 5);
            uiForm.setUiControl("ext_Annt_Soc_Sec_Nbr", ext_Annt_Soc_Sec_Nbr, true, 7, 8, 11, "YELLOW", "999-99-9999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "Sex:", "BLUE", 7, 32, 4);
            uiForm.setUiControl("ext_Ph_Sex", ext_Ph_Sex, true, 7, 37, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "Date of Birth:", "BLUE", 7, 42, 14);
            uiForm.setUiControl("ext_Pymnt_Dob", ext_Pymnt_Dob, true, 7, 59, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_9", "ANNUITANT NAME AND ADDRESS:", "BLUE", 8, 2, 27);
            uiForm.setUiLabel("label_10", "PAYEE NAME AND ADDRESS:", "BLUE", 8, 42, 23);
            uiForm.setUiControl("ext_Pymnt_Nme_2", ext_Pymnt_Nme.getValue(2), true, 9, 2, 38, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Nme_1", ext_Pymnt_Nme.getValue(1), true, 9, 42, 38, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line1_Txt_2", ext_Pymnt_Addr_Line1_Txt.getValue(2), true, 10, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line1_Txt_1", ext_Pymnt_Addr_Line1_Txt.getValue(1), true, 10, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line2_Txt_2", ext_Pymnt_Addr_Line2_Txt.getValue(2), true, 11, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line2_Txt_1", ext_Pymnt_Addr_Line2_Txt.getValue(1), true, 11, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line3_Txt_2", ext_Pymnt_Addr_Line3_Txt.getValue(2), true, 12, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line3_Txt_1", ext_Pymnt_Addr_Line3_Txt.getValue(1), true, 12, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line4_Txt_2", ext_Pymnt_Addr_Line4_Txt.getValue(2), true, 13, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line4_Txt_1", ext_Pymnt_Addr_Line4_Txt.getValue(1), true, 13, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line5_Txt_2", ext_Pymnt_Addr_Line5_Txt.getValue(2), true, 14, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line5_Txt_1", ext_Pymnt_Addr_Line5_Txt.getValue(1), true, 14, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line6_Txt_2", ext_Pymnt_Addr_Line6_Txt.getValue(2), true, 15, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line6_Txt_1", ext_Pymnt_Addr_Line6_Txt.getValue(1), true, 15, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "Citizenship:", "BLUE", 16, 2, 12);
            uiForm.setUiControl("pnd_Ws_Pnd_Ctznshp_Display", pnd_Ws_Pnd_Ctznshp_Display, true, 16, 15, 9, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_12", "Residence:", "BLUE", 16, 42, 10);
            uiForm.setUiControl("pnd_Ws_Pnd_State_Disp_Desc", pnd_Ws_Pnd_State_Disp_Desc, true, 16, 53, 20, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Option", pnd_Option, true, 17, 2, 7, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Option_Display", pnd_Ws_Pnd_Option_Display, true, 17, 10, 35, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Ws_Mode", pnd_Ws_Mode, true, 17, 47, 5, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Mode_Display", pnd_Ws_Pnd_Mode_Display, true, 17, 53, 18, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_13", "Payment Date...:", "BLUE", 18, 2, 16);
            uiForm.setUiControl("ext_Pymnt_Check_Dte", ext_Pymnt_Check_Dte, true, 18, 19, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_14", "Accounting Date:", "BLUE", 18, 36, 16);
            uiForm.setUiControl("ext_Pymnt_Acctg_Dte", ext_Pymnt_Acctg_Dte, true, 18, 53, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_15", "Payment Method :", "BLUE", 19, 2, 16);
            uiForm.setUiControl("pnd_Ws_Pnd_Pymnt_Display", pnd_Ws_Pnd_Pymnt_Display, true, 19, 19, 17, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_16", "Number:", "BLUE", 19, 39, 7);
            uiForm.setUiControl("pnd_Check_Number_A11", pnd_Check_Number_A11, true, 19, 47, 11, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_17", "SEQ NO.:", "BLUE", 19, 62, 8);
            uiForm.setUiControl("ext_Pymnt_Check_Seq_Nbr", ext_Pymnt_Check_Seq_Nbr, true, 19, 71, 7, "YELLOW", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text", pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text, true, 20, 2, 16, "YELLOW", true, false, 
                pnd_Pymnt_Eft_Transit_Id_Text_Cv, null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Eft_Transit_Id", ext_Pymnt_Eft_Transit_Id, true, 20, 19, 9, "BLUE", "999999999", true, true, pnd_Pymnt_Eft_Transit_Id_Text_Cv, 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text", pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text, true, 20, 30, 8, "YELLOW", true, false, pnd_Pymnt_Eft_Acct_Nbr_Text_Cv, 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Eft_Acct_Nbr", ext_Pymnt_Eft_Acct_Nbr, true, 20, 39, 21, "BLUE", true, false, pnd_Pymnt_Eft_Acct_Nbr_Text_Cv, 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text", pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text, true, 20, 61, 17, "YELLOW", true, false, pnd_Pymnt_Chk_Sav_Ind_Text_Cv, 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Chk_Sav_Ind", ext_Pymnt_Chk_Sav_Ind, true, 20, 79, 1, "BLUE", true, false, pnd_Pymnt_Chk_Sav_Ind_Text_Cv, null, 
                "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "Total Gross Payment Amt:", "BLUE", 21, 2, 24);
            uiForm.setUiControl("pnd_Ws_Pymnt_Gross_Amt", pnd_Ws_Pymnt_Gross_Amt, true, 21, 27, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "Total Net Payment Amt:", "BLUE", 21, 42, 22);
            uiForm.setUiControl("ext_Pymnt_Check_Amt", ext_Pymnt_Check_Amt, true, 21, 65, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "Da Cont/Cert's Settled :", "BLUE", 22, 2, 24);
            uiForm.setUiControl("ext_Cntrct_Da_Tiaa_1_Nbr", ext_Cntrct_Da_Tiaa_1_Nbr, true, 22, 27, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ext_Cntrct_Da_Tiaa_2_Nbr", ext_Cntrct_Da_Tiaa_2_Nbr, true, 22, 36, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ext_Cntrct_Da_Tiaa_3_Nbr", ext_Cntrct_Da_Tiaa_3_Nbr, true, 22, 45, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ext_Cntrct_Da_Tiaa_4_Nbr", ext_Cntrct_Da_Tiaa_4_Nbr, true, 22, 54, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ext_Cntrct_Da_Tiaa_5_Nbr", ext_Cntrct_Da_Tiaa_5_Nbr, true, 22, 63, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ext_Cntrct_Da_Cref_1_Nbr", ext_Cntrct_Da_Cref_1_Nbr, true, 22, 72, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Screen_Trailer", pnd_Screen_Trailer, true, 23, 3, 75, "YELLOW", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
