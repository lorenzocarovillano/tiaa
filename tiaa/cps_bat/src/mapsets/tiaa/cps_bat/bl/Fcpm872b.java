/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:00 PM
**        *   FROM NATURAL MAP   :  Fcpm872b
************************************************************
**        * FILE NAME               : Fcpm872b.java
**        * CLASS NAME              : Fcpm872b
**        * INSTANCE NAME           : Fcpm872b
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #DISP-ACCT-LEDGR-AMT-CV(*) #PAGE-NUMBER #PROGRAM #RUN-TIME                                                               *     #SCREEN-HEADER 
    #SCREEN-HEADER2 #WS.#DISP-ACCT-LEDGR-AMT(*)                                                               *     #WS.#DISP-ACCT-LEDGR-DESC(*) #WS.#DISP-ACCT-LEDGR-IND(*) 
    *     #WS.#DISP-LEDGER-ISA(*) #WS.#DISP-LEDGER-NBR(*)                                                                          *     #WS.#PYMNT-ACCTG-DTE
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm872b extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Disp_Acct_Ledgr_Amt_Cv;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Program;
    private DbsField pnd_Run_Time;
    private DbsField pnd_Screen_Header;
    private DbsField pnd_Screen_Header2;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind;
    private DbsField pnd_Ws_Pnd_Disp_Ledger_Isa;
    private DbsField pnd_Ws_Pnd_Disp_Ledger_Nbr;
    private DbsField pnd_Ws_Pnd_Pymnt_Acctg_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Disp_Acct_Ledgr_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Disp_Acct_Ledgr_Amt_Cv", "#DISP-ACCT-LEDGR-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 15));
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Run_Time = parameters.newFieldInRecord("pnd_Run_Time", "#RUN-TIME", FieldType.TIME);
        pnd_Screen_Header = parameters.newFieldInRecord("pnd_Screen_Header", "#SCREEN-HEADER", FieldType.STRING, 35);
        pnd_Screen_Header2 = parameters.newFieldInRecord("pnd_Screen_Header2", "#SCREEN-HEADER2", FieldType.STRING, 60);
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt", "#WS.#DISP-ACCT-LEDGR-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 15));
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc", "#WS.#DISP-ACCT-LEDGR-DESC", FieldType.STRING, 
            37, new DbsArrayController(1, 15));
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind", "#WS.#DISP-ACCT-LEDGR-IND", FieldType.STRING, 
            2, new DbsArrayController(1, 15));
        pnd_Ws_Pnd_Disp_Ledger_Isa = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Disp_Ledger_Isa", "#WS.#DISP-LEDGER-ISA", FieldType.STRING, 5, new DbsArrayController(1, 
            15));
        pnd_Ws_Pnd_Disp_Ledger_Nbr = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Disp_Ledger_Nbr", "#WS.#DISP-LEDGER-NBR", FieldType.STRING, 15, new 
            DbsArrayController(1, 15));
        pnd_Ws_Pnd_Pymnt_Acctg_Dte = parameters.newFieldInRecord("pnd_Ws_Pnd_Pymnt_Acctg_Dte", "#WS.#PYMNT-ACCTG-DTE", FieldType.DATE);
        parameters.reset();
    }

    public Fcpm872b() throws Exception
    {
        super("Fcpm872b");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=022 LS=081 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm872b", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm872b"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Run_Time", pnd_Run_Time, true, 1, 2, 20, "YELLOW", "MM/DD/YYYY' '-' 'HH:IIAP", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "PAGE:", "BLUE", 1, 68, 5);
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 74, 6, "YELLOW", "ZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("astINIT_USER", Global.getINIT_USER(), true, 2, 2, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "-", "BLUE", 2, 13, 1);
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 2, 15, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Screen_Header", pnd_Screen_Header, true, 3, 24, 35, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Screen_Header2", pnd_Screen_Header2, true, 4, 11, 60, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "Payment Ledgers for Accounting Date:", "BLUE", 5, 18, 36);
            uiForm.setUiControl("pnd_Ws_Pnd_Pymnt_Acctg_Dte", pnd_Ws_Pnd_Pymnt_Acctg_Dte, true, 5, 55, 10, "YELLOW", "MM/DD/YYYY", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "Ledger Number", "BLUE", 6, 2, 13);
            uiForm.setUiLabel("label_5", "Ledger Description", "BLUE", 6, 18, 18);
            uiForm.setUiLabel("label_6", "I S A", "BLUE", 6, 56, 5);
            uiForm.setUiLabel("label_7", "Amount", "BLUE", 6, 74, 6);
            uiForm.setUiLabel("label_8", "------------------------------------------------------------------------------", "", 7, 2, 78);
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_1", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(1), true, 8, 2, 15, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_1", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(1), true, 8, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_1", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(1), true, 8, 56, 5, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_1", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(1), true, 8, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_1", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(1), true, 8, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_2", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(2), true, 9, 2, 15, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_2", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(2), true, 9, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_2", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(2), true, 9, 56, 5, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_2", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(2), true, 9, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_2", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(2), true, 9, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_3", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(3), true, 10, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_3", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(3), true, 10, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_3", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(3), true, 10, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_3", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(3), true, 10, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_3", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(3), true, 10, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_4", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(4), true, 11, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_4", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(4), true, 11, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_4", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(4), true, 11, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_4", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(4), true, 11, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_4", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(4), true, 11, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_5", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(5), true, 12, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_5", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(5), true, 12, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_5", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(5), true, 12, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_5", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(5), true, 12, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_5", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(5), true, 12, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(5), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_6", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(6), true, 13, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_6", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(6), true, 13, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_6", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(6), true, 13, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_6", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(6), true, 13, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_6", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(6), true, 13, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(6), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_7", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(7), true, 14, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_7", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(7), true, 14, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_7", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(7), true, 14, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_7", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(7), true, 14, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_7", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(7), true, 14, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(7), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_8", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(8), true, 15, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_8", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(8), true, 15, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_8", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(8), true, 15, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_8", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(8), true, 15, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_8", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(8), true, 15, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(8), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_9", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(9), true, 16, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_9", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(9), true, 16, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_9", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(9), true, 16, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_9", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(9), true, 16, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_9", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(9), true, 16, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(9), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_10", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(10), true, 17, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_10", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(10), true, 17, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_10", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(10), true, 17, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_10", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(10), true, 17, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_10", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(10), true, 17, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(10), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_11", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(11), true, 18, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_11", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(11), true, 18, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_11", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(11), true, 18, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_11", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(11), true, 18, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_11", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(11), true, 18, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(11), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_12", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(12), true, 19, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_12", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(12), true, 19, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_12", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(12), true, 19, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_12", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(12), true, 19, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_12", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(12), true, 19, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(12), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_13", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(13), true, 20, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_13", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(13), true, 20, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_13", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(13), true, 20, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_13", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(13), true, 20, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_13", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(13), true, 20, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(13), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_14", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(14), true, 21, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_14", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(14), true, 21, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_14", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(14), true, 21, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_14", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(14), true, 21, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_14", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(14), true, 21, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(14), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_15", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(15), true, 22, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_15", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(15), true, 22, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_15", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(15), true, 22, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_15", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(15), true, 22, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_15", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(15), true, 22, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(15), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
