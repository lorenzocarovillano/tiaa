/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:04 PM
**        *   FROM NATURAL MAP   :  Fcpm872g
************************************************************
**        * FILE NAME               : Fcpm872g.java
**        * CLASS NAME              : Fcpm872g
**        * INSTANCE NAME           : Fcpm872g
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #DISP-ACCT-LEDGR-AMT-CV(*) #KEY-EFM.CNTRCT-CANCEL-RDRW-ACTVTY-CDE                                                        *     #KEY-EFM.CNTRCT-ORGN-CDE 
    #KEY-EFM.CNTRCT-UNQ-ID-NBR                                                                      *     #KEY-EFM.PYMNT-REQST-LOG-DTE-TIME #SCREEN-HEADER 
    #SCREEN-HEADER2                                                         *     #WS.#DISP-ACCT-LEDGR-AMT(*) #WS.#DISP-ACCT-LEDGR-DESC(*)              
    *     #WS.#DISP-ACCT-LEDGR-IND(*) #WS.#DISP-LEDGER-ISA(*)                                                                      *     #WS.#DISP-LEDGER-NBR(*) 
    #WS.#LINE-COUNT(*) #WS.#PAGE-NUMBER                                                              *     #WS.#PYMNT-ACCTG-DTE
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm872g extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Disp_Acct_Ledgr_Amt_Cv;
    private DbsField pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Key_Efm_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Efm_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Screen_Header;
    private DbsField pnd_Screen_Header2;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind;
    private DbsField pnd_Ws_Pnd_Disp_Ledger_Isa;
    private DbsField pnd_Ws_Pnd_Disp_Ledger_Nbr;
    private DbsField pnd_Ws_Pnd_Line_Count;
    private DbsField pnd_Ws_Pnd_Page_Number;
    private DbsField pnd_Ws_Pnd_Pymnt_Acctg_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Disp_Acct_Ledgr_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Disp_Acct_Ledgr_Amt_Cv", "#DISP-ACCT-LEDGR-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 15));
        pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde", "#KEY-EFM.CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Key_Efm_Cntrct_Orgn_Cde = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Orgn_Cde", "#KEY-EFM.CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Efm_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Unq_Id_Nbr", "#KEY-EFM.CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time = parameters.newFieldInRecord("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time", "#KEY-EFM.PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Screen_Header = parameters.newFieldInRecord("pnd_Screen_Header", "#SCREEN-HEADER", FieldType.STRING, 35);
        pnd_Screen_Header2 = parameters.newFieldInRecord("pnd_Screen_Header2", "#SCREEN-HEADER2", FieldType.STRING, 60);
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt", "#WS.#DISP-ACCT-LEDGR-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 15));
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc", "#WS.#DISP-ACCT-LEDGR-DESC", FieldType.STRING, 
            37, new DbsArrayController(1, 15));
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind", "#WS.#DISP-ACCT-LEDGR-IND", FieldType.STRING, 
            2, new DbsArrayController(1, 15));
        pnd_Ws_Pnd_Disp_Ledger_Isa = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Disp_Ledger_Isa", "#WS.#DISP-LEDGER-ISA", FieldType.STRING, 5, new DbsArrayController(1, 
            15));
        pnd_Ws_Pnd_Disp_Ledger_Nbr = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Disp_Ledger_Nbr", "#WS.#DISP-LEDGER-NBR", FieldType.STRING, 15, new 
            DbsArrayController(1, 15));
        pnd_Ws_Pnd_Line_Count = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Line_Count", "#WS.#LINE-COUNT", FieldType.PACKED_DECIMAL, 2, new DbsArrayController(1, 
            15));
        pnd_Ws_Pnd_Page_Number = parameters.newFieldInRecord("pnd_Ws_Pnd_Page_Number", "#WS.#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Ws_Pnd_Pymnt_Acctg_Dte = parameters.newFieldInRecord("pnd_Ws_Pnd_Pymnt_Acctg_Dte", "#WS.#PYMNT-ACCTG-DTE", FieldType.DATE);
        parameters.reset();
    }

    public Fcpm872g() throws Exception
    {
        super("Fcpm872g");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=020 LS=124 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm872g", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm872g"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Screen_Header", pnd_Screen_Header, true, 1, 24, 35, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 1, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 1, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 1, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number", pnd_Ws_Pnd_Page_Number, true, 1, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "01", "BLUE", 1, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 1, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Screen_Header2", pnd_Screen_Header2, true, 2, 11, 60, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time2", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 2, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde2", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 2, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr2", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 2, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number2", pnd_Ws_Pnd_Page_Number, true, 2, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "02", "BLUE", 2, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde2", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 2, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "Payment Ledgers for Accounting Date:", "BLUE", 3, 18, 36);
            uiForm.setUiControl("pnd_Ws_Pnd_Pymnt_Acctg_Dte", pnd_Ws_Pnd_Pymnt_Acctg_Dte, true, 3, 55, 10, "YELLOW", "MM/DD/YYYY", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time3", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 3, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde3", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 3, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr3", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 3, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number3", pnd_Ws_Pnd_Page_Number, true, 3, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "03", "BLUE", 3, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde3", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 3, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "Ledger Number", "BLUE", 4, 2, 13);
            uiForm.setUiLabel("label_6", "Ledger Description", "BLUE", 4, 18, 18);
            uiForm.setUiLabel("label_7", "I S A", "BLUE", 4, 56, 5);
            uiForm.setUiLabel("label_8", "Amount", "BLUE", 4, 74, 6);
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time4", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 4, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde4", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 4, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr4", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 4, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number4", pnd_Ws_Pnd_Page_Number, true, 4, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "04", "BLUE", 4, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde4", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 4, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "------------------------------------------------------------------------------", "", 5, 2, 78);
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time5", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 5, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde5", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 5, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr5", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 5, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number5", pnd_Ws_Pnd_Page_Number, true, 5, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "05", "BLUE", 5, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde5", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 5, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_1", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(1), true, 6, 2, 15, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_1", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(1), true, 6, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_1", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(1), true, 6, 56, 5, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_1", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(1), true, 6, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_1", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(1), true, 6, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time6", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 6, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde6", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 6, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr6", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 6, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number6", pnd_Ws_Pnd_Page_Number, true, 6, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_1", pnd_Ws_Pnd_Line_Count.getValue(1), true, 6, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde6", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 6, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_2", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(2), true, 7, 2, 15, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_2", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(2), true, 7, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_2", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(2), true, 7, 56, 5, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_2", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(2), true, 7, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_2", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(2), true, 7, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time7", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 7, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde7", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 7, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr7", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 7, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number7", pnd_Ws_Pnd_Page_Number, true, 7, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_2", pnd_Ws_Pnd_Line_Count.getValue(2), true, 7, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde7", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 7, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_3", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(3), true, 8, 2, 15, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_3", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(3), true, 8, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_3", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(3), true, 8, 56, 5, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_3", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(3), true, 8, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_3", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(3), true, 8, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time8", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 8, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde8", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 8, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr8", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 8, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number8", pnd_Ws_Pnd_Page_Number, true, 8, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_3", pnd_Ws_Pnd_Line_Count.getValue(3), true, 8, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde8", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 8, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_4", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(4), true, 9, 2, 15, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_4", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(4), true, 9, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_4", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(4), true, 9, 56, 5, "YELLOW", true, false, null, null, 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_4", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(4), true, 9, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_4", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(4), true, 9, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time9", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 9, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde9", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 9, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr9", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 9, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number9", pnd_Ws_Pnd_Page_Number, true, 9, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_4", pnd_Ws_Pnd_Line_Count.getValue(4), true, 9, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde9", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 9, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_5", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(5), true, 10, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_5", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(5), true, 10, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_5", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(5), true, 10, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_5", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(5), true, 10, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_5", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(5), true, 10, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(5), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time10", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 10, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde10", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 10, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr10", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 10, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number10", pnd_Ws_Pnd_Page_Number, true, 10, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_5", pnd_Ws_Pnd_Line_Count.getValue(5), true, 10, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde10", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 10, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_6", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(6), true, 11, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_6", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(6), true, 11, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_6", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(6), true, 11, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_6", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(6), true, 11, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_6", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(6), true, 11, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(6), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time11", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 11, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde11", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 11, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr11", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 11, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number11", pnd_Ws_Pnd_Page_Number, true, 11, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_6", pnd_Ws_Pnd_Line_Count.getValue(6), true, 11, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde11", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 11, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_7", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(7), true, 12, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_7", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(7), true, 12, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_7", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(7), true, 12, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_7", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(7), true, 12, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_7", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(7), true, 12, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(7), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time12", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 12, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde12", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 12, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr12", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 12, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number12", pnd_Ws_Pnd_Page_Number, true, 12, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_7", pnd_Ws_Pnd_Line_Count.getValue(7), true, 12, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde12", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 12, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_8", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(8), true, 13, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_8", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(8), true, 13, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_8", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(8), true, 13, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_8", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(8), true, 13, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_8", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(8), true, 13, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(8), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time13", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 13, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde13", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 13, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr13", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 13, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number13", pnd_Ws_Pnd_Page_Number, true, 13, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_8", pnd_Ws_Pnd_Line_Count.getValue(8), true, 13, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde13", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 13, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_9", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(9), true, 14, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_9", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(9), true, 14, 18, 37, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_9", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(9), true, 14, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_9", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(9), true, 14, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_9", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(9), true, 14, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(9), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time14", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 14, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde14", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 14, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr14", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 14, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number14", pnd_Ws_Pnd_Page_Number, true, 14, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_9", pnd_Ws_Pnd_Line_Count.getValue(9), true, 14, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde14", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 14, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_10", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(10), true, 15, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_10", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(10), true, 15, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_10", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(10), true, 15, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_10", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(10), true, 15, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_10", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(10), true, 15, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(10), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time15", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 15, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde15", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 15, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr15", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 15, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number15", pnd_Ws_Pnd_Page_Number, true, 15, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_10", pnd_Ws_Pnd_Line_Count.getValue(10), true, 15, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde15", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 15, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_11", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(11), true, 16, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_11", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(11), true, 16, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_11", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(11), true, 16, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_11", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(11), true, 16, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_11", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(11), true, 16, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(11), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time16", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 16, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde16", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 16, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr16", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 16, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number16", pnd_Ws_Pnd_Page_Number, true, 16, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_11", pnd_Ws_Pnd_Line_Count.getValue(11), true, 16, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde16", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 16, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_12", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(12), true, 17, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_12", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(12), true, 17, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_12", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(12), true, 17, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_12", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(12), true, 17, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_12", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(12), true, 17, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(12), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time17", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 17, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde17", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 17, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr17", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 17, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number17", pnd_Ws_Pnd_Page_Number, true, 17, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_12", pnd_Ws_Pnd_Line_Count.getValue(12), true, 17, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde17", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 17, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_13", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(13), true, 18, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_13", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(13), true, 18, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_13", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(13), true, 18, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_13", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(13), true, 18, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_13", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(13), true, 18, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(13), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time18", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 18, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde18", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 18, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr18", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 18, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number18", pnd_Ws_Pnd_Page_Number, true, 18, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_13", pnd_Ws_Pnd_Line_Count.getValue(13), true, 18, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde18", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 18, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_14", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(14), true, 19, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_14", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(14), true, 19, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_14", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(14), true, 19, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_14", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(14), true, 19, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_14", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(14), true, 19, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(14), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time19", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 19, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde19", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 19, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr19", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 19, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number19", pnd_Ws_Pnd_Page_Number, true, 19, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_14", pnd_Ws_Pnd_Line_Count.getValue(14), true, 19, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde19", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 19, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Nbr_15", pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(15), true, 20, 2, 15, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc_15", pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(15), true, 20, 18, 37, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Ledger_Isa_15", pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(15), true, 20, 56, 5, "YELLOW", true, false, null, 
                null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind_15", pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(15), true, 20, 62, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_15", pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(15), true, 20, 65, 15, "YELLOW", "-ZZZ,ZZZ,ZZ9.99", 
                true, true, pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(15), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time20", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 20, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde20", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 20, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr20", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 20, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number20", pnd_Ws_Pnd_Page_Number, true, 20, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Line_Count_15", pnd_Ws_Pnd_Line_Count.getValue(15), true, 20, 119, 2, "YELLOW", "99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde20", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 20, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
