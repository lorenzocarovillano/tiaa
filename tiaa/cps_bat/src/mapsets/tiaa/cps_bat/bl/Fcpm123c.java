/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:49:16 PM
**        *   FROM NATURAL MAP   :  Fcpm123c
************************************************************
**        * FILE NAME               : Fcpm123c.java
**        * CLASS NAME              : Fcpm123c
**        * INSTANCE NAME           : Fcpm123c
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     ##PYMNT-DED-AMT(*) ##PYMNT-DED-AMT-CV(*) #FED-DISPLAY                                                                    *     #INV-ACCT-CDE-DESC-CV(*) 
    #INV-ACCT-CDE-DESC-DC(*)                                                                        *     #INV-ACCT-CNTRCT-AMT(*) #INV-ACCT-CNTRCT-AMT-CV(*) 
    *     #INV-ACCT-DCI-AMT(*) #INV-ACCT-DCI-AMT-CV(*) #INV-ACCT-DPI-AMT(*)                                                        *     #INV-ACCT-DPI-AMT-CV(*) 
    #INV-ACCT-DVDND-AMT(*)                                                                           *     #INV-ACCT-DVDND-AMT-CV(*) #INV-ACCT-FDRL-TAX-AMT(*) 
    *     #INV-ACCT-FDRL-TAX-AMT-CV(*) #INV-ACCT-IVC-AMT(*)                                                                        *     #INV-ACCT-IVC-AMT-CV(*) 
    #INV-ACCT-LOCAL-TAX-AMT(*)                                                                       *     #INV-ACCT-LOCAL-TAX-AMT-CV(*) #INV-ACCT-NET-PYMNT-AMT(*) 
    *     #INV-ACCT-NET-PYMNT-AMT-CV(*) #INV-ACCT-SETTL-AMT(*)                                                                     *     #INV-ACCT-SETTL-AMT-CV(*) 
    #INV-ACCT-STATE-TAX-AMT(*)                                                                     *     #INV-ACCT-STATE-TAX-AMT-CV(*) #MODE-DISPLAY #ORGN-DISPLAY 
    *     #PAGE-NUMBER #PROGRAM #PYMNT-SETTLMNT-DTE(*)                                                                             *     #PYMNT-SETTLMNT-DTE-CV(*) 
    #RUN-TIME #STATE-DISPLAY #TYPE-DISPLAY                                                         *     #WS.#INSTALLMENT-HDR(*) #WS.#INV-ACCT-UNIT-QTY-A(*) 
    *     #WS.#INV-ACCT-UNIT-VALUE-A(*)
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm123c extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Pnd_Pymnt_Ded_Amt;
    private DbsField pnd_Pnd_Pymnt_Ded_Amt_Cv;
    private DbsField pnd_Fed_Display;
    private DbsField pnd_Inv_Acct_Cde_Desc_Cv;
    private DbsField pnd_Inv_Acct_Cde_Desc_Dc;
    private DbsField pnd_Inv_Acct_Cntrct_Amt;
    private DbsField pnd_Inv_Acct_Cntrct_Amt_Cv;
    private DbsField pnd_Inv_Acct_Dci_Amt;
    private DbsField pnd_Inv_Acct_Dci_Amt_Cv;
    private DbsField pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Inv_Acct_Dpi_Amt_Cv;
    private DbsField pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Inv_Acct_Dvdnd_Amt_Cv;
    private DbsField pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Inv_Acct_Fdrl_Tax_Amt_Cv;
    private DbsField pnd_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Inv_Acct_Ivc_Amt_Cv;
    private DbsField pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Inv_Acct_Local_Tax_Amt_Cv;
    private DbsField pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Inv_Acct_Net_Pymnt_Amt_Cv;
    private DbsField pnd_Inv_Acct_Settl_Amt;
    private DbsField pnd_Inv_Acct_Settl_Amt_Cv;
    private DbsField pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Inv_Acct_State_Tax_Amt_Cv;
    private DbsField pnd_Mode_Display;
    private DbsField pnd_Orgn_Display;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Program;
    private DbsField pnd_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Pymnt_Settlmnt_Dte_Cv;
    private DbsField pnd_Run_Time;
    private DbsField pnd_State_Display;
    private DbsField pnd_Type_Display;
    private DbsField pnd_Ws_Pnd_Installment_Hdr;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Value_A;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Pnd_Pymnt_Ded_Amt = parameters.newFieldArrayInRecord("pnd_Pnd_Pymnt_Ded_Amt", "##PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            4));
        pnd_Pnd_Pymnt_Ded_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Pnd_Pymnt_Ded_Amt_Cv", "##PYMNT-DED-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Fed_Display = parameters.newFieldInRecord("pnd_Fed_Display", "#FED-DISPLAY", FieldType.STRING, 3);
        pnd_Inv_Acct_Cde_Desc_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cde_Desc_Cv", "#INV-ACCT-CDE-DESC-CV", FieldType.ATTRIBUTE_CONTROL, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Cde_Desc_Dc = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cde_Desc_Dc", "#INV-ACCT-CDE-DESC-DC", FieldType.STRING, 11, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Cntrct_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cntrct_Amt", "#INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 12, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Cntrct_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cntrct_Amt_Cv", "#INV-ACCT-CNTRCT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Dci_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dci_Amt", "#INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Dci_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dci_Amt_Cv", "#INV-ACCT-DCI-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Dpi_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Dpi_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dpi_Amt_Cv", "#INV-ACCT-DPI-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Dvdnd_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dvdnd_Amt", "#INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 12, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Dvdnd_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dvdnd_Amt_Cv", "#INV-ACCT-DVDND-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Fdrl_Tax_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 10, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Fdrl_Tax_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Fdrl_Tax_Amt_Cv", "#INV-ACCT-FDRL-TAX-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Ivc_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Ivc_Amt", "#INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Ivc_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Ivc_Amt_Cv", "#INV-ACCT-IVC-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Local_Tax_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Local_Tax_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Local_Tax_Amt_Cv", "#INV-ACCT-LOCAL-TAX-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Net_Pymnt_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Net_Pymnt_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Net_Pymnt_Amt_Cv", "#INV-ACCT-NET-PYMNT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Settl_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Settl_Amt", "#INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 12, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Settl_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Settl_Amt_Cv", "#INV-ACCT-SETTL-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_State_Tax_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_State_Tax_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_State_Tax_Amt_Cv", "#INV-ACCT-STATE-TAX-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Mode_Display = parameters.newFieldInRecord("pnd_Mode_Display", "#MODE-DISPLAY", FieldType.STRING, 18);
        pnd_Orgn_Display = parameters.newFieldInRecord("pnd_Orgn_Display", "#ORGN-DISPLAY", FieldType.STRING, 43);
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Pymnt_Settlmnt_Dte = parameters.newFieldArrayInRecord("pnd_Pymnt_Settlmnt_Dte", "#PYMNT-SETTLMNT-DTE", FieldType.DATE, new DbsArrayController(1, 
            4));
        pnd_Pymnt_Settlmnt_Dte_Cv = parameters.newFieldArrayInRecord("pnd_Pymnt_Settlmnt_Dte_Cv", "#PYMNT-SETTLMNT-DTE-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Run_Time = parameters.newFieldInRecord("pnd_Run_Time", "#RUN-TIME", FieldType.TIME);
        pnd_State_Display = parameters.newFieldInRecord("pnd_State_Display", "#STATE-DISPLAY", FieldType.STRING, 2);
        pnd_Type_Display = parameters.newFieldInRecord("pnd_Type_Display", "#TYPE-DISPLAY", FieldType.STRING, 56);
        pnd_Ws_Pnd_Installment_Hdr = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Installment_Hdr", "#WS.#INSTALLMENT-HDR", FieldType.STRING, 14, new 
            DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A", "#WS.#INV-ACCT-UNIT-QTY-A", FieldType.STRING, 
            12, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Unit_Value_A = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Unit_Value_A", "#WS.#INV-ACCT-UNIT-VALUE-A", FieldType.STRING, 
            12, new DbsArrayController(1, 4));
        parameters.reset();
    }

    public Fcpm123c() throws Exception
    {
        super("Fcpm123c");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=023 LS=080 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm123c", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm123c"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Run_Time", pnd_Run_Time, true, 1, 2, 20, "YELLOW", "MM/DD/YYYY' '-' 'HH:IIAP", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "PAGE:", "BLUE", 1, 68, 5);
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 74, 6, "YELLOW", "ZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("astINIT_USER", Global.getINIT_USER(), true, 2, 2, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "-", "BLUE", 2, 13, 1);
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 2, 15, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Orgn_Display", pnd_Orgn_Display, true, 3, 20, 43, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Type_Display", pnd_Type_Display, true, 4, 13, 56, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "MODE:", "BLUE", 5, 45, 5);
            uiForm.setUiControl("pnd_Mode_Display", pnd_Mode_Display, true, 5, 51, 18, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "-------------------------------------------------------------------------------", "", 6, 1, 79);
            uiForm.setUiControl("pnd_Ws_Pnd_Installment_Hdr_1", pnd_Ws_Pnd_Installment_Hdr.getValue(1), true, 7, 21, 14, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Installment_Hdr_2", pnd_Ws_Pnd_Installment_Hdr.getValue(2), true, 7, 36, 14, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Installment_Hdr_3", pnd_Ws_Pnd_Installment_Hdr.getValue(3), true, 7, 51, 14, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Installment_Hdr_4", pnd_Ws_Pnd_Installment_Hdr.getValue(4), true, 7, 66, 14, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "INSTALLMENT/EFF DT:", "BLUE", 8, 1, 19);
            uiForm.setUiControl("pnd_Pymnt_Settlmnt_Dte_1", pnd_Pymnt_Settlmnt_Dte.getValue(1), true, 8, 25, 10, "YELLOW", "MM/DD/YYYY", true, false, 
                pnd_Pymnt_Settlmnt_Dte_Cv.getValue(1), null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Settlmnt_Dte_2", pnd_Pymnt_Settlmnt_Dte.getValue(2), true, 8, 40, 10, "YELLOW", "MM/DD/YYYY", true, false, 
                pnd_Pymnt_Settlmnt_Dte_Cv.getValue(2), null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Settlmnt_Dte_3", pnd_Pymnt_Settlmnt_Dte.getValue(3), true, 8, 55, 10, "YELLOW", "MM/DD/YYYY", true, false, 
                pnd_Pymnt_Settlmnt_Dte_Cv.getValue(3), null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Settlmnt_Dte_4", pnd_Pymnt_Settlmnt_Dte.getValue(4), true, 8, 70, 10, "YELLOW", "MM/DD/YYYY", true, false, 
                pnd_Pymnt_Settlmnt_Dte_Cv.getValue(4), null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "INVESTMENT ACCOUNT:", "BLUE", 9, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_Dc_1", pnd_Inv_Acct_Cde_Desc_Dc.getValue(1), true, 9, 24, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(1), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_Dc_2", pnd_Inv_Acct_Cde_Desc_Dc.getValue(2), true, 9, 39, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(2), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_Dc_3", pnd_Inv_Acct_Cde_Desc_Dc.getValue(3), true, 9, 54, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(3), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_Dc_4", pnd_Inv_Acct_Cde_Desc_Dc.getValue(4), true, 9, 69, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(4), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "SETTLEMENT AMOUNT.:", "BLUE", 10, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_1", pnd_Inv_Acct_Settl_Amt.getValue(1), true, 10, 21, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_2", pnd_Inv_Acct_Settl_Amt.getValue(2), true, 10, 36, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_3", pnd_Inv_Acct_Settl_Amt.getValue(3), true, 10, 51, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_4", pnd_Inv_Acct_Settl_Amt.getValue(4), true, 10, 66, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "CONTRACTUAL AMOUNT:", "BLUE", 11, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_1", pnd_Inv_Acct_Cntrct_Amt.getValue(1), true, 11, 21, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_2", pnd_Inv_Acct_Cntrct_Amt.getValue(2), true, 11, 36, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_3", pnd_Inv_Acct_Cntrct_Amt.getValue(3), true, 11, 51, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_4", pnd_Inv_Acct_Cntrct_Amt.getValue(4), true, 11, 66, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "DIVIDEND AMOUNT...:", "BLUE", 12, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_1", pnd_Inv_Acct_Dvdnd_Amt.getValue(1), true, 12, 21, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_2", pnd_Inv_Acct_Dvdnd_Amt.getValue(2), true, 12, 36, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_3", pnd_Inv_Acct_Dvdnd_Amt.getValue(3), true, 12, 51, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_4", pnd_Inv_Acct_Dvdnd_Amt.getValue(4), true, 12, 66, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "UNITS.............:", "BLUE", 13, 1, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A_1", pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A.getValue(1), true, 13, 23, 12, "BLUE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A_2", pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A.getValue(2), true, 13, 38, 12, "BLUE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A_3", pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A.getValue(3), true, 13, 53, 12, "BLUE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A_4", pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A.getValue(4), true, 13, 68, 12, "BLUE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "UNIT VALUE........:", "BLUE", 14, 1, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_A_1", pnd_Ws_Pnd_Inv_Acct_Unit_Value_A.getValue(1), true, 14, 23, 12, "BLUE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_A_2", pnd_Ws_Pnd_Inv_Acct_Unit_Value_A.getValue(2), true, 14, 38, 12, "BLUE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_A_3", pnd_Ws_Pnd_Inv_Acct_Unit_Value_A.getValue(3), true, 14, 53, 12, "BLUE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_A_4", pnd_Ws_Pnd_Inv_Acct_Unit_Value_A.getValue(4), true, 14, 68, 12, "BLUE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "IVC AMOUNT........:", "BLUE", 15, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_1", pnd_Inv_Acct_Ivc_Amt.getValue(1), true, 15, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_2", pnd_Inv_Acct_Ivc_Amt.getValue(2), true, 15, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_3", pnd_Inv_Acct_Ivc_Amt.getValue(3), true, 15, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_4", pnd_Inv_Acct_Ivc_Amt.getValue(4), true, 15, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "OVERPAYMENT.......:", "BLUE", 16, 1, 19);
            uiForm.setUiControl("pnd_Pnd_Pymnt_Ded_Amt_1", pnd_Pnd_Pymnt_Ded_Amt.getValue(1), true, 16, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, 
                pnd_Pnd_Pymnt_Ded_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pnd_Pymnt_Ded_Amt_2", pnd_Pnd_Pymnt_Ded_Amt.getValue(2), true, 16, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, 
                pnd_Pnd_Pymnt_Ded_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pnd_Pymnt_Ded_Amt_3", pnd_Pnd_Pymnt_Ded_Amt.getValue(3), true, 16, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, 
                pnd_Pnd_Pymnt_Ded_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pnd_Pymnt_Ded_Amt_4", pnd_Pnd_Pymnt_Ded_Amt.getValue(4), true, 16, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, 
                pnd_Pnd_Pymnt_Ded_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "DCI AMOUNT........:", "BLUE", 17, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Dci_Amt_1", pnd_Inv_Acct_Dci_Amt.getValue(1), true, 17, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dci_Amt_Cv.getValue(1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dci_Amt_2", pnd_Inv_Acct_Dci_Amt.getValue(2), true, 17, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dci_Amt_Cv.getValue(2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dci_Amt_3", pnd_Inv_Acct_Dci_Amt.getValue(3), true, 17, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dci_Amt_Cv.getValue(3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dci_Amt_4", pnd_Inv_Acct_Dci_Amt.getValue(4), true, 17, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dci_Amt_Cv.getValue(4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "DPI AMOUNT........:", "BLUE", 18, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_1", pnd_Inv_Acct_Dpi_Amt.getValue(1), true, 18, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_2", pnd_Inv_Acct_Dpi_Amt.getValue(2), true, 18, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_3", pnd_Inv_Acct_Dpi_Amt.getValue(3), true, 18, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_4", pnd_Inv_Acct_Dpi_Amt.getValue(4), true, 18, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "FEDERAL TAX(", "BLUE", 19, 1, 12);
            uiForm.setUiControl("pnd_Fed_Display", pnd_Fed_Display, true, 19, 14, 3, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "):", "BLUE", 19, 18, 2);
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_1", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(1), true, 19, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_2", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(2), true, 19, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_3", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(3), true, 19, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_4", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(4), true, 19, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "STATE TAX..(", "BLUE", 20, 1, 12);
            uiForm.setUiControl("pnd_State_Display", pnd_State_Display, true, 20, 14, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", ").:", "BLUE", 20, 17, 3);
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_1", pnd_Inv_Acct_State_Tax_Amt.getValue(1), true, 20, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_2", pnd_Inv_Acct_State_Tax_Amt.getValue(2), true, 20, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_3", pnd_Inv_Acct_State_Tax_Amt.getValue(3), true, 20, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_4", pnd_Inv_Acct_State_Tax_Amt.getValue(4), true, 20, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "LOCAL TAX.........:", "BLUE", 21, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_1", pnd_Inv_Acct_Local_Tax_Amt.getValue(1), true, 21, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_2", pnd_Inv_Acct_Local_Tax_Amt.getValue(2), true, 21, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_3", pnd_Inv_Acct_Local_Tax_Amt.getValue(3), true, 21, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_4", pnd_Inv_Acct_Local_Tax_Amt.getValue(4), true, 21, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "NET PAYMENT.......:", "BLUE", 22, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_1", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(1), true, 22, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_2", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(2), true, 22, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_3", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(3), true, 22, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_4", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(4), true, 22, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
