/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:03 PM
**        *   FROM NATURAL MAP   :  Fcpm872f
************************************************************
**        * FILE NAME               : Fcpm872f.java
**        * CLASS NAME              : Fcpm872f
**        * INSTANCE NAME           : Fcpm872f
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #INV-ACCT-CDE-DESC-CV(*) #INV-ACCT-CNTRCT-AMT-CV(*)                                                                      *     #INV-ACCT-DPI-DCI-AMT-CV(*) 
    #INV-ACCT-DVDND-AMT-CV(*)                                                                    *     #INV-ACCT-FDRL-TAX-CV(*) #INV-ACCT-GROSS-AMT-CV(*) 
    *     #INV-ACCT-IVC-AMT-CV(*) #INV-ACCT-LOCAL-TAX-CV(*)                                                                        *     #INV-ACCT-NET-PYMNT-CV(*) 
    #INV-ACCT-SETTL-AMT-CV(*)                                                                      *     #INV-ACCT-STATE-TAX-CV(*) #INV-ACCT-TOTAL-TAX-CV(*) 
    *     #INV-ACCT-TXBLE-AMT-CV(*) #INV-ACCT-UNIT-QTY-CV(*)                                                                       *     #INV-ACCT-UNIT-VALUE-CV(*) 
    #KEY-EFM.CNTRCT-CANCEL-RDRW-ACTVTY-CDE                                                        *     #KEY-EFM.CNTRCT-ORGN-CDE #KEY-EFM.CNTRCT-UNQ-ID-NBR 
    *     #KEY-EFM.PYMNT-REQST-LOG-DTE-TIME #SCREEN-HEADER                                                                         *     #WS.#EFFECTIVE-DTE 
    #WS.#FED-DISPLAY #WS.#FUND-DISPLAY                                                                    *     #WS.#GROSS-AMT-DISP #WS.#INV-ACCT-CDE-DESC(*) 
    *     #WS.#INV-ACCT-CNTRCT-AMT(*) #WS.#INV-ACCT-DPI-DCI-AMT(*)                                                                 *     #WS.#INV-ACCT-DVDND-AMT(*) 
    #WS.#INV-ACCT-FDRL-TAX(*)                                                                     *     #WS.#INV-ACCT-GROSS-AMT(*) #WS.#INV-ACCT-IVC-AMT(*) 
    *     #WS.#INV-ACCT-LOCAL-TAX(*) #WS.#INV-ACCT-NET-PYMNT(*)                                                                    *     #WS.#INV-ACCT-SETTL-AMT(*) 
    #WS.#INV-ACCT-STATE-TAX(*)                                                                    *     #WS.#INV-ACCT-TOTAL-TAX(*) #WS.#INV-ACCT-TXBLE-AMT(*) 
    *     #WS.#INV-ACCT-UNIT-QTY(*) #WS.#INV-ACCT-UNIT-VALUE(*)                                                                    *     #WS.#PAGE-NUMBER 
    #WS.#STATE-DISP-X EXT.PYMNT-SETTLMNT-DTE
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm872f extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Inv_Acct_Cde_Desc_Cv;
    private DbsField pnd_Inv_Acct_Cntrct_Amt_Cv;
    private DbsField pnd_Inv_Acct_Dpi_Dci_Amt_Cv;
    private DbsField pnd_Inv_Acct_Dvdnd_Amt_Cv;
    private DbsField pnd_Inv_Acct_Fdrl_Tax_Cv;
    private DbsField pnd_Inv_Acct_Gross_Amt_Cv;
    private DbsField pnd_Inv_Acct_Ivc_Amt_Cv;
    private DbsField pnd_Inv_Acct_Local_Tax_Cv;
    private DbsField pnd_Inv_Acct_Net_Pymnt_Cv;
    private DbsField pnd_Inv_Acct_Settl_Amt_Cv;
    private DbsField pnd_Inv_Acct_State_Tax_Cv;
    private DbsField pnd_Inv_Acct_Total_Tax_Cv;
    private DbsField pnd_Inv_Acct_Txble_Amt_Cv;
    private DbsField pnd_Inv_Acct_Unit_Qty_Cv;
    private DbsField pnd_Inv_Acct_Unit_Value_Cv;
    private DbsField pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Key_Efm_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Efm_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Screen_Header;
    private DbsField pnd_Ws_Pnd_Effective_Dte;
    private DbsField pnd_Ws_Pnd_Fed_Display;
    private DbsField pnd_Ws_Pnd_Fund_Display;
    private DbsField pnd_Ws_Pnd_Gross_Amt_Disp;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cde_Desc;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Local_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Net_Pymnt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_State_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Total_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Txble_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Pnd_Page_Number;
    private DbsField pnd_Ws_Pnd_State_Disp_X;
    private DbsField ext_Pymnt_Settlmnt_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Inv_Acct_Cde_Desc_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cde_Desc_Cv", "#INV-ACCT-CDE-DESC-CV", FieldType.ATTRIBUTE_CONTROL, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Cntrct_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cntrct_Amt_Cv", "#INV-ACCT-CNTRCT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Dpi_Dci_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dpi_Dci_Amt_Cv", "#INV-ACCT-DPI-DCI-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Dvdnd_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dvdnd_Amt_Cv", "#INV-ACCT-DVDND-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Fdrl_Tax_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Fdrl_Tax_Cv", "#INV-ACCT-FDRL-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Gross_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Gross_Amt_Cv", "#INV-ACCT-GROSS-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Ivc_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Ivc_Amt_Cv", "#INV-ACCT-IVC-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Local_Tax_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Local_Tax_Cv", "#INV-ACCT-LOCAL-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Net_Pymnt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Net_Pymnt_Cv", "#INV-ACCT-NET-PYMNT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Settl_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Settl_Amt_Cv", "#INV-ACCT-SETTL-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_State_Tax_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_State_Tax_Cv", "#INV-ACCT-STATE-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Total_Tax_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Total_Tax_Cv", "#INV-ACCT-TOTAL-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Txble_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Txble_Amt_Cv", "#INV-ACCT-TXBLE-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Unit_Qty_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Qty_Cv", "#INV-ACCT-UNIT-QTY-CV", FieldType.ATTRIBUTE_CONTROL, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Unit_Value_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Value_Cv", "#INV-ACCT-UNIT-VALUE-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde", "#KEY-EFM.CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Key_Efm_Cntrct_Orgn_Cde = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Orgn_Cde", "#KEY-EFM.CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Efm_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Unq_Id_Nbr", "#KEY-EFM.CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time = parameters.newFieldInRecord("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time", "#KEY-EFM.PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Screen_Header = parameters.newFieldInRecord("pnd_Screen_Header", "#SCREEN-HEADER", FieldType.STRING, 35);
        pnd_Ws_Pnd_Effective_Dte = parameters.newFieldInRecord("pnd_Ws_Pnd_Effective_Dte", "#WS.#EFFECTIVE-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Fed_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Fed_Display", "#WS.#FED-DISPLAY", FieldType.STRING, 3);
        pnd_Ws_Pnd_Fund_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Fund_Display", "#WS.#FUND-DISPLAY", FieldType.STRING, 60);
        pnd_Ws_Pnd_Gross_Amt_Disp = parameters.newFieldInRecord("pnd_Ws_Pnd_Gross_Amt_Disp", "#WS.#GROSS-AMT-DISP", FieldType.STRING, 19);
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Cde_Desc", "#WS.#INV-ACCT-CDE-DESC", FieldType.STRING, 11, 
            new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt", "#WS.#INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt", "#WS.#INV-ACCT-DPI-DCI-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt", "#WS.#INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax", "#WS.#INV-ACCT-FDRL-TAX", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Gross_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Gross_Amt", "#WS.#INV-ACCT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Ivc_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt", "#WS.#INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Local_Tax = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Local_Tax", "#WS.#INV-ACCT-LOCAL-TAX", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Net_Pymnt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt", "#WS.#INV-ACCT-NET-PYMNT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Settl_Amt", "#WS.#INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_State_Tax = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_State_Tax", "#WS.#INV-ACCT-STATE-TAX", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Total_Tax = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Total_Tax", "#WS.#INV-ACCT-TOTAL-TAX", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Txble_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Txble_Amt", "#WS.#INV-ACCT-TXBLE-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Unit_Qty = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Unit_Qty", "#WS.#INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            10, 3, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Unit_Value = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Unit_Value", "#WS.#INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            10, 4, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Page_Number = parameters.newFieldInRecord("pnd_Ws_Pnd_Page_Number", "#WS.#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Ws_Pnd_State_Disp_X = parameters.newFieldInRecord("pnd_Ws_Pnd_State_Disp_X", "#WS.#STATE-DISP-X", FieldType.STRING, 2);
        ext_Pymnt_Settlmnt_Dte = parameters.newFieldInRecord("ext_Pymnt_Settlmnt_Dte", "EXT.PYMNT-SETTLMNT-DTE", FieldType.DATE);
        parameters.reset();
    }

    public Fcpm872f() throws Exception
    {
        super("Fcpm872f");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=020 LS=124 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm872f", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm872f"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Screen_Header", pnd_Screen_Header, true, 1, 24, 35, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 1, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 1, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 1, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number", pnd_Ws_Pnd_Page_Number, true, 1, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "01", "BLUE", 1, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 1, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Fund_Display", pnd_Ws_Pnd_Fund_Display, true, 2, 11, 60, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time2", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 2, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde2", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 2, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr2", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 2, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number2", pnd_Ws_Pnd_Page_Number, true, 2, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "02", "BLUE", 2, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde2", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 2, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time3", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 3, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde3", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 3, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr3", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 3, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number3", pnd_Ws_Pnd_Page_Number, true, 3, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "03", "BLUE", 3, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde3", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 3, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "Payment Due Date..:", "BLUE", 4, 2, 19);
            uiForm.setUiControl("ext_Pymnt_Settlmnt_Dte", ext_Pymnt_Settlmnt_Dte, true, 4, 22, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "Effective", "BLUE", 4, 53, 9);
            uiForm.setUiLabel("label_6", "Date:", "BLUE", 4, 64, 5);
            uiForm.setUiControl("pnd_Ws_Pnd_Effective_Dte", pnd_Ws_Pnd_Effective_Dte, true, 4, 70, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time4", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 4, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde4", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 4, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr4", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 4, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number4", pnd_Ws_Pnd_Page_Number, true, 4, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "04", "BLUE", 4, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde4", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 4, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "Investment Account:", "BLUE", 5, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cde_Desc_1", pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(1), true, 5, 24, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(1), 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cde_Desc_2", pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(2), true, 5, 39, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(2), 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cde_Desc_3", pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(3), true, 5, 54, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(3), 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cde_Desc_4", pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(4), true, 5, 69, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(4), 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time5", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 5, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde5", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 5, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr5", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 5, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number5", pnd_Ws_Pnd_Page_Number, true, 5, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "05", "BLUE", 5, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde5", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 5, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Gross_Amt_Disp", pnd_Ws_Pnd_Gross_Amt_Disp, true, 6, 2, 19, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Settl_Amt_1", pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(1), true, 6, 22, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Settl_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Settl_Amt_2", pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(2), true, 6, 37, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Settl_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Settl_Amt_3", pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(3), true, 6, 52, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Settl_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Settl_Amt_4", pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(4), true, 6, 67, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Settl_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time6", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 6, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde6", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 6, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr6", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 6, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number6", pnd_Ws_Pnd_Page_Number, true, 6, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "06", "BLUE", 6, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde6", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 6, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "Contractual Amount:", "BLUE", 7, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_1", pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt.getValue(1), true, 7, 22, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_2", pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt.getValue(2), true, 7, 37, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_3", pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt.getValue(3), true, 7, 52, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_4", pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt.getValue(4), true, 7, 67, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time7", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 7, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde7", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 7, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr7", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 7, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number7", pnd_Ws_Pnd_Page_Number, true, 7, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "07", "BLUE", 7, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde7", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 7, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "Dividend Amount...:", "BLUE", 8, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_1", pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(1), true, 8, 22, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_2", pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(2), true, 8, 37, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_3", pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(3), true, 8, 52, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_4", pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(4), true, 8, 67, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time8", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 8, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde8", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 8, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr8", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 8, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number8", pnd_Ws_Pnd_Page_Number, true, 8, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "08", "BLUE", 8, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde8", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 8, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "Units.............:", "BLUE", 9, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_1", pnd_Ws_Pnd_Inv_Acct_Unit_Qty.getValue(1), true, 9, 23, 12, "YELLOW", "-ZZZ,ZZ9.999", 
                true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_2", pnd_Ws_Pnd_Inv_Acct_Unit_Qty.getValue(2), true, 9, 38, 12, "YELLOW", "-ZZZ,ZZ9.999", 
                true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_3", pnd_Ws_Pnd_Inv_Acct_Unit_Qty.getValue(3), true, 9, 53, 12, "YELLOW", "-ZZZ,ZZ9.999", 
                true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_4", pnd_Ws_Pnd_Inv_Acct_Unit_Qty.getValue(4), true, 9, 68, 12, "YELLOW", "-ZZZ,ZZ9.999", 
                true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time9", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 9, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde9", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 9, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr9", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 9, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number9", pnd_Ws_Pnd_Page_Number, true, 9, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "09", "BLUE", 9, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde9", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 9, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "Unit Value........:", "BLUE", 10, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_1", pnd_Ws_Pnd_Inv_Acct_Unit_Value.getValue(1), true, 10, 23, 12, "YELLOW", "-ZZ,ZZ9.9999", 
                true, true, pnd_Inv_Acct_Unit_Value_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_2", pnd_Ws_Pnd_Inv_Acct_Unit_Value.getValue(2), true, 10, 38, 12, "YELLOW", "-ZZ,ZZ9.9999", 
                true, true, pnd_Inv_Acct_Unit_Value_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_3", pnd_Ws_Pnd_Inv_Acct_Unit_Value.getValue(3), true, 10, 53, 12, "YELLOW", "-ZZ,ZZ9.9999", 
                true, true, pnd_Inv_Acct_Unit_Value_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_4", pnd_Ws_Pnd_Inv_Acct_Unit_Value.getValue(4), true, 10, 68, 12, "YELLOW", "-ZZ,ZZ9.9999", 
                true, true, pnd_Inv_Acct_Unit_Value_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time10", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 10, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde10", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 10, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr10", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 10, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number10", pnd_Ws_Pnd_Page_Number, true, 10, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "10", "BLUE", 10, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde10", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 10, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "DPI/DCI Amount....:", "BLUE", 11, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_1", pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt.getValue(1), true, 11, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dpi_Dci_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_2", pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt.getValue(2), true, 11, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dpi_Dci_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_3", pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt.getValue(3), true, 11, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dpi_Dci_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_4", pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt.getValue(4), true, 11, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dpi_Dci_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time11", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 11, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde11", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 11, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr11", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 11, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number11", pnd_Ws_Pnd_Page_Number, true, 11, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "11", "BLUE", 11, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde11", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 11, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "Gross Amount......:", "BLUE", 12, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Gross_Amt_1", pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(1), true, 12, 22, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Gross_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Gross_Amt_2", pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(2), true, 12, 37, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Gross_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Gross_Amt_3", pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(3), true, 12, 52, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Gross_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Gross_Amt_4", pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(4), true, 12, 67, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Gross_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time12", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 12, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde12", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 12, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr12", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 12, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number12", pnd_Ws_Pnd_Page_Number, true, 12, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "12", "BLUE", 12, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde12", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 12, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "IVC Amount........:", "BLUE", 13, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_1", pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(1), true, 13, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_2", pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(2), true, 13, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_3", pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(3), true, 13, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_4", pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(4), true, 13, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time13", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 13, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde13", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 13, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr13", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 13, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number13", pnd_Ws_Pnd_Page_Number, true, 13, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "13", "BLUE", 13, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde13", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 13, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "Taxable Amount....:", "BLUE", 14, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Txble_Amt_1", pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(1), true, 14, 22, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Txble_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Txble_Amt_2", pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(2), true, 14, 37, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Txble_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Txble_Amt_3", pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(3), true, 14, 52, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Txble_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Txble_Amt_4", pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(4), true, 14, 67, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Txble_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time14", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 14, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde14", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 14, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr14", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 14, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number14", pnd_Ws_Pnd_Page_Number, true, 14, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "14", "BLUE", 14, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde14", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 14, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "Federal Tax(", "BLUE", 15, 2, 12);
            uiForm.setUiControl("pnd_Ws_Pnd_Fed_Display", pnd_Ws_Pnd_Fed_Display, true, 15, 15, 3, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_28", "):", "BLUE", 15, 19, 2);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_1", pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(1), true, 15, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Fdrl_Tax_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_2", pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(2), true, 15, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Fdrl_Tax_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_3", pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(3), true, 15, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Fdrl_Tax_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_4", pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(4), true, 15, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Fdrl_Tax_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time15", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 15, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde15", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 15, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr15", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 15, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number15", pnd_Ws_Pnd_Page_Number, true, 15, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "15", "BLUE", 15, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde15", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 15, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "State Tax..(", "BLUE", 16, 2, 12);
            uiForm.setUiControl("pnd_Ws_Pnd_State_Disp_X", pnd_Ws_Pnd_State_Disp_X, true, 16, 15, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_31", ").:", "BLUE", 16, 18, 3);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_State_Tax_1", pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(1), true, 16, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_State_Tax_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_State_Tax_2", pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(2), true, 16, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_State_Tax_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_State_Tax_3", pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(3), true, 16, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_State_Tax_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_State_Tax_4", pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(4), true, 16, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_State_Tax_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time16", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 16, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde16", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 16, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr16", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 16, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number16", pnd_Ws_Pnd_Page_Number, true, 16, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "16", "BLUE", 16, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde16", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 16, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "Local Tax.........:", "BLUE", 17, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Local_Tax_1", pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(1), true, 17, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Local_Tax_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Local_Tax_2", pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(2), true, 17, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Local_Tax_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Local_Tax_3", pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(3), true, 17, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Local_Tax_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Local_Tax_4", pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(4), true, 17, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Local_Tax_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time17", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 17, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde17", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 17, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr17", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 17, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number17", pnd_Ws_Pnd_Page_Number, true, 17, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "17", "BLUE", 17, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde17", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 17, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "Total Taxes.......:", "BLUE", 18, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Total_Tax_1", pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(1), true, 18, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Total_Tax_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Total_Tax_2", pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(2), true, 18, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Total_Tax_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Total_Tax_3", pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(3), true, 18, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Total_Tax_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Total_Tax_4", pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(4), true, 18, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Total_Tax_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time18", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 18, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde18", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 18, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr18", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 18, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number18", pnd_Ws_Pnd_Page_Number, true, 18, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "18", "BLUE", 18, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde18", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 18, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "Net Payment Amount:", "BLUE", 19, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_1", pnd_Ws_Pnd_Inv_Acct_Net_Pymnt.getValue(1), true, 19, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Net_Pymnt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_2", pnd_Ws_Pnd_Inv_Acct_Net_Pymnt.getValue(2), true, 19, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Net_Pymnt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_3", pnd_Ws_Pnd_Inv_Acct_Net_Pymnt.getValue(3), true, 19, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Net_Pymnt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_4", pnd_Ws_Pnd_Inv_Acct_Net_Pymnt.getValue(4), true, 19, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Net_Pymnt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time19", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 19, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde19", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 19, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr19", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 19, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number19", pnd_Ws_Pnd_Page_Number, true, 19, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "19", "BLUE", 19, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde19", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 19, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time20", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 20, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde20", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 20, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr20", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 20, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number20", pnd_Ws_Pnd_Page_Number, true, 20, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_39", "20", "BLUE", 20, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde20", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 20, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
