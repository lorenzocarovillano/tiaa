/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:01 PM
**        *   FROM NATURAL MAP   :  Fcpm872d
************************************************************
**        * FILE NAME               : Fcpm872d.java
**        * CLASS NAME              : Fcpm872d
**        * INSTANCE NAME           : Fcpm872d
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #PAGE-NUMBER #PMNT-TAX.TAX-FED-FILING-STAT-DESC                                                                          *     #PMNT-TAX.TAX-LOC-FILING-STAT-DESC 
    *     #PMNT-TAX.TAX-STA-FILING-STAT-DESC #PROGRAM #RUN-TIME                                                                    *     #SCREEN-HEADER 
    #WS.#FUND-DISPLAY #WS.#IVC-IND                                                                            *     EXT.PYMNT-TAX-FORM-1001 EXT.PYMNT-TAX-FORM-1078 
    *     PYMNT-TAX.CNTRCT-NBR PYMNT-TAX.TAX-FED-ALLOW-CNT                                                                         *     PYMNT-TAX.TAX-FED-ELEC-OPTION 
    PYMNT-TAX.TAX-FED-FIXED-AMT                                                                *     PYMNT-TAX.TAX-FED-FIXED-PCT PYMNT-TAX.TAX-LOC-ALLOW-CNT 
    *     PYMNT-TAX.TAX-LOC-BLIND-CNT PYMNT-TAX.TAX-LOC-ELEC-OPTION                                                                *     PYMNT-TAX.TAX-LOC-FIXED-AMT 
    PYMNT-TAX.TAX-LOC-FIXED-PCT                                                                  *     PYMNT-TAX.TAX-LOC-SPOUSE-CNT PYMNT-TAX.TAX-STA-ALLOW-CNT 
    *     PYMNT-TAX.TAX-STA-BLIND-CNT PYMNT-TAX.TAX-STA-ELEC-OPTION                                                                *     PYMNT-TAX.TAX-STA-FIXED-AMT 
    PYMNT-TAX.TAX-STA-FIXED-PCT                                                                  *     PYMNT-TAX.TAX-STA-SPOUSE-CNT
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm872d extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Page_Number;
    private DbsField pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc;
    private DbsField pnd_Pmnt_Tax_Tax_Loc_Filing_Stat_Desc;
    private DbsField pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc;
    private DbsField pnd_Program;
    private DbsField pnd_Run_Time;
    private DbsField pnd_Screen_Header;
    private DbsField pnd_Ws_Pnd_Fund_Display;
    private DbsField pnd_Ws_Pnd_Ivc_Ind;
    private DbsField ext_Pymnt_Tax_Form_1001;
    private DbsField ext_Pymnt_Tax_Form_1078;
    private DbsField pymnt_Tax_Cntrct_Nbr;
    private DbsField pymnt_Tax_Tax_Fed_Allow_Cnt;
    private DbsField pymnt_Tax_Tax_Fed_Elec_Option;
    private DbsField pymnt_Tax_Tax_Fed_Fixed_Amt;
    private DbsField pymnt_Tax_Tax_Fed_Fixed_Pct;
    private DbsField pymnt_Tax_Tax_Loc_Allow_Cnt;
    private DbsField pymnt_Tax_Tax_Loc_Blind_Cnt;
    private DbsField pymnt_Tax_Tax_Loc_Elec_Option;
    private DbsField pymnt_Tax_Tax_Loc_Fixed_Amt;
    private DbsField pymnt_Tax_Tax_Loc_Fixed_Pct;
    private DbsField pymnt_Tax_Tax_Loc_Spouse_Cnt;
    private DbsField pymnt_Tax_Tax_Sta_Allow_Cnt;
    private DbsField pymnt_Tax_Tax_Sta_Blind_Cnt;
    private DbsField pymnt_Tax_Tax_Sta_Elec_Option;
    private DbsField pymnt_Tax_Tax_Sta_Fixed_Amt;
    private DbsField pymnt_Tax_Tax_Sta_Fixed_Pct;
    private DbsField pymnt_Tax_Tax_Sta_Spouse_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc = parameters.newFieldInRecord("pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc", "#PMNT-TAX.TAX-FED-FILING-STAT-DESC", 
            FieldType.STRING, 3);
        pnd_Pmnt_Tax_Tax_Loc_Filing_Stat_Desc = parameters.newFieldInRecord("pnd_Pmnt_Tax_Tax_Loc_Filing_Stat_Desc", "#PMNT-TAX.TAX-LOC-FILING-STAT-DESC", 
            FieldType.STRING, 3);
        pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc = parameters.newFieldInRecord("pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc", "#PMNT-TAX.TAX-STA-FILING-STAT-DESC", 
            FieldType.STRING, 3);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Run_Time = parameters.newFieldInRecord("pnd_Run_Time", "#RUN-TIME", FieldType.TIME);
        pnd_Screen_Header = parameters.newFieldInRecord("pnd_Screen_Header", "#SCREEN-HEADER", FieldType.STRING, 35);
        pnd_Ws_Pnd_Fund_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Fund_Display", "#WS.#FUND-DISPLAY", FieldType.STRING, 60);
        pnd_Ws_Pnd_Ivc_Ind = parameters.newFieldInRecord("pnd_Ws_Pnd_Ivc_Ind", "#WS.#IVC-IND", FieldType.BOOLEAN, 1);
        ext_Pymnt_Tax_Form_1001 = parameters.newFieldInRecord("ext_Pymnt_Tax_Form_1001", "EXT.PYMNT-TAX-FORM-1001", FieldType.STRING, 1);
        ext_Pymnt_Tax_Form_1078 = parameters.newFieldInRecord("ext_Pymnt_Tax_Form_1078", "EXT.PYMNT-TAX-FORM-1078", FieldType.STRING, 1);
        pymnt_Tax_Cntrct_Nbr = parameters.newFieldInRecord("pymnt_Tax_Cntrct_Nbr", "PYMNT-TAX.CNTRCT-NBR", FieldType.STRING, 10);
        pymnt_Tax_Tax_Fed_Allow_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Fed_Allow_Cnt", "PYMNT-TAX.TAX-FED-ALLOW-CNT", FieldType.NUMERIC, 2);
        pymnt_Tax_Tax_Fed_Elec_Option = parameters.newFieldInRecord("pymnt_Tax_Tax_Fed_Elec_Option", "PYMNT-TAX.TAX-FED-ELEC-OPTION", FieldType.STRING, 
            1);
        pymnt_Tax_Tax_Fed_Fixed_Amt = parameters.newFieldInRecord("pymnt_Tax_Tax_Fed_Fixed_Amt", "PYMNT-TAX.TAX-FED-FIXED-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pymnt_Tax_Tax_Fed_Fixed_Pct = parameters.newFieldInRecord("pymnt_Tax_Tax_Fed_Fixed_Pct", "PYMNT-TAX.TAX-FED-FIXED-PCT", FieldType.PACKED_DECIMAL, 
            6, 2);
        pymnt_Tax_Tax_Loc_Allow_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Allow_Cnt", "PYMNT-TAX.TAX-LOC-ALLOW-CNT", FieldType.NUMERIC, 2);
        pymnt_Tax_Tax_Loc_Blind_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Blind_Cnt", "PYMNT-TAX.TAX-LOC-BLIND-CNT", FieldType.NUMERIC, 2);
        pymnt_Tax_Tax_Loc_Elec_Option = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Elec_Option", "PYMNT-TAX.TAX-LOC-ELEC-OPTION", FieldType.STRING, 
            1);
        pymnt_Tax_Tax_Loc_Fixed_Amt = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Fixed_Amt", "PYMNT-TAX.TAX-LOC-FIXED-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pymnt_Tax_Tax_Loc_Fixed_Pct = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Fixed_Pct", "PYMNT-TAX.TAX-LOC-FIXED-PCT", FieldType.PACKED_DECIMAL, 
            6, 2);
        pymnt_Tax_Tax_Loc_Spouse_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Loc_Spouse_Cnt", "PYMNT-TAX.TAX-LOC-SPOUSE-CNT", FieldType.NUMERIC, 
            2);
        pymnt_Tax_Tax_Sta_Allow_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Allow_Cnt", "PYMNT-TAX.TAX-STA-ALLOW-CNT", FieldType.NUMERIC, 2);
        pymnt_Tax_Tax_Sta_Blind_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Blind_Cnt", "PYMNT-TAX.TAX-STA-BLIND-CNT", FieldType.NUMERIC, 2);
        pymnt_Tax_Tax_Sta_Elec_Option = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Elec_Option", "PYMNT-TAX.TAX-STA-ELEC-OPTION", FieldType.STRING, 
            1);
        pymnt_Tax_Tax_Sta_Fixed_Amt = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Fixed_Amt", "PYMNT-TAX.TAX-STA-FIXED-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pymnt_Tax_Tax_Sta_Fixed_Pct = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Fixed_Pct", "PYMNT-TAX.TAX-STA-FIXED-PCT", FieldType.PACKED_DECIMAL, 
            6, 2);
        pymnt_Tax_Tax_Sta_Spouse_Cnt = parameters.newFieldInRecord("pymnt_Tax_Tax_Sta_Spouse_Cnt", "PYMNT-TAX.TAX-STA-SPOUSE-CNT", FieldType.NUMERIC, 
            2);
        parameters.reset();
    }

    public Fcpm872d() throws Exception
    {
        super("Fcpm872d");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=019 LS=081 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm872d", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm872d"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Run_Time", pnd_Run_Time, true, 1, 2, 20, "YELLOW", "MM/DD/YYYY' '-' 'HH:IIAP", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "PAGE:", "BLUE", 1, 68, 5);
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 74, 6, "YELLOW", "ZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("astINIT_USER", Global.getINIT_USER(), true, 2, 2, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "-", "BLUE", 2, 13, 1);
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 2, 15, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Screen_Header", pnd_Screen_Header, true, 3, 24, 35, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Fund_Display", pnd_Ws_Pnd_Fund_Display, true, 4, 11, 60, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "Tax Information", "BLUE", 6, 33, 15);
            uiForm.setUiLabel("label_4", "---------------", "BLUE", 7, 33, 15);
            uiForm.setUiLabel("label_5", "IVC Recovery Type....:", "BLUE", 8, 1, 22);
            uiForm.setUiControl("pnd_Ws_Pnd_Ivc_Ind", pnd_Ws_Pnd_Ivc_Ind, true, 8, 24, 7, "YELLOW", "UNKNOWN/KNOWN", true, false, null, null, "AD=ILOFHW' '~TG=KNOWNUNKNOWN", 
                ' ');
            uiForm.setUiLabel("label_6", "Policy Number:", "BLUE", 8, 55, 14);
            uiForm.setUiControl("pymnt_Tax_Cntrct_Nbr", pymnt_Tax_Cntrct_Nbr, true, 8, 70, 10, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_7", "Tax Form 1078 Receipt:", "BLUE", 9, 1, 22);
            uiForm.setUiControl("ext_Pymnt_Tax_Form_1078", ext_Pymnt_Tax_Form_1078, true, 9, 24, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_8", "Tax Form 1001 Receipt:", "BLUE", 10, 1, 22);
            uiForm.setUiControl("ext_Pymnt_Tax_Form_1001", ext_Pymnt_Tax_Form_1001, true, 10, 24, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_9", "Federal", "BLUE", 11, 32, 7);
            uiForm.setUiLabel("label_10", "State", "BLUE", 11, 50, 5);
            uiForm.setUiLabel("label_11", "Local", "BLUE", 11, 66, 5);
            uiForm.setUiLabel("label_12", "--------------- --------------- ---------------", "BLUE", 12, 29, 47);
            uiForm.setUiLabel("label_13", "Tax Election Response.....:", "BLUE", 13, 1, 27);
            uiForm.setUiControl("pymnt_Tax_Tax_Fed_Elec_Option", pymnt_Tax_Tax_Fed_Elec_Option, true, 13, 38, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Elec_Option", pymnt_Tax_Tax_Sta_Elec_Option, true, 13, 54, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Elec_Option", pymnt_Tax_Tax_Loc_Elec_Option, true, 13, 70, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_14", "Filing Status.............:", "BLUE", 14, 1, 27);
            uiForm.setUiControl("pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc", pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc, true, 14, 38, 3, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc", pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc, true, 14, 54, 3, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pmnt_Tax_Tax_Loc_Filing_Stat_Desc", pnd_Pmnt_Tax_Tax_Loc_Filing_Stat_Desc, true, 14, 70, 3, "YELLOW", true, false, 
                null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "Allowance Count...........:", "BLUE", 15, 1, 27);
            uiForm.setUiControl("pymnt_Tax_Tax_Fed_Allow_Cnt", pymnt_Tax_Tax_Fed_Allow_Cnt, true, 15, 37, 2, "YELLOW", "Z9", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Allow_Cnt", pymnt_Tax_Tax_Sta_Allow_Cnt, true, 15, 53, 2, "YELLOW", "Z9", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Allow_Cnt", pymnt_Tax_Tax_Loc_Allow_Cnt, true, 15, 69, 2, "YELLOW", "Z9", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "Addl/Fixed Withholding Amt:", "BLUE", 16, 1, 27);
            uiForm.setUiControl("pymnt_Tax_Tax_Fed_Fixed_Amt", pymnt_Tax_Tax_Fed_Fixed_Amt, true, 16, 29, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Fixed_Amt", pymnt_Tax_Tax_Sta_Fixed_Amt, true, 16, 45, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Fixed_Amt", pymnt_Tax_Tax_Loc_Fixed_Amt, true, 16, 61, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "Addl/Fixed Withholding Pct:", "BLUE", 17, 1, 27);
            uiForm.setUiControl("pymnt_Tax_Tax_Fed_Fixed_Pct", pymnt_Tax_Tax_Fed_Fixed_Pct, true, 17, 35, 9, "YELLOW", "-ZZ9.9999", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Fixed_Pct", pymnt_Tax_Tax_Sta_Fixed_Pct, true, 17, 51, 9, "YELLOW", "-ZZ9.9999", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Fixed_Pct", pymnt_Tax_Tax_Loc_Fixed_Pct, true, 17, 67, 9, "YELLOW", "-ZZ9.9999", true, true, null, 
                "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "Allowance Spouse Count....:", "BLUE", 18, 1, 27);
            uiForm.setUiLabel("label_19", "N/A", "BLUE", 18, 36, 3);
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Spouse_Cnt", pymnt_Tax_Tax_Sta_Spouse_Cnt, true, 18, 53, 2, "YELLOW", "99", true, false, null, "0123456789+-, ", 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Spouse_Cnt", pymnt_Tax_Tax_Loc_Spouse_Cnt, true, 18, 69, 2, "YELLOW", "99", true, false, null, "0123456789+-, ", 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "Allowance Blind Count.....:", "BLUE", 19, 1, 27);
            uiForm.setUiLabel("label_21", "N/A", "BLUE", 19, 36, 3);
            uiForm.setUiControl("pymnt_Tax_Tax_Sta_Blind_Cnt", pymnt_Tax_Tax_Sta_Blind_Cnt, true, 19, 53, 2, "YELLOW", "99", true, false, null, "0123456789+-, ", 
                "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pymnt_Tax_Tax_Loc_Blind_Cnt", pymnt_Tax_Tax_Loc_Blind_Cnt, true, 19, 69, 2, "YELLOW", "99", true, false, null, "0123456789+-, ", 
                "AD=I?OFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
