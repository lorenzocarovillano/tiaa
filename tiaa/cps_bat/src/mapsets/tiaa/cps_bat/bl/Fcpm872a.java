/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:49:59 PM
**        *   FROM NATURAL MAP   :  Fcpm872a
************************************************************
**        * FILE NAME               : Fcpm872a.java
**        * CLASS NAME              : Fcpm872a
**        * INSTANCE NAME           : Fcpm872a
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #INV-ACCT-CDE-DESC-CV(*) #INV-ACCT-CNTRCT-AMT-CV(*)                                                                      *     #INV-ACCT-DPI-DCI-AMT-CV(*) 
    #INV-ACCT-DVDND-AMT-CV(*)                                                                    *     #INV-ACCT-FDRL-TAX-CV(*) #INV-ACCT-GROSS-AMT-CV(*) 
    *     #INV-ACCT-IVC-AMT-CV(*) #INV-ACCT-LOCAL-TAX-CV(*)                                                                        *     #INV-ACCT-NET-PYMNT-CV(*) 
    #INV-ACCT-SETTL-AMT-CV(*)                                                                      *     #INV-ACCT-STATE-TAX-CV(*) #INV-ACCT-TOTAL-TAX-CV(*) 
    *     #INV-ACCT-TXBLE-AMT-CV(*) #INV-ACCT-UNIT-QTY-CV(*)                                                                       *     #INV-ACCT-UNIT-VALUE-CV(*) 
    #PAGE-NUMBER #PROGRAM #RUN-TIME                                                               *     #SCREEN-HEADER #WS.#EFFECTIVE-DTE #WS.#FED-DISPLAY 
    *     #WS.#FUND-DISPLAY #WS.#GROSS-AMT-DISP #WS.#INV-ACCT-CDE-DESC(*)                                                          *     #WS.#INV-ACCT-CNTRCT-AMT(*) 
    #WS.#INV-ACCT-DPI-DCI-AMT(*)                                                                 *     #WS.#INV-ACCT-DVDND-AMT(*) #WS.#INV-ACCT-FDRL-TAX(*) 
    *     #WS.#INV-ACCT-GROSS-AMT(*) #WS.#INV-ACCT-IVC-AMT(*)                                                                      *     #WS.#INV-ACCT-LOCAL-TAX(*) 
    #WS.#INV-ACCT-NET-PYMNT(*)                                                                    *     #WS.#INV-ACCT-SETTL-AMT(*) #WS.#INV-ACCT-STATE-TAX(*) 
    *     #WS.#INV-ACCT-TOTAL-TAX(*) #WS.#INV-ACCT-TXBLE-AMT(*)                                                                    *     #WS.#INV-ACCT-UNIT-QTY(*) 
    #WS.#INV-ACCT-UNIT-VALUE(*)                                                                    *     #WS.#STATE-DISP-X EXT.PYMNT-SETTLMNT-DTE
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm872a extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Inv_Acct_Cde_Desc_Cv;
    private DbsField pnd_Inv_Acct_Cntrct_Amt_Cv;
    private DbsField pnd_Inv_Acct_Dpi_Dci_Amt_Cv;
    private DbsField pnd_Inv_Acct_Dvdnd_Amt_Cv;
    private DbsField pnd_Inv_Acct_Fdrl_Tax_Cv;
    private DbsField pnd_Inv_Acct_Gross_Amt_Cv;
    private DbsField pnd_Inv_Acct_Ivc_Amt_Cv;
    private DbsField pnd_Inv_Acct_Local_Tax_Cv;
    private DbsField pnd_Inv_Acct_Net_Pymnt_Cv;
    private DbsField pnd_Inv_Acct_Settl_Amt_Cv;
    private DbsField pnd_Inv_Acct_State_Tax_Cv;
    private DbsField pnd_Inv_Acct_Total_Tax_Cv;
    private DbsField pnd_Inv_Acct_Txble_Amt_Cv;
    private DbsField pnd_Inv_Acct_Unit_Qty_Cv;
    private DbsField pnd_Inv_Acct_Unit_Value_Cv;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Program;
    private DbsField pnd_Run_Time;
    private DbsField pnd_Screen_Header;
    private DbsField pnd_Ws_Pnd_Effective_Dte;
    private DbsField pnd_Ws_Pnd_Fed_Display;
    private DbsField pnd_Ws_Pnd_Fund_Display;
    private DbsField pnd_Ws_Pnd_Gross_Amt_Disp;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cde_Desc;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Local_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Net_Pymnt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_State_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Total_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Txble_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Pnd_State_Disp_X;
    private DbsField ext_Pymnt_Settlmnt_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Inv_Acct_Cde_Desc_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cde_Desc_Cv", "#INV-ACCT-CDE-DESC-CV", FieldType.ATTRIBUTE_CONTROL, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Cntrct_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cntrct_Amt_Cv", "#INV-ACCT-CNTRCT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Dpi_Dci_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dpi_Dci_Amt_Cv", "#INV-ACCT-DPI-DCI-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Dvdnd_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dvdnd_Amt_Cv", "#INV-ACCT-DVDND-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Fdrl_Tax_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Fdrl_Tax_Cv", "#INV-ACCT-FDRL-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Gross_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Gross_Amt_Cv", "#INV-ACCT-GROSS-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Ivc_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Ivc_Amt_Cv", "#INV-ACCT-IVC-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Local_Tax_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Local_Tax_Cv", "#INV-ACCT-LOCAL-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Net_Pymnt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Net_Pymnt_Cv", "#INV-ACCT-NET-PYMNT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Settl_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Settl_Amt_Cv", "#INV-ACCT-SETTL-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_State_Tax_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_State_Tax_Cv", "#INV-ACCT-STATE-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Total_Tax_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Total_Tax_Cv", "#INV-ACCT-TOTAL-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Txble_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Txble_Amt_Cv", "#INV-ACCT-TXBLE-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Unit_Qty_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Qty_Cv", "#INV-ACCT-UNIT-QTY-CV", FieldType.ATTRIBUTE_CONTROL, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Unit_Value_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Value_Cv", "#INV-ACCT-UNIT-VALUE-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Run_Time = parameters.newFieldInRecord("pnd_Run_Time", "#RUN-TIME", FieldType.TIME);
        pnd_Screen_Header = parameters.newFieldInRecord("pnd_Screen_Header", "#SCREEN-HEADER", FieldType.STRING, 35);
        pnd_Ws_Pnd_Effective_Dte = parameters.newFieldInRecord("pnd_Ws_Pnd_Effective_Dte", "#WS.#EFFECTIVE-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Fed_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Fed_Display", "#WS.#FED-DISPLAY", FieldType.STRING, 3);
        pnd_Ws_Pnd_Fund_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Fund_Display", "#WS.#FUND-DISPLAY", FieldType.STRING, 60);
        pnd_Ws_Pnd_Gross_Amt_Disp = parameters.newFieldInRecord("pnd_Ws_Pnd_Gross_Amt_Disp", "#WS.#GROSS-AMT-DISP", FieldType.STRING, 19);
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Cde_Desc", "#WS.#INV-ACCT-CDE-DESC", FieldType.STRING, 11, 
            new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt", "#WS.#INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt", "#WS.#INV-ACCT-DPI-DCI-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt", "#WS.#INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax", "#WS.#INV-ACCT-FDRL-TAX", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Gross_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Gross_Amt", "#WS.#INV-ACCT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Ivc_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt", "#WS.#INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Local_Tax = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Local_Tax", "#WS.#INV-ACCT-LOCAL-TAX", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Net_Pymnt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt", "#WS.#INV-ACCT-NET-PYMNT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Settl_Amt", "#WS.#INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_State_Tax = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_State_Tax", "#WS.#INV-ACCT-STATE-TAX", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Total_Tax = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Total_Tax", "#WS.#INV-ACCT-TOTAL-TAX", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Txble_Amt = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Txble_Amt", "#WS.#INV-ACCT-TXBLE-AMT", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Unit_Qty = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Unit_Qty", "#WS.#INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            10, 3, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Unit_Value = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Inv_Acct_Unit_Value", "#WS.#INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            10, 4, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_State_Disp_X = parameters.newFieldInRecord("pnd_Ws_Pnd_State_Disp_X", "#WS.#STATE-DISP-X", FieldType.STRING, 2);
        ext_Pymnt_Settlmnt_Dte = parameters.newFieldInRecord("ext_Pymnt_Settlmnt_Dte", "EXT.PYMNT-SETTLMNT-DTE", FieldType.DATE);
        parameters.reset();
    }

    public Fcpm872a() throws Exception
    {
        super("Fcpm872a");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=022 LS=081 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm872a", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm872a"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Run_Time", pnd_Run_Time, true, 1, 2, 20, "YELLOW", "MM/DD/YYYY' '-' 'HH:IIAP", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "PAGE:", "BLUE", 1, 68, 5);
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 74, 6, "YELLOW", "ZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("astINIT_USER", Global.getINIT_USER(), true, 2, 2, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "-", "BLUE", 2, 13, 1);
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 2, 15, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Screen_Header", pnd_Screen_Header, true, 3, 24, 35, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Fund_Display", pnd_Ws_Pnd_Fund_Display, true, 4, 11, 60, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "Payment Due Date..:", "BLUE", 6, 2, 19);
            uiForm.setUiControl("ext_Pymnt_Settlmnt_Dte", ext_Pymnt_Settlmnt_Dte, true, 6, 22, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "Effective", "BLUE", 6, 53, 9);
            uiForm.setUiLabel("label_5", "Date:", "BLUE", 6, 64, 5);
            uiForm.setUiControl("pnd_Ws_Pnd_Effective_Dte", pnd_Ws_Pnd_Effective_Dte, true, 6, 70, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "Investment Account:", "BLUE", 7, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cde_Desc_1", pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(1), true, 7, 24, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(1), 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cde_Desc_2", pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(2), true, 7, 39, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(2), 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cde_Desc_3", pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(3), true, 7, 54, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(3), 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cde_Desc_4", pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(4), true, 7, 69, 11, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(4), 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Gross_Amt_Disp", pnd_Ws_Pnd_Gross_Amt_Disp, true, 8, 2, 19, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Settl_Amt_1", pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(1), true, 8, 22, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Settl_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Settl_Amt_2", pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(2), true, 8, 37, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Settl_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Settl_Amt_3", pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(3), true, 8, 52, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Settl_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Settl_Amt_4", pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(4), true, 8, 67, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Settl_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "Contractual Amount:", "BLUE", 9, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_1", pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt.getValue(1), true, 9, 22, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_2", pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt.getValue(2), true, 9, 37, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_3", pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt.getValue(3), true, 9, 52, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_4", pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt.getValue(4), true, 9, 67, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "Dividend Amount...:", "BLUE", 10, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_1", pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(1), true, 10, 22, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_2", pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(2), true, 10, 37, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_3", pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(3), true, 10, 52, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_4", pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(4), true, 10, 67, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "Units.............:", "BLUE", 11, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_1", pnd_Ws_Pnd_Inv_Acct_Unit_Qty.getValue(1), true, 11, 23, 12, "YELLOW", "-ZZZ,ZZ9.999", 
                true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_2", pnd_Ws_Pnd_Inv_Acct_Unit_Qty.getValue(2), true, 11, 38, 12, "YELLOW", "-ZZZ,ZZ9.999", 
                true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_3", pnd_Ws_Pnd_Inv_Acct_Unit_Qty.getValue(3), true, 11, 53, 12, "YELLOW", "-ZZZ,ZZ9.999", 
                true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_4", pnd_Ws_Pnd_Inv_Acct_Unit_Qty.getValue(4), true, 11, 68, 12, "YELLOW", "-ZZZ,ZZ9.999", 
                true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "Unit Value........:", "BLUE", 12, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_1", pnd_Ws_Pnd_Inv_Acct_Unit_Value.getValue(1), true, 12, 23, 12, "YELLOW", "-ZZ,ZZ9.9999", 
                true, true, pnd_Inv_Acct_Unit_Value_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_2", pnd_Ws_Pnd_Inv_Acct_Unit_Value.getValue(2), true, 12, 38, 12, "YELLOW", "-ZZ,ZZ9.9999", 
                true, true, pnd_Inv_Acct_Unit_Value_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_3", pnd_Ws_Pnd_Inv_Acct_Unit_Value.getValue(3), true, 12, 53, 12, "YELLOW", "-ZZ,ZZ9.9999", 
                true, true, pnd_Inv_Acct_Unit_Value_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Unit_Value_4", pnd_Ws_Pnd_Inv_Acct_Unit_Value.getValue(4), true, 12, 68, 12, "YELLOW", "-ZZ,ZZ9.9999", 
                true, true, pnd_Inv_Acct_Unit_Value_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "DPI/DCI Amount....:", "BLUE", 13, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_1", pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt.getValue(1), true, 13, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dpi_Dci_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_2", pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt.getValue(2), true, 13, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dpi_Dci_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_3", pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt.getValue(3), true, 13, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dpi_Dci_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_4", pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt.getValue(4), true, 13, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Dpi_Dci_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "Gross Amount......:", "BLUE", 14, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Gross_Amt_1", pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(1), true, 14, 22, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Gross_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Gross_Amt_2", pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(2), true, 14, 37, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Gross_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Gross_Amt_3", pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(3), true, 14, 52, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Gross_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Gross_Amt_4", pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(4), true, 14, 67, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Gross_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "IVC Amount........:", "BLUE", 15, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_1", pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(1), true, 15, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_2", pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(2), true, 15, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_3", pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(3), true, 15, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_4", pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(4), true, 15, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "Taxable Amount....:", "BLUE", 16, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Txble_Amt_1", pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(1), true, 16, 22, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Txble_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Txble_Amt_2", pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(2), true, 16, 37, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Txble_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Txble_Amt_3", pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(3), true, 16, 52, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Txble_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Txble_Amt_4", pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(4), true, 16, 67, 13, "YELLOW", "-ZZZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Txble_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "Federal Tax(", "BLUE", 17, 2, 12);
            uiForm.setUiControl("pnd_Ws_Pnd_Fed_Display", pnd_Ws_Pnd_Fed_Display, true, 17, 15, 3, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_16", "):", "BLUE", 17, 19, 2);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_1", pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(1), true, 17, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Fdrl_Tax_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_2", pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(2), true, 17, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Fdrl_Tax_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_3", pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(3), true, 17, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Fdrl_Tax_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_4", pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(4), true, 17, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Fdrl_Tax_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "State Tax..(", "BLUE", 18, 2, 12);
            uiForm.setUiControl("pnd_Ws_Pnd_State_Disp_X", pnd_Ws_Pnd_State_Disp_X, true, 18, 15, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_18", ").:", "BLUE", 18, 18, 3);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_State_Tax_1", pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(1), true, 18, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_State_Tax_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_State_Tax_2", pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(2), true, 18, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_State_Tax_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_State_Tax_3", pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(3), true, 18, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_State_Tax_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_State_Tax_4", pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(4), true, 18, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_State_Tax_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "Local Tax.........:", "BLUE", 19, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Local_Tax_1", pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(1), true, 19, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Local_Tax_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Local_Tax_2", pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(2), true, 19, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Local_Tax_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Local_Tax_3", pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(3), true, 19, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Local_Tax_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Local_Tax_4", pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(4), true, 19, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Local_Tax_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "Total Taxes.......:", "BLUE", 20, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Total_Tax_1", pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(1), true, 20, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Total_Tax_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Total_Tax_2", pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(2), true, 20, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Total_Tax_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Total_Tax_3", pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(3), true, 20, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Total_Tax_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Total_Tax_4", pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(4), true, 20, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Total_Tax_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "Net Payment Amount:", "BLUE", 21, 2, 19);
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_1", pnd_Ws_Pnd_Inv_Acct_Net_Pymnt.getValue(1), true, 21, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Net_Pymnt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_2", pnd_Ws_Pnd_Inv_Acct_Net_Pymnt.getValue(2), true, 21, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Net_Pymnt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_3", pnd_Ws_Pnd_Inv_Acct_Net_Pymnt.getValue(3), true, 21, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Net_Pymnt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_4", pnd_Ws_Pnd_Inv_Acct_Net_Pymnt.getValue(4), true, 21, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, pnd_Inv_Acct_Net_Pymnt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
