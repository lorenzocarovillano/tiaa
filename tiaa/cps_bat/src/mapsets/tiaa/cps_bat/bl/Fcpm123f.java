/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:49:18 PM
**        *   FROM NATURAL MAP   :  Fcpm123f
************************************************************
**        * FILE NAME               : Fcpm123f.java
**        * CLASS NAME              : Fcpm123f
**        * INSTANCE NAME           : Fcpm123f
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #CHECK-NUMBER-A11 #CNTRCT-CANCEL-RDRW-ACTVTY-CDE #CNTRCT-ORGN-CDE                                                        *     #CNTRCT-UNQ-ID-NBR 
    #CSR-DISPLAY #CTZNSHP-DISPLAY                                                                         *     #CURRENCY-DISPLAY #ORGN-DISPLAY #PAGE-NUMBER 
    *     #PYMNT-CHK-SAV-IND-CV #PYMNT-CHK-SAV-IND-TEXT                                                                            *     #PYMNT-CHK-SAV-IND-TEXT-CV 
    #PYMNT-DISPLAY #PYMNT-EFT-ACCT-NBR-CV                                                         *     #PYMNT-EFT-ACCT-NBR-TEXT #PYMNT-EFT-ACCT-NBR-TEXT-CV 
    *     #PYMNT-EFT-TRANSIT-ID-CV #PYMNT-EFT-TRANSIT-ID-TEXT                                                                      *     #PYMNT-EFT-TRANSIT-ID-TEXT-CV 
    #PYMNT-REQST-LOG-DTE-TIME                                                                  *     #STATE-DISPLAY #TYPE-DISPLAY #WARRANT-EXTRACT.ANNT-SOC-SEC-NBR 
    *     #WARRANT-EXTRACT.CNTRCT-PAYEE-CDE                                                                                        *     #WARRANT-EXTRACT.CNTRCT-PPCN-NBR 
    *     #WARRANT-EXTRACT.CNTRCT-UNQ-ID-NBR                                                                                       *     #WARRANT-EXTRACT.PYMNT-ACCTG-DTE 
    *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE1-TXT(*)                                                                                 *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE2-TXT(*) 
    *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE3-TXT(*)                                                                                 *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE4-TXT(*) 
    *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE5-TXT(*)                                                                                 *     #WARRANT-EXTRACT.PYMNT-ADDR-LINE6-TXT(*) 
    *     #WARRANT-EXTRACT.PYMNT-CHECK-AMT #WARRANT-EXTRACT.PYMNT-CHECK-DTE                                                        *     #WARRANT-EXTRACT.PYMNT-CHECK-SEQ-NBR 
    *     #WARRANT-EXTRACT.PYMNT-CHK-SAV-IND                                                                                       *     #WARRANT-EXTRACT.PYMNT-EFT-ACCT-NBR 
    *     #WARRANT-EXTRACT.PYMNT-EFT-TRANSIT-ID                                                                                    *     #WARRANT-EXTRACT.PYMNT-INTRFCE-DTE 
    #WARRANT-EXTRACT.PYMNT-NME(*)                                                         *     #WARRANT-EXTRACT.PYMNT-SETTLMNT-DTE
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm123f extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Check_Number_A11;
    private DbsField pnd_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Csr_Display;
    private DbsField pnd_Ctznshp_Display;
    private DbsField pnd_Currency_Display;
    private DbsField pnd_Orgn_Display;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Pymnt_Chk_Sav_Ind_Cv;
    private DbsField pnd_Pymnt_Chk_Sav_Ind_Text;
    private DbsField pnd_Pymnt_Chk_Sav_Ind_Text_Cv;
    private DbsField pnd_Pymnt_Display;
    private DbsField pnd_Pymnt_Eft_Acct_Nbr_Cv;
    private DbsField pnd_Pymnt_Eft_Acct_Nbr_Text;
    private DbsField pnd_Pymnt_Eft_Acct_Nbr_Text_Cv;
    private DbsField pnd_Pymnt_Eft_Transit_Id_Cv;
    private DbsField pnd_Pymnt_Eft_Transit_Id_Text;
    private DbsField pnd_Pymnt_Eft_Transit_Id_Text_Cv;
    private DbsField pnd_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_State_Display;
    private DbsField pnd_Type_Display;
    private DbsField pnd_Warrant_Extract_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Warrant_Extract_Cntrct_Payee_Cde;
    private DbsField pnd_Warrant_Extract_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Acctg_Dte;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Warrant_Extract_Pymnt_Check_Amt;
    private DbsField pnd_Warrant_Extract_Pymnt_Check_Dte;
    private DbsField pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind;
    private DbsField pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Warrant_Extract_Pymnt_Eft_Transit_Id;
    private DbsField pnd_Warrant_Extract_Pymnt_Intrfce_Dte;
    private DbsField pnd_Warrant_Extract_Pymnt_Nme;
    private DbsField pnd_Warrant_Extract_Pymnt_Settlmnt_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Check_Number_A11 = parameters.newFieldInRecord("pnd_Check_Number_A11", "#CHECK-NUMBER-A11", FieldType.STRING, 11);
        pnd_Cntrct_Cancel_Rdrw_Actvty_Cde = parameters.newFieldInRecord("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde", "#CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2);
        pnd_Cntrct_Orgn_Cde = parameters.newFieldInRecord("pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Cntrct_Unq_Id_Nbr", "#CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Csr_Display = parameters.newFieldInRecord("pnd_Csr_Display", "#CSR-DISPLAY", FieldType.STRING, 18);
        pnd_Ctznshp_Display = parameters.newFieldInRecord("pnd_Ctznshp_Display", "#CTZNSHP-DISPLAY", FieldType.STRING, 9);
        pnd_Currency_Display = parameters.newFieldInRecord("pnd_Currency_Display", "#CURRENCY-DISPLAY", FieldType.STRING, 4);
        pnd_Orgn_Display = parameters.newFieldInRecord("pnd_Orgn_Display", "#ORGN-DISPLAY", FieldType.STRING, 43);
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Pymnt_Chk_Sav_Ind_Cv = parameters.newFieldInRecord("pnd_Pymnt_Chk_Sav_Ind_Cv", "#PYMNT-CHK-SAV-IND-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Pymnt_Chk_Sav_Ind_Text = parameters.newFieldInRecord("pnd_Pymnt_Chk_Sav_Ind_Text", "#PYMNT-CHK-SAV-IND-TEXT", FieldType.STRING, 14);
        pnd_Pymnt_Chk_Sav_Ind_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Chk_Sav_Ind_Text_Cv", "#PYMNT-CHK-SAV-IND-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Pymnt_Display = parameters.newFieldInRecord("pnd_Pymnt_Display", "#PYMNT-DISPLAY", FieldType.STRING, 11);
        pnd_Pymnt_Eft_Acct_Nbr_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Acct_Nbr_Cv", "#PYMNT-EFT-ACCT-NBR-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Pymnt_Eft_Acct_Nbr_Text = parameters.newFieldInRecord("pnd_Pymnt_Eft_Acct_Nbr_Text", "#PYMNT-EFT-ACCT-NBR-TEXT", FieldType.STRING, 8);
        pnd_Pymnt_Eft_Acct_Nbr_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Acct_Nbr_Text_Cv", "#PYMNT-EFT-ACCT-NBR-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Pymnt_Eft_Transit_Id_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Transit_Id_Cv", "#PYMNT-EFT-TRANSIT-ID-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Pymnt_Eft_Transit_Id_Text = parameters.newFieldInRecord("pnd_Pymnt_Eft_Transit_Id_Text", "#PYMNT-EFT-TRANSIT-ID-TEXT", FieldType.STRING, 16);
        pnd_Pymnt_Eft_Transit_Id_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Transit_Id_Text_Cv", "#PYMNT-EFT-TRANSIT-ID-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Pymnt_Reqst_Log_Dte_Time = parameters.newFieldInRecord("pnd_Pymnt_Reqst_Log_Dte_Time", "#PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_State_Display = parameters.newFieldInRecord("pnd_State_Display", "#STATE-DISPLAY", FieldType.STRING, 2);
        pnd_Type_Display = parameters.newFieldInRecord("pnd_Type_Display", "#TYPE-DISPLAY", FieldType.STRING, 56);
        pnd_Warrant_Extract_Annt_Soc_Sec_Nbr = parameters.newFieldInRecord("pnd_Warrant_Extract_Annt_Soc_Sec_Nbr", "#WARRANT-EXTRACT.ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Warrant_Extract_Cntrct_Payee_Cde = parameters.newFieldInRecord("pnd_Warrant_Extract_Cntrct_Payee_Cde", "#WARRANT-EXTRACT.CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Warrant_Extract_Cntrct_Ppcn_Nbr = parameters.newFieldInRecord("pnd_Warrant_Extract_Cntrct_Ppcn_Nbr", "#WARRANT-EXTRACT.CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr", "#WARRANT-EXTRACT.CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Warrant_Extract_Pymnt_Acctg_Dte = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Acctg_Dte", "#WARRANT-EXTRACT.PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE3-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE4-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE5-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt", "#WARRANT-EXTRACT.PYMNT-ADDR-LINE6-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Check_Amt = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Check_Amt", "#WARRANT-EXTRACT.PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Warrant_Extract_Pymnt_Check_Dte = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Check_Dte", "#WARRANT-EXTRACT.PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr", "#WARRANT-EXTRACT.PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind", "#WARRANT-EXTRACT.PYMNT-CHK-SAV-IND", 
            FieldType.STRING, 1);
        pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr", "#WARRANT-EXTRACT.PYMNT-EFT-ACCT-NBR", 
            FieldType.STRING, 21);
        pnd_Warrant_Extract_Pymnt_Eft_Transit_Id = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Eft_Transit_Id", "#WARRANT-EXTRACT.PYMNT-EFT-TRANSIT-ID", 
            FieldType.NUMERIC, 9);
        pnd_Warrant_Extract_Pymnt_Intrfce_Dte = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Intrfce_Dte", "#WARRANT-EXTRACT.PYMNT-INTRFCE-DTE", 
            FieldType.DATE);
        pnd_Warrant_Extract_Pymnt_Nme = parameters.newFieldArrayInRecord("pnd_Warrant_Extract_Pymnt_Nme", "#WARRANT-EXTRACT.PYMNT-NME", FieldType.STRING, 
            38, new DbsArrayController(1, 2));
        pnd_Warrant_Extract_Pymnt_Settlmnt_Dte = parameters.newFieldInRecord("pnd_Warrant_Extract_Pymnt_Settlmnt_Dte", "#WARRANT-EXTRACT.PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);
        parameters.reset();
    }

    public Fcpm123f() throws Exception
    {
        super("Fcpm123f");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=020 LS=124 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm123f", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm123f"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "INTERFACE DATE:", "BLUE", 1, 1, 15);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Intrfce_Dte", pnd_Warrant_Extract_Pymnt_Intrfce_Dte, true, 1, 17, 10, "YELLOW", "MM/DD/YYYY", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time", pnd_Pymnt_Reqst_Log_Dte_Time, true, 1, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde", pnd_Cntrct_Orgn_Cde, true, 1, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr", pnd_Cntrct_Unq_Id_Nbr, true, 1, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "01", "BLUE", 1, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 1, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time2", pnd_Pymnt_Reqst_Log_Dte_Time, true, 2, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde2", pnd_Cntrct_Orgn_Cde, true, 2, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr2", pnd_Cntrct_Unq_Id_Nbr, true, 2, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number2", pnd_Page_Number, true, 2, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "02", "BLUE", 2, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde2", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 2, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Orgn_Display", pnd_Orgn_Display, true, 3, 21, 43, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time3", pnd_Pymnt_Reqst_Log_Dte_Time, true, 3, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde3", pnd_Cntrct_Orgn_Cde, true, 3, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr3", pnd_Cntrct_Unq_Id_Nbr, true, 3, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number3", pnd_Page_Number, true, 3, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_4", "03", "BLUE", 3, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde3", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 3, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Type_Display", pnd_Type_Display, true, 4, 14, 56, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time4", pnd_Pymnt_Reqst_Log_Dte_Time, true, 4, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde4", pnd_Cntrct_Orgn_Cde, true, 4, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr4", pnd_Cntrct_Unq_Id_Nbr, true, 4, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number4", pnd_Page_Number, true, 4, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "04", "BLUE", 4, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde4", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 4, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Csr_Display", pnd_Csr_Display, true, 5, 36, 18, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time5", pnd_Pymnt_Reqst_Log_Dte_Time, true, 5, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde5", pnd_Cntrct_Orgn_Cde, true, 5, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr5", pnd_Cntrct_Unq_Id_Nbr, true, 5, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number5", pnd_Page_Number, true, 5, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_6", "05", "BLUE", 5, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde5", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 5, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time6", pnd_Pymnt_Reqst_Log_Dte_Time, true, 6, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde6", pnd_Cntrct_Orgn_Cde, true, 6, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr6", pnd_Cntrct_Unq_Id_Nbr, true, 6, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number6", pnd_Page_Number, true, 6, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_7", "06", "BLUE", 6, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde6", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 6, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "PIN :", "BLUE", 7, 1, 5);
            uiForm.setUiControl("pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr", pnd_Warrant_Extract_Cntrct_Unq_Id_Nbr, true, 7, 7, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "CONTRACT:", "BLUE", 7, 21, 9);
            uiForm.setUiControl("pnd_Warrant_Extract_Cntrct_Ppcn_Nbr", pnd_Warrant_Extract_Cntrct_Ppcn_Nbr, true, 7, 31, 11, "YELLOW", "XXXXXXX-XXX", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "PAYEE:", "BLUE", 7, 44, 6);
            uiForm.setUiControl("pnd_Warrant_Extract_Cntrct_Payee_Cde", pnd_Warrant_Extract_Cntrct_Payee_Cde, true, 7, 51, 4, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time7", pnd_Pymnt_Reqst_Log_Dte_Time, true, 7, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde7", pnd_Cntrct_Orgn_Cde, true, 7, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr7", pnd_Cntrct_Unq_Id_Nbr, true, 7, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number7", pnd_Page_Number, true, 7, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_11", "07", "BLUE", 7, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde7", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 7, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "SSN.:", "BLUE", 8, 1, 5);
            uiForm.setUiControl("pnd_Warrant_Extract_Annt_Soc_Sec_Nbr", pnd_Warrant_Extract_Annt_Soc_Sec_Nbr, true, 8, 7, 11, "YELLOW", "999-99-9999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time8", pnd_Pymnt_Reqst_Log_Dte_Time, true, 8, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde8", pnd_Cntrct_Orgn_Cde, true, 8, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr8", pnd_Cntrct_Unq_Id_Nbr, true, 8, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number8", pnd_Page_Number, true, 8, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_13", "08", "BLUE", 8, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde8", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 8, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "BENEFICIARY NAME AND ADDRESS:", "BLUE", 9, 2, 29);
            uiForm.setUiLabel("label_15", "PAYEE NAME AND ADDRESS:", "BLUE", 9, 42, 23);
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time9", pnd_Pymnt_Reqst_Log_Dte_Time, true, 9, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde9", pnd_Cntrct_Orgn_Cde, true, 9, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr9", pnd_Cntrct_Unq_Id_Nbr, true, 9, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number9", pnd_Page_Number, true, 9, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_16", "09", "BLUE", 9, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde9", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 9, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Nme_2", pnd_Warrant_Extract_Pymnt_Nme.getValue(2), true, 10, 2, 38, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Nme_1", pnd_Warrant_Extract_Pymnt_Nme.getValue(1), true, 10, 42, 38, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time10", pnd_Pymnt_Reqst_Log_Dte_Time, true, 10, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde10", pnd_Cntrct_Orgn_Cde, true, 10, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr10", pnd_Cntrct_Unq_Id_Nbr, true, 10, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number10", pnd_Page_Number, true, 10, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_17", "10", "BLUE", 10, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde10", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 10, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt.getValue(2), true, 11, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line1_Txt.getValue(1), true, 11, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time11", pnd_Pymnt_Reqst_Log_Dte_Time, true, 11, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde11", pnd_Cntrct_Orgn_Cde, true, 11, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr11", pnd_Cntrct_Unq_Id_Nbr, true, 11, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number11", pnd_Page_Number, true, 11, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_18", "11", "BLUE", 11, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde11", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 11, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt.getValue(2), true, 12, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line2_Txt.getValue(1), true, 12, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time12", pnd_Pymnt_Reqst_Log_Dte_Time, true, 12, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde12", pnd_Cntrct_Orgn_Cde, true, 12, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr12", pnd_Cntrct_Unq_Id_Nbr, true, 12, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number12", pnd_Page_Number, true, 12, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_19", "12", "BLUE", 12, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde12", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 12, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt.getValue(2), true, 13, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line3_Txt.getValue(1), true, 13, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time13", pnd_Pymnt_Reqst_Log_Dte_Time, true, 13, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde13", pnd_Cntrct_Orgn_Cde, true, 13, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr13", pnd_Cntrct_Unq_Id_Nbr, true, 13, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number13", pnd_Page_Number, true, 13, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_20", "13", "BLUE", 13, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde13", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 13, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt.getValue(2), true, 14, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line4_Txt.getValue(1), true, 14, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time14", pnd_Pymnt_Reqst_Log_Dte_Time, true, 14, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde14", pnd_Cntrct_Orgn_Cde, true, 14, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr14", pnd_Cntrct_Unq_Id_Nbr, true, 14, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number14", pnd_Page_Number, true, 14, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_21", "14", "BLUE", 14, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde14", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 14, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt.getValue(2), true, 15, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line5_Txt.getValue(1), true, 15, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time15", pnd_Pymnt_Reqst_Log_Dte_Time, true, 15, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde15", pnd_Cntrct_Orgn_Cde, true, 15, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr15", pnd_Cntrct_Unq_Id_Nbr, true, 15, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number15", pnd_Page_Number, true, 15, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_22", "15", "BLUE", 15, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde15", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 15, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt_2", pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt.getValue(2), true, 16, 2, 35, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt_1", pnd_Warrant_Extract_Pymnt_Addr_Line6_Txt.getValue(1), true, 16, 42, 35, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time16", pnd_Pymnt_Reqst_Log_Dte_Time, true, 16, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde16", pnd_Cntrct_Orgn_Cde, true, 16, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr16", pnd_Cntrct_Unq_Id_Nbr, true, 16, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number16", pnd_Page_Number, true, 16, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_23", "16", "BLUE", 16, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde16", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 16, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "CITIZENSHIP:", "BLUE", 17, 1, 12);
            uiForm.setUiControl("pnd_Ctznshp_Display", pnd_Ctznshp_Display, true, 17, 14, 9, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "RESIDENCE:", "BLUE", 17, 24, 10);
            uiForm.setUiControl("pnd_State_Display", pnd_State_Display, true, 17, 35, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "SETTLEMENT DATE:", "BLUE", 17, 38, 16);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Settlmnt_Dte", pnd_Warrant_Extract_Pymnt_Settlmnt_Dte, true, 17, 55, 10, "YELLOW", "MM/DD/YYYY", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "CURRENCY:", "BLUE", 17, 66, 9);
            uiForm.setUiControl("pnd_Currency_Display", pnd_Currency_Display, true, 17, 76, 4, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time17", pnd_Pymnt_Reqst_Log_Dte_Time, true, 17, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde17", pnd_Cntrct_Orgn_Cde, true, 17, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr17", pnd_Cntrct_Unq_Id_Nbr, true, 17, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number17", pnd_Page_Number, true, 17, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_28", "17", "BLUE", 17, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde17", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 17, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "ACCOUNTING DATE:", "BLUE", 18, 1, 16);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Acctg_Dte", pnd_Warrant_Extract_Pymnt_Acctg_Dte, true, 18, 18, 10, "YELLOW", "MM/DD/YYYY", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "PAYMENT DATE:", "BLUE", 18, 29, 13);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Check_Dte", pnd_Warrant_Extract_Pymnt_Check_Dte, true, 18, 43, 10, "YELLOW", "MM/DD/YYYY", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "AMOUNT:", "BLUE", 18, 54, 7);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Check_Amt", pnd_Warrant_Extract_Pymnt_Check_Amt, true, 18, 62, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time18", pnd_Pymnt_Reqst_Log_Dte_Time, true, 18, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde18", pnd_Cntrct_Orgn_Cde, true, 18, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr18", pnd_Cntrct_Unq_Id_Nbr, true, 18, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number18", pnd_Page_Number, true, 18, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_32", "18", "BLUE", 18, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde18", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 18, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "METHOD:", "BLUE", 19, 1, 7);
            uiForm.setUiControl("pnd_Pymnt_Display", pnd_Pymnt_Display, true, 19, 9, 11, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "NUMBER:", "BLUE", 19, 26, 7);
            uiForm.setUiControl("pnd_Check_Number_A11", pnd_Check_Number_A11, true, 19, 34, 11, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_35", "SEQ NO.:", "BLUE", 19, 53, 8);
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr", pnd_Warrant_Extract_Pymnt_Check_Seq_Nbr, true, 19, 62, 7, "YELLOW", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time19", pnd_Pymnt_Reqst_Log_Dte_Time, true, 19, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde19", pnd_Cntrct_Orgn_Cde, true, 19, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr19", pnd_Cntrct_Unq_Id_Nbr, true, 19, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number19", pnd_Page_Number, true, 19, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_36", "19", "BLUE", 19, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde19", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 19, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Eft_Transit_Id_Text", pnd_Pymnt_Eft_Transit_Id_Text, true, 20, 2, 16, "YELLOW", true, false, pnd_Pymnt_Eft_Transit_Id_Text_Cv, 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Eft_Transit_Id", pnd_Warrant_Extract_Pymnt_Eft_Transit_Id, true, 20, 19, 9, "YELLOW", "999999999", 
                true, true, pnd_Pymnt_Eft_Transit_Id_Cv, "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Eft_Acct_Nbr_Text", pnd_Pymnt_Eft_Acct_Nbr_Text, true, 20, 31, 8, "YELLOW", true, false, pnd_Pymnt_Eft_Acct_Nbr_Text_Cv, 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr", pnd_Warrant_Extract_Pymnt_Eft_Acct_Nbr, true, 20, 40, 21, "YELLOW", true, false, 
                pnd_Pymnt_Eft_Acct_Nbr_Cv, null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Chk_Sav_Ind_Text", pnd_Pymnt_Chk_Sav_Ind_Text, true, 20, 64, 14, "YELLOW", true, false, pnd_Pymnt_Chk_Sav_Ind_Text_Cv, 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind", pnd_Warrant_Extract_Pymnt_Chk_Sav_Ind, true, 20, 79, 1, "YELLOW", true, false, 
                pnd_Pymnt_Chk_Sav_Ind_Cv, null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time20", pnd_Pymnt_Reqst_Log_Dte_Time, true, 20, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde20", pnd_Cntrct_Orgn_Cde, true, 20, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr20", pnd_Cntrct_Unq_Id_Nbr, true, 20, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number20", pnd_Page_Number, true, 20, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_37", "20", "BLUE", 20, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde20", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 20, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
