/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:49:15 PM
**        *   FROM NATURAL MAP   :  Fcpm123b
************************************************************
**        * FILE NAME               : Fcpm123b.java
**        * CLASS NAME              : Fcpm123b
**        * INSTANCE NAME           : Fcpm123b
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #FED-DISPLAY #INV-ACCT-CDE-DESC(*) #INV-ACCT-CDE-DESC-CV(*)                                                              *     #INV-ACCT-CNTRCT-AMT(*) 
    #INV-ACCT-CNTRCT-AMT-CV(*)                                                                       *     #INV-ACCT-DPI-AMT(*) #INV-ACCT-DPI-AMT-CV(*) 
    *     #INV-ACCT-DVDND-AMT(*) #INV-ACCT-DVDND-AMT-CV(*)                                                                         *     #INV-ACCT-EXP-AMT(*) 
    #INV-ACCT-EXP-AMT-CV(*)                                                                             *     #INV-ACCT-FDRL-TAX-AMT(*) #INV-ACCT-FDRL-TAX-AMT-CV(*) 
    *     #INV-ACCT-IVC-AMT(*) #INV-ACCT-IVC-AMT-CV(*)                                                                             *     #INV-ACCT-LOCAL-TAX-AMT(*) 
    #INV-ACCT-LOCAL-TAX-AMT-CV(*)                                                                 *     #INV-ACCT-NET-PYMNT-AMT(*) #INV-ACCT-NET-PYMNT-AMT-CV(*) 
    *     #INV-ACCT-SETTL-AMT(*) #INV-ACCT-SETTL-AMT-CV(*)                                                                         *     #INV-ACCT-STATE-TAX-AMT(*) 
    #INV-ACCT-STATE-TAX-AMT-CV(*)                                                                 *     #INV-ACCT-UNIT-QTY(*) #INV-ACCT-UNIT-QTY-CV(*)  
    *     #INV-ACCT-UNIT-VALUE(*) #INV-ACCT-UNIT-VALUE-CV(*) #ORGN-DISPLAY                                                         *     #PAGE-NUMBER #PROGRAM 
    #RUN-TIME #STATE-DISPLAY #TYPE-DISPLAY
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm123b extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Fed_Display;
    private DbsField pnd_Inv_Acct_Cde_Desc;
    private DbsField pnd_Inv_Acct_Cde_Desc_Cv;
    private DbsField pnd_Inv_Acct_Cntrct_Amt;
    private DbsField pnd_Inv_Acct_Cntrct_Amt_Cv;
    private DbsField pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Inv_Acct_Dpi_Amt_Cv;
    private DbsField pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Inv_Acct_Dvdnd_Amt_Cv;
    private DbsField pnd_Inv_Acct_Exp_Amt;
    private DbsField pnd_Inv_Acct_Exp_Amt_Cv;
    private DbsField pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Inv_Acct_Fdrl_Tax_Amt_Cv;
    private DbsField pnd_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Inv_Acct_Ivc_Amt_Cv;
    private DbsField pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Inv_Acct_Local_Tax_Amt_Cv;
    private DbsField pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Inv_Acct_Net_Pymnt_Amt_Cv;
    private DbsField pnd_Inv_Acct_Settl_Amt;
    private DbsField pnd_Inv_Acct_Settl_Amt_Cv;
    private DbsField pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Inv_Acct_State_Tax_Amt_Cv;
    private DbsField pnd_Inv_Acct_Unit_Qty;
    private DbsField pnd_Inv_Acct_Unit_Qty_Cv;
    private DbsField pnd_Inv_Acct_Unit_Value;
    private DbsField pnd_Inv_Acct_Unit_Value_Cv;
    private DbsField pnd_Orgn_Display;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Program;
    private DbsField pnd_Run_Time;
    private DbsField pnd_State_Display;
    private DbsField pnd_Type_Display;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Fed_Display = parameters.newFieldInRecord("pnd_Fed_Display", "#FED-DISPLAY", FieldType.STRING, 3);
        pnd_Inv_Acct_Cde_Desc = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cde_Desc", "#INV-ACCT-CDE-DESC", FieldType.STRING, 8, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Cde_Desc_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cde_Desc_Cv", "#INV-ACCT-CDE-DESC-CV", FieldType.ATTRIBUTE_CONTROL, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Cntrct_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cntrct_Amt", "#INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 12, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Cntrct_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cntrct_Amt_Cv", "#INV-ACCT-CNTRCT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Dpi_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Dpi_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dpi_Amt_Cv", "#INV-ACCT-DPI-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Dvdnd_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dvdnd_Amt", "#INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 12, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Dvdnd_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dvdnd_Amt_Cv", "#INV-ACCT-DVDND-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Exp_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Exp_Amt", "#INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Exp_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Exp_Amt_Cv", "#INV-ACCT-EXP-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Fdrl_Tax_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 10, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Fdrl_Tax_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Fdrl_Tax_Amt_Cv", "#INV-ACCT-FDRL-TAX-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Ivc_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Ivc_Amt", "#INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Ivc_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Ivc_Amt_Cv", "#INV-ACCT-IVC-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Local_Tax_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Local_Tax_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Local_Tax_Amt_Cv", "#INV-ACCT-LOCAL-TAX-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Net_Pymnt_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Net_Pymnt_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Net_Pymnt_Amt_Cv", "#INV-ACCT-NET-PYMNT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Settl_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Settl_Amt", "#INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 12, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Settl_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Settl_Amt_Cv", "#INV-ACCT-SETTL-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_State_Tax_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_State_Tax_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_State_Tax_Amt_Cv", "#INV-ACCT-STATE-TAX-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Unit_Qty = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Qty", "#INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 10, 3, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Unit_Qty_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Qty_Cv", "#INV-ACCT-UNIT-QTY-CV", FieldType.ATTRIBUTE_CONTROL, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Unit_Value = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Value", "#INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 10, 4, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Unit_Value_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Value_Cv", "#INV-ACCT-UNIT-VALUE-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Orgn_Display = parameters.newFieldInRecord("pnd_Orgn_Display", "#ORGN-DISPLAY", FieldType.STRING, 43);
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Run_Time = parameters.newFieldInRecord("pnd_Run_Time", "#RUN-TIME", FieldType.TIME);
        pnd_State_Display = parameters.newFieldInRecord("pnd_State_Display", "#STATE-DISPLAY", FieldType.STRING, 2);
        pnd_Type_Display = parameters.newFieldInRecord("pnd_Type_Display", "#TYPE-DISPLAY", FieldType.STRING, 56);
        parameters.reset();
    }

    public Fcpm123b() throws Exception
    {
        super("Fcpm123b");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=020 LS=080 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm123b", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm123b"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Run_Time", pnd_Run_Time, true, 1, 2, 20, "YELLOW", "MM/DD/YYYY' '-' 'HH:IIAP", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "PAGE:", "BLUE", 1, 68, 5);
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 74, 6, "YELLOW", "ZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("astINIT_USER", Global.getINIT_USER(), true, 2, 2, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "-", "BLUE", 2, 13, 1);
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 2, 15, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Orgn_Display", pnd_Orgn_Display, true, 4, 20, 43, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Type_Display", pnd_Type_Display, true, 5, 13, 56, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "-------------------------------------------------------------------------------", "", 7, 1, 79);
            uiForm.setUiLabel("label_4", "INVESTMENT ACCOUNT:", "BLUE", 8, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_1", pnd_Inv_Acct_Cde_Desc.getValue(1), true, 8, 27, 8, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(1), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_2", pnd_Inv_Acct_Cde_Desc.getValue(2), true, 8, 42, 8, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(2), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_3", pnd_Inv_Acct_Cde_Desc.getValue(3), true, 8, 57, 8, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(3), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_4", pnd_Inv_Acct_Cde_Desc.getValue(4), true, 8, 72, 8, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(4), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "SETTLEMENT AMOUNT.:", "BLUE", 9, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_1", pnd_Inv_Acct_Settl_Amt.getValue(1), true, 9, 21, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_2", pnd_Inv_Acct_Settl_Amt.getValue(2), true, 9, 36, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_3", pnd_Inv_Acct_Settl_Amt.getValue(3), true, 9, 51, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_4", pnd_Inv_Acct_Settl_Amt.getValue(4), true, 9, 66, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "CONTRACTUAL AMOUNT:", "BLUE", 10, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_1", pnd_Inv_Acct_Cntrct_Amt.getValue(1), true, 10, 21, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_2", pnd_Inv_Acct_Cntrct_Amt.getValue(2), true, 10, 36, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_3", pnd_Inv_Acct_Cntrct_Amt.getValue(3), true, 10, 51, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_4", pnd_Inv_Acct_Cntrct_Amt.getValue(4), true, 10, 66, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "DIVIDEND AMOUNT...:", "BLUE", 11, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_1", pnd_Inv_Acct_Dvdnd_Amt.getValue(1), true, 11, 21, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_2", pnd_Inv_Acct_Dvdnd_Amt.getValue(2), true, 11, 36, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_3", pnd_Inv_Acct_Dvdnd_Amt.getValue(3), true, 11, 51, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_4", pnd_Inv_Acct_Dvdnd_Amt.getValue(4), true, 11, 66, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "UNITS.............:", "BLUE", 12, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Qty_1", pnd_Inv_Acct_Unit_Qty.getValue(1), true, 12, 23, 12, "YELLOW", "-ZZZ,ZZ9.999", true, true, 
                pnd_Inv_Acct_Unit_Qty_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Qty_2", pnd_Inv_Acct_Unit_Qty.getValue(2), true, 12, 38, 12, "YELLOW", "-ZZZ,ZZ9.999", true, true, 
                pnd_Inv_Acct_Unit_Qty_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Qty_3", pnd_Inv_Acct_Unit_Qty.getValue(3), true, 12, 53, 12, "YELLOW", "-ZZZ,ZZ9.999", true, true, 
                pnd_Inv_Acct_Unit_Qty_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Qty_4", pnd_Inv_Acct_Unit_Qty.getValue(4), true, 12, 68, 12, "YELLOW", "-ZZZ,ZZ9.999", true, true, 
                pnd_Inv_Acct_Unit_Qty_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "UNIT VALUE........:", "BLUE", 13, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Value_1", pnd_Inv_Acct_Unit_Value.getValue(1), true, 13, 23, 12, "YELLOW", "-ZZ,ZZ9.9999", true, true, 
                pnd_Inv_Acct_Unit_Value_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Value_2", pnd_Inv_Acct_Unit_Value.getValue(2), true, 13, 38, 12, "YELLOW", "-ZZ,ZZ9.9999", true, true, 
                pnd_Inv_Acct_Unit_Value_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Value_3", pnd_Inv_Acct_Unit_Value.getValue(3), true, 13, 53, 12, "YELLOW", "-ZZ,ZZ9.9999", true, true, 
                pnd_Inv_Acct_Unit_Value_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Value_4", pnd_Inv_Acct_Unit_Value.getValue(4), true, 13, 68, 12, "YELLOW", "-ZZ,ZZ9.9999", true, true, 
                pnd_Inv_Acct_Unit_Value_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "IVC AMOUNT........:", "BLUE", 14, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_1", pnd_Inv_Acct_Ivc_Amt.getValue(1), true, 14, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_2", pnd_Inv_Acct_Ivc_Amt.getValue(2), true, 14, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_3", pnd_Inv_Acct_Ivc_Amt.getValue(3), true, 14, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_4", pnd_Inv_Acct_Ivc_Amt.getValue(4), true, 14, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "EXPENSE CHARGE....:", "BLUE", 15, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Exp_Amt_1", pnd_Inv_Acct_Exp_Amt.getValue(1), true, 15, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Exp_Amt_Cv.getValue(1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Exp_Amt_2", pnd_Inv_Acct_Exp_Amt.getValue(2), true, 15, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Exp_Amt_Cv.getValue(2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Exp_Amt_3", pnd_Inv_Acct_Exp_Amt.getValue(3), true, 15, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Exp_Amt_Cv.getValue(3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Exp_Amt_4", pnd_Inv_Acct_Exp_Amt.getValue(4), true, 15, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Exp_Amt_Cv.getValue(4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "DPI AMOUNT........:", "BLUE", 16, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_1", pnd_Inv_Acct_Dpi_Amt.getValue(1), true, 16, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_2", pnd_Inv_Acct_Dpi_Amt.getValue(2), true, 16, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_3", pnd_Inv_Acct_Dpi_Amt.getValue(3), true, 16, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_4", pnd_Inv_Acct_Dpi_Amt.getValue(4), true, 16, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "FEDERAL TAX(", "BLUE", 17, 1, 12);
            uiForm.setUiControl("pnd_Fed_Display", pnd_Fed_Display, true, 17, 14, 3, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "):", "BLUE", 17, 18, 2);
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_1", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(1), true, 17, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_2", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(2), true, 17, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_3", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(3), true, 17, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_4", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(4), true, 17, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "STATE TAX..(", "BLUE", 18, 1, 12);
            uiForm.setUiControl("pnd_State_Display", pnd_State_Display, true, 18, 14, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", ").:", "BLUE", 18, 17, 3);
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_1", pnd_Inv_Acct_State_Tax_Amt.getValue(1), true, 18, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_2", pnd_Inv_Acct_State_Tax_Amt.getValue(2), true, 18, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_3", pnd_Inv_Acct_State_Tax_Amt.getValue(3), true, 18, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_4", pnd_Inv_Acct_State_Tax_Amt.getValue(4), true, 18, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "LOCAL TAX.........:", "BLUE", 19, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_1", pnd_Inv_Acct_Local_Tax_Amt.getValue(1), true, 19, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_2", pnd_Inv_Acct_Local_Tax_Amt.getValue(2), true, 19, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_3", pnd_Inv_Acct_Local_Tax_Amt.getValue(3), true, 19, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_4", pnd_Inv_Acct_Local_Tax_Amt.getValue(4), true, 19, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "NET PAYMENT.......:", "BLUE", 20, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_1", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(1), true, 20, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_2", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(2), true, 20, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_3", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(3), true, 20, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_4", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(4), true, 20, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
