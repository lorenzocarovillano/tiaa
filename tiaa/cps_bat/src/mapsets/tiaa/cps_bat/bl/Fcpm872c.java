/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:01 PM
**        *   FROM NATURAL MAP   :  Fcpm872c
************************************************************
**        * FILE NAME               : Fcpm872c.java
**        * CLASS NAME              : Fcpm872c
**        * INSTANCE NAME           : Fcpm872c
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #PAGE-NUMBER #PROGRAM #ROLLOVER-IND-CV #RTB-PERCENT-CV #RUN-TIME                                                         *     #SCREEN-HEADER 
    #SCREEN-HEADER2 #WS.#ROLLOVER-IND #WS.#RTB-PERCENT                                                        *     #WS.#20-PER-WITHOLD #20-PER-WITHOLD-CV 
    EXT.CNTRCT-ROLL-DEST-CDE                                                          *     EXT.PYMNT-EFT-ACCT-NBR
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm872c extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Page_Number;
    private DbsField pnd_Program;
    private DbsField pnd_Rollover_Ind_Cv;
    private DbsField pnd_Rtb_Percent_Cv;
    private DbsField pnd_Run_Time;
    private DbsField pnd_Screen_Header;
    private DbsField pnd_Screen_Header2;
    private DbsField pnd_Ws_Pnd_Rollover_Ind;
    private DbsField pnd_Ws_Pnd_Rtb_Percent;
    private DbsField pnd_Ws_Pnd_20_Per_Withold;
    private DbsField pnd_20_Per_Withold_Cv;
    private DbsField ext_Cntrct_Roll_Dest_Cde;
    private DbsField ext_Pymnt_Eft_Acct_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Program = parameters.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Rollover_Ind_Cv = parameters.newFieldInRecord("pnd_Rollover_Ind_Cv", "#ROLLOVER-IND-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Rtb_Percent_Cv = parameters.newFieldInRecord("pnd_Rtb_Percent_Cv", "#RTB-PERCENT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Run_Time = parameters.newFieldInRecord("pnd_Run_Time", "#RUN-TIME", FieldType.TIME);
        pnd_Screen_Header = parameters.newFieldInRecord("pnd_Screen_Header", "#SCREEN-HEADER", FieldType.STRING, 35);
        pnd_Screen_Header2 = parameters.newFieldInRecord("pnd_Screen_Header2", "#SCREEN-HEADER2", FieldType.STRING, 60);
        pnd_Ws_Pnd_Rollover_Ind = parameters.newFieldInRecord("pnd_Ws_Pnd_Rollover_Ind", "#WS.#ROLLOVER-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Rtb_Percent = parameters.newFieldInRecord("pnd_Ws_Pnd_Rtb_Percent", "#WS.#RTB-PERCENT", FieldType.PACKED_DECIMAL, 8, 4);
        pnd_Ws_Pnd_20_Per_Withold = parameters.newFieldInRecord("pnd_Ws_Pnd_20_Per_Withold", "#WS.#20-PER-WITHOLD", FieldType.BOOLEAN, 1);
        pnd_20_Per_Withold_Cv = parameters.newFieldInRecord("pnd_20_Per_Withold_Cv", "#20-PER-WITHOLD-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        ext_Cntrct_Roll_Dest_Cde = parameters.newFieldInRecord("ext_Cntrct_Roll_Dest_Cde", "EXT.CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        ext_Pymnt_Eft_Acct_Nbr = parameters.newFieldInRecord("ext_Pymnt_Eft_Acct_Nbr", "EXT.PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        parameters.reset();
    }

    public Fcpm872c() throws Exception
    {
        super("Fcpm872c");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=012 LS=081 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm872c", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm872c"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Run_Time", pnd_Run_Time, true, 1, 2, 20, "YELLOW", "MM/DD/YYYY' '-' 'HH:IIAP", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "PAGE:", "BLUE", 1, 68, 5);
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 74, 6, "YELLOW", "ZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("astINIT_USER", Global.getINIT_USER(), true, 2, 2, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "-", "BLUE", 2, 13, 1);
            uiForm.setUiControl("pnd_Program", pnd_Program, true, 2, 15, 8, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Screen_Header", pnd_Screen_Header, true, 3, 24, 35, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Screen_Header2", pnd_Screen_Header2, true, 4, 11, 60, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "RTB/Annuity Certain Information", "BLUE", 6, 25, 31);
            uiForm.setUiLabel("label_4", "% of Total RTB...........:", "BLUE", 8, 2, 26);
            uiForm.setUiControl("pnd_Ws_Pnd_Rtb_Percent", pnd_Ws_Pnd_Rtb_Percent, true, 8, 29, 10, "YELLOW", "-ZZ9.9999%", true, true, pnd_Rtb_Percent_Cv, 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "Rollover Indicator.......:", "BLUE", 9, 2, 26);
            uiForm.setUiControl("pnd_Ws_Pnd_Rollover_Ind", pnd_Ws_Pnd_Rollover_Ind, true, 9, 37, 1, "YELLOW", "N/Y", true, false, pnd_Rollover_Ind_Cv, 
                null, "AD=YLOFHW' '~TG=YN", ' ');
            uiForm.setUiLabel("label_6", "Destination Code.........:", "BLUE", 10, 2, 26);
            uiForm.setUiControl("ext_Cntrct_Roll_Dest_Cde", ext_Cntrct_Roll_Dest_Cde, true, 10, 34, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_7", "To Contract Number.......:", "BLUE", 11, 2, 26);
            uiForm.setUiControl("ext_Pymnt_Eft_Acct_Nbr", ext_Pymnt_Eft_Acct_Nbr, true, 11, 30, 21, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_8", "20% Withholding Indicator:", "BLUE", 12, 2, 26);
            uiForm.setUiControl("pnd_Ws_Pnd_20_Per_Withold", pnd_Ws_Pnd_20_Per_Withold, true, 12, 37, 1, "YELLOW", "N/Y", true, false, pnd_20_Per_Withold_Cv, 
                null, "AD=YLOFHW' '~TG=YN", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
