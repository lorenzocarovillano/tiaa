/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:01 PM
**        *   FROM NATURAL MAP   :  Fcpm872e
************************************************************
**        * FILE NAME               : Fcpm872e.java
**        * CLASS NAME              : Fcpm872e
**        * INSTANCE NAME           : Fcpm872e
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #CHECK-NUMBER-A11 #IA-DA-CONTRACT #KEY-DETAIL.CNTRCT-PAYEE-CDE                                                           *     #KEY-DETAIL.CNTRCT-PPCN-NBR 
    #KEY-DETAIL.CNTRCT-UNQ-ID-NBR                                                                *     #KEY-EFM.CNTRCT-CANCEL-RDRW-ACTVTY-CDE #KEY-EFM.CNTRCT-ORGN-CDE 
    *     #KEY-EFM.CNTRCT-UNQ-ID-NBR #KEY-EFM.PYMNT-REQST-LOG-DTE-TIME                                                             *     #OPTION #PYMNT-CHK-SAV-IND-TEXT-CV 
    #PYMNT-EFT-ACCT-NBR-TEXT-CV                                                           *     #PYMNT-EFT-TRANSIT-ID-TEXT-CV #SCREEN-HEADER #SCREEN-HEADER2 
    *     #WS.#CSR-DISPLAY #WS.#CTZNSHP-DISPLAY #WS.#MODE-DISPLAY                                                                  *     #WS.#OPTION-DISPLAY 
    #WS.#PAGE-NUMBER #WS.#PYMNT-CHK-SAV-IND-TEXT                                                         *     #WS.#PYMNT-DISPLAY #WS.#PYMNT-EFT-ACCT-NBR-TEXT 
    *     #WS.#PYMNT-EFT-TRANSIT-ID-TEXT #WS.#STATE-DISP-DESC #WS-MODE                                                             *     #WS-PYMNT-GROSS-AMT 
    EXT.ANNT-SOC-SEC-NBR EXT.CNTRCT-DA-CREF-1-NBR                                                        *     EXT.CNTRCT-DA-TIAA-1-NBR EXT.CNTRCT-DA-TIAA-2-NBR 
    *     EXT.CNTRCT-DA-TIAA-3-NBR EXT.CNTRCT-DA-TIAA-4-NBR                                                                        *     EXT.CNTRCT-DA-TIAA-5-NBR 
    EXT.PH-SEX EXT.PYMNT-ACCTG-DTE                                                                  *     EXT.PYMNT-ADDR-LINE1-TXT(*) EXT.PYMNT-ADDR-LINE2-TXT(*) 
    *     EXT.PYMNT-ADDR-LINE3-TXT(*) EXT.PYMNT-ADDR-LINE4-TXT(*)                                                                  *     EXT.PYMNT-ADDR-LINE5-TXT(*) 
    EXT.PYMNT-ADDR-LINE6-TXT(*)                                                                  *     EXT.PYMNT-CHECK-AMT EXT.PYMNT-CHECK-DTE EXT.PYMNT-CHECK-SEQ-NBR 
    *     EXT.PYMNT-CHK-SAV-IND EXT.PYMNT-DOB EXT.PYMNT-EFT-ACCT-NBR                                                               *     EXT.PYMNT-EFT-TRANSIT-ID 
    EXT.PYMNT-INTRFCE-DTE EXT.PYMNT-NME(*)
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm872e extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Check_Number_A11;
    private DbsField pnd_Ia_Da_Contract;
    private DbsField pnd_Key_Detail_Cntrct_Payee_Cde;
    private DbsField pnd_Key_Detail_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Key_Detail_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Key_Efm_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Efm_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Option;
    private DbsField pnd_Pymnt_Chk_Sav_Ind_Text_Cv;
    private DbsField pnd_Pymnt_Eft_Acct_Nbr_Text_Cv;
    private DbsField pnd_Pymnt_Eft_Transit_Id_Text_Cv;
    private DbsField pnd_Screen_Header;
    private DbsField pnd_Screen_Header2;
    private DbsField pnd_Ws_Pnd_Csr_Display;
    private DbsField pnd_Ws_Pnd_Ctznshp_Display;
    private DbsField pnd_Ws_Pnd_Mode_Display;
    private DbsField pnd_Ws_Pnd_Option_Display;
    private DbsField pnd_Ws_Pnd_Page_Number;
    private DbsField pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text;
    private DbsField pnd_Ws_Pnd_Pymnt_Display;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text;
    private DbsField pnd_Ws_Pnd_State_Disp_Desc;
    private DbsField pnd_Ws_Mode;
    private DbsField pnd_Ws_Pymnt_Gross_Amt;
    private DbsField ext_Annt_Soc_Sec_Nbr;
    private DbsField ext_Cntrct_Da_Cref_1_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField ext_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField ext_Ph_Sex;
    private DbsField ext_Pymnt_Acctg_Dte;
    private DbsField ext_Pymnt_Addr_Line1_Txt;
    private DbsField ext_Pymnt_Addr_Line2_Txt;
    private DbsField ext_Pymnt_Addr_Line3_Txt;
    private DbsField ext_Pymnt_Addr_Line4_Txt;
    private DbsField ext_Pymnt_Addr_Line5_Txt;
    private DbsField ext_Pymnt_Addr_Line6_Txt;
    private DbsField ext_Pymnt_Check_Amt;
    private DbsField ext_Pymnt_Check_Dte;
    private DbsField ext_Pymnt_Check_Seq_Nbr;
    private DbsField ext_Pymnt_Chk_Sav_Ind;
    private DbsField ext_Pymnt_Dob;
    private DbsField ext_Pymnt_Eft_Acct_Nbr;
    private DbsField ext_Pymnt_Eft_Transit_Id;
    private DbsField ext_Pymnt_Intrfce_Dte;
    private DbsField ext_Pymnt_Nme;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Check_Number_A11 = parameters.newFieldInRecord("pnd_Check_Number_A11", "#CHECK-NUMBER-A11", FieldType.STRING, 11);
        pnd_Ia_Da_Contract = parameters.newFieldInRecord("pnd_Ia_Da_Contract", "#IA-DA-CONTRACT", FieldType.STRING, 2);
        pnd_Key_Detail_Cntrct_Payee_Cde = parameters.newFieldInRecord("pnd_Key_Detail_Cntrct_Payee_Cde", "#KEY-DETAIL.CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Key_Detail_Cntrct_Ppcn_Nbr = parameters.newFieldInRecord("pnd_Key_Detail_Cntrct_Ppcn_Nbr", "#KEY-DETAIL.CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Key_Detail_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Key_Detail_Cntrct_Unq_Id_Nbr", "#KEY-DETAIL.CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde", "#KEY-EFM.CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Key_Efm_Cntrct_Orgn_Cde = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Orgn_Cde", "#KEY-EFM.CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Efm_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Unq_Id_Nbr", "#KEY-EFM.CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time = parameters.newFieldInRecord("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time", "#KEY-EFM.PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Option = parameters.newFieldInRecord("pnd_Option", "#OPTION", FieldType.STRING, 7);
        pnd_Pymnt_Chk_Sav_Ind_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Chk_Sav_Ind_Text_Cv", "#PYMNT-CHK-SAV-IND-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Pymnt_Eft_Acct_Nbr_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Acct_Nbr_Text_Cv", "#PYMNT-EFT-ACCT-NBR-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Pymnt_Eft_Transit_Id_Text_Cv = parameters.newFieldInRecord("pnd_Pymnt_Eft_Transit_Id_Text_Cv", "#PYMNT-EFT-TRANSIT-ID-TEXT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Screen_Header = parameters.newFieldInRecord("pnd_Screen_Header", "#SCREEN-HEADER", FieldType.STRING, 35);
        pnd_Screen_Header2 = parameters.newFieldInRecord("pnd_Screen_Header2", "#SCREEN-HEADER2", FieldType.STRING, 60);
        pnd_Ws_Pnd_Csr_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Csr_Display", "#WS.#CSR-DISPLAY", FieldType.STRING, 9);
        pnd_Ws_Pnd_Ctznshp_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Ctznshp_Display", "#WS.#CTZNSHP-DISPLAY", FieldType.STRING, 9);
        pnd_Ws_Pnd_Mode_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Mode_Display", "#WS.#MODE-DISPLAY", FieldType.STRING, 18);
        pnd_Ws_Pnd_Option_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Option_Display", "#WS.#OPTION-DISPLAY", FieldType.STRING, 35);
        pnd_Ws_Pnd_Page_Number = parameters.newFieldInRecord("pnd_Ws_Pnd_Page_Number", "#WS.#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text = parameters.newFieldInRecord("pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text", "#WS.#PYMNT-CHK-SAV-IND-TEXT", FieldType.STRING, 
            17);
        pnd_Ws_Pnd_Pymnt_Display = parameters.newFieldInRecord("pnd_Ws_Pnd_Pymnt_Display", "#WS.#PYMNT-DISPLAY", FieldType.STRING, 17);
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text = parameters.newFieldInRecord("pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text", "#WS.#PYMNT-EFT-ACCT-NBR-TEXT", FieldType.STRING, 
            8);
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text = parameters.newFieldInRecord("pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text", "#WS.#PYMNT-EFT-TRANSIT-ID-TEXT", FieldType.STRING, 
            16);
        pnd_Ws_Pnd_State_Disp_Desc = parameters.newFieldInRecord("pnd_Ws_Pnd_State_Disp_Desc", "#WS.#STATE-DISP-DESC", FieldType.STRING, 20);
        pnd_Ws_Mode = parameters.newFieldInRecord("pnd_Ws_Mode", "#WS-MODE", FieldType.STRING, 5);
        pnd_Ws_Pymnt_Gross_Amt = parameters.newFieldInRecord("pnd_Ws_Pymnt_Gross_Amt", "#WS-PYMNT-GROSS-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        ext_Annt_Soc_Sec_Nbr = parameters.newFieldInRecord("ext_Annt_Soc_Sec_Nbr", "EXT.ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        ext_Cntrct_Da_Cref_1_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Cref_1_Nbr", "EXT.CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_1_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Tiaa_1_Nbr", "EXT.CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_2_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Tiaa_2_Nbr", "EXT.CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_3_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Tiaa_3_Nbr", "EXT.CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_4_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Tiaa_4_Nbr", "EXT.CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        ext_Cntrct_Da_Tiaa_5_Nbr = parameters.newFieldInRecord("ext_Cntrct_Da_Tiaa_5_Nbr", "EXT.CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        ext_Ph_Sex = parameters.newFieldInRecord("ext_Ph_Sex", "EXT.PH-SEX", FieldType.STRING, 1);
        ext_Pymnt_Acctg_Dte = parameters.newFieldInRecord("ext_Pymnt_Acctg_Dte", "EXT.PYMNT-ACCTG-DTE", FieldType.DATE);
        ext_Pymnt_Addr_Line1_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line1_Txt", "EXT.PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Addr_Line2_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line2_Txt", "EXT.PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Addr_Line3_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line3_Txt", "EXT.PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Addr_Line4_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line4_Txt", "EXT.PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Addr_Line5_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line5_Txt", "EXT.PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Addr_Line6_Txt = parameters.newFieldArrayInRecord("ext_Pymnt_Addr_Line6_Txt", "EXT.PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 35, new 
            DbsArrayController(1, 2));
        ext_Pymnt_Check_Amt = parameters.newFieldInRecord("ext_Pymnt_Check_Amt", "EXT.PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        ext_Pymnt_Check_Dte = parameters.newFieldInRecord("ext_Pymnt_Check_Dte", "EXT.PYMNT-CHECK-DTE", FieldType.DATE);
        ext_Pymnt_Check_Seq_Nbr = parameters.newFieldInRecord("ext_Pymnt_Check_Seq_Nbr", "EXT.PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        ext_Pymnt_Chk_Sav_Ind = parameters.newFieldInRecord("ext_Pymnt_Chk_Sav_Ind", "EXT.PYMNT-CHK-SAV-IND", FieldType.STRING, 1);
        ext_Pymnt_Dob = parameters.newFieldInRecord("ext_Pymnt_Dob", "EXT.PYMNT-DOB", FieldType.DATE);
        ext_Pymnt_Eft_Acct_Nbr = parameters.newFieldInRecord("ext_Pymnt_Eft_Acct_Nbr", "EXT.PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        ext_Pymnt_Eft_Transit_Id = parameters.newFieldInRecord("ext_Pymnt_Eft_Transit_Id", "EXT.PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9);
        ext_Pymnt_Intrfce_Dte = parameters.newFieldInRecord("ext_Pymnt_Intrfce_Dte", "EXT.PYMNT-INTRFCE-DTE", FieldType.DATE);
        ext_Pymnt_Nme = parameters.newFieldArrayInRecord("ext_Pymnt_Nme", "EXT.PYMNT-NME", FieldType.STRING, 38, new DbsArrayController(1, 2));
        parameters.reset();
    }

    public Fcpm872e() throws Exception
    {
        super("Fcpm872e");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=020 LS=124 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm872e", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm872e"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Screen_Header", pnd_Screen_Header, true, 1, 24, 35, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 1, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 1, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 1, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number", pnd_Ws_Pnd_Page_Number, true, 1, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "01", "BLUE", 1, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 1, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Screen_Header2", pnd_Screen_Header2, true, 2, 11, 60, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time2", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 2, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde2", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 2, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr2", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 2, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number2", pnd_Ws_Pnd_Page_Number, true, 2, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "02", "BLUE", 2, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde2", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 2, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "INTERFACE DATE:", "BLUE", 3, 2, 15);
            uiForm.setUiControl("ext_Pymnt_Intrfce_Dte", ext_Pymnt_Intrfce_Dte, true, 3, 18, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Csr_Display", pnd_Ws_Pnd_Csr_Display, true, 3, 30, 9, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time3", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 3, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde3", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 3, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr3", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 3, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number3", pnd_Ws_Pnd_Page_Number, true, 3, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "03", "BLUE", 3, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde3", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 3, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "PIN :", "BLUE", 4, 2, 5);
            uiForm.setUiControl("pnd_Key_Detail_Cntrct_Unq_Id_Nbr", pnd_Key_Detail_Cntrct_Unq_Id_Nbr, true, 4, 8, 12, "YELLOW", "999999999999", true, 
                true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ia_Da_Contract", pnd_Ia_Da_Contract, true, 4, 32, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "Contract/Certificate:", "BLUE", 4, 35, 21);
            uiForm.setUiControl("pnd_Key_Detail_Cntrct_Ppcn_Nbr", pnd_Key_Detail_Cntrct_Ppcn_Nbr, true, 4, 57, 9, "YELLOW", "XXXXXXX-X", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Detail_Cntrct_Payee_Cde", pnd_Key_Detail_Cntrct_Payee_Cde, true, 4, 67, 4, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time4", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 4, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde4", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 4, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr4", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 4, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number4", pnd_Ws_Pnd_Page_Number, true, 4, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "04", "BLUE", 4, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde4", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 4, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "SSN.:", "BLUE", 5, 2, 5);
            uiForm.setUiControl("ext_Annt_Soc_Sec_Nbr", ext_Annt_Soc_Sec_Nbr, true, 5, 8, 11, "YELLOW", "999-99-9999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "Sex:", "BLUE", 5, 32, 4);
            uiForm.setUiControl("ext_Ph_Sex", ext_Ph_Sex, true, 5, 37, 1, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "Date of Birth:", "BLUE", 5, 42, 14);
            uiForm.setUiControl("ext_Pymnt_Dob", ext_Pymnt_Dob, true, 5, 57, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time5", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 5, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde5", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 5, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr5", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 5, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number5", pnd_Ws_Pnd_Page_Number, true, 5, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "05", "BLUE", 5, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde5", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 5, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "ANNUITANT NAME AND ADDRESS:", "BLUE", 6, 2, 27);
            uiForm.setUiLabel("label_13", "PAYEE NAME AND ADDRESS:", "BLUE", 6, 42, 23);
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time6", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 6, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde6", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 6, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr6", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 6, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number6", pnd_Ws_Pnd_Page_Number, true, 6, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "06", "BLUE", 6, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde6", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 6, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Nme_2", ext_Pymnt_Nme.getValue(2), true, 7, 2, 38, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Nme_1", ext_Pymnt_Nme.getValue(1), true, 7, 42, 38, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time7", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 7, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde7", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 7, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr7", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 7, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number7", pnd_Ws_Pnd_Page_Number, true, 7, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "07", "BLUE", 7, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde7", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 7, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line1_Txt_2", ext_Pymnt_Addr_Line1_Txt.getValue(2), true, 8, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line1_Txt_1", ext_Pymnt_Addr_Line1_Txt.getValue(1), true, 8, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time8", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 8, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde8", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 8, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr8", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 8, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number8", pnd_Ws_Pnd_Page_Number, true, 8, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "08", "BLUE", 8, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde8", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 8, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line2_Txt_2", ext_Pymnt_Addr_Line2_Txt.getValue(2), true, 9, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line2_Txt_1", ext_Pymnt_Addr_Line2_Txt.getValue(1), true, 9, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time9", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 9, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde9", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 9, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr9", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 9, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number9", pnd_Ws_Pnd_Page_Number, true, 9, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "09", "BLUE", 9, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde9", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 9, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line3_Txt_2", ext_Pymnt_Addr_Line3_Txt.getValue(2), true, 10, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line3_Txt_1", ext_Pymnt_Addr_Line3_Txt.getValue(1), true, 10, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time10", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 10, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde10", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 10, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr10", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 10, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number10", pnd_Ws_Pnd_Page_Number, true, 10, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "10", "BLUE", 10, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde10", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 10, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line4_Txt_2", ext_Pymnt_Addr_Line4_Txt.getValue(2), true, 11, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line4_Txt_1", ext_Pymnt_Addr_Line4_Txt.getValue(1), true, 11, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time11", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 11, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde11", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 11, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr11", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 11, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number11", pnd_Ws_Pnd_Page_Number, true, 11, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "11", "BLUE", 11, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde11", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 11, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line5_Txt_2", ext_Pymnt_Addr_Line5_Txt.getValue(2), true, 12, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line5_Txt_1", ext_Pymnt_Addr_Line5_Txt.getValue(1), true, 12, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time12", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 12, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde12", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 12, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr12", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 12, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number12", pnd_Ws_Pnd_Page_Number, true, 12, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "12", "BLUE", 12, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde12", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 12, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line6_Txt_2", ext_Pymnt_Addr_Line6_Txt.getValue(2), true, 13, 2, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Addr_Line6_Txt_1", ext_Pymnt_Addr_Line6_Txt.getValue(1), true, 13, 42, 35, "YELLOW", true, false, null, null, 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time13", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 13, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde13", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 13, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr13", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 13, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number13", pnd_Ws_Pnd_Page_Number, true, 13, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "13", "BLUE", 13, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde13", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 13, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "Citizenship:", "BLUE", 14, 2, 12);
            uiForm.setUiControl("pnd_Ws_Pnd_Ctznshp_Display", pnd_Ws_Pnd_Ctznshp_Display, true, 14, 15, 9, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_23", "Residence:", "BLUE", 14, 42, 10);
            uiForm.setUiControl("pnd_Ws_Pnd_State_Disp_Desc", pnd_Ws_Pnd_State_Disp_Desc, true, 14, 53, 20, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time14", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 14, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde14", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 14, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr14", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 14, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number14", pnd_Ws_Pnd_Page_Number, true, 14, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "14", "BLUE", 14, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde14", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 14, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Option", pnd_Option, true, 15, 2, 7, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Option_Display", pnd_Ws_Pnd_Option_Display, true, 15, 10, 35, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Ws_Mode", pnd_Ws_Mode, true, 15, 47, 5, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Mode_Display", pnd_Ws_Pnd_Mode_Display, true, 15, 53, 18, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time15", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 15, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde15", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 15, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr15", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 15, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number15", pnd_Ws_Pnd_Page_Number, true, 15, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "15", "BLUE", 15, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde15", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 15, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "Payment Date...:", "BLUE", 16, 2, 16);
            uiForm.setUiControl("ext_Pymnt_Check_Dte", ext_Pymnt_Check_Dte, true, 16, 19, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_27", "Accounting Date:", "BLUE", 16, 36, 16);
            uiForm.setUiControl("ext_Pymnt_Acctg_Dte", ext_Pymnt_Acctg_Dte, true, 16, 53, 10, "YELLOW", "MM/DD/YYYY", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time16", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 16, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde16", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 16, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr16", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 16, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number16", pnd_Ws_Pnd_Page_Number, true, 16, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "16", "BLUE", 16, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde16", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 16, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "Payment Method :", "BLUE", 17, 2, 16);
            uiForm.setUiControl("pnd_Ws_Pnd_Pymnt_Display", pnd_Ws_Pnd_Pymnt_Display, true, 17, 19, 17, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_30", "Number:", "BLUE", 17, 40, 7);
            uiForm.setUiControl("pnd_Check_Number_A11", pnd_Check_Number_A11, true, 17, 48, 11, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_31", "SEQ NO.:", "BLUE", 17, 62, 8);
            uiForm.setUiControl("ext_Pymnt_Check_Seq_Nbr", ext_Pymnt_Check_Seq_Nbr, true, 17, 71, 7, "YELLOW", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time17", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 17, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde17", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 17, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr17", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 17, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number17", pnd_Ws_Pnd_Page_Number, true, 17, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "17", "BLUE", 17, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde17", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 17, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text", pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text, true, 18, 2, 16, "YELLOW", true, false, 
                pnd_Pymnt_Eft_Transit_Id_Text_Cv, null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Eft_Transit_Id", ext_Pymnt_Eft_Transit_Id, true, 18, 19, 9, "BLUE", "999999999", true, true, pnd_Pymnt_Eft_Transit_Id_Text_Cv, 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text", pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text, true, 18, 30, 8, "YELLOW", true, false, pnd_Pymnt_Eft_Acct_Nbr_Text_Cv, 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Eft_Acct_Nbr", ext_Pymnt_Eft_Acct_Nbr, true, 18, 39, 21, "BLUE", true, false, pnd_Pymnt_Eft_Acct_Nbr_Text_Cv, 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text", pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text, true, 18, 61, 17, "YELLOW", true, false, pnd_Pymnt_Chk_Sav_Ind_Text_Cv, 
                null, "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("ext_Pymnt_Chk_Sav_Ind", ext_Pymnt_Chk_Sav_Ind, true, 18, 79, 1, "BLUE", true, false, pnd_Pymnt_Chk_Sav_Ind_Text_Cv, null, 
                "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time18", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 18, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde18", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 18, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr18", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 18, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number18", pnd_Ws_Pnd_Page_Number, true, 18, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "18", "BLUE", 18, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde18", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 18, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "Total Gross Payment Amt:", "BLUE", 19, 2, 24);
            uiForm.setUiControl("pnd_Ws_Pymnt_Gross_Amt", pnd_Ws_Pymnt_Gross_Amt, true, 19, 27, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "Total Net Payment Amt:", "BLUE", 19, 42, 22);
            uiForm.setUiControl("ext_Pymnt_Check_Amt", ext_Pymnt_Check_Amt, true, 19, 65, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time19", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 19, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde19", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 19, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr19", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 19, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number19", pnd_Ws_Pnd_Page_Number, true, 19, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "19", "BLUE", 19, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde19", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 19, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "Da Cont/Cert's Settled :", "BLUE", 20, 2, 24);
            uiForm.setUiControl("ext_Cntrct_Da_Tiaa_1_Nbr", ext_Cntrct_Da_Tiaa_1_Nbr, true, 20, 27, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ext_Cntrct_Da_Tiaa_2_Nbr", ext_Cntrct_Da_Tiaa_2_Nbr, true, 20, 36, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ext_Cntrct_Da_Tiaa_3_Nbr", ext_Cntrct_Da_Tiaa_3_Nbr, true, 20, 45, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ext_Cntrct_Da_Tiaa_4_Nbr", ext_Cntrct_Da_Tiaa_4_Nbr, true, 20, 54, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ext_Cntrct_Da_Tiaa_5_Nbr", ext_Cntrct_Da_Tiaa_5_Nbr, true, 20, 63, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("ext_Cntrct_Da_Cref_1_Nbr", ext_Cntrct_Da_Cref_1_Nbr, true, 20, 72, 8, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time20", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 20, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde20", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 20, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr20", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 20, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number20", pnd_Ws_Pnd_Page_Number, true, 20, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "20", "BLUE", 20, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde20", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 20, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
