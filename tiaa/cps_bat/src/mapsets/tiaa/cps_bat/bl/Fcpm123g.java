/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:49:19 PM
**        *   FROM NATURAL MAP   :  Fcpm123g
************************************************************
**        * FILE NAME               : Fcpm123g.java
**        * CLASS NAME              : Fcpm123g
**        * INSTANCE NAME           : Fcpm123g
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #CNTRCT-CANCEL-RDRW-ACTVTY-CDE #CNTRCT-ORGN-CDE                                                                          *     #CNTRCT-UNQ-ID-NBR 
    #FED-DISPLAY #INV-ACCT-CDE-DESC(*)                                                                    *     #INV-ACCT-CDE-DESC-CV(*) #INV-ACCT-CNTRCT-AMT(*) 
    *     #INV-ACCT-CNTRCT-AMT-CV(*) #INV-ACCT-DPI-AMT(*)                                                                          *     #INV-ACCT-DPI-AMT-CV(*) 
    #INV-ACCT-DVDND-AMT(*)                                                                           *     #INV-ACCT-DVDND-AMT-CV(*) #INV-ACCT-EXP-AMT(*) 
    *     #INV-ACCT-EXP-AMT-CV(*) #INV-ACCT-FDRL-TAX-AMT(*)                                                                        *     #INV-ACCT-FDRL-TAX-AMT-CV(*) 
    #INV-ACCT-IVC-AMT(*)                                                                        *     #INV-ACCT-IVC-AMT-CV(*) #INV-ACCT-LOCAL-TAX-AMT(*) 
    *     #INV-ACCT-LOCAL-TAX-AMT-CV(*) #INV-ACCT-NET-PYMNT-AMT(*)                                                                 *     #INV-ACCT-NET-PYMNT-AMT-CV(*) 
    #INV-ACCT-SETTL-AMT(*)                                                                     *     #INV-ACCT-SETTL-AMT-CV(*) #INV-ACCT-STATE-TAX-AMT(*) 
    *     #INV-ACCT-STATE-TAX-AMT-CV(*) #INV-ACCT-UNIT-QTY(*)                                                                      *     #INV-ACCT-UNIT-QTY-CV(*) 
    #INV-ACCT-UNIT-VALUE(*)                                                                         *     #INV-ACCT-UNIT-VALUE-CV(*) #ORGN-DISPLAY #PAGE-NUMBER 
    *     #PYMNT-REQST-LOG-DTE-TIME #STATE-DISPLAY #TYPE-DISPLAY
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm123g extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Fed_Display;
    private DbsField pnd_Inv_Acct_Cde_Desc;
    private DbsField pnd_Inv_Acct_Cde_Desc_Cv;
    private DbsField pnd_Inv_Acct_Cntrct_Amt;
    private DbsField pnd_Inv_Acct_Cntrct_Amt_Cv;
    private DbsField pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Inv_Acct_Dpi_Amt_Cv;
    private DbsField pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Inv_Acct_Dvdnd_Amt_Cv;
    private DbsField pnd_Inv_Acct_Exp_Amt;
    private DbsField pnd_Inv_Acct_Exp_Amt_Cv;
    private DbsField pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Inv_Acct_Fdrl_Tax_Amt_Cv;
    private DbsField pnd_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Inv_Acct_Ivc_Amt_Cv;
    private DbsField pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Inv_Acct_Local_Tax_Amt_Cv;
    private DbsField pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Inv_Acct_Net_Pymnt_Amt_Cv;
    private DbsField pnd_Inv_Acct_Settl_Amt;
    private DbsField pnd_Inv_Acct_Settl_Amt_Cv;
    private DbsField pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Inv_Acct_State_Tax_Amt_Cv;
    private DbsField pnd_Inv_Acct_Unit_Qty;
    private DbsField pnd_Inv_Acct_Unit_Qty_Cv;
    private DbsField pnd_Inv_Acct_Unit_Value;
    private DbsField pnd_Inv_Acct_Unit_Value_Cv;
    private DbsField pnd_Orgn_Display;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_State_Display;
    private DbsField pnd_Type_Display;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cntrct_Cancel_Rdrw_Actvty_Cde = parameters.newFieldInRecord("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde", "#CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2);
        pnd_Cntrct_Orgn_Cde = parameters.newFieldInRecord("pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Cntrct_Unq_Id_Nbr", "#CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Fed_Display = parameters.newFieldInRecord("pnd_Fed_Display", "#FED-DISPLAY", FieldType.STRING, 3);
        pnd_Inv_Acct_Cde_Desc = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cde_Desc", "#INV-ACCT-CDE-DESC", FieldType.STRING, 8, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Cde_Desc_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cde_Desc_Cv", "#INV-ACCT-CDE-DESC-CV", FieldType.ATTRIBUTE_CONTROL, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Cntrct_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cntrct_Amt", "#INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 12, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Cntrct_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Cntrct_Amt_Cv", "#INV-ACCT-CNTRCT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Dpi_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Dpi_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dpi_Amt_Cv", "#INV-ACCT-DPI-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Dvdnd_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dvdnd_Amt", "#INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 12, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Dvdnd_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Dvdnd_Amt_Cv", "#INV-ACCT-DVDND-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Exp_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Exp_Amt", "#INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Exp_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Exp_Amt_Cv", "#INV-ACCT-EXP-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Fdrl_Tax_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 10, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Fdrl_Tax_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Fdrl_Tax_Amt_Cv", "#INV-ACCT-FDRL-TAX-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Ivc_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Ivc_Amt", "#INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 10, 2, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Ivc_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Ivc_Amt_Cv", "#INV-ACCT-IVC-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Local_Tax_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Local_Tax_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Local_Tax_Amt_Cv", "#INV-ACCT-LOCAL-TAX-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Net_Pymnt_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Net_Pymnt_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Net_Pymnt_Amt_Cv", "#INV-ACCT-NET-PYMNT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Settl_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Settl_Amt", "#INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 12, 2, new 
            DbsArrayController(1, 4));
        pnd_Inv_Acct_Settl_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Settl_Amt_Cv", "#INV-ACCT-SETTL-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_State_Tax_Amt = parameters.newFieldArrayInRecord("pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            10, 2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_State_Tax_Amt_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_State_Tax_Amt_Cv", "#INV-ACCT-STATE-TAX-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Inv_Acct_Unit_Qty = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Qty", "#INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 10, 3, new DbsArrayController(1, 
            4));
        pnd_Inv_Acct_Unit_Qty_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Qty_Cv", "#INV-ACCT-UNIT-QTY-CV", FieldType.ATTRIBUTE_CONTROL, 2, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Unit_Value = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Value", "#INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 10, 4, 
            new DbsArrayController(1, 4));
        pnd_Inv_Acct_Unit_Value_Cv = parameters.newFieldArrayInRecord("pnd_Inv_Acct_Unit_Value_Cv", "#INV-ACCT-UNIT-VALUE-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 4));
        pnd_Orgn_Display = parameters.newFieldInRecord("pnd_Orgn_Display", "#ORGN-DISPLAY", FieldType.STRING, 43);
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Pymnt_Reqst_Log_Dte_Time = parameters.newFieldInRecord("pnd_Pymnt_Reqst_Log_Dte_Time", "#PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_State_Display = parameters.newFieldInRecord("pnd_State_Display", "#STATE-DISPLAY", FieldType.STRING, 2);
        pnd_Type_Display = parameters.newFieldInRecord("pnd_Type_Display", "#TYPE-DISPLAY", FieldType.STRING, 56);
        parameters.reset();
    }

    public Fcpm123g() throws Exception
    {
        super("Fcpm123g");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=020 LS=124 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm123g", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm123g"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Orgn_Display", pnd_Orgn_Display, true, 1, 21, 43, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time", pnd_Pymnt_Reqst_Log_Dte_Time, true, 1, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde", pnd_Cntrct_Orgn_Cde, true, 1, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr", pnd_Cntrct_Unq_Id_Nbr, true, 1, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "01", "BLUE", 1, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 1, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Type_Display", pnd_Type_Display, true, 2, 14, 56, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time2", pnd_Pymnt_Reqst_Log_Dte_Time, true, 2, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde2", pnd_Cntrct_Orgn_Cde, true, 2, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr2", pnd_Cntrct_Unq_Id_Nbr, true, 2, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number2", pnd_Page_Number, true, 2, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "02", "BLUE", 2, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde2", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 2, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time3", pnd_Pymnt_Reqst_Log_Dte_Time, true, 3, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde3", pnd_Cntrct_Orgn_Cde, true, 3, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr3", pnd_Cntrct_Unq_Id_Nbr, true, 3, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number3", pnd_Page_Number, true, 3, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "03", "BLUE", 3, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde3", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 3, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "-------------------------------------------------------------------------------", "", 4, 1, 79);
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time4", pnd_Pymnt_Reqst_Log_Dte_Time, true, 4, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde4", pnd_Cntrct_Orgn_Cde, true, 4, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr4", pnd_Cntrct_Unq_Id_Nbr, true, 4, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number4", pnd_Page_Number, true, 4, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "04", "BLUE", 4, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde4", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 4, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "INVESTMENT ACCOUNT:", "BLUE", 5, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_1", pnd_Inv_Acct_Cde_Desc.getValue(1), true, 5, 27, 8, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(1), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_2", pnd_Inv_Acct_Cde_Desc.getValue(2), true, 5, 42, 8, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(2), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_3", pnd_Inv_Acct_Cde_Desc.getValue(3), true, 5, 57, 8, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(3), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cde_Desc_4", pnd_Inv_Acct_Cde_Desc.getValue(4), true, 5, 72, 8, "YELLOW", true, false, pnd_Inv_Acct_Cde_Desc_Cv.getValue(4), 
                null, "AD=Y?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time5", pnd_Pymnt_Reqst_Log_Dte_Time, true, 5, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde5", pnd_Cntrct_Orgn_Cde, true, 5, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr5", pnd_Cntrct_Unq_Id_Nbr, true, 5, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number5", pnd_Page_Number, true, 5, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_7", "05", "BLUE", 5, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde5", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 5, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "SETTLEMENT AMOUNT.:", "BLUE", 6, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_1", pnd_Inv_Acct_Settl_Amt.getValue(1), true, 6, 21, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_2", pnd_Inv_Acct_Settl_Amt.getValue(2), true, 6, 36, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_3", pnd_Inv_Acct_Settl_Amt.getValue(3), true, 6, 51, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Settl_Amt_4", pnd_Inv_Acct_Settl_Amt.getValue(4), true, 6, 66, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Settl_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time6", pnd_Pymnt_Reqst_Log_Dte_Time, true, 6, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde6", pnd_Cntrct_Orgn_Cde, true, 6, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr6", pnd_Cntrct_Unq_Id_Nbr, true, 6, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number6", pnd_Page_Number, true, 6, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_9", "06", "BLUE", 6, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde6", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 6, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "CONTRACTUAL AMOUNT:", "BLUE", 7, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_1", pnd_Inv_Acct_Cntrct_Amt.getValue(1), true, 7, 21, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_2", pnd_Inv_Acct_Cntrct_Amt.getValue(2), true, 7, 36, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_3", pnd_Inv_Acct_Cntrct_Amt.getValue(3), true, 7, 51, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Cntrct_Amt_4", pnd_Inv_Acct_Cntrct_Amt.getValue(4), true, 7, 66, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time7", pnd_Pymnt_Reqst_Log_Dte_Time, true, 7, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde7", pnd_Cntrct_Orgn_Cde, true, 7, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr7", pnd_Cntrct_Unq_Id_Nbr, true, 7, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number7", pnd_Page_Number, true, 7, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_11", "07", "BLUE", 7, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde7", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 7, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "DIVIDENT AMOUNT...:", "BLUE", 8, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_1", pnd_Inv_Acct_Dvdnd_Amt.getValue(1), true, 8, 21, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_2", pnd_Inv_Acct_Dvdnd_Amt.getValue(2), true, 8, 36, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_3", pnd_Inv_Acct_Dvdnd_Amt.getValue(3), true, 8, 51, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dvdnd_Amt_4", pnd_Inv_Acct_Dvdnd_Amt.getValue(4), true, 8, 66, 14, "YELLOW", "-ZZZZZZ,ZZ9.99", true, true, 
                pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time8", pnd_Pymnt_Reqst_Log_Dte_Time, true, 8, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde8", pnd_Cntrct_Orgn_Cde, true, 8, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr8", pnd_Cntrct_Unq_Id_Nbr, true, 8, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number8", pnd_Page_Number, true, 8, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_13", "08", "BLUE", 8, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde8", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 8, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "UNITS.............:", "BLUE", 9, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Qty_1", pnd_Inv_Acct_Unit_Qty.getValue(1), true, 9, 23, 12, "YELLOW", "-ZZZ,ZZ9.999", true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Qty_2", pnd_Inv_Acct_Unit_Qty.getValue(2), true, 9, 38, 12, "YELLOW", "-ZZZ,ZZ9.999", true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Qty_3", pnd_Inv_Acct_Unit_Qty.getValue(3), true, 9, 53, 12, "YELLOW", "-ZZZ,ZZ9.999", true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Qty_4", pnd_Inv_Acct_Unit_Qty.getValue(4), true, 9, 68, 12, "YELLOW", "-ZZZ,ZZ9.999", true, true, pnd_Inv_Acct_Unit_Qty_Cv.getValue(4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time9", pnd_Pymnt_Reqst_Log_Dte_Time, true, 9, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde9", pnd_Cntrct_Orgn_Cde, true, 9, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr9", pnd_Cntrct_Unq_Id_Nbr, true, 9, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number9", pnd_Page_Number, true, 9, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_15", "09", "BLUE", 9, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde9", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 9, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "UNIT VALUE........:", "BLUE", 10, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Value_1", pnd_Inv_Acct_Unit_Value.getValue(1), true, 10, 23, 12, "YELLOW", "-ZZ,ZZ9.9999", true, true, 
                pnd_Inv_Acct_Unit_Value_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Value_2", pnd_Inv_Acct_Unit_Value.getValue(2), true, 10, 38, 12, "YELLOW", "-ZZ,ZZ9.9999", true, true, 
                pnd_Inv_Acct_Unit_Value_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Value_3", pnd_Inv_Acct_Unit_Value.getValue(3), true, 10, 53, 12, "YELLOW", "-ZZ,ZZ9.9999", true, true, 
                pnd_Inv_Acct_Unit_Value_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Unit_Value_4", pnd_Inv_Acct_Unit_Value.getValue(4), true, 10, 68, 12, "YELLOW", "-ZZ,ZZ9.9999", true, true, 
                pnd_Inv_Acct_Unit_Value_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time10", pnd_Pymnt_Reqst_Log_Dte_Time, true, 10, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde10", pnd_Cntrct_Orgn_Cde, true, 10, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr10", pnd_Cntrct_Unq_Id_Nbr, true, 10, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number10", pnd_Page_Number, true, 10, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_17", "10", "BLUE", 10, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde10", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 10, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "IVC AMOUNT........:", "BLUE", 11, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_1", pnd_Inv_Acct_Ivc_Amt.getValue(1), true, 11, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_2", pnd_Inv_Acct_Ivc_Amt.getValue(2), true, 11, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_3", pnd_Inv_Acct_Ivc_Amt.getValue(3), true, 11, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Ivc_Amt_4", pnd_Inv_Acct_Ivc_Amt.getValue(4), true, 11, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Ivc_Amt_Cv.getValue(4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time11", pnd_Pymnt_Reqst_Log_Dte_Time, true, 11, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde11", pnd_Cntrct_Orgn_Cde, true, 11, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr11", pnd_Cntrct_Unq_Id_Nbr, true, 11, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number11", pnd_Page_Number, true, 11, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_19", "11", "BLUE", 11, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde11", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 11, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "EXPENSE CHARGE....:", "BLUE", 12, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Exp_Amt_1", pnd_Inv_Acct_Exp_Amt.getValue(1), true, 12, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Exp_Amt_Cv.getValue(1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Exp_Amt_2", pnd_Inv_Acct_Exp_Amt.getValue(2), true, 12, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Exp_Amt_Cv.getValue(2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Exp_Amt_3", pnd_Inv_Acct_Exp_Amt.getValue(3), true, 12, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Exp_Amt_Cv.getValue(3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Exp_Amt_4", pnd_Inv_Acct_Exp_Amt.getValue(4), true, 12, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Exp_Amt_Cv.getValue(4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time12", pnd_Pymnt_Reqst_Log_Dte_Time, true, 12, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde12", pnd_Cntrct_Orgn_Cde, true, 12, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr12", pnd_Cntrct_Unq_Id_Nbr, true, 12, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number12", pnd_Page_Number, true, 12, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_21", "12", "BLUE", 12, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde12", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 12, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "DPI AMOUNT........:", "BLUE", 13, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_1", pnd_Inv_Acct_Dpi_Amt.getValue(1), true, 13, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(1), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_2", pnd_Inv_Acct_Dpi_Amt.getValue(2), true, 13, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(2), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_3", pnd_Inv_Acct_Dpi_Amt.getValue(3), true, 13, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(3), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Dpi_Amt_4", pnd_Inv_Acct_Dpi_Amt.getValue(4), true, 13, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, true, pnd_Inv_Acct_Dpi_Amt_Cv.getValue(4), 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time13", pnd_Pymnt_Reqst_Log_Dte_Time, true, 13, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde13", pnd_Cntrct_Orgn_Cde, true, 13, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr13", pnd_Cntrct_Unq_Id_Nbr, true, 13, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number13", pnd_Page_Number, true, 13, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_23", "13", "BLUE", 13, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde13", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 13, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "FEDERAL TAX(", "BLUE", 14, 1, 12);
            uiForm.setUiControl("pnd_Fed_Display", pnd_Fed_Display, true, 14, 14, 3, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "):", "BLUE", 14, 18, 2);
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_1", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(1), true, 14, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_2", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(2), true, 14, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_3", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(3), true, 14, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Fdrl_Tax_Amt_4", pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(4), true, 14, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time14", pnd_Pymnt_Reqst_Log_Dte_Time, true, 14, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde14", pnd_Cntrct_Orgn_Cde, true, 14, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr14", pnd_Cntrct_Unq_Id_Nbr, true, 14, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number14", pnd_Page_Number, true, 14, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_26", "14", "BLUE", 14, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde14", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 14, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "STATE TAX..(", "BLUE", 15, 1, 12);
            uiForm.setUiControl("pnd_State_Display", pnd_State_Display, true, 15, 14, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", ").:", "BLUE", 15, 17, 3);
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_1", pnd_Inv_Acct_State_Tax_Amt.getValue(1), true, 15, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_2", pnd_Inv_Acct_State_Tax_Amt.getValue(2), true, 15, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_3", pnd_Inv_Acct_State_Tax_Amt.getValue(3), true, 15, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_State_Tax_Amt_4", pnd_Inv_Acct_State_Tax_Amt.getValue(4), true, 15, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time15", pnd_Pymnt_Reqst_Log_Dte_Time, true, 15, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde15", pnd_Cntrct_Orgn_Cde, true, 15, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr15", pnd_Cntrct_Unq_Id_Nbr, true, 15, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number15", pnd_Page_Number, true, 15, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_29", "15", "BLUE", 15, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde15", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 15, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "LOCAL TAX.........:", "BLUE", 16, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_1", pnd_Inv_Acct_Local_Tax_Amt.getValue(1), true, 16, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_2", pnd_Inv_Acct_Local_Tax_Amt.getValue(2), true, 16, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_3", pnd_Inv_Acct_Local_Tax_Amt.getValue(3), true, 16, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Local_Tax_Amt_4", pnd_Inv_Acct_Local_Tax_Amt.getValue(4), true, 16, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time16", pnd_Pymnt_Reqst_Log_Dte_Time, true, 16, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde16", pnd_Cntrct_Orgn_Cde, true, 16, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr16", pnd_Cntrct_Unq_Id_Nbr, true, 16, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number16", pnd_Page_Number, true, 16, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_31", "16", "BLUE", 16, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde16", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 16, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "NET PAYMENT.......:", "BLUE", 17, 1, 19);
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_1", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(1), true, 17, 22, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(1), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_2", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(2), true, 17, 37, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(2), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_3", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(3), true, 17, 52, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(3), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Inv_Acct_Net_Pymnt_Amt_4", pnd_Inv_Acct_Net_Pymnt_Amt.getValue(4), true, 17, 67, 13, "YELLOW", "-Z,ZZZ,ZZ9.99", true, 
                true, pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(4), "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time17", pnd_Pymnt_Reqst_Log_Dte_Time, true, 17, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde17", pnd_Cntrct_Orgn_Cde, true, 17, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr17", pnd_Cntrct_Unq_Id_Nbr, true, 17, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number17", pnd_Page_Number, true, 17, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_33", "17", "BLUE", 17, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde17", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 17, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time18", pnd_Pymnt_Reqst_Log_Dte_Time, true, 18, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde18", pnd_Cntrct_Orgn_Cde, true, 18, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr18", pnd_Cntrct_Unq_Id_Nbr, true, 18, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number18", pnd_Page_Number, true, 18, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_34", "18", "BLUE", 18, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde18", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 18, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time19", pnd_Pymnt_Reqst_Log_Dte_Time, true, 19, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde19", pnd_Cntrct_Orgn_Cde, true, 19, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr19", pnd_Cntrct_Unq_Id_Nbr, true, 19, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number19", pnd_Page_Number, true, 19, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_35", "19", "BLUE", 19, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde19", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 19, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Pymnt_Reqst_Log_Dte_Time20", pnd_Pymnt_Reqst_Log_Dte_Time, true, 20, 81, 15, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Orgn_Cde20", pnd_Cntrct_Orgn_Cde, true, 20, 97, 2, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cntrct_Unq_Id_Nbr20", pnd_Cntrct_Unq_Id_Nbr, true, 20, 100, 12, "YELLOW", "999999999999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number20", pnd_Page_Number, true, 20, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_36", "20", "BLUE", 20, 119, 2);
            uiForm.setUiControl("pnd_Cntrct_Cancel_Rdrw_Actvty_Cde20", pnd_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 20, 122, 2, "YELLOW", true, false, null, 
                null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
