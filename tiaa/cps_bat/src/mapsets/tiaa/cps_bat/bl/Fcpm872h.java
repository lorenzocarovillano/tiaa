/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:50:05 PM
**        *   FROM NATURAL MAP   :  Fcpm872h
************************************************************
**        * FILE NAME               : Fcpm872h.java
**        * CLASS NAME              : Fcpm872h
**        * INSTANCE NAME           : Fcpm872h
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #KEY-EFM.CNTRCT-CANCEL-RDRW-ACTVTY-CDE #KEY-EFM.CNTRCT-ORGN-CDE                                                          *     #KEY-EFM.CNTRCT-UNQ-ID-NBR 
    #KEY-EFM.PYMNT-REQST-LOG-DTE-TIME                                                             *     #ROLLOVER-IND-CV #RTB-PERCENT-CV #SCREEN-HEADER2 
    #WS.#PAGE-NUMBER                                                        *     #WS.#ROLLOVER-IND #WS.#RTB-PERCENT #WS.#20-PER-WITHOLD                
    *     #20-PER-WITHOLD-CV EXT.CNTRCT-ROLL-DEST-CDE                                                                              *     EXT.PYMNT-EFT-ACCT-NBR
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm872h extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Key_Efm_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Efm_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Rollover_Ind_Cv;
    private DbsField pnd_Rtb_Percent_Cv;
    private DbsField pnd_Screen_Header2;
    private DbsField pnd_Ws_Pnd_Page_Number;
    private DbsField pnd_Ws_Pnd_Rollover_Ind;
    private DbsField pnd_Ws_Pnd_Rtb_Percent;
    private DbsField pnd_Ws_Pnd_20_Per_Withold;
    private DbsField pnd_20_Per_Withold_Cv;
    private DbsField ext_Cntrct_Roll_Dest_Cde;
    private DbsField ext_Pymnt_Eft_Acct_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde", "#KEY-EFM.CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Key_Efm_Cntrct_Orgn_Cde = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Orgn_Cde", "#KEY-EFM.CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Efm_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Key_Efm_Cntrct_Unq_Id_Nbr", "#KEY-EFM.CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time = parameters.newFieldInRecord("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time", "#KEY-EFM.PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Rollover_Ind_Cv = parameters.newFieldInRecord("pnd_Rollover_Ind_Cv", "#ROLLOVER-IND-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Rtb_Percent_Cv = parameters.newFieldInRecord("pnd_Rtb_Percent_Cv", "#RTB-PERCENT-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Screen_Header2 = parameters.newFieldInRecord("pnd_Screen_Header2", "#SCREEN-HEADER2", FieldType.STRING, 60);
        pnd_Ws_Pnd_Page_Number = parameters.newFieldInRecord("pnd_Ws_Pnd_Page_Number", "#WS.#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Ws_Pnd_Rollover_Ind = parameters.newFieldInRecord("pnd_Ws_Pnd_Rollover_Ind", "#WS.#ROLLOVER-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Rtb_Percent = parameters.newFieldInRecord("pnd_Ws_Pnd_Rtb_Percent", "#WS.#RTB-PERCENT", FieldType.PACKED_DECIMAL, 8, 4);
        pnd_Ws_Pnd_20_Per_Withold = parameters.newFieldInRecord("pnd_Ws_Pnd_20_Per_Withold", "#WS.#20-PER-WITHOLD", FieldType.BOOLEAN, 1);
        pnd_20_Per_Withold_Cv = parameters.newFieldInRecord("pnd_20_Per_Withold_Cv", "#20-PER-WITHOLD-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        ext_Cntrct_Roll_Dest_Cde = parameters.newFieldInRecord("ext_Cntrct_Roll_Dest_Cde", "EXT.CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        ext_Pymnt_Eft_Acct_Nbr = parameters.newFieldInRecord("ext_Pymnt_Eft_Acct_Nbr", "EXT.PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        parameters.reset();
    }

    public Fcpm872h() throws Exception
    {
        super("Fcpm872h");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=020 LS=124 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm872h", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm872h"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "Annuitization Payment Warrant", "BLUE", 1, 26, 29);
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 1, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 1, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 1, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number", pnd_Ws_Pnd_Page_Number, true, 1, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "01", "BLUE", 1, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 1, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Screen_Header2", pnd_Screen_Header2, true, 2, 9, 60, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time2", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 2, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde2", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 2, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr2", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 2, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number2", pnd_Ws_Pnd_Page_Number, true, 2, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "02", "BLUE", 2, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde2", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 2, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time3", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 3, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde3", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 3, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr3", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 3, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number3", pnd_Ws_Pnd_Page_Number, true, 3, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "03", "BLUE", 3, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde3", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 3, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_5", "RTB/Annuity Certain Information", "BLUE", 4, 25, 31);
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time4", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 4, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde4", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 4, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr4", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 4, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number4", pnd_Ws_Pnd_Page_Number, true, 4, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "04", "BLUE", 4, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde4", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 4, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time5", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 5, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde5", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 5, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr5", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 5, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number5", pnd_Ws_Pnd_Page_Number, true, 5, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "05", "BLUE", 5, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde5", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 5, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "% of Total RTB...........:", "BLUE", 6, 2, 26);
            uiForm.setUiControl("pnd_Ws_Pnd_Rtb_Percent", pnd_Ws_Pnd_Rtb_Percent, true, 6, 29, 10, "YELLOW", "-ZZ9.9999%", true, true, pnd_Rtb_Percent_Cv, 
                "0123456789+-, ", "AD=YLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time6", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 6, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde6", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 6, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr6", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 6, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number6", pnd_Ws_Pnd_Page_Number, true, 6, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "06", "BLUE", 6, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde6", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 6, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "Rollover Indicator.......:", "BLUE", 7, 2, 26);
            uiForm.setUiControl("pnd_Ws_Pnd_Rollover_Ind", pnd_Ws_Pnd_Rollover_Ind, true, 7, 37, 1, "YELLOW", "N/Y", true, false, pnd_Rollover_Ind_Cv, 
                null, "AD=YLOFHW' '~TG=YN", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time7", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 7, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde7", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 7, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr7", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 7, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number7", pnd_Ws_Pnd_Page_Number, true, 7, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "07", "BLUE", 7, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde7", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 7, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "Destination Code.........:", "BLUE", 8, 2, 26);
            uiForm.setUiControl("ext_Cntrct_Roll_Dest_Cde", ext_Cntrct_Roll_Dest_Cde, true, 8, 34, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time8", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 8, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde8", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 8, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr8", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 8, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number8", pnd_Ws_Pnd_Page_Number, true, 8, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "08", "BLUE", 8, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde8", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 8, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "To Contract Number.......:", "BLUE", 9, 2, 26);
            uiForm.setUiControl("ext_Pymnt_Eft_Acct_Nbr", ext_Pymnt_Eft_Acct_Nbr, true, 9, 30, 21, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time9", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 9, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde9", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 9, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr9", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 9, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number9", pnd_Ws_Pnd_Page_Number, true, 9, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "09", "BLUE", 9, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde9", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 9, 122, 2, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "20% Withholding Indicator:", "BLUE", 10, 2, 26);
            uiForm.setUiControl("pnd_Ws_Pnd_20_Per_Withold", pnd_Ws_Pnd_20_Per_Withold, true, 10, 37, 1, "YELLOW", "N/Y", true, false, pnd_20_Per_Withold_Cv, 
                null, "AD=YLOFHW' '~TG=YN", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time10", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 10, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde10", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 10, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr10", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 10, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number10", pnd_Ws_Pnd_Page_Number, true, 10, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "10", "BLUE", 10, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde10", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 10, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time11", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 11, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde11", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 11, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr11", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 11, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number11", pnd_Ws_Pnd_Page_Number, true, 11, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "11", "BLUE", 11, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde11", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 11, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time12", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 12, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde12", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 12, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr12", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 12, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number12", pnd_Ws_Pnd_Page_Number, true, 12, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "12", "BLUE", 12, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde12", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 12, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time13", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 13, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde13", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 13, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr13", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 13, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number13", pnd_Ws_Pnd_Page_Number, true, 13, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "13", "BLUE", 13, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde13", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 13, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time14", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 14, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde14", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 14, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr14", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 14, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number14", pnd_Ws_Pnd_Page_Number, true, 14, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "14", "BLUE", 14, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde14", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 14, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time15", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 15, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde15", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 15, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr15", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 15, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number15", pnd_Ws_Pnd_Page_Number, true, 15, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "15", "BLUE", 15, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde15", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 15, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time16", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 16, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde16", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 16, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr16", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 16, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number16", pnd_Ws_Pnd_Page_Number, true, 16, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "16", "BLUE", 16, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde16", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 16, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time17", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 17, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde17", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 17, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr17", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 17, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number17", pnd_Ws_Pnd_Page_Number, true, 17, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "17", "BLUE", 17, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde17", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 17, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time18", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 18, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde18", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 18, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr18", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 18, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number18", pnd_Ws_Pnd_Page_Number, true, 18, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "18", "BLUE", 18, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde18", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 18, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time19", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 19, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde19", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 19, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr19", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 19, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number19", pnd_Ws_Pnd_Page_Number, true, 19, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "19", "BLUE", 19, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde19", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 19, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time20", pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time, true, 20, 81, 15, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Orgn_Cde20", pnd_Key_Efm_Cntrct_Orgn_Cde, true, 20, 97, 2, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Unq_Id_Nbr20", pnd_Key_Efm_Cntrct_Unq_Id_Nbr, true, 20, 100, 12, "YELLOW", "999999999999", true, true, 
                null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Page_Number20", pnd_Ws_Pnd_Page_Number, true, 20, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", 
                "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "20", "BLUE", 20, 119, 2);
            uiForm.setUiControl("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde20", pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde, true, 20, 122, 2, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
