/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:49:23 PM
**        *   FROM NATURAL MAP   :  Fcpm399b
************************************************************
**        * FILE NAME               : Fcpm399b.java
**        * CLASS NAME              : Fcpm399b
**        * INSTANCE NAME           : Fcpm399b
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #ACFS-DATA #PAGE-NUMBER #REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE                                                              *     #REC-TYPE-10-DETAIL.CNTRCT-UNQ-ID-NBR 
    *     #REC-TYPE-10-DETAIL.PYMNT-REQST-LOG-DTE-TIME #TYPE-DISPLAY                                                               *     #WS.#ACFS-INVALID-REASONS(*)
************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpm399b extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Acfs_Data;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde;
    private DbsField pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Type_Display;
    private DbsField pnd_Ws_Pnd_Acfs_Invalid_Reasons;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Acfs_Data = parameters.newFieldInRecord("pnd_Acfs_Data", "#ACFS-DATA", FieldType.STRING, 4);
        pnd_Page_Number = parameters.newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde = parameters.newFieldInRecord("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde", "#REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr = parameters.newFieldInRecord("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr", "#REC-TYPE-10-DETAIL.CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time = parameters.newFieldInRecord("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time", "#REC-TYPE-10-DETAIL.PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Type_Display = parameters.newFieldInRecord("pnd_Type_Display", "#TYPE-DISPLAY", FieldType.STRING, 56);
        pnd_Ws_Pnd_Acfs_Invalid_Reasons = parameters.newFieldArrayInRecord("pnd_Ws_Pnd_Acfs_Invalid_Reasons", "#WS.#ACFS-INVALID-REASONS", FieldType.STRING, 
            50, new DbsArrayController(1, 10));
        parameters.reset();
    }

    public Fcpm399b() throws Exception
    {
        super("Fcpm399b");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=015 LS=126 ZP=OFF SG=OFF KD=OFF IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Fcpm399b", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Fcpm399b"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "CONSOLIDATED PAYMENT SYSTEM", "BLUE", 1, 26, 27);
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 1, 81, 15, "YELLOW", 
                true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 1, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 1, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number", pnd_Page_Number, true, 1, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "01", "BLUE", 1, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data", pnd_Acfs_Data, true, 1, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Type_Display", pnd_Type_Display, true, 2, 11, 56, "YELLOW", true, false, null, null, "AD=I?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time2", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 2, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde2", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 2, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr2", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 2, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number2", pnd_Page_Number, true, 2, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "02", "BLUE", 2, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data2", pnd_Acfs_Data, true, 2, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "Alternate Carrier Fact Sheet Audit Trail", "BLUE", 3, 19, 40);
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time3", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 3, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde3", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 3, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr3", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 3, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number3", pnd_Page_Number, true, 3, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "03", "BLUE", 3, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data3", pnd_Acfs_Data, true, 3, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time4", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 4, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde4", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 4, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr4", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 4, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number4", pnd_Page_Number, true, 4, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_6", "04", "BLUE", 4, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data4", pnd_Acfs_Data, true, 4, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time5", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 5, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde5", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 5, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr5", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 5, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number5", pnd_Page_Number, true, 5, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_7", "05", "BLUE", 5, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data5", pnd_Acfs_Data, true, 5, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "Invalid Reasons:", "BLUE", 6, 2, 16);
            uiForm.setUiControl("pnd_Ws_Pnd_Acfs_Invalid_Reasons_1", pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(1), true, 6, 19, 50, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time6", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 6, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde6", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 6, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr6", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 6, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number6", pnd_Page_Number, true, 6, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_9", "06", "BLUE", 6, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data6", pnd_Acfs_Data, true, 6, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Acfs_Invalid_Reasons_2", pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(2), true, 7, 19, 50, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time7", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 7, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde7", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 7, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr7", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 7, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number7", pnd_Page_Number, true, 7, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_10", "07", "BLUE", 7, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data7", pnd_Acfs_Data, true, 7, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Acfs_Invalid_Reasons_3", pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(3), true, 8, 19, 50, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time8", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 8, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde8", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 8, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr8", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 8, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number8", pnd_Page_Number, true, 8, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_11", "08", "BLUE", 8, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data8", pnd_Acfs_Data, true, 8, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Acfs_Invalid_Reasons_4", pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(4), true, 9, 19, 50, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time9", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 9, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde9", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 9, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr9", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 9, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number9", pnd_Page_Number, true, 9, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_12", "09", "BLUE", 9, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data9", pnd_Acfs_Data, true, 9, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Acfs_Invalid_Reasons_5", pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(5), true, 10, 19, 50, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time10", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 10, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde10", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 10, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr10", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 10, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number10", pnd_Page_Number, true, 10, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_13", "10", "BLUE", 10, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data10", pnd_Acfs_Data, true, 10, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Acfs_Invalid_Reasons_6", pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(6), true, 11, 19, 50, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time11", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 11, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde11", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 11, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr11", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 11, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number11", pnd_Page_Number, true, 11, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_14", "11", "BLUE", 11, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data11", pnd_Acfs_Data, true, 11, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Acfs_Invalid_Reasons_7", pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(7), true, 12, 19, 50, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time12", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 12, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde12", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 12, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr12", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 12, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number12", pnd_Page_Number, true, 12, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_15", "12", "BLUE", 12, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data12", pnd_Acfs_Data, true, 12, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Acfs_Invalid_Reasons_8", pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(8), true, 13, 19, 50, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time13", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 13, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde13", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 13, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr13", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 13, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number13", pnd_Page_Number, true, 13, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_16", "13", "BLUE", 13, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data13", pnd_Acfs_Data, true, 13, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Acfs_Invalid_Reasons_9", pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(9), true, 14, 19, 50, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time14", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 14, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde14", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 14, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr14", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 14, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number14", pnd_Page_Number, true, 14, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_17", "14", "BLUE", 14, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data14", pnd_Acfs_Data, true, 14, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Ws_Pnd_Acfs_Invalid_Reasons_10", pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(10), true, 15, 19, 50, "YELLOW", true, 
                false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time15", pnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time, true, 15, 81, 15, 
                "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde15", pnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde, true, 15, 97, 2, "YELLOW", true, false, 
                null, null, "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr15", pnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr, true, 15, 100, 12, "YELLOW", "999999999999", 
                true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Page_Number15", pnd_Page_Number, true, 15, 113, 5, "YELLOW", "99999", true, true, null, "0123456789+-, ", "AD=ILOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_18", "15", "BLUE", 15, 119, 2);
            uiForm.setUiControl("pnd_Acfs_Data15", pnd_Acfs_Data, true, 15, 122, 4, "YELLOW", true, false, null, null, "AD=ILOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
