/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:14 PM
**        * FROM NATURAL PROGRAM : Fcpp812
************************************************************
**        * FILE NAME            : Fcpp812.java
**        * CLASS NAME           : Fcpp812
**        * INSTANCE NAME        : Fcpp812
************************************************************
************************************************************************
*
* PROGRAM   : FCPP812
*
* SYSTEM    : CPS
* TITLE     : NO TAXES/VOLUNTARY DEDUCTIONS TAKEN
* GENERATED :
*
* FUNCTION  : GENERATE A REPORT OF ALL CONTRACTS WHOSE PAYMENTS WERE
*           : NOT REDUCED BY TAXES AND/OR VOLUNTARY DEDUCTIONS.
*           :
* HISTORY   :
*           : 04/16/97  RITA SALGADO
*           : - RESTOWED TO USE FCPLPMNT
* 09/11/00  : A. YOUNG - RE-COMPILED DUE TO FCPLPMNT.
* 08/01/01  : A. REYHANIAN  READ EXPANDED FILE (WITH TAX WITHHOLDING)
* 05/05/03  : R. CARREON    RESTOW DUE TO NEW FCPA800
* 04/22/08  : LCW - RESTOWED FOR ROTH 403B. ROTH-MAJOR1
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGE IN FCPA800 & FCPLPMNT
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 05/09/2017 :SAI K      - RECOMPILE DUE TO CHANGES TO FCPA800
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp812 extends BLNatBase
{
    // Data Areas
    private LdaFcplpmnt ldaFcplpmnt;
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpa802 pdaFcpa802;
    private LdaFcpl801a ldaFcpl801a;
    private PdaFcpacntl pdaFcpacntl;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Index;
    private DbsField pnd_Gross_Amt;
    private DbsField pnd_Ivc_Amt;
    private DbsField pnd_Deductions;
    private DbsField pnd_Net_Amt;
    private DbsField pnd_Taxes;
    private DbsField pnd_Pymnt_Nbr;
    private DbsField pnd_D_Gross_Amt;
    private DbsField pnd_D_Deductions;
    private DbsField pnd_D_Net_Amt;
    private DbsField pnd_T_Gross_Amt;
    private DbsField pnd_T_Ivc_Amt;
    private DbsField pnd_T_Net_Amt;
    private DbsField pnd_T_Taxes;
    private DbsField pnd_Soc_Sec_Nbr;

    private DbsGroup pnd_Pymnt_Key;
    private DbsField pnd_Pymnt_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_Key_Pymnt_Prcss_Seq_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpa802 = new PdaFcpa802(localVariables);
        ldaFcpl801a = new LdaFcpl801a();
        registerRecord(ldaFcpl801a);
        pdaFcpacntl = new PdaFcpacntl(localVariables);

        // Local Variables
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Gross_Amt = localVariables.newFieldInRecord("pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ivc_Amt = localVariables.newFieldInRecord("pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Deductions = localVariables.newFieldInRecord("pnd_Deductions", "#DEDUCTIONS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Net_Amt = localVariables.newFieldInRecord("pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Taxes = localVariables.newFieldInRecord("pnd_Taxes", "#TAXES", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pymnt_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Nbr", "#PYMNT-NBR", FieldType.STRING, 8);
        pnd_D_Gross_Amt = localVariables.newFieldInRecord("pnd_D_Gross_Amt", "#D-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_D_Deductions = localVariables.newFieldInRecord("pnd_D_Deductions", "#D-DEDUCTIONS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_D_Net_Amt = localVariables.newFieldInRecord("pnd_D_Net_Amt", "#D-NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Gross_Amt = localVariables.newFieldInRecord("pnd_T_Gross_Amt", "#T-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Ivc_Amt = localVariables.newFieldInRecord("pnd_T_Ivc_Amt", "#T-IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Net_Amt = localVariables.newFieldInRecord("pnd_T_Net_Amt", "#T-NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Taxes = localVariables.newFieldInRecord("pnd_T_Taxes", "#T-TAXES", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Soc_Sec_Nbr = localVariables.newFieldInRecord("pnd_Soc_Sec_Nbr", "#SOC-SEC-NBR", FieldType.STRING, 11);

        pnd_Pymnt_Key = localVariables.newGroupInRecord("pnd_Pymnt_Key", "#PYMNT-KEY");
        pnd_Pymnt_Key_Cntrct_Ppcn_Nbr = pnd_Pymnt_Key.newFieldInGroup("pnd_Pymnt_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_Key_Cntrct_Invrse_Dte = pnd_Pymnt_Key.newFieldInGroup("pnd_Pymnt_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Pymnt_Key_Cntrct_Orgn_Cde = pnd_Pymnt_Key.newFieldInGroup("pnd_Pymnt_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_Key_Pymnt_Prcss_Seq_Nbr = pnd_Pymnt_Key.newFieldInGroup("pnd_Pymnt_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplpmnt.initializeValues();
        ldaFcpl801a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Fcpp812() throws Exception
    {
        super("Fcpp812");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP812", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 55;//Natural: FORMAT ( 02 ) LS = 132 PS = 55
        //*                                                                                                                                                               //Natural: ON ERROR
        getWorkFiles().read(1, pdaFcpacntl.getCntl());                                                                                                                    //Natural: READ WORK FILE 1 ONCE CNTL
        //*  READ ERROR/REJECT WORK FILE AND SELECT RECORDS CREATED FOR
        //*    'no voluntary deductions' AND 'no taxes'
        //*  AD WORK FILE 2 PYMNT-ADDR-INFO  WF-ERROR-MSG INV-INFO(*)       /* ABRM
        //*  ABRM
        //*  ABRM
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 PYMNT-ADDR-INFO WF-ERROR-MSG WF-PYMNT-TAX-REC INV-INFO ( * )
        while (condition(getWorkFiles().read(2, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), ldaFcpl801a.getWf_Error_Msg(), pdaFcpa802.getWf_Pymnt_Tax_Grp_Wf_Pymnt_Tax_Rec(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue("*"))))
        {
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(8,":",10).greaterOrEqual(3) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(8, //Natural: IF WF-PYMNT-ADDR-GRP.GTN-RPT-CODE ( 8:10 ) = 03 THRU 05
                ":",10).lessOrEqual(5)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(1),  //Natural: END-ALL
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(4), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(7), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(10), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count(), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(3), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(6), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(9), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(12), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(15), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(18), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(21), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(24), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(27), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(30), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(33), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(36), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(39), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(2), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(8), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(14), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(20), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(26), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(32), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(38), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(1), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(4), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(7), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(10), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(13), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(16), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(19), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(22), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(25), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(28), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(31), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(34), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(37), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(40), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(2), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(4), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(6), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(8), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(10), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(12), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(14), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(16), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(18), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(20), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(22), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(24), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(26), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(28), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(30), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(32), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(34), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(36), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(38), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(40), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(2), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(4), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(6), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(8), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(10), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(12), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(14), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(16), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(18), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(20), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(22), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(24), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(26), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(28), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(30), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(32), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(34), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(36), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(38), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(40), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(2), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(4), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(6), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(8), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(10), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(12), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(14), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(16), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(18), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(20), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(22), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(24), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(26), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(28), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(30), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(32), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(34), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(36), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(38), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(40), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(2), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(4), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(6), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(8), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(10), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(12), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(14), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(16), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(18), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(20), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(22), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(24), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(26), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(28), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(30), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(32), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(34), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(36), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(38), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(40), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(3), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(6), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(9), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(2), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(8), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Tax_Info(), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                                         //Natural: SORT BY WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR USING WF-PYMNT-ADDR-GRP.GTN-RPT-CODE ( * ) WF-PYMNT-ADDR-GRP.ANNT-SOC-SEC-NBR WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( * ) WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( * ) WF-PYMNT-ADDR-GRP.INV-ACCT-IVC-AMT ( * ) WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( * ) WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( * ) WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( * ) WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( * ) WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE ( * ) WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( * ) WF-PYMNT-ADDR-GRP.PYMNT-TAX-INFO WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT
        SORT01:
        while (condition(getSort().readSortOutData(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(6), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(2), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(8), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(14), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(20), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(26), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(32), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(38), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(4), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(10), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(16), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(22), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(28), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(34), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(40), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(6), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(12), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(18), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(24), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(30), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(36), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(2), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(8), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(2), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(8), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(4), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(10), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Tax_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt())))
        {
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 01 ) TITLE LEFT JUSTIFIED *PROGRAM '- 1' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 49T 'TAXES NOT WITHHELD FOR' CNTL.CNTL-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY )
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 02 ) TITLE LEFT JUSTIFIED *PROGRAM '- 2' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 40T 'VOLUNTARY DEDUCTIONS NOT TAKEN FOR' CNTL.CNTL-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY )
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Pymnt_Key.setValuesByName(pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec());                                                                           //Natural: MOVE BY NAME WF-PYMNT-ADDR-REC TO #PYMNT-KEY
            ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                          //Natural: READ ( 1 ) FCP-CONS-PYMNT BY FCP-CONS-PYMNT.PPCN-INV-ORGN-PRCSS-INST
            (
            "READ02",
            new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") },
            1
            );
            READ02:
            while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("READ02")))
            {
                pnd_Pymnt_Nbr.reset();                                                                                                                                    //Natural: RESET #PYMNT-NBR
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr().notEquals(pnd_Pymnt_Key_Cntrct_Ppcn_Nbr) || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte().notEquals(pnd_Pymnt_Key_Cntrct_Invrse_Dte)  //Natural: IF FCP-CONS-PYMNT.CNTRCT-PPCN-NBR NE #PYMNT-KEY.CNTRCT-PPCN-NBR OR FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE NE #PYMNT-KEY.CNTRCT-INVRSE-DTE OR FCP-CONS-PYMNT.CNTRCT-ORGN-CDE NE #PYMNT-KEY.CNTRCT-ORGN-CDE OR FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NBR NE #PYMNT-KEY.PYMNT-PRCSS-SEQ-NBR
                    || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().notEquals(pnd_Pymnt_Key_Cntrct_Orgn_Cde) || ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr().notEquals(pnd_Pymnt_Key_Pymnt_Prcss_Seq_Nbr)))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet1006 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF FCP-CONS-PYMNT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 2
                if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(2))))
                {
                    decideConditionsMet1006++;
                    //* TMM-2001-11
                    pnd_Pymnt_Nbr.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "E", ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Scrty_Nbr()));                  //Natural: COMPRESS 'E' FCP-CONS-PYMNT.PYMNT-CHECK-SCRTY-NBR INTO #PYMNT-NBR LEAVING NO
                }                                                                                                                                                         //Natural: VALUE 3,4,9
                else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(3) || ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(4) 
                    || ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(9))))
                {
                    decideConditionsMet1006++;
                    pnd_Pymnt_Nbr.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "G", ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Scrty_Nbr()));                  //Natural: COMPRESS 'G' FCP-CONS-PYMNT.PYMNT-CHECK-SCRTY-NBR INTO #PYMNT-NBR LEAVING NO
                }                                                                                                                                                         //Natural: VALUE 8
                else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(8))))
                {
                    decideConditionsMet1006++;
                    pnd_Pymnt_Nbr.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "R", ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Scrty_Nbr()));                  //Natural: COMPRESS 'R' FCP-CONS-PYMNT.PYMNT-CHECK-SCRTY-NBR INTO #PYMNT-NBR LEAVING NO
                }                                                                                                                                                         //Natural: NONE VALUES
                else if (condition())
                {
                    pnd_Pymnt_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Nbr());                                                                              //Natural: MOVE FCP-CONS-PYMNT.PYMNT-CHECK-NBR TO #PYMNT-NBR
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Gross_Amt.compute(new ComputeParameters(false, pnd_Gross_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero())); //Natural: ASSIGN #GROSS-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:INV-ACCT-COUNT ) + 0
            pnd_Ivc_Amt.compute(new ComputeParameters(false, pnd_Ivc_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero())); //Natural: ASSIGN #IVC-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-IVC-AMT ( 1:INV-ACCT-COUNT ) + 0
            pnd_Taxes.compute(new ComputeParameters(false, pnd_Taxes), pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Fed_C_Tax_Withhold().add(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Sta_C_Tax_Withhold()).add(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Loc_C_Tax_Withhold())); //Natural: ASSIGN #TAXES := WF-PYMNT-ADDR-GRP.TAX-FED-C-TAX-WITHHOLD + WF-PYMNT-ADDR-GRP.TAX-STA-C-TAX-WITHHOLD + WF-PYMNT-ADDR-GRP.TAX-LOC-C-TAX-WITHHOLD
            pnd_Taxes.nsubtract(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));                //Natural: ASSIGN #TAXES := #TAXES - WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:INV-ACCT-COUNT )
            pnd_Taxes.nsubtract(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));               //Natural: ASSIGN #TAXES := #TAXES - WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:INV-ACCT-COUNT )
            pnd_Taxes.nsubtract(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));               //Natural: ASSIGN #TAXES := #TAXES - WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:INV-ACCT-COUNT )
            pnd_Deductions.compute(new ComputeParameters(false, pnd_Deductions), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*").add(getZero()));           //Natural: ASSIGN #DEDUCTIONS := WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( * ) + 0
            pnd_Net_Amt.compute(new ComputeParameters(false, pnd_Net_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero())); //Natural: ASSIGN #NET-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:INV-ACCT-COUNT ) + 0
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Ind().equals(getZero())))                                                                          //Natural: IF WF-PYMNT-ADDR-GRP.ANNT-SOC-SEC-IND = 0
            {
                pnd_Soc_Sec_Nbr.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(),new ReportEditMask("999-99-9999"));                                     //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) TO #SOC-SEC-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Soc_Sec_Nbr.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(),new ReportEditMask("99-9999999"));                                      //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.ANNT-SOC-SEC-NBR ( EM = 99-9999999 ) TO #SOC-SEC-NBR
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1036 = 0;                                                                                                                            //Natural: DECIDE ON EVERY VALUE OF WF-PYMNT-ADDR-GRP.GTN-RPT-CODE ( 8:10 );//Natural: VALUE 03
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(8).equals(3) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(9).equals(3) 
                || pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(10).equals(3)))
            {
                decideConditionsMet1036++;
                                                                                                                                                                          //Natural: PERFORM PRINT-NO-TAXES
                sub_Print_No_Taxes();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 04
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(8).equals(4) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(9).equals(4) 
                || pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(10).equals(4)))
            {
                decideConditionsMet1036++;
                                                                                                                                                                          //Natural: PERFORM PRINT-NO-DEDUCTIONS
                sub_Print_No_Deductions();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 05
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(8).equals(5) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(9).equals(5) 
                || pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(10).equals(5)))
            {
                decideConditionsMet1036++;
                                                                                                                                                                          //Natural: PERFORM PRINT-NO-TAXES
                sub_Print_No_Taxes();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PRINT-NO-DEDUCTIONS
                sub_Print_No_Deductions();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE VALUES
            if (condition(decideConditionsMet1036 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //* ******************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-NO-TAXES
            //* ******************
            //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-NO-DEDUCTIONS
        }                                                                                                                                                                 //Natural: END-SORT
        endSort();
        if (condition(pnd_T_Gross_Amt.equals(getZero()) && pnd_T_Taxes.equals(getZero()) && pnd_T_Net_Amt.equals(getZero())))                                             //Natural: IF #T-GROSS-AMT = 0 AND #T-TAXES = 0 AND #T-NET-AMT = 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 01 ) ///// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '***   All Taxes were Included in the Net Calculation      ***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***   All Taxes were Included in the Net Calculation      ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, NEWLINE,NEWLINE,new ReportTAsterisk(pnd_Gross_Amt),pnd_T_Gross_Amt, new ReportEditMask ("ZZZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ivc_Amt),pnd_T_Ivc_Amt,  //Natural: WRITE ( 01 ) // T*#GROSS-AMT #T-GROSS-AMT ( EM = ZZZZZ,ZZZ.99- ) T*#IVC-AMT #T-IVC-AMT ( EM = ZZZZZ,ZZZ.99- ) T*#TAXES #T-TAXES ( EM = ZZZZZ,ZZZ.99- ) T*#NET-AMT #T-NET-AMT ( EM = ZZZZZZ,ZZZ.99- )
                new ReportEditMask ("ZZZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Taxes),pnd_T_Taxes, new ReportEditMask ("ZZZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Net_Amt),pnd_T_Net_Amt, 
                new ReportEditMask ("ZZZZZZ,ZZZ.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_D_Gross_Amt.equals(getZero()) && pnd_D_Deductions.equals(getZero()) && pnd_D_Net_Amt.equals(getZero())))                                        //Natural: IF #D-GROSS-AMT = 0 AND #D-DEDUCTIONS = 0 AND #D-NET-AMT = 0
        {
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 02 ) ///// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*** All Deductions were Included in the Net Calculation   ***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"*** All Deductions were Included in the Net Calculation   ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, NEWLINE,NEWLINE,new ReportTAsterisk(pnd_Gross_Amt),pnd_D_Gross_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Deductions),pnd_D_Deductions,  //Natural: WRITE ( 02 ) // T*#GROSS-AMT #D-GROSS-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#DEDUCTIONS #D-DEDUCTIONS ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#NET-AMT #D-NET-AMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Net_Amt),pnd_D_Net_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_No_Taxes() throws Exception                                                                                                                    //Natural: PRINT-NO-TAXES
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************
        getReports().display(1, new ReportMatrixColumnUnderline("-"),"Combine/Number",                                                                                    //Natural: DISPLAY ( 01 ) ( UC = - ) 'Combine/Number' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR ( EM = XXXXXXX-X ) '/Contract#' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) '/Soc Sec No' #SOC-SEC-NBR '/Payee' WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) ( AL = 30 ) '/Gross Amt' #GROSS-AMT ( EM = Z,ZZZ,ZZZ.99- ) '/IVC Amt' #IVC-AMT ( EM = Z,ZZZ,ZZZ.99- ) 'Taxes Calculated/Not Withheld' #TAXES ( EM = Z,ZZZ,ZZZ.99- ) '/Net Payment' #NET-AMT ( EM = ZZ,ZZZ,ZZZ.99- ) 'Payment/Number' #PYMNT-NBR
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"/Soc Sec No",
        		pnd_Soc_Sec_Nbr,"/Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), new AlphanumericLength (30),"/Gross Amt",
        		pnd_Gross_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99-"),"/IVC Amt",
        		pnd_Ivc_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99-"),"Taxes Calculated/Not Withheld",
        		pnd_Taxes, new ReportEditMask ("Z,ZZZ,ZZZ.99-"),"/Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"),"Payment/Number",
        		pnd_Pymnt_Nbr);
        if (Global.isEscape()) return;
        pnd_T_Gross_Amt.nadd(pnd_Gross_Amt);                                                                                                                              //Natural: ADD #GROSS-AMT TO #T-GROSS-AMT
        pnd_T_Ivc_Amt.nadd(pnd_Ivc_Amt);                                                                                                                                  //Natural: ADD #IVC-AMT TO #T-IVC-AMT
        pnd_T_Taxes.nadd(pnd_Taxes);                                                                                                                                      //Natural: ADD #TAXES TO #T-TAXES
        pnd_T_Net_Amt.nadd(pnd_Net_Amt);                                                                                                                                  //Natural: ADD #NET-AMT TO #T-NET-AMT
        //*  PRINT-NO-DEDUCTIONS
    }
    private void sub_Print_No_Deductions() throws Exception                                                                                                               //Natural: PRINT-NO-DEDUCTIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************
        getReports().display(2, new ReportMatrixColumnUnderline("-"),"Combine/Number",                                                                                    //Natural: DISPLAY ( 02 ) ( UC = - ) 'Combine/Number' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR ( EM = XXXXXXX-X ) '/Contract#' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) '/Soc Sec No' #SOC-SEC-NBR '/Payee' WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) '/Gross Amt' #GROSS-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) 'Deductions/Not Taken' #DEDUCTIONS ( EM = ZZZ,ZZZ,ZZZ.99- ) '/Net Payment' #NET-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) 'Payment/Number' #PYMNT-NBR
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"/Soc Sec No",
        		pnd_Soc_Sec_Nbr,"/Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1),"/Gross Amt",
        		pnd_Gross_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"Deductions/Not Taken",
        		pnd_Deductions, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"/Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"Payment/Number",
        		pnd_Pymnt_Nbr);
        if (Global.isEscape()) return;
        pnd_D_Gross_Amt.nadd(pnd_Gross_Amt);                                                                                                                              //Natural: ADD #GROSS-AMT TO #D-GROSS-AMT
        pnd_D_Deductions.nadd(pnd_Deductions);                                                                                                                            //Natural: ADD #DEDUCTIONS TO #D-DEDUCTIONS
        pnd_D_Net_Amt.nadd(pnd_Net_Amt);                                                                                                                                  //Natural: ADD #NET-AMT TO #D-NET-AMT
        //*  PRINT-NO-DEDUCTIONS
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,
            "**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),"- 1",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(49),"TAXES NOT WITHHELD FOR",pdaFcpacntl.getCntl_Cntl_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"));
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),"- 2",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(40),"VOLUNTARY DEDUCTIONS NOT TAKEN FOR",pdaFcpacntl.getCntl_Cntl_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"));

        getReports().setDisplayColumns(1, new ReportMatrixColumnUnderline("-"),"Combine/Number",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"/Soc Sec No",
        		pnd_Soc_Sec_Nbr,"/Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme(), new AlphanumericLength (30),"/Gross Amt",
        		pnd_Gross_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99-"),"/IVC Amt",
        		pnd_Ivc_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99-"),"Taxes Calculated/Not Withheld",
        		pnd_Taxes, new ReportEditMask ("Z,ZZZ,ZZZ.99-"),"/Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"),"Payment/Number",
        		pnd_Pymnt_Nbr);
        getReports().setDisplayColumns(2, new ReportMatrixColumnUnderline("-"),"Combine/Number",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"/Soc Sec No",
        		pnd_Soc_Sec_Nbr,"/Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme(),"/Gross Amt",
        		pnd_Gross_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"Deductions/Not Taken",
        		pnd_Deductions, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"/Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"Payment/Number",
        		pnd_Pymnt_Nbr);
    }
}
