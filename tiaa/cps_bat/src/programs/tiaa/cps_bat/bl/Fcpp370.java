/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:56 PM
**        * FROM NATURAL PROGRAM : Fcpp370
************************************************************
**        * FILE NAME            : Fcpp370.java
**        * CLASS NAME           : Fcpp370
**        * INSTANCE NAME        : Fcpp370
************************************************************
************************************************************************
* PROGRAM  : FCPP370
* SYSTEM   : CPS
* FUNCTION : THIS PROGRAM EXTRACTS RECORDS FROM THE DATABASE, FOR A
*            SPECIFIED PERIOD (DATE), FOR IADEATH,
*            CHECK PRINTING PROCESS.
*            WHILE CREATING THE EXTRACT, THE PROGRAM ALSO DETERMINES
*            THE CHARACTERISTIC OF THE PRINTING DOCUMENT. I.E., IS IT
*            GOING TO BE A SIMPLEX, A DUPLEX, A TRIPLEX, ETC. IN
*            ADDITION, FOR EACH EXTRACTED RECORD, IT DETERMINES THE
*            PAGE ON WHICH THE LINE IS TO BE PRINTED.
*
*            LATER IN THE PROCESS, THE EXTRACTED RECORDS,
*            ARE SORTED AND PRINTED.
* HISTORY:
*
*  12/07/98  ROXAN CARREON
*            MODIFY PROGRAM TO ACCEPT MULTIPLE INV-ACCT OCCURRENCES
*
*     07/99  VALERY FULTON
*            ADD  CNR-ORGNL-INVRSE-DTE FIELD TO #WS-HEADER-RECORD
*
*  08/04/99  LEON GURTOVNIK
*            MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*  05/00     TOM MCGEE ADD CNTRCT-ANNTY-INS-TYPE
*                         CNTRCT-ANNTY-TYPE-CDE
*  08/25/00  M.ACLAN
*            ADDED CNTRCT-TYPE-CDE ON WORK FILE 08
* CNTRCT-INSURANCE-OPTION
*                         CNTRCT-LIFE-CONTINGENCY TO #WS-HEADER-REC
* 09/11/00 : A. YOUNG    -  RE-COMPILED DUE TO FCPLPMNT.
*    11/02 : R. CARREON  -  STOW. EGTRRA CHANGES IN LDA.
*    12/02 : R. CARREON  -  CHANGED CALENDAR FILE ACCESS TO CALL CPWA115
*            ALL REFERENCE TO FCPA115 WAS CHANGED TO CPWA115
* 09/29/06 : R. WILKINS (RHW) ADD INTERNAL SEQUENCE TO END OF FILE "8"
* 04/26/2008: ROTH-MAJOR1 - (AER) - STOW ONLY
* 12/09/2011: R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPLPMNT
* 08/23/2019 : REPLACING PDQ MODULES WITH ADS MODULES I.E.
*              PDQA360,PDQA362,PDQN360,PDQN362 TO
*              ADSA360,ADSA362,ADSN360,ADSN362 RESPECTIVELY. -TAG:VIKRAM
* 01/15/2020: CTS CPS SUNSET (CHG694525) TAG: CTS-0115
*             MODIFED TO PROCESS STOPS ONLY.IT WILL NOT REISSUE CHECKS.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp370 extends BLNatBase
{
    // Data Areas
    private PdaAdsa360 pdaAdsa360;
    private PdaAdsa362 pdaAdsa362;
    private PdaCpwa115 pdaCpwa115;
    private LdaFcpladdr ldaFcpladdr;
    private LdaFcplcntu ldaFcplcntu;
    private LdaFcplpmnt ldaFcplpmnt;
    private LdaFcpl370 ldaFcpl370;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Abend_Code;
    private DbsField pnd_Processing_Futures;
    private DbsField pnd_I1;
    private DbsField pnd_I2;
    private DbsField pnd_Ci1;
    private DbsField pnd_Ci2;
    private DbsField pnd_C_Rcrd_10;
    private DbsField pnd_C_Rcrd_20;
    private DbsField pnd_C_Rcrd_30;
    private DbsField pnd_C_Selected_Pymnts;
    private DbsField pnd_Program;
    private DbsField pnd_Nas_Start_Key;

    private DbsGroup pnd_Nas_Start_Key__R_Field_1;
    private DbsField pnd_Nas_Start_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Nas_Start_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Nas_Start_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Nas_Start_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Nas_Start_Key_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;
    private DbsField pnd_Date_Numeric;

    private DbsGroup pnd_Date_Numeric__R_Field_2;
    private DbsField pnd_Date_Numeric_Pnd_Date_Alpha;

    private DbsGroup pnd_Ct_Cntrct_Typ;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Type_Code;
    private DbsField pnd_Ws_Prime_Key;
    private DbsField pnd_Counter;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_K;
    private DbsField pnd_J;

    private DbsGroup pnd_Ws_Key;
    private DbsField pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Check_Date;

    private DbsGroup pnd_Ws_Check_Date__R_Field_3;
    private DbsField pnd_Ws_Check_Date_Filler;
    private DbsField pnd_Ws_Check_Date_Pnd_Ws_Inverse_7_Bytes;
    private DbsField pnd_Ws_First_Time;
    private DbsField pnd_Ws_Inv_Acct_Filler;
    private DbsField pnd_Ws_Name_N_Address_Filler;
    private DbsField pnd_At_Top_Of_Body;
    private DbsField pnd_Ws_Lines_In_Grp;
    private DbsField pnd_Lines_Remained_On_Page;
    private DbsField pnd_Ws_T1_Lines_On_Page;
    private DbsField pnd_Ws_Current_X_Plex;
    private DbsField pnd_Jan_Dec;
    private DbsField pnd_Apr_Mar;
    private DbsField pnd_May_Apr;
    private DbsField pnd_Settlmnt_Dte;

    private DbsGroup pnd_Settlmnt_Dte__R_Field_4;
    private DbsField pnd_Settlmnt_Dte_Pnd_Settlmnt_Yyyy;
    private DbsField pnd_Settlmnt_Dte_Pnd_Settlmnt_Mm;
    private DbsField pnd_Settlmnt_Dte_Pnd_Settlmnt_Dd;
    private DbsField pnd_W_Dte;

    private DbsGroup pnd_W_Dte__R_Field_5;
    private DbsField pnd_W_Dte_Pnd_W_Yyyy;
    private DbsField pnd_W_Dte_Pnd_W_Mm;
    private DbsField pnd_W_Dte_Pnd_W_Dd;
    private DbsField pnd_Issue_Dte;
    private DbsField pnd_First;
    private DbsField pnd_I;

    private DbsGroup pnd_Lcv;
    private DbsField pnd_Lcv_Inv_Acct_Unit_Qty;
    private DbsField pnd_Lcv_Inv_Acct_Unit_Value;
    private DbsField pnd_Lcv_Inv_Acct_Settl_Amt;
    private DbsField pnd_Lcv_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Lcv_Inv_Acct_Dci_Amt;
    private DbsField pnd_Lcv_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Lcv_Inv_Acct_Start_Accum_Amt;
    private DbsField pnd_Lcv_Inv_Acct_End_Accum_Amt;
    private DbsField pnd_Lcv_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Lcv_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Lcv_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pnd_Lcv_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Lcv_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Lcv_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Lcv_Inv_Acct_Exp_Amt;
    private DbsField pnd_Lcv_Pymnt_Ded_Cde;
    private DbsField pnd_Lcv_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Lcv_Pymnt_Ded_Amt;

    private DbsGroup pnd_Lcv_Pnd_Lcv_Array_Info;
    private DbsField pnd_Lcv_Inv_Acct_Cde;
    private DbsField pnd_Lcv_Inv_Acct_Fed_Cde;
    private DbsField pnd_Lcv_Inv_Acct_State_Cde;
    private DbsField pnd_Lcv_Inv_Acct_Local_Cde;
    private DbsField pnd_Lcv_Inv_Acct_Ivc_Ind;
    private DbsField pnd_Lcv_Inv_Acct_Valuat_Period;

    private DbsGroup pnd_Lcv_Pnd_Lcv_Single_Info;
    private DbsField pnd_Lcv_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Lcv_Cntrct_Payee_Cde;
    private DbsField pnd_Lcv_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Lcv_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Lcv_Cntrct_Option_Cde;
    private DbsField pnd_Lcv_Cntrct_Mode_Cde;
    private DbsField pnd_Lcv_Pymnt_Settlmnt_Dte;

    private DbsGroup pnd_Ws_Work_Variables;
    private DbsField pnd_Ws_Work_Variables_Pnd_X;
    private DbsField pnd_Ws_Work_Variables_Pnd_R;
    private DbsField pnd_Ws_Work_Variables_Pnd_Level_20;
    private DbsField pnd_Ws_Work_Variables_Pnd_First_Pass;
    private DbsField pnd_Ws_Work_Variables_Pnd_Ws_Reval;
    private DbsField pnd_Ws_Work_Variables_Pnd_Ws_Settl_Amt;
    private DbsField pnd_Ws_Work_Variables_Pnd_Ws_Negative_Ovr_Pymnt;
    private DbsField pnd_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Ded_Run_Tot;
    private DbsField pnd_Last_Side;
    private DbsField pnd_Used_Lines;
    private DbsField pnd_Total_Sett;
    private DbsField pnd_Internal_Seq_Number;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAdsa360 = new PdaAdsa360(localVariables);
        pdaAdsa362 = new PdaAdsa362(localVariables);
        pdaCpwa115 = new PdaCpwa115(localVariables);
        ldaFcpladdr = new LdaFcpladdr();
        registerRecord(ldaFcpladdr);
        registerRecord(ldaFcpladdr.getVw_fcp_Cons_Addr());
        ldaFcplcntu = new LdaFcplcntu();
        registerRecord(ldaFcplcntu);
        registerRecord(ldaFcplcntu.getVw_fcp_Cons_Cntrl());
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());
        ldaFcpl370 = new LdaFcpl370();
        registerRecord(ldaFcpl370);

        // Local Variables
        pnd_Abend_Code = localVariables.newFieldInRecord("pnd_Abend_Code", "#ABEND-CODE", FieldType.INTEGER, 2);
        pnd_Processing_Futures = localVariables.newFieldInRecord("pnd_Processing_Futures", "#PROCESSING-FUTURES", FieldType.BOOLEAN, 1);
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ci1 = localVariables.newFieldInRecord("pnd_Ci1", "#CI1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ci2 = localVariables.newFieldInRecord("pnd_Ci2", "#CI2", FieldType.PACKED_DECIMAL, 3);
        pnd_C_Rcrd_10 = localVariables.newFieldInRecord("pnd_C_Rcrd_10", "#C-RCRD-10", FieldType.INTEGER, 4);
        pnd_C_Rcrd_20 = localVariables.newFieldInRecord("pnd_C_Rcrd_20", "#C-RCRD-20", FieldType.INTEGER, 4);
        pnd_C_Rcrd_30 = localVariables.newFieldInRecord("pnd_C_Rcrd_30", "#C-RCRD-30", FieldType.INTEGER, 4);
        pnd_C_Selected_Pymnts = localVariables.newFieldInRecord("pnd_C_Selected_Pymnts", "#C-SELECTED-PYMNTS", FieldType.INTEGER, 4);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Nas_Start_Key = localVariables.newFieldInRecord("pnd_Nas_Start_Key", "#NAS-START-KEY", FieldType.STRING, 31);

        pnd_Nas_Start_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Nas_Start_Key__R_Field_1", "REDEFINE", pnd_Nas_Start_Key);
        pnd_Nas_Start_Key_Cntrct_Ppcn_Nbr = pnd_Nas_Start_Key__R_Field_1.newFieldInGroup("pnd_Nas_Start_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Nas_Start_Key_Cntrct_Payee_Cde = pnd_Nas_Start_Key__R_Field_1.newFieldInGroup("pnd_Nas_Start_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Nas_Start_Key_Cntrct_Orgn_Cde = pnd_Nas_Start_Key__R_Field_1.newFieldInGroup("pnd_Nas_Start_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Nas_Start_Key_Cntrct_Invrse_Dte = pnd_Nas_Start_Key__R_Field_1.newFieldInGroup("pnd_Nas_Start_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Nas_Start_Key_Pymnt_Prcss_Seq_Nbr = pnd_Nas_Start_Key__R_Field_1.newFieldInGroup("pnd_Nas_Start_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);
        pnd_Date_Numeric = localVariables.newFieldInRecord("pnd_Date_Numeric", "#DATE-NUMERIC", FieldType.NUMERIC, 8);

        pnd_Date_Numeric__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Numeric__R_Field_2", "REDEFINE", pnd_Date_Numeric);
        pnd_Date_Numeric_Pnd_Date_Alpha = pnd_Date_Numeric__R_Field_2.newFieldInGroup("pnd_Date_Numeric_Pnd_Date_Alpha", "#DATE-ALPHA", FieldType.STRING, 
            8);

        pnd_Ct_Cntrct_Typ = localVariables.newGroupArrayInRecord("pnd_Ct_Cntrct_Typ", "#CT-CNTRCT-TYP", new DbsArrayController(1, 50));
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Type_Code", "#TYPE-CODE", FieldType.STRING, 2);
        pnd_Ws_Prime_Key = localVariables.newFieldInRecord("pnd_Ws_Prime_Key", "#WS-PRIME-KEY", FieldType.STRING, 11);
        pnd_Counter = localVariables.newFieldInRecord("pnd_Counter", "#COUNTER", FieldType.PACKED_DECIMAL, 10);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.INTEGER, 2);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);

        pnd_Ws_Key = localVariables.newGroupInRecord("pnd_Ws_Key", "#WS-KEY");
        pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr", "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 
            2);
        pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr", "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 
            2);
        pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex", "#WS-SIMPLEX-DUPLEX-MULTIPLEX", 
            FieldType.STRING, 1);
        pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code", "#WS-SAVE-CONTRACT-HOLD-CODE", 
            FieldType.STRING, 4);
        pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Nbr", "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Seq_Nbr", "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr", "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Check_Date = localVariables.newFieldInRecord("pnd_Ws_Check_Date", "#WS-CHECK-DATE", FieldType.NUMERIC, 8);

        pnd_Ws_Check_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Check_Date__R_Field_3", "REDEFINE", pnd_Ws_Check_Date);
        pnd_Ws_Check_Date_Filler = pnd_Ws_Check_Date__R_Field_3.newFieldInGroup("pnd_Ws_Check_Date_Filler", "FILLER", FieldType.STRING, 1);
        pnd_Ws_Check_Date_Pnd_Ws_Inverse_7_Bytes = pnd_Ws_Check_Date__R_Field_3.newFieldInGroup("pnd_Ws_Check_Date_Pnd_Ws_Inverse_7_Bytes", "#WS-INVERSE-7-BYTES", 
            FieldType.NUMERIC, 7);
        pnd_Ws_First_Time = localVariables.newFieldInRecord("pnd_Ws_First_Time", "#WS-FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Ws_Inv_Acct_Filler = localVariables.newFieldInRecord("pnd_Ws_Inv_Acct_Filler", "#WS-INV-ACCT-FILLER", FieldType.STRING, 180);
        pnd_Ws_Name_N_Address_Filler = localVariables.newFieldInRecord("pnd_Ws_Name_N_Address_Filler", "#WS-NAME-N-ADDRESS-FILLER", FieldType.STRING, 
            8);
        pnd_At_Top_Of_Body = localVariables.newFieldInRecord("pnd_At_Top_Of_Body", "#AT-TOP-OF-BODY", FieldType.BOOLEAN, 1);
        pnd_Ws_Lines_In_Grp = localVariables.newFieldInRecord("pnd_Ws_Lines_In_Grp", "#WS-LINES-IN-GRP", FieldType.PACKED_DECIMAL, 3);
        pnd_Lines_Remained_On_Page = localVariables.newFieldInRecord("pnd_Lines_Remained_On_Page", "#LINES-REMAINED-ON-PAGE", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_T1_Lines_On_Page = localVariables.newFieldArrayInRecord("pnd_Ws_T1_Lines_On_Page", "#WS-T1-LINES-ON-PAGE", FieldType.PACKED_DECIMAL, 3, 
            new DbsArrayController(1, 4));
        pnd_Ws_Current_X_Plex = localVariables.newFieldInRecord("pnd_Ws_Current_X_Plex", "#WS-CURRENT-X-PLEX", FieldType.PACKED_DECIMAL, 1);
        pnd_Jan_Dec = localVariables.newFieldInRecord("pnd_Jan_Dec", "#JAN-DEC", FieldType.BOOLEAN, 1);
        pnd_Apr_Mar = localVariables.newFieldInRecord("pnd_Apr_Mar", "#APR-MAR", FieldType.BOOLEAN, 1);
        pnd_May_Apr = localVariables.newFieldInRecord("pnd_May_Apr", "#MAY-APR", FieldType.BOOLEAN, 1);
        pnd_Settlmnt_Dte = localVariables.newFieldInRecord("pnd_Settlmnt_Dte", "#SETTLMNT-DTE", FieldType.STRING, 8);

        pnd_Settlmnt_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_Settlmnt_Dte__R_Field_4", "REDEFINE", pnd_Settlmnt_Dte);
        pnd_Settlmnt_Dte_Pnd_Settlmnt_Yyyy = pnd_Settlmnt_Dte__R_Field_4.newFieldInGroup("pnd_Settlmnt_Dte_Pnd_Settlmnt_Yyyy", "#SETTLMNT-YYYY", FieldType.STRING, 
            4);
        pnd_Settlmnt_Dte_Pnd_Settlmnt_Mm = pnd_Settlmnt_Dte__R_Field_4.newFieldInGroup("pnd_Settlmnt_Dte_Pnd_Settlmnt_Mm", "#SETTLMNT-MM", FieldType.STRING, 
            2);
        pnd_Settlmnt_Dte_Pnd_Settlmnt_Dd = pnd_Settlmnt_Dte__R_Field_4.newFieldInGroup("pnd_Settlmnt_Dte_Pnd_Settlmnt_Dd", "#SETTLMNT-DD", FieldType.STRING, 
            2);
        pnd_W_Dte = localVariables.newFieldInRecord("pnd_W_Dte", "#W-DTE", FieldType.STRING, 8);

        pnd_W_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_W_Dte__R_Field_5", "REDEFINE", pnd_W_Dte);
        pnd_W_Dte_Pnd_W_Yyyy = pnd_W_Dte__R_Field_5.newFieldInGroup("pnd_W_Dte_Pnd_W_Yyyy", "#W-YYYY", FieldType.NUMERIC, 4);
        pnd_W_Dte_Pnd_W_Mm = pnd_W_Dte__R_Field_5.newFieldInGroup("pnd_W_Dte_Pnd_W_Mm", "#W-MM", FieldType.NUMERIC, 2);
        pnd_W_Dte_Pnd_W_Dd = pnd_W_Dte__R_Field_5.newFieldInGroup("pnd_W_Dte_Pnd_W_Dd", "#W-DD", FieldType.NUMERIC, 2);
        pnd_Issue_Dte = localVariables.newFieldInRecord("pnd_Issue_Dte", "#ISSUE-DTE", FieldType.STRING, 6);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 4);

        pnd_Lcv = localVariables.newGroupArrayInRecord("pnd_Lcv", "#LCV", new DbsArrayController(1, 40));
        pnd_Lcv_Inv_Acct_Unit_Qty = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Lcv_Inv_Acct_Unit_Value = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 9, 4);
        pnd_Lcv_Inv_Acct_Settl_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Lcv_Inv_Acct_Ivc_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Lcv_Inv_Acct_Dci_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Lcv_Inv_Acct_Dpi_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Lcv_Inv_Acct_Start_Accum_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Lcv_Inv_Acct_End_Accum_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Lcv_Inv_Acct_Dvdnd_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Lcv_Inv_Acct_Net_Pymnt_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Lcv_Inv_Acct_Adj_Ivc_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Lcv_Inv_Acct_Fdrl_Tax_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Lcv_Inv_Acct_State_Tax_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Lcv_Inv_Acct_Local_Tax_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Lcv_Inv_Acct_Exp_Amt = pnd_Lcv.newFieldInGroup("pnd_Lcv_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Lcv_Pymnt_Ded_Cde = pnd_Lcv.newFieldArrayInGroup("pnd_Lcv_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            10));
        pnd_Lcv_Pymnt_Ded_Payee_Cde = pnd_Lcv.newFieldArrayInGroup("pnd_Lcv_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 8, new DbsArrayController(1, 
            10));
        pnd_Lcv_Pymnt_Ded_Amt = pnd_Lcv.newFieldArrayInGroup("pnd_Lcv_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            10));

        pnd_Lcv_Pnd_Lcv_Array_Info = pnd_Lcv.newGroupInGroup("pnd_Lcv_Pnd_Lcv_Array_Info", "#LCV-ARRAY-INFO");
        pnd_Lcv_Inv_Acct_Cde = pnd_Lcv_Pnd_Lcv_Array_Info.newFieldInGroup("pnd_Lcv_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        pnd_Lcv_Inv_Acct_Fed_Cde = pnd_Lcv_Pnd_Lcv_Array_Info.newFieldInGroup("pnd_Lcv_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 1);
        pnd_Lcv_Inv_Acct_State_Cde = pnd_Lcv_Pnd_Lcv_Array_Info.newFieldInGroup("pnd_Lcv_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", FieldType.STRING, 
            1);
        pnd_Lcv_Inv_Acct_Local_Cde = pnd_Lcv_Pnd_Lcv_Array_Info.newFieldInGroup("pnd_Lcv_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", FieldType.STRING, 
            1);
        pnd_Lcv_Inv_Acct_Ivc_Ind = pnd_Lcv_Pnd_Lcv_Array_Info.newFieldInGroup("pnd_Lcv_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 1);
        pnd_Lcv_Inv_Acct_Valuat_Period = pnd_Lcv_Pnd_Lcv_Array_Info.newFieldInGroup("pnd_Lcv_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 
            1);

        pnd_Lcv_Pnd_Lcv_Single_Info = pnd_Lcv.newGroupInGroup("pnd_Lcv_Pnd_Lcv_Single_Info", "#LCV-SINGLE-INFO");
        pnd_Lcv_Cntrct_Ppcn_Nbr = pnd_Lcv_Pnd_Lcv_Single_Info.newFieldInGroup("pnd_Lcv_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Lcv_Cntrct_Payee_Cde = pnd_Lcv_Pnd_Lcv_Single_Info.newFieldInGroup("pnd_Lcv_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Lcv_Cntrct_Pymnt_Type_Ind = pnd_Lcv_Pnd_Lcv_Single_Info.newFieldInGroup("pnd_Lcv_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 
            1);
        pnd_Lcv_Cntrct_Sttlmnt_Type_Ind = pnd_Lcv_Pnd_Lcv_Single_Info.newFieldInGroup("pnd_Lcv_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 
            1);
        pnd_Lcv_Cntrct_Option_Cde = pnd_Lcv_Pnd_Lcv_Single_Info.newFieldInGroup("pnd_Lcv_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2);
        pnd_Lcv_Cntrct_Mode_Cde = pnd_Lcv_Pnd_Lcv_Single_Info.newFieldInGroup("pnd_Lcv_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        pnd_Lcv_Pymnt_Settlmnt_Dte = pnd_Lcv_Pnd_Lcv_Single_Info.newFieldInGroup("pnd_Lcv_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);

        pnd_Ws_Work_Variables = localVariables.newGroupInRecord("pnd_Ws_Work_Variables", "#WS-WORK-VARIABLES");
        pnd_Ws_Work_Variables_Pnd_X = pnd_Ws_Work_Variables.newFieldInGroup("pnd_Ws_Work_Variables_Pnd_X", "#X", FieldType.NUMERIC, 4);
        pnd_Ws_Work_Variables_Pnd_R = pnd_Ws_Work_Variables.newFieldInGroup("pnd_Ws_Work_Variables_Pnd_R", "#R", FieldType.NUMERIC, 4);
        pnd_Ws_Work_Variables_Pnd_Level_20 = pnd_Ws_Work_Variables.newFieldInGroup("pnd_Ws_Work_Variables_Pnd_Level_20", "#LEVEL-20", FieldType.NUMERIC, 
            4);
        pnd_Ws_Work_Variables_Pnd_First_Pass = pnd_Ws_Work_Variables.newFieldInGroup("pnd_Ws_Work_Variables_Pnd_First_Pass", "#FIRST-PASS", FieldType.NUMERIC, 
            1);
        pnd_Ws_Work_Variables_Pnd_Ws_Reval = pnd_Ws_Work_Variables.newFieldInGroup("pnd_Ws_Work_Variables_Pnd_Ws_Reval", "#WS-REVAL", FieldType.NUMERIC, 
            1);
        pnd_Ws_Work_Variables_Pnd_Ws_Settl_Amt = pnd_Ws_Work_Variables.newFieldInGroup("pnd_Ws_Work_Variables_Pnd_Ws_Settl_Amt", "#WS-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Work_Variables_Pnd_Ws_Negative_Ovr_Pymnt = pnd_Ws_Work_Variables.newFieldInGroup("pnd_Ws_Work_Variables_Pnd_Ws_Negative_Ovr_Pymnt", "#WS-NEGATIVE-OVR-PYMNT", 
            FieldType.STRING, 4);
        pnd_Cnr_Orgnl_Invrse_Dte = localVariables.newFieldInRecord("pnd_Cnr_Orgnl_Invrse_Dte", "#CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Ded_Run_Tot = localVariables.newFieldInRecord("pnd_Ded_Run_Tot", "#DED-RUN-TOT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Last_Side = localVariables.newFieldInRecord("pnd_Last_Side", "#LAST-SIDE", FieldType.PACKED_DECIMAL, 7);
        pnd_Used_Lines = localVariables.newFieldInRecord("pnd_Used_Lines", "#USED-LINES", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Sett = localVariables.newFieldInRecord("pnd_Total_Sett", "#TOTAL-SETT", FieldType.NUMERIC, 17, 2);
        pnd_Internal_Seq_Number = localVariables.newFieldInRecord("pnd_Internal_Seq_Number", "#INTERNAL-SEQ-NUMBER", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpladdr.initializeValues();
        ldaFcplcntu.initializeValues();
        ldaFcplpmnt.initializeValues();
        ldaFcpl370.initializeValues();

        localVariables.reset();
        pnd_Header0_1.setInitialValue("           CONSOLIDATED PAYMENT SYSTEM");
        pnd_Header0_2.setInitialValue("IA DEATH BATCH EXTRACT CONTROL REPORT");
        pnd_Ws_Prime_Key.setInitialValue("DCD00000000");
        pnd_C.setInitialValue(0);
        pnd_D.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_J.setInitialValue(0);
        pnd_Ws_First_Time.setInitialValue(true);
        pnd_Ws_Inv_Acct_Filler.setInitialValue(" ");
        pnd_Ws_Name_N_Address_Filler.setInitialValue(" ");
        pnd_Ws_T1_Lines_On_Page.getValue(1).setInitialValue(12);
        pnd_Ws_T1_Lines_On_Page.getValue(2).setInitialValue(16);
        pnd_Ws_T1_Lines_On_Page.getValue(3).setInitialValue(36);
        pnd_Ws_T1_Lines_On_Page.getValue(4).setInitialValue(36);
        pnd_First.setInitialValue(true);
        pnd_I.setInitialValue(1);
        pnd_Ws_Work_Variables_Pnd_First_Pass.setInitialValue(0);
        pnd_Ws_Work_Variables_Pnd_Ws_Reval.setInitialValue(0);
        pnd_Ws_Work_Variables_Pnd_Ws_Settl_Amt.setInitialValue(0);
        pnd_Ws_Work_Variables_Pnd_Ws_Negative_Ovr_Pymnt.setInitialValue("   ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp370() throws Exception
    {
        super("Fcpp370");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt0, 0);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60 ZP = ON
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        ldaFcplcntu.getVw_fcp_Cons_Cntrl().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) FCP-CONS-CNTRL BY CNTL-ORG-CDE-INVRSE-DTE = 'DC'
        (
        "READ01",
        new Wc[] { new Wc("CNTL_ORG_CDE_INVRSE_DTE", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTL_ORG_CDE_INVRSE_DTE", "ASC") },
        1
        );
        READ01:
        while (condition(ldaFcplcntu.getVw_fcp_Cons_Cntrl().readNextRow("READ01")))
        {
            if (condition(ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("DC")))                                                                                    //Natural: IF FCP-CONS-CNTRL.CNTL-ORGN-CDE = 'DC'
            {
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),Global.getTIME(),"ERROR:  CONTROL RECORD FOUND FOR PRIOR JOB");                            //Natural: WRITE *PROGRAM *TIME 'ERROR:  CONTROL RECORD FOUND FOR PRIOR JOB'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Type_Cde().getValue("*").reset();                                                                                          //Natural: RESET FCP-CONS-CNTRL.CNTL-TYPE-CDE ( * ) FCP-CONS-CNTRL.CNTL-CNT ( * ) FCP-CONS-CNTRL.CNTL-GROSS-AMT ( * ) FCP-CONS-CNTRL.CNTL-NET-AMT ( * )
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue("*").reset();
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue("*").reset();
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue("*").reset();
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Counter.reset();                                                                                                                                              //Natural: RESET #COUNTER #PROCESSING-FUTURES
        pnd_Processing_Futures.reset();
        ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                              //Natural: READ FCP-CONS-PYMNT BY ORGN-STATUS-INVRSE-SEQ STARTING FROM #WS-PRIME-KEY
        (
        "READ_PYMNTS",
        new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Ws_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
        );
        READ_PYMNTS:
        while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("READ_PYMNTS")))
        {
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().notEquals("DC") || ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Stats_Cde().notEquals("D")))             //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE NE 'DC' OR FCP-CONS-PYMNT.PYMNT-STATS-CDE NE 'D'
            {
                if (true) break READ_PYMNTS;                                                                                                                              //Natural: ESCAPE BOTTOM ( READ-PYMNTS. )
            }                                                                                                                                                             //Natural: END-IF
            //*  CTS-0115 >>
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))                                                                     //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  #COUNTER := *COUNTER
            pnd_Counter.nadd(1);                                                                                                                                          //Natural: ASSIGN #COUNTER := #COUNTER + 1
            //*  CTS-0115 <<
            //*  ........................ JFT TO DETERMINE WHEN DATE BOUNDARY OCCURS
            pnd_Settlmnt_Dte.setValueEdited(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Settlmnt_Dte(),new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED FCP-CONS-PYMNT.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO #SETTLMNT-DTE
                                                                                                                                                                          //Natural: PERFORM ACCUM-FOR-CONTROL
            sub_Accum_For_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PYMNTS"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PYMNTS"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM EVALUATE-A-PYMNT-RECORD
            sub_Evaluate_A_Pymnt_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PYMNTS"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PYMNTS"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  .............................. JFT START OF DATE BOUNDARY
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                pnd_First.reset();                                                                                                                                        //Natural: RESET #FIRST
                pnd_Issue_Dte.setValueEdited(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ia_Issue_Dte(),new ReportEditMask("YYYYMM"));                                            //Natural: MOVE EDITED FCP-CONS-PYMNT.PYMNT-IA-ISSUE-DTE ( EM = YYYYMM ) TO #ISSUE-DTE
                //*                                                                                                                                                       //Natural: DECIDE FOR FIRST CONDITION
                short decideConditionsMet836 = 0;                                                                                                                         //Natural: WHEN FCP-CONS-PYMNT.INV-ACCT-CDE ( * ) = 'T' OR = 'G' OR = ' T' OR = ' G' OR = 'TG'
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue("*").equals("T") || ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue("*").equals("G") 
                    || ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue("*").equals(" T") || ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue("*").equals(" G") 
                    || ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue("*").equals("TG")))
                {
                    decideConditionsMet836++;
                    pnd_Jan_Dec.setValue(true);                                                                                                                           //Natural: ASSIGN #JAN-DEC := TRUE
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_May_Apr.setValue(true);                                                                                                                           //Natural: ASSIGN #MAY-APR := TRUE
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),Global.getTIME(),"at end of 'current date' payments:",NEWLINE,"Number of records read....:",       //Natural: WRITE *PROGRAM *TIME 'at end of "current date" payments:' / 'Number of records read....:'#COUNTER / 'Number of payments selected:' #C-SELECTED-PYMNTS
            pnd_Counter,NEWLINE,"Number of payments selected:",pnd_C_Selected_Pymnts);
        if (Global.isEscape()) return;
        //*  ....  WRAP THE LAST PAYMENT
        if (condition(! (pnd_Ws_First_Time.getBoolean())))                                                                                                                //Natural: IF NOT #WS-FIRST-TIME
        {
            //*   IF #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-START(#I) NE
            //*       #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-END(#I) AND
            //*     #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'L'
            //*   GET NEXT 2 PERFORM STATEMENTS INSIDE THE FOR LOOP  -  ROXAN
            //*    FOR #X 1 FCP-CONS-PYMNT.C*INV-ACCT
            //*      PERFORM EXTRACT-LCV-FROM-CRUNCHED-DATA
            //*      PERFORM CRUNCH-LCV-DATA
            //*    END-FOR
            //*  END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-ON-WHICH-SIDE-OF-PAPER
            sub_Print_On_Which_Side_Of_Paper();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRAP-A-PAYMENT
            sub_Wrap_A_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),Global.getTIME(),"AT END OF program PROCESSING:",NEWLINE,"NUMBER OF RECORD TYPE 10:",              //Natural: WRITE *PROGRAM *TIME 'AT END OF program PROCESSING:' / 'NUMBER OF RECORD TYPE 10:' #C-RCRD-10 / 'NUMBER OF RECORD TYPE 20:' #C-RCRD-20 / 'NUMBER OF RECORD TYPE 30:' #C-RCRD-30
            pnd_C_Rcrd_10,NEWLINE,"NUMBER OF RECORD TYPE 20:",pnd_C_Rcrd_20,NEWLINE,"NUMBER OF RECORD TYPE 30:",pnd_C_Rcrd_30);
        if (Global.isEscape()) return;
        //* *--> COMMENTED-OUT 07-24-95 BY FRANK
        //* *--> ALWAYS DISPLAY DATE, FOR ON-LINE FIELD: LAST UPDATE FOR
        //* *IF #COUNTER GT 0
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REC
        sub_Write_Control_Rec();
        if (condition(Global.isEscape())) {return;}
        //* *END-IF
        //*   ROXAN
        if (condition(pnd_Ws_Work_Variables_Pnd_Ws_Negative_Ovr_Pymnt.equals("0007")))                                                                                    //Natural: IF #WS-NEGATIVE-OVR-PYMNT EQ '0007'
        {
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"* --------------------------------------------- *",NEWLINE,"* -                                           - *", //Natural: WRITE // '* --------------------------------------------- *' / '* -                                           - *' / '* -                                           - *' / '* -        CALL HELPDESK  X8444               - *' / '* -                                           - *' / '* -                                           - *' / '* -                                           - *' / '* -                                           - *' / '* --------------------------------------------- *'
                NEWLINE,"* -                                           - *",NEWLINE,"* -        CALL HELPDESK  X8444               - *",NEWLINE,"* -                                           - *",
                NEWLINE,"* -                                           - *",NEWLINE,"* -                                           - *",NEWLINE,"* -                                           - *",
                NEWLINE,"* --------------------------------------------- *");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),Global.getTIME(),"END OF  PROGRAM RUN");                                                           //Natural: WRITE *PROGRAM *TIME 'END OF  PROGRAM RUN'
        if (Global.isEscape()) return;
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVALUATE-A-PYMNT-RECORD
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRAP-A-PAYMENT
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ON-WHICH-SIDE-OF-PAPER
        //* *#I := #I - 1
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-CRUNCHED-FIELDS
        //*  ======================================================================
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CRUNCH-INV-ACCT-FIELDS
        //*  ======================================================================
        //*  ADD FCP-CONS-PYMNT.INV-ACCT-UNIT-QTY(*)
        //*    TO #WS-OCCURS.INV-ACCT-UNIT-QTY(*)
        //*        ADD FCP-CONS-PYMNT.PYMNT-DED-AMT(#I1)
        //*          TO #WS-OCCURS.PYMNT-DED-AMT(#X,#I2)
        //*        ADD FCP-CONS-PYMNT.PYMNT-DED-AMT(#I1)
        //*          TO #WS-OCCURS.PYMNT-DED-AMT(#X,#I2)
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATE-N-WRITE-OCCURS-RECORD
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATE-N-WRITE-OCCURS-RECORD-2
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATE-N-WRITE-NAME-N-ADDR-2
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-MAILING-NAME
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
        //* *#WS-SIMPLEX-DUPLEX-MULTIPLEX := 1
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NAME-N-ADDRESS
        //*  ======================================================================
        //*  GET THE ADDRESS FOR THE CURRENT PAYMENT RECIPIENT
        //*  THE RECORD IS KEPT IN THE VIEW AREA AND IS USED AT WRAP-A-PAYMENT
        //*  TO WRITE OUT ADDRESS RELATED DATA
        //*  ----------------------------------------------------------------------
        //*  ........... SET UP THE START KEY FOR NAME AND ADDRESS
        //*  ..... IF THE ADDRESS RECORD IS MISSING - WE TERMINATE ?
        //*  ======================================================================
        //* *DEFINE SUBROUTINE ACCUM-FOR-CONTROL
        //*  ======================================================================
        //*  DETERMINE INDECIES OF CONTROL COUNTERS
        //*  #CI1 = CONTROLS BY CONTRACT TYPE
        //*  #CI2 = CONTROLS BY PAYMENT TYPE (CHECK, EFT, GLOBAL PAY)
        //*  ---------------------------------------------------------------------
        //*  ............. DETERMINE INDEX BY CONTRACT TYPE
        //* *IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-IND = ' '
        //* *  DECIDE ON FIRST VALUE OF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE
        //* *    VALUE 'L' #CI1 := 1
        //* *    VALUE 'PP'
        //* *      IF FCP-CONS-PYMNT.PYMNT-FTRE-IND = ' '
        //* *        #CI1 := 2
        //* *      ELSE
        //* *        #CI1 := 3
        //* *      END-IF
        //* *    NONE VALUE IGNORE
        //* *  END-DECIDE
        //* *ELSE
        //* *  DECIDE ON FIRST VALUE OF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE
        //* *    VALUE 'L' #CI1 := 17
        //* *    VALUE 'PP'
        //* *      IF FCP-CONS-PYMNT.PYMNT-FTRE-IND = ' '
        //* *        #CI1 := 18
        //* *      ELSE
        //* *        #CI1 := 19
        //* *      END-IF
        //* *    NONE VALUE IGNORE
        //* *  END-DECIDE
        //* *END-IF
        //* *DECIDE ON FIRST VALUE OF FCP-CONS-PYMNT.PYMNT-PAY-TYPE-REQ-IND
        //* *  VALUE 1 #CI2 := 48 /* CHECKS
        //* *  VALUE 2 #CI2 := 49 /* EFT
        //* *  VALUE 3 #CI2 := 50 /* GLOBALS
        //* *  NONE VALUE IGNORE
        //* *END-DECIDE
        //*  ...... ADD FOR CONTROL TOTALS
        //*  ...... ADD FOR CONTROL TOTALS BY CONTRACT TYPE
        //* *FCP-CONS-CNTRL.CNTL-TYPE-CDE (#CI1) := FCP-CONS-PYMNT.CNTRCT-TYPE-CDE
        //* *ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT(*) TO
        //* *  FCP-CONS-CNTRL.CNTL-GROSS-AMT(#CI1)
        //*  ...... ADD FOR CONTROL TOTALS BY PAY TYPE
        //* *ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT(*) TO
        //* *  FCP-CONS-CNTRL.CNTL-GROSS-AMT(#CI2)
        //* *IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
        //* *  ADD 1 TO #C-SELECTED-PYMNTS
        //* *  ADD 1 TO FCP-CONS-CNTRL.CNTL-CNT(#CI1)
        //* *  ADD 1 TO FCP-CONS-CNTRL.CNTL-CNT(#CI2)
        //* *  ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT
        //* *    TO FCP-CONS-CNTRL.CNTL-NET-AMT(#CI2)
        //* *  ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT
        //* *    TO FCP-CONS-CNTRL.CNTL-NET-AMT(#CI1)
        //* *END-IF
        //* *END-SUBROUTINE /* ACCUM-FOR-CONTROL
        //*  ======================================================================
        //* *--> INSERTED 03-14-95 BY FRANK
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-FOR-CONTROL
        //*  ...... ADD FOR CONTROL TOTALS
        //*  ...... ADD FOR CONTROL TOTALS BY CONTRACT TYPE
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXTRACT-LCV-FROM-CRUNCHED-DATA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CRUNCH-LCV-DATA
        //*  ---------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-FOR-FUND-DEDUCTION
    }
    private void sub_Evaluate_A_Pymnt_Record() throws Exception                                                                                                           //Natural: EVALUATE-A-PYMNT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  ........... IF NEW PAYMENT
        //* * 11-27-95 BY FRANK/RITA ADDED TEST FOR COMBINE #
        //*   RESET FIRST REC OF INSTLMNTS - ROXAN
        if (condition(pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num.notEquals(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num()) || (pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr.notEquals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cmbn_Nbr())))) //Natural: IF #WS-SAVE-PYMNT-PRCSS-SEQ-NUM NE FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM OR ( #WS-CNTRCT-CMBN-NBR NE FCP-CONS-PYMNT.CNTRCT-CMBN-NBR )
        {
            pnd_Ws_Work_Variables_Pnd_First_Pass.setValue(1);                                                                                                             //Natural: ASSIGN #FIRST-PASS := 1
            //*  IF FIRST PAYMENT BEING PROCESSED
            if (condition(pnd_Ws_First_Time.getBoolean()))                                                                                                                //Natural: IF #WS-FIRST-TIME
            {
                pnd_Ws_First_Time.setValue(false);                                                                                                                        //Natural: ASSIGN #WS-FIRST-TIME := FALSE
                //*   WRAP THE PREVIOUS PAYMENT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    IF #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-START(#I) NE
                //*        #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-END(#I) AND
                //*      #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'L'
                //*  PLACE NEXT 2 PERFORM STATEMENTS WITHIN THE LOOP           /* ROXAN
                //* *    FOR #R 1 #I
                //* *    FOR #R 1 FCP-CONS-PYMNT.C*INV-ACCT
                //*        PERFORM EXTRACT-LCV-FROM-CRUNCHED-DATA
                //*        PERFORM CRUNCH-LCV-DATA
                //*      END-FOR
                //*    END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-ON-WHICH-SIDE-OF-PAPER
                sub_Print_On_Which_Side_Of_Paper();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRAP-A-PAYMENT
                sub_Wrap_A_Payment();
                if (condition(Global.isEscape())) {return;}
                //*  JFT TO TRIGGER START OF DATE BOUNDARY LOGIC
            }                                                                                                                                                             //Natural: END-IF
            pnd_First.setValue(true);                                                                                                                                     //Natural: ASSIGN #FIRST := TRUE
            pnd_I.resetInitial();                                                                                                                                         //Natural: RESET INITIAL #I #WS-OCCURS #APR-MAR #JAN-DEC #MAY-APR
            ldaFcpl370.getPnd_Ws_Occurs().resetInitial();
            pnd_Apr_Mar.resetInitial();
            pnd_Jan_Dec.resetInitial();
            pnd_May_Apr.resetInitial();
            pnd_Ws_Work_Variables_Pnd_Ws_Reval.reset();                                                                                                                   //Natural: RESET #WS-REVAL #LEVEL-20 #X
            pnd_Ws_Work_Variables_Pnd_Level_20.reset();
            pnd_Ws_Work_Variables_Pnd_X.reset();
            //*  RITA
            //*  #I - RECEIVING
            //*  #X - GIVING
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
            sub_Initialize_Fields();
            if (condition(Global.isEscape())) {return;}
            FOR01:                                                                                                                                                        //Natural: FOR #I #I FCP-CONS-PYMNT.C*INV-ACCT
            for (pnd_I.setValue(pnd_I); condition(pnd_I.lessOrEqual(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct())); pnd_I.nadd(1))
            {
                pnd_Ws_Work_Variables_Pnd_X.setValue(pnd_I);                                                                                                              //Natural: ASSIGN #X := #I
                                                                                                                                                                          //Natural: PERFORM INIT-CRUNCHED-FIELDS
                sub_Init_Crunched_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  #I = C*INV
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_I.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct());                                                                                           //Natural: ASSIGN #I := FCP-CONS-PYMNT.C*INV-ACCT
            pnd_Ws_Work_Variables_Pnd_Level_20.setValue(pnd_Ws_Work_Variables_Pnd_X);                                                                                     //Natural: ASSIGN #LEVEL-20 := #X
                                                                                                                                                                          //Natural: PERFORM READ-NAME-N-ADDRESS
            sub_Read_Name_N_Address();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Work_Variables_Pnd_X.setValue(0);                                                                                                                      //Natural: ASSIGN #X := 0
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  BREAK IN DATE RANGE
            if (condition((pnd_Settlmnt_Dte_Pnd_Settlmnt_Mm.equals("01") && pnd_Jan_Dec.getBoolean()) || (pnd_Settlmnt_Dte_Pnd_Settlmnt_Mm.equals("04")                   //Natural: IF ( #SETTLMNT-MM = '01' AND #JAN-DEC ) OR ( #SETTLMNT-MM = '04' AND #APR-MAR ) OR ( #SETTLMNT-MM = '05' AND #MAY-APR )
                && pnd_Apr_Mar.getBoolean()) || (pnd_Settlmnt_Dte_Pnd_Settlmnt_Mm.equals("05") && pnd_May_Apr.getBoolean())))
            {
                if (condition(pnd_I.greaterOrEqual(40)))                                                                                                                  //Natural: IF #I GE 40
                {
                    pnd_Abend_Code.setValue(81);                                                                                                                          //Natural: ASSIGN #ABEND-CODE := 81
                    getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"PROGRAM TERMINATES WITH ABEND CODE:",pnd_Abend_Code);                                 //Natural: WRITE *PROGRAM 'PROGRAM TERMINATES WITH ABEND CODE:' #ABEND-CODE
                    if (Global.isEscape()) return;
                    getReports().write(0, ReportOption.NOTITLE,"Reason: Date ranges cover more than 20 years");                                                           //Natural: WRITE 'Reason: Date ranges cover more than 20 years'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(pnd_Abend_Code);  if (true) return;                                                                                                 //Natural: TERMINATE #ABEND-CODE
                }                                                                                                                                                         //Natural: END-IF
                //*    #I := #I + 1                     COMMENT OUT   -  ROXAN
                //*  TIAA/CREF REVALUATION
                //*  PLACE PERFORM STATEMENT INSIDE FOR LOOP
                if (condition(pnd_Ws_Work_Variables_Pnd_Ws_Reval.equals(1)))                                                                                              //Natural: IF #WS-REVAL = 1
                {
                    pnd_Ws_Work_Variables_Pnd_Level_20.nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct());                                                          //Natural: ASSIGN #LEVEL-20 := #LEVEL-20 + C*INV-ACCT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Work_Variables_Pnd_Ws_Reval.setValue(1);                                                                                                           //Natural: ASSIGN #WS-REVAL := 1
                FOR02:                                                                                                                                                    //Natural: FOR #X 1 FCP-CONS-PYMNT.C*INV-ACCT
                for (pnd_Ws_Work_Variables_Pnd_X.setValue(1); condition(pnd_Ws_Work_Variables_Pnd_X.lessOrEqual(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct())); 
                    pnd_Ws_Work_Variables_Pnd_X.nadd(1))
                {
                    pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_Ws_Work_Variables_Pnd_Level_20.add(pnd_Ws_Work_Variables_Pnd_X));                              //Natural: ASSIGN #I := #LEVEL-20 + #X
                    //*  #I - RECEIVING
                                                                                                                                                                          //Natural: PERFORM INIT-CRUNCHED-FIELDS
                    sub_Init_Crunched_Fields();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  #X - GIVING
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   MOVE PERFORM STATEMENT INSIDE FOR LOOP           /* ROXAN
        //*  #X - RECEIVING
        pnd_Ded_Run_Tot.reset();                                                                                                                                          //Natural: RESET #DED-RUN-TOT
        FOR03:                                                                                                                                                            //Natural: FOR #R 1 FCP-CONS-PYMNT.C*INV-ACCT
        for (pnd_Ws_Work_Variables_Pnd_R.setValue(1); condition(pnd_Ws_Work_Variables_Pnd_R.lessOrEqual(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct())); 
            pnd_Ws_Work_Variables_Pnd_R.nadd(1))
        {
            //*  #R - GIVING
            if (condition(pnd_Ws_Work_Variables_Pnd_Ws_Reval.equals(1)))                                                                                                  //Natural: IF #WS-REVAL = 1
            {
                pnd_Ws_Work_Variables_Pnd_X.compute(new ComputeParameters(false, pnd_Ws_Work_Variables_Pnd_X), pnd_Ws_Work_Variables_Pnd_Level_20.add(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ASSIGN #X := #LEVEL-20 + #R
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Work_Variables_Pnd_X.setValue(pnd_Ws_Work_Variables_Pnd_R);                                                                                        //Natural: ASSIGN #X := #R
            }                                                                                                                                                             //Natural: END-IF
            //*  SUM DATA INTO TABLE ENTRY (#I)
                                                                                                                                                                          //Natural: PERFORM CRUNCH-INV-ACCT-FIELDS
            sub_Crunch_Inv_Acct_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  EVALUATE-A-PYMNT-RECORD
    }
    private void sub_Wrap_A_Payment() throws Exception                                                                                                                    //Natural: WRAP-A-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  WRITE OUT THE PREVIOUS PAYMENT RECORDS FROM THE WORKING STORAGE TABLES
        //*  ----------------------------------------------------------------------
        //* *--> INSERTED 03-31-95 BY FRANK
        //* *--> THE FIELD #WS-KEY.#WS-PYMNT-CHECK-NBR IS USE TO HOLD THE CHECK
        //* *--> PAYMENT DATE (INVERSE) BECAUSE WE CAN HAVE MULTIPLE SEQUENCE
        //* *--> NUMBERED RECORDS WITH 'DIFFERENT' CHECK PAYMENT DATE.
        //* *--> THE INVERSE DATE AND SEQUENCE NUMBER MAKES IT UNIQUE.
        //* *-----------------------------------------------------------------*
        //*  IB 9/16/99
        //*    ROXAN   4/27/99
        ldaFcpl370.getPnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte().setValue(pnd_Cnr_Orgnl_Invrse_Dte);                                                                     //Natural: MOVE #CNR-ORGNL-INVRSE-DTE TO #WS-HEADER-RECORD.CNR-ORGNL-INVRSE-DTE
        pnd_Ws_Work_Variables_Pnd_Level_20.setValue(pnd_Ws_Work_Variables_Pnd_X);                                                                                         //Natural: ASSIGN #LEVEL-20 := #X
        if (condition(ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ") || ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))) //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' OR = 'R'
        {
            pnd_Ws_Check_Date.setValue(ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Invrse_Dte());                                                                           //Natural: ASSIGN #WS-CHECK-DATE := #WS-HEADER-RECORD.CNTRCT-INVRSE-DTE
            pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Nbr.setValue(pnd_Ws_Check_Date_Pnd_Ws_Inverse_7_Bytes);                                                                         //Natural: ASSIGN #WS-KEY.#WS-PYMNT-CHECK-NBR := #WS-INVERSE-7-BYTES
            pnd_C_Rcrd_10.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C-RCRD-10
            //*    RHW 9/29/06
            pnd_Internal_Seq_Number.nadd(1);                                                                                                                              //Natural: ADD 1 TO #INTERNAL-SEQ-NUMBER
            pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(10);                                                                                                             //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 10
            pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(1);                                                                                                              //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = 1
            //*  MA 08/25/00
            //*   RHW    9/29/06
            getWorkFiles().write(8, false, pnd_Ws_Key, ldaFcpl370.getPnd_Ws_Header_Record(), ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Type_Cde(), pnd_Internal_Seq_Number); //Natural: WRITE WORK FILE 8 #WS-KEY #WS-HEADER-RECORD #WS-HEADER-RECORD.CNTRCT-TYPE-CDE #INTERNAL-SEQ-NUMBER
                                                                                                                                                                          //Natural: PERFORM SEPARATE-N-WRITE-OCCURS-RECORD
            sub_Separate_N_Write_Occurs_Record();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
            sub_Separate_N_Write_Name_N_Addr_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  LEON 08-04-99
            //*  JWO 2010-11
            if (condition(ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S")  //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'S' OR = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR'
                || ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
                || ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
                || ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR")))
            {
                pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(10);                                                                                                         //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 10
                pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(1);                                                                                                          //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = 1
                getWorkFiles().write(2, false, pnd_Ws_Key, ldaFcpl370.getPnd_Ws_Header_Record());                                                                         //Natural: WRITE WORK FILE 2 #WS-KEY #WS-HEADER-RECORD
                                                                                                                                                                          //Natural: PERFORM SEPARATE-N-WRITE-OCCURS-RECORD-2
                sub_Separate_N_Write_Occurs_Record_2();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM SEPARATE-N-WRITE-NAME-N-ADDR-2
                sub_Separate_N_Write_Name_N_Addr_2();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRAP-A-PAYMENT
    }
    private void sub_Print_On_Which_Side_Of_Paper() throws Exception                                                                                                      //Natural: PRINT-ON-WHICH-SIDE-OF-PAPER
    {
        if (BLNatReinput.isReinput()) return;

        //*  ====================================================================
        pnd_Ws_Lines_In_Grp.reset();                                                                                                                                      //Natural: RESET #WS-LINES-IN-GRP
        pnd_At_Top_Of_Body.setValue(true);                                                                                                                                //Natural: ASSIGN #AT-TOP-OF-BODY := TRUE
        pnd_Ws_Current_X_Plex.setValue(1);                                                                                                                                //Natural: ASSIGN #WS-CURRENT-X-PLEX := 1
        pnd_Lines_Remained_On_Page.setValue(pnd_Ws_T1_Lines_On_Page.getValue(1));                                                                                         //Natural: ASSIGN #LINES-REMAINED-ON-PAGE := #WS-T1-LINES-ON-PAGE ( 1 )
        FOR04:                                                                                                                                                            //Natural: FOR #K 1 TO #I
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(pnd_I)); pnd_K.nadd(1))
        {
            FOR05:                                                                                                                                                        //Natural: FOR #J 1 TO 10
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(10)); pnd_J.nadd(1))
            {
                if (condition(ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_K,pnd_J).notEquals(getZero())))                                                    //Natural: IF #WS-OCCURS.PYMNT-DED-CDE ( #K,#J ) NE 0
                {
                    pnd_Ws_Lines_In_Grp.nadd(1);                                                                                                                          //Natural: ADD 1 TO #WS-LINES-IN-GRP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_K).notEquals(getZero())))                                                      //Natural: IF #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #K ) NE 0
            {
                pnd_Ws_Lines_In_Grp.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-LINES-IN-GRP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_K).notEquals(getZero())))                                                     //Natural: IF #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #K ) NE 0
            {
                pnd_Ws_Lines_In_Grp.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-LINES-IN-GRP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_K).notEquals(getZero())))                                                     //Natural: IF #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #K ) NE 0
            {
                pnd_Ws_Lines_In_Grp.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-LINES-IN-GRP
            }                                                                                                                                                             //Natural: END-IF
            //*   IF FIRST "group" IN THE BODY AREA (SIMPLEX, DUPLEX, OR ANY)
            if (condition(pnd_At_Top_Of_Body.getBoolean()))                                                                                                               //Natural: IF #AT-TOP-OF-BODY
            {
                pnd_At_Top_Of_Body.setValue(false);                                                                                                                       //Natural: ASSIGN #AT-TOP-OF-BODY := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Lines_In_Grp.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-LINES-IN-GRP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Lines_In_Grp.less(3)))                                                                                                                   //Natural: IF #WS-LINES-IN-GRP LT 3
            {
                pnd_Ws_Lines_In_Grp.setValue(3);                                                                                                                          //Natural: ASSIGN #WS-LINES-IN-GRP := 3
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Lines_Remained_On_Page.less(pnd_Ws_Lines_In_Grp)))                                                                                          //Natural: IF #LINES-REMAINED-ON-PAGE LT #WS-LINES-IN-GRP
            {
                //*  ...........  TURN TO THE NEXT PAGE
                pnd_Ws_Current_X_Plex.nadd(1);                                                                                                                            //Natural: ADD 1 TO #WS-CURRENT-X-PLEX
                pnd_At_Top_Of_Body.setValue(true);                                                                                                                        //Natural: ASSIGN #AT-TOP-OF-BODY := TRUE
                //*   ROXAN
                if (condition(ldaFcpl370.getPnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind().equals(1)))                                                                     //Natural: IF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND = 1
                {
                    pnd_Ws_T1_Lines_On_Page.getValue(2).setValue(28);                                                                                                     //Natural: ASSIGN #WS-T1-LINES-ON-PAGE ( 2 ) := 28
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_T1_Lines_On_Page.getValue(2).setValue(39);                                                                                                     //Natural: ASSIGN #WS-T1-LINES-ON-PAGE ( 2 ) := 39
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Current_X_Plex.greater(4)))                                                                                                          //Natural: IF #WS-CURRENT-X-PLEX > 4
                {
                    pnd_Lines_Remained_On_Page.setValue(pnd_Ws_T1_Lines_On_Page.getValue(4));                                                                             //Natural: ASSIGN #LINES-REMAINED-ON-PAGE := #WS-T1-LINES-ON-PAGE ( 4 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Lines_Remained_On_Page.setValue(pnd_Ws_T1_Lines_On_Page.getValue(pnd_Ws_Current_X_Plex));                                                         //Natural: ASSIGN #LINES-REMAINED-ON-PAGE := #WS-T1-LINES-ON-PAGE ( #WS-CURRENT-X-PLEX )
                }                                                                                                                                                         //Natural: END-IF
                //*  NO NEED FOR SPACE FROM PREV GRP
                pnd_Ws_Lines_In_Grp.nsubtract(1);                                                                                                                         //Natural: SUBTRACT 1 FROM #WS-LINES-IN-GRP
                if (condition(pnd_Lines_Remained_On_Page.less(pnd_Ws_Lines_In_Grp)))                                                                                      //Natural: IF #LINES-REMAINED-ON-PAGE LT #WS-LINES-IN-GRP
                {
                    pnd_Abend_Code.setValue(80);                                                                                                                          //Natural: ASSIGN #ABEND-CODE := 80
                    getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"PROGRAM TERMINATES WITH ABEND CODE:",pnd_Abend_Code);                                 //Natural: WRITE *PROGRAM 'PROGRAM TERMINATES WITH ABEND CODE:' #ABEND-CODE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"Reason: Printing group does not fit on a single page");                                                   //Natural: WRITE 'Reason: Printing group does not fit on a single page'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(pnd_Abend_Code);  if (true) return;                                                                                                 //Natural: TERMINATE #ABEND-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl370.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_K).setValue(pnd_Ws_Current_X_Plex);                                                                    //Natural: ASSIGN #WS-SIDE ( #K ) := #WS-CURRENT-X-PLEX
            pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex.setValue(pnd_Ws_Current_X_Plex);                                                                                   //Natural: ASSIGN #WS-SIMPLEX-DUPLEX-MULTIPLEX := #WS-CURRENT-X-PLEX
            pnd_Lines_Remained_On_Page.nsubtract(pnd_Ws_Lines_In_Grp);                                                                                                    //Natural: SUBTRACT #WS-LINES-IN-GRP FROM #LINES-REMAINED-ON-PAGE
            pnd_Used_Lines.nadd(pnd_Ws_Lines_In_Grp);                                                                                                                     //Natural: ASSIGN #USED-LINES := #USED-LINES + #WS-LINES-IN-GRP
            pnd_Ws_Lines_In_Grp.reset();                                                                                                                                  //Natural: RESET #WS-LINES-IN-GRP
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Last_Side.setValue(ldaFcpl370.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_I));                                                                                //Natural: ASSIGN #LAST-SIDE := #WS-SIDE ( #I )
        if (condition(pnd_Used_Lines.greaterOrEqual(pnd_Ws_T1_Lines_On_Page.getValue(pnd_Last_Side))))                                                                    //Natural: IF #USED-LINES GE #WS-T1-LINES-ON-PAGE ( #LAST-SIDE )
        {
            ldaFcpl370.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_I).nadd(1);                                                                                            //Natural: ASSIGN #WS-SIDE ( #I ) := #WS-SIDE ( #I ) + 1
            pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex.setValue(ldaFcpl370.getPnd_Ws_Occurs_Pnd_Ws_Side().getValue(pnd_I));                                               //Natural: ASSIGN #WS-SIMPLEX-DUPLEX-MULTIPLEX := #WS-SIDE ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I.nadd(1);                                                                                                                                                    //Natural: ASSIGN #I := #I + 1
        pnd_Used_Lines.reset();                                                                                                                                           //Natural: RESET #USED-LINES
        //*  PRINT-ON-WHICH-SIDE-OF-PAPER
    }
    //*  RESET DATA INT TABLE
    private void sub_Init_Crunched_Fields() throws Exception                                                                                                              //Natural: INIT-CRUNCHED-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl370.getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Settlmnt_Dte());                    //Natural: ASSIGN #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-START ( #I ) := FCP-CONS-PYMNT.PYMNT-SETTLMNT-DTE
        ldaFcpl370.getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Settlmnt_Dte());                          //Natural: ASSIGN #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE ( #I ) := FCP-CONS-PYMNT.PYMNT-SETTLMNT-DTE
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_Ws_Work_Variables_Pnd_X));          //Natural: ASSIGN #WS-OCCURS.INV-ACCT-CDE ( #I ) := FCP-CONS-PYMNT.INV-ACCT-CDE ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Cde().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_Ws_Work_Variables_Pnd_X));          //Natural: ASSIGN #WS-OCCURS.INV-ACCT-CDE ( #I ) := FCP-CONS-PYMNT.INV-ACCT-CDE ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Fed_Cde().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Fed_Cde().getValue(pnd_Ws_Work_Variables_Pnd_X));  //Natural: ASSIGN #WS-OCCURS.INV-ACCT-FED-CDE ( #I ) := FCP-CONS-PYMNT.INV-ACCT-FED-CDE ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_State_Cde().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_State_Cde().getValue(pnd_Ws_Work_Variables_Pnd_X)); //Natural: ASSIGN #WS-OCCURS.INV-ACCT-STATE-CDE ( #I ) := FCP-CONS-PYMNT.INV-ACCT-STATE-CDE ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Local_Cde().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Local_Cde().getValue(pnd_Ws_Work_Variables_Pnd_X)); //Natural: ASSIGN #WS-OCCURS.INV-ACCT-LOCAL-CDE ( #I ) := FCP-CONS-PYMNT.INV-ACCT-LOCAL-CDE ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Ivc_Ind().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Ivc_Ind().getValue(pnd_Ws_Work_Variables_Pnd_X));  //Natural: ASSIGN #WS-OCCURS.INV-ACCT-IVC-IND ( #I ) := FCP-CONS-PYMNT.INV-ACCT-IVC-IND ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Valuat_Period().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Work_Variables_Pnd_X)); //Natural: ASSIGN #WS-OCCURS.INV-ACCT-VALUAT-PERIOD ( #I ) := FCP-CONS-PYMNT.INV-ACCT-VALUAT-PERIOD ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Cntrct_Ppcn_Nbr().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                          //Natural: ASSIGN #WS-OCCURS.CNTRCT-PPCN-NBR ( #I ) := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        ldaFcpl370.getPnd_Ws_Occurs_Cntrct_Payee_Cde().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde());                                        //Natural: ASSIGN #WS-OCCURS.CNTRCT-PAYEE-CDE ( #I ) := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
        ldaFcpl370.getPnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Pymnt_Type_Ind());                              //Natural: ASSIGN #WS-OCCURS.CNTRCT-PYMNT-TYPE-IND ( #I ) := FCP-CONS-PYMNT.CNTRCT-PYMNT-TYPE-IND
        ldaFcpl370.getPnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Sttlmnt_Type_Ind());                          //Natural: ASSIGN #WS-OCCURS.CNTRCT-STTLMNT-TYPE-IND ( #I ) := FCP-CONS-PYMNT.CNTRCT-STTLMNT-TYPE-IND
        ldaFcpl370.getPnd_Ws_Occurs_Cntrct_Option_Cde().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Option_Cde());                                      //Natural: ASSIGN #WS-OCCURS.CNTRCT-OPTION-CDE ( #I ) := FCP-CONS-PYMNT.CNTRCT-OPTION-CDE
        ldaFcpl370.getPnd_Ws_Occurs_Cntrct_Mode_Cde().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Mode_Cde());                                          //Natural: ASSIGN #WS-OCCURS.CNTRCT-MODE-CDE ( #I ) := FCP-CONS-PYMNT.CNTRCT-MODE-CDE
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Unit_Value().getValue(pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Unit_Value().getValue(pnd_Ws_Work_Variables_Pnd_X)); //Natural: ASSIGN #WS-OCCURS.INV-ACCT-UNIT-VALUE ( #I ) := FCP-CONS-PYMNT.INV-ACCT-UNIT-VALUE ( #X )
        //*  INIT-CRUNCHED-FIELDS /* RESET DATA INT TABLE
    }
    //*  SUM DATA INTO ENTRY (#I)
    private void sub_Crunch_Inv_Acct_Fields() throws Exception                                                                                                            //Natural: CRUNCH-INV-ACCT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl370.getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End().getValue(pnd_Ws_Work_Variables_Pnd_X).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Settlmnt_Dte()); //Natural: ASSIGN #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-END ( #X ) := FCP-CONS-PYMNT.PYMNT-SETTLMNT-DTE
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( #R ) TO #WS-OCCURS.INV-ACCT-SETTL-AMT ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-DVDND-AMT ( #R ) TO #WS-OCCURS.INV-ACCT-DVDND-AMT ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-IVC-AMT ( #R ) TO #WS-OCCURS.INV-ACCT-IVC-AMT ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-DCI-AMT ( #R ) TO #WS-OCCURS.INV-ACCT-DCI-AMT ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-DPI-AMT ( #R ) TO #WS-OCCURS.INV-ACCT-DPI-AMT ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-NET-PYMNT-AMT ( #R ) TO #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Adj_Ivc_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-ADJ-IVC-AMT ( #R ) TO #WS-OCCURS.INV-ACCT-ADJ-IVC-AMT ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-FDRL-TAX-AMT ( #R ) TO #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-STATE-TAX-AMT ( #R ) TO #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-LOCAL-TAX-AMT ( #R ) TO #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Exp_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Exp_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-EXP-AMT ( #R ) TO #WS-OCCURS.INV-ACCT-EXP-AMT ( #X )
        //*  ALL DDCTNS ON IN REC
        //*  ALL DDCTNS ON SUMRZD REC
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Unit_Qty().getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Unit_Qty().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-UNIT-QTY ( #R ) TO #WS-OCCURS.INV-ACCT-UNIT-QTY ( #X )
        FOR_ALL_DDCTNS:                                                                                                                                                   //Natural: FOR #I1 1 TO C*PYMNT-DED-GRP
        for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp())); pnd_I1.nadd(1))
        {
            FOR_SUM_TBL:                                                                                                                                                  //Natural: FOR #I2 FROM 1 TO 10
            for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(10)); pnd_I2.nadd(1))
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Cde().getValue(pnd_I1).equals(5)))                                                                  //Natural: IF FCP-CONS-PYMNT.PYMNT-DED-CDE ( #I1 ) = 5
                {
                    if (condition(pnd_Ded_Run_Tot.equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Amt().getValue(pnd_I1))))                                                //Natural: IF #DED-RUN-TOT = FCP-CONS-PYMNT.PYMNT-DED-AMT ( #I1 )
                    {
                        if (true) break FOR_ALL_DDCTNS;                                                                                                                   //Natural: ESCAPE BOTTOM ( FOR-ALL-DDCTNS. )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  MATCH ON DEDUCTION
                    short decideConditionsMet1301 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #WS-OCCURS.PYMNT-DED-CDE ( #X,#I1 ) = FCP-CONS-PYMNT.PYMNT-DED-CDE ( #I1 )
                    if (condition(ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_Ws_Work_Variables_Pnd_X,pnd_I1).equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Cde().getValue(pnd_I1))))
                    {
                        decideConditionsMet1301++;
                        //*                                                  /*   ROXAN
                        //*  EMPTY AVAILABLE ENTRY FOR A NEW DEDUCTION TYPE
                        //*  EMPTY ENTRY?
                        //*  ADD NEW CODE TO TABLE
                                                                                                                                                                          //Natural: PERFORM COMPUTE-FOR-FUND-DEDUCTION
                        sub_Compute_For_Fund_Deduction();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FOR_SUM_TBL"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FOR_SUM_TBL"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: WHEN #WS-OCCURS.PYMNT-DED-CDE ( #X,#I1 ) = 0
                    else if (condition(ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_Ws_Work_Variables_Pnd_X,pnd_I1).equals(getZero())))
                    {
                        decideConditionsMet1301++;
                        ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_Ws_Work_Variables_Pnd_X,pnd_I1).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Cde().getValue(pnd_I1)); //Natural: ASSIGN #WS-OCCURS.PYMNT-DED-CDE ( #X,#I1 ) := FCP-CONS-PYMNT.PYMNT-DED-CDE ( #I1 )
                        //*                                                    /*   ROXAN
                        //*  SET THE TABLE COUNTER
                        //* DONE WITH THIS DEDCUTION
                        //*  KEEP SEARCHING THE TABLE
                                                                                                                                                                          //Natural: PERFORM COMPUTE-FOR-FUND-DEDUCTION
                        sub_Compute_For_Fund_Deduction();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FOR_SUM_TBL"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FOR_SUM_TBL"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        ldaFcpl370.getPnd_Ws_Occurs_Pnd_Cntr_Deductions().getValue(pnd_Ws_Work_Variables_Pnd_X).setValue(pnd_I1);                                         //Natural: ASSIGN #WS-OCCURS.#CNTR-DEDUCTIONS ( #X ) := #I1
                    }                                                                                                                                                     //Natural: WHEN ANY
                    if (condition(decideConditionsMet1301 > 0))
                    {
                        if (true) break FOR_SUM_TBL;                                                                                                                      //Natural: ESCAPE BOTTOM ( FOR-SUM-TBL. )
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                //*  (FOR-SUM-TBL.)
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FOR_ALL_DDCTNS"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FOR_ALL_DDCTNS"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  THE TABLE IS FULL AND THE DDCTN NOT FOUND IN TABLE
            if (condition(pnd_I2.greater(10)))                                                                                                                            //Natural: IF #I2 GT 10
            {
                pnd_Abend_Code.setValue(79);                                                                                                                              //Natural: ASSIGN #ABEND-CODE := 79
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"PROGRAM TERMINATES WITH ABEND CODE:",pnd_Abend_Code);                                     //Natural: WRITE *PROGRAM 'PROGRAM TERMINATES WITH ABEND CODE:' #ABEND-CODE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FOR_ALL_DDCTNS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FOR_ALL_DDCTNS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"Reason: Too many DEDUCTIONS to fit in summary record",NEWLINE,"Deduction Table:",NEWLINE,"=",                 //Natural: WRITE 'Reason: Too many DEDUCTIONS to fit in summary record' / 'Deduction Table:' / '=' #WS-OCCURS.PYMNT-DED-CDE ( #X,* ) / '=' #WS-OCCURS.PYMNT-DED-AMT ( #X,* ) // 'Deductions on Incoming record:' / '=' FCP-CONS-PYMNT.C*PYMNT-DED-GRP / '=' FCP-CONS-PYMNT.PYMNT-DED-CDE ( * ) / '=' FCP-CONS-PYMNT.PYMNT-DED-AMT ( * ) / '=' FCP-CONS-PYMNT.PYMNT-DED-PAYEE-CDE ( * ) / 'Deduction Indexes at time of abend:' '=' #I1 '=' #I2
                    ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_Ws_Work_Variables_Pnd_X,"*"),NEWLINE,"=",ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X,
                    "*"),NEWLINE,NEWLINE,"Deductions on Incoming record:",NEWLINE,"=",ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp(),NEWLINE,"=",
                    ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Cde().getValue("*"),NEWLINE,"=",ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Amt().getValue("*"),
                    NEWLINE,"=",ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Payee_Cde().getValue("*"),NEWLINE,"Deduction Indexes at time of abend:","=",pnd_I1,
                    "=",pnd_I2);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FOR_ALL_DDCTNS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FOR_ALL_DDCTNS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(pnd_Abend_Code);  if (true) return;                                                                                                     //Natural: TERMINATE #ABEND-CODE
            }                                                                                                                                                             //Natural: END-IF
            //*  (FOR-ALL-DDCTNS.)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  END OF #I TO #X FOR ALL FIELDS  -   ROXAN
        //*  SAVE THE PAYMENT INFORMATION TO BE USED AS LAST COMMUTED VALUE
        //*  CHANGED 1 TO #X FOR ALL FIELDS  -   ROXAN
        //*  LCV REFERENCE FIELDS CHANGED FROM SINGLE TO MULTIPLE OCCURRENCES
        pnd_Lcv.getValue("*").reset();                                                                                                                                    //Natural: RESET #LCV ( * )
        pnd_Lcv_Pnd_Lcv_Single_Info.getValue("*").setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                    //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #LCV.#LCV-SINGLE-INFO ( * )
        pnd_Lcv_Inv_Acct_Cde.getValue(pnd_Ws_Work_Variables_Pnd_X).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_Ws_Work_Variables_Pnd_R));          //Natural: ASSIGN #LCV.INV-ACCT-CDE ( #X ) := FCP-CONS-PYMNT.INV-ACCT-CDE ( #R )
        pnd_Lcv_Inv_Acct_Fed_Cde.getValue(pnd_Ws_Work_Variables_Pnd_X).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Fed_Cde().getValue(pnd_Ws_Work_Variables_Pnd_R));  //Natural: ASSIGN #LCV.INV-ACCT-FED-CDE ( #X ) := FCP-CONS-PYMNT.INV-ACCT-FED-CDE ( #R )
        pnd_Lcv_Inv_Acct_State_Cde.getValue(pnd_Ws_Work_Variables_Pnd_X).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_State_Cde().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ASSIGN #LCV.INV-ACCT-STATE-CDE ( #X ) := FCP-CONS-PYMNT.INV-ACCT-STATE-CDE ( #R )
        pnd_Lcv_Inv_Acct_Local_Cde.getValue(pnd_Ws_Work_Variables_Pnd_X).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Local_Cde().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ASSIGN #LCV.INV-ACCT-LOCAL-CDE ( #X ) := FCP-CONS-PYMNT.INV-ACCT-LOCAL-CDE ( #R )
        pnd_Lcv_Inv_Acct_Ivc_Ind.getValue(pnd_Ws_Work_Variables_Pnd_X).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Ivc_Ind().getValue(pnd_Ws_Work_Variables_Pnd_R));  //Natural: ASSIGN #LCV.INV-ACCT-IVC-IND ( #X ) := FCP-CONS-PYMNT.INV-ACCT-IVC-IND ( #R )
        pnd_Lcv_Inv_Acct_Valuat_Period.getValue(pnd_Ws_Work_Variables_Pnd_X).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ASSIGN #LCV.INV-ACCT-VALUAT-PERIOD ( #X ) := FCP-CONS-PYMNT.INV-ACCT-VALUAT-PERIOD ( #R )
        pnd_Lcv_Pymnt_Settlmnt_Dte.getValue(1).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Settlmnt_Dte());                                                              //Natural: ASSIGN #LCV.PYMNT-SETTLMNT-DTE ( 1 ) := FCP-CONS-PYMNT.PYMNT-SETTLMNT-DTE
        pnd_Lcv_Inv_Acct_Unit_Value.getValue(pnd_Ws_Work_Variables_Pnd_X).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Unit_Value().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ASSIGN #LCV.INV-ACCT-UNIT-VALUE ( #X ) := FCP-CONS-PYMNT.INV-ACCT-UNIT-VALUE ( #R )
        pnd_Lcv_Inv_Acct_Unit_Qty.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Unit_Qty().getValue(pnd_Ws_Work_Variables_Pnd_R));    //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-UNIT-QTY ( #R ) TO #LCV.INV-ACCT-UNIT-QTY ( #X )
        pnd_Lcv_Inv_Acct_Settl_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R));  //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( #R ) TO #LCV.INV-ACCT-SETTL-AMT ( #X )
        pnd_Lcv_Inv_Acct_Dvdnd_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R));  //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-DVDND-AMT ( #R ) TO #LCV.INV-ACCT-DVDND-AMT ( #X )
        pnd_Lcv_Inv_Acct_Ivc_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R));      //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-IVC-AMT ( #R ) TO #LCV.INV-ACCT-IVC-AMT ( #X )
        pnd_Lcv_Inv_Acct_Dci_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R));      //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-DCI-AMT ( #R ) TO #LCV.INV-ACCT-DCI-AMT ( #X )
        pnd_Lcv_Inv_Acct_Dpi_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R));      //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-DPI-AMT ( #R ) TO #LCV.INV-ACCT-DPI-AMT ( #X )
        pnd_Lcv_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-NET-PYMNT-AMT ( #R ) TO #LCV.INV-ACCT-NET-PYMNT-AMT ( #X )
        pnd_Lcv_Inv_Acct_Adj_Ivc_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Adj_Ivc_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-ADJ-IVC-AMT ( #R ) TO #LCV.INV-ACCT-ADJ-IVC-AMT ( #X )
        pnd_Lcv_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-FDRL-TAX-AMT ( #R ) TO #LCV.INV-ACCT-FDRL-TAX-AMT ( #X )
        pnd_Lcv_Inv_Acct_State_Tax_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-STATE-TAX-AMT ( #R ) TO #LCV.INV-ACCT-STATE-TAX-AMT ( #X )
        pnd_Lcv_Inv_Acct_Local_Tax_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R)); //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-LOCAL-TAX-AMT ( #R ) TO #LCV.INV-ACCT-LOCAL-TAX-AMT ( #X )
        pnd_Lcv_Inv_Acct_Exp_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Exp_Amt().getValue(pnd_Ws_Work_Variables_Pnd_R));      //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-EXP-AMT ( #R ) TO #LCV.INV-ACCT-EXP-AMT ( #X )
        pnd_Lcv_Pymnt_Ded_Cde.getValue(pnd_Ws_Work_Variables_Pnd_X,"*").setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Cde().getValue("*"));                            //Natural: ASSIGN #LCV.PYMNT-DED-CDE ( #X,* ) := FCP-CONS-PYMNT.PYMNT-DED-CDE ( * )
        pnd_Lcv_Pymnt_Ded_Payee_Cde.getValue(pnd_Ws_Work_Variables_Pnd_X,"*").setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Payee_Cde().getValue("*"));                //Natural: ASSIGN #LCV.PYMNT-DED-PAYEE-CDE ( #X,* ) := FCP-CONS-PYMNT.PYMNT-DED-PAYEE-CDE ( * )
        pnd_Lcv_Pymnt_Ded_Amt.getValue(pnd_Ws_Work_Variables_Pnd_X,"*").setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Amt().getValue("*"));                            //Natural: ASSIGN #LCV.PYMNT-DED-AMT ( #X,* ) := FCP-CONS-PYMNT.PYMNT-DED-AMT ( * )
        //*  CRUNCH-INV-ACCT-FIELDS
    }
    private void sub_Separate_N_Write_Occurs_Record() throws Exception                                                                                                    //Natural: SEPARATE-N-WRITE-OCCURS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(20);                                                                                                                 //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 20
        ldaFcpl370.getPnd_Ws_Occurs_Pnd_Cntr_Inv_Acct().setValue(pnd_Ws_Work_Variables_Pnd_Level_20);                                                                     //Natural: ASSIGN #CNTR-INV-ACCT = #LEVEL-20
        pnd_C_Rcrd_20.nadd(pnd_Ws_Work_Variables_Pnd_Level_20);                                                                                                           //Natural: ADD #LEVEL-20 TO #C-RCRD-20
        FOR06:                                                                                                                                                            //Natural: FOR #C = 1 TO #LEVEL-20
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_Ws_Work_Variables_Pnd_Level_20)); pnd_C.nadd(1))
        {
            pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_C);                                                                                                          //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR := #C
            //*  MA 08/25/00
            //*   RHW    9/29/06
            getWorkFiles().write(8, false, pnd_Ws_Key, ldaFcpl370.getPnd_Ws_Occurs_Pnd_Cntr_Inv_Acct(), ldaFcpl370.getPnd_Ws_Occurs_Pnd_Ws_Occurs_Entry().getValue(pnd_C),  //Natural: WRITE WORK FILE 8 #WS-KEY #CNTR-INV-ACCT #WS-OCCURS.#WS-OCCURS-ENTRY ( #C ) #WS-INV-ACCT-FILLER #WS-HEADER-RECORD.CNTRCT-TYPE-CDE #INTERNAL-SEQ-NUMBER
                pnd_Ws_Inv_Acct_Filler, ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Type_Cde(), pnd_Internal_Seq_Number);
            pnd_Total_Sett.nadd(ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_C));                                                                        //Natural: ADD #WS-OCCURS.INV-ACCT-SETTL-AMT ( #C ) TO #TOTAL-SETT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Separate_N_Write_Occurs_Record_2() throws Exception                                                                                                  //Natural: SEPARATE-N-WRITE-OCCURS-RECORD-2
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(20);                                                                                                                 //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 20
        ldaFcpl370.getPnd_Ws_Occurs_Pnd_Cntr_Inv_Acct().setValue(pnd_Ws_Work_Variables_Pnd_Level_20);                                                                     //Natural: ASSIGN #CNTR-INV-ACCT = #LEVEL-20
        pnd_C_Rcrd_20.nadd(pnd_Ws_Work_Variables_Pnd_Level_20);                                                                                                           //Natural: ADD #LEVEL-20 TO #C-RCRD-20
        FOR07:                                                                                                                                                            //Natural: FOR #C = 1 TO #LEVEL-20
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_Ws_Work_Variables_Pnd_Level_20)); pnd_C.nadd(1))
        {
            pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_C);                                                                                                          //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR := #C
            getWorkFiles().write(2, false, pnd_Ws_Key, ldaFcpl370.getPnd_Ws_Occurs_Pnd_Cntr_Inv_Acct(), ldaFcpl370.getPnd_Ws_Occurs_Pnd_Ws_Occurs_Entry().getValue(pnd_C),  //Natural: WRITE WORK FILE 2 #WS-KEY #CNTR-INV-ACCT #WS-OCCURS.#WS-OCCURS-ENTRY ( #C ) #WS-INV-ACCT-FILLER
                pnd_Ws_Inv_Acct_Filler);
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Separate_N_Write_Name_N_Addr_Record() throws Exception                                                                                               //Natural: SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
                                                                                                                                                                          //Natural: PERFORM GEN-MAILING-NAME
        sub_Gen_Mailing_Name();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp().greater(2)))                                                                        //Natural: IF FCP-CONS-ADDR.C*PYMNT-NME-AND-ADDR-GRP > 2
        {
            ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp().setValue(2);                                                                                  //Natural: ASSIGN FCP-CONS-ADDR.C*PYMNT-NME-AND-ADDR-GRP := 2
        }                                                                                                                                                                 //Natural: END-IF
        FOR08:                                                                                                                                                            //Natural: FOR #D = 1 TO FCP-CONS-ADDR.C*PYMNT-NME-AND-ADDR-GRP
        for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp())); pnd_D.nadd(1))
        {
            if (condition(ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Nme().getValue(pnd_D).notEquals(" ")))                                                                       //Natural: IF FCP-CONS-ADDR.PYMNT-NME ( #D ) NE ' '
            {
                pnd_C_Rcrd_30.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #C-RCRD-30
                pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(30);                                                                                                         //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 30
                pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_D);                                                                                                      //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = #D
                //*  MA 08/25/00
                //*   RHW    9/29/06
                getWorkFiles().write(8, false, pnd_Ws_Key, ldaFcpladdr.getFcp_Cons_Addr_Rcrd_Typ(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Orgn_Cde(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Ppcn_Nbr(),  //Natural: WRITE WORK FILE 8 #WS-KEY FCP-CONS-ADDR.RCRD-TYP FCP-CONS-ADDR.CNTRCT-ORGN-CDE FCP-CONS-ADDR.CNTRCT-PPCN-NBR FCP-CONS-ADDR.CNTRCT-PAYEE-CDE FCP-CONS-ADDR.CNTRCT-INVRSE-DTE FCP-CONS-ADDR.PYMNT-PRCSS-SEQ-NBR #WS-NAME-N-ADDRESS.PH-NAME FCP-CONS-ADDR.PYMNT-NME ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE1-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE2-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE3-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE4-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE5-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE6-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-ZIP-CDE ( #D ) FCP-CONS-ADDR.PYMNT-POSTL-DATA ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-TYPE-IND ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LAST-CHG-DTE ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LAST-CHG-TME ( #D ) FCP-CONS-ADDR.PYMNT-EFT-TRANSIT-ID FCP-CONS-ADDR.PYMNT-EFT-ACCT-NBR FCP-CONS-ADDR.PYMNT-CHK-SAV-IND FCP-CONS-ADDR.PYMNT-DECEASED-NME FCP-CONS-ADDR.CNTRCT-HOLD-TME #WS-NAME-N-ADDRESS-FILLER #WS-HEADER-RECORD.CNTRCT-TYPE-CDE #INTERNAL-SEQ-NUMBER
                    ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Payee_Cde(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Invrse_Dte(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr(), 
                    ldaFcpl370.getPnd_Ws_Name_N_Address_Ph_Name(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Nme().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line1_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line2_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line3_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line4_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line5_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line6_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Zip_Cde().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Postl_Data().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Type_Ind().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Eft_Transit_Id(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Eft_Acct_Nbr(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Chk_Sav_Ind(), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Deceased_Nme(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Hold_Tme(), pnd_Ws_Name_N_Address_Filler, ldaFcpl370.getPnd_Ws_Header_Record_Cntrct_Type_Cde(), 
                    pnd_Internal_Seq_Number);
                //* *    FCP-CONS-PYMNT.CNTRCT-TYPE-CDE       /* MA 08/25/00
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Separate_N_Write_Name_N_Addr_2() throws Exception                                                                                                    //Natural: SEPARATE-N-WRITE-NAME-N-ADDR-2
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
                                                                                                                                                                          //Natural: PERFORM GEN-MAILING-NAME
        sub_Gen_Mailing_Name();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp().greater(2)))                                                                        //Natural: IF FCP-CONS-ADDR.C*PYMNT-NME-AND-ADDR-GRP > 2
        {
            ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp().setValue(2);                                                                                  //Natural: ASSIGN FCP-CONS-ADDR.C*PYMNT-NME-AND-ADDR-GRP := 2
        }                                                                                                                                                                 //Natural: END-IF
        FOR09:                                                                                                                                                            //Natural: FOR #D = 1 TO FCP-CONS-ADDR.C*PYMNT-NME-AND-ADDR-GRP
        for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp())); pnd_D.nadd(1))
        {
            if (condition(ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Nme().getValue(pnd_D).notEquals(" ")))                                                                       //Natural: IF FCP-CONS-ADDR.PYMNT-NME ( #D ) NE ' '
            {
                pnd_C_Rcrd_30.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #C-RCRD-30
                pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(30);                                                                                                         //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 30
                pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_D);                                                                                                      //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = #D
                getWorkFiles().write(2, false, pnd_Ws_Key, ldaFcpladdr.getFcp_Cons_Addr_Rcrd_Typ(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Orgn_Cde(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Ppcn_Nbr(),  //Natural: WRITE WORK FILE 2 #WS-KEY FCP-CONS-ADDR.RCRD-TYP FCP-CONS-ADDR.CNTRCT-ORGN-CDE FCP-CONS-ADDR.CNTRCT-PPCN-NBR FCP-CONS-ADDR.CNTRCT-PAYEE-CDE FCP-CONS-ADDR.CNTRCT-INVRSE-DTE FCP-CONS-ADDR.PYMNT-PRCSS-SEQ-NBR #WS-NAME-N-ADDRESS.PH-NAME FCP-CONS-ADDR.PYMNT-NME ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE1-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE2-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE3-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE4-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE5-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE6-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-ZIP-CDE ( #D ) FCP-CONS-ADDR.PYMNT-POSTL-DATA ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-TYPE-IND ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LAST-CHG-DTE ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LAST-CHG-TME ( #D ) FCP-CONS-ADDR.PYMNT-EFT-TRANSIT-ID FCP-CONS-ADDR.PYMNT-EFT-ACCT-NBR FCP-CONS-ADDR.PYMNT-CHK-SAV-IND FCP-CONS-ADDR.PYMNT-DECEASED-NME FCP-CONS-ADDR.CNTRCT-HOLD-TME #WS-NAME-N-ADDRESS-FILLER
                    ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Payee_Cde(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Invrse_Dte(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr(), 
                    ldaFcpl370.getPnd_Ws_Name_N_Address_Ph_Name(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Nme().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line1_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line2_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line3_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line4_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line5_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line6_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Zip_Cde().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Postl_Data().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Type_Ind().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Eft_Transit_Id(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Eft_Acct_Nbr(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Chk_Sav_Ind(), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Deceased_Nme(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Hold_Tme(), pnd_Ws_Name_N_Address_Filler);
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Gen_Mailing_Name() throws Exception                                                                                                                  //Natural: GEN-MAILING-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GENERATE A MAILING NAME SO THAT VARIOUS COMPONENET LIKE JR., II.,
        //*  PROF., DR., ETC DO NOT APPEAR IN A NON SENSEABLE SEQUENCE.
        //*  GENERATE THE MAILING NAME COMPONENTS OUT OF:
        //*  1. THE NAME COMPONENTS FROM THE RECORD
        //*  2. NAME MESSAGING ALGORITHM USED BY THE "name and address" TEAM
        //*     IN ORDER TO GENERATE THE "Mailing Name"
        //*  ----------------------------------------------------------------------
        pdaAdsa362.getPnd_In_Middle_Name().reset();                                                                                                                       //Natural: RESET #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX #OUT-PREFIX #OUT-RETURN-CODE #IN-DATA #OUT-DATA
        pdaAdsa362.getPnd_Out_Middle_Name().reset();
        pdaAdsa362.getPnd_Out_Suffix().reset();
        pdaAdsa362.getPnd_Out_Prefix().reset();
        pdaAdsa362.getPnd_Out_Return_Code().reset();
        pdaAdsa360.getPnd_In_Data().reset();
        pdaAdsa360.getPnd_Out_Data().reset();
        ldaFcpl370.getPnd_Ws_Name_N_Address_Ph_First_Name().setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_First_Name());                                                       //Natural: ASSIGN #WS-NAME-N-ADDRESS.PH-FIRST-NAME := FCP-CONS-ADDR.PH-FIRST-NAME
        ldaFcpl370.getPnd_Ws_Name_N_Address_Ph_Middle_Name().setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_Middle_Name());                                                     //Natural: ASSIGN #WS-NAME-N-ADDRESS.PH-MIDDLE-NAME := FCP-CONS-ADDR.PH-MIDDLE-NAME
        ldaFcpl370.getPnd_Ws_Name_N_Address_Ph_Last_Name().setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_Last_Name());                                                         //Natural: ASSIGN #WS-NAME-N-ADDRESS.PH-LAST-NAME := FCP-CONS-ADDR.PH-LAST-NAME
        pdaAdsa360.getPnd_In_Data_Pnd_First_Name().setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_First_Name());                                                                //Natural: ASSIGN #FIRST-NAME := FCP-CONS-ADDR.PH-FIRST-NAME
        pdaAdsa362.getPnd_In_Middle_Name().setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_Middle_Name());                                                                       //Natural: ASSIGN #IN-MIDDLE-NAME := FCP-CONS-ADDR.PH-MIDDLE-NAME
        pdaAdsa360.getPnd_In_Data_Pnd_Last_Name().setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_Last_Name());                                                                  //Natural: ASSIGN #LAST-NAME := FCP-CONS-ADDR.PH-LAST-NAME
        //* *CALLNAT 'PDQN362' #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX/*VIKRAM
        //*  VIKRAM
        DbsUtil.callnat(Adsn362.class , getCurrentProcessState(), pdaAdsa362.getPnd_In_Middle_Name(), pdaAdsa362.getPnd_Out_Middle_Name(), pdaAdsa362.getPnd_Out_Suffix(),  //Natural: CALLNAT 'ADSN362' #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX #OUT-PREFIX #OUT-RETURN-CODE
            pdaAdsa362.getPnd_Out_Prefix(), pdaAdsa362.getPnd_Out_Return_Code());
        if (condition(Global.isEscape())) return;
        pdaAdsa360.getPnd_In_Data_Pnd_Prefix().setValue(pdaAdsa362.getPnd_Out_Prefix());                                                                                  //Natural: ASSIGN #PREFIX := #OUT-PREFIX
        pdaAdsa360.getPnd_In_Data_Pnd_Middle_Name().setValue(pdaAdsa362.getPnd_Out_Middle_Name());                                                                        //Natural: ASSIGN #MIDDLE-NAME := #OUT-MIDDLE-NAME
        pdaAdsa360.getPnd_In_Data_Pnd_Suffix().setValue(pdaAdsa362.getPnd_Out_Suffix());                                                                                  //Natural: ASSIGN #SUFFIX := #OUT-SUFFIX
        pdaAdsa360.getPnd_In_Data_Pnd_Length().setValue(38);                                                                                                              //Natural: ASSIGN #LENGTH := 38
        pdaAdsa360.getPnd_In_Data_Pnd_Format().setValue("1");                                                                                                             //Natural: ASSIGN #FORMAT := '1'
        //* *CALLNAT 'PDQN360' #IN-DATA #OUT-DATA    /* VIKRAM
        //*  VIKRAM
        DbsUtil.callnat(Adsn360.class , getCurrentProcessState(), pdaAdsa360.getPnd_In_Data(), pdaAdsa360.getPnd_Out_Data());                                             //Natural: CALLNAT 'ADSN360' #IN-DATA #OUT-DATA
        if (condition(Global.isEscape())) return;
        ldaFcpl370.getPnd_Ws_Name_N_Address_Ph_First_Name().setValue(DbsUtil.compress(pdaAdsa362.getPnd_Out_Prefix(), ldaFcpl370.getPnd_Ws_Name_N_Address_Ph_First_Name())); //Natural: COMPRESS #OUT-PREFIX #WS-NAME-N-ADDRESS.PH-FIRST-NAME INTO #WS-NAME-N-ADDRESS.PH-FIRST-NAME
        ldaFcpl370.getPnd_Ws_Name_N_Address_Ph_Middle_Name().setValue(pdaAdsa362.getPnd_Out_Middle_Name());                                                               //Natural: ASSIGN #WS-NAME-N-ADDRESS.PH-MIDDLE-NAME = #OUT-MIDDLE-NAME
        ldaFcpl370.getPnd_Ws_Name_N_Address_Ph_Last_Name().setValue(DbsUtil.compress(ldaFcpl370.getPnd_Ws_Name_N_Address_Ph_Last_Name(), pdaAdsa362.getPnd_Out_Suffix())); //Natural: COMPRESS #WS-NAME-N-ADDRESS.PH-LAST-NAME #OUT-SUFFIX INTO #WS-NAME-N-ADDRESS.PH-LAST-NAME
        //*  GEN-MAILING-NAME
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  INITIALIZE FIELDS FOR PROCESSING OF A PAYMENT
        //*  ----------------------------------------------------------------------
        ldaFcpl370.getPnd_Ws_Occurs().reset();                                                                                                                            //Natural: RESET #WS-OCCURS
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                        //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
        {
            ldaFcpl370.getPnd_Ws_Header_Record().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                     //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #WS-HEADER-RECORD
            //*  IB 9/16/99
            pnd_Cnr_Orgnl_Invrse_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cnr_Orgnl_Invrse_Dte());                                                                      //Natural: MOVE FCP-CONS-PYMNT.CNR-ORGNL-INVRSE-DTE TO #CNR-ORGNL-INVRSE-DTE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Hold_Cde().equals(" ")))                                                                                       //Natural: IF FCP-CONS-PYMNT.CNTRCT-HOLD-CDE EQ ' '
        {
            pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code.setValue("0000");                                                                                                   //Natural: ASSIGN #WS-SAVE-CONTRACT-HOLD-CODE := '0000'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Hold_Cde());                                                          //Natural: ASSIGN #WS-SAVE-CONTRACT-HOLD-CODE := FCP-CONS-PYMNT.CNTRCT-HOLD-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num());                                                         //Natural: ASSIGN #WS-SAVE-PYMNT-PRCSS-SEQ-NUM := FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM
        pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cmbn_Nbr());                                                                      //Natural: ASSIGN #WS-CNTRCT-CMBN-NBR := FCP-CONS-PYMNT.CNTRCT-CMBN-NBR
    }
    private void sub_Read_Name_N_Address() throws Exception                                                                                                               //Natural: READ-NAME-N-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Nas_Start_Key_Cntrct_Ppcn_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                                      //Natural: ASSIGN #NAS-START-KEY.CNTRCT-PPCN-NBR := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        pnd_Nas_Start_Key_Cntrct_Payee_Cde.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde());                                                                    //Natural: ASSIGN #NAS-START-KEY.CNTRCT-PAYEE-CDE := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
        pnd_Nas_Start_Key_Cntrct_Orgn_Cde.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                      //Natural: ASSIGN #NAS-START-KEY.CNTRCT-ORGN-CDE := FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        pnd_Nas_Start_Key_Cntrct_Invrse_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte());                                                                  //Natural: ASSIGN #NAS-START-KEY.CNTRCT-INVRSE-DTE := FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE
        pnd_Nas_Start_Key_Pymnt_Prcss_Seq_Nbr.setValue(pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num);                                                                       //Natural: ASSIGN #NAS-START-KEY.PYMNT-PRCSS-SEQ-NBR := #WS-SAVE-PYMNT-PRCSS-SEQ-NUM
        //*  ..........  NOW READ THE NAME AND ADDRESS FILE
        ldaFcpladdr.getVw_fcp_Cons_Addr().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) FCP-CONS-ADDR BY PPCN-PAYEE-ORGN-INVRS-SEQ STARTING FROM #NAS-START-KEY
        (
        "READ_NAME_N_ADDRESS",
        new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Nas_Start_Key, WcType.BY) },
        new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
        1
        );
        READ_NAME_N_ADDRESS:
        while (condition(ldaFcpladdr.getVw_fcp_Cons_Addr().readNextRow("READ_NAME_N_ADDRESS")))
        {
            //*  ..... IF THE ADDRESS RECORD IS FOUND - WE ARE HAPPY
            if (condition((ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Ppcn_Nbr().equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr())) && (ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Payee_Cde().equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde()))  //Natural: IF ( FCP-CONS-ADDR.CNTRCT-PPCN-NBR = FCP-CONS-PYMNT.CNTRCT-PPCN-NBR ) AND ( FCP-CONS-ADDR.CNTRCT-PAYEE-CDE = FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE ) AND ( FCP-CONS-ADDR.CNTRCT-ORGN-CDE = FCP-CONS-PYMNT.CNTRCT-ORGN-CDE )
                && (ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Orgn_Cde().equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde()))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Abend_Code.setValue(78);                                                                                                                              //Natural: ASSIGN #ABEND-CODE := 78
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"PROGRAM TERMINATES WITH ABEND CODE:",pnd_Abend_Code);                                     //Natural: WRITE *PROGRAM 'PROGRAM TERMINATES WITH ABEND CODE:' #ABEND-CODE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_NAME_N_ADDRESS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_NAME_N_ADDRESS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"REASON: NO ADDRESS RECORD FOUND","=",pnd_Nas_Start_Key);                                                      //Natural: WRITE 'REASON: NO ADDRESS RECORD FOUND' '=' #NAS-START-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_NAME_N_ADDRESS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_NAME_N_ADDRESS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(pnd_Abend_Code);  if (true) return;                                                                                                     //Natural: TERMINATE #ABEND-CODE
            }                                                                                                                                                             //Natural: END-IF
            //*  READ-NAME-N-ADDRESS
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Accum_For_Control() throws Exception                                                                                                                 //Natural: ACCUM-FOR-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  DETERMINE INDECIES OF CONTROL COUNTERS
        //*  #CI1 = CONTROLS BY CONTRACT TYPE
        //*  #CI2 = CONTROLS BY PAYMENT TYPE (CHECK, EFT, GLOBAL PAY)
        //*  ---------------------------------------------------------------------
        //*  ............. DETERMINE INDEX BY CONTRACT TYPE
        short decideConditionsMet1521 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE ' '
        if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" "))))
        {
            decideConditionsMet1521++;
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("DC")))                                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'DC'
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("L")))                                                                               //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'L'
                {
                    pnd_Ci1.setValue(1);                                                                                                                                  //Natural: ASSIGN #CI1 := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("PP")))                                                                          //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'PP'
                    {
                        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ftre_Ind().notEquals("F")))                                                                     //Natural: IF FCP-CONS-PYMNT.PYMNT-FTRE-IND NE 'F'
                        {
                            pnd_Ci1.setValue(2);                                                                                                                          //Natural: ASSIGN #CI1 := 2
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ci1.setValue(3);                                                                                                                          //Natural: ASSIGN #CI1 := 3
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*   'CN' LEON 08-04-99
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'C', 'CN','CP', 'RP', 'PR'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR"))))
        {
            decideConditionsMet1521++;
            if (condition(((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("L") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("PP"))                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'L' OR = 'PP' AND FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'DC'
                && ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("DC"))))
            {
                pnd_Ci1.setValue(12);                                                                                                                                     //Natural: ASSIGN #CI1 := 12
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((((((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("10") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("20"))             //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '10' OR = '20' OR = '30' OR = '31' OR = '40' OR = '50' AND FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'MS'
                || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("30")) || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("31")) || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("40")) 
                || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("50")) && ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("MS"))))
            {
                pnd_Ci1.setValue(25);                                                                                                                                     //Natural: ASSIGN #CI1 := 25
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))))
        {
            decideConditionsMet1521++;
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("DC")))                                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'DC'
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("L")))                                                                               //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'L'
                {
                    pnd_Ci1.setValue(14);                                                                                                                                 //Natural: ASSIGN #CI1 := 14
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("PP")))                                                                          //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'PP'
                    {
                        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ftre_Ind().notEquals("F")))                                                                     //Natural: IF FCP-CONS-PYMNT.PYMNT-FTRE-IND NE 'F'
                        {
                            pnd_Ci1.setValue(15);                                                                                                                         //Natural: ASSIGN #CI1 := 15
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ci1.setValue(16);                                                                                                                         //Natural: ASSIGN #CI1 := 16
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("MS")))                                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'MS'
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("10")))                                                                              //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '10'
                {
                    pnd_Ci1.setValue(27);                                                                                                                                 //Natural: ASSIGN #CI1 := 27
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("20")))                                                                          //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '20'
                    {
                        pnd_Ci1.setValue(28);                                                                                                                             //Natural: ASSIGN #CI1 := 28
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("30")))                                                                      //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '30'
                        {
                            pnd_Ci1.setValue(29);                                                                                                                         //Natural: ASSIGN #CI1 := 29
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("31")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '31'
                            {
                                pnd_Ci1.setValue(30);                                                                                                                     //Natural: ASSIGN #CI1 := 30
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("40")))                                                              //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '40'
                                {
                                    pnd_Ci1.setValue(31);                                                                                                                 //Natural: ASSIGN #CI1 := 31
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("50")))                                                          //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '50'
                                    {
                                        pnd_Ci1.setValue(32);                                                                                                             //Natural: ASSIGN #CI1 := 32
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*   'SN' LEON 08-04-99
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'S', 'SN','SP'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP"))))
        {
            decideConditionsMet1521++;
            if (condition(((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("L") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("PP"))                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'L' OR = 'PP' AND FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'DC'
                && ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("DC"))))
            {
                pnd_Ci1.setValue(13);                                                                                                                                     //Natural: ASSIGN #CI1 := 13
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((((((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("10") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("20"))             //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '10' OR = '20' OR = '30' OR = '31' OR = '40' OR = '50' AND FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'MS'
                || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("30")) || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("31")) || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("40")) 
                || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("50")) && ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("MS"))))
            {
                pnd_Ci1.setValue(26);                                                                                                                                     //Natural: ASSIGN #CI1 := 26
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet1598 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF FCP-CONS-PYMNT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet1598++;
            //*  CHECKS AND REDRAWN CHECKS
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" "))) //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R' OR = ' '
            {
                pnd_Ci2.setValue(48);                                                                                                                                     //Natural: ASSIGN #CI2 := 48
                //*  EFT
                //*  GLOBALS   /* TMM-2001-11
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet1598++;
            pnd_Ci2.setValue(49);                                                                                                                                         //Natural: ASSIGN #CI2 := 49
        }                                                                                                                                                                 //Natural: VALUE 3,4,9
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(3) || ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(4) 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(9))))
        {
            decideConditionsMet1598++;
            pnd_Ci2.setValue(50);                                                                                                                                         //Natural: ASSIGN #CI2 := 50
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT(*) TO
        //* *  FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT(#CI2)
        //*  CANCELS        /* 'CN' LEON 08-04-99
        //*  CANCELS        /* JWO 2011-01
        //*  STOPS          /* 'SN' LEON 08-04-99
        //*  STOPS          /* JWO 2011-01
        short decideConditionsMet1621 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE 'C', 'CN'
        if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN"))))
        {
            decideConditionsMet1621++;
            pnd_Ci2.setValue(46);                                                                                                                                         //Natural: ASSIGN #CI2 := 46
        }                                                                                                                                                                 //Natural: VALUE 'CP', 'RP', 'PR'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR"))))
        {
            decideConditionsMet1621++;
            pnd_Ci2.setValue(46);                                                                                                                                         //Natural: ASSIGN #CI2 := 46
        }                                                                                                                                                                 //Natural: VALUE 'S', 'SN'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN"))))
        {
            decideConditionsMet1621++;
            pnd_Ci2.setValue(47);                                                                                                                                         //Natural: ASSIGN #CI2 := 47
        }                                                                                                                                                                 //Natural: VALUE 'SP'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP"))))
        {
            decideConditionsMet1621++;
            pnd_Ci2.setValue(47);                                                                                                                                         //Natural: ASSIGN #CI2 := 47
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Type_Cde().getValue(pnd_Ci1).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde());                                        //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-TYPE-CDE ( #CI1 ) := FCP-CONS-PYMNT.CNTRCT-TYPE-CDE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(pnd_Ci1).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue("*"));                          //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( * ) TO FCP-CONS-CNTRL.CNTL-GROSS-AMT ( #CI1 )
        //*  ...... ADD FOR CONTROL TOTALS BY PAY TYPE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(pnd_Ci2).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue("*"));                          //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( * ) TO FCP-CONS-CNTRL.CNTL-GROSS-AMT ( #CI2 )
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                        //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
        {
            pnd_C_Selected_Pymnts.nadd(1);                                                                                                                                //Natural: ADD 1 TO #C-SELECTED-PYMNTS
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(pnd_Ci1).nadd(1);                                                                                           //Natural: ADD 1 TO FCP-CONS-CNTRL.CNTL-CNT ( #CI1 )
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(pnd_Ci2).nadd(1);                                                                                           //Natural: ADD 1 TO FCP-CONS-CNTRL.CNTL-CNT ( #CI2 )
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(pnd_Ci2).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Amt());                                         //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO FCP-CONS-CNTRL.CNTL-NET-AMT ( #CI2 )
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(pnd_Ci1).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Amt());                                         //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO FCP-CONS-CNTRL.CNTL-NET-AMT ( #CI1 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ACCUM-FOR-CONTROL
    }
    private void sub_Write_Control_Rec() throws Exception                                                                                                                 //Natural: WRITE-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*   CHANGED CALL TO CPWA115      ROXAN - 12/04/02
        //* CALLNAT 'FCPN115' USING FCPA115
        DbsUtil.callnat(Cpwn115.class , getCurrentProcessState(), pdaCpwa115.getCpwa115());                                                                               //Natural: CALLNAT 'CPWN115' USING CPWA115
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpwa115.getCpwa115_Error_Code().notEquals(getZero())))                                                                                           //Natural: IF CPWA115.ERROR-CODE NE 0
        {
            getReports().write(0, ReportOption.NOTITLE,"ERROR: FCPP370 detects invalid RETURN-CODE of",pdaCpwa115.getCpwa115_Error_Code(),"from",pdaCpwa115.getCpwa115_Error_Program()); //Natural: WRITE 'ERROR: FCPP370 detects invalid RETURN-CODE of' CPWA115.ERROR-CODE 'from' CPWA115.ERROR-PROGRAM
            if (Global.isEscape()) return;
            DbsUtil.stop();  if (true) return;                                                                                                                            //Natural: STOP
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().setValue("DC");                                                                                                     //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-ORGN-CDE := 'DC'
        pnd_Date_Numeric.setValue(pdaCpwa115.getCpwa115_Interface_Date());                                                                                                //Natural: ASSIGN #DATE-NUMERIC := CPWA115.INTERFACE-DATE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cycle_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_Numeric_Pnd_Date_Alpha);                                    //Natural: MOVE EDITED #DATE-ALPHA TO FCP-CONS-CNTRL.CNTL-CYCLE-DTE ( EM = YYYYMMDD )
        pnd_Date_Numeric.setValue(pdaCpwa115.getCpwa115_Payment_Date());                                                                                                  //Natural: ASSIGN #DATE-NUMERIC := CPWA115.PAYMENT-DATE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Check_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_Numeric_Pnd_Date_Alpha);                                    //Natural: MOVE EDITED #DATE-ALPHA TO FCP-CONS-CNTRL.CNTL-CHECK-DTE ( EM = YYYYMMDD )
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Invrse_Dte().compute(new ComputeParameters(false, ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Invrse_Dte()), (DbsField.subtract(100000000, //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-INVRSE-DTE := ( 100000000 - CPWA115.PAYMENT-DATE )
            pdaCpwa115.getCpwa115_Payment_Date())));
        ldaFcplcntu.getVw_fcp_Cons_Cntrl().insertDBRow();                                                                                                                 //Natural: STORE FCP-CONS-CNTRL
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Extract_Lcv_From_Crunched_Data() throws Exception                                                                                                    //Natural: EXTRACT-LCV-FROM-CRUNCHED-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_W_Dte.setValueEdited(pnd_Lcv_Pymnt_Settlmnt_Dte.getValue(1),new ReportEditMask("YYYYMMDD"));                                                                  //Natural: MOVE EDITED #LCV.PYMNT-SETTLMNT-DTE ( 1 ) ( EM = YYYYMMDD ) TO #W-DTE
        if (condition(pnd_W_Dte_Pnd_W_Mm.equals(1)))                                                                                                                      //Natural: IF #W-MM = 1
        {
            pnd_W_Dte_Pnd_W_Mm.setValue(12);                                                                                                                              //Natural: ASSIGN #W-MM := 12
            pnd_W_Dte_Pnd_W_Yyyy.nsubtract(1);                                                                                                                            //Natural: SUBTRACT 1 FROM #W-YYYY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_W_Dte_Pnd_W_Mm.nsubtract(1);                                                                                                                              //Natural: SUBTRACT 1 FROM #W-MM
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl370.getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End().getValue(pnd_I).setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Dte);                          //Natural: MOVE EDITED #W-DTE TO #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-END ( #I ) ( EM = YYYYMMDD )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_Settl_Amt.getValue(pnd_I));                                           //Natural: SUBTRACT #LCV.INV-ACCT-SETTL-AMT ( #I ) FROM #WS-OCCURS.INV-ACCT-SETTL-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_Dvdnd_Amt.getValue(pnd_I));                                           //Natural: SUBTRACT #LCV.INV-ACCT-DVDND-AMT ( #I ) FROM #WS-OCCURS.INV-ACCT-DVDND-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_Ivc_Amt.getValue(pnd_I));                                               //Natural: SUBTRACT #LCV.INV-ACCT-IVC-AMT ( #I ) FROM #WS-OCCURS.INV-ACCT-IVC-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dci_Amt().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_Dci_Amt.getValue(pnd_I));                                               //Natural: SUBTRACT #LCV.INV-ACCT-DCI-AMT ( #I ) FROM #WS-OCCURS.INV-ACCT-DCI-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_Dpi_Amt.getValue(pnd_I));                                               //Natural: SUBTRACT #LCV.INV-ACCT-DPI-AMT ( #I ) FROM #WS-OCCURS.INV-ACCT-DPI-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_I));                                   //Natural: SUBTRACT #LCV.INV-ACCT-NET-PYMNT-AMT ( #I ) FROM #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_Adj_Ivc_Amt.getValue(pnd_I));                                       //Natural: SUBTRACT #LCV.INV-ACCT-ADJ-IVC-AMT ( #I ) FROM #WS-OCCURS.INV-ACCT-ADJ-IVC-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_I));                                     //Natural: SUBTRACT #LCV.INV-ACCT-FDRL-TAX-AMT ( #I ) FROM #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_State_Tax_Amt.getValue(pnd_I));                                   //Natural: SUBTRACT #LCV.INV-ACCT-STATE-TAX-AMT ( #I ) FROM #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_Local_Tax_Amt.getValue(pnd_I));                                   //Natural: SUBTRACT #LCV.INV-ACCT-LOCAL-TAX-AMT ( #I ) FROM #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Exp_Amt().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_Exp_Amt.getValue(pnd_I));                                               //Natural: SUBTRACT #LCV.INV-ACCT-EXP-AMT ( #I ) FROM #WS-OCCURS.INV-ACCT-EXP-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Unit_Qty().getValue(pnd_I).nsubtract(pnd_Lcv_Inv_Acct_Unit_Qty.getValue(pnd_I));                                             //Natural: SUBTRACT #LCV.INV-ACCT-UNIT-QTY ( #I ) FROM #WS-OCCURS.INV-ACCT-UNIT-QTY ( #I )
        FOR10:                                                                                                                                                            //Natural: FOR #K 1 TO 10
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(10)); pnd_K.nadd(1))
        {
            if (condition(ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_I,pnd_K).equals(pnd_Lcv_Pymnt_Ded_Cde.getValue(pnd_I,pnd_K))))                         //Natural: IF #WS-OCCURS.PYMNT-DED-CDE ( #I,#K ) = #LCV.PYMNT-DED-CDE ( #I,#K )
            {
                ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_I,pnd_K).nsubtract(pnd_Lcv_Pymnt_Ded_Amt.getValue(pnd_I,pnd_K));                                 //Natural: SUBTRACT #LCV.PYMNT-DED-AMT ( #I,#K ) FROM #WS-OCCURS.PYMNT-DED-AMT ( #I,#K )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Crunch_Lcv_Data() throws Exception                                                                                                                   //Natural: CRUNCH-LCV-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_I.less(40)))                                                                                                                                    //Natural: IF #I LT 40
        {
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Abend_Code.setValue(81);                                                                                                                                  //Natural: ASSIGN #ABEND-CODE := 81
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"PROGRAM TERMINATES WITH ABEND CODE:",pnd_Abend_Code);                                         //Natural: WRITE *PROGRAM 'PROGRAM TERMINATES WITH ABEND CODE:' #ABEND-CODE
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"Reason: Date ranges cover more than 20 years");                                                                   //Natural: WRITE 'Reason: Date ranges cover more than 20 years'
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Abend_Code);  if (true) return;                                                                                                         //Natural: TERMINATE #ABEND-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //*   REMOVE REFERENCE TO LCV
        //*  MOVE BY NAME #LCV.#LCV-SINGLE-INFO TO #WS-OCCURS.#WS-OCCURS-ENTRY(#I)
        //*  MOVE BY NAME #LCV.#LCV-ARRAY-INFO TO #WS-OCCURS.#WS-OCCURS-ENTRY(#I)
        ldaFcpl370.getPnd_Ws_Occurs_Pnd_Ws_Occurs_Entry().getValue(pnd_I).setValuesByName(pnd_Lcv_Pnd_Lcv_Single_Info.getValue(pnd_I));                                   //Natural: MOVE BY NAME #LCV-SINGLE-INFO ( #I ) TO #WS-OCCURS.#WS-OCCURS-ENTRY ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Pnd_Ws_Occurs_Entry().getValue(pnd_I).setValuesByName(pnd_Lcv_Pnd_Lcv_Array_Info.getValue(pnd_I));                                    //Natural: MOVE BY NAME #LCV-ARRAY-INFO ( #I ) TO #WS-OCCURS.#WS-OCCURS-ENTRY ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_Start().getValue(pnd_I).setValue(pnd_Lcv_Pymnt_Settlmnt_Dte.getValue(pnd_I));                            //Natural: ASSIGN #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-START ( #I ) := #LCV.PYMNT-SETTLMNT-DTE ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_End().getValue(pnd_I).setValue(pnd_Lcv_Pymnt_Settlmnt_Dte.getValue(pnd_I));                              //Natural: ASSIGN #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE-END ( #I ) := #LCV.PYMNT-SETTLMNT-DTE ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_I).setValue(pnd_Lcv_Inv_Acct_Settl_Amt.getValue(pnd_I));                                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-SETTL-AMT ( #I ) := #LCV.INV-ACCT-SETTL-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_I).setValue(pnd_Lcv_Inv_Acct_Dvdnd_Amt.getValue(pnd_I));                                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-DVDND-AMT ( #I ) := #LCV.INV-ACCT-DVDND-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Ivc_Amt().getValue(pnd_I).setValue(pnd_Lcv_Inv_Acct_Ivc_Amt.getValue(pnd_I));                                                //Natural: ASSIGN #WS-OCCURS.INV-ACCT-IVC-AMT ( #I ) := #LCV.INV-ACCT-IVC-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dci_Amt().getValue(pnd_I).setValue(pnd_Lcv_Inv_Acct_Dci_Amt.getValue(pnd_I));                                                //Natural: ASSIGN #WS-OCCURS.INV-ACCT-DCI-AMT ( #I ) := #LCV.INV-ACCT-DCI-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_I).setValue(pnd_Lcv_Inv_Acct_Dpi_Amt.getValue(pnd_I));                                                //Natural: ASSIGN #WS-OCCURS.INV-ACCT-DPI-AMT ( #I ) := #LCV.INV-ACCT-DPI-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I).setValue(pnd_Lcv_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_I));                                    //Natural: ASSIGN #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #I ) := #LCV.INV-ACCT-NET-PYMNT-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt().getValue(pnd_I).setValue(pnd_Lcv_Inv_Acct_Adj_Ivc_Amt.getValue(pnd_I));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-ADJ-IVC-AMT ( #I ) := #LCV.INV-ACCT-ADJ-IVC-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I).setValue(pnd_Lcv_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_I));                                      //Natural: ASSIGN #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #I ) := #LCV.INV-ACCT-FDRL-TAX-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_I).setValue(pnd_Lcv_Inv_Acct_State_Tax_Amt.getValue(pnd_I));                                    //Natural: ASSIGN #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #I ) := #LCV.INV-ACCT-STATE-TAX-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_I).setValue(pnd_Lcv_Inv_Acct_Local_Tax_Amt.getValue(pnd_I));                                    //Natural: ASSIGN #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #I ) := #LCV.INV-ACCT-LOCAL-TAX-AMT ( #I )
        ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Exp_Amt().getValue(pnd_I).setValue(pnd_Lcv_Inv_Acct_Exp_Amt.getValue(pnd_I));                                                //Natural: ASSIGN #WS-OCCURS.INV-ACCT-EXP-AMT ( #I ) := #LCV.INV-ACCT-EXP-AMT ( #I )
        FOR11:                                                                                                                                                            //Natural: FOR #K 1 TO 10
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(10)); pnd_K.nadd(1))
        {
            ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Cde().getValue(pnd_I,pnd_K).setValue(pnd_Lcv_Pymnt_Ded_Cde.getValue(pnd_I,pnd_K));                                      //Natural: ASSIGN #WS-OCCURS.PYMNT-DED-CDE ( #I,#K ) := #LCV.PYMNT-DED-CDE ( #I,#K )
            ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_I,pnd_K).setValue(pnd_Lcv_Pymnt_Ded_Amt.getValue(pnd_I,pnd_K));                                      //Natural: ASSIGN #WS-OCCURS.PYMNT-DED-AMT ( #I,#K ) := #LCV.PYMNT-DED-AMT ( #I,#K )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Compute_For_Fund_Deduction() throws Exception                                                                                                        //Natural: COMPUTE-FOR-FUND-DEDUCTION
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------------------------------------
        pnd_Ws_Work_Variables_Pnd_Ws_Settl_Amt.reset();                                                                                                                   //Natural: RESET #WS-SETTL-AMT
        pnd_Ws_Work_Variables_Pnd_Ws_Settl_Amt.compute(new ComputeParameters(false, pnd_Ws_Work_Variables_Pnd_Ws_Settl_Amt), ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X).add(ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X)).add(ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X)).add(ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X))); //Natural: ASSIGN #WS-SETTL-AMT := #WS-OCCURS.INV-ACCT-SETTL-AMT ( #X ) + #WS-OCCURS.INV-ACCT-DVDND-AMT ( #X ) + #WS-OCCURS.INV-ACCT-DCI-AMT ( #X ) + #WS-OCCURS.INV-ACCT-DPI-AMT ( #X )
        ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X,pnd_I1).compute(new ComputeParameters(false, ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X,pnd_I1)),  //Natural: ASSIGN #WS-OCCURS.PYMNT-DED-AMT ( #X,#I1 ) := #WS-SETTL-AMT - #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #X ) - #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #X ) - #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #X ) - #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #X )
            pnd_Ws_Work_Variables_Pnd_Ws_Settl_Amt.subtract(ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X)).subtract(ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X)).subtract(ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X)).subtract(ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X)));
        pnd_Ded_Run_Tot.nadd(ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X,pnd_I1));                                                   //Natural: ASSIGN #DED-RUN-TOT := #DED-RUN-TOT + #WS-OCCURS.PYMNT-DED-AMT ( #X,#I1 )
        if (condition(ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X,pnd_I1).less(getZero())))                                          //Natural: IF #WS-OCCURS.PYMNT-DED-AMT ( #X,#I1 ) < 0
        {
            pnd_Ws_Work_Variables_Pnd_Ws_Negative_Ovr_Pymnt.setValue("0007");                                                                                             //Natural: ASSIGN #WS-NEGATIVE-OVR-PYMNT := '0007'
            getReports().write(0, ReportOption.NOTITLE,"* ------------------------------------------------ *",NEWLINE,"* -                                              - *", //Natural: WRITE '* ------------------------------------------------ *' / '* -                                              - *' / '* -             CALL SYSTEMS                     - *' / '* -                                              - *' / '* ------------------------------------------------ *' / '* -                                              - *' / '* -       PPCN NBR     : ' #WS-OCCURS.CNTRCT-PPCN-NBR ( #X ) / '* -       SETTLE AMT   : ' #WS-OCCURS.INV-ACCT-SETTL-AMT ( #X ) / '* -       DIVIDEND AMT : ' #WS-OCCURS.INV-ACCT-DVDND-AMT ( #X ) / '* -       DCI AMT      : ' #WS-OCCURS.INV-ACCT-DCI-AMT ( #X ) / '* -       DPI AMT      : ' #WS-OCCURS.INV-ACCT-DPI-AMT ( #X ) / '* -       NET AMT      : ' #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #X ) / '* -       OVER PAYMENT : ' #WS-OCCURS.PYMNT-DED-AMT ( #X,#I1 ) / '* ------------------------------------------------ *'
                NEWLINE,"* -             CALL SYSTEMS                     - *",NEWLINE,"* -                                              - *",NEWLINE,"* ------------------------------------------------ *",
                NEWLINE,"* -                                              - *",NEWLINE,"* -       PPCN NBR     : ",ldaFcpl370.getPnd_Ws_Occurs_Cntrct_Ppcn_Nbr().getValue(pnd_Ws_Work_Variables_Pnd_X),
                NEWLINE,"* -       SETTLE AMT   : ",ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X),NEWLINE,"* -       DIVIDEND AMT : ",
                ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X),NEWLINE,"* -       DCI AMT      : ",ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X),
                NEWLINE,"* -       DPI AMT      : ",ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X),NEWLINE,"* -       NET AMT      : ",
                ldaFcpl370.getPnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X),NEWLINE,"* -       OVER PAYMENT : ",ldaFcpl370.getPnd_Ws_Occurs_Pymnt_Ded_Amt().getValue(pnd_Ws_Work_Variables_Pnd_X,
                pnd_I1),NEWLINE,"* ------------------------------------------------ *");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header0_1,new TabSetting(124),"Page",getReports().getPageNumberDbs(0),  //Natural: WRITE NOTITLE *PROGRAM 41T #HEADER0-1 124T 'Page' *PAGE-NUMBER ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER0-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header0_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60 ZP=ON");
    }
}
