/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:20 PM
**        * FROM NATURAL PROGRAM : Fcpp231c
************************************************************
**        * FILE NAME            : Fcpp231c.java
**        * CLASS NAME           : Fcpp231c
**        * INSTANCE NAME        : Fcpp231c
************************************************************
***********************************************************************
* PROGRAM   : FCPP231C
* SYSTEM    : CPS
* TITLE     : AUDIT REPORT
* GENERATED :
* FUNCTION  : PRINT AUDIT REPORT
*           :
* HISTORY   :
* 08/18/99  : R. CARREON  -  CREATED
*           :
***********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp231c extends BLNatBase
{
    // Data Areas
    private LdaFcpl231a ldaFcpl231a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ws_Write_Data;
    private DbsField pnd_Ws_Save_Orgn_Cde;
    private DbsField pnd_Ws_Soc_Sec;
    private DbsField pnd_Ws_Date_Alpha;
    private DbsField pnd_Ws_Gross_Amt;
    private DbsField pnd_Ws_Taxable_Amt;
    private DbsField pnd_Ws_Date;

    private DbsGroup pnd_Ws_Date__R_Field_1;
    private DbsField pnd_Ws_Date_Pnd_Ws_Date_1;
    private DbsField pnd_Ws_Date_Pnd_Ws_Date_2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl231a = new LdaFcpl231a();
        registerRecord(ldaFcpl231a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Ws_Write_Data = localVariables.newFieldInRecord("pnd_Ws_Write_Data", "#WS-WRITE-DATA", FieldType.BOOLEAN, 1);
        pnd_Ws_Save_Orgn_Cde = localVariables.newFieldInRecord("pnd_Ws_Save_Orgn_Cde", "#WS-SAVE-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Soc_Sec = localVariables.newFieldInRecord("pnd_Ws_Soc_Sec", "#WS-SOC-SEC", FieldType.STRING, 11);
        pnd_Ws_Date_Alpha = localVariables.newFieldInRecord("pnd_Ws_Date_Alpha", "#WS-DATE-ALPHA", FieldType.DATE);
        pnd_Ws_Gross_Amt = localVariables.newFieldInRecord("pnd_Ws_Gross_Amt", "#WS-GROSS-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Ws_Taxable_Amt = localVariables.newFieldInRecord("pnd_Ws_Taxable_Amt", "#WS-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Ws_Date = localVariables.newFieldInRecord("pnd_Ws_Date", "#WS-DATE", FieldType.NUMERIC, 9);

        pnd_Ws_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Date__R_Field_1", "REDEFINE", pnd_Ws_Date);
        pnd_Ws_Date_Pnd_Ws_Date_1 = pnd_Ws_Date__R_Field_1.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Date_1", "#WS-DATE-1", FieldType.STRING, 1);
        pnd_Ws_Date_Pnd_Ws_Date_2 = pnd_Ws_Date__R_Field_1.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Date_2", "#WS-DATE-2", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl231a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp231c() throws Exception
    {
        super("Fcpp231c");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        READWORK01:                                                                                                                                                       //Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: READ WORK FILE 1 FCPL231A
        while (condition(getWorkFiles().read(1, ldaFcpl231a.getFcpl231a())))
        {
            CheckAtStartofData68();

            //*                                                                                                                                                           //Natural: AT START OF DATA
            if (condition(pnd_Ws_Save_Orgn_Cde.notEquals(ldaFcpl231a.getFcpl231a_Cntrct_Orgn_Cde())))                                                                     //Natural: IF #WS-SAVE-ORGN-CDE NE CNTRCT-ORGN-CDE
            {
                pnd_Ws_Save_Orgn_Cde.setValue(ldaFcpl231a.getFcpl231a_Cntrct_Orgn_Cde());                                                                                 //Natural: ASSIGN #WS-SAVE-ORGN-CDE := CNTRCT-ORGN-CDE
                if (condition(! (pnd_Ws_Write_Data.getBoolean())))                                                                                                        //Natural: IF NOT #WS-WRITE-DATA
                {
                    getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(41)," *** THERE WAS NO PAYMENT FOR THIS ORIGIN CODE *** ");                      //Natural: WRITE ( 1 ) //// 41T ' *** THERE WAS NO PAYMENT FOR THIS ORIGIN CODE *** '
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(59),"   END OF REPORT ");                                                                    //Natural: WRITE ( 1 ) /// 59T '   END OF REPORT '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().getPageNumberDbs(1).reset();                                                                                                                 //Natural: RESET *PAGE-NUMBER ( 1 )
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, " ");                                                                                                                               //Natural: WRITE ( 1 ) ' '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Write_Data.setValue(false);                                                                                                                        //Natural: ASSIGN #WS-WRITE-DATA := FALSE
            }                                                                                                                                                             //Natural: END-IF
            //*   IF ANNT-SOC-SEC-NBR = 0
            if (condition(ldaFcpl231a.getFcpl231a_Cntrct_Ppcn_Nbr().equals(" ")))                                                                                         //Natural: IF CNTRCT-PPCN-NBR = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl231a.getFcpl231a_Annt_Soc_Sec_Ind().equals(getZero())))                                                                                  //Natural: IF ANNT-SOC-SEC-IND = 0
            {
                pnd_Ws_Soc_Sec.setValueEdited(ldaFcpl231a.getFcpl231a_Annt_Soc_Sec_Nbr(),new ReportEditMask("999-99-9999"));                                              //Natural: MOVE EDITED ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) TO #WS-SOC-SEC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Soc_Sec.setValueEdited(ldaFcpl231a.getFcpl231a_Annt_Soc_Sec_Nbr(),new ReportEditMask("9-99999999"));                                               //Natural: MOVE EDITED ANNT-SOC-SEC-NBR ( EM = 99-99999999 ) TO #WS-SOC-SEC
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Gross_Amt.compute(new ComputeParameters(false, pnd_Ws_Gross_Amt), ldaFcpl231a.getFcpl231a_Inv_Acct_Settl_Amt().add(ldaFcpl231a.getFcpl231a_Inv_Acct_Dvdnd_Amt())); //Natural: ASSIGN #WS-GROSS-AMT := INV-ACCT-SETTL-AMT + INV-ACCT-DVDND-AMT
            pnd_Ws_Taxable_Amt.compute(new ComputeParameters(false, pnd_Ws_Taxable_Amt), pnd_Ws_Gross_Amt.subtract(ldaFcpl231a.getFcpl231a_Inv_Acct_Ivc_Amt()));          //Natural: ASSIGN #WS-TAXABLE-AMT := #WS-GROSS-AMT - INV-ACCT-IVC-AMT
            if (condition(ldaFcpl231a.getFcpl231a_Cnr_Orgnl_Invrse_Dte().greater(getZero())))                                                                             //Natural: IF CNR-ORGNL-INVRSE-DTE > 0
            {
                pnd_Ws_Date.compute(new ComputeParameters(false, pnd_Ws_Date), DbsField.subtract(100000000,ldaFcpl231a.getFcpl231a_Cnr_Orgnl_Invrse_Dte()));              //Natural: ASSIGN #WS-DATE := 100000000 - CNR-ORGNL-INVRSE-DTE
                pnd_Ws_Date_Alpha.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Date_Pnd_Ws_Date_2);                                                               //Natural: MOVE EDITED #WS-DATE-2 TO #WS-DATE-ALPHA ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(getReports().getAstLinesLeft(1).less(8)))                                                                                                       //Natural: NEWPAGE ( 1 ) LESS THAN 8 LINES
            {
                getReports().newPage(1);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }
            getReports().write(1, NEWLINE,new TabSetting(1),pnd_Ws_Soc_Sec,new TabSetting(13),ldaFcpl231a.getFcpl231a_Cntrct_Ppcn_Nbr(), new ReportEditMask               //Natural: WRITE ( 1 ) / 001T #WS-SOC-SEC 013T CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) 024T #WS-DATE-ALPHA ( EM = MM/DD/YY ) 033T #PARTICIPANT-NAME ( AL = 24 ) 059T #WS-GROSS-AMT ( EM = ZZZ,ZZZ.99 ) 070T INV-ACCT-IVC-AMT ( EM = ZZZ,ZZZ.99 ) 081T #WS-TAXABLE-AMT ( EM = ZZZ,ZZZ.99 ) 092T INV-ACCT-FDRL-TAX-AMT ( EM = ZZZ,ZZZ.99 ) 103T INV-ACCT-STATE-TAX-AMT ( EM = ZZ,ZZZ.99 ) 113T INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZ.99 )
                ("XXXXXXX-X"),new TabSetting(24),pnd_Ws_Date_Alpha, new ReportEditMask ("MM/DD/YY"),new TabSetting(33),ldaFcpl231a.getFcpl231a_Pnd_Participant_Name(), 
                new AlphanumericLength (24),new TabSetting(59),pnd_Ws_Gross_Amt, new ReportEditMask ("ZZZ,ZZZ.99"),new TabSetting(70),ldaFcpl231a.getFcpl231a_Inv_Acct_Ivc_Amt(), 
                new ReportEditMask ("ZZZ,ZZZ.99"),new TabSetting(81),pnd_Ws_Taxable_Amt, new ReportEditMask ("ZZZ,ZZZ.99"),new TabSetting(92),ldaFcpl231a.getFcpl231a_Inv_Acct_Fdrl_Tax_Amt(), 
                new ReportEditMask ("ZZZ,ZZZ.99"),new TabSetting(103),ldaFcpl231a.getFcpl231a_Inv_Acct_State_Tax_Amt(), new ReportEditMask ("ZZ,ZZZ.99"),new 
                TabSetting(113),ldaFcpl231a.getFcpl231a_Inv_Acct_Local_Tax_Amt(), new ReportEditMask ("ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaFcpl231a.getFcpl231a_Pnd_Current_Address().greater(" ")))                                                                                    //Natural: IF #CURRENT-ADDRESS > ' '
            {
                getReports().write(1, new TabSetting(24),"CURRENT  ADDR :",new TabSetting(40),ldaFcpl231a.getFcpl231a_Pnd_Current_Address(), new AlphanumericLength       //Natural: WRITE ( 1 ) 024T 'CURRENT  ADDR :' 040T #CURRENT-ADDRESS ( AL = 50 ) / 024T 'PREVIOUS ADDR :' 040T #PREVIOUS-ADDRESS ( AL = 50 )
                    (50),NEWLINE,new TabSetting(24),"PREVIOUS ADDR :",new TabSetting(40),ldaFcpl231a.getFcpl231a_Pnd_Previous_Address(), new AlphanumericLength 
                    (50));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Write_Data.setValue(true);                                                                                                                             //Natural: ASSIGN #WS-WRITE-DATA := TRUE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(! (pnd_Ws_Write_Data.getBoolean())))                                                                                                                //Natural: IF NOT #WS-WRITE-DATA
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(41)," *** THERE WAS NO PAYMENT FOR THIS ORIGIN CODE *** ");                              //Natural: WRITE ( 1 ) //// 41T ' *** THERE WAS NO PAYMENT FOR THIS ORIGIN CODE *** '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(59),"   END OF REPORT ");                                                                            //Natural: WRITE ( 1 ) /// 59T '   END OF REPORT '
        if (Global.isEscape()) return;
        //*  -------------------- SUBROUTINES ----------------------------------*
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 001T *DATU '-' 012T *TIMX ( EM = HH':'IIAP ) 030T CNTRCT-ORGN-CDE 053T 'CONSOLIDATED PAYMENT SYSTEM' 118T 'PAGE:' *PAGE-NUMBER ( 1 ) ( AD = L ) / 001T *INIT-USER '-' 012T *PROGRAM 045T 'DAILY CANCEL/STOP AND REDRAW WITH ADDRESS CHANGES' / 001T *LIBRARY-ID 059T 'FOR ' PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) // 001T 'TAX-ID' 013T 'CONTRACT' 024T 'ORIGINAL' 063T 'GROSS' 076T 'IVC' 084T 'TAXABLE' 095T 'FEDERAL' 107T 'STATE' 114T 'LOCAL' / 001T 'SSN' 014T 'NUMBER ' 024T 'PAYMENT' 033T 'PARTICIPANT NAME' 063T 'AMOUNT' 074T 'AMOUNT' 084T 'AMOUNT' 097T '  TAX  ' 108T ' TAX ' 115T 'TAX ' / 001T '-' ( 11 ) 013T '-' ( 09 ) 024T '-' ( 08 ) 033T '-' ( 25 ) 059T '-' ( 10 ) 070T '-' ( 10 ) 081T '-' ( 10 ) 092T '-' ( 10 ) 103T '-' ( 9 ) 113T '------'
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),Global.getDATU(),"-",new TabSetting(12),Global.getTIMX(), 
            new ReportEditMask ("HH':'IIAP"),new TabSetting(30),ldaFcpl231a.getFcpl231a_Cntrct_Orgn_Cde(),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(118),"PAGE:",getReports().getPageNumberDbs(1), new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(1),Global.getINIT_USER(),"-",new 
            TabSetting(12),Global.getPROGRAM(),new TabSetting(45),"DAILY CANCEL/STOP AND REDRAW WITH ADDRESS CHANGES",NEWLINE,new TabSetting(1),Global.getLIBRARY_ID(),new 
            TabSetting(59),"FOR ",ldaFcpl231a.getFcpl231a_Pymnt_Check_Dte(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(1),"TAX-ID",new 
            TabSetting(13),"CONTRACT",new TabSetting(24),"ORIGINAL",new TabSetting(63),"GROSS",new TabSetting(76),"IVC",new TabSetting(84),"TAXABLE",new 
            TabSetting(95),"FEDERAL",new TabSetting(107),"STATE",new TabSetting(114),"LOCAL",NEWLINE,new TabSetting(1),"SSN",new TabSetting(14),"NUMBER ",new 
            TabSetting(24),"PAYMENT",new TabSetting(33),"PARTICIPANT NAME",new TabSetting(63),"AMOUNT",new TabSetting(74),"AMOUNT",new TabSetting(84),"AMOUNT",new 
            TabSetting(97),"  TAX  ",new TabSetting(108)," TAX ",new TabSetting(115),"TAX ",NEWLINE,new TabSetting(1),"-",new RepeatItem(11),new TabSetting(13),"-",new 
            RepeatItem(9),new TabSetting(24),"-",new RepeatItem(8),new TabSetting(33),"-",new RepeatItem(25),new TabSetting(59),"-",new RepeatItem(10),new 
            TabSetting(70),"-",new RepeatItem(10),new TabSetting(81),"-",new RepeatItem(10),new TabSetting(92),"-",new RepeatItem(10),new TabSetting(103),"-",new 
            RepeatItem(9),new TabSetting(113),"------");
    }
    private void CheckAtStartofData68() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            getReports().write(1, " ");                                                                                                                                   //Natural: WRITE ( 1 ) ' '
            if (condition(Global.isEscape())) return;
            pnd_Ws_Save_Orgn_Cde.setValue(ldaFcpl231a.getFcpl231a_Cntrct_Orgn_Cde());                                                                                     //Natural: ASSIGN #WS-SAVE-ORGN-CDE := CNTRCT-ORGN-CDE
        }                                                                                                                                                                 //Natural: END-START
    }
}
