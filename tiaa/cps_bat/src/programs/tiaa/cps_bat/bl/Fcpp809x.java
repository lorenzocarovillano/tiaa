/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:09 PM
**        * FROM NATURAL PROGRAM : Fcpp809x
************************************************************
**        * FILE NAME            : Fcpp809x.java
**        * CLASS NAME           : Fcpp809x
**        * INSTANCE NAME        : Fcpp809x
************************************************************
************************************************************************
* PROGRAM   : FCPP809
* SYSTEM    : CPS
* TITLE     : DEDUCTIONS EXTRACT FRO PA-SELECT, PERSONAL ANNUITY
* GENERATED :
*           : PA-SELECT/PERSONAL ANNUITY
*           :
*           :
* HISTORY   :
* ---------   ---------------------------------
* 06/29/00      CREATION
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp809x extends BLNatBase
{
    // Data Areas
    private LdaFcpl804 ldaFcpl804;
    private LdaFcpl809 ldaFcpl809;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ded_Code;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl804 = new LdaFcpl804();
        registerRecord(ldaFcpl804);
        ldaFcpl809 = new LdaFcpl809();
        registerRecord(ldaFcpl809);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Ded_Code = localVariables.newFieldInRecord("pnd_Ded_Code", "#DED-CODE", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl804.initializeValues();
        ldaFcpl809.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp809x() throws Exception
    {
        super("Fcpp809x");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP809X", onError);
        //*  ------------------------------------------------
        //*  ------------------------------------------------
        //*  ------------------------------------------------                                                                                                             //Natural: ON ERROR
        //*  READ EXTRACTED DEDUCTIONS FILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 DEDUCTION-REC
        while (condition(getWorkFiles().read(1, ldaFcpl804.getDeduction_Rec())))
        {
            if (condition(!(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(7) || ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(9))))                             //Natural: ACCEPT IF PYMNT-DED-CDE = 007 OR = 009
            {
                continue;
            }
            ldaFcpl809.getPnd_Ltc_Ded().reset();                                                                                                                          //Natural: RESET #LTC-DED
            ldaFcpl809.getPnd_Ltc_Ded_Pnd_Pymnt_Check_Dte().setValueEdited(ldaFcpl804.getDeduction_Rec_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                 //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #PYMNT-CHECK-DTE
            ldaFcpl809.getPnd_Ltc_Ded_Pnd_Pymnt_Name_A9().setValue(ldaFcpl804.getDeduction_Rec_Ph_Last_Name());                                                           //Natural: MOVE PH-LAST-NAME TO #PYMNT-NAME-A9
            setValueToSubstring(ldaFcpl804.getDeduction_Rec_Ph_First_Init(),ldaFcpl809.getPnd_Ltc_Ded_Pnd_Pymnt_Name_A9(),8,1);                                           //Natural: MOVE PH-FIRST-INIT TO SUBSTRING ( #PYMNT-NAME-A9,8,1 )
            setValueToSubstring(ldaFcpl804.getDeduction_Rec_Ph_Middle_Init(),ldaFcpl809.getPnd_Ltc_Ded_Pnd_Pymnt_Name_A9(),9,1);                                          //Natural: MOVE PH-MIDDLE-INIT TO SUBSTRING ( #PYMNT-NAME-A9,9,1 )
            ldaFcpl809.getPnd_Ltc_Ded_Pnd_Annt_Soc_Sec_Nbr().setValue(ldaFcpl804.getDeduction_Rec_Annt_Soc_Sec_Nbr());                                                    //Natural: MOVE ANNT-SOC-SEC-NBR TO #ANNT-SOC-SEC-NBR
            ldaFcpl809.getPnd_Ltc_Ded_Pnd_Pymnt_Ded_Seq_Nbr().setValue(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Seq());                                                //Natural: MOVE PYMNT-DED-PAYEE-SEQ TO #PYMNT-DED-SEQ-NBR
            ldaFcpl809.getPnd_Ltc_Ded_Pnd_Cntrct_Ppcn_Nbr_A8().setValue(ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr());                                                   //Natural: MOVE CNTRCT-PPCN-NBR TO #CNTRCT-PPCN-NBR-A8
            ldaFcpl809.getPnd_Ltc_Ded_Pnd_Cntrct_Payee_Cde_A2().setValue(ldaFcpl804.getDeduction_Rec_Cntrct_Payee_Cde());                                                 //Natural: MOVE CNTRCT-PAYEE-CDE TO #CNTRCT-PAYEE-CDE-A2
            ldaFcpl809.getPnd_Ltc_Ded_Pnd_Pymnt_Ded_Amt().setValue(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                                                          //Natural: MOVE PYMNT-DED-AMT TO #PYMNT-DED-AMT
            getWorkFiles().write(2, false, ldaFcpl809.getPnd_Ltc_Ded(), ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde());                                                     //Natural: WRITE WORK FILE 2 #LTC-DED PYMNT-DED-CDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"***********************************************************",NEWLINE,"***********************************************************", //Natural: WRITE // '***********************************************************' / '***********************************************************' / '***'*PROGRAM '  ERROR:' *ERROR-NR 'LINE:' *ERROR-LINE / '***  LAST RECORD READ:' / '***         COMBINE #:' CNTRCT-CMBN-NBR / '***              PPCN:' CNTRCT-PPCN-NBR / '***********************************************************' / '***********************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  ERROR:",Global.getERROR_NR(),"LINE:",Global.getERROR_LINE(),NEWLINE,"***  LAST RECORD READ:",NEWLINE,"***         COMBINE #:",
            ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(),NEWLINE,"***********************************************************",
            NEWLINE,"***********************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
}
