/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:24 PM
**        * FROM NATURAL PROGRAM : Fcpp815
************************************************************
**        * FILE NAME            : Fcpp815.java
**        * CLASS NAME           : Fcpp815
**        * INSTANCE NAME        : Fcpp815
************************************************************
************************************************************************
* PROGRAM  : FCPP815
*
* SYSTEM   : CPS
* TITLE    : IA NON-CHECK FILE - ATTACH A REFERENECE NUMBER
* DATE     : JAN 1996
* AUTHOR   : LEON SILBERSTEIN
*
* FUNCTION : THIS PROGRAM ASSIGNS A SEQUENTIAL NUMBER TO THE INCOMING
*            PAYMENT RECORDS, AND A UNIQUE REFERENCE NUMBER TO:
*            EFT PAYMENTS AND  INTERNAL ROLLOVERS (IRO's)
*            THE UNIGUE SECURITY NUMBER FOR GLOBAL PAYMENT, IS ASSIGNED
*            BY THE GLOBAL STATEMENT PROCESS.
*
* HISTORY  : LCW - 04/28/08- RE-STOWED FOR FCPA800 ROTH-MAJOR1 CHANGES.
*          : TMM - 2002-04 - GLOBAL PAYMENT FIX
*          : TMM - THE PYMNT-CHECK-SCRTY-NBRMUST BE USED INSTEAD
*          :OF ADDING ONE TO THE CHECK NUMBER IN ORDER FOR THE NUMBER
*          :TO STAY IN SYNC WITH THE GLOBAL PAYMENT PROCESS TO CITIBANK
*          :PROGRAM FCPP831 WILL USE PYMNT-CHECK-SCRTY-NBR IN ITS TRANS
*          :MISSIONS TO THE BANK RESULTING IN A MISMATCH WITH THE BANK
*          : SEE - STREAM P2235CPM - MODULE FCPP831
*          : RCC - RESTOW DUE TO NEW FCPA800
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 05/16/2017 : SAI K     - RECOMPILE DUE TO CHANGES TO FCPA800
************************************************************************
* NOTES, ETC:
*
*   THE INPUT FILE IS EXPECTED TO:
*     *. ALL RECORDS SHOULD HAVE CNTRCT-ORGN-CDE = 'AP'
*     *. ALL RECORDS SHOULD BE PROCESSED ON THE SAME DATE, AND THUS
*        THE SAME VALUE IN CNTRCT-INVRSE-DTE
*   THE INPUT FILE IS SORTED, IN ASCENDING ORDER, AS FOLLOWING:
*     *. CNTRCT-ORGN-CDE        (A2)
*     *. CNTRCT-INVRSE-DTE      (N8)
*     *. PYMNT-PAY-TYPE-REQ-IND (N1)
*     *. PYMNT-PRCSS-SEQ-NBR    (N9)
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp815 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpaacum pdaFcpaacum;
    private PdaFcpacrpt pdaFcpacrpt;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cnt_Rec_Out;
    private DbsField pnd_Cnt_Rec_In;
    private DbsField pnd_Ix1;

    private DbsGroup pnd_Cntrl;
    private DbsField pnd_Cntrl_Pnd_Cnt_Pymnts;
    private DbsField pnd_Cntrl_Pnd_Amt_Pymnts;
    private DbsField pnd_Cntrl_Pnd_Cnt_Rcrds;
    private DbsField pnd_Cntrl_Pnd_Amt_Rcrds;
    private DbsField pnd_Eft_Refpnd;
    private DbsField pnd_Glb_Refpnd;
    private DbsField pnd_Iro_Refpnd;
    private DbsField pnd_Pymnt_Check_Seq_Nbr;

    private DbsGroup pnd_Old;
    private DbsField pnd_Old_Pymnt_Pay_Type_Req_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpaacum = new PdaFcpaacum(localVariables);
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);

        // Local Variables
        pnd_Cnt_Rec_Out = localVariables.newFieldInRecord("pnd_Cnt_Rec_Out", "#CNT-REC-OUT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Rec_In = localVariables.newFieldInRecord("pnd_Cnt_Rec_In", "#CNT-REC-IN", FieldType.PACKED_DECIMAL, 7);
        pnd_Ix1 = localVariables.newFieldInRecord("pnd_Ix1", "#IX1", FieldType.PACKED_DECIMAL, 3);

        pnd_Cntrl = localVariables.newGroupInRecord("pnd_Cntrl", "#CNTRL");
        pnd_Cntrl_Pnd_Cnt_Pymnts = pnd_Cntrl.newFieldArrayInGroup("pnd_Cntrl_Pnd_Cnt_Pymnts", "#CNT-PYMNTS", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            5));
        pnd_Cntrl_Pnd_Amt_Pymnts = pnd_Cntrl.newFieldArrayInGroup("pnd_Cntrl_Pnd_Amt_Pymnts", "#AMT-PYMNTS", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Cntrl_Pnd_Cnt_Rcrds = pnd_Cntrl.newFieldArrayInGroup("pnd_Cntrl_Pnd_Cnt_Rcrds", "#CNT-RCRDS", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            5));
        pnd_Cntrl_Pnd_Amt_Rcrds = pnd_Cntrl.newFieldArrayInGroup("pnd_Cntrl_Pnd_Amt_Rcrds", "#AMT-RCRDS", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Eft_Refpnd = localVariables.newFieldInRecord("pnd_Eft_Refpnd", "#EFT-REF#", FieldType.NUMERIC, 7);
        pnd_Glb_Refpnd = localVariables.newFieldInRecord("pnd_Glb_Refpnd", "#GLB-REF#", FieldType.NUMERIC, 7);
        pnd_Iro_Refpnd = localVariables.newFieldInRecord("pnd_Iro_Refpnd", "#IRO-REF#", FieldType.NUMERIC, 7);
        pnd_Pymnt_Check_Seq_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Check_Seq_Nbr", "#PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);

        pnd_Old = localVariables.newGroupInRecord("pnd_Old", "#OLD");
        pnd_Old_Pymnt_Pay_Type_Req_Ind = pnd_Old.newFieldInGroup("pnd_Old_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp815() throws Exception
    {
        super("Fcpp815");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                          //Natural: FORMAT LS = 132 PS = 60;//Natural: FORMAT ( 15 ) LS = 132 PS = 60;//Natural: ASSIGN #PROGRAM := *PROGRAM
        //*  ..... STARTUP .......
        //*  ... SET WHICH FIELDS TO CONTROL
        //*  PYMNT CNT
        //*  SETTL-AMT
        //*  NET-PYMNT-AMT
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: MOVE TRUE TO #ACCUM-TRUTH-TABLE ( 1 ) #ACCUM-TRUTH-TABLE ( 2 ) #ACCUM-TRUTH-TABLE ( 7 )
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).setValue(true);
        RW1:                                                                                                                                                              //Natural: READ WORK FILE 1 WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            //*  ..... VALIDATE THE FILE AND SORT SEQUENCE
            pnd_Cnt_Rec_In.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #CNT-REC-IN
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde().notEquals("AP")))                                                                             //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE NE 'AP'
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(15, NEWLINE,"***",new TabSetting(25),"INPUT Record has an invalid origin code.",new TabSetting(77),"***",NEWLINE,"***",new             //Natural: WRITE ( 15 ) / '***' 25T 'INPUT Record has an invalid origin code.' 77T '***' / '***' 25T 'Expecting "AP", input:' WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE 77T '***'
                    TabSetting(25),"Expecting 'AP', input:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde(),new TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(42);  if (true) return;                                                                                                                 //Natural: TERMINATE 42
            }                                                                                                                                                             //Natural: END-IF
            //*  A SINGLE DATE
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte().isBreak()))                                                                                 //Natural: IF BREAK OF WF-PYMNT-ADDR-GRP.CNTRCT-INVRSE-DTE
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(15, NEWLINE,"***",new TabSetting(25),"INPUT Data has data for more than",new TabSetting(77),"***",NEWLINE,"***",new                    //Natural: WRITE ( 15 ) / '***' 25T 'INPUT Data has data for more than' 77T '***' / '***' 25T 'One Date.' 77T '***'
                    TabSetting(25),"One Date.",new TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(44);  if (true) return;                                                                                                                 //Natural: TERMINATE 44
            }                                                                                                                                                             //Natural: END-IF
            //* *  IF BREAK OF WF-PYMNT-ADDR-GRP.PYMNT-PRCSS-SEQ-NUM /* NEW PAYMENT
            //* *    IF WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR NE 1 /* VERIFY INSTALLMENT 1
            //* *      PERFORM ERROR-DISPLAY-START
            //* *      WRITE (15)
            //* *        / '***' 25T 'Missing Installment "01" for'           77T '***'
            //* *        / '***' 25T 'Process Sequence Number:'
            //* *        WF-PYMNT-ADDR-GRP.PYMNT-PRCSS-SEQ-NUM                77T '***'
            //* *      PERFORM ERROR-DISPLAY-END
            //* *      TERMINATE 41
            //* *    END-IF
            //* *  END-IF /* IF-BBREAK: NEWPAYMENT
            //*  ........ PROCESS THE RECORD
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().equals(1)))                                                                                  //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR EQ 1
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-A-PYMNT
                sub_Process_A_Pymnt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RW1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(true);                                                                                           //Natural: ASSIGN #NEW-PYMNT-IND := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(false);                                                                                          //Natural: ASSIGN #NEW-PYMNT-IND := FALSE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cntrl_Pnd_Cnt_Rcrds.getValue(pnd_Ix1).nadd(1);                                                                                                            //Natural: ADD 1 TO #CNT-RCRDS ( #IX1 )
            pnd_Cntrl_Pnd_Amt_Rcrds.getValue(pnd_Ix1).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt());                                                            //Natural: ADD WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT TO #AMT-RCRDS ( #IX1 )
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
            sub_Accum_Control_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  .... UPDATE THE FILE WITH SEQUENCE AND REFERENCE NUMBERS
            //*  .... UPDATE WITH SEQ NUMBER
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Seq_Nbr().setValue(pnd_Pymnt_Check_Seq_Nbr);                                                                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SEQ-NBR := #PYMNT-CHECK-SEQ-NBR
            //*  .... UPDATE WITH REFERENCE NUMBER FOR THE TYPE-TYPE
            short decideConditionsMet590 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #IX1;//Natural: VALUE 1
            if (condition((pnd_Ix1.equals(1))))
            {
                decideConditionsMet590++;
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr().setValue(pnd_Eft_Refpnd);                                                                         //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR := #EFT-REF#
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ix1.equals(2))))
            {
                decideConditionsMet590++;
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr().setValue(pnd_Glb_Refpnd);                                                                         //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR := #GLB-REF#
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Ix1.equals(3))))
            {
                decideConditionsMet590++;
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr().setValue(pnd_Iro_Refpnd);                                                                         //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR := #IRO-REF#
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  ..... MARK THE RECORD AS "Processed" ("P")
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Stats_Cde().setValue("P");                                                                                              //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-STATS-CDE := 'P'
            //*  ..... WRITE OUT THE UPDATED NON-CHECK FILE
            getWorkFiles().write(2, true, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",                   //Natural: WRITE WORK FILE 2 VARIABLE WF-PYMNT-ADDR-GRP.PYMNT-ADDR-INFO WF-PYMNT-ADDR-GRP.INV-INFO ( 1:INV-ACCT-COUNT )
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
            pnd_Cnt_Rec_Out.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNT-REC-OUT
            //*  (RW1.)
        }                                                                                                                                                                 //Natural: END-WORK
        RW1_Exit:
        if (Global.isEscape()) return;
        //*  ...... AT END OF FILE
        //*  ..... WRITE OUT CONTROL COUNTS, AMOUNTS, NUMBERS, ETC:
        getReports().write(0, Global.getPROGRAM(),Global.getTIMX(),NEWLINE,"Records Read  from input  work file:",pnd_Cnt_Rec_In,NEWLINE,"Records written on output work file:",pnd_Cnt_Rec_Out,NEWLINE,NEWLINE,"Number of Payments and Amounts on file:",NEWLINE,NEWLINE,"EFT:",new  //Natural: WRITE *PROGRAM *TIMX / 'Records Read  from input  work file:' #CNT-REC-IN / 'Records written on output work file:' #CNT-REC-OUT // 'Number of Payments and Amounts on file:'/ / 'EFT:' 20T 'Payments:' #CNTRL.#CNT-PYMNTS ( 1 ) ( EM = Z,ZZZ,ZZ9 ) 40T 'Amount:' #CNTRL.#AMT-PYMNTS ( 1 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) / 'GLOBAL:' 20T 'Payments:' #CNTRL.#CNT-PYMNTS ( 2 ) ( EM = Z,ZZZ,ZZ9 ) 40T 'Amount:' #CNTRL.#AMT-PYMNTS ( 2 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) / 'Internal ROllOver:' 20T 'Payments:' #CNTRL.#CNT-PYMNTS ( 3 ) ( EM = Z,ZZZ,ZZ9 ) 40T 'Amount:' #CNTRL.#AMT-PYMNTS ( 3 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) / 'Checks:' 20T 'Payments:' #CNTRL.#CNT-PYMNTS ( 5 ) ( EM = Z,ZZZ,ZZ9 ) 40T 'Amount:' #CNTRL.#AMT-PYMNTS ( 5 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) / 'OTHERS:' 20T 'Payments:' #CNTRL.#CNT-PYMNTS ( 4 ) ( EM = Z,ZZZ,ZZ9 ) 40T 'Amount:' #CNTRL.#AMT-PYMNTS ( 4 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) // 'Number of Installments and Amounts on file:'/ / 'EFT:' 20T 'Installments:' #CNTRL.#CNT-RCRDS ( 1 ) ( EM = Z,ZZZ,ZZ9 ) 50T 'Amount:' #CNTRL.#AMT-RCRDS ( 1 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) / 'GLOBAL:' 20T 'Installments:' #CNTRL.#CNT-RCRDS ( 2 ) ( EM = Z,ZZZ,ZZ9 ) 50T 'Amount:' #CNTRL.#AMT-RCRDS ( 2 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) / 'Internal ROllOver:' 20T 'Installments:' #CNTRL.#CNT-RCRDS ( 3 ) ( EM = Z,ZZZ,ZZ9 ) 50T 'Amount:' #CNTRL.#AMT-RCRDS ( 3 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) / 'CHECKS:' 20T 'Installments:' #CNTRL.#CNT-RCRDS ( 5 ) ( EM = Z,ZZZ,ZZ9 ) 50T 'Amount:' #CNTRL.#AMT-RCRDS ( 5 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) / 'OTHERS:' 20T 'Installments:' #CNTRL.#CNT-RCRDS ( 4 ) ( EM = Z,ZZZ,ZZ9 ) 50T 'Amount:' #CNTRL.#AMT-RCRDS ( 4 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) // 'Reference Numbers:'// / 'The Highest Reference Number for EFT:' 60T #EFT-REF# / 'The Highest Reference Number for Global:' 60T #GLB-REF# / 'The Highest Reference Number for Internal RollOver:' 60T #IRO-REF# //'The Highest Payment Check Sequence Number:' 60T #PYMNT-CHECK-SEQ-NBR
            TabSetting(20),"Payments:",pnd_Cntrl_Pnd_Cnt_Pymnts.getValue(1), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(40),"Amount:",pnd_Cntrl_Pnd_Amt_Pymnts.getValue(1), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"GLOBAL:",new TabSetting(20),"Payments:",pnd_Cntrl_Pnd_Cnt_Pymnts.getValue(2), new ReportEditMask 
            ("Z,ZZZ,ZZ9"),new TabSetting(40),"Amount:",pnd_Cntrl_Pnd_Amt_Pymnts.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"Internal ROllOver:",new 
            TabSetting(20),"Payments:",pnd_Cntrl_Pnd_Cnt_Pymnts.getValue(3), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(40),"Amount:",pnd_Cntrl_Pnd_Amt_Pymnts.getValue(3), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"Checks:",new TabSetting(20),"Payments:",pnd_Cntrl_Pnd_Cnt_Pymnts.getValue(5), new ReportEditMask 
            ("Z,ZZZ,ZZ9"),new TabSetting(40),"Amount:",pnd_Cntrl_Pnd_Amt_Pymnts.getValue(5), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"OTHERS:",new 
            TabSetting(20),"Payments:",pnd_Cntrl_Pnd_Cnt_Pymnts.getValue(4), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(40),"Amount:",pnd_Cntrl_Pnd_Amt_Pymnts.getValue(4), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,"Number of Installments and Amounts on file:",NEWLINE,NEWLINE,"EFT:",new TabSetting(20),"Installments:",pnd_Cntrl_Pnd_Cnt_Rcrds.getValue(1), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(50),"Amount:",pnd_Cntrl_Pnd_Amt_Rcrds.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"GLOBAL:",new 
            TabSetting(20),"Installments:",pnd_Cntrl_Pnd_Cnt_Rcrds.getValue(2), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(50),"Amount:",pnd_Cntrl_Pnd_Amt_Rcrds.getValue(2), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"Internal ROllOver:",new TabSetting(20),"Installments:",pnd_Cntrl_Pnd_Cnt_Rcrds.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(50),"Amount:",pnd_Cntrl_Pnd_Amt_Rcrds.getValue(3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"CHECKS:",new 
            TabSetting(20),"Installments:",pnd_Cntrl_Pnd_Cnt_Rcrds.getValue(5), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(50),"Amount:",pnd_Cntrl_Pnd_Amt_Rcrds.getValue(5), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,"OTHERS:",new TabSetting(20),"Installments:",pnd_Cntrl_Pnd_Cnt_Rcrds.getValue(4), new ReportEditMask 
            ("Z,ZZZ,ZZ9"),new TabSetting(50),"Amount:",pnd_Cntrl_Pnd_Amt_Rcrds.getValue(4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,"Reference Numbers:",NEWLINE,NEWLINE,NEWLINE,"The Highest Reference Number for EFT:",new 
            TabSetting(60),pnd_Eft_Refpnd,NEWLINE,"The Highest Reference Number for Global:",new TabSetting(60),pnd_Glb_Refpnd,NEWLINE,"The Highest Reference Number for Internal RollOver:",new 
            TabSetting(60),pnd_Iro_Refpnd,NEWLINE,NEWLINE,"The Highest Payment Check Sequence Number:",new TabSetting(60),pnd_Pymnt_Check_Seq_Nbr);
        if (Global.isEscape()) return;
        //* *
        //* *
        //* *
        //* *
        //* *
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(2).setValue(true);                                                                                         //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 2 ) := TRUE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(3).setValue(true);                                                                                         //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 3 ) := TRUE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(4).setValue(true);                                                                                         //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 4 ) := TRUE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(false);                                                                                                       //Natural: ASSIGN #FCPACRPT.#NO-ABEND := FALSE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("   ANNUITY PAYMENTS NON-CHECK UPDATE");                                                                         //Natural: ASSIGN #FCPACRPT.#TITLE := '   ANNUITY PAYMENTS NON-CHECK UPDATE'
        DbsUtil.callnat(Fcpncrpt.class , getCurrentProcessState(), pdaFcpaacum.getPnd_Fcpaacum(), pdaFcpacrpt.getPnd_Fcpacrpt());                                         //Natural: CALLNAT 'FCPNCRPT' USING #FCPAACUM #FCPACRPT
        if (condition(Global.isEscape())) return;
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,Global.getPROGRAM(),Global.getTIME(),"END OF  PROGRAM RUN");                                                        //Natural: WRITE ( 0 ) /// *PROGRAM *TIME 'END OF  PROGRAM RUN'
        if (Global.isEscape()) return;
        //* *
        //* *
        //* ****************   S U B R O U T I N E S   ***************
        //*  =====================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-A-PYMNT
        //* *  ADD 1 TO #GLB-REF#
        //*  =====================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //*  =====================================================================
        //*  =====================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  =====================================================================
        //* ***********************************************************************
        //*  COPYCODE   : FCPCACUM
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : "AP" - CONTROL ACCUMULATIONS.
        //* ***********************************************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-TOTALS
    }
    private void sub_Process_A_Pymnt() throws Exception                                                                                                                   //Natural: PROCESS-A-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        //*  =====================================================================
        //*  .... IF CHANGE IN PAYTYPE - DETERMINE INDEX
        //*  NEWPAYTYPE
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().notEquals(pnd_Old_Pymnt_Pay_Type_Req_Ind)))                                                //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND NE #OLD.PYMNT-PAY-TYPE-REQ-IND
        {
            pnd_Old_Pymnt_Pay_Type_Req_Ind.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind());                                                            //Natural: ASSIGN #OLD.PYMNT-PAY-TYPE-REQ-IND := WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND
            //*  CHK
            //*  EFT
            //* TMM-2001/11
            //*  GLOBALS
            //*  IRO (INTERNAL ROLLOVER)
            //*  OTHERS
            short decideConditionsMet665 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet665++;
                pnd_Ix1.setValue(5);                                                                                                                                      //Natural: ASSIGN #IX1 := 5
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(1);                                                                                                //Natural: ASSIGN #ACCUM-OCCUR := 1
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet665++;
                pnd_Ix1.setValue(1);                                                                                                                                      //Natural: ASSIGN #IX1 := 1
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(2);                                                                                                //Natural: ASSIGN #ACCUM-OCCUR := 2
            }                                                                                                                                                             //Natural: VALUE 3,4,9
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(4) 
                || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(9))))
            {
                decideConditionsMet665++;
                pnd_Ix1.setValue(2);                                                                                                                                      //Natural: ASSIGN #IX1 := 2
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(3);                                                                                                //Natural: ASSIGN #ACCUM-OCCUR := 3
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(8))))
            {
                decideConditionsMet665++;
                pnd_Ix1.setValue(3);                                                                                                                                      //Natural: ASSIGN #IX1 := 3
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(4);                                                                                                //Natural: ASSIGN #ACCUM-OCCUR := 4
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Ix1.setValue(4);                                                                                                                                      //Natural: ASSIGN #IX1 := 4
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... ACCUM FOR CONTROLS
        pnd_Cntrl_Pnd_Cnt_Pymnts.getValue(pnd_Ix1).nadd(1);                                                                                                               //Natural: ADD 1 TO #CNT-PYMNTS ( #IX1 )
        pnd_Cntrl_Pnd_Amt_Pymnts.getValue(pnd_Ix1).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt());                                                               //Natural: ADD WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT TO #AMT-PYMNTS ( #IX1 )
        //*  .... DETERMINE SEQ NUMBER
        pnd_Pymnt_Check_Seq_Nbr.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #PYMNT-CHECK-SEQ-NBR
        //*  .... DETERMINE REFERENCE NUMBER FOR THE TYPE-TYPE
        //*         FOR GLOBAL USE THE SECURITY NUMBER FROM THE RECORD
        short decideConditionsMet689 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #IX1;//Natural: VALUE 1
        if (condition((pnd_Ix1.equals(1))))
        {
            decideConditionsMet689++;
            //*  TMM
            pnd_Eft_Refpnd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #EFT-REF#
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Ix1.equals(2))))
        {
            decideConditionsMet689++;
            pnd_Glb_Refpnd.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr());                                                                             //Natural: ASSIGN #GLB-REF# := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Ix1.equals(3))))
        {
            decideConditionsMet689++;
            pnd_Iro_Refpnd.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #IRO-REF#
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  PROCESS-A-PYMNT
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(15));                                                                                                                //Natural: NEWPAGE ( 15 )
        if (condition(Global.isEscape())){return;}
        getReports().write(15, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new //Natural: WRITE ( 15 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(15, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new         //Natural: WRITE ( 15 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Control_Totals() throws Exception                                                                                                              //Natural: ACCUM-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  #MAX-OCCUR
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().greaterOrEqual(1) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().lessOrEqual(50)))                  //Natural: IF #ACCUM-OCCUR = 1 THRU 50
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count());                                                      //Natural: ASSIGN #@@MAX-FUND := WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT
        short decideConditionsMet718 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) AND #FCPAACUM.#NEW-PYMNT-IND
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).equals(true) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().getBoolean()))
        {
            decideConditionsMet718++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                  //Natural: ADD 1 TO #FCPAACUM.#PYMNT-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).equals(true)))
        {
            decideConditionsMet718++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#SETTL-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 3 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(3).equals(true)))
        {
            decideConditionsMet718++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Cntrct_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#CNTRCT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 4 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(4).equals(true)))
        {
            decideConditionsMet718++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dvdnd_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DVDND-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 5 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(5).equals(true)))
        {
            decideConditionsMet718++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dci_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DCI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DCI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 6 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(6).equals(true)))
        {
            decideConditionsMet718++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dpi_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DPI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DPI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).equals(true)))
        {
            decideConditionsMet718++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#NET-PYMNT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 8 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(8).equals(true)))
        {
            decideConditionsMet718++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Fdrl_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#FDRL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 9 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(9).equals(true)))
        {
            decideConditionsMet718++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_State_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#STATE-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 10 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(10).equals(true)))
        {
            decideConditionsMet718++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Local_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#LOCAL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet718 > 0))
        {
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                    //Natural: ADD 1 TO #FCPAACUM.#REC-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet718 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");
        Global.format(15, "LS=132 PS=60");
    }
}
