/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:08 PM
**        * FROM NATURAL PROGRAM : Fcpp876
************************************************************
**        * FILE NAME            : Fcpp876.java
**        * CLASS NAME           : Fcpp876
**        * INSTANCE NAME        : Fcpp876
************************************************************
************************************************************************
* PROGRAM  : FCPP876
* SYSTEM   : CPS
* TITLE    : CPS DAILY CYCLES CHECK PRINT
* FUNCTION : CREATE "next check" FILE.
* HISTORY  : 02/26/01  RCC  - CHANGE ERROR MESSAGE WHEN SEQ NUMBER NE 1
*          : 01/05/2006   R. LANDRUM - POSITIVE PAY/PAYEE MATCH
*            INPUT CHECK & SEQ NBRS NOW RETRIEVED FROM REFERENCE TABLE.
*************************** NOTE !!! **********************************
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTETERNALLY.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp876 extends BLNatBase
{
    // Data Areas
    private LdaFcpl876 ldaFcpl876;
    private LdaFcpl876a ldaFcpl876a;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Parm;
    private DbsField pnd_Input_Parm_Pnd_Input_Parm_Check_Prefix;
    private DbsField pnd_Input_Parm_Pnd_Input_Parm_Check_Nbr;
    private DbsField pnd_Input_Parm_Pnd_Input_Parm_Filler;
    private DbsField pnd_Input_Parm_Pnd_Input_Parm_Check_Seq_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl876 = new LdaFcpl876();
        registerRecord(ldaFcpl876);
        ldaFcpl876a = new LdaFcpl876a();
        registerRecord(ldaFcpl876a);
        localVariables = new DbsRecord();
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables

        pnd_Input_Parm = localVariables.newGroupInRecord("pnd_Input_Parm", "#INPUT-PARM");
        pnd_Input_Parm_Pnd_Input_Parm_Check_Prefix = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Parm_Check_Prefix", "#INPUT-PARM-CHECK-PREFIX", 
            FieldType.NUMERIC, 3);
        pnd_Input_Parm_Pnd_Input_Parm_Check_Nbr = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Parm_Check_Nbr", "#INPUT-PARM-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Input_Parm_Pnd_Input_Parm_Filler = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Parm_Filler", "#INPUT-PARM-FILLER", FieldType.STRING, 
            1);
        pnd_Input_Parm_Pnd_Input_Parm_Check_Seq_Nbr = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Parm_Check_Seq_Nbr", "#INPUT-PARM-CHECK-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl876.initializeValues();
        ldaFcpl876a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp876() throws Exception
    {
        super("Fcpp876");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 80 ZP = ON
        //*  MAIN PROGRAM *
        //* ***************
        //* *************** START RLANDRUM POS-PAY PAYEE MATCH ********************
        //*  NEXT CHECK & SEQ NBR NOW RETRIEVED FROM REF TBL FOR POSPAY/PAYEE MATCH
        //* *READ WORK FILE 2 ONCE #INPUT-PARM /* RL-NEXT CHECK & SEQ NBR RETRIEVED
        //* *AT  END OF FILE
        //* *  PERFORM ERROR-DISPLAY-START
        //* *  WRITE '***' 25T 'MISSING INPUT PARAMETER'            77T '***'
        //* *  PERFORM ERROR-DISPLAY-END
        //* *  TERMINATE 50
        //* *END-ENDFILE
        //*  RL
                                                                                                                                                                          //Natural: PERFORM GET-START-CHECK-NO
        sub_Get_Start_Check_No();
        if (condition(Global.isEscape())) {return;}
        //* ************* RL COMMENTED OUT FOR PAYEE MATCH (NORESTART)*************
        //* *IF #INPUT-PARM-CHECK-SEQ-NBR NE 1
        //* *  PERFORM ERROR-DISPLAY-START
        //* *  WRITE
        //* *    '***' 6T 'PARAMETERS RECEIVED :'                      77T '***' /
        //* *    '***' 6T '   NEXT CHECK NUMBER     :'
        //* *    #INPUT-PARM-CHECK-NBR(EM=9999999)          77T '***' /
        //* *    '***' 6T '   NEXT SEQUENCE NYMBER  :'
        //* *    #INPUT-PARM-CHECK-SEQ-NBR(EM=9999999)   77T '***' /
        //* *    '***' 77T     '***' /
        //* *    '***' 6T 'CANNOT USE THIS PROCEDURE FOR A RESTART.'   77T '***' /
        //* *    '***' 77T     '***' /
        //* *    '***' 6T 'CHECK SEQUENCE NUMBER MUST START FROM "1"'    -
        //* *    'FOR A RERUN'                                77T '***' /
        //* *    '***' 6T 'OR'                                         77T '***' /
        //* *    '***' 6T 'USE PROCEDURE P1415CPR TO RESTART FROM CHECK ' -
        //* *    'SEQUENCE NO. "'
        //* *    #INPUT-PARM-CHECK-SEQ-NBR(EM=9999999) '"' 77T '***' /
        //* *    '***' 77T     '***' /
        //* *    '***' 6T 'NOTIFY SYSTEM SUPPORT IF THE ERROR PERSISTS.'77T '***' /
        //* *    '***' 77T     '***' /
        //* *    '***' 77T     '***' /
        //* *    '***' 77T     '***' /
        //* *    '***' 77T     '***' /
        //* *    '***' '!'(71) '***' /
        //* *    '*'(79)
        //*   PERFORM ERROR-DISPLAY-END
        //* *  TERMINATE 51
        //* *END-IF
        //* ***************** END RLANDRUM POS-PAY PAYEE MATCH ********************
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().setValue(pnd_Input_Parm_Pnd_Input_Parm_Check_Seq_Nbr);                                                            //Natural: ASSIGN #FCPL876.PYMNT-CHECK-SEQ-NBR := #INPUT-PARM-CHECK-SEQ-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().setValue(pnd_Input_Parm_Pnd_Input_Parm_Check_Nbr);                                                                    //Natural: ASSIGN #FCPL876.PYMNT-CHECK-NBR := #INPUT-PARM-CHECK-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr().setValue(1);                                                                                                    //Natural: ASSIGN #FCPL876.PYMNT-CHECK-SCRTY-NBR := 1
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Last_Rollover_Nbr().setValue(1);                                                                                                  //Natural: ASSIGN #FCPL876.PYMNT-LAST-ROLLOVER-NBR := 1
        ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().setValue(true);                                                                                                       //Natural: ASSIGN #FCPL876.#BAR-NEW-RUN := TRUE
        getWorkFiles().write(1, false, ldaFcpl876.getPnd_Fcpl876());                                                                                                      //Natural: WRITE WORK FILE 1 #FCPL876
        ldaFcpl876a.getPnd_Fcpl876a().setValue("!@#SUP02#@!SEPARATOR");                                                                                                   //Natural: ASSIGN #FCPL876A := '!@#SUP02#@!SEPARATOR'
        getWorkFiles().write(8, false, ldaFcpl876a.getPnd_Fcpl876a());                                                                                                    //Natural: WRITE WORK FILE 8 #FCPL876A
        //* *************** START RLANDRUM POS-PAY PAYEE MATCH ********************
        //*  --------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-START-CHECK-NO
        //* ***************** END RLANDRUM POS-PAY PAYEE MATCH ********************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Get_Start_Check_No() throws Exception                                                                                                                //Natural: GET-START-CHECK-NO
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------- *
        //*  GET STARTING CHECK & SEQ NBRS FROM REF TABLE FOR POS-PAY/PAYEE MATCH
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        //*  RL TESTING
        getReports().write(0, "1050",Global.getPROGRAM(),"=",pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),NEWLINE,"=",pdaFcpa110.getFcpa110_Fcpa110_Source_Code(),NEWLINE,"=",pdaFcpa110.getFcpa110_Start_Seq_No(),NEWLINE,"=",pdaFcpa110.getFcpa110_Start_Check_Prefix_N3(),NEWLINE,"=",pdaFcpa110.getFcpa110_Start_Check_No_N7(),NEWLINE,"=",pdaFcpa110.getFcpa110_Bank_Transmission_Cde(),NEWLINE,"=",pdaFcpa110.getFcpa110_Account_No(),NEWLINE,"=",pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(),  //Natural: WRITE '1050' *PROGRAM '=' FCPA110.FCPA110-RETURN-CODE / '=' FCPA110.FCPA110-SOURCE-CODE / '=' FCPA110.START-SEQ-NO / '=' FCPA110.START-CHECK-PREFIX-N3 / '=' FCPA110.START-CHECK-NO-N7 / '=' FCPA110.BANK-TRANSMISSION-CDE / '=' FCPA110.ACCOUNT-NO / '=' FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) /
            new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),NEWLINE);
        if (Global.isEscape()) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().greater("0000")))                                                                                       //Natural: IF FCPA110.FCPA110-RETURN-CODE GT '0000'
        {
            getReports().write(0, "=",pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),"0000");                                                                                //Natural: WRITE '=' FCPA110.FCPA110-RETURN-CODE '0000'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Input_Parm_Pnd_Input_Parm_Check_Seq_Nbr.setValue(pdaFcpa110.getFcpa110_Start_Seq_No());                                                                       //Natural: ASSIGN #INPUT-PARM-CHECK-SEQ-NBR := FCPA110.START-SEQ-NO
        pnd_Input_Parm_Pnd_Input_Parm_Check_Nbr.setValue(pdaFcpa110.getFcpa110_Start_Check_No_N7());                                                                      //Natural: ASSIGN #INPUT-PARM-CHECK-NBR := FCPA110.START-CHECK-NO-N7
        pnd_Input_Parm_Pnd_Input_Parm_Check_Prefix.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                               //Natural: ASSIGN #INPUT-PARM-CHECK-PREFIX := FCPA110.START-CHECK-PREFIX-N3
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=80 ZP=ON");
    }
}
