/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:38 PM
**        * FROM NATURAL PROGRAM : Fcpp255
************************************************************
**        * FILE NAME            : Fcpp255.java
**        * CLASS NAME           : Fcpp255
**        * INSTANCE NAME        : Fcpp255
************************************************************
************************************************************************
* PROGRAM  : FCPP255
* SYSTEM   : CPS
* AUTHOR   : ALTHEA A. YOUNG
* DATE     : SEPT. 28, 1998
* FUNCTION : THIS PROGRAM EXTRACTS FROM THE PAYMENT FILE ON THE CPS
*            DATABASE, ALL THE STOP PAYMENTS, THAT HAVE NOT BEEN
*            COMMUNICATED TO THE BANK.  THIS FORMAT WILL BE USED AS OF
*            NOVEMBER 9, 1998.
*
*            FOR FIRST UNION BANK ONLY!!!
*
* *********************************************************************
* PROGRAM DESCRIPTION:
* *********************************************************************
*
*
* THIS PROGRAM EXTRACTS FROM THE PAYMENT FILE ON THE CPS
* DATABASE, ALL THE STOP PAYMENTS, THAT HAVE NOT BEEN
* COMMUNICATED TO THE BANK.
* THE NEWLY STOPPED PAYMENTS ARE WRITTEN TO A WORK FILE AND REPORTED
* ON A HARD-COPY PRINTOUT AS WELL.
* AS RECORDS ARE "downloaded" THE CNR-CS-NEW-STOP-IND IS
* UPDATED FROM WHATEVER VALUE IT IS INTO AN "I".
* WHEN THE "download" PROGRAM IS DONE, THE WORK FILE
* IS CATALOGED INTO MVS. IT IS THEN FOLLOWED BY AN ADDITIONAL
* STEP (IN THE BATCH RUN), WHICH RESETS ALL THE PAYMENT
* RECORDS WHICH ARE MARKED WITH "I", THUS REMOVING THEM
* FROM THE LIST OF STOPS TO BE COMMUNICATED TO THE BANK.
* ----------------------------------------------------------------------
* PROGRAMMED ABENDS:
* ----------------------------------------------------------------------
* CONDITIONCODE 33:
*        A PROGRAMS INTERNAL ERROR OCCURED:
*         THE NUMBER OF STOPS COUNTED BY "stop-ind" DOES NOT EQUAL
*         THE NUMBER OF STOPS COUNTED AS A WHOLE.
* ----------------------------------------------------------------------
*
* RESTART:
*            RESTART PROCEDURE FOR THIS PROGRAM IS RERUN.
*
* STOP-FILE - SEQUENCE OF RECORDS:
*  DETAIL-REC-D   'D' (FOR EACH STOPPED CHECK)
*  TRAILER-REC-T  'T'
*
* HISTORY:
*
* 09/28/98: ALTHEA A. YOUNG - ADAPTED FROM FCPP254.
*
* 05/15/2006 RL PAYEE MATCH - INCREASED CHECK NBR TO N10.
* 05/23/2006 RL BYPASS WORK FILE OF STOPS FOR MAY 23,2006 TO RESOLVE
*               STOPS THAT HAVE BEEN HANDLED MANUALLY.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp255 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Check_Number_N10;

    private DbsGroup pnd_Check_Number_N10__R_Field_1;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N3;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N7;

    private DataAccessProgramView vw_fcp_Cons_Pymnt;
    private DbsField fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Stats_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr;

    private DbsGroup fcp_Cons_Pymnt__R_Field_2;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr_N3;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr_N7;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Amt;
    private DbsField fcp_Cons_Pymnt_Pymnt_Acctg_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Intrfce_Dte;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Rqust_Term_Id;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Actvty_Cde;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Intrfce_Dte;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Check_Dte;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind;
    private DbsField pnd_Force_Et_Count;
    private DbsField pnd_Stop_Isn;
    private DbsField pnd_Check_Datex;

    private DbsGroup pnd_Check_Datex__R_Field_3;
    private DbsField pnd_Check_Datex_Pnd_Check_Date;
    private DbsField pnd_Program;
    private DbsField pnd_Abend_Cde;
    private DbsField pnd_Time_Hhmmsst;

    private DbsGroup pnd_Time_Hhmmsst__R_Field_4;
    private DbsField pnd_Time_Hhmmsst_Pnd_Time_Hhmmss;
    private DbsField pnd_Rpt_Date;
    private DbsField pnd_Rpt_Time;
    private DbsField pnd_Run_Date_N;
    private DbsField pnd_Cnr_Cs_Rqust_Tme;

    private DbsGroup pnd_Wsarea;
    private DbsField pnd_Wsarea_Pnd_Page_Number_Rpt1;
    private DbsField pnd_Wsarea_Pnd_Et_Cnt;
    private DbsField pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Y;
    private DbsField pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_I;
    private DbsField pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Other;
    private DbsField pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_All;

    private DbsGroup detail_Rec_D;
    private DbsField detail_Rec_D_Customer_Id_Num_D;
    private DbsField detail_Rec_D_Record_Type;
    private DbsField detail_Rec_D_Bank_Num;
    private DbsField detail_Rec_D_Account_Num;
    private DbsField detail_Rec_D_Run_Date;
    private DbsField detail_Rec_D_Run_Time;
    private DbsField detail_Rec_D_Input_Type;
    private DbsField detail_Rec_D_Stop_Type;
    private DbsField detail_Rec_D_Beginning_Check_Num;
    private DbsField detail_Rec_D_Ending_Check_Num;
    private DbsField detail_Rec_D_Check_Amt;

    private DbsGroup detail_Rec_D__R_Field_5;
    private DbsField detail_Rec_D_Check_Amt_Decimal;
    private DbsField detail_Rec_D_Check_Issue_Date_N;

    private DbsGroup detail_Rec_D__R_Field_6;
    private DbsField detail_Rec_D_Check_Issue_Date_A;
    private DbsField detail_Rec_D_Payee_Name;
    private DbsField detail_Rec_D_Reason;
    private DbsField detail_Rec_D_Auto_Check_Paid_Inquiry;
    private DbsField detail_Rec_D_Stop_Duration_Years;
    private DbsField detail_Rec_D_User_Id;

    private DbsGroup trailer_Rec_T;
    private DbsField trailer_Rec_T_Customer_Id_Num_T;
    private DbsField trailer_Rec_T_Record_Type;
    private DbsField trailer_Rec_T_Filler_01;

    private DbsGroup trailer_Rec_T__R_Field_7;
    private DbsField trailer_Rec_T_Filler_01_A;
    private DbsField trailer_Rec_T_Filler_01_B;
    private DbsField trailer_Rec_T_Total_Stops;
    private DbsField trailer_Rec_T_Filler_02;

    private DbsGroup cntlrec;
    private DbsField cntlrec_Cntlrec_Full;

    private DbsGroup cntlrec__R_Field_8;
    private DbsField cntlrec_Create_Date;
    private DbsField cntlrec_Create_Time;
    private DbsField cntlrec_Pnd_Ws_Number_Of_Stopped_Payments;
    private DbsField cntlrec_Pnd_Ws_Stop_Amount;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Check_Number_N10 = localVariables.newFieldInRecord("pnd_Check_Number_N10", "#CHECK-NUMBER-N10", FieldType.NUMERIC, 10);

        pnd_Check_Number_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_1", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_N3 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N3", "#CHECK-NUMBER-N3", 
            FieldType.NUMERIC, 3);
        pnd_Check_Number_N10_Pnd_Check_Number_N7 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N7", "#CHECK-NUMBER-N7", 
            FieldType.NUMERIC, 7);

        vw_fcp_Cons_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnt", "FCP-CONS-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Pymnt_Cntrct_Payee_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Pymnt_Cntrct_Orgn_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Pymnt_Pymnt_Stats_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_STATS_CDE");
        fcp_Cons_Pymnt_Pymnt_Check_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_CHECK_DTE");
        fcp_Cons_Pymnt_Pymnt_Check_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        fcp_Cons_Pymnt_Pymnt_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");

        fcp_Cons_Pymnt__R_Field_2 = vw_fcp_Cons_Pymnt.getRecord().newGroupInGroup("fcp_Cons_Pymnt__R_Field_2", "REDEFINE", fcp_Cons_Pymnt_Pymnt_Nbr);
        fcp_Cons_Pymnt_Pymnt_Nbr_N3 = fcp_Cons_Pymnt__R_Field_2.newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr_N3", "PYMNT-NBR-N3", FieldType.NUMERIC, 3);
        fcp_Cons_Pymnt_Pymnt_Nbr_N7 = fcp_Cons_Pymnt__R_Field_2.newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr_N7", "PYMNT-NBR-N7", FieldType.NUMERIC, 7);
        fcp_Cons_Pymnt_Pymnt_Check_Amt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        fcp_Cons_Pymnt_Pymnt_Acctg_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_ACCTG_DTE");
        fcp_Cons_Pymnt_Pymnt_Intrfce_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_INTRFCE_DTE");
        fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte", "CNR-CS-RQUST-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_CS_RQUST_DTE");
        fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme", "CNR-CS-RQUST-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "CNR_CS_RQUST_TME");
        fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id", "CNR-CS-RQUST-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNR_CS_RQUST_USER_ID");
        fcp_Cons_Pymnt_Cnr_Cs_Rqust_Term_Id = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Rqust_Term_Id", "CNR-CS-RQUST-TERM-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNR_CS_RQUST_TERM_ID");
        fcp_Cons_Pymnt_Cnr_Cs_Actvty_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Actvty_Cde", "CNR-CS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNR_CS_ACTVTY_CDE");
        fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Intrfce_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Intrfce_Dte", "CNR-CS-PYMNT-INTRFCE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_CS_PYMNT_INTRFCE_DTE");
        fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Acctg_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_CS_PYMNT_ACCTG_DTE");
        fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Check_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Check_Dte", "CNR-CS-PYMNT-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_CS_PYMNT_CHECK_DTE");
        fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind", "CNR-CS-NEW-STOP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNR_CS_NEW_STOP_IND");
        registerRecord(vw_fcp_Cons_Pymnt);

        pnd_Force_Et_Count = localVariables.newFieldInRecord("pnd_Force_Et_Count", "#FORCE-ET-COUNT", FieldType.NUMERIC, 5);
        pnd_Stop_Isn = localVariables.newFieldInRecord("pnd_Stop_Isn", "#STOP-ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Check_Datex = localVariables.newFieldInRecord("pnd_Check_Datex", "#CHECK-DATEX", FieldType.STRING, 8);

        pnd_Check_Datex__R_Field_3 = localVariables.newGroupInRecord("pnd_Check_Datex__R_Field_3", "REDEFINE", pnd_Check_Datex);
        pnd_Check_Datex_Pnd_Check_Date = pnd_Check_Datex__R_Field_3.newFieldInGroup("pnd_Check_Datex_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 
            8);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Abend_Cde = localVariables.newFieldInRecord("pnd_Abend_Cde", "#ABEND-CDE", FieldType.NUMERIC, 3);
        pnd_Time_Hhmmsst = localVariables.newFieldInRecord("pnd_Time_Hhmmsst", "#TIME-HHMMSST", FieldType.NUMERIC, 7);

        pnd_Time_Hhmmsst__R_Field_4 = localVariables.newGroupInRecord("pnd_Time_Hhmmsst__R_Field_4", "REDEFINE", pnd_Time_Hhmmsst);
        pnd_Time_Hhmmsst_Pnd_Time_Hhmmss = pnd_Time_Hhmmsst__R_Field_4.newFieldInGroup("pnd_Time_Hhmmsst_Pnd_Time_Hhmmss", "#TIME-HHMMSS", FieldType.NUMERIC, 
            6);
        pnd_Rpt_Date = localVariables.newFieldInRecord("pnd_Rpt_Date", "#RPT-DATE", FieldType.STRING, 8);
        pnd_Rpt_Time = localVariables.newFieldInRecord("pnd_Rpt_Time", "#RPT-TIME", FieldType.STRING, 10);
        pnd_Run_Date_N = localVariables.newFieldInRecord("pnd_Run_Date_N", "#RUN-DATE-N", FieldType.NUMERIC, 8);
        pnd_Cnr_Cs_Rqust_Tme = localVariables.newFieldInRecord("pnd_Cnr_Cs_Rqust_Tme", "#CNR-CS-RQUST-TME", FieldType.STRING, 9);

        pnd_Wsarea = localVariables.newGroupInRecord("pnd_Wsarea", "#WSAREA");
        pnd_Wsarea_Pnd_Page_Number_Rpt1 = pnd_Wsarea.newFieldInGroup("pnd_Wsarea_Pnd_Page_Number_Rpt1", "#PAGE-NUMBER-RPT1", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Wsarea_Pnd_Et_Cnt = pnd_Wsarea.newFieldInGroup("pnd_Wsarea_Pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Y = pnd_Wsarea.newFieldInGroup("pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Y", "#CNT-RECS-MARKED-AS-Y", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_I = pnd_Wsarea.newFieldInGroup("pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_I", "#CNT-RECS-MARKED-AS-I", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Other = pnd_Wsarea.newFieldInGroup("pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Other", "#CNT-RECS-MARKED-AS-OTHER", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_All = pnd_Wsarea.newFieldInGroup("pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_All", "#CNT-RECS-MARKED-AS-ALL", FieldType.PACKED_DECIMAL, 
            5);

        detail_Rec_D = localVariables.newGroupInRecord("detail_Rec_D", "DETAIL-REC-D");
        detail_Rec_D_Customer_Id_Num_D = detail_Rec_D.newFieldInGroup("detail_Rec_D_Customer_Id_Num_D", "CUSTOMER-ID-NUM-D", FieldType.NUMERIC, 8);
        detail_Rec_D_Record_Type = detail_Rec_D.newFieldInGroup("detail_Rec_D_Record_Type", "RECORD-TYPE", FieldType.STRING, 1);
        detail_Rec_D_Bank_Num = detail_Rec_D.newFieldInGroup("detail_Rec_D_Bank_Num", "BANK-NUM", FieldType.NUMERIC, 3);
        detail_Rec_D_Account_Num = detail_Rec_D.newFieldInGroup("detail_Rec_D_Account_Num", "ACCOUNT-NUM", FieldType.NUMERIC, 13);
        detail_Rec_D_Run_Date = detail_Rec_D.newFieldInGroup("detail_Rec_D_Run_Date", "RUN-DATE", FieldType.NUMERIC, 8);
        detail_Rec_D_Run_Time = detail_Rec_D.newFieldInGroup("detail_Rec_D_Run_Time", "RUN-TIME", FieldType.NUMERIC, 6);
        detail_Rec_D_Input_Type = detail_Rec_D.newFieldInGroup("detail_Rec_D_Input_Type", "INPUT-TYPE", FieldType.STRING, 2);
        detail_Rec_D_Stop_Type = detail_Rec_D.newFieldInGroup("detail_Rec_D_Stop_Type", "STOP-TYPE", FieldType.STRING, 1);
        detail_Rec_D_Beginning_Check_Num = detail_Rec_D.newFieldInGroup("detail_Rec_D_Beginning_Check_Num", "BEGINNING-CHECK-NUM", FieldType.NUMERIC, 
            11);
        detail_Rec_D_Ending_Check_Num = detail_Rec_D.newFieldInGroup("detail_Rec_D_Ending_Check_Num", "ENDING-CHECK-NUM", FieldType.NUMERIC, 11);
        detail_Rec_D_Check_Amt = detail_Rec_D.newFieldInGroup("detail_Rec_D_Check_Amt", "CHECK-AMT", FieldType.NUMERIC, 15);

        detail_Rec_D__R_Field_5 = detail_Rec_D.newGroupInGroup("detail_Rec_D__R_Field_5", "REDEFINE", detail_Rec_D_Check_Amt);
        detail_Rec_D_Check_Amt_Decimal = detail_Rec_D__R_Field_5.newFieldInGroup("detail_Rec_D_Check_Amt_Decimal", "CHECK-AMT-DECIMAL", FieldType.NUMERIC, 
            15, 2);
        detail_Rec_D_Check_Issue_Date_N = detail_Rec_D.newFieldInGroup("detail_Rec_D_Check_Issue_Date_N", "CHECK-ISSUE-DATE-N", FieldType.NUMERIC, 8);

        detail_Rec_D__R_Field_6 = detail_Rec_D.newGroupInGroup("detail_Rec_D__R_Field_6", "REDEFINE", detail_Rec_D_Check_Issue_Date_N);
        detail_Rec_D_Check_Issue_Date_A = detail_Rec_D__R_Field_6.newFieldInGroup("detail_Rec_D_Check_Issue_Date_A", "CHECK-ISSUE-DATE-A", FieldType.STRING, 
            8);
        detail_Rec_D_Payee_Name = detail_Rec_D.newFieldInGroup("detail_Rec_D_Payee_Name", "PAYEE-NAME", FieldType.STRING, 10);
        detail_Rec_D_Reason = detail_Rec_D.newFieldInGroup("detail_Rec_D_Reason", "REASON", FieldType.STRING, 10);
        detail_Rec_D_Auto_Check_Paid_Inquiry = detail_Rec_D.newFieldInGroup("detail_Rec_D_Auto_Check_Paid_Inquiry", "AUTO-CHECK-PAID-INQUIRY", FieldType.STRING, 
            1);
        detail_Rec_D_Stop_Duration_Years = detail_Rec_D.newFieldInGroup("detail_Rec_D_Stop_Duration_Years", "STOP-DURATION-YEARS", FieldType.NUMERIC, 
            1);
        detail_Rec_D_User_Id = detail_Rec_D.newFieldInGroup("detail_Rec_D_User_Id", "USER-ID", FieldType.NUMERIC, 6);

        trailer_Rec_T = localVariables.newGroupInRecord("trailer_Rec_T", "TRAILER-REC-T");
        trailer_Rec_T_Customer_Id_Num_T = trailer_Rec_T.newFieldInGroup("trailer_Rec_T_Customer_Id_Num_T", "CUSTOMER-ID-NUM-T", FieldType.NUMERIC, 8);
        trailer_Rec_T_Record_Type = trailer_Rec_T.newFieldInGroup("trailer_Rec_T_Record_Type", "RECORD-TYPE", FieldType.STRING, 1);
        trailer_Rec_T_Filler_01 = trailer_Rec_T.newFieldInGroup("trailer_Rec_T_Filler_01", "FILLER-01", FieldType.STRING, 30);

        trailer_Rec_T__R_Field_7 = trailer_Rec_T.newGroupInGroup("trailer_Rec_T__R_Field_7", "REDEFINE", trailer_Rec_T_Filler_01);
        trailer_Rec_T_Filler_01_A = trailer_Rec_T__R_Field_7.newFieldInGroup("trailer_Rec_T_Filler_01_A", "FILLER-01-A", FieldType.NUMERIC, 15);
        trailer_Rec_T_Filler_01_B = trailer_Rec_T__R_Field_7.newFieldInGroup("trailer_Rec_T_Filler_01_B", "FILLER-01-B", FieldType.NUMERIC, 15);
        trailer_Rec_T_Total_Stops = trailer_Rec_T.newFieldInGroup("trailer_Rec_T_Total_Stops", "TOTAL-STOPS", FieldType.NUMERIC, 7);
        trailer_Rec_T_Filler_02 = trailer_Rec_T.newFieldInGroup("trailer_Rec_T_Filler_02", "FILLER-02", FieldType.STRING, 69);

        cntlrec = localVariables.newGroupInRecord("cntlrec", "CNTLREC");
        cntlrec_Cntlrec_Full = cntlrec.newFieldInGroup("cntlrec_Cntlrec_Full", "CNTLREC-FULL", FieldType.STRING, 80);

        cntlrec__R_Field_8 = localVariables.newGroupInRecord("cntlrec__R_Field_8", "REDEFINE", cntlrec);
        cntlrec_Create_Date = cntlrec__R_Field_8.newFieldInGroup("cntlrec_Create_Date", "CREATE-DATE", FieldType.STRING, 8);
        cntlrec_Create_Time = cntlrec__R_Field_8.newFieldInGroup("cntlrec_Create_Time", "CREATE-TIME", FieldType.STRING, 10);
        cntlrec_Pnd_Ws_Number_Of_Stopped_Payments = cntlrec__R_Field_8.newFieldInGroup("cntlrec_Pnd_Ws_Number_Of_Stopped_Payments", "#WS-NUMBER-OF-STOPPED-PAYMENTS", 
            FieldType.NUMERIC, 7);
        cntlrec_Pnd_Ws_Stop_Amount = cntlrec__R_Field_8.newFieldInGroup("cntlrec_Pnd_Ws_Stop_Amount", "#WS-STOP-AMOUNT", FieldType.NUMERIC, 15, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnt.reset();

        localVariables.reset();
        pnd_Force_Et_Count.setInitialValue(25);
        pnd_Wsarea_Pnd_Et_Cnt.setInitialValue(0);
        detail_Rec_D_Customer_Id_Num_D.setInitialValue(13241);
        detail_Rec_D_Record_Type.setInitialValue("D");
        detail_Rec_D_Bank_Num.setInitialValue(75);
        detail_Rec_D_Account_Num.setInitialValue(-2147483648);
        detail_Rec_D_Run_Date.setInitialValue(0);
        detail_Rec_D_Run_Time.setInitialValue(0);
        detail_Rec_D_Input_Type.setInitialValue("BS");
        detail_Rec_D_Stop_Type.setInitialValue("E");
        detail_Rec_D_Beginning_Check_Num.setInitialValue(0);
        detail_Rec_D_Ending_Check_Num.setInitialValue(0);
        detail_Rec_D_Check_Amt.setInitialValue(0);
        detail_Rec_D_Check_Issue_Date_N.setInitialValue(0);
        detail_Rec_D_Payee_Name.setInitialValue(" ");
        detail_Rec_D_Reason.setInitialValue(" ");
        detail_Rec_D_Auto_Check_Paid_Inquiry.setInitialValue("Y");
        detail_Rec_D_Stop_Duration_Years.setInitialValue(2);
        detail_Rec_D_User_Id.setInitialValue(48087);
        trailer_Rec_T_Customer_Id_Num_T.setInitialValue(13241);
        trailer_Rec_T_Record_Type.setInitialValue("T");
        trailer_Rec_T_Filler_01.setInitialValue("999999999999999999999999999999");
        trailer_Rec_T_Total_Stops.setInitialValue(0);
        trailer_Rec_T_Filler_02.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp255() throws Exception
    {
        super("Fcpp255");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  .............. DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "RPT1");                                                                                                                            //Natural: DEFINE PRINTER ( RPT1 = 1 )
        //*  .............. ENVIRONMENTAL INITIALIZATIONS                                                                                                                 //Natural: FORMAT ( RPT1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            Global.getERROR_TA().setValue("INFP9000");                                                                                                                    //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Rpt_Date.setValue(Global.getDATU());                                                                                                                          //Natural: ASSIGN #RPT-DATE := *DATU
        pnd_Rpt_Time.setValue(Global.getTIME());                                                                                                                          //Natural: ASSIGN #RPT-TIME := *TIME
        pnd_Time_Hhmmsst.setValue(Global.getTIMN());                                                                                                                      //Natural: ASSIGN #TIME-HHMMSST := *TIMN
        pnd_Run_Date_N.setValue(Global.getDATN());                                                                                                                        //Natural: ASSIGN #RUN-DATE-N := *DATN
        detail_Rec_D_Run_Date.setValue(pnd_Run_Date_N);                                                                                                                   //Natural: ASSIGN DETAIL-REC-D.RUN-DATE := #RUN-DATE-N
        detail_Rec_D_Run_Time.setValue(pnd_Time_Hhmmsst_Pnd_Time_Hhmmss);                                                                                                 //Natural: ASSIGN DETAIL-REC-D.RUN-TIME := #TIME-HHMMSS
        cntlrec_Pnd_Ws_Number_Of_Stopped_Payments.setValue(0);                                                                                                            //Natural: ASSIGN #WS-NUMBER-OF-STOPPED-PAYMENTS := 0
        cntlrec_Pnd_Ws_Stop_Amount.setValue(0);                                                                                                                           //Natural: ASSIGN #WS-STOP-AMOUNT := 0
        //*  .................. START-OF-PROGRAM
        //*  ............ SELECT ALL THE NEW STOP AND WRITE TO FILE AND REPORT
                                                                                                                                                                          //Natural: PERFORM SELECT-ALL-NEW-STOPS
        sub_Select_All_New_Stops();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM SELECT-ALL-NEW-STOPS-N10
        sub_Select_All_New_Stops_N10();
        if (condition(Global.isEscape())) {return;}
        //*  ............ CREATE THE TRAILER RECORD FOR THE "STOP FILE"
                                                                                                                                                                          //Natural: PERFORM CREATE-TRAILER-FOR-STOP-FILE
        sub_Create_Trailer_For_Stop_File();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DO-END-OF-PROGRAM
        sub_Do_End_Of_Program();
        if (condition(Global.isEscape())) {return;}
        //*  ............ AT TOP OF PAGE
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( RPT1 )
        //*  =====================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-ALL-NEW-STOPS
        //*  =====================================================================
        //*  SELECT ALL THE NEW STOPS TO THE "STOP FILE" AND REPORT
        //*  ---------------------------------------------------------------------
        //* *
        //* * 'RUN/TM '      RUN-TIME
        //*  ..... UPDATE THE STOP-IND TO THE INTERMIDIATE VALUE "I"
        //*  =============== RL MAY 19, 2006 - INCLUDE 2200 CS ====================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SELECT-ALL-NEW-STOPS-N10
        //*  =====================================================================
        //*  SELECT ALL THE NEW STOPS TO THE "STOP FILE" AND REPORT
        //*  ---------------------------------------------------------------------
        //* *
        //*  ..... UPDATE THE STOP-IND TO THE INTERMIDIATE VALUE "I"
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TRAILER-FOR-STOP-FILE
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DO-END-OF-PROGRAM
        //*  ======================================================================
        //*  .... WRITE A CONTROL RECORD TO WORKFILE 2 TO BE MATCHED BY THE
        //*  .... PROGRAM WHICH RESETS THE STOP-IND,
    }
    //*  RL N7 CHECK-NBR & CS IND
    private void sub_Select_All_New_Stops() throws Exception                                                                                                              //Natural: SELECT-ALL-NEW-STOPS
    {
        if (BLNatReinput.isReinput()) return;

        vw_fcp_Cons_Pymnt.startDatabaseRead                                                                                                                               //Natural: READ FCP-CONS-PYMNT BY CHECK-NBR-STOP-IND
        (
        "PND_PND_L2370",
        new Oc[] { new Oc("CHECK_NBR_STOP_IND", "ASC") }
        );
        PND_PND_L2370:
        while (condition(vw_fcp_Cons_Pymnt.readNextRow("PND_PND_L2370")))
        {
            pnd_Stop_Isn.setValue(vw_fcp_Cons_Pymnt.getAstISN("PND_PND_L2370"));                                                                                          //Natural: ASSIGN #STOP-ISN := *ISN ( ##L2370. )
            detail_Rec_D_Check_Issue_Date_A.setValueEdited(fcp_Cons_Pymnt_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED FCP-CONS-PYMNT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO CHECK-ISSUE-DATE-A
            //*  ......... CREATE THE DETAIL RECORD AND WRITE IT TO THE STOP FILE
            //* ******************* RL MAY 15, 2006 BEGIN *******************
            detail_Rec_D_Beginning_Check_Num.reset();                                                                                                                     //Natural: RESET DETAIL-REC-D.BEGINNING-CHECK-NUM DETAIL-REC-D.ENDING-CHECK-NUM #CHECK-NUMBER-N10
            detail_Rec_D_Ending_Check_Num.reset();
            pnd_Check_Number_N10.reset();
            short decideConditionsMet278 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN FCP-CONS-PYMNT.PYMNT-NBR-N7 GT 0 AND FCP-CONS-PYMNT.PYMNT-NBR-N3 = 170 OR = 220
            if (condition((fcp_Cons_Pymnt_Pymnt_Nbr_N7.greater(getZero()) && (fcp_Cons_Pymnt_Pymnt_Nbr_N3.equals(170) || fcp_Cons_Pymnt_Pymnt_Nbr_N3.equals(220)))))
            {
                decideConditionsMet278++;
                detail_Rec_D_Beginning_Check_Num.setValue(fcp_Cons_Pymnt_Pymnt_Nbr);                                                                                      //Natural: MOVE FCP-CONS-PYMNT.PYMNT-NBR TO DETAIL-REC-D.BEGINNING-CHECK-NUM DETAIL-REC-D.ENDING-CHECK-NUM #CHECK-NUMBER-N10
                detail_Rec_D_Ending_Check_Num.setValue(fcp_Cons_Pymnt_Pymnt_Nbr);
                pnd_Check_Number_N10.setValue(fcp_Cons_Pymnt_Pymnt_Nbr);
            }                                                                                                                                                             //Natural: WHEN FCP-CONS-PYMNT.PYMNT-CHECK-NBR GT 0 AND FCP-CONS-PYMNT.PYMNT-NBR-N7 EQ 0
            else if (condition(fcp_Cons_Pymnt_Pymnt_Check_Nbr.greater(getZero()) && fcp_Cons_Pymnt_Pymnt_Nbr_N7.equals(getZero())))
            {
                decideConditionsMet278++;
                detail_Rec_D_Beginning_Check_Num.setValue(fcp_Cons_Pymnt_Pymnt_Check_Nbr);                                                                                //Natural: MOVE FCP-CONS-PYMNT.PYMNT-CHECK-NBR TO DETAIL-REC-D.BEGINNING-CHECK-NUM DETAIL-REC-D.ENDING-CHECK-NUM #CHECK-NUMBER-N10
                detail_Rec_D_Ending_Check_Num.setValue(fcp_Cons_Pymnt_Pymnt_Check_Nbr);
                pnd_Check_Number_N10.setValue(fcp_Cons_Pymnt_Pymnt_Check_Nbr);
            }                                                                                                                                                             //Natural: WHEN FCP-CONS-PYMNT.PYMNT-NBR-N7 GT 0 AND FCP-CONS-PYMNT.PYMNT-NBR-N3 GT 0 AND NOT ( FCP-CONS-PYMNT.PYMNT-NBR-N3 = 170 OR = 220 )
            else if (condition(((fcp_Cons_Pymnt_Pymnt_Nbr_N7.greater(getZero()) && fcp_Cons_Pymnt_Pymnt_Nbr_N3.greater(getZero())) && ! ((fcp_Cons_Pymnt_Pymnt_Nbr_N3.equals(170) 
                || fcp_Cons_Pymnt_Pymnt_Nbr_N3.equals(220))))))
            {
                decideConditionsMet278++;
                //*  N7
                //*  N10
                getReports().write(0, "*** 10 DIGIT FROM ANOTHER SETTTLEMNT SYSTEM  ***",NEWLINE,"=",fcp_Cons_Pymnt_Pymnt_Check_Nbr,NEWLINE,"=",fcp_Cons_Pymnt_Pymnt_Nbr, //Natural: WRITE '*** 10 DIGIT FROM ANOTHER SETTTLEMNT SYSTEM  ***' / '=' FCP-CONS-PYMNT.PYMNT-CHECK-NBR / '=' FCP-CONS-PYMNT.PYMNT-NBR /
                    NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L2370"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2370"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet278 > 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                //*  N7
                //*  N10
                getReports().write(0, "*** MISSING OR INVALID CHECK NUMBER  ***",NEWLINE,"=",fcp_Cons_Pymnt_Pymnt_Check_Nbr,NEWLINE,"=",fcp_Cons_Pymnt_Pymnt_Nbr,         //Natural: WRITE '*** MISSING OR INVALID CHECK NUMBER  ***' / '=' FCP-CONS-PYMNT.PYMNT-CHECK-NBR / '=' FCP-CONS-PYMNT.PYMNT-NBR /
                    NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L2370"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2370"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-DECIDE
            //* ******************** RL MAY 15, 2006 END ********************
            cntlrec_Pnd_Ws_Stop_Amount.nadd(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                              //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO #WS-STOP-AMOUNT
            cntlrec_Pnd_Ws_Number_Of_Stopped_Payments.nadd(1);                                                                                                            //Natural: ADD 1 TO #WS-NUMBER-OF-STOPPED-PAYMENTS
            short decideConditionsMet301 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE FCP-CONS-PYMNT.CNR-CS-NEW-STOP-IND;//Natural: VALUE 'Y'
            if (condition((fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.equals("Y"))))
            {
                decideConditionsMet301++;
                pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Y.nadd(1);                                                                                                              //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-Y
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.equals("I"))))
            {
                decideConditionsMet301++;
                pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_I.nadd(1);                                                                                                              //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-I
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Other.nadd(1);                                                                                                          //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-OTHER
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Cnr_Cs_Rqust_Tme.setValueEdited(fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme,new ReportEditMask("99':'99':'999"));                                                     //Natural: MOVE EDITED FCP-CONS-PYMNT.CNR-CS-RQUST-TME ( EM = 99':'99':'999 ) TO #CNR-CS-RQUST-TME
            detail_Rec_D_Check_Amt_Decimal.setValue(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                      //Natural: MOVE FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO DETAIL-REC-D.CHECK-AMT-DECIMAL
            detail_Rec_D_Check_Issue_Date_A.setValueEdited(fcp_Cons_Pymnt_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED FCP-CONS-PYMNT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO DETAIL-REC-D.CHECK-ISSUE-DATE-A
            detail_Rec_D_Reason.setValue(fcp_Cons_Pymnt_Cntrct_Orgn_Cde);                                                                                                 //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE TO DETAIL-REC-D.REASON
            //* RL 5-23-06 BYPASS DTL BUT RESET IND & WRITE TRL
            if (condition(Global.getDATN().equals(20060523)))                                                                                                             //Natural: IF *DATN = 20060523
            {
                getReports().display(0, "CUST/ID",                                                                                                                        //Natural: DISPLAY 'CUST/ID' CUSTOMER-ID-NUM-D 'ACCOUNT/NBR' ACCOUNT-NUM 'RUN/DTE' RUN-DATE 'BEGIN/CHECK' BEGINNING-CHECK-NUM 'END/CHECK' ENDING-CHECK-NUM 'CHECK/AMOUNT' CHECK-AMT
                		detail_Rec_D_Customer_Id_Num_D,"ACCOUNT/NBR",
                		detail_Rec_D_Account_Num,"RUN/DTE",
                		detail_Rec_D_Run_Date,"BEGIN/CHECK",
                		detail_Rec_D_Beginning_Check_Num,"END/CHECK",
                		detail_Rec_D_Ending_Check_Num,"CHECK/AMOUNT",
                		detail_Rec_D_Check_Amt);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L2370"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2370"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(1, false, detail_Rec_D);                                                                                                             //Natural: WRITE WORK FILE 1 DETAIL-REC-D
            }                                                                                                                                                             //Natural: END-IF
            //*  ..... REPORT THE DETAIL RECORD
            //*  RL
            //*  RL
            //*  RL
            //*  RL
            //*  RL
            //*  RL
            //*  RL
            //*  RL
            //*  RL
            getReports().write(2, new FieldAttributes ("AD=D"),new TabSetting(2),cntlrec_Pnd_Ws_Number_Of_Stopped_Payments, new FieldAttributes ("AD=ODL"),               //Natural: WRITE ( RPT1 ) ( AD = D ) 002T CNTLREC.#WS-NUMBER-OF-STOPPED-PAYMENTS ( AD = ODL CD = NE NL = 3 ) 006T #CHECK-NUMBER-N10 ( AD = ODR CD = NE ) 017T FCP-CONS-PYMNT.PYMNT-CHECK-AMT ( AD = ODL EM = -ZZZZ,ZZZ.99 CD = NE ) 030T FCP-CONS-PYMNT.PYMNT-CHECK-DTE ( AD = ODL CD = NE ) 039T FCP-CONS-PYMNT.PYMNT-ACCTG-DTE ( AD = ODL CD = NE ) 048T FCP-CONS-PYMNT.CNR-CS-RQUST-DTE ( AD = ODL CD = NE ) 057T #CNR-CS-RQUST-TME ( AL = 8 CD = NE ) 066T FCP-CONS-PYMNT.CNR-CS-RQUST-USER-ID ( AD = ODL CD = NE ) 075T FCP-CONS-PYMNT.CNR-CS-PYMNT-INTRFCE-DTE ( AD = ODL CD = NE ) 085T FCP-CONS-PYMNT.CNTRCT-PPCN-NBR ( AL = 8 CD = NE ) 094T FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE ( CD = NE )
                Color.white, new NumericLength (3),new TabSetting(6),pnd_Check_Number_N10, new FieldAttributes ("AD=ODR"), Color.white,new TabSetting(17),fcp_Cons_Pymnt_Pymnt_Check_Amt, 
                new FieldAttributes ("AD=ODL"), new ReportEditMask ("-ZZZZ,ZZZ.99"), Color.white,new TabSetting(30),fcp_Cons_Pymnt_Pymnt_Check_Dte, new 
                FieldAttributes ("AD=ODL"), Color.white,new TabSetting(39),fcp_Cons_Pymnt_Pymnt_Acctg_Dte, new FieldAttributes ("AD=ODL"), Color.white,new 
                TabSetting(48),fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte, new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(57),pnd_Cnr_Cs_Rqust_Tme, new 
                AlphanumericLength (8), Color.white,new TabSetting(66),fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id, new FieldAttributes ("AD=ODL"), Color.white,new 
                TabSetting(75),fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Intrfce_Dte, new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(85),fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr, 
                new AlphanumericLength (8), Color.white,new TabSetting(94),fcp_Cons_Pymnt_Cntrct_Payee_Cde, Color.white);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L2370"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2370"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            PND_PND_L3310:                                                                                                                                                //Natural: GET FCP-CONS-PYMNT #STOP-ISN
            vw_fcp_Cons_Pymnt.readByID(pnd_Stop_Isn.getLong(), "PND_PND_L3310");
            fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.setValue("I");                                                                                                             //Natural: ASSIGN FCP-CONS-PYMNT.CNR-CS-NEW-STOP-IND := 'I'
            vw_fcp_Cons_Pymnt.updateDBRow("PND_PND_L3310");                                                                                                               //Natural: UPDATE ( ##L3310. )
            pnd_Wsarea_Pnd_Et_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ET-CNT
            if (condition(pnd_Wsarea_Pnd_Et_Cnt.greaterOrEqual(pnd_Force_Et_Count)))                                                                                      //Natural: IF #ET-CNT GE #FORCE-ET-COUNT
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Wsarea_Pnd_Et_Cnt.reset();                                                                                                                            //Natural: RESET #ET-CNT
            }                                                                                                                                                             //Natural: END-IF
            //* *
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ......... MAKE SURE ALL THE UPDATED RECORDS ARE COMMITED
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  SELECT-ALL-NEW-STOPS
    }
    private void sub_Select_All_New_Stops_N10() throws Exception                                                                                                          //Natural: SELECT-ALL-NEW-STOPS-N10
    {
        if (BLNatReinput.isReinput()) return;

        vw_fcp_Cons_Pymnt.startDatabaseRead                                                                                                                               //Natural: READ FCP-CONS-PYMNT BY PYMNT-NBR-STOP-IND
        (
        "PND_PND_L3500",
        new Oc[] { new Oc("PYMNT_NBR_STOP_IND", "ASC") }
        );
        PND_PND_L3500:
        while (condition(vw_fcp_Cons_Pymnt.readNextRow("PND_PND_L3500")))
        {
            if (condition(!(fcp_Cons_Pymnt_Pymnt_Nbr_N3.equals(220))))                                                                                                    //Natural: ACCEPT IF PYMNT-NBR-N3 = 220
            {
                continue;
            }
            pnd_Stop_Isn.setValue(vw_fcp_Cons_Pymnt.getAstISN("PND_PND_L3500"));                                                                                          //Natural: ASSIGN #STOP-ISN := *ISN ( ##L3500. )
            detail_Rec_D_Check_Issue_Date_A.setValueEdited(fcp_Cons_Pymnt_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED FCP-CONS-PYMNT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO CHECK-ISSUE-DATE-A
            cntlrec_Pnd_Ws_Stop_Amount.nadd(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                              //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO #WS-STOP-AMOUNT
            cntlrec_Pnd_Ws_Number_Of_Stopped_Payments.nadd(1);                                                                                                            //Natural: ADD 1 TO #WS-NUMBER-OF-STOPPED-PAYMENTS
            short decideConditionsMet362 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE FCP-CONS-PYMNT.CNR-CS-NEW-STOP-IND;//Natural: VALUE 'Y'
            if (condition((fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.equals("Y"))))
            {
                decideConditionsMet362++;
                pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Y.nadd(1);                                                                                                              //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-Y
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.equals("I"))))
            {
                decideConditionsMet362++;
                pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_I.nadd(1);                                                                                                              //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-I
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Other.nadd(1);                                                                                                          //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-OTHER
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Cnr_Cs_Rqust_Tme.setValueEdited(fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme,new ReportEditMask("99':'99':'999"));                                                     //Natural: MOVE EDITED FCP-CONS-PYMNT.CNR-CS-RQUST-TME ( EM = 99':'99':'999 ) TO #CNR-CS-RQUST-TME
            //*  ......... CREATE THE DETAIL RECORD AND WRITE IT TO THE STOP FILE
            detail_Rec_D_Beginning_Check_Num.setValue(fcp_Cons_Pymnt_Pymnt_Nbr);                                                                                          //Natural: MOVE FCP-CONS-PYMNT.PYMNT-NBR TO DETAIL-REC-D.BEGINNING-CHECK-NUM DETAIL-REC-D.ENDING-CHECK-NUM #CHECK-NUMBER-N10
            detail_Rec_D_Ending_Check_Num.setValue(fcp_Cons_Pymnt_Pymnt_Nbr);
            pnd_Check_Number_N10.setValue(fcp_Cons_Pymnt_Pymnt_Nbr);
            detail_Rec_D_Check_Amt_Decimal.setValue(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                      //Natural: MOVE FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO DETAIL-REC-D.CHECK-AMT-DECIMAL
            detail_Rec_D_Check_Issue_Date_A.setValueEdited(fcp_Cons_Pymnt_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED FCP-CONS-PYMNT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO DETAIL-REC-D.CHECK-ISSUE-DATE-A
            detail_Rec_D_Reason.setValue(fcp_Cons_Pymnt_Cntrct_Orgn_Cde);                                                                                                 //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE TO DETAIL-REC-D.REASON
            //* RL 5-23-06 BYPASS DTL BUT RESET IND & WRITE TRL
            if (condition(Global.getDATN().equals(20060523)))                                                                                                             //Natural: IF *DATN = 20060523
            {
                getReports().write(0, "10 DIGIT-","ACCT:",detail_Rec_D_Account_Num,"DT:",detail_Rec_D_Run_Date,"CHK:",detail_Rec_D_Beginning_Check_Num,                   //Natural: WRITE '10 DIGIT-' 'ACCT:' ACCOUNT-NUM 'DT:' RUN-DATE 'CHK:' BEGINNING-CHECK-NUM 'AMT:' CHECK-AMT
                    "AMT:",detail_Rec_D_Check_Amt);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L3500"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3500"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(1, false, detail_Rec_D);                                                                                                             //Natural: WRITE WORK FILE 1 DETAIL-REC-D
            }                                                                                                                                                             //Natural: END-IF
            //*  ..... REPORT THE DETAIL RECORD
            //*  RL MAY 15, 2006
            getReports().write(2, new FieldAttributes ("AD=D"),new TabSetting(2),cntlrec_Pnd_Ws_Number_Of_Stopped_Payments, new FieldAttributes ("AD=ODL"),               //Natural: WRITE ( RPT1 ) ( AD = D ) 002T CNTLREC.#WS-NUMBER-OF-STOPPED-PAYMENTS ( AD = ODL CD = NE NL = 3 ) 006T #CHECK-NUMBER-N10 ( AD = ODR CD = NE ) 017T FCP-CONS-PYMNT.PYMNT-CHECK-AMT ( AD = ODL EM = -ZZZZ,ZZZ.99 CD = NE ) 030T FCP-CONS-PYMNT.PYMNT-CHECK-DTE ( AD = ODL CD = NE ) 039T FCP-CONS-PYMNT.PYMNT-ACCTG-DTE ( AD = ODL CD = NE ) 048T FCP-CONS-PYMNT.CNR-CS-RQUST-DTE ( AD = ODL CD = NE ) 057T #CNR-CS-RQUST-TME ( AL = 8 CD = NE ) 066T FCP-CONS-PYMNT.CNR-CS-RQUST-USER-ID ( AD = ODL CD = NE ) 075T FCP-CONS-PYMNT.CNR-CS-PYMNT-INTRFCE-DTE ( AD = ODL CD = NE ) 085T FCP-CONS-PYMNT.CNTRCT-PPCN-NBR ( AL = 8 CD = NE ) 094T FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE ( CD = NE )
                Color.white, new NumericLength (3),new TabSetting(6),pnd_Check_Number_N10, new FieldAttributes ("AD=ODR"), Color.white,new TabSetting(17),fcp_Cons_Pymnt_Pymnt_Check_Amt, 
                new FieldAttributes ("AD=ODL"), new ReportEditMask ("-ZZZZ,ZZZ.99"), Color.white,new TabSetting(30),fcp_Cons_Pymnt_Pymnt_Check_Dte, new 
                FieldAttributes ("AD=ODL"), Color.white,new TabSetting(39),fcp_Cons_Pymnt_Pymnt_Acctg_Dte, new FieldAttributes ("AD=ODL"), Color.white,new 
                TabSetting(48),fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte, new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(57),pnd_Cnr_Cs_Rqust_Tme, new 
                AlphanumericLength (8), Color.white,new TabSetting(66),fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id, new FieldAttributes ("AD=ODL"), Color.white,new 
                TabSetting(75),fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Intrfce_Dte, new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(85),fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr, 
                new AlphanumericLength (8), Color.white,new TabSetting(94),fcp_Cons_Pymnt_Cntrct_Payee_Cde, Color.white);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L3500"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3500"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            PND_PND_L4140:                                                                                                                                                //Natural: GET FCP-CONS-PYMNT #STOP-ISN
            vw_fcp_Cons_Pymnt.readByID(pnd_Stop_Isn.getLong(), "PND_PND_L4140");
            fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.setValue("I");                                                                                                             //Natural: ASSIGN FCP-CONS-PYMNT.CNR-CS-NEW-STOP-IND := 'I'
            vw_fcp_Cons_Pymnt.updateDBRow("PND_PND_L4140");                                                                                                               //Natural: UPDATE ( ##L4140. )
            pnd_Wsarea_Pnd_Et_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ET-CNT
            if (condition(pnd_Wsarea_Pnd_Et_Cnt.greaterOrEqual(pnd_Force_Et_Count)))                                                                                      //Natural: IF #ET-CNT GE #FORCE-ET-COUNT
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Wsarea_Pnd_Et_Cnt.reset();                                                                                                                            //Natural: RESET #ET-CNT
            }                                                                                                                                                             //Natural: END-IF
            //* *
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ......... MAKE SURE ALL THE UPDATED RECORDS ARE COMMITED
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  SELECT-ALL-NEW-STOPS-N10
    }
    private void sub_Create_Trailer_For_Stop_File() throws Exception                                                                                                      //Natural: CREATE-TRAILER-FOR-STOP-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        if (condition(cntlrec_Pnd_Ws_Number_Of_Stopped_Payments.greater(getZero())))                                                                                      //Natural: IF #WS-NUMBER-OF-STOPPED-PAYMENTS GT 0
        {
            trailer_Rec_T_Total_Stops.setValue(cntlrec_Pnd_Ws_Number_Of_Stopped_Payments);                                                                                //Natural: ASSIGN TRAILER-REC-T.TOTAL-STOPS := #WS-NUMBER-OF-STOPPED-PAYMENTS
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(1, false, trailer_Rec_T);                                                                                                                    //Natural: WRITE WORK FILE 1 TRAILER-REC-T
        //*  CREATE-TRAILER-FOR-STOP-FILE
    }
    private void sub_Do_End_Of_Program() throws Exception                                                                                                                 //Natural: DO-END-OF-PROGRAM
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_All.compute(new ComputeParameters(false, pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_All), pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Y.add(pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_I).add(pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Other)); //Natural: ASSIGN #CNT-RECS-MARKED-AS-ALL := #CNT-RECS-MARKED-AS-Y + #CNT-RECS-MARKED-AS-I + #CNT-RECS-MARKED-AS-OTHER
        //*  ..... REPORT STATISTICS ON THE REPORT
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( RPT1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"END OF RUN at time:",Global.getTIME(),NEWLINE,NEWLINE,NEWLINE,"Number of stopped payments....:",cntlrec_Pnd_Ws_Number_Of_Stopped_Payments,  //Natural: WRITE ( RPT1 ) /// 'END OF RUN at time:' *TIME // / 'Number of stopped payments....:' #WS-NUMBER-OF-STOPPED-PAYMENTS ( AD = ODL ) / 'Total amount stopped .........:' #WS-STOP-AMOUNT ( AD = ODL EM = Z,ZZZ,ZZZ,ZZZ,ZZZ.99 )
            new FieldAttributes ("AD=ODL"),NEWLINE,"Total amount stopped .........:",cntlrec_Pnd_Ws_Stop_Amount, new FieldAttributes ("AD=ODL"), new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  ..... REPORT STATISTICS ON SYSOUT
        getReports().write(0, Global.getPROGRAM(),"END OF RUN at time:",Global.getTIME(),NEWLINE,NEWLINE,"Records read from Database    :",pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_All,NEWLINE,"Records read with STOP-IND 'Y':",pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Y,NEWLINE,"Records read with STOP-IND 'I':",pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_I,NEWLINE,"Records read with STOP-IND any:",pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_Other,NEWLINE,"total amount stopped .........:",cntlrec_Pnd_Ws_Stop_Amount,  //Natural: WRITE *PROGRAM 'END OF RUN at time:' *TIME / / 'Records read from Database    :' #CNT-RECS-MARKED-AS-ALL / 'Records read with STOP-IND "Y":' #CNT-RECS-MARKED-AS-Y / 'Records read with STOP-IND "I":' #CNT-RECS-MARKED-AS-I / 'Records read with STOP-IND any:' #CNT-RECS-MARKED-AS-OTHER / 'total amount stopped .........:' #WS-STOP-AMOUNT ( EM = Z,ZZZ,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  ..... IS THE WHOLE EQUAL THE PARTS?
        if (condition(cntlrec_Pnd_Ws_Number_Of_Stopped_Payments.notEquals(pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_All)))                                                        //Natural: IF #WS-NUMBER-OF-STOPPED-PAYMENTS NE #CNT-RECS-MARKED-AS-ALL
        {
            pnd_Abend_Cde.setValue(33);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 33
            getReports().write(0, Global.getPROGRAM(),Global.getTIME(),"A programs Internal error occured:",NEWLINE,"The number of stops counted by 'stop-ind' does not equal", //Natural: WRITE *PROGRAM *TIME 'A programs Internal error occured:' / 'The number of stops counted by "stop-ind" does not equal' / 'the number of stops counted as a whole:' / '#WS-NUMBER-OF-STOPPED-PAYMENTS NE #CNT-RECS-MARKED-AS-ALL' / '=' #WS-NUMBER-OF-STOPPED-PAYMENTS / '=' #CNT-RECS-MARKED-AS-ALL / 'The program abends with abend code:' #ABEND-CDE
                NEWLINE,"the number of stops counted as a whole:",NEWLINE,"#WS-NUMBER-OF-STOPPED-PAYMENTS NE #CNT-RECS-MARKED-AS-ALL",NEWLINE,"=",cntlrec_Pnd_Ws_Number_Of_Stopped_Payments,
                NEWLINE,"=",pnd_Wsarea_Pnd_Cnt_Recs_Marked_As_All,NEWLINE,"The program abends with abend code:",pnd_Abend_Cde);
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                          //Natural: TERMINATE #ABEND-CDE
        }                                                                                                                                                                 //Natural: END-IF
        cntlrec_Create_Date.setValue(Global.getDATU());                                                                                                                   //Natural: ASSIGN CNTLREC.CREATE-DATE := *DATU
        cntlrec_Create_Time.setValue(Global.getTIME());                                                                                                                   //Natural: ASSIGN CNTLREC.CREATE-TIME := *TIME
        getWorkFiles().write(2, false, cntlrec);                                                                                                                          //Natural: WRITE WORK FILE 2 CNTLREC
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,"Control record is written with the following data:",NEWLINE,NEWLINE,"CREATE-DATE...............:",                 //Natural: WRITE /// 'Control record is written with the following data:' / / 'CREATE-DATE...............:' CNTLREC.CREATE-DATE / 'CREATE-TIME...............:' CNTLREC.CREATE-TIME / 'NUMBER OF STOPPED PAYMENTS:' CNTLREC.#WS-NUMBER-OF-STOPPED-PAYMENTS / 'STOP-AMOUNT...............:' CNTLREC.#WS-STOP-AMOUNT
            cntlrec_Create_Date,NEWLINE,"CREATE-TIME...............:",cntlrec_Create_Time,NEWLINE,"NUMBER OF STOPPED PAYMENTS:",cntlrec_Pnd_Ws_Number_Of_Stopped_Payments,
            NEWLINE,"STOP-AMOUNT...............:",cntlrec_Pnd_Ws_Stop_Amount);
        if (Global.isEscape()) return;
        //*  DO-END-OF-PROGRAM
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Wsarea_Pnd_Page_Number_Rpt1.setValue(getReports().getPageNumberDbs(2));                                                                           //Natural: ASSIGN #PAGE-NUMBER-RPT1 := *PAGE-NUMBER ( RPT1 )
                    //*  FORMAT ZP=OFF SG=OFF KD=OFF IP=OFF
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    //*  RL
                    getReports().write(2, new FieldAttributes ("AD=D"),new TabSetting(2),pnd_Program, new FieldAttributes ("AD=ODL"), Color.white,new                     //Natural: WRITE ( RPT1 ) ( AD = D ) 002T #PROGRAM ( AD = ODL CD = NE ) 043T 'CONSOLIDATED PAYMENT SYSTEM' ( BL ) 103T 'Page Number:' ( BL ) 116T #PAGE-NUMBER-RPT1 ( AD = OD CD = NE ) / 002T *DATX ( AD = OD EM = LLL' 'DD','YY CD = NE ) 043T 'List of FUB STOP FILE Checks' ( BL ) 103T *TIMX ( AD = OD EM = HH':'II' 'AP CD = NE ) / 038T 'Created On Date:' ( BL ) 055T #RPT-DATE ( AD = ODL CD = NE ) 064T 'Time:' ( BL ) 070T #RPT-TIME ( AD = ODL CD = NE ) / / 001T 'Stop Check' ( BL ) 020T 'CHECK' ( BL ) 030T 'CHECK' ( BL ) 039T 'CHECK' ( BL ) 048T '..... STOP REQUEST ....... INTERFACE' ( BL ) 085T 'PPCN' ( BL ) / 001T 'nmbr Number' ( BL ) 020T 'AMOUNT' ( BL ) 030T 'DATE' ( BL ) 039T 'ACCT DTE DATE' ( BL ) 057T 'TIME' ( BL ) 066T 'USER-ID' ( BL ) 075T 'DATE' ( BL ) 085T 'NBR ' ( BL ) 093T 'PAYEE' ( BL ) / 001T '---- ---------- ------------ -------- -------- -------- -------- -------- --------' 085T '-------------'
                        TabSetting(43),"CONSOLIDATED PAYMENT SYSTEM", Color.blue,new TabSetting(103),"Page Number:", Color.blue,new TabSetting(116),pnd_Wsarea_Pnd_Page_Number_Rpt1, 
                        new FieldAttributes ("AD=OD"), Color.white,NEWLINE,new TabSetting(2),Global.getDATX(), new FieldAttributes ("AD=OD"), new ReportEditMask 
                        ("LLL' 'DD','YY"), Color.white,new TabSetting(43),"List of FUB STOP FILE Checks", Color.blue,new TabSetting(103),Global.getTIMX(), 
                        new FieldAttributes ("AD=OD"), new ReportEditMask ("HH':'II' 'AP"), Color.white,NEWLINE,new TabSetting(38),"Created On Date:", Color.blue,new 
                        TabSetting(55),pnd_Rpt_Date, new FieldAttributes ("AD=ODL"), Color.white,new TabSetting(64),"Time:", Color.blue,new TabSetting(70),pnd_Rpt_Time, 
                        new FieldAttributes ("AD=ODL"), Color.white,NEWLINE,NEWLINE,new TabSetting(1),"Stop Check", Color.blue,new TabSetting(20),"CHECK", 
                        Color.blue,new TabSetting(30),"CHECK", Color.blue,new TabSetting(39),"CHECK", Color.blue,new TabSetting(48),"..... STOP REQUEST ....... INTERFACE", 
                        Color.blue,new TabSetting(85),"PPCN", Color.blue,NEWLINE,new TabSetting(1),"nmbr Number", Color.blue,new TabSetting(20),"AMOUNT", 
                        Color.blue,new TabSetting(30),"DATE", Color.blue,new TabSetting(39),"ACCT DTE DATE", Color.blue,new TabSetting(57),"TIME", Color.blue,new 
                        TabSetting(66),"USER-ID", Color.blue,new TabSetting(75),"DATE", Color.blue,new TabSetting(85),"NBR ", Color.blue,new TabSetting(93),"PAYEE", 
                        Color.blue,NEWLINE,new TabSetting(1),"---- ---------- ------------ -------- -------- -------- -------- -------- --------",new TabSetting(85),
                        "-------------");
                    //* *  017T 'Check'(BL)
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP ( RPT1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(0, "CUST/ID",
        		detail_Rec_D_Customer_Id_Num_D,"ACCOUNT/NBR",
        		detail_Rec_D_Account_Num,"RUN/DTE",
        		detail_Rec_D_Run_Date,"BEGIN/CHECK",
        		detail_Rec_D_Beginning_Check_Num,"END/CHECK",
        		detail_Rec_D_Ending_Check_Num,"CHECK/AMOUNT",
        		detail_Rec_D_Check_Amt);
    }
}
